<?php
$use_smtp = true;
ini_set('display_errors', 1);

date_default_timezone_set('Asia/Kuala_Lumpur');

$paths = array(
    realpath(dirname(__FILE__) . '/../library'),
    '.',
);

set_include_path(implode(PATH_SEPARATOR, $paths));

$path = realpath(dirname(__FILE__) . '/../');

require_once($path . '/library/Zend/Loader/Autoloader.php');
$loader = Zend_Loader_Autoloader::getInstance();

$config = $config = new Zend_Config_Ini($path . '/application/configs/application.ini', 'development');

define('SMTP_SEND', $config->constants->SMTP_SEND);
define('SMTP_USERNAME', $config->constants->SMTP_USERNAME);
define('SMTP_PASSWORD', $config->constants->SMTP_PASSWORD);
define('SMTP_SERVER', $config->constants->SMTP_SERVER);
define('SMTP_FROM', $config->constants->SMTP_FROM);
define('SMTP_FROM_EMAIL', $config->constants->SMTP_FROM_EMAIL);

$db = Zend_Db::factory($config->resources->db->adapter, $config->resources->db->params);

$select = $db->select()
    ->from(array('eq'=>'email_que'))
    ->where('eq.date_send is null')
    ->where("eq.recepient_email LIKE '%@%.%'")
    ->where('eq.retry_count <= 3')
    ->limit(15,0);

$stmt = $db->query($select);
$row = $stmt->fetchAll();
//print_r($row);"<pre>";exit;

if (SMTP_SEND == 0) die;

try {
    foreach ($row as $emailData) {
        if ($use_smtp == true) {
            $config = array('auth' => 'login', 'username' => SMTP_USERNAME, 'password' => SMTP_PASSWORD, 'ssl' => 'tls','port'=>'587');

            $mailTransport = new Zend_Mail_Transport_Smtp(SMTP_SERVER, $config);
            Zend_Mail::setDefaultTransport($mailTransport);

        } else {
            /*
             * Using server's sendmail
             */
            $transport = new Zend_Mail_Transport_Smtp('localhost');
            Zend_Mail::setDefaultTransport($transport);
        }


        $email = $emailData['recepient_email'];
        $subject = $emailData['subject'];
        $message = $emailData['content'];
        $smtp_from_email = SMTP_FROM_EMAIL;
        $smtp_from = SMTP_FROM;

//        $emailcc = $emailData['emailcc'];
//        $emailbcc = $emailData['emailbcc'];
//        $smtp_from_email = $emailData['smtp_from_email'];
//        $smtp_from = $emailData['smtp_from'];

        //$attachment_path = $emailData['attachment_path'];
        //$attachment_filename = $emailData['attachment_filename'];

        //get email attachment
        $select = $db->select()
            ->from(array('eq' => 'email_que_attachment'))
            ->where('eq.eqa_emailque_id = ' . $emailData['id']);

        $stmt = $db->query($select);
        $emailAttachment = $stmt->fetchAll();

        //wrap $message with inline template
        $wraptemplate = 0; //change to 0 if dont want

        if ($wraptemplate == 1) {
            $message = "<div id=\"mailtpl\" style=\"background:#f0fbfc; margin-bottom:20px; padding:20px;\">
					<div class=\"logo\" style=\"padding-left:10px; margin-bottom:10px;\"><img src=\"http://rmos.mtcsb.my/images/logo_new.png\" alt=\"OUM\" /></div>
					<div class=\"wrap\" style=\"background:#fff; border:1px solid #5997a9; font-family: 'Lucida Grande', Arial, sans-serif; padding:0; margin:0;\">	
						<!--<div class=\"head\"><h2 style=\"background:#0897c2;  padding:6px; border-bottom:1px solid #59a9a9; margin:0;\"><a href=\"http://www.inceif.org\" style=\"text-decoration:none; text-transform:uppercase; color:#fff; font: bold 16px/16px \"Lucida Grande\"\">INCEIF</a></h2></div>-->
						<div class=\"body\" style=\"padding:10px; font:normal 11px \"Lucida Grande\", Arial; color:#545a5a; line-height:1.4em\">
							".$message."
						</div>
						<div class=\"footer\" style=\"padding:10px; margin-top:10px; border-top:1px solid #eee; font-size:9px; color:#ccc;\">
							&copy; ".date('Y')." OUM&reg; Jalan Tun Ismail, Kuala Lumpur, 50480 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur
						</div>
					</div>
				</div>";
        }


        //Prepare email

        if (count($emailAttachment) > 0) {
            $mail = new Zend_Mail();
            $mail->setFrom($smtp_from_email, $smtp_from);
            $mail->addTo($email);
            $mail->setSubject($subject);
            $mail->setBodyHtml($message);
//            $mail->addCc($emailcc);
//            $mail->addBcc($emailbcc);

            foreach ($emailAttachment as $emailAttachmentLoop) {
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $fileContents = file_get_contents($emailAttachmentLoop['eqa_path']);
                $file = $mail->createAttachment($fileContents);
                $file->filename = $emailAttachmentLoop['eqa_filename'];
            }
        } else {
            $mail = new Zend_Mail();
            $mail->addTo($email);
            $mail->setSubject($subject);
            $mail->setBodyHtml($message);
            $mail->setFrom($smtp_from_email, $smtp_from);
//            $mail->addCc($emailcc);
//            $mail->addBcc($emailbcc);

        }

        //Send it!
        $sent = true;
        try {
            $mail->send();
        } catch (Exception $e) {

            $sent = false;
            echo $e->getMessage();
            echo "<br />";
        }

        //Do stuff (display error message, log it, redirect user, etc)
        if ($sent) {
            //Mail was sent successfully.
            echo "Mail Sent " . $emailData['id'] . " : " . date('Y-m-d H:i:s') . "\n";

            //update table
            $data = array(
                'date_send' => date("Y-m-d H:i:s")
            );

            $db->update('email_que', $data, array('id = ?' => $emailData['id']));

            //if comm_rec_id
            if (isset($emailData['comm_rec_id']) && $emailData['comm_rec_id'] != '') {
                $db->update('comm_compose_recipients', array('cr_status' => 1), 'cr_id = ' . $emailData['comm_rec_id']);
            }

        } else {
            //Mail failed to send.
            echo "Mail Send Failed " . $emailData['id'] . " : " . date('Y-m-d H:i:s') . "\n";

            //update error try
            $data = array(
                'retry_count' => $emailData['retry_count'] + 1
            );

            $db->update('email_que', $data, 'id = ' . $emailData['id']);
        }
    }

} catch (Zend_Exception $e) {
    //Do something with exception
    echo $e->getMessage();
}


exit;