<?php
ini_set('display_errors',1);

exit;

//decommissioned - dependencies moved to email_que
//munzir
//15/9/2014

date_default_timezone_set('Asia/Kuala_Lumpur');

$paths = array(
	    realpath(dirname(__FILE__) . '/../library'),
	    '.',
	);

set_include_path(implode(PATH_SEPARATOR, $paths));

$path = realpath(dirname(__FILE__).'/../');

require_once($path.'/library/Zend/Loader/Autoloader.php');
$loader = Zend_Loader_Autoloader::getInstance();

$config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');


$db = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);


//------------------------------------------------------------------------------------
$select = $db->select()->from(array('a'=>'comm_compose_recipients'))->where('a.cr_status = 0')->limit(500);

$rows = $db->fetchAll($select);

$use_smtp = true;

try 
{
	if( $use_smtp == true )
	{
		/*
		 * Using SMTP server
		 */
		 $configdb = array(
				'auth' => 'login',
				'username' => "inceif@meteor.com.my",
				'password' => 'meteor123',
				'ssl' => 'tls',
				'port' => "587"
			);

		$mailTransport = new Zend_Mail_Transport_Smtp("smtp.gmail.com", $configdb);
		Zend_Mail::setDefaultTransport($mailTransport);
		
	}
	else
	{
		/*
		 * Using server's sendmail
		 */
		$transport = new Zend_Mail_Transport_Smtp('localhost');
		Zend_Mail::setDefaultTransport($transport);
	}

	foreach ($rows as $row)
	{
		$email = $row['cr_email'];
		$subject = $row['cr_subject'];
		$message = $row['cr_content'];
		$attachment_path = $config->constants->DOCUMENT_PATH.'/attachments/';	 
		$attachment_filename = $row['cr_attachment_filename'];
		$attachment_realname = $row['cr_attachment_name'];

		
		//wrap $message with inline template
		$wraptemplate = 1; //change to 0 if dont want

		if ( $wraptemplate == 1 )
		{
			$message = "<div id=\"mailtpl\" style=\"background:#f0fcfa; margin-bottom:20px; padding:20px;\">
				<div class=\"wrap\" style=\"background:#fff; border:1px solid #5997a9; font-family: 'Lucida Grande', Arial, sans-serif; padding:0; margin:0;\">	
					<div class=\"head\"><h2 style=\"background:#0897c2;  padding:6px; border-bottom:1px solid #59a9a9; margin:0;\"><a href=\"http://www.inceif.org\" style=\"text-decoration:none; text-transform:uppercase; color:#fff; font: bold 16px/16px \"Lucida Grande\"\">INCEIF</a></h2></div>
					<div class=\"body\" style=\"padding:10px; font:normal 11px \"Lucida Grande\", Arial; color:#545a5a; line-height:1.4em\">
						".$message."
					</div>
					<div class=\"footer\" style=\"padding:10px; margin-top:10px; border-top:1px solid #eee; font-size:9px; color:#ccc;\">
						&copy; ".date('Y')." INCEIF&reg; Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)
					</div>
				</div>
			</div>";
		}

		//Prepare email				
		if( $attachment_filename != '' && file_exists( $attachment_path.$attachment_filename) )
		{
			$fileContents = file_get_contents($attachment_path.$attachment_filename);
			
			$mail = new Zend_Mail();
			$mail->setType(Zend_Mime::MULTIPART_RELATED);
			$mail->setFrom('do_not_reply@inceif.org', 'INCEIF e-University');			
			$mail->addTo($email); 
			$mail->setSubject($subject);
			$mail->setBodyHtml($message);	
			$file = $mail->createAttachment($fileContents);
			$file->filename = $attachment_filename;
			
		}else{
			
			$mail = new Zend_Mail();
			$mail->addTo($email);
			$mail->setSubject($subject);
			$mail->setBodyHtml($message);
			$mail->setFrom('do_not_reply@inceif.org', 'INCEIF e-University');
		
		}
		
		echo 'sent <br />';
		
		
		//Send it!
		$sent = true;
		try 
		{
			$mail->send();
		}
		catch (Exception $e)
		{
			$sent = false;
			echo $e->getMessage();
			echo "<br />";
		}
		
		if($sent)
		{
			//update table
			$data = array(
				'cr_datesent' => date("Y-m-d H:i:s"),
				'cr_status'	=> 1
			);
			
			$db->update('comm_compose_recipients', $data,'cr_id = '.$row['cr_id']);  
		}
		
	}
	
} catch (Zend_Exception $e){
	//Do something with exception
	echo $e->getMessage();
}    


exit;
?>