<?php 
$paths = array(
		realpath(dirname(__FILE__) . '/../library'),
		'.',
	);
		

set_include_path(implode(PATH_SEPARATOR, $paths));
		
$path = realpath(dirname(__FILE__).'/../');
		
require_once($path.'/library/Zend/Loader/Autoloader.php');
$loader = Zend_Loader_Autoloader::getInstance();
		
$config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');
$db = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
	
$dir = '/data/cdn/migrate/AppDoc';

$i=0;

$db->delete("migrate_applicant_files");

foreach (new DirectoryIterator($dir) as $fileInfo) 
{
	if($fileInfo->isDot()) continue;
	$i++;	
    	echo $i.'. '.$fileInfo->getFilename() . " ( ".filesize($dir.'/'.$fileInfo->getFilename()).")<br>\n";

	$data = array(	'filename' => $fileInfo->getFilename(),
			'filesize' => filesize($dir.'/'.$fileInfo->getFilename())	
			);

	$db->insert("migrate_applicant_files", $data);
}