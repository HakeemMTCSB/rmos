<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class erpUserSync{
    
    public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
    }
    
    public function process(){
        echo 'erp intergration start -> ';
        //echo '';
        $this->truncateTempTable();
        
        $this->conn->beginTransaction();
        try{
            $this->importDataToTempTable();
            echo 'import data success  -> ';
            $this->conn->commit();
        } catch (Exception $ex) {
            echo 'import data failed  -> ';
            
            $this->conn->rollBack();
            
            //todo insert log
            $logData = array(
                'erp_log_process'=>'import data to temp table', 
                'erp_log_message'=>$ex, 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $this->insertLog($logData);
        }
        
        $this->conn->beginTransaction();
        try{
            echo 'get imported data success  -> ';
            
            $data = $this->getTempData();
            $this->conn->commit();
        } catch (Exception $ex) {
            echo 'get imported data failed  -> ';
            
            $this->conn->rollBack();
            
            //todo insert log
            $logData = array(
                'erp_log_process'=>'get data from temp table', 
                'erp_log_message'=>$ex, 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $this->insertLog($logData);
        }
        
        if ($data){
            foreach ($data as $dataLoop){
                var_dump($dataLoop);
                $userData = array(
                    'FirstName'=>$dataLoop['Salutation'].' '.$dataLoop['First_Name'].' '.$dataLoop['Last_Name'], 
                    'FullName'=>$dataLoop['Salutation'].' '.$dataLoop['First_Name'].' '.$dataLoop['Last_Name'], 
                    'Phone'=>$dataLoop['Phone_No_Office'], 
                    'Mobile'=>$dataLoop['Phone_No_Mobile'], 
                    'Email'=>$dataLoop['Email_Address'], 
                    'Active'=>$dataLoop['Status']=='Active' ? 1:0, 
                    'UpdDate'=>date('Y-m-d h:i:s'), 
                    'UpdUser'=>1, 
                    'StaffAcademic'=>$dataLoop['Staff_type']=='Academician' ? 0:1,
                );
                
                echo 'insert imported data success  -> ';
                    
                $this->insertUserData($userData);

                //todo insert log
                $logData = array(
                    'erp_log_process'=>'insert data', 
                    'erp_log_message'=>'success', 
                    'erp_log_date'=>date('Y-m-d h:i:s')
                );
                $this->insertLog($logData);
            }
        }else{
            echo 'empty data ->';
            
            //todo insert log
            $logData = array(
                'erp_log_process'=>'empty data', 
                'erp_log_message'=>'-', 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $this->insertLog($logData);
        }
        
        $this->conn->beginTransaction();
        try{
            echo 'truncate temp table success -> process complete.';
            
            $this->truncateTempTable();
            $this->conn->commit();
            
            //todo insert log
            $logData = array(
                'erp_log_process'=>'truncate temp table', 
                'erp_log_message'=>'process success', 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $this->insertLog($logData);
            
        } catch (Exception $ex) {
            echo 'truncate temp table failed -> process uncomplete.';
            
            $this->conn->rollBack();
            
            //todo insert log
            $logData = array(
                'erp_log_process'=>'truncate temp table', 
                'erp_log_message'=>$ex, 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $this->insertLog($logData);
        }
        
        exit;
    }
    
    public function importDataToTempTable(){
        
        $date = date('dmY', strtotime('-1 day', strtotime(date('Y-m-d'))));
        //echo $date; exit;
        //$date = 'ddmmyyyy';
        $url = '/var/www/html/sms/documents/erpmount/';
        
        $sql = "LOAD DATA INFILE '".$this->config->constants->ERP_URL."staff_info_".$date.".csv' ";
        $sql .= "INTO TABLE erp_user_sync ";
        $sql .= "FIELDS TERMINATED BY ',' ";
        $sql .= "ENCLOSED BY ";
        $sql .= "'";
        $sql .= '"';
        $sql .= "' ";
        $sql .= "LINES TERMINATED BY '\n'";
        $sql .= "IGNORE 1 ROWS";
        
        $execute = $this->conn->query($sql);
        return $execute;
    }
    
    public function truncateTempTable(){
        $sql = "TRUNCATE TABLE erp_user_sync";
        $execute = $this->conn->query($sql);
        return $execute;
    }
    
    public function getTempData(){
        $sql = $this->conn->select()->from(array('a'=>'erp_user_sync'));
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function insertUserData($data){
        $this->conn->insert('tbl_staffmaster', $data);
        $id = $this->conn->lastInsertId('tbl_staffmaster', 'IdStaff');
        return $id;
    }
    
    public function insertLog($data){
        $this->conn->insert('erp_user_sync_log', $data);
        $id = $this->conn->lastInsertId('erp_user_sync_log', 'erp_log_id');
        return $id;
    }
}

//run this function
$status = new erpUserSync();
$status->process();
?>