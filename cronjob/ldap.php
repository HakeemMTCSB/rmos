<?php
	// using ldap bind
	//student Setting
	$ldaprdn  = 'mtcsb_admin@student.inceif.org';     // ldap rdn or dn
	$ldappass = 'Sms_ldap';  // associated password
	$ldap_port = 636;
	$ldap_host = "ldaps://studentdc01.student.inceif.org";

	//staff setting
	$ldaprdn_staff   = 'mtcsb_ldap@inceif.org';     // ldap rdn or dn
	$ldappass_staff  = 'Sms_ldap';  // associated password
	$ldap_port_staff = 636;
	$ldap_host_staff = "ldap://inceifadr201.inceif.org";
	
	$sent_email = true; // for sending 
	
	$paths = array(
	    realpath(dirname(__FILE__) . '/../library'),
	    '.',
	);
	set_include_path(implode(PATH_SEPARATOR, $paths));
	$path = realpath(dirname(__FILE__).'/../');
	
	//require_once('/var/www/html/sms/library/Zend/Loader/Autoloader.php');
	require_once($path.'/library/Zend/Loader/Autoloader.php');
	
	$loader = Zend_Loader_Autoloader::getInstance();
	
	$config = new Zend_Config_Ini($path.'/application/configs/application.ini',"development");
	
	//get database
	$db = new Zend_Db_Adapter_Pdo_Mysql(array(
	    'host'     => $config->resources->db->params->host,
	    'username' => $config->resources->db->params->username,
	    'password' => $config->resources->db->params->password,
	    'dbname'   => $config->resources->db->params->dbname
	));

	
	//grab data from library_api status != DONE
	$select = $db->select()->from(array('ldap'=>'ldap_api'))->where('ldap.status_type NOT IN (?)', array('DONE','ERROR'));
	                     
	$stmt = $db->query($select);
    $ldap_data = $stmt->fetchAll();
	
	foreach ($ldap_data as $index => $details )
	{
		foreach ($details as $key => $value)
		{
			if(($value == NULL) || ($value == ''))
			{
				unset($details[$key]);
			}
			
			if($key == 'name') {
				if(($value != NULL) || ($value != ''))
				{
					$users['cn'] = $value;
					$users['displayName'] = $value;
				}
			}
			
			if($key == 'givenName') {
				if(($value != NULL) || ($value != ''))
				{
					//$users['sn'] = $value;
					//$users['displayName'] = $value;
				}
			}
			
			if($key == 'username') {
				if(($value != NULL) || ($value != ''))
				{
					$users['sAMAccountName'] = $value;
				}
			}
			
			if($key == 'mail') {
				if(($value != NULL) || ($value != ''))
				{
					$users['UserPrincipalName'] = $value;
				}
			}
				
			$users[$key] = $value;
		}	
		
		unset($users['status_type']);
		unset($users['action_type']);
		unset($users['account_type']);
	
		if($details['action_type'] == 'NEW')
		{
			$users['UserAccountControl'] = "544";
			$users['objectclass'][0] = 'top';
			$users['objectclass'][1] = 'person';
			$users['objectclass'][2] = 'organizationalPerson';
			$users['objectclass'][3] = 'user';
			
			if($details['account_type'] == 'STUDENT') {
				
				if(isset($details['program_code']))
				{
					$ou = getFolder($details['program_code'],$details['intake_id']);
				}
				
				if(isset($users['unicodePwd']))
				{
					$users['unicodePwd'] = encodePassword($users['unicodePwd']);
				}
				
				$ldapconn = ldap_connect($ldap_host,$ldap_port)
					or die("Could not connect to LDAP server.");
				
				if ($ldapconn) {
					ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
					ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
					// binding to ldap server
					$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
					
					$folder_exist = checkFolder($ldapconn,$ou);
					
					if($folder_exist == false) 
					{
						
						$newOu['objectclass'][0] = 'top';
						$newOu['objectclass'][1] = 'organizationalUnit';
						$newOu['OU'] = $ou;
						
						if(ldap_add($ldapconn,"OU=".$ou.",OU=".strtoupper($users['award']).",DC=student,DC=inceif,DC=org",$newOu)){
							echo 'success';
						}
					}
					
					$ou_dc = "CN=".$users['name'].",OU=".$ou.",OU=".strtoupper($users['award']).",DC=student,DC=inceif,DC=org";
					
					unset($users['award']);
					unset($users['program_code']);
					unset($users['intake_id']);
					unset($users['created_date']);
					unset($users['ldap_id']);
					unset($users['username']);
					unset($users['response']);
					unset($users['finished_date']);
				
					if(ldap_add($ldapconn,$ou_dc,$users)) {
						$id = $details['ldap_id'];
					
						$data = array(
							'status_type' => 'DONE',
							'response' => 'SUCCESS',
							'finished_date' => date('Y-m-d H:i:s')
						);
					
						$db->update('ldap_api',$data, 'ldap_id = '. (int)$id);
						//if($sent_email == true)
						//{
							send_notification($ou_dc,$users);
							//$sent_email = false;
						//}
						
					} else {
						$id = $details['ldap_id'];
					
						$error = ldap_errno($ldapconn) .' : '.ldap_error($ldapconn);
						$data = array(
							'status_type' => 'ERROR',
							'response' => $error,
							
						);
					
						$db->update('ldap_api',$data, 'ldap_id = '. (int)$id);
					}
				
				} 
			}
		}
		elseif ($details['action_type'] == 'PASSWORD') 
		{
			if($details['account_type'] == 'STUDENT') {
				
				if(isset($users['unicodePwd']))
				{
					$users['unicodePwd'] = encodePassword($users['unicodePwd']);
				}
				
				$ldapconn = ldap_connect($ldap_host,$ldap_port)
					or die("Could not connect to LDAP server.");
				
				if ($ldapconn) {
					ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
					ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
					// binding to ldap server
					$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
					
					$baseDn = "DC=student,DC=inceif,DC=org";
					$username = "(sAMAccountName=".$users['username'].")";
					
					$ldapBase = get_user_dn( $ldapconn, $baseDn,  $username);
					
					unset($users['ldap_id']);
					$newPassword = array('unicodePwd' => $users['unicodePwd']);
					
					if(ldap_mod_replace($ldapconn,$ldapBase,$newPassword)) {
						$id = $details['ldap_id'];
					
						$data = array(
							'status_type' => 'DONE',
							'response' => 'SUCCESS',
							'finished_date' => date('Y-m-d H:i:s')
						);
					
						$db->update('ldap_api',$data, 'ldap_id = '. (int)$id);
					} else {
						$id = $details['ldap_id'];
					
						$error = ldap_errno($ldapconn) .' : '.ldap_error($ldapconn);
						$data = array(
							'status_type' => 'ERROR',
							'response' => $error,
							
						);
					
						$db->update('ldap_api',$data, 'ldap_id = '. (int)$id);
					} 
				
				}
			}
			elseif($details['account_type'] == 'STAFF') 
			{
				
				if(isset($users['unicodePwd']))
				{
					$users['unicodePwd'] = encodePassword($users['unicodePwd']);
				}
				
				$ldapconn = ldap_connect($ldap_host_staff,$ldap_port)
					or die("Could not connect to LDAP server.");
				
				if ($ldapconn) 
				{
					ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
					ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
					// binding to ldap server
					$ldapbind = ldap_bind($ldapconn, $ldaprdn_staff, $ldappass_staff);
					
					$baseDn = "DC=inceif,DC=org";
					$username = "(sAMAccountName=".$users['username'].")";
					
					$ldapBase = get_user_dn( $ldapconn, $baseDn,  $username);
					
					unset($users['ldap_id']);
					$newPassword = array('unicodePwd' => $users['unicodePwd']);
					if($result = ldap_mod_replace($ldapconn,$ldapBase,$newPassword)) {
						$id = $details['ldap_id'];
					
						$data = array(
							'status_type' => 'DONE',
							'response' => 'SUCCESS',
							'finished_date' => date('Y-m-d H:i:s')
						);
					
						$db->update('ldap_api',$data, 'ldap_id = '. (int)$id);
					} else {
						$id = $details['ldap_id'];
					
						$error = ldap_errno($ldapconn) .' : '.ldap_error($ldapconn);
						$data = array(
							'status_type' => 'ERROR',
							'response' => $error,
							
						);
					
						$db->update('ldap_api',$data, 'ldap_id = '. (int)$id);
					}
				
				}
			
			
			}
		}
		
		ldap_close($ldapconn);
		
		unset($users);
		exit;
	}
	
		

	function encodePassword($password)
	{
		$password="\"".$password."\"";
		$encoded="";
		for ($i=0; $i <strlen($password); $i++){ $encoded.="{$password{$i}}\000"; }
		return $encoded;
	}

	function getFolder($code,$intake) 
	{
		$code = strtoupper($code);
		$intake = strtoupper(str_replace('',' ',$intake));
		
		$subfolder = $code .'_'.$intake;
		
		return $subfolder;	
	}
	
	function checkFolder($ldapconn,$ou) 
	{
		$searchResults = ldap_search( $ldapconn, "DC=student,DC=inceif,DC=org", "|(OU=".$ou.")" );
		
		if ( !is_resource( $searchResults ) )
			return false;
		else
			return true;	
	}

	function get_user_dn( $ldap_conn, $basedn, $user_name ) {

		$searchResults = ldap_search( $ldap_conn, $basedn, $user_name );
		if ( !is_resource( $searchResults ) )
		die('Error in search results.');

		/* Get the first entry from the searched result */
		$entry = ldap_first_entry( $ldap_conn, $searchResults );
		return ldap_get_dn( $ldap_conn, $entry );
	}
	
	function send_notification($ou_dc,$users) {
		$use_smtp = true ;
		
		$to = 'ictsupport@inceif.org';
		//$to = 'jasdy@meteor.com.my';
		$text = 'A new account has been created in inceif AD. <br />Ou : '.$ou_dc.'<br />User : '.$users['sAMAccountName'];
		$header= '';
		
		if( $use_smtp == true ){
		/*
		 * Using SMTP server
		 */

			$config = array('auth' => 'login','username'=>'euniversity@inceif.org','password' => 'e-university','ssl' => 'tls','port'=>'587'); 
	
			$mailTransport = new Zend_Mail_Transport_Smtp("pod51021.outlook.com", $config);
			Zend_Mail::setDefaultTransport($mailTransport);
			
		}else{
			/*
			 * Using server's sendmail
			 */
			$transport = new Zend_Mail_Transport_Smtp('localhost');
			Zend_Mail::setDefaultTransport($transport);
		}
	    	
	    	
		$email = $to;
		$subject = 'New Account has been created';
		$message = $text;
		
		$mail = new Zend_Mail();
		$mail->addTo($email);
		$mail->setSubject($subject);
		$mail->setBodyHtml($message);
		$mail->setFrom('euniversity@inceif.org', 'INCEIF e-University');
		$mail->send();
		
	}
?>
