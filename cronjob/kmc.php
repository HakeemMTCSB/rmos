<?php	
	ini_set('display_errors',1);
  	
	date_default_timezone_set('Asia/Kuala_Lumpur');
	
	$domain = "https://lss.inceif.org/api/v1/";
	$paths = array(
	    realpath(dirname(__FILE__) . '/../library'),
	    '.',
	);
	set_include_path(implode(PATH_SEPARATOR, $paths));
	$path = realpath(dirname(__FILE__).'/../');
	
	//require_once('/var/www/html/sms/library/Zend/Loader/Autoloader.php');
	require_once($path.'/library/Zend/Loader/Autoloader.php');
	$loader = Zend_Loader_Autoloader::getInstance();
	
	//$config = new Zend_Config_Ini('/var/www/html/sms/application/configs/application.ini',"development");
	$config = new Zend_Config_Ini($path.'/application/configs/application.ini',"development");
	
	//get database
	$db = new Zend_Db_Adapter_Pdo_Mysql(array(
	    'host'     => $config->resources->db->params->host,
	    'username' => $config->resources->db->params->username,
	    'password' => $config->resources->db->params->password,
	    'dbname'   => $config->resources->db->params->dbname
	));
	
	//grab data from library_api status != DONE
	$select = $db->select()->from(array('kmc'=>'library_api'))->where('kmc.action_status = ?', 'NEW');
	                     
	$stmt = $db->query($select);
    $kmc_data = $stmt->fetchAll();
    
	foreach ($kmc_data as $index => $details)
	{
		foreach ($details as $key => $value)
		{
			
			if($key == 'user_type')
			{
				$details['type'] = NULL;
				if(($value != NULL) || ($value != ''))
				{
					$details['type'] = $value;
				}
				unset($details['user_type']);
			}
			elseif($key == 'permanent_country') {
				
				
				if(($value != NULL) || ($value != ''))
				{
					$select = $db->select()->from(array('country'=>'tbl_countries'))->where('country.CountryName = ?', $details['permanent_country']);
	                     
					$stmt = $db->query($select);
					$country = $stmt->fetch();
				
					$details['permanent_country'] = $country['CountryISO3'];
				}
				else
				{
					unset($details['permanent_country']);
				}
				
			}
			elseif($key == 'correspondence_country') {
				if(($value != NULL) || ($value != ''))
				{
					$select = $db->select()->from(array('country'=>'tbl_countries'))->where('country.CountryName = ?', $details['correspondence_country']);
							 
					$stmt = $db->query($select);
					$country = $stmt->fetch();
					
					$details['correspondence_country'] = $country['CountryISO3'];
				}
				else {
					unset($details['correspondence_country']);
				}
			}
			elseif($key == 'program_name')
			{
				$program_name = array(
					'CHARTERED ISLAMIC FINANCE PROFESSIONAL' => 'Chartered Islamic Finance Professional',
					//'INDUSTRIAL PhD - ISLAMIC FINANCE' => ,
					'MASTER OF SCIENCE (MSc) IN ISLAMIC FINANCE' => 'Master of Science (MSC) in Islamic Finance',
					'MASTERS IN ISLAMIC FINANCE' => 'Masters in Islamic Finance',
					'MASTERS IN ISLAMIC FINANCE PRACTICE' => 'Masters in Islamic Finance Practice',
					'PhD IN ISLAMIC FINANCE' => 'PhD in Islamic Finance'
				);
				
				if(($value != NULL) || ($value != ''))
				{
					if(array_key_exists($details[$key],$program_name))
					{
						$details[$key] = $program_name[$details[$key]];
					}
				}
				else
				{
					unset($details[$key]);
				}
			}
			elseif($key == 'program_type')
			{
				if(($value != NULL) || ($value != ''))
				{
					if($value == 'By Coursework')
					{
						$details[$key] = 'by-coursework';
					}
					elseif($value == 'By Coursework & Dissertation')
					{
						$details[$key] = 'by-coursework-dissertation';
					}
					else
					{
						unset($details['program_type']);
					}
				}
			}
			elseif($key == 'program_resource_fee')
			{
				if($value != NULL)
                {
                    $fee = explode(' ', trim($value));
                    if (count($fee)==2)
                    {
                        $details['program_currency'] = $fee[0];
                    }
                    if (isset($fee[1]))
                    {
                        $details[$key] = $fee[1];
                    }

					/*
					$fee = explode(' ',$value);
					if(isset($fee[1])) 
					{
						if($fee[0] == 'MYR') 
						{
							$details[$key] = $fee[1];
						}
						else 
						{
							if($fee[1] == '250.00') 
							{
								$details[$key] = '900.00';
							}
							else
							{
								$details[$key] = '500.00';
							}
						}
					}
					else
					{
						$details[$key] = $fee[0];
					}
					*/

				}
				else
				{
					unset($details['program_resource_fee']);
				}
			}
			elseif($key == 'password')
			{
				if((is_null($value)) || ($value == ''))
				{
					unset($details[$key]);
					
				}
				else
				{
					$details[$key] = $value;
				}
			}
			elseif($key == 'response') {
				unset($details[$key]);
			}
			else
			{
				if((is_null($value)) || ($value == ''))
				{
					unset($details[$key]);
					
				}
				
			}
			
		}
		
		$user = array('user' => $details,'timestamp' => time());
		
		$json = json_encode($user);
        //echo $json; exit;
		
		
		if($details['action_type'] == 'user')
		{
			if($details['action'] == 'ADD')
			{
				$serverURL = $domain."users/".$details['username'];
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,$serverURL);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch,CURLOPT_HTTPHEADER,array(
						'Content-Type: application/json',
						'X-API-Public_Key: d33e3ae4859d2bb156176bbf48bd811c1e4bfac6c4322ef9'
					));	
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				
				$response = curl_exec($ch);
				$response = str_replace(array("\n","\r","\r\n"),'',$response);
				curl_close($ch);
				$decode = json_decode($response, true);
				
				if( (isset($decode['code'])) && ($decode['code'] == '404') )
				{
					unset($decode);
					
					$serverURL = $domain."users";
					$ch = curl_init();
					
					curl_setopt($ch,CURLOPT_URL,$serverURL);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch,CURLOPT_HTTPHEADER,array(
							'Content-Type: application/json',
							'Content-Length: ' .strlen($json),
							'X-API-Public_Key: d33e3ae4859d2bb156176bbf48bd811c1e4bfac6c4322ef9'
						));	
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					$response = curl_exec($ch);
					$response = str_replace(array("\n","\r","\r\n"),'',$response);
					
					curl_close($ch);
					
					$decode = json_decode($response, true);
					
					if($decode['code'] == '200')
					{
						//UPDATE TABLE
						
						$id = $details['id'];
						
						$data = array(
							'action_status' => 'DONE',
							'response' => $response,
							'date_finished' => date('Y-m-d H:i:s')
						);
						
						$db->update('library_api',$data, 'id = '. (int)$id);
					}
					else
					{
						//SIMPAN RESPONSE SAHAJA
						$id = $details['id'];
						$data = array(
							'response' => $response
						);
						$db->update('library_api',$data, 'id = '. (int)$id);
					}
				}
				else
				{
					$serverURL = $domain."users/".$details['username'];
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL,$serverURL);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch,CURLOPT_HTTPHEADER,array(
							'Content-Type: application/json',
							'Content-Length: ' .strlen($json),
							'X-API-Public_Key: d33e3ae4859d2bb156176bbf48bd811c1e4bfac6c4322ef9'
						));	
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					$response = curl_exec($ch);
					$response = str_replace(array("\n","\r","\r\n"),'',$response);
					
					curl_close($ch);
					
					$decode = json_decode($response, true);
					
					if($decode['code'] == '200')
					{
						//UPDATE TABLE
						
						$id = $details['id'];
						
						$data = array(
							'action_status' => 'DONE',
							'response' => $response,
							'date_finished' => date('Y-m-d H:i:s')
						);
						
						$db->update('library_api',$data, 'id = '. (int)$id);
					}
					else
					{
						//SIMPAN RESPONSE SAHAJA
						$id = $details['id'];
						$data = array(
							'response' => $response
						);
						$db->update('library_api',$data, 'id = '. (int)$id);
					}
				}
			}
			elseif($details['action'] == 'EDIT')
			{
				$serverURL = $domain."users/".$details['username'];
				$ch = curl_init();
				
				curl_setopt($ch,CURLOPT_URL,$serverURL);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch,CURLOPT_HTTPHEADER,array(
						'Content-Type: application/json',
						'Content-Length: ' .strlen($json),
						'X-API-Public_Key: d33e3ae4859d2bb156176bbf48bd811c1e4bfac6c4322ef9'
					));	
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				$response = curl_exec($ch);
				$response = str_replace(array("\n","\r","\r\n"),'',$response);
				
				curl_close($ch);
				
				$decode = json_decode($response, true);
				
				if($decode['code'] == '200')
				{
					//UPDATE TABLE
					
					$id = $details['id'];
					
					$data = array(
						'action_status' => 'DONE',
						'response' => $response,
						'date_finished' => date('Y-m-d H:i:s')
					);
					
					$db->update('library_api',$data, 'id = '. (int)$id);
				}
				else
				{
					//SIMPAN RESPONSE SAHAJA
					$id = $details['id'];
					$data = array(
						'response' => $response
					);
					$db->update('library_api',$data, 'id = '. (int)$id);
				}
			}
		}
		elseif(($details['action_type'] == 'password') && (isset($details['password'])))
		{
			unset($user);
			
			$user = array(
				'user' => array(
					'password' => $details['password']
				), 
				'timestamp' => time()
			);
			
			$json = json_encode($user,true);
			
			$serverURL = $domain."users/".$details['username']."/password";
			$ch = curl_init();
		
			curl_setopt($ch,CURLOPT_URL,$serverURL);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch,CURLOPT_HTTPHEADER,array(
					'Content-Type: application/json',
					'Content-Length: ' .strlen($json),
					'X-API-Public_Key: d33e3ae4859d2bb156176bbf48bd811c1e4bfac6c4322ef9'
				));	
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			$response = curl_exec($ch);
			$response = str_replace(array("\n","\r","\r\n"),'',$response);

			curl_close($ch);
			
			$decode = json_decode($response, true);
			if($decode['code']== '200')
			{
				//UPDATE TABLE
				
				$id = $details['id'];
				
				$data = array(
					'action_status' => 'DONE',
					'response' => $response,
					'date_finished' => date('Y-m-d H:i:s')
				);
				
				$db->update('library_api',$data, 'id = '. (int)$id);
			}
			else
			{
				//SIMPAN RESPONSE SAHAJA
				$id = $details['id'];
				$data = array(
					'response' => $response
				);
				$db->update('library_api',$data, 'id = '. (int)$id);
			}
		}
		elseif($details['action_type'] == 'status')
		{
			
			unset($user);
			
			$user = array(
				'users' => array(
					'status' => $details['status'],
					'status_message' => $details['status_message']
					), 
				'timestamp' => time()
			);
			
			$json = json_decode($user,true);
			
			$serverURL = $domain."users/".$details['username']."/status";
			$ch = curl_init();
			
			curl_setopt($ch,CURLOPT_URL,$serverURL);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch,CURLOPT_HTTPHEADER,array(
					'Content-Type: application/json',
					'Content-Length: ' .strlen($json),
					'X-API-Public_Key: d33e3ae4859d2bb156176bbf48bd811c1e4bfac6c4322ef9'
				));	
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			$response = curl_exec($ch);
			$response = str_replace(array("\n","\r","\r\n"),'',$response);
			
			curl_close($ch);
			
			$decode = json_decode($response, true);
			
			if($decode['code'] == '200')
			{
				//UPDATE TABLE
				
				$id = $details['id'];
				
				$data = array(
					'action_status' => 'DONE',
					'response' => $response,
					'date_finished' => date('Y-m-d H:i:s')
				);
				
				$db->update('library_api',$data, 'id = '. (int)$id);
			}
			else
			{
				//SIMPAN RESPONSE SAHAJA
				$id = $details['id'];
				$data = array(
					'response' => $response
				);
				$db->update('library_api',$data, 'id = '. (int)$id);
			}
		}
	}
	
	exit;
?>
