<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class updatesemesterpart{
    
    public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
    }
    
    public function process(){
        $list = $this->getStudentList();
        
        if ($list){
            foreach ($list as $loop){
                if ($loop['IdStudentRegistration']==3488){
                    var_dump($loop['IdStudentRegistration']);
                    $semList = $this->getSemester($loop['IdStudentRegistration']);
                     
                    if ($semList){
                        foreach ($semList as $semLoop){
                            var_dump($semLoop['SemesterMainName'].'-'.$semLoop['SemesterMainCode']);
                            //var_dump($loop['IdStudentRegistration']);
                            //exit;
                            $subList = $this->getSubject($loop['IdStudentRegistration'], $semLoop['IdSemesterMaster']);
                            
                            if ($subList){
                                foreach ($subList as $subLoop){
                                    $equivid = false;
                                    $course_status = $this->getSubjectStatus($loop['IdProgram'], $loop['IdLandscape'], $subLoop['IdSubject']);
                                    if(!$course_status){
                                        $equivid = $this->getEqSubjectStatus($loop['IdLandscape'], $subLoop["IdSubject"]);

                                        if($equivid){
                                            $course_status = $this->getSubjectStatus($loop['IdProgram'], $loop['IdLandscape'], $equivid);
                                        }
                                    }
                                    
                                    var_dump($course_status['block']);
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        exit;
    }
    
    public function getStudentList(){
        $sql = $this->conn->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->where('a.IdProgram = ?', 5);
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function getSemester($id){
        $sql = $this->conn->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.idSemester = b.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $id)
            ->order('b.SemesterMainStartDate ASC');
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function getSubject($id, $semester){
        $sql = $this->conn->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->join(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.IdSemesterMain = ?', $semester);
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function getSubjectStatus($program_id,$landscape_id,$subject_id){
        $sql = $this->conn->select()
            ->from(array("ls"=>"tbl_landscapesubject"))
            ->joinLeft(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('BahasaIndonesia','SubCode','SubjectName'=>'SubjectName'))
            ->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('DefinitionDesc'))
            ->joinLeft(array("pr"=>"tbl_programrequirement"),'pr.IdProgramReq=ls.IdProgramReq')
            ->joinLeft(array("lb"=>"tbl_landscapeblock"),'ls.idlevel=lb.idblock',array("block"))
            ->where("ls.IdProgram = ?",$program_id)
            ->where("ls.IdLandscape = ?",$landscape_id)
            ->where("s.IdSubject = ?",$subject_id)
            ->where("IDProgramMajoring = 0");
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getEqSubjectStatus($idlandscape,$idsubject){
        $sql = $this->conn->select()
            ->from(array("a"=>"tbl_landscapesubject"))
            ->join(array("b"=>"tbl_subjectequivalent"),"a.idsubject=b.idsubjectequivalent")
            ->where("a.idlandscape = ?",$idlandscape)
            ->where("b.idsubject =?",$idsubject);
        
        $result = $this->conn->fetchRow($sql);
        
        if(is_array($result)){
            return $result["idsubjectequivalent"];
        }else{
            return false;
        }
    }
}

//run this function
$status = new updatesemesterpart();
$status->process();
?>