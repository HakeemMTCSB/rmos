<?php 
class ProcessChangeStatus {

    public $conn;
    
    public function __construct()
    {    	
    
		$paths = array(
			    realpath(dirname(__FILE__) . '/../library'),
			    '.',
		);
		
		set_include_path(implode(PATH_SEPARATOR, $paths));
		
		$path = realpath(dirname(__FILE__).'/../');
		
		require_once($path.'/library/Zend/Loader/Autoloader.php');
		$loader = Zend_Loader_Autoloader::getInstance();
		
		$config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');
			
		$this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
	
    }
    
    public function process(){
    	   	
	  		
	  	    //get active student only
	  	    $student_list = $this->getActiveStudent();
	  	
     	 	foreach($student_list as $index=>$row){     	 		
     	 		
     	 		//get student semester status	 		
	        	 $cur_semester = $this->getStudentCurrentSemester($row);
	        	 
	        	 if($cur_semester){
	        	 	
	        	 	 $student_list[$index]['current_semester']=$cur_semester;
	        	 	
		        	 $semester = $this->getSemesterStatusBySemester($row['IdStudentRegistration'],$cur_semester['IdSemesterMaster']);
		        	 
		        	 if($semester){ 	
		        	 	if($semester['studentsemesterstatus']==250){
		        	 		//defer @ tangguh
		        	 		//no need to process
		        	 	}else{        	 		
		        	 		$student_list[$index]['semester_status']=$semester['studentsemesterstatus'];
		        	 		$this->changeStatus($row,$cur_semester);
		        	 	}			        	 	
		        	 }else{
		        	 	$student_list[$index]['semester_status']='Not Register';
		        	 	$this->changeStatus($row,$cur_semester);
		        	 }
		        	 
	        	 }else{
	        	 	    $student_list[$index]['current_semester']=null;
	        	 	    $student_list[$index]['semester_status']='Semester not available';
	        	 	    //dont process. Error nanti. Current semester does not set up.
	        	 }
	       
	 		}
		  	
		 
		  
    }
    
    public function changeStatus($row,$cur_semester=null){    	
    		
    		
    		/* -------------------------------------------
    		 *    TODO : insert dalam table sss_cronjob_report
    		   -------------------------------------------  */
    		
    			$report['IdStudentRegistration']=$row['IdStudentRegistration'];
     	 		$report['name']=$row['appl_fname'].''.$row['appl_lname'];
     	 		$report['description']= array();
     	 		
     	 	/* -------------------------------------------
    		 *    end insert dalam table sss_cronjob_report
    		   -------------------------------------------  */
    			
    	
    		$IdStudentRegistration = $row['IdStudentRegistration'];
			$current_semId = $cur_semester['IdSemesterMaster'];
						
			//get consecutive_sem
			$scheme = $this->getProgramSchemeDetails($row['IdProgram']);
			$consecutive_sem = $scheme['consecutive_sem'];
						 	
			$semester_list = $this->getSemesterByScheme($row,$consecutive_sem);
			
			$not_register = 0;			
			
			if(count($semester_list)>0){	
									
				$sem_tbl = '<table border=1>';
				foreach($semester_list as $sem){
					
					//foreach sem get semester status
					$semester_status = $this->getSemesterStatusBySemester($IdStudentRegistration,$sem['IdSemesterMaster']);
					
					if(!$semester_status){
						
						//update semester status to not registered
						$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
							            'idSemester' => $sem['IdSemesterMaster'],
							            'IdSemesterMain' => $sem['IdSemesterMaster'],								
							            'studentsemesterstatus' => 131, 	//Not Register idDefType = 32 (student semester status)
				                        'Level'=>0,
							            'UpdDate' => date ( 'Y-m-d H:i:s'),
							            'UpdUser' => 1
       					 );		
						$this->addStudentSemesterStatus($data);
						
						$not_register = $not_register + 1;
						
						$semester_status = 'Semester ID:'.$sem['IdSemesterMaster'].' - Status: 131 - Add student semester status info';						
						$report['semester_status'] = $semester_status;
						
					}else{
						
						if($semester_status['studentsemesterstatus']==131){
							$not_register = $not_register + 1;
						}
						$semester_status = $semester_status['studentsemesterstatus'];
						$report['semester_status'] = 'Semester ID:'.$sem['IdSemesterMaster'].' - Status: '.$semester_status;
						
					}
					
					$sem_tbl .= '<tr>';
					$sem_tbl .= '<td>'.$sem['IdSemesterMaster'].'</td>';
					$sem_tbl .= '<td>'.$sem['SemesterMainName'].'</td>';
					$sem_tbl .= '<td>'.$sem['SemesterMainStartDate'].'</td>';
					$sem_tbl .= '<td>'.$semester_status.'</td>';
					$sem_tbl .= '</tr>';
					
				}//end foreach semester
				
				$sem_tbl .= '</table><br>';
				
				//echo $sem_tbl;
										
			}//end if semester
			
			
			
			if($not_register >= $consecutive_sem){
				
				//set this semester student semester status as dormant								
				$desc ='No of not register sem greater than and equal to consecutive semester. Set current semester status eq to DORMANT';
				array_push($report['description'],$desc);
				//$this->setSemesterStatus($IdStudentRegistration,$current_semId,844);
				
			}else{
				
				//check any course registered at current semester
				$subjects = $this->getTotalRegisteredBySemester($IdStudentRegistration,$current_semId);
				$desc = 'System check if any course registered at current semester';
				array_push($report['description'],$desc);
					
				if(count($subjects)>0){
					//there are registered subject														
					//set current semester status =  REGISTERED
					$desc = 'There are subject registered at current semester. Set current semester status =  REGISTERED';
					array_push($report['description'],$desc);
					//$this->setSemesterStatus($IdStudentRegistration,$current_semId,130);
					
				}else{
					//no subject registered
					//check if on previous sem, student semester status = DORMANT
					$desc  = 'There are no subject registered at current semester. Check previous semester status ';
					array_push($report['description'],$desc);
					$this->checkDormantStatus($IdStudentRegistration,$current_semId); 
				}
			}
			
			
			echo '<pre>';
			print_r($report);
			echo '</pre>';
			
    }
    
    
    protected function getActiveStudent(){
    	
    		  $select = $this->conn->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))     
    		  						 ->join(array('sp'=>'student_profile'),'sp.id=sa.sp_id',array('appl_fname','appl_mname','appl_lname'))
			                         ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('ArabicName','ProgramName','IdScheme'))
			                         ->join(array('sm' => 'tbl_semestermaster'),'sm.IdSemesterMaster=sa.IdSemesterMain',array('SemesterMainStartDate','SemesterMainEndDate'))
									 ->join(array('p' => 'tbl_program'), "p.IdProgram = sa.IdProgram", array("IdScheme"))
			                         ->where('sa.profileStatus=92')//def type=20 (92:active)
			                         ->order("sa.registrationId ASC");	  
			  $student_list = $this->conn->fetchAll($select);
		  			 		
		 	  return $student_list;
    }
    
	protected function getStudentCurrentSemester($student)
    {
        
        $sql = $this->conn->select()
                  ->from(array('sm' => 'tbl_semestermaster'))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$student['IdScheme']) 
                  ->where('sm.Branch = ?',(int)$student['IdBranch'])
                  ->order('sm.SemesterMainStartDate desc');    
        $result = $this->conn->fetchRow($sql); 
        
        if(!$result){
        	$sql = $this->conn->select()
                  ->from(array('sm' => 'tbl_semestermaster'))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$student['IdScheme'])
                  ->where('sm.Branch = ?',0)
                  ->order('sm.SemesterMainStartDate desc');
        	$result = $this->conn->fetchRow($sql); 
        }
        
        return $result;
    	
    }
    
	protected function getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain){
			
			$sql = $this->conn->select()	
					->from(array('sss' => 'tbl_studentsemesterstatus'))
					->where('IdStudentRegistration  = ?',$IdStudentRegistration)
					->where('IdSemesterMain = ?',$IdSemesterMain);
			$result = $this->conn->fetchRow($sql);
			return $result;		
	}
	
	protected function getSemesterByScheme($student,$consecutive_sem=1){
    	
    	//$student['SemesterMainStartDate'] = '2014-06-01';
         
        $sql = $this->conn->select()
                  ->from(array('sm' => 'tbl_semestermaster'))               
                  ->where('sm.IdScheme = ?',(int)$student['IdScheme'])
                  ->where('sm.Branch = ?',(int)$student['IdBranch'])
                  ->where('sm.SemesterMainEndDate < CURDATE()') //kene enable kan ni saje off sebab xde data
                  ->where('sm.SemesterMainStartDate >= ?',$student['SemesterMainStartDate'])
                 // ->orwhere('(sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE())')
                  ->order('sm.SemesterMainStartDate DESC')
                  ->limit($consecutive_sem,0);             
        $result = $this->conn->fetchAll($sql); 

        if(count($result)==0){
        	
        $sql2 = $this->conn->select()
                  ->from(array('sm' => 'tbl_semestermaster'))               
                  ->where('sm.IdScheme = ?',(int)$student['IdScheme'])    
                  ->where('sm.Branch = ?',0)		             
                  ->where('sm.SemesterMainEndDate < CURDATE() AND sm.SemesterMainStartDate >= ?',$student['SemesterMainStartDate'])
                //  ->orwhere('(sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE())')
                  ->order('sm.SemesterMainStartDate DESC')
                  ->limit($consecutive_sem,0);             
        		$result2 = $this->conn->fetchAll($sql2); 
        		$result = $result2;
        
        }
        return $result;
    }
    
    protected function addStudentSemesterStatus($data){
    	$this->conn->insert('tbl_studentsemesterstatus', $data);
    }
    
    
	public function getTotalRegisteredBySemester($idStudentRegistration,$idSemester){
					
		$select = $this->conn->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('(srs.Active = 1 OR srs.Active = 4 OR srs.Active = 5 )');
	 				
	 	$row = $this->conn->fetchAll($select);
	 	
	 	return $row;
		
	}
	
	public function checkDormantStatus($IdStudentRegistration,$current_semId){
		
		//get student previous semester status
		
		$prev_sem = $this->getStudentPreviousSemester($IdStudentRegistration);
		
		if(isset($prev_sem) && $prev_sem['studentsemesterstatus']==844){//Dormant
			//set current semester status =  DORMANT
			$desc = 'Previous semester status = DORMANT. Set current semester status DORMANT';
			array_push($report['description'],$desc);
			//$this->setSemesterStatus($IdStudentRegistration,$current_semId,844);
			
		}else{
			//set current semester status =  NOT REGISTERED
			$desc = 'Previous semester status != dormant set current semester status NOT REGISTERED';
			array_push($report['description'],$desc);
			//$this->setSemesterStatus($IdStudentRegistration,$current_semId,131);
		}
		
	}
	
	
	public function getStudentPreviousSemester($IdStudentRegistration){
		
		/*
		 * SELECT sss.* ,sm.*
			FROM `tbl_studentsemesterstatus` as sss
			JOIN tbl_semestermaster as sm ON sm.IdSemesterMaster =  sss.`IdSemesterMain`
			WHERE sss.`IdStudentRegistration` = 3296
			AND SemesterMainEndDate < '2015-09-21'
			ORDER BY sm.SemesterMainStartDate DESC
		 */
		
		$db = Zend_Db_Table::getDefaultAdapter();      
			
		//$curdate = '2015-09-21';
		$curdate = 'CURDATE()';
		
		$sql = $this->conn->select()	
				->from(array('sss' => 'tbl_studentsemesterstatus'))
				->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =  sss.IdSemesterMain')
				->where('sss.IdStudentRegistration  = ?',$IdStudentRegistration)
				->where('sm.SemesterMainEndDate < ?',$curdate)
				->order('sm.SemesterMainStartDate DESC');
		$result = $this->conn->fetchRow($sql);
		return $result;		
	}
	
	
	/*
	 * Function : To get Scheme Based on Program
	 * Param : IdScheme fetch from program
	 */
	
	public function getProgramSchemeDetails($idProgram) {
	
		$select = $this->conn->select()
					 ->from(array("s"=>"tbl_scheme"))
					 ->join(array('p'=>'tbl_program'),'p.IdScheme = s.IdScheme',array())
					 ->where('p.IdProgram = ?',$idProgram);
		$larrResult = $this->conn->fetchRow($select);
		return $larrResult;
	}

}


$status = new ProcessChangeStatus();
$status->process();
