<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
date_timezone_set('Asia/Kuala_Lumpur');
class semesterstatusupdate{
    public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
    }
    
    public function process(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        echo 'update semester status start';
        $list = $this->getStudentList();
        //var_dump($list); exit;
        $url = 'C:/izham/semstatuslog';
        $file = $url.'/'.'log.txt';
        //$contentLog = file_get_contents($file);
        $contentLog = '';
        
        if ($list){
            foreach ($list as $key => $loop){
                var_dump('Student ID : '.$loop['registrationId']);
                var_dump('Student Status : '.$loop['profileStatus']);
                $contentLog .= 'Student ID : '.$loop['registrationId']."\r\n";
                $contentLog .= 'Student Status : '.$loop['profileStatus']."\r\n";
                $intakeInfo = $this->getIntake($loop['IdIntake']);
                $semInfo = $this->getSemester($loop['IdScheme'], $intakeInfo['sem_year'], $intakeInfo['sem_seq']);
                $currentSem = $this->getCurrentSemester($loop['IdScheme']);
                
                $semList = $this->getSemesterList($loop['IdScheme'], $semInfo['SemesterMainStartDate'], $currentSem['SemesterMainStartDate']);
                
                if ($semList){
                    $i = 0;
                    foreach ($semList as $key2 => $semLoop){
                        $checkSemStatus = $this->checkSemesterStatus($loop['IdStudentRegistration'], $semLoop['IdSemesterMaster']);
                        $checkSemSubject = $this->checkSubjectRegister($loop['IdStudentRegistration'], $semLoop['IdSemesterMaster']);
                        
                        $i++;
                        
                        var_dump('Update Semester Status : '.$semLoop['SemesterMainCode']);
                        $contentLog .= 'Update Semester Status : '.$semLoop['SemesterMainCode']."\r\n";
                        if ($i == count($semList)){
                            switch ($loop['profileStatus']){
                                case 92: //Active
                                    if ($checkSemStatus){
                                        //if ($checkSemStatus['studentsemesterstatus']==130 || $checkSemStatus['studentsemesterstatus']==131){
                                            if ($checkSemSubject){
                                                $semStatus = 130;
                                            }else{
                                                $semStatus = 131;
                                            }

                                            if ($semStatus == 130){
                                                $data = array(
                                                    'IdBlock'=>null, 
                                                    'studentsemesterstatus'=>$semStatus, 
                                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                                    'UpdUser'=>1, 
                                                    'UpdRole'=>'admin'
                                                );
                                                $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                            }else{
                                                //check dormant or not
                                                $getLastSemReg = $this->getLastSemesterRegister($loop['IdStudentRegistration']);

                                                if ($getLastSemReg){
                                                    $date1 = $getLastSemReg['SemesterMainStartDate'];
                                                    $substractvalue = 2;
                                                }else{
                                                    $date1 = $semInfo['SemesterMainStartDate'];
                                                    $substractvalue = 1;
                                                }
                                                
                                                if ($loop['IdScheme']==1){ //ps
                                                    $semType = 0;
                                                    $consecutivesem = 3;
                                                }else if($loop['IdScheme']==11){ //gs
                                                    $semType = 172;
                                                    $consecutivesem = 2;
                                                }else{ //open
                                                    $semType = 172;
                                                    $consecutivesem = 2;
                                                }
                                                
                                                $listUnregSem = $this->listUnregisteredSemester($loop['IdScheme'], $date1, $semLoop['SemesterMainStartDate'], $semType);
                                            
                                                $semcount = count($listUnregSem)-$substractvalue;
                                                //var_dump($listUnregSem); exit;
                                                if ($semcount >= $consecutivesem){ //dormant
                                                    $data = array(
                                                        'IdBlock'=>null, 
                                                        'studentsemesterstatus'=>844, 
                                                        'UpdDate'=>date('Y-m-d h:i:s'), 
                                                        'UpdUser'=>1, 
                                                        'UpdRole'=>'admin'
                                                    );
                                                    $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                                }else{ //unregistered
                                                    $data = array(
                                                        'IdBlock'=>null, 
                                                        'studentsemesterstatus'=>$semStatus, 
                                                        'UpdDate'=>date('Y-m-d h:i:s'), 
                                                        'UpdUser'=>1, 
                                                        'UpdRole'=>'admin'
                                                    );
                                                    $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                                }
                                            }
                                        //}
                                    }else{
                                        if ($checkSemSubject){
                                            $semStatus = 130;
                                        }else{
                                            $semStatus = 131;
                                        }
                                        
                                        if ($semStatus == 130){
                                            $data = array(
                                                'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                                'idSemester'=>$semLoop['IdSemesterMaster'], 
                                                'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                                'IdBlock'=>null, 
                                                'studentsemesterstatus'=>$semStatus, 
                                                'Level'=>1, 
                                                'UpdDate'=>date('Y-m-d h:i:s'), 
                                                'UpdUser'=>1, 
                                                'UpdRole'=>'admin'
                                            );
                                            $idsemstatus = $this->insertSemesterStatus($data);

                                            $data2 = array(
                                                'idstudentsemsterstatus'=>$idsemstatus,
                                                'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                                'idSemester'=>$semLoop['IdSemesterMaster'], 
                                                'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                                'IdBlock'=>null, 
                                                'studentsemesterstatus'=>$semStatus, 
                                                'Level'=>1, 
                                                'UpdDate'=>date('Y-m-d h:i:s'), 
                                                'UpdUser'=>1, 
                                                'UpdRole'=>'admin',
                                                'Type'=>null, 
                                                'message'=>0, 
                                                'createddt'=>date('Y-m-d h:i:s'), 
                                                'createdby'=>1
                                            );
                                            $this->insertSemesterStatusHistory($data2);
                                        }else{
                                            //check dormant or not
                                            $getLastSemReg = $this->getLastSemesterRegister($loop['IdStudentRegistration']);

                                            if ($getLastSemReg){
                                                $date1 = $getLastSemReg['SemesterMainStartDate'];
                                                $substractvalue = 2;
                                            }else{
                                                $date1 = $semInfo['SemesterMainStartDate'];
                                                $substractvalue = 1;
                                            }

                                            if ($loop['IdScheme']==1){ //ps
                                                $semType = 0;
                                                $consecutivesem = 3;
                                            }else if($loop['IdScheme']==11){ //gs
                                                $semType = 172;
                                                $consecutivesem = 2;
                                            }else{ //open
                                                $semType = 172;
                                                $consecutivesem = 2;
                                            }

                                            $listUnregSem = $this->listUnregisteredSemester($loop['IdScheme'], $date1, $semLoop['SemesterMainStartDate'], $semType);

                                            $semcount = count($listUnregSem)-$substractvalue;
                                            
                                            if ($semcount >= $consecutivesem){ //dormant
                                                $data = array(
                                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                                    'idSemester'=>$semLoop['IdSemesterMaster'], 
                                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                                    'IdBlock'=>null, 
                                                    'studentsemesterstatus'=>844, 
                                                    'Level'=>1, 
                                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                                    'UpdUser'=>1, 
                                                    'UpdRole'=>'admin'
                                                );
                                                $idsemstatus = $this->insertSemesterStatus($data);

                                                $data2 = array(
                                                    'idstudentsemsterstatus'=>$idsemstatus,
                                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                                    'idSemester'=>$semLoop['IdSemesterMaster'], 
                                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                                    'IdBlock'=>null, 
                                                    'studentsemesterstatus'=>844, 
                                                    'Level'=>1, 
                                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                                    'UpdUser'=>1, 
                                                    'UpdRole'=>'admin',
                                                    'Type'=>null, 
                                                    'message'=>0, 
                                                    'createddt'=>date('Y-m-d h:i:s'), 
                                                    'createdby'=>1
                                                );
                                                $this->insertSemesterStatusHistory($data2);
                                            }else{ //unregistered
                                                $data = array(
                                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                                    'idSemester'=>$semLoop['IdSemesterMaster'], 
                                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                                    'IdBlock'=>null, 
                                                    'studentsemesterstatus'=>$semStatus, 
                                                    'Level'=>1, 
                                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                                    'UpdUser'=>1, 
                                                    'UpdRole'=>'admin'
                                                );
                                                $idsemstatus = $this->insertSemesterStatus($data);

                                                $data2 = array(
                                                    'idstudentsemsterstatus'=>$idsemstatus,
                                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                                    'idSemester'=>$semLoop['IdSemesterMaster'], 
                                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                                    'IdBlock'=>null, 
                                                    'studentsemesterstatus'=>$semStatus, 
                                                    'Level'=>1, 
                                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                                    'UpdUser'=>1, 
                                                    'UpdRole'=>'admin',
                                                    'Type'=>null, 
                                                    'message'=>0, 
                                                    'createddt'=>date('Y-m-d h:i:s'), 
                                                    'createdby'=>1
                                                );
                                                $this->insertSemesterStatusHistory($data2);
                                            }
                                        }
                                    }
                                    break;
                                case 94: //Terminated
                                    if ($checkSemStatus){
                                        $data = array(
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>132, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                    }else{
                                        $data = array(
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>132, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $idsemstatus = $this->insertSemesterStatus($data);

                                        $data2 = array(
                                            'idstudentsemsterstatus'=>$idsemstatus,
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>132, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin',
                                            'Type'=>null, 
                                            'message'=>0, 
                                            'createddt'=>date('Y-m-d h:i:s'), 
                                            'createdby'=>1
                                        );
                                        $this->insertSemesterStatusHistory($data2);
                                    }
                                    break;
                                case 96: //Graduated
                                    if ($checkSemStatus){
                                        $data = array(
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>848, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                    }else{
                                        $data = array(
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>848, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $idsemstatus = $this->insertSemesterStatus($data);

                                        $data2 = array(
                                            'idstudentsemsterstatus'=>$idsemstatus,
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>848, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin',
                                            'Type'=>null, 
                                            'message'=>0, 
                                            'createddt'=>date('Y-m-d h:i:s'), 
                                            'createdby'=>1
                                        );
                                        $this->insertSemesterStatusHistory($data2);
                                    }
                                    break;
                                case 248: //Completed
                                    if ($checkSemStatus){
                                        $data = array(
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>229, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                    }else{
                                        $data = array(
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>229, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $idsemstatus = $this->insertSemesterStatus($data);

                                        $data2 = array(
                                            'idstudentsemsterstatus'=>$idsemstatus,
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>229, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin',
                                            'Type'=>null, 
                                            'message'=>0, 
                                            'createddt'=>date('Y-m-d h:i:s'), 
                                            'createdby'=>1
                                        );
                                        $this->insertSemesterStatusHistory($data2);
                                    }
                                    break;
                                case 249: //Quit
                                    if ($checkSemStatus){
                                        if ($checkSemStatus['studentsemesterstatus']==130 || $checkSemStatus['studentsemesterstatus']==131){
                                            $data = array(
                                                'IdBlock'=>null, 
                                                'studentsemesterstatus'=>835, 
                                                'UpdDate'=>date('Y-m-d h:i:s'), 
                                                'UpdUser'=>1, 
                                                'UpdRole'=>'admin'
                                            );
                                            $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                        }
                                    }else{
                                        $data = array(
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>835, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin'
                                        );
                                        $idsemstatus = $this->insertSemesterStatus($data);

                                        $data2 = array(
                                            'idstudentsemsterstatus'=>$idsemstatus,
                                            'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                            'idSemester'=>$semLoop['IdSemesterMaster'], 
                                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                            'IdBlock'=>null, 
                                            'studentsemesterstatus'=>835, 
                                            'Level'=>1, 
                                            'UpdDate'=>date('Y-m-d h:i:s'), 
                                            'UpdUser'=>1, 
                                            'UpdRole'=>'admin',
                                            'Type'=>null, 
                                            'message'=>0, 
                                            'createddt'=>date('Y-m-d h:i:s'), 
                                            'createdby'=>1
                                        );
                                        $this->insertSemesterStatusHistory($data2);
                                    }
                                    break;
                            }
                        }else{
                            if (!$checkSemStatus){
                                if ($checkSemSubject){
                                    $semStatus = 130;
                                }else{
                                    $semStatus = 131;
                                }

                                $data = array(
                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                    'idSemester'=>$semLoop['IdSemesterMaster'], 
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                    'IdBlock'=>null, 
                                    'studentsemesterstatus'=>$semStatus, 
                                    'Level'=>1, 
                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                    'UpdUser'=>1, 
                                    'UpdRole'=>'admin'
                                );
                                $idsemstatus = $this->insertSemesterStatus($data);

                                $data2 = array(
                                    'idstudentsemsterstatus'=>$idsemstatus,
                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                                    'idSemester'=>$semLoop['IdSemesterMaster'], 
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'], 
                                    'IdBlock'=>null, 
                                    'studentsemesterstatus'=>$semStatus, 
                                    'Level'=>1, 
                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                    'UpdUser'=>1, 
                                    'UpdRole'=>'admin',
                                    'Type'=>null, 
                                    'message'=>0, 
                                    'createddt'=>date('Y-m-d h:i:s'), 
                                    'createdby'=>1
                                );
                                $this->insertSemesterStatusHistory($data2);
                            }else{
                                if ($checkSemSubject){
                                    $semStatus = 130;
                                }else{
                                    $semStatus = 131;
                                }
                                
                                $data = array(
                                    'IdBlock'=>null, 
                                    'studentsemesterstatus'=>$semStatus, 
                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                    'UpdUser'=>1, 
                                    'UpdRole'=>'admin'
                                );
                                $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                            }
                        }
                    }
                }
                
                $contentLog .= "\r\n";
                
                //this use for break the loop
                /*if ($key == 3){
                    break;
                }*/
            }
        }
        
        $datetime = date('Y-m-d h:i:s');
        
        /*if(!is_dir($url)){
            mkdir($url);
        }*/
        //file_put_contents($file, $contentLog);
        //fwrite($file, $contentLog);
    }
    
    public function getStudentList(){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregistration'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram=b.IdProgram');
            //->where('a.IdStudentRegistration = ?', 3546);
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function getIntake($id){
        $sql = $this->conn->select()->from(array('a'=>'tbl_intake'))
            ->where('a.IdIntake = ?', $id);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getSemester($idScheme, $year, $month){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getCurrentSemester($idScheme){
        $date = date('Y-m-d');
        
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getSemesterList($idScheme, $date1, $date2){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.IsCountable = ?', 1)
            ->where('a.SemesterMainStartDate BETWEEN "'.$date1.'" AND "'.$date2.'"')
            ->order('a.SemesterMainStartDate ASC');
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function checkSemesterStatus($idstudent, $idsemester){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentsemesterstatus'))
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.idSemester = ?', $idsemester);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function checkSubjectRegister($idstudent, $idsemester){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.IdSemesterMain = ?', $idsemester)
            ->where('a.Active <> ?', 3);
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function insertSemesterStatus($data){
        $this->conn->insert('tbl_studentsemesterstatus', $data);
        $id = $this->conn->lastInsertId('tbl_studentsemesterstatus', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function insertSemesterStatusHistory($data){
        $this->conn->insert('tbl_studentsemesterstatus_history', $data);
        $id = $this->conn->lastInsertId('tbl_studentsemesterstatus_history', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function updateSemesterStatus($data, $id){
        $update = $this->conn->update('tbl_studentsemesterstatus', $data, 'idstudentsemsterstatus = '.$id);
        return $update;
    }
    
    public function getLastSemesterRegister($idstudent){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregsubjects'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.IdSemesterMain=b.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('b.IsCountable = ?', 1)
            ->where('a.Active <> ?', 3)
            ->order('b.SemesterMainStartDate DESC');
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function listUnregisteredSemester($scheme, $date1, $date2, $semType=0){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.SemesterMainStartDate BETWEEN "'.$date1.'" AND "'.$date2.'"')
            ->where('a.IsCountable = ?', 1)
            ->where('a.SemesterFunctionType IS NULL');
        
        if ($semType!=0){
            $sql->where('a.SemesterType = ?', $semType);
        }
        
        $sql->order('a.SemesterMainStartDate ASC');
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
}
//run this function
$status = new semesterstatusupdate();
$status->process();
?>