<?php

//set unlimited
ini_set('display_errors', 'On');
ini_set('html_errors', 0);
error_reporting(-1);
set_time_limit(0);
ini_set('memory_limit', '-1');
date_default_timezone_set('Asia/Kuala_Lumpur');

class cronService
{
	public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
        
        require_once($path.'/library/Cms/common_functions.php');
        require_once($path.'/library/PHPExcel/Classes/PHPExcel.php');
        
    }
	
	public function close($id, $results=null,$recurring=0) 
	{
		$dataUpdate = array(
								'inprogress' => 0,
								'executed_date' => new Zend_Db_Expr('NOW()'), 
								'executed' => 1, 
								'execution_time' => $this->time_end(),
								'results' => $results
							);
		
		if ( $recurring == 1)
		{
			$dataUpdate['recurring_lastexecuted'] = new Zend_Db_Expr('NOW()');
			$dataUpdate['executed'] = 0;
		}
		
		$this->conn->update('cron_service', $dataUpdate, array('id = ?' =>  $id) );
	}
	
	function time_start($id=null)
	{
		$starttime = microtime();	
		$starttime = explode(" ",$starttime);
		$starttime = $starttime[1] + $starttime[0];

		$this->_TIMESTART = $starttime;
	
		//update into in progress
		if ( $id != null )
		{
			$dataUpdate = array(
									'inprogress' => 1, 
								);
			
			$this->conn->update('cron_service', $dataUpdate, array('id = ?' =>  $id) );
		}
	}

	function time_end()
	{
		$starttime =  $this->_TIMESTART;

		$endtime = microtime();
		$endtime = explode(" ",$endtime);
		$endtime = $endtime[1] + $endtime[0];
		$stime = $endtime - $starttime;
		return round($stime,4);
	}
	
	public function updateSoa($id) 
	{
		
		$dataUpdate = array(
			'executed_date' => new Zend_Db_Expr('NOW()'), 
			'executed' => 1, 
		);
		
		$this->conn->update('finance_service', $dataUpdate, 'id_cron ='.  $id );
	}
	
	
	/*
		REPORT SAMPLE
	*/
	public function reportSample()
	{
		//do something
		echo '123';

		return 'Any remarks/results or URL or whatever. Will be saved in service table for reference or linking with action if needed.';
	}
	
	public function ageing()
	{
        
		$service = new cronService();
		$db = $service->conn;
		
		//get search parameter
		$select = $service->conn->select()
			->from(array('a'=>'finance_service'))
			->where('a.executed = 0')
			->order('a.created_date asc');
		$results = $service->conn->fetchRow($select);
		
		$idCron = $results['id_cron'];
		
		$semesterList=$this->listAllSemester();
		
		foreach($semesterList as $sem){
			$semester[]=$sem['IdSemesterMaster'];
		}
		
		$formData = array(
			'program'=>'ALL',
			'date_from'=>'2005-01-01',
			'date_to'=>date('Y-m-d'),
			'semester'=>$semester,
		);
	
		    $list = $this->getOutstandingInvoiceReport($formData);
			
			$totalend = 0;
			$reportArray = array();
		    $i=0;
		    foreach($list as $data){
				
				$service->time_start();

				//start
		    	$studentName = '';
		    	if($data['appl_fullname']){
		    		$studentName = $data['appl_fullname'];
		    	}else{
		    		$studentName = $data['student_fullname'];
		    	}
				
		    	$reportArray[$i]['invoice_no'] = $data['invoice_no'];
		    	$reportArray[$i]['invoice_date'] = $data['invoice_date'];
		    	$reportArray[$i]['registrationId'] = isset($data['registrationId'])?$data['registrationId']:$data['at_pes_id'];
		    	$reportArray[$i]['student_fullname'] = $studentName;
		    	$reportArray[$i]['profileStatus'] = $data['profileStatus'];
		    	$reportArray[$i]['SemesterMainName'] = $data['SemesterMainName'];
		    	$reportArray[$i]['applicant_program'] = $data['applicant_program'];
		    	$reportArray[$i]['mop'] = $data['mop'];
		    	$reportArray[$i]['mos'] = $data['mos'];
		    	$reportArray[$i]['pt'] = $data['pt'];
		    	$reportArray[$i]['fi_name'] = $data['fi_name'];
		    	$reportArray[$i]['appl_email'] = $data['appl_email'];
		    	$reportArray[$i]['amount'] = $data['amount'];	
		    	$reportArray[$i]['dt_discount'] = $data['dt_discount'];
		    	$reportArray[$i]['currency_id'] = $data['currency_id'];
		    	$reportArray[$i]['SubCode'] = $data['SubCode'];
		    	$reportArray[$i]['MigrateCode'] = $data['MigrateCode'];
		    	$reportArray[$i]['bill_description'] = $data['bill_description'];
		    	
//	    		$newbalance = 0;
	    		//get receipt_invoice

		    	$invMain =  $data['invMainID'];
		    	$invDetail =  $data['idDetail'];
		    	$reportArray[$i]['invMainID'] = $invMain;
		    	$reportArray[$i]['idDetail'] = $invDetail;
		    	
		    	$searchRecInv = array(
			    	'date_from'=>$formData['date_from'],
			    	'date_to'=>$formData['date_to'],
			    	'invoice_id'=>$invMain,
			    	'invoice_detail_id'=>$invDetail
		    	);
		    	
//		    	echo "<pre>";
//		    	print_r($reportArray);
		    	
		    	/* 
		    	 * FORMULA
		    	 * [payment] - [advance_payment_utilize] - [discount] - [CN] + [refund] = [outstanding_amount]
		    	 * 
		    	 * checking balance from 
		    	 * 1) receipt_invoice
		    	 * 2) advance payment utilization
		    	 * 3) discount
		    	 * 4) CN drop by asad
		    	 */
		    	
		    	//get outstanding invoice
		    	$balancePaid = $this->getOutstandingInv($searchRecInv);
		    	
		    	$reportArray[$i]['balancePaid'] = $balancePaid;	
		    	
		    	//get cn amount based on date
		    	
		    	$searchCN = array(
			    	'date_from'=>$formData['date_from'],
			    	'date_to'=>$formData['date_to'],
			    	'invoice_id'=>$invMain,
			    	'invoice_detail_id'=>$invDetail
		    	);
		    	
		    	$cnAmount = $this->getSearchDataCN($searchCN);
		    	 
		    	$newbalance = $data['amount'] - $cnAmount['cnamount'];
		    	if($balancePaid){
		    		$newbalance = $newbalance - $balancePaid;
		    	}
		    	
			    $reportArray[$i]['balance'] = $newbalance;	
			    
			    $amountCur = $newbalance;
	    		$amountCur = $amountCur * $data['cr_exchange_rate'];
	    		$reportArray[$i]['amount_convert'] = $amountCur;
		    		
		    	
		    	if($data['scholarship']){ //scholarship
					$reportArray[$i]['funding'] = $data['scholarship'];
		    	}elseif($data['sponsorship']){ //scholarship
					$reportArray[$i]['funding'] = $data['sponsorship'];
		    	}else{
		    		$reportArray[$i]['funding'] = 'Self Funding';
		    	}
		    	
		    	$days = (strtotime( $formData['date_to']) - strtotime($data['invoice_date'])) / (60 * 60 * 24);
		    	$reportArray[$i]['days'] = $days;
		    	

		    	if($newbalance <= 0){
		    		unset($reportArray[$i]);
		    	}
			    	
		    	$i++;
				
				//end
				$end = $service->time_end();
				$totalend += $end;

				echo $i.'. '.$end . ''."\n";
				//if ( $i == 10 )
				//{
				//	exit;
				//}
		    }
			
			
//		    $fileSave = $this->saveToDB($reportArray,$idCron); takyah save db kot
		    
			echo "\n\n".'Total Time: '.$totalend.' secs';
				
		    $dirName = $this->createUploadFile();
		 	$fileExcel = $this->generateExcel($reportArray,$dirName,$idCron);
		    
		return 'ageing';
	}
	
	public function listAllSemester(){
	
		$service = new cronService();
		$db = $service->conn;
		$selectData = $db->select()
			->from(array('a'=>'tbl_semestermaster'))
			->where('a.display = 1');
	
		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;
		}

	}
	
	public function getOutstandingInvoiceReport($searchData=null){
		
		$service = new cronService();
		$db = $service->conn;
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$semester = isset($searchData['semester'])?$searchData['semester']:0;
		$semester_id = isset($searchData['semester_id'])?$searchData['semester_id']:0;
		
		/*$selectData2 = $db->select()
					->from(array('a'=>'fee_structure_program_item'),array('fspi_fee_id'))
					->where("fspi_type = 'application'")
					->group('fspi_fee_id');
		$itemExcluded = $db->fetchAll($selectData2);*/
		
//		$itemExcluded = array(24,25,34,35,43,44,75); //exclude fee item from application/with proforma
		
		
		// STUDENTS
				//registrationId, student_fullname, appl_email, appl_fullname, at_pes_id, dt_discount, sponsorship

		$students = $db->select()
					->from(array('ivd'=>'invoice_detail'),array('fi_id','idDetail'=>'ivd.id','balance','amount','cn_amount', new Zend_Db_Expr('str.registrationId, CONCAT_WS(\' \',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname, sa.appl_email, NULL as appl_fullname, 0 as at_pes_id'),'dcnt.dt_discount','CONCAT_WS(\' \',sptt.fName, sptt.lName) as sponsorship'))
					 ->join(array('a'=>'invoice_main'),"ivd.invoice_main_id = a.id ",array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','status','invMainID'=>'a.id','MigrateCode','not_include','currency_id','bill_description'))
					 
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
					->joinLeft(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration AND (a.IdStudentRegistration IS NOT NULL OR a.IdStudentRegistration != 0)',array())
					->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array())
					->joinLeft(array('pa'=>'tbl_program'),'str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
					
					->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
					->joinLeft(array('e'=>'tbl_program_scheme'), 'str.IdProgramScheme = e.IdProgramScheme',array())
					->joinLeft(array('sts'=>'tbl_definationms'), 'str.profileStatus = sts.idDefinition', array('profileStatus'=>'sts.DefinitionDesc'))
					->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
					->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
					->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
					->joinLeft(array('fi'=>'fee_item'),'ivd.fi_id = fi.fi_id',array('fi_name'))
					

					//sponsorship,sch_name,dt_discount
					->joinLeft(array('sp'=>'sponsor_application'), 'sp.sa_cust_id = str.IdStudentRegistration', array())
					->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = str.IdStudentRegistration', array())
					->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
					->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = str.IdStudentRegistration', array())
					->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array())
					->joinLeft(array('dcn'=>'discount_type_tag'), 'dcn.IdStudentRegistration=str.IdStudentRegistration', array())
					->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array())
					
					->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
					->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode'))
					->where('(str.IdStudentRegistration != 0 OR str.IdStudentRegistration IS NOT NULL)')
					->where("DATE(a.invoice_date) >= ?",$dateFrom)
					->where("DATE(a.invoice_date) <= ?",$dateTo)
					->where("a.status IN ('A','W')")
					->where("IFNULL(fi.fi_exclude,0) = 0") 
					->where("a.not_include = 0") 
					//->order(new Zend_Db_Expr('NULL'), '')
					->group('ivd.id');
		
		if($semester){
			$students->where('a.semester IN (?)', $semester);
		}
		
		if($semester_id){
			$students->where('a.semester IN (?)', $semester_id);
		}
		
		if($program != 'ALL'){
			$students->where('pa.IdProgram =?', $program);
		}


		// APPLICANTS

		$applicants = $db->select()
					->from(array('ivd'=>'invoice_detail'),
					
					//registrationId, student_fullname, appl_email, appl_fullname, at_pes_id, dt_discount, sponsorship
					array('fi_id','idDetail'=>'ivd.id','balance','amount','cn_amount', new Zend_Db_Expr('NULL as registrationId, NULL as student_fullname, ap.appl_email,CONCAT_WS(\' \',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname, at.at_pes_id, NULL as dt_discount, NULL as sponsorship')))

					 ->join(array('a'=>'invoice_main'),"ivd.invoice_main_id = a.id ",array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','status','invMainID'=>'a.id','MigrateCode','not_include','currency_id','bill_description'))
					 
					
					 ->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
					 ->joinLeft(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					 ->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.trans_id',array())
					 ->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array())
					 ->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
					  
					 ->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						
					 ->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
					 ->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme ',array())
					 ->joinLeft(array('sts'=>'tbl_definationms'), 'sts.idDefinition=at.at_status', array('profileStatus'=>'sts.DefinitionDesc'))
			         ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			         ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			         ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			         ->joinLeft(array('fi'=>'fee_item'),'ivd.fi_id = fi.fi_id',array('fi_name'))
					
					->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_trans_id = at.at_trans_id', array())
					->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
					
			         
					 ->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
			         ->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode'))
					 ->where("DATE(a.invoice_date) >= ?",$dateFrom)
					 ->where("DATE(a.invoice_date) <= ?",$dateTo)
					 ->where("a.status IN ('A','W')")
					 ->where('( a.IdStudentRegistration = 0 OR a.IdStudentRegistration IS NULL )')
//->where("a.IdStudentRegistration IN (3703)")
//->where("a.trans_id IN (2173)")

					->where("IFNULL(fi.fi_exclude,0) = 0") 
					->where("a.not_include = 0") 
					//->order('a.invoice_date asc')
					//->order('a.bill_number')
					//->order(new Zend_Db_Expr('NULL'), '')
					->group('ivd.id');

		if($semester){
			$applicants->where('a.semester IN (?)', $semester);
		}
		
		if($semester_id){
			$applicants->where('a.semester IN (?)', $semester_id);
		}
		
		if($program != 'ALL'){
			$applicants->where('pa.IdProgram =?', $program);
		}
		
	
		$selectAll = $db->select()->union(array($applicants, $students), Zend_Db_Select::SQL_UNION_ALL )->order(new Zend_Db_Expr('NULL'), '');
		
		if(!$selectAll){
			return null;
		}else{
			return $db->fetchAll($selectAll);	
		}

		
		// OLD QUERY BELOW
		// 8/26/2015
		$selectData = $db->select()
					->from(array('ivd'=>'invoice_detail'),array('fi_id','idDetail'=>'ivd.id','balance','amount','cn_amount'))
					 ->join(array('a'=>'invoice_main'),"ivd.invoice_main_id = a.id ",array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','status','invMainID'=>'a.id','MigrateCode','not_include','currency_id','bill_description'))
					 
					 					/*->from(array('a'=>'invoice_main'),array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','status'))
					 ->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id = a.id',array('fi_id','balance'))*/
					 
					 ->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
					 ->joinLeft(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					 ->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.trans_id',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname",'appl_email'))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration AND (a.IdStudentRegistration IS NOT NULL OR a.IdStudentRegistration != 0)',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname",'appl_email'))
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						
						->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'str.IdProgramScheme = e.IdProgramScheme OR app.ap_prog_scheme = e.IdProgramScheme ',array())
						->joinLeft(array('sts'=>'tbl_definationms'), 'str.profileStatus = sts.idDefinition OR sts.idDefinition=at.at_status', array('profileStatus'=>'sts.DefinitionDesc'))
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			            ->joinLeft(array('fi'=>'fee_item'),'ivd.fi_id = fi.fi_id',array('fi_name'))
			            ->joinLeft(array('sp'=>'sponsor_application'), 'sp.sa_cust_id = str.IdStudentRegistration', array())
			            ->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = str.IdStudentRegistration OR schtg.sa_trans_id = at.at_trans_id', array())
			            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
			            ->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = str.IdStudentRegistration', array())
			            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
			            
			            ->joinLeft(array('dcn'=>'discount_type_tag'), 'dcn.IdStudentRegistration=str.IdStudentRegistration', array())
			            ->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array('dt_discount'))
						->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
			            ->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode'))
					->where("DATE(a.invoice_date) >= ?",$dateFrom)
					->where("DATE(a.invoice_date) <= ?",$dateTo)
					->where("a.status IN ('A','W')")
//->where("a.IdStudentRegistration IN (3703)")
//->where("a.trans_id IN (2173)")

					->where("IFNULL(fi.fi_exclude,0) = 0") 
					->where("a.not_include = 0") 
					
					->order('a.invoice_date asc')
					->order('a.bill_number')
					->group('ivd.id');
					
		if($semester){
			$selectData->where('a.semester IN (?)', $semester);
		}
		
		if($semester_id){
			$selectData->where('a.semester IN (?)', $semester_id);
		}
		
		if($program != 'ALL'){
			$selectData->where('pa.IdProgram =?', $program);
		}
		

		$row = $db->fetchAll($selectData);
		
		/*$i=0;
		foreach($row as $data){
			
			if($data['balance'] == 0){
				unset($row[$i]);
			}
			
			$i++;
			
		}*/

		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
public function getOutstandingInv($searchData=null){
		
		/* checking balance from 
    	 * 1) receipt_invoice
    	 * 2) advance payment utilization
    	 * 3) discount
    	 * 4) CN drop by asad
    	 */
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoice_id = $searchData['invoice_id'];
		$inv_Detail = $searchData['invoice_detail_id'];
		
		$service = new cronService();
		$db = $service->conn;
		
		// 1) receipt_invoice
		$selectData = $db->select()
					->from(array('ri'=>'receipt_invoice'),array('rcp_inv_amount'))
					->joinLeft(array('b'=>'receipt'),'b.rcp_id = ri.rcp_inv_rcp_id AND b.rcp_status = "APPROVE"',array())
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = b.rcp_cur_id',array())
					->joinLeft(array('a'=>'invoice_main'),'a.id = ri.rcp_inv_invoice_id',array('exchange_rate','currency_id'))
					//->where("b.rcp_status = 'APPROVE'")
					->where('ri.rcp_inv_invoice_id = ?', $invoice_id)
					->where('ri.rcp_inv_invoice_dtl_id = ?', $inv_Detail)
					->where("DATE(b.rcp_receive_date) >= ?",$dateFrom)
					->where("DATE(b.rcp_receive_date) <= ?",$dateTo);

		$row = $db->fetchAll($selectData);
		
		$paidAmount=0;
		if($row){
			foreach($row as $data){
				$paidAmount += $data['rcp_inv_amount'];
			}
		}
		
		// 2) advance payment utilization
		$selectData2 = $db->select()
					->from(array('d'=>'advance_payment_detail',array('advpydet_total_paid')))
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.advpydet_inv_id',array('exchange_rate'))
					->join(array('da'=>'advance_payment'), 'd.advpydet_advpy_id = da.advpy_id',array('currency_id'=>'advpy_cur_id'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = da.advpy_cur_id',array())
					->where("da.advpy_status = 'A'")
					->where('d.advpydet_inv_id = ?', $invoice_id)
					->where('d.advpydet_inv_det_id = ?', $inv_Detail)
					->where("DATE(d.advpydet_upddate) >= ?",$dateFrom)
					->where("DATE(d.advpydet_upddate) <= ?",$dateTo);
		
		$row2 = $db->fetchAll($selectData2);
		
//		echo "<pre>";
//		print_r($row2);
		if($row2){
			foreach($row2 as $data){
	//			$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
	//			$dataCur = $curRateDB->getRateByDate($data['currency_id'],$data['advpydet_upddate']);//usd
	//			
				$amountCur = $data['advpydet_total_paid'];
	////			if($data['currency_id'] == 2){
	//				$amountCur = round($amountCur * $dataCur['cr_exchange_rate'],2);
	//			}
				$paidAmount += $amountCur;
			}
		}
		
		
		// 3) discount
		$selectData3 = $db->select()
					->from(array('d'=>'discount_detail'),array('dcnt_amount'))
					->join(array('da'=>'discount'),'da.dcnt_id = d.dcnt_id',array())
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.dcnt_invoice_id',array('exchange_rate','currency_id'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array())
					->where("d.status = 'A'")
					->where('d.dcnt_invoice_id = ?', $invoice_id)
					->where('d.dcnt_invoice_det_id = ?', $inv_Detail)
					->where("DATE(da.dcnt_create_date) >= ?",$dateFrom)
					->where("DATE(da.dcnt_create_date) <= ?",$dateTo);
		
		$row3 = $db->fetchAll($selectData3);
		
		if($row3){
			foreach($row3 as $data){
				$paidAmount += $data['dcnt_amount'];
			}
		}
		
//		echo "<hr>".$paidAmount."<br>";
		return $paidAmount;
	}
	
public function getSearchDataCN($searchData=null){
	
		$service = new cronService();
		$db = $service->conn;
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoiceID = $searchData['invoice_id'];
		$invoiceDetailID = $searchData['invoice_detail_id'];
		
		$selectData = $db->select()
						->from(array('a'=>'credit_note_detail'),array('cur_id','cnamount'=>'amount'))
						->join(array('sc'=>'credit_note'), 'sc.cn_id = a.cn_id',array('cnamountmain'=>'sc.cn_amount','*'))
						->where('a.invoice_main_id = ?',$invoiceID)
						->where('a.invoice_detail_id = ?',$invoiceDetailID)
						->where("sc.cn_status ='A'");
											
		if($dateFrom!=''){
			$selectData->where("DATE(sc.cn_create_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(sc.cn_create_date) <= ?",$dateTo);
		}	
		
		
		$row = $db->fetchRow($selectData);

		return $row;
	}
	
	public function saveToDB($arrayStack,$id){
		
		$service = new cronService();
		$db = $service->conn;

		foreach($arrayStack as $stk){
			$data['date'] = $stk['invoice_date'];
			$data['invoice_no'] = $stk['invoice_no'];
			$data['registrationId'] = $stk['registrationId'];
			$data['os_day'] = $stk['days'];
			
			$this->conn->insert('cron_ageing', $data);
		}
		
		
		
	}
	public function generateExcel($arrayStack,$dirName,$id){
		
		$service = new cronService();
		$db = $service->conn;
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("SMS")
									 ->setTitle("SOA Detail")
									 ->setSubject("SOA Detail");
		
		// Add some data
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Date')
		                              ->setCellValue('B1', 'Invoice No')
		                              ->setCellValue('C1', 'Student ID')
		                              ->setCellValue('D1', 'Student Name')
		                              ->setCellValue('E1', 'Status')
		                              ->setCellValue('F1', 'Semester')
		                              ->setCellValue('G1', 'Programme')
		                              ->setCellValue('H1', 'Programme Mode')
		                              ->setCellValue('I1', 'Study Mode')
		                              ->setCellValue('J1', 'Programme Type')
		                              ->setCellValue('K1', 'Fee Type')
		                              ->setCellValue('L1', 'Course')
		                              ->setCellValue('M1', 'Funding Method')
		                              ->setCellValue('N1', 'Discount')
		                              ->setCellValue('O1', 'Email')
		                              ->setCellValue('P1', 'Amount (USD)')
		                              ->setCellValue('Q1', 'Amount (MYR)')
		                              ->setCellValue('R1', 'Days');
		
		                              
		$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);
		                              
		//lets manipulate the data
		if($arrayStack){
			$noExcel = 2;
			foreach ($arrayStack as $entry):
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$noExcel, date('d-m-Y',strtotime($entry['invoice_date'])));
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$noExcel, $entry['invoice_no']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$noExcel, $entry['registrationId']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$noExcel, $entry['student_fullname']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$noExcel, $entry['profileStatus']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$noExcel, $entry['SemesterMainName']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$noExcel, $entry['applicant_program']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$noExcel, $entry['mop']);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$noExcel, $entry['mos']);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$noExcel, $entry['pt']);
				
				$desc = '';
				if($entry['MigrateCode']!=''){
					$desc = $entry['bill_description'];
				}else{
					$desc = $entry['fi_name'];
				}
		
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$noExcel, $desc);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$noExcel, $entry['SubCode']);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$noExcel, $entry['funding']);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$noExcel, $entry['dt_discount']);
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$noExcel, $entry['appl_email']);
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$noExcel, ($entry['currency_id'] == 2)?number_format($entry['balance'],2):'');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$noExcel, $entry['amount_convert']);
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$noExcel, $entry['days']);
			
				$noExcel++;	
			endforeach;
		}                        
		
		//sheet name
		$objPHPExcel->getActiveSheet()->setTitle('Ageing');
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		//filename
		$fileName = date('Ymd_His').'_ageing.xlsx';
		
		//save into folder
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($dirName.'/'.$fileName);
		
		//update filepath
		$pathName = "report/finance/".date('Ym')."/".$fileName;
		
		$dataUpdate = array(
			'filepath' => $pathName, 
			'path' => $dirName.'/'.$fileName, 
			'filename' => $fileName, 
		);
		
		$db->update('finance_service', $dataUpdate, 'id_cron ='.  $id );
		
		
	}
	
	public function createUploadFile()
	{
		
		$folderDate = date('Ymd');
        $dir = $this->config->constants->APP_DOC_PATH."/report/finance/".$folderDate;
            
        if ( !is_dir($dir) ){
            if ( mkdir_p($dir) === false )
                {
                   echo 'Cannot create attachment folder ('.$dir.')';
                }
        }
        
        return $dir;
			
	}
	

		
	
}

//Process
$service = new cronService();

$select = $service->conn->select()->from(array('a'=>'cron_service'))->where('a.executed = 0 AND inprogress = 0')->limit(5);
$results = $service->conn->fetchAll($select);

foreach ( $results as $row )
{
	if ( is_callable( array( $service, $row['action']) ) )
	{
		$exec = 1;
		$recurring = 0;

		if ( $row['recurring'] == 1)
		{
			$exec = 0;

			$checkdate = strtotime($row['recurring_day'].'-'.date('m-Y'));

			if ( $row['recurring_type'] == 'month' )
			{

				if ( date('d') == date('d', $checkdate) && strtotime(date('d-m-Y')) > strtotime($row['recurring_lastexecuted']) )
				{
					$exec = 1;
					$recurring = 1;
				}
			}
			else if ( $row['recurring_type'] == 'day' )
			{
				$howmany = howDays( strtotime($row['recurring_lastexecuted']), time() );

				if ( $howmany >= $row['recurring_day'] )
				{
					$exec = 1;
					$recurring = 1;

				}
			}
		}

		if ( $exec )
		{
			$service->time_start($row['id']);
			
			$func_results = call_user_func( array( $service, $row['action']) );

			//done
			$service->close($row['id'], $func_results, $recurring );
		}

		//updatereport soa
		if($row['action'] == 'reportSoaDetail'){
			$service->updateSoa($row['id']);
		}
	}
}

echo 'Done. '.time();
exit;

function howDays($from, $to) {
    $first_date = $from;
    $second_date = $to;
    $offset = $second_date-$first_date; 
    return floor($offset/60/60/24);
}
?>