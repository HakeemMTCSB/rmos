<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/9/2015
 * Time: 11:37 AM
 */
class updatecurrentsemesterstatus{
    public $conn;

    public function __construct(){

        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
    }

    public function process(){
        $list = $this->getStudentList();

        //var_dump($list);
        //exit;

        if ($list){
            foreach ($list as $loop){
                $intakeInfo = $this->getIntake($loop['IdIntake']);
                $semInfo = $this->getSemester($loop['IdScheme'], $intakeInfo['sem_year'], $intakeInfo['sem_seq']);
                $curSem = $this->getCurrentSemester($loop['IdScheme']);

                if ($curSem){
                    $checkSubjectReg = $this->checkSubjectRegister($loop['IdStudentRegistration'], $curSem['IdSemesterMaster']);
                    if ($checkSubjectReg){
                        $status = 130;
                    }else{
                        //check dormant or not
                        $getLastSemReg = $this->getLastSemesterRegister($loop['IdStudentRegistration']);

                        if ($getLastSemReg){
                            $date1 = $getLastSemReg['SemesterMainStartDate'];
                            $substractvalue = 2;
                        }else{
                            $date1 = $semInfo['SemesterMainStartDate'];
                            $substractvalue = 1;
                        }

                        if ($loop['IdScheme']==1){ //ps
                            $semType = 0;
                            $consecutivesem = 3;
                        }else if($loop['IdScheme']==11){ //gs
                            $semType = 172;
                            $consecutivesem = 2;
                        }else{ //open
                            $semType = 172;
                            $consecutivesem = 2;
                        }

                        $listUnregSem = $this->listUnregisteredSemester($loop['IdScheme'], $date1, $curSem['SemesterMainStartDate'], $semType);

                        $semcount = count($listUnregSem)-$substractvalue;
                        //var_dump($listUnregSem); //exit;
                        if ($semcount >= $consecutivesem){ //dormant
                            $status = 844;
                        }else{ //unregistered
                            $status = 131;
                        }
                    }

                    $checkSemStatus = $this->checkSemesterStatus($loop['IdStudentRegistration'], $curSem['IdSemesterMaster']);

                    if ($checkSemStatus){
                        if ($checkSemStatus['studentsemesterstatus']!=250) {
                            $data = array(
                                'IdBlock' => null,
                                'studentsemesterstatus' => $status,
                                'UpdDate' => date('Y-m-d h:i:s'),
                                'UpdUser' => 1,
                                'UpdRole' => 'admin'
                            );

                            $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                        }
                    }else{
                        $data = array(
                            'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                            'idSemester'=>$curSem['IdSemesterMaster'],
                            'IdSemesterMain'=>$curSem['IdSemesterMaster'],
                            'IdBlock'=>null,
                            'studentsemesterstatus'=>$status,
                            'Level'=>1,
                            'UpdDate'=>date('Y-m-d h:i:s'),
                            'UpdUser'=>1,
                            'UpdRole'=>'admin'
                        );
                        $idsemstatus = $this->insertSemesterStatus($data);

                        $data2 = array(
                            'idstudentsemsterstatus'=>$idsemstatus,
                            'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                            'idSemester'=>$curSem['IdSemesterMaster'],
                            'IdSemesterMain'=>$curSem['IdSemesterMaster'],
                            'IdBlock'=>null,
                            'studentsemesterstatus'=>$status,
                            'Level'=>1,
                            'UpdDate'=>date('Y-m-d h:i:s'),
                            'UpdUser'=>1,
                            'UpdRole'=>'admin',
                            'Type'=>null,
                            'message'=>0,
                            'createddt'=>date('Y-m-d h:i:s'),
                            'createdby'=>1
                        );
                        $this->insertSemesterStatusHistory($data2);
                    }
                }
                //exit;
            }
        }
    }

    public function getStudentList(){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregistration'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram=b.IdProgram')
            //->where('a.IdStudentRegistration = ?', 4250)
            ->where('a.profileStatus = ?', 92);
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }

    public function getCurrentSemester($idScheme){
        $date = date('Y-m-d');

        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date);

        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }

    public function checkSemesterStatus($idstudent, $idsemester){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentsemesterstatus'))
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.idSemester = ?', $idsemester);

        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }

    public function checkSubjectRegister($idstudent, $idsemester){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.IdSemesterMain = ?', $idsemester)
            ->where('a.Active <> ?', 3);

        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }

    public function insertSemesterStatus($data){
        $this->conn->insert('tbl_studentsemesterstatus', $data);
        $id = $this->conn->lastInsertId('tbl_studentsemesterstatus', 'idstudentsemsterstatus');
        return $id;
    }

    public function insertSemesterStatusHistory($data){
        $this->conn->insert('tbl_studentsemesterstatus_history', $data);
        $id = $this->conn->lastInsertId('tbl_studentsemesterstatus_history', 'idstudentsemsterstatus');
        return $id;
    }

    public function updateSemesterStatus($data, $id){
        $update = $this->conn->update('tbl_studentsemesterstatus', $data, 'idstudentsemsterstatus = '.$id);
        return $update;
    }

    public function getLastSemesterRegister($idstudent){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentsemesterstatus'))
        //$sql = $this->conn->select()->from(array('a'=>'tbl_studentregsubjects'))
            //->join(array('a'=>'tbl_semestermaster'), 'a.IdSemesterMain=b.IdSemesterMaster')
            ->join(array('b'=>'tbl_semestermaster'), 'a.IdSemesterMain=b.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('b.IsCountable = ?', 1)
            //->where('a.Active <> ?', 3)
            ->where('a.studentsemesterstatus = 130 OR a.studentsemesterstatus = 250')
            ->order('b.SemesterMainStartDate DESC');

        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }

    public function listUnregisteredSemester($scheme, $date1, $date2, $semType=0){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.SemesterMainStartDate >= ?', $date1)
            ->where('a.SemesterMainStartDate <= ?', $date2)
            ->where('a.IsCountable = ?', 1)
            ->where('a.special_semester = ?', 0)
            ->where('a.SemesterFunctionType IS NULL');

        if ($semType!=0){
            $sql->where('a.SemesterType = ?', $semType);
        }

        $sql->order('a.SemesterMainStartDate ASC');

        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }

    public function getIntake($id){
        $sql = $this->conn->select()->from(array('a'=>'tbl_intake'))
            ->where('a.IdIntake = ?', $id);

        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }

    public function getSemester($idScheme, $year, $month){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month);

        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
}

//run this function
$status = new updatecurrentsemesterstatus();
$status->process();
?>