<?php 
class Migrate {

    public $conn;
    
    public function __construct()
    {
        $this->conn = new PDO('mysql:host=localhost;dbname=meteor_cmspro', 'root', 'inceif4dm1n#');
    }
    
    public function import_positions() {
        global $conn;
        
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $select = $this->conn->query("SELECT position FROM migrate_staff GROUP BY position");
        try {
            $i = 1;
            foreach($select as $row) {
                $insert_positions = array(
                    'idDefType' => '3',
                    'DefinitionCode' => 'A'.$i,
                    'DefinitionDesc' => $row['position'],
                    'Status' => 0,
                    'BhasaIndonesia' => '-'
                );
                $i++;
                
                $insert = "INSERT INTO tbl_definationms(idDefType,
                            DefinitionCode,
                            DefinitionDesc,
                            Status,BhasaIndonesia) VALUES (
                            :idDefType,
                            :DefinitionCode,
                            :DefinitionDesc,
                            :Status,:BhasaIndonesia)";
                
                $stmt = $this->conn->prepare($insert);
                                                              
                $stmt->bindParam(':idDefType', $insert_positions['idDefType'], PDO::PARAM_STR);      
                $stmt->bindParam(':DefinitionCode', $insert_positions['DefinitionCode'], PDO::PARAM_STR);
                $stmt->bindParam(':DefinitionDesc', $insert_positions['DefinitionDesc'], PDO::PARAM_STR);
                $stmt->bindParam(':Status', $insert_positions['Status'], PDO::PARAM_STR);
                $stmt->bindParam(':BhasaIndonesia', $insert_positions['BhasaIndonesia'], PDO::PARAM_STR);
               
                                                      
                $stmt->execute();                 
               
            }
            
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "department";
            die();
        }
        
        $this->import_department();
    }
    
    public function import_department() {
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $select = $this->conn->query("SELECT department FROM migrate_staff GROUP BY department");
        try {
            $i = 1;
            foreach($select as $row) {
                $insert_department = array(
                    'DepartmentType' => 0,
                    'IdCollege' => 0,
                    'DeptCode' => 'B'.$i,
                    'DepartmentName' => $row['department'],
                    'Active' => 1,
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => 1
                );
                $i++;
                
                $insert = "INSERT INTO tbl_departmentmaster(DepartmentType,
                            IdCollege,
                            DeptCode,
                            DepartmentName,
                            Active,UpdDate,UpdUser) VALUES (
                            :DepartmentType,
                            :IdCollege,
                            :DeptCode,
                            :DepartmentName,
                            :Active,
                            :UpdDate,:UpdUser)";
                
                $stmt = $this->conn->prepare($insert);
                                                              
                $stmt->bindParam(':DepartmentType', $insert_department['DepartmentType'], PDO::PARAM_STR);      
                $stmt->bindParam(':IdCollege', $insert_department['IdCollege'], PDO::PARAM_STR);
                $stmt->bindParam(':DeptCode', $insert_department['DeptCode'], PDO::PARAM_STR);
                $stmt->bindParam(':DepartmentName', $insert_department['DepartmentName'], PDO::PARAM_STR);
                $stmt->bindParam(':Active', $insert_department['Active'], PDO::PARAM_STR);
                $stmt->bindParam(':UpdDate', $insert_department['UpdDate'], PDO::PARAM_STR);
                $stmt->bindParam(':UpdUser', $insert_department['UpdUser'], PDO::PARAM_STR);
               
                                                      
                $stmt->execute();                 
               
            }
            
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "position";
            die();
        }
        
        $this->import_staff();
    }
    
    public function import_staff() {
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $select = $this->conn->query("SELECT personel_number,salutation,name,position,department,email 
            FROM migrate_staff");
            
        try {
            $i = 1;
            foreach($select as $row) {
                
                $getPosition = "SELECT * FROM tbl_definationms WHERE idDefType = 3 AND DefinitionDesc = ?";
                $gtp = $this->conn->prepare($getPosition);
                $gtp->execute(array($row['position']));
                $gtp_r = $gtp->fetch();
                
                $getDepartment = "SELECT * FROM tbl_departmentmaster WHERE DepartmentName = ?";
                $gtd = $this->conn->prepare($getDepartment);
                $gtd->execute(array($row['department']));
                $gtd_r = $gtd->fetch();
                
                $first = explode(' ',$row['name']);
                
                $insert_staff = array(
                    'StaffId'           => $row['personel_number'],
                    'FrontSalutation'   => $row['salutation'],
                    'FirstName'         => $first[0],
                    'FullName'          => $row['name'],
                    'IdDepartment'  => $gtd_r['IdDepartment'],
                    'IdLevel'       => $gtp_r['idDefinition'],
                    'Email'         => $row['email'],
                    'Active'        => 1,
                    'StaffType'     => 0,
                    'Staff'         => 0,
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => 1
                );
                $i++;
                
                $insert = "INSERT INTO tbl_staffmaster(StaffId,
                            FrontSalutation,
                            FirstName,
                            FullName,
                            IdDepartment,
                            IdLevel,
                            Email,
                            StaffType,
                            Active,UpdDate,UpdUser) VALUES (
                            :StaffId,
                            :FrontSalutation,
                            :FirstName,
                            :FullName,
                            :IdDepartment,
                            :IdLevel,
                            :Email,
                            :StaffType,
                            :Active,
                            :UpdDate,
                            :UpdUser
                            )";
                
                $stmt = $this->conn->prepare($insert);
                                                              
                $stmt->bindParam(':StaffId', $insert_staff['StaffId'], PDO::PARAM_STR);      
                $stmt->bindParam(':FrontSalutation', $insert_staff['FrontSalutation'], PDO::PARAM_STR);
                $stmt->bindParam(':FirstName', $insert_staff['FirstName'], PDO::PARAM_STR);
                $stmt->bindParam(':FullName', $insert_staff['FullName'], PDO::PARAM_STR);
                $stmt->bindParam(':IdDepartment', $insert_staff['IdDepartment'], PDO::PARAM_STR);
                $stmt->bindParam(':IdLevel', $insert_staff['IdLevel'], PDO::PARAM_STR);
                $stmt->bindParam(':Email', $insert_staff['Email'], PDO::PARAM_STR);
                $stmt->bindParam(':StaffType', $insert_staff['StaffType'], PDO::PARAM_STR);
                $stmt->bindParam(':Active', $insert_staff['Active'], PDO::PARAM_STR);
                $stmt->bindParam(':UpdDate', $insert_staff['UpdDate'], PDO::PARAM_STR);
                $stmt->bindParam(':UpdUser', $insert_staff['UpdUser'], PDO::PARAM_STR);
                                                                    
                $stmt->execute();
               //print_r($insert_staff);
            }
            
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "staff";
            die();
        }
    }
}
  
    
    
    $Migrate = new Migrate();
    //$Migrate->import_positions();
    
    $Migrate->import_staff();
    
?>