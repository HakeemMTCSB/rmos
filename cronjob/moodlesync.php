<?php

//comment
    //$argument1 = $argv[1];

    ini_set('display_errors',1);
  	
	date_default_timezone_set('Asia/Kuala_Lumpur');
	
	$paths = array(
	    realpath(dirname(__FILE__) . '/../library'),
	    '.',
	);
	set_include_path(implode(PATH_SEPARATOR, $paths));
	
	
	require_once('/var/www/html/sis/library/Zend/Loader/Autoloader.php');
	//require_once('/Users/alif/git/sis/library/Zend/Loader/Autoloader.php');
	$loader = Zend_Loader_Autoloader::getInstance();
	
	$config = new Zend_Config_Ini('/var/www/html/sis/application/configs/application.ini',"development");
	
	//get database
	$dbsms = new Zend_Db_Adapter_Pdo_Mysql(array(
	    'host'     => $config->resources->db->params->host,
	    'username' => $config->resources->db->params->username,
	    'password' => $config->resources->db->params->password,
	    'dbname'   => $config->resources->db->params->dbname
	));
	
	$dbmoodle= new Zend_Db_Adapter_Pdo_Mysql(array(
	    'host'     => MOODLE_DBSERVER,
	    'username' => MOODLE_DBNAME,
	    'password' => MOODLE_DBUSER,
	    'dbname'   => MOODLE_DBPASS
	));	
	
	$sql = $dbmoodle->select()
			->from("mdl_user");
			
	

?>