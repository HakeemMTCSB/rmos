<?php

//set unlimited
ini_set('display_errors', 'On');
ini_set('html_errors', 0);
error_reporting(-1);
set_time_limit(0);
ini_set('memory_limit', '-1');
date_default_timezone_set('Asia/Kuala_Lumpur');

class cronService
{
	public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);
        
        require_once($path.'/library/Cms/common_functions.php');
        require_once($path.'/library/PHPExcel/Classes/PHPExcel.php');
        require_once($path.'/application/modules/studentfinance/models/DbTable/PaymentAmountCron.php');

    }
	
	public function close($id, $results=null,$recurring=0) 
	{
		$dataUpdate = array(
								'inprogress' => 0,
								'executed_date' => new Zend_Db_Expr('NOW()'), 
								'executed' => 1, 
								'execution_time' => $this->time_end(),
								'results' => $results
							);
		
		if ( $recurring == 1)
		{
			$dataUpdate['recurring_lastexecuted'] = new Zend_Db_Expr('NOW()');
			$dataUpdate['executed'] = 0;
		}
		
		$this->conn->update('cron_service', $dataUpdate, array('id = ?' =>  $id) );
	}
	
	function time_start($id)
	{
		$starttime = microtime();	
		$starttime = explode(" ",$starttime);
		$starttime = $starttime[1] + $starttime[0];

		$this->_TIMESTART = $starttime;
	
		//update into in progress
		$dataUpdate = array(
								'inprogress' => 1, 
							);
		
		$this->conn->update('cron_service', $dataUpdate, array('id = ?' =>  $id) );
	}

	function time_end()
	{
		$starttime =  $this->_TIMESTART;

		$endtime = microtime();
		$endtime = explode(" ",$endtime);
		$endtime = $endtime[1] + $endtime[0];
		$stime = $endtime - $starttime;
		return round($stime,4);
	}
	
	public function updateSoa($id) 
	{
		
		$dataUpdate = array(
			'executed_date' => new Zend_Db_Expr('NOW()'), 
			'executed' => 1, 
		);
		
		$this->conn->update('finance_service', $dataUpdate, 'id_cron ='.  $id );
	}
	
	
	/*
		REPORT SAMPLE
	*/
	public function reportSample()
	{
		//do something
		echo '123';

		return 'Any remarks/results or URL or whatever. Will be saved in service table for reference or linking with action if needed.';
	}
	
	public function reportSoaDetail()
	{
        
		$service = new cronService();
		$db = $service->conn;
		
		//get search parameter
		$select = $service->conn->select()
			->from(array('a'=>'finance_service'))
			->where('a.executed = 0')
			->order('a.created_date asc');
		$results = $service->conn->fetchRow($select);
		
		$idCron = $results['id_cron'];
		
		$type = $results['type'];
		$dateFrom = $results['date_from'];
		$dateTo = $results['date_to'];
		$student_Array = explode(',',$results['student_id']);
		$n = count($student_Array);

			$arrayStack = array();
			$i=0;
			while($i < $n){
				
				$trans_id = $student_Array[$i];
				
		    	//get profile
		    	if($type == 1){ //applicant
			    	
			    	$select = $db->select()
						->from(array('ApplicantTransaction' => 'applicant_transaction'), array('ApplicantTransaction.*','IdBranch'=>'ApplicantTransaction.branch_id'))
						->joinLeft(array('ApplicantProfile' => 'applicant_profile' ), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id', 
							array ('ApplicantProfile.*', "
								CONCAT_WS (
								' ',
								IFNULL(ApplicantProfile.appl_fname,''),
								IFNULL(ApplicantProfile.appl_mname,''),
								IFNULL(ApplicantProfile.appl_lname,'')
								) as name"
							))
			            ->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=ApplicantTransaction.at_trans_id')
			            ->join(array('prg' =>'tbl_program'), 'prg.IdProgram=apr.ap_prog_id', array('ArabicName','ProgramName','ProgramCode'))
			            ->join(array('bch' =>'tbl_branchofficevenue'), 'bch.IdBranch = ApplicantTransaction.branch_id', array('BranchName'))
			            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=apr.ap_prog_scheme', array())
			            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
			            ->join(array('i'=>'tbl_intake'),'i.IdIntake=ApplicantTransaction.at_intake', array('IntakeDesc','IntakeDefaultLanguage'))
						->where('ApplicantTransaction.at_trans_id = ?', $trans_id);
					
					$profile = $db->fetchRow($select);
			    	$appl_id = $profile['appl_id'];
		    	}elseif($type == 2){ //student
		    		 $select = $db->select()
                        ->from(array('a' => 'tbl_studentregistration'), array('a.registrationId', 'a.IdProgram','at_pes_id'=>'a.registrationId','*'))
                        ->joinLeft(array('as' => 'student_profile'), "a.sp_id = as.id", array("CONCAT_WS(' ',IFNULL(as.appl_fname,''),IFNULL(as.appl_mname,''),IFNULL(as.appl_lname,'')) as name",'*' ))
                        ->joinLeft(array('b' => 'tbl_program'), "a.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme",'ProgramCode'))
                        ->join(array('bch' =>'tbl_branchofficevenue'), 'bch.IdBranch = a.IdBranch', array('BranchName'))
                        ->joinLeft(array('c' => 'tbl_intake'), "a.IdIntake = c.IdIntake", array("c.IntakeDesc"))
                        ->joinLeft(array('d' => 'tbl_definationms'), "a.profileStatus = d.idDefinition", array("d.DefinitionDesc"))
                        ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=a.IdProgramScheme', array())
			            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
                        ->where("a.IdStudentRegistration =?", $trans_id);
       					$profile = $db->fetchRow($select);
		    	}
		    	
	    	if($profile){
		    	
				$arrayStack[$i]['profile'] = $profile;
				
				$select_invoice = $db->select()
										->from(array('im'=>'invoice_main'),array(
											'id'=>'im.id',
											'record_date'=>'im.invoice_date',
											'description' => 'fc.fi_name',
											'txn_type' => new Zend_Db_Expr ('"Invoice"'),
											'debit' =>'bill_amount',
											'credit' => new Zend_Db_Expr ('"0.00"'),
											'document' => 'bill_number',
											'invoice_no' => 'bill_number',
											'receipt_no' => new Zend_Db_Expr ('"-"'),
											'subject' => 's.SubCode',
											'fc_seq'=> 'im.id',
											'migrate_desc' => 'bill_description',
											'migrate_code' => 'MigrateCode',
											'exchange_rate'=>'im.exchange_rate',
										)
								)
								->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id=im.id',array())
								->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id',array())
								->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=ivs.subject_id',array())
								->joinLeft(array('fc'=>'fee_item'),'fc.fi_id=ivd.fi_id',array())
								->join(array('c'=>'tbl_currency'),'c.cur_id=im.currency_id', array('cur_code','cur_id','cur_desc'))		
								//->where('im.currency_id= ?',$currency)						  
								
								->where("im.status = 'A'")
								->group('im.id');
					if($type == 1){
						$select_invoice->where('im.trans_id = ?', $trans_id);
					}else{
						$select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
					}
					
					/*if($dateFrom!=''){
						$select_invoice->where("DATE(im.invoice_date) >= ?",$dateFrom);
					}
					
					if($dateTo!=''){
						$select_invoice->where("DATE(im.invoice_date) <= ?",$dateTo);
					}*/
									
					$select_payment = $db->select()
									->from(
										array('pm'=>'receipt'),array(
												'id'=>'pm.rcp_id',
												'record_date'=>'pm.rcp_receive_date',
												'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
												'txn_type' => new Zend_Db_Expr ('"Payment"'),
												'debit' =>new Zend_Db_Expr ('"0.00"'),
												'credit' => 'rcp_amount',
												'document' => 'rcp_no',
												'invoice_no' => 'rcp_no',
												'receipt_no' => 'rcp_no',
												'subject' =>  new Zend_Db_Expr ('""'),
												'fc_seq'=> 'c.cur_id'	,
												'migrate_desc' => 'pm.rcp_description',
												'migrate_code' => 'MigrateCode',		
												'exchange_rate' =>  new Zend_Db_Expr ('""'),															
											)
									)
									
									->join(array('p'=>'payment'),'p.p_rcp_id = pm.rcp_id', array())	
									->join(array('c'=>'tbl_currency'),'c.cur_id=pm.rcp_cur_id', array('cur_code','cur_id','cur_desc'))	
									//->where('pm.rcp_cur_id= ?',$currency)		
									->where("pm.rcp_status = 'APPROVE'");
					if($type == 1){
						$select_payment->where('pm.rcp_trans_id = ?', $trans_id);
					}else{
						$select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
					}				

					/*if($dateFrom!=''){
						$select_payment->where("DATE(pm.rcp_receive_date) >= ?",$dateFrom);
					}
					
					if($dateTo!=''){
						$select_payment->where("DATE(pm.rcp_receive_date) <= ?",$dateTo);
					}
					*/
					$select_credit = $db->select()
									->from(
										array('cn'=>'credit_note'),array(
												'id'=>'cn.cn_id',
												'record_date'=>'cn.cn_create_date',
												'description' => 'cn.cn_description',
												'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
												'debit' =>new Zend_Db_Expr ('"0.00"'),
												'credit' => 'cn_amount',
												'document' => 'cn_billing_no',
												'invoice_no' => 'cn_billing_no',
												'receipt_no' => 'cn_billing_no',
												'subject' =>  new Zend_Db_Expr ('""'),
												'fc_seq'=> 'c.cur_id',
												'migrate_desc' => 'cn.cn_description',	
										 		'migrate_code' => 'MigrateCode',
												'exchange_rate' =>  'i.exchange_rate',																
											)
									)
									
									->join(array('c'=>'tbl_currency'),'c.cur_id=cn.cn_cur_id', array('cur_code','cur_id','cur_desc'))	
									->joinLeft(array('i'=>'invoice_main'),'i.id = cn.cn_invoice_id', array())	
									//->where('pm.rcp_cur_id= ?',$currency)		
									->where("cn.cn_status = 'A'");
					if($type == 1){
						$select_credit->where('cn.cn_trans_id = ?', $trans_id);
					}else{
						$select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
					}	
					
					/*if($dateFrom!=''){
						$select_credit->where("DATE(cn.cn_create_date) >= ?",$dateFrom);
					}
					
					if($dateTo!=''){
						$select_credit->where("DATE(cn.cn_create_date) <= ?",$dateTo);
					}*/
					
					$select_discount = $db->select()
							->from(
								array('a'=>'discount_detail'),array(
										'id'=>'dn.dcnt_id',
										'record_date'=>'dn.dcnt_approve_date',
										'description' => 'dn.dcnt_description',
										'txn_type' => new Zend_Db_Expr ('"Discount"'),
										'debit' =>new Zend_Db_Expr ('"0.00"'),
										'credit' => 'SUM(a.dcnt_amount)',
										'document' => 'dn.dcnt_fomulir_id',
										'invoice_no' => 'dn.dcnt_fomulir_id',
										'receipt_no' => 'dn.dcnt_fomulir_id',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'dn.dcnt_description',	
								 		'migrate_code' => new Zend_Db_Expr ('""'),
										'exchange_rate' =>  'i.exchange_rate',																
									)
							)
							
							->join(array('dn'=>'discount'),'dn.dcnt_id = a.dcnt_id', array())	
							->join(array('c'=>'tbl_currency'),'c.cur_id=dn.dcnt_currency_id', array('cur_code','cur_id','cur_desc'))	
							->joinLeft(array('i'=>'invoice_main'),'i.id = a.dcnt_invoice_id', array())	
							//->where('pm.rcp_cur_id= ?',$currency)		
							->where("dn.dcnt_status = 'A'")
							->group('dn.dcnt_id');
							
					if($type == 1){
						$select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
					}else{
						$select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
					}

					/*if($dateFrom!=''){
						$select_discount->where("DATE(dn.dcnt_approve_date) >= ?",$dateFrom);
					}
					
					if($dateTo!=''){
						$select_discount->where("DATE(dn.dcnt_approve_date) <= ?",$dateTo);
					}*/
					
					$select_refund = $db->select()
							->from(
								array('rn'=>'refund'),array(
										'id'=>'rfd_id',
										'record_date'=>'rfd_date',
										'description' => 'rfd_desc',
										'txn_type' => new Zend_Db_Expr ('"Refund"'),
										'debit' => 'rfd_amount',
										'credit' => new Zend_Db_Expr ('"0.00"'),
										'document' => 'rfd_fomulir',
										'invoice_no' => 'rfd_fomulir',
										'receipt_no' => 'rfd_fomulir',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'rfd_desc',	
								 		'migrate_code' => 'MigrateCode',
										'exchange_rate' =>  'rfd_exchange_rate',																
									)
							)
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=rn.rfd_currency_id', array('cur_code','cur_id','cur_desc'))	
							->where("rn.rfd_status = 'A'");
							
					if($type == 1){
						$select_refund->where('rn.rfd_trans_id = ?', $trans_id);
					}else{
						$select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
					}	
				
					/*if($dateFrom!=''){
						$select_refund->where("DATE(rn.rfd_date) >= ?",$dateFrom);
					}
					
					if($dateTo!=''){
						$select_refund->where("DATE(rn.rfd_date) <= ?",$dateTo);
					}*/
											
					//get array payment				
					$select = $db->select()
							    ->union(array($select_invoice, $select_payment,$select_credit,$select_discount,$select_refund),  Zend_Db_Select::SQL_UNION_ALL)
							    ->order("record_date asc")
							    ->order("txn_type asc")
							   
							   ;
				$results = $db->fetchAll($select);
				
				if(!$results){
					$results = null;
				}
				
				$arrayStack[$i]['soa'] = $results;
				
					//process data
					$amountCurr = array();
					$curencyArray = array();
					$balance = 0;
					
					if($results){
						foreach ( $results as $row )
						{
							$curencyArray[$row['cur_id']] = $row['cur_code'];
							
							if($row['exchange_rate']){
								$dataCur = $this->getDataCurrency($row['exchange_rate']);
							}else{
								$dateTrans = date('Y-m-d', strtotime($row['record_date']) );
								$dataCur = $this->getRateByDate($row['cur_id'],$dateTrans);
							}
								
							if($row['cur_id'] == 2){
								$amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'],2);
								$amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'],2);
							}else{
								$amountDebit = $row['debit'];
								$amountCredit = $row['credit'];
							}
				
							if( $row['txn_type']=="Invoice" ){
								$balance = $balance + $amountDebit;
							}else
							if( $row['txn_type']=="Payment" ){
					
								$balance = $balance - $amountCredit;
								
							}else
							if( $row['txn_type']=="Credit Note" ){
								$balance = $balance - $amountCredit;
							}else
							if( $row['txn_type']=="Debit Note" ){
								$balance = $balance + $row['debit'];
							}else
							if( $row['txn_type']=="Refund" ){
								$balance = $balance + $row['debit'];
							}
							
							$amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
							$amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
							$amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');
							
						}
					}
		
				    $curAvailable = array('2'=>'USD','1'=>'MYR');
				    $arrayStack[$i]['amountCur'] = $amountCurr;
	    		}
					
				$i++;
			}
			
			$dirName = $this->createUploadFile();
			$fileExcel = $this->generateExcel($arrayStack,$curAvailable,$dirName,$idCron);

		return 'soa detail';
	}
	
	public function generateExcel($arrayStack,$curAvailable,$dirName,$id){
		
		$service = new cronService();
		$db = $service->conn;
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("SMS")
									 ->setTitle("SOA Detail")
									 ->setSubject("SOA Detail");
		
		// Add some data
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID')
		                              ->setCellValue('B1', 'Name')
		                              ->setCellValue('C1', 'Programme')
		                              ->setCellValue('D1', 'Programme Mode')
		                              ->setCellValue('E1', 'Study Mode')
		                              ->setCellValue('F1', 'Programme Type')
		                              ->setCellValue('G1', 'Intake')
		                              ->setCellValue('H1', 'Date')
		                              ->setCellValue('I1', 'Transaction Type')
		                              ->setCellValue('J1', 'Reference No')
		                              ->setCellValue('K1', 'Description')
		                              ->setCellValue('L1', 'Debit')
		                              ->setCellValue('N1', 'Credit')
		                              ->setCellValue('O1', 'Balance');
		
		$objPHPExcel->getActiveSheet()->mergeCells('L1:M1');
		
		$objPHPExcel->getActiveSheet()->setCellValue('L2', 'USD')
		                              ->setCellValue('M2', 'MYR')
		                              ->setCellValue('N2', 'MYR')
		                              ->setCellValue('O2', 'MYR');
		                              
		$objPHPExcel->getActiveSheet()->getStyle('A1:O2')->getFont()->setBold(true);
		                              
		//lets manipulate the data
		if($arrayStack){
			$noExcel = 3;
			foreach ($arrayStack as $entryMain):
			
				$profile = $entryMain['profile'];
				$balance = 0;
				$totalDebit = 0;
				
				if($entryMain['soa']){
					
					foreach ($entryMain['soa'] as $entry):
					
						$arrayDebit = array();
						if($entry['cur_id'] == 2){
							
							if($entry['exchange_rate']){
								$dataCur = $this->getDataCurrency($entry['exchange_rate']);
								$amountDebit = number_format($entry['debit'] * $dataCur['cr_exchange_rate'],2);
								$arrayDebit['USD'] = $entry['debit'];
								$arrayDebit['MYR'] = $amountDebit;
								$totalDebit = $amountDebit;
							}
							
						}
						
						if($entry['cur_id'] == 1){
							
							$dataCur = $this->getRateByDate(2,$entry['record_date']);
							$amountDebit = number_format($entry['debit'] / $dataCur['cr_exchange_rate'],2);
							$arrayDebit['MYR'] = $entry['debit'];
							$arrayDebit['USD'] = $amountDebit;
							$totalDebit = $entry['debit'];
						}
					
						if( $entry['txn_type']=="Invoice" ){
							
							$balance = $balance + $totalDebit;
							
						}elseif( $entry['txn_type']=="Proforma" ){
							
							$balance = $balance + $totalDebit;
							
						}else
						if( $entry['txn_type']=="Payment" ){
							
							if($entry['cur_id'] == 2){
								$amountCredit = round($entry['credit'] * $dataCur['cr_exchange_rate'],2);
							}
							
							if($entry['cur_id'] == 1){
								$amountCredit = $entry['credit'];
							}
							
							$balance = $balance - $amountCredit;
						}else
						if( $entry['txn_type']=="Credit Note" ){
							$balance = $balance - $entry['credit'];
						}else
						if( $entry['txn_type']=="Debit Note" ){
							$balance = $balance + $entry['debit'];
						}else
						if( $entry['txn_type']=="Refund" ){
							$balance = $balance + $entry['debit'];
						}
				
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$noExcel, $profile['at_pes_id']);
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$noExcel, $profile['name']);
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$noExcel, $profile['ProgramCode']);
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$noExcel, $profile['ProgramMode']);
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$noExcel, $profile['StudyMode']);
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$noExcel, $profile['ProgramType']);
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$noExcel, $profile['IntakeDesc']);
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$noExcel, date('d-m-Y', strtotime($entry['record_date']) ));
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$noExcel, $entry['txn_type']);
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$noExcel, $entry['invoice_no']);

						$invoiceDesc = ($entry['subject']) ? $entry['subject'].' - ' : '' ;
						$invoiceDesc .= $entry['description'];
						if($entry['migrate_code']!=''){
							$invoiceDesc = $entry['migrate_desc'];
						}
						
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$noExcel, $invoiceDesc);
						
						//amount
						if($entry['txn_type'] == 'Invoice' || $entry['txn_type'] == 'Proforma'){
							if($curAvailable){
								foreach($curAvailable as $key=>$cur){
									
									foreach($arrayDebit as $keyD=>$dbt){
										
										if($key == $entry['cur_id']){
											if($entry['cur_id'] == 2){
												$char = 'L';
											}else{
												$char = 'M';
											}
											
											$objPHPExcel->getActiveSheet()->getStyle($char.$noExcel)->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_DARKGREEN ) );
										}
										
										
										
										if($cur == $keyD){
											if($key == 2){
												$objPHPExcel->getActiveSheet()->setCellValue('L'.$noExcel, $dbt);
												$objPHPExcel->getActiveSheet()->getStyle('L'.$noExcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
											}else{
												$objPHPExcel->getActiveSheet()->setCellValue('M'.$noExcel, $dbt);
												$objPHPExcel->getActiveSheet()->getStyle('M'.$noExcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
											}
										}
										
									}
									
								}
							}
						}else{
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$noExcel, '0.00');
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$noExcel, '0.00');
							$objPHPExcel->getActiveSheet()->getStyle('L'.$noExcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$objPHPExcel->getActiveSheet()->getStyle('M'.$noExcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						}
						
						$credit = !isset($entryMain['amountCur'][$entry['invoice_no']]['MYR']['credit']) ? '0.00' : number_format($entryMain['amountCur'][$entry['invoice_no']]['MYR']['credit'],2);
						$objPHPExcel->getActiveSheet()->setCellValue('N'.$noExcel, $credit);
						$objPHPExcel->getActiveSheet()->getStyle('N'.$noExcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						
						$balance = !isset($entryMain['amountCur'][$entry['invoice_no']]['MYR']['balance']) ? '0.00' : $entryMain['amountCur'][$entry['invoice_no']]['MYR']['balance'];
						$objPHPExcel->getActiveSheet()->setCellValue('O'.$noExcel, $balance);
						$objPHPExcel->getActiveSheet()->getStyle('O'.$noExcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						
						$noExcel++;	
					endforeach;
				}
			
			endforeach;
		}                              
		
		//sheet name
		$objPHPExcel->getActiveSheet()->setTitle('SOA Detail');
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		//filename
		$fileName = date('Ymd_His').'_statementofaccount_detail.xlsx';
		
		//save into folder
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($dirName.'/'.$fileName);
		
		//update filepath
		$pathName = "report/finance/".date('Ym')."/".$fileName;
		
		$dataUpdate = array(
			'filepath' => $pathName, 
			'path' => $dirName.'/'.$fileName, 
			'filename' => $fileName, 
		);
		
		$db->update('finance_service', $dataUpdate, 'id_cron ='.  $id );
		
		
	}
	
	public function createUploadFile()
	{
		
		$folderDate = date('Ym');
        $dir = $this->config->constants->APP_DOC_PATH."/report/finance/".$folderDate;
            
        if ( !is_dir($dir) ){
            if ( mkdir_p($dir) === false )
                {
                   echo 'Cannot create attachment folder ('.$dir.')';
                }
        }
        
        return $dir;
			
	}
	
	public function getDataCurrency($id){
	
		$service = new cronService();
		$db = $service->conn;
		$selectData = $db->select()
			->from(array('cr'=>'tbl_currency_rate'))
			->where('cr.cr_id = ?', $id);
	
		$row = $db->fetchRow($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;
		}

	}
	
	public function getRateByDate($currency_id,$date)
	{
	
		$service = new cronService();
		$db = $service->conn;
		$dateTrans = date('Y-m-d', strtotime($date) );
	
		$selectData = $db->select()
			->from(array('cr'=>'tbl_currency_rate'))
			->where('cr.cr_cur_id = ?', $currency_id)
			->where("cr.cr_effective_date <= '$dateTrans'")
			->order('cr.cr_effective_date desc')
			->limit(1);
	
		$row = $db->fetchRow($selectData);
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function generateRental()
	{
		
		$service = new cronService();
		$db = $service->conn;
		
		//get search parameter
		$select = $service->conn->select()
			->from(array('a'=>'tbl_hostel_registration'))
			
			->join( array( 'HostelRoom' => 'tbl_hostel_room' ), "a.IdHostelRoom = HostelRoom.IdHostelRoom", array( 'HostelRoom.RoomCode as Room', 'HostelRoom.RoomName as RoomName','RoomType' ) )
			->join( array( 'RoomType' => 'tbl_hostel_room_type' ), "HostelRoom.RoomType = RoomType.IdHostelRoomType", array() )
			->join(array('b'=>'tbl_room_charges'),'b.IdHostelRoom = HostelRoom.RoomType')
			
			->join( array( 'c'=>'tbl_studentregistration' ), 'c.IdStudentRegistration = a.IdStudentRegistration', array( 'branch_id'=>'IdBranch','IdIntake' ) )
			->join( array( 'e'=>'tbl_intake' ), 'e.IdIntake = c.IdIntake', array( 'sem_year','sem_seq' ) )
			->join( array( 'd'=>'tbl_program' ), 'd.IdProgram = c.IdProgram', array( 'ap_prog_scheme'=>'IdScheme' ) )
			->where('a.StudentStatus IN (1,3)') //checkin & changeroom
//			->where('c.IdStudentRegistration = 3812')
			->where('a.CheckInDate IS NOT NULL')//check in status only
			->where('b.CustomerType = 263')//student only
//			->where("a.CheckInDate <= '2015-09-05'") //to cater previous month - hardcode
			->group('a.IdStudentRegistration')
//			->limit(2)
			;


		
		$results = $service->conn->fetchAll($select);


		$newArray = array();
//		$newArray = $results;
		$m = 1;
		foreach($results as $i=>$data){

			//get current semester
			$semesterData = $this->getStudentCurrentSemester($data);
			$semesterID = $semesterData['IdSemesterMaster'];


			$newArray[$i]['semesterId'] = $semesterID;

			$IdStudentRegistration = $data['IdStudentRegistration'];
			$newArray[$i]['IdStudentRegistration'] = $IdStudentRegistration;
			
			$roomType = $data['RoomType'];
			$IdIntake = $data['IdIntake'];
			$CheckInDate = date('Y-m-d',strtotime($data['CheckInDate']));


			/*
			 * select a.IdIntake, b.IntakeDesc,b.sem_year,b.sem_seq, b.*,a.* from tbl_room_charges a
				join tbl_intake b on b.IdIntake = a.IdIntake
				where a.IdHostelRoom = 5 and
				b.sem_year <= 2015

				and
				MONTH(STR_TO_DATE(b.sem_seq,'%b')) <= 9

				order by b.sem_year desc,
				CASE b.sem_seq
					WHEN 'SEP'
						THEN '0'
					WHEN 'JUN'
						THEN  '1'
					WHEN 'JAN'
						THEN '2'
				END

			*/


			$selectIntake = $service->conn->select()
				->from(array('a'=>'tbl_room_charges'),array('StartDate'=>'DATE(StartDate)','Rate','Currency'))
				->join( array( 'b' => 'tbl_intake' ), "b.IdIntake = a.IdIntake", array( '*' ) )
				->where('a.IdHostelRoom = ?',$roomType)
//				->where('a.IdIntake = ?',$IdIntake)
				->where("b.sem_year <= ".$data['sem_year']." or MONTH(STR_TO_DATE(b.sem_seq,'%b')) <= ".date('m',strtotime($data['sem_seq'])))
				//->where("MONTH(STR_TO_DATE(b.sem_seq,'%b')) <= ?",date('m',strtotime($data['sem_seq'])))
				->where("'$CheckInDate' >= DATE(a.StartDate)")
				->where("CURDATE() >= DATE(a.StartDate)")
				->order("b.sem_year desc,
					CASE `b.sem_seq`
						WHEN 'SEP'
							THEN '0'
						WHEN 'JUN'
							THEN  '1'
						WHEN 'JAN'
							THEN '2'
						END ")
			->limit(1);

			$resultsCharges = $service->conn->fetchRow($selectIntake);

			$currentDate = date('Y-m-d');
//			$currentDate = '2015-09-05';

			$newArray[$i]['genInvoice'] = 0;

			if($resultsCharges){
				$newArray[$i]['checkExist'] = 1;


				$charges = $resultsCharges;
//				foreach($resultsCharges as $charges){
					$startDate = $charges['StartDate'];
					
					$amount = $charges['Rate'];
					$curID = $charges['Currency'];
					$feeID = 64; //student rental


					
					if($currentDate >= $startDate && $CheckInDate >= $startDate ){

						$checkExist = $this->checkRentalInvoiceExist($IdStudentRegistration);


						if($checkExist) {
							$newArray[$i]['genInvoice'] = 2;
						}else{
							$this->generateInvoice($IdStudentRegistration, $feeID, $semesterID, $amount, $curID);
							$newArray[$i]['genInvoice'] = 1;

						}
					}

				$m++;
//				}
			}else{
				$newArray[$i]['checkExist'] = 0;
			}

			$dataUn = array(
				'IdStudentRegistration'=>$IdStudentRegistration,
				'IdIntake' => $data['IdIntake'],
				'date_create'=>date('Y-m-d H:i:s'),
				'status'=>$newArray[$i]['checkExist'],
				'invoice'=>$newArray[$i]['genInvoice']
			);
			$this->conn->insert('cron_hostel', $dataUn);
			
		}

//		print_r($newArray);
		/*echo "<br>";
		echo count($m);
		exit;*/
		
	}
	
	public function generateInvoice($studentID,$feeID,$semesterID,$amount=0,$curID=0){
		
		$creator = 665; //system generated
		
		$service = new cronService();
		$db = $service->conn;
	
		//checking CP
		$studentInfo = $this->getStudentInfo($studentID);
		
		$checkingInvoice = $studentInfo['invoice'];
		
		if($checkingInvoice == 1){
			
			if($amount){
				$amountAdd = $amount;
				$curID = $curID;
			}else{
				//get fee item
				$fee_item_list = $this->getStudentInvoiceInfo($studentID,$feeID);
				$fee_item_list = $fee_item_list[0];
				$curID = $fee_item_list['fsi_cur_id'];
				$amountAdd =  $fee_item_list['amount'];
			}
			
			$currency = $this->getCurrentExchangeRate($curID);
			
			//get fee info
			$feeInfo = $this->getFeeData($feeID);
			$feeDesc = $feeInfo['fi_name'].' - '.date('F y');
//			$feeDesc = $feeInfo['fi_name'].' - September 15';
			$db->beginTransaction();
			try{
					
				//insert into invoice_main
				
				$bill_no =  $this->getBillSeq(2, date('Y'));
				$data_invoice = array(
					'bill_number' => $bill_no,
					'appl_id' => isset($studentInfo['appl_id'])?$studentInfo['appl_id']:0,
					'trans_id' => isset($studentInfo['transaction_id'])?$studentInfo['transaction_id']:0,
					'IdStudentRegistration' => $studentID,
					'academic_year' => '',
					'semester' => $semesterID,
					'bill_amount' => $amountAdd,
					'bill_paid' => 0,
					'bill_balance' => $amountAdd,
					'bill_description' =>$feeDesc,
					'program_id' => $studentInfo['IdProgram'],
					'fs_id' => $studentInfo['fs_id'],
					'currency_id' => $curID,
					'exchange_rate' => $currency['cr_exchange_rate'],
					'invoice_date'=>date('Y-m-d'),
//					'invoice_date'=>'2015-09-05',
					'date_create'=>date('Y-m-d H:i:s'),
					'creator'=>$creator//by admin
				);
			
				$this->conn->insert('invoice_main', $data_invoice);
				$invoice_id = $this->conn->lastInsertId();
				
				//insert invoice detail
					
				$invoiceDetailData = array(
                    'invoice_main_id' => $invoice_id,
                    'fi_id' =>  $feeID,
                    'fee_item_description' =>  $feeInfo['fi_name'],
                    'cur_id' =>  $curID,
                    'amount' => $amountAdd,
					'balance' => $amountAdd,
					'exchange_rate' => $currency['cr_exchange_rate']
				);
	
				$invoice_detail_id = $this->conn->insert('invoice_detail', $invoiceDetailData);
				
				$db->commit();
				
			}catch(Exception $e){
				
				$db->rollBack();
				//save error message
			   	$msg['se_IdStudentRegistration'] = $studentID;
			   	$msg['se_title'] = 'Error generate rental';
			   	$msg['se_message'] = $e->getMessage();
			   	$msg['se_createdby'] = $creator;
			   	$msg['se_createddt'] = date("Y-m-d H:i:s");
		   		$this->conn->insert('system_error', $msg);
        		throw $e;
			}
		}
		
	}
	
	public function getStudentCurrentSemester($data)
    {
       	$service = new cronService();
		$db = $service->conn;
        
        $sql = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme']);
		
		if ( isset($data['branch_id']) )
		{
			$sql->where('sm.Branch = ?',(int)$data['branch_id']);   
		}
		
        $result = $db->fetchRow($sql); 
        
        if(!$result){
        	 $sql = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme']) ;
        	$result = $db->fetchRow($sql); 
        }
        
        return $result;
    	
    }
    
	public function getStudentInfo($id=0){
			
		$service = new cronService();
		$db = $service->conn;
		
		$select = $db->select()
				->from(array('sr'=>'tbl_studentregistration'))
				->join(array('ap'=>'student_profile'),'sr.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
				->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ArabicName','ProgramName','ProgramCode'))
				->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDefaultLanguage','IntakeId'))
				->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('StudentStatus'=>'DefinitionDesc'))
				->joinLeft(array('pm'=>'tbl_programmajoring'),'pm.IDProgramMajoring=sr.IDProgramMajoring',array('majoring'=>'BahasaDescription'))
				->joinLeft(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=sr.IdBranch',array('BranchName','invoice'))
				->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('CollegeName'=>'ArabicName','c.IdCollege'))
				->joinLeft(array('sm'=>'tbl_staffmaster'),'sm.IdStaff=sr.AcademicAdvisor',array('AcademicAdvisor'=>'FullName',"FrontSalutation","BackSalutation"))
				->where('sr.IdStudentRegistration = ?',$id);
//				->where('sr.profileStatus = 92'); //Active
				
		$row = $db->fetchRow($select);						
		
		return $row;
	}
	
	public function getStudentInvoiceInfo($studentID,$feeID){
		
		/*
		 * get invoice per fee item
		 * 
		 */
	
		//checking CP
		$studentInfo = $this->getStudentInfo($studentID);
		
		$checkingInvoice = $studentInfo['invoice'];
		
		if($checkingInvoice == 1){
		
			//get fee item
			$fee_item_to_charge = array();
			
			//fee structure detail
			$fsProgramItem = $this->getStructureDataFee($studentInfo['fs_id'],$feeID);
			
		    if ( !empty($fsProgramItem) ) 
			{
				foreach ($fsProgramItem as $keyfs=>$fsitem)
				{
					$fee_item = array();
			
					//fix amount
					if($fsitem['fi_amount_calculation_type'] == 300 ){
						$fsitem['amount'] = $fsitem['fsi_amount'];
					}
					
					//amount
					if ( $fsitem['fsi_amount'] > 0 )
					{
						$fsitem['amount'] = $fsitem['fsi_amount'];
						
					}else{
					
						//Fix Amount (Based on Country Origin)
						if ( $fsitem['fi_amount_calculation_type'] == 618 )
							{
								//get fee_item_category
								$feeCountryData = $this->getItemData($fsitem['fspi_fee_id'],$idCountry,$cur);
								
								$fsitem['amount'] = $feeCountryData['fic_amount'];
							}
					}
					
		
					$fee_item_to_charge[] = $fsitem;
				}
			}
//			echo "<pre>";
//			print_r($fee_item_to_charge_new);
					
			return $fee_item_to_charge;
		}else{
			return null;
		}
	}
	
	public function getCurrentExchangeRate($currency_id){
	
		$service = new cronService();
		$db = $service->conn;
	
		$selectData = $db->select()
			->from(array('cr'=>'tbl_currency_rate'))
			->where('cr.cr_cur_id = ?', $currency_id)
			->where('cr.cr_effective_date <= now()')
			->order('cr.cr_effective_date DESC');
	
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getStructureDataFee($fee_structure_id,$feeID=0){
		
		$service = new cronService();
		$db = $service->conn;
		
		$selectData = $db->select()
					->from(array('fsi'=>'fee_structure_item'),array('fsi_amount','fsi_cur_id','fsi_registration_item_id'))
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = fsi.fsi_item_id',array('*'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = fsi.fsi_cur_id')
					->where("fsi.fsi_structure_id = ?",$fee_structure_id)
					->where("fsi.fsi_item_id = ?",$feeID);
		
		$selectDataProgram = $db->select()
					->from(array('fsi'=>'fee_structure_program_item'),array('fsi_amount'=>'fspi_amount','fsi_cur_id'=>'fspi_currency_id','fsi_registration_item_id'=>'fspi_currency_id'))
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = fsi.fspi_fee_id',array('*'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = fsi.fspi_currency_id')
					->where("fsi.fspi_fs_id = ?",$fee_structure_id)
					->where("fsi.fspi_fee_id = ?",$feeID)
					->group('fsi.fspi_fee_id');
		
		$select = $db->select()
		    ->union(array($selectData, $selectDataProgram),  Zend_Db_Select::SQL_UNION_ALL);
		    		   
		$row = $db->fetchAll($select);

		if($row){
			return $row;
		}else{
			return null;
		}	
	}
	
	public function getItemData($fee_id, $idCountry, $cur){
		
		$service = new cronService();
		$db = $service->conn;
		
		$selectData = $db->select()
					->from(array('fsp'=>'fee_item_country'))
					->where("fsp.fic_fi_id= ?", $fee_id)
					->where("fsp.fic_idCountry = ?", $idCountry)
					->where('fsp.fic_cur_id = ?',$cur);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getFeeData($id=0){
		
		$service = new cronService();
		$db = $service->conn;
		
		$selectData = $db->select()
					->from(array('fi'=>'fee_item'))
					->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
					->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
					->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'));
		
		if($id!=0){
			$selectData->where("fi.fi_id = ?",$id);
			
			$row = $db->fetchRow($selectData);
			
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$service = new cronService();
		$db = $service->conn;
		
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
	
	public function generateListOutstanding()
		{
			
			$service = new cronService();
			$db = $service->conn;
			
			//get search parameter
			$select = $service->conn->select()
				->from(array('a'=>'tbl_studentregistration'))
				->where('a.profileStatus = ?', 92);
			
			$results = $service->conn->fetchAll($select);
			
			$dataArray = array();
			$m = 0;
			
			//delete tbl
			$this->conn->delete('finance_outstanding_temp');
					
			foreach($results as $data){
				
				$transID = $data['IdStudentRegistration'];
				//get balance
				$balance = $this->getOutstandingBalance($transID,2);
				
				if($balance['status'] == 2){ //outstanding
					$dataArray[$m]['IdStudentRegistration'] = $transID;
					$dataArray[$m]['balance'] = $balance['amount'];
					
					
					
					//checking at temp table
					
					$selectOs = $service->conn->select()
						->from(array('a'=>'finance_outstanding_temp'))
						->where('a.IdStudentRegistration = ?',$transID);
					
					$resultsOsTemp = $service->conn->fetchRow($selectOs);
					
					if($resultsOsTemp){//update
						$dataUpdate['created_date'] = new Zend_Db_Expr('NOW()');
						$this->conn->update('finance_outstanding_temp', $dataUpdate, array('id = ?' =>  $id) );
					}else{//remove
						$dataUpdate['created_date'] = new Zend_Db_Expr('NOW()');
						$dataUpdate['IdStudentRegistration'] = $transID;
						$dataUpdate['os_balance'] = $balance['amount'];
						$this->conn->insert('finance_outstanding_temp',$dataUpdate);
					}
			
					$m++;
				}
			}

			return 'Total Updated: '.$m;

//			echo "<pre>";
//			print_r($dataArray);
//			exit;
			
		}
		
		
	public function getOutstandingBalance($transID,$type,$semesterID=0){
    
    	 /*
         1. checking funding type = self funding only
         2. checking outstanding proforma invoice where A n invoice_id = null
         3. checking advance payment
         *  invoice & proforma & advance payment amount in MYR
         *  return os balance
         */
    	
    	$status = null;
    	$service = new cronService();
		$db = $service->conn;
    	
    	$selectScholar = $db->select()
							->from(array('schtg'=>'tbl_scholarship_studenttag'),array(
								'IdStudentRegistration'=>'schtg.sa_cust_id',
								'type'=>new Zend_Db_Expr ('"Scholarship"'),
								'fundingMethod'=>'sca.sch_name',
								'start_date'=>'DATE(schtg.sa_start_date)',
								'end_date'=>"IFNULL(DATE(schtg.sa_end_date),'0000-00-00')",
								)
							)
				            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array())
							->where("schtg.sa_cust_id = ?",$transID);
							
		$selectSponsor = $db->select()
							->from(array('spt'=>'tbl_sponsor_tag'),array(
								'IdStudentRegistration'=>"spt.StudentId",
								'type'=>new Zend_Db_Expr ('"Sponsorship"'),
								'fundingMethod'=>"CONCAT_WS(' ',sptt.fName, sptt.lName)",
								'start_date'=>'DATE(spt.StartDate)',
								'end_date'=>"IFNULL(DATE(spt.EndDate),'0000-00-00')",
								)
							)
				            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array())
							->where("spt.StudentId = ?",$transID);

		 $selectFunding = $db->select()
					    ->union(array($selectScholar, $selectSponsor),  Zend_Db_Select::SQL_UNION_ALL);

		$resultsFunding = $db->fetchAll($selectFunding);	
		
//		echo "<pre>";
//		print_r($resultsFunding);
		$date = new DateTime('+1 day');
		$currentDate = $date->format('Y-m-d');
		
		foreach($resultsFunding as $key=>$fund){
			$disStartDate = $fund['start_date'];
    		$disEndDate =  $fund['end_date'];
    		if($disEndDate != '0000-00-00'){
    			$disEndDate = $disEndDate;
    		}else{
    			$disEndDate = $currentDate;
    		}
    		
			if($currentDate >= $disStartDate){
    			if($currentDate <= $disEndDate){
    				
    			}else{
    				unset($resultsFunding[$key]);
    			}
    			
    		}else{
    			unset($resultsFunding[$key]);
    		}
    		
	    				
		}
//echo "<pre>";
//		print_r($resultsFunding);
//		echo count($resultsFunding);
//		exit;
		
		//get os balance from setup
//		$finOsBal = $this->getFinanceConfiguration();
		
		$finOsBal = 0;
		$status = 3; // no record/no invoice
		$newAmount = 0;

		//syafa suruh buat
		//if(count($resultsFunding) == 0){
    	
	    	$select_invoice = $db->select()
								->from(array('im'=>'invoice_main'),array(
									'id'=>'im.id',
									'record_date'=>'im.date_create',
									'amount' =>'bill_balance',
									'invoice_no' => 'bill_number',
									'currency_id' => 'currency_id',
								)
						)
						->where("im.status = 'A'");
						//->where('bill_balance > 0');
	    	if($type == 1){ //applicant
	    		$select_invoice->where('im.trans_id  = ?', $transID);
	    	}elseif($type == 2){ //student
	    		$select_invoice->where('im.IdStudentRegistration  = ?', $transID);
	    	}
	    	
	    	if($semesterID!=0){
	    		$select_invoice->where('im.semester  = ?', $semesterID);
	    	}

	    	/*$select_proforma = $db->select()
								->from(array('im'=>'proforma_invoice_main'),array(
									'id'=>'im.id',
									'record_date'=>'im.date_create',
									'amount' =>'bill_amount',
									'invoice_no' => 'bill_number',
									'currency_id' => 'currency_id',
								)
						)
//						->where('im.trans_id = ?', $transID)
						->where("im.status = 'A'")
						->where("im.invoice_id = 0");
	    	if($type == 1){ //applicant
	    		$select_proforma->where('im.trans_id  = ?', $transID);
	    	}elseif($type == 2){ //student
	    		$select_proforma->where('im.IdStudentRegistration  = ?', $transID);
	    	}				
							
	        $select = $db->select()
						    ->union(array($select_invoice, $select_proforma),  Zend_Db_Select::SQL_UNION_ALL)
						    ->order("record_date desc");

				$results = $db->fetchAll($select);			
				if(!$results){
					$results = null;
				}*/
	
	         $results = $db->fetchAll($select_invoice);
	         
	         
	         if($results){
		         $amount = 0;
		         foreach($results as $data){
					 $pamodel = new Studentfinance_Model_DbTable_PaymentAmountCron();

					 $balance = $pamodel->getBalanceMain($data['id']);

					 $amt = str_replace(',', '', $balance['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance['bill_balance']);
					 //$amt = str_replace(',', '', $data['amount']) < 0.00 ? 0.00:str_replace(',', '', $data['amount']);
		         	if($data['currency_id'] != 1){
						$rate = $this->getRateByDate($data['currency_id'],$data['record_date']);//usd
		         		$amt = round($amt * $rate['cr_exchange_rate'], 2);
		         	}
		         	
		         	$amount += $amt;
		         }
		         
		         $newAmount = $amount;
		         if($newAmount <= $finOsBal){
		         		$status = 1; // no outstanding
		         }else{
		         	$status = 2; // outstanding
		         }
	         
	         }else{
	         	$status = 3; // no record/no invoice
	         }
		//}
		
         return array('status'=>$status,'amount'=>$newAmount);

    }

	public function checkRentalInvoiceExist($studentID){

		$service = new cronService();
		$db = $service->conn;

		$feeID = 64;
		$currentMonth = date('m');
		$currentYear = date('Y');
//		$currentMonth = '9';

		$selectData = $db->select()
			->from(array('a'=>'invoice_main'))
			->join(array('b'=>'invoice_detail'),'b.invoice_main_id = a.id',array())
			->where("b.fi_id = ?", $feeID)
			->where("a.IdStudentRegistration = ?", $studentID)
			->where("MONTH(a.invoice_date) = ?",$currentMonth)
			->where("YEAR(a.invoice_date) = ?",$currentYear)
			->where("a.status = 'A'");

		$row = $db->fetchRow($selectData);
		return $row;
	}

	public function updateStudentStatus(){
		include 'updatecurrentsemesterstatus.php';

		return 'Success';
	}
}


//Process
$service = new cronService();

$select = $service->conn->select()->from(array('a'=>'cron_service'))->where('a.executed = 0 AND inprogress = 0')->limit(5);
$results = $service->conn->fetchAll($select);

foreach ( $results as $row )
{
	if ( is_callable( array( $service, $row['action']) ) )
	{
		$exec = 1;
		$recurring = 0;

		if ( $row['recurring'] == 1)
		{
			$exec = 0;

			$checkdate = strtotime($row['recurring_day'].'-'.date('m-Y'));

			if ( $row['recurring_type'] == 'month' )
			{

				if ( date('d') == date('d', $checkdate) && strtotime(date('d-m-Y')) > strtotime($row['recurring_lastexecuted']) )
				{
					$exec = 1;
					$recurring = 1;
				}
			}
			else if ( $row['recurring_type'] == 'day' )
			{
				$howmany = howDays( strtotime($row['recurring_lastexecuted']), time() );

				if ( $howmany >= $row['recurring_day'] )
				{
					$exec = 1;
					$recurring = 1;

				}
			}
		}

		if ( $exec )
		{
			$service->time_start($row['id']);
			
			$func_results = call_user_func( array( $service, $row['action']) );

			//done
			$service->close($row['id'], $func_results, $recurring );
		}

		//updatereport soa
		if($row['action'] == 'reportSoaDetail'){
			$service->updateSoa($row['id']);
		}
	}
}

echo 'Done. '.time();
exit;

function howDays($from, $to) {
    $first_date = $from;
    $second_date = $to;
    $offset = $second_date-$first_date; 
    return floor($offset/60/60/24);
}
?>