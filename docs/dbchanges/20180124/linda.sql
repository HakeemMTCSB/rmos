-- tbl_award_level

CREATE TABLE `tbl_award_level` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `GradeId` bigint(20) NOT NULL,
  `GradeDesc` varchar(100) DEFAULT NULL,
  `GradeTutFeech` varchar(10) DEFAULT NULL,
  `GradeAccode` varchar(15) DEFAULT NULL,
  `GradeCmpy` varchar(10) DEFAULT NULL,
  `GradeAcodeLv0` varchar(10) DEFAULT NULL,
  `GradeAcodeLv1` varchar(10) DEFAULT NULL,
  `GradeAcodeLv2` varchar(10) DEFAULT NULL,
  `GradeAcodeLv3` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tbl_award_level`
ADD `GradeType` varchar(15) NULL AFTER `GradeDesc`;

---insert table maintenance 20180122
INSERT INTO `registry_types` (`code`, `name`, `name_malay`, `active`,`created_by`,`updated_by`)
values ('program-type', 'Program Type', 'Jenis Program', '1','1','1');

INSERT INTO `registry_values` (`type_id`, `code`, `name`, `name_malay`, `description`,`created_by`,`updated_by`)
VALUES ((select id from registry_types where code = 'program-type'),'postgraduate','PostGraduate','PostGraduate','PostGraduate','1','1');

INSERT INTO `registry_values` (`type_id`, `code`, `name`, `name_malay`, `description`,`created_by`,`updated_by`)
VALUES ((select id from registry_types where code = 'program-type'),'undergraduate','UnderGraduate','UnderGraduate','UnderGraduate','1','1');
