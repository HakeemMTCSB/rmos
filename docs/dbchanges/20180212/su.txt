ALTER TABLE `applicant_transaction`
ADD `at_create_role` varchar(20) NULL AFTER `at_create_by`;

ALTER TABLE `tbl_status_sts`
ADD `IdConfig` int(11) NOT NULL COMMENT 'tbl_application_configs';

ALTER TABLE `tbl_status_sts`
ADD INDEX `IdConfig` (`IdConfig`),
ADD INDEX `sts_name` (`sts_name`);

ALTER TABLE `tbl_status_sts`
CHANGE `sts_name` `sts_name` int NOT NULL COMMENT 'registry_values' AFTER `sts_Id`,
CHANGE `sts_programScheme` `sts_programScheme` int(11) NULL COMMENT 'fk tbl_program_scheme' AFTER `sts_modDate`,
CHANGE `sts_stdCtgy` `sts_stdCtgy` int(11) NULL COMMENT 'tbl_defination' AFTER `sts_programScheme`;

ALTER TABLE `applicant_status_history`
CHANGE `ash_oldStatus` `ash_oldStatus` int(11) NULL AFTER `ash_status`;

ALTER TABLE `applicant_program`
CHANGE `ap_at_trans_id` `ap_at_trans_id` int NOT NULL COMMENT 'fk application_transaction(at_trans_id)' AFTER `ap_id`,
ADD `IdScheme` int NOT NULL COMMENT 'tbl_scheme' AFTER `ap_at_trans_id`,
CHANGE `ap_prog_id` `ap_prog_id` int NOT NULL AFTER `IdScheme`;

ALTER TABLE `applicant_program`
ADD `ap_level_of_study` int(11) NOT NULL AFTER `IdScheme`,
CHANGE `program_mode` `program_mode` int(11) NULL AFTER `mode_study`;

ALTER TABLE `applicant_program`
CHANGE `program_type` `program_type` int(11) NULL AFTER `program_mode`;

ALTER TABLE `applicant_program`
ADD `IdConfig` int(11) NULL COMMENT 'tbl_application_configs' AFTER `ap_prog_scheme`;

ALTER TABLE `applicant_working_experience`
CHANGE `aw_years` `aw_years` int(11) NULL COMMENT 'Years of industry' AFTER `aw_industry_id`;

ALTER TABLE `applicant_profile`
ADD `created_role` varchar(50) NULL DEFAULT 'applicant' AFTER `create_date`,
ADD `created_by` int NULL AFTER `created_role`,
ADD `agent_id` int NULL COMMENT 'tbl_agent' AFTER `created_by`;