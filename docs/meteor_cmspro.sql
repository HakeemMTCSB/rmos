SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE advance_payment (
  advpy_id bigint(20) NOT NULL AUTO_INCREMENT,
  advpy_appl_id bigint(20) NOT NULL,
  advpy_acad_year_id bigint(20) DEFAULT NULL,
  advpy_sem_id bigint(20) DEFAULT NULL,
  advpy_prog_code varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  advpy_fomulir varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  advpy_invoice_no varchar(15) DEFAULT NULL,
  advpy_invoice_id int(11) DEFAULT NULL,
  advpy_payment_id bigint(20) DEFAULT NULL,
  advpy_refund_id bigint(20) DEFAULT NULL,
  advpy_description text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  advpy_amount decimal(20,2) NOT NULL,
  advpy_total_paid decimal(20,2) NOT NULL,
  advpy_total_balance decimal(20,2) NOT NULL,
  advpy_status tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'A: active; X: inactive',
  advpy_creator bigint(20) NOT NULL,
  advpy_create_date datetime NOT NULL,
  PRIMARY KEY (advpy_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE advance_payment_detail (
  advpydet_id bigint(20) NOT NULL AUTO_INCREMENT,
  advpydet_advpy_id bigint(20) NOT NULL COMMENT 'fk advance_payment',
  advpydet_bill_no varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  advpydet_refund_cheque_id bigint(20) DEFAULT NULL,
  advpydet_refund_id bigint(20) DEFAULT NULL,
  advpydet_total_paid decimal(20,2) NOT NULL,
  PRIMARY KEY (advpydet_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='amout used from adv payment table';

CREATE TABLE agent_form_number (
  afn_id int(11) NOT NULL AUTO_INCREMENT,
  afn_form_no int(8) NOT NULL,
  afn_taken_status tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: available; 1:taken',
  afn_type int(11) NOT NULL,
  afn_academic_year int(11) DEFAULT NULL,
  afn_intake int(11) NOT NULL DEFAULT '78',
  PRIMARY KEY (afn_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='tbl to hold manual form pes id';

CREATE TABLE applicant_assessment (
  aar_id int(11) NOT NULL AUTO_INCREMENT,
  aar_trans_id int(11) NOT NULL,
  aar_rating_dean tinyint(1) DEFAULT NULL,
  aar_dean_status int(11) DEFAULT NULL,
  aar_rating_rector tinyint(11) DEFAULT NULL,
  aar_rector_status int(11) DEFAULT NULL,
  aar_remarks text,
  aar_dean_selectionid int(11) NOT NULL,
  aar_rector_selectionid int(11) NOT NULL,
  aar_final_selectionid int(11) NOT NULL,
  aar_dean_rateby int(11) DEFAULT NULL,
  aar_dean_ratedt datetime DEFAULT NULL,
  aar_rector_rateby int(11) DEFAULT NULL,
  aar_rector_ratedt datetime DEFAULT NULL,
  aar_approvalby int(11) DEFAULT NULL,
  aar_approvaldt datetime NOT NULL,
  aar_academic_year int(11) DEFAULT NULL,
  aar_reg_start_date date DEFAULT NULL,
  aar_reg_end_date date DEFAULT NULL,
  aar_payment_start_date date DEFAULT NULL,
  aar_payment_end_date date DEFAULT NULL,
  PRIMARY KEY (aar_id),
  UNIQUE KEY aar_id (aar_id),
  KEY aar_trans_id (aar_trans_id),
  KEY aar_dean_selectionid (aar_dean_selectionid),
  KEY aar_rector_selectionid (aar_rector_selectionid),
  KEY aar_rector_selectionid_2 (aar_rector_selectionid)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_assessment_912013 (
  aar_id int(11) NOT NULL AUTO_INCREMENT,
  aar_trans_id int(11) NOT NULL,
  aar_rating_dean tinyint(1) DEFAULT NULL,
  aar_dean_status int(11) DEFAULT NULL,
  aar_rating_rector tinyint(11) DEFAULT NULL,
  aar_rector_status int(11) DEFAULT NULL,
  aar_remarks text,
  aar_dean_selectionid int(11) NOT NULL,
  aar_rector_selectionid int(11) NOT NULL,
  aar_final_selectionid int(11) NOT NULL,
  aar_dean_rateby int(11) DEFAULT NULL,
  aar_dean_ratedt datetime DEFAULT NULL,
  aar_rector_rateby int(11) DEFAULT NULL,
  aar_rector_ratedt datetime DEFAULT NULL,
  aar_approvalby int(11) DEFAULT NULL,
  aar_approvaldt datetime NOT NULL,
  aar_academic_year int(11) DEFAULT NULL,
  aar_reg_start_date date DEFAULT NULL,
  aar_reg_end_date date DEFAULT NULL,
  aar_payment_start_date date DEFAULT NULL,
  aar_payment_end_date date DEFAULT NULL,
  PRIMARY KEY (aar_id),
  UNIQUE KEY aar_id (aar_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_assessment_date (
  id int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  lock_status tinyint(4) NOT NULL COMMENT '0: not lock; 1: lock',
  lock_by int(11) NOT NULL COMMENT 'fk_tbl_user',
  lock_date datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE applicant_assessment_usm (
  aau_id int(11) NOT NULL AUTO_INCREMENT,
  aau_trans_id int(11) NOT NULL,
  aau_ap_id int(11) NOT NULL COMMENT 'fk applicant_program',
  aau_createddt datetime NOT NULL,
  aau_createdby int(11) NOT NULL,
  aau_rector_ranking int(11) DEFAULT NULL,
  aau_rector_status int(11) DEFAULT NULL,
  aau_rector_createby int(11) DEFAULT NULL,
  aau_rector_createdt datetime DEFAULT NULL,
  aau_rector_selectionid int(11) DEFAULT NULL COMMENT 'applicant_assessment_usm_detl',
  aau_reversal_status int(11) NOT NULL DEFAULT '0' COMMENT '1:yes',
  PRIMARY KEY (aau_id),
  KEY aau_trans_id (aau_trans_id),
  KEY aau_ap_id (aau_ap_id),
  KEY aau_rector_selectionid (aau_rector_selectionid),
  KEY aau_rector_status (aau_rector_status)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_assessment_usm_detl (
  aaud_id int(11) NOT NULL AUTO_INCREMENT,
  aaud_type int(11) NOT NULL COMMENT '1: Dean 2:Rektor',
  aaud_nomor varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  aaud_decree_date date DEFAULT NULL,
  aaud_academic_year int(11) DEFAULT NULL,
  aaud_selection_period varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  aaud_reg_start_date date DEFAULT NULL,
  aaud_reg_end_date date DEFAULT NULL,
  aaud_payment_start_date date DEFAULT NULL,
  aaud_payment_end_date date DEFAULT NULL,
  aaud_lock_status int(11) NOT NULL DEFAULT '0' COMMENT '1:locked  0:unlock',
  aaud_lock_by int(11) DEFAULT NULL,
  aaud_lock_date datetime DEFAULT NULL,
  aaud_createddt datetime DEFAULT NULL,
  aaud_createdby int(11) DEFAULT NULL,
  PRIMARY KEY (aaud_id),
  KEY aaud_nomor (aaud_nomor)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_assessment_usm_mei2013 (
  aau_id int(11) NOT NULL AUTO_INCREMENT,
  aau_trans_id int(11) NOT NULL,
  aau_ap_id int(11) NOT NULL COMMENT 'fk applicant_program',
  aau_createddt datetime NOT NULL,
  aau_createdby int(11) NOT NULL,
  aau_rector_ranking int(11) DEFAULT NULL,
  aau_rector_status int(11) DEFAULT NULL,
  aau_rector_createby int(11) DEFAULT NULL,
  aau_rector_createdt datetime DEFAULT NULL,
  aau_rector_selectionid int(11) DEFAULT NULL COMMENT 'applicant_assessment_usm_detl',
  aau_reversal_status int(11) NOT NULL DEFAULT '0' COMMENT '1:yes',
  PRIMARY KEY (aau_id),
  KEY aau_trans_id (aau_trans_id),
  KEY aau_ap_id (aau_ap_id),
  KEY aau_rector_selectionid (aau_rector_selectionid),
  KEY aau_rector_status (aau_rector_status)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_change_program (
  acp_id int(11) NOT NULL AUTO_INCREMENT,
  acp_appl_id int(11) NOT NULL,
  acp_trans_id_from int(11) NOT NULL,
  acp_trans_id_to int(11) NOT NULL,
  acp_createddt datetime NOT NULL,
  acp_createdby int(11) DEFAULT NULL,
  PRIMARY KEY (acp_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_documents (
  ad_id int(11) NOT NULL AUTO_INCREMENT,
  ad_appl_id int(11) NOT NULL COMMENT 'sebenarnye txn ID bukan appl_id',
  ad_type int(11) DEFAULT NULL,
  ad_filepath varchar(200) NOT NULL,
  ad_filename varchar(200) NOT NULL,
  ad_createddt date NOT NULL,
  ad_kode_sandi varchar(250) DEFAULT NULL,
  PRIMARY KEY (ad_id),
  KEY ad_type (ad_type)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_documents_230513 (
  ad_id int(11) NOT NULL AUTO_INCREMENT,
  ad_appl_id int(11) NOT NULL COMMENT 'sebenarnye txn ID bukan appl_id',
  ad_type int(11) DEFAULT NULL,
  ad_filepath varchar(200) NOT NULL,
  ad_filename varchar(200) NOT NULL,
  ad_createddt date NOT NULL,
  PRIMARY KEY (ad_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_documents_20092013 (
  ad_id int(11) NOT NULL AUTO_INCREMENT,
  ad_appl_id int(11) NOT NULL COMMENT 'sebenarnye txn ID bukan appl_id',
  ad_type int(11) DEFAULT NULL,
  ad_filepath varchar(200) NOT NULL,
  ad_filename varchar(200) NOT NULL,
  ad_createddt date NOT NULL,
  ad_kode_sandi varchar(250) DEFAULT NULL,
  PRIMARY KEY (ad_id),
  KEY ad_type (ad_type)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_education (
  ae_id int(11) NOT NULL AUTO_INCREMENT,
  ae_appl_id bigint(20) NOT NULL,
  ae_transaction_id int(11) NOT NULL,
  ae_institution int(11) NOT NULL COMMENT 'fk.school_master',
  ae_discipline_code varchar(3) NOT NULL COMMENT 'fk school_majoring(smj_code)',
  ae_year_from date NOT NULL,
  ae_year_end date NOT NULL,
  ae_year tinyint(4) DEFAULT NULL,
  ae_award varchar(20) DEFAULT NULL,
  PRIMARY KEY (ae_id),
  KEY ae_transaction_id (ae_transaction_id),
  KEY ae_appl_id (ae_appl_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_education_detl (
  aed_id int(11) NOT NULL AUTO_INCREMENT,
  aed_ae_id int(11) NOT NULL COMMENT 'fk applicant_education(ae_id)',
  aed_subject_id int(11) NOT NULL COMMENT 'fk chool_majoring_subject(sms_id)',
  aed_sem1 int(3) DEFAULT NULL,
  aed_sem2 int(3) DEFAULT NULL,
  aed_sem3 int(3) DEFAULT NULL,
  aed_sem4 int(3) DEFAULT NULL,
  aed_sem5 int(3) DEFAULT NULL,
  aed_sem6 int(3) DEFAULT NULL,
  aed_grade varchar(10) DEFAULT NULL,
  aed_average decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (aed_id),
  KEY applicant_education_detl_fk1 (aed_ae_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_family (
  af_id int(11) NOT NULL AUTO_INCREMENT,
  af_appl_id int(11) NOT NULL,
  af_name varchar(100) NOT NULL,
  af_relation_type int(11) NOT NULL COMMENT 'fk sis_setup_detl(ssd_id)',
  af_family_condition int(11) DEFAULT NULL COMMENT 'fk sis_setup_detl',
  af_address_rt varchar(3) DEFAULT NULL,
  af_address_rw varchar(3) DEFAULT NULL,
  af_address1 varchar(300) NOT NULL,
  af_address2 varchar(300) DEFAULT NULL,
  af_kelurahan varchar(200) DEFAULT NULL,
  af_kecamatan varchar(200) DEFAULT NULL,
  af_city int(11) NOT NULL,
  af_state bigint(20) unsigned NOT NULL,
  af_province int(11) DEFAULT NULL,
  af_postcode bigint(5) NOT NULL,
  af_phone varchar(15) NOT NULL,
  af_email varchar(50) NOT NULL,
  af_job varchar(100) NOT NULL,
  af_education_level int(11) DEFAULT NULL,
  PRIMARY KEY (af_id),
  KEY applicant_family_fk_1 (af_state),
  KEY applicant_family_fk_2 (af_city),
  KEY applicant_family_fk_3 (af_relation_type),
  KEY af_appl_id (af_appl_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_family_20130823 (
  af_id int(11) NOT NULL AUTO_INCREMENT,
  af_appl_id int(11) NOT NULL,
  af_name varchar(100) NOT NULL,
  af_relation_type int(11) NOT NULL COMMENT 'fk sis_setup_detl(ssd_id)',
  af_family_condition int(11) DEFAULT NULL COMMENT 'fk sis_setup_detl',
  af_address1 varchar(300) NOT NULL,
  af_address2 varchar(300) NOT NULL,
  af_city int(11) NOT NULL COMMENT 'fk tbl_city',
  af_postcode bigint(5) NOT NULL,
  af_state bigint(20) unsigned NOT NULL,
  af_phone varchar(15) NOT NULL,
  af_email varchar(50) NOT NULL,
  af_job varchar(100) NOT NULL,
  af_education_level int(11) DEFAULT NULL,
  PRIMARY KEY (af_id),
  KEY applicant_family_fk_1 (af_state),
  KEY applicant_family_fk_2 (af_city),
  KEY applicant_family_fk_3 (af_relation_type),
  KEY af_appl_id (af_appl_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_language (
  al_id tinyint(4) NOT NULL AUTO_INCREMENT,
  al_language varchar(20) NOT NULL,
  PRIMARY KEY (al_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_mark_raw (
  tmp_id bigint(20) NOT NULL AUTO_INCREMENT,
  tmp_name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  tmp_ptest_code varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  tmp_set_code varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  tmp_answerraw text COLLATE utf8_unicode_ci NOT NULL,
  tmp_sched_id int(11) NOT NULL,
  tmp_file varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (tmp_id),
  KEY tmp_sched_id (tmp_sched_id),
  KEY tmp_set_code (tmp_set_code),
  KEY tmp_file (tmp_file),
  KEY tmp_id (tmp_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE applicant_profile (
  appl_id bigint(20) NOT NULL AUTO_INCREMENT,
  appl_fname varchar(100) DEFAULT NULL,
  appl_mname varchar(100) DEFAULT NULL,
  appl_lname varchar(100) NOT NULL,
  appl_address_rw varchar(3) DEFAULT NULL,
  appl_address_rt varchar(3) DEFAULT NULL,
  appl_address1 varchar(200) DEFAULT NULL,
  appl_address2 varchar(200) DEFAULT NULL,
  appl_kelurahan varchar(200) DEFAULT NULL,
  appl_kecamatan varchar(200) DEFAULT NULL,
  appl_postcode varchar(5) NOT NULL,
  appl_city int(11) DEFAULT NULL,
  appl_state int(11) NOT NULL,
  appl_province int(11) DEFAULT NULL COMMENT 'Country',
  appl_email varchar(100) NOT NULL,
  appl_username varchar(100) DEFAULT NULL COMMENT 'later nanti nak pakai just create aje',
  appl_password varchar(100) NOT NULL,
  appl_dob varchar(100) NOT NULL COMMENT 'dd-mm-yyyy',
  appl_birth_place varchar(200) NOT NULL,
  appl_gender tinyint(1) NOT NULL COMMENT '1:  male 2: female',
  appl_prefer_lang int(11) NOT NULL DEFAULT '2' COMMENT 'fk applicant_languange(al_id)',
  appl_nationality int(11) NOT NULL DEFAULT '1',
  appl_admission_type int(11) DEFAULT NULL COMMENT 'jgn pakai ni lagi guna at_appl_type dkt transaction',
  appl_religion tinyint(4) DEFAULT NULL,
  appl_marital_status int(11) DEFAULT NULL,
  appl_no_of_child int(11) DEFAULT NULL,
  appl_jacket_size int(11) DEFAULT NULL,
  appl_parent_salary decimal(20,2) DEFAULT NULL,
  appl_phone_home int(13) DEFAULT NULL,
  appl_phone_hp varchar(13) DEFAULT NULL,
  appl_caddress_rt varchar(3) DEFAULT NULL,
  appl_caddress_rw varchar(3) DEFAULT NULL,
  appl_caddress1 varchar(200) DEFAULT NULL COMMENT 'correspondence add',
  appl_caddress2 varchar(200) DEFAULT NULL,
  appl_ckelurahan varchar(200) DEFAULT NULL,
  appl_ckecamatan varchar(200) DEFAULT NULL,
  appl_ccity int(11) DEFAULT NULL,
  appl_cstate int(11) DEFAULT NULL,
  appl_cprovince int(11) DEFAULT NULL,
  appl_cpostcode int(11) DEFAULT NULL,
  change_passwordby int(11) NOT NULL,
  change_passworddt datetime NOT NULL,
  appl_role int(11) DEFAULT NULL COMMENT '0:Applicant  1:Student',
  PRIMARY KEY (appl_id),
  UNIQUE KEY appl_email (appl_email),
  KEY applicant_profile_fk1 (appl_prefer_lang)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_profile_6dis (
  appl_id bigint(20) NOT NULL AUTO_INCREMENT,
  appl_fname varchar(100) NOT NULL,
  appl_mname varchar(100) DEFAULT NULL,
  appl_lname varchar(100) NOT NULL,
  appl_address1 varchar(200) DEFAULT NULL,
  appl_address2 varchar(200) DEFAULT NULL,
  appl_city int(11) DEFAULT NULL,
  appl_postcode varchar(5) NOT NULL,
  appl_state int(11) NOT NULL,
  appl_province int(11) DEFAULT NULL,
  appl_email varchar(100) NOT NULL,
  appl_password varchar(100) NOT NULL,
  appl_dob varchar(100) NOT NULL,
  appl_gender tinyint(1) NOT NULL COMMENT '1:  male 2: female',
  appl_prefer_lang int(11) NOT NULL COMMENT 'fk applicant_languange(al_id)',
  appl_nationality int(11) DEFAULT NULL,
  appl_admission_type int(11) DEFAULT NULL,
  appl_religion tinyint(4) DEFAULT NULL,
  appl_marital_status int(11) DEFAULT NULL,
  appl_no_of_child int(11) DEFAULT NULL,
  appl_jacket_size int(11) DEFAULT NULL,
  appl_phone_home int(13) DEFAULT NULL,
  appl_phone_hp varchar(13) DEFAULT NULL,
  appl_caddress1 varchar(200) DEFAULT NULL COMMENT 'correspondence add',
  appl_caddress2 varchar(200) DEFAULT NULL,
  appl_ccity int(11) DEFAULT NULL,
  appl_cstate int(11) DEFAULT NULL,
  appl_cpostcode int(11) DEFAULT NULL,
  appl_cprovince int(11) DEFAULT NULL,
  PRIMARY KEY (appl_id),
  UNIQUE KEY appl_email (appl_email),
  KEY applicant_profile_fk1 (appl_prefer_lang)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_proforma_invoice (
  id int(11) NOT NULL AUTO_INCREMENT,
  billing_no varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  payee_id varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  appl_id int(11) NOT NULL COMMENT 'fk_applicant_profile',
  txn_id int(11) NOT NULL COMMENT 'fk_applicant_transaction',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  address varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ref1 varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'faculty_code-shortname',
  ref2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'blank',
  ref3 varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'program_code-shortname',
  ref4 varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'intake year',
  ref5 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  amount_total decimal(20,2) NOT NULL,
  amount1 decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'SP',
  amount2 decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'BPP-POKOK',
  amount3 decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'BPP-SKS',
  amount4 decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'PRAKTIKUM',
  amount5 decimal(20,2) NOT NULL DEFAULT '0.00',
  amount6 decimal(20,2) NOT NULL DEFAULT '0.00',
  amount7 decimal(20,2) NOT NULL DEFAULT '0.00',
  amount8 decimal(20,2) NOT NULL DEFAULT '0.00',
  amount9 decimal(20,2) NOT NULL DEFAULT '0.00',
  amount10 decimal(20,2) NOT NULL DEFAULT '0.00',
  register_no varchar(9) COLLATE utf8_unicode_ci NOT NULL COMMENT 'random generated no.',
  due_date date DEFAULT NULL,
  offer_date date NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY billing_no (billing_no),
  KEY payee_id (payee_id),
  KEY txn_id (txn_id),
  KEY appl_id (appl_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE applicant_program (
  ap_id int(11) NOT NULL AUTO_INCREMENT,
  ap_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  ap_prog_code varchar(4) NOT NULL COMMENT 'fk tbl_program(ProgramCode)',
  ap_ptest_prog_id int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  ap_preference tinyint(2) NOT NULL,
  ap_usm_mark decimal(10,2) DEFAULT NULL,
  ap_usm_status tinyint(4) NOT NULL COMMENT '1:Offer/Accept 2:Reject',
  UNIQUE KEY app_id (ap_id),
  KEY ap_at_trans_id (ap_at_trans_id),
  KEY ap_prog_code (ap_prog_code),
  KEY ap_usm_status (ap_usm_status),
  KEY ap_ptest_prog_id (ap_ptest_prog_id),
  KEY ap_preference (ap_preference)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_program_20130204 (
  ap_id int(11) NOT NULL AUTO_INCREMENT,
  ap_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  ap_prog_code varchar(4) NOT NULL COMMENT 'fk tbl_program(ProgramCode)',
  ap_ptest_prog_id int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  ap_preference tinyint(2) NOT NULL,
  ap_usm_mark decimal(10,2) NOT NULL,
  ap_usm_status tinyint(4) NOT NULL COMMENT '1:Offer/Accept',
  UNIQUE KEY app_id (ap_id),
  KEY ap_at_trans_id (ap_at_trans_id),
  KEY ap_prog_code (ap_prog_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_program_20131124 (
  ap_id int(11) NOT NULL AUTO_INCREMENT,
  ap_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  ap_prog_code varchar(4) NOT NULL COMMENT 'fk tbl_program(ProgramCode)',
  ap_ptest_prog_id int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  ap_preference tinyint(2) NOT NULL,
  ap_usm_mark decimal(10,2) DEFAULT NULL,
  ap_usm_status tinyint(4) NOT NULL COMMENT '1:Offer/Accept 2:Reject',
  UNIQUE KEY app_id (ap_id),
  KEY ap_at_trans_id (ap_at_trans_id),
  KEY ap_prog_code (ap_prog_code),
  KEY ap_usm_status (ap_usm_status),
  KEY ap_ptest_prog_id (ap_ptest_prog_id),
  KEY ap_preference (ap_preference)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_ptest (
  apt_id int(11) NOT NULL AUTO_INCREMENT,
  apt_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  apt_appl_id int(11) NOT NULL,
  apt_no_pes varchar(10) NOT NULL COMMENT 'format no 2 yg pertama year, 1 tu semester, 1 period, 5 placement test running no1100001',
  apt_ptest_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head',
  apt_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apt_currency varchar(30) DEFAULT NULL,
  apt_fee_amt decimal(10,2) NOT NULL,
  apt_bill_no varchar(12) DEFAULT NULL,
  apt_pin_no varchar(12) DEFAULT NULL,
  apt_room_id int(11) DEFAULT NULL COMMENT 'fk appl_room(ar_id)',
  apt_sit_no varchar(15) DEFAULT NULL,
  apt_status varchar(10) NOT NULL COMMENT 'APPLY, SUCCESS',
  apt_usm_attendance tinyint(4) NOT NULL DEFAULT '0',
  apt_mark decimal(10,2) NOT NULL,
  UNIQUE KEY apt_id (apt_id),
  KEY applicant_ptest_fk_2 (apt_at_trans_id),
  KEY apt_at_trans_id (apt_at_trans_id),
  KEY apt_status (apt_status),
  KEY apt_aps_id (apt_aps_id),
  KEY apt_room_id (apt_room_id),
  KEY apt_ptest_code (apt_ptest_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_ptest_20130109_error_no_ptest (
  apt_id int(11) NOT NULL AUTO_INCREMENT,
  apt_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  apt_appl_id int(11) NOT NULL,
  apt_no_pes varchar(10) NOT NULL COMMENT 'format no 2 yg pertama year, 1 tu semester, 1 period, 5 placement test running no1100001',
  apt_ptest_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head',
  apt_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apt_currency varchar(30) DEFAULT NULL,
  apt_fee_amt decimal(10,2) NOT NULL,
  apt_bill_no varchar(12) DEFAULT NULL,
  apt_pin_no varchar(12) DEFAULT NULL,
  apt_room_id int(11) DEFAULT NULL COMMENT 'fk appl_room(ar_id)',
  apt_sit_no varchar(15) DEFAULT NULL,
  apt_status varchar(10) NOT NULL COMMENT 'APPLY, SUCCESS',
  UNIQUE KEY apt_id (apt_id),
  KEY applicant_ptest_fk_2 (apt_at_trans_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_ptest_20130121 (
  apt_id int(11) NOT NULL AUTO_INCREMENT,
  apt_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  apt_appl_id int(11) NOT NULL,
  apt_no_pes varchar(10) NOT NULL COMMENT 'format no 2 yg pertama year, 1 tu semester, 1 period, 5 placement test running no1100001',
  apt_ptest_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head',
  apt_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apt_currency varchar(30) DEFAULT NULL,
  apt_fee_amt decimal(10,2) NOT NULL,
  apt_bill_no varchar(12) DEFAULT NULL,
  apt_pin_no varchar(12) DEFAULT NULL,
  apt_room_id int(11) DEFAULT NULL COMMENT 'fk appl_room(ar_id)',
  apt_sit_no varchar(15) DEFAULT NULL,
  apt_status varchar(10) NOT NULL COMMENT 'APPLY, SUCCESS',
  UNIQUE KEY apt_id (apt_id),
  KEY applicant_ptest_fk_2 (apt_at_trans_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_ptest_20130121_2 (
  apt_id int(11) NOT NULL AUTO_INCREMENT,
  apt_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  apt_appl_id int(11) NOT NULL,
  apt_no_pes varchar(10) NOT NULL COMMENT 'format no 2 yg pertama year, 1 tu semester, 1 period, 5 placement test running no1100001',
  apt_ptest_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head',
  apt_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apt_currency varchar(30) DEFAULT NULL,
  apt_fee_amt decimal(10,2) NOT NULL,
  apt_bill_no varchar(12) DEFAULT NULL,
  apt_pin_no varchar(12) DEFAULT NULL,
  apt_room_id int(11) DEFAULT NULL COMMENT 'fk appl_room(ar_id)',
  apt_sit_no varchar(15) DEFAULT NULL,
  apt_status varchar(10) NOT NULL COMMENT 'APPLY, SUCCESS',
  UNIQUE KEY apt_id (apt_id),
  KEY applicant_ptest_fk_2 (apt_at_trans_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_ptest_ans (
  apa_id int(11) NOT NULL AUTO_INCREMENT,
  apa_trans_id int(11) NOT NULL,
  apa_ptest_code varchar(10) NOT NULL,
  apa_set_code varchar(10) NOT NULL,
  apa_date datetime NOT NULL,
  apa_user_by bigint(20) NOT NULL,
  apa_status tinyint(4) NOT NULL DEFAULT '1',
  pcode varchar(20) NOT NULL DEFAULT 'USM2014',
  PRIMARY KEY (apa_id),
  UNIQUE KEY apa_id (apa_id),
  KEY apa_trans_id (apa_trans_id),
  KEY apa_user_by (apa_user_by)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_ptest_ans_detl (
  apad_id int(11) NOT NULL AUTO_INCREMENT,
  apad_apa_id int(11) NOT NULL COMMENT 'fk applicant_ptest_ans(apa_id)',
  apad_ques_no int(5) NOT NULL,
  apad_appl_ans varchar(1) NOT NULL,
  apad_status_ans tinyint(4) NOT NULL COMMENT '1 betul 0 salah',
  PRIMARY KEY (apad_id),
  KEY apad_apa_id (apad_apa_id),
  KEY apad_status_ans (apad_status_ans)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_ptest_comp_mark (
  apcm_id bigint(20) NOT NULL AUTO_INCREMENT,
  apcm_at_trans_id int(11) NOT NULL COMMENT 'FK to applicant_transaction',
  apcm_apd_id int(11) NOT NULL COMMENT 'FK to appl_placement_detl',
  apcm_mark decimal(10,2) NOT NULL,
  apcm_status tinyint(4) NOT NULL DEFAULT '1',
  apcm_prog_code varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  pcode varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USM2014',
  PRIMARY KEY (apcm_id),
  KEY apcm_at_trans_id (apcm_at_trans_id),
  KEY apcm_ac_id (apcm_apd_id),
  KEY apcm_status (apcm_status),
  KEY apcm_prog_code (apcm_prog_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE applicant_ptest_log (
  apt_log_id int(11) NOT NULL AUTO_INCREMENT,
  apt_log_date datetime NOT NULL,
  apt_id int(11) NOT NULL,
  apt_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  apt_appl_id int(11) NOT NULL,
  apt_no_pes varchar(10) NOT NULL COMMENT 'format no 2 yg pertama year, 1 tu semester, 1 period, 5 placement test running no1100001',
  apt_ptest_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head',
  apt_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apt_currency varchar(30) DEFAULT NULL,
  apt_fee_amt decimal(10,2) NOT NULL,
  apt_bill_no varchar(12) DEFAULT NULL,
  apt_pin_no varchar(12) DEFAULT NULL,
  apt_room_id int(11) DEFAULT NULL COMMENT 'fk appl_room(ar_id)',
  apt_sit_no varchar(15) DEFAULT NULL,
  apt_status tinyint(4) NOT NULL,
  apt_usm_attendance tinyint(4) NOT NULL,
  apt_mark decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (apt_log_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_ptest_roomid32 (
  apt_id int(11) NOT NULL AUTO_INCREMENT,
  apt_at_trans_id bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  apt_appl_id int(11) NOT NULL,
  apt_no_pes varchar(10) NOT NULL COMMENT 'format no 2 yg pertama year, 1 tu semester, 1 period, 5 placement test running no1100001',
  apt_ptest_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head',
  apt_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apt_currency varchar(30) DEFAULT NULL,
  apt_fee_amt decimal(10,2) NOT NULL,
  apt_bill_no varchar(12) DEFAULT NULL,
  apt_pin_no varchar(12) DEFAULT NULL,
  apt_room_id int(11) DEFAULT NULL COMMENT 'fk appl_room(ar_id)',
  apt_sit_no varchar(15) DEFAULT NULL,
  apt_status varchar(10) NOT NULL COMMENT 'APPLY, SUCCESS',
  apt_usm_attendance tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE KEY apt_id (apt_id),
  KEY applicant_ptest_fk_2 (apt_at_trans_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE applicant_quit (
  aq_id int(11) NOT NULL AUTO_INCREMENT,
  aq_trans_id int(11) DEFAULT NULL,
  aq_reason int(11) DEFAULT NULL,
  aq_authorised_personnel varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  aq_relationship int(11) DEFAULT NULL,
  aq_address text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  aq_identity_type int(11) DEFAULT NULL,
  aq_identity_no varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  aq_createddt datetime DEFAULT NULL,
  aq_approvaldt date DEFAULT NULL COMMENT 'for status approved/reject',
  aq_approvalby int(11) DEFAULT NULL COMMENT 'for status approved/reject',
  aq_processdt datetime DEFAULT NULL COMMENT 'ini system date bila admin process appliation ',
  aq_processby int(11) DEFAULT NULL,
  aq_remarks text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  aq_cheque_id int(11) DEFAULT NULL,
  PRIMARY KEY (aq_id),
  KEY aq_trans_id (aq_trans_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_quit_revert (
  aqr_id int(11) NOT NULL AUTO_INCREMENT,
  aq_id int(11) NOT NULL,
  aq_trans_id int(11) DEFAULT NULL,
  aq_reason int(11) DEFAULT NULL,
  aq_authorised_personnel varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  aq_relationship int(11) DEFAULT NULL,
  aq_address text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  aq_identity_type int(11) DEFAULT NULL,
  aq_identity_no varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  aq_createddt datetime DEFAULT NULL,
  aq_approvaldt date DEFAULT NULL COMMENT 'for status approved/reject',
  aq_approvalby int(11) DEFAULT NULL COMMENT 'for status approved/reject',
  aq_processdt datetime DEFAULT NULL COMMENT 'ini system date bila admin process appliation ',
  aq_processby int(11) DEFAULT NULL,
  aq_remarks text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  aq_cheque_id int(11) DEFAULT NULL,
  aqr_remark text,
  aqr_by bigint(20) NOT NULL,
  aqr_date datetime NOT NULL,
  PRIMARY KEY (aqr_id),
  KEY aq_trans_id (aq_trans_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE applicant_reversal_track (
  art_id int(11) NOT NULL AUTO_INCREMENT,
  art_appl_type int(11) NOT NULL,
  art_trans_id int(11) NOT NULL,
  art_createdby int(11) NOT NULL,
  art_createddt datetime NOT NULL,
  PRIMARY KEY (art_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_selection_detl (
  asd_id int(11) NOT NULL AUTO_INCREMENT,
  asd_type int(11) NOT NULL COMMENT '1: Dean 2: Rector 3:Final Approval',
  asd_nomor varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  asd_decree_date date NOT NULL,
  asd_faculty_id int(11) NOT NULL,
  asd_selection_period varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  asd_intake_id int(11) NOT NULL,
  asd_academic_year int(11) NOT NULL COMMENT 'fk_tbl_academic_year (ay_id)',
  asd_period_id int(11) NOT NULL,
  asd_createddt datetime NOT NULL,
  asd_createdby int(11) NOT NULL,
  PRIMARY KEY (asd_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE applicant_temp_usm_selection (
  ats_id int(11) NOT NULL AUTO_INCREMENT,
  ats_transaction_id int(11) NOT NULL,
  ats_ap_id int(11) NOT NULL COMMENT 'fk applicant_program ',
  ats_program_code varchar(11) CHARACTER SET utf8 NOT NULL,
  ats_preference tinyint(11) NOT NULL,
  PRIMARY KEY (ats_id),
  KEY ats_transaction_id (ats_transaction_id),
  KEY ats_ap_id (ats_ap_id),
  KEY ats_program_code (ats_program_code)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_temp_usm_selection522013 (
  ats_id int(11) NOT NULL AUTO_INCREMENT,
  ats_transaction_id int(11) NOT NULL,
  ats_ap_id int(11) NOT NULL COMMENT 'fk applicant_program ',
  ats_program_code varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ats_preference tinyint(11) NOT NULL,
  PRIMARY KEY (ats_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE applicant_transaction (
  at_trans_id bigint(20) NOT NULL AUTO_INCREMENT,
  at_appl_id int(11) NOT NULL COMMENT 'fk  applicant_profile(ap_id)',
  at_pes_id varchar(15) DEFAULT NULL,
  at_appl_type int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  at_academic_year int(11) NOT NULL DEFAULT '3' COMMENT 'fk  academic_year(ay_id)',
  at_intake int(11) NOT NULL,
  at_period int(11) NOT NULL,
  at_status varchar(10) NOT NULL COMMENT 'APPLY, CLOSE, PROCESS, OFFER, REJECT',
  at_selection_status int(11) NOT NULL DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted 4: In Pool (USM)',
  at_create_by int(11) NOT NULL,
  at_create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  at_submit_date datetime DEFAULT NULL,
  agent_id int(11) DEFAULT NULL,
  entry_type int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  at_payment_status tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not paid 1:paid tobe removed x pakai dah; 2: biasiswa',
  at_quit_status int(4) NOT NULL DEFAULT '0' COMMENT '1: Apply Quit 2:Approved Quit 3:Reject 4:Incomplete ',
  at_move_id int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_change_program',
  at_document_verified int(11) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  at_document_verifiedby int(11) DEFAULT NULL,
  at_document_verifieddt datetime DEFAULT NULL,
  at_registration_status int(11) DEFAULT NULL COMMENT '1: Active',
  at_IdStudentRegistration int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  at_registration_date datetime DEFAULT NULL,
  PRIMARY KEY (at_trans_id),
  KEY at_appl_id (at_appl_id),
  KEY at_appl_type (at_appl_type),
  KEY at_period (at_period),
  KEY at_status (at_status),
  KEY at_intake (at_intake),
  KEY entry_type (entry_type),
  KEY at_selection_status (at_selection_status),
  KEY at_document_verified (at_document_verified)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_transaction31012013 (
  at_trans_id bigint(20) NOT NULL AUTO_INCREMENT,
  at_appl_id int(11) NOT NULL COMMENT 'fk  applicant_profile(ap_id)',
  at_pes_id varchar(15) DEFAULT NULL,
  at_appl_type int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  at_academic_year int(11) NOT NULL DEFAULT '2' COMMENT 'fk  academic_year(ay_id)',
  at_intake int(11) NOT NULL,
  at_period int(11) NOT NULL,
  at_status varchar(10) NOT NULL COMMENT 'APPLY, CLOSE, PROCESS, OFFER, REJECT,REGISTERED',
  at_selection_status int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted',
  at_create_by int(11) NOT NULL,
  at_create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  at_submit_date datetime DEFAULT NULL,
  agent_id int(11) DEFAULT NULL,
  entry_type int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  PRIMARY KEY (at_trans_id),
  KEY at_appl_id (at_appl_id),
  KEY at_appl_type (at_appl_type),
  KEY at_period (at_period)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_transaction_722013 (
  at_trans_id bigint(20) NOT NULL AUTO_INCREMENT,
  at_appl_id int(11) NOT NULL COMMENT 'fk  applicant_profile(ap_id)',
  at_pes_id varchar(15) DEFAULT NULL,
  at_appl_type int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  at_academic_year int(11) NOT NULL DEFAULT '2' COMMENT 'fk  academic_year(ay_id)',
  at_intake int(11) NOT NULL,
  at_period int(11) NOT NULL,
  at_status varchar(10) NOT NULL COMMENT 'APPLY, CLOSE, PROCESS, OFFER, REJECT,REGISTERED',
  at_selection_status int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted 4: In Pool (USM)',
  at_create_by int(11) NOT NULL,
  at_create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  at_submit_date datetime DEFAULT NULL,
  agent_id int(11) DEFAULT NULL,
  entry_type int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  PRIMARY KEY (at_trans_id),
  KEY at_appl_id (at_appl_id),
  KEY at_appl_type (at_appl_type),
  KEY at_period (at_period)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE applicant_transaction_20121217 (
  at_trans_id bigint(20) NOT NULL AUTO_INCREMENT,
  at_appl_id int(11) NOT NULL COMMENT 'fk  applicant_profile(ap_id)',
  at_pes_id varchar(15) DEFAULT NULL,
  at_appl_type int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  at_academic_year int(11) NOT NULL DEFAULT '2' COMMENT 'fk  academic_year(ay_id)',
  at_intake int(11) NOT NULL,
  at_period int(11) NOT NULL,
  at_status varchar(10) NOT NULL COMMENT 'APPLY, CLOSE, PROCESS, OFFER, REJECT,REGISTERED',
  at_selection_status int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted',
  at_create_by int(11) NOT NULL,
  at_create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  at_submit_date date DEFAULT NULL,
  agent_id int(11) DEFAULT NULL,
  PRIMARY KEY (at_trans_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE application_profile_6dis_2 (
  appl_id bigint(20) NOT NULL AUTO_INCREMENT,
  appl_fname varchar(100) NOT NULL,
  appl_mname varchar(100) DEFAULT NULL,
  appl_lname varchar(100) NOT NULL,
  appl_address1 varchar(200) DEFAULT NULL,
  appl_address2 varchar(200) DEFAULT NULL,
  appl_city int(11) DEFAULT NULL,
  appl_postcode varchar(5) NOT NULL,
  appl_state int(11) NOT NULL,
  appl_province int(11) DEFAULT NULL,
  appl_email varchar(100) NOT NULL,
  appl_password varchar(100) NOT NULL,
  appl_dob varchar(100) NOT NULL,
  appl_gender tinyint(1) NOT NULL COMMENT '1:  male 2: female',
  appl_prefer_lang int(11) NOT NULL COMMENT 'fk applicant_languange(al_id)',
  appl_nationality int(11) DEFAULT NULL,
  appl_admission_type int(11) DEFAULT NULL,
  appl_religion tinyint(4) DEFAULT NULL,
  appl_marital_status int(11) DEFAULT NULL,
  appl_no_of_child int(11) DEFAULT NULL,
  appl_jacket_size int(11) DEFAULT NULL,
  appl_phone_home int(13) DEFAULT NULL,
  appl_phone_hp varchar(13) DEFAULT NULL,
  appl_caddress1 varchar(200) DEFAULT NULL COMMENT 'correspondence add',
  appl_caddress2 varchar(200) DEFAULT NULL,
  appl_ccity int(11) DEFAULT NULL,
  appl_cstate int(11) DEFAULT NULL,
  appl_cpostcode int(11) DEFAULT NULL,
  appl_cprovince int(11) DEFAULT NULL,
  PRIMARY KEY (appl_id),
  UNIQUE KEY appl_email (appl_email),
  KEY applicant_profile_fk1 (appl_prefer_lang)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE appl_ansscheme (
  aas_id int(11) NOT NULL AUTO_INCREMENT,
  aas_ptest_code varchar(11) NOT NULL,
  aas_set_code int(11) NOT NULL,
  aas_total_quest int(11) NOT NULL,
  aas_status tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active 0:inactive',
  PRIMARY KEY (aas_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE appl_ansscheme_detl (
  aad_id bigint(11) NOT NULL AUTO_INCREMENT,
  aad_anscheme_id int(11) NOT NULL,
  aad_ques_no int(5) NOT NULL,
  aad_ques_ans varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (aad_anscheme_id,aad_ques_no),
  UNIQUE KEY aad_id (aad_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE appl_component (
  ac_id int(11) NOT NULL AUTO_INCREMENT,
  ac_comp_code varchar(10) NOT NULL,
  ac_comp_name varchar(200) NOT NULL,
  ac_comp_name_bahasa varchar(200) NOT NULL,
  ac_short_name varchar(20) NOT NULL,
  ac_test_type int(11) NOT NULL,
  ac_start_time time DEFAULT NULL,
  ac_end_time time DEFAULT NULL,
  ac_update_by int(11) NOT NULL,
  ac_update_date datetime NOT NULL,
  ac_status tinyint(4) NOT NULL,
  aph_type smallint(6) NOT NULL DEFAULT '0' COMMENT '0: USM 1:Psychology',
  PRIMARY KEY (ac_comp_code),
  UNIQUE KEY ac_id (ac_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_family_job (
  afj_id int(11) NOT NULL AUTO_INCREMENT,
  afj_title varchar(300) NOT NULL,
  afj_description text,
  PRIMARY KEY (afj_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE appl_location (
  al_id int(11) NOT NULL AUTO_INCREMENT,
  al_location_code varchar(10) NOT NULL DEFAULT '',
  al_location_name varchar(50) NOT NULL,
  al_address1 varchar(50) NOT NULL,
  al_address2 varchar(50) DEFAULT NULL,
  al_city bigint(20) DEFAULT NULL,
  al_state bigint(20) DEFAULT NULL,
  al_country bigint(20) DEFAULT NULL,
  al_phone varchar(20) DEFAULT NULL,
  al_contact_person varchar(100) DEFAULT NULL,
  al_status tinyint(1) NOT NULL COMMENT '1=Active 0= Inactive',
  al_update_by bigint(20) NOT NULL,
  al_update_date datetime NOT NULL,
  PRIMARY KEY (al_location_code),
  UNIQUE KEY al_id (al_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE appl_pin_to_bank (
  billing_no int(8) DEFAULT NULL,
  PAYEE_ID int(8) NOT NULL DEFAULT '0',
  ADDRESS_1 varchar(20) DEFAULT 'Universitas Trisakti',
  BILL_REF_1 varchar(17) DEFAULT 'Biaya Pendaftaran',
  BILL_REF_2 varchar(10) DEFAULT 'USM online',
  BILL_REF_3 varchar(9) DEFAULT '2013/2014',
  AMOUNT_TOTAL int(6) DEFAULT '250000',
  REGISTER_NO varchar(10) NOT NULL DEFAULT '',
  bank varchar(10) NOT NULL DEFAULT 'BNI',
  `status` varchar(1) NOT NULL DEFAULT 'E' COMMENT 'E -Entry - P -PROCESS',
  intakeId int(11) NOT NULL DEFAULT '78',
  PRIMARY KEY (PAYEE_ID,REGISTER_NO),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE appl_pin_to_bank_17122012 (
  billing_no int(8) DEFAULT NULL,
  PAYEE_ID int(8) NOT NULL DEFAULT '0',
  ADDRESS_1 varchar(20) DEFAULT NULL,
  BILL_REF_1 varchar(17) DEFAULT NULL,
  BILL_REF_2 varchar(10) DEFAULT NULL,
  BILL_REF_3 varchar(9) DEFAULT NULL,
  AMOUNT_TOTAL int(6) DEFAULT NULL,
  REGISTER_NO varchar(10) NOT NULL DEFAULT '',
  bank varchar(10) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT 'E -Entry - P -PROCESS',
  PRIMARY KEY (PAYEE_ID,REGISTER_NO)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_detl (
  apd_id int(11) NOT NULL AUTO_INCREMENT,
  apd_placement_code varchar(10) NOT NULL,
  apd_comp_code varchar(10) NOT NULL,
  apd_total_question int(11) NOT NULL,
  apd_questno_start int(3) NOT NULL,
  apd_questno_end int(3) NOT NULL,
  PRIMARY KEY (apd_placement_code,apd_comp_code),
  UNIQUE KEY apd_placement_seq (apd_id),
  KEY appl_placement_detl_ibfk_2 (apd_comp_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_fee_setup (
  apfs_id int(11) NOT NULL AUTO_INCREMENT,
  apfs_fee_type varchar(10) NOT NULL COMMENT 'fk appl_placement_fee_type(apft_fee_code)',
  apfs_value int(11) NOT NULL COMMENT 'Fee Type Prog - number of prog, fee location value location id',
  apfs_amt decimal(10,2) NOT NULL,
  apfs_currency varchar(5) NOT NULL,
  apfs_start_date date NOT NULL,
  apfs_end_date date DEFAULT NULL,
  apfs_create_by int(11) NOT NULL,
  apfs_create_date datetime NOT NULL,
  PRIMARY KEY (apfs_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_fee_type (
  apft_fee_code varchar(10) NOT NULL,
  apft_fee_desc varchar(20) NOT NULL,
  PRIMARY KEY (apft_fee_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_head (
  aph_id int(11) NOT NULL AUTO_INCREMENT,
  aph_placement_code varchar(10) NOT NULL,
  aph_placement_name varchar(200) NOT NULL,
  aph_academic_year int(11) NOT NULL COMMENT 'PK tbl_intake',
  aph_batch int(11) DEFAULT NULL,
  aph_fees_program tinyint(1) NOT NULL COMMENT '1=''Yes'', 0 =No',
  aph_fees_location tinyint(1) NOT NULL COMMENT '1=''Yes'', 0 =No',
  aph_start_date date NOT NULL,
  aph_end_date date NOT NULL,
  aph_effective_date date NOT NULL,
  aph_create_by int(10) NOT NULL,
  aph_create_date datetime NOT NULL,
  aph_wizard tinyint(4) NOT NULL DEFAULT '1',
  aph_appl_running_no int(11) NOT NULL DEFAULT '0',
  aph_testtype tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (aph_placement_code),
  UNIQUE KEY aph_id (aph_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_program (
  app_id int(11) NOT NULL AUTO_INCREMENT,
  app_placement_code varchar(10) NOT NULL COMMENT 'fk appl_placement_head(apd_placement_code)',
  app_program_code varchar(10) NOT NULL,
  app_pass_mark int(3) NOT NULL,
  app_status tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (app_placement_code,app_program_code),
  UNIQUE KEY app_id (app_id),
  KEY ` appl_placement_program_fk_2` (app_program_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_program_setup (
  apps_id int(11) NOT NULL AUTO_INCREMENT,
  apps_program_id bigint(20) unsigned NOT NULL COMMENT 'fk tbl_program(idProgram)',
  apps_comp_code varchar(10) NOT NULL,
  apps_create_by int(11) NOT NULL COMMENT 'fk tbl_user',
  apps_create_date datetime NOT NULL,
  aph_type smallint(6) NOT NULL DEFAULT '0' COMMENT '0: USM 1:Psychology',
  PRIMARY KEY (apps_program_id,apps_comp_code),
  UNIQUE KEY apps_id (apps_id),
  KEY appl_placement_program_setup_ibfk_2 (apps_comp_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_schedule (
  aps_id int(11) NOT NULL AUTO_INCREMENT,
  aps_placement_code varchar(10) NOT NULL COMMENT 'fk to appl_placement_head(aph_placement_code)',
  aps_location_id int(11) NOT NULL COMMENT 'fk appl_location',
  aps_test_date date NOT NULL,
  aps_start_time time NOT NULL,
  PRIMARY KEY (aps_id),
  KEY appl_placement_schedule_ibfk_1 (aps_placement_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_schedule_seatno (
  apss_id int(11) NOT NULL AUTO_INCREMENT,
  apss_aps_id int(11) NOT NULL,
  apss_room_id int(11) NOT NULL,
  apss_exam_capasity int(11) NOT NULL,
  apss_exam_apply int(11) NOT NULL DEFAULT '0',
  apss_examno_flag varchar(1) NOT NULL DEFAULT 'F',
  apss_examno_f int(3) NOT NULL DEFAULT '0',
  apss_examno_l int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (apss_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_schedule_time (
  apst_id int(11) NOT NULL AUTO_INCREMENT,
  apst_aps_id int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  apst_test_type int(11) NOT NULL COMMENT 'fk appl_test_type(act_id)',
  apst_time_start time NOT NULL,
  apst_time_end time DEFAULT NULL,
  PRIMARY KEY (apst_id),
  KEY appl_placement_schedule_time_fk1 (apst_aps_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_placement_weightage (
  apw_id int(11) NOT NULL AUTO_INCREMENT,
  apw_app_id int(11) NOT NULL,
  apw_apd_id int(11) NOT NULL COMMENT 'fk appl_placement_detl(apd_id)',
  apw_weightage int(3) NOT NULL,
  PRIMARY KEY (apw_app_id,apw_apd_id),
  UNIQUE KEY apw_id (apw_id),
  KEY appl_placement_weightage_fk_2 (apw_apd_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_program_req (
  apr_id int(11) NOT NULL AUTO_INCREMENT,
  apr_academic_year int(11) NOT NULL COMMENT 'fk tbl_program(program_code)',
  apr_program_code varchar(4) NOT NULL,
  apr_decipline_code varchar(3) NOT NULL,
  PRIMARY KEY (apr_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE appl_program_req_mig (
  AcademicYear varchar(9) DEFAULT NULL,
  Periode varchar(1) DEFAULT NULL,
  ProgramCode int(3) DEFAULT NULL,
  HighSchoolDeciplineCode int(3) DEFAULT NULL,
  semester int(1) DEFAULT NULL,
  EntranceCode varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE appl_room (
  av_id int(11) unsigned NOT NULL AUTO_INCREMENT,
  av_location_code int(11) unsigned NOT NULL COMMENT 'FK to tbl_appl_location',
  av_room_name varchar(100) NOT NULL,
  av_room_name_short varchar(50) NOT NULL,
  av_room_code varchar(50) NOT NULL,
  av_building varchar(50) NOT NULL,
  av_tutorial_capacity int(11) NOT NULL DEFAULT '0',
  av_exam_capacity int(11) NOT NULL DEFAULT '0',
  av_update_by bigint(20) NOT NULL,
  av_update_date datetime NOT NULL,
  av_status tinyint(1) NOT NULL DEFAULT '1',
  av_seq int(11) DEFAULT NULL,
  PRIMARY KEY (av_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_room_assign (
  ara_id int(11) NOT NULL AUTO_INCREMENT,
  ara_program1 varchar(2) NOT NULL COMMENT 'fk_app_id',
  ara_program2 varchar(2) NOT NULL,
  ara_room varchar(2) NOT NULL,
  PRIMARY KEY (ara_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_room_test_type (
  artt_id int(11) NOT NULL AUTO_INCREMENT,
  artt_code varchar(15) NOT NULL,
  artt_name varchar(100) NOT NULL,
  PRIMARY KEY (artt_code),
  UNIQUE KEY artt_id (artt_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE appl_room_type (
  art_id int(11) NOT NULL AUTO_INCREMENT,
  art_room_id int(11) NOT NULL,
  art_test_type int(3) NOT NULL,
  PRIMARY KEY (art_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE appl_test_type (
  act_id int(11) NOT NULL AUTO_INCREMENT,
  act_name varchar(50) DEFAULT NULL,
  act_start_time time DEFAULT NULL,
  act_end_time time DEFAULT NULL,
  PRIMARY KEY (act_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE appl_upload_file (
  auf_id int(11) NOT NULL AUTO_INCREMENT,
  auf_appl_id int(11) NOT NULL COMMENT 'changed to transaction_id',
  auf_file_name varchar(1000) NOT NULL,
  auf_file_type int(11) DEFAULT NULL,
  auf_upload_date datetime NOT NULL,
  auf_upload_by int(11) NOT NULL,
  pathupload varchar(500) NOT NULL,
  PRIMARY KEY (auf_id),
  KEY auf_appl_id (auf_appl_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE auditlog (
  audit_id int(11) NOT NULL AUTO_INCREMENT,
  tableName varchar(255) DEFAULT NULL,
  rowPK int(11) DEFAULT NULL,
  fieldName varchar(255) DEFAULT NULL,
  old_value blob,
  new_value blob,
  ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (audit_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE a_usm21 (
  id int(11) NOT NULL AUTO_INCREMENT,
  nomor int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE b_results (
  thn_akad varchar(9) DEFAULT NULL,
  semester int(1) DEFAULT NULL,
  nama_fak varchar(29) DEFAULT NULL,
  nama_jur varchar(22) DEFAULT NULL,
  KD_PIL int(4) DEFAULT NULL,
  NIM varchar(10) DEFAULT NULL,
  nopes int(10) DEFAULT NULL,
  no_form int(10) DEFAULT NULL,
  nama varchar(43) DEFAULT NULL,
  paket varchar(1) DEFAULT NULL,
  nama_ot varchar(29) DEFAULT NULL,
  jn_kel varchar(1) DEFAULT NULL,
  alamat varchar(79) DEFAULT NULL,
  kd_pos varchar(5) DEFAULT NULL,
  kota varchar(30) DEFAULT NULL,
  no_rum varchar(6) DEFAULT NULL,
  rt int(3) DEFAULT NULL,
  rw varchar(3) DEFAULT NULL,
  telp varchar(14) DEFAULT NULL,
  sts_proses int(2) DEFAULT NULL,
  peringkat int(1) DEFAULT NULL,
  jns_masuk varchar(1) DEFAULT NULL,
  NM_MASUK varchar(16) DEFAULT NULL,
  terima int(1) DEFAULT NULL,
  n_of_slip int(1) DEFAULT NULL,
  agama int(1) DEFAULT NULL,
  nm_agama varchar(8) DEFAULT NULL,
  prop_lhr int(2) DEFAULT NULL,
  kodya_lhr int(3) DEFAULT NULL,
  kota_lhr varchar(26) DEFAULT NULL,
  tgl_lhr varchar(11) DEFAULT NULL,
  gel int(1) DEFAULT NULL,
  jur_sla int(3) DEFAULT NULL,
  kd_sla int(8) DEFAULT NULL,
  nm_slta varchar(42) DEFAULT NULL,
  prop_sla int(2) DEFAULT NULL,
  kody_sla int(3) DEFAULT NULL,
  kota_sla varchar(20) DEFAULT NULL,
  alamat_ot varchar(79) DEFAULT NULL,
  norumot varchar(6) DEFAULT NULL,
  pos_ot int(5) DEFAULT NULL,
  prop_ot int(2) DEFAULT NULL,
  rt_ot int(3) DEFAULT NULL,
  rw_ot varchar(3) DEFAULT NULL,
  telp_ot varchar(13) DEFAULT NULL,
  adaayh int(1) DEFAULT NULL,
  adaibu int(1) DEFAULT NULL,
  ddkayh int(1) DEFAULT NULL,
  ddkibu int(1) DEFAULT NULL,
  kerjaayh int(2) DEFAULT NULL,
  pekerjaan_ayah varchar(31) DEFAULT NULL,
  kerjaibu int(2) DEFAULT NULL,
  kerku varchar(10) DEFAULT NULL,
  kes varchar(10) DEFAULT NULL,
  nem varchar(6) DEFAULT NULL,
  proplhr int(2) DEFAULT NULL,
  kodya int(3) DEFAULT NULL,
  kodya_ot int(3) DEFAULT NULL,
  kota_ot varchar(30) DEFAULT NULL,
  ln_ot int(3) DEFAULT NULL,
  nama_ibu varchar(25) DEFAULT NULL,
  negara_wn int(3) DEFAULT NULL,
  prop int(2) DEFAULT NULL,
  w_negara int(1) DEFAULT NULL,
  biaya_sts int(1) DEFAULT NULL,
  daf_ptn varchar(10) DEFAULT NULL,
  daf_pts varchar(10) DEFAULT NULL,
  olhraga varchar(10) DEFAULT NULL,
  sts_bea int(1) DEFAULT NULL,
  sts_sipil int(1) DEFAULT NULL,
  tgllhrd int(5) DEFAULT NULL,
  tgl_hrd varchar(10) DEFAULT NULL,
  e_mail varchar(34) DEFAULT NULL,
  STS_KULIAH varchar(10) DEFAULT NULL,
  TGL_LHRDT int(5) DEFAULT NULL,
  TGLLHRDT varchar(10) DEFAULT NULL,
  NO_SKR varchar(43) DEFAULT NULL,
  TGL_SKR decimal(7,2) DEFAULT NULL,
  TGLSKR varchar(10) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE course_group_attendance (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  group_id bigint(20) NOT NULL,
  total_registered_student int(11) NOT NULL,
  class_date datetime NOT NULL,
  group_schedule_id bigint(20) DEFAULT NULL,
  lecturer_id bigint(20) DEFAULT NULL,
  remark text,
  lecturer_activity text,
  student_assignment text,
  evaluation text,
  create_by bigint(20) NOT NULL,
  create_date datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE course_group_attendance_detail (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  course_group_att_id bigint(20) NOT NULL COMMENT 'fk course_group_attendance',
  student_id bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  student_nim varchar(15) NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms',
  last_edit_by bigint(20) NOT NULL,
  last_edit_date datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE course_group_schedule (
  sc_id int(11) NOT NULL AUTO_INCREMENT,
  idGroup int(11) NOT NULL COMMENT 'fk tbl_course_tagging_group',
  IdLecturer int(11) DEFAULT NULL,
  idCollege int(11) DEFAULT NULL,
  sc_day varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  sc_start_time time NOT NULL,
  sc_end_time time NOT NULL,
  sc_venue varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  sc_class varchar(5) DEFAULT NULL,
  sc_createdby int(11) NOT NULL,
  sc_createddt datetime NOT NULL,
  PRIMARY KEY (sc_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE credit_note (
  cn_id bigint(20) NOT NULL AUTO_INCREMENT,
  cn_billing_no varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  cn_fomulir varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  appl_id bigint(20) DEFAULT NULL,
  cn_amount decimal(20,2) NOT NULL,
  cn_description text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  cn_creator bigint(20) NOT NULL,
  cn_create_date datetime NOT NULL,
  cn_approver bigint(20) DEFAULT NULL,
  cn_approve_date datetime DEFAULT NULL,
  cn_cancel_by bigint(20) DEFAULT NULL,
  cn_cancel_date datetime DEFAULT NULL,
  PRIMARY KEY (cn_id),
  KEY cn_fomulir (cn_fomulir),
  KEY appl_id (appl_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE data1 (
  id int(11) NOT NULL AUTO_INCREMENT,
  field1 varchar(255) DEFAULT NULL,
  field2 varchar(255) DEFAULT NULL,
  field3 varchar(50) DEFAULT NULL,
  field4 varchar(25) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
DROP TRIGGER IF EXISTS `data1_AU`;
DELIMITER //
CREATE TRIGGER `data1_AU` AFTER UPDATE ON `data1`
 FOR EACH ROW BEGIN IF OLD.id != NEW.id THEN INSERT INTO oum_sis.auditlog (tableName, rowPK, fieldName, old_value, new_value) VALUES ( 'data1', NEW.id, 'id', OLD.id, NEW.id); END IF; IF OLD.field1 != NEW.field1 THEN INSERT INTO oum_sis.auditlog (tableName, rowPK, fieldName, old_value, new_value) VALUES ( 'data1', NEW.id, 'field1', OLD.field1, NEW.field1); END IF; IF OLD.field2 != NEW.field2 THEN INSERT INTO oum_sis.auditlog (tableName, rowPK, fieldName, old_value, new_value) VALUES ( 'data1', NEW.id, 'field2', OLD.field2, NEW.field2); END IF; IF OLD.field3 != NEW.field3 THEN INSERT INTO oum_sis.auditlog (tableName, rowPK, fieldName, old_value, new_value) VALUES ( 'data1', NEW.id, 'field3', OLD.field3, NEW.field3); END IF; IF OLD.field4 != NEW.field4 THEN INSERT INTO oum_sis.auditlog (tableName, rowPK, fieldName, old_value, new_value) VALUES ( 'data1', NEW.id, 'field4', OLD.field4, NEW.field4); END IF;
END
//
DELIMITER ;

CREATE TABLE discount (
  dcnt_id bigint(20) NOT NULL AUTO_INCREMENT,
  dcnt_appl_id bigint(20) NOT NULL COMMENT 'fk_applicant_profile',
  dcnt_txn_id bigint(20) NOT NULL COMMENT 'fk_applicant_transaction',
  dcnt_fomulir_id varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  dcnt_type_id bigint(20) NOT NULL COMMENT 'fk_discount_type',
  dcnt_letter_number varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  dcnt_amount decimal(20,2) NOT NULL,
  dcnt_invoice_id bigint(20) DEFAULT NULL,
  dcnt_creator bigint(20) NOT NULL,
  dcnt_create_date datetime NOT NULL,
  PRIMARY KEY (dcnt_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE discount_type (
  dt_id bigint(20) NOT NULL AUTO_INCREMENT,
  dt_discount varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  dt_description text COLLATE utf8_unicode_ci NOT NULL,
  dt_creator bigint(20) NOT NULL,
  dt_create_date datetime NOT NULL,
  dt_delete_status tinyint(4) NOT NULL DEFAULT '0',
  dt_delete_by bigint(20) DEFAULT NULL,
  dt_delete_date datetime DEFAULT NULL,
  PRIMARY KEY (dt_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE email_blast_history (
  id int(11) NOT NULL AUTO_INCREMENT,
  send_date datetime NOT NULL,
  send_by int(11) NOT NULL,
  application_transaction int(11) NOT NULL,
  `subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  content text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1: success; 0: fail',
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE email_blast_tmpl (
  ebt_id int(11) NOT NULL AUTO_INCREMENT,
  ebt_template_name varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ebt_email_from varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ebt_email_from_name varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ebt_create_by bigint(20) unsigned NOT NULL,
  ebt_create_date datetime NOT NULL,
  ebt_update_by bigint(20) DEFAULT NULL,
  ebt_update_date datetime DEFAULT NULL,
  PRIMARY KEY (ebt_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE email_blast_tmpl_content (
  ebtc_id int(11) NOT NULL AUTO_INCREMENT,
  ebtc_ebt_id int(11) NOT NULL,
  ebtc_language tinyint(4) NOT NULL COMMENT '1: english; 2: Indonesia',
  ebtc_subject varchar(100) NOT NULL,
  ebtc_header varchar(100) DEFAULT NULL,
  ebtc_body text NOT NULL,
  ebtc_footer varchar(100) DEFAULT NULL,
  PRIMARY KEY (ebtc_id),
  KEY email_template_detl_fk1 (ebtc_ebt_id),
  KEY email_template_detl_fk2 (ebtc_language)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE email_que (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  recepient_email varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  content text COLLATE utf8_unicode_ci NOT NULL,
  attachment_filename varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  attachment_path varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  date_que datetime NOT NULL,
  date_send datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE email_template_detl (
  etd_id int(11) NOT NULL AUTO_INCREMENT,
  etd_eth_id int(11) NOT NULL,
  etd_language tinyint(4) NOT NULL,
  etd_subject varchar(100) NOT NULL,
  etd_header varchar(100) DEFAULT NULL,
  etd_body varchar(7277) NOT NULL,
  etd_footer varchar(100) DEFAULT NULL,
  PRIMARY KEY (etd_id),
  KEY email_template_detl_fk1 (etd_eth_id),
  KEY email_template_detl_fk2 (etd_language)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE email_template_head (
  eth_id int(11) NOT NULL AUTO_INCREMENT,
  eth_template_name varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  eth_email_from varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  eth_email_from_name varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  eth_create_by bigint(20) unsigned NOT NULL,
  eth_create_date datetime NOT NULL,
  PRIMARY KEY (eth_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE exam_group (
  eg_id int(20) NOT NULL AUTO_INCREMENT,
  eg_sem_id int(10) unsigned NOT NULL,
  eg_sub_id int(10) unsigned NOT NULL,
  eg_group_name varchar(200) NOT NULL,
  eg_group_code varchar(10) NOT NULL,
  eg_assessment_type bigint(20) NOT NULL COMMENT 'fk tbl_examination_assessment_type',
  eg_capacity int(11) NOT NULL,
  eg_room_id int(11) unsigned NOT NULL COMMENT 'fk appl_room',
  eg_date date NOT NULL,
  eg_start_time time NOT NULL,
  eg_end_time time NOT NULL,
  eg_create_by bigint(20) NOT NULL,
  eg_create_date datetime NOT NULL,
  PRIMARY KEY (eg_id),
  KEY eg_room_id (eg_room_id),
  KEY eg_sem_id (eg_sem_id,eg_sub_id),
  KEY eg_assessment_type (eg_assessment_type)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE exam_group_attendance (
  ega_id int(11) NOT NULL AUTO_INCREMENT,
  ega_eg_id int(11) NOT NULL COMMENT 'fk exam_group',
  ega_student_id bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  ega_student_nim varchar(20) NOT NULL,
  ega_status int(11) NOT NULL COMMENT 'fk_tbl_definationms',
  ega_last_edit_by bigint(20) NOT NULL,
  ega_last_edit_date datetime NOT NULL,
  PRIMARY KEY (ega_id),
  UNIQUE KEY ega_eg_id_2 (ega_eg_id,ega_student_id),
  KEY ega_eg_id (ega_eg_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE exam_group_student (
  egst_id int(11) NOT NULL AUTO_INCREMENT,
  egst_group_id int(11) NOT NULL COMMENT 'fk exam_group',
  egst_student_id int(11) NOT NULL COMMENT 'fk tbl_studentregistration',
  egst_student_nim varchar(15) NOT NULL,
  egst_subject_id int(11) NOT NULL,
  egst_semester_id bigint(20) NOT NULL COMMENT 'fk tbl_semesterMain',
  egst_seat varchar(50) DEFAULT NULL,
  PRIMARY KEY (egst_id),
  KEY egst_group_id (egst_group_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE exam_group_supervisor (
  egs_id int(11) NOT NULL AUTO_INCREMENT,
  egs_eg_id int(11) NOT NULL,
  egs_staff_id bigint(20) unsigned NOT NULL,
  egs_create_by bigint(20) NOT NULL,
  egs_create_date datetime NOT NULL,
  PRIMARY KEY (egs_id),
  KEY egs_eg_id (egs_eg_id,egs_staff_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE fee_discount_rank (
  fdr_id int(11) NOT NULL AUTO_INCREMENT,
  fdr_rank int(11) NOT NULL,
  fdr_item_id int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  fdr_percentage decimal(10,2) DEFAULT NULL,
  fdr_amount decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (fdr_id),
  KEY fdr_rank (fdr_rank),
  KEY fdr_item_id (fdr_item_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_item (
  fi_id int(11) NOT NULL AUTO_INCREMENT,
  fi_name varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  fi_name_bahasa varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  fi_name_short varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  fi_code varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  fi_amount_calculation_type int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Calculation Type)',
  fi_frequency_mode int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Frequency Mode)',
  fi_active tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (fi_id),
  KEY fi_amount_calculation_type (fi_amount_calculation_type),
  KEY fi_frequency_mode (fi_frequency_mode)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_item_account (
  fiacc_id int(11) NOT NULL AUTO_INCREMENT,
  fiacc_fee_item int(11) NOT NULL COMMENT 'fk_fee_item',
  fiacc_program_id int(11) NOT NULL COMMENT 'fk tbl_program',
  fiacc_faculty_id int(11) NOT NULL COMMENT 'fk_collegemaster',
  fiacc_account varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  fiacc_bank bigint(20) NOT NULL COMMENT 'fk_tbl_definationms',
  fiacc_creator bigint(20) NOT NULL COMMENT 'fk_tbl_user',
  fiacc_create_date datetime NOT NULL,
  fiacc_update_by bigint(20) DEFAULT NULL COMMENT 'fk_tbl_user',
  fiacc_update_date datetime DEFAULT NULL,
  PRIMARY KEY (fiacc_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_quit (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  intake_id bigint(20) NOT NULL COMMENT 'fk_tbl_intake',
  last_effective_date date NOT NULL,
  update_by bigint(20) NOT NULL,
  update_date datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1: active',
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE fee_quit_faculty (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  fq_id bigint(20) NOT NULL COMMENT 'fk_fee_quit',
  college_id bigint(20) NOT NULL,
  amount decimal(10,2) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY fq_id (fq_id,college_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE fee_structure (
  fs_id int(11) NOT NULL AUTO_INCREMENT,
  fs_name varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  fs_description text COLLATE utf8_unicode_ci,
  fs_intake_start int(11) NOT NULL,
  fs_intake_end int(11) DEFAULT NULL,
  fs_student_category int(11) NOT NULL COMMENT 'fl_tbl_definationms (Fee Structure Student Category)',
  PRIMARY KEY (fs_id),
  KEY fs_intake_start (fs_intake_start),
  KEY fs_intake_end (fs_intake_end),
  KEY fs_student_category (fs_student_category)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_structure_item (
  fsi_id int(11) NOT NULL AUTO_INCREMENT,
  fsi_structure_id int(11) NOT NULL COMMENT 'fk_fee_structure',
  fsi_item_id int(11) NOT NULL COMMENT 'fk_fee_item',
  fsi_amount decimal(11,2) NOT NULL,
  PRIMARY KEY (fsi_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_structure_item_semester (
  fsis_id int(11) NOT NULL AUTO_INCREMENT,
  fsis_item_id int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  fsis_semester int(11) NOT NULL,
  PRIMARY KEY (fsis_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_structure_plan (
  fsp_id int(11) NOT NULL AUTO_INCREMENT,
  fsp_structure_id int(11) NOT NULL COMMENT 'fk_fee_structure',
  fsp_name varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  fsp_billing_no varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  fsp_bil_installment int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (fsp_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='cicilan table';

CREATE TABLE fee_structure_plan_detl (
  fspd_id int(11) NOT NULL AUTO_INCREMENT,
  fspd_plan_id int(11) NOT NULL COMMENT 'fk_fee_structure_plan',
  fspd_installment_no int(11) NOT NULL,
  fspd_item_id int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  fspd_percentage decimal(10,2) DEFAULT NULL,
  fspd_amount decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (fspd_id),
  UNIQUE KEY fspd_plan_id (fspd_plan_id,fspd_installment_no,fspd_item_id),
  KEY fspd_plan_id_2 (fspd_plan_id),
  KEY fspd_item_id (fspd_item_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE fee_structure_program (
  fsp_id int(11) NOT NULL AUTO_INCREMENT,
  fsp_fs_id int(11) NOT NULL COMMENT 'fk_fee_structure',
  fsp_program_id int(11) NOT NULL,
  fsp_first_sem_sks int(11) NOT NULL,
  PRIMARY KEY (fsp_id),
  KEY fsp_program_id (fsp_program_id),
  KEY fsp_fs_id (fsp_fs_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE icourse (
  coursecode varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  arabic varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  eng varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  credithour int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE idepartment (
  id int(11) NOT NULL,
  arabic varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  eng varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  idfaculty int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE ifaculty (
  id int(11) NOT NULL,
  arabic varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  eng varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE ilandscape (
  progcode int(11) NOT NULL,
  progname_a varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  progname_e varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  sem int(11) NOT NULL,
  semlandscape int(11) NOT NULL,
  coursecode int(11) NOT NULL,
  coursename_a varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  coursename_e varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  credithour int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE invoice_delete_history (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  invoice_main_id int(11) NOT NULL,
  bill_number varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  appl_id bigint(20) DEFAULT NULL,
  no_fomulir varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  academic_year int(11) NOT NULL COMMENT 'fk  academic_year',
  semester bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  bill_amount decimal(20,2) NOT NULL,
  bill_paid decimal(20,2) DEFAULT NULL,
  bill_balance decimal(20,2) DEFAULT NULL,
  bill_description varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  college_id bigint(20) NOT NULL,
  program_code varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  date_create datetime NOT NULL,
  creator bigint(20) NOT NULL COMMENT 'fk tbl_user',
  fs_id bigint(20) NOT NULL COMMENT 'fk fee_structure',
  fsp_id bigint(20) NOT NULL COMMENT 'fk fee_structure_plan',
  cn_amount decimal(20,2) DEFAULT '0.00',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A:active X: cancel',
  cancel_by bigint(20) DEFAULT NULL,
  cancel_date datetime DEFAULT NULL,
  delete_by bigint(20) NOT NULL,
  delete_date datetime NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY bill_number (bill_number),
  KEY no_fomulir (no_fomulir),
  KEY appl_id (appl_id),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE invoice_detail (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  invoice_main_id bigint(20) NOT NULL,
  fi_id int(11) NOT NULL COMMENT 'fk fee_item',
  fee_item_description varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  amount decimal(20,2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE invoice_detail_1162013 (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  invoice_main_id bigint(20) NOT NULL,
  fi_id int(11) NOT NULL COMMENT 'fk fee_item',
  fee_item_description varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  amount decimal(20,2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE invoice_main (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  bill_number varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  appl_id bigint(20) DEFAULT NULL,
  no_fomulir varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  academic_year int(11) NOT NULL COMMENT 'fk  academic_year',
  semester bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  bill_amount decimal(20,2) NOT NULL,
  bill_paid decimal(20,2) DEFAULT NULL,
  bill_balance decimal(20,2) DEFAULT NULL,
  bill_description varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  college_id bigint(20) NOT NULL,
  program_code varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  date_create datetime NOT NULL,
  creator bigint(20) NOT NULL COMMENT 'fk tbl_user',
  fs_id bigint(20) NOT NULL COMMENT 'fk fee_structure',
  fsp_id bigint(20) NOT NULL COMMENT 'fk fee_structure_plan',
  cn_amount decimal(20,2) DEFAULT '0.00',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A:active X: cancel',
  cancel_by bigint(20) DEFAULT NULL,
  cancel_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY bill_number (bill_number),
  KEY no_fomulir (no_fomulir),
  KEY appl_id (appl_id),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE invoice_main_1162013 (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  bill_number varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  appl_id bigint(20) DEFAULT NULL,
  no_fomulir varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  academic_year int(11) NOT NULL COMMENT 'fk  academic_year',
  semester bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  bill_amount decimal(20,2) NOT NULL,
  bill_paid decimal(20,2) DEFAULT NULL,
  bill_balance decimal(20,2) DEFAULT NULL,
  bill_description varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  college_id bigint(20) NOT NULL,
  program_code varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  date_create datetime NOT NULL,
  creator bigint(20) NOT NULL COMMENT 'fk tbl_user',
  fs_id bigint(20) NOT NULL COMMENT 'fk fee_structure',
  fsp_id bigint(20) NOT NULL COMMENT 'fk fee_structure_plan',
  cn_amount decimal(10,2) DEFAULT '0.00',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A:active X: cancel',
  cancel_by bigint(20) DEFAULT NULL,
  cancel_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY no_fomulir (no_fomulir),
  KEY appl_id (appl_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE iprogram (
  id int(11) NOT NULL,
  arabic varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  eng varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  award varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE istaff (
  id int(11) NOT NULL,
  staffname varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  faculty varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  faccode int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE isubject (
  a varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  b varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  c varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  d varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  e varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE kfm_directories (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  parent int(11) NOT NULL,
  maxwidth int(11) DEFAULT '0',
  maxheight int(11) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE kfm_files (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `directory` int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE kfm_files_images (
  id int(11) NOT NULL AUTO_INCREMENT,
  caption text,
  file_id int(11) NOT NULL,
  width int(11) DEFAULT '0',
  height int(11) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_files_images_thumbs (
  id int(11) NOT NULL AUTO_INCREMENT,
  image_id int(11) NOT NULL,
  width int(11) DEFAULT '0',
  height int(11) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_parameters (
  `name` text,
  `value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_plugin_extensions (
  id int(11) NOT NULL AUTO_INCREMENT,
  extension varchar(64) DEFAULT NULL,
  `plugin` varchar(64) DEFAULT NULL,
  user_id int(8) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_session (
  id int(11) NOT NULL AUTO_INCREMENT,
  cookie varchar(32) DEFAULT NULL,
  last_accessed datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE kfm_session_vars (
  session_id int(11) DEFAULT NULL,
  varname text,
  varvalue text,
  KEY session_id (session_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_settings (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `value` varchar(256) DEFAULT NULL,
  user_id int(8) DEFAULT NULL,
  usersetting int(1) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE kfm_tagged_files (
  file_id int(11) DEFAULT NULL,
  tag_id int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_tags (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_translations (
  original text,
  translation text,
  `language` varchar(2) DEFAULT NULL,
  calls int(11) DEFAULT '0',
  `found` int(11) DEFAULT '1',
  `context` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE kfm_users (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(16) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `status` int(1) DEFAULT '2',
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE merge_log (
  id int(11) NOT NULL AUTO_INCREMENT,
  m_formulir1 varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  m_formulir2 varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  mail_archive varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  mail_maintain varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  from_appl_id int(11) NOT NULL,
  to_appl_id int(11) NOT NULL,
  mergedate datetime NOT NULL,
  mergeby int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE NIDON (
  nik varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  nama varchar(29) DEFAULT NULL,
  nidon varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE payment_bank_record (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  file_path text COLLATE utf8_unicode_ci NOT NULL,
  upload_by int(11) NOT NULL,
  upload_date datetime NOT NULL,
  file_created_date datetime NOT NULL,
  period_start date NOT NULL,
  period_end date NOT NULL,
  total_record int(11) NOT NULL,
  total_amount decimal(30,2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment_bank_record_detail (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  pbr_id bigint(20) NOT NULL,
  billing_no varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  payee_id varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  bill_ref_1 text COLLATE utf8_unicode_ci NOT NULL,
  bill_ref_2 text COLLATE utf8_unicode_ci NOT NULL,
  bill_ref_3 text COLLATE utf8_unicode_ci NOT NULL,
  bill_ref_4 text COLLATE utf8_unicode_ci NOT NULL,
  bill_ref_5 text COLLATE utf8_unicode_ci NOT NULL,
  amount_total decimal(20,0) NOT NULL,
  amount_1 decimal(20,0) NOT NULL,
  amount_2 decimal(20,0) NOT NULL,
  amount_3 decimal(20,0) NOT NULL,
  amount_4 decimal(20,0) NOT NULL,
  amount_5 decimal(20,0) NOT NULL,
  amount_6 decimal(20,0) NOT NULL,
  amount_7 decimal(20,0) NOT NULL,
  amount_8 decimal(20,0) NOT NULL,
  amount_9 decimal(20,0) NOT NULL,
  amount_10 decimal(20,0) NOT NULL,
  status_payment tinytext COLLATE utf8_unicode_ci NOT NULL,
  keterangan text COLLATE utf8_unicode_ci NOT NULL,
  paid_date date NOT NULL,
  bancs_journal_number varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  payment_channel varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  branch_id varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  payment_method varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  debet_account text COLLATE utf8_unicode_ci NOT NULL,
  account_record_type varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  finance_processing tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  KEY account_record_type (account_record_type),
  KEY payee_id (payee_id),
  KEY billing_no (billing_no)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment_detail (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  pm_id bigint(20) NOT NULL,
  item_description text COLLATE utf8_unicode_ci NOT NULL,
  item_amount decimal(20,2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment_import (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  payer varchar(15) NOT NULL,
  appl_id bigint(20) NOT NULL,
  payment_date datetime NOT NULL,
  amount decimal(20,2) NOT NULL,
  description text NOT NULL,
  payment_mode varchar(50) NOT NULL,
  bank_reference varchar(50) NOT NULL,
  importer bigint(20) NOT NULL,
  import_date datetime NOT NULL,
  payment_main_id bigint(20) NOT NULL COMMENT 'fk payment_main',
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE payment_main (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  billing_no varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  payer varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  appl_id int(11) DEFAULT NULL,
  payment_description text COLLATE utf8_unicode_ci NOT NULL,
  amount decimal(20,2) NOT NULL,
  payment_mode varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  transaction_reference bigint(20) DEFAULT NULL COMMENT 'pk payment_bank_record_detail',
  slip_transaction_reference varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  payment_date datetime NOT NULL,
  PRIMARY KEY (id),
  KEY billing_no (billing_no),
  KEY payer (payer),
  KEY appl_id (appl_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment_plan (
  pp_id bigint(20) NOT NULL AUTO_INCREMENT,
  pp_appl_id int(11) NOT NULL,
  pp_installment_no int(11) NOT NULL,
  pp_amount decimal(10,2) NOT NULL,
  pp_creator bigint(20) NOT NULL,
  pp_create_date datetime NOT NULL,
  PRIMARY KEY (pp_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment_plan_detail (
  ppd_id bigint(20) NOT NULL AUTO_INCREMENT,
  ppd_payment_plan_id bigint(20) NOT NULL,
  ppd_invoice_id bigint(20) NOT NULL,
  ppd_installment_no int(11) NOT NULL,
  PRIMARY KEY (ppd_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment_plan_invoice (
  ppi_id bigint(11) NOT NULL AUTO_INCREMENT,
  ppi_plan_id bigint(11) NOT NULL COMMENT 'fk_payment_plan',
  ppi_invoice_id bigint(11) NOT NULL COMMENT 'fk_invoice_main',
  ppi_invoice_no varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (ppi_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table to store invoice that will be repayment plan';

CREATE TABLE raw_email (
  appid varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (appid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE raw_staff (
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nostaff varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  add1 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  add2 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  postcode varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  tel1 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  tel2 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  mobile varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  sex varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  dob varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  race varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  religion varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nationality varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  mstatus varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  icno varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  pic varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  dept varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  reportto varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  regdate varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  expdate varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  faculty varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  cstatus varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE refund (
  rfd_id int(11) NOT NULL AUTO_INCREMENT,
  rfd_appl_id bigint(11) NOT NULL,
  rfd_acad_year_id bigint(11) NOT NULL,
  rfd_sem_id bigint(11) DEFAULT NULL,
  rfd_fomulir varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  rfd_desc text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  rfd_amount decimal(20,2) NOT NULL,
  rdf_refund_cheque_id bigint(20) DEFAULT NULL,
  rfd_creator bigint(11) NOT NULL,
  rfd_create_date datetime NOT NULL,
  rfd_avdpy_id bigint(11) DEFAULT NULL,
  rfd_bank_acc varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  rfd_approver_id bigint(20) DEFAULT NULL,
  rfd_approve_date datetime DEFAULT NULL,
  rfd_status char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A:active X: cancel',
  PRIMARY KEY (rfd_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE refund_cheque (
  rchq_id bigint(20) NOT NULL AUTO_INCREMENT,
  rchq_cheque_no varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  rchq_bank_name varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  rchq_amount decimal(10,2) NOT NULL,
  rchq_issue_date date NOT NULL,
  rchq_insert_date datetime NOT NULL,
  rchq_insert_by bigint(20) NOT NULL,
  rchq_status char(1) NOT NULL DEFAULT 'A',
  rchq_cancel_by bigint(20) DEFAULT NULL,
  rchq_cancel_date datetime DEFAULT NULL,
  rchq_collector_name varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  rchq_collector_date datetime DEFAULT NULL,
  rchq_collector_id varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  rchq_collector_id_type int(11) DEFAULT NULL,
  rchq_collector_update_by bigint(20) DEFAULT NULL,
  PRIMARY KEY (rchq_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE refund_detail (
  rfdd_id int(11) NOT NULL AUTO_INCREMENT,
  rfdd_refund_id bigint(20) NOT NULL,
  rfdd_advpydet_id bigint(20) NOT NULL,
  rfdd_amount decimal(20,2) NOT NULL,
  PRIMARY KEY (rfdd_id),
  KEY rfdd_refund_id (rfdd_refund_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE school_decipline_subject (
  sds_id int(11) NOT NULL AUTO_INCREMENT,
  sds_discipline_code varchar(3) NOT NULL COMMENT 'fk school_discipline(smj_code)',
  sds_subject varchar(100) NOT NULL,
  PRIMARY KEY (sds_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE school_discipline (
  smd_code varchar(3) NOT NULL,
  smd_desc varchar(150) NOT NULL,
  smd_school_type int(11) NOT NULL COMMENT 'school_type(st_id)',
  PRIMARY KEY (smd_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE school_master (
  sm_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  sm_name varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  sm_name_bahasa varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_type varchar(15) NOT NULL,
  sm_address1 varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  sm_address2 varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_city int(20) NOT NULL,
  sm_state int(20) unsigned NOT NULL,
  sm_country int(20) unsigned NOT NULL,
  sm_email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_url varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_status tinyint(1) NOT NULL DEFAULT '1',
  sm_create_date datetime NOT NULL,
  sm_create_by bigint(20) unsigned NOT NULL,
  sm_phone_o varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_school_status bigint(20) unsigned DEFAULT NULL,
  sm_school_code varchar(20) DEFAULT NULL,
  PRIMARY KEY (sm_id),
  KEY sm_state (sm_state),
  KEY sm_name (sm_name)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE school_master_20121218 (
  sm_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  sm_name varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  sm_name_bahasa varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_type varchar(15) NOT NULL,
  sm_address1 varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  sm_address2 varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_city int(20) NOT NULL,
  sm_state int(20) unsigned NOT NULL,
  sm_country int(20) unsigned NOT NULL,
  sm_email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_url varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_status tinyint(1) NOT NULL DEFAULT '1',
  sm_create_date datetime NOT NULL,
  sm_create_by bigint(20) unsigned NOT NULL,
  sm_phone_o varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_school_status bigint(20) unsigned DEFAULT NULL,
  sm_school_code varchar(20) DEFAULT NULL,
  PRIMARY KEY (sm_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE school_subject (
  ss_id int(11) NOT NULL AUTO_INCREMENT,
  ss_subject varchar(100) NOT NULL,
  ss_subject_bahasa varchar(100) DEFAULT NULL,
  ss_core_subject tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 core subj ',
  PRIMARY KEY (ss_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE school_type (
  st_id int(11) NOT NULL AUTO_INCREMENT,
  st_code varchar(20) NOT NULL,
  st_name varchar(50) NOT NULL,
  st_name_bahasa varchar(50) DEFAULT NULL,
  PRIMARY KEY (st_code),
  UNIQUE KEY sl_id (st_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE semester_activity_schedule (
  sas_id int(11) NOT NULL AUTO_INCREMENT,
  sas_sem_id int(11) NOT NULL,
  sas_activity_id int(11) NOT NULL,
  sas_start_date date DEFAULT NULL,
  sas_end_date date DEFAULT NULL,
  sas_activity_date date DEFAULT NULL,
  PRIMARY KEY (sas_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE seq (
  `name` varchar(20) CHARACTER SET latin1 NOT NULL,
  val int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE sis_language (
  sl_id int(11) NOT NULL AUTO_INCREMENT,
  sl_var_name varchar(100) NOT NULL,
  sl_english varchar(300) NOT NULL,
  sl_bahasa varchar(300) NOT NULL,
  createdby int(11) DEFAULT NULL,
  createddt datetime DEFAULT NULL,
  `Language` int(11) DEFAULT NULL,
  PRIMARY KEY (sl_id),
  UNIQUE KEY sl_var_name (sl_var_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE sis_setup_detl (
  ssd_id int(11) NOT NULL AUTO_INCREMENT,
  ssd_code varchar(10) NOT NULL,
  ssd_seq int(11) NOT NULL,
  ssd_name varchar(50) NOT NULL,
  ssd_name_bahasa varchar(50) NOT NULL,
  PRIMARY KEY (ssd_id),
  KEY sis_setup_detl_fk1 (ssd_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE sis_setup_detl_612 (
  ssd_id int(11) NOT NULL AUTO_INCREMENT,
  ssd_code varchar(10) NOT NULL,
  ssd_seq int(11) NOT NULL,
  ssd_name varchar(50) NOT NULL,
  ssd_name_bahasa varchar(50) NOT NULL,
  PRIMARY KEY (ssd_id),
  KEY sis_setup_detl_fk1 (ssd_code)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE sis_setup_head (
  ssh_id int(11) NOT NULL AUTO_INCREMENT,
  ssh_code varchar(10) NOT NULL,
  ssh_desc varchar(100) NOT NULL,
  PRIMARY KEY (ssh_code),
  UNIQUE KEY ssh_id (ssh_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE spmbraw (
  id int(11) NOT NULL AUTO_INCREMENT,
  nopes varchar(255) NOT NULL,
  fname varchar(255) NOT NULL,
  alamat varchar(255) NOT NULL,
  tel varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  nilai varchar(255) NOT NULL,
  mutu varchar(255) NOT NULL,
  dob varchar(255) NOT NULL,
  province varchar(255) NOT NULL,
  sex varchar(11) NOT NULL,
  prog varchar(255) NOT NULL,
  `status` varchar(25) NOT NULL,
  appl_id int(11) NOT NULL,
  transid int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE spmbraw1 (
  id int(11) NOT NULL AUTO_INCREMENT,
  nopes varchar(255) NOT NULL,
  fname varchar(255) NOT NULL,
  alamat varchar(255) NOT NULL,
  tel varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  nilai varchar(255) NOT NULL,
  mutu varchar(255) NOT NULL,
  dob varchar(255) NOT NULL,
  province varchar(255) NOT NULL,
  sex varchar(11) NOT NULL,
  prog varchar(255) NOT NULL,
  `status` varchar(25) NOT NULL,
  appl_id int(11) NOT NULL,
  transid int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE student_ansscheme (
  sas_id int(11) NOT NULL AUTO_INCREMENT,
  sas_IdMarksDistributionMaster bigint(20) NOT NULL,
  sas_IdMarksDistributionDetails bigint(20) DEFAULT NULL,
  sas_set_code varchar(10) NOT NULL,
  sas_total_quest int(11) NOT NULL,
  sas_status tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active 0:inactive',
  PRIMARY KEY (sas_id),
  KEY sas_IdMarksDistributionMaster (sas_IdMarksDistributionMaster),
  KEY sas_IdMarksDistributionDetails (sas_IdMarksDistributionDetails),
  KEY sas_id (sas_id),
  KEY sas_IdMarksDistributionMaster_2 (sas_IdMarksDistributionMaster)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE student_ansscheme_detl (
  sad_id bigint(11) NOT NULL AUTO_INCREMENT,
  sad_anscheme_id int(11) NOT NULL,
  sad_ques_no int(5) NOT NULL,
  sad_ques_ans varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (sad_anscheme_id,sad_ques_no),
  UNIQUE KEY sad_id (sad_id),
  KEY sad_anscheme_id (sad_anscheme_id),
  KEY sad_id_2 (sad_id),
  KEY sad_anscheme_id_2 (sad_anscheme_id),
  KEY sad_ques_no (sad_ques_no)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE student_mark_raw (
  tmp_id bigint(20) NOT NULL AUTO_INCREMENT,
  tmp_name varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  tmp_set_code varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  tmp_answers_raw text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  tmp_file varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (tmp_id),
  KEY tmp_set_code (tmp_set_code),
  KEY tmp_file (tmp_file)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE student_omr_answers (
  soa_id int(11) NOT NULL AUTO_INCREMENT,
  soa_IdStudentRegistration int(11) NOT NULL,
  soa_exam_groupid int(11) NOT NULL COMMENT 'fk exam_group',
  soa_IdMarksDistributionMaster int(11) NOT NULL,
  soa_IdMarksDistributionDetails int(11) NOT NULL,
  soa_set_code varchar(10) NOT NULL,
  soa_createddt datetime NOT NULL,
  soa_createdby bigint(20) NOT NULL,
  soa_status tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (soa_id),
  KEY soa_id (soa_id),
  KEY soa_IdStudentRegistration (soa_IdStudentRegistration),
  KEY soa_exam_groupid (soa_exam_groupid),
  KEY soa_IdStudentRegistration_2 (soa_IdStudentRegistration),
  KEY soa_id_2 (soa_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE student_omr_ans_detl (
  soad_id int(11) NOT NULL AUTO_INCREMENT,
  soad_soa_id int(11) NOT NULL COMMENT 'fk student_omr_answer)',
  soad_ques_no int(5) NOT NULL,
  soad_student_ans varchar(1) NOT NULL,
  soad_status_ans tinyint(4) NOT NULL COMMENT '1 betul 0 salah',
  PRIMARY KEY (soad_id),
  KEY apad_apa_id (soad_soa_id),
  KEY apad_status_ans (soad_status_ans),
  KEY soad_soa_id (soad_soa_id),
  KEY soad_id (soad_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE student_profile (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  appl_id bigint(20) NOT NULL,
  appl_fname varchar(100) DEFAULT NULL,
  appl_mname varchar(100) DEFAULT NULL,
  appl_lname varchar(100) NOT NULL,
  appl_name_kartu varchar(20) DEFAULT NULL,
  appl_address_rw varchar(100) DEFAULT NULL,
  appl_address_rt varchar(100) DEFAULT NULL,
  appl_address1 varchar(200) DEFAULT NULL,
  appl_address2 varchar(200) DEFAULT NULL,
  appl_kelurahan varchar(200) DEFAULT NULL,
  appl_kecamatan varchar(200) DEFAULT NULL,
  appl_city int(11) DEFAULT NULL,
  appl_postcode varchar(5) DEFAULT NULL,
  appl_state int(11) DEFAULT NULL,
  appl_province int(11) DEFAULT NULL COMMENT 'Country',
  appl_email varchar(100) NOT NULL,
  appl_username varchar(100) DEFAULT NULL COMMENT 'later nanti nak pakai just create aje',
  appl_password varchar(100) NOT NULL,
  appl_dob varchar(100) NOT NULL,
  appl_birth_place varchar(200) NOT NULL,
  appl_gender tinyint(1) NOT NULL COMMENT '1:  male 2: female',
  appl_prefer_lang int(11) NOT NULL DEFAULT '2' COMMENT 'fk applicant_languange(al_id)',
  appl_nationality int(11) NOT NULL DEFAULT '1',
  appl_religion tinyint(4) DEFAULT NULL,
  appl_marital_status int(11) DEFAULT NULL,
  appl_no_of_child int(11) DEFAULT NULL,
  appl_jacket_size int(11) DEFAULT NULL,
  appl_parent_salary decimal(20,2) DEFAULT NULL,
  appl_phone_home int(13) DEFAULT NULL,
  appl_phone_hp varchar(13) DEFAULT NULL,
  appl_caddress_rt varchar(100) DEFAULT NULL,
  appl_caddress_rw varchar(10) DEFAULT NULL,
  appl_caddress1 varchar(200) DEFAULT NULL COMMENT 'correspondence add',
  appl_caddress2 varchar(200) DEFAULT NULL,
  appl_ckelurahan varchar(200) DEFAULT NULL,
  appl_ckecamatan varchar(200) DEFAULT NULL,
  appl_ccity int(11) DEFAULT NULL,
  appl_cstate int(11) DEFAULT NULL,
  appl_cpostcode int(11) DEFAULT NULL,
  appl_cprovince int(11) DEFAULT NULL,
  change_passwordby int(11) NOT NULL,
  change_passworddt datetime NOT NULL,
  appl_role int(11) DEFAULT NULL COMMENT '0:Applicant  1:Student',
  migrate_date datetime NOT NULL,
  migrate_by int(11) NOT NULL,
  burekol_verify_date datetime DEFAULT NULL,
  burekol_verify_by bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY appl_id (appl_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE student_profile_20130910 (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  appl_id bigint(20) NOT NULL,
  appl_fname varchar(100) NOT NULL,
  appl_mname varchar(100) DEFAULT NULL,
  appl_lname varchar(100) NOT NULL,
  appl_name_kartu varchar(20) DEFAULT NULL,
  appl_address_rw varchar(100) DEFAULT NULL,
  appl_address_rt varchar(100) DEFAULT NULL,
  appl_address1 varchar(200) DEFAULT NULL,
  appl_address2 varchar(200) DEFAULT NULL,
  appl_city int(11) DEFAULT NULL,
  appl_postcode varchar(5) DEFAULT NULL,
  appl_kelurahan varchar(200) DEFAULT NULL,
  appl_kecamatan varchar(200) DEFAULT NULL,
  appl_state int(11) DEFAULT NULL,
  appl_province int(11) DEFAULT NULL COMMENT 'Country',
  appl_email varchar(100) NOT NULL,
  appl_username varchar(100) DEFAULT NULL COMMENT 'later nanti nak pakai just create aje',
  appl_password varchar(100) NOT NULL,
  appl_dob varchar(100) NOT NULL,
  appl_birth_place varchar(200) NOT NULL,
  appl_gender tinyint(1) NOT NULL COMMENT '1:  male 2: female',
  appl_prefer_lang int(11) NOT NULL DEFAULT '2' COMMENT 'fk applicant_languange(al_id)',
  appl_nationality int(11) NOT NULL DEFAULT '1',
  appl_religion tinyint(4) DEFAULT NULL,
  appl_marital_status int(11) DEFAULT NULL,
  appl_no_of_child int(11) DEFAULT NULL,
  appl_jacket_size int(11) DEFAULT NULL,
  appl_phone_home int(13) DEFAULT NULL,
  appl_phone_hp varchar(13) DEFAULT NULL,
  appl_caddress_rt varchar(100) DEFAULT NULL,
  appl_caddress1 varchar(200) DEFAULT NULL COMMENT 'correspondence add',
  appl_caddress2 varchar(200) DEFAULT NULL,
  appl_caddress_rw varchar(10) DEFAULT NULL,
  appl_ccity int(11) DEFAULT NULL,
  appl_cstate int(11) DEFAULT NULL,
  appl_ckelurahan varchar(200) DEFAULT NULL,
  appl_ckecamatan varchar(200) DEFAULT NULL,
  appl_cpostcode int(11) DEFAULT NULL,
  appl_cprovince int(11) DEFAULT NULL,
  change_passwordby int(11) NOT NULL,
  change_passworddt datetime NOT NULL,
  appl_role int(11) DEFAULT NULL COMMENT '0:Applicant  1:Student',
  migrate_date datetime NOT NULL,
  migrate_by int(11) NOT NULL,
  burekol_verify_date datetime DEFAULT NULL,
  burekol_verify_by bigint(20) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE student_registrationid_seqno (
  srs_id int(11) NOT NULL AUTO_INCREMENT,
  srs_program_code varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  srs_award int(11) NOT NULL,
  srs_intake int(11) NOT NULL,
  srs_semester int(11) DEFAULT NULL,
  srs_branch int(11) DEFAULT NULL,
  srs_intake_sequence int(11) DEFAULT NULL,
  srs_seqno int(11) NOT NULL,
  srs_updateby int(11) NOT NULL,
  srs_updatedt datetime NOT NULL,
  PRIMARY KEY (srs_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE subjectre (
  a varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  b varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  c varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  d varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE subjectxre (
  a varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  b varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  c varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  d varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE suspend_detail (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  sm_id bigint(20) NOT NULL,
  item_description text COLLATE utf8_unicode_ci NOT NULL,
  item_amount decimal(20,2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE suspend_main (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  billing_no varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  appl_id bigint(20) DEFAULT NULL,
  `payment _description` text COLLATE utf8_unicode_ci NOT NULL,
  amount decimal(20,2) NOT NULL,
  payment_mode varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  transaction_reference bigint(20) DEFAULT NULL COMMENT 'pk payment_bank_record_detail',
  payment_date datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `TABLE 427` (
  `COL 1` varchar(78) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `TABLE 428` (
  NIM_FE varchar(9) DEFAULT NULL,
  Noform varchar(10) DEFAULT NULL,
  Registrationid varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `COL 4` varchar(32) DEFAULT NULL,
  `COL 5` varchar(20) DEFAULT NULL,
  `COL 6` varchar(20) DEFAULT NULL,
  `COL 7` varchar(10) DEFAULT NULL,
  `COL 8` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE tbl_academicprogress_subjects (
  IdAcademicProgressSub bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudent bigint(20) DEFAULT NULL COMMENT 'FK to tbl_studentregistration',
  IdAcademicProgress bigint(20) NOT NULL COMMENT 'FK to tbl_academeic_progress',
  Year_Level_Block int(11) DEFAULT NULL COMMENT 'field to hold the year/level/block ',
  Semester int(11) DEFAULT NULL COMMENT 'field to hold semester',
  IdCourseType varchar(100) DEFAULT NULL,
  CourseID varchar(20) DEFAULT NULL,
  CourseDescription varchar(100) DEFAULT NULL,
  CreditHours double DEFAULT NULL,
  Grade varchar(10) DEFAULT NULL,
  IsRegistered binary(1) DEFAULT NULL COMMENT '0: Not registered, 1:registered',
  UpdUser bigint(20) DEFAULT NULL COMMENT 'FK to user table',
  updDate datetime DEFAULT NULL,
  PRIMARY KEY (IdAcademicProgressSub)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_academicstatus (
  IdAcademicStatus bigint(20) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) unsigned DEFAULT NULL,
  IdSemester bigint(20) unsigned NOT NULL,
  SemesterCode varchar(75) DEFAULT NULL,
  IdAward bigint(20) DEFAULT NULL,
  IdScheme bigint(20) DEFAULT NULL,
  AcademicStatus bigint(20) unsigned NOT NULL COMMENT '0:GPA 1:CGPA',
  BasedOn varchar(20) NOT NULL,
  Minimum decimal(10,2) DEFAULT NULL,
  Maximum decimal(10,2) DEFAULT NULL,
  MinimumTotalMarks decimal(10,2) DEFAULT NULL,
  MaximumTotalMarks decimal(10,2) DEFAULT NULL,
  StatusEnglishName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  StatusArabicName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ClassHonorship varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  Active tinytext NOT NULL,
  PRIMARY KEY (IdAcademicStatus),
  KEY IdProgram (IdProgram),
  KEY IdSemester (IdSemester),
  KEY UpdUser (UpdUser),
  KEY IdSemester_2 (IdSemester),
  KEY IdAcademicStatus (IdAcademicStatus),
  KEY IdProgram_2 (IdProgram),
  KEY IdSemester_3 (IdSemester),
  KEY IdAcademicStatus_2 (IdAcademicStatus),
  KEY IdProgram_3 (IdProgram),
  KEY IdSemester_4 (IdSemester),
  KEY IdScheme (IdScheme)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_academicstatus_details (
  IdAcademicStatusDetail bigint(20) NOT NULL AUTO_INCREMENT,
  IdAcademicStatus bigint(20) NOT NULL,
  Minimum decimal(10,2) NOT NULL,
  Maximum decimal(10,2) NOT NULL,
  Gradepoint decimal(10,2) DEFAULT NULL COMMENT 'x pakai',
  Gradevalue varchar(20) DEFAULT NULL COMMENT 'x pakai',
  StatusEnglishName varchar(100) NOT NULL,
  StatusArabicName varchar(100) DEFAULT NULL,
  PRIMARY KEY (IdAcademicStatusDetail),
  KEY IdAcademicStatusDetail (IdAcademicStatusDetail),
  KEY IdAcademicStatus (IdAcademicStatus),
  KEY IdAcademicStatusDetail_2 (IdAcademicStatusDetail),
  KEY IdAcademicStatus_2 (IdAcademicStatus)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_academic_period (
  ap_id int(11) NOT NULL AUTO_INCREMENT,
  ap_intake_id int(11) DEFAULT NULL COMMENT 'fk_tbl_academic_period (IdIntake)',
  ap_month varchar(10) DEFAULT NULL,
  ap_year varchar(10) DEFAULT NULL,
  ap_code varchar(10) NOT NULL,
  ap_desc varchar(50) NOT NULL,
  ap_number int(11) DEFAULT NULL COMMENT '1-10 sahaja, refer jadual penerimaan calon',
  PRIMARY KEY (ap_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='period untuk intake (application)';

CREATE TABLE tbl_academic_period_20130102 (
  ap_id int(11) NOT NULL AUTO_INCREMENT,
  ap_intake_id int(11) DEFAULT NULL,
  ap_month varchar(10) DEFAULT NULL,
  ap_year varchar(10) DEFAULT NULL,
  ap_code varchar(10) NOT NULL,
  ap_desc varchar(50) NOT NULL,
  PRIMARY KEY (ap_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_academic_progress (
  IdAcademicProgress bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudent bigint(20) DEFAULT NULL COMMENT 'FK to tbl_studentregistration',
  IdScheme bigint(20) DEFAULT NULL COMMENT 'FK to scheme table',
  IdFaculty bigint(20) DEFAULT NULL COMMENT 'FK to faculty table',
  IdProgram bigint(20) DEFAULT NULL COMMENT 'FK to tbl_program',
  IdIntake bigint(20) DEFAULT NULL COMMENT 'FK to tbl_intake',
  IdBranch bigint(20) DEFAULT NULL COMMENT 'FK to branch table',
  ProfileStatus bigint(20) DEFAULT NULL COMMENT 'FK to maintenance table',
  LandscapeType int(11) DEFAULT NULL COMMENT '1: block, 2:level, 3:semester',
  UpdUser bigint(20) DEFAULT NULL COMMENT 'FK to user table',
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdAcademicProgress)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_academic_year (
  ay_id int(11) NOT NULL AUTO_INCREMENT,
  ay_code varchar(10) NOT NULL,
  ay_desc varchar(50) NOT NULL,
  ay_start_date date NOT NULL COMMENT 'tarikh besar utk semester',
  ay_end_date date NOT NULL COMMENT 'tarikh besar utk semester',
  ay_year_status varchar(10) NOT NULL COMMENT 'PAST, CURRENT, NEXT, FUTURE',
  PRIMARY KEY (ay_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='tahun yang student belajar';

CREATE TABLE tbl_academic_year_20130102 (
  ay_id int(11) NOT NULL AUTO_INCREMENT,
  ay_code varchar(10) NOT NULL,
  ay_desc varchar(50) NOT NULL,
  ay_year_status varchar(10) NOT NULL COMMENT 'PAST, CURRENT, NEXT',
  PRIMARY KEY (ay_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_accountgroup (
  IdAccountGroup bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  GroupName varchar(100) NOT NULL,
  GroupCode varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Description varchar(200) NOT NULL,
  IdParent bigint(20) unsigned DEFAULT NULL,
  Active binary(1) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdAccountGroup),
  KEY UpdUser (UpdUser),
  KEY IdParent (IdParent)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_accountmaster (
  idAccount bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  AccountName varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  AccShortName varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PrefixCode varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  AccountType tinyint(1) NOT NULL DEFAULT '0' COMMENT '0- Debit , 1- Credit',
  Description varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  Period bigint(20) NOT NULL,
  Revenue tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - yes , 0 - no',
  AccountCreatedDt datetime NOT NULL,
  AccountCreatedBy bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user ',
  IdGroup bigint(20) DEFAULT '0' COMMENT 'Not Physical FK to tbl_Accountmaster ',
  BillingModule bigint(20) DEFAULT NULL COMMENT 'FK to tbl_definisionms',
  duringRegistration tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-if during registration',
  AdvancePayment tinyint(1) NOT NULL DEFAULT '0',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  Active binary(1) DEFAULT NULL COMMENT '0:No, 1:Yes',
  AccountCode varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  Deposit binary(1) NOT NULL DEFAULT '0',
  Refund binary(1) NOT NULL DEFAULT '0',
  duringProcessing tinyint(1) DEFAULT '0' COMMENT '0-No,1-Yes',
  PRIMARY KEY (idAccount),
  KEY FK_tbl_AccountMaster_AccountCreatedBy (AccountCreatedBy),
  KEY FK_tbl_AccountMaster_UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_activity (
  idActivity int(11) NOT NULL AUTO_INCREMENT,
  ActivityName varchar(100) NOT NULL,
  ActivityColorCode varchar(7) NOT NULL COMMENT 'HTML Color Code',
  UpdDate int(11) NOT NULL,
  UpdUser int(1) NOT NULL,
  PRIMARY KEY (idActivity)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_activity_calender (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL COMMENT '1:semester 2:period',
  IdSemester int(10) DEFAULT NULL,
  IdSemesterMain int(10) DEFAULT NULL,
  IdPeriod int(11) DEFAULT NULL,
  IdActivity int(10) NOT NULL,
  StartDate date NOT NULL,
  EndDate date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_address (
  add_id int(11) NOT NULL AUTO_INCREMENT,
  add_org_name varchar(30) NOT NULL COMMENT 'eg. branch,faculty etc utk mudah programmer simpan nama table',
  add_org_id int(11) NOT NULL COMMENT 'organization fk id ',
  add_address_type int(4) NOT NULL COMMENT 'fk tbl_defination (98)',
  add_country int(11) NOT NULL COMMENT 'fk tbl_country',
  add_state int(11) NOT NULL COMMENT 'fk_tbl_state',
  add_city int(11) DEFAULT NULL,
  add_address1 text NOT NULL,
  add_address2 text,
  add_zipcode varchar(20) NOT NULL,
  add_phone varchar(20) NOT NULL,
  add_phone2 varchar(20) DEFAULT NULL,
  add_fax varchar(20) DEFAULT NULL,
  add_email varchar(200) DEFAULT NULL,
  add_affiliate int(11) NOT NULL DEFAULT '0',
  add_createddt datetime NOT NULL,
  add_createdby int(11) NOT NULL,
  add_modifyby int(11) DEFAULT NULL,
  add_modifydt datetime DEFAULT NULL,
  PRIMARY KEY (add_id),
  KEY add_org_name (add_org_name),
  KEY add_org_id (add_org_id),
  KEY add_org_name_2 (add_org_name),
  KEY add_org_id_2 (add_org_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_address_history (
  addh_id int(11) NOT NULL AUTO_INCREMENT,
  add_id int(11) NOT NULL COMMENT 'fk tbl_address',
  addh_org_name varchar(30) NOT NULL COMMENT 'eg. branch,faculty etc utk mudah programmer simpan nama table',
  addh_org_id int(11) NOT NULL COMMENT 'organization fk id ',
  addh_address_type int(4) NOT NULL COMMENT 'fk tbl_defination (98)',
  addh_country int(11) NOT NULL COMMENT 'fk tbl_country',
  addh_state int(11) NOT NULL COMMENT 'fk_tbl_state',
  addh_city int(11) DEFAULT '0',
  addh_address1 text NOT NULL,
  addh_address2 text,
  addh_zipcode varchar(20) NOT NULL,
  addh_phone varchar(20) NOT NULL,
  addh_phone2 varchar(20) DEFAULT NULL,
  addh_fax varchar(20) DEFAULT NULL,
  addh_email varchar(200) DEFAULT NULL,
  addh_affiliate int(11) NOT NULL DEFAULT '0',
  addh_createddt datetime NOT NULL,
  addh_createdby int(11) NOT NULL,
  addh_changedt datetime NOT NULL,
  addh_changeby int(11) NOT NULL,
  PRIMARY KEY (addh_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_agentapplicanttagging (
  IdAgentApplicantTag bigint(20) NOT NULL AUTO_INCREMENT,
  IdAgent varchar(50) NOT NULL,
  InchargeImperson bigint(20) NOT NULL,
  IdStudent varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  AssignedOn date NOT NULL,
  AssignedBy bigint(20) NOT NULL,
  PRIMARY KEY (IdAgentApplicantTag)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_agentmaster (
  IdAgentMaster bigint(20) NOT NULL AUTO_INCREMENT,
  AgentType bigint(20) NOT NULL,
  AgentCode varchar(50) NOT NULL,
  AgentName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentName1 text,
  AgentAdd1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  AgentAdd2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  City bigint(20) unsigned NOT NULL,
  State bigint(20) unsigned NOT NULL,
  Country bigint(20) unsigned NOT NULL,
  Phone varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Fax varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  URL varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ContactPerson varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Desgination varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ContactPhone varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ContactCell varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL,
  EffectiveDate datetime NOT NULL,
  AgentField1 varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField2 varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField3 varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField4 varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField5 varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  passwd varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (IdAgentMaster),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_agentpaymentdetails (
  IdPayment bigint(20) NOT NULL AUTO_INCREMENT,
  AgentType text NOT NULL,
  Intake bigint(20) NOT NULL,
  Amount varchar(50) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdPayment)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_agentprogram (
  IdAgentProgram bigint(20) NOT NULL AUTO_INCREMENT,
  IdAgentMaster bigint(20) unsigned NOT NULL,
  IdProgram bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdAgentProgram),
  KEY IdAgentMaster (IdAgentMaster),
  KEY IdProgram (IdProgram)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_announcement (
  sl_id int(11) NOT NULL AUTO_INCREMENT,
  sl_var_name varchar(100) NOT NULL,
  sl_english text NOT NULL,
  sl_bahasa text NOT NULL,
  PRIMARY KEY (sl_id),
  UNIQUE KEY sl_var_name (sl_var_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_appeal (
  IdAppeal bigint(20) NOT NULL AUTO_INCREMENT,
  IdRegistration bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentregistration',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentappication',
  IdMarksDistributionMaster bigint(20) unsigned NOT NULL,
  IdMarksDistributionDetails bigint(20) unsigned NOT NULL,
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'tbl_subject',
  Idverifiermarks bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_verifiermarks',
  AppealCode varchar(50) NOT NULL,
  MarksObtained int(10) unsigned NOT NULL,
  MarksExpected int(10) unsigned NOT NULL,
  Comments varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate date NOT NULL,
  AppealStatus binary(1) NOT NULL,
  PRIMARY KEY (IdAppeal)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_appealmarksentry (
  IdAppealEntry bigint(20) NOT NULL AUTO_INCREMENT,
  IdAppeal bigint(20) NOT NULL,
  NewMarks decimal(12,2) NOT NULL,
  DateOfRemarks date NOT NULL,
  RemarkedBy bigint(20) NOT NULL,
  PRIMARY KEY (IdAppealEntry),
  KEY IdAppeal (IdAppeal)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_appealmarks_config (
  IdAppealMarksConfig bigint(20) NOT NULL AUTO_INCREMENT,
  IdUniversity bigint(20) DEFAULT NULL,
  MaxAppeal tinyint(5) DEFAULT NULL COMMENT 'Min is 1',
  ChooseMarks bigint(20) DEFAULT NULL COMMENT 'FK to tbl_definationms : 85',
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  SemesterCode varchar(100) DEFAULT NULL COMMENT 'Semestercode',
  PRIMARY KEY (IdAppealMarksConfig)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_applicant (
  IdApplicant bigint(20) NOT NULL AUTO_INCREMENT,
  Emaillogin varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  ApplicantCode varchar(100) NOT NULL,
  RegisteredDate datetime NOT NULL,
  tab_value tinyint(4) DEFAULT '1' COMMENT '1:Ppart, 2:Pprog, 3:Qua, 4:Decl, 5:Admsn',
  PRIMARY KEY (IdApplicant)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_applicant_personal_detail (
  IdApplicantPersonalDetail bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplicant bigint(20) NOT NULL,
  ApplicantCode varchar(100) NOT NULL,
  FName varchar(50) NOT NULL,
  MName varchar(50) DEFAULT NULL,
  LName varchar(50) NOT NULL,
  defaultlangname varchar(50) DEFAULT NULL,
  NameField1 varchar(50) NOT NULL,
  NameField2 varchar(50) NOT NULL,
  NameField3 varchar(50) NOT NULL,
  NameField4 varchar(50) NOT NULL,
  NameField5 varchar(50) NOT NULL,
  PPNos varchar(50) NOT NULL,
  PPIssueDt date DEFAULT NULL,
  PPExpDt date DEFAULT NULL,
  PPField1 varchar(50) DEFAULT NULL,
  PPField2 varchar(50) DEFAULT NULL,
  PPField3 varchar(50) DEFAULT NULL,
  PPField4 varchar(50) DEFAULT NULL,
  PPField5 varchar(50) DEFAULT NULL,
  VisaDetailField1 varchar(20) NOT NULL,
  VisaDetailField2 varchar(20) NOT NULL,
  VisaDetailField3 varchar(20) NOT NULL,
  VisaDetailField4 varchar(20) NOT NULL,
  VisaDetailField5 varchar(20) NOT NULL,
  ExtraIdField1 varchar(50) NOT NULL,
  ExtraIdField2 varchar(50) NOT NULL,
  ExtraIdField3 varchar(50) NOT NULL,
  ExtraIdField4 varchar(50) NOT NULL,
  ExtraIdField5 varchar(50) NOT NULL,
  ExtraIdField6 varchar(50) NOT NULL,
  ExtraIdField7 varchar(50) NOT NULL,
  ExtraIdField8 varchar(50) NOT NULL,
  ExtraIdField9 varchar(50) NOT NULL,
  ExtraIdField10 varchar(50) NOT NULL,
  ExtraIdField11 varchar(50) NOT NULL,
  ExtraIdField12 varchar(50) NOT NULL,
  ExtraIdField13 varchar(50) NOT NULL,
  ExtraIdField14 varchar(50) NOT NULL,
  ExtraIdField15 varchar(50) NOT NULL,
  ExtraIdField16 varchar(50) NOT NULL,
  ExtraIdField17 varchar(50) NOT NULL,
  ExtraIdField18 varchar(50) NOT NULL,
  ExtraIdField19 varchar(50) NOT NULL,
  ExtraIdField20 varchar(50) NOT NULL,
  Nationality bigint(20) NOT NULL,
  Gender binary(1) NOT NULL,
  HomeNumber varchar(100) NOT NULL,
  OfficeNumber varchar(50) NOT NULL,
  MobileNumber varchar(50) NOT NULL,
  email varchar(100) NOT NULL,
  MaritalStatus int(10) DEFAULT NULL,
  CorrespondenceAdd varchar(100) DEFAULT NULL,
  CorrespondenceAdd2 varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  CorrespondenceCountry bigint(11) DEFAULT NULL,
  CorrespondenceProvince bigint(11) DEFAULT NULL,
  CorrespondenceCity bigint(11) DEFAULT NULL,
  CorrespondenceZip varchar(20) DEFAULT NULL,
  IdProgram bigint(20) NOT NULL,
  `status` bigint(20) NOT NULL,
  DOB date DEFAULT NULL,
  ApplicantCodeFormat varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (IdApplicantPersonalDetail)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_applicant_preffered (
  IdApplicationPreferred bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplicant bigint(20) NOT NULL,
  IdIntake bigint(20) NOT NULL,
  IdProgramLevel bigint(20) NOT NULL,
  IdPriorityNo bigint(20) NOT NULL,
  IdProgram bigint(20) NOT NULL,
  IdBranch bigint(20) NOT NULL,
  IdScheme bigint(20) NOT NULL,
  PRIMARY KEY (IdApplicationPreferred)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_applicant_qualification (
  IdApplicationQualification bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplicant bigint(20) NOT NULL,
  IdPlaceObtained bigint(20) NOT NULL,
  yearobtained varchar(50) NOT NULL,
  IdEducationalLevel bigint(20) NOT NULL,
  certificatename varchar(100) NOT NULL,
  IdInstitute bigint(20) DEFAULT NULL,
  otherinstitute varchar(50) DEFAULT NULL,
  instituteaddress1 varchar(100) NOT NULL,
  instituteaddress2 varchar(100) NOT NULL,
  country bigint(20) NOT NULL,
  state bigint(20) NOT NULL,
  city bigint(20) NOT NULL,
  zipcode varchar(50) NOT NULL,
  phone varchar(50) NOT NULL,
  phonecountrycode varchar(50) NOT NULL,
  phonestatecode varchar(50) NOT NULL,
  fax varchar(50) NOT NULL,
  faxcountrycode varchar(50) NOT NULL,
  faxstatecode varchar(50) NOT NULL,
  IdResultItem bigint(20) NOT NULL,
  resulttotal varchar(50) NOT NULL,
  PRIMARY KEY (IdApplicationQualification)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_applicant_subject_detail (
  IdApplicationSubjectDetail bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplicationQualification bigint(20) NOT NULL,
  IdSubject bigint(20) NOT NULL,
  IdSubjectGrade bigint(20) NOT NULL,
  PRIMARY KEY (IdApplicationSubjectDetail)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_application_config (
  IdAppConfig bigint(20) NOT NULL AUTO_INCREMENT,
  RevertProcessMigration binary(1) NOT NULL,
  RevertProcessApproval binary(1) NOT NULL,
  AgentAppointment binary(1) NOT NULL,
  ApplicationValidity varchar(10) NOT NULL,
  ApplicationVal int(11) NOT NULL,
  IdUniversity bigint(20) NOT NULL,
  NoofPreferred int(20) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdAppConfig)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_application_history (
  IdApplicationHistory bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  PRIMARY KEY (IdApplicationHistory)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_application_status_message (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdAppConfig bigint(20) NOT NULL,
  ApplicationStatus bigint(20) NOT NULL,
  Message varchar(300) NOT NULL,
  Email varchar(500) NOT NULL,
  Description varchar(500) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_autocredittransfer (
  IdAutoCredit bigint(20) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) NOT NULL,
  IdInstitution bigint(20) NOT NULL,
  IdQualification bigint(20) NOT NULL,
  IdSpecialization bigint(20) NOT NULL,
  MaxValueNextProm int(11) NOT NULL,
  YearLimit int(11) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdAutoCredit)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_autocredittransferdetails (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdAutoCredit bigint(20) NOT NULL,
  IdSubject bigint(20) NOT NULL,
  MinGradPoints int(11) NOT NULL,
  Remarks varchar(100) DEFAULT NULL,
  IdCourse bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_awardlevel (
  IdAllowance bigint(20) NOT NULL AUTO_INCREMENT,
  IdLevel bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdAllowanceLevel bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdAllowance),
  KEY IdLevel (IdLevel),
  KEY IdAllowanceLevel (IdAllowanceLevel),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_bank (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  IdBank bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  BankName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  BankAdd1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  BankAdd2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  State varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Country bigint(20) DEFAULT NULL,
  Phone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Email varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  URL varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ContactPerson varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  Desgination varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ContactPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ContactCell varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL,
  MICR varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BKCode1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BKCode2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Branch varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (IdBank)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_bankdetails (
  IdAccount bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdBank bigint(20) unsigned NOT NULL,
  AccountType bigint(20) unsigned NOT NULL,
  AccountNumber varchar(20) NOT NULL,
  AccountingHeadName varchar(100) NOT NULL,
  Description varchar(200) NOT NULL,
  Active binary(1) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdAccount),
  KEY IdBank (IdBank),
  KEY UpdUser (UpdUser),
  KEY AccountType (AccountType)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_billprogram (
  idBillProgram bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idBillStructureMaster bigint(20) unsigned NOT NULL,
  idProgram bigint(20) unsigned NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (idBillProgram)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_billstructuredetails (
  idBillStructureDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idBillStructureMaster bigint(20) unsigned NOT NULL,
  idVoucherType bigint(20) unsigned NOT NULL,
  itemDescription varchar(255) DEFAULT NULL,
  idCalculationMethod bigint(20) unsigned NOT NULL COMMENT 'tbl_definitionms(monthly,yearly...)',
  Amount float(20,3) NOT NULL,
  idDebitAccount bigint(20) unsigned NOT NULL,
  idCreditAccount bigint(20) unsigned NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (idBillStructureDetails)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_billstructuremaster (
  idBillStructureMaster bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  BillMasterCode varchar(50) DEFAULT NULL,
  IdIntakeFrom bigint(20) unsigned NOT NULL COMMENT 'tbl_semestermaster',
  IdIntakeTo bigint(20) unsigned NOT NULL COMMENT 'tbl_semestermaster',
  IdCategory bigint(20) unsigned NOT NULL COMMENT 'tbl_definitionms',
  effectiveDate date NOT NULL,
  Description varchar(255) DEFAULT '',
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (idBillStructureMaster)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_blocksemesteryear (
  IdLandscapetempblockyearsemester bigint(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(11) NOT NULL,
  `Year` bigint(11) NOT NULL,
  YearSemester bigint(11) NOT NULL,
  PRIMARY KEY (IdLandscapetempblockyearsemester)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_branchofficevenue (
  IdBranch bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  BranchName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ShortName text,
  BranchCode varchar(30) DEFAULT NULL,
  Branch int(11) NOT NULL DEFAULT '0',
  Arabic varchar(150) DEFAULT NULL,
  Addr1 varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Addr2 varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  idCountry bigint(20) unsigned DEFAULT NULL,
  idState bigint(20) unsigned DEFAULT NULL,
  Email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Phone varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  zipCode varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AffiliatedTo int(11) DEFAULT NULL,
  IdType bigint(20) unsigned NOT NULL DEFAULT '0',
  IdLink bigint(20) unsigned NOT NULL DEFAULT '0',
  IdLinkType bigint(20) unsigned NOT NULL DEFAULT '0',
  Active binary(1) NOT NULL,
  StartDate date DEFAULT NULL,
  EndDate date DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdBranch),
  KEY UpdUser (UpdUser),
  KEY idCountry (idCountry),
  KEY idState (idState)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_branchregistrationmap (
  IdRegistration int(4) NOT NULL AUTO_INCREMENT,
  IdBranch bigint(20) unsigned NOT NULL,
  RegistrationLoc int(50) NOT NULL,
  Programme int(11) NOT NULL,
  PRIMARY KEY (IdRegistration),
  KEY br_fk_rl (IdBranch)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_cgpacalculation (
  IdCGPAcalculation bigint(20) NOT NULL AUTO_INCREMENT,
  IdRegistration bigint(20) unsigned NOT NULL,
  Cgpa decimal(10,2) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  IdApplication bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdCGPAcalculation)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_change_program_application (
  IdChangeProgramApplication bigint(20) NOT NULL AUTO_INCREMENT,
  IdCPApplication varchar(100) NOT NULL,
  IdStudent bigint(20) NOT NULL,
  Reason varchar(255) NOT NULL,
  ApplicationDate date NOT NULL,
  AppliedBy bigint(20) NOT NULL,
  ApplicationStatus bigint(20) NOT NULL,
  CurrentProgram bigint(20) NOT NULL,
  NewProgram bigint(20) NOT NULL,
  EffectiveSemester varchar(60) NOT NULL,
  Remarks varchar(255) NOT NULL,
  ApprovedBy bigint(20) NOT NULL,
  ApprovedDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdChangeProgramApplication)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_change_program_course_transferred (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdChangeProgram bigint(20) NOT NULL,
  IdSemesterMain bigint(20) NOT NULL,
  IdSemesterDetail bigint(20) NOT NULL,
  IdCourse bigint(20) NOT NULL,
  Grade varchar(10) NOT NULL,
  `Type` bigint(20) NOT NULL,
  Upduser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_change_status_application (
  IdChangeStatusApplication bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) NOT NULL,
  CSApplicationCode varchar(30) NOT NULL,
  Reason bigint(20) NOT NULL,
  IdEffectiveSemester varchar(100) NOT NULL,
  IdApplyingFor bigint(20) NOT NULL,
  ApplicationStatus bigint(20) NOT NULL,
  ApplicationDate date NOT NULL,
  AppliedBy bigint(20) NOT NULL,
  Remarks varchar(100) NOT NULL,
  ApprovedBy bigint(20) NOT NULL,
  DateApproved date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdChangeStatusApplication)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_charges (
  IdCharges bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdAccountMaster bigint(20) unsigned NOT NULL,
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_program',
  effectiveDate date NOT NULL,
  Rate decimal(12,2) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  Active binary(1) NOT NULL,
  PRIMARY KEY (IdCharges),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_chiefofprogramlist (
  IdChiefOfProgramList bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  FromDate date NOT NULL,
  ToDate date DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdChiefOfProgramList),
  KEY tbl_registrarlist_ibfk_1 (UpdUser),
  KEY tbl_registrarlist_ibfk_2 (IdProgram),
  KEY tbl_registrarlist_ibfk_3 (IdStaff)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_city (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  idCity bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  CityCode varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  idState bigint(20) unsigned NOT NULL,
  CityName varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL,
  PRIMARY KEY (idCity),
  KEY idState (idState),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_city_dummy (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  idCity bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  CityCode varchar(201) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  idState bigint(20) unsigned NOT NULL,
  CityName varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL,
  PRIMARY KEY (idCity),
  KEY idState (idState),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_collegemaster (
  IdCollege bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  CollegeName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ShortName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  CollegeCode varchar(75) NOT NULL,
  ArabicName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  IsDepartment binary(1) NOT NULL DEFAULT '0' COMMENT '0: not department  1: Department',
  Add1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Add2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  State bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_state',
  Country bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_countries',
  Zip varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  AffiliatedTo bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_universitymaster',
  Email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Phone1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Phone2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  CollegeType binary(1) NOT NULL COMMENT '(0:Branch of university, 1:Independent college)',
  Idhead int(11) DEFAULT NULL,
  Vicedean1 bigint(20) unsigned DEFAULT NULL,
  Vicedean2 bigint(20) unsigned DEFAULT NULL,
  Vicedean3 bigint(20) unsigned DEFAULT NULL,
  Vicedean4 bigint(20) unsigned DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  StartDate date DEFAULT NULL,
  EndDate date DEFAULT NULL,
  PRIMARY KEY (IdCollege),
  KEY FK_tbl_collegemaster_State (State),
  KEY FK_tbl_collegemaster_Country (Country),
  KEY FK_tbl_collegemaster_AffiliatedTo (AffiliatedTo),
  KEY FK_tbl_collegemaster_UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_collegemasterbak (
  IdCollege bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  CollegeName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ShortName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  CollegeCode varchar(75) NOT NULL,
  ArabicName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  IsDepartment binary(1) NOT NULL DEFAULT '0' COMMENT '0: not department  1: Department',
  Add1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Add2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  State bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_state',
  Country bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_countries',
  Zip varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  AffiliatedTo bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_universitymaster',
  Email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Phone1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Phone2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  CollegeType binary(1) NOT NULL COMMENT '(0:Branch of university, 1:Independent college)',
  Idhead int(11) NOT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdCollege),
  KEY FK_tbl_collegemaster_State (State),
  KEY FK_tbl_collegemaster_Country (Country),
  KEY FK_tbl_collegemaster_AffiliatedTo (AffiliatedTo),
  KEY FK_tbl_collegemaster_UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_config (
  UpdDate datetime NOT NULL,
  Landscape binary(1) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  idUniversity bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_universutymaster',
  idConfig bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  noofrowsingrid int(11) NOT NULL,
  DefaultCountry bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_country',
  DefaultDropDownLanguage bigint(20) NOT NULL COMMENT '0:English,1:Bahana Indonesia',
  PPField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField5 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField5 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SMTPServer varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  SMTPUsername varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  SMTPPassword varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  SMTPPort tinyint(5) DEFAULT NULL,
  `SSL` binary(1) DEFAULT NULL,
  DefaultEmail varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  ApplicantCodeType tinyint(1) NOT NULL DEFAULT '0',
  ApplicantIdFormat varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ApplicantPrefix varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ResetApplicantSeq varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationCodeType tinyint(4) NOT NULL DEFAULT '0',
  BillNoType tinyint(4) NOT NULL,
  BillNoTypeSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  BillNoTypeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BillNoTypeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BillNoTypeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BillNoTypeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  StudentIdType tinyint(4) NOT NULL,
  StudentIdPrefix varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  StudentIdFormat varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ResetStudentIdSeq varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CourseIdType tinyint(4) NOT NULL,
  CoursePrefix varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CourseIdFormat varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ResetCourseSeq tinyint(4) NOT NULL,
  AgentIdType tinyint(4) NOT NULL,
  AgentIdFormat varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  AgentPrefix varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ResetAgentSeq varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ReceiptType tinyint(4) NOT NULL,
  ReceiptTypeSeparator varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  ReceiptTypeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ReceiptTypeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ReceiptTypeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ReceiptTypeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationIdFormat varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  RegistrationPrefix varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ResetRegistrationSeq varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  RegistrationCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationCodeText1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationCodeText2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationCodeText3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegistrationCodeText4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCode binary(1) NOT NULL DEFAULT '0',
  base binary(1) NOT NULL DEFAULT '0',
  levelbase binary(1) NOT NULL DEFAULT '0',
  FirstletterSeparator varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  FixedSeparator varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  FixedText varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  GeneralbaseSeparator varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseField1 varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseField2 varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseField3 varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseField4 varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseText1 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseText2 varchar(255) CHARACTER SET utf8 COLLATE utf8_turkish_ci DEFAULT NULL,
  generalbaseText3 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseText4 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  ItemCode binary(1) NOT NULL DEFAULT '0',
  Itembase binary(1) NOT NULL DEFAULT '0',
  itemlevelbase binary(1) NOT NULL DEFAULT '0',
  FirstletteritemSeparator varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  FixeditemSeparator varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  FixeditemText varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  GeneralbaseitemSeparator varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemField1 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemField2 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemField3 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemField4 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemText1 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemText2 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemText3 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  generalbaseitemText4 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeType tinyint(1) NOT NULL DEFAULT '0',
  SemesterSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeText1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeText2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeText3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SemesterCodeText4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectCodeType tinyint(4) NOT NULL DEFAULT '0',
  SubjectIdFormat varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectPrefix varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ResetSubjectSeq varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  DepartmentCodeType tinyint(1) NOT NULL DEFAULT '0',
  DepartmentSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  DepartmentCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  DepartmentCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  DepartmentCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  DepartmentCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CollegeCodeType tinyint(1) NOT NULL DEFAULT '0',
  CollegeSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  CollegeCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CollegeCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CollegeCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeType tinyint(1) NOT NULL DEFAULT '0',
  AccountSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeText1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeText2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeText3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccountCodeText4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  InvoiceSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  InvoiceCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  InvoiceCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  InvoiceCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  InvoiceCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AppealCodeType tinyint(1) NOT NULL DEFAULT '0',
  AppealCodeSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  AppealCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AppealCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AppealCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AppealCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  StaffIdType tinyint(4) NOT NULL DEFAULT '0',
  StaffIdFormat varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  StaffPrefix varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ResetStaffSeq varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CollegeAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CourseAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  DepartmentAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  DeanAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RegisterAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ProgramAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BranchAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  LandscapeAliasName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` int(5) DEFAULT NULL,
  AccDtlCount int(11) DEFAULT '0',
  AccDtl1 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl2 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl3 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl4 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl5 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl6 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl7 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl8 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl9 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl10 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl11 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl12 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl13 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl14 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl15 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl16 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl17 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl18 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl19 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl20 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtlCount int(11) DEFAULT '0',
  MoheDtl1 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl2 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl3 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl4 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl5 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl6 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl7 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl8 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl9 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl10 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl11 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl12 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl13 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl14 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl15 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl16 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl17 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl18 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl19 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl20 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  LocalStudent binary(1) NOT NULL DEFAULT '0',
  BlockType binary(1) NOT NULL DEFAULT '0' COMMENT '0-Manual, 1 - Auto',
  BlockName varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BlockSeparator varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  NoRefCode tinyint(5) unsigned NOT NULL,
  RefCodeField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RefCodeField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RefCodeField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RefCodeField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  BKCode1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  BKCode2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  AgentField5 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  InternationalPlacementTest tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  InternationalCertification tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  InternationalAndOr tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  LocalPlacementTest tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  LocalCertification tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  LocalAndOr tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  InternationalPlcamenetTestProcessingFee binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  InternationalCertificationProcessingFee binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  LocalPlcamenetTestProcessingFee binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  LocalCertificationProcessingFee binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  Completionofyears varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  HijriDate int(5) DEFAULT NULL,
  HijriDateOptions varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  NoofWarnings tinyint(3) DEFAULT '0' COMMENT 'No of Maximum warnings (Disciplinary Action)',
  AddDrop binary(1) NOT NULL,
  MarksAppeal smallint(5) NOT NULL DEFAULT '0' COMMENT '0:Take Latest,1:Take Highest',
  TakeMarks smallint(5) NOT NULL DEFAULT '0' COMMENT '0:Marks by average;1:Marks by rank',
  marksdistributiontype int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0: course(subject) based 1: lecture based',
  Failedsubjects binary(1) NOT NULL DEFAULT '0' COMMENT '0:push failed subjects 1: ignore failed subjects',
  Branch binary(1) NOT NULL,
  Office binary(1) NOT NULL,
  Venue binary(1) NOT NULL,
  Scheme binary(1) NOT NULL,
  NameDtlCount int(11) NOT NULL,
  NameDtl1 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  NameDtl2 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  NameDtl3 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  NameDtl4 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  NameDtl5 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtlCount int(10) NOT NULL,
  ExtarIdDtl1 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl2 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl3 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl4 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl5 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl6 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl7 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl8 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl9 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl10 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl11 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl12 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl13 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl14 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl15 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl16 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl17 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl18 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl19 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ExtarIdDtl20 varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  inventoryIdType tinyint(4) NOT NULL,
  inventoryPrefix varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  inventoryIdFormat varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (idConfig)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_countries (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  idCountry bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  CountryName varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  DefaultLanguage int(11) DEFAULT NULL,
  CountryCode int(10) NOT NULL DEFAULT '123',
  Alias varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  CountryIso char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  CountryISO3 char(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) DEFAULT NULL COMMENT '1:Active, 0:Inactive',
  CurrencyShortName varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  CurrencySymbol varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  CurrencyName varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (idCountry)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Country and their ISO Formats';

CREATE TABLE tbl_coursemaster (
  IdCoursemaster bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  CourseName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ArabicName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdCoursemaster),
  KEY tbl_coursemaster_ibfk_1 (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_coursetype (
  IdCourseType bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  CourseType varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Bahasaindonesia varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  MandatoryCreditHrs binary(1) NOT NULL COMMENT '1:Yes,0:No',
  MandatoryAmount binary(1) NOT NULL COMMENT '1:Yes,0:No',
  CourseExam binary(1) NOT NULL COMMENT '1:Yes,0:No',
  ExamTimeTable bigint(20) NOT NULL,
  Practical bigint(20) NOT NULL,
  PassFailGrade bigint(20) NOT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  Audit binary(1) NOT NULL COMMENT '1:Yes;0:No',
  Project binary(1) NOT NULL COMMENT '1:Yes;0:No',
  ClassTimetable binary(1) NOT NULL COMMENT '1:Yes,0:No',
  PRIMARY KEY (IdCourseType),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_coursetypedetails (
  IdCourseTypeDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdCourseType bigint(20) unsigned NOT NULL,
  ComponentName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Description varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  deleteFlag binary(1) NOT NULL DEFAULT '0',
  Active binary(1) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  KEY IdCourseTypeDetails (IdCourseTypeDetails),
  KEY IdCourseType (IdCourseType)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_course_group_student_mapping (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdCourseTaggingGroup bigint(20) NOT NULL,
  IdStudent bigint(20) NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_course_tagging_group (
  IdCourseTaggingGroup int(11) NOT NULL AUTO_INCREMENT,
  IdLecturer bigint(20) NOT NULL,
  IdSemester int(11) NOT NULL COMMENT 'fk tbl_semestermaster',
  SemCode varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  IdSubject bigint(20) NOT NULL,
  GroupName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  GroupCode varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  IdUniversity bigint(20) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdCourseTaggingGroup)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_credittransfer (
  IdCreditTransfer bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdStudentRegistration bigint(20) DEFAULT NULL,
  IdCTApplication varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentapplication',
  IdProgram bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_program',
  `Status` binary(1) DEFAULT NULL COMMENT '1:Approved,0:Rejected',
  ApplicationStatus bigint(20) DEFAULT NULL,
  DateApplied datetime DEFAULT NULL,
  DateApproved datetime DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  Comments varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (IdCreditTransfer),
  KEY UpdUser (UpdUser),
  KEY IdProgram (IdProgram),
  KEY IdApplication (IdApplication)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_credittransfersubjects (
  IdCreditTransferSubjects bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdCreditTransfer bigint(20) unsigned DEFAULT NULL,
  IdSubject bigint(20) unsigned DEFAULT NULL,
  IdSemester bigint(20) DEFAULT NULL,
  IdSemesterMain bigint(20) DEFAULT NULL,
  IdCourse bigint(20) DEFAULT NULL,
  CreditHours int(3) DEFAULT NULL,
  creditStatus tinyint(5) DEFAULT NULL COMMENT '1:approve, 0: disapprove',
  ApplicationDate date DEFAULT NULL,
  AppliedBy bigint(1) DEFAULT NULL,
  ApprovedDate date DEFAULT NULL,
  Approvedby bigint(20) DEFAULT NULL,
  ApplicationStatus bigint(20) DEFAULT NULL,
  IdInstitution bigint(20) DEFAULT NULL,
  IdQualification bigint(20) DEFAULT NULL,
  IdSpecialization bigint(20) DEFAULT NULL,
  EquivalentCourse bigint(20) DEFAULT NULL,
  EquivalentGrade int(20) DEFAULT NULL,
  EquivalentCreditHours int(3) DEFAULT NULL,
  Marks bigint(20) DEFAULT NULL,
  Comments varchar(30) DEFAULT NULL,
  disapprovalComment varchar(255) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdCreditTransferSubjects),
  KEY IdCreditTransfer (IdCreditTransfer),
  KEY IdSubject (IdSubject)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_credittransferuploads (
  IdCreditTransferSubjectsDocs bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdCreditTransferSubjects bigint(20) unsigned NOT NULL,
  FileLocation varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  FileName varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UploadedFilename varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  FileSize varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  MIMEType varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  Comments varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  DocumentsVerified binary(1) NOT NULL DEFAULT '0',
  DocumentsApproved binary(1) NOT NULL DEFAULT '0',
  CreditTransferComments varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  documentcategory bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (IdCreditTransferSubjectsDocs),
  KEY IdCreditTransferSubjects (IdCreditTransferSubjects)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_deanlist (
  IdDeanList bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdCollege bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster',
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  FromDate date NOT NULL,
  ToDate date DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdDeanList),
  KEY tbl_deanlist_ibfk_1 (UpdUser),
  KEY tbl_deanlist_ibfk_2 (IdCollege),
  KEY tbl_deanlist_ibfk_3 (IdStaff)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_definationms (
  idDefinition bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idDefType int(10) unsigned NOT NULL,
  DefinitionCode varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  DefinitionDesc varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `Status` binary(1) NOT NULL COMMENT '0: Active , 1 : InActive',
  BahasaIndonesia varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  Description varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  BhasaIndonesia varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (idDefinition),
  KEY FK_tbl_DefinationMS_1 (idDefType),
  KEY FK_idDefType (idDefType)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Detail Data Defination';

CREATE TABLE tbl_definationtypems (
  idDefType int(10) unsigned NOT NULL AUTO_INCREMENT,
  defTypeDesc varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '1',
  BahasaIndonesia varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  Description varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  DefinitionTypeDesc varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (idDefType)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Keep Master Data Defination';

CREATE TABLE tbl_departmentmaster (
  IdDepartment bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  DepartmentType tinyint(1) NOT NULL COMMENT '0:Dept under College, 1:Dept under branch',
  IdCollege bigint(20) unsigned NOT NULL,
  DepartmentName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ArabicName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ShortName varchar(20) DEFAULT NULL,
  DeptCode varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Active tinyint(1) NOT NULL COMMENT '0:Inactive,1:Active',
  StartDate date DEFAULT NULL,
  EndDate date DEFAULT NULL,
  Addr1 varchar(200) DEFAULT NULL,
  Addr2 varchar(200) DEFAULT NULL,
  idState int(11) DEFAULT NULL,
  idCountry int(11) DEFAULT NULL,
  Email varchar(20) DEFAULT NULL,
  Phone varchar(20) DEFAULT NULL,
  AffiliatedTo int(11) DEFAULT NULL,
  zipCode varchar(20) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdDepartment),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_departmentmaster_bak (
  IdDepartment bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  DepartmentType binary(1) NOT NULL COMMENT '0:Dept under College, 1:Dept under branch',
  IdCollege bigint(20) unsigned NOT NULL,
  DepartmentName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ArabicName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ShortName varchar(20) DEFAULT NULL,
  DeptCode varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL COMMENT '0:Inactive,1:Active',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdDepartment),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_disciplinaryaction (
  IdDisciplinaryAction bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'FK To Student Application',
  IdDispciplinaryActionType bigint(20) unsigned NOT NULL COMMENT 'FK To Definition MS( Disciplinary Actions)',
  DateOfMistake date NOT NULL,
  ReportingDate date NOT NULL,
  DetailedNarration varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PenaltyDescription varchar(205) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PenaltyAmount decimal(12,2) NOT NULL,
  WarningIssued binary(1) NOT NULL,
  SemesterId bigint(20) unsigned NOT NULL,
  UploadNotice varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  Approve binary(1) NOT NULL,
  PRIMARY KEY (IdDisciplinaryAction),
  KEY IdApplication (IdApplication),
  KEY IdDispciplinaryActionType (IdDispciplinaryActionType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_disciplinaryactiondetails (
  iddisciplinayactiondetails bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdDisciplinaryAction bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_disciplinaryaction',
  idSubject bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_subject',
  idSemester bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_semester',
  DisplinaryAction bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_definitionms',
  deleteFlag binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (iddisciplinayactiondetails)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_disciplinaryactionmailsentto (
  IdDisciplinaryActionMailSentTo bigint(20) NOT NULL AUTO_INCREMENT,
  IdDisciplinaryAction bigint(20) NOT NULL COMMENT 'FK to tbl_disciplinaryaction',
  DiscMsgCollegeHead binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  DiscMsgParent binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  DiscMsgSponor binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  DiscMsgRegistrar binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdDisciplinaryActionMailSentTo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_disciplinaryactionmaster (
  idDisciplinaryActionMaster bigint(20) NOT NULL AUTO_INCREMENT,
  idDisciplinaryActionType bigint(20) NOT NULL,
  DiscMsgCollegeHead tinyint(1) DEFAULT '0',
  DiscMsgParent tinyint(1) DEFAULT '0',
  DiscMsgSponor tinyint(1) DEFAULT '0',
  DiscMsgRegistar tinyint(1) DEFAULT '0',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (idDisciplinaryActionMaster)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_documentdetails (
  idDocumentDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentapplication',
  FileLocation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  FileName varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  UploadedFilename varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Uploaded file name with the uploaded datetime attached to the filename',
  FileSize varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  MIMEType varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  UpdUser bigint(20) unsigned DEFAULT NULL,
  Comments varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  documentcategory bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (idDocumentDetails),
  KEY UpdUser (UpdUser),
  KEY IdApplication (IdApplication)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_education_subject (
  IdEducationSubject bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudEduDtl bigint(20) NOT NULL,
  IdSubject bigint(20) NOT NULL,
  IdSubjectGrade bigint(20) NOT NULL,
  PRIMARY KEY (IdEducationSubject)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_emailtemplate (
  idTemplate bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  TemplateName varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  TemplateFrom varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  TemplateFromDesc varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  TemplateSubject varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  TemplateHeader varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  TemplateBody varchar(7277) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  TemplateFooter varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Deleteflag binary(1) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  idDefinition bigint(20) unsigned NOT NULL,
  PRIMARY KEY (idTemplate),
  KEY FK_tbl_emailtemplate_1 (idDefinition),
  KEY FK_tbl_emailtemplate_2 (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_examination_assessment_item (
  IdExaminationAssessmentType bigint(20) NOT NULL AUTO_INCREMENT,
  IdDescription varchar(50) NOT NULL,
  Description varchar(255) NOT NULL,
  DescriptionDefaultlang varchar(255) DEFAULT NULL,
  UpdDate int(11) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdExaminationAssessmentType)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_examination_assessment_type (
  IdExaminationAssessmentType bigint(20) NOT NULL AUTO_INCREMENT,
  IdDescription varchar(50) NOT NULL,
  Description varchar(255) NOT NULL,
  DescriptionDefaultlang varchar(255) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdExaminationAssessmentType)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_examination_resit_configuration (
  IdExaminationResitConfiguration bigint(20) NOT NULL AUTO_INCREMENT,
  ApplicationCount int(10) NOT NULL,
  MinimumGrade bigint(20) NOT NULL COMMENT 'IdGradeSetUp from table tbl_gradesetup',
  FinalMarks bigint(20) NOT NULL COMMENT 'tbl_defination - ID 284 and 285',
  IdUniversity bigint(20) NOT NULL,
  UpdatedOn datetime NOT NULL,
  UpdatedBy bigint(20) NOT NULL,
  PRIMARY KEY (IdExaminationResitConfiguration)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE tbl_feesetupdetail (
  IdFeesSetupDetail bigint(20) NOT NULL AUTO_INCREMENT,
  IdFeessetupMaster bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_feessetupmaster',
  Description varchar(50) DEFAULT NULL,
  DebitAccount int(100) NOT NULL,
  CreditAccount int(100) NOT NULL,
  Amount float(10,2) NOT NULL,
  `Mode` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  PRIMARY KEY (IdFeesSetupDetail),
  KEY IdFeessetupMaster (IdFeessetupMaster),
  KEY `Mode` (`Mode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_feesetupmaster (
  IdFeesSetupMaster bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  VoucherNo varchar(40) NOT NULL,
  StartingIntake bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  EndSemester bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  IdCategory bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdPlantype bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_plantype',
  Description varchar(100) DEFAULT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK  to tbl_user',
  Active int(11) NOT NULL,
  PRIMARY KEY (IdFeesSetupMaster),
  KEY StartingIntake (StartingIntake),
  KEY EndSemester (EndSemester),
  KEY IdCategory (IdCategory),
  KEY IdPlantype (IdPlantype),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_gpacalculation (
  Idgpacalculation bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) unsigned NOT NULL,
  Gpa decimal(10,2) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  IdApplication bigint(20) unsigned NOT NULL,
  PRIMARY KEY (Idgpacalculation)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TRIGGER IF EXISTS `gpa_after_insert`;
DELIMITER //
CREATE TRIGGER `gpa_after_insert` AFTER INSERT ON `tbl_gpacalculation`
 FOR EACH ROW BEGIN   
   
    DECLARE cgpaamt float(10,2); 
    DECLARE iDSTUDREGISTRATION int; 

  	

 	SET cgpaamt = (SELECT AVG(Gpa) FROM `tbl_gpacalculation` WHERE tbl_gpacalculation.IdStudentRegistration=NEW.IdStudentRegistration GROUP BY NEW.IdStudentRegistration) ;   
     
        INSERT INTO tbl_cgpacalculation (IdRegistration,IdApplication,Cgpa,UpdDate,UpdUser) VALUES (NEW.IdStudentRegistration,NEW.	IdApplication,cgpaamt,NEW.UpdDate,NEW.UpdUser);  

    END
//
DELIMITER ;

CREATE TABLE tbl_gpastudents (
  IdGpaStudents bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) unsigned NOT NULL,
  IdSemester bigint(20) unsigned NOT NULL,
  AcademicStatus bigint(20) unsigned NOT NULL,
  Marks int(11) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdGpaStudents)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_gpa_calculation_mode (
  IdGpaCalculationmode bigint(20) NOT NULL AUTO_INCREMENT,
  IdAcademicStatus bigint(20) NOT NULL,
  LandscapeType bigint(20) NOT NULL,
  `Level` int(10) NOT NULL,
  Semester int(10) NOT NULL,
  PRIMARY KEY (IdGpaCalculationmode)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_grade (
  IdStudentGrade bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudent bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentregistration',
  SemesterNumber bigint(20) unsigned NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdStudentGrade)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_gradesetup (
  IdGradeSetUp bigint(20) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_program',
  IdGradeSetUpMain bigint(20) NOT NULL COMMENT 'fk to tbl_gradesetup_main',
  IdSubject bigint(20) unsigned DEFAULT NULL,
  IdSemester bigint(20) unsigned DEFAULT NULL,
  IdScheme bigint(20) DEFAULT NULL,
  IdAward bigint(20) DEFAULT NULL,
  BasedOn binary(1) DEFAULT NULL,
  EffectiveDate date DEFAULT NULL,
  GradeName varchar(100) DEFAULT NULL,
  GradeDesc varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  GradePoint decimal(10,2) NOT NULL,
  MinPoint decimal(10,2) NOT NULL,
  MaxPoint decimal(10,2) NOT NULL,
  Rank bigint(20) unsigned NOT NULL,
  Pass varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  DescEnglishName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  DescArabicName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  DefaultLanguage varchar(100) DEFAULT NULL,
  Grade bigint(20) DEFAULT NULL,
  deleteFlag binary(1) DEFAULT '0',
  Active binary(1) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdGradeSetUp),
  KEY IdProgram (IdProgram),
  KEY IdGradeSetUp (IdGradeSetUp),
  KEY IdGradeSetUpMain (IdGradeSetUpMain),
  KEY IdSubject (IdSubject),
  KEY IdSemester (IdSemester)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_gradesetup_main (
  IdGradeSetUpMain bigint(20) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) DEFAULT NULL,
  IdSubject bigint(20) DEFAULT NULL,
  IdSemester bigint(11) DEFAULT NULL,
  IdScheme bigint(20) DEFAULT NULL,
  IdAward bigint(20) DEFAULT NULL,
  BasedOn tinyint(5) DEFAULT NULL,
  Active binary(1) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  PRIMARY KEY (IdGradeSetUpMain),
  KEY IdGradeSetUpMain (IdGradeSetUpMain),
  KEY IdProgram (IdProgram),
  KEY IdSubject (IdSubject),
  KEY IdSemester (IdSemester)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_group_subject (
  IdGroupSubject bigint(20) NOT NULL AUTO_INCREMENT,
  IdSubject bigint(20) NOT NULL,
  GroupId bigint(20) unsigned NOT NULL,
  ProgramEntry bigint(20) NOT NULL,
  IdQualification int(20) unsigned NOT NULL,
  fieldofstudy int(20) unsigned NOT NULL,
  MinGradePoint int(20) NOT NULL,
  NoofSubject int(20) NOT NULL,
  PRIMARY KEY (IdGroupSubject)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_headofdepartmentlist (
  IdHeadOfDepartmentList bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdDepartment bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  FromDate date NOT NULL,
  ToDate date DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdHeadOfDepartmentList),
  KEY tbl_headofdepartmentlist_ibfk_1 (UpdUser),
  KEY tbl_headofdepartmentlist_ibfk_2 (IdDepartment),
  KEY tbl_headofdepartmentlist_ibfk_3 (IdStaff)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_headofdepartmentlist_bak (
  IdHeadOfDepartmentList bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdDepartment bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  FromDate date NOT NULL,
  ToDate date DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdHeadOfDepartmentList),
  KEY tbl_headofdepartmentlist_ibfk_1 (UpdUser),
  KEY tbl_headofdepartmentlist_ibfk_2 (IdDepartment),
  KEY tbl_headofdepartmentlist_ibfk_3 (IdStaff)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_block (
  IdHostelBlock bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL,
  ShortName varchar(255) DEFAULT NULL,
  DefaultLang varchar(255) DEFAULT NULL,
  Address1 varchar(255) DEFAULT NULL,
  Address2 varchar(255) DEFAULT NULL,
  City int(11) DEFAULT NULL,
  State int(11) DEFAULT NULL,
  Zipcode varchar(50) DEFAULT NULL,
  Country int(11) DEFAULT NULL,
  Phone varchar(255) DEFAULT NULL,
  Fax varchar(255) DEFAULT NULL,
  Active int(11) DEFAULT '1',
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdHostelBlock)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_config (
  Idhostelconfiguration bigint(20) NOT NULL AUTO_INCREMENT,
  IdUniversity varchar(20) NOT NULL,
  Applicableforapproval binary(1) DEFAULT '0',
  Applicableforrevert binary(1) DEFAULT '0',
  Applicableforeservice binary(1) DEFAULT '0',
  Applicableforextend binary(1) DEFAULT '0',
  Applicableforgenbill binary(1) DEFAULT '0',
  chargesRate int(11) DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (Idhostelconfiguration)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_history (
  IdHistory bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) DEFAULT NULL,
  IdHostelRoom bigint(20) DEFAULT NULL,
  ActivityDate datetime DEFAULT NULL,
  Activity varchar(100) DEFAULT NULL,
  checkinDate datetime DEFAULT NULL,
  OldValue varchar(100) DEFAULT NULL,
  NewValue varchar(100) DEFAULT NULL,
  Remarks varchar(255) DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdHistory)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_inventory (
  IdHostelInventory bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  ShortName varchar(100) DEFAULT NULL,
  DefaultLang varchar(255) DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdHostelInventory)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_item (
  IdHostelItem bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  ShortName varchar(100) DEFAULT NULL,
  DefaultLang varchar(255) DEFAULT NULL,
  Active binary(1) DEFAULT '1' COMMENT '1: Active, 0:Inactive',
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdHostelItem)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_item_register (
  IdHostelItemRegister bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) NOT NULL,
  IdHostelItem bigint(20) NOT NULL,
  Quantity int(20) NOT NULL,
  UpdUser bigint(11) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdHostelItemRegister)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_level (
  IdHostelLevel bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Code` varchar(100) NOT NULL,
  ShortName varchar(100) NOT NULL,
  DefaultLang varchar(100) NOT NULL,
  IdHostelBlock bigint(20) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdHostelLevel)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_registration (
  IdHostelregistration bigint(20) NOT NULL AUTO_INCREMENT,
  StudentStatus tinyint(4) DEFAULT NULL COMMENT '1:checkin,2:checkout,3:changeroom,4:outcampus,5:campuscancel',
  IdStudentRegistration bigint(20) DEFAULT NULL,
  SemesterCode varchar(50) DEFAULT NULL,
  IdHostelRoom bigint(20) DEFAULT NULL,
  CheckInDate datetime DEFAULT NULL,
  CheckOutDate datetime DEFAULT NULL,
  SeatNo int(10) DEFAULT NULL,
  RoomKeyTakenDate datetime DEFAULT NULL,
  RoomKeyReturnDate datetime DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdHostelregistration)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_reservation (
  IdHostelReservation bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration int(20) NOT NULL,
  IdHostelRoom bigint(20) NOT NULL,
  SeatNo int(10) DEFAULT NULL,
  Active int(1) NOT NULL,
  `Status` bigint(20) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdHostelReservation)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_reservation_history (
  IdHostelReservationHistory bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) NOT NULL COMMENT 'foreign key of student registration',
  IdHostelRoom bigint(20) NOT NULL COMMENT 'foreign key of hostel room table',
  Activity varchar(50) NOT NULL COMMENT 'name of activity as reserve room ,cancel rervation etc',
  IpAddress varchar(50) NOT NULL COMMENT 'IP address of machine by which any activity has done',
  `Date` datetime NOT NULL COMMENT 'On which date this activity has done',
  OldValue bigint(20) NOT NULL COMMENT 'old status of reservation application as approve, entry,reject',
  NewValue bigint(20) NOT NULL COMMENT 'new status of reservation application',
  Remarks varchar(255) DEFAULT NULL COMMENT 'Remarks, when canceling the reservation',
  Upduser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdHostelReservationHistory)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_reservation_setup (
  IdReservation bigint(20) NOT NULL AUTO_INCREMENT,
  SemesterCode varchar(50) DEFAULT NULL,
  Priority bigint(11) NOT NULL,
  ValueType bigint(11) NOT NULL,
  `Condition` bigint(11) NOT NULL,
  ValueId bigint(11) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(11) NOT NULL,
  PRIMARY KEY (IdReservation)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_room (
  IdHostelRoom bigint(20) NOT NULL AUTO_INCREMENT,
  RoomName varchar(100) NOT NULL,
  RoomCode varchar(50) NOT NULL,
  DefaultLang varchar(50) DEFAULT NULL,
  `Level` bigint(20) NOT NULL,
  `Block` bigint(20) NOT NULL,
  RoomType bigint(20) NOT NULL,
  Capacity int(11) NOT NULL,
  OccupiedCapacity int(10) NOT NULL DEFAULT '0',
  Active binary(1) NOT NULL,
  UpdUser bigint(11) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdHostelRoom)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_room_inventory (
  IdHostelRoomInventory bigint(20) NOT NULL AUTO_INCREMENT,
  IdHostelRoom bigint(20) NOT NULL,
  InventoryName bigint(20) NOT NULL,
  Quantity int(11) NOT NULL,
  UpdUser bigint(11) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdHostelRoomInventory)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_hostel_room_type (
  IdHostelRoomType bigint(20) NOT NULL AUTO_INCREMENT,
  RoomType varchar(50) NOT NULL,
  RoomCode varchar(50) NOT NULL,
  ShortName varchar(50) NOT NULL,
  DefaultLang varchar(50) DEFAULT NULL,
  Active binary(1) NOT NULL,
  UpdUser bigint(11) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdHostelRoomType)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_idstudgrade (
  IdStudGrade bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentGrade bigint(20) NOT NULL COMMENT 'FK to tbl_grade',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subject',
  IdGrade bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_gradesetup',
  PRIMARY KEY (IdStudGrade)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_institution (
  idInstitution int(11) NOT NULL AUTO_INCREMENT,
  InstitutionName varchar(150) NOT NULL,
  InstitutionCode varchar(50) NOT NULL,
  InstitutionAddress1 varchar(150) NOT NULL,
  InstitutionAddress2 varchar(150) NOT NULL,
  Country bigint(20) NOT NULL,
  State bigint(20) NOT NULL,
  City varchar(80) NOT NULL,
  Zip varchar(20) DEFAULT NULL,
  InstitutionPhoneNumber varchar(11) NOT NULL,
  InstitutionFaxNumber varchar(11) NOT NULL,
  Active int(10) NOT NULL DEFAULT '0',
  UpdDate int(11) NOT NULL,
  UpdUser int(11) NOT NULL,
  PRIMARY KEY (idInstitution)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_institution_subject_mapping (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdInstitution bigint(20) NOT NULL,
  IdSubject bigint(20) NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_intake (
  IdIntake bigint(20) NOT NULL AUTO_INCREMENT,
  IntakeId varchar(50) NOT NULL,
  IntakeDesc varchar(100) NOT NULL,
  IntakeDefaultLanguage varchar(100) NOT NULL,
  ApplicationStartDate date NOT NULL,
  ApplicationEndDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdIntake),
  UNIQUE KEY IntakeId (IntakeId)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='table head utk period (application)';

CREATE TABLE tbl_intake_20130102 (
  IdIntake bigint(20) NOT NULL AUTO_INCREMENT,
  IntakeId varchar(50) NOT NULL,
  IntakeDesc varchar(100) NOT NULL,
  IntakeDefaultLanguage varchar(100) NOT NULL,
  ApplicationStartDate date NOT NULL,
  ApplicationEndDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdIntake)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_intake_branch_mapping (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdIntake bigint(20) NOT NULL,
  IdProgram bigint(20) NOT NULL,
  IdBranch bigint(20) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_intake_program (
  ip_id int(11) NOT NULL AUTO_INCREMENT,
  IdIntake int(11) NOT NULL COMMENT 'fk tbl_intake',
  IdProgram int(11) NOT NULL COMMENT 'fk tbl_program',
  IdProgramScheme int(11) NOT NULL COMMENT 'fk tbl_program_scheme',
  IdStudentCategory int(11) NOT NULL COMMENT 'fk tbl_definations',
  ApplicationStartDate date NOT NULL,
  ApplicationEndDate date NOT NULL,
  Quota int(11) NOT NULL,
  createdby int(11) NOT NULL,
  createddt datetime NOT NULL,
  PRIMARY KEY (ip_id),
  UNIQUE KEY IdIntake (IdIntake,IdProgram,IdStudentCategory,ApplicationStartDate,ApplicationEndDate),
  KEY IdIntake_2 (IdIntake),
  KEY IdProgram (IdProgram)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_invoicedetails (
  IdInvoiceDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdInvoice bigint(20) unsigned NOT NULL,
  idAccount bigint(20) unsigned NOT NULL,
  Discount decimal(12,2) DEFAULT '0.00',
  Description varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Amount decimal(12,2) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  Active binary(1) DEFAULT NULL COMMENT '0:No, 1:Yes',
  PRIMARY KEY (IdInvoiceDetails)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_invoicemaster (
  IdInvoice bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStudent bigint(20) unsigned NOT NULL,
  IdSemester bigint(20) unsigned NOT NULL,
  InvoiceNo varchar(20) DEFAULT NULL,
  InvoiceDt date DEFAULT NULL,
  InvoiceAmt decimal(12,2) NOT NULL,
  AcdmcPeriod bigint(20) unsigned DEFAULT '0',
  Naration varchar(50) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  Active binary(1) DEFAULT NULL COMMENT '0:No, 1:Yes',
  Approved binary(1) NOT NULL DEFAULT '0',
  idsponsor bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (IdInvoice)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_invoicesubjectdetails (
  IdInvoicesubdetail bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdInvoice bigint(20) unsigned NOT NULL,
  Amount decimal(10,0) NOT NULL DEFAULT '0',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  IdSubject bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdInvoicesubdetail)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_invoicetermsandconditions (
  IdInvoiceTermAndCondition bigint(20) NOT NULL AUTO_INCREMENT,
  IdInvoice bigint(20) unsigned NOT NULL,
  idTermsandconditions bigint(20) NOT NULL,
  PRIMARY KEY (IdInvoiceTermAndCondition),
  KEY idTermsandconditions (idTermsandconditions),
  KEY IdInvoice (IdInvoice)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_itemgroup (
  IdItem bigint(20) NOT NULL AUTO_INCREMENT,
  ItemCode varchar(50) NOT NULL,
  EnglishDescription varchar(50) NOT NULL,
  BahasaDescription varchar(50) NOT NULL,
  Alias varchar(50) NOT NULL,
  ShortName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  IdBank bigint(20) unsigned NOT NULL,
  IdAccount bigint(20) unsigned NOT NULL,
  IdAccountGroup bigint(20) unsigned NOT NULL,
  Active binary(1) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdItem),
  KEY IdBank (IdBank),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscape (
  IdLandscape bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  LandscapeType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdStartSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_semester',
  SemsterCount int(5) NOT NULL DEFAULT '0',
  Blockcount int(11) NOT NULL DEFAULT '0',
  YearCount int(11) DEFAULT NULL,
  Active int(11) NOT NULL COMMENT '1: Active 0:Inactive',
  `Default` int(11) NOT NULL DEFAULT '0' COMMENT '1: Yes 0:No',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  TotalCreditHours decimal(10,2) DEFAULT NULL,
  Scheme bigint(20) unsigned NOT NULL,
  ProgramDescription varchar(50) DEFAULT NULL,
  landscapeDefaultLanguage varchar(150) DEFAULT NULL,
  AddDrop binary(1) DEFAULT NULL,
  modifyby int(11) NOT NULL,
  modifydt datetime NOT NULL,
  PRIMARY KEY (IdLandscape),
  KEY tbl_landscape_ibfk_1 (UpdUser),
  KEY tbl_landscape_ibfk_2 (IdStartSemester),
  KEY tbl_landscape_ibfk_3 (IdProgram),
  KEY tbl_landscape_ibfk_4 (LandscapeType),
  KEY Scheme (Scheme)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_landscapeblock (
  idblock bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idlandscape bigint(20) NOT NULL,
  `block` int(11) NOT NULL,
  blockname varchar(50) NOT NULL,
  semester int(11) NOT NULL COMMENT 'block semester level',
  CreditHours decimal(10,2) DEFAULT '0.00',
  createdby int(11) NOT NULL,
  createddt datetime NOT NULL,
  modifyby int(11) NOT NULL,
  modifydt datetime NOT NULL,
  UNIQUE KEY idblock (idblock)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscapeblocksemester (
  IdLandscapeblocksemester int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  semesterid bigint(20) unsigned NOT NULL,
  createddt datetime NOT NULL,
  createdby int(11) NOT NULL,
  PRIMARY KEY (IdLandscapeblocksemester)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscapeblocksemester_june (
  IdLandscapeblocksemester int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  semesterid bigint(20) unsigned NOT NULL,
  createddt datetime NOT NULL,
  createdby int(11) NOT NULL,
  PRIMARY KEY (IdLandscapeblocksemester)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscapeblocksubject (
  IdLandscapeblocksubject int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  subjectid bigint(20) unsigned NOT NULL,
  coursetypeid int(11) DEFAULT NULL,
  IdProgramReq int(11) NOT NULL COMMENT 'fk tbl_programrequirement',
  createddt datetime NOT NULL,
  createdby int(11) NOT NULL,
  PRIMARY KEY (IdLandscapeblocksubject)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscapeblocksubject_june (
  IdLandscapeblocksubject int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  subjectid bigint(20) unsigned NOT NULL,
  coursetypeid int(11) DEFAULT NULL,
  IdProgramReq int(11) NOT NULL COMMENT 'fk tbl_programrequirement',
  createddt datetime NOT NULL,
  createdby int(11) NOT NULL,
  PRIMARY KEY (IdLandscapeblocksubject)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscapeblock_june (
  idblock bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idlandscape bigint(20) NOT NULL,
  `block` int(11) NOT NULL,
  blockname varchar(50) NOT NULL,
  semester int(11) NOT NULL COMMENT 'block semester level',
  CreditHours decimal(10,2) DEFAULT '0.00',
  createdby int(11) NOT NULL,
  createddt datetime NOT NULL,
  modifyby int(11) NOT NULL,
  modifydt datetime NOT NULL,
  UNIQUE KEY idblock (idblock)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_landscapesubject (
  IdLandscapeSub bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  IdLandscape bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_landscape',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  SubjectType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  CreditHours float NOT NULL,
  IDProgramMajoring bigint(20) unsigned NOT NULL,
  IdProgramReq int(11) NOT NULL COMMENT 'fk tbl_programrequirement',
  Compulsory binary(1) NOT NULL DEFAULT '0' COMMENT '0: Not Compulsory, 1: Compulsory',
  Active int(11) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdLandscapeSub),
  KEY IdProgram (IdProgram),
  KEY IdLandscape (IdLandscape),
  KEY IdSubject (IdSubject),
  KEY SubjectType (SubjectType),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_landscapesubject_june (
  IdLandscapeSub bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  IdLandscape bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_landscape',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  SubjectType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  CreditHours float NOT NULL,
  IDProgramMajoring bigint(20) unsigned NOT NULL,
  IdProgramReq int(11) NOT NULL COMMENT 'fk tbl_programrequirement',
  Compulsory binary(1) NOT NULL DEFAULT '0' COMMENT '0: Not Compulsory, 1: Compulsory',
  Active int(11) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdLandscapeSub),
  KEY IdProgram (IdProgram),
  KEY IdLandscape (IdLandscape),
  KEY IdSubject (IdSubject),
  KEY SubjectType (SubjectType),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_landscape_june (
  IdLandscape bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  LandscapeType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdStartSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_semester',
  SemsterCount int(5) NOT NULL DEFAULT '0',
  Blockcount int(11) NOT NULL DEFAULT '0',
  YearCount int(11) DEFAULT NULL,
  Active int(11) NOT NULL COMMENT '1: Active 0:Inactive',
  `Default` int(11) NOT NULL DEFAULT '0' COMMENT '1: Yes 0:No',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  TotalCreditHours decimal(10,2) DEFAULT NULL,
  Scheme bigint(20) unsigned NOT NULL,
  ProgramDescription varchar(50) DEFAULT NULL,
  landscapeDefaultLanguage varchar(150) DEFAULT NULL,
  AddDrop binary(1) DEFAULT NULL,
  modifyby int(11) NOT NULL,
  modifydt datetime NOT NULL,
  PRIMARY KEY (IdLandscape),
  KEY tbl_landscape_ibfk_1 (UpdUser),
  KEY tbl_landscape_ibfk_2 (IdStartSemester),
  KEY tbl_landscape_ibfk_3 (IdProgram),
  KEY tbl_landscape_ibfk_4 (LandscapeType),
  KEY Scheme (Scheme)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_language (
  id int(11) NOT NULL AUTO_INCREMENT,
  system text CHARACTER SET latin1 NOT NULL,
  english text CHARACTER SET latin1,
  arabic text CHARACTER SET utf8,
  createdby int(11) NOT NULL,
  createddt datetime NOT NULL,
  `Language` int(5) NOT NULL,
  PRIMARY KEY (id),
  KEY `Language` (`Language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_learningmode (
  IdLearningMode bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  LearningModeName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Description varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdLearningMode),
  KEY tbl_learningmode_ibfk_1 (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_log (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  `level` int(11) NOT NULL,
  hostname varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  ip varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  message text CHARACTER SET latin1,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  Description varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_marksdistributiondetails (
  IdMarksDistributionDetails bigint(20) NOT NULL AUTO_INCREMENT,
  IdMarksDistributionMaster bigint(20) unsigned DEFAULT NULL,
  idStaff bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_staffmaster',
  idSemester bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_semester',
  IdComponentType bigint(20) DEFAULT NULL,
  IdComponentItem bigint(20) DEFAULT NULL,
  `Status` bigint(20) NOT NULL,
  ComponentName varchar(100) DEFAULT NULL,
  Weightage decimal(10,2) NOT NULL,
  PassMark decimal(10,2) DEFAULT NULL,
  TotalMark decimal(10,2) NOT NULL,
  Percentage decimal(10,2) NOT NULL,
  PassStatus bigint(20) unsigned DEFAULT NULL,
  deleteFlag binary(1) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdMarksDistributionDetails),
  KEY IdMarksDistributionMaster (IdMarksDistributionMaster),
  KEY idSemester (idSemester),
  KEY IdComponentType (IdComponentType),
  KEY IdMarksDistributionDetails (IdMarksDistributionDetails)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_marksdistributionmaster (
  IdMarksDistributionMaster bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  MarksApplicationCode varchar(50) DEFAULT NULL,
  `Name` varchar(50) NOT NULL,
  IdComponentType bigint(20) NOT NULL,
  IdComponentItem bigint(20) NOT NULL DEFAULT '0',
  IdScheme bigint(20) DEFAULT NULL,
  IdFaculty bigint(20) DEFAULT NULL,
  Marks decimal(10,2) NOT NULL,
  Percentage decimal(10,2) NOT NULL,
  TotalMark decimal(10,2) NOT NULL,
  IdProgram bigint(20) DEFAULT NULL,
  IdCourse bigint(20) NOT NULL,
  semester bigint(100) DEFAULT NULL,
  EffectiveDate date NOT NULL,
  `Status` bigint(20) NOT NULL,
  ApprovedBy bigint(20) DEFAULT NULL,
  ApprovedOn datetime DEFAULT NULL,
  RejectedBy bigint(20) DEFAULT NULL,
  RejectedOn datetime DEFAULT NULL,
  Remarks varchar(255) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdMarksDistributionMaster),
  KEY IdMarksDistributionMaster (IdMarksDistributionMaster),
  KEY IdProgram (IdProgram),
  KEY IdCourse (IdCourse),
  KEY IdScheme (IdScheme),
  KEY semester (semester)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_marksentrysetup (
  IdMarksEntrySetup bigint(20) NOT NULL AUTO_INCREMENT,
  IdSubject bigint(20) NOT NULL,
  IdStaff bigint(20) unsigned NOT NULL,
  Verification binary(1) NOT NULL,
  EffectiveDate date NOT NULL,
  Rank int(11) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  Active binary(1) NOT NULL,
  PRIMARY KEY (IdMarksEntrySetup)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_module_accessibility (
  ma_id int(11) NOT NULL AUTO_INCREMENT,
  ma_role_id int(11) NOT NULL,
  ma_start_date date NOT NULL,
  ma_end_date date DEFAULT NULL,
  ma_faculty_id int(11) DEFAULT NULL,
  ma_program_id int(11) DEFAULT NULL,
  ma_branch_id int(11) DEFAULT NULL,
  ma_course_id int(11) DEFAULT NULL,
  ma_createddt datetime NOT NULL,
  ma_createdby int(11) NOT NULL,
  PRIMARY KEY (ma_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_nav_menu (
  id int(11) NOT NULL AUTO_INCREMENT,
  role_id int(11) NOT NULL,
  label varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  title varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  module varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  controller varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  seq_order int(11) NOT NULL DEFAULT '0',
  visible int(11) NOT NULL DEFAULT '1' COMMENT '1:show 0:hide',
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_nav_sidebar (
  id int(11) NOT NULL AUTO_INCREMENT,
  nav_menu_id int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  module varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  controller varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  title_head tinyint(4) NOT NULL DEFAULT '0',
  seq_order int(11) NOT NULL DEFAULT '0',
  visible tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_oldlandsacpesemester (
  IdOldLandscapesemester bigint(20) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) NOT NULL,
  semesterid int(11) NOT NULL,
  PRIMARY KEY (IdOldLandscapesemester)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_paymentvarification (
  idPaymentVarification bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL,
  PaymentRecived tinyint(1) DEFAULT '0' COMMENT '1-yes,0-No',
  FullPayment tinyint(1) DEFAULT '0' COMMENT '1-yes,0-No',
  OtherDocumentVarified tinyint(1) DEFAULT '0' COMMENT '1-yes,0-No',
  Comments varchar(255) DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (idPaymentVarification),
  KEY IdApplication (IdApplication),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_placementtest (
  IdPlacementTest bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  PlacementTestName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PlacementTestDate date NOT NULL,
  Addr1 text NOT NULL,
  Addr2 text NOT NULL,
  idCountry bigint(20) NOT NULL,
  idState bigint(20) NOT NULL,
  City bigint(20) NOT NULL,
  Zip bigint(20) NOT NULL,
  Phone varchar(20) NOT NULL,
  PlacementTestTime time NOT NULL,
  MarksBasedOn binary(1) DEFAULT NULL COMMENT '0: Total marks 1: Individual marks',
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  MinMark bigint(20) DEFAULT NULL,
  TotalMark bigint(20) DEFAULT NULL,
  PassingMark bigint(20) DEFAULT NULL,
  PRIMARY KEY (IdPlacementTest),
  KEY tbl_placementtest_ibfk_1 (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_placementtestcomponent (
  IdPlacementTestComponent bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdPlacementTest bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_placementtest',
  ComponentName varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ComponentWeightage int(5) NOT NULL,
  ComponentTotalMarks int(5) NOT NULL,
  MinimumMark int(5) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdPlacementTestComponent),
  KEY tbl_placementtestcomponent_ibfk_1 (UpdUser),
  KEY tbl_placementtestcomponent_ibfk_2 (IdPlacementTest)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_placementtestmarks (
  IdPlacementTestMarks bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdPlacementTest bigint(20) NOT NULL,
  IdPlacementTestComponent bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_placementtestcomponent',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_studentapplication',
  Marks decimal(4,2) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdPlacementTestMarks),
  KEY tbl_placementtestmarks_ibfk_3 (IdPlacementTestComponent),
  KEY tbl_placementtestmarks_ibfk_1 (UpdUser),
  KEY tbl_placementtestmarks_ibfk_2 (IdApplication)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_placementtestprogram (
  IdPlacementTestProgram bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdPlacementTest bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_placementtest',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdPlacementTestProgram),
  KEY tbl_placementtestbranch_ibfk_1 (UpdUser),
  KEY tbl_placementtestbranch_ibfk_2 (IdProgram),
  KEY tbl_placementtestbranch_ibfk_3 (IdPlacementTest)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_plantype (
  IdPlantype bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  Planname varchar(45) NOT NULL,
  Description varchar(50) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  Active binary(1) NOT NULL,
  PRIMARY KEY (IdPlantype)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_program (
  IdProgram bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  ProgramName varchar(100) NOT NULL,
  ProgramCode varchar(10) NOT NULL,
  ShortName varchar(20) DEFAULT NULL,
  Duration int(5) NOT NULL,
  OptimalDuration int(5) NOT NULL DEFAULT '0',
  TotalCreditHours decimal(12,2) NOT NULL DEFAULT '0.00',
  ArabicName varchar(100) DEFAULT NULL,
  Award bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  PssbOffer tinyint(1) NOT NULL DEFAULT '1',
  UsmOffer tinyint(1) NOT NULL DEFAULT '1',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PlacementTestType bigint(20) NOT NULL COMMENT 'FK to tbl_definitionms',
  MinimumAge bigint(20) NOT NULL,
  AgentVerification binary(1) NOT NULL DEFAULT '0' COMMENT '1:Yes;0:No',
  SelectColgDept1 bigint(20) unsigned NOT NULL,
  IdCollege1 bigint(20) unsigned NOT NULL,
  IdScheme bigint(20) NOT NULL,
  programSalutation int(11) NOT NULL,
  SelectColgDept tinyint(4) NOT NULL,
  IdCollege int(11) NOT NULL,
  ptest_room_type varchar(10) NOT NULL,
  Estimate_Fee_R1 decimal(20,2) DEFAULT NULL,
  Estimate_Fee_R2 decimal(20,2) DEFAULT NULL,
  Estimate_Fee_R3 decimal(20,2) DEFAULT NULL,
  Program_code_EPSBED varchar(6) NOT NULL,
  Strata_code_EPSBED char(1) NOT NULL,
  PRIMARY KEY (IdProgram),
  UNIQUE KEY ProgramCode (ProgramCode),
  KEY tbl_program_ibfk_1 (UpdUser),
  KEY tbl_program_ibfk_4 (Award),
  KEY IdCollege (IdCollege),
  KEY ptest_room_type (ptest_room_type),
  KEY IdScheme (IdScheme),
  KEY PlacementTestType (PlacementTestType),
  KEY SelectColgDept (SelectColgDept)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_program2 (
  IdProgram bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  ProgramName varchar(100) NOT NULL,
  ProgramCode varchar(10) NOT NULL,
  ShortName varchar(20) DEFAULT NULL,
  Duration int(5) NOT NULL,
  OptimalDuration int(5) NOT NULL DEFAULT '0',
  TotalCreditHours decimal(12,2) NOT NULL DEFAULT '0.00',
  ArabicName varchar(100) DEFAULT NULL,
  Award bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PlacementTestType bigint(20) NOT NULL COMMENT 'FK to tbl_definitionms',
  MinimumAge bigint(20) NOT NULL,
  AgentVerification binary(1) NOT NULL DEFAULT '0' COMMENT '1:Yes;0:No',
  SelectColgDept1 bigint(20) unsigned NOT NULL,
  IdCollege1 bigint(20) unsigned NOT NULL,
  IdScheme bigint(20) NOT NULL,
  programSalutation int(11) NOT NULL,
  SelectColgDept tinyint(4) NOT NULL,
  IdCollege int(11) NOT NULL,
  ptest_room_type varchar(10) NOT NULL,
  PRIMARY KEY (IdProgram),
  KEY tbl_program_ibfk_1 (UpdUser),
  KEY tbl_program_ibfk_4 (Award)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_programaccreditiondetails (
  IdProgramAccreditionDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  AccredictionDate date DEFAULT NULL,
  AccredictionReferences varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl1 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl2 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl3 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl4 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl5 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl6 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl7 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl8 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl9 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl10 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl11 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl12 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl13 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl14 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl15 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl16 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl17 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl18 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl19 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl20 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccreditionType bigint(20) NOT NULL COMMENT 'fk to tbl_definitonms',
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdProgramAccreditionDetails),
  KEY IdProgram (IdProgram),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_programbranchlink (
  IdProgramBranchLink bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdCollege bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  MaxQuota int(11) NOT NULL DEFAULT '0',
  Quota int(5) NOT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdProgramBranchLink),
  KEY tbl_programbranchlink_ibfk_1 (UpdUser),
  KEY tbl_programbranchlink_ibfk_3 (IdProgram),
  KEY IdCollege (IdCollege)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_programchangebackup (
  IdProgramcChangebackup bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL,
  IdProgram bigint(20) unsigned NOT NULL,
  IdProgramOld bigint(20) unsigned NOT NULL,
  EffectiveDate date DEFAULT NULL,
  Description varchar(250) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  Approved tinyint(1) DEFAULT '0' COMMENT '0-No,1-Yes',
  Comments varchar(250) DEFAULT NULL,
  PRIMARY KEY (IdProgramcChangebackup)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programcharges (
  IdProgramCharges bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_program',
  IdCharges bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_charges',
  Charges decimal(12,2) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdProgramCharges),
  KEY IdProgram (IdProgram),
  KEY IdCharges (IdCharges),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_programchecklist (
  IdCheckList int(10) unsigned NOT NULL AUTO_INCREMENT,
  CheckListName varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Description varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Priority bigint(20) NOT NULL DEFAULT '0',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_program',
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  UpdDate datetime NOT NULL,
  Active binary(1) NOT NULL,
  Mandatory binary(1) NOT NULL DEFAULT '1',
  AddPlacementTest binary(1) NOT NULL,
  PRIMARY KEY (IdCheckList),
  KEY UpdUser (UpdUser),
  KEY IdProgram (IdProgram)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programentry (
  IdProgramEntry bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  IdScheme bigint(20) unsigned NOT NULL,
  MinAge tinyint(3) DEFAULT NULL,
  InternationalPlacementTest tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  InternationalCertification tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  InternationalAndOr tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  LocalPlacementTest tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  LocalCertification tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  LocalAndOr tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  Autoct binary(1) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdProgramEntry),
  KEY tbl_programentry_ibfk_1 (UpdUser),
  KEY tbl_programentry_ibfk_3 (IdProgram)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_programentrylearningmode (
  Idprogramentrylearningmode bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdProgramEntry bigint(20) unsigned NOT NULL,
  LearningMode bigint(20) unsigned NOT NULL,
  PRIMARY KEY (Idprogramentrylearningmode),
  KEY IdProgramEntry (IdProgramEntry),
  KEY LearningMode (LearningMode)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programentryrequirement (
  IdProgramEntryReq bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgramEntry bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_programentry',
  `Desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Item bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  Unit bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Condition` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Value` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Validity varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  EntryLevel bigint(20) NOT NULL COMMENT 'fk to definisionms where defTypeDesc = Award',
  IdSpecialization bigint(20) unsigned NOT NULL,
  GroupId bigint(10) NOT NULL,
  Mandatory binary(1) NOT NULL COMMENT '1: Yes 0:No',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdProgramEntryReq),
  KEY tbl_programentryrequirement_ibfk_1 (UpdUser),
  KEY tbl_programentryrequirement_ibfk_2 (IdProgramEntry),
  KEY tbl_programentryrequirement_ibfk_3 (Item),
  KEY tbl_programentryrequirement_ibfk_4 (Unit),
  KEY tbl_programentryrequirement_ibfk_5 (`Condition`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_programentrysubject (
  idProgramentrySubject bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdProgramEntry bigint(20) unsigned NOT NULL,
  IdSubject bigint(20) DEFAULT '0',
  SubjectName varchar(50) DEFAULT NULL,
  Marks varchar(20) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (idProgramentrySubject)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programfees (
  IdProgramFees bigint(20) NOT NULL AUTO_INCREMENT,
  IdFeesSetupMaster bigint(20) unsigned NOT NULL COMMENT 'FK To tbl_feessetupmaster',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'FK To tbl_program',
  PRIMARY KEY (IdProgramFees),
  KEY IdFeesSetupMaster (IdFeesSetupMaster),
  KEY IdProgram (IdProgram)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programhistory (
  IdHistory bigint(20) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL,
  Active bigint(20) NOT NULL COMMENT '0:Inactive,1:Active',
  Remarks text NOT NULL,
  PRIMARY KEY (IdHistory)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programlearningmodedetails (
  IdProgramLearningModeDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  IdLearningMode bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_definationms',
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdProgramLearningModeDetails),
  KEY IdProgram (IdProgram),
  KEY IdLearningMode (IdLearningMode)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_programmajoring (
  IDProgramMajoring bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key ',
  IdMajor varchar(20) DEFAULT NULL,
  idProgram bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_program',
  EnglishDescription varchar(200) NOT NULL,
  BahasaDescription varchar(200) NOT NULL,
  modifyby int(11) DEFAULT NULL,
  modifydt int(11) DEFAULT NULL,
  PRIMARY KEY (IDProgramMajoring),
  KEY idProgram (idProgram)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_programmohedetails (
  IdProgramMOHEDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  MoheDate date DEFAULT NULL,
  MoheReferences varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl1 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl2 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl3 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl4 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl5 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl6 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl7 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl8 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl9 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl10 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl11 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl12 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl13 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl14 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl15 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl16 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl17 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl18 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl19 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MoheDtl20 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdProgramMOHEDetails),
  KEY IdProgram (IdProgram),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_programrequirement (
  IdProgramReq bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdLandscape bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_landscape',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  SubjectType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  CreditHours int(3) NOT NULL,
  Compulsory int(11) DEFAULT NULL COMMENT '1:Yes 2:Not',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  modifyby int(11) DEFAULT NULL,
  modifydt datetime DEFAULT NULL,
  PRIMARY KEY (IdProgramReq),
  KEY tbl_programrequirement_ibfk_1 (UpdUser),
  KEY tbl_programrequirement_ibfk_2 (IdLandscape),
  KEY tbl_programrequirement_ibfk_3 (IdProgram),
  KEY tbl_programrequirement_ibfk_4 (SubjectType)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_programrequirement_june (
  IdProgramReq bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdLandscape bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_landscape',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  SubjectType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  CreditHours int(3) NOT NULL,
  Compulsory int(11) DEFAULT NULL COMMENT '1:Yes 2:Not',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  modifyby int(11) DEFAULT NULL,
  modifydt datetime DEFAULT NULL,
  PRIMARY KEY (IdProgramReq),
  KEY tbl_programrequirement_ibfk_1 (UpdUser),
  KEY tbl_programrequirement_ibfk_2 (IdLandscape),
  KEY tbl_programrequirement_ibfk_3 (IdProgram),
  KEY tbl_programrequirement_ibfk_4 (SubjectType)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_program_scheme (
  IdProgramScheme bigint(20) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) NOT NULL,
  mode_of_program int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_definations',
  mode_of_study int(11) NOT NULL,
  program_type int(11) NOT NULL,
  createdby int(11) NOT NULL,
  createddt datetime NOT NULL,
  PRIMARY KEY (IdProgramScheme),
  UNIQUE KEY IdProgram_3 (IdProgram,mode_of_program,mode_of_study),
  KEY IdProgram_2 (IdProgram)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_qualificationmaster (
  IdQualification int(11) NOT NULL AUTO_INCREMENT,
  QualificationLevel text NOT NULL,
  QualificationRank int(10) NOT NULL,
  Active int(10) NOT NULL DEFAULT '0',
  SpecialTreatment int(10) DEFAULT '0',
  UpdUser int(11) NOT NULL,
  UpdDate int(11) NOT NULL,
  PRIMARY KEY (IdQualification)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_qualification_subject_mapping (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdQualification bigint(20) NOT NULL,
  IdSubject bigint(20) NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_recieptmaster (
  idReciept bigint(20) NOT NULL AUTO_INCREMENT,
  RecieptNumber int(20) NOT NULL,
  RecieptDate date NOT NULL,
  InvoiceId bigint(20) unsigned DEFAULT NULL,
  StudentId bigint(20) unsigned NOT NULL,
  SemesterId bigint(20) unsigned NOT NULL,
  IdProgram bigint(20) unsigned NOT NULL,
  UpdDate date NOT NULL,
  UpdUser varchar(50) NOT NULL,
  ApprovedBy varchar(50) NOT NULL,
  ChangedBy varchar(50) NOT NULL,
  IdCollege bigint(20) NOT NULL,
  ModeOfPayment bigint(20) unsigned NOT NULL,
  ChequeDate date DEFAULT NULL,
  ChequeNo varchar(20) DEFAULT NULL,
  PRIMARY KEY (idReciept),
  KEY StudentId (StudentId),
  KEY SemesterId (SemesterId),
  KEY InvoiceId (InvoiceId),
  KEY IdProgram (IdProgram),
  KEY ModeOfPayment (ModeOfPayment)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_recieptmasterdetails (
  idRecieptDetails bigint(20) NOT NULL AUTO_INCREMENT,
  idReciept bigint(20) unsigned NOT NULL,
  idItem bigint(20) unsigned NOT NULL,
  AmountPaid decimal(10,2) NOT NULL,
  UpdUser varchar(50) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (idRecieptDetails),
  KEY idReciept (idReciept),
  KEY idItem (idItem)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_record_config (
  IdRecordConfig bigint(20) NOT NULL AUTO_INCREMENT,
  ApprovalCreditTransfer int(10) NOT NULL,
  RevertCreditTransfer int(10) NOT NULL,
  EserviceCreditTransfer int(10) NOT NULL,
  ApprovalChangeProgram int(10) NOT NULL,
  RevertChangeProgram int(10) NOT NULL,
  EserviceChangeProgram int(10) NOT NULL,
  ApprovalChangeStatus int(10) NOT NULL,
  RevertChangeStatus int(10) NOT NULL,
  EserviceChangeStatus int(10) NOT NULL,
  IdUniversity bigint(20) NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdRecordConfig)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_record_config_messages (
  IdRecordMessage bigint(20) NOT NULL AUTO_INCREMENT,
  IdRecordConfig int(20) NOT NULL,
  RecordModule varchar(100) NOT NULL,
  RecordModuleStatus bigint(20) NOT NULL,
  Email varchar(100) NOT NULL,
  Description varchar(200) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdRecordMessage)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_record_reason_defer (
  IdRecordResonDefer bigint(20) NOT NULL AUTO_INCREMENT,
  IdUniversity bigint(20) NOT NULL,
  ReasonDefer varchar(200) NOT NULL,
  DeferDescription varchar(100) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdRecordResonDefer)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_record_reason_quit (
  IdRecordResonQuit bigint(20) NOT NULL AUTO_INCREMENT,
  IdUniversity bigint(20) NOT NULL,
  Reason varchar(100) DEFAULT NULL,
  QuitDescription varchar(100) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdRecordResonQuit)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_registrarlist (
  IdRegistrarList bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdUniversity bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_universitymaster',
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  FromDate date NOT NULL,
  ToDate date DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdRegistrarList),
  KEY tbl_registrarlist_ibfk_1 (UpdUser),
  KEY tbl_registrarlist_ibfk_2 (IdUniversity),
  KEY tbl_registrarlist_ibfk_3 (IdStaff)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_registrationlocation (
  IdRegistrationLocation int(11) NOT NULL AUTO_INCREMENT,
  RegistrationLocationCode varchar(30) NOT NULL,
  RegistrationLocationName varchar(100) NOT NULL,
  RegistrationLocationShortName varchar(100) NOT NULL,
  RegistrationLocationDescription varchar(100) NOT NULL,
  RegistrationLocationIntake bigint(20) NOT NULL,
  RegistrationLocationStatus binary(1) NOT NULL,
  RegistrationLocationAddress1 varchar(50) NOT NULL,
  RegistrationLocationAddress2 varchar(50) NOT NULL,
  `Status` bigint(20) NOT NULL DEFAULT '1',
  country varchar(20) NOT NULL,
  state varchar(20) NOT NULL,
  city varchar(20) NOT NULL,
  zipCode varchar(20) NOT NULL,
  RegPhone varchar(20) NOT NULL,
  fax varchar(20) NOT NULL,
  PRIMARY KEY (IdRegistrationLocation)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_registration_configuration (
  regConfiguration_id int(11) NOT NULL AUTO_INCREMENT,
  registrationType tinyint(4) DEFAULT NULL,
  minFinanceBalance binary(1) DEFAULT NULL,
  academicLandscapeReqt binary(1) DEFAULT NULL,
  minCgpa binary(1) DEFAULT NULL,
  studentDisciplinary binary(1) DEFAULT NULL,
  UpdDate date NOT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  PRIMARY KEY (regConfiguration_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_registration_info (
  IdRegistrationInfo bigint(10) NOT NULL AUTO_INCREMENT,
  IdRegistrationLocation bigint(10) NOT NULL,
  RegistrationLocationScheme bigint(20) NOT NULL,
  RegistrationLocationDate varchar(50) NOT NULL,
  RegistrationLocationTime varchar(50) NOT NULL,
  RegistrationLocationRemarks varchar(50) NOT NULL,
  PRIMARY KEY (IdRegistrationInfo)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_resources (
  idResources bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  Module varchar(255) NOT NULL,
  Controller varchar(255) NOT NULL,
  `Action` varchar(45) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  RouteName varchar(255) DEFAULT NULL,
  Modified datetime NOT NULL,
  Created datetime NOT NULL,
  PRIMARY KEY (idResources),
  KEY Controller (Controller),
  KEY Module (Module),
  KEY `Action` (`Action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_resources_new (
  r_id bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  r_menu_id int(11) NOT NULL COMMENT 'fk tbl_top_menu',
  r_title_id int(11) NOT NULL COMMENT 'fk tbl_sidebar_menu',
  r_screen_id int(11) DEFAULT NULL COMMENT 'fk tbl_sidebar_menu',
  r_controller varchar(255) NOT NULL,
  r_group tinyint(4) NOT NULL COMMENT '1: View 2:Add 3:Edit 4:Delete 5:Approve',
  r_action varchar(255) NOT NULL,
  r_createddt datetime NOT NULL,
  r_createdby int(11) NOT NULL,
  PRIMARY KEY (r_id),
  KEY r_menu_id (r_menu_id),
  KEY r_title_id (r_title_id),
  KEY r_screen_id (r_screen_id),
  KEY r_controller (r_controller),
  KEY r_group (r_group),
  KEY r_action (r_action)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_roleresources (
  idRoleResources bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  idRoles bigint(11) unsigned NOT NULL,
  idResources bigint(11) unsigned NOT NULL,
  modified datetime NOT NULL,
  created datetime NOT NULL,
  PRIMARY KEY (idRoleResources),
  KEY idRoles (idRoles),
  KEY idResources (idResources)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_role_resources_nav (
  rrn_id int(11) NOT NULL AUTO_INCREMENT,
  rrn_role_id int(11) NOT NULL COMMENT 'fk tbl_defination  :idDefType=1',
  rrn_menu_id int(11) DEFAULT NULL COMMENT 'fk tbl_top_menu',
  rrn_title_id int(11) DEFAULT NULL COMMENT 'fk tbl_sidebar_menu',
  rrn_screen_id int(11) DEFAULT NULL COMMENT 'fk tbl_sidebar_menu',
  rrn_res_group tinyint(4) NOT NULL DEFAULT '0',
  rrn_createddt datetime NOT NULL,
  rrn_createdby int(11) NOT NULL,
  PRIMARY KEY (rrn_id),
  UNIQUE KEY rrn_role_id (rrn_role_id,rrn_menu_id,rrn_title_id,rrn_screen_id,rrn_res_group),
  KEY rrn_id (rrn_id),
  KEY rrn_menu_id_2 (rrn_menu_id),
  KEY rrn_title_id_2 (rrn_title_id),
  KEY rrn_screen_id_2 (rrn_screen_id),
  KEY rrn_res_group_2 (rrn_res_group),
  KEY rrn_role_id_2 (rrn_role_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_room_charges (
  IdRoomCharges bigint(20) NOT NULL AUTO_INCREMENT,
  IdHostelRoom bigint(20) NOT NULL,
  SemesterCode varchar(50) NOT NULL,
  StartDate datetime NOT NULL,
  EndDate datetime NOT NULL,
  Rate double NOT NULL,
  CustomerType bigint(11) NOT NULL,
  UpdUser bigint(11) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdRoomCharges)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_scheme (
  IdScheme bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  SchemeCode varchar(20) DEFAULT NULL,
  EnglishDescription varchar(255) NOT NULL DEFAULT '',
  ArabicDescription varchar(255) NOT NULL DEFAULT '',
  `Mode` bigint(20) unsigned DEFAULT NULL COMMENT 'made it to null on 190712-sp4-10998',
  Active binary(1) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  UpdUser bigint(20) unsigned DEFAULT NULL,
  english_temp varchar(50) DEFAULT NULL,
  arabic_temp varchar(50) DEFAULT NULL,
  PRIMARY KEY (IdScheme),
  KEY `Mode` (`Mode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_schoolmaster (
  idSchool bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idCity bigint(20) unsigned NOT NULL,
  idState bigint(20) unsigned NOT NULL,
  SchoolType varchar(15) DEFAULT NULL,
  SchoolName varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  SchoolCode varchar(20) DEFAULT NULL,
  School_ArabicName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Address1 varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Address2 varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Email varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Url varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  workPhone varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  country bigint(20) unsigned NOT NULL,
  PRIMARY KEY (idSchool),
  KEY idState (idState),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_school_decipline_subject (
  sds_id int(11) NOT NULL AUTO_INCREMENT,
  sds_discipline_code varchar(3) NOT NULL COMMENT 'fk school_discipline(smj_code)',
  sds_subject varchar(100) NOT NULL,
  PRIMARY KEY (sds_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_school_discipline (
  smd_code varchar(3) NOT NULL,
  smd_desc varchar(150) NOT NULL,
  smd_school_type int(11) NOT NULL COMMENT 'school_type(st_id)',
  PRIMARY KEY (smd_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE tbl_school_subject (
  ss_id int(11) NOT NULL AUTO_INCREMENT,
  ss_subject varchar(100) NOT NULL,
  ss_subject_bahasa varchar(100) DEFAULT NULL,
  ss_core_subject tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 core subj ',
  PRIMARY KEY (ss_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_school_type (
  st_id int(11) NOT NULL AUTO_INCREMENT,
  st_code varchar(20) NOT NULL,
  st_name varchar(50) NOT NULL,
  PRIMARY KEY (st_code),
  UNIQUE KEY sl_id (st_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_semester (
  IdSemester bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  Semester bigint(20) NOT NULL,
  SemesterCode varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  SemesterStartDate date NOT NULL,
  SemesterEndDate date NOT NULL,
  StudentIntake bigint(20) NOT NULL COMMENT '0:No, There will be no intake for this semester, 1:Yes, There will be student intake for this semester',
  SemesterStatus bigint(20) NOT NULL COMMENT 'Status of the semester',
  Active binary(1) NOT NULL DEFAULT '1' COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  Program bigint(20) DEFAULT NULL COMMENT 'FK to tbl_scheme',
  PRIMARY KEY (IdSemester),
  KEY tbl_semester_ibfk_1 (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_semestermaster (
  IdSemesterMaster bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  SemesterMainName varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  SemesterMainDefaultLanguage varchar(100) DEFAULT NULL,
  SemesterMainCode varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  IsCountable tinyint(1) NOT NULL COMMENT '1: Active 0:Inactive',
  SemesterMainStatus int(11) NOT NULL,
  DummyStatus int(2) DEFAULT NULL,
  SemesterMainStartDate date NOT NULL,
  SemesterMainEndDate date NOT NULL,
  Scheme int(11) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdSemesterMaster),
  KEY tbl_semestermaster_ibfk_1 (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_semesterprogramlink (
  IdSemesterProgramLink bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_semester',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdSemesterProgramLink),
  KEY tbl_semesterprogramlink_ibfk_1 (UpdUser),
  KEY tbl_semesterprogramlink_ibfk_2 (IdSemester),
  KEY tbl_semesterprogramlink_ibfk_3 (IdProgram)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_semesterstatus (
  IdSemesterStatus bigint(20) NOT NULL AUTO_INCREMENT,
  IdSemester bigint(20) NOT NULL COMMENT 'FK  to tbl_semester',
  SemesterStatus int(11) NOT NULL COMMENT '0:Previous;1:Present;2:Past',
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (IdSemesterStatus),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_semester_1162013 (
  IdSemester bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  Semester bigint(20) NOT NULL,
  SemesterCode varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  SemesterStartDate date NOT NULL,
  SemesterEndDate date NOT NULL,
  StudentIntake bigint(20) NOT NULL COMMENT '0:No, There will be no intake for this semester, 1:Yes, There will be student intake for this semester',
  SemesterStatus bigint(20) NOT NULL COMMENT 'Status of the semester',
  Active binary(1) NOT NULL DEFAULT '1' COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  Program bigint(20) DEFAULT NULL COMMENT 'FK to tbl_scheme',
  PRIMARY KEY (IdSemester),
  KEY tbl_semester_ibfk_1 (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_sendoffer (
  IdApprove bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication int(11) NOT NULL,
  IdProgram int(11) NOT NULL,
  Approved int(11) NOT NULL,
  UpdUser int(11) NOT NULL,
  `Update` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (IdApprove)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_sidebar_menu (
  sm_id int(11) NOT NULL AUTO_INCREMENT,
  sm_tm_id int(11) NOT NULL,
  sm_name varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  sm_desc text COLLATE utf8_unicode_ci NOT NULL,
  sm_parentid int(11) NOT NULL DEFAULT '0',
  sm_seq_order int(11) NOT NULL DEFAULT '0',
  sm_visibility tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:Hide 1:Show',
  PRIMARY KEY (sm_id),
  KEY sm_tm_id (sm_tm_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_specialization (
  IdSpecialization bigint(20) NOT NULL AUTO_INCREMENT,
  Specialization varchar(100) NOT NULL,
  Description varchar(150) CHARACTER SET utf8 NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdSpecialization)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_sponsor (
  idsponsor bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  lName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  mName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  fName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  DOB date NOT NULL DEFAULT '0000-00-00',
  gender binary(1) DEFAULT '0',
  Add1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  Add2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  zipCode varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  State varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Country bigint(20) DEFAULT NULL,
  HomePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  WorkPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CellPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Email varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  Organisation varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  typeSponsor binary(1) NOT NULL DEFAULT '0' COMMENT '0-Individual, 1-Organisation',
  url varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (idsponsor)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_sponsortemplate (
  idSponsortemplate bigint(20) NOT NULL AUTO_INCREMENT,
  templateName varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  Description varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (idSponsortemplate),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_sponsortemplatedetails (
  idSponsortemplatedetails bigint(20) NOT NULL AUTO_INCREMENT,
  idSponsortemplate bigint(20) NOT NULL,
  idAccount bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (idSponsortemplatedetails),
  KEY idSponsortemplate (idSponsortemplate)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_staffmaster (
  IdStaff bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  FirstName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  SecondName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  ThirdName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FourthName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FullName varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ArabicName varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField6 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField7 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField8 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField9 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField10 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField11 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField12 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField13 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField14 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField15 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField16 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField17 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField18 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField19 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField20 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  DOB date DEFAULT NULL,
  gender tinyint(1) NOT NULL DEFAULT '1',
  Add1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Add2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  State bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_state',
  Country bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_countries',
  Zip varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  Phone varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Mobile varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Email varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Active tinyint(1) NOT NULL COMMENT '(0:Inactive,1:Active)',
  StaffType tinyint(1) NOT NULL COMMENT '0:Staff under College, 1:Staff under Branch',
  IdCollege bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster of IdCollegeMaster (IdCollege/IdBranch. Depends on field StaffType)',
  IdLevel bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_definationms',
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  StaffAcademic tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Academic Staff,1:Non-Academic Staff',
  StaffId varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  FrontSalutation bigint(20) NOT NULL DEFAULT '0',
  BackSalutation bigint(20) NOT NULL COMMENT 'FK  to tbl_definitionms',
  NationalIdendificationNumber1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  NationalIdendificationNumber2 int(11) DEFAULT NULL,
  NationalIdendificationNumber3 int(11) DEFAULT NULL,
  NationalIdendificationNumber4 int(11) DEFAULT NULL,
  Religion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  PlaceOfBirth bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_city',
  BankId bigint(20) DEFAULT '0' COMMENT 'FK to tbl_bank',
  BankAccountNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  HighestQualification varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  QualificationNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePage varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdendityNoOfStateLecturer varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  AuthorizationLetterNumber varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PtiAuthorizationLetterNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PtiName varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PreviousPtiNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermitNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  StaffJobType bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  JoiningDate date NOT NULL,
  `group` bigint(20) NOT NULL,
  Dosen_Code_EPSBED varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (IdStaff),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_staffmaster_20121121 (
  IdStaff bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  FirstName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  SecondName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  ThirdName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FourthName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FullName varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ArabicName varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField6 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField7 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField8 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField9 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField10 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField11 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField12 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField13 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField14 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField15 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField16 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField17 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField18 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField19 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField20 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  DOB date DEFAULT NULL,
  gender tinyint(1) NOT NULL DEFAULT '1',
  Add1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Add2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  State bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_state',
  Country bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_countries',
  Zip varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  Phone varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Mobile varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Email varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL COMMENT '(0:Inactive,1:Active)',
  StaffType tinyint(1) NOT NULL COMMENT '0:Staff under College, 1:Staff under Branch',
  IdCollege bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster of IdCollegeMaster (IdCollege/IdBranch. Depends on field StaffType)',
  IdLevel bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_definationms',
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  StaffAcademic binary(1) NOT NULL DEFAULT '0' COMMENT '0:Academic Staff,1:Non-Academic Staff',
  StaffId varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  FrontSalutation bigint(20) NOT NULL DEFAULT '0',
  BackSalutation bigint(20) NOT NULL COMMENT 'FK  to tbl_definitionms',
  NationalIdendificationNumber1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  NationalIdendificationNumber2 int(11) DEFAULT NULL,
  NationalIdendificationNumber3 int(11) DEFAULT NULL,
  NationalIdendificationNumber4 int(11) DEFAULT NULL,
  Religion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  PlaceOfBirth bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_city',
  BankId bigint(20) DEFAULT '0' COMMENT 'FK to tbl_bank',
  BankAccountNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  HighestQualification varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  QualificationNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePage varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdendityNoOfStateLecturer varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  AuthorizationLetterNumber varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PtiAuthorizationLetterNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PtiName varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PreviousPtiNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermitNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  StaffJobType bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  JoiningDate date NOT NULL,
  `group` bigint(20) NOT NULL,
  PRIMARY KEY (IdStaff),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_staffmaster_ori (
  IdStaff bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  FirstName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  SecondName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  ThirdName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FourthName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FullName varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ArabicName varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField6 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField7 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField8 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField9 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField10 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField11 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField12 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField13 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField14 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField15 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField16 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField17 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField18 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField19 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField20 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  DOB date DEFAULT NULL,
  gender tinyint(1) NOT NULL DEFAULT '1',
  Add1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Add2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  State bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_state',
  Country bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_countries',
  Zip varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  Phone varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Mobile varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Email varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL COMMENT '(0:Inactive,1:Active)',
  StaffType binary(1) NOT NULL COMMENT '0:Staff under College, 1:Staff under Branch',
  IdCollege bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster of IdCollegeMaster (IdCollege/IdBranch. Depends on field StaffType)',
  IdLevel bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_definationms',
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  StaffAcademic binary(1) NOT NULL DEFAULT '0' COMMENT '0:Academic Staff,1:Non-Academic Staff',
  StaffId varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  FrontSalutation bigint(20) NOT NULL DEFAULT '0',
  BackSalutation bigint(20) NOT NULL COMMENT 'FK  to tbl_definitionms',
  NationalIdendificationNumber1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  NationalIdendificationNumber2 int(11) DEFAULT NULL,
  NationalIdendificationNumber3 int(11) DEFAULT NULL,
  NationalIdendificationNumber4 int(11) DEFAULT NULL,
  Religion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  PlaceOfBirth bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_city',
  BankId bigint(20) DEFAULT '0' COMMENT 'FK to tbl_bank',
  BankAccountNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  HighestQualification varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  QualificationNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePage varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdendityNoOfStateLecturer varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  AuthorizationLetterNumber varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PtiAuthorizationLetterNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PtiName varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PreviousPtiNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermitNo varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  StaffJobType bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  JoiningDate date NOT NULL,
  PRIMARY KEY (IdStaff),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_staffsubject (
  idStaffSubject bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'foriegn key to tbl_staffmaster',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'foriegn key to tbl_subjectmaster',
  PRIMARY KEY (idStaffSubject)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_staffsubjects (
  IdStaffSubjects bigint(20) NOT NULL AUTO_INCREMENT,
  IdStaff bigint(20) NOT NULL COMMENT 'FK to tbl_staff',
  IdSemester bigint(20) NOT NULL COMMENT 'FK to tbl_semester',
  IdSubject bigint(20) NOT NULL COMMENT 'FK to tbl_subject',
  EffectiveDate date NOT NULL,
  UpdDate date NOT NULL,
  UpdUser int(11) NOT NULL COMMENT 'FK to tbl_user',
  Active binary(1) NOT NULL,
  PRIMARY KEY (IdStaffSubjects)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_state (
  idState bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  StateCode varchar(20) NOT NULL,
  state_Code_EPSBED char(2) NOT NULL,
  StateName varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  idCountry bigint(20) unsigned NOT NULL,
  Active binary(1) NOT NULL COMMENT '1:Active, 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  PRIMARY KEY (idState),
  KEY FK_tbl_State_1 (idCountry)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_state_2012120xx (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  idState bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  StateCode varchar(20) NOT NULL,
  idCountry bigint(20) unsigned NOT NULL,
  StateName varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL COMMENT '1:Active, 0:Inactive',
  PRIMARY KEY (idState),
  KEY FK_tbl_State_1 (idCountry)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentappfilesupload (
  IdStudentAppFilesUpload bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_application',
  FileName varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UploadedFileName varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UploadPath varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  DocumentType bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_definationms',
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdStudentAppFilesUpload),
  KEY IdApplication (IdApplication),
  KEY DocumentType (DocumentType),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentapplication (
  IdApplication bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) DEFAULT NULL COMMENT 'from tbl_studentregistration. gets inserted only when status changed to ACTIVATED',
  registrationId varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'this inserts when student gets registered',
  EmergencyCountry bigint(20) NOT NULL,
  EmergencyState bigint(20) NOT NULL,
  EmergencyCity bigint(20) NOT NULL,
  EmergencyZip varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  IdApplicant varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` bigint(20) unsigned DEFAULT NULL,
  StudentId varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  StudentCollegeId varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  MName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  LName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  TypeofId varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField6 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField7 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField8 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField9 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField10 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField11 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField12 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField13 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField14 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField15 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField16 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField17 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField18 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField19 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField20 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  DateOfBirth date NOT NULL,
  photo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  Gender binary(1) NOT NULL COMMENT '0: Male 1:Female',
  NoOfChildren smallint(10) DEFAULT '0',
  Nationality varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PermAddressDetails varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PermCity varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PermState bigint(20) unsigned NOT NULL,
  PermCountry bigint(20) unsigned NOT NULL,
  PermZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsState bigint(20) unsigned DEFAULT NULL,
  CorrsCountry bigint(20) unsigned DEFAULT NULL,
  CorrsZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusCountry bigint(20) DEFAULT NULL,
  OutcampusState bigint(20) DEFAULT NULL,
  OutcampusCity bigint(20) DEFAULT NULL,
  OutcampusZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT '000-000-000',
  CellPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmailAddress varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  idCollege bigint(20) NOT NULL,
  idsponsor bigint(20) unsigned DEFAULT NULL,
  InternationalStd bigint(20) DEFAULT NULL,
  PPNos varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPIssueDt date DEFAULT NULL,
  PPExpDt date DEFAULT NULL,
  PPField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField5 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField1 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField2 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField3 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField4 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  VisaDetailField5 varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  Active binary(1) NOT NULL COMMENT '''1:Active, 0:Inactive''',
  hobbies varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  othercurricularactivity varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  Relative binary(1) NOT NULL DEFAULT '0',
  RelativeDiscountType bigint(20) DEFAULT '0',
  `Local` binary(1) NOT NULL DEFAULT '0' COMMENT '"0=Local Student 1=Non Local Student"',
  Sponsorpercent decimal(10,2) NOT NULL DEFAULT '0.00',
  idSponsortemplate bigint(20) DEFAULT NULL,
  idcoursedetails bigint(20) NOT NULL DEFAULT '1',
  Studapp bigint(20) NOT NULL DEFAULT '0',
  PreffredHostel bigint(20) DEFAULT NULL,
  PreferredBlock bigint(20) DEFAULT NULL,
  PreffredFloor bigint(20) DEFAULT NULL,
  PreffredroomType bigint(20) DEFAULT NULL,
  PrefferedApartment bigint(20) DEFAULT NULL,
  PreffredBed bigint(20) DEFAULT NULL,
  bedAllotedto bigint(20) DEFAULT '0',
  RoomPrefredFrom date DEFAULT '0000-00-00',
  RoomPrefredto date DEFAULT '0000-00-00',
  CountryFirstVisitDate date DEFAULT NULL,
  currentjobfromdate date DEFAULT '0000-00-00',
  currentjobtodate date DEFAULT '0000-00-00',
  currentjobtitle varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  currentjoborganizationtype bigint(20) DEFAULT NULL,
  currentjobemployer varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  currentjobemployeraddress varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  currentjobsalary varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  Termination binary(1) DEFAULT '0' COMMENT '1:Terminated 0: Un-terminated',
  IDCourse bigint(20) NOT NULL,
  ICNumber varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  Registered binary(1) NOT NULL COMMENT '1:Yes, 0:No',
  Offered binary(1) NOT NULL COMMENT '1:Yes, 0:No',
  Accepted binary(1) NOT NULL COMMENT '1:Yes, 0:No',
  ApplicationType binary(1) NOT NULL COMMENT '1:Online, 0:Manual',
  IdPlacementtest bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_placementtest',
  ApplicationDate datetime NOT NULL,
  PlaceOfBirth varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  TypeOfResidence int(11) NOT NULL COMMENT '1=Rent a hoouse;0=Family House',
  Religion int(11) DEFAULT NULL COMMENT 'FK  from tbl_definitionms',
  BloodGroup int(11) DEFAULT NULL COMMENT 'FK  from tbl_definitionms',
  NameOfFather varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  NameOfMother varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  CircOfFather binary(1) NOT NULL DEFAULT '1' COMMENT '0;deceased 1:alive',
  CircOfMother binary(1) NOT NULL DEFAULT '1' COMMENT '0;deceased 1:alive',
  ParentAddressDetails varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PostCode varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  City bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_city',
  Telephone varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  Handphone varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PEmail varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  MaritalStatus int(10) DEFAULT NULL,
  ParentJob bigint(20) unsigned DEFAULT NULL COMMENT 'FK to definitionMs',
  ParentEducation varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  Referrel bigint(20) unsigned DEFAULT NULL COMMENT 'FK to definitionMs',
  JacketSize bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_definationms',
  LocalPlacementTest binary(1) NOT NULL DEFAULT '0',
  LocalCertification binary(1) NOT NULL DEFAULT '0',
  InternationalPlacementTest binary(1) NOT NULL DEFAULT '0',
  InternationalCertification binary(1) NOT NULL DEFAULT '0',
  Rejected binary(1) NOT NULL DEFAULT '0' COMMENT '0-No,1-Yes',
  RejectedComments varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  DateRejected datetime DEFAULT NULL,
  RejectedBy bigint(10) DEFAULT NULL,
  RejectedReason bigint(20) NOT NULL,
  intake bigint(20) unsigned DEFAULT NULL,
  DateCreated datetime DEFAULT NULL,
  `Source` bigint(10) DEFAULT NULL,
  Scheme bigint(20) unsigned DEFAULT NULL,
  DateIncompleted datetime DEFAULT NULL,
  IncompleteBy bigint(10) DEFAULT NULL,
  IncompleteComments varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  ReasonIncomplete bigint(20) NOT NULL,
  Dateofferred date DEFAULT NULL,
  OfferredBy bigint(10) DEFAULT NULL,
  ProgramOfferred bigint(10) DEFAULT NULL,
  BranchOfferred bigint(20) unsigned DEFAULT NULL,
  DateRegistered datetime DEFAULT NULL,
  RegisteredBy bigint(10) DEFAULT NULL,
  ProgramRegistered bigint(10) DEFAULT NULL,
  FullArabicName varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  Race bigint(10) DEFAULT NULL,
  SpecialTreatment bigint(10) DEFAULT NULL,
  SpecialTreatmentType int(10) DEFAULT NULL,
  DateProvisionalOffered date DEFAULT NULL,
  DateProvisionalOfferedBy bigint(20) DEFAULT NULL,
  ProvisionalProgramOffered bigint(20) DEFAULT NULL,
  ProvisionalBranchOffered bigint(20) unsigned DEFAULT NULL,
  RelationshipType bigint(20) unsigned NOT NULL,
  RelativeName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  EmergencyAddressDetails varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  EmergencyHomePhone varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  EmergencyCellPhone varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  EmergencyOffPhone varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  is_migrated binary(1) DEFAULT NULL,
  IdApplicantFormat varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (IdApplication),
  KEY FK_tbl_StudentApplication_PermState (PermState),
  KEY FK_tbl_StudentApplication_PermCountry (PermCountry),
  KEY FK_tbl_StudentApplication_CorrsState (CorrsState),
  KEY FK_tbl_StudentApplication_CorrsCountry (CorrsCountry),
  KEY JacketSize (JacketSize),
  KEY Referrel (Referrel),
  KEY ParentJob (ParentJob),
  KEY City (City)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentattandance (
  IdStudentAttandance bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentregistration',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  AttandanceStatus binary(1) NOT NULL DEFAULT '0' COMMENT '1=Present;0=Absent',
  UpdDate date NOT NULL,
  UpdUser bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (IdStudentAttandance)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentcurricularactivity (
  IdExtraCurActivity bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Foreign Key TO Student Application',
  idActivity bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key To Definition MS',
  GamingLevel bigint(20) NOT NULL COMMENT 'FK to definitionms',
  DateOfActivity date NOT NULL,
  PositionHeld varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  DetailedDescription varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  SpecialHonor varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdExtraCurActivity)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_studentdetain (
  idStudentDetain bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL,
  IdSemester bigint(20) unsigned NOT NULL,
  IdAction bigint(20) unsigned NOT NULL,
  ToEffectiveDate date NOT NULL,
  Comments varchar(250) DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (idStudentDetain)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studenteducationdetails (
  IdStudEduDtl bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  ProgCheckListName bigint(20) NOT NULL COMMENT 'FK to tbl_programchecklist',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentapplication',
  InstitutionName varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UniversityName varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  StudyPlace varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  MajorFiledOfStudy varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  YearOfStudyFrom varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  YearOfStudyTo varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  DegreeType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definationms ( Degree Type )',
  GradeOrCGPA varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  TypeOfSchool int(11) DEFAULT NULL,
  StatusOfSchool int(11) DEFAULT NULL,
  TypeOfStudent bigint(20) DEFAULT NULL COMMENT '1: international 0 : Local',
  HomeTownSchool varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  CreditTransferFrom varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectId bigint(20) DEFAULT NULL COMMENT 'FK to tbl_subjectmaster',
  SubjectMark bigint(20) DEFAULT '0',
  IdQualification bigint(20) DEFAULT NULL,
  IdSpecialization bigint(20) DEFAULT NULL,
  IdInstitute bigint(20) DEFAULT NULL,
  YearGraduated varchar(100) DEFAULT NULL,
  IdResultItem varchar(100) DEFAULT NULL,
  Resulttotal varchar(100) DEFAULT NULL,
  PRIMARY KEY (IdStudEduDtl)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_studentemploymentdetails (
  IdStudentEmployment bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) NOT NULL COMMENT 'FK to tbl_studentapplication',
  `From` date NOT NULL,
  `To` date NOT NULL,
  Posttitle varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  OrganisationType bigint(20) NOT NULL,
  EmployerName varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  EmployerAddress varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (IdStudentEmployment)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentparentdetails (
  IdStudparentDtl int(11) NOT NULL AUTO_INCREMENT,
  IdApplication int(11) NOT NULL COMMENT 'FK To tbl_studentapplication',
  NameOfFather varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  NameOfMother varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  CircOfFather int(11) NOT NULL COMMENT '1=life;0=deceased',
  CircOfMother int(11) NOT NULL COMMENT '1=life;0=deceased',
  ParentAddressDetails varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PostCode varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  City int(11) NOT NULL COMMENT 'FK tbl_definitionms',
  Telephone int(11) NOT NULL,
  Handphone int(11) NOT NULL,
  EmailAddress varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ParentJob int(11) NOT NULL COMMENT 'FK To tbl_definitionms',
  ParentEducation int(11) NOT NULL COMMENT 'FK To tbl_definitionms',
  STPTrisakti int(11) NOT NULL,
  PRIMARY KEY (IdStudparentDtl)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentplacementtest (
  IdStudentPlacementTest bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL,
  IdPlacementTest bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdStudentPlacementTest)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentregcharges (
  IdStudentRegCharges bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdStudentRegistration bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  IdCharge bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_charges',
  Charge decimal(12,2) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdStudentRegCharges),
  KEY IdCharge (IdCharge),
  KEY UpdUser (UpdUser),
  KEY IdStudentRegistration (IdStudentRegistration)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentregistration (
  IdStudentRegistration bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Fk applicant_profile',
  transaction_id bigint(20) NOT NULL COMMENT 'fk applicant_transaction',
  registrationId varchar(75) COLLATE utf8_unicode_ci NOT NULL COMMENT 'matric no',
  email varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdSemestersyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_semster',
  IdSemester bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_semester',
  Semesterstatus int(11) DEFAULT '1' COMMENT '0:inactive,1:Active,2:deceased',
  SemesterType int(11) NOT NULL COMMENT '1: Main 2: Details',
  IdSemesterMain bigint(20) DEFAULT NULL,
  IdSemesterDetails bigint(20) DEFAULT NULL,
  IdLandscape bigint(20) NOT NULL,
  IdProgram bigint(20) DEFAULT NULL COMMENT 'fk tbl_program',
  IdProgramMajoring int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_programmajoring',
  IdBranch bigint(20) DEFAULT NULL COMMENT 'fk tbl_branchofficeavenue  BranchId which will be offered (Ekonomi only)',
  IdIntake bigint(20) DEFAULT NULL,
  `Status` bigint(20) DEFAULT NULL COMMENT '1: Active',
  profileStatus bigint(20) DEFAULT NULL COMMENT 'fk tbl_definationms (Student Status)',
  FName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  MName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  LName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FullArabicName varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  DateOfBirth date DEFAULT NULL,
  TypeofId varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PlaceOfBirth varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  MaritalStatus int(10) DEFAULT NULL,
  Religion int(11) DEFAULT NULL,
  Race bigint(10) DEFAULT NULL,
  Gender binary(1) DEFAULT NULL,
  Nationality varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  SpecialTreatment bigint(10) DEFAULT NULL,
  SpecialTreatmentType int(10) DEFAULT NULL,
  PermAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermCountry bigint(20) DEFAULT NULL,
  PermState bigint(20) DEFAULT NULL,
  PermCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsCountry bigint(20) DEFAULT NULL,
  CorrsState bigint(20) DEFAULT NULL,
  CorrsCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusCountry bigint(20) DEFAULT NULL,
  OutcampusState bigint(20) DEFAULT NULL,
  OutcampusCity bigint(20) DEFAULT NULL,
  OutcampusZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CellPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RelationshipType bigint(20) DEFAULT NULL,
  RelativeName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyCountry bigint(20) DEFAULT NULL,
  EmergencyState bigint(20) DEFAULT NULL,
  EmergencyCity bigint(20) DEFAULT NULL,
  EmergencyZip varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyHomePhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyCellPhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyOffPhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  ApplicationDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  OldIdStudentRegistration int(11) DEFAULT NULL,
  IdStudentRegistrationFormatted varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  student_type tinyint(4) NOT NULL DEFAULT '0',
  AcademicAdvisor int(11) NOT NULL DEFAULT '0',
  repository varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  oldnim int(12) DEFAULT NULL,
  PRIMARY KEY (IdStudentRegistration),
  UNIQUE KEY registrationId (registrationId),
  KEY UpdUser (UpdUser),
  KEY IdSemester (IdSemester),
  KEY IdSemestersyllabus (IdSemestersyllabus)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentregistration_11062013 (
  IdStudentRegistration bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentapplication',
  registrationId varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdApplicant varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  Psswrd varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  email varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdSemestersyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_semster',
  IdSemester bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_semester',
  Semesterstatus int(11) DEFAULT '1' COMMENT '0:inactive,1:Active,2:deceased',
  IdSemesterMain bigint(20) DEFAULT NULL,
  IdSemesterDetails bigint(20) DEFAULT NULL,
  IdLandscape bigint(20) NOT NULL,
  IdProgram bigint(20) DEFAULT NULL COMMENT 'pogram which is offered',
  IdBranch bigint(20) DEFAULT NULL COMMENT 'BranchId wwhich will be offered',
  IdIntake bigint(20) DEFAULT NULL COMMENT 'lookup from intake setup based on applicants''''s program offered tagging',
  `Status` bigint(20) DEFAULT NULL COMMENT 'from Maintennance like "Activated" , etc..',
  profileStatus bigint(20) DEFAULT NULL,
  FName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  MName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  LName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FullArabicName varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  DateOfBirth date DEFAULT NULL,
  TypeofId varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PlaceOfBirth varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  MaritalStatus int(10) DEFAULT NULL,
  Religion int(11) DEFAULT NULL,
  Race bigint(10) DEFAULT NULL,
  Gender binary(1) DEFAULT NULL,
  Nationality varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  SpecialTreatment bigint(10) DEFAULT NULL,
  SpecialTreatmentType int(10) DEFAULT NULL,
  PermAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermCountry bigint(20) DEFAULT NULL,
  PermState bigint(20) DEFAULT NULL,
  PermCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsCountry bigint(20) DEFAULT NULL,
  CorrsState bigint(20) DEFAULT NULL,
  CorrsCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusCountry bigint(20) DEFAULT NULL,
  OutcampusState bigint(20) DEFAULT NULL,
  OutcampusCity bigint(20) DEFAULT NULL,
  OutcampusZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CellPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RelationshipType bigint(20) DEFAULT NULL,
  RelativeName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyCountry bigint(20) DEFAULT NULL,
  EmergencyState bigint(20) DEFAULT NULL,
  EmergencyCity bigint(20) DEFAULT NULL,
  EmergencyZip varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyHomePhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyCellPhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyOffPhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  ApplicationDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  OldIdStudentRegistration int(11) DEFAULT NULL,
  IdStudentRegistrationFormatted varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (IdStudentRegistration),
  KEY UpdUser (UpdUser),
  KEY IdSemester (IdSemester),
  KEY IdSemestersyllabus (IdSemestersyllabus)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentregistration_20130912 (
  IdStudentRegistration bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Fk applicant_profile',
  transaction_id bigint(20) NOT NULL COMMENT 'fk applicant_transaction',
  registrationId varchar(75) COLLATE utf8_unicode_ci NOT NULL COMMENT 'matric no',
  email varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ExtraIdField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PPField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdSemestersyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_semster',
  IdSemester bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_semester',
  Semesterstatus int(11) DEFAULT '1' COMMENT '0:inactive,1:Active,2:deceased',
  SemesterType int(11) NOT NULL COMMENT '1: Main 2: Details',
  IdSemesterMain bigint(20) DEFAULT NULL,
  IdSemesterDetails bigint(20) DEFAULT NULL,
  IdLandscape bigint(20) NOT NULL,
  IdProgram bigint(20) DEFAULT NULL COMMENT 'fk tbl_program',
  IdProgramMajoring int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_programmajoring',
  IdBranch bigint(20) DEFAULT NULL COMMENT 'fk tbl_branchofficeavenue  BranchId which will be offered (Ekonomi only)',
  IdIntake bigint(20) DEFAULT NULL,
  `Status` bigint(20) DEFAULT NULL COMMENT '1: Active',
  profileStatus bigint(20) DEFAULT NULL COMMENT 'fk tbl_definationms (Student Status)',
  FName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  MName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  LName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FullArabicName varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  DateOfBirth date DEFAULT NULL,
  TypeofId varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PlaceOfBirth varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  MaritalStatus int(10) DEFAULT NULL,
  Religion int(11) DEFAULT NULL,
  Race bigint(10) DEFAULT NULL,
  Gender binary(1) DEFAULT NULL,
  Nationality varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  SpecialTreatment bigint(10) DEFAULT NULL,
  SpecialTreatmentType int(10) DEFAULT NULL,
  PermAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermCountry bigint(20) DEFAULT NULL,
  PermState bigint(20) DEFAULT NULL,
  PermCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PermZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsCountry bigint(20) DEFAULT NULL,
  CorrsState bigint(20) DEFAULT NULL,
  CorrsCity varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  CorrsZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  OutcampusCountry bigint(20) DEFAULT NULL,
  OutcampusState bigint(20) DEFAULT NULL,
  OutcampusCity bigint(20) DEFAULT NULL,
  OutcampusZip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  HomePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  CellPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  RelationshipType bigint(20) DEFAULT NULL,
  RelativeName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyAddressDetails varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyCountry bigint(20) DEFAULT NULL,
  EmergencyState bigint(20) DEFAULT NULL,
  EmergencyCity bigint(20) DEFAULT NULL,
  EmergencyZip varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyHomePhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyCellPhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  EmergencyOffPhone varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  ApplicationDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  OldIdStudentRegistration int(11) DEFAULT NULL,
  IdStudentRegistrationFormatted varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  student_type tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: New Student 1: Senior Student',
  AcademicAdvisor int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_staffmaster',
  PRIMARY KEY (IdStudentRegistration),
  UNIQUE KEY registrationId (registrationId),
  KEY UpdUser (UpdUser),
  KEY IdSemester (IdSemester),
  KEY IdSemestersyllabus (IdSemestersyllabus)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentregsubjects (
  IdStudentRegSubjects bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdStudentRegistration bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  SubjectsApproved binary(1) NOT NULL DEFAULT '0',
  SubjectsApprovedComments varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdSemesterMain bigint(20) DEFAULT NULL,
  IdSemesterDetails bigint(20) DEFAULT NULL,
  SemesterLevel tinyint(4) DEFAULT NULL COMMENT 'level/sort semester',
  IdLandscapeSub int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_landscapesubject',
  IdBlock int(11) DEFAULT NULL COMMENT 'fk tbl_blocklandscape',
  BlockLevel tinyint(4) DEFAULT NULL COMMENT 'level/sort block',
  IdGrade varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  Active tinyint(4) DEFAULT NULL COMMENT '0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer',
  exam_status varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'C: Completed   IN:Incomplete  MG:Missing Grade F:Fraud NR:No Record',
  final_course_mark decimal(10,2) DEFAULT NULL COMMENT '80',
  grade_point decimal(10,2) DEFAULT NULL COMMENT '3.00',
  grade_name varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A',
  grade_desc varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Baik,Sangat Baik etc',
  grade_status tinyint(4) NOT NULL COMMENT 'Pass/Fail',
  mark_approveby int(11) DEFAULT NULL COMMENT 'tbl_staffmaster',
  mark_approvedt datetime DEFAULT NULL,
  IdCourseTaggingGroup int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_course_tagging_group',
  PRIMARY KEY (IdStudentRegSubjects),
  KEY IdSubject (IdSubject),
  KEY UpdUser (UpdUser),
  KEY IdStudentRegistration (IdStudentRegistration),
  KEY IdSemesterMain (IdSemesterMain),
  KEY IdStudentRegSubjects (IdStudentRegSubjects)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentregsubjects_1162013 (
  IdStudentRegSubjects bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdStudentRegistration bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  IdCourse bigint(20) DEFAULT NULL,
  SubjectsApproved binary(1) NOT NULL DEFAULT '0',
  SubjectsApprovedComments varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdSemesterMain bigint(20) DEFAULT NULL,
  IdSemesterDetails bigint(20) DEFAULT NULL,
  IdGrade varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  Active binary(1) DEFAULT NULL COMMENT '1:Active 0:inactive',
  PRIMARY KEY (IdStudentRegSubjects),
  KEY IdSubject (IdSubject),
  KEY UpdUser (UpdUser),
  KEY IdStudentRegistration (IdStudentRegistration)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_studentscredittransfer (
  IdStudentsCredittransfer int(20) unsigned NOT NULL AUTO_INCREMENT,
  DocumentsVerified binary(1) NOT NULL COMMENT '1:Verified,0:Not Verified',
  DocumentsApproved binary(1) NOT NULL COMMENT '1:approved,0:Not Approved',
  Comments varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  IdApplication bigint(20) unsigned NOT NULL,
  IdCreditTransferSubjectsDocs bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdStudentsCredittransfer)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentsemesterstatus (
  idstudentsemsterstatus bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) DEFAULT NULL,
  idSemester bigint(20) unsigned DEFAULT NULL COMMENT 'IdSemesterDetails',
  IdSemesterMain bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  IdBlock int(11) DEFAULT NULL COMMENT 'fk  tbl_landscapeblocks',
  studentsemesterstatus smallint(6) DEFAULT NULL COMMENT 'fk tbl_definationms (Student Semester Status)',
  Reason varchar(255) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_user',
  PRIMARY KEY (idstudentsemsterstatus)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentsemesterstatus_1162013 (
  idstudentsemsterstatus bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) DEFAULT NULL,
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentapplication',
  idSemester bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_semester',
  IdSemesterMain bigint(20) DEFAULT NULL,
  studentsemesterstatus smallint(6) DEFAULT NULL,
  Reason varchar(255) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_user',
  PRIMARY KEY (idstudentsemsterstatus)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_studentstatus (
  IdStudentStatus bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdApplication bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentapplication',
  `Status` int(11) NOT NULL COMMENT '1:Active, 2:Terminated, 3:Deceased, 4:Graduated',
  Reason text COLLATE utf8_unicode_ci NOT NULL,
  SemesterStatus int(11) DEFAULT NULL COMMENT '1:Defer, 2:Suspended',
  EffSemester bigint(20) unsigned DEFAULT NULL,
  NoofSemester int(11) DEFAULT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdStudentStatus),
  KEY IdApplication (IdApplication),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_student_detail_marks_entry (
  IdStudentMarksEntryDetail bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentMarksEntry bigint(20) DEFAULT NULL,
  Component bigint(20) DEFAULT NULL,
  ComponentItem bigint(20) DEFAULT NULL,
  ComponentDetail bigint(20) DEFAULT NULL,
  MarksObtained decimal(10,2) DEFAULT NULL COMMENT '(Raw Mark) Jumlah markah yang student perolehi',
  FinalMarksObtained decimal(10,2) NOT NULL COMMENT 'Jumlah markah yang student perolehi by Percentage',
  TotalMarks decimal(10,2) DEFAULT NULL COMMENT 'Jumlah markah keseluruhan',
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdStudentMarksEntryDetail),
  KEY IdStudentMarksEntryDetail (IdStudentMarksEntryDetail),
  KEY IdStudentMarksEntry (IdStudentMarksEntry)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_student_detail_marks_entry18112013 (
  IdStudentMarksEntryDetail bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentMarksEntry bigint(20) DEFAULT NULL,
  Component bigint(20) DEFAULT NULL,
  ComponentItem bigint(20) DEFAULT NULL,
  ComponentDetail bigint(20) DEFAULT NULL,
  MarksObtained decimal(10,2) DEFAULT NULL,
  TotalMarks decimal(10,2) DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdStudentMarksEntryDetail),
  KEY IdStudentMarksEntryDetail (IdStudentMarksEntryDetail),
  KEY IdStudentMarksEntry (IdStudentMarksEntry)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_student_generated_id (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) NOT NULL,
  YYYY int(4) DEFAULT '0',
  YY tinyint(4) DEFAULT '0',
  seqno varchar(50) DEFAULT '',
  px varchar(30) DEFAULT ' ',
  iid varchar(50) DEFAULT '',
  b varchar(50) DEFAULT '',
  a varchar(50) DEFAULT '',
  g varchar(2) DEFAULT '',
  UpdDate datetime NOT NULL,
  p varchar(50) DEFAULT '',
  UpdUser bigint(20) NOT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table to store student generated id';

CREATE TABLE tbl_student_grade (
  sg_id int(11) NOT NULL AUTO_INCREMENT,
  sg_IdStudentRegistration bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  sg_semesterId int(11) NOT NULL COMMENT 'fk tbl_semestermain',
  sg_idstudentsemsterstatus bigint(20) NOT NULL COMMENT 'fk tbl_studentsemesterstatus',
  sg_sem_credithour int(11) NOT NULL,
  sg_sem_totalpoint decimal(10,2) NOT NULL,
  sg_gpa decimal(10,2) NOT NULL,
  sg_gpa_status varchar(30) NOT NULL,
  sg_cum_credithour int(11) NOT NULL,
  sg_cum_totalpoint decimal(10,2) NOT NULL,
  sg_cgpa decimal(10,2) NOT NULL,
  sg_cgpa_status varchar(30) NOT NULL,
  PRIMARY KEY (sg_id),
  KEY sg_registrationId (sg_IdStudentRegistration),
  KEY sg_semesterId (sg_semesterId),
  KEY sg_idstudentsemsterstatus (sg_idstudentsemsterstatus)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE tbl_student_marks_entry (
  IdStudentMarksEntry bigint(20) NOT NULL AUTO_INCREMENT,
  IdSemester varchar(200) DEFAULT NULL,
  Course bigint(20) DEFAULT NULL,
  IdMarksDistributionMaster int(11) NOT NULL COMMENT 'fk tbl_marksdistributionmaster',
  Component bigint(20) DEFAULT NULL,
  ComponentItem bigint(20) DEFAULT NULL,
  AttendanceStatus bigint(20) DEFAULT NULL,
  Instructor bigint(20) DEFAULT NULL,
  MarksEntryStatus bigint(20) DEFAULT NULL,
  ApprovedBy bigint(20) DEFAULT NULL,
  ApprovedOn datetime DEFAULT NULL,
  Remarks varchar(255) DEFAULT NULL,
  IdStudentRegistration bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregistration',
  IdStudentRegSubjects bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregsubjects',
  MarksTotal decimal(10,2) DEFAULT NULL COMMENT 'Jumlah markah keseluruhan kertas soalan (xyah letak kat sini pun xpe refer aje mark distribution letak pun xpe) ',
  TotalMarkObtained decimal(10,2) DEFAULT NULL COMMENT '(Raw Mark) Jumlah markah yang student perolehi',
  FinalTotalMarkObtained decimal(10,2) NOT NULL COMMENT 'ini markah setelah pengiraan percentage component',
  TotalMarkObtainedScaling decimal(10,2) DEFAULT NULL,
  TotalMarkObtainedResit decimal(10,2) DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdStudentMarksEntry),
  KEY IdStudentMarksEntry (IdStudentMarksEntry),
  KEY IdSemester (IdSemester),
  KEY Course (Course),
  KEY ` IdMarksDistributionMaster` (IdMarksDistributionMaster),
  KEY IdStudentRegistration (IdStudentRegistration),
  KEY IdStudentRegSubjects (IdStudentRegSubjects)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_student_marks_entry18112013 (
  IdStudentMarksEntry bigint(20) NOT NULL AUTO_INCREMENT,
  IdSemester varchar(200) DEFAULT NULL,
  Course bigint(20) DEFAULT NULL,
  IdMarksDistributionMaster int(11) NOT NULL COMMENT 'fk tbl_marksdistributionmaster',
  Component bigint(20) DEFAULT NULL,
  ComponentItem bigint(20) DEFAULT NULL,
  AttendanceStatus bigint(20) DEFAULT NULL,
  Instructor bigint(20) DEFAULT NULL,
  MarksEntryStatus bigint(20) DEFAULT NULL,
  ApprovedBy bigint(20) DEFAULT NULL,
  ApprovedOn datetime DEFAULT NULL,
  Remarks varchar(255) DEFAULT NULL,
  IdStudentRegistration bigint(20) DEFAULT NULL,
  IdStudentRegSubjects bigint(20) DEFAULT NULL,
  MarksTotal decimal(10,2) DEFAULT NULL,
  TotalMarkObtained decimal(10,2) DEFAULT NULL,
  TotalMarkObtainedScaling decimal(10,2) DEFAULT NULL,
  TotalMarkObtainedResit decimal(10,2) DEFAULT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  PRIMARY KEY (IdStudentMarksEntry),
  KEY IdStudentMarksEntry (IdStudentMarksEntry),
  KEY IdSemester (IdSemester),
  KEY Course (Course),
  KEY ` IdMarksDistributionMaster` (IdMarksDistributionMaster),
  KEY IdStudentRegistration (IdStudentRegistration),
  KEY IdStudentRegSubjects (IdStudentRegSubjects)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_student_preffered (
  IdStudentPreferred bigint(20) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) NOT NULL,
  IdIntake bigint(20) NOT NULL,
  IdProgramLevel bigint(20) NOT NULL,
  IdPriorityNo bigint(20) NOT NULL,
  IdProgram bigint(20) NOT NULL,
  IdBranch bigint(20) NOT NULL,
  IdScheme bigint(20) NOT NULL,
  CreatedBy bigint(20) NOT NULL,
  CreatedAt datetime NOT NULL,
  PRIMARY KEY (IdStudentPreferred)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_student_release_status (
  IdStudentRelease bigint(10) NOT NULL AUTO_INCREMENT,
  IdStudent bigint(10) NOT NULL,
  IdCategory bigint(10) NOT NULL,
  IdReleaseType bigint(10) NOT NULL,
  IdIntake bigint(10) NOT NULL,
  Reason varchar(200) NOT NULL,
  UpdUser bigint(10) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdStudentRelease)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table to save student release data';

CREATE TABLE tbl_student_status_history (
  IdStudentHistory bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) NOT NULL,
  profileStatus bigint(20) NOT NULL,
  IpAddress varchar(50) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdStudentHistory)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_student_status_history_1162013 (
  IdStudentHistory bigint(20) NOT NULL AUTO_INCREMENT,
  IdStudentRegistration bigint(20) NOT NULL,
  profileStatus bigint(20) NOT NULL,
  IpAddress varchar(50) DEFAULT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdStudentHistory)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_subcredithoursdistrbtn (
  IdSubjcredithoursdistrbtn bigint(20) NOT NULL AUTO_INCREMENT,
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subjectmaster',
  CreditHour decimal(12,2) NOT NULL,
  Idcomponents bigint(20) unsigned NOT NULL COMMENT 'FK TO tbl_definitionms',
  IdcomponentItem bigint(20) NOT NULL,
  PRIMARY KEY (IdSubjcredithoursdistrbtn),
  KEY IdSubject (IdSubject,Idcomponents)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subject (
  IdSubject bigint(11) NOT NULL AUTO_INCREMENT,
  SubjectName text NOT NULL,
  SubjectCode text NOT NULL,
  SubjectCatagory bigint(11) DEFAULT NULL,
  Institution bigint(11) NOT NULL DEFAULT '0',
  UpdUser bigint(11) NOT NULL,
  UpdDate date NOT NULL,
  SubjectCodeFormat varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (IdSubject)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectcoordinatorlist (
  IdSubjectCoordinatorList bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_subjectmaster',
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  FromDate date DEFAULT NULL,
  ToDate date DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (IdSubjectCoordinatorList),
  KEY tbl_subjectcoordinatorlist_ibfk_1 (UpdUser),
  KEY tbl_subjectcoordinatorlist_ibfk_2 (IdSubject),
  KEY tbl_subjectcoordinatorlist_ibfk_3 (IdStaff)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_subjectdetails (
  IdSubjectDetails bigint(20) NOT NULL AUTO_INCREMENT,
  IdSubject bigint(20) NOT NULL,
  SubjectDetailId varchar(100) NOT NULL,
  SubjectDetailDesc varchar(100) NOT NULL,
  UpdDate date NOT NULL,
  UpdUser date NOT NULL,
  PRIMARY KEY (IdSubjectDetails)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectequivalent (
  idequivalent bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idsubject bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_subjectmaster',
  idsubjectequivalent bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_subjectmaster',
  Active binary(1) DEFAULT NULL,
  UpdDate datetime DEFAULT NULL,
  UpdUser bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (idequivalent)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectgradepoint (
  Idsubjectgradepoint bigint(20) NOT NULL AUTO_INCREMENT,
  Idsubjectgradetype bigint(20) NOT NULL,
  subjectgrade varchar(50) NOT NULL,
  subjectgradedescription varchar(100) NOT NULL,
  subjectgradepoint varchar(50) NOT NULL,
  PRIMARY KEY (Idsubjectgradepoint)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectgradetype (
  Idsubjectgradetype bigint(20) NOT NULL AUTO_INCREMENT,
  Iduniversity bigint(20) NOT NULL,
  subjectcodetype varchar(100) NOT NULL,
  subjectcodedescription varchar(100) NOT NULL,
  Idqualification bigint(20) NOT NULL,
  PRIMARY KEY (Idsubjectgradetype)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectmarksentry (
  idSubjectMarksEntry bigint(20) NOT NULL AUTO_INCREMENT,
  IdMarksDistributionDetails bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_marksdistributiondetails',
  `Year` varchar(20) NOT NULL,
  IdSemester int(20) NOT NULL,
  idStaff varchar(20) NOT NULL,
  IdStudentRegistration bigint(20) unsigned NOT NULL,
  idSubject bigint(20) unsigned NOT NULL,
  StudentId varchar(20) NOT NULL,
  Grade varchar(20) DEFAULT NULL,
  subjectmarks decimal(12,2) NOT NULL DEFAULT '0.00',
  SubjectCode varchar(20) NOT NULL,
  Active binary(1) NOT NULL DEFAULT '1' COMMENT '1:Active 0:inactive',
  UpdUser bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT 'FK to tbl_user',
  UpdDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (idSubjectMarksEntry),
  KEY idSubject (StudentId),
  KEY UpdUser (UpdUser),
  KEY idStaff (IdSemester),
  KEY IdStudentRegistration (`Year`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectmaster (
  IdSubject bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdFaculty bigint(20) unsigned NOT NULL,
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  SubjectName varchar(50) DEFAULT NULL,
  ShortName varchar(20) NOT NULL,
  subjectMainDefaultLanguage varchar(100) DEFAULT NULL,
  courseDescription varchar(255) DEFAULT NULL,
  BahasaIndonesia varchar(50) DEFAULT NULL,
  ArabicName varchar(255) DEFAULT NULL,
  SubCode varchar(75) NOT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  CreditHours float NOT NULL,
  AmtPerHour decimal(12,2) NOT NULL DEFAULT '0.00',
  CourseType bigint(20) unsigned NOT NULL DEFAULT '1',
  MinCreditHours int(11) NOT NULL,
  IdDefaultSyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_Syllabus',
  ClassTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With class timetable;0:Without class timetable',
  ExamTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With exam timetable;0:Without exam timetable',
  ReligiousSubject binary(1) NOT NULL DEFAULT '0' COMMENT '1:Religious;0:Non religious',
  IdReligion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definitionms',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  SubCodeFormat varchar(100) DEFAULT NULL,
  IdFormat varchar(60) DEFAULT NULL,
  `Group` int(11) NOT NULL,
  PRIMARY KEY (IdSubject),
  UNIQUE KEY SubCode (SubCode),
  KEY UpdUser (UpdUser),
  KEY IdDepartment (IdDepartment)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_subjectmaster_bkp (
  IdSubject bigint(20) unsigned NOT NULL DEFAULT '0',
  IdFaculty bigint(20) unsigned NOT NULL,
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  SubjectName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  ShortName varchar(20) CHARACTER SET utf8 NOT NULL,
  courseDescription varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  BahasaIndonesia varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  ArabicName varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  SubCode varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  CreditHours float NOT NULL,
  AmtPerHour decimal(12,2) NOT NULL DEFAULT '0.00',
  CourseType bigint(20) unsigned NOT NULL DEFAULT '1',
  MinCreditHours int(11) NOT NULL,
  IdDefaultSyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_Syllabus',
  ClassTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With class timetable;0:Without class timetable',
  ExamTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With exam timetable;0:Without exam timetable',
  ReligiousSubject binary(1) NOT NULL DEFAULT '0' COMMENT '1:Religious;0:Non religious',
  IdReligion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definitionms',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  SubCodeFormat varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  IdFormat varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_subjectmaster_june (
  IdSubject bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdFaculty bigint(20) unsigned NOT NULL,
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  SubjectName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ShortName varchar(20) NOT NULL,
  subjectMainDefaultLanguage varchar(100) DEFAULT NULL,
  courseDescription varchar(255) DEFAULT NULL,
  BahasaIndonesia varchar(50) DEFAULT NULL,
  ArabicName varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  SubCode varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  CreditHours float NOT NULL,
  AmtPerHour decimal(12,2) NOT NULL DEFAULT '0.00',
  CourseType bigint(20) unsigned NOT NULL DEFAULT '1',
  MinCreditHours int(11) NOT NULL,
  IdDefaultSyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_Syllabus',
  ClassTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With class timetable;0:Without class timetable',
  ExamTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With exam timetable;0:Without exam timetable',
  ReligiousSubject binary(1) NOT NULL DEFAULT '0' COMMENT '1:Religious;0:Non religious',
  IdReligion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definitionms',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  SubCodeFormat varchar(100) DEFAULT NULL,
  IdFormat varchar(60) DEFAULT NULL,
  `Group` int(11) NOT NULL,
  PRIMARY KEY (IdSubject),
  KEY UpdUser (UpdUser),
  KEY IdDepartment (IdDepartment)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_subjectmaster_nov12 (
  IdSubject bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdFaculty bigint(20) unsigned NOT NULL,
  IdDepartment bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  SubjectName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ShortName varchar(20) NOT NULL,
  subjectMainDefaultLanguage varchar(100) DEFAULT NULL,
  courseDescription varchar(255) DEFAULT NULL,
  BahasaIndonesia varchar(50) DEFAULT NULL,
  ArabicName varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  SubCode varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Active binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  CreditHours float NOT NULL,
  AmtPerHour decimal(12,2) NOT NULL DEFAULT '0.00',
  CourseType bigint(20) unsigned NOT NULL DEFAULT '1',
  MinCreditHours int(11) NOT NULL,
  IdDefaultSyllabus bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_Syllabus',
  ClassTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With class timetable;0:Without class timetable',
  ExamTimeTable binary(1) NOT NULL DEFAULT '0' COMMENT '1:With exam timetable;0:Without exam timetable',
  ReligiousSubject binary(1) NOT NULL DEFAULT '0' COMMENT '1:Religious;0:Non religious',
  IdReligion bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definitionms',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  SubCodeFormat varchar(100) DEFAULT NULL,
  IdFormat varchar(60) DEFAULT NULL,
  `Group` int(11) NOT NULL,
  PRIMARY KEY (IdSubject),
  KEY UpdUser (UpdUser),
  KEY IdDepartment (IdDepartment)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_subjectprerequisites (
  IdSubjectPrerequisites bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdLandscape int(11) NOT NULL COMMENT ' fk tbl_landscape',
  IdLandscapeSub int(11) NOT NULL COMMENT 'fk tbl_landscapesubject',
  IdLandscapeblocksubject int(11) NOT NULL COMMENT 'fk tbl_landscapeblocksubject',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  IdRequiredSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  PrerequisiteType int(4) NOT NULL,
  PrerequisiteGrade varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  MinCreditHours decimal(12,2) NOT NULL,
  createddt datetime NOT NULL,
  createdby int(11) NOT NULL,
  PRIMARY KEY (IdSubjectPrerequisites),
  KEY IdSubject (IdSubject),
  KEY IdRequiredSubject (IdRequiredSubject)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_subjectprerequisites_june (
  IdSubjectPrerequisites bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdLandscape int(11) NOT NULL COMMENT ' fk tbl_landscape',
  IdLandscapeSub int(11) NOT NULL COMMENT 'fk tbl_landscapesubject',
  IdLandscapeblocksubject int(11) NOT NULL COMMENT 'fk tbl_landscapeblocksubject',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  IdRequiredSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  PrerequisiteType int(4) NOT NULL,
  PrerequisiteGrade varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  MinCreditHours decimal(12,2) NOT NULL,
  createddt datetime NOT NULL,
  createdby int(11) NOT NULL,
  PRIMARY KEY (IdSubjectPrerequisites),
  KEY IdSubject (IdSubject),
  KEY IdRequiredSubject (IdRequiredSubject)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_subjectprogram (
  IdSubjectProgram int(11) NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) NOT NULL COMMENT 'FK to tbl_program',
  IdSubject bigint(20) DEFAULT '0' COMMENT 'FK to tbl_subject',
  Mark bigint(20) NOT NULL,
  UpdUser bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  UpdDate date NOT NULL,
  Active binary(1) NOT NULL COMMENT '1=Active;0=Inactive',
  SubjectNamenew varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  CheckSubject binary(1) NOT NULL COMMENT '1:from Idsubject;0: from SubjectName',
  PRIMARY KEY (IdSubjectProgram)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectregistration (
  IdSubjectRegister bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_program',
  EffectiveDate date NOT NULL,
  AcademicStatus int(11) NOT NULL COMMENT '0=GPA;1=CGPA',
  TerminateStatus bigint(20) NOT NULL COMMENT 'FK to tbl_definitionms',
  Semester int(11) NOT NULL,
  VirtualSemester int(11) NOT NULL,
  Min float NOT NULL,
  Max float NOT NULL,
  Subjects int(11) NOT NULL,
  UpdUser bigint(20) NOT NULL COMMENT 'FK to tbl_users',
  UpdDate date NOT NULL,
  Active binary(1) NOT NULL COMMENT '0=Inactive;1=Active',
  PRIMARY KEY (IdSubjectRegister)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectsoffered (
  IdSubjectsOffered bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  Branch int(11) NOT NULL,
  MinQuota int(11) NOT NULL,
  MaxQuota int(11) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  PRIMARY KEY (IdSubjectsOffered),
  KEY IdSemester (IdSemester),
  KEY IdSubject (IdSubject),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_subjectstaffverification (
  IdSubjectStaffVerification bigint(20) NOT NULL AUTO_INCREMENT,
  IdStaff bigint(20) unsigned NOT NULL DEFAULT '0',
  IdSemester bigint(20) unsigned NOT NULL,
  IdSubject bigint(20) NOT NULL COMMENT 'FK to tbl_subject',
  SameLecturer binary(1) NOT NULL,
  Active binary(1) NOT NULL COMMENT '1:Active;0:Inacive',
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate date NOT NULL,
  PRIMARY KEY (IdSubjectStaffVerification)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_subjectwithdrawalpolicy (
  IdSubjectWithdrawalPolicy bigint(20) NOT NULL AUTO_INCREMENT,
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subject',
  Days bigint(20) unsigned NOT NULL,
  Percentage float NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  UpdDate datetime NOT NULL,
  Active binary(1) NOT NULL,
  PRIMARY KEY (IdSubjectWithdrawalPolicy),
  KEY IdSemester (IdSemester),
  KEY IdSubject (IdSubject),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_templandscape (
  IdLandscapetemp bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  LandscapeType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdStartSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_semester',
  SemsterCount int(5) NOT NULL DEFAULT '0',
  Blockcount int(11) DEFAULT NULL,
  Active int(11) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `Unicode` int(11) NOT NULL,
  session_id varchar(200) NOT NULL,
  deleteFlag int(11) NOT NULL,
  PRIMARY KEY (IdLandscapetemp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_templandscapeblock (
  IdLandscapetempblock int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  blockname varchar(100) NOT NULL,
  CreditHours int(11) NOT NULL,
  session_id varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  deleteFlag int(11) NOT NULL,
  PRIMARY KEY (IdLandscapetempblock)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_templandscapeblocksemester (
  IdLandscapetempblocksemester int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  semesterid bigint(20) unsigned NOT NULL,
  session_id varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  deleteFlag int(11) NOT NULL,
  PRIMARY KEY (IdLandscapetempblocksemester)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_templandscapeblocksubject (
  IdLandscapetempblocksubject int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape bigint(20) unsigned NOT NULL,
  blockid bigint(20) unsigned NOT NULL,
  subjectid bigint(20) unsigned NOT NULL,
  coursetypeid int(11) DEFAULT NULL,
  session_id varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  deleteFlag int(11) NOT NULL,
  PRIMARY KEY (IdLandscapetempblocksubject)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_templandscapeblockyear (
  IdLandscapetempblockyear int(11) NOT NULL AUTO_INCREMENT,
  IdLandscape int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  YearSemester int(11) NOT NULL,
  session_id varchar(200) NOT NULL,
  deleteFlag int(11) NOT NULL,
  PRIMARY KEY (IdLandscapetempblockyear)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_templandscapesubject (
  IdTempLandscapeSub bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  IdSubject bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  SubjectType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  IdSemester bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  CreditHours int(3) NOT NULL,
  Compulsory binary(1) NOT NULL DEFAULT '0' COMMENT '0: Not Compulsory, 1: Compulsory',
  Active int(11) NOT NULL COMMENT '1: Active 0:Inactive',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `unicode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(75) NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL DEFAULT '1',
  IDProgramMajoring bigint(20) unsigned NOT NULL,
  PRIMARY KEY (IdTempLandscapeSub),
  KEY IdProgram (IdProgram),
  KEY IdSubject (IdSubject),
  KEY SubjectType (SubjectType),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_tempplacementtestcomponent (
  Idtemp bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdPlacementTestComponent bigint(20) unsigned NOT NULL,
  IdPlacementTest bigint(20) unsigned NOT NULL,
  ComponentName varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  ComponentWeightage int(5) NOT NULL,
  ComponentTotalMarks int(5) NOT NULL,
  sessionId varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL DEFAULT '1',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  PRIMARY KEY (Idtemp),
  KEY IdPlacementTestComponent (IdPlacementTestComponent),
  KEY IdPlacementTest (IdPlacementTest),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_tempprogramaccreditiondetails (
  IdTempProgramAccreditionDetails bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  AccredictionDate date DEFAULT NULL,
  AccredictionReferences varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl1 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl2 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl3 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl4 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl5 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl6 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl7 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl8 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl9 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl10 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl11 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl12 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl13 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl14 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl15 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl16 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl17 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl18 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl19 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccDtl20 varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  AccreditionType bigint(20) NOT NULL COMMENT 'fk to tbl_definitonms',
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL,
  PRIMARY KEY (IdTempProgramAccreditionDetails),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_tempprogramcharges (
  IdTemp bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_program',
  IdCharges bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_charges',
  Charges decimal(12,2) NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  `unicode` int(11) NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  deleteflag int(11) NOT NULL,
  PRIMARY KEY (IdTemp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_tempprogramentryrequirement (
  IdTemp bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `Desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Item bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  Unit bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Condition` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Value` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Validity varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  EntryLevel bigint(20) NOT NULL,
  IdSpecialization bigint(20) unsigned NOT NULL,
  Mandatory binary(1) NOT NULL COMMENT '1: Yes 0:No',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `unicode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  GroupId varchar(100) NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (IdTemp),
  KEY tbl_programentryrequirement_ibfk_1 (UpdUser),
  KEY tbl_programentryrequirement_ibfk_3 (Item),
  KEY tbl_programentryrequirement_ibfk_4 (Unit),
  KEY tbl_programentryrequirement_ibfk_5 (`Condition`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_tempprogramquota (
  IdTemp int(11) NOT NULL AUTO_INCREMENT,
  IdProgramQuota bigint(20) unsigned NOT NULL COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  IdQuota bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_definationms',
  Quota varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  UpdDate datetime NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL,
  PRIMARY KEY (IdTemp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_tempprogramrequirement (
  IdTempProgramReq bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  IdProgram bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  SubjectType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  CreditHours int(3) NOT NULL,
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `unicode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (IdTempProgramReq),
  KEY tbl_programrequirement_ibfk_1 (UpdUser),
  KEY tbl_programrequirement_ibfk_3 (IdProgram),
  KEY tbl_programrequirement_ibfk_4 (SubjectType)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tbl_tempstaffsubject (
  idTempStaffSubject bigint(20) NOT NULL AUTO_INCREMENT,
  IdSubject bigint(20) NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL,
  PRIMARY KEY (idTempStaffSubject)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_tempstudenteducationdetails (
  IdTemp bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  InstitutionName varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UniversityName varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  StudyPlace varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  MajorFiledOfStudy varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  YearOfStudyFrom varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  YearOfStudyTo varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  DegreeType bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definationms ( Degree Type )',
  GradeOrCGPA varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UpdUser bigint(20) NOT NULL,
  UpdDate datetime NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(100) NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL,
  TypeOfSchool int(11) DEFAULT NULL,
  StatusOfSchool int(11) DEFAULT NULL,
  TypeOfStudent bigint(20) DEFAULT NULL COMMENT '1: international 0 : Local',
  HomeTownSchool varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  CreditTransferFrom varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  SubjectId bigint(20) NOT NULL COMMENT 'FK to tbl_subjectmaster',
  SubjectMark bigint(20) NOT NULL,
  PRIMARY KEY (IdTemp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tbl_tempsubjectprerequisites (
  IdTempSubjectPrerequisites bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdRequiredSubject bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subject master',
  PrerequisiteType int(4) NOT NULL,
  PrerequisiteGrade varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  sessionId varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  idExists bigint(20) NOT NULL,
  deleteFlag binary(1) NOT NULL,
  PRIMARY KEY (IdTempSubjectPrerequisites)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_termsandconditions (
  idTermsandconditions bigint(20) NOT NULL AUTO_INCREMENT,
  Termname varchar(100) NOT NULL,
  Description varchar(255) DEFAULT NULL,
  `Default` tinyint(1) NOT NULL DEFAULT '1',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (idTermsandconditions)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_top_menu (
  tm_id int(11) NOT NULL AUTO_INCREMENT,
  tm_name varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  tm_desc text COLLATE utf8_unicode_ci,
  tm_seq_order int(11) NOT NULL DEFAULT '0',
  tm_visibility tinyint(11) NOT NULL DEFAULT '1' COMMENT '1:show 0:hide',
  PRIMARY KEY (tm_id),
  KEY tm_id (tm_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_universitymaster (
  IdUniversity bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  Univ_Name varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Univ_ArabicName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  ShortName varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Add1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Add2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  City varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  State bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_state',
  Country bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_countries',
  Zip varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Phone1 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Phone2 varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Fax varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  Email varchar(50) CHARACTER SET utf8 COLLATE utf8_turkish_ci DEFAULT NULL,
  Url varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  Active binary(1) NOT NULL COMMENT '0:Inactive, 1:Active',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (IdUniversity),
  KEY State (State),
  KEY Country (Country),
  KEY UpdUser (UpdUser)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_user (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  iduser bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_staff',
  loginName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  passwd varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  userArabicName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  lName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  mName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  fName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  DOB date NOT NULL,
  gender tinyint(1) NOT NULL DEFAULT '1',
  addr1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  addr2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  city varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  state varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  country varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  zipCode varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  homePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  workPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  cellPhone varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  email varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdRole bigint(20) unsigned DEFAULT NULL,
  NoOfLoginAttempt tinyint(3) unsigned DEFAULT '0',
  LastLogAttemptOn datetime DEFAULT NULL,
  LockStatus binary(1) NOT NULL DEFAULT '0' COMMENT '0:Unlock, 1 :Lock',
  UserStatus binary(1) NOT NULL COMMENT '1:Active, 0:Inactive',
  PRIMARY KEY (iduser),
  KEY mName (mName)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_user_ori (
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) DEFAULT NULL,
  iduser bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdStaff bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_staff',
  loginName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  passwd varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  userArabicName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  lName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  mName varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  fName varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  NameField1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField3 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField4 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  NameField5 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  DOB date NOT NULL,
  gender tinyint(1) NOT NULL DEFAULT '1',
  addr1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  addr2 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  city varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  state varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  country varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  zipCode varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  homePhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  workPhone varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  cellPhone varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  email varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  IdRole bigint(20) unsigned DEFAULT NULL,
  NoOfLoginAttempt tinyint(3) unsigned DEFAULT '0',
  LastLogAttemptOn datetime DEFAULT NULL,
  LockStatus binary(1) NOT NULL DEFAULT '0' COMMENT '0:Unlock, 1 :Lock',
  UserStatus binary(1) NOT NULL COMMENT '1:Active, 0:Inactive',
  PRIMARY KEY (iduser),
  KEY mName (mName)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tbl_varifiedprogramchecklist (
  IdVarifiedProgramChecklist bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL,
  IdCheckList bigint(20) unsigned NOT NULL,
  IdProgram bigint(20) unsigned NOT NULL,
  Varified tinyint(1) DEFAULT '0',
  `Status` text NOT NULL,
  Comments varchar(100) DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdVarifiedProgramChecklist)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_verifiermarks (
  idVerifierMarks bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idSubjectMarksEntry bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subjectsmarksentry',
  idverifier bigint(20) DEFAULT NULL,
  Rank int(20) DEFAULT NULL,
  verifiresubjectmarks decimal(12,2) NOT NULL DEFAULT '0.00',
  UpdDate datetime NOT NULL,
  UpdUser bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  Active binary(1) NOT NULL,
  PRIMARY KEY (idVerifierMarks)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_verifypayments (
  IdVerifyPayments bigint(10) NOT NULL AUTO_INCREMENT,
  IdApplication bigint(20) unsigned NOT NULL,
  IdSemester bigint(20) unsigned NOT NULL,
  VerifyPayment binary(1) NOT NULL COMMENT '0: Not-Verified,1:Verified',
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  PRIMARY KEY (IdVerifyPayments)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tbl_voucherdetails (
  idvoucherdetails bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  idvoucher bigint(20) unsigned NOT NULL COMMENT 'References tbl_vouchermaster',
  amount float(15,2) NOT NULL,
  description varchar(255) NOT NULL,
  PRIMARY KEY (idvoucherdetails),
  KEY idvoucher (idvoucher)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_vouchermaster (
  idvoucher bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  Voucherdate date NOT NULL DEFAULT '0000-00-00',
  Vouchertype int(11) NOT NULL,
  Vouchernumber bigint(50) NOT NULL,
  idstudent bigint(20) NOT NULL,
  approved binary(1) NOT NULL DEFAULT '0',
  Fromdate date NOT NULL DEFAULT '0000-00-00',
  Todate date NOT NULL DEFAULT '0000-00-00',
  Upddate datetime NOT NULL,
  Upduser bigint(20) NOT NULL,
  PRIMARY KEY (idvoucher)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tbl_vouchertype (
  idVoucherType bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  VoucherType varchar(255) NOT NULL,
  VoucherCode varchar(50) NOT NULL,
  Description varchar(255) DEFAULT NULL,
  UpdUser bigint(20) unsigned NOT NULL,
  UpdDate datetime NOT NULL,
  Active tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active,0-Inactive',
  PRIMARY KEY (idVoucherType)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE tpa_component_test (
  id int(11) NOT NULL AUTO_INCREMENT,
  aph_id int(11) NOT NULL COMMENT 'appl_placement_head',
  ac_id int(11) NOT NULL COMMENT 'appl_component',
  tp_id int(11) NOT NULL COMMENT 'tpa_konversi',
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tpa_konversi (
  tk_id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (tk_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE tpa_konversi_detl (
  id int(11) NOT NULL AUTO_INCREMENT,
  tk_id int(11) NOT NULL,
  mark int(11) NOT NULL,
  konversi int(11) NOT NULL,
  PRIMARY KEY (id),
  KEY tk_id (tk_id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


ALTER TABLE `appl_placement_detl`
  ADD CONSTRAINT appl_placement_detl_ibfk_1 FOREIGN KEY (apd_placement_code) REFERENCES appl_placement_head (aph_placement_code) ON DELETE CASCADE,
  ADD CONSTRAINT appl_placement_detl_ibfk_2 FOREIGN KEY (apd_comp_code) REFERENCES appl_component (ac_comp_code);

ALTER TABLE `email_template_detl`
  ADD CONSTRAINT email_template_detl_ibfk_1 FOREIGN KEY (etd_eth_id) REFERENCES email_template_head (eth_id),
  ADD CONSTRAINT email_template_detl_ibfk_2 FOREIGN KEY (etd_language) REFERENCES applicant_language (al_id);

ALTER TABLE `exam_group`
  ADD CONSTRAINT exam_group_ibfk_1 FOREIGN KEY (eg_room_id) REFERENCES appl_room (av_id);

ALTER TABLE `exam_group_attendance`
  ADD CONSTRAINT exam_group_attendance_ibfk_1 FOREIGN KEY (ega_eg_id) REFERENCES exam_group (eg_id);

ALTER TABLE `exam_group_supervisor`
  ADD CONSTRAINT exam_group_supervisor_ibfk_1 FOREIGN KEY (egs_eg_id) REFERENCES exam_group (eg_id);

ALTER TABLE `sis_setup_detl`
  ADD CONSTRAINT sis_setup_detl_ibfk_1 FOREIGN KEY (ssd_code) REFERENCES sis_setup_head (ssh_code);

ALTER TABLE `sis_setup_detl_612`
  ADD CONSTRAINT sis_setup_detl_612_ibfk_1 FOREIGN KEY (ssd_code) REFERENCES sis_setup_head (ssh_code);

ALTER TABLE `tbl_accountmaster`
  ADD CONSTRAINT FK_tbl_AccountMaster_AccountCreatedBy FOREIGN KEY (AccountCreatedBy) REFERENCES tbl_user (iduser),
  ADD CONSTRAINT FK_tbl_AccountMaster_UpdUser FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser);

ALTER TABLE `tbl_agentmaster`
  ADD CONSTRAINT tbl_agentmaster_ibfk_1 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser);

ALTER TABLE `tbl_agentprogram`
  ADD CONSTRAINT tbl_agentprogram_ibfk_1 FOREIGN KEY (IdProgram) REFERENCES tbl_program (IdProgram);

ALTER TABLE `tbl_appealmarksentry`
  ADD CONSTRAINT tbl_appealmarksentry_ibfk_1 FOREIGN KEY (IdAppeal) REFERENCES tbl_appeal (IdAppeal);

ALTER TABLE `tbl_awardlevel`
  ADD CONSTRAINT tbl_awardlevel_ibfk_1 FOREIGN KEY (IdLevel) REFERENCES tbl_definationms (idDefinition),
  ADD CONSTRAINT tbl_awardlevel_ibfk_2 FOREIGN KEY (IdAllowanceLevel) REFERENCES tbl_definationms (idDefinition),
  ADD CONSTRAINT tbl_awardlevel_ibfk_3 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser);

ALTER TABLE `tbl_branchofficevenue`
  ADD CONSTRAINT tbl_branchofficevenue_ibfk_3 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT tbl_branchofficevenue_ibfk_4 FOREIGN KEY (idCountry) REFERENCES tbl_countries (idCountry) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT tbl_branchofficevenue_ibfk_6 FOREIGN KEY (idCountry) REFERENCES tbl_countries (idCountry) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT tbl_branchofficevenue_ibfk_7 FOREIGN KEY (idState) REFERENCES tbl_state (idState) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_branchregistrationmap`
  ADD CONSTRAINT br_fk_rl FOREIGN KEY (IdBranch) REFERENCES tbl_branchofficevenue (IdBranch);

ALTER TABLE `tbl_chiefofprogramlist`
  ADD CONSTRAINT tbl_chiefofprogramlist_ibfk_1 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser),
  ADD CONSTRAINT tbl_chiefofprogramlist_ibfk_2 FOREIGN KEY (IdProgram) REFERENCES tbl_program (IdProgram);

ALTER TABLE `tbl_collegemaster`
  ADD CONSTRAINT tbl_collegemaster_ibfk_10 FOREIGN KEY (Country) REFERENCES tbl_countries (idCountry),
  ADD CONSTRAINT tbl_collegemaster_ibfk_11 FOREIGN KEY (AffiliatedTo) REFERENCES tbl_universitymaster (IdUniversity),
  ADD CONSTRAINT tbl_collegemaster_ibfk_12 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser),
  ADD CONSTRAINT tbl_collegemaster_ibfk_13 FOREIGN KEY (State) REFERENCES tbl_state_2012120xx (idState) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_collegemasterbak`
  ADD CONSTRAINT FK_tbl_collegemaster_ibfk_1 FOREIGN KEY (AffiliatedTo) REFERENCES tbl_universitymaster (IdUniversity),
  ADD CONSTRAINT FK_tbl_collegemaster_ibfk_2 FOREIGN KEY (Country) REFERENCES tbl_countries (idCountry),
  ADD CONSTRAINT FK_tbl_collegemaster_ibfk_3 FOREIGN KEY (State) REFERENCES tbl_state_2012120xx (idState),
  ADD CONSTRAINT FK_tbl_collegemaster_ibfk_4 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser);

ALTER TABLE `tbl_coursemaster`
  ADD CONSTRAINT tbl_coursemaster_ibfk_1 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser);

ALTER TABLE `tbl_coursetype`
  ADD CONSTRAINT tbl_coursetype_ibfk_1 FOREIGN KEY (UpdUser) REFERENCES tbl_user (iduser);

ALTER TABLE `tbl_coursetypedetails`
  ADD CONSTRAINT tbl_coursetypedetails_ibfk_1 FOREIGN KEY (IdCourseType) REFERENCES tbl_coursetype (IdCourseType);
