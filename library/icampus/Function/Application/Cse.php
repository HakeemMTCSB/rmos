<?php 
class icampus_Function_Application_Cse extends Zend_View_Helper_Abstract{
	
	public function generate($txnId)
	{
		$invoiceClass = new icampus_Function_Studentfinance_Invoice();    
		$translate = Zend_Registry::get('Zend_Translate');
		//echo 'lalala'; exit;
		$invoice = $invoiceClass->getApplicantProformaInfo($txnId, $level=0, $fee_category_id=null);   
		//if($invoice['proforma_invoice']['total_amount']!=0){  
			//$processing_fee = $invoice['proforma_invoice']['currency']['cur_code'].' '.$invoice['proforma_invoice']['total_amount'];
		//}else{
			$processing_fee = '';
		//}
		
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $appProfileDB->getProfileProgram($txnId);

		$templateDB = new App_Model_General_DbTable_EmailTemplate();
		$templateData = $templateDB->getEmailTemplate(592,$applicant['ap_prog_scheme'],$applicant['appl_category']); //entry
		
		if ( empty($templateData) )
		{
			throw new Exception('No template found');
		}

		//get the email attachment
		$attachment = $templateDB->getEmailAttachment($templateData['stp_Id'],$locale='en_US');	

                //var_dump($attachment); exit;
		if ( empty($attachment) )
		{
			throw new Exception('Invalid Template Attachment');
		}
		else
		{
			/*$proformakey = 0;
			foreach ( $attachment as $key => $atth )
			{
				if ( $atth['tpl_name'] == 'Proforma Invoice' )
				{
					$proformakey = $key;
				}
			}
			
			unset($attachment[$proformakey]);*/
			
                        ///var_dump($attachment);
			$attachment_info = $this->generateAttachment($attachment,$applicant,$processing_fee);
		}
	}

	function generateAttachment($attachment_list,$applicant,$processing_fee)
	{
        
		$cmsTemplateTags = new Cms_TemplateTags();
		
		//print_r($applicant);exit;
		$attachment_info = array();
                
		foreach($attachment_list as $index=>$attachment)
		{
			$template = $attachment['tpl_content'];
						
			//$templateReplace = $this->templateTags($template,$applicant,$processing_fee);
			$templateReplace = $cmsTemplateTags->parseContent($applicant['at_trans_id'], $attachment['tpl_id'], $template);
			
			//output filename 
			$attachment_filename = $applicant["at_pes_id"].'_'.$attachment['typeName'].'_'.date('YmdHis');
			
			$option = array(
			'content' => $templateReplace,
			'save' => false,
			'file_name' => $attachment_filename,
			'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}',
							
						'header' => '<script type="text/php">
						if ( isset($pdf) ) {
							$header = $pdf->open_object();

							$w = $pdf->get_width();
							$h = $pdf->get_height();
							$color = array(0,0,0);

							$img_w = 180; 
							$img_h = 59;
							$pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

							// Draw a line along the bottom
							$font = Font_Metrics::get_font("Helvetica");
							$size = 6;
							$text_height = Font_Metrics::get_font_height($font, $size)+2;
							$y = $h - (3.5 * $text_height)-10;
							$pdf->line(10, $y, $w - 10, $y, $color, 1);

					// Draw a second line along the bottom
							$y = $h - (3.5 * $text_height)+10;
							$pdf->line(10, $y, $w - 10, $y, $color, 1);

							$pdf->close_object();

							$pdf->add_object($header, "all");
						}
						</script>',
					'footer' => '<script type="text/php">
						if ( isset($pdf) ) {
							$footer = $pdf->open_object();

							$font = Font_Metrics::get_font("Helvetica");
							$size = 6;
							$color = array(0,0,0);
							$text_height = Font_Metrics::get_font_height($font, $size)+2;

							$w = $pdf->get_width();
							$h = $pdf->get_height();


							// Draw a line along the bottom
							$y = $h - (3.5 * $text_height)-10;
							//$pdf->line(10, $y, $w - 10, $y, $color, 1);

							//1st row footer
							$text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
							$width = Font_Metrics::get_text_width($text, $font, $size);	
							$y = $h - (2 * $text_height)-20;
							$x = ($w - $width) / 2.0;

							$pdf->page_text($x, $y, $text, $font, $size, $color);

							//2nd row footer
							$text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
							$width = Font_Metrics::get_text_width($text, $font, $size);	
							$y = $h - (1 * $text_height)-20;
							$x = ($w - $width) / 2.0;

							$pdf->page_text($x, $y, $text, $font, $size, $color);

						   

							$pdf->close_object();

							$pdf->add_object($footer, "all");
						}
						</script>' 	
		);
		
		$pdf = generatePdf($option);
                
                $modelApp = new Application_Model_DbTable_ApplicantApproval();
                
                $auth = Zend_Auth::getInstance();
                $userId = $auth->getIdentity()->iduser;
                
                //store history generate pdf
                $dataGeneratePdf = array(
                    'tgd_txn_id' => $applicant['at_trans_id'],
                    'tgd_filename' => $attachment_filename.'.pdf',
                    'sth_id'=> $attachment['sth_Id'],
                    'tgd_fileurl' =>$applicant['at_repository'].'/'.$attachment_filename.'.pdf',
                    'tgd_updDate' => date('Y-m-d H:i:s'),
                    'tgd_updUser' => $userId
                );
                $modelApp->storePdfDocGenHistory($dataGeneratePdf);
                
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'Proforma Succesfully Regenerate'));
                $this->_redirect($this->baseUrl . '/application/index/edit/id/'.$txnId.'/idSection/1');
		
			
		}//end foreach
					
		return $attachment_info;
	}
	
	function templateTags($template,$applicant,$processing_fee)
	{
		$address = $applicant["appl_address1"];
		if($applicant["appl_address2"]){
			$address.='<br>'.$applicant["appl_address2"];
		}
		
		if($applicant["appl_postcode"]){
			$address.='<br>'.$applicant["appl_postcode"].' ';
		}
		
		if($transInfo["CityName"]){
                $address.= $transInfo["CityName"];
                }else{
                    $address.= $transInfo["appl_city_others"];
                }

                if($transInfo["StateName"]){
                        $address.=', '.$transInfo["StateName"];
                }else{
                    $address.= $transInfo["appl_state_others"];
                }
		
		$address.= '<br>'.$applicant["CountryName"];
		
		$template = str_replace("[Applicant Name]",$applicant["appl_fname"].' '.$applicant["appl_lname"],$template);
		$template = str_replace("[Programme]",$applicant['program_name'],$template);
		$template = str_replace("[Applicant ID]",$applicant['at_pes_id'],$template);
		$template = str_replace("[NRIC]",$applicant['appl_idnumber'],$template);
		$template = str_replace("[Date]",date('d-m-Y'),$template);
		$template = str_replace("[Applicant Address]",strtoupper($address),$template);
		$template = str_replace("[Proforma]",$processing_fee,$template);
		$template = str_replace("[Mode of Study]",$applicant['StudyMode'],$template);
		$template = str_replace("[Applicant Email]",$applicant['appl_email'],$template);
		$template = str_replace("[Applicant Password]",$applicant['appl_password'],$template);
		$template = str_replace("[Salutation]",$applicant['Salutation'],$template);
		$template = str_replace("[Semester]",$applicant['Semester'],$template);
		$template = str_replace("[Mode of Programme]",$applicant['ProgramMode'],$template);
		$template = str_replace("[Intake]",$applicant['Semester'],$template);
		
		
        $programSchemeId = $this->getApplicantProgram($applicant['ap_at_trans_id']);
        $programId = $this->getApplicantProgramId($programSchemeId);
        // $programId = 2;                    
        if($programId == 2)
            $level = 0;
        else 
            $level = 1;
        
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
		$invoice = $invoiceClass->getApplicantProformaInfo($applicant['at_trans_id'], $level, null);
			
		if ( isset($invoice['proforma_invoice_info'][0]['bill_number']) )
		{
			$template = str_replace("[Proforma Ref No]",$invoice['proforma_invoice_info'][0]['bill_number'],$template);
		}
       
        //pre assign if not found
        
        foreach ($invoice['proforma_invoice_info'] as $key => $value)
        {
            if(isset($value['fee_category']))
            {
                if($value['fee_category'] == 2)
                {
                    $template = str_replace("[Fee - Application Processing]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 8)
                {
                    $template = str_replace("[Fee - Initial Tuition]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 5)
                {
                    $template = str_replace("[Fee - Student Union]",$value['bill_amount'],$template);
                }elseif($value['fee_category'] == 6)
                {
                    $template = str_replace("[Fee - Resource]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 3)
                {
                    $template = str_replace("[Fee - Personal Bond]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 4)
                {
                    $template = str_replace("[Fee - Security Deposit]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 12)
                {
                    $template = str_replace("[Fee - Student Pass Visa]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 9)
                {
                    $template = str_replace("[Fee - Insurance]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 13)
                {
                    $template = str_replace("[Fee - iKad]",$value['bill_amount'],$template);
                }
                elseif($value['fee_category'] == 11)
                {
                    $template = str_replace("[Fee - Medical Check-up]",$value['bill_amount'],$template);
                }
            }
        }
       
        $model = new App_Model_Application_DbTable_Additional();
		$requiredDocList = $model->getRequiredDocument($applicant['appl_category'], $applicant['ap_prog_scheme']);
		$financeDocList = $model->getFinanceDocument($applicant['appl_category'], $applicant['ap_prog_scheme']);
		
		$requiredDoc = '';
		$i = 1;
		if ($financeDocList){
			$requiredDoc .= $i.') Finance Document<br/><br/>';
			$i++;
			$requiredDoc .= '<ul>';
			foreach ($financeDocList as $financeDocLoop){
				$requiredDoc .= '<li>'.$financeDocLoop['dcl_desc'].'</li>';
			}
			$requiredDoc .= '</ul><br/>';
		}
		
		if ($requiredDocList){
			foreach ($requiredDocList as $requiredDocLoop){
				$requiredDoc .= $i.') '.$requiredDocLoop['dcl_desc'].'<br/><br/>';
				$i++;
			}
			
			$template = str_replace("[Required Documents]",$requiredDoc,$template);
		}
		
		return $template;		
	}
    
    public function getApplicantProgram($trans_id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
                       ->from('applicant_program')
                       ->where('ap_at_trans_id =?',(int)$trans_id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        
        return $larrResult['ap_prog_scheme'];
    }
    public function getApplicantProgramId($scheme)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
                       ->from('tbl_program_scheme')
                       ->where('IdProgramScheme =?',(int)$scheme);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        
        return $larrResult['IdProgram'];
    }
}