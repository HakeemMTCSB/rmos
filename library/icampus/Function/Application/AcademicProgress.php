<?php 
class icampus_Function_Application_AcademicProgress extends Zend_View_Helper_Abstract{
    
	function getExpectedGraduateStudent($idStudentRegistration,$return=null){
		 	
			
				//get student info
				$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
				$student = $studentRegistrationDB->fetchStudentHistoryDetails($idStudentRegistration);
		
                $recordDb = new Records_Model_DbTable_Academicprogress(); 
                $courses = $recordDb->getAllregistered($idStudentRegistration);
                
                //get landscape info
				$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
				$landscape = $landscapeDB->getLandscapeDetails($student['IdLandscape']);
				
				$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
				$programRequirement = $progReqDB->getlandscapecoursetype($student['IdProgram'],$student['IdLandscape']);
			
                 
                if($landscape['LandscapeType']==43){
	                $compulsory['completed'] = array();
	                $compulsory['registered'] = array();
	                $compulsory['open'] = array();
	                
	                $elective['completed'] = array();
	                $elective['registered'] = array();
	                $elective['open'] = array();
                    $research = array();
                    
                }else{
                	
	                $compulsory = array();
	                $elective = array();
	                $research = array();
                }
                
                $subject_ids = array();
                           
                $z = 0;
                $p = 0;
                $q = 0;
                $r = 0;
                $countCtCredit = 0;
                $countExCredit = 0;
                $countCompletePaperCore = 0;
                $countCompletePaperElective = 0;
                $countCompleteCreditHoursCore = 0;
                $countCompleteCreditHoursElective = 0;
                $countCreditHoursCore = 0;
                $countCreditHoursElective = 0;
                $cum_elective_ch=0;
                $researchGred = '-';
                $researchCreditHours = 0;
                $researchRemarks = '';
                $countCreditHoursResearch = 0;
                $total_paper_taken = 0;
                //$countCreditHoursResearch = 0;
                $reminaingPaperMIFP=0;
                $countPapersTaken = 0;
                $ch2=0;
                
                $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                
                //get total no of compulsory subject
                
                
                //echo "<pre>";print_r($courses);echo "</pre>";
                //var_dump(count($courses)); //exit;
                foreach($courses as $a => $course)
                {
                    $subsem = $recordDb->getSemesterById($course['IdSemesterMain']);
                    
                    $publishresult = false;
                    
                    if ($subsem){
                        $publishInfo = $recordDb->getPublishResult($student['IdProgram'], $subsem['IdSemesterMaster']);
                        
                        if ($publishInfo){
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                            $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));
                            
                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                $publishresult = true;
                            }
                        }
                    }
                    
                    if ($return=='rpaper' || $return=='caudit'){
                        $publishresult = true;
                    }
                    
                	$equivid = false;
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($student['IdProgram'],$student['IdLandscape'],$course['IdSubject']);
                    if(!$course_status){
            			$equivid=$recordDb->checkEquivStatus($student['IdLandscape'],$course["IdSubject"]);
            	
		            	if($equivid){
		            		$course_status = $landscapeSubjectDb->getCommonSubjectStatus($student['IdProgram'],$student['IdLandscape'],$equivid);
		            	}
           			}
                                
                                
                    $equivsubid=$recordDb->checkEquivStatus($student['IdLandscape'],$course["IdSubject"]);
                    
                    if ($equivsubid){
                        $subject_ids[$q] = $equivsubid;
                        $q++;
                    }
                                
                    //$getRegisteredCompleteCourses;
                    if($landscape['LandscapeType']==43){
                            //var_dump($course);
                            //var_dump($course_status);
	                    if($course_status['DefinitionDesc'] == "Core")
	                    {
                                //var_dump($course);
	                        //$completed=$recordDb->checkcompleted($studentdetails['IdStudentRegistration'],$course["IdSubject"]);
	                    	//$others=$recordDb->checkregistered($studentdetails['IdStudentRegistration'],$course["IdSubject"]);
	                        if($course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U")//Complete
	                        {
	                        	//echo "<pre>";print_r($course);echo "</pre><hr>";
	                      		$compulsory['completed'][$z] = $course;
	                      		$compulsory['completed'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
	                        	if($course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U"){
	                                $compulsory['completed'][$z]['grademark'] = $landscape['AssessmentMethod']!='point' ? $course["exam_status"]:$course["grade_name"];
	                            }
                                    
                                    if ($course["grade_name"]=="U"){
                                        $compulsory['completed'][$z]['CreditHours'] = 0;
                                    }
                                    
                                    if ($course['exam_status']=='CT'){
                                        $countCtCredit = $course['CreditHours']+$countCtCredit;
                                    }else if ($course['exam_status']=='EX'){
                                        $countExCredit = $course['CreditHours']+$countExCredit;
                                    }
	                            
	                         	if($course["grade_name"]!="F"){ //Fail Masuk dlm list tapi not completed
	                          	    $subject_ids[$q] = $course['IdSubject'];
	                         		if($equivid){
	                    				$subject_ids[$q] = $equivid;
	                   				}
                                                        
	                                $q++;
                                        
	                            }
                                    
                                    //count paper/credit hourse
                                    /*
                                     * $programRequirementLoop['RequirementType']==1 ==>Credit hour else paper*/
	                            
                                    if ($course["exam_status"]!="U" && $course["grade_name"]!="F"){
                                        foreach ($programRequirement as $programRequirementLoop){
                                            if ($programRequirementLoop['SubjectType'] == 394){
                                                if ($programRequirementLoop['RequirementType']==1){
                                                	
                                                    $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                    $countCreditHoursCore = $ch+$countCreditHoursCore;
                                                    
                                                }else{
                                                    $countCreditHoursCore++;
                                                }
                                            }
                                        }
                                        
                                        $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                        $course['mark_approval_status'] = 2;
                                        if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                            $countCompleteCreditHoursCore = $countCompleteCreditHoursCore+$ch2;
                                            $countCompletePaperCore++;
                                        }
                                    }
                                    
	                            $z++;
	                        }else if ($course["exam_status"]=="C"){
                                    $compulsory['completed'][$z] = $course;
	                      		$compulsory['completed'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
	                        	if($course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U"){
	                                $compulsory['completed'][$z]['grademark'] = $landscape['AssessmentMethod']!='point' ? $course["exam_status"]:$course["grade_name"];
	                            }
                                    
                                    if ($course["grade_name"]=="U"){
                                        $compulsory['completed'][$z]['CreditHours'] = 0;
                                    }
                                    
                                    if ($course['exam_status']=='CT'){
                                        $countCtCredit = $course['CreditHours']+$countCtCredit;
                                    }else if ($course['exam_status']=='EX'){
                                        $countExCredit = $course['CreditHours']+$countExCredit;
                                    }
	                            
	                         	if($course["grade_name"]!="F"){ //Fail Masuk dlm list tapi not completed
	                          	    $subject_ids[$q] = $course['IdSubject'];
	                         		if($equivid){
	                    				$subject_ids[$q] = $equivid;
	                   				}
                                                        
	                                $q++;
                                        
	                            }
                                    
                                    //count paper/credit hourse
                                    if ($course["exam_status"]!="U" && $course["grade_name"]!="F"){
                                        foreach ($programRequirement as $programRequirementLoop){
                                            if ($programRequirementLoop['SubjectType'] == 394){
                                                if ($programRequirementLoop['RequirementType']==1){
                                                    $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                    $countCreditHoursCore = $ch+$countCreditHoursCore;
                                                }else{
                                                    $countCreditHoursCore++;
                                                }
                                            }
                                        }
                                        
                                        $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                        
                                        //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                        if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                            $countCompleteCreditHoursCore = $countCompleteCreditHoursCore+$ch2;
                                            $countCompletePaperCore++;
                                        }
                                    }
                                    
	                            $z++;
                                }else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/{

                                $compulsory['registered'][$z] = $course;
                                $compulsory['registered'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"]=="U"){
                                    $compulsory['registered'][$z]['CreditHours'] = 0;
                                    $compulsory['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status']=='CT'){
                                    $countCtCredit = $course['CreditHours']+$countCtCredit;
                                }else if ($course['exam_status']=='EX'){
                                    $countExCredit = $course['CreditHours']+$countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"]!="U"){
                                    foreach ($programRequirement as $programRequirementLoop){
                                        if ($programRequirementLoop['SubjectType'] == 394){
                                            if ($programRequirementLoop['RequirementType']==1){
                                                $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                $countCreditHoursCore = $ch+$countCreditHoursCore;
                                            }else{
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
	                        	/*$compulsory['registered'][$z] = $course;
	                      		$compulsory['registered'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course['grade_name']:'';
	                      		$subject_ids[$q] = $course['IdSubject'];
	                      		$q++;
	                      		$z++;
                                        
                                        if ($course["grade_name"]=="U"){
                                            $compulsory['completed'][$z]['CreditHours'] = 0;
                                        }
                                        
                                        if ($course['exam_status']=='CT'){
                                            $countCtCredit = $course['CreditHours']+$countCtCredit;
                                        }else if ($course['exam_status']=='EX'){
                                            $countExCredit = $course['CreditHours']+$countExCredit;
                                        }*/
                                        
                                        //count paper/credit hourse
                                        /*if ($course["exam_status"]!="U" && $course["grade_name"]!="F"){
                                            foreach ($programRequirement as $programRequirementLoop){
                                                if ($programRequirementLoop['SubjectType'] == 394){
                                                    if ($programRequirementLoop['RequirementType']==1){
                                                        $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                        $countCreditHoursCore = $ch+$countCreditHoursCore;
                                                    }else{
                                                        $countCreditHoursCore++;
                                                    }
                                                }
                                            }

                                            $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];

                                            if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                                $countCompleteCreditHoursCore = $countCompleteCreditHoursCore+$ch2;
                                                $countCompletePaperCore++;
                                            }
                                        }*/
                                        
                                        //count paper/credit hourse
                                        /*if ($course["exam_status"]!="U"){
                                            foreach ($programRequirement as $programRequirementLoop){
                                                if ($programRequirementLoop['SubjectType'] == 394){
                                                    if ($programRequirementLoop['RequirementType']==1){
                                                        $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                        $countCreditHoursCore = $ch+$countCreditHoursCore;
                                                    }else{
                                                        $countCreditHoursCore++;
                                                    }
                                                }
                                            }
                                        }*/
	                        }
	                    }
	                    elseif($course_status['DefinitionDesc'] == "Elective")
	                    {
	                       //var_dump($course);
	                       if($course["exam_status"]=="EX" || $course["exam_status"]=="CT"|| $course["exam_status"]=="U")//Complete
	                        {
	                        	//echo "<pre>";print_r($course);echo "</pre><hr>";
	                      		$elective['completed'][$p] = $course;
	                      		$elective['completed'][$p]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
	                        	if($course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U"){
	                                $elective['completed'][$p]['grademark'] = $landscape['AssessmentMethod']!='point' ? $course["exam_status"]:$course["grade_name"];
	                            }
                                    
                                    if ($course["grade_name"]=="U"){
                                        $elective['completed'][$p]['CreditHours'] = 0;
                                    }
                                    
                                    if ($course['exam_status']=='CT'){
                                        $countCtCredit = $course['CreditHours']+$countCtCredit;
                                    }else if ($course['exam_status']=='EX'){
                                        $countExCredit = $course['CreditHours']+$countExCredit;
                                    }
	                            
	                         	if($course["grade_name"]!="F"){ //Fail Masuk dlm list tapi not completed
	                          	    $subject_ids[$q] = $course['IdSubject'];
	                          	    $cum_elective_ch = $cum_elective_ch + $course["CreditHours"];
	                                $q++;
                                        
	                            }
                                    
                                    //count paper/credit hourse
                                    if ($course["exam_status"]!="U" && $course["grade_name"]!="F"){
                                        foreach ($programRequirement as $programRequirementLoop){
                                            if ($programRequirementLoop['SubjectType'] == 275){
                                                if ($programRequirementLoop['RequirementType']==1){
                                                    $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                    $countCreditHoursElective = $ch+$countCreditHoursElective;
                                                }else{
                                                    $countCreditHoursElective++;
                                                }
                                            }
                                        }
                                        
                                        $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                        $course['mark_approval_status'] = 2;
                                        if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                            $countCompleteCreditHoursElective = $countCompleteCreditHoursElective+$ch2;
                                            $countCompletePaperElective++;
                                        }
                                    }
                                    
	                            $p++;
	                        }else if ($course["exam_status"]=="C"){
                                    $elective['completed'][$p] = $course;
	                      		$elective['completed'][$p]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
	                        	if($course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U"){
	                                $elective['completed'][$p]['grademark'] = $landscape['AssessmentMethod']!='point' ? $course["exam_status"]:$course["grade_name"];
	                            }
                                    
                                    if ($course["grade_name"]=="U"){
                                        $elective['completed'][$p]['CreditHours'] = 0;
                                    }
                                    
                                    if ($course['exam_status']=='CT'){
                                        $countCtCredit = $course['CreditHours']+$countCtCredit;
                                    }else if ($course['exam_status']=='EX'){
                                        $countExCredit = $course['CreditHours']+$countExCredit;
                                    }
	                            
	                         	if($course["grade_name"]!="F"){ //Fail Masuk dlm list tapi not completed
	                          	    $subject_ids[$q] = $course['IdSubject'];
	                          	    $cum_elective_ch = $cum_elective_ch + $course["CreditHours"];
	                                $q++;
                                        
	                            }
                                    
                                    //count paper/credit hourse
                                    if ($course["exam_status"]!="U" && $course["grade_name"]!="F"){
                                        foreach ($programRequirement as $programRequirementLoop){
                                            if ($programRequirementLoop['SubjectType'] == 275){
                                                if ($programRequirementLoop['RequirementType']==1){
                                                    $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                    $countCreditHoursElective = $ch+$countCreditHoursElective;
                                                }else{
                                                    $countCreditHoursElective++;
                                                }
                                            }
                                        }
                                        
                                        $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                        
                                        //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                        if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                            $countCompleteCreditHoursElective = $countCompleteCreditHoursElective+$ch2;
                                            $countCompletePaperElective++;
                                        }
                                    }
                                    
	                            $p++;
                                }else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/{

                               $elective['registered'][$z] = $course;
                               $elective['registered'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
                               $subject_ids[$q] = $course['IdSubject'];

                               if ($course["grade_name"]=="U"){
                                   $elective['registered'][$z]['CreditHours'] = 0;
                                   $elective['registered'][$z]['grademark'] = $course["grade_name"];
                               }

                               if ($course['exam_status']=='CT'){
                                   $countCtCredit = $course['CreditHours']+$countCtCredit;
                               }else if ($course['exam_status']=='EX'){
                                   $countExCredit = $course['CreditHours']+$countExCredit;
                               }

                               //count paper/credit hourse
                               if ($course["exam_status"]!="U"){
                                   foreach ($programRequirement as $programRequirementLoop){
                                       if ($programRequirementLoop['SubjectType'] == 394){
                                           if ($programRequirementLoop['RequirementType']==1){
                                               $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                               $countCreditHoursElective = $ch+$countCreditHoursElective;
                                           }else{
                                               $countCreditHoursElective++;
                                           }
                                       }
                                   }
                               }

                               $q++;
                               $z++;
	                        	/*$elective['registered'][$z] = $course;
	                      		$elective['registered'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course['grade_name']:'';
	                      		$subject_ids[$q] = $course['IdSubject'];
	                      		$q++;
	                      		$z++;
                                        
                                        if ($course["grade_name"]=="U"){
                                            $elective['registered'][$z]['CreditHours'] = 0;
                                        }
                                        
                                        if ($course['exam_status']=='CT'){
                                            $countCtCredit = $course['CreditHours']+$countCtCredit;
                                        }else if ($course['exam_status']=='EX'){
                                            $countExCredit = $course['CreditHours']+$countExCredit;
                                        }
                                        
                                        //count paper/credit hourse
                                        /*if ($course["exam_status"]!="U" && $course["grade_name"]!="F"){
                                            foreach ($programRequirement as $programRequirementLoop){
                                                if ($programRequirementLoop['SubjectType'] == 275){
                                                    if ($programRequirementLoop['RequirementType']==1){
                                                        $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                        $countCreditHoursElective = $ch+$countCreditHoursElective;
                                                    }else{
                                                        $countCreditHoursElective++;
                                                    }
                                                }
                                            }

                                            $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];

                                            if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                                $countCompleteCreditHoursElective = $countCompleteCreditHoursElective+$ch2;
                                                $countCompletePaperElective++;
                                            }
                                        }*/
                                        
                                        //count paper/credit hourse
                                        /*if ($course["exam_status"]!="U"){
                                            foreach ($programRequirement as $programRequirementLoop){
                                                if ($programRequirementLoop['SubjectType'] == 394){
                                                    if ($programRequirementLoop['RequirementType']==1){
                                                        $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                                        $countCreditHoursCore = $ch+$countCreditHoursCore;
                                                    }else{
                                                        $countCreditHoursCore++;
                                                    }
                                                }
                                            }
                                        }*/
	                        }
	                    }else if ($course_status['DefinitionDesc'] == "Research"){
                                if ($researchGred == '-'){
                                    if ($course['exam_status'] != 'IP' && $course['exam_status'] != ''){
                                        $researchGred = $course['grade_name'];
                                        $researchRemarks = '';
                                    }else{
                                        $researchRemarks = 'In Progress';
                                    }
                                }
                                
                                //count credit hours
                                if ($course["exam_status"]!="U"){
                                    $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:0;
                                    $researchCreditHours = $researchCreditHours+$ch2;
                                    if ($course["exam_status"]!=null){
                                        $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:0;
                                        $countCreditHoursResearch = $countCreditHoursResearch+$ch;
                                    }
                                }
                                
                                //$researchCreditHours = $course['credit_hour_registered'] + $researchCreditHours;
                                
                                $research['name'] = $course['SubCode'].' '.$course['SubName'];
                                $research['credithours'] = $researchCreditHours;
                                $research['grademark'] = $researchGred;
                                $research['remarks'] = $researchRemarks;

                                $r++;
                            }
                    }elseif($landscape['LandscapeType']==42){
                    	if($course["exam_status"]=="C" ||$course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U")//Complete
                        {
                        	//echo "<pre>";print_r($course);echo "</pre><hr>";
                      		$compulsory[$course_status['block']]['completed'][$z] = $course;
                      		$compulsory[$course_status['block']]['completed'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course["grade_name"]:'';
                        	if($course["exam_status"]=="EX" || $course["exam_status"]=="CT" || $course["exam_status"]=="U"){
                                $compulsory[$course_status['block']]['completed'][$z]['grademark'] = $landscape['AssessmentMethod']!='point' ? $course["exam_status"]:$course["grade_name"];
                            }
                            
                                if ($course["grade_name"]=="U"){
                                    $compulsory[$course_status['block']]['completed'][$z]['CreditHours'] = 0;
                                }
                                
                                if ($course['exam_status']=='CT'){
                                    $countCtCredit = $course['CreditHours']+$countCtCredit;
                                }else if ($course['exam_status']=='EX'){
                                    $countExCredit = $course['CreditHours']+$countExCredit;
                                }
                            
                         	if($course["grade_name"]!="F"){ //Fail Masuk dlm list tapi not completed
                          	    $subject_ids[$q] = $course['IdSubject'];
                         		    if($equivid){
	                    				$subject_ids[$q] = $equivid;
	                   				} 

                                $q++;
                                
                            }
                            
                            //count paper/credit hourse
                            if ($course["exam_status"]!="U"){
                                if ($programRequirement[0]['RequirementType']==1){
                                    $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                    $countCreditHoursCore = $ch+$countCreditHoursCore;
                                }else{
                                    $countCreditHoursCore++;
                                }

                                $ch2 = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                      
                                if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true){
                                    $countCompleteCreditHoursCore = $countCompleteCreditHoursCore+$ch2;
                                    $countCompletePaperCore++;
                                }
                            }
                            
                            $z++;
                        }else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/{
                        	//echo "masuk";
                        	$compulsory[$course_status['block']]['completed'][$z] = $course;
                        	$compulsory[$course_status['block']]['completed'][$z]['grademark'] = ($course['mark_approval_status']==2 && $publishresult==true) ? $course['grade_name']:'';
                        	$subject_ids[$q]=$course['IdSubject'];
                        	$q++;
                                
                                if ($course["grade_name"]=="U"){
                                    $compulsory[$course_status['block']]['completed'][$z]['CreditHours'] = 0;
                                }
                                
                                if ($course['exam_status']=='CT'){
                                    $countCtCredit = $course['CreditHours']+$countCtCredit;
                                }else if ($course['exam_status']=='EX'){
                                    $countExCredit = $course['CreditHours']+$countExCredit;
                                }
                                
                                //count paper/credit hourse
                                if ($course["exam_status"]!="U"){
                                    if ($programRequirement[0]['RequirementType']==1){
                                        $ch = $course['credit_hour_registered']!=null && $course['credit_hour_registered']!='' ? $course['credit_hour_registered']:$course['CreditHours'];
                                        $countCreditHoursCore = $ch+$countCreditHoursCore;
                                    }else{
                                        $countCreditHoursCore++;
                                    }
                                }
                                
                                $z++;
                        }               	
                    }
                }
                
                
                
                //get remaining Course------------------------------------------------
                //echo "<pre>";print_r($compulsory);echo"<pre>";exit;
                //get remaining Course
                $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                $remaining_courses  = $landscapeSubjectDb->getRemainingCourses($student['IdProgram'],$student['IdLandscape'],$subject_ids);
                $elecreq = $recordDb->getElectiveReq($student['IdLandscape']);
                //echo "<pre>";print_r($remaining_courses);echo"<pre>";
               // echo "xxxx = $elecreq === $cum_elective_ch";
                //var_dump($remaining_courses);
                $researchRem = array();
                
                foreach($remaining_courses as $b => $remaining)
                {
                    if($landscape['LandscapeType']==43){
	                	if($remaining['DefinitionDesc'] == "Core")
	                    {
	                        $compulsory['open'][$z] = $remaining;
	                        $compulsory['open'][$z]['grademark'] = '-';
	                        $z++;
	                    }
	                    elseif($remaining['DefinitionDesc'] == 'Elective')
	                    {
	                    	if($cum_elective_ch < $elecreq){
		                        $elective['open'][$p] = $remaining;
		                        $elective['open'][$p]['grademark'] = '-';
		                        $p++;
	                    	}
	                    }
                            else if($remaining['DefinitionDesc'] == 'Research'){
                                $researchRem['open'][$p] = $remaining;
                                $researchRem['open'][$p]['grademark'] = '-';
                                $p++;
                            }
                    }else{
                    	$compulsory[$remaining['block']]['open'][$z] = $remaining;
	                    $compulsory[$remaining['block']]['open'][$z]['grademark'] = '-';
	                    $z++;
                    }
                }
                //end get remaining Course------------------------------------------------
                
                if($landscape['LandscapeType']==43){
                        $row_span = 0;
                	if(array_key_exists("registered",$compulsory)){
	                	$row_span = count($compulsory['completed']) + count($compulsory['registered']);
                	}else{
                		$row_span = count($compulsory['completed']);
                	}
                        //$this->view->rowspan = $row_span;
	                
	                if(array_key_exists("registered",$elective)){
	                	$row_span_elective = count($elective['completed']) + count($elective['registered']);
	                }else{
	                	$row_span_elective = count($elective['completed']);
	                }
	                //$this->view->rowspan_elective = $row_span_elective;
	                //$this->view->elective = $elective;
                        //var_dump($elective);
	                $row_span_open_compulsary = count($compulsory['open']);
	                $row_span_open_elective   = count($elective['open']);
	                
	                //$this->view->rowspan_open_compulsary = $row_span_open_compulsary;
	                //$this->view->rowspan_open_elective   = $row_span_open_elective;
            
                }
                //var_dump($compulsory);
                //var_dump($elective);
                //var_dump($research);
                //$this->view->research = $research;
                //$this->view->researchRem = $researchRem;
                
                //count complete subject
                $viewRemainingSubjectCore = true;
                $viewRemainingSubjectElective = true;
                $viewRemainingSubjectResearch = true;
                
                //$this->view->countCreditHoursCore = $countCreditHoursCore+$countCreditHoursElective;
                //var_dump($countCreditHoursCore);
                $chr1 = 0;
                $chr2 = 0;
                $chr3 = 0;
                $remPaperCore = 0;
                $remPaperElective = 0;
                $creditRequiredCore = 0;
                $creditRequiredElective = 0;
                $creditRequiredResearch = 0;
                $completePart1 = 0;
                $completePart2 = 0;
                $completePart3 = 0;
                $remainingPart1 = 0;
                $remainingPart2 = 0;
                $remainingPart3 = 0;
                $cifprequired = 0;
                //var_dump($countCreditHoursResearch);
                if($landscape['LandscapeType']==43){
                	//core paper
                    foreach ($programRequirement as $programRequirementLoop){
                        if ($programRequirementLoop['SubjectType'] == 394){
                            
                            $creditRequiredCore = $programRequirementLoop['CreditHours'];
                            $remPaperCore = ($programRequirementLoop['CreditHours']/3)-($countCompleteCreditHoursCore/3);
                            
                            if ($remPaperCore < 0){
                                $remPaperCore = 0;
                            }
                            if ($countCreditHoursCore >= $programRequirementLoop['CreditHours']){
                                $viewRemainingSubjectCore = false;
                                //$chr1 = 0;
                            }/*else{
                                $chr1 = $programRequirementLoop['CreditHours']-$countCreditHoursCore;
                            }*/

                            if ($programRequirementLoop['RequirementType']==1){
                                $chr1 = $programRequirementLoop['CreditHours']-$countCompleteCreditHoursCore;
                            }else{
                                $chr1 = $programRequirementLoop['CreditHours']-$countCompletePaperCore;
                            }
                        }
                    }
                    
                    //eelective paper
                    foreach ($programRequirement as $programRequirementLoop){
                        if ($programRequirementLoop['SubjectType'] == 275){
                            
                            $creditRequiredElective = $programRequirementLoop['CreditHours'];
                            $remPaperElective = ($programRequirementLoop['CreditHours']/3)-($countCompletePaperElective);
                            
                            if ($remPaperElective < 0){
                                $remPaperElective = 0;
                            }
                            if ($countCreditHoursElective >= $programRequirementLoop['CreditHours']){
                                $viewRemainingSubjectElective = false;
                                //$chr2 = 0;
                            }/*else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursElective;
                            }*/
                            
                            if ($programRequirementLoop['RequirementType']==1){
                                $chr2 = $programRequirementLoop['CreditHours']-$countCompleteCreditHoursElective;
                            }else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCompletePaperElective;
                            }
                        }
                    }
                    
                    //Research paper
                    $remPaperResearch = 0;
                    foreach ($programRequirement as $programRequirementLoop){
                        if ($programRequirementLoop['SubjectType'] == 271){
                            $creditRequiredResearch = $programRequirementLoop['CreditHours'];
                            
                            if ($countCreditHoursResearch >= $programRequirementLoop['CreditHours']){
                                $viewRemainingSubjectResearch = false;
                                $chr3 = 0;
                               
                            }else{
                                $chr3 = $programRequirementLoop['CreditHours']-$countCreditHoursResearch;
                                $remPaperResearch =1;//remaining
                            }
                        }
                    }
                    
                    $reminaingPaperMIFP = $remPaperCore+$remPaperElective;
                    
                    
                    if ($chr1 == null || $chr1 <= 0){
                        $chr1 = 0;
                    }
                    if ($chr2 == null || $chr2 <= 0){
                        $chr2 = 0;
                    }
                    if ($chr3 == null || $chr3 <= 0){
                        $chr3 = 0;
                        //$countCreditHoursResearch = 1;
                    }
                    /*if ($chr1 == null){
                        $chr1 = 0;
                    }
                    if ($chr2 == null){
                        $chr2 = 0;
                    }
                    if ($chr3 == null){
                        $chr3 = 0;
                    }*/
                    $thischr = $chr1+$chr2+$chr3;
                    
                }else if($landscape['LandscapeType']==42){
                    $chr = 0;
                    if ($programRequirement){
                        if ($countCreditHoursCore >= $programRequirement[0]['CreditHours']){
                            $viewRemainingSubjectCore = false;
                            $chr = 0;
                        }else{
                            $chr = $programRequirement[0]['CreditHours']-$countCreditHoursCore;
                        }
                    }
                    //$thischr = $chr;
                    
                    //count part summary
                    $completePart1 = 0;
                    $completePart2 = 0;
                    $completePart3 = 0;
                    $remainingPart1 = 0;
                    $remainingPart2 = 0;
                    $remainingPart3 = 0;

                    if ($compulsory){
                        foreach ($compulsory as $key => $compulsoryLoop){
                            if ($key == 1){
                                if (isset($compulsoryLoop['completed'])){
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub){
                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);
                    
                                        $publishresult = false;

                                        if ($subsem){
                                            $publishInfo = $recordDb->getPublishResult($student['IdProgram'], $subsem['IdSemesterMaster']);

                                            if ($publishInfo){
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                                                $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                                    $publishresult = true;
                                                }
                                            }
                                        }
                                        
                                        if ($return=='rpaper' || $return=='caudit'){
                                            $publishresult = true;
                                        }
                                        
                                        if (($compulsoryLoopSub["exam_status"]=="C" ||$compulsoryLoopSub["exam_status"]=="EX" || $compulsoryLoopSub["exam_status"]=="CT" || $compulsoryLoopSub["exam_status"]=="U") && $compulsoryLoopSub['mark_approval_status']==2 && $publishresult==true){
                                            if ($compulsoryLoopSub["exam_status"]!="U"){
                                                if ($compulsoryLoopSub["grade_name"]!="F"){
                                                    $completePart1++;
                                                }
                                            }
                                        }else{
                                            $remainingPart1++;
                                        }
                                    }
                                }
                                
                                if (isset($compulsoryLoop['open'])){
                                    foreach ($compulsoryLoop['open'] as $compulsoryLoopSub){
                                        $remainingPart1++;
                                    }
                                }
                            }else if($key == 2){
                                if (isset($compulsoryLoop['completed'])){
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub){
                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);
                    
                                        $publishresult = false;

                                        if ($subsem){
                                            $publishInfo = $recordDb->getPublishResult($student['IdProgram'], $subsem['IdSemesterMaster']);

                                            if ($publishInfo){
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                                                $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                                    $publishresult = true;
                                                }
                                            }
                                        }
                                        
                                        if ($return=='rpaper' || $return=='caudit'){
                                            $publishresult = true;
                                        }
                                        
                                        if (($compulsoryLoopSub["exam_status"]=="C" ||$compulsoryLoopSub["exam_status"]=="EX" || $compulsoryLoopSub["exam_status"]=="CT" || $compulsoryLoopSub["exam_status"]=="U") && $compulsoryLoopSub['mark_approval_status']==2 && $publishresult==true){
                                            if ($compulsoryLoopSub["exam_status"]!="U"){
                                                if ($compulsoryLoopSub["grade_name"]!="F"){
                                                    $completePart2++;
                                                }
                                            }
                                        }else{
                                            $remainingPart2++;
                                        }
                                    }
                                }
                                
                                if (isset($compulsoryLoop['open'])){
                                    foreach ($compulsoryLoop['open'] as $compulsoryLoopSub){
                                        $remainingPart2++;
                                    }
                                }
                            }else if($key == 3){
                                if (isset($compulsoryLoop['completed'])){
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub){
                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);
                    
                                        $publishresult = false;

                                        if ($subsem){
                                            $publishInfo = $recordDb->getPublishResult($student['IdProgram'], $subsem['IdSemesterMaster']);

                                            if ($publishInfo){
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                                                $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                                    $publishresult = true;
                                                }
                                            }
                                        }
                                        
                                        if ($return=='rpaper' || $return=='caudit'){
                                            $publishresult = true;
                                        }
                                        
                                        if (($compulsoryLoopSub["exam_status"]=="C" ||$compulsoryLoopSub["exam_status"]=="EX" || $compulsoryLoopSub["exam_status"]=="CT" || $compulsoryLoopSub["exam_status"]=="U") && $compulsoryLoopSub['mark_approval_status']==2 && $publishresult==true){
                                            if ($compulsoryLoopSub["exam_status"]!="U"){
                                                $completePart3++;
                                            }
                                        }else{
                                        	$remainingPart3++;
                                        }
                                    }
                                }
                                
                                if (isset($compulsoryLoop['open'])){
                                    if (count($compulsoryLoop['open']) > 1){
                                        $remainingPart3++;
                                    }
                                }
                            }
                        }
                    }
                    
                    //$this->view->completePart1 = $completePart1;
                    //$this->view->completePart2 = $completePart2;
                    //$this->view->completePart3 = $completePart3;
                    //$this->view->remainingPart1 = $remainingPart1;
                    //$this->view->remainingPart2 = $remainingPart2;
                    //$this->view->remainingPart3 = $remainingPart3;
                    //var_dump(count($compulsoryLoop['open']));
                    
                    /*if ($viewRemainingSubjectCore == false){
                        $remainingPart1 = 0;
                        $remainingPart2 = 0;
                    }*/
                    
                    /*echo '----------------------------------------------------<br/>';
                    echo 'ID          : '.$student['registrationId'].'<br/>';
                    echo 'remaining 1 : '.$remainingPart1.'<br/>';
                    echo 'remaining 2 : '.$remainingPart2.'<br/>';
                    echo 'remaining 3 : '.$remainingPart3.'<br/>';
                    echo 'required    : '.$programRequirement[0]['CreditHours'].'<br/>';
                    echo 'complete 1  : '.$completePart1.'<br/>';
                    echo 'complete 2  : '.$completePart2.'<br/>';
                    echo 'complete 3  : '.$completePart3.'<br/>';
                    echo 'complete    : '.$countCompletePaperCore.'<br/>';
                    echo '----------------------------------------------------<br/>';*/
                    
                    //CIFP SHJ
                    //$thischr = $remainingPart1+$remainingPart2+$remainingPart3;
                    //$thischr = $programRequirement[0]['CreditHours']-($completePart1+$completePart2+$completePart3);
                    if ($programRequirement){
                        foreach ($programRequirement as $programRequirementLoop){
                            $cifprequired = $programRequirementLoop['CreditHours']+$cifprequired;
                        }
                    }

                    $thischr = $cifprequired-($countCompletePaperCore)+$remainingPart3;
                    //$cifprequired = $programRequirement[0]['CreditHours'];
                    //var_dump($thischr);
                    //$total_paper_completed = $countCompletePaperCore+$countCompletePaperElective+$countCreditHoursResearch;
                }
                
                $Asol = 0;
                $Atol = 0;
                $Btam = 0;
                $Bsol = 0;
                $Btol = 0;
                $Ctam = 0;
                $Csol = 0;
                $Dsol = 0;
                $Fsol = 0;
                $NP = 0;
                $P = 0;
                $PD = 0;
                $CT = 0;
                $EX = 0;
                $U = 0;

                if ($courses){
                    foreach ($courses as $coursesLoop){
                        $equivid = false;
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($student['IdProgram'],$student['IdLandscape'],$coursesLoop['IdSubject']);
                        if(!$course_status){
                            $equivid=$recordDb->checkEquivStatus($student['IdLandscape'],$coursesLoop["IdSubject"]);

                            if($equivid){
                                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($student['IdProgram'],$student['IdLandscape'],$equivid);
                            }
                        }
                        
                        $subsem = $recordDb->getSemesterById($coursesLoop['IdSemesterMain']);
                    
                        $publishresult = false;

                        if ($subsem){
                            $publishInfo = $recordDb->getPublishResult($student['IdProgram'], $subsem['IdSemesterMaster']);

                            if ($publishInfo){
                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                                $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                    $publishresult = true;
                                }
                            }
                        }
                        
                        if ($return=='rpaper' || $return=='caudit'){
                            $publishresult = true;
                        }
                        
                        if ($coursesLoop["mark_approval_status"]==2 && $publishresult==true && $course_status){
                            if (trim($coursesLoop['grade_name'])=='A'){
                                $Asol++;
                            }else if(trim($coursesLoop['grade_name'])=='A-'){
                                $Atol++;
                            }else if(trim($coursesLoop['grade_name'])=='B+'){
                                $Btam++;
                            }else if(trim($coursesLoop['grade_name'])=='B'){
                                $Bsol++;
                            }else if(trim($coursesLoop['grade_name'])=='B-'){
                                $Btol++;
                            }else if(trim($coursesLoop['grade_name'])=='C+'){
                                $Ctam++;
                            }else if(trim($coursesLoop['grade_name'])=='C'){
                                $Csol++;
                            }else if(trim($coursesLoop['grade_name'])=='D'){
                                $Dsol++;
                            }else if(trim($coursesLoop['grade_name'])=='F'){
                                $Fsol++;
                            }else if(trim($coursesLoop['grade_name'])=='NP'){
                                $NP++;
                            }else if(trim($coursesLoop['grade_name'])=='P'){
                                $P++;
                            }else if(trim($coursesLoop['grade_name'])=='PD'){
                                $PD++;
                            }else if(trim($coursesLoop['grade_name'])=='CT'){
                                $CT++;
                            }else if(trim($coursesLoop['grade_name'])=='EX'){
                                $EX++;
                            }else if(trim($coursesLoop['grade_name'])=='U'){
                                $U++;
                            }
                        }
                    }
                }

                $CAP = array(
                    'A' => $Asol,
                    'A-' => $Atol,
                    'B+' => $Btam,
                    'B' => $Bsol,
                    'B-' => $Btol,
                    'C+' => $Ctam,
                    'C' => $Csol,
                    'D' => $Dsol,
                    'F' => $Fsol,
                    'NP' => $NP,
                    'P' => $P,
                    'PD' => $PD,
                    'CT' => $CT,
                    'EX' => $EX,
                    'U' => $U
                );

                $semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
                $regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
                $studentGradeDB = new Examination_Model_DbTable_StudentGrade();

                $registered_semester = $semesterStatusDB->getListSemesterRegistered($idStudentRegistration);
                $student_grade = false;
                $ttt = 0;
                foreach($registered_semester as $index=>$semester){

                    $subsem = $recordDb->getSemesterById($semester['IdSemesterMain']);

                    $publishresult = false;

                    if ($subsem){
                        $publishInfo = $recordDb->getPublishResult($student['IdProgram'], $subsem['IdSemesterMaster']);

                        if ($publishInfo){
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                            $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                $publishresult = true;
                            }
                        }
                    }

                    //get subject registered in each semester
                    if($semester["IsCountable"]==1){
                        $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($idStudentRegistration,$semester['IdSemesterMain']);
                    }else{
                        $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($idStudentRegistration,$semester['IdSemesterMain']);
                    }
                    //var_dump($subject_list);
                    $registered_semester[$index]['subjects']=$subject_list;

                    $ttt++;

                    if ($publishresult == true){
                        //get grade info
                        $student_grade = $studentGradeDB->getStudentGrade($idStudentRegistration,$semester['IdSemesterMain']);
                    }
                }

                if (!$student_grade){
                    $student_grade['sg_cum_credithour']=0;
                    $student_grade['sg_cgpa']=0;
                }
                

				if($return=='portal'){
                	//CIFP
                	$complete_chr_cifp = $completePart1+$completePart2+$completePart3;
                	$complete_chr_other = $countCompleteCreditHoursCore+$countCompleteCreditHoursElective+$countCreditHoursResearch;
                	return array('remaining'=>$thischr,'complete_chr_cifp'=>$complete_chr_cifp,'complete_chr_other'=>$complete_chr_other);
                }else
                if($return=='rpaper'){
                	//remaining paper
                	//array(CIPF,MIFP)
                        //var_dump($thischr);
                	return array($thischr,$reminaingPaperMIFP,$cifprequired);
                }else if($return=='caudit'){
                    /*echo '-----------------------------------------------------------------<br/>';
                    echo 'id : '.$student['registrationId'].'-'.$student['IdStudentRegistration'].'<br/>';
                    echo 'remaining core : '.$chr1.'<br/>';
                    echo 'remaining elective : '.$chr2.'<br/>';
                    echo 'remaining research : '.$chr3.'<br/>';
                    echo 'required core : '.$creditRequiredCore.'<br/>';
                    echo 'required elective : '.$creditRequiredElective.'<br/>';
                    echo 'required research : '.$creditRequiredResearch.'<br/>';
                    echo 'complete core : '.$countCompleteCreditHoursCore.'<br/>';
                    echo 'complete elective : '.$countCompleteCreditHoursElective.'<br/>';
                    echo 'complete research : '.$countCreditHoursResearch.'<br/>';
                    echo '-----------------------------------------------------------------<br/>';*/
                    
                    if (isset($research['grademark']) && $research['grademark']!='P'){
                        $countCreditHoursResearch = 0;
                    }
                    
                    $cauditArr = array(
                        'credit_hours_completed_core'=>$countCompleteCreditHoursCore,
                        'credit_hours_required_core'=>$creditRequiredCore,
                        'credit_hours_remaining_core'=>$chr1,
                        'credit_hours_completed_elective'=>$countCompleteCreditHoursElective,
                        'credit_hours_required_elective'=>$creditRequiredElective,
                        'credit_hours_remaining_elective'=>$chr2,
                        'credit_hours_completed_research'=>$countCreditHoursResearch,
                        'credit_hours_required_research'=>$creditRequiredResearch,
                        'credit_hours_remaining_research'=>$chr3,
                        'remaining_paper_mifp'=>$reminaingPaperMIFP,
                        'this_chr'=>$thischr,
                        'cap'=>$CAP, //gred count
                        'remaining_part1'=>$remainingPart1,
                        'remaining_part2'=>$remainingPart2,
                        'remaining_part3'=>$remainingPart3,
                        'cifprequired'=>$cifprequired
                    );
                    
                    return $cauditArr;
                }else if ($return == 'financialaid'){
                    $financialaidArr = array(
                        'cap'=>$CAP,
                        'paper_attemp'=>$countPapersTaken,
                        'cgpa'=>$student_grade['sg_cgpa']
                    );
                    
                    return $financialaidArr;
                }
                else{
                    if ($student['IdProgram'] == 3){
                        if (isset($research['grademark']) && $research['grademark'] == '-'){
                            $thischr = $thischr + 1;
                        }
                    }

                	 return  $thischr;
                }
               
             
	}
}

?>