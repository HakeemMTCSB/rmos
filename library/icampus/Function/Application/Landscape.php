<?php 
class icampus_Function_Application_Landscape extends Zend_View_Helper_Abstract{
	
	/*
	 * Get landscape id from applicant_transaction. If null assign landscape to txn
	 */
	public static function getApplicantLandscapeId($txnId){

		//get applicant program
		$db = Zend_Db_Table::getDefaultAdapter();
		$select_program = $db->select()
							->from(array('ap'=>'applicant_program'))
							->where("ap.ap_at_trans_id = ?", (int)$txnId);
		
		$row_program = $db->fetchRow($select_program);
		
		$program_id = null;
		if($row_program){
			$program_id = $row_program['ap_prog_id'];
		}else{
			throw new Exception('No applicant program data');
		}
		
		//get landscape from txn 
		$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $txnDb->fetchRow( array('at_trans_id = ?'=> $txnId) );
		
		//assign landscape if null
		$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
		//if($txnData['IdLandscape'] == null || $txnData['IdLandscape'] == 0){
				
				
			//get landscape based on program, scheme and active
			$where = array(
					'IdProgram = ?' => $program_id,
					'IdStartSemester = ?' => $txnData['at_intake'],
					'IdProgramScheme = ?' => $row_program['ap_prog_scheme'],
					'Active = ?' => 1
			);
			$row_landscape = $landscapeDb->fetchRow($where);
			
			
				
			if($row_landscape == null){
				throw new Exception('No Active landscape');
			}else{
				//update tbl transaction
				$data_update = array('IdLandscape' =>$row_landscape['IdLandscape']);
				$txnDb->update($data_update, 'at_trans_id = '.$txnId);
		
				$landscape_id = $row_landscape['IdLandscape'];
			}
		
		/*}else{
			$landscape_id = $txnData['IdLandscape'];
		}*/
		
		return $landscape_id;
	}
	
/*
	 * Get landscape id from tbl_studentregistration. If null assign landscape to txn
	 */
	public static function getStudentLandscapeId($txnId){

		//get student registration
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
							->from(array('ap'=>'tbl_studentregistration'))
							->where("ap.IdStudentRegistration = ?", (int)$txnId);
		
		$txnData = $db->fetchRow($select);
		
		$program_id = null;
		if($txnData){
			$program_id = $txnData['IdProgram'];
		}else{
			throw new Exception('No student program data');
		}
		
		//assign landscape if null
		$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
		if($txnData['IdLandscape'] == null){
				
				
			//get landscape based on program, scheme, intake and active
			$where = array(
					'IdProgram = ?' => $program_id,
					'IdProgramScheme = ?' => $txnData['IdProgramScheme'],
					'IdStartSemester = ?'=> $txnData['IdIntake'],
					'Active = ?' => 1
			);
			
//			echo "<pre>";
//			print_r($where);
			$row_landscape = $landscapeDb->fetchRow($where);
				
			if($row_landscape == null){
				throw new Exception('No Active landscape');
			}else{
				//update tbl transaction
				$data_update = array('IdLandscape' =>$row_landscape['IdLandscape']);
				$txnDb->update($data_update, 'IdStudentRegistration = '.$txnId);
		
				$landscape_id = $row_landscape['IdLandscape'];
			}
		
		}else{
			$landscape_id = $txnData['IdLandscape'];
		}
		
		return $landscape_id;
	}
}
?>