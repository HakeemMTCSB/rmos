<?php

class icampus_Function_General_Format extends Zend_View_Helper_Abstract{
	
	function generateStudentID($student_type, $intake){
									
		//generate student ID
		$studentid_format = array();

		$seqnoDb = new Registration_Model_DbTable_StudentRegistrationSeqno();

        //intake
        $intake = $seqnoDb->getIntake($intake);

        if ($intake){
            $sequence = $seqnoDb->getData($intake['sem_year']);
        }else{
            $sequence = $seqnoDb->getData(date('Y'));
        }

		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
                
                if($student_type==741){ 
                    $seqno = $sequence['srs_vseqno'];
                    $numberlength = "%04d";
                }else{
                    $seqno = $sequence['srs_seqno']; 
                    $numberlength = "%05d";
                }
		
		
		//format
		 $format_config = explode('|',$config['StudentIdFormat']);
		
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['StudentIdPrefix'];
			}elseif($format=='YYYY'){
				$result = date('Y', strtotime($sequence['srs_year'].'-01-01'));
			}elseif($format=='YY'){
				$result = date('y', strtotime($sequence['srs_year'].'-01-01'));
			}elseif($format=='seqno'){				
				$result = sprintf($numberlength, $seqno);
			}elseif($format=='type'){
				if($student_type==741){ 
					//Visiting					
					$result = 'V';
				}
			}
			
			if(isset($result)) array_push($studentid_format,$result);
		}
	
		//var_dump(implode("-",$studentid_format));
		$studentID = implode("",$studentid_format);
		
		//update sequence
                if($student_type==741){ 
                    $seqnoDb->updateData(array('srs_vseqno'=>$seqno+1),$sequence['srs_id']);
                }else{
                    $seqnoDb->updateData(array('srs_seqno'=>$seqno+1),$sequence['srs_id']);
                }
		
		return	$studentID;
				
	}
	
	
	function generateApplicationID($type=0,$format,$prefix){
                
		$sequence = $this->getLastSequence($type);
		
		$applicationid_format = array();
		
		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
		
		//format
        $format_config = explode('|',$config[$format]);
       
        for($i=0; $i<count($format_config); $i++){
            $format = $format_config[$i];
            if($format=='px'){ //prefix
                $result = $config[$prefix];
            }elseif($format=='YYYY'){
                $result = date('Y');
            }elseif($format=='YY'){
                $result = date('y');
            }elseif($format=='seqno'){				
                $result = sprintf("%05d", $sequence['tsi_seq_no']);
            }
            array_push($applicationid_format,$result);
        }

        $applicationID = implode("",$applicationid_format);

        //update sequence		
        $this->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
		
        return $applicationID;
    }
    
    
	function getLastSequence($type){
			 	
		$auth = Zend_Auth::getInstance();
		 
        $row = $this->getSequence($type, date('Y'));

        if (!$row){
            $dataSeq = array(
                'tsi_type'=>$type, 
                'tsi_seq_no'=>1, 
                'tsi_seq_year'=>date('Y'), 
                'tsi_upd_date'=>date('Y-m-d'), 
                'tsi_upd_user'=>$auth->getIdentity()->iduser
            );
            $this->insertSequence($dataSeq);
            $this->getLastSequence($type);
        }        
        return $row;
	}
	 	
	function getSequence($type, $year){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sequence_id'))
            ->where('a.tsi_type = ?', $type)
            ->where('a.tsi_seq_year = ?', $year);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
 	function insertSequence($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_sequence_id', $bind);        
        $id = $db->lastInsertId('tbl_sequence_id', 'tsi_id');
        return $id;
    }
    
	public function updateSequence($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update('tbl_sequence_id', $bind, 'tsi_id = "'.$id.'"');
    }
 	
}
 ?>