<?php 
class icampus_Function_Moodle_Integrate{
	function syncSubject($idstudreg,$mdl_userid){
		$courseregDb = new CourseRegistration_Model_DbTable_CourseRegistration();	
		//Sync Registered
		//dptkan list subject registered for this semester SMS
		
		$allregsms = $courseregDb->getCurrentRegistered($idstudreg);
		$cursem = $courseregDb->getCurrentRegisteredSem($idstudreg);
		$moodledb = new GeneralSetup_Model_DbTable_Moodle();
		
		if(is_array($allregsms)){
			$moodledb->assignStudBatch($allregsms);
		}
		//print_r($allregsms);
		//Sync Drop
		$allmoodlesubjects=$moodledb->getStudRegSubject($mdl_userid,$cursem["IdSemesterMain"]);
		//echo "<pre>";print_r($allmoodlesubjects);
		if(is_array($allmoodlesubjects)){
			foreach($allmoodlesubjects as $subject)
			{
				$subjinfo=explode("/",$subject["idnumber"]);
				if(!$courseregDb->isRegistered($idstudreg,$subjinfo[0],$subjinfo[1])){
					//echo $subjinfo[1].":Drop<hr>";
					$moodledb->dropSubject($subject["regid"],$mdl_userid,$subject["contextid"],$subject["courseid"],$subject["gmid"]);
				}
			}
		}
		//exit;
	}
	
	function getMoodleId($username){
		$moodledb = new GeneralSetup_Model_DbTable_Moodle();
		$mdl_userid = $moodledb->getUserId($username);
		return $mdl_userid;
	}
	
	function addMoodleSubjectViaCourseSection($username,$idcoursetagging,$idsubject){
		$stud["appl_username"]=$username;
		$stud["IdCourseTaggingGroup"]=$idcoursetagging;
		$stud["IdSubject"]=$idsubject;
		$moodledb = new GeneralSetup_Model_DbTable_Moodle();
		$moodledb->assignStud($stud);		
	}
	
	function dropMoodleSubject($username,$idsubject){
		echo "Drop";
	}
}
?>