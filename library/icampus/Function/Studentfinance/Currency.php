<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class icampus_Function_Studentfinance_Currency{
	
	public static function getDefaultCurrencyAmount($amount,$amount_currency_id){
	
		$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
		$rate = $currencyRateDb->getCurrentExchangeRate($amount_currency_id);
		
		if(!$rate){
			throw new Exception('Currency rate not set');
		}
		
		return $amount * $rate['cr_exchange_rate'];
	
	}
	
}