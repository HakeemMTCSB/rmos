<?php

/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @author Suliana <suliana@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 *
 * Dependency class
 *    GeneralSetup_Model_DbTable_Intake
 *    GeneralSetup_Model_DbTable_Semestermaster
 *    Studentfinance_Model_DbTable_FeeStructure
 *    Studentfinance_Model_DbTable_FeeStructureItem
 *  Studentfinance_Model_DbTable_FeeStructureProgram
 *    Studentfinance_Model_DbTable_FeeCategory
 *    Studentfinance_Model_DbTable_FeeStructureItemSemester
 *    Studentfinance_Model_DbTable_Currency
 *    Studentfinance_Model_DbTable_CurrencyRate
 *    Studentfinance_Model_DbTable_ProformaInvoiceMain
 *    Studentfinance_Model_DbTable_ProformaInvoiceDetail
 *    App_Model_Record_DbTable_StudentRegistration
 *
 */
class icampus_Function_Studentfinance_Invoice
{

    /*
	 * Generate proforma invoice for applicant who is offered
	 */
    public function generateApplicantProformaInvoice($txnId = null, $level = 0, $semester_id = null, $invoice_desc = null)
    {

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        //txnData
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'applicant_profile'))
            ->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
            ->join(array('c' => 'applicant_program'), 'c.ap_at_trans_id = b.at_trans_id')
            ->joinLeft(array('d'=>'tbl_program'), 'c.ap_prog_id = IdProgram', array('IdScheme'))
            ->where('b.at_trans_id = ?', $txnId);

        $txnData = $db->fetchRow($select);

        if ($txnData == null) {
            throw new Exception('No such applicant');
        }

        //semester info
        $semester = null;
        if ($semester_id) {
            $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
            $semester = $semesterDb->getData($semester_id);

            if ($semester == null) {
                throw new Exception('No such semester');
            }
        }


        //get fee structure and set if not set yet
        $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
        //$feeStructureData = $feeStructureDb->getApplicantFeeStructure($txnData['ap_prog_id'], $txnData['ap_prog_scheme'], $txnData['at_intake'], $txnData['appl_category']);
        //var_dump($txnData['at_fs_id']); exit;
        if ($txnData['at_fs_id'] == null || $txnData['at_fs_id'] == 0) {
            //echo 'lalalala'; exit;
            $feeStructureData = $feeStructureDb->getApplicantFeeStructure($txnData['ap_prog_id'], $txnData['ap_prog_scheme'], $txnData['at_intake'], $txnData['appl_category']);

            $txnData['at_fs_id'] = $feeStructureData['fs_id'];

            //update applicnt program
            $db->update('applicant_transaction', array('at_fs_id' => $feeStructureData['fs_id']), 'at_trans_id = ' . $txnId);
        } else {
            $feeStructureData = $feeStructureDb->fetchRow('fs_id = ' . $txnData['at_fs_id']);

            if (!$feeStructureData){
                $feeStructureData = $feeStructureDb->getApplicantFeeStructure($txnData['ap_prog_id'], $txnData['ap_prog_scheme'], $txnData['at_intake'], $txnData['appl_category']);

                $txnData['at_fs_id'] = $feeStructureData['fs_id'];

                //update applicnt program
                $db->update('applicant_transaction', array('at_fs_id' => $feeStructureData['fs_id']), 'at_trans_id = ' . $txnId);
            }
        }

        //fees to charge
        $fees = $this->getApplicantSemesterFeeInfo($txnId, $semester_id, $level, $txnData['appl_nationality'], 1);
        //echo "<pre>";
        //print_r($fees);
        //exit;

        if ($fees == null) {
            //throw new Exception('No fee to charge data');
        }
//		exit;
        //fee categorized by group
        $arr_fee_by_group = array();
        foreach ($fees as $fee) {
            $arr_fee_by_group[$fee['fi_fc_id']][] = $fee;
        }

        //loop fees by category and issue based on same category
        $feeGroupDb = new Studentfinance_Model_DbTable_FeeCategory();

		echo "<pre>";
		print_r($arr_fee_by_group);
		//exit;

        foreach ($arr_fee_by_group as $fee_category_id => $fee_group) {


            //fee group info
            $feeGroupData = $feeGroupDb->fetchRow('fc_id = ' . $fee_category_id)->toArray();

            $amount_paid = 0;
            $total_invoice_amount = 0;
            $total_invoice_amount_default_currency = 0;

            foreach ($fee_group as $fee) {
                //harcode emgs untuk gs sahaja
                if (isset($txnData['IdScheme']) && $txnData['IdScheme']==11 && $txnData['appl_nationality']!=96){
                    if (strtotime('2016-04-01') <= strtotime(date('Y-m-d'))){
                        if ($fee['fi_id']==143){ //processing fee
                            $fee['amount']=1060.00;
                        }else if ($fee['fi_id']==145){
                            $fee['amount']=265.00;
                        }else if ($fee['fi_id']==144){
                            $fee['amount']=477.00;
                        }else if ($fee['fi_id']==146){
                            $fee['amount']=53.00;
                        }else if ($fee['fi_id']==80){
                            $fee['amount']=$fee['amount'];
                        }
                    }
                }

                $total_invoice_amount += $fee['amount'];
                $invoice_desc = $fee['fee_categoryname'];
            }


            //fee structure currency
            $currencyDb = new Studentfinance_Model_DbTable_Currency();
            //$currency = $currencyDb->fetchRow('cur_id = '.$feeStructureData['fs_cur_id'])->toArray();
            $currency = $currencyDb->fetchRow('cur_id = ' . $fee['cur_id'])->toArray();
            $currency_id = $currency['cur_id'];


            if ($currency['cur_default'] == 'N') {
                //get exchange rate
                $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                $rate = $currencyRateDb->getCurrentExchangeRate($currency['cur_id']);


                $total_invoice_amount_default_currency = $total_invoice_amount * $rate['cr_exchange_rate'];
            } else {
                $total_invoice_amount_default_currency = $total_invoice_amount;
            }

            //debug($currency);

            $proformaInvoiceMainDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
            $proformaInvoiceDetailDb = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();

            if ($level == 0) {

                //check already issue or not
                //if( $txnData['at_processing_fee']==null ){

                /*if($invoice_desc==null && $level==0){
						$invoice_desc = $feeGroupData['fc_desc'];
					}
					
					if($invoice_desc==null){
						$invoice_desc = $feeGroupData['fc_desc'];
					}*/

                /**
                 * checking proforma has been generated or not
                 * if exist -> exclude
                 * if not -> generate
                 *
                 */

                $selectProforma = $db->select()
                    ->from(array('a' => 'proforma_invoice_main'))
                    ->where('a.trans_id = ?', $txnId)
                    ->where('a.fee_category = ?', $fee_category_id)
                    ->where("a.status ='A'");

                $proformaDataChecking = $db->fetchRow($selectProforma);

                if (!$proformaDataChecking) {

                    $bill_no = $this->getBillSeq(1, date('Y'));

                    //add main
                    $data = array(
                        'bill_number' => $bill_no,
                        'appl_id' => $txnData['appl_id'],
                        'trans_id' => $txnData['at_trans_id'],
                        //'academic_year' => $semester['AcademicYear'],
                        //'semester' => $semester_id,
                        'bill_amount' => $total_invoice_amount,
                        'bill_description' => $invoice_desc,
                        'program_id' => $txnData['ap_prog_id'],
                        'creator' => $creator,
                        'fs_id' => $txnData['at_fs_id'],
                        'status' => 'A',
                        'currency_id' => $currency_id,
                        'bill_amount_default_currency' => $total_invoice_amount_default_currency,
                        'invoice_type' => 'PROCESSING',
                        'fee_category' => $fee_category_id,
                        'branch_id' => $txnData['branch_id']
                    );
                    $main_id = $proformaInvoiceMainDb->insert($data);


                    //add detail
                    $proformaInvoiceSubjectDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();
                    foreach ($fee_group as $fee) {

                        if (isset($txnData['IdScheme']) && $txnData['IdScheme']==11 && $txnData['appl_nationality']!=96){
                            if (strtotime('2016-04-01') <= strtotime(date('Y-m-d'))){
                                /*if ($fee['fi_id']==143){ //processing fee
                                    $fee['amount']=1208.40;
                                }else if ($fee['fi_id']==145){
                                    $fee['amount']=265.00;
                                }else if ($fee['fi_id']==144){
                                    $fee['amount']=1060.00;
                                }else if ($fee['fi_id']==146){
                                    $fee['amount']=106.00;
                                }else if ($fee['fi_id']==80){
                                    $fee['amount']=$fee['amount']*2;
                                }*/

                                if ($fee['fi_id']==143){ //processing fee
                                    $fee['amount']=1060.00;
                                }else if ($fee['fi_id']==145){
                                    $fee['amount']=265.00;
                                }else if ($fee['fi_id']==144){
                                    $fee['amount']=477.00;
                                }else if ($fee['fi_id']==146){
                                    $fee['amount']=53.00;
                                }else if ($fee['fi_id']==80){
                                    $fee['amount']=$fee['amount'];
                                }
                            }
                        }

                        $data_detail = array(
                            'proforma_invoice_main_id' => $main_id,
                            'fi_id' => $fee['fi_id'],
                            'fee_item_description' => $fee['fi_name'],
                            'cur_id' => $currency_id,
                            'amount' => $fee['amount'],
                            'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($fee['amount'], $fee['fspi_currency_id'])
                        );

                        $invoice_detail_id = $proformaInvoiceDetailDb->insert($data_detail);

                        //add proforma invoice subject if any
                        if (isset($fee['subject'])) {
                            foreach ($fee['subject'] as $subject) {
                                $data_psub = array(
                                    'proforma_invoice_main_id' => $main_id,
                                    'proforma_invoice_detail_id' => $invoice_detail_id,
                                    'subject_id' => $subject['IdSubject'],
                                    'amount' => $subject['amount']
                                );

                                $proformaInvoiceSubjectDb->insert($data_psub);
                            }
                        }
                    }
                }

                //update processing fee info to txn table
                $txnUpdData = array('at_processing_fee' => 'ISSUED');
                $n = $db->update('applicant_transaction', $txnUpdData, 'at_trans_id = ' . $txnId);


                /*}else{
					throw new Exception('proforma already issued for this applicant');
				}*/

            } else {
                //TODO: check if already issue proforma invoice

                //if( $txnData['at_tution_fee']==null ){

                if ($invoice_desc == null && $level == 1) {
                    $invoice_desc = $feeGroupData['fc_desc'];
                }

                if ($invoice_desc == null) {
                    $invoice_desc = $feeGroupData['fc_desc'];
                }

                /**
                 * checking proforma has been generated or not
                 * if exist -> exclude
                 * if not -> generate
                 */

                $selectProforma = $db->select()
                    ->from(array('a' => 'proforma_invoice_main'))
                    ->where('a.trans_id = ?', $txnId)
                    ->where('a.fee_category = ?', $fee_category_id)
                    ->where("a.status ='A'");

                $proformaDataChecking = $db->fetchRow($selectProforma);

                if (!$proformaDataChecking) {

                    $bill_no = $this->getBillSeq(1, date('Y'));

                    //add main
                    $data = array(
                        'bill_number' => $bill_no,
                        'appl_id' => $txnData['appl_id'],
                        'trans_id' => $txnData['at_trans_id'],
                        'semester' => $semester_id,
                        'bill_amount' => $total_invoice_amount,
                        'bill_description' => $invoice_desc,
                        'program_id' => $txnData['ap_prog_id'],
                        'creator' => $creator,
                        'fs_id' => $txnData['at_fs_id'],
                        'status' => 'A',
                        'currency_id' => $feeStructureData['fs_cur_id'],
                        'bill_amount_default_currency' => $total_invoice_amount_default_currency,
                        'invoice_type' => 'TUTION_FEE',
                        'fee_category' => $fee_category_id,
                        'branch_id' => $txnData['branch_id']
                    );

                    if ($semester) {
                        $data['academic_year'] = $semester['AcademicYear'];
                    }

                    $main_id = $proformaInvoiceMainDb->insert($data);

                    //add detail
                    $proformaInvoiceSubjectDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();

                    foreach ($fee_group as $fee) {
                        //addedby Jasdy
                        if (!isset($fee['fsi_cur_id']))
                            $fee['fsi_cur_id'] = $fee['fspi_currency_id'];

                        if (isset($txnData['IdScheme']) && $txnData['IdScheme']==11 && $txnData['appl_nationality']!=96){
                            if (strtotime('2016-04-01') <= strtotime(date('Y-m-d'))){
                                if ($fee['fi_id']==143){ //processing fee
                                    $fee['amount']=1208.40;
                                }else if ($fee['fi_id']==145){
                                    $fee['amount']=265.00;
                                }else if ($fee['fi_id']==144){
                                    $fee['amount']=1060.00;
                                }else if ($fee['fi_id']==146){
                                    $fee['amount']=106.00;
                                }else if ($fee['fi_id']==80){
                                    $fee['amount']=$fee['amount']*2;
                                }
                            }
                        }

                        //end added by jasdy
                        $data_detail = array(
                            'proforma_invoice_main_id' => $main_id,
                            'fi_id' => $fee['fi_id'],
                            'fee_item_description' => $fee['fi_name'],
                            'cur_id' => $fee['fsi_cur_id'],
                            'amount' => $fee['amount'],
                            'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($fee['amount'], $fee['fsi_cur_id'])
                        );

                        $invoice_detail_id = $proformaInvoiceDetailDb->insert($data_detail);

                        //add proforma invoice subject if any
                        if (isset($fee['subject'])) {
                            foreach ($fee['subject'] as $subject) {
                                $data_psub = array(
                                    'proforma_invoice_main_id' => $main_id,
                                    'proforma_invoice_detail_id' => $invoice_detail_id,
                                    'subject_id' => $subject['IdSubject'],
                                    'cur_id' => $fee['fsi_cur_id'],
                                    'amount' => $subject['amount'],
                                    'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($subject['amount'], $fee['fsi_cur_id'])
                                );

                                $proformaInvoiceSubjectDb->insert($data_psub);
                            }
                        }
                    }

                    //update tution fee info to txn table
                    $txnUpdData = array('at_tution_fee' => 'ISSUED');
                    $n = $db->update('applicant_transaction', $txnUpdData, 'at_trans_id = ' . $txnId);

                }

                /*}else{
					throw new Exception('proforma already issued for this applicant');
				}*/

            }
        }

        return true;
    }

    public function getApplicantSemesterFeeInfo($txnId = null, $semester_id = null, $semlevel = 0, $idCountry = 0, $cur = 0)
    {

        if ($txnId == null) {
            throw new Exception('No Transaction ID');
        }

        if ((int)$semlevel > 1 && $semester_id == null) {
            throw new Exception('No Semester ID');
        }

        //get applicant & txn info
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'applicant_profile'))
            ->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
            ->where('b.at_trans_id = ?', $txnId);

        $txnData = $db->fetchRow($select);

        if ($txnData == null) {
            throw new Exception('No applicant/student data');
        }


        //get applicant program
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'applicant_program'))
            ->join(array('b' => 'tbl_program'), 'b.IdProgram = a.ap_prog_id')
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = a.ap_prog_scheme')
            ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=c.mode_of_program', array('ProgramMode' => 'DefinitionDesc', 'ProgramModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=c.mode_of_study', array('StudyMode' => 'DefinitionDesc', 'StudyModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=c.program_type', array('ProgramType' => 'DefinitionDesc', 'ProgramTypeMy' => 'BahasaIndonesia'))
            ->where('a.ap_at_trans_id = ?', $txnId);

        $programData = $db->fetchRow($select);

        //semester data
        if ($semester_id) {
            $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
            $semester = $semesterDb->getData($semester_id);
        } else {
            $semester = null;
        }

        if ($txnData == null) {
            throw new Exception('No program data');
        }

        $feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
        $fsProgramInfo = $feeStructureProgramDb->getStructureData($txnData['at_fs_id'], $programData['ap_prog_id'], $programData['ap_prog_scheme']);
        $fsPid = $txnData['at_fs_id'];
        $fsProgram = $programData['ProgramName'];
        $fsPscheme = $programData['ProgramMode'] . ' ' . $programData['StudyMode'] . ' ' . $programData['ProgramType'];
        if (empty($fsProgramInfo)) {
            throw new Exception("Fee Structure Programme Item not found for id ($fsPid), programme ($fsProgram), scheme ($fsPscheme)");
        }

        //first sem credit hour
        $ch_bil_subject = 0;
        if ($semlevel == 1) {

            //$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
            //$fsProgramInfo = $feeStructureProgramDb->getStructureData($txnData['at_fs_id'],$programData['IdProgram'],$programData['ap_prog_scheme']);


            //check if setting for default 1st sem credithour / bil subject
            if (isset($fsProgramInfo) && isset($fsProgramInfo['fsp_first_sem_sks']) && $fsProgramInfo['fsp_first_sem_sks'] != 0) {

                $ch_bil_subject = $fsProgramInfo['fsp_first_sem_sks'];

            } else {
                //landscape
                $landscape_id = icampus_Function_Application_Landscape::getApplicantLandscapeId($txnId);

                //get subject
                $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
                $compulsory_course = $landscapeSubjectDb->getCommonCourse($programData['IdProgram'], $landscape_id, 1);
                //$elective_course = $landscapeSubjectDb->getCommonCourse($programData['IdProgram'],$landscape_id,2);

                //$arr_subject = array_merge($compulsory_course, $elective_course);
                $arr_subject = $compulsory_course;

                //sum CH or bil subject
                for ($i = 0; $i < sizeof($arr_subject); $i++) {
                    $ch_bil_subject += $arr_subject[$i]['CreditHours'];
                }
            }

        } else {
            //TODO:get subject registered in particular semester

            $arr_subject = array();
        }


        //get fee structure and set if not set yet
        if ($txnData['at_fs_id'] == null || $txnData['at_fs_id'] == 0) {
            $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
            $feeStructureData = $feeStructureDb->getApplicantFeeStructure($programData['ap_prog_id'], $programData['ap_prog_scheme'], $txnData['at_intake'], $this->getFeeStructureCategory($txnId));

            $txnData['at_fs_id'] = $feeStructureData['fs_id'];

            //update applicnt program
            $db->update('applicant_transaction', array('at_fs_id' => $feeStructureData['fs_id']), 'at_trans_id = ' . $txnId);
        }


        /*
		 * FEE STRUCTURE CALCULATION
		*/

        $fee_item_to_charge = array();

        //get program_item : application
        if ($semlevel == 0) {
            $fsProgramItem = $feeStructureProgramDb->getItemData($txnData['at_fs_id'], $fsProgramInfo['fsp_id'], 'application');
        } else {
            $fsProgramItem = $feeStructureProgramDb->getItemData($txnData['at_fs_id'], $fsProgramInfo['fsp_id'], 'registration');
        }


        foreach ($fsProgramItem as $m => $fsitem) {
            $fee_item = array();

            //initial fee
            if ($fsitem['fspi_document_type'] == 811) {
                $landscape_id = icampus_Function_Application_Landscape::getApplicantLandscapeId($txnId);
                $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
                $landscapeNewDB = new App_Model_General_DbTable_Landscape();
                $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
                $intakeDb = new GeneralSetup_Model_DbTable_Intake();
                $semDb = new GeneralSetup_Model_DbTable_Semestermaster();

                $info = $landscapeDb->getData($landscape_id);

                if (empty($info)) {
                    throw new Exception('Cannot find Landscape for applicant');
                }

                //$intake = $intakeDb->fngetIntakeDetails($info['IdStartSemester']);
                $intake = $landscapeNewDB->getIntakeInfo($txnData['at_intake']);


                if (empty($intake)) {
                    throw new Exception('Cannot find Intake for landscape');
                }

                $semester = $landscapeNewDB->getSemesterInfo($intake['sem_year'], $intake['sem_seq']);
                //$semester = $semDb->getSemBySeq($intake[0]['sem_year'], $intake[0]['sem_seq']);

                if (empty($semester)) {
                    throw new Exception('Invalid Semester ID');
                }

                //171 short 172 long
                $semester_type = $semester['SemesterType'];

                //get prereg subjects
                $preregdata = $landscapeSubjectDb->getLandscapeSubjectTag($landscape_id);
                $subprereg = json_decode($preregdata['subjectdata'], true);
                $preregdatabytype = $this->getPreregData($subprereg);

                $total_subject = $preregdatabytype[$semester_type]['count'];
                $fsitem['subject'] = isset($preregdatabytype[$semester_type] ['subject']) ? $preregdatabytype[$semester_type] ['subject'] : array();


                //match against calctype
                if ($fsitem['fi_amount_calculation_type'] == 301) {
                    if (!empty($fsitem['subject'])) {
                        foreach ($fsitem['subject'] as $key => $subject) {
                            $fsitem['subject'][$key]['amount'] = $fsitem['fspi_amount'];
                        }

                        $fsitem['fspi_amount'] = $fsitem['fspi_amount'] * $total_subject;
                    }
                }
            }
            $fsitem['idcountry'] = $idCountry;

            //amount
//            if ($fsitem['fspi_amount'] > 0) {
//                $fsitem['amount'] = $fsitem['fspi_amount'];
//
//            } else {

            //check country origin
            if ($fsitem['fi_amount_calculation_type'] == 618) {
                //get fee_item_category
                $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                $feeCountryData = $feeCountryDB->getItemData($fsitem['fspi_fee_id'], $idCountry, $cur);

                $fsitem['amount'] = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
            }else{
                $fsitem['amount'] = $fsitem['fspi_amount'];
            }
//            }

            $fee_item_to_charge[] = $fsitem;
        }

        foreach ($fee_item_to_charge as $n => $fee) {

            if ($fee['amount']) {

            } else {
                unset($fee_item_to_charge[$n]);
            }
        }

//		echo "<pre>";
//		print_r($fsitem);
//		echo "<hr>";
//		exit;

        return array_values($fee_item_to_charge);
    }

    public function getPreregData($data)
    {
        $result = array();
        foreach ($data as $sub_id => $subdata) {
            foreach ($subdata as $semtype => $val) {
                if ($val == 1) {
                    $result[$semtype] = !isset($result[$semtype]) ? array() : $result[$semtype];
                    if (!isset($result[$semtype]['count'])) {
                        $result[$semtype]['count'] = 0;
                    }

                    $result[$semtype]['count']++;
                    $result[$semtype]['subject'][] = array('IdSubject' => $sub_id, 'amount' => 0);
                }
            }
        }

        return $result;
    }

    /*
	 * Function to get applicant proforma invoice
	 */
    public function getApplicantProformaInfo($txnId, $level = 0, $fee_category_id = null)
    {


        if ($level == 0) {
            $invoice_type = 'PROCESSING';
        } else
            if ($level == 1) {
                $invoice_type = 'TUTION_FEE';
            } else {
                throw new Exception('No applicant invoice for level more than 1');
            }


        $result = array();

        //get applicant & txn info
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'applicant_profile'))
            ->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
            ->where('b.at_trans_id = ?', $txnId);

        $txnData = $db->fetchRow($select);


        if ($txnData == null) {
            throw new Exception('No applicant/student data');
        }

        $result['profile'] = $txnData;

        //get proforma invoice Main
        $select = $db->select()
            ->from(array('a' => 'proforma_invoice_main'))
            //->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id', array())
            ->joinLeft(array('c' => 'tbl_fee_category'), 'c.fc_id = a.fee_category')
            ->where('a.trans_id = ?', $txnId)
            ->where("a.invoice_type = ?", $invoice_type)
            ->where("a.status = ?", 'A');

        if ($fee_category_id != null) {
            $select->where('a.fee_category = ' . (int)$fee_category_id);
        }

        $proforma_invoice = $db->fetchAll($select);
        $arrayCurrency = array();
        $arrayCurrencyData = array();
        //get proforma invoice detail
        for ($i = 0; $i < sizeof($proforma_invoice); $i++) {

            $proforma_invoice_detail = null;

            $select_detail = $db->select()
                ->from(array('a' => 'proforma_invoice_detail'))
                ->where('a.proforma_invoice_main_id = ?', $proforma_invoice[$i]['id']);

            $proforma_invoice_detail = $db->fetchAll($select_detail);

            if ($proforma_invoice_detail) {
                $proforma_invoice[$i]['detail'] = $proforma_invoice_detail;

                foreach ($proforma_invoice_detail as $pinvoicedet) {

                    $currencyDb = new Studentfinance_Model_DbTable_Currency();
                    $currency = $currencyDb->fetchRow('cur_id = ' . $pinvoicedet['cur_id'])->toArray();

                    $proforma_invoice[$i]['detail']['currency'] = $currency;
                    $arrayCurrency[] = $currency['cur_code'];
                    $arrayCurrencyData[] = $currency;

                }

            }
        }

        $result['proforma_invoice_info'] = $proforma_invoice;
        $result['totalcurrency'] = array_unique($arrayCurrency);
        $result['currencyData'] = $arrayCurrencyData;

        //calculate total amount
        if ($proforma_invoice) {

//			$result['proforma_invoice']['total_amount'] = 0;
//			$result['proforma_invoice']['total_amount_default_currency'] = 1;
            $a = 0;
            foreach ($result['totalcurrency'] as $key => $cur) {
                $curID = $cur;
//				$result['proforma_invoice'][$a]  = 'lala';
                $amountCurrent = 0;
                foreach ($proforma_invoice as $pinvoice) {
                    if ($curID == $pinvoice['detail']['currency']['cur_code']) {
                        $amountCurrent = $amountCurrent + $pinvoice['bill_amount'];
                    }

                    $result['proforma_invoice_total'][$a] = $amountCurrent;
//					$result['proforma_invoice'][$a]['total_amount_default_currency'] = $pinvoice['bill_amount_default_currency'];

                }
                $a++;
            }
            $result['proforma_invoice']['total_amount'] = $amountCurrent;
            /*//inject currency info (taken from 1st invoice)
			$currencyDb = new Studentfinance_Model_DbTable_Currency();
			$currency = $currencyDb->fetchRow('cur_id = '.$proforma_invoice[0]['currency_id'])->toArray();
			
			$result['proforma_invoice']['currency'] = $currency;*/

        } else {
            $result['proforma_invoice']['total_amount'] = 0;
        }


        return $result;

    }

    /*
	 * Generate invoice form proforma invoice
	*/
    public function generateInvoiceFromProforma($proforma_invoice_id, $creator_id = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        if ($proforma_invoice_id == null) {
            throw new Exception('No proforma invoice ID');
        }

        //get proforma data
        $proformaInvoiceMainDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
        $proforma_invoice = $proformaInvoiceMainDb->fetchRow(array('id = ?' => $proforma_invoice_id));

        if ($proforma_invoice == null) {
            throw new Exception('Unknown proforma invoice');
        } else {
            $proforma_invoice = $proforma_invoice->toArray();


            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
                ->from(array('a' => 'applicant_transaction'))
                ->where('a.at_trans_id = ?', $proforma_invoice['trans_id']);

            $txnData = $db->fetchRow($select);

            //get current semester
            $landscapeNewDB = new App_Model_General_DbTable_Landscape();
            $intake = $landscapeNewDB->getIntakeInfo($txnData['at_intake']);
            $semesterinfo = $landscapeNewDB->getSemesterInfo($intake['sem_year'], $intake['sem_seq']);

            $semester = $semesterinfo['IdSemesterMaster'];


            //get current currency rate 
            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
            $currencyRate = $currencyRateDB->getCurrentExchangeRate($proforma_invoice['currency_id']);
            $proforma_invoice['exchange_rate'] = $currencyRate['cr_id'];

            //proforma invoice detail
            $proformaInvoiceDetailDb = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
            $proforma_invoice_detail = $proformaInvoiceDetailDb->fetchAll(array('proforma_invoice_main_id = ?' => $proforma_invoice['id']));

            if ($proforma_invoice_detail) {
                $invoice_detail = $proforma_invoice_detail->toArray();

                //add invoice subject if any
                $proformaInvoiceSubjectDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();
                foreach ($invoice_detail as $index => &$fee_item) {

                    $currencyRate = $currencyRateDB->getCurrentExchangeRate($fee_item['cur_id']);
                    $fee_item['exchange_rate'] = $currencyRate['cr_id'];


                    $where = array(
                        'proforma_invoice_main_id = ?' => $proforma_invoice['id'],
                        'proforma_invoice_detail_id = ?' => $fee_item['id']
                    );
                    if ($invoiceSubject = $proformaInvoiceSubjectDb->fetchAll($where)) {
                        $fee_item['subject'] = $invoiceSubject->toArray();
                    }

                }

                $proforma_invoice['fee_item'] = $invoice_detail;


            } else {
                $proforma_invoice['fee_item'] = null;
            }

            /*echo "<pre>";
			print_r($proforma_invoice);
			exit;*/


            //get student id
            $selectApplicant = $db->select()
                ->from(array('a' => 'applicant_transaction'))
                ->where("at_trans_id = ?", $proforma_invoice['trans_id']);

            $rowApplicant = $db->fetchRow($selectApplicant);
            $IdStudentRegistration = isset($rowApplicant['at_IdStudentRegistration']) ? $rowApplicant['at_IdStudentRegistration'] : 0;

            $db = Zend_Db_Table::getDefaultAdapter();
            $db->beginTransaction();
            try {

                //insert invoice main
                $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                $amountMain = $proforma_invoice['bill_amount'];

                $bill_no = $this->getBillSeq(2, date('Y'));
                $data_invoice = array(
                    'bill_number' => $bill_no,
                    'appl_id' => $proforma_invoice['appl_id'],
                    'trans_id' => $proforma_invoice['trans_id'],
                    'IdStudentRegistration' => $IdStudentRegistration,
                    'academic_year' => $proforma_invoice['academic_year'],
                    'semester' => $semester,
                    'bill_amount' => $amountMain,
                    //'bill_amount_default_currency' => $proforma_invoice['bill_amount_default_currency'],
                    'bill_paid' => 0,
                    'bill_balance' => $amountMain,
                    'bill_description' => $proforma_invoice['bill_description'],
                    'program_id' => $proforma_invoice['program_id'],
                    'fs_id' => $proforma_invoice['fs_id'],
                    'currency_id' => $proforma_invoice['currency_id'],
                    'exchange_rate' => $proforma_invoice['exchange_rate'],
                    'proforma_invoice_id' => $proforma_invoice['id'],
                    'invoice_date' => date('Y-m-d'),
                    'date_create' => date('Y-m-d H:i:s')
                );

                if ($creator_id != null) {
                    $data_invoice['creator'] = $creator_id;
                }

//				echo "<pre>";
//			print_r($data_invoice);

                $invoice_id = $invoiceMainDb->insert($data_invoice);

                //update proforma invoice id
                $db->update('proforma_invoice_main', array('invoice_id' => $invoice_id), 'id = ' . $proforma_invoice['id']);

                //insert invoice detail
                $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                for ($a = 0; $a < sizeof($proforma_invoice['fee_item']); $a++) {
                    $fee_item_data = $proforma_invoice['fee_item'][$a];

                    //checking GST
                    $checkGst = $this->checkingGST($fee_item_data['fi_id']);

                    $amount = $fee_item_data['amount'];
                    /*if($checkGst){
						$amount = $amount * $checkGst['fi_gst_tax'];
					}*/

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invoice_id,
                        'fi_id' => $fee_item_data['fi_id'],
                        'fee_item_description' => $fee_item_data['fee_item_description'],
                        'cur_id' => $fee_item_data['cur_id'],
                        'amount' => $amount,
                        'balance' => $amount,
                        'exchange_rate' => $proforma_invoice['exchange_rate']
                        //'amount_default_currency' => $fee_item_data['amount_default_currency']
                    );

                    $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                    if (isset($fee_item_data['subject'])) {
                        $subject_list = $fee_item_data['subject'];

                        //insert invoice subject if any
                        if ($subject_list) {
                            $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
                            foreach ($subject_list as $subject) {

                                $amountSubject = $subject['amount'];
                                /*if($checkGst){
								$amountSubject = $amountSubject * $checkGst['fi_gst_tax'];
							}*/

                                $dataSubject = array(
                                    'invoice_main_id' => $invoice_id,
                                    'invoice_detail_id' => $invoice_detail_id,
                                    'subject_id' => $subject['subject_id'],
                                    'amount' => $amountSubject,
                                    'balance' => $amountSubject,
                                );

                                $invoiceSubjectDb->insert($dataSubject);
                            }
                        }

                    }

                }

                $db->commit();

            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }

        }

        return true;
    }


    /*
	 * Generate proforma invoice for student
	*/
    public function generateStudentProformaInvoice()
    {
        //TODO: generate invoice for student
    }


    /*
	 * Get Fee structure info by semester (applicant)
	*/


    /*
	 * Get Fee structure info by semester (student)
	*/
    public function getStudentSemesterFeeInfo($idStudentRegistration = null, $semester_id, $isStudent = true)
    {

        if ($idStudentRegistration == null) {
            throw new Exception('No Transaction ID');
        }

        if ($semester_id == null) {
            throw new Exception('No Semester ID');
        }

        //get student info


        //get program info


        //semester data
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $semester = $semesterDb->getData($semester_id);

        if ($txnData == null) {
            throw new Exception('No program data');
        }


        $semlevel = 0;
        //student process
        if ($isStudent) {
            //student current level
            $studentRegistrationDb = new App_Model_Record_DbTable_StudentRegistration();
            $semlevel = $studentRegistrationDb->getStudentLevelByProgram($txnData['at_intake'], $programData['ap_prog_id']);

            //get registered subject
            $registered_course_list = $studentRegistrationDb->getCourseRegisteredBySemester();

        }


        //get fee structure and set if not set yet
        if ($txnData['at_fs_id'] == null || $txnData['at_fs_id'] == 0) {
            $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
            $feeStructureData = $feeStructureDb->getApplicantFeeStructure($programData['ap_prog_id'], $programData['ap_prog_scheme'], $txnData['at_intake'], $this->getFeeStructureCategory($txnId));

            $txnData['at_fs_id'] = $feeStructureData['fs_id'];

            //update applicnt program
            $db->update('applicant_transaction', array('at_fs_id' => $feeStructureData['fs_id']), 'at_trans_id = ' . $txnId);
        }


        /*
		 * FEE STRUCTURE CALCULATION
		*/

        $fee_item_to_charge = array();

        //fee structure detail
        $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
        $fee_item_list = $feeStructureItemDb->getStructureData($txnData['at_fs_id']);
        //debug($fee_item_list);

        //do looping to get what fee item to charge
        for ($i = 0; $i < sizeof($fee_item_list); $i++) {

            $fee_item = $fee_item_list[$i];

            //application fees (sem 0)
            if ($semlevel == 0) {

                if ($fee_item['fi_frequency_mode'] == 630) {
                    $fee_item_to_charge[] = $fee_item;
                }

            } else {

                //sem 1 only
                if ($fee_item['fi_frequency_mode'] == 304 && $semlevel == 1) {
                    $fee_item_to_charge[] = $fee_item;
                }

                //every sem
                if ($fee_item['fi_frequency_mode'] == 303) {
                    $fee_item_to_charge[] = $fee_item;
                }

                //every year
                if ($fee_item['fi_frequency_mode'] == 304) {

                    //get intake to get complete Anniversary
                    $intakeDb = new GeneralSetup_Model_DbTable_Intake();
                    $intake = $intakeDb->fetchRow('IdIntake = ' . $txnData['at_intake']);

                    if ($intake['sem_seq'] == $semester['sem_seq']) {
                        $fee_item_to_charge[] = $fee_item;
                    }


                }

                //defined semester
                if ($fee_item['fi_frequency_mode'] == 305) {

                    //get sem defined
                    $feeSemesterDb = new Studentfinance_Model_DbTable_FeeStructureItemSemester();
                    if ($feeSemesterDb->getStructureItemData($fee_item['fsi_id'], $semlevel)) {
                        $fee_item_to_charge[] = $fee_item;
                    }

                }

            }
        }

        //do looping to calculate charges from fee item to charge
        for ($i = 0; $i < sizeof($fee_item_to_charge); $i++) {

            $fee_item = &$fee_item_to_charge[$i];


            //fix amount
            if ($fee_item['fi_amount_calculation_type'] == 300) {
                $fee_item['amount'] = $fee_item['fsi_amount'];
            }

            //fix amount selected subject
            if ($fee_item['fi_amount_calculation_type'] == 459) {

            }

            //fix amount by country origin
            if ($fee_item['fi_amount_calculation_type'] == 618) {

            }

            //credit hour multiplication
            if ($fee_item['fi_amount_calculation_type'] == 299) {

            }

            //credit hour multiplication (level)
            if ($fee_item['fi_amount_calculation_type'] == 632) {

            }

            //subject multiplication
            if ($fee_item['fi_amount_calculation_type'] == 301) {

            }

            //subject multiplication (level)
            if ($fee_item['fi_amount_calculation_type'] == 633) {

            }

        }


        dd($fee_item_to_charge);

        exit;
    }


    /*
	 * function to return true if txn profile's nationality is Indonesia
	*/
    private function getFeeStructureCategory($txn_id)
    {

        //get profile
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('at' => 'applicant_transaction'))
            ->join(array('ap' => 'applicant_profile'), 'ap.appl_id = at.at_appl_id')
            ->where("at_trans_id = " . $txn_id);

        $row = $db->fetchRow($select);

        return $row['appl_category'];
    }

    /*
	 * Get Bill no from mysql function
	 */
    public function getBillSeq($type, $year)
    {

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }

    public function getTuitionFee($fsId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'fee_structure'), array('value' => '*'))
            ->joinLeft(array('b' => 'tbl_currency'), 'a.fs_cur_id = b.cur_id')
            ->where('a.fs_id  = ?', $fsId);

        $result = $db->fetchRow($select);
        return $result;
    }


    public function checkOutstandingBalance($transID, $type)
    {

        /*
         1. checking outstanding proforma invoice where A n invoice_id = null
         2. checking advance payment
         *  invoice & proforma & advance payment amount in MYR
         */

        $status = null;
        $db = Zend_Db_Table::getDefaultAdapter();


        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.date_create',
                    'amount' => 'bill_balance',
                    'invoice_no' => 'bill_number',
                    'currency_id' => 'currency_id',
                )
            )
//					->where('im.trans_id = ?', $transID)
            ->where("im.status = 'A'");
        if ($type == 1) { //applicant
            $select_invoice->where('im.trans_id  = ?', $transID);
        } elseif ($type == 2) { //student
            $select_invoice->where('im.IdStudentRegistration  = ?', $transID);
        }

        $select_proforma = $db->select()
            ->from(array('im' => 'proforma_invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.date_create',
                    'amount' => 'bill_amount',
                    'invoice_no' => 'bill_number',
                    'currency_id' => 'currency_id',
                )
            )
            ->where('im.trans_id = ?', $transID)
            ->where("im.status = 'A'")
            ->where("im.invoice_id = 0");
        if ($type == 1) { //applicant
            $select_proforma->where('im.trans_id  = ?', $transID);
        } elseif ($type == 2) { //student
            $select_proforma->where('im.IdStudentRegistration  = ?', $transID);
        }

        $select = $db->select()
            ->union(array($select_invoice, $select_proforma), Zend_Db_Select::SQL_UNION_ALL)
            ->order("record_date desc");

        $results = $db->fetchAll($select);
        if (!$results) {
            $results = null;
        }

        $result = $db->fetchAll($select);

        $newAmount = 0;
        if ($result) {
            $amount = 0;
            foreach ($result as $data) {
                $amt = $data['amount'];
                if ($data['currency_id'] == 2) {
                    $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                    $rate = $currencyRateDb->getCurrentExchangeRate(2);//usd
                    $amt = round($amt * $rate['cr_exchange_rate'], 2);
                }

                $amount += $amt;
            }

            //check advance payment
            $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
            $advPaymentData = $advancePaymentDb->getApplicantAvdPayment($transID, 1);

            $advAmount = 0;
            if ($advPaymentData) {
                foreach ($advPaymentData as $adv) {
                    $amtAdv = $adv['advpy_total_balance'];
                    if ($adv['advpy_cur_id'] == 2) {
                        $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                        $rate = $currencyRateDb->getCurrentExchangeRate(2);//usd
                        $amtAdv = round($amtAdv * $rate['cr_exchange_rate'], 2);
                    }

                    $advAmount += $amtAdv;
                }
            }
            $newAmount = $amount - $advAmount;
            if ($newAmount <= 0) {
                $status = 1; // no outstanding
            } else {
                $status = 2; // outstanding
            }

        } else {
            $status = 3; // no record/no invoice
        }
        return $status;

    }

    public function updateFeeStructureApplicant()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'applicant_transaction_20141226'), array('at_trans_id', 'at_fs_id'))
            ->join(array('e' => 'applicant_profile'), 'e.appl_id = a.at_appl_id', array())
            ->join(array('b' => 'applicant_program'), 'b.ap_at_trans_id = a.at_trans_id', array('ap_prog_id', 'ap_prog_scheme'))
            ->join(array('c' => 'fee_structure_program'), 'c.fsp_program_id = b.ap_prog_id and c.fsp_idProgramScheme = b.ap_prog_scheme', array('fsp_fs_id'))
            ->join(array('d' => 'fee_structure'), 'd.fs_id = c.fsp_fs_id and d.fs_intake_start = a.at_intake and d.fs_student_category = e.appl_category', array())
            ->where('a.at_intake IN (18,14,50)')
            ->where('a.at_fs_id is null')
            ->group('a.at_trans_id');

        /*	SELECT a.at_trans_id, a.at_fs_id, b.ap_prog_id, b.ap_prog_scheme, c.fsp_fs_id FROM `applicant_transaction` a
join applicant_profile e on e.appl_id = a.at_appl_id
join applicant_program b on b.ap_at_trans_id = a.at_trans_id
join fee_structure_program c on c.fsp_program_id = b.ap_prog_id and c.fsp_idProgramScheme = b.ap_prog_scheme 
join fee_structure d on d.fs_id = c.fsp_fs_id and d.fs_intake_start = a.at_intake and d.fs_student_category = e.appl_category
WHERE `at_intake` IN (18,14,50)  AND `at_fs_id` IS NULL
group by a.at_trans_id*/

        $result = $db->fetchAll($select);
//            echo "<pre>";
//            print_r($result);

        foreach ($result as $data) {

            $dataUpd = array('at_fs_id' => $data['fsp_fs_id']);

            //print_r($dataUpd);
            $id = $data['at_trans_id'];
            $fs_id = $data['fsp_fs_id'];

            echo $sql = "update applicant_transaction set at_fs_id = $fs_id where at_trans_id = $id;";
            echo "<br>";
            $db->update('applicant_transaction_20141226', $dataUpd, $db->quoteInto("at_trans_id = ?", $id));

        }
        // return $result;
    }

    public function updateFeeStructureStudent()
    {//not complete
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('IdStudentRegistration', 'fs_id'))
            ->join(array('b' => 'student_profile'), 'b.id = a.sp_id', array('appl_category'))
//                ->join(array('b'=>'applicant_program'), 'b.ap_at_trans_id = a.at_trans_id',array('ap_prog_id', 'ap_prog_scheme'))
            ->join(array('c' => 'fee_structure_program'), 'c.fsp_program_id = a.IdProgram and c.fsp_idProgramScheme = a.IdProgramScheme', array('fsp_fs_id'))
            ->join(array('d' => 'fee_structure'), 'd.fs_id = c.fsp_fs_id and d.fs_intake_start = a.IdIntake and d.fs_student_category = b.appl_category', array())
            //->where('a.at_intake IN (18,14,50)')
            ->where('a.fs_id is null')
            ->group('a.IdStudentRegistration');

        /*	SELECT a.at_trans_id, a.at_fs_id, b.ap_prog_id, b.ap_prog_scheme, c.fsp_fs_id FROM `applicant_transaction` a
join applicant_profile e on e.appl_id = a.at_appl_id
join applicant_program b on b.ap_at_trans_id = a.at_trans_id
join fee_structure_program c on c.fsp_program_id = b.ap_prog_id and c.fsp_idProgramScheme = b.ap_prog_scheme 
join fee_structure d on d.fs_id = c.fsp_fs_id and d.fs_intake_start = a.at_intake and d.fs_student_category = e.appl_category
WHERE `at_intake` IN (18,14,50)  AND `at_fs_id` IS NULL
group by a.at_trans_id*/

        $result = $db->fetchAll($select);
//            echo "<pre>";
//            print_r($result);

        foreach ($result as $data) {

            $dataUpd = array('fs_id' => $data['fsp_fs_id']);

            //print_r($dataUpd);
            $id = $data['IdStudentRegistration'];
            $fs_id = $data['fsp_fs_id'];

            echo $sql = "update tbl_studentregistration set fs_id = $fs_id where IdStudentRegistration = $id;";
            echo "<br>";
            $db->update('tbl_studentregistration', $dataUpd, $db->quoteInto("IdStudentRegistration = ?", $id));

        }
        // return $result;
    }

    public function calculateInvoiceAmountByCourse($studentID, $semesterID, $courseID, $registrationItem, $type = null, $credithour = null, $schemeMode = 0)
    {

        /*
		 * Script to calculate invoice amount for course registration
		 * Parameter in: Student ID, Semester, Subject Code, Registration Item)
		 * Parameter out : Amount
		 *  1.	Cek  fee structure id (if ada proceed no 2, else assign fee structure)
			2.	Cara calculation : Cek  dekat Fee � Detail tab, 
				a.	Registration item field
				b.	Amount
				c.	Currency
				d.	Calculation type  eg, by credit hour ke, per subject ke
		 * 
		 */

        /*$studentID = 3970; //IdStudentRegistration -> tbl_studentregistration
		$courseID = array(106,114); //course id
		$registrationItem = 830; //registration item_id
		$semesterID = 6; // semester register*/

//		echo "+".$studentID.'@'.$semesterID.'@'.$courseID.'@'.$registrationItem.'<br>';

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];

        if ($checkingInvoice == 1) {


            $db = Zend_Db_Table::getDefaultAdapter();

            $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->join(array('b' => 'student_profile'), 'b.id = a.sp_id', array('*'))
//	                ->join(array('c'=>'tbl_program_scheme'), 'c.IdProgramScheme = a.IdProgramScheme',array('mode_of_study'))
                ->where('a.IdStudentRegistration =?', $studentID);

            $txnData = $db->fetchRow($select);

            $landscapeID = $txnData['IdLandscape'];

            //get program scheme 

            /*$selectScheme = $db->select()
	                ->from(array('a'=>'tbl_program_scheme'))
	                ->where('a.IdProgram =?',$txnData['IdProgram'])
	                ->where('a.mode_of_program =?',$schemeMode);
	        $txnDataScheme = $db->fetchRow($selectScheme);*/

//	        foreach($txnDataScheme as $scheme){
//	        	if($scheme['mode_of_program'] != $schemeMode){
//	        $programScheme = $txnDataScheme['IdProgramScheme'];
//	        	}
//	        }

            //online 577 / face to face 578
            if ($schemeMode) {
                $feeStrData = $feeStructureDb->getApplicantFeeStructure($txnData['IdProgram'], $schemeMode, $txnData['IdIntake'], $txnData['appl_category']);

                if ($feeStrData) {
                    $fsId = $feeStrData['fs_id'];
                } else {
                    $fsId = $txnData['fs_id'];
                }

            } else {
                $fsId = $txnData['fs_id'];
            }


            //get fee structure and set if not set yet
            if ($txnData['fs_id'] == null || $txnData['fs_id'] == 0) {

                $feeStructureData = $feeStructureDb->getApplicantFeeStructure($txnData['IdProgram'], $txnData['IdProgramScheme'], $txnData['IdIntake'], $txnData['appl_category']);

                $txnData['fs_id'] = $feeStructureData['fs_id'];

                //update student fee structure
                $db->update('tbl_studentregistration', array('fs_id' => $feeStructureData['fs_id']), 'IdStudentRegistration = ' . $studentID);
            }
            //get fee item
            $fee_item_to_charge = array();

            //get level
            $selectLevel = $db->select()
                ->from(array("ls" => "tbl_landscapesubject"))
                ->joinLeft(array('l' => 'tbl_landscapeblock'), 'l.idblock=ls.IdLevel', array('l.block as Level'))
                ->where("ls.IdSubject = ?", $courseID)
                ->where('ls.IdLandscape = ?', $landscapeID);
            $rowLevel = $db->fetchRow($selectLevel);


            $level = 0;
            if (isset($rowLevel['Level'])) {
                $level = $rowLevel['Level'];
            }


            //fee structure detail
            $feeStructureItemDb = new App_Model_Finance_DbTable_FeeStructureItem();
            $fsProgramItem = $feeStructureItemDb->getStructureData($fsId, $registrationItem, $level, $type);

            if (!empty($fsProgramItem)) {
                foreach ($fsProgramItem as $keyfs => $fsitem) {

                    $fee_item = array();

                    $landscape_id = icampus_Function_Application_Landscape::getStudentLandscapeId($studentID);
                    $landscapeDb = new App_Model_General_DbTable_Landscape();
                    $landscapeNewDB = new App_Model_General_DbTable_Landscape();
                    $landscapeSubjectDb = new App_Model_General_DbTable_Landscapesubject();
                    $intakeDb = new App_Model_General_DbTable_Intake();
                    $semDb = new App_Model_General_DbTable_Semestermaster();

                    $info = $landscapeDb->getData($landscape_id);

                    /*if ( empty($info) )
					{
						throw new Exception('Cannot find Landscape for student');
					}
					
					$intake = $landscapeNewDB->getIntakeInfo($txnData['IdIntake']);
					
					
					if ( empty($intake) )
					{
						throw new Exception('Cannot find Intake for landscape');
					}
					
					$semester = $landscapeNewDB->getSemesterInfo($intake['sem_year'], $intake['sem_seq']);
					
					if ( empty($semester) )
					{
						throw new Exception( 'Invalid Semester ID');
					}
					*/
                    //get credit_hour_registered

                    //get landscape subject
                    $compulsory_course = $landscapeSubjectDb->getCommonLandscapeCourse($landscape_id, $registrationItem, $semesterID, $courseID);

                    $arr_subject = $compulsory_course;

                    $ch_bil_subject = 0;
                    for ($i = 0; $i < sizeof($arr_subject); $i++) {
                        $ch_bil_subject += $arr_subject[$i]['CreditHours'];
                    }

                    $selectRegSubject = $db->select()
                        ->from(array('a' => 'tbl_studentregsubjects'), array('*', 'CreditHours' => 'a.credit_hour_registered'))
                        ->joinLeft(array("s" => "tbl_subjectmaster"), 's.IdSubject=a.IdSubject', array('BahasaIndonesia', 'SubCode', 'SubjectName' => 'SubjectName'))
                        ->where('a.IdStudentRegistration =?', $studentID)
                        ->where('a.IdSubject =?', $courseID)
                        ->where('a.IdSemesterMain =?', $semesterID)
                        ->where('a.credit_hour_registered IS NOT NULL ');

                    $txnRegCredit = $db->fetchRow($selectRegSubject);

                    if ($txnRegCredit) {
                        $ch_bil_subject = $txnRegCredit['credit_hour_registered'];
                        $compulsory_course[0] = $txnRegCredit;
                    } else {

                        //get landscape subject
                        $compulsory_course = $landscapeSubjectDb->getCommonLandscapeCourse($landscape_id, $registrationItem, $semesterID, $courseID);

                        $arr_subject = $compulsory_course;

                        $ch_bil_subject = 0;
                        for ($i = 0; $i < sizeof($arr_subject); $i++) {
                            $ch_bil_subject += $arr_subject[$i]['CreditHours'];
                        }
                    }

                    if ($credithour) {
                        $ch_bil_subject = $credithour;
                    } else {
                        $ch_bil_subject = $ch_bil_subject;
                    }

                    if (!empty($fsitem['subject'])) {
                        foreach ($fsitem['subject'] as $key => $subject) {
                            if ($courseID == $subject['IdSubject']){
                                $fsitem['fsi_amount'] = $subject['fsisub_subject_amount'];
                                $fsitem['fi_amount_calculation_type'] = $subject['fsisub_amount_calculation_type'];
                            }
                        }
                    }

                    $fsitem['key'] = 0;
                    $fsitem['subject'] = $compulsory_course;
                    $fsitem['creditHours'] = $ch_bil_subject;
                    $fsitem['fs_id'] = $fsId;

                    $total_subject = count($compulsory_course);

                    //Subject Multiplication
                    if ($fsitem['fi_amount_calculation_type'] == 301) {
                        if (!empty($fsitem['subject'])) {
                            foreach ($fsitem['subject'] as $key => $subject) {
                                $fsitem['subject'][$key]['amount'] = $fsitem['fsi_amount'];
                            }

                            $fsitem['fsi_amount'] = $fsitem['fsi_amount'] * $total_subject;


                        }
                    }

                    //Credit Hour Multiplication
                    if ($fsitem['fi_amount_calculation_type'] == 299) {
                        if (!empty($fsitem['subject'])) {
                            foreach ($fsitem['subject'] as $key => $subject) {
                                $fsitem['subject'][$key]['amount'] = $fsitem['fsi_amount'];
                            }

                            $fsitem['fsi_amount'] = $fsitem['fsi_amount'] * $ch_bil_subject;

                        }
                    }

                    //fix amount
                    if ($fsitem['fi_amount_calculation_type'] == 300) {
                        $fsitem['amount'] = $fsitem['fsi_amount'];
                    }


                    //Subject Multiplication (Level)
                    if ($fsitem['fi_amount_calculation_type'] == 633) {
                        $fsitem['amount'] = $fsitem['fsi_amount'];
                        $fsitem['key'] = 1;
                    }

                    //temporary solution 
                    if ($credithour == 'X') {
                        $fsitem['fsi_amount'] = 1000;
                    }

                    //amount
//            if ($fsitem['fspi_amount'] > 0) {
//                $fsitem['amount'] = $fsitem['fspi_amount'];
//
//            } else {

                    //check country origin
                    if ($fsitem['fi_amount_calculation_type'] == 618) {
                        //get fee_item_category
                        $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                        $feeCountryData = $feeCountryDB->getItemData($fsitem['fspi_fee_id'], $idCountry, $cur);

                        $fsitem['amount'] = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
                    }else{
                        $fsitem['amount'] = isset($fsitem['fspi_amount']) ? $fsitem['fspi_amount']:0.00;
                    }
//            }

                    $fee_item_to_charge[] = $fsitem;
                }
            }

            $fee_item_to_charge_new = array();
            foreach ($fee_item_to_charge as $key => $dataVal) {
                if ($dataVal['key'] == 0) {
                } else {
                    if ($dataVal['level'] == 0) {
                        unset($fee_item_to_charge[$key]);
                    }
                }
                $fee_item_to_charge_new = array_values($fee_item_to_charge);

            }
//			echo "<pre>";
//			print_r($fee_item_to_charge_new);

            return $fee_item_to_charge_new;
        } else {
            return null;
        }

    }


    public function generateInvoiceStudent($studentID, $semesterID, $subjectID, $type = null, $credithour = null, $commit = 0, $schemeMode = 0)
    {

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

//		$type = 'RPT';
        $db = Zend_Db_Table::getDefaultAdapter();

        if ($semesterID == null) {
            throw new Exception('No semester ID');
        }

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];

        if ($checkingInvoice == 1) {

            //generate invoice calculation type = yearly
			$this->generateInvoiceYearly($studentID); // enable on 22/09/2015
			$this->generateInvoiceDefinedSemester($studentID); // enable by izham

            //get course registered
            $regSubjectsDb = new CourseRegistration_Model_DbTable_Studentregsubjects();

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects_detail'))
                ->join(array('b' => 'tbl_subjectmaster'), 'b.IdSubject = a.subject_id')
                ->where('a.student_id = ?', $studentID)
                ->where('a.semester_id = ?', $semesterID)
                ->where('a.subject_id = ?', $subjectID)
                ->where('a.invoice_id is null');
            //->where('a.item_id = ?', $itemID);

            $regSubjectStudent = $db->fetchAll($select);
            //var_dump($regSubjectStudent); exit;
//			echo "<pre>";
//			print_r($regSubjectStudent);
//			exit;

            //student profile info
            $studentregDB = new CourseRegistration_Model_DbTable_Studentregistration();
            $studentdata = $studentregDB->getData($studentID);

            $invoice_data = array();

            if ($regSubjectStudent == null) {
                throw new Exception('No registered course');
            } else {
                $i = 0;

                $invoice_data['IdSubject'] = $regSubjectStudent[0]['IdSubject'];
                $invoice_data['SubCode'] = $regSubjectStudent[0]['SubCode'];
                $invoice_data['SubjectName'] = $regSubjectStudent[0]['SubjectName'];
                $amountInv = 0;

                foreach ($regSubjectStudent as $data) {

                    $generate = true;

                    if ($studentInfo['mode_of_program']==56){
                        $wsub = $this->checkSubjectBefore($studentID, $subjectID, $semesterID);

                        if ($wsub){
                            if ($data['item_id']==890){
                                $itemw = $this->getItemBefore($wsub[0]['IdStudentRegSubjects']);

                                if (isset($itemw['refund']) && $itemw['refund'] != 100) {
                                    $generate = false;
                                }
                            }
                        }
                    }

                    if ($generate == true) {

                        $calcamount = $this->calculateInvoiceAmountByCourse($studentID, $semesterID, $subjectID, $data['item_id'], $type, $credithour, $schemeMode);

                        if ($calcamount) {

                            $calInfo = $calcamount[0];
                            $invoice_data['fs_id'] = $calInfo['fs_id'];
                            $invoice_data['fee_item'][$i] = $calcamount[0];
                            $invoice_data['fee_item'][$i]['idRegSubj'] = $data['id'];
                            $invoice_data['fee_item'][$i]['invoice_reg'] = $data['invoice_id'];

                            //get current currency rate
                            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                            $currencyRate = $currencyRateDB->getCurrentExchangeRate($calInfo['cur_id']);
                            $invoice_data['fee_item'][$i]['exchange_rate'] = $currencyRate['cr_id'];
                            $invoice_data['fee_item'][$i]['cur_id'] = $calInfo['cur_id'];

                            $amountInv += $calInfo['fsi_amount'];
                            $i++;
                        }
                    }


                }

                $invoice_data['amount'] = $amountInv;

                /*echo "<pre>";
				print_r($invoice_data);
				exit;*/


                if ($commit == 1) {
                    $db->beginTransaction();
                }

                if ($amountInv != 0) {

                    try {

                        //insert into invoice_main
                        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                        $bill_no = $this->getBillSeq(2, date('Y'));
                        $data_invoice = array(
                            'bill_number' => $bill_no,
                            'appl_id' => $studentdata['appl_id'],
                            'trans_id' => $studentdata['transaction_id'],
                            'IdStudentRegistration' => $studentID,
                            'academic_year' => '',
                            'semester' => $semesterID,
                            'bill_amount' => $invoice_data['amount'],
                            //'bill_amount_default_currency' => $proforma_invoice['bill_amount_default_currency'],
                            'bill_paid' => 0,
                            'bill_balance' => $invoice_data['amount'],
                            'bill_description' => 'Tuition Fee for ' . $invoice_data['SubCode'],
                            'program_id' => $studentdata['IdProgram'],
                            'fs_id' => isset($invoice_data['fs_id']) ? $invoice_data['fs_id'] : $studentdata['fs_id'],
                            'currency_id' => $invoice_data['fee_item'][0]['cur_id'],
                            'exchange_rate' => $invoice_data['fee_item'][0]['exchange_rate'],
                            'invoice_date' => date('Y-m-d'),
                            'date_create' => date('Y-m-d H:i:s'),
                            'creator' => $creator//by admin
                        );

                         $invoice_id = $invoiceMainDb->insert($data_invoice);

                        //insert invoice detail
                        $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                        for ($a = 0; $a < sizeof($invoice_data['fee_item']); $a++) {
                            $fee_item_data = $invoice_data['fee_item'][$a];

                            $idRegSub = $fee_item_data['idRegSubj'];
                            $invoice_reg = $fee_item_data['invoice_reg'];

                            if (!isset($invoice_reg)) {
                                $invoiceDetailData = array(
                                    'invoice_main_id' => $invoice_id,
                                    'fi_id' => $fee_item_data['fi_id'],
                                    'fee_item_description' => $fee_item_data['fi_name'],
                                    'cur_id' => $fee_item_data['cur_id'],
                                    'amount' => $fee_item_data['fsi_amount'],
                                    'balance' => $fee_item_data['fsi_amount'],
                                    'exchange_rate' => $invoice_data['fee_item'][0]['exchange_rate']
                                );


                                $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                                $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();

                                $dataSubject = array(
                                    'invoice_main_id' => $invoice_id,
                                    'invoice_detail_id' => $invoice_detail_id,
                                    'subject_id' => $invoice_data['IdSubject'],
                                    'amount' => $fee_item_data['fsi_amount'],
                                    'balance' => $fee_item_data['fsi_amount'],
                                );

                                $invoiceSubjectDb->insert($dataSubject);

                                //update invoice no at tbl_studentregsubjects_detail
                                $dataUpdSubj = array('invoice_id' => $invoice_id);
                                $db->update('tbl_studentregsubjects_detail', $dataUpdSubj, $db->quoteInto("id = ?", $idRegSub));
                            }
                        }
                        if ($commit == 1) {
                            $db->commit();
                        }

                    } catch (Exception $e) {

                        if ($commit == 1) {
                            $db->rollBack();
                        }

                        //save error message
                        $sysErDb = new App_Model_General_DbTable_SystemError();
                        $msg['se_IdStudentRegistration'] = $studentID;
                        $msg['se_title'] = 'Error generate invoice';
                        $msg['se_message'] = $e->getMessage();
                        $msg['se_createdby'] = $creator;
                        $msg['se_createddt'] = date("Y-m-d H:i:s");
                        $sysErDb->addData($msg);

                        throw $e;
                    }
                }
            }
        }
    }

    public function checkWithdrawSubject($studentId, $subjectId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentRegistration = ?', $studentId)
            ->where('a.IdSubject = ?', $subjectId)
            ->where('a.Active = ?', 3);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkSubjectBefore($studentId, $subjectId, $semesterID){
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectSem = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdSemesterMaster = ?', $semesterID);

        $sem = $db->fetchRow($selectSem);

        $result = false;

        if ($sem) {
            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects'))
                ->join(array('b' => 'tbl_semestermaster'), 'a.IdSemesterMain = b.IdSemesterMaster')
                ->where('a.IdStudentRegistration = ?', $studentId)
                ->where('a.IdSubject = ?', $subjectId)
                ->where('b.SemesterMainStartDate < ?', $sem['SemesterMainStartDate'])
                ->order('b.SemesterMainStartDate DESC');

            $result = $db->fetchAll($select);

        }

        return $result;
    }

    public function getItemBefore($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'))
            ->where('a.regsub_id = ?', $id)
            ->where('a.item_id = ?', 890);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function generateCreditNote($studentID, $subjectID, $invoiceID, $itemID = 0, $percentageDefined = 0, $commit = 0, $initial = 0)
    {

        /*
		 * Parameter  in : Student ID,  Subject, Invoice No, Registration Item)
			Student ID, Subject, Invoice No -> Mandatory
			Registration Item -> Optional
		 */

        /*$studentID = 3968; //IdStudentRegistration -> tbl_studentregistration
		$subjectID = 106; //course id
		$invoiceID = 715; // invoice id
		$itemID = 890; // item id
		$percentage = 10/20/50/100
		*/

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $IdProgram = $studentInfo['IdProgram'];
        $IdProgramScheme = $studentInfo['IdProgramScheme'];

//		if($initial == 1){
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('*', 'cn_amount_main' => 'a.cn_amount'))
            ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = a.id', array('*', 'id_detail' => 'c.id'))
            ->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('subject_id', 'amountSubject' => 'amount'))
            ->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
            ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=c.fi_id', array(''))
            ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
            ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
            ->where('a.id = ?', $invoiceID)
            ->where('b.subject_id = ?', $subjectID)
            ->where('a.IdStudentRegistration = ?', $studentID);
//				->group('a.id');

        if ($initial == 0) {
            if ($itemID) {
                $select->where('d.fsi_registration_item_id = ?', $itemID)
                    ->group('b.subject_id');
            }
        }


        $txnData = $db->fetchAll($select);

        if ($txnData == null) {
            return null;
//			throw new Exception('No data to issue credit note');
        } else {

            $balanceNew = 0;

            $fee_item_to_charge = array();
            $totalAmount = 0;
            foreach ($txnData as $data) {

                $selectCN = $db->select()
                    ->from(array('a' => 'credit_note_detail'))
                    ->join(array('c' => 'credit_note'), 'c.cn_id = a.cn_id', array('*'))
                    ->where('a.invoice_main_id = ?', $invoiceID)
                    ->where('a.invoice_detail_id = ?', $data['id_detail'])
                    ->where("c.cn_status = 'A'");

                $txnDataSN = $db->fetchRow($selectCN);

                if ($txnDataSN) {
                    return null;
                } else {

                    $semesterID = $data['semester'];

                    if ($percentageDefined) {
                        $fee_item_to_charge[$data['invoice_main_id']]['act_amount'] = $percentageDefined;
                        $fee_item_to_charge[$data['invoice_main_id']]['act_type'] = 0;
                    } else {

                        //activity calendar
                        $selectActivity = $db->select()
                            ->from(array('a' => 'tbl_activity_calender'))
                            ->join(array('b' => 'tbl_registration_item'), 'b.semester_id = a.IdSemesterMain')
                            ->join(array('c' => 'tbl_registration_item_activity'), 'c.ria_ri_id = b.ri_id AND ria_activity_id = a.IdActivity', array('ria_amount', 'ria_calculation_type'))
                            ->where('a.EndDate >= CURDATE() AND a.StartDate <= CURDATE()')
                            ->where('a.IdSemesterMain =?', $semesterID)
                            ->where('b.program_id =?', $IdProgram)
                            ->where('b.program_scheme_id =?', $IdProgramScheme)
                            ->where('b.item_id =?', $itemID);

                        $activityData = $db->fetchRow($selectActivity);

                        //calculate based on activity if any
                        if ($activityData) {
                            $activityCalType = $activityData['ria_calculation_type'];
                            $activityAmount = $activityData['ria_amount'];

                            $percentage = $activityAmount;
                            /* if($activityCalType == 0){
					    	$percentage = 100 - $activityAmount;
					    }
					    */
                            $fee_item_to_charge[$data['invoice_main_id']]['act_amount'] = $percentage;
                            $fee_item_to_charge[$data['invoice_main_id']]['act_type'] = $activityCalType;

                        } else {
                            $fee_item_to_charge[$data['invoice_main_id']]['act_amount'] = 100;
                            $fee_item_to_charge[$data['invoice_main_id']]['act_type'] = 0;
                        }
                    }

                    $fee_item_to_charge[$data['invoice_main_id']]['fee_item'][] = $data;

                    if ($initial == 1) {
                        $totalAmount += $data['amountSubject'];
                    } else {
                        $totalAmount += $data['amount'];
                    }
                    $fee_item_to_charge[$data['invoice_main_id']]['amount'] = $totalAmount;
                }
            }

//		echo "<pre>";
//		print_r($fee_item_to_charge);
//		exit;

            if ($commit == 1) {
                $db->beginTransaction();
            }

            try {

                //insert into invoice_main
                $creditMainDb = new Studentfinance_Model_DbTable_CreditNote();

                $bill_no = $this->getBillSeq(4, date('Y'));

                $feeItem = $fee_item_to_charge[$invoiceID]['fee_item'][0];

                if ($feeItem['SubCode']) {
                    if ($feeItem['item_name']) {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['SubjectName'] . ', ' . $feeItem['item_name'];
                    } else {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['bill_description'];
                    }

                } else {
                    $cn_desc = $feeItem['bill_description'];
                }

                $amountCN = $fee_item_to_charge[$invoiceID]['amount'];
                if ($fee_item_to_charge[$invoiceID]['act_type'] == 0) {
                    if ($fee_item_to_charge[$invoiceID]['act_amount'] != 100) {
                        $percentage = 100 - $fee_item_to_charge[$invoiceID]['act_amount'];
                        $amountCN = ($amountCN * ($percentage / 100));
                    }
                } elseif ($fee_item_to_charge[$invoiceID]['act_type'] == 1) {
                    $amountCN = $fee_item_to_charge[$invoiceID]['act_amount'];
                }

                if ($amountCN > 0) {
                    $data_cn = array(
                        'cn_billing_no' => $bill_no,
                        'cn_appl_id' => $feeItem['appl_id'],
                        'cn_trans_id' => $feeItem['trans_id'],
                        'cn_IdStudentRegistration' => $feeItem['IdStudentRegistration'],
                        'cn_amount' => $amountCN,
                        'cn_description' => $cn_desc,
                        'cn_cur_id' => $feeItem['currency_id'],
                        'cn_invoice_id' => $invoiceID,
                        'cn_create_date' => date('Y-m-d H:i:s'),
                        'cn_creator' => $creator,//by system
                        'cn_source' => 0,
                        'cn_status' => 'A',//active/approve
                        'cn_approve_date' => date('Y-m-d H:i:s'),
                        'cn_approver' => 1,//by admin
                    );

//					echo "<pre>";
//					print_r($data_cn);

                    $cn_id = $creditMainDb->insert($data_cn);

                    //insert invoice detail
                    $creditDetailDb = new Studentfinance_Model_DbTable_CreditNoteDetail();

                    foreach ($fee_item_to_charge[$invoiceID]['fee_item'] as $data) {


                        if ($initial == 1) {
                            $amountCN = $data['amountSubject'];
                        } else {
                            $amountCN = $data['amount'];
                        }

                        if ($fee_item_to_charge[$invoiceID]['act_type'] == 0 || $fee_item_to_charge[$invoiceID]['act_type'] == '') {
                            if ($fee_item_to_charge[$invoiceID]['act_amount'] != 100) {
                                $percentage = 100 - $fee_item_to_charge[$invoiceID]['act_amount'];
                                $amountCN = $amountCN - ($amountCN * ($percentage / 100));
                            } else {
                                $amountCN = $amountCN;
                            }
                        } elseif ($fee_item_to_charge[$invoiceID]['act_type'] == 1) {
                            $amountCN = $amountCN - $fee_item_to_charge[$invoiceID]['act_amount'];
                        }

//						if($data['balance'] <= 0){
//							$balanceCN = ($amountCN);
//						}else{
                        $balanceCN = $data['balance'] - ($amountCN);
//						}

                        $cnDetailData = array(
                            'cn_id' => $cn_id,
                            'invoice_main_id' => $invoiceID,
                            'invoice_detail_id' => $data['id_detail'],
                            'amount' => $amountCN,
                            'cur_id' => $data['currency_id']
                        );


                        $cn_detail_id = $creditDetailDb->insert($cnDetailData);

                        //update amount cn at invoice_detail
                        $amountCNNew = $data['cn_amount'] + $amountCN;

                        $dataUpdDetail = array('cn_amount' => $amountCNNew, 'balance' => $balanceCN);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $data['id_detail']));


                        //update amount cn at invoice_main

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invoiceID);

                        $balanceNew = $invMain['bill_balance'] - ($amountCN);
                        $amountCNNew = $invMain['cn_amount_main'] + $amountCN;
                        $dataUpd = array('cn_amount' => $amountCNNew, 'bill_balance' => $balanceNew);
                        $db->update('invoice_main', $dataUpd, $db->quoteInto("id = ?", $invoiceID));

                    }


                }

                if ($commit == 1) {
                    $db->commit();
                }

            } catch (Exception $e) {

                if ($commit == 1) {
                    $db->rollBack();
                }

                //save error message
                $sysErDb = new App_Model_General_DbTable_SystemError();
                $msg['se_IdStudentRegistration'] = $studentID;
                $msg['se_title'] = 'Error generate credit note';
                $msg['se_message'] = $e->getMessage();
                $msg['se_createdby'] = $creator;
                $msg['se_createddt'] = date("Y-m-d H:i:s");
                $sysErDb->addData($msg);

                throw $e;
            }
            return true;
        }

    }

    public function calculateOtherInvoice($studentID, $type, $subjectID, $level = 0, $fsId = 0, $itemId = 0)
    {

        /*
		 * Script to calculate invoice amount for other invoice
		 * Parameter in: student ID, type = CT,EX etc, level = 0 application 1 = approval
		 * Parameter out : Amount
		 *  1.	Cara calculation : Cek  dekat Fee � Detail tab & setup finance activity, 
				a.	fee item
				b.	Amount
				c.	Currency
		 * 
		 */

        /*$studentID = 4003; //IdStudentRegistration -> tbl_studentregistration
		$type = 'AU'; //type kt tbl_Defi*/

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];

        if ($checkingInvoice == 1) {

            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->join(array('b' => 'student_profile'), 'b.id = a.sp_id', array('*'))
                ->where('a.IdStudentRegistration =?', $studentID);

            $txnData = $db->fetchRow($select);

            $programID = $txnData['IdProgram'];

            if ($fsId) {
                $fsId = $fsId;
            } else {
                $fsId = $txnData['fs_id'];
            }

            //get fee item
            $fee_item_to_charge = array();

            //fee structure detail
            $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureOther();
            $fsitem = $feeStructureItemDb->getStructureData($fsId, $type, $programID, $level, $itemId);

            if (!empty($fsitem)) {

                $selectSubj = $db->select()
                    ->from(array('a' => 'tbl_subjectmaster'))
                    ->where('a.IdSubject =?', $subjectID);

                $subjectData = $db->fetchRow($selectSubj);

                $ch_bil_subject = $subjectData['CreditHours'];
                $fsitem['subject'] = array($subjectData);
                $fsitem['creditHours'] = $ch_bil_subject;

                $total_subject = 1;
                $fsitem['totalSubject'] = $total_subject;
                //fix amount
                if ($fsitem['fi_amount_calculation_type'] == 300) {
                    $fsitem['amount'] = $fsitem['fsi_amount'];
                }

                //Subject Multiplication
                if ($fsitem['fi_amount_calculation_type'] == 301) {
                    if (!empty($fsitem['subject'])) {
                        foreach ($fsitem['subject'] as $key => $subject) {
                            $fsitem['subject'][$key]['amount'] = $fsitem['fsi_amount'];
                        }

                        $fsitem['fsi_amount'] = $fsitem['fsi_amount'] * $total_subject;


                    }
                }

                //Credit Hour Multiplication
                if ($fsitem['fi_amount_calculation_type'] == 299) {
                    if (!empty($fsitem['subject'])) {
                        foreach ($fsitem['subject'] as $key => $subject) {
                            $fsitem['subject'][$key]['amount'] = $fsitem['fsi_amount'];
                        }

                        $fsitem['fsi_amount'] = $fsitem['fsi_amount'] * $ch_bil_subject;

                    }
                }


                if ($fsitem['fsi_amount'] > 0) {
                    $fsitem['amount'] = $fsitem['fsi_amount'];

                }

                $fee_item_to_charge[] = $fsitem;

            }

//			echo "<pre>";
//			print_r($fee_item_to_charge);	
//			exit;
            return $fee_item_to_charge;
        } else {
            return null;
        }

    }

    public function generateOtherInvoiceStudent($studentID, $semesterID, $type, $subjectID = 0, $level = 0, $fsId = 0, $itemId = 0)
    {

        /*$studentID = 4003; //IdStudentRegistration -> tbl_studentregistration
		$type = 'AU'; //type kt tbl_Defi
		$semesterID = 6;*/

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'))
            ->join(array('b' => 'student_profile'), 'b.id = a.sp_id', array('*'))
            ->where('a.IdStudentRegistration =?', $studentID);

        $txnData = $db->fetchRow($select);

        if ($semesterID == null) {
            throw new Exception('No semester');
        }

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];

        if ($checkingInvoice == 1) {

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->join(array('b' => 'student_profile'), 'b.id = a.sp_id', array('*'))
                ->where('a.IdStudentRegistration =?', $studentID);

            $txnData = $db->fetchRow($select);

            $programID = $txnData['IdProgram'];

            //get definition
            $selectDef = $db->select()
                ->from(array('a' => 'tbl_definationms'))
                ->where('a.DefinitionCode =?', $type)
                ->where('a.idDefType =168');//finance activity

            $DefData = $db->fetchRow($selectDef);

            //student profile info
            $studentregDB = new CourseRegistration_Model_DbTable_Studentregistration();
            $studentdata = $studentregDB->getData($studentID);

            $calcamount = $this->calculateOtherInvoice($studentID, $type, $subjectID, $level, $fsId, $itemId);

            $invoice_data = array();
            if ($calcamount) {

                $calcamount = $calcamount[0];
                $invoice_data['fee_item'] = $calcamount;

                //get current currency rate 
                $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                $currencyRate = $currencyRateDB->getCurrentExchangeRate($calcamount['cur_id']);
                $invoice_data['fee_item']['exchange_rate'] = $currencyRate['cr_id'];
                $invoice_data['fee_item']['cur_id'] = $calcamount['cur_id'];

                $amountInv = $calcamount['fsi_amount'];


                if ($subjectID) {
                    //subject info
                    $selectSubj = $db->select()
                        ->from(array('a' => 'tbl_subjectmaster'))
                        ->where('a.IdSubject =?', $subjectID);

                    $subjectData = $db->fetchRow($selectSubj);

                    $invoice_data['IdSubject'] = $subjectData['IdSubject'];
                    $invoice_data['SubCode'] = $subjectData['SubCode'];
                    $invoice_data['SubjectName'] = $subjectData['SubjectName'];
                }


                $invoice_data['amount'] = $amountInv;

            }


            /*echo "<pre>";
				print_r($invoice_data);
				exit;*/

            if ($invoice_data) {

                $db->beginTransaction();
                try {

                    $fee_item_data = $invoice_data['fee_item'];

                    $proformaInvoiceMainDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $proformaInvoiceDetailDb = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();

                    $invDesc = $DefData['DefinitionDesc'] . ' - ' . $invoice_data['fee_item']['fc_desc'];

                    if ($subjectID) {
                        $invDesc = $invoice_data['SubCode'] . ' - ' . $DefData['DefinitionDesc'] . ' - ' . $invoice_data['fee_item']['item_name'];
                    }


                    if ($fee_item_data['fso_proforma'] == 1) {
                        //generate proforma 

                        $bill_no = $this->getBillSeq(1, date('Y'));


                        //add main
                        $data = array(
                            'bill_number' => $bill_no,
                            'appl_id' => $studentdata['appl_id'],
                            'trans_id' => $studentdata['transaction_id'],
                            'IdStudentRegistration' => $studentID,
                            'semester' => $semesterID,
                            'bill_amount' => $invoice_data['amount'],
                            'bill_description' => $invDesc,
                            'program_id' => $studentdata['IdProgram'],
                            'creator' => $creator,
                            'fs_id' => $studentdata['fs_id'],
                            'status' => 'A',
                            'currency_id' => $invoice_data['fee_item']['cur_id'],
                            //								'bill_amount_default_currency' => $total_invoice_amount_default_currency,
                            'invoice_type' => 'PROCESSING',
                            'fee_category' => $invoice_data['fee_item']['fc_id'],
                            'branch_id' => $studentdata['IdBranch'],
                            'invoice_type' => $type,

                        );
                        $main_id = $proformaInvoiceMainDb->insert($data);

                        $data_detail = array(
                            'proforma_invoice_main_id' => $main_id,
                            'fi_id' => $fee_item_data['fi_id'],
                            'fee_item_description' => $fee_item_data['fi_name'],
                            'cur_id' => $invoice_data['fee_item']['cur_id'],
                            'amount' => $fee_item_data['fsi_amount'],
                            //												'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($fee['amount'],$fee['fspi_currency_id'])
                        );
                        $invoice_id = $main_id;
                        $invoice_detail_id = $proformaInvoiceDetailDb->insert($data_detail);

                        $invoiceSubjectDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();

                        $dataSubject = array(
                            'proforma_invoice_main_id' => $invoice_id,
                            'proforma_invoice_detail_id' => $invoice_detail_id,
                            'subject_id' => $invoice_data['IdSubject'],
                            'amount' => $fee_item_data['fsi_amount']
                        );

                        $invoiceSubjectDb->insert($dataSubject);

                    } else {
                        //generate invoice

                        //insert into invoice_main
                        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                        $bill_no = $this->getBillSeq(2, date('Y'));
                        $data_invoice = array(
                            'bill_number' => $bill_no,
                            'appl_id' => $studentdata['appl_id'],
                            'trans_id' => $studentdata['transaction_id'],
                            'IdStudentRegistration' => $studentID,
                            'academic_year' => '',
                            'semester' => $semesterID,
                            'bill_amount' => $invoice_data['amount'],
                            //'bill_amount_default_currency' => $proforma_invoice['bill_amount_default_currency'],
                            'bill_paid' => 0,
                            'bill_balance' => $invoice_data['amount'],
                            'bill_description' => $invDesc,
                            'program_id' => $studentdata['IdProgram'],
                            'fs_id' => $studentdata['fs_id'],
                            'currency_id' => $invoice_data['fee_item']['cur_id'],
                            'exchange_rate' => $invoice_data['fee_item']['exchange_rate'],
                            'invoice_date' => date('Y-m-d'),
                            'date_create' => date('Y-m-d H:i:s'),
                            'invoice_type' => $type,
                            'creator' => $creator//by admin
                        );

                        $invoice_id = $invoiceMainDb->insert($data_invoice);

                        //insert invoice detail
                        $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                        $invoiceDetailData = array(
                            'invoice_main_id' => $invoice_id,
                            'fi_id' => $fee_item_data['fi_id'],
                            'fee_item_description' => $fee_item_data['fi_name'],
                            'cur_id' => $fee_item_data['cur_id'],
                            'amount' => $fee_item_data['fsi_amount'],
                            'balance' => $fee_item_data['fsi_amount'],
                            'exchange_rate' => $fee_item_data['exchange_rate']
                        );

                        $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                        $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();

                        $dataSubject = array(
                            'invoice_main_id' => $invoice_id,
                            'invoice_detail_id' => $invoice_detail_id,
                            'subject_id' => $invoice_data['IdSubject'],
                            'amount' => $fee_item_data['fsi_amount'],
                            'balance' => $fee_item_data['fsi_amount'],
                        );

                        $invoiceSubjectDb->insert($dataSubject);

                    }

                    $db->commit();

                    return $invoice_id;

                } catch (Exception $e) {

                    $db->rollBack();

                    //save error message
                    $sysErDb = new App_Model_General_DbTable_SystemError();
                    $msg['se_IdStudentRegistration'] = $studentID;
                    $msg['se_title'] = 'Error generate other invoice';
                    $msg['se_message'] = $e->getMessage();
                    $msg['se_createdby'] = $creator;
                    $msg['se_createddt'] = date("Y-m-d H:i:s");
                    $sysErDb->addData($msg);

                    throw $e;
                }
            }
        }
    }

    public function generateCreditNoteByInvoice($studentID, $invoiceID, $commit = 1, $type = 0)
    {

        /*
		 * Parameter  in : Student ID,  Subject, Invoice No, Registration Item)
			Student ID, Subject, Invoice No -> Mandatory
			Registration Item -> Optional
			type -> 0=student 1=applicant
		 */

        /*$studentID = 3968; //IdStudentRegistration -> tbl_studentregistration
		$subjectID = 106; //course id
		$invoiceID = 715; // invoice id
		$itemID = 890; // item id*/

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('*', 'cn_amount_main' => 'a.cn_amount'))
            ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = a.id', array('*', 'id_detail' => 'c.id'))
            ->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('subject_id'))
            ->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
            ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=c.fi_id', array(''))
            ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
            ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
            ->where('a.id = ?', $invoiceID);

        if ($type == 1) {
            $select->where('a.trans_id = ?', $studentID);
        } else {
            $select->where('a.IdStudentRegistration = ?', $studentID);
        }

        $txnData = $db->fetchAll($select);

        if ($txnData == null) {
            return null;
//			throw new Exception('No data to issue credit note');
        } else {

            $balanceNew = 0;

            $fee_item_to_charge = array();
            $totalAmount = 0;
            foreach ($txnData as $data) {
                $fee_item_to_charge[$data['invoice_main_id']]['fee_item'][] = $data;
                $totalAmount += $data['amount'];
                $fee_item_to_charge[$data['invoice_main_id']]['amount'] = $totalAmount;
            }

            /*echo "<pre>";
		print_r($fee_item_to_charge);
		exit;*/

            if ($commit == 1) {
                $db->beginTransaction();
            }

            try {

                //insert into invoice_main
                $creditMainDb = new Studentfinance_Model_DbTable_CreditNote();

                $bill_no = $this->getBillSeq(4, date('Y'));

                $feeItem = $fee_item_to_charge[$invoiceID]['fee_item'][0];

                if ($feeItem['SubCode']) {
                    if ($feeItem['item_name']) {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['SubjectName'] . ', ' . $feeItem['item_name'];
                    } else {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['bill_description'];
                    }

                } else {
                    $cn_desc = $feeItem['bill_description'];
                }

                $amountCN = $fee_item_to_charge[$invoiceID]['amount'];
                if ($amountCN > 0) {
                    $data_cn = array(
                        'cn_billing_no' => $bill_no,
                        'cn_appl_id' => $feeItem['appl_id'],
                        'cn_trans_id' => $feeItem['trans_id'],
                        'cn_IdStudentRegistration' => $feeItem['IdStudentRegistration'],
                        'cn_amount' => $amountCN,
                        'cn_description' => $cn_desc,
                        'cn_cur_id' => $feeItem['currency_id'],
                        'cn_invoice_id' => $invoiceID,
                        'cn_create_date' => date('Y-m-d H:i:s'),
                        'cn_creator' => $creator,//by system
                        'cn_source' => 0,
                        'cn_status' => 'A',//active/approve
                        'cn_approve_date' => date('Y-m-d H:i:s'),
                        'cn_approver' => 1,//by admin
                    );

                    $cn_id = $creditMainDb->insert($data_cn);

                    //insert invoice detail
                    $creditDetailDb = new Studentfinance_Model_DbTable_CreditNoteDetail();

                    foreach ($fee_item_to_charge[$invoiceID]['fee_item'] as $data) {

                        $cnDetailData = array(
                            'cn_id' => $cn_id,
                            'invoice_main_id' => $invoiceID,
                            'invoice_detail_id' => $data['id_detail'],
                            'amount' => $data['amount'],
                            'cur_id' => $data['currency_id']
                        );

                        $cn_detail_id = $creditDetailDb->insert($cnDetailData);

                        //update amount cn at invoice_detail

                        $balanceNew1 = $data['balance'] - $data['amount'];
                        $amountCNNew1 = $data['amount'];
                        $dataUpdDetail = array('cn_amount' => $amountCNNew1, 'balance' => $balanceNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $data['id_detail']));

                        //update amount cn at invoice_main
                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invoiceID);

                        $balanceNew = $invMain['bill_balance'] - $data['amount'];
                        $amountCNNew = $invMain['cn_amount'] + $data['amount'];
                        $dataUpd = array('cn_amount' => $amountCNNew, 'bill_balance' => $balanceNew);
                        $db->update('invoice_main', $dataUpd, $db->quoteInto("id = ?", $invoiceID));

                    }


                }

                if ($commit == 1) {
                    $db->commit();
                }

            } catch (Exception $e) {

                if ($commit == 1) {
                    $db->rollBack();
                }

                //save error message
                $sysErDb = new App_Model_General_DbTable_SystemError();
                $msg['se_IdStudentRegistration'] = $studentID;
                $msg['se_title'] = 'Error generate credit note';
                $msg['se_message'] = $e->getMessage();
                $msg['se_createdby'] = $creator;
                $msg['se_createddt'] = date("Y-m-d H:i:s");
                $sysErDb->addData($msg);

                throw $e;
            }
            return true;
        }

    }

    public function getStudentInvoiceInfo($studentID, $feeID)
    {

        /*
		 * get invoice per fee item
		 * 
		 */

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $idCountry = $studentInfo['appl_nationality'];

        $checkingInvoice = $studentInfo['invoice'];

        if ($checkingInvoice == 1) {

            //get fee item
            $fee_item_to_charge = array();

            //fee structure detail
            $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
            $fsProgramItem = $feeStructureItemDb->getStructureDataFee($studentInfo['fs_id'], $feeID);

            if (!empty($fsProgramItem)) {
                foreach ($fsProgramItem as $keyfs => $fsitem) {
                    $fee_item = array();

                    //fix amount
                    if ($fsitem['fi_amount_calculation_type'] == 300) {
                        $fsitem['amount'] = $fsitem['fsi_amount'];
                    }

                    //amount
//            if ($fsitem['fspi_amount'] > 0) {
//                $fsitem['amount'] = $fsitem['fspi_amount'];
//
//            } else {

                    //check country origin
                    if ($fsitem['fi_amount_calculation_type'] == 618) {
                        //get fee_item_category
                        $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                        $feeCountryData = $feeCountryDB->getItemData($fsitem['fi_id'], $idCountry, $fsitem['cur_id']);

                        $fsitem['amount'] = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
                    }else{
                        $fsitem['amount'] = $fsitem['fsi_amount'];
                    }
//            }


                    $fee_item_to_charge[] = $fsitem;
                }
            }


//			echo "<pre>";
//			print_r($fee_item_to_charge_new);

            return $fee_item_to_charge;
        } else {
            return null;
        }

    }

    /*
     * used for visa
     */
    public function generateInvoice($studentID, $feeID, $semesterID, $amount = 0, $curID = 0, $subjectID = 0, $commit = 0)
    {

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];

        if ($checkingInvoice == 1) {
//			$amountAdd = 0;
//			$curID= 0;
            if ($amount) {
                $amountAdd = $amount;
                $curID = $curID;
            } else {
                //get fee item
                $fee_item_list = $this->getStudentInvoiceInfo($studentID, $feeID);
                //var_dump($fee_item_list); exit;
                $fee_item_list2 = $fee_item_list;

                if ($fee_item_list) {
                    $fee_item_list = $fee_item_list[0];
                    $curID = $fee_item_list['fsi_cur_id'];
                    $amountAdd = $fee_item_list['amount'];
                }else{
                    $fee_item_list = array();
                    $curID = null;
                    $amountAdd = null;
                }
            }

            $invoiceStatus = false;
            if ($curID != null && $amountAdd != null){

                $invoiceStatus = true;
                $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                $currency = $currencyDb->getCurrentExchangeRate($curID);
                //var_dump($currency); exit;
                //get fee info

                $selectSubject = $db->select()
                    ->from(array('a' => 'tbl_subjectmaster'))
                    ->where('a.IdSubject = ?', $subjectID);
                $resultsSubject = $db->fetchRow($selectSubject);

                $feeitemDb = new Studentfinance_Model_DbTable_FeeItem();
                $feeInfo = $feeitemDb->getData($feeID);

                if ($subjectID) {
                    $feeDesc = "Tuition Fee for " . $resultsSubject['SubCode'];
                } else {
                    $feeDesc = $feeInfo['fi_name'];
                }

                if ($commit == 1) {
                    $db->beginTransaction();
                }
                try {

                    //insert into invoice_main
                    $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                    $bill_no = $this->getBillSeq(2, date('Y'));
                    $data_invoice = array(
                        'bill_number' => $bill_no,
                        'appl_id' => isset($studentInfo['appl_id']) ? $studentInfo['appl_id'] : 0,
                        'trans_id' => isset($studentInfo['transaction_id']) ? $studentInfo['transaction_id'] : 0,
                        'IdStudentRegistration' => $studentID,
                        'academic_year' => '',
                        'semester' => $semesterID,
                        'bill_amount' => $amountAdd,
                        'bill_paid' => 0,
                        'bill_balance' => $amountAdd,
                        'bill_description' => $feeDesc,
                        'program_id' => $studentInfo['IdProgram'],
                        'fs_id' => $studentInfo['fs_id'],
                        'currency_id' => $curID,
                        'exchange_rate' => $currency['cr_exchange_rate'],
                        'invoice_date' => date('Y-m-d'),
                        'date_create' => date('Y-m-d H:i:s'),
                        'creator' => $creator//by admin
                    );


                    $invoice_id = $invoiceMainDb->insert($data_invoice);

                    //insert invoice detail
                    $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invoice_id,
                        'fi_id' => $feeID,
                        'fee_item_description' => $feeInfo['fi_name'],
                        'cur_id' => $curID,
                        'amount' => $amountAdd,
                        'balance' => $amountAdd,
                        'exchange_rate' => $currency['cr_exchange_rate']
                    );

                    $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                    if ($subjectID) {
                        $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();

                        $dataSubject = array(
                            'invoice_main_id' => $invoice_id,
                            'invoice_detail_id' => $invoice_detail_id,
                            'subject_id' => $subjectID,
                            'amount' => $amountAdd,
                            'balance' => $amountAdd,
                        );

                        $invoiceSubjectDb->insert($dataSubject);
                    }
                    if ($commit == 1) {
                        $db->commit();
                    }

                } catch (Exception $e) {

                    if ($commit == 1) {
                        $db->rollBack();
                    }

                    //save error message
                    $sysErDb = new App_Model_General_DbTable_SystemError();
                    $msg['se_IdStudentRegistration'] = $studentID;
                    $msg['se_title'] = 'Error generate invoice';
                    $msg['se_message'] = $e->getMessage();
                    $msg['se_createdby'] = $creator;
                    $msg['se_createddt'] = date("Y-m-d H:i:s");
                    $sysErDb->addData($msg);

                    throw $e;
                }
            }
        }

        return $invoiceStatus;
    }

    public function checkOutstandingBalanceBySubject($studentID, $subjectID)
    {

        /*
         1. checking outstanding proforma invoice where A n invoice_id = null
         2. checking advance payment
         *  invoice & proforma & advance payment amount in MYR
         */

        $status = null;
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects_detail'))
            ->join(array('c' => 'invoice_main'), 'c.id = a.invoice_id', array('bill_balance', 'currency_id', 'exchange_rate'))
            ->where('a.student_id = ?', $studentID)
            ->where('a.subject_id = ?', $subjectID);

        $result = $db->fetchAll($select);

        if ($result) {
            $amount = 0;
            foreach ($result as $data) {
                $amt = $data['bill_balance'];
                if ($data['currency_id'] == 2) {
                    $amt = round($amt * $data['exchange_rate'], 2);
                }

                $amount += $amt;
            }

            if ($amount <= 0) {
                $status = 1; // no outstanding
            } else {
                $status = 2; // outstanding
            }

        } else {
            $status = 3; // no record/no invoice
        }
        return $status;

    }

    public function generateCreditNoteEntryDetail($studentID, $invoiceID, $percentageDefined = 0, $amount = 0, $commit = 0, $dateCreated = 0, $SubID = null)
    {

        /* Generate function for generate credit note
		 * Parameter  in : Student ID,  Subject, $percentageDefined, amount)
		 * $invoiceID  - id invoice_detail
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('*', 'cn_amount_main' => 'a.cn_amount', 'idMain' => 'a.id'))
            ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = a.id', array('*', 'id_detail' => 'c.id'))
            //->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('subject_id'))
            //->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
            ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=c.fi_id', array(''))
            ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
            ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
            ->where('c.id = ?', $invoiceID);
//		->where('a.IdStudentRegistration = ?', $studentID);
//		->orWhere('a.trans_id = ?', $studentID);
//		exit;

        if ($SubID != null && $SubID != 'null'){
            $select->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('b.subject_id', 'b.amount as amount_sub'));
            $select->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id');
            $select->where('b.subject_id = ?', $SubID);
        }

        $txnData = $db->fetchAll($select);

        $invoiceID = $txnData[0]['idMain'];

        if ($txnData == null) {
            return null;
//			throw new Exception('No data to issue credit note');
        } else {

            $balanceNew = 0;
            $fee_item_to_charge = array();
            $totalAmount = 0;
            foreach ($txnData as $key => $data) {
                if ($SubID != null && $data['amount_sub']!=null){
                    $data['amount']=$data['amount_sub'];
                    $txnData[$key]['amount']=$data['amount_sub'];
                }
                $fee_item_to_charge[$data['invoice_main_id']]['fee_item'][$key] = $data;
                $totalAmount += $data['amount'];

            }

            $amountCN = $totalAmount;
            $typeAmount = 0;
            if ($percentageDefined) {
                $amountCN = ($totalAmount * ($percentageDefined / 100));
                $typeAmount = $percentageDefined;
            }
            if ($amount) {
                $amountCN = $amount;
            }

            $fee_item_to_charge[$data['invoice_main_id']]['amount'] = $amountCN;

            /*echo "<pre>";
		print_r($fee_item_to_charge);
//		exit;*/

            if ($commit == 1) {
                $db->beginTransaction();
            }

            try {

                //insert into invoice_main
                $creditMainDb = new Studentfinance_Model_DbTable_CreditNote();

                $bill_no = $this->getBillSeq(4, date('Y'));

                $feeItem = $fee_item_to_charge[$invoiceID]['fee_item'][0];

                if ($feeItem['SubCode']) {
                    if ($feeItem['item_name']) {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['SubjectName'] . ', ' . $feeItem['item_name'];
                    } else {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['bill_description'];
                    }

                } else {
                    $cn_desc = $feeItem['bill_description'];
                }

                $amountCN = $fee_item_to_charge[$invoiceID]['amount'];

                if ($dateCreated) {
                    $dateCreated = date('Y-m-d', strtotime($dateCreated));
                } else {
                    $dateCreated = date('Y-m-d H:i:s');
                }

                if ($amountCN > 0) {
                    $data_cn = array(
                        'cn_billing_no' => $bill_no,
                        'cn_appl_id' => $feeItem['appl_id'],
                        'cn_trans_id' => $feeItem['trans_id'],
                        'cn_IdStudentRegistration' => $feeItem['IdStudentRegistration'],
                        'cn_amount' => $amountCN,
                        'cn_description' => $cn_desc,
                        'cn_cur_id' => $feeItem['currency_id'],
                        'cn_invoice_id' => $invoiceID,
                        'cn_create_date' => $dateCreated,
                        'type_amount' => $typeAmount,
                        'cn_creator' => $creator,//by system
                        'cn_source' => 1,//by student portal
                        'cn_status' => 'A',//active/approve
                        'cn_approve_date' => date('Y-m-d H:i:s'),
                        'cn_approver' => 1,//by admin
                    );

                    $cn_id = $creditMainDb->insert($data_cn);

                    //insert invoice detail
                    $creditDetailDb = new Studentfinance_Model_DbTable_CreditNoteDetail();

                    foreach ($fee_item_to_charge[$invoiceID]['fee_item'] as $data) {

                        $cnDetailData = array(
                            'cn_id' => $cn_id,
                            'invoice_main_id' => $invoiceID,
                            'invoice_detail_id' => $data['id_detail'],
                            'amount' => $amountCN,
                            'cur_id' => $data['currency_id']
                        );

                        $cn_detail_id = $creditDetailDb->insert($cnDetailData);

                        //update amount cn at invoice_detail
                        $balanceNew1 = $data['balance'] - $amountCN;
                        $amountCNNew1 = $amountCN;
//						$balanceNew1 = $data['balance'] - $amountCN;
//						$amountCNNew1 = $data['cn_amount'] + $amountCN;
                        $dataUpdDetail = array('cn_amount' => $amountCNNew1, 'balance' => $balanceNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $data['id_detail']));

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invoiceID);

                        //update amount cn at invoice_main
                        $balanceNew = $invMain['bill_balance'] - $amountCNNew1;
                        $amountCNNew = $invMain['cn_amount'] + $amountCNNew1;
                        $dataUpd = array('cn_amount' => $amountCNNew, 'bill_balance' => $balanceNew);
                        $db->update('invoice_main', $dataUpd, $db->quoteInto("id = ?", $invoiceID));
                    }
                }

                if ($commit == 1) {
                    $db->commit();
                }

                return $cn_id;

            } catch (Exception $e) {

                if ($commit == 1) {
                    $db->rollBack();
                }

                //save error message
                $sysErDb = new App_Model_General_DbTable_SystemError();
                $msg['se_IdStudentRegistration'] = $studentID;
                $msg['se_title'] = 'Error generate credit note entry';
                $msg['se_message'] = $e->getMessage();
                $msg['se_createdby'] = $creator;
                $msg['se_createddt'] = date("Y-m-d H:i:s");
                $sysErDb->addData($msg);

                throw $e;
                return null;
            }

        }

    }

    public function generateCreditNoteEntry($studentID, $invoiceID, $percentageDefined = 0, $amount = 0, $commit = 0)
    {

        /* Generate function for generate credit note
		 * Parameter  in : Student ID,  Subject, $percentageDefined, amount)
		 * $invoiceID  - id invoice main
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('*', 'cn_amount_main' => 'a.cn_amount', 'idMain' => 'a.id'))
            ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = a.id', array('*', 'id_detail' => 'c.id'))
            ->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('subject_id'))
            ->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
            ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=c.fi_id', array(''))
            ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
            ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
            ->where('a.id = ?', $invoiceID)
            ->where('a.IdStudentRegistration = ?', $studentID)
            ->orWhere('a.trans_id = ?', $studentID);
//		exit;
        $txnData = $db->fetchAll($select);

        $invoiceID = $txnData[0]['idMain'];

        if ($txnData == null) {
            return null;
//			throw new Exception('No data to issue credit note');
        } else {

            $balanceNew = 0;
            $fee_item_to_charge = array();
            $totalAmount = 0;
            foreach ($txnData as $data) {
                $fee_item_to_charge[$data['invoice_main_id']]['fee_item'][] = $data;
                $totalAmount += $data['amount'];

            }

            $amountCN = $totalAmount;

            if ($percentageDefined) {
                $amountCN = ($totalAmount * ($percentageDefined / 100));
            }
            if ($amount) {
                $amountCN = $amount;
            }

            $fee_item_to_charge[$data['invoice_main_id']]['amount'] = $amountCN;

            /*echo "<pre>";
		print_r($fee_item_to_charge);
		exit;*/

            if ($commit == 1) {
                $db->beginTransaction();
            }

            try {

                //insert into invoice_main
                $creditMainDb = new Studentfinance_Model_DbTable_CreditNote();

                $bill_no = $this->getBillSeq(4, date('Y'));

                $feeItem = $fee_item_to_charge[$invoiceID]['fee_item'][0];

                if ($feeItem['SubCode']) {
                    if ($feeItem['item_name']) {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['SubjectName'] . ', ' . $feeItem['item_name'];
                    } else {
                        $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['bill_description'];
                    }

                } else {
                    $cn_desc = $feeItem['bill_description'];
                }

                $amountCN = $fee_item_to_charge[$invoiceID]['amount'];
                if ($amountCN > 0) {
                    $data_cn = array(
                        'cn_billing_no' => $bill_no,
                        'cn_appl_id' => $feeItem['appl_id'],
                        'cn_trans_id' => $feeItem['trans_id'],
                        'cn_IdStudentRegistration' => $feeItem['IdStudentRegistration'],
                        'cn_amount' => $amountCN,
                        'cn_description' => $cn_desc,
                        'cn_cur_id' => $feeItem['currency_id'],
                        'cn_invoice_id' => $invoiceID,
                        'cn_create_date' => date('Y-m-d H:i:s'),
                        'cn_creator' => $creator,//by system
                        'cn_source' => 1,//by student portal
                        'cn_status' => 'A',//active/approve
                        'cn_approve_date' => date('Y-m-d H:i:s'),
                        'cn_approver' => 1,//by admin
                    );

                    $cn_id = $creditMainDb->insert($data_cn);

                    //insert invoice detail
                    $creditDetailDb = new Studentfinance_Model_DbTable_CreditNoteDetail();

                    foreach ($fee_item_to_charge[$invoiceID]['fee_item'] as $data) {

                        $cnDetailData = array(
                            'cn_id' => $cn_id,
                            'invoice_main_id' => $invoiceID,
                            'invoice_detail_id' => $data['id_detail'],
                            'amount' => $data['amount'],
                            'cur_id' => $data['currency_id']
                        );

                        $cn_detail_id = $creditDetailDb->insert($cnDetailData);

                        //update amount cn at invoice_detail
                        $balanceNew1 = $data['balance'] - $data['amount'];
                        $amountCNNew1 = $data['amount'];
//						$balanceNew1 = $data['balance'] - $amountCN;
//						$amountCNNew1 = $data['cn_amount'] + $amountCN;
                        $dataUpdDetail = array('cn_amount' => $amountCNNew1, 'balance' => $balanceNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $data['id_detail']));

                    }

                    $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                    $invMain = $invoiceMainDB->getData($invoiceID);

                    //update amount cn at invoice_main
                    $balanceNew = $invMain['bill_balance'] - $amountCN;
                    $amountCNNew = $invMain['cn_amount'] + $amountCN;
                    $dataUpd = array('cn_amount' => $amountCNNew, 'bill_balance' => $balanceNew);
                    $db->update('invoice_main', $dataUpd, $db->quoteInto("id = ?", $invoiceID));
                }

                if ($commit == 1) {
                    $db->commit();
                }

                return $cn_id;

            } catch (Exception $e) {

                if ($commit == 1) {
                    $db->rollBack();
                }

                //save error message
                $sysErDb = new App_Model_General_DbTable_SystemError();
                $msg['se_IdStudentRegistration'] = $studentID;
                $msg['se_title'] = 'Error generate credit note entry';
                $msg['se_message'] = $e->getMessage();
                $msg['se_createdby'] = $creator;
                $msg['se_createddt'] = date("Y-m-d H:i:s");
                $sysErDb->addData($msg);

                throw $e;
                return null;
            }

        }

    }

    public function generateDiscountNote($studentID, $invoiceID, $amount, $description = null, $discountType = 0, $btch_id = 0, $commit = 0)
    {

        /* Generate function for generate discount note
		 * Parameter  in : Student ID,  Subject, $percentageDefined, amount)
		 * $invoiceID  - id invoice_detail array
		 * $amount - array
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $fee_item_to_charge = array();

        $balanceNew = 0;
        $totalAmount = 0;

        foreach ($invoiceID as $key => $invID) {

            $amountDN = !isset($amount[$key]) ? 0 : $amount[$key];
            $select = $db->select()
                ->from(array('a' => 'invoice_main'), array('*', 'cn_amount_main' => 'a.cn_amount', 'idMain' => 'a.id'))
                ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = a.id', array('*', 'id_detail' => 'c.id'))
                ->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('subject_id'))
                ->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
                ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
                ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=c.fi_id', array(''))
                ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
                ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
                ->where('c.id = ?', $invID);

            $txnData = $db->fetchRow($select);


            $fee_item_to_charge['fee_item'][$key] = $txnData;
            $fee_item_to_charge['fee_item'][$key]['discount'] = $amountDN;
            $totalAmount += $amountDN;
            $fee_item_to_charge['totalDiscount'] = $totalAmount;
        }

        if ($commit == 1) {
            $db->beginTransaction();
        }

        try {

            $discountMainDb = new Studentfinance_Model_DbTable_Discount();

            $bill_no = $this->getBillSeq(9, date('Y'));

            $feeItem = $fee_item_to_charge['fee_item'][0];

            if ($feeItem['SubCode']) {
                if ($feeItem['item_name']) {
                    $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['SubjectName'] . ', ' . $feeItem['fee_item_description'];
                } else {
                    $cn_desc = $feeItem['SubCode'] . ' - ' . $feeItem['fee_item_description'];
                }

            } else {
                $cn_desc = $feeItem['fee_item_description'];
            }

            $amountTotalDN = $fee_item_to_charge['totalDiscount'];
            if ($amountTotalDN > 0) {
                $data_dn = array(
                    'dcnt_fomulir_id' => $bill_no,
                    'dcnt_appl_id' => $feeItem['appl_id'],
                    'dcnt_txn_id' => $feeItem['trans_id'],
                    'dcnt_IdStudentRegistration' => $studentID,
                    'dcnt_batch_id' => $btch_id,
                    'dcnt_amount' => $amountTotalDN,
                    'dcnt_type_id' => $discountType,
                    'dcnt_description' => $description,
                    'dcnt_currency_id' => $feeItem['currency_id'],
                    'dcnt_creator' => $creator,//by system
                    'dcnt_create_date' => date('Y-m-d H:i:s'),
                    'dcnt_status' => 'E',//entry
                );

                $dn_id = $discountMainDb->insert($data_dn);

                $discountDetailDb = new Studentfinance_Model_DbTable_DiscountNoteDetail();

                foreach ($fee_item_to_charge['fee_item'] as $data) {

                    $cnDetailData = array(
                        'dcnt_id' => $dn_id,
                        'dcnt_invoice_id' => $data['idMain'],
                        'dcnt_invoice_det_id' => $data['id'],
                        'dcnt_amount' => $data['discount'],
                        'dcnt_description' => $cn_desc
                    );
                    $dn_detail_id = $discountDetailDb->insert($cnDetailData);

                }

            }

            if ($commit == 1) {
                $db->commit();
            }

            return $dn_id;

        } catch (Exception $e) {

            if ($commit == 1) {
                $db->rollBack();
            }

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error generate Discount by Batch';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
            return null;
        }

    }

    public function discountNoteApproval($discId)
    {

        /* Generate function for approve discount note
		 * Parameter  in : discount ID, 
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'discount_detail'), array('dcnt_invoice_id', 'dcnt_invoice_det_id', 'dcnt_amount_detail' => 'a.dcnt_amount'))
            ->join(array('d' => 'discount'), 'd.dcnt_id = a.dcnt_id', array('dcnt_fomulir_id', 'dcnt_IdStudentRegistration', 'dcnt_txn_id', 'dcnt_type_id', 'dcnt_amount', 'dcnt_batch_id', 'dcnt_currency_id', 'dcnt_id'))
            ->join(array('b' => 'invoice_main'), 'b.id = a.dcnt_invoice_id', array('bill_balance', 'dn_amount_main' => 'b.dn_amount'))
            ->join(array('c' => 'invoice_detail'), 'c.id = a.dcnt_invoice_det_id and c.invoice_main_id = b.id', array('balance', 'dn_amount'))
            ->where('a.dcnt_id = ?', $discId);

        $txnData = $db->fetchAll($select);

        $studentID = $txnData[0]['dcnt_IdStudentRegistration'];
        try {

            $discountMainDb = new Studentfinance_Model_DbTable_Discount();
            $totalDN = 0;
            foreach ($txnData as $feeItem) {

                $amountDN = $feeItem['dcnt_amount_detail'];
                $invId = $feeItem['dcnt_invoice_id'];
                $invDetailId = $feeItem['dcnt_invoice_det_id'];

                $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                $invDet = $invoiceDetailDB->getData($invDetailId);
                $balanceNew1 = $invDet['balance'] - $amountDN;
                $amountDNNew1 = $invDet['dn_amount'] + $amountDN;
                $dataUpdDetail = array('dn_amount' => $amountDNNew1, 'balance' => $balanceNew1);
                $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetailId));

                //update amount cn at invoice_main

                $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                $invMain = $invoiceMainDB->getData($invId);
                $balanceNew = $invMain['bill_balance'] - $amountDN;
                $amountDNNew = $invMain['dn_amount'] + $amountDN;
                $dataUpdMain = array('dn_amount' => $amountDNNew, 'bill_balance' => $balanceNew);
                $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invId));

            }

            //update disocunt
            $dataUpd = array('dcnt_status' => 'A', 'dcnt_approve_by' => $creator, 'dcnt_approve_date' => date('Y-m-d H:i:s'));
            $db->update('discount', $dataUpd, $db->quoteInto("dcnt_id = ?", $discId));
        } catch (Exception $e) {

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error Approve Discount';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
            return null;
        }

    }

    public function generateRefundEntry($studentID, $advID, $dataArray, $commit = 0)
    {

        /* Generate function for generate refund
		 * Parameter  in : Student ID, advance payment id, array
		 * $advID  - id advance_payment
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($studentID);

        $fee_item_to_charge = array();

        $select = $db->select()
            ->from(array('a' => 'advance_payment'))
            ->joinLeft(array('c' => 'advance_payment_detail'), 'c.advpydet_advpy_id = a.advpy_id')
            ->where('a.advpy_id = ?', $advID);

        $txnData = $db->fetchAll($select);

        $feeItem = $txnData[0];

        $fee_item_to_charge = $txnData;

        $description = $dataArray['desc'];
        $amount = $dataArray['amount'];
        $currency = $feeItem['advpy_cur_id'];
        $refundtype = $dataArray['refundtype'];
        $processtype = $dataArray['processtype'];
        $studentid_new = $dataArray['studentid_new'];
        $applicantid_new = $dataArray['applicantid_new'];
        $doc_date = date('Y-m-d', strtotime($dataArray['doc_date']));
        $payto = $dataArray['payto'];
        $payname = $dataArray['payname'];
        $rchq_cheque_no = $dataArray['rchq_cheque_no'];
        $rchq_bank_name = $dataArray['rchq_bank_name'];
        $rchq_issue_date = date('Y-m-d', strtotime($dataArray['rchq_issue_date']));

        $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
        $rate = $currencyRateDb->getCurrentExchangeRate($currency);

        //get current semester
        $profile = array('ap_prog_scheme' => $profile['IdScheme'], 'branch_id' => $profile['IdBranch']);
        $semesterDB = new Registration_Model_DbTable_Semester();
        $semester = $semesterDB->getApplicantCurrentSemester($profile);

        if ($commit == 1) {
            $db->beginTransaction();
        }

        try {

            $refundDb = new Studentfinance_Model_DbTable_Refund();

            $bill_no = $this->getBillSeq(10, date('Y'));

            $data_dn = array(
                'rfd_fomulir' => $bill_no,
                'rfd_appl_id' => $feeItem['advpy_appl_id'],
                'rfd_trans_id' => $feeItem['advpy_trans_id'],
                'rfd_IdStudentRegistration' => isset($feeItem['advpy_idStudentRegistration']) ? $feeItem['advpy_idStudentRegistration'] : 0,
                'rfd_amount' => $amount,
                'rfd_type' => $refundtype,
                'rfd_process_type' => $processtype,
                'rfd_studentid_new' => $studentid_new,
                'rfd_applicantid_new' => $applicantid_new,
                'rfd_exchange_rate' => $rate['cr_exchange_rate'],
                'rfd_desc' => $description,
                'rfd_avdpy_id' => $advID,
                'rfd_sem_id' => $semester['IdSemesterMaster'],
                'rfd_currency_id' => $currency,
                'rfd_date' => $doc_date,
                'rfd_pay_to' => $payto,
                'rfd_pay_name' => $payname,
                'rfd_creator' => $creator,//by system
                'rfd_create_date' => date('Y-m-d H:i:s'),
                'rfd_status' => 'E',//entry
            );

            $dn_id = $refundDb->insert($data_dn);

            $refundDetailDb = new Studentfinance_Model_DbTable_RefundDetail();

            $cnDetailData = array(
                'rfdd_refund_id' => $dn_id,
                'rfdd_adv_id' => $advID,
                'rfdd_advpydet_id' => isset($feeItem['advpydet_id']) ? $feeItem['advpydet_id'] : 0,
                'rfdd_amount' => $amount
            );
            $dn_detail_id = $refundDetailDb->insert($cnDetailData);

            $refundChequeDb = new Studentfinance_Model_DbTable_RefundCheque();

            if ($processtype == 913) {
                $cnChequeData = array(
                    'rchq_rfd_id' => $dn_id,
                    'rchq_cheque_no' => $rchq_cheque_no,
                    'rchq_bank_name' => $rchq_bank_name,
                    'rchq_amount' => $amount,
                    'rchq_issue_date' => $rchq_issue_date
                );
                $dn_cheque_id = $refundChequeDb->insert($cnChequeData);

                $upd_data2 = array(
                    'rdf_refund_cheque_id' => $dn_cheque_id
                );

                $db->update('refund', $upd_data2, array('rfd_id = ?' => $dn_id));

            }

            if ($commit == 1) {
                $db->commit();
            }

            return $dn_id;

        } catch (Exception $e) {

            if ($commit == 1) {
                $db->rollBack();
            }

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error generate Refund Entry';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
            return null;
        }

    }

    public function refundApproval($rfdId)
    {

        /* Generate function for approve discount note
		 * Parameter  in : discount ID, 
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'refund'))
            ->joinLeft(array('adv' => 'advance_payment'), 'adv.advpy_id = a.rfd_avdpy_id')
            ->where('a.rfd_id = ?', $rfdId);

        $txnData = $db->fetchRow($select);

        $studentID = $txnData['rfd_IdStudentRegistration'];
        try {

            $refundDb = new Studentfinance_Model_DbTable_Refund();

            $amount = $txnData['rfd_amount'];
            $advId = $txnData['rfd_avdpy_id'];

            $advancePaymentDB = new Studentfinance_Model_DbTable_AdvancePayment();
            $advData = $advancePaymentDB->getData($advId);
            $balanceNew = $advData['advpy_total_balance'] - $amount;
            $dataUpdDetail = array('advpy_refund_id' => $txnData['rfd_id'], 'advpy_total_balance' => $balanceNew);
            $db->update('advance_payment', $dataUpdDetail, $db->quoteInto("advpy_id = ?", $advId));

            //insert advance payment detail
            $advancePaymentDetailDB = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
            $dataUpdMain = array(
                'advpydet_refund_id' => $txnData['rfd_id'],
                'advpydet_total_paid' => $amount,
                'advpydet_updby' => $creator,
                'advpydet_upddate' => date("Y-m-d H:i:s"),
                'advpydet_exchange_rate' => $txnData['rfd_exchange_rate'],
                'advpydet_advpy_id' => $advId
            );
            $advDetID = $advancePaymentDetailDB->insert($dataUpdMain);

            //update REFUND
            $dataUpd = array('rfd_status' => 'A', 'rfd_approver_id' => $creator, 'rfd_approve_date' => date('Y-m-d H:i:s'));
            $db->update('refund', $dataUpd, $db->quoteInto("rfd_id = ?", $rfdId));


            //update REFUND detail
            $dataUpd2 = array('rfdd_advpydet_id' => $advDetID);
            $db->update('refund_detail', $dataUpd2, $db->quoteInto("rfdd_refund_id = ?", $rfdId));

        } catch (Exception $e) {

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error Approve Refund';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
            return null;
        }

    }

    public function getActivityOtherData($programId, $level = 0)
    {//0 application, 1 = approval
        $db = Zend_Db_Table::getDefaultAdapter();

        if ($level == 0) {
            $level = 869;
        } elseif ($level == 1) {
            $level = 870;
        }

        $select = $db->select()
            ->from(array('a' => 'fee_structure_other'))
            ->where('a.fso_program_id  = ?', $programId)
            ->where('a.fso_trigger  = ?', $level);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkingGST($feeId)
    {//id fee_item
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'fee_item'))
            ->where('a.fi_id  = ?', $feeId)
            ->where('a.fi_gst = 1')
            ->where('a.fi_gst_tax IS NOT NULL')
            ->where('CURDATE() >= a.fi_effective_date');

        $result = $db->fetchRow($select);
        return $result;
    }

    public function generateSponsorInvoice($studentID, $btch_id = 0, $description, $invoice_date, $spType, $spTypeId, $dataArray, $commit = 1)
    {

//	echo "<pre>";
//	print_r($dataArray);
//	exit;
        /* Generate function for generate sponsor invoice
		 * $dataArray  -   
		 * [IdStudentRegistration]Array
		 * 			[invoice_detail_id] => 27760
                    [invoice_main_id] => 26488
                    [invoiceAmount] => 175.00
                    [sponsorCalcType] => 2
                    [sponsorCalcAmount] => 80
                    [amountSponsor] => 140
                    [amountSponsorReceipt] => 140
		 */


        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $fee_item_to_charge = array();

        $balanceNew = 0;
        $totalAmount = 0;
        foreach ($dataArray as $key => $inv) {
            $invID = $inv['invoice_detail_id'];
            $amountSN = !isset($inv['amountSponsor']) ? 0 : $inv['amountSponsor'];
            $amountSNR = !isset($inv['amountSponsorReceipt']) ? $inv['amountSponsor'] : $inv['amountSponsorReceipt'];

            $select = $db->select()
                ->from(array('c' => 'invoice_detail'), array('*', 'id_detail' => 'c.id'))
                ->join(array('a' => 'invoice_main'), 'c.invoice_main_id = a.id', array('*', 'cn_amount_main' => 'a.cn_amount', 'idMain' => 'a.id'))
                ->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id', array('subject_id'))
                ->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
                ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
                ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=c.fi_id', array(''))
                ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
                ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
                ->where('c.id = ?', $invID);

            $txnData = $db->fetchRow($select);


            $fee_item_to_charge['fee_item'][$key] = $txnData;
            $fee_item_to_charge['fee_item'][$key]['sponsor'] = $amountSN;
            $fee_item_to_charge['fee_item'][$key]['sponsorreceipt'] = $amountSNR;
            $totalAmount += $amountSN;
            $fee_item_to_charge['totalSponsor'] = $totalAmount;
        }


        if ($commit == 1) {
            $db->beginTransaction();
        }

        try {

            $sponsorMainDb = new Studentfinance_Model_DbTable_SponsorInvoice();

            $bill_no = $this->getBillSeq(11, date('Y'));

            $feeItem = $fee_item_to_charge['fee_item'][0];


            $amountTotalSN = $fee_item_to_charge['totalSponsor'];
            //dd($fee_item_to_charge);
            //if ($amountTotalSN > 0) {
                $data_dn = array(
                    'sp_fomulir_id' => $bill_no,
                    'sp_txn_id' => $feeItem['trans_id'],
                    'sp_IdStudentRegistration' => $studentID,
                    'sp_batch_id' => $btch_id,
                    'sp_amount' => $amountTotalSN,
                    'sp_balance' => $amountTotalSN,
                    'sp_type' => $spType,
                    'sp_type_id' => $spTypeId,
                    'sp_invoice_date' => date('Y-m-d', strtotime($invoice_date)),
                    'sp_description' => $description,
                    'sp_currency_id' => $feeItem['currency_id'],
                    'sp_exchange_rate' => $feeItem['exchange_rate'],
                    'sp_creator' => $creator,
                    'sp_create_date' => date('Y-m-d H:i:s'),
                    'sp_status' => 'E',//entry
                );

                $dn_id = $sponsorMainDb->insert($data_dn);

                $sponsorDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();

                foreach ($fee_item_to_charge['fee_item'] as $data) {

                    if ($data['SubCode']) {
                        if ($data['item_name']) {
                            $cn_desc = $data['SubCode'] . ' - ' . $data['SubjectName'] . ', ' . $feeItem['fee_item_description'];
                        } else {
                            $cn_desc = $data['SubCode'] . ' - ' . $data['fee_item_description'];
                        }

                    } else {
                        $cn_desc = $data['bill_description'];
                    }

                    $cnDetailData = array(
                        'sp_id' => $dn_id,
                        'sp_invoice_id' => $data['idMain'],
                        'sp_invoice_det_id' => $data['id_detail'],
                        'sp_amount' => $data['sponsor'],
                        'sp_balance' => $data['sponsor'],
                        'sp_receipt' => $data['sponsor'],
                        'sp_description' => $cn_desc
                    );
                    $dn_detail_id = $sponsorDetailDb->insert($cnDetailData);

                }

            //}

            if ($commit == 1) {
                $db->commit();
            }

            return $dn_id;

        } catch (Exception $e) {

            if ($commit == 1) {
                $db->rollBack();
            }

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error generate Sponsor Invoice by Batch';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
            return null;
        }

    }

    public function sponsorInvoiceApproval($spId)
    {

        /* Generate function for approve sponsor invoice
		 * Parameter  in : sp ID, 
		 */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'sponsor_invoice_detail'), array('sp_invoice_id', 'sp_invoice_det_id', 'sp_amount_detail' => 'a.sp_amount', 'sp_id_detail' => 'a.id'))
            ->join(array('d' => 'sponsor_invoice_main'), 'd.sp_id = a.sp_id', array('sp_fomulir_id', 'sp_IdStudentRegistration', 'sp_txn_id', 'sp_type_id', 'sp_type', 'sp_amount', 'sp_batch_id', 'sp_currency_id', 'sp_id'))
            ->join(array('b' => 'invoice_main'), 'b.id = a.sp_invoice_id', array('bill_balance', 'sp_amount_main' => 'b.sp_amount'))
            ->join(array('c' => 'invoice_detail'), 'c.id = a.sp_invoice_det_id and c.invoice_main_id = b.id', array('balance', 'sp_amount'))
            ->where('a.sp_id = ?', $spId);

        $txnData = $db->fetchAll($select);

        $studentID = $txnData[0]['sp_IdStudentRegistration'];
        try {

            $discountMainDb = new Studentfinance_Model_DbTable_Discount();
            $totalDN = 0;
            foreach ($txnData as $feeItem) {

                $amountDN = $feeItem['sp_amount_detail'];
                $invId = $feeItem['sp_invoice_id'];
                $invDetailId = $feeItem['sp_invoice_det_id'];

                $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                $invDet = $invoiceDetailDB->getData($invDetailId);

                $balanceNew1 = $invDet['balance'] - $amountDN;
                $amountDNNew1 = $invDet['sp_amount'] + $amountDN;
                $dataUpdDetail = array('sp_amount' => $amountDNNew1, 'sp_id' => $feeItem['sp_id_detail']);
                $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetailId));

                //update amount cn at invoice_main

                $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                $invMain = $invoiceMainDB->getData($invId);

                $balanceNew = $invMain['bill_balance'] - $amountDN;
                $amountDNNew = $invMain['sp_amount'] + $amountDN;
                $dataUpdMain = array('sp_amount' => $amountDNNew, 'sp_id' => $feeItem['sp_id']);
                $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invId));

            }

            //update disocunt
            $dataUpd = array('sp_status' => 'A', 'sp_approve_by' => $creator, 'sp_approve_date' => date('Y-m-d H:i:s'));
            $db->update('sponsor_invoice_main', $dataUpd, $db->quoteInto("sp_id = ?", $spId));
        } catch (Exception $e) {

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error Approve Sponsor Invoice';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
            return null;
        }

    }

    public function checkOutstandingSoa($trans_id, $type)
    {

        //profile
        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
        }


        //account
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'exchange_rate' => 'im.exchange_rate',
                )
            )
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_id', 'cur_code'))
            ->where("im.status = 'A'")
            ->group('im.id');
        if ($type == 1) {
            $select_invoice->where('im.trans_id = ?', $trans_id);
        } else {
            $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
        }


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'rcp_amount',
                    'exchange_rate' => new Zend_Db_Expr ('""'),
                )
            )
            ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_id', 'cur_code'))
            ->where("pm.rcp_status = 'APPROVE'");

        if ($type == 1) {
            $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
        } else {
            $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
        }


        $select_credit = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn.cn_id',
                    'record_date' => 'cn.cn_create_date',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_id', 'cur_code'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
            ->where("cn.cn_status = 'A'");

        if ($type == 1) {
            $select_credit->where('cn.cn_trans_id = ?', $trans_id);
        } else {
            $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
        }

        $select_discount = $db->select()
            ->from(
                array('a' => 'discount_detail'), array(
                    'id' => 'dn.dcnt_id',
                    'record_date' => 'dn.dcnt_approve_date',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'SUM(a.dcnt_amount)',
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_id', 'cur_code'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("dn.dcnt_status = 'A'")
            ->group('dn.dcnt_id');

        if ($type == 1) {
            $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        $select_refund = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_date',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'exchange_rate' => 'rfd_exchange_rate',
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_id', 'cur_code'))
            ->where("rn.rfd_status = 'A'");

        if ($type == 1) {
            $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
        }

        //advance payment from invoice deposit only
        $select_advance = $db->select()
            ->from(
                array('adv' => 'advance_payment'), array(
                    'id' => 'advpy_id',
                    'record_date' => 'advpy_date',
                    'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'advpy_amount',
                    'exchange_rate' => "IFNULL(adv.advpy_exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=adv.advpy_cur_id', array('cur_code', 'cur_id'))
            ->join(array('i' => 'invoice_main'), 'i.id=adv.advpy_invoice_id AND i.adv_transfer_id = adv.advpy_id', array())
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = i.id', array())
            ->join(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
            ->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
            ->where("i.status = 'A'")
            ->where("adv.advpy_status = 'A'");

        if ($type == 1) {
            $select_advance->where('adv.advpy_trans_id = ?', $trans_id);
        } else {
            $select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);
        }


        //get array payment				
        $select = $db->select()
            ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance), Zend_Db_Select::SQL_UNION_ALL)
            ->order("record_date asc")
            ->order("txn_type asc");

        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        //process data
        $amountCurr = array();
        $curencyArray = array();
        $balance = 0;

        if ($results) {
            foreach ($results as $row) {

                $curencyArray[$row['cur_id']] = $row['cur_code'];

                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();


                if ($row['exchange_rate']) {
                    $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                } else {
                    $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
//                        $dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                }

                if ($row['cur_id'] == 2) {
                    $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                    $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                } else {
                    $amountDebit = $row['debit'];
                    $amountCredit = $row['credit'];
                }

                if ($row['txn_type'] == "Invoice") {
                    $balance = $balance + $amountDebit;
                } else if ($row['txn_type'] == "Payment") {
                    $balance = $balance - $amountCredit;
                } else if ($row['txn_type'] == "Credit Note") {
                    $balance = $balance - $amountCredit;
                } else if ($row['txn_type'] == "Debit Note") {
                    $balance = $balance + $row['debit'];
                } else if ($row['txn_type'] == "Refund") {
                    $balance = $balance + $row['debit'];
                }

                $amountCurr['debit'] = $amountDebit;
                $amountCurr['credit'] = $amountCredit;
                $amountCurr['balance'] = number_format($balance, 2, '.', ',');

            }
        }

        return isset($amountCurr['balance']) ? $amountCurr['balance'] : '0.00';
    }

    public function checkingInitialFee($transID, $subjectID)
    {

        /*
       		 1. checking invoice status = exempted, no need to registered
			 2. checking invoice initial fee - if paid, Active = 1;else Active = 0
         */

        $registered = 0;
        $Active = 0;
        $invoiceID = NULL;

        $status = null;
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('ivd' => 'invoice_detail'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.date_create',
                    'amount' => 'balance',
                    'invoice_no' => 'im.bill_number',
                    'currency_id' => 'im.currency_id',
                    'fi_id' => 'fi_id',
                    'status' => 'im.status',
                )
            )
            ->join(array('im' => 'invoice_main'), 'ivd.invoice_main_id = im.id', array())
            ->join(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id = im.id AND ivs.invoice_detail_id = ivd.id ', array())
            ->where('im.trans_id = ?', $transID)
            ->where('ivs.subject_id = ?', $subjectID)
            ->where("im.status != 'X'");

        $select_proforma = $db->select()
            ->from(array('ivd' => 'proforma_invoice_detail'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.date_create',
                    'amount' => 'amount',
                    'invoice_no' => 'im.bill_number',
                    'currency_id' => 'im.currency_id',
                    'fi_id' => 'fi_id',
                    'status' => 'im.status',
                )
            )
            ->join(array('im' => 'proforma_invoice_main'), 'ivd.proforma_invoice_main_id = im.id', array())
            ->join(array('ivs' => 'proforma_invoice_subject'), 'ivs.proforma_invoice_main_id = im.id AND ivs.proforma_invoice_detail_id = ivd.id ', array())
            ->where('im.trans_id = ?', $transID)
            ->where('ivs.subject_id = ?', $subjectID)
            ->where("im.status != 'X'")
            ->where("im.invoice_id = 0");

        $select = $db->select()
            ->union(array($select_invoice, $select_proforma), Zend_Db_Select::SQL_UNION_ALL)
            ->order("record_date desc");

        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        $result = $db->fetchAll($select);

        $newAmount = 0;
        if ($result) {
            $amount = 0;
            foreach ($result as $data) {

                if ($data['status'] == 'A') {//registered/pre-registered
                    $amt = $data['amount'];
                    if ($data['currency_id'] == 2) {
                        $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                        $rate = $currencyRateDb->getCurrentExchangeRate(2);//usd
                        $amt = round($amt * $rate['cr_exchange_rate'], 2);
                    }

                    $amount += $amt;

                    $registered = 1;

                } elseif ($data['status'] == 'W') {//registered
                    $registered = 1;
                    $Active = 1;
//	         		$invoiceID = $data['id'];
                } elseif ($data['status'] == 'E') {//not registered
                    $registered = 0;
                }
            }

            if ($data['status'] == 'A') {
                //check advance payment
                $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
                $advPaymentData = $advancePaymentDb->getApplicantAvdPayment($transID, 1);

                $advAmount = 0;
                if ($advPaymentData) {
                    foreach ($advPaymentData as $adv) {
                        $amtAdv = $adv['advpy_total_balance'];
                        if ($adv['advpy_cur_id'] == 2) {
                            $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                            $rate = $currencyRateDb->getCurrentExchangeRate(2);//usd
                            $amtAdv = round($amtAdv * $rate['cr_exchange_rate'], 2);
                        }

                        $advAmount += $amtAdv;
                    }
                }
                $newAmount = $amount - $advAmount;
                if ($newAmount <= 0) {
                    $status = 1; // no outstanding
                    $Active = 1;
                    $invoiceID = $result[0]['id'];
                } else {
                    $status = 2; // outstanding
                }
            }
        }

        $arrayStatus = array('Active' => $Active, 'register' => $registered, 'invoice_id' => $invoiceID);

        return $arrayStatus;

    }

    public function getListStudentDataOutstanding($post = NULL, $IdSemester = NULL, $param = 0)
    {
        $auth = Zend_Auth::getInstance();
        $studentRegDB = new Registration_Model_DbTable_Studentregistration();
        $staffInfo = $studentRegDB->staffinfo($auth->getIdentity()->IdStaff);
        //var_dump($staffInfo); exit;

        $session = new Zend_Session_Namespace('sis');
        //$auth = Zend_Auth::getInstance();

        $db = Zend_Db_Table::getDefaultAdapter();


        $lstrSelect = $db->select()->from(array('a' => 'finance_outstanding_temp'))
            ->join(array('sa' => 'tbl_studentregistration'), 'sa.IdStudentRegistration = a.IdStudentRegistration', array('*'))->join(array('p' => 'student_profile'), 'p.id=sa.sp_id', array('appl_fname', 'appl_mname', 'appl_lname', 'appl_religion'))
            ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=sa.profileStatus', array('deftn.DefinitionCode', 'Status', 'DefinitionDesc'))//Application status
            ->joinLeft(array('defination' => 'tbl_definationms'), 'defination.idDefinition=sa.profileStatus', array('profileStatus' => 'DefinitionCode'))//Application STtsu
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('ArabicName', 'ProgramName', 'ProgramCode', 'prg.IdScheme'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sa.IdIntake', array('intk.IntakeDesc', 'IntakeDefaultLanguage'))
            ->joinLeft(array('s' => 'tbl_staffmaster'), 's.IdStaff = sa.AcademicAdvisor', array('advisor' => 'Fullname'))
            ->joinLeft(array('lk' => 'sis_setup_detl'), "lk.ssd_id=p.appl_religion", array("religion" => "ssd_name"))
            ->joinLeft(array('defination2' => 'tbl_definationms'), 'defination2.idDefinition=p.appl_category', array('stdCtgy' => 'DefinitionDesc'))
            ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sa.IdProgramScheme')
            ->joinLeft(array('e' => 'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop' => 'e.DefinitionDesc', 'mop_my' => 'BahasaIndonesia'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos' => 'f.DefinitionDesc', 'mos_my' => 'BahasaIndonesia'))
            ->joinLeft(array('g' => 'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt' => 'g.DefinitionDesc', 'pt_my' => 'BahasaIndonesia'))
            ->order("sa.registrationId DESC");

        if ($IdSemester) {
            $lstrSelect->joinLeft(array("sem" => "tbl_studentsemesterstatus"), 'sa.IdStudentRegistration=sem.IdStudentRegistration');
            $lstrSelect->joinLeft(array("semd" => "tbl_semestermaster"), 'sem.IdSemesterMain=semd.IdSemesterMaster');
            if ($IdSemester == "CURRENT") {
                $lstrSelect->where("semd.SemesterMainStartDate <= '" . date("Y-m-d") . "' AND semd.SemesterMainEndDate >= '" . date("Y-m-d") . "'");
            } else {
                $lstrSelect->where('semd.IdSemesterMaster = ?', $IdSemester);
            }
        }

        if ($param != 0) {
            if ($staffInfo) {
                if (isset($staffInfo['StaffAcademic']) && $staffInfo['StaffAcademic'] == 0) {
                    $lstrSelect->where("s.IdStaff = ?", $auth->getIdentity()->IdStaff);
                }
            }
        }

        /*if($session->IdRole == 311 || $session->IdRole == 298){ 			
			$lstrSelect->where("prg.IdCollege =?",$session->idCollege);
		}else{
			
			if(isset($post['IdCollege']) && !empty($post['IdCollege'])){
				$lstrSelect->where("prg.IdCollege =?",$post["IdCollege"]);
			}
		}              
	    
		if($session->IdRole == 445 || $session->IdRole == 3){ 
						
			//$lstrSelect->where("sa.AcademicAdvisor =?",$auth->getIdentity()->IdStaff);
		}		
		*/

        if (isset($post['applicant_name']) && !empty($post['applicant_name'])) {

            $lstrSelect->where("(p.appl_fname LIKE '%" . $post['applicant_name'] . "%'");
            $lstrSelect->orwhere("p.appl_mname LIKE '%" . $post['applicant_name'] . "%'");
            $lstrSelect->orwhere("p.appl_lname LIKE '%" . $post['applicant_name'] . "%')");
        }

        if (isset($post['applicant_nomor']) && !empty($post['applicant_nomor'])) {
            $lstrSelect->where("sa.registrationId LIKE '%" . $post['applicant_nomor'] . "%'");
        }

        if (isset($post['at_pes_id']) && !empty($post['at_pes_id'])) {
            $lstrSelect->where("at.at_pes_id LIKE '%" . $post['at_pes_id'] . "%'");
        }

        if (isset($post['profile_status']) && !empty($post['profile_status'])) {
            $lstrSelect->where("sa.profileStatus = ?", $post['profile_status']);
        }

        if (isset($post['advisor']) && !empty($post['advisor'])) {
            $lstrSelect->where("sa.AcademicAdvisor = ?", $post['advisor']);
        }

        if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {
            $lstrSelect->where("sa.IdProgram = ?", $post['IdProgram']);
        }

        if (isset($post['IdProgramScheme']) && !empty($post['IdProgramScheme'])) {
            $lstrSelect->where("sa.IdProgramScheme = ?", $post['IdProgramScheme']);
        }

        if (isset($post['IdIntake']) && !empty($post['IdIntake'])) {
            $lstrSelect->where("sa.IdIntake = ?", $post['IdIntake']);
        }


        if (isset($post['s_student_id']) && !empty($post['s_student_id'])) {
            if (isset($post['e_student_id']) && !empty($post['e_student_id'])) {
                $lstrSelect->where("sa.registrationId >= '" . $post['s_student_id'] . "' AND sa.registrationId <= '" . $post['e_student_id'] . "'");
            } else {
                $lstrSelect->where("sa.registrationId >= '" . $post['s_student_id'] . "' AND sa.registrationId <= '" . $post['s_student_id'] . "'");
            }
        }

        if (isset($post['student_id']) && !empty($post['student_id'])) {
            $lstrSelect->where("sa.registrationId = '" . $post['student_id'] . "'");

        }

        if (isset($post['tagging_status']) && !empty($post['tagging_status'])) {

            if ($post['tagging_status'] == 1) {
                $lstrSelect->where("sa.AcademicAdvisor != 0");
            } else if ($post['tagging_status'] == 2) {
                $lstrSelect->where("sa.AcademicAdvisor = 0");
            }
        }


        if (isset($post['registrationId_from']) && !empty($post['registrationId_from'])) {
            $lstrSelect->where("sa.registrationId BETWEEN '" . $post['registrationId_from'] . "' AND '" . $post['registrationId_to'] . "'");
        }


        if (isset($post['profileStatus']) && !empty($post['profileStatus'])) {
            $lstrSelect->where("sa.profileStatus = ?", $post['profileStatus']);
        }

        if (isset($post['religion']) && !empty($post['religion'])) {
            $lstrSelect->where("p.appl_religion = ?", $post['religion']);
        }

        if (isset($post['appl_category']) && !empty($post['appl_category'])) {
            $lstrSelect->where("p.appl_category = ?", $post['appl_category']);
        }

        if (isset($post['IdBranch']) && !empty($post['IdBranch'])) {
            $lstrSelect->where("sa.IdBranch = ?", $post['IdBranch']);
        }

        if (isset($post['student_passport']) && !empty($post['student_passport'])) {
            $lstrSelect->join(array('passport' => 'student_passport'), 'passport.sp_id=p.id', array('p_passport_no'));
            $lstrSelect->where("passport.p_passport_no LIKE '%" . $post['student_passport'] . "%'");
        }

        return $lstrSelect;
    }

    public function getListStudentOutstanding($post = NULL, $IdSemester = NULL, $param = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $this->getListStudentDataOutstanding($post, $IdSemester, $param);

//        echo $lstrSelect;exit;
//        $result = $db->fetchAll($lstrSelect);
        return $lstrSelect;
    }

    public function getOutstandingBalance($transID, $type, $semesterID = 0)
    {

        /*
         1. checking funding type = self funding only
         2. checking outstanding proforma invoice where A n invoice_id = null
         3. checking advance payment
         *  invoice & proforma & advance payment amount in MYR
         *  return os balance
         */

        $status = null;
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectScholar = $db->select()
            ->from(array('schtg' => 'tbl_scholarship_studenttag'), array(
                    'IdStudentRegistration' => 'schtg.sa_cust_id',
                    'type' => new Zend_Db_Expr ('"Scholarship"'),
                    'fundingMethod' => 'sca.sch_name',
                    'start_date' => 'DATE(schtg.sa_start_date)',
                    'end_date' => "IFNULL(DATE(schtg.sa_end_date),'0000-00-00')",
                )
            )
            ->joinLeft(array('sca' => 'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array())
            ->where("schtg.sa_cust_id = ?", $transID);

        $selectSponsor = $db->select()
            ->from(array('spt' => 'tbl_sponsor_tag'), array(
                    'IdStudentRegistration' => "spt.StudentId",
                    'type' => new Zend_Db_Expr ('"Sponsorship"'),
                    'fundingMethod' => "CONCAT_WS(' ',sptt.fName, sptt.lName)",
                    'start_date' => 'DATE(spt.StartDate)',
                    'end_date' => "IFNULL(DATE(spt.EndDate),'0000-00-00')",
                )
            )
            ->joinLeft(array('sptt' => 'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array())
            ->where("spt.StudentId = ?", $transID);

        $selectFunding = $db->select()
            ->union(array($selectScholar, $selectSponsor), Zend_Db_Select::SQL_UNION_ALL);

        $resultsFunding = $db->fetchAll($selectFunding);

//		echo "<pre>";
//		print_r($resultsFunding);
        $date = new DateTime('+1 day');
        $currentDate = $date->format('Y-m-d');

        foreach ($resultsFunding as $key => $fund) {
            $disStartDate = $fund['start_date'];
            $disEndDate = $fund['end_date'];
            if ($disEndDate != '0000-00-00') {
                $disEndDate = $disEndDate;
            } else {
                $disEndDate = $currentDate;
            }

            if ($currentDate >= $disStartDate) {
                if ($currentDate <= $disEndDate) {

                } else {
                    unset($resultsFunding[$key]);
                }

            } else {
                unset($resultsFunding[$key]);
            }


        }
//echo "<pre>";
//		print_r($resultsFunding);
//		echo count($resultsFunding);
//		exit;

        //get os balance from setup
//		$finOsBal = $this->getFinanceConfiguration();
        $finOsBal = 0;
        $status = 3; // no record/no invoice
        $newAmount = 0;

        //if (count($resultsFunding) == 0) {

            $select_invoice = $db->select()
                ->from(array('im' => 'invoice_main'), array(
                        'id' => 'im.id',
                        'record_date' => 'im.date_create',
                        'amount' => 'bill_balance',
                        'invoice_no' => 'bill_number',
                        'currency_id' => 'currency_id',
                    )
                )
                //->where('im.trans_id = ?', $transID)// salah kalau nak check student
                ->where("im.status = 'A'");
            if ($type == 1) { //applicant
                $select_invoice->where('im.trans_id  = ?', $transID);
            } elseif ($type == 2) { //student
                $select_invoice->where('im.IdStudentRegistration  = ?', $transID);
            }

            if ($semesterID != 0) {
                $select_invoice->where('im.semester  = ?', $semesterID);
            }

            $select_proforma = $db->select()
                ->from(array('im' => 'proforma_invoice_main'), array(
                        'id' => 'im.id',
                        'record_date' => 'im.date_create',
                        'amount' => 'bill_amount',
                        'invoice_no' => 'bill_number',
                        'currency_id' => 'currency_id',
                    )
                )
//						->where('im.trans_id = ?', $transID)
                ->where("im.status = 'A'")
                ->where("im.invoice_id = 0");
            if ($type == 1) { //applicant
                $select_proforma->where('im.trans_id  = ?', $transID);
            } elseif ($type == 2) { //student
                $select_proforma->where('im.IdStudentRegistration  = ?', $transID);
            }

            $select = $db->select()
                ->union(array($select_invoice, $select_proforma), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date desc");

            $results = $db->fetchAll($select);
            if (!$results) {
                $results = null;
            }

            $result = $db->fetchAll($select);


            if ($result) {
                $amount = 0;
                foreach ($result as $data) {
                    $amt = $data['amount'];
                    if ($data['currency_id'] == 2) {
                        $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                        $rate = $currencyRateDb->getCurrentExchangeRate(2);//usd
                        $amt = round($amt * $rate['cr_exchange_rate'], 2);
                    }

                    $amount += $amt;
                }

                $newAmount = $amount;
                if ($newAmount <= $finOsBal) {
                    $status = 1; // no outstanding
                } else {
                    $status = 2; // outstanding
                }

            } else {
                $status = 3; // no record/no invoice
            }
        //}

        return array('status' => $status, 'amount' => $newAmount);

    }

    public function generateInvoiceYearly($studentID, $commit = 0)
    {

        /*
		 * Script to generate invoice yearly
		 * Parameter in: Student ID
		 */

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);
        $semesterDB = new Registration_Model_DbTable_Semester();
        $semester = $semesterDB->getApplicantCurrentSemester($profile);

        $checkingInvoice = $studentInfo['invoice'];
        $fs_id = $studentInfo['fs_id'];
        $currentYear = date('Y');
        $idCountry = $studentInfo['appl_nationality'];

        if (trim($studentInfo['sem_seq']) == trim($semester['sem_seq'])) {
            if ($checkingInvoice == 1) {

                $db = Zend_Db_Table::getDefaultAdapter();

                $select = $db->select()
                    ->from(array('a' => 'fee_structure_item'))
                    ->join(array('b' => 'fee_item'), 'b.fi_id = a.fsi_item_id', array('*'))
                    ->join(array('c' => 'tbl_fee_category'), 'c.fc_id = b.fi_fc_id', array('fc_generateyear'))
                    ->where('c.fc_generateyear = 1')
                    ->where('a.fsi_structure_id =?', $fs_id)
                    ->where('b.fi_frequency_mode =?', 304)
                    ->group('a.fsi_id');

                if ($studentInfo['resource_fee_exclude'] == 1){
                    $select->where('b.fi_fc_id NOT IN (7, 6)');
                }

                $fsProgramItem = $db->fetchAll($select);

                $fee_item_to_charge = array();
                $feeIDArr = array(0);

                if (!empty($fsProgramItem)) {
                    foreach ($fsProgramItem as $keyfs2 => $fsitem2) {
                        array_push($feeIDArr, $fsitem2['fi_id']);
                    }
                }

                if (!empty($fsProgramItem)) {
                    foreach ($fsProgramItem as $keyfs => $fsitem) {
                        $feeID = $fsitem['fi_id'];

                        //checking invoice already generated or not

                        $selectInv = $db->select()
                            ->from(array('a' => 'invoice_detail'))
                            ->join(array('b' => 'invoice_main'), 'b.id = a.invoice_main_id', array('*'))
                            ->join(array('c'=>'tbl_semestermaster'), 'b.semester = c.IdSemesterMaster')
                            ->where("b.status IN ('A', 'W')")
                            ->where('b.IdStudentRegistration =?', $studentID)
                            ->where('a.fi_id IN (?)', $feeIDArr)
                            ->where('c.AcademicYear = ?', $currentYear);
                            //->where('year(b.invoice_date) = ?', $currentYear);

                        $invoiceFee = $db->fetchRow($selectInv);

                        if ($invoiceFee) {

                        } else {

                            //get current semester
                            $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);
                            $semesterDB = new Registration_Model_DbTable_Semester();
                            $semester = $semesterDB->getApplicantCurrentSemester($profile);
                            $semesterID = $semester['IdSemesterMaster'];


                            //check country origin
                            if ($fsitem['fi_amount_calculation_type'] == 618) {
                                //get fee_item_category
                                $curID = $fsitem['fsi_cur_id'];
                                $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                                $feeCountryData = $feeCountryDB->getItemData($feeID, $idCountry, $curID);

                                //yearly
                                $amount = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];

                            } else {
                                //yearly
                                $amount = $fsitem['fsi_amount'];
                                $curID = $fsitem['fsi_cur_id'];
                            }

                            $this->generateInvoice($studentID, $feeID, $semesterID, $amount, $curID, 0, 0);
                        }

                    }
                }
            }
        }
    }

    public function generateInvoiceDefinedSemester($studentID, $commit = 0)
    {

        /*
		 * Script to generate invoice yearly
		 * Parameter in: Student ID
		 */

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);
        $semesterDB = new Registration_Model_DbTable_Semester();
        $semester = $semesterDB->getApplicantCurrentSemester($profile);

        $checkingInvoice = $studentInfo['invoice'];
        $fs_id = $studentInfo['fs_id'];
        $currentYear = date('Y');
        $idCountry = $studentInfo['appl_nationality'];

        //if (trim($studentInfo['sem_seq']) == trim($semester['sem_seq'])) {
            if ($checkingInvoice == 1) {

                $db = Zend_Db_Table::getDefaultAdapter();

                $select = $db->select()
                    ->from(array('a' => 'fee_structure_item'))
                    ->join(array('b' => 'fee_item'), 'b.fi_id = a.fsi_item_id', array('*'))
                    ->join(array('c' => 'tbl_fee_category'), 'c.fc_id = b.fi_fc_id', array('fc_generateyear'))
                    //->where('c.fc_generateyear = 1')
                    ->where('a.fsi_structure_id =?', $fs_id)
                    ->where('b.fi_frequency_mode =?', 305)
                    ->group('a.fsi_id');

                if ($studentInfo['resource_fee_exclude'] == 1){
                    $select->where('b.fi_fc_id NOT IN (7, 6)');
                }

                $fsProgramItem = $db->fetchAll($select);

                ///dd($fsProgramItem); exit;

                $fee_item_to_charge = array();
                $feeIDArr = array(0);

                if (!empty($fsProgramItem)) {
                    foreach ($fsProgramItem as $keyfs2 => $fsitem2) {
                        array_push($feeIDArr, $fsitem2['fi_id']);
                    }
                }

                if (!empty($fsProgramItem)) {
                    foreach ($fsProgramItem as $keyfs => $fsitem) {
                        //@todo check define semester

                        $selectDefineSemester = $db->select()
                            ->from(array('a'=>'fee_structure_item_semester'))
                            ->where('a.fsis_item_id = ?', $fsitem['fsi_id']);

                        $defineSemester = $db->fetchAll($selectDefineSemester);

                        if ($defineSemester) {

                            $ds = array();

                            foreach ($defineSemester as $defineSemesterLoop){
                                if ($defineSemesterLoop['fsis_semester']==1){
                                    array_push($ds, 'JAN');
                                }else if ($defineSemesterLoop['fsis_semester']==2){
                                    array_push($ds, 'JUN');
                                }else if ($defineSemesterLoop['fsis_semester']==3){
                                    array_push($ds, 'SEP');
                                }
                            }

                            if (in_array($semester['sem_seq'], $ds)) {

                                $feeID = $fsitem['fi_id'];

                                //checking invoice already generated or not

                                $selectInv = $db->select()
                                    ->from(array('a' => 'invoice_detail'))
                                    ->join(array('b' => 'invoice_main'), 'b.id = a.invoice_main_id', array('*'))
                                    ->join(array('c' => 'tbl_semestermaster'), 'b.semester = c.IdSemesterMaster')
                                    ->where("b.status IN ('A', 'W')")
                                    ->where('b.IdStudentRegistration =?', $studentID)
                                    //->where('a.fi_id IN (?)', $feeIDArr)
                                    ->where('a.fi_id IN (?)', $feeID)
                                    ->where('c.sem_seq = ?', $semester['sem_seq']);
                                //->where('year(b.invoice_date) = ?', $currentYear);

                                $invoiceFee = $db->fetchRow($selectInv);

                                //dd($invoiceFee); exit;

                                if ($invoiceFee) {

                                } else {

                                    //get current semester
                                    $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);
                                    $semesterDB = new Registration_Model_DbTable_Semester();
                                    $semester = $semesterDB->getApplicantCurrentSemester($profile);
                                    $semesterID = $semester['IdSemesterMaster'];


                                    //check country origin
                                    if ($fsitem['fi_amount_calculation_type'] == 618) {
                                        //get fee_item_category
                                        $curID = $fsitem['fsi_cur_id'];
                                        $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                                        $feeCountryData = $feeCountryDB->getItemData($feeID, $idCountry, $curID);

                                        //yearly
                                        $amount = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];

                                    } else {
                                        //yearly
                                        $amount = $fsitem['fsi_amount'];
                                        $curID = $fsitem['fsi_cur_id'];
                                    }

                                    $this->generateInvoice($studentID, $feeID, $semesterID, $amount, $curID, 0, 0);
                                }
                            }
                        }
                    }
                }
            }
        //}
    }

    public function getAmountResourceFee($IdStudentRegistration)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'invoice_main'))
            ->join(array('c' => 'tbl_currency'), 'c.cur_id = a.currency_id')
            ->where('a.IdStudentRegistration  = ?', $IdStudentRegistration)
            ->where("a.bill_description LIKE '%Resource Fee%' or a.bill_description LIKE '%Services Fee%'")
            ->where("a.status != 'X'");

        $result = $db->fetchRow($select);

        return array('currency' => $result['cur_code'], 'amount' => $result['bill_amount']);
    }


    public function generateInvoiceConvocation($studentID, $convoID, $feeID, $guestNo = 1)
    {

        /**
         * function to generate convocation fee
         */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];
        $invoice_id = null;
        if ($checkingInvoice == 1) {

            //get fee info
            $feeConvoDB = new Studentfinance_Model_DbTable_FeeItemConvo();
            $feeConvoData = $feeConvoDB->getDataConvoFee($convoID, $feeID);

            if ($feeConvoData) {
                $amountAdd = $feeConvoData['fv_amount'];
                if ($guestNo > 1) {
                    $amountAdd = $amountAdd * $guestNo;
                }

                $curID = $feeConvoData['fv_currency_id'];
                $feeDesc = $feeConvoData['fi_name'] . ' for Session ' . $feeConvoData['c_session'] . '/' . $feeConvoData['c_year'];
                $feeID = $feeConvoData['fv_fi_id'];

                $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                $currency = $currencyDb->getCurrentExchangeRate($curID);

                //get current semester
                $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);
                $semesterDB = new Registration_Model_DbTable_Semester();
                $semester = $semesterDB->getApplicantCurrentSemester($profile);
                $semesterID = $semester['IdSemesterMaster'];

                $db->beginTransaction();
                try {

                    //insert into invoice_main
                    $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                    $bill_no = $this->getBillSeq(2, date('Y'));
                    $data_invoice = array(
                        'bill_number' => $bill_no,
                        'appl_id' => isset($studentInfo['appl_id']) ? $studentInfo['appl_id'] : 0,
                        'trans_id' => isset($studentInfo['transaction_id']) ? $studentInfo['transaction_id'] : 0,
                        'IdStudentRegistration' => isset($studentInfo['IdStudentRegistration']) ? $studentInfo['IdStudentRegistration'] : 0,
                        'academic_year' => '',
                        'semester' => $semesterID,
                        'bill_amount' => $amountAdd,
                        'bill_paid' => 0,
                        'bill_balance' => $amountAdd,
                        'bill_description' => $feeDesc,
                        'program_id' => $studentInfo['IdProgram'],
                        'fs_id' => $studentInfo['fs_id'],
                        'currency_id' => $curID,
                        'exchange_rate' => $currency['cr_exchange_rate'],
                        'invoice_date' => date('Y-m-d'),
                        'date_create' => date('Y-m-d H:i:s'),
                        'creator' => $creator//by admin
                    );


                    $invoice_id = $invoiceMainDb->insert($data_invoice);

                    //insert invoice detail
                    $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invoice_id,
                        'fi_id' => $feeID,
                        'fee_item_description' => $feeConvoData['fi_name'],
                        'cur_id' => $curID,
                        'amount' => $amountAdd,
                        'balance' => $amountAdd,
                        'exchange_rate' => $currency['cr_exchange_rate']
                    );

                    $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                    $db->commit();

                } catch (Exception $e) {

                    $db->rollBack();

                    //save error message
                    $sysErDb = new App_Model_General_DbTable_SystemError();
                    $msg['se_IdStudentRegistration'] = $studentID;
                    $msg['se_title'] = 'Error generate invoice';
                    $msg['se_message'] = $e->getMessage();
                    $msg['se_createdby'] = $creator;
                    $msg['se_createddt'] = date("Y-m-d H:i:s");
                    $sysErDb->addData($msg);

                    throw $e;
                }
            }
        }
        return $invoice_id;
    }

    public function generateInvoicePostage($studentID, $feeID, $countryId, $stateId = 0)
    {

        /**
         * function to generate postage fee
         */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        //checking CP
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];
        $invoice_id = null;

//        if ($checkingInvoice == 1) {

            $curID = 1;//myr only

            $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
            $currency = $currencyDb->getCurrentExchangeRate($curID);

            /*get fee info
			 * 1. check state
			 * 2. if state amount null, check country
			 */

            $amountAdd = 0;

            if ($stateId) {
                $feeStateDB = new Studentfinance_Model_DbTable_FeeItemState();
                $feeStateData = $feeStateDB->getItemData($feeID, $stateId, $curID);

                if ($feeStateData) {
                    $amountAdd = $feeStateData['fic_amount'];
                    $feeDesc = $feeStateData['fi_name'];
                } else {
                    $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                    $feeCountryData = $feeCountryDB->getItemData($feeID, $countryId, $curID);
                    if ($feeCountryData) {
                        $amountAdd = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
                        $feeDesc = $feeCountryData['fi_name'];
                    }
                }
            } else {
                $feeCountryDB = new Studentfinance_Model_DbTable_FeeCountry();
                $feeCountryData = $feeCountryDB->getItemData($feeID, $countryId, $curID);
                if ($feeCountryData) {
                    $amountAdd = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
                    $feeDesc = $feeCountryData['fi_name'];
                }
            }

            if ($amountAdd != 0) {

                //get current semester
                $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);
                $semesterDB = new Registration_Model_DbTable_Semester();
                $semester = $semesterDB->getApplicantCurrentSemester($profile);
                $semesterID = $semester['IdSemesterMaster'];

                $db->beginTransaction();
                try {

                    //insert into invoice_main
                    $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                    $bill_no = $this->getBillSeq(2, date('Y'));
                    $data_invoice = array(
                        'bill_number' => $bill_no,
                        'appl_id' => isset($studentInfo['appl_id']) ? $studentInfo['appl_id'] : 0,
                        'trans_id' => isset($studentInfo['transaction_id']) ? $studentInfo['transaction_id'] : 0,
                        'IdStudentRegistration' => isset($studentInfo['IdStudentRegistration']) ? $studentInfo['IdStudentRegistration'] : 0,
                        'academic_year' => '',
                        'semester' => $semesterID,
                        'bill_amount' => $amountAdd,
                        'bill_paid' => 0,
                        'bill_balance' => $amountAdd,
                        'bill_description' => $feeDesc,
                        'program_id' => $studentInfo['IdProgram'],
                        'fs_id' => $studentInfo['fs_id'],
                        'currency_id' => $curID,
                        'exchange_rate' => $currency['cr_exchange_rate'],
                        'invoice_date' => date('Y-m-d'),
                        'date_create' => date('Y-m-d H:i:s'),
                        'creator' => $creator//by admin
                    );

//							echo "<pre>";
//							print_r($data_invoice);

                    $invoice_id = $invoiceMainDb->insert($data_invoice);

                    //insert invoice detail
                    $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invoice_id,
                        'fi_id' => $feeID,
                        'fee_item_description' => $feeDesc,
                        'cur_id' => $curID,
                        'amount' => $amountAdd,
                        'balance' => $amountAdd,
                        'exchange_rate' => $currency['cr_exchange_rate']
                    );

                    $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                    $db->commit();

                } catch (Exception $e) {

                    $db->rollBack();

                    //save error message
                    $sysErDb = new App_Model_General_DbTable_SystemError();
                    $msg['se_IdStudentRegistration'] = $studentID;
                    $msg['se_title'] = 'Error generate invoice';
                    $msg['se_message'] = $e->getMessage();
                    $msg['se_createdby'] = $creator;
                    $msg['se_createddt'] = date("Y-m-d H:i:s");
                    $sysErDb->addData($msg);

                    throw $e;
                }
            }
//        }
        return $invoice_id;
    }

    public function calculateAmountConvocation($convoID, $feeID, $guestNo = 1)
    {

        /**
         * function to calculate convo amount
         */

        //get fee info
        $feeConvoDB = new Studentfinance_Model_DbTable_FeeItemConvo();
        $feeConvoData = $feeConvoDB->getDataConvoFee($convoID, $feeID);
        $amountAdd = NULL;
        if ($feeConvoData) {
            $amountAdd = $feeConvoData['fv_amount'];
            if ($guestNo > 1) {
                $amountAdd = $amountAdd * $guestNo;
            }
        }

        return $feeConvoData['cur_code'] . $amountAdd;

    }

    public function balance($trans_id, $type, $dateFrom, $dateTo)
    {

        //$idStudentRegistration = $this->_getParam('id_reg', null);
        //$this->view->idStudentRegistration = $idStudentRegistration;

        //title
        //$this->view->title = $this->view->translate("Statement of Account");

        //profile
        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
        }


        //$this->view->profile = $profile;

        //account
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'description' => 'GROUP_CONCAT(fc.fi_name)',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'bill_number',
                    'invoice_no' => 'bill_number',
                    'receipt_no' => new Zend_Db_Expr ('"-"'),
                    'subject' => 'GROUP_CONCAT(distinct s.SubCode)',
                    'fc_seq' => 'ivd.fi_id',
                    'migrate_desc' => 'bill_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => 'im.exchange_rate',
                )
            )
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            //->where('im.currency_id= ?',$currency)

            ->where("im.status NOT IN ('X','0')")//display ALL status except X,0=entry (24/27/07/2015)
            ->group('im.id');
        if ($type == 1) {
            $select_invoice->where('im.trans_id = ?', $trans_id);
        } else {
            $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_invoice->where("DATE(im.invoice_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_invoice->where("DATE(im.invoice_date) <= ?", $dateTo);
        }


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => "rcp_amount",
                    'document' => 'rcp_gainloss_soa',
                    'invoice_no' => 'rcp_no',
                    'receipt_no' => 'rcp_no',
                    'subject' => 'rcp_gainloss',//gain/loss
                    'fc_seq' => 'rcp_gainloss_amt',//gain/loss - myr
                    'migrate_desc' => 'pm.rcp_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('a' => 'tbl_receivable_adjustment'), "a.IdReceipt = pm.rcp_id and a.Status = 'APPROVE'", array())
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("pm.rcp_status = 'APPROVE' OR (pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0 AND MONTH(pm.rcp_receive_date) != MONTH(a.EnterDate))");
//							->orWhere("pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0");
        if ($type == 1) {
            $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
        } else {
            $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_payment->where("DATE(pm.rcp_receive_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_payment->where("DATE(pm.rcp_receive_date) <= ?", $dateTo);
        }

        $select_credit = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn.cn_id',
                    'record_date' => 'cn.cn_create_date',
                    'description' => 'cn.cn_description',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'document' => 'cn_billing_no',
                    'invoice_no' => 'cn_billing_no',
                    'receipt_no' => 'cn_billing_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'cn.cn_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(i.exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("cn.cn_status = 'A'");
        if ($type == 1) {
            $select_credit->where('cn.cn_trans_id = ?', $trans_id);
        } else {
            $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_credit->where("DATE(cn.cn_create_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_credit->where("DATE(cn.cn_create_date) <= ?", $dateTo);
        }

        $select_discount = $db->select()
            ->from(
                array('a' => 'discount_detail'), array(
                    'id' => 'dn.dcnt_id',
                    'record_date' => 'dn.dcnt_approve_date',
                    'description' => 'dn.dcnt_description',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'SUM(a.dcnt_amount)',
                    'document' => 'dn.dcnt_fomulir_id',
                    'invoice_no' => 'dn.dcnt_fomulir_id',
                    'receipt_no' => 'dn.dcnt_fomulir_id',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dn.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
//							->joinLeft(array('adj'=>'discount_adjustment'),"adj.IdDiscount = a.id and adj.Status= 'APPROVE' AND (MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date))", array())
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date)")
            ->group('dn.dcnt_id');

        if ($type == 1) {
            $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_discount->where("DATE(dn.dcnt_approve_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_discount->where("DATE(dn.dcnt_approve_date) <= ?", $dateTo);
        }

        //refund
        $select_refund = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'rfd_fomulir',
                    'invoice_no' => 'rfd_fomulir',
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rn.rfd_date) != MONTH(rn.rfd_cancel_date)")
            ->where("rn.rfd_status IN  ('A','X')");

        if ($type == 1) {
            $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_refund->where("DATE(rfd_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_refund->where("DATE(rfd_date) <= ?", $dateTo);
        }

        //advance payment from invoice deposit only
        $select_advance = $db->select()
            ->from(
                array('adv' => 'advance_payment'), array(
                    'id' => 'advpy_id',
                    'record_date' => 'advpy_date',
                    'description' => "CONCAT(advpy_description,' - ',fi.fi_name)",
                    'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'advpy_amount',
                    'document' => 'advpy_fomulir',
                    'invoice_no' => 'advpy_fomulir',
                    'receipt_no' => 'advpy_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'advpy_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(adv.advpy_exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=adv.advpy_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('i' => 'invoice_main'), 'i.id=adv.advpy_invoice_id AND i.adv_transfer_id = adv.advpy_id', array())
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = i.id', array())
            ->join(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
            ->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
            ->where("i.status = 'A'")
            ->where("adv.advpy_status = 'A'");

        if ($type == 1) {
            $select_advance->where('adv.advpy_trans_id = ?', $trans_id);
        } else {
            $select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_advance->where("DATE(advpy_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_advance->where("DATE(advpy_date) <= ?", $dateTo);
        }


        //refund cancel
        $select_refund_cancel = $db->select()
            ->from(
                array('rnc' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_cancel_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund Adjustment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'rfd_amount',
                    'document' => 'rfd_fomulir',
                    'invoice_no' => "CONCAT(rfd_fomulir,'-',rfd_id)",
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rnc.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rnc.rfd_date) != MONTH(rnc.rfd_cancel_date)")
            ->where("rnc.rfd_status = 'X'");

        if ($type == 1) {
            $select_refund_cancel->where('rnc.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund_cancel->where('rnc.rfd_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_refund_cancel->where("DATE(rfd_cancel_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_refund_cancel->where("DATE(rfd_cancel_date) <= ?", $dateTo);
        }

        //discount adjustment
        $select_discount_cancel = $db->select()
            ->from(
                array('dsn' => 'discount_adjustment'), array(
                    'id' => 'dsn.id',
                    'record_date' => 'dsn.EnterDate',
                    'description' => "CONCAT(dsnt.dcnt_description,' (',dsnta.dcnt_fomulir_id,')')",
                    'txn_type' => new Zend_Db_Expr ('"Discount Adjustment"'),
                    'debit' => 'dsn.amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => "AdjustmentCode",
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dsnt.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dsn.cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('dsnt' => 'discount_detail'), 'dsnt.id=dsn.IdDiscount', array())
            ->join(array('dsnta' => 'discount'), 'dsnta.dcnt_id=dsnt.dcnt_id', array())
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = dsnt.dcnt_invoice_id', array())
            ->where("dsn.Status = 'APPROVE'")
            ->where("MONTH(dsnta.dcnt_approve_date) != MONTH(dsnta.dcnt_cancel_date)")
            ->where('dsn.AdjustmentType = 1'); //cancel

        if ($type == 1) {
            $select_discount_cancel->where('dsnta.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount_cancel->where('dsnta.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_discount_cancel->where("DATE(dsn.EnterDate) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_discount_cancel->where("DATE(dsn.EnterDate) <= ?", $dateTo);
        }


        //payment adjustment - cancel approved receipt
        //checking not same month with receipt only

        $select_receipt_adj = $db->select()
            ->from(
                array('a' => 'tbl_receivable_adjustment'), array(
                    'id' => 'a.IdReceivableAdjustment',
                    'record_date' => 'a.EnterDate',
                    'description' => 'a.Remarks',
                    'txn_type' => new Zend_Db_Expr ('"Payment Adjustment"'),
                    'debit' => 'b.rcp_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => 'AdjustmentCode',
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'a.Remarks',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'receipt'), 'b.rcp_id = a.IdReceipt', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("a.Status = 'APPROVE'")
            ->where("MONTH(b.rcp_receive_date) != MONTH(a.EnterDate)");

        if ($type == 1) {
            $select_receipt_adj->where('b.rcp_trans_id = ?', $trans_id);
        } else {
            $select_receipt_adj->where('b.rcp_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_receipt_adj->where("DATE(a.EnterDate) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_receipt_adj->where("DATE(a.EnterDate) <= ?", $dateTo);
        }

        //sponsor payment
        if ($type == 2) {
            $select_receipt_sponsor = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'b.rcp_receive_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
                        'debit' => new Zend_Db_Expr ('"0.00"'),
                        'credit' => 'a.rcp_inv_amount',
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'b.rcp_no',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->where("b.rcp_status = 'APPROVE'");


            $select_receipt_sponsor->where('a.IdStudentRegistration = ?', $trans_id);

            if ($dateFrom != '') {
                $select_receipt_sponsor->where("DATE(b.rcp_receive_date) >= ?", $dateFrom);
            }

            if ($dateTo != '') {
                $select_receipt_sponsor->where("DATE(b.rcp_receive_date) <= ?", $dateTo);
            }

            $select_sponsor_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'e.sp_cancel_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'e.sp_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", e.id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                ->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                ->where("e.sp_status = ?", 0)
                ->where("b.rcp_status = 'APPROVE'");

            $select_sponsor_adjustment->where('a.IdStudentRegistration = ?', $trans_id);

            if ($dateFrom != '') {
                $select_sponsor_adjustment->where("DATE(e.sp_cancel_date) >= ?", $dateFrom);
            }

            if ($dateTo != '') {
                $select_sponsor_adjustment->where("DATE(e.sp_cancel_date) <= ?", $dateTo);
            }
        }

        //get array payment

        if ($type == 1) {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");

        } else {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");
        }

        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        //$this->view->account = $results;

        //process data
        $amountCurr = array();
        $curencyArray = array();
        $balance = 0;

        if ($results) {
            foreach ($results as $row) {
                $curencyArray[$row['cur_id']] = $row['cur_code'];

                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();


                if ($row['exchange_rate']) {
                    if ($row['exchange_rate'] == 'x') {
                        $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
//							$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                    } else {
                        $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                    }
                } else {
                    $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);
//						$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                }

//					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
//					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
//					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];
                if ($row['cur_id'] == 2) {
                    $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                    $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                } else {
                    $amountDebit = $row['debit'];
                    $amountCredit = $row['credit'];
                }
//					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;

                if ($row['txn_type'] == "Invoice") {
                    $balance = $balance + $amountDebit;
                } else
                    if ($row['txn_type'] == "Payment") {

                        /*$balance = $balance - $amountCredit;

                        ///hide on 05/08/2015 - do nothing for no
                        //unhide 12/08/2015

                        if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                            if ($row['subject'] == 'gain') {
                                $balance = $balance + $row['fc_seq'];
                                $amountCredit = $amountCredit - $row['fc_seq'];
                            } elseif ($row['subject'] == 'loss') {//loss
                                $balance = $balance - $row['fc_seq'];
                                $amountCredit = $amountCredit + $row['fc_seq'];
                            }
                        }*/

                        //cek invoice value and receipt value
                        $getReceiptInvoice = $curRateDB->getReceiptInvoice($row['id']);

                        $invoiceValue = 0;

                        if ($getReceiptInvoice){
                            foreach ($getReceiptInvoice as $getReceiptInvoiceLoop){
                                if ($getReceiptInvoiceLoop['rcp_inv_cur_id'] != 1) {
                                    $getReceiptInvoiceRate = $curRateDB->getRateByDate($getReceiptInvoiceLoop['rcp_inv_cur_id'], $getReceiptInvoiceLoop['invoice_date']);
                                    $invoiceValue2 = round($getReceiptInvoiceLoop['rcp_inv_amount'] * $getReceiptInvoiceRate['cr_exchange_rate'], 2);
                                }else{
                                    $invoiceValue2 = $getReceiptInvoiceLoop['rcp_inv_amount'];
                                }

                                $invoiceValue = ($invoiceValue+$invoiceValue2);
                            }
                        }

                        if ($row['cur_id'] != 1) {
                            $getInvoiceRate = $curRateDB->getRateByDate($row['cur_id'], $row['record_date']);
                            $receiptValue = round($row['credit'] * $getInvoiceRate['cr_exchange_rate'], 2);
                        }else{
                            $receiptValue = $row['credit'];
                        }

                        $checkGainLoss = $receiptValue-$invoiceValue;
                        $balance = $balance - $amountCredit;

                        ///hide on 05/08/2015 - do nothing for no
                        //unhide 12/08/2015
                        if ($checkGainLoss > 0) {
                            if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                                if ($row['subject'] == 'gain') {
                                    $balance = $balance + $row['fc_seq'];
                                    $amountCredit = $amountCredit - $row['fc_seq'];
                                } elseif ($row['subject'] == 'loss') {//loss
                                    $balance = $balance - $row['fc_seq'];
                                    $amountCredit = $amountCredit + $row['fc_seq'];
                                }
                            }
                        }

                    } else
                        if ($row['txn_type'] == "Credit Note") {
                            $balance = $balance - $amountCredit;
                        } else
                            if ($row['txn_type'] == "Discount") {
                                $balance = $balance - $amountCredit;
                            } else
                                if ($row['txn_type'] == "Debit Note") {
                                    $balance = $balance + $row['debit'];
                                } else
                                    if ($row['txn_type'] == "Refund") {
                                        $balance = $balance + $amountDebit;
                                    } else
                                        if ($row['txn_type'] == "Advance Payment") {

                                            $balance = $balance - $amountCredit;

                                        } else
                                            if ($row['txn_type'] == "Refund Adjustment") {

                                                $balance = $balance - $amountCredit;

                                            } else
                                                if ($row['txn_type'] == "Discount Adjustment") {

                                                    $balance = $balance + $amountDebit;

                                                } else
                                                    if ($row['txn_type'] == "Payment Adjustment") {

                                                        $balance = $balance + $amountDebit;

                                                    } else
                                                        if ($row['txn_type'] == "Sponsor Payment") {

                                                            $balance = $balance - $amountCredit;

                                                        } else
                                                            if ($row['txn_type'] == "Sponsor Payment Adjustment") {

                                                                $balance = $balance + $amountDebit;

                                                            }
                $amountCurr['cur_id'] = $row['cur_id'];
                $amountCurr['debit'] = $amountDebit;
                $amountCurr['credit'] = $amountCredit;
                $amountCurr['balance'] = number_format($balance, 2, '.', ',');


            }

        }
//echo "<pre>";
//			print_r($results);
        /*$curAvailable = array('2'=>'USD','1'=>'MYR');

            $this->view->availableCurrency= $curAvailable;*/
        return $amountCurr;
    }
}