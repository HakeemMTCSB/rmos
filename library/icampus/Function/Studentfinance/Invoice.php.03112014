<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 * 
 * Dependency class
 * 	GeneralSetup_Model_DbTable_Intake
 * 	GeneralSetup_Model_DbTable_Semestermaster
 * 	Studentfinance_Model_DbTable_FeeStructure
 * 	Studentfinance_Model_DbTable_FeeStructureItem
 *  Studentfinance_Model_DbTable_FeeStructureProgram
 * 	Studentfinance_Model_DbTable_FeeCategory
 * 	Studentfinance_Model_DbTable_FeeStructureItemSemester
 * 	Studentfinance_Model_DbTable_Currency
 * 	Studentfinance_Model_DbTable_CurrencyRate
 * 	Studentfinance_Model_DbTable_ProformaInvoiceMain
 * 	Studentfinance_Model_DbTable_ProformaInvoiceDetail
 * 	App_Model_Record_DbTable_StudentRegistration
 * 
 */
class icampus_Function_Studentfinance_Invoice{
	
	/*
	 * Generate proforma invoice for applicant who is offered
	 */
	public function generateApplicantProformaInvoice($txnId=null, $level=0, $semester_id=null, $invoice_desc=null) {
		
		//txnData
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
		->from(array('a' => 'applicant_profile'))
		->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
		->join(array('c' => 'applicant_program'), 'c.ap_at_trans_id = b.at_trans_id')
		->where('b.at_trans_id = ?', $txnId);
			
		$txnData = $db->fetchRow($select);
		
		if($txnData==null){
			throw new Exception('No such applicant');
		}
		
		//semester info
		$semester = null;
		if($semester_id){
			$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDb->getData($semester_id);
			
			if($semester==null){
				throw new Exception('No such semester');
			}
		}

		
		//get fee structure and set if not set yet
		$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
		if($txnData['at_fs_id']==null || $txnData['at_fs_id']==0 ){
			$feeStructureData = $feeStructureDb->getApplicantFeeStructure($txnData['ap_prog_id'], $txnData['ap_prog_scheme'], $txnData['at_intake'], $txnData['appl_category']);
		
			$txnData['at_fs_id'] = $feeStructureData['fs_id'];
		
			//update applicnt program
			$db->update('applicant_transaction', array('at_fs_id'=>$feeStructureData['fs_id']), 'at_trans_id = '.$txnId);
		}else{
			$feeStructureData = $feeStructureDb->fetchRow('fs_id = '.$txnData['at_fs_id'])->toArray();
		}
		
		
		//fees to charge
		$fees = $this->getApplicantSemesterFeeInfo($txnId, $semester_id, $level);
		if($fees==null){
			throw new Exception('No fee to charge data');
		}
		
		//fee categorized by group
		$arr_fee_by_group = array();
		foreach ($fees as $fee){
			$arr_fee_by_group[$fee['fi_fc_id']][] = $fee;
		}

		//loop fees by category and issue based on same category
		$feeGroupDb = new Studentfinance_Model_DbTable_FeeCategory();
		
		foreach($arr_fee_by_group as $fee_category_id=>$fee_group){
			
			
			//fee group info
			$feeGroupData = $feeGroupDb->fetchRow('fc_id = '.$fee_category_id)->toArray();
			
			$amount_paid = 0;
			$total_invoice_amount = 0;
			$total_invoice_amount_default_currency = 0;
			
			foreach ($fee_group as $fee){
				$total_invoice_amount += $fee['amount'];
			}
			
			
			
			//fee structure currency
			$currencyDb = new Studentfinance_Model_DbTable_Currency();
			$currency = $currencyDb->fetchRow('cur_id = '.$feeStructureData['fs_cur_id'])->toArray();
			
			
			
			if($currency['cur_default'] == 'N'){
				//get exchange rate
				$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
				$rate = $currencyRateDb->getCurrentExchangeRate($currency['cur_id']);
				
				$total_invoice_amount_default_currency = $total_invoice_amount * $rate['cr_exchange_rate'];
			}else{
				$total_invoice_amount_default_currency = $total_invoice_amount;
			}
			//debug($currency);
			
			
			$proformaInvoiceMainDb 		= new Studentfinance_Model_DbTable_ProformaInvoiceMain();
			$proformaInvoiceDetailDb 	= new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
			
			if($level==0){
				
				//check already issue or not
				if( $txnData['at_processing_fee']==null ){
					
					if($invoice_desc==null && $level==0){
						$invoice_desc = $feeGroupData['fc_desc'];
					}
					
					if($invoice_desc==null){
						$invoice_desc = $feeGroupData['fc_desc'];
					}
					
					$bill_no =  $this->getBillSeq(1, date('Y'));
					
					
					//add main
					$data = array(
							'bill_number' => $bill_no,
							'appl_id' => $txnData['appl_id'],
							'trans_id' => $txnData['at_trans_id'],
							//'academic_year' => $semester['AcademicYear'],
							//'semester' => $semester_id,
							'bill_amount' => $total_invoice_amount,
							'bill_description' => $invoice_desc,
							'program_id' => $txnData['ap_prog_id'],
							'creator' => '-1',
							'fs_id' => $txnData['at_fs_id'],
							'status' => 'A',
							'currency_id' => $feeStructureData['fs_cur_id'],
							'bill_amount_default_currency' => $total_invoice_amount_default_currency,
							'invoice_type' => 'PROCESSING',
							'fee_category' => $fee_category_id
					);
					
					$main_id = $proformaInvoiceMainDb->insert($data);
					
					
					//add detail
					foreach ($fee_group as $fee){
						
						$data_detail = array(
											'proforma_invoice_main_id' => $main_id,
											'fi_id' => $fee['fi_id'],
											'fee_item_description' => $fee['fi_name'],
											'cur_id' => $fee['fsi_cur_id'],
											'amount' => $fee['amount'],
											'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($fee['amount'],$fee['fsi_cur_id'])
										);
						
						$proformaInvoiceDetailDb->insert($data_detail);
					}
					
					//update processing fee info to txn table
					$txnUpdData = array('at_processing_fee' => 'ISSUED');
					$n = $db->update('applicant_transaction', $txnUpdData, 'at_trans_id = '.$txnId);
					
					
				}else{
					throw new Exception('proforma already issued for this applicant');
				}
				
			}else{
				//TODO: check if already issue proforma invoice
				
				if( $txnData['at_tution_fee']==null ){
				
					if($invoice_desc==null && $level==1){
						$invoice_desc = $feeGroupData['fc_desc'];
					}
				
					if($invoice_desc==null){
						$invoice_desc = $feeGroupData['fc_desc'];
					}
				
					$bill_no =  $this->getBillSeq(1, date('Y'));
				
				
					//add main
					$data = array(
							'bill_number' => $bill_no,
							'appl_id' => $txnData['appl_id'],
							'trans_id' => $txnData['at_trans_id'],
							'semester' => $semester_id,
							'bill_amount' => $total_invoice_amount,
							'bill_description' => $invoice_desc,
							'program_id' => $txnData['ap_prog_id'],
							'creator' => '-1',
							'fs_id' => $txnData['at_fs_id'],
							'status' => 'A',
							'currency_id' => $feeStructureData['fs_cur_id'],
							'bill_amount_default_currency' => $total_invoice_amount_default_currency,
							'invoice_type' => 'TUTION_FEE',
							'fee_category' => $fee_category_id
					);
					
					if($semester){
						$data['academic_year'] = $semester['AcademicYear'];
					}
				
					$main_id = $proformaInvoiceMainDb->insert($data);
				
				
					//add detail
					$proformaInvoiceSubjectDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();
					foreach ($fee_group as $fee){
						$data_detail = array(
								'proforma_invoice_main_id' => $main_id,
								'fi_id' => $fee['fi_id'],
								'fee_item_description' => $fee['fi_name'],
								'cur_id' => $fee['fsi_cur_id'],
								'amount' => $fee['amount'],
								'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($fee['amount'],$fee['fsi_cur_id'])
						);
							
						$invoice_detail_id = $proformaInvoiceDetailDb->insert($data_detail);
						
						//add proforma invoice subject if any
						if( isset($fee['subject']) ){
							foreach ($fee['subject'] as $subject){
								$data_psub = array(
									'proforma_invoice_main_id' => $main_id,
									'proforma_invoice_detail_id' => $invoice_detail_id,
									'subject_id' => $subject['IdSubject'],
									'cur_id' => $fee['fsi_cur_id'],
									'amount' =>$subject['amount'],
									'amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($subject['amount'], $fee['fsi_cur_id'])
								);
								
								$proformaInvoiceSubjectDb->insert($data_psub);
							}
						}
					}
					
					//update tution fee info to txn table
					$txnUpdData = array('at_tution_fee' => 'ISSUED');
					$n = $db->update('applicant_transaction', $txnUpdData, 'at_trans_id = '.$txnId);
				
					
					
				
				}else{
					throw new Exception('proforma already issued for this applicant');
				}
				
			}
		}
		
		return true;
	}
	
	public function getApplicantSemesterFeeInfo($txnId=null, $semester_id=null, $semlevel=0){
	
		if($txnId==null){
			throw new Exception('No Transaction ID');
		}
	
		if( (int)$semlevel > 1  && $semester_id==null){
			throw new Exception('No Semester ID');
		}
	
		//get applicant & txn info
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db->select()
		->from(array('a' => 'applicant_profile'))
		->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
		->where('b.at_trans_id = ?', $txnId);
			
		$txnData = $db->fetchRow($select);
	
		if($txnData==null){
			throw new Exception('No applicant/student data');
		}
		
	
		//get applicant program
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db->select()
		->from(array('a' => 'applicant_program'))
		->join(array('b' => 'tbl_program'), 'b.IdProgram = a.ap_prog_id')
		->where('a.ap_at_trans_id = ?', $txnId);
			
		$programData = $db->fetchRow($select);
		
		//semester data
		if($semester_id){
			$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDb->getData($semester_id);
		}else{
			$semester = null;
		}
	
		if($txnData==null){
			throw new Exception('No program data');
		}
		
		
		//first sem credit hour
		$ch_bil_subject = 0;
		if($semlevel==1){
			
			$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
			$fsProgramInfo = $feeStructureProgramDb->getStructureData($txnData['at_fs_id'],$programData['IdProgram'],$programData['ap_prog_scheme']);
			
			
			//check if setting for default 1st sem credithour / bil subject
			if( isset($fsProgramInfo) && isset($fsProgramInfo['fsp_first_sem_sks']) && $fsProgramInfo['fsp_first_sem_sks'] != 0 ){
				
				$ch_bil_subject = $fsProgramInfo['fsp_first_sem_sks'];
				
			}else{
				//landscape
				$landscape_id = icampus_Function_Application_Landscape::getApplicantLandscapeId($txnId);
				
				//get subject
				$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
				$compulsory_course = $landscapeSubjectDb->getCommonCourse($programData['IdProgram'],$landscape_id,1);
				//$elective_course = $landscapeSubjectDb->getCommonCourse($programData['IdProgram'],$landscape_id,2);
					
				//$arr_subject = array_merge($compulsory_course, $elective_course);
				$arr_subject = $compulsory_course;
				
				//sum CH or bil subject
				for($i=0; $i<sizeof($arr_subject); $i++){
					$ch_bil_subject += $arr_subject[$i]['CreditHours'];
				}
			}
			
		}else{
			//TODO:get subject registered in particular semester
			
			$arr_subject = array();
		}
	
	
			
		//get fee structure and set if not set yet
		if($txnData['at_fs_id']==null || $txnData['at_fs_id']==0 ){
			$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
			$feeStructureData = $feeStructureDb->getApplicantFeeStructure($programData['ap_prog_id'], $programData['ap_prog_scheme'],$this->getFeeStructureCategory($txnId));
				
			$txnData['at_fs_id'] = $feeStructureData['fs_id'];
				
			//update applicnt program
			$db->update('applicant_transaction', array('at_fs_id'=>$feeStructureData['fs_id']), 'at_trans_id = '.$txnId);
		}
	
	
		/*
		 * FEE STRUCTURE CALCULATION
		*/
	
		$fee_item_to_charge = array();
	
		//fee structure detail
		$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
		$fee_item_list = $feeStructureItemDb->getStructureData($txnData['at_fs_id']);
	
		//do looping to get what fee item to charge
		for($i=0; $i<sizeof($fee_item_list); $i++){
	
			$fee_item = $fee_item_list[$i];
				
			//application fees (sem 0)
			if( $semlevel == 0 ){
	
				if( $fee_item['fi_frequency_mode'] == 630){
					$fee_item_to_charge[] = $fee_item;
				}
	
			}else{
	
				//every sem
				if( $fee_item['fi_frequency_mode'] == 303 ){
					$fee_item_to_charge[] = $fee_item;
				}
	
				//every year
				if( $fee_item['fi_frequency_mode'] == 304 ){
						
					//get intake to get complete Anniversary
					$intakeDb = new GeneralSetup_Model_DbTable_Intake();
					$intake = $intakeDb->fetchRow('IdIntake = '.$txnData['at_intake'])->toArray();

					if( $intake['sem_seq'] == $semester['sem_seq'] || $semlevel == 1 ){
						$fee_item_to_charge[] = $fee_item;
					}
						
				}
	
				//defined semester
				if( $fee_item['fi_frequency_mode'] == 305  ){
						
					//get sem defined
					$feeSemesterDb = new Studentfinance_Model_DbTable_FeeStructureItemSemester();
					if($feeSemesterDb->getStructureItemData($fee_item['fsi_id'],$semlevel)){
						$fee_item_to_charge[] = $fee_item;
					}
	
				}
				
				//TODO: defined part
	
			}
		}
				
		//do looping to calculate charges from fee item to charge
		foreach($fee_item_to_charge as $key=>&$fee_item){
				
				
			//fix amount
			if($fee_item['fi_amount_calculation_type'] == 300 ){
				$fee_item['amount'] = $fee_item['fsi_amount'];
			}
				
			//fix amount selected subject
			if($fee_item['fi_amount_calculation_type'] == 459 ){
				//TODO: check sem registered subject with fs subject
				//Not applicable for 1st level
				
			}
				
			//fix amount by country origin
			if($fee_item['fi_amount_calculation_type'] == 618 ){
				//get origin fees for particular fee item
				$feeItemCountryDb = new Studentfinance_Model_DbTable_FeeItemCountry();
	
				$countryfee = $feeItemCountryDb->getFeeItemByCountry($fee_item['fi_id'],$txnData['appl_nationality']);
	
				if($countryfee){
					$fee_item['amount'] = $countryfee['fic_amount'];
				}else{
					$fee_item['amount'] = 0;
				}
			}
				
			//credit hour multiplication
			if($fee_item['fi_amount_calculation_type'] == 299 ){
				
				$fee_item['amount'] = $ch_bil_subject * $fee_item['fsi_amount'] ;
			}
				
			//credit hour multiplication (level)
			if($fee_item['fi_amount_calculation_type'] == 632 ){
				//TODO: refer landscape for part
					
				unset($fee_item_to_charge[$key]);
			}
				
			//subject multiplication
			if( $fee_item['fi_amount_calculation_type'] == 301 ){
				$fee_item['amount'] = $ch_bil_subject * $fee_item['fsi_amount'] ;
				
				//insert if having subject in landscape
				if(isset($arr_subject)){
					$subject_list = $arr_subject;
					foreach ($subject_list as $index=>&$subject){
						$subject['amount'] = $fee_item['fsi_amount'];
					}
					
					$fee_item['subject'] = $subject_list;
				}
			}
				
			//subject multiplication (level)
			if( $fee_item['fi_amount_calculation_type'] == 633 ){
				//TODO: refer landscape for part
				
				unset($fee_item_to_charge[$key]);
			}
				
		}
					
		return array_values($fee_item_to_charge);
	}
	
	
	/*
	 * Function to get applicant proforma invoice
	 */
	public function getApplicantProformaInfo($txnId, $level=0, $fee_category_id=null){
		
		
		if($level==0){
			$invoice_type='PROCESSING';
		}else
		if($level==1){
			$invoice_type='TUTION_FEE';
		}else{
			throw new Exception('No applicant invoice for level more than 1');
		}
		
		
		$result = array();
		
		//get applicant & txn info
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					->from(array('a' => 'applicant_profile'))
					->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
					->where('b.at_trans_id = ?', $txnId);
			
		$txnData = $db->fetchRow($select);
		
		
		if($txnData==null){
			throw new Exception('No applicant/student data');
		}
		
		$result['profile'] = $txnData;
		
		//get proforma invoice Main
		$select = $db->select()
					->from(array('a' => 'proforma_invoice_main'))
					->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id', array())
					->joinLeft(array('c' => 'tbl_fee_category'), 'c.fc_id = a.fee_category')
					->where('b.at_trans_id = ?', $txnId)
					->where("a.invoice_type = '".$invoice_type."'");
		
		if($fee_category_id!=null){
			$select->where('a.fee_category = '.(int)$fee_category_id);
		}
			
		$proforma_invoice = $db->fetchAll($select);
		
		//get proforma invoice detail
		for($i=0; $i<sizeof($proforma_invoice); $i++){
			
			$proforma_invoice_detail = null;
			
			$select_detail = $db->select()
								->from(array('a' => 'proforma_invoice_detail'))
								->where('a.proforma_invoice_main_id = ?', $proforma_invoice[$i]['id']);
			
			$proforma_invoice_detail = $db->fetchAll($select_detail);
			
			if($proforma_invoice_detail){
				$proforma_invoice[$i]['detail'] = $proforma_invoice_detail;
			}
		}
		
		$result['proforma_invoice_info'] = $proforma_invoice;
		
		//calculate total amount
		if($proforma_invoice){
			
			$result['proforma_invoice']['total_amount'] = 0;
			$result['proforma_invoice']['total_amount_default_currency'] = 0;
			
			foreach ($proforma_invoice as $pinvoice){
				$result['proforma_invoice']['total_amount'] += $pinvoice['bill_amount'];
				$result['proforma_invoice']['total_amount_default_currency'] += $pinvoice['bill_amount_default_currency'];
			}
			
			//inject currency info (taken from 1st invoice)
			$currencyDb = new Studentfinance_Model_DbTable_Currency();
			$currency = $currencyDb->fetchRow('cur_id = '.$proforma_invoice[0]['currency_id'])->toArray();
			
			$result['proforma_invoice']['currency'] = $currency;
			
		}else{
			$result['proforma_invoice']['total_amount'] = 0;
		}
		
		return $result;
		
	}
	
	/*
	 * Generate invoice form proforma invoice
	*/
	public function generateInvoiceFromProforma($proforma_invoice_id, $creator_id = null){
		
		if($proforma_invoice_id==null){
			throw new Exception('No proforma invoice ID');
		}
		
		//get proforma data
		$proformaInvoiceMainDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
		$proforma_invoice = $proformaInvoiceMainDb->fetchRow(array('id = ?'=>$proforma_invoice_id));
		
		if($proforma_invoice==null){
			throw new Exception('Unknown proforma invoice');
		}else{
			$proforma_invoice = $proforma_invoice->toArray();
			
			//proforma invoice detail
			$proformaInvoiceDetailDb = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
			$proforma_invoice_detail = $proformaInvoiceDetailDb->fetchAll(array('proforma_invoice_main_id = ?'=>$proforma_invoice['id']));
			
			if($proforma_invoice_detail){
				$invoice_detail = $proforma_invoice_detail->toArray();
				
				//add invoice subject if any
				$proformaInvoiceSubjectDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();
				foreach ($invoice_detail as $index=>&$fee_item){
					
					$where = array(
						'proforma_invoice_main_id = ?'=> $proforma_invoice['id'],
						'proforma_invoice_detail_id = ?' => $fee_item['id']
					);
					if( $invoiceSubject = $proformaInvoiceSubjectDb->fetchAll($where) ){
						$fee_item['subject'] = $invoiceSubject->toArray();
					}
				}
				
				
			 	$proforma_invoice['fee_item'] = $invoice_detail;
			}else{
				$proforma_invoice['fee_item'] = null;
			} 
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			try{
				
				//insert invoice main
				$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
				
				$bill_no =  $this->getBillSeq(2, date('Y'));
				$data_invoice = array(
					'bill_number' => $bill_no,
					'appl_id' => $proforma_invoice['appl_id'],
					'trans_id' => $proforma_invoice['trans_id'],
					'IdStudentRegistration' => $proforma_invoice['IdStudentRegistration'],
					'academic_year' => $proforma_invoice['academic_year'],
					'semester' => $proforma_invoice['semester'],
					'bill_amount' => $proforma_invoice['bill_amount'],
					'bill_amount_default_currency' => $proforma_invoice['bill_amount_default_currency'],
					'bill_paid' => 0,
					'bill_balance' => $proforma_invoice['bill_amount'],
					'bill_description' => $proforma_invoice['bill_description'],
					'program_id' => $proforma_invoice['program_id'],
					'fs_id' => $proforma_invoice['fs_id'],
					'currency_id' => $proforma_invoice['currency_id'],
					'proforma_invoice_id' => $proforma_invoice['id'],
				);
				
				if($creator_id != null){
					$data_invoice['creator'] = $creator_id;
				}
				
				$invoice_id = $invoiceMainDb->insert($data_invoice);
				
				//insert invoice detail
				$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
				
				for($i=0; $i<sizeof($proforma_invoice['fee_item']); $i++){
					$fee_item = $proforma_invoice['fee_item'][$i];
					
					unset($fee_item['id']);
					$fee_item['invoice_main_id'] = $invoice_id;
					unset($fee_item['proforma_invoice_main_id']);
					
					$subject_list = null;
					if(isset($fee_item['subject'])){
						$subject_list = $fee_item['subject'];
						unset($fee_item['subject']);
					}
					
					$invoice_detail_id = $invoiceDetailDb->insert($fee_item);
					
					//insert invoice subject if any
					if($subject_list){
						$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
						foreach ($subject_list as &$subject){
							unset($subject['proforma_invoice_main_id']);
							unset($subject['proforma_invoice_detail_id']);
							$subject['invoice_main_id'] = $invoice_id;
							$subject['invoice_detail_id'] = $invoice_detail_id;
							
							$invoiceSubjectDb->insert($subject);
						}
					}
				}
				
				
				
				
				$db->commit();
				
			}catch (Exception $e){
        		$db->rollBack();
        		throw $e;
			}
			
		}
				
		return true;
	}
	
	
	/*
	 * Generate proforma invoice for student
	*/
	public function generateStudentProformaInvoice(){
		//TODO: generate invoice for student
	}
	
	
	/*
	 * Get Fee structure info by semester (applicant)
	*/
	
	
	/*
	 * Get Fee structure info by semester (student)
	*/
	public function getStudentSemesterFeeInfo($idStudentRegistration=null, $semester_id, $isStudent = true){
	
		if($idStudentRegistration==null){
			throw new Exception('No Transaction ID');
		}
	
		if($semester_id==null){
			throw new Exception('No Semester ID');
		}
	
		//get student info
		
	
		//get program info
		
	
		//semester data
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$semester = $semesterDb->getData($semester_id);
	
		if($txnData==null){
			throw new Exception('No program data');
		}
	
	
		$semlevel = 0;
		//student process
		if($isStudent){
			//student current level
			$studentRegistrationDb = new App_Model_Record_DbTable_StudentRegistration();
			$semlevel = $studentRegistrationDb->getStudentLevelByProgram($txnData['at_intake'],$programData['ap_prog_id']);
				
			//get registered subject
			$registered_course_list = $studentRegistrationDb->getCourseRegisteredBySemester();
				
		}
	
	
	
			
		//get fee structure and set if not set yet
		if($txnData['at_fs_id']==null || $txnData['at_fs_id']==0 ){
			$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
			$feeStructureData = $feeStructureDb->getApplicantFeeStructure($programData['ap_prog_id'], $programData['ap_prog_scheme'],$this->getFeeStructureCategory($txnId));
				
			$txnData['at_fs_id'] = $feeStructureData['fs_id'];
				
			//update applicnt program
			$db->update('applicant_transaction', array('at_fs_id'=>$feeStructureData['fs_id']), 'at_trans_id = '.$txnId);
		}
	
	
	
		/*
		 * FEE STRUCTURE CALCULATION
		*/
	
		$fee_item_to_charge = array();
	
		//fee structure detail
		$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
		$fee_item_list = $feeStructureItemDb->getStructureData($txnData['at_fs_id']);
		//debug($fee_item_list);
	
		//do looping to get what fee item to charge
		for($i=0; $i<sizeof($fee_item_list); $i++){
				
			$fee_item = $fee_item_list[$i];
				
			//application fees (sem 0)
			if( $semlevel == 0 ){
	
				if( $fee_item['fi_frequency_mode'] == 630){
					$fee_item_to_charge[] = $fee_item;
				}
	
			}else{
					
				//sem 1 only
				if( $fee_item['fi_frequency_mode'] == 304  && $semlevel == 1 ){
					$fee_item_to_charge[] = $fee_item;
				}
	
				//every sem
				if( $fee_item['fi_frequency_mode'] == 303 ){
					$fee_item_to_charge[] = $fee_item;
				}
	
				//every year
				if( $fee_item['fi_frequency_mode'] == 304 ){
						
					//get intake to get complete Anniversary
					$intakeDb = new GeneralSetup_Model_DbTable_Intake();
					$intake = $intakeDb->fetchRow('IdIntake = '.$txnData['at_intake']);
						
					if( $intake['sem_seq'] == $semester['sem_seq'] ){
						$fee_item_to_charge[] = $fee_item;
					}
						
	
				}
	
				//defined semester
				if( $fee_item['fi_frequency_mode'] == 305  ){
						
					//get sem defined
					$feeSemesterDb = new Studentfinance_Model_DbTable_FeeStructureItemSemester();
					if($feeSemesterDb->getStructureItemData($fee_item['fsi_id'],$semlevel)){
						$fee_item_to_charge[] = $fee_item;
					}
	
				}
	
			}
		}
	
		//do looping to calculate charges from fee item to charge
		for($i=0; $i<sizeof($fee_item_to_charge); $i++){
				
			$fee_item = &$fee_item_to_charge[$i];
				
				
			//fix amount
			if($fee_item['fi_amount_calculation_type'] == 300 ){
				$fee_item['amount'] = $fee_item['fsi_amount'];
			}
				
			//fix amount selected subject
			if($fee_item['fi_amount_calculation_type'] == 459 ){
	
			}
				
			//fix amount by country origin
			if($fee_item['fi_amount_calculation_type'] == 618 ){
					
			}
				
			//credit hour multiplication
			if($fee_item['fi_amount_calculation_type'] == 299 ){
					
			}
				
			//credit hour multiplication (level)
			if($fee_item['fi_amount_calculation_type'] == 632 ){
					
			}
				
			//subject multiplication
			if($fee_item['fi_amount_calculation_type'] == 301 ){
					
			}
				
			//subject multiplication (level)
			if($fee_item['fi_amount_calculation_type'] == 633 ){
					
			}
				
		}
	
	
	
		dd($fee_item_to_charge);
	
		exit;
	}
	
	
	
	
	
	
	/*
	 * function to return true if txn profile's nationality is Indonesia
	*/
	private function getFeeStructureCategory($txn_id){
		
		//get profile
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('at'=>'applicant_transaction'))
					->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
					->where("at_trans_id = ".$txn_id);
	
		$row = $db->fetchRow($select);
	
		return $row['appl_category'];
	}
	
	/*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
}