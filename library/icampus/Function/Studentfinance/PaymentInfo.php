<?php 
class icampus_Function_Studentfinance_PaymentInfo{
 
  /*
   * Get payment status and invoice detail
   */
  public function getStudentPaymentInfo($idRegistration,$semesterMainId=null, $academicYearId=null){
    
    //get student profile info
    $studentProfileDb = new Records_Model_DbTable_Studentprofile();
    $profile = $studentProfileDb->fnGetStudentProfileByIdStudentRegistration($idRegistration);
    
    
    //get invoice 
    $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
    
    
    $condition = array('appl_id =?' => $profile['appl_id']);
    
    //specific semester
    if($semesterMainId){
      $condition['semester = ?'] = $semesterMainId;
    }
    
    //specific academic year
    if($academicYearId){
      $condition['academic_year = ?'] = $academicYearId;
    }
    
    
    $invoices = $invoiceMainDb->fetchAll($condition)->toArray();
    
    
    //pack invoice into array
    $result['idStudentRegistration'] = $idRegistration;
    $result['nim'] = $profile['registrationId'];
    
    $result['fullname'] = "";
    $profile['appl_fname']!=""?$result['fullname'] .= $profile['appl_fname']." ":"";
    $profile['appl_mname']!=""?$result['fullname'] .= $profile['appl_mname']." ":"";
    $profile['appl_lname']!=""?$result['fullname'] .= $profile['appl_lname']." ":"";
    
    if($semesterMainId){
      $result['semester_id'] = $semesterMainId;
    }
    if($academicYearId){
      $result['academic_year_id'] = $academicYearId;
    }
    
    $result['total_invoice_amount'] = 0;
    $result['total_invoice_paid'] = 0;
    $result['total_invoice_balance'] = 0;
    
    if($invoices){
      $result['invoices'] = $invoices;
    }else{
      $result['invoices'] = null;
    }
    
    
    
    foreach ($invoices as $invoice){
      $result['total_invoice_amount'] += $invoice['bill_amount'];
      $result['total_invoice_paid'] += $invoice['bill_paid'];
      $result['total_invoice_balance'] += $invoice['bill_balance'];
    }
    
    //get exception
    if($semesterMainId){
    
      $db = Zend_Db_Table::getDefaultAdapter();
      $selectData = $db->select()
      ->from(array('pe'=>'payment_exception'))
      ->where("pe.idsemester = ?", (int)$semesterMainId)
      ->where("pe.idreg = ?", $profile['registrationId']);
    
      $row_ex = $db->fetchAll($selectData);
      $result['exception'] = array();
      foreach ($row_ex as $exp){
        $result['exception'][$exp['extype']] = true;
      }
    }
    
    return $result;
    
  } 
  
  public function checkingApplicantDocumentChecklist($appTrxId){
		/*
		 * checking applicant document exist or not
		 * if not exist -> insert all
		 */
		$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
		$fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
		$checkDocExist = $fsProgramDB->getApplicantDocumentList($appTrxId);
		$totalDocExist = count($checkDocExist);
		
		$appTransDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$appDataProfile = $appTransDB->getProfileProgram($appTrxId);
		
		$programSchemeId = $appDataProfile['ap_prog_scheme'];
		$stdCtgy = $appDataProfile['appl_category'];
		
		if($totalDocExist == 0){ 
			
			//get applicant document status
			
			$docChecklist = $checklistVerificationDB->fnGetDocCheckList($programSchemeId, $stdCtgy, $appTrxId);
			
			foreach($docChecklist as $dataDoc){
				
				$list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$appTrxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
				

				if($list){
					foreach($list as $listLoop){
						$docStatus = array(
	                                'ads_txn_id'=>$appTrxId,
	                                'ads_dcl_id'=>$dataDoc['dcl_Id'],
	                                'ads_ad_id'=>0,
	                                'ads_section_id'=>$dataDoc['dcl_sectionid'],
	                                'ads_table_name'=>$dataDoc['table_name'],
	                                'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
	                                'ads_status'=>0,
	                                'ads_comment'=>'',
	                                'ads_createBy'=>1,
	                                'ads_createDate'=>date('Y-m-d H:i:s')
	                            );
	                            
	                     $fsProgramDB->insert($docStatus); 

					}
				}
			}
		}
		
		/*end*/
   	
   }
   
	public function updateChecklist(&$arrayAds){
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
			
		$tmp = array();
		$n=0;
		foreach($arrayAds as $arg)
		{
		    $tmp[$arg['id']]['tot'][$n] = $arg['total'];
		    $tmp[$arg['id']]['bal'][$n] = $arg['balance'];
		    $tmp[$arg['id']]['cur'] = $arg['cur'];
		    $tmp[$arg['id']]['receipt_no'] = $arg['receipt_no'];
		    $n++;
		}
		
		$output = array();
		$m=0;
		foreach($tmp as $type => $labels)
		{
			 $output[$m] = array(
		        'id' => $type,
		        'total' => number_format(array_sum($labels['tot']),2),
		    	'balance' => number_format(array_sum($labels['bal']),2),
				 'cur'=>$labels['cur'],
				 'receipt'=>$labels['receipt_no']
			    );
		
			   $m++;
		}

		//update applicant document type checklist	
		foreach($output as $out){
			if($out['balance'] <= 0){
				$docStatus = array(
					'ads_status'=>4,//completed
					'ads_comment'=> $out['receipt'].", ".$out['cur']."".$out['total'] ,
					'ads_modBy'=>665,
					'ads_modDate'=>date('Y-m-d H:i:s'),
				);
				
//				echo "<pre>";
//				print_r($output);
				$appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
				$appDocStatusDB->update($docStatus, array('ads_id =?'=>$out['id']) );
			}
		}
	}
	
	
/*
		 * update applicant_transaction status
		 * only status COmplete (4) baru update
		 */
	public function updateStatusTransaction($appTrxId){	
		$defModelDB = new App_Model_General_DbTable_Definationms();
		$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
		
		$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData= $txnDb->fetchRow( array('at_trans_id = ?'=> $appTrxId) );
		
		$appStatus = $txnData['at_status'];
		
		$docList = $checklistVerificationDB->fnGetChecklistStatus($appTrxId);
	 	if (count($docList) > 0){
        	$i=0;
            foreach($docList as $docLoop){
               if(($docLoop['ads_status']==4 || $docLoop['ads_status']==5) && $docLoop['dcl_mandatory']==1){
                     $i++;
               }
            }
                    //var_dump(count($docList));var_dump($i); exit;
                    if (($appStatus == '595')){//($oldStatus == '592') || hide in 21/08/2015
//         	if($appStatus == 595 || $appStatus == 592){ //only update at_status transaction if status entry/incomplete
                    if (count($docList)==$i){
                        
                        $getStatus = $defModelDB->getIdByDefType('Status', 'Complete');
                        
                        $oldStatus = $checklistVerificationDB->universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                        $data = array(
                            'at_status'=>$getStatus['key']
                        );
                        $checklistVerificationDB->updateAppTrans($data, $appTrxId);
                        
                        $dataHistory = array(
                            'ash_trans_id'=>$appTrxId,
                            'ash_status'=>$data['at_status'],
                            'ash_oldStatus'=>$oldStatus,
                            'ash_changeType'=>1,
                            'ash_updUser'=>1,
                            'ash_userType'=>1,
                            'ash_updDate'=>date('Y-m-d H:i:s')
                        );
                        
                        $checklistVerificationDB->storeStatusUpdateHistory($dataHistory);
                    }
                }
	 	}
	}
	
	public function updateCourseStatus($invoiceID){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_invoice = $db->select()
							->from(array('ivs'=>'invoice_subject'))
							->join(array('im'=>'invoice_main'),'ivs.invoice_main_id=im.id',array('*'))
							->where('im.id = ?', $invoiceID)
							->where('im.bill_balance <= 0')
							->group('ivs.invoice_main_id');
		$row = $db->fetchAll($select_invoice);
		
		if($row){
			$txnID = $row[0]['IdStudentRegistration'];
			
			if($txnID){
				foreach($row as $data){
					$subjectID = $data['subject_id'];
					
					$typeApp =  $data['invoice_type'];
					
					$select_reg = $db->select()
									->from(array('a'=>'tbl_studentregsubjects'))
									->where('a.IdStudentRegistration = ?', $txnID)
									->where('a.IdSubject = ?', $subjectID)
									->order('a.UpdDate DESC')
									->limit('1')
									;
									
					$rowReg = $db->fetchRow($select_reg);
					
					if($rowReg){
						$idRegSubject = $rowReg['IdStudentRegSubjects'];
						
						/*case 0: $newval = 'Pre-Registered';	break;
						case 1: $newval = 'Registered';		break;
						case 2: $newval = 'Drop';			break;
						case 3: $newval = 'Withdraw';		break;
						case 4: $newval = 'Repeat';			break;
						case 5: $newval = 'Refer';			break;
						case 6:	$newval = 'Replace';		break;
						case 9: $newval = 'Pre-Repeat';		break;
						case 10:$newval = 'Pre-Replace';	break;*/
						
						$courseStatus = 0;
						if($rowReg['Active'] == 0){//pre-registered
							$courseStatus = 1;
						}elseif($rowReg['Active'] == 9){//pre-Repeat
							$courseStatus = 4; 
						}elseif($rowReg['Active'] == 10){//pre-Replace
							$courseStatus = 6;
						}else{
							$courseStatus = $rowReg['Active'] ;
						}
						
						$updData = array ('Active'=>$courseStatus);
						$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
					}
					
					if($typeApp == 'AU'){
						$select_audit = $db->select()
									->from(array('a'=>'tbl_auditpaperapplicationitem'))
									->join(array('d' => 'tbl_auditpaperapplicationcourse'), 'a.api_apc_id = d.apc_id',array('subject_id'=>'apc_courseid','apc_program','apc_programscheme'))
									->join(array('da' => 'tbl_auditpaperapplication'), 'da.apa_id = a.api_apa_id',array('created_date'=>'apa_upddate','apa_semesterid'))
									->where('da.apa_studregid = ?', $txnID)
									->where('d.apc_courseid = ?', $subjectID)
									->where('a.api_invid_0 = ?', $invoiceID)
									->orWhere('a.api_invid_1 = ?', $invoiceID);
		
						$rowAudit = $db->fetchRow($select_audit);
						if($invoiceID == $rowAudit['api_invid_0']){
							$updData = array ('api_paid_0'=>1);
							$db->update('tbl_auditpaperapplicationitem',$updData,array('api_id = ?' => $rowAudit['api_id']));
						}
						
						if($invoiceID == $rowAudit['api_invid_1']){
							$updData = array ('api_paid_1'=>1);
							$db->update('tbl_auditpaperapplicationitem',$updData,array('api_id = ?' => $rowAudit['api_id']));
							
							//update course registration
							$updData = array ('Active'=>1);//registered
							$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
						}
					
					}
					
					if($typeApp == 'CT' || $typeApp == 'EX'){
						$select_ct = $db->select()
									->from(array('a'=>'tbl_credittransfer'))
									->where('a.invoiceid_0 = ?', $invoiceID)
									->orWhere('a.invoiceid_1 = ?', $invoiceID);
						
						$rowCt = $db->fetchRow($select_ct);
						
						if($invoiceID == $rowAudit['invoiceid_0']){
							$updData = array ('paid_0'=>1);
							$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
						}elseif($invoiceID == $rowAudit['invoiceid_1']){
							$updData = array ('paid_1'=>1);
							$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
						}
					}
					
				}
			}
		}
	}
	
	public function updateCourseStatusCancel($invoiceID){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_invoice = $db->select()
							->from(array('ivs'=>'invoice_subject'))
							->join(array('im'=>'invoice_main'),'ivs.invoice_main_id=im.id',array('*'))
							->where('ivs.invoice_main_id = ?', $invoiceID)
							->group('ivs.invoice_main_id');
		$row = $db->fetchAll($select_invoice);
		$txnID = $row[0]['IdStudentRegistration'];
		
		
		
		foreach($row as $data){
			$subjectID = $data['subject_id'];
			
			$typeApp =  $data['invoice_type'];
			
			$select_reg = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->where('a.IdStudentRegistration = ?', $txnID)
							->where('a.IdSubject = ?', $subjectID)
							->order('a.UpdDate DESC')
							->limit('1')
							;
							
			
			$rowReg = $db->fetchRow($select_reg);
			
			$idRegSubject = $rowReg['IdStudentRegSubjects'];
			
			/*case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
			case 6:	$newval = 'Replace';		break;
			case 9: $newval = 'Pre-Repeat';		break;
			case 10:$newval = 'Pre-Replace';	break;*/
			
			$courseStatus = 0;
			if($rowReg['Active'] == 1){//pre-registered
				$courseStatus = 0;
			}elseif($rowReg['Active'] == 4){//pre-Repeat
				$courseStatus = 9; 
			}elseif($rowReg['Active'] == 6){//pre-Replace
				$courseStatus = 10;
			}else{
				$courseStatus = $rowReg['Active'] ;
			}
			
			$updData = array ('Active'=>$courseStatus);
			$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
			
			if($typeApp == 'AU'){
				$select_audit = $db->select()
							->from(array('a'=>'tbl_auditpaperapplication'))
							->where('a.apa_subjectreg_id = ?', $idRegSubject)
							->where('a.apa_invid_0 = ?', $invoiceID)
							->orWhere('a.apa_invid_1 = ?', $invoiceID);
				
				$rowAudit = $db->fetchRow($select_audit);
				
				if($invoiceID == $rowAudit['apa_invid_0']){
					$updData = array ('apa_paid_0'=>0);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}elseif($invoiceID == $rowAudit['apa_invid_1']){
					$updData = array ('apa_paid_1'=>0);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}
			
			}
			
			if($typeApp == 'CT' || $typeApp == 'EX'){
				$select_ct = $db->select()
							->from(array('a'=>'tbl_credittransfer'))
							->where('a.invoiceid_0 = ?', $invoiceID)
							->orWhere('a.invoiceid_1 = ?', $invoiceID);
				
				$rowCt = $db->fetchRow($select_ct);
				
				if($invoiceID == $rowAudit['invoiceid_0']){
					$updData = array ('paid_0'=>0);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}elseif($invoiceID == $rowAudit['invoiceid_1']){
					$updData = array ('paid_1'=>0);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}
			}
			
		}
	}
	public function updateChecklistDocument($id){//receipt_id
//		echo $id;
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoice_list = $invoiceMainDb->getInvoiceFromAdvance($id);
		
//		echo "<pre>";
//				print_r($invoice_list);
		foreach ($invoice_list as $invoice){
//				$iv = array(
//						'invoice_no' => $invoice['bill_number'],
//						'amount' => 0,
//						'currency' => $invoice['cur_symbol_prefix']
//					);
				$appTrxId = $invoice['advpy_trans_id'];
//				if($invoice['invoice']){
//					foreach ($invoice['invoice'] as $ri){
						
	//					$iv['amount'] += $ri['bill_amount'];
						$total_payment = $invoice['bill_paid'];
						$total_balance = $invoice['bill_balance'];
						
						$feeID = $invoice['fi_id'];
						$receipt_no = $invoice['rcp_no'];
						$bill_paid = $invoice['bill_amount'];
						$currencyPaid = $invoice['cur_symbol_prefix'];
						
						if($appTrxId){
						/*
						 * get fee structure program item
						 * update docuemnt checklist status
						 */
							$fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();			
							$fsProgram = $fsProgramDB->getApplicantDocumentType($appTrxId,$feeID);
							$newArray1 = array();	
							$ads_id = $fsProgram['ads_id'];
							if(isset($ads_id)){
								$arrayAds [] = array(
									'id'=>$ads_id,
									'total'	=>$bill_paid,
									'balance'=>$total_balance,
									'cur'=>$currencyPaid,
									'receipt_no'=>$receipt_no
								);
							}
							array_push($newArray1,$arrayAds);
						
							
//						echo "<pre>";
//				print_r($arrayAds);
					$this->updateChecklist($arrayAds);
					$this->updateChecklistApplicantStatus($appTrxId);
				
						}
//					}	
//				}
				
			}
			
	}
	
		public function updateChecklistApplicantStatus($appTrxId){//trans_id
/*
			 * update applicant_transaction status
			 * only status COmplete (4) baru update
			 */
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
			$txnProfile = $applicantProfileDb->getApplicantRegistrationDetail($appTrxId);
			
			$appStatus = $txnProfile['at_status'];
			
			
			$defModelDB = new App_Model_General_DbTable_Definationms();
				$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
				
			$docList = $checklistVerificationDB->fnGetChecklistStatus($appTrxId);
			if (count($docList) > 0){
                    $i=0;
                    foreach($docList as $docLoop){
                        if(($docLoop['ads_status']==4 || $docLoop['ads_status']==5) && $docLoop['dcl_mandatory']==1){
                            $i++;
                        }
                    }
                    //var_dump(count($docList));var_dump($i); exit;
                    if (count($docList)==$i){
                        
                        $getStatus = $defModelDB->getIdByDefType('Status', 'Complete');
                        
                        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                        if (($oldStatus == '595')){//($oldStatus == '592') || 
                        
                            $data = array(
                                'at_status'=>$getStatus['key']
                            );
                            $checklistVerificationDB->updateAppTrans($data, $appTrxId);

                            $dataHistory = array(
                                'ash_trans_id'=>$appTrxId,
                                'ash_status'=>$data['at_status'],
                                'ash_oldStatus'=>$oldStatus,
                                'ash_changeType'=>1,
                                'ash_updUser'=>$getUserIdentity->id,
                                'ash_userType'=>1,
                                'ash_updDate'=>date('Y-m-d H:i:s')
                            );

                            $checklistVerificationDB->storeStatusUpdateHistory($dataHistory);
                            $this->view->status = $getStatus['key'];
                        }
                    }else{
                        
                        $getStatus = $defModelDB->getIdByDefType('Status', 'Incomplete');
                        
                        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                        if (($oldStatus == '595')){//($oldStatus == '592') || 
                        
                            $data = array(
                                'at_status'=>$getStatus['key']
                            );
                            $checklistVerificationDB->updateAppTrans($data, $appTrxId);

                            $dataHistory = array(
                                'ash_trans_id'=>$appTrxId,
                                'ash_status'=>$data['at_status'],
                                'ash_oldStatus'=>$oldStatus,
                                'ash_changeType'=>1,
                                'ash_updUser'=>$getUserIdentity->id,
                                'ash_userType'=>1,
                                'ash_updDate'=>date('Y-m-d H:i:s')
                            );

                            $checklistVerificationDB->storeStatusUpdateHistory($dataHistory);
                            $this->view->status = $getStatus['key'];
                        }
                    }
                }
		}
}
?>