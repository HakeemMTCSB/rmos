<?php
class Cms_ExamMarkEntry
{
    public static function get($IdGroup, $IdProgram, $IdSemester, $IdSubject, $array = false)
    {
        $markentryinfo = new Examination_Model_DbTable_StudentMarkEntryInfo();
        $get = $markentryinfo->fetchRow(array(
            'id_group = ?'      => $IdGroup,
            'id_program = ?'    => $IdProgram,
            'id_semester = ?'   => $IdSemester,
            'id_subject = ?'    => $IdSubject
        ));

        if ( $array )
        {
            $get = !empty($get) ? $get->toArray() : $get;
        }

        return $get;
    }

    public static function update($IdGroup, $IdProgram, $IdSemester, $IdSubject)
    {
        $markentryinfo = new Examination_Model_DbTable_StudentMarkEntryInfo();
        $auth = Zend_Auth::getInstance()->getIdentity();

        $get = self::get($IdGroup, $IdProgram, $IdSemester, $IdSubject);

        if ( empty($get) )
        {
            $id = $markentryinfo->insert(
                                    array(
                                            'id_group'      => $IdGroup,
                                            'id_program'    => $IdProgram,
                                            'id_semester'   => $IdSemester,
                                            'id_subject'    => $IdSubject,
                                            'created_date'  => new Zend_Db_Expr('NOW()'),
                                            'created_by'    => $auth->iduser
                                    )
            );

            return $id;
        }
        else
        {
            $data = array(
                                    'updated_date'  => new Zend_Db_Expr('NOW()'),
                                    'updated_by'    => $auth->iduser
            );

            $markentryinfo->update($data, array('marks_entry_id = ?' => $get->marks_entry_id ));

            return $get->marks_entry_id;
        }
    }
}