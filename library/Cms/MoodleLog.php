<?php
class Cms_MoodleLog
{
	public static function add($logtype,$message='')
	{
		$logDb = new App_Model_MoodleLog();

		$data = array(
						'session_id'	=> Zend_Session::getId(),
						'title'			=> $logtype,
						'message'		=> $message,
						'created_by'	=> Zend_Auth::getInstance()->getIdentity()->iduser,
						'created_date' 	=> new Zend_Db_Expr('NOW()')
		);

		$logDb->insert($data);
	}
}