<?php
class Cms_Cache 
{
	public $param = array();
	public $get;
	
	protected function setDefault()
	{
		$this->param['lifetime'] = 3600;
		$this->param['automatic_serialization'] = true;
		$this->param['cache_dir'] = DOCUMENT_PATH.'/cache';
	}

	public function __construct($parameters = array())
	{
		$this->setDefault();

		foreach($parameters as $key => $value) 
		{
            $this->param[$key] = $value;
        }

		if ( !mkdir_p( $this->param['cache_dir'] ) )
		{
			throw new Exception('Cannot create cache folder: '.$this->param['cache_dir'] );
		}

		$frontend= array(
							'lifetime' => $this->param['lifetime'],
							'automatic_serialization' => $this->param['automatic_serialization']
		);

		$backend= array(
							'cache_dir' => $this->param['cache_dir'],
		);

		$cache = Zend_Cache::factory('core',
										'File',
										$frontend,
										$backend
		);
		
		$this->cache = $cache;
	}

	public function get()
	{
		return $this->cache;
	}

	public function clear()
	{
		$this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);
	}
}