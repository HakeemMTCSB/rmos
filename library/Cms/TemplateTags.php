<?php

class Cms_TemplateTags 
{
	/*
	 *  Function : Student Template Tags
	 */
	public function parseTag($sp_id, $tpl_id, $content)
    {
         
        $studentDB = new Records_Model_DbTable_Studentprofile();
		$student = $studentDB->getProfile($sp_id);

        $templateTags = $this->getTemplateTag($tpl_id);
                
        foreach ($templateTags as $tag)
        {
        	$tag_name = $tag['tag_name'];
        	
        	
	        if($tag_name=='Salutation') {
	        	 $content = str_replace("[".$tag['tag_name']."]", $student['Salutation'], $content); 
	        } 
	        else
        	if($tag_name=='Applicant Name') {
	        	$content = str_replace("[".$tag['tag_name']."]", $student['appl_fname'].' '.$student['appl_lname'], $content);
	        }
                if($tag_name=='Student Name') {
	        	$content = str_replace("[".$tag['tag_name']."]", $student['appl_fname'].' '.$student['appl_lname'], $content);
	        }
	        else
        	if($tag_name=='Student ID') {
	        	 $content = str_replace("[".$tag['tag_name']."]", $student['registrationId'], $content); 
	        }
        	else
        	if($tag_name=='Applicant ID') {
	        	 $content = str_replace("[".$tag['tag_name']."]", $student['registrationId'], $content); 
	        }
	        else
        	if($tag_name=='Program') {
	        	$content = str_replace("[".$tag['tag_name']."]", $student['program_name'], $content); 
	        } 
        	else
        	if($tag_name=='NRIC') {
	        	$content = str_replace("[".$tag['tag_name']."]", $student['appl_idnumber'], $content); 
	        }
	        else
	        if($tag_name=='Date'){
	        	 $content = str_replace("[".$tag['tag_name']."]", date("d F Y"), $content); 
	        } 
	        else
	        if($tag_name=='Applicant Address'){
	        	 //student info
                    $address = trim($student["appl_address1"]);
                    if($student["appl_address2"]){
                            $address.='<br/>'.$student["appl_address2"].' ';
                    }
                    
                    if($student["appl_address3"]){
                            $address.='<br/>'.$student["appl_address3"].' ';
                    }

                    if($student["appl_postcode"]){
                            $address.='<br/>'.$student["appl_postcode"].' ';
                    }

                    if($student["appl_city"]!=99){
                            $address.= '<br />'.$student["CityName"].' ';
                    }else{
                        $address.= '<br/ >'.$student["appl_city_others"].' ';
                    }

                    if($student["appl_state"]!=99){
                            $address.=$student["StateName"].' ';
                    }else{
                        $address.= $student["appl_state_others"];
                    }

                    $address.= '<br/>'.$student["CountryName"];
                    
                    $content = str_replace("[".$tag['tag_name']."]", strtoupper($address), $content);
	        }
        }

        return $content;
            	
    }
    
    
    
    /*
     * Function : Applicant Template Tags
     */
	public function parseContent($trans_id, $tpl_id, $content)
    {
		if ( $tpl_id == '' ) return $content;

    	$icampusClass = new icampus_Function_Studentfinance_Invoice();
    	$icampusCSEClass = new icampus_Function_Application_Cse();
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	
    	$appDB = new Application_Model_DbTable_ApplicantApproval();
        $templateTags = $appDB->getTemplateTag($tpl_id);
        
        $appProfileDB  = new Application_Model_DbTable_ApplicantProfile();
        $profile = $appProfileDB->getApplicantProfile($trans_id);
        //var_dump($profile); exit;
        //echo '<pre>';
        //print_r($templateTags);
       
        foreach ($templateTags as $tag)
        {
            switch($tag['tag_name'])
            {
               
                case "Applicant Name":
                        $content = str_replace("[".$tag['tag_name']."]", trim($profile['appl_fname'].' '.$profile['appl_lname']), $content);
                break;
                
                case "Applicant ID":                    
                    $content = str_replace("[".$tag['tag_name']."]", $profile['at_pes_id'], $content);
                break;
                                
                
                case "Date":
                    $content = str_replace("[".$tag['tag_name']."]", date("d F Y"), $content); 
                break;
                
                case "Program Status":
                   // $programSchemeCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = '.$trans_id);
                   // $mopCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_program', 'IdProgramScheme = '.$programSchemeCode);
                   // $mosCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_study', 'IdProgramScheme = '.$programSchemeCode);
                   // $ptCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'program_type', 'IdProgramScheme = '.$programSchemeCode);
                   // $mop = Application_Model_DbTable_ChecklistVerification::fnGetDefination($mopCode);
                   // $mos = Application_Model_DbTable_ChecklistVerification::fnGetDefination($mosCode);
                   // $pt = Application_Model_DbTable_ChecklistVerification::fnGetDefination($ptCode);
                    $content = str_replace("[".$tag['tag_name']."]", $profile['ProgramType']." ".$profile['StudyMode']." ".$profile['StudyMode'], $content);
                break;
                
                                       
                case "Study Period":
                    $progSchemeInfo = $appProfileDB->getProgramSchemeInfo($profile['ap_prog_scheme']);
                    if ($progSchemeInfo){
                        $studperiod_min = floor($progSchemeInfo['Duration']/12) . ' year'.(($progSchemeInfo['Duration']/12)==1?'':'s').' ' . ( $progSchemeInfo['Duration']%12 == 0 ? '' : $progSchemeInfo['Duration']%12 . ' months' );
                        $studperiod_max = floor($progSchemeInfo['OptimalDuration']/12) . ' year'.(($progSchemeInfo['OptimalDuration']/12)==1?'':'s').' ' . ( $progSchemeInfo['OptimalDuration']%12 == 0 ? '' : $progSchemeInfo['OptimalDuration']%12 . ' months' );
                        $studyPeriod = "Min. Duration : ".$studperiod_min.", Max. Duration : ".$studperiod_max;
                        $content = str_replace("[".$tag['tag_name']."]", $studyPeriod, $content);
                    }else{
                        $content = str_replace("[".$tag['tag_name']."]", 'No Program Scheme', $content);
                    }
                break;
                
                case 'Date of Attend':
                    $transInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('applicant_transaction', '*', 'at_trans_id = '.$trans_id);
                    $info = Application_Model_DbTable_ChecklistVerification::universalQueryRow('intake_registration_location', '*', 'IdIntake = '.$transInfo['at_intake']);
                    $dateofcomm = $this->getDateFromSemesterSetup($trans_id, 37);
                    $content = str_replace("[".$tag['tag_name']."]", isset($dateofcomm['StartDate']) ? date("d F Y", strtotime($dateofcomm['StartDate'])):'', $content);
                break;
                
                               
                case 'Time of Attend':
                    $transInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('applicant_transaction', '*', 'at_trans_id = '.$trans_id);
                    $info = Application_Model_DbTable_ChecklistVerification::universalQueryRow('intake_registration_location', '*', 'IdIntake = '.$transInfo['at_intake']);
                    $content = str_replace("[".$tag['tag_name']."]", $info['rl_time'], $content);
                break;
            
                case 'Venue':
                    $transInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('applicant_transaction', '*', 'at_trans_id = '.$trans_id);
                    $info = Application_Model_DbTable_ChecklistVerification::universalQueryRow('intake_registration_location', '*', 'IdIntake = '.$transInfo['at_intake']);
                    $content = str_replace("[".$tag['tag_name']."]", $info['rl_address'], $content);
                break;                            
                                
                case 'Intake':
                    $intakeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_intake', 'at_trans_id = '.$trans_id);
                    $intake = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_intake', 'IntakeDesc', 'IdIntake = '.$intakeId);
                    $content = str_replace("[".$tag['tag_name']."]", $intake, $content);
                break;
                
                case "Applicant Address":
                    
                	/*$city  = ($profile['appl_city']!=99) ? $profile['CityName']:$profile['appl_city_others'];
                	$state = ($profile['appl_state']!=99) ? $profile['StateName']:$profile['appl_state_others'];
                	
                    $applicantAddress  = $profile['appl_address1']."<br/>";
                    $applicantAddress .= $profile['appl_address2']."<br/>";
                    $applicantAddress .= $profile['appl_postcode']." ".$city."<br/>";
                    $applicantAddress .= $state."<br>";
                    $applicantAddress .= $profile['CountryName'];*/
                    
                    //student info
                    $address = $profile["appl_address1"];
                    //$address = '';

                    if($profile["appl_address2"]){
                            $address.='<br />'.$profile["appl_address2"].' ';
                    }
                    
                    if($profile["appl_address3"]){
                            $address.='<br />'.$profile["appl_address3"].' ';
                    }

                    if($profile["appl_postcode"]){
                            $address.='<br />'.$profile["appl_postcode"].' ';
                    }

                    if($profile["appl_city"]!=99){
                            $address.= $profile["CityName"].' ';
                    }else{
                        $address.= $profile["appl_city_others"].' ';
                    }

                    if($profile["appl_state"]!=99){
                            $address.='<br />'.$profile["StateName"].' ';
                    }else{
                        $address.= '<br />'.$profile["appl_state_others"].' ';
                    }

                    $address.= $profile["CountryName"];

                    //echo $address; exit;

                    //var_dump($profile["CountryName"]); exit;

                    //$address = htmlspecialchars($address, ENT_QUOTES);

                    $content = str_replace("[".$tag['tag_name']."]", strtoupper($address), $content);
                    
                break;

                case "Correspondence Address":

                    /*$city  = ($profile['appl_city']!=99) ? $profile['CityName']:$profile['appl_city_others'];
                    $state = ($profile['appl_state']!=99) ? $profile['StateName']:$profile['appl_state_others'];

                    $applicantAddress  = $profile['appl_address1']."<br/>";
                    $applicantAddress .= $profile['appl_address2']."<br/>";
                    $applicantAddress .= $profile['appl_postcode']." ".$city."<br/>";
                    $applicantAddress .= $state."<br>";
                    $applicantAddress .= $profile['CountryName'];*/

                    //student info
                    $address = $profile["appl_address1"];
                    //$address = '';

                    if($profile["appl_address2"]){
                        $address.='<br />'.$profile["appl_address2"].' ';
                    }

                    if($profile["appl_address3"]){
                        $address.='<br />'.$profile["appl_address3"].' ';
                    }

                    if($profile["appl_postcode"]){
                        $address.='<br />'.$profile["appl_postcode"].' ';
                    }

                    if($profile["appl_city"]!=99){
                        $address.= $profile["CityName"].' ';
                    }else{
                        $address.= $profile["appl_city_others"].' ';
                    }

                    if($profile["appl_state"]!=99){
                        $address.='<br />'.$profile["StateName"].' ';
                    }else{
                        $address.= '<br />'.$profile["appl_state_others"].' ';
                    }

                    $address.= $profile["CountryName"];

                    //echo $address; exit;

                    //var_dump($profile["CountryName"]); exit;

                    //$address = htmlspecialchars($address, ENT_QUOTES);

                    $content = str_replace("[".$tag['tag_name']."]", strtoupper($address), $content);

                    break;
                
                case "Salutation":
                    //$applId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = '.$trans_id);
                    //$salutationCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_salutation', 'appl_id = '.$applId);
                    //$salutation = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = '.$salutationCode);
                    $content = str_replace("[".$tag['tag_name']."]", $profile['Salutation'], $content); 
                break;
                                
                case "Incomplete Checklist":     
                	         	
					$content = $this->parseContentIncompleteChecklist($trans_id, $content, $tag['tag_name']);
				break;
                
                case "Applicant Email":                	
					$content = str_replace("[".$tag['tag_name']."]", $profile['appl_email'], $content); 
				break;
				
				case "Applicant Password":                	
					$content = str_replace("[".$tag['tag_name']."]", $profile['appl_password'], $content); 
				break;
                
				case 'Programme':
                     $content = str_replace("[".$tag['tag_name']."]", $profile['program_name'], $content);
                break;
				
				case 'Offered Checklist':
                     
                break;
				
				case 'Proforma Ref No':
					if ( preg_match( '/\[Proforma Ref No\]/', $content) )
					{
						$programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id='.$trans_id);
						$programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme='.$programSchemeId);

						$level = $this->getLevel($profile['at_status']);

						$programInfo = $this->getProgram($programId);
						if ($programInfo['IdScheme']==1){
							$level = 0;
						}
								
						$invoice = $icampusClass->getApplicantProformaInfo($profile['at_trans_id'],$level);
						if(count($invoice['proforma_invoice_info'])>0){
							$content = str_replace("[".$tag['tag_name']."]", $invoice['proforma_invoice_info'][0]['bill_number'], $content);
						}
					}
                break;
                
                case 'NRIC':
                    //$profileId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = '.$trans_id);
                    //$nric = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_idnumber', 'appl_id = '.$profileId);
                    $content = str_replace("[".$tag['tag_name']."]", $profile['appl_idnumber'], $content);
                break;
                
                case 'Mode of Programme':
                      $content = str_replace("[".$tag['tag_name']."]", $profile['ProgramMode'], $content);
                break;
            
                case 'Mode of Study':
                     $content = str_replace("[".$tag['tag_name']."]", $profile['StudyMode'], $content);
                break;
                
                case 'Programme Type':               
                    $content = str_replace("[".$tag['tag_name']."]", $profile['ProgramType'], $content);
                break;
                
                case 'Semester':
                    $intake = Application_Model_DbTable_ChecklistVerification::universalQueryRow('tbl_intake', '*', 'IdIntake = '.$profile['at_intake']);
                    $semester = Application_Model_DbTable_ChecklistVerification::universalQueryRow('tbl_semestermaster', '*', 'AcademicYear = '.$intake['sem_year'].' AND sem_seq = "'.$intake['sem_seq'].'"');
                    $content = str_replace("[".$tag['tag_name']."]", $semester['SemesterMainName'], $content);
                break;

                case 'Proforma':
					if ( preg_match( '/\[Proforma\]/', $content) )
					{
//	                    echo "<pre>";print_r($proformaInfo);
	                    $programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id='.$trans_id);
	                    $programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme='.$programSchemeId);
	                    $program = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram='.$programId);
	                    $programCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramCode', 'IdProgram='.$programId);
                            
                            $level = $this->getLevel($profile['at_status']);
                            
                            $programInfo = $this->getProgram($programId);
                            if ($programInfo['IdScheme']==1){
                                $level = 0;
                            }
                            
                            $class = new icampus_Function_Studentfinance_Invoice();
	                    $proformaInfo = $class->getApplicantProformaInfo($trans_id, $level);
                            //var_dump($proformaInfo);
                            /*if (count($proformaInfo['proforma_invoice_info']) > 0){
                                //var_dump($proformaInfo); exit;
                                $proforma = "<table id='tfhover' class='tftable' border='1'>";
                                $proforma .= "<thead>";
                                $proforma .= "<tr>";
                                $proforma .= "<th>Item Code</th>";
                                $proforma .= "<th>Description</th>";
                                $proforma .= "<th>Type</th>";
                                foreach($proformaInfo['totalcurrency'] as $key=>$invCur){
                                    $proforma .= "<th>Amount (".$invCur.")</th>";
                                }
                                //$proforma .= "<th>Amount (".$proformaInfo['proforma_invoice_info'][0]['detail']['currency']['cur_code'].")</th>";
                                $proforma .= "</tr>";
                                $proforma .= "</thead>";
                                $proforma .= "<tbody>";
                                if($proformaInfo['proforma_invoice']['total_amount']!=0){ 
                                    foreach($proformaInfo['proforma_invoice_info'] as $proformaInfoLoop){
                                        $proforma .= "<tr>";
                                        //$proforma .= "<td>".$proformaInfoLoop['fc_code']."</td>";
                                        $proforma .= "<td>".$programCode."</td>";
                                        //$proforma .= "<td>".$proformaInfoLoop['fc_desc']."</td>";
                                        $proforma .= "<td>".$program."</td>";
                                        //$proforma .= "<td>".$proformaInfoLoop['invoice_type']."</td>";
                                        $proforma .= "<td>".$proformaInfoLoop['fc_desc']."</td>";
                                        //$proforma .= "<td>".$proformaInfoLoop['bill_amount']."</td>";
                                        foreach($invoice['totalcurrency'] as $key=>$invCur){
                                            $curInvDet = $proformaInfoLoop['detail']['currency']['cur_code'];

                                            if($curInvDet == $invCur){
                                                    $amountDet = $proformaInfoLoop['bill_amount'];

                                            }else{
                                                    $amountDet = "&nbsp;";
                                            }
                                            $proforma .= "<td align='right'>".$amountDet."</td>";
                                        }
                                        $proforma .= "</tr>";
                                    }
                                }
                                $proforma .= "<tr><td colspan='4' align='right'>Total : ".$proformaInfo['proforma_invoice_info'][0]['detail']['currency']['cur_code'].' '.$proformaInfo['proforma_invoice']['total_amount']."</td></tr>";
                                $proforma .= "</tbody>";
                                $proforma .= "</table>";
                            }*/
                           
                            $totalCurrency = count($proformaInfo['totalcurrency']);
                            $colspan = $totalCurrency + 3;
                            ///var_dump($totalCurrency);
                            $proforma = "<table id='tfhover' class='tftable' border='1'>";
                            //$proforma = "<table border='1'>";
                            $proforma .= "<thead>";
                            $proforma .= "<tr>";
                            $proforma .= "<th width='10%'>Item Code</th>";
                            $proforma .= "<th width='30%'>Description</th>";
                            $proforma .= "<th width='30%'>Type</th>";
                            foreach($proformaInfo['totalcurrency'] as $key=>$invCur){
                                $proforma .= "<th width='10%'>Amount (".$invCur.")</th>";
                            }
                            //$invoice['proforma_invoice']['currency']['cur_code']
                            $proforma .= "</tr>";
                            $proforma .= "</thead>";
                            $proforma .= "<tbody>";

                            if (count($proformaInfo['proforma_invoice_info']) > 0){
                                    $a=0;

                                foreach($proformaInfo['proforma_invoice_info'] as $proformaInfoLoop){
                                	
                                	$amountGst = $proformaInfoLoop['bill_amount_gst'];
                                	
                                	if($amountGst!=0){
                                		$descType = $proformaInfoLoop['bill_description'];
                                	}else{
                                		$feeId = $proformaInfoLoop['detail'][0]['fi_id'];
                                		if(($feeId == 143) || ($feeId == 144) || ($feeId == 145) || ($feeId == 146)){
                                			$descType = $proformaInfoLoop['detail'][0]['fee_item_description'];
                                		}else{
                                			$descType = $proformaInfoLoop['fc_desc'];
                                		}
                                	}
                                	
                                    $proforma .= "<tr>";
                                    //$proforma .= "<td>".$proformaInfoLoop['fc_code']."</td>";
                                    $proforma .= "<td>".$programCode."</td>";
                                    //$proforma .= "<td>".$proformaInfoLoop['fc_desc']."</td>";
                                    $proforma .= "<td>".$program."</td>";
                                    //$proforma .= "<td>".$proformaInfoLoop['invoice_type']."</td>";
                                    $proforma .= "<td>".$descType."</td>";

                                    foreach($proformaInfo['totalcurrency'] as $key=>$invCur){
                                            $curInvDet = $proformaInfoLoop['detail']['currency']['cur_code'];

                                            if($curInvDet == $invCur){
                                                    $amountDet = $proformaInfoLoop['bill_amount'];

                                            }else{
                                                    $amountDet = "&nbsp;";
                                            }
                                            $proforma .= "<td align='right'>".$amountDet."</td>";
                                    }
                                    $proforma .= "</tr>";
                                    $a++;

                                    /*if ($a >= 10){
                                        break;
                                        //$proforma .= "<div style='page-break-after: always'></div>";
                                    }*/
                                }
                            }
                            $proforma .= "<tr><td colspan='3' align='right'>Total : </td>";

                            foreach($proformaInfo['proforma_invoice_total'] as $key=>$invCur){
                                    $proforma .= "<td align='right'>".sprintf ("%.2f", $invCur)."</td>";
                            }

                            $proforma .= "</tr>";
                            $proforma .= "</tbody>";
                            $proforma .= "</table>";

                            /*if (count($proformaInfoLoop) >= 10){
                                $proforma .= "<div style='page-break-after: always'></div>";
                            }*/

                            //echo $proforma; exit;
                            $content = str_replace("[".$tag['tag_name']."]", $proforma, $content);
//	                    echo $content;
//	                    exit;
					}
	                   
                break;
                
                case "Date of Commencement":
                	$dateofcomm = $this->getDateFromSemesterSetup($trans_id, 39);
                    $content = str_replace("[Date of Commencement]", isset($dateofcomm['StartDate']) ? date("d F Y", strtotime($dateofcomm['StartDate'])):'', $content);
                break;
                
                case "End Date of Document Submission":
                	$enddatesubdoc = $this->getIntakeProgramInfo($profile['at_intake'], $profile['ap_prog_id'], $profile['ap_prog_scheme'], $profile['appl_category']);
                    $content = str_replace("[End Date of Document Submission]", isset($enddatesubdoc['AdmissionEndDate']) ? date("d F Y", strtotime($enddatesubdoc['AdmissionEndDate'])):'', $content);
                break;
                
                case 'Required Documents':
                	$content = $this->requiredDocumentChecklist($profile,$content);
                break;
                
                case "Closing date for registration":
                	 $dateclosingreg = $this->getDateFromSemesterSetup($trans_id, 18);
                     $content = str_replace("[Closing date for registration]", isset($dateclosingreg['EndDate']) ? date("d F Y", strtotime($dateclosingreg['EndDate'])):'', $content);
                break;
                
                case "Last date of submission of assignments":
                     $dateassingsub = $this->getDateFromSemesterSetup($trans_id, 45);
                     $content = str_replace("[Last date of submission of assignments]", isset($dateassingsub['StartDate']) ? date("d F Y", strtotime($dateassingsub['StartDate'])):'', $content);
                break;
                
                case "Final examination":
                	 $datefinex = $this->getDateFromSemesterSetup($trans_id, 42);
                     $content = str_replace("[Final examination]", isset($datefinex['StartDate']) ? date("d F Y", strtotime($datefinex['StartDate'])):'', $content);
                break;
                
                case "Semester end":
                	 $datesemend = $this->getDateFromSemesterSetup($trans_id, 47);
                     $content = str_replace("[Semester end]", isset($datesemend['StartDate']) ? date("d F Y", strtotime($datesemend['StartDate'])):'', $content);
                break;
                
                case "Result release":
                	 $dateresultrelease = $this->getDateFromSemesterSetup($trans_id, 38);
                     $content = str_replace("[Result release]", isset($dateresultrelease['StartDate']) ? date("d F Y", strtotime($dateresultrelease['StartDate'])):'', $content);
                break;
                
                //TODO
                case "Faculty":
                	
                break;
                
                case "Fee - Security Deposit":
                	$Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,4);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
                case 'Fee - Resource':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoiceResource1 = $Invoice->getDataByTxnId($trans_id,array(6, 81));
                    $invoiceResource2 = $Invoice->getDataByTxnId($trans_id,7);
                    
                    if ($invoiceResource1){
                        $currency = $invoiceResource1['currencyName'];
                    }else{
                        $currency = $invoiceResource2['currencyName'];
                    }

                    $fee_resource_amt = ($invoiceResource1['bill_amount']+$invoiceResource2['bill_amount']);
                    //$template = str_replace("[Fee - Resource]",$currency.' '.$fee_resource_amt,$template);
                    
                    $content = str_replace("[".$tag['tag_name']."]",$currency.' '.$fee_resource_amt,$content);
                break;
                
                case 'Fee - Student Union':
                  	$Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,5);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
               case 'Fee - Personal Bond':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,3);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
            		}else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break; 
                
                case 'Fee - EMGS Visa Processing Fee':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,10);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
                case 'Fee - Student Pass Visa':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,12);
                    if($invoice){
                   		 $content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;

                case 'Fee - Student Services':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getFee($trans_id, '87, 89, 90, 91');
                    if($invoice){
                        $content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                        $content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                    break;

                case 'Visa':
                    $FeeCountry = new Studentfinance_Model_DbTable_FeeCountry();
                    $invoice = $FeeCountry->getItemData(80, $profile['appl_nationality'], 1);
                    if($invoice){
                        $content = str_replace("[".$tag['tag_name']."]", $invoice['fic_amount'], $content);
                    }else{
                        $content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                    break;
                
                case 'Fee - Resource Fee':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoiceResource1 = $Invoice->getDataByTxnId($trans_id,array(6, 81));
                    $invoiceResource2 = $Invoice->getDataByTxnId($trans_id,7);
                    
                    if ($invoiceResource1){
                        $currency = $invoiceResource1['currencyName'];
                    }else{
                        $currency = $invoiceResource2['currencyName'];
                    }

                    $fee_resource_amt = ($invoiceResource1['bill_amount']+$invoiceResource2['bill_amount']);
                    //$template = str_replace("[Fee - Resource]",$currency.' '.$fee_resource_amt,$template);
                    
                    $content = str_replace("[".$tag['tag_name']."]",$currency.' '.$fee_resource_amt,$content);
                break;
                
                case 'Fee - Insurance':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,9);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
            		}else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
                case 'Fee - iKad':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,13);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
                case 'Fee - Medical Check-up':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,11);                    
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
            		}else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
                case 'Fee - Total Tuition':
                    $class = new icampus_Function_Studentfinance_Invoice();
                    
                    if ($profile['at_fs_id']!=null){
                        $proformaInfo = $class->getTuitionFee($profile['at_fs_id']);  
                        if($proformaInfo){         
                            $content = str_replace("[".$tag['tag_name']."]", $proformaInfo['cur_code'].' '.$proformaInfo['fs_estimated_total_fee'], $content);
                        }else{
                            $content = str_replace("[".$tag['tag_name']."]", null, $content);
                        }
                    }
                break;
                
                case 'Tuition Fee':
                    $class = new icampus_Function_Studentfinance_Invoice();
                    if ($profile['at_fs_id']!=null){
                        $proformaInfo = $class->getTuitionFee($profile['at_fs_id']);  
                        if($proformaInfo){         
                            $content = str_replace("[".$tag['tag_name']."]", $proformaInfo['cur_code'].' '.$proformaInfo['fs_estimated_total_fee'], $content);
                        }else{
                            $content = str_replace("[".$tag['tag_name']."]", null, $content);
                        }
                    }
                break;
                               
                
				case 'Fee - Application Processing':
					$Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,2);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
				break;
            
                case 'Fee - Progressive Payment Facility':
                   	$Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,36);
                    if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                break;
                
                case 'Fee - Initial Tuition':
                	 $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                	 $invoice = $Invoice->getDataByTxnId($trans_id,8);
             		 if($invoice){
                    	$content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                     }else{
                    	$content = str_replace("[".$tag['tag_name']."]", null, $content);
                     }
                break;

                case 'PC - Basic Course Fees':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id,array(80, 8));
                    if($invoice){
                        $content = str_replace("[".$tag['tag_name']."]", $invoice['currencyName'].' '.$invoice['bill_amount'], $content);
                    }else{
                        $content = str_replace("[".$tag['tag_name']."]", null, $content);
                    }
                    break;
                    
            }
        }
        return $content;
    }
    
    
	protected function parseContentIncompleteChecklist($app_id, $content, $tagName)
	{
                $this->groupsModel = new Communication_Model_DbTable_Groups();
		$incompleteChecklist = $this->groupsModel->getIncompleteChecklist($app_id, 3);
		
		$class_th = ' font:normal 11px Arial; background-color:#f1f1f1; color:#666; padding: 4px 4px; border-bottom:1px solid #ccc; font-weight:normal; text-align:left ';
		$class_td = ' font:normal 11px Arial; background-color:#fafafa; border-bottom:1px solid #e1e1e1; padding: 4px; font-weight:normal; vertical-align:top';

		$listChecklist = '';
		if (count($incompleteChecklist) > 0)
		{
			$i=1;
			$listChecklist .= '<div style="background:#ccc;padding:1px 1px 0 1px;"><table class="table" bgcolor="#fcfcfc" width="100%" border="0" cellspacing="0" cellpadding="5" style="font:normal 11px \"Lucida Grande\", Arial; background-color: #fcfcfc; border-spacing: 0; border-collapse: collapse;">';
			$listChecklist .= '<tr>';
			$listChecklist .= '<th width="5%" style="'.$class_th.'">No.</th>';
			$listChecklist .= '<th width="50%" style="'.$class_th.'">Checklist Description</th>';
			$listChecklist .= '<th width="35%" style="'.$class_th.'">Comment</th>';
			$listChecklist .= '<th style="'.$class_th.'">Status</th>';
			$listChecklist .= '</tr>';
			foreach ($incompleteChecklist as $incompleteChecklistLoop){
				
				$checklistDesc = Communication_Model_DbTable_Groups::getChecklistDesc($incompleteChecklistLoop['table_name'], '*', $incompleteChecklistLoop['table_primary_id'].' = '.$incompleteChecklistLoop['ads_table_id'], $incompleteChecklistLoop['table_join'], $incompleteChecklistLoop['join_column1'], $incompleteChecklistLoop['join_column2']);
				//var_dump($checklistDesc); 
				
				$status = '';
				switch ($incompleteChecklistLoop['ads_status']){
					case 1:
						$status = 'New';
						break;
					case 2:
						$status = 'Viewed';
						break;
					case 3:
						$status = 'Incomplete';
						break;
					case 3:
						$status = 'Complete';
						break;
				}
				
				$listChecklist .= '<tr>';
				$listChecklist .= '<td valign="top" style="'.$class_td.'">'.$i.'</td>';
				$listChecklist .= '<td valign="top" style="'.$class_td.'">'.$incompleteChecklistLoop['dcl_desc'].'</td>';
				$listChecklist .= '<td valign="top" style="'.$class_td.'">'.$incompleteChecklistLoop['ads_comment'].'</td>';
				$listChecklist .= '<td style="'.$class_td.'">'.$status.'</td>';
				$listChecklist .= '</tr>';
				$i++;
			}
			$listChecklist .= '</table></div>';
		}
		
		$content = str_replace("[".$tagName."]", $listChecklist, $content);
		
		return $content;
	}
	
	
	public function requiredDocumentChecklist($applicant,$content){
		
		$model = new App_Model_Application_DbTable_Additional();
		$requiredDocList = $model->getRequiredDocument($applicant['appl_category'], $applicant['ap_prog_scheme']);
		$financeDocList = $model->getFinanceDocument($applicant['appl_category'], $applicant['ap_prog_scheme']);
		
		$requiredDoc = '';
		$i = 1;
		if ($financeDocList){
			$requiredDoc .= $i.') Finance Document<br/><br/>';
			$i++;
			$requiredDoc .= '<ul>';
			foreach ($financeDocList as $financeDocLoop){
				$requiredDoc .= '<li>'.$financeDocLoop['dcl_desc'].'</li>';
			}
			$requiredDoc .= '</ul><br/>';
		}
		
		if ($requiredDocList){
			foreach ($requiredDocList as $requiredDocLoop){
				$requiredDoc .= $i.') '.$requiredDocLoop['dcl_desc'].'<br/><br/>';
				$i++;
			}
			
			$content = str_replace("[Required Documents]",$requiredDoc,$content);
		}
		
		return $content;
	}
	
	public function getDateFromSemesterSetup($txnId, $actId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->join(array('b'=>'tbl_intake'), 'a.at_intake = b.IdIntake')
            ->join(array('c'=>'tbl_semestermaster'), 'b.sem_year = c.AcademicYear AND b.sem_seq = c.sem_seq')
            ->join(array('d'=>'tbl_activity_calender'), 'c.IdSemesterMaster = d.IdSemesterMain')
            ->where('a.at_trans_id = ?', $txnId)
            ->where('d.IdActivity = ?', $actId)
            ->order('d.StartDate ASC');
        
        $result = $db->fetchRow($select);
        
        if($result){
        	return $result;
        }else{
        	return null;
        }
    }
    
	public function getIntakeProgramInfo($intakeId, $programId, $progSchemeId, $studCat){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake_program'), array('value'=>'*'))
            ->where('a.IdIntake = ?', $intakeId)
            ->where('a.IdProgram = ?', $programId)
            ->where('a.IdProgramScheme = ?', $progSchemeId)
            ->where('a.IdStudentCategory = ?', $studCat);
        
        $result = $db->fetchRow($select);
        
		if($result){
        	return $result;
        }else{
        	return null;
        }
    }
    
 public function getTemplateTag($tplId){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"comm_template_tags"),array("value"=>"a.*"))
            ->where('a.tpl_id = '.$tplId.' or a.tpl_id = 0')
            ->order("a.tpl_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    public function getLevel($status){
        switch ($status){
            case 592:
                $level = 0;
                break;
            case 595:
                $level = 0;
                break;
            case 596:
                $level = 0;
                break;
            case 597:
                $level = 0;
                break;
            case 595:
                $level = 0;
                break;
            case 593:
                $level = 1;
                break;
            case 599:
                $level = 1;
                break;
            case 601:
                $level = 1;
                break;
            default:
                $level = 0;
        }
        
        return $level;
    }
    
    public function getProgram($programid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.Idprogram = ?', $programid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    
	public function parseContentFinance($trans_id, $tpl_id, $content,$moduleComm,$activityReminderId=0) //$trans_id = IdStudentRegistration
    {

    	$invoiceClass = new icampus_Function_Studentfinance_Invoice();
    	$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		$profile = $studentRegDB->getTheStudentRegistrationDetail($trans_id);
		   
        $templateTags = $this->getTemplateTagFinance($tpl_id,$moduleComm);
        
        if($activityReminderId){
        	$templateCondition = $this->getTemplateCondition($activityReminderId);
        }else{
        	$templateCondition = array();
        }

        foreach ($templateTags as $tag)
        {
            switch($tag['tag_name'])
            {
               
                case "Student Name":
                        $content = str_replace("[".$tag['tag_name']."]", trim($profile['appl_fname'].' '.$profile['appl_lname']), $content);
                break;
                
                case "Student ID":                    
                    $content = str_replace("[".$tag['tag_name']."]", $profile['registrationId'], $content);
                break;

                case 'Programme':
                     $content = str_replace("[".$tag['tag_name']."]", $profile['ProgramName'], $content);
                break;
                
                case 'Programme Scheme':
                     $content = str_replace("[".$tag['tag_name']."]", $profile['ProgramMode'].'<br>'.$profile['StudyMode'], $content);
                break;
                
                case 'Intake':
                     $content = str_replace("[".$tag['tag_name']."]", $profile['IntakeDesc'], $content);
                break;
                
                case 'Last Payment Date':
                	$lastDate=NULL;
                	if($templateCondition){
                		$lastDate = date('d-F-Y',strtotime($templateCondition['last_date']));
                	}
                     $content = str_replace("[".$tag['tag_name']."]",$lastDate , $content);
                break;
                
                case 'Outstanding Balance':
                	
                	 $getOutstandingBalance = $invoiceClass->getOutstandingBalance($trans_id,2);
                     $content = str_replace("[".$tag['tag_name']."]", number_format($getOutstandingBalance['amount'],2), $content);
                break;
                    
            }
        }
        return $content;
    }
    
	public function getTemplateTagFinance($tplId,$moduleComm){
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
	            ->from(array("a"=>"comm_template_tags"))
	            ->where('a.tpl_id = '.$tplId.' or a.tpl_id = 0')
	             ->where('a.module = ?',$moduleComm)
	            ->order("a.tpl_id");
	           
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
 	public function getTemplateCondition($id){
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
	            ->from(array("a"=>"comm_template_condition"))
	             ->where('a.cond_id = ?',$id);
	           
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
}

?>