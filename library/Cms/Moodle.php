<?php
class Cms_Moodle
{
	public static function getLogin($user)
	{
		$guid = MOODLE_GUID;
		$url = MOODLE_URL;

		if ( !isset($guid) || !defined('MOODLE_GUID') ) return false;
		if ( !isset($url) || !defined('MOODLE_URL') ) return false;

		$data = array(
						"stamp"			=> time(),
						"id"			=> $user->iduser,
						"email"			=> $user->email,
						"username"		=> $user->loginName,
						"passwordhash"	=> $user->passwd						
		);
		
		
		$details = http_build_query($data);
		
		$url = rtrim(MOODLE_URL.'auth/meteorsso/login.php?data='.self::encrypt($details, MOODLE_GUID));
		
		return $url;
		
	}

	public static function encrypt($value, $key) 
	{
		if (!$value) {return "";}
		$text = $value;
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key.$key), $text, MCRYPT_MODE_ECB, $iv);

		// encode data so that $_GET won't urldecode it and mess up some characters
		$data = base64_encode($crypttext);
		$data = str_replace(array('+','/','='),array('-','_',''),$data);
		return trim($data);
	}
}