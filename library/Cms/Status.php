<?php

class Cms_Status 
{
	/*
	 * Function : To update student profile status and to add into history table
	 */
	public function profileStatus($IdStudentRegistration,$profilestatus,$type=null)
    {
	    	$auth = Zend_Auth::getInstance();
	    		     	
	        //add profile status into history table
	        $this->addProfileStatusHistory($IdStudentRegistration,$profilestatus['profileStatus'],$type);
	                       
	        //update profile status
	        $studentRegDB = new Registration_Model_DbTable_Studentregistration();
	        $studentRegDB->updateStudentProfile($IdStudentRegistration,$profilestatus);
    }
    
    
	/*
	 * Function : To update student semester status and to add into history table
	 */
	public function copyDeleteAddSemesterStatus($IdStudentRegistration,$idSemester,$data,$message,$type)
    {
	    	$auth = Zend_Auth::getInstance();
                       
	    	$studentSemesterStatusDB = new Records_Model_DbTable_SemesterChangestatus();
	    	
	    	$studentSemesterStatusDB->copyanddeletesemesterstatus($IdStudentRegistration,$idSemester,$message,$type);
	    		    	
	    	if($data){
	    		$studentSemesterStatusDB->addData($data);
	    	}
    }
    
	public function addProfileStatusHistory($IdStudentRegistration,$profilestatus,$type=null)
    {
	    	$auth = Zend_Auth::getInstance();	    	
	     
	     	$studentHistoryDB =  new Registration_Model_DbTable_Studenthistory();
	  
	        //add profile status into history table
	        $statusArray['profileStatus'] = $profilestatus;
	        $statusArray['IdStudentRegistration'] = $IdStudentRegistration;
	        $statusArray['IpAddress'] = $_SERVER['REMOTE_ADDR'];    //$this->getRequest()->getServer('REMOTE_ADDR');
	        $statusArray['Type'] = $type; 
	        $statusArray['UpdUser'] = $auth->getIdentity()->iduser;
	        $statusArray['UpdDate'] = date('Y-m-d H:i:s');
	        $studentHistoryDB->addStudentProfileHistory($statusArray);	                    
	                
    }
}

?>