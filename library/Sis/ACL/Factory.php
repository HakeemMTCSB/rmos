<?php
	//Factory Class of ACL
	class Sis_ACL_Factory 
	{
		
		private static $_sessionNameSpace = 'Sis_ACL_Namespace';//Set an ACL Namespace
		private static $_objAuth;
		private static $_objAclSession;
		private static $_objAcl;
		
		//Function to get the ACL from DB or session
		public static function get(Zend_Auth $objAuth,$bolClearACL=false) 
		{
			self::$_objAuth = $objAuth;
			self::$_objAclSession = new Zend_Session_Namespace(self::$_sessionNameSpace);
			//var_dump(self::$_objAclSession->acl);die();
			if($bolClearACL) { self::_clear(); }
			//return (isset(self::$_objAclSession->acl)) ? self::$_objAclSession->acl : self::_loadAclFromDB();
			
			if(isset(self::$_objAclSession->acl)) {
				//echo "Use Session ACL";die();
				return self::$_objAclSession->acl;		
			}else{ 
		    	//echo "Load DB ACL";die();
		        return self::_loadAclFromDB();
			}
		}
		
		//Function to clear the session ACL
		private static function _clear() 
		{
			unset(self::$_objAclSession->acl);
		}
		
		//Function to save ACL to the session
		private static function _saveAclToSession() 
		{
			self::$_objAclSession->acl = self::$_objAcl;
		}
		
		//Function to load the roles, resources, role resource map from the DB and set roles for the resources
		private static function _loadAclFromDB() 
		{
			$lobjdeftype = new App_Model_Definitiontype();
			//$lobjmodelresources = new App_Model_Resources();
			//$lobjmodelrolesresources = new App_Model_RoleResources();
			
			self::$_objAcl = new Zend_Acl();
			
			//add roles
			$arrRoles = $lobjdeftype->fnGetDefinations('Role');
			foreach($arrRoles as $role){
				self::$_objAcl->addRole(new Zend_Acl_Role($role['idDefinition']));
			}
			
			
			//add resource
			//$arrResources = $lobjmodelresources->fngetAllRows();
			$navResourceDb = new GeneralSetup_Model_DbTable_TblResources();
			$arrResources = $navResourceDb->getDistinctResource();
			
			foreach($arrResources as $resource){
				self::$_objAcl->addResource(new Zend_Acl_Resource($resource['r_module'] .'::' .$resource['r_controller'] .'::' .$resource['r_action']));
			}

			try {
				self::$_objAcl->addResource(new Zend_Acl_Resource('default::index::login'));
			} catch (Exception $e) {

			}

			try {
				self::$_objAcl->addResource(new Zend_Acl_Resource('default::index::logout'));
			} catch (Exception $e) {

			}

			try {
				self::$_objAcl->addResource(new Zend_Acl_Resource('default::changepassword::index'));
			} catch (Exception $e) {

			}

			try {
				self::$_objAcl->addResource(new Zend_Acl_Resource('default::error'));
			} catch (Exception $e) {

			}

			try {
				self::$_objAcl->addResource(new Zend_Acl_Resource('default::error::noresource'));
			} catch (Exception $e) {

			}
			
			
			
	 		//deny all( since we do not use inheritance acl
			foreach($arrRoles as $role){
				self::$_objAcl->deny($role['idDefinition'],null,null);
			}
			
			//get roles nav
			$roleNavDb = new GeneralSetup_Model_DbTable_RoleResourcesNav();
			$arrNav = $roleNavDb->fetchAll(array('rrn_role_id =? '=>self::$_objAuth->getIdentity()->IdRole))->toArray();
			
			//get nav resource
			$arrRoleResources = array();
			foreach ($arrNav as $nav){
				$where = array(
					'r_menu_id=?' => $nav['rrn_menu_id'],
					'r_title_id=?' => $nav['rrn_title_id'],
					'r_screen_id=?' => $nav['rrn_screen_id'],
					'r_group=?' => $nav['rrn_res_group'],
				);
				
				$arr_resource = $navResourceDb->fetchAll($where)->toArray();
				
				if($arr_resource){
					$arrRoleResources = array_merge($arrRoleResources, $arr_resource);
				}
			}
			
			// allow roles to resources
			foreach($arrRoleResources as $roleResource){
				if($roleResource['r_module']==null || $roleResource['r_module']==''){
					$roleResource['r_module']== 'default';
				}
				
				$resource = $roleResource['r_module'] .'::' .$roleResource['r_controller'] .'::' .$roleResource['r_action'];
				
				self::$_objAcl->allow(self::$_objAuth->getIdentity()->IdRole,$resource);
			}
			
			//allow basic auth
			self::$_objAcl->allow(self::$_objAuth->getIdentity()->IdRole,'default::index::logout');
			self::$_objAcl->allow(self::$_objAuth->getIdentity()->IdRole,'default::index::login');
			self::$_objAcl->allow(self::$_objAuth->getIdentity()->IdRole,'default::changepassword::index');
			
			
			/*
			//add role resource mapping
			$arrRoleResources = $lobjmodelrolesresources->fngetAllRows();
			
	        // allow roles to resources
	        foreach($arrRoleResources as $roleResource){
	            self::$_objAcl->allow($roleResource['idRoles'],$roleResource['Module'] .'::' .$roleResource['Controller'] .'::' .$roleResource['Action']);
	        }*/	
	        	
			self::_saveAclToSession();	
			
			return self::$_objAcl;
		}
	}