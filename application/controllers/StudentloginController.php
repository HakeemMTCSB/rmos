<?php
class StudentloginController extends Zend_Controller_Action {
	private $gstrsessionSIS;//Global Session Name
	
	public function init() {
		$this->fnsetObj();		
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$this->_helper->layout()->setLayout('/student/usty');
  	}

	public function fnsetObj(){
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjacademicresultForm = new App_Form_Academicresult();		
		$this->academicresult = new App_Model_Academicresult();
	}
	
	function indexAction() {
         $lobjsis = Zend_Registry::get('sis'); //setting the registry variable
         if($lobjsis->IdStudentRegistration) {
         	
         } 
         else 
         	$this->_redirect('studentlogin/login'); //redirecting to loginpage if not authenticated	
	}

   function loginAction() {
   		$this->_helper->layout->disableLayout (); //
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
        $lobjform = new App_Form_Login(); //intialize login form
        $this->view->lobjform = $lobjform; //send the form object to the view

        if ($this->_request->isPost()) {
	        Zend_Loader::loadClass('Zend_Filter_StripTags');
	        $filter = new Zend_Filter_StripTags();
	        $username = $filter->filter($this->_request->getPost('username'));
	        $password = $filter->filter($this->_request->getPost('password'));
  
			$dbAdapter = Zend_Db_Table::getDefaultAdapter();
			$authathen = new App_Model_Studentlogin();
            $result = $authathen->fnStudentauth($username,$password);
            if ($result) {
				$this->gstrsessionSIS->__set('IdStudentRegistration',$result['IdStudentRegistration']);	
				$this->gstrsessionSIS->__set('RegistrationID',$result['registrationId']);	
				$this->gstrsessionSIS->__set('name',$result['FName'].' '.$result['MName'].' '.$result['LName']);	
				$this->_redirect( $this->baseUrl . '/studentlogin/index');
            } else {
            	$this->view->alertError = 'Login failed. Either username or password is incorrect';
            }     
        }        
		$this->render(); //render the view
    }
    
    public function academicresultAction(){
    	    	
    	 $lobjsis = Zend_Registry::get('sis'); //setting the registry variable
    	 $acdemicresult = new App_Model_Academicresult();
		 $university= $acdemicresult->fnGetUniversityId($lobjsis->RegistrationID);
		 $university['AffiliatedTo'];
		 
		 if($university['AffiliatedTo']) {
			$idUniversity= $university['AffiliatedTo'];
		 } else {
			$idUniversity= 1;
		 }

		 $initialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		 $larrInitialSettings = $initialconfig->fnGetInitialConfigDetails($idUniversity);
		 $this->view->markdentrytype = $larrInitialSettings['MarksAppeal'];
		 $this->view->TakeMarks = $larrInitialSettings['TakeMarks'];
		 
         if($lobjsis->RegistrationID) {
         	$acdemicresult = new App_Model_Academicresult();
			$sdudentresults =$this->view->sdudentresults= $larrresult = $acdemicresult->fnGetStudentAcademicResultsList($lobjsis->RegistrationID,$this->view->TakeMarks);

			/*foreach($sdudentresults as $sdudentresults1) {
				echo $arraysubmarks[]=$sdudentresults1['verifiresubjectmarks'];	
			}
			
			echo array_sum($arraysubmarks);*/
			$this->view->noresults = "No results found"; 
         } 
         else 
         	$this->_redirect('studentlogin/login');
    	
    }
    
	public function appeallistAction(){	
		
		$this->view->lobjacademicresultForm=$this->lobjacademicresultForm;
		$lintIdSubject = ( int ) $this->_getParam ( 'subjid' );
		$Subjname = $this->_getParam ( 'subjname' );
		$Subjmarks = $this->_getParam ( 'subjmarks' );
		$registrationId=( int ) $this->_getParam ( 'registrationId' );
		$applicationId=( int ) $this->_getParam ( 'applicationId' );
		$IdMarksDistributionMaster =( int ) $this->_getParam ( 'IdMarksDistributionMaster' );		
		$IdMarksDistributionDetails =( int ) $this->_getParam ( 'IdMarksDistributionDetails' );
		$Subjdistrbn = $this->_getParam ( 'Subjdistrbn' );
		$Cmpname = $this->_getParam ( 'Cmpname' );
		$idVerifierMarks = $this->_getParam ( 'idverifier' );

		$this->view->lobjacademicresultForm->IdSubject->setValue( $lintIdSubject );
		$this->view->lobjacademicresultForm->IdRegistration->setValue( $registrationId );
		$this->view->lobjacademicresultForm->IdApplication->setValue( $applicationId );
		$this->view->lobjacademicresultForm->UpdDate -> setValue(date('Y-m-d'));
		$this->view->lobjacademicresultForm->MarksObtained->setValue($Subjmarks);		
		$this->view->lobjacademicresultForm->IdMarksDistributionMaster->setValue($IdMarksDistributionMaster);
		$this->view->lobjacademicresultForm->IdMarksDistributionDetails->setValue($IdMarksDistributionDetails);
		$this->view->lobjacademicresultForm->Idverifiermarks->setValue($idVerifierMarks);
		
		
		
		$this->view->lobjacademicresultForm->programid->setValue(( int ) $this->_getParam ( 'programid' ));
		
		$this->view->subjectname=$Subjname;
		$this->view->marksobt=$Subjmarks;
		$this->view->Subjdistrbn=$Subjdistrbn;
		$this->view->Cmpname=$Cmpname;
				
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			
			$lintlastId=$this->academicresult->fnAddappealList($larrformData);

			$this->academicresult->fnGenerateCodes($lintlastId,$larrformData['IdRegistration'],$larrformData['IdSubject'],$larrformData['programid']);	
			echo "<script> 
					var x=window.confirm('Appeal Saved Successfully');
					if(x){
						parent.location ='".$this->view->baseUrl()."/studentlogin/academicresult/index/';	 	
					}
				</script>";	
			echo "<script>window.location ='".$this->view->baseUrl()."/studentlogin/academicresult/index/';</script>"; 
		}
	}
	
	public function changepasswordAction() {
		
		
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
	 	$lobjChangepasswordform = new App_Form_Changepassword(); 

	    // making form available to view
  	    $this->view->form = $lobjChangepasswordform;
	   
  	    // checking for clear button is pressed or not
  	    if ($this->getRequest()->getParam('Clear'))
  	    {
  	    		$this->_redirect( $this->baseUrl . '/studentlogin/changepassword');    		
  	    }
  	    
  	    // checking wheather form is pressed or not
	    if($this->getRequest()->ispost())		
		{
			 
			// creating db object			
			$lobjdb = Zend_Db_Table::getDefaultAdapter();
						
			// getting userid form present session 
			//$lobjdb = new App_Model_DefModel();
			$auth = Zend_Auth::getInstance();
			$gobjsessionsis = Zend_Registry::get('sis');
			$sid=$gobjsessionsis->IdStudentRegistration;
		   
			
			//$lintiduser = $auth->getIdentity()->IdStudentRegistration;
			//$lintiduser=$gobjsessionsis->name;
			
			
			
			
			
			// getting current user password from db
			$lstrsql = $lobjdb->select()
						->from('tbl_studentregistration','Psswrd')
						->where("IdStudentRegistration = $sid ");
					
				
								
			$stroldpasswordfromdb = App_Model_DefModel::selectrow($lstrsql);
					
		  

			// getting entered old password data from form & making md5 string of it 
			$strhashmd5 = $this->getRequest()->getParam('oldpassword');				
			//$strhashmd5 = md5($strpasswordFromForm);
			
			// if oldpassword from db and entered oldpassword in form is not equal then display message

			
			// getting newpassword & retype password data from form  
			$stroldPasswordFromForm = $this->getRequest()->getParam('oldpassword');
			$strnewPasswordFromForm = $this->getRequest()->getParam('newpassword');
			$strretypepasswordFromForm = $this->getRequest()->getParam('retypepassword');
			
						
/*			if($stroldPasswordFromForm == $strnewPasswordFromForm) {
				echo '<script language="javascript">alert("Old password & New password are same")</script>';				
			} */
			if ($stroldpasswordfromdb["Psswrd"] != $strhashmd5)
			{
				echo '<script language="javascript">alert("Old Password is Invalid")</script>';
			}
			// checking wheather newpassword & retype is equal or not
			elseif($strnewPasswordFromForm != $strretypepasswordFromForm)
			{
				
				echo '<script language="javascript">alert("New password & retype password is not matching")</script>';
			}
			
			// update new password in db if validation is correct
			elseif ($stroldpasswordfromdb["Psswrd"] == $strhashmd5 && $strnewPasswordFromForm == $strretypepasswordFromForm )
			{
				$strupdatePassword = $strretypepasswordFromForm; 
				$lobjdata =  array('Psswrd'=>$strupdatePassword);
				$lintwhereCondition = "IdStudentRegistration = $sid";
				$lobjresult = App_Model_DefModel::update('tbl_studentregistration',$lobjdata,$lintwhereCondition);
                    
				echo '<script language="javascript">alert("Password has been updated")</script>';	
				
				//$this->_redirect( $this->baseUrl . '/generalsetup/changepassword/index');    
						
			}			
		}
		
	
		
		
	}

    public function logoutAction() {
    	Zend_Session:: namespaceUnset('sis');
		Zend_Session::destroy();
		$this->_redirect( $this->baseUrl . '/student/');
    }
    
public function reportAction() {
	    //$this->view->lobjacademicresultForm=$this->lobjacademicresultForm;
		//echo "harsha";
		$lobjsis = Zend_Registry::get('sis');
		$rigid=$lobjsis->IdStudentRegistration;
		//print_r($rigid);
		$examdetailsmodel= new App_Model_Studentlogin();
		$examdetails=$examdetailsmodel->fngetexamdetails($rigid);
		$this->view->studentname=$examdetails[0]['StudentName'];
		$this->view->IdRegistration=$examdetails[0]['IdStudentRegistration'];
		$this->view->Programname=$examdetails[0]['ProgramName'];
		$this->view->Collegename=$examdetails[0]['CollegeName'];
		
		/*echo "<pre>";
		print_r($examdetails);*/
		
	}
	
   public function pdfexportAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjsis = Zend_Registry::get('sis');
		$rigid=$lobjsis->IdStudentRegistration;
		//print_r($rigid);
		$examdetailsmodel= new App_Model_Studentlogin();
		$examdetails=$examdetailsmodel->fngetexamdetails($rigid);
		
		$CheckedValuesList =array();
		
		//echo count($larrcompanydetails);die();
			    								
		$CheckedValuesList[]=$examdetails [0]['StudentName'];
		$CheckedValuesList[] =$examdetails [0]['IdStudentRegistration'];		
		$CheckedValuesList[] =$examdetails[0]['ProgramName'];
		$CheckedValuesList[] =$examdetails[0]['CollegeName'];		
		
		/*echo "<pre>";	   
	    print_r($CheckedValuesList);
	    die();*/
	    
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img align=center  width="150" height="100" src="../public/images/trisakti-logo.png" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter (date ( "d-m-Y H:i:s" ) . "                                          ".'{PAGENO}{nbpg}');
		// LOAD a stylesheet
		//$stylesheet = file_get_contents('../public/css/default.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = "Student Examination Slip";
		$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		
		$tabledata= "<table width='80%' border=1 align=center>
						<tr>
							<td width='20%'>Student Name </td>
							<td align=center width='50%'>$CheckedValuesList[0]</td>
						</tr>
						<tr>	
							<td width='20%'>Registration Id </td>
							<td align=center width='50%'>$CheckedValuesList[1]</td>
						</tr>
						<tr>	
							<td width='20%'>Program Name </td>
							<td align=center width='50%'>$CheckedValuesList[2]</td>
						</tr>
						<tr>	
							<td width='20%'>College Name </td>
							<td align=center width='50%'>$CheckedValuesList[3]</td>												
				</table><br><br>";
		
        	$tabledata.= "<table width='100%'>	<tr>	
							<td align=left>Signature of chancellor</td>
							<td align=right>Signature of Candidate</td>
						</tr>			
		                 </table><br><br><br><br>";
        	$tabledata.= "<table width='50%'  align=center>	<tr>	
							<td></td><td></td>
							<td>Seal and Signature of University</td>
						</tr>			
		                 </table>";
		
		$mpdf->WriteHTML($tabledata); 
		$mpdf->Output('StudentExaminationSlip.pdf','D');
		}
	

}