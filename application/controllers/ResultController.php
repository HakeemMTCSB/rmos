<?php
class ResultController extends Zend_Controller_Action {

	private $lobjstudentapplication;

	
	public function init() {
		$this->_helper->layout()->setLayout('/reg/usty');
		$this->fnsetObj();		
  	}

	public function fnsetObj(){

		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();

	}
	public function confirmationAction(){	
		if ($this->_request->isPost() && $this->_request->getPost('Save')) { // save opeartion		
			$lobjFormData = $this->_request->getPost();
			$icnumber = $lobjFormData['ICNumber'];	
			$agent = new App_Model_Agent();
			$this->view->studenteducationdetails = $agent->fnEducationdetailviewlist($icnumber);
			$this->view->count = count($this->view->studenteducationdetails);
			if($this->view->count > 0) {
				$agent = new App_Model_Agent();
				$this->view->sdudentresults = $agent->fnGetStudentResultsList($icnumber);
				$this->view->noresults = "No results found"; 
			}
			else {
				$this->view->alertError = 'Incorrect Studentid';
			}
		}	
	}
		
	public function studentresultsAction() {

	}
}