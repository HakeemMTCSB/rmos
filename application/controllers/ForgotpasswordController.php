<?php
class ForgotpasswordController extends Zend_Controller_Action {
	
 	private $gstrsessionSIS;//Global Session Name
	public function init() {
  		$this->_helper->layout()->setLayout('login/misc');
	}
	
  	
	public function indexOldAction()
	{
		$form = new App_Form_Forgotpassword();	
		$this->view->errorMsg = '';
		$this->view->sent = 0;
		
		$this->view->Msg = $this->view->translate('Enter your email address to reset your password');

		if ($this->_request->isPost()) 
		{	
			$larrformData = $this->_request->getPost();
			
			$db = getDb();
			$select = $db->select()
						  ->from('tbl_user',array('email','iduser','loginName'))
						  ->where("email = ?",$larrformData['email']);

			$larrResult = $db->fetchRow($select);

			$form->populate($larrformData);
			
			if ( empty($larrResult) )
			{
				$this->view->errorMsg = $this->view->translate('Invalid email address. Try again');
			}
			else
			{
				$key = generateRandomString(16);
				$keylink = APPLICATION_URL."/forgotpassword/reset/id/".$larrResult['iduser']."/key/".$key;

				$db->update('tbl_user', array('userkey'=>$key), $db->quoteInto('iduser=?',$larrResult['iduser']));
				
				$message = $this->view->translate('Click the link below to reset your password'). "<br />";
				$message .= $keylink;

				//send email
				$mail = new Cms_SendMail();
				$mail->fnSendMail($larrResult['email'], $this->view->translate('INCEIF e-University - Forgot Password'), $message);
				
				$this->view->Msg = $this->view->translate('An email has been sent to your account. Follow the instruction to reset your password.');

				$this->view->sent = 1;
			}
		}

		$this->view->form = $form;
		
	}

    public function indexAction()
    {
        $form = new App_Form_Forgotpassword();
        $this->view->errorMsg = '';
        $this->view->sent = 0;

        $this->view->Msg = $this->view->translate('Enter your email address to reset your password');
        if ($this->_request->isPost())
        {
            $larrformData = $this->_request->getPost();

            $db = getDb();
            $select = $db->select()
                ->from('tbl_user',array('email','iduser','loginName','fName'))
                ->where("email = ?",$larrformData['email'])
                ->where("UserStatus = ?",1);//active

            $larrResult = $db->fetchRow($select);

            $form->populate($larrformData);

            if ( empty($larrResult) )
            {
                $this->view->errorMsg = $this->view->translate('Invalid email address. Try again');
            }
            else
            {
                $key = generateRandomString(16);

                $keylink = $this->view->serverUrl() ."/forgotpassword/reset/id/".$larrResult['iduser']."/key/".$key;

                $db->update('tbl_user', array('userkey'=>$key), $db->quoteInto('iduser=?',$larrResult['iduser']));

//                //email success
//                $info = array(
//                    'template_id' => 71,
//                    'name' => strtoupper($larrResult['loginName']),
//                    'keylink' => $keylink,
//                    'email' => $larrResult['email']
//                );
//
//                $email = new icampus_Function_Email_Email();
//                $email->sendEmail($info);

                //email new
                $emailDb = new App_Model_Email();
                $commDb = new Communication_Model_DbTable_Template();

                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'forgot-password', 0, 1);
                $template = $gettemplate[0];
                $larrResult['keylink'] = $keylink;
                
                $dataEmail = array(
                    'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $larrResult['email'],
                    'subject'         => $template['tpl_name'],
                    'content'         => Thesis_Model_DbTable_Registration::parseContent($larrResult, $template['tpl_content']),
                    'date_que'        => date('Y-m-d H:i:s')
                );

                $emailDb->add($dataEmail);
                //email new

                $this->view->Msg = $this->view->translate('An email has been sent to you. To reset your password, please open the verification email and click on the link provided. ');

                $this->view->sent = 1;
            }
        }

        $this->view->form = $form;

    }
	
	public function resetAction()
	{
		$form = new App_Form_Forgotpassword();	
		$id = $this->_getParam('id');
		$key = $this->_getParam('key');

		$this->view->errorMsg = '';
		$this->view->Msg = '';
		$this->view->sent = 0;
		
		//default msg 
		$this->view->Msg = $this->view->translate('Set your new password below');

		// get userinfo
		$db = getDb();
		$select = $db->select()
					  ->from('tbl_user',array('email','iduser','loginName','userkey'))
					  ->where("iduser = ?",$id);

		$userinfo = $db->fetchRow($select);
		
		if ( empty($userinfo) )
		{
			$this->view->errorMsg = $this->view->translate('Invalid User ID');
			return false;
		}

		if ( $userinfo['userkey'] != $key  )
		{
			$this->view->errorMsg = $this->view->translate('Key doesn\'t match. Please try again.');
			return false;
		}

		//set new pass
		if ($this->_request->isPost()) 
		{	
			$larrformData = $this->_request->getPost();
			
			$newpass = md5($larrformData['password']);

			$db->update('tbl_user', array('passwd'=>$newpass), $db->quoteInto('iduser=?',$userinfo['iduser']));
			
			$this->view->Msg = $this->view->translate('Your new password has been set.');
			$this->view->sent = 1;
		}

		$this->view->form = $form;

	}

}