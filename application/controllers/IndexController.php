<?php
class IndexController extends Zend_Controller_Action {

	private $gstrsessionSIS;//Global Session Name
	public function init() { //instantiate log object

	}

	public function indexAction() {
		//landing page
		
		/*
		 * Get the roles depending on user role
		 */
		$auth = Zend_Auth :: getInstance();
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		/*$menuArray = $dbAdapter->fetchAll("SELECT * FROM tbl_nav_menu WHERE role_id = ".$auth->getIdentity()->IdRole." ORDER BY seq_order ");*/
		$menuArray = $dbAdapter->fetchAll("SELECT  
		    		b.tm_module as module, b.tm_controller as controller, b.tm_action as action, b.tm_name as label, b.tm_desc as title, b.tm_seq_order as seq_order, b.tm_id as id 
		    		FROM  tbl_role_resources_nav a
					JOIN tbl_top_menu b on b.tm_id = a.rrn_menu_id 
					where rrn_role_id = ".$auth->getIdentity()->IdRole." 
					AND b.tm_visibility=1
					GROUP BY `rrn_menu_id`");


		$UserDetail = $dbAdapter->fetchRow("SELECT * FROM tbl_user WHERE idUser = ".$auth->getIdentity()->id);
		$this->view->userDetail = $UserDetail;
		$this->view->moduleList = $menuArray;
		
		$auth = Zend_Auth::getInstance();

		//announcement
		$announceDB = new App_Model_General_DbTable_Announcements();
		$this->view->importantannouncements = $announceDB->getStudentAnnouncements( null, null, 'sms', $auth->getIdentity()->iduser);

		foreach ( $this->view->importantannouncements as $i => $announcement )
		{
			$files = $announceDB->getFiles($announcement['id']);
			$this->view->importantannouncements[$i]['files'] = $files;
		}
		
	}

	function loginAction() {

		//check if already has identity
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
//			$this->_redirect($this->view->url(array('module'=>'default','controller'=>'index', 'action'=>'index'),'default',false));
		}
		
		
		$this->_helper->layout()->setLayout('login/login');
		
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$lobjform = new App_Form_Login(); //intialize login form
		$lobjform->setAction($this->view->url(array('controller' => 'index', 'action' => 'login'), 'default', TRUE));
		
		$this->view->lobjform = $lobjform; //send the form object to the view
		$lobjuniversitymodel = new GeneralSetup_Model_DbTable_University();
		$larruniversityresult = $lobjuniversitymodel->fnGetUniversityList();
		
		if(count($larruniversityresult)>0)
			$lobjform->IdUniversity->addMultiOptions($larruniversityresult);
		else
			$lobjform->IdUniversity->addMultiOptions(array("0"=>"No University"));

		if ($this->_request->isPost()) {
			
			$larrformData = $this->_request->getPost();

			Zend_Loader::loadClass('Zend_Filter_StripTags');
			$filter = new Zend_Filter_StripTags();
			$username = $filter->filter($this->_request->getPost('username'));
			$password = $filter->filter($this->_request->getPost('password'));
			$IdUniversity = $filter->filter($this->_request->getPost('IdUniversity'));
			$larrdefaultlanguage = $lobjuniversitymodel->fngetUniversityLanguage($IdUniversity);

			if(isset($larrdefaultlanguage[0]['UnivLang'])){
				$lstrdefaultlanguage = $larrdefaultlanguage[0]['UnivLang'];
			}
			else{
				$larrdefaultlanguage[0]['UnivLang'] = "English";
				$lstrdefaultlanguage = $larrdefaultlanguage[0]['UnivLang'];
			}
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter();
			$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);


			$authAdapter->setTableName('tbl_user')
			->setIdentityColumn('loginName')
			->setCredentialColumn('passwd');

			$authAdapter->setIdentity($username);
			$authAdapter->setCredential(md5($password));

			$auth = Zend_Auth::getInstance();
			$authns = new Zend_Session_Namespace($auth->getStorage()->getNamespace());
			$authns->setExpirationSeconds(60*60); // 30 mins

			$result = $auth->authenticate($authAdapter);

			if ($result->isValid()) {
				//$data = $authAdapter->getResultRowObject(null, 'passwd');
				$data = $authAdapter->getResultRowObject();
				$fulldata = $data;
				//unset($data->passwd);
				
				$data->id = $data->iduser;
				
				$auth->getStorage()->write($data);
				$auth->getIdentity()->iduser;

				$larrCommonModel = new App_Model_Common();
				$Rolename = $larrCommonModel->fnGetRoleName($auth->getIdentity()->IdRole);

				$staffdetails = $larrCommonModel->fnGetStaff($auth->getIdentity()->IdStaff);
				
				$this->gstrsessionSIS->__set('idUniversity',$IdUniversity);
				$this->gstrsessionSIS->__set('idCollege',isset($staffdetails['IdCollege']) ? $staffdetails['IdCollege'] : 0);
				$this->gstrsessionSIS->__set('userType',isset($staffdetails['StaffType']) ? $staffdetails['StaffType'] : 0);  // user type 0:college  1: branch
				$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				$this->gstrsessionSIS->__set('UniversityLanguage',$lstrdefaultlanguage);
				$this->gstrsessionSIS->__set('FullName', isset($staffdetails['FullName']) ? $staffdetails['FullName']:'');
				$this->gstrsessionSIS->__set('LastLogin', $auth->getIdentity()->LastLogin);
				
				//ini yatie tambah nak tahu Defination Code 13/12/2012
				$this->gstrsessionSIS->__set('IdRole',$auth->getIdentity()->IdRole);
				

				if($staffdetails['StaffType']==0) {
					$this->gstrsessionSIS->__set('UserCollegeId','0');
				} else if($staffdetails['StaffType']==1) {
					$this->gstrsessionSIS->__set('UserCollegeId',$staffdetails['IdCollege']);
				}

				if($IdUniversity == '0' || $IdUniversity == '') {
					$this->gstrsessionSIS->__set('universityname','No University');
				} else {
					$lobjuniversitymodel = new GeneralSetup_Model_DbTable_University();
					$universityname = $lobjuniversitymodel->fnGetUniversityName($IdUniversity);
					$IdUniversityname= $universityname['Univ_Name'];
					$this->gstrsessionSIS->__set('universityname',$IdUniversityname);
				}
				
				$db = getDb();
				$db->update('tbl_user', array('LastLogin' => new Zend_Db_Expr('NOW()')), 'iduser='.(int)$data->iduser );
				

				//icampus_Function_General_hooks::dispatchEvent('onUserLogin', array('userId' => $data->iduser));
				//moodle
				//$loginMoodle = '/data/htdocs/moodle/login.php';
				//if ( file_exists($loginMoodle) )
				//{
				//	$this->view->username = $this->_request->getPost('username');
				//	$this->view->password = $this->_request->getPost('password');
					
				//	$this->_helper->layout->disableLayout();
				//	echo $this->render('gateway');
				//	return;
				//}
				
				$loginMoodle = '/data/htdocs/moodle/login.php';
				if ( defined('MOODLE_LOGIN') && MOODLE_LOGIN == 1 )
				{
					//$this->authMoodle($this->_request->getPost('username'), $this->_request->getPost('password') );
					//$this->view->username = $this->_request->getPost('username');
					//$this->view->password = $this->_request->getPost('password');
					
					//$this->_helper->layout->disableLayout();
					//echo $this->render('gateway');
					//return;
				}

				if($auth->getIdentity()->IdRole == 1075) {
					$pd_status = $larrCommonModel->fnGetStaffpd($auth->getIdentity()->IdStaff);
					if($pd_status['Active'] == 0){
						$storage = new Zend_Auth_Storage_Session();
						$storage->clear();
						Zend_Layout::getMvcInstance()->assign('alertError', 'Access denied for Programme Director Portal');
					}else {
						$this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'index', 'action' => 'index'), 'default', false));
					}
				}else {

					if ($larrformData['redirect'] != '' && !preg_match("/login/", $larrformData['redirect'])) {
						$this->_redirect($larrformData['redirect'], array('prependBase' => false));
					} else {
						$this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'index', 'action' => 'index'), 'default', false));
					}

				}

			} else {
				$this->view->alertError = 'Login failed. Either username or password is incorrect';
				Zend_Layout::getMvcInstance()->assign('alertError', 'Login failed. Either username or password is incorrect');
			}
		}
	}
	
	protected function authMoodle($username='', $password='')
	{
		$url = 'http://mlms.inceif.org/logincurlhelper.php';
			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, 'username='.$username.'&password='.$password);
		curl_setopt ($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		// get http header for cookies
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		// forward current cookies to curl
		$cookies = array();
		foreach ($_COOKIE as $key => $value)
		{
		    if ($key != 'Array')
		    {
		        $cookies[] = $key . '=' . $value;
		    }
		}

	
		curl_setopt( $ch, CURLOPT_COOKIE, implode(';', $cookies) );
		// Stop session so curl can use the same session without conflicts
		session_write_close();
		$response = curl_exec($ch);
		curl_close($ch);
		// Session restart
		session_start();
		// Seperate header and body
		list($header, $body) = explode("\r\n\r\n", $response, 2);
		// extract cookies form curl and forward them to browser
		preg_match_all('/^(Set-Cookie:\s*[^\n]*)$/mi', $header, $cookies);
		
		foreach($cookies[0] AS $cookie)
		{
		     header($cookie, false);
		}
		//echo $body;
	}

	public function logoutAction() {
		Zend_Session:: namespaceUnset('sis');
		$storage = new Zend_Auth_Storage_Session();
		$storage->clear();
		//$this->_redirect($this->view->url(array('controller'=>'index', 'action'=>'login'),'default',true));
		$this->_redirect( $this->baseUrl . '/');
	}

	public function thesisPortalAction()
	{
		$auth = Zend_Auth::getInstance();
		$student = $auth->getIdentity();

		/*if ( !defined('THESIS_URL') )
		{
			die('THESIS_URL not defined');
		}*/

		$url = 'http://thesis.inceif.org';

		$data = array( 'username' => $student->loginName, 'password' => $student->passwd );

		$token  = urlencode( encrypt( json_encode($data) ) );
		$this->_redirect($url.'/authentication/validate/?token='.$token.'&type=1', array('prependBase' => false));
	}
}




