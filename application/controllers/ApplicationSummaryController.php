<?php

class ApplicationSummaryController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->Transaction = new App_Model_Application_DbTable_ApplicantTransaction();
        $this->Profile = new App_Model_Application_DbTable_ApplicantProfile();
        $this->Program = new App_Model_Application_DbTable_ApplicantProgram();
        $this->Health = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $this->Accomodation = new App_Model_Application_DbTable_ApplicantAccomodation();
        $this->Financial = new App_Model_Application_DbTable_ApplicantFinancial();
        $this->Education = new App_Model_Application_DbTable_ApplicantQualification();
        $this->Employment = new App_Model_Application_DbTable_ApplicantEmployment();
		$this->EnglishProficiency = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$this->Visa = new App_Model_Application_DbTable_ApplicantVisa();
		$this->QuantitativeProficiency = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();
		$this->Research = new App_Model_Application_DbTable_ApplicantResearchPublication();
        $this->Industry = new App_Model_Application_DbTable_ApplicantWorkingExperience();
        $this->appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $this->Template = new App_Model_General_DbTable_CommTemplate();
        $this->Referee = new App_Model_Application_DbTable_ApplicantReferees();
        $this->ChecklistVerification = new Application_Model_DbTable_ChecklistVerification();
    }

    function retrieve_related($trans_id) {

		$related['transaction'] = $this->Transaction->getTransaction($trans_id);
		$related['personal'] = $this->Profile->getApplicantProfile($trans_id);
                //var_dump($related['personal']); exit;
		$related['program'] = $this->Program->getProgrambyTxn($trans_id);

		$related['accomodation'] = $this->Accomodation->getDatabyTxn($trans_id);
		$related['education'] = $this->Education->getTransData(null, $trans_id);

		$related['employment'] = $this->Employment->getTransData (null,$trans_id);
		$related['english-proficiency'] = $this->EnglishProficiency->getData($trans_id);
		$related['financial-particulars'] = $this->Financial->getDataByTxn($trans_id);
		$related['health-condition'] = $this->Health->getData($trans_id);
		$related['industry-working-experience'] = $this->Industry->getInfo($trans_id);
		$related['visa'] = $this->Visa->getTransData ($trans_id);
		$related['quantitative'] = $this->QuantitativeProficiency->getQuantitativeData($trans_id);
		$related['research'] = $this->Research->getResearchData($trans_id);
		$related['referee'] = $this->Referee->getData ($trans_id);
		
		
		$programSchemeId = $related['program']['ap_prog_scheme'];
        $intake = $related['transaction']['at_intake'];
        $program_id = $related['personal']['program_id'];
        
		$related['initial-registration'] = $this->getLandscape($program_id,$programSchemeId,$intake);
		
                $related['essay'] = array();
		
		return($related);
	}
	
	function getLandscape($program_id,$programSchemeId,$intake){
		
		$landscapeDB = new App_Model_General_DbTable_Landscape();
        $subject_list = $landscapeDB->getLandscapeSubject($program_id,$programSchemeId,$intake);
                                        $landscapeInfo = $landscapeDB->getLandscapeInfo($program_id,$programSchemeId,$intake);
        $tagInfo = $landscapeDB->getLandscapeSubjectTag($landscapeInfo['IdLandscape']);
                                        $tagEncode = json_decode($tagInfo['subjectdata'],true);
                                        $intakeInfo = $landscapeDB->getIntakeInfo($intake);
                                        $semInfo = $landscapeDB->getSemesterInfo($intakeInfo['sem_year'], $intakeInfo['sem_seq']);
                                        //$subject_list = $landscapeDB->getSubject();
                                        
                                        $i = 0;
                                        $subject_list2=null;
                                        foreach ($subject_list as $loop){
                                            if (isset($tagEncode[$loop['IdSubject']])){
                                                //var_dump($tagEncode[$loop['IdSubject']]);
                                                if (isset($tagEncode[$loop['IdSubject']][$semInfo['SemesterType']])){
                                                        $subject_list2[$i] = $loop;
                                                        $i++;
                                                }
                                            }
                                        }
                                        
                                        $table = '<table border=0 width="100%" class="table">
        			<tr>
        			   <th>'.$this->view->translate("No").'</th>
        			   <th>'.$this->view->translate("Subject Code").'</th>
        			   <th align="left">'.$this->view->translate("Subject Name").'</th>
        			   <th>'.$this->view->translate("Credit Hour").'</th>		        			
        			</tr>';
        if($subject_list2){
	        foreach ($subject_list2 as $index=>$subject) {        	
	        	$no = $index+1;
	        	$table .= "<tr>
	        				<td align='center'>".$no."</td>
	        				<td align='center'>".$subject['SubCode']."</td>
	        				<td align='left'>".$subject['SubjectName']."</td>
	        				<td align='center'>".$subject['CreditHours']."</td>";	
	        	$table .= "</tr>";
	        }
        }
        $table .= '</table>';	
        
        return $table;
        
	}

	function indexAction() {
		
		$this->_helper->layout->disableLayout();
		$auth = Zend_Auth::getInstance();
		$appl_id = $auth->getIdentity()->appl_id; 
		
		//$transID = $auth->getIdentity()->transaction_id;
		$trans_id = $this->_request->getParam('trans_id');
		if(!empty($trans_id)) {
			$transID = $trans_id;
		}
		
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appProgram = $appProfileDB->getProfileProgram($transID);

		if( $appProgram['ap_prog_scheme'] != '' || $appProgram['appl_category'] != '' ) {
			$sectionDb = new App_Model_General_DbTable_ApplicationSection();
			$sidebarList = $sectionDb->getSection($appProgram['ap_prog_id'],$appProgram['ap_prog_scheme'],$appProgram['appl_category']);
		}else{
			$sidebarList = array();
		}
		
		$appSectionStatusDB = new App_Model_Application_DbTable_ApplicantSectionStatus();
		$pd_section = $appSectionStatusDB->getStatus($transID,1);

		$related = $this->retrieve_related($transID);

		$program = $this->Program->getProgrambyTxn($transID);
        $profile = $this->Profile->getApplicantProfile($transID);
        $programSchemeId = $program['ap_prog_scheme'];
        $stdCtgy = $profile['appl_category'];
        
        $this->view->guidelines = $this->Template->retrieve_template('application-form-guidelines');
		$this->view->declaration = $this->Template->retrieve_template('application-form-declaration');
        //get the stuff they need checking
        $this->view->docchecklist = $this->ChecklistVerification->fnGetDocCheckList($programSchemeId, $stdCtgy);
        

		$this->view->appProgram = $appProgram;
		$this->view->items = $sidebarList;
		$this->view->related = $related;
		$this->view->transID = $transID;

	}

	function printmeAction() {

		//set unlimited
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		
		$auth = Zend_Auth::getInstance();
		//$appl_id = $this->_request->getParam('appl_id');
		$transID = $this->_request->getParam('trans_id');
		
		$download = $this->view->download = $this->_request->getParam('download',0);
                
		if ( $download == 0 ) {
			$this->_helper->layout()->setLayout('/printstudentsummary');
		}
		else
		{
                        //$this->_helper->layout()->setLayout('/printstudentsummary');
			$this->_helper->layout->disableLayout();
		}

		$this->view->base_path = realpath(APPLICATION_PATH . '../../public/');
	
		$GLOBALS['item_count'] = 0;

		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appProgram = $appProfileDB->getProfileProgram($transID);

		$appTransact  = new App_Model_Application_DbTable_ApplicantTransaction();
		$transaction = $appTransact->getTransaction($transID);
		
		$this->view->trans_id = $transaction['at_pes_id'];
                
		if($appProgram['ap_prog_scheme']!='' || $appProgram['appl_category']!=''){
			$sectionDb = new App_Model_General_DbTable_ApplicationSection();
			$sidebarList = $sectionDb->getSection($appProgram['ap_prog_id'],$appProgram['ap_prog_scheme'],$appProgram['appl_category']);
		}else{
			$sidebarList = array();
		}
		
		$appSectionStatusDB = new App_Model_Application_DbTable_ApplicantSectionStatus();
		$pd_section = $appSectionStatusDB->getStatus($transID,1);

		$related = $this->retrieve_related($transID);

		$this->view->guidelines = $this->Template->retrieve_template('application-form-guidelines');
		//$this->view->declaration = $this->Template->retrieve_template('application-form-declaration');
                $this->view->declaration = $this->Template->retrieve_template_v2($appProgram['ap_prog_scheme'], $appProgram['appl_category']);
                //var_dump($this->view->declaration); exit;
		$program = $related['program'];
        $profile = $related['personal'];
        $transaction = $related['transaction'];
        
        $programSchemeId = $program['ap_prog_scheme'];
        $stdCtgy = $profile['appl_category'];
        $intake = $transaction['at_intake'];
        $program_id = $profile['program_id'];
        
        
        //var_dump($related); exit;
        //get the stuff they need checking
        $this->view->docchecklist = $this->ChecklistVerification->fnGetDocCheckList($programSchemeId, $stdCtgy, $transID);

		$this->view->appProgram = $appProgram;
		$this->view->items = $sidebarList;
		$this->view->related = $related;
		$this->view->transID = $transID;
		$this->view->transaction = $transaction;
	}




}