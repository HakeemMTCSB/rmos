<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 29/10/2015
 * Time: 3:18 PM
 */
class ProfileController extends Zend_Controller_Action {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new App_Model_Profile();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Profile');
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $profile = $this->model->getProfile($userId);
        $this->view->profile = $profile;
    }
}