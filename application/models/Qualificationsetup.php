<?php
class App_Model_Qualificationsetup extends Zend_Db_Table {
	protected $_name = 'tbl_qualificationmaster'; // table name
	private $lobjDbAdpt;

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddQualification($larrformData) {
		unset($larrformData['IdSubject']);
		$this->insert($larrformData);
		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'IdQualification');
    return $insertId;
	}

	public function fngetQualificationDetails() { //Function to get the Qualification details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from($this->_name);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetQualificationDetail(){//Function to get the subject semester list
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_qualificationmaster'),array("key"=>"a.IdQualification","value"=>"a.QualificationLevel"));
		 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchQualification($post = array()) { //Function for searching the Qualification details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("qm"=>"tbl_qualificationmaster"),array("qm.*"))
		->where('qm.QualificationLevel like "%" ? "%"',$post['field3'])
		->where('qm.QualificationRank like "%" ? "%"',$post['field2'])
		->order("qm.QualificationLevel");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnupdateQualification($formData,$lintIdQualification) { //Function for updating the Qualification details
		unset ( $formData ['Save'] );
		unset ( $formData ['IdSubject'] );
		$where = 'idQualification= '.$lintIdQualification;
		$this->update($formData,$where);
	}

	public function fnDeleteQualification($IdQualification) {  // function to delete qualification details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_qualificationmaster';
		$where = $db->quoteInto('idQualification = ?', $IdQualification);
		$db->delete('tbl_qualificationmaster', $where);
	}

	public function fngetQualificationList(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_qualificationmaster"),array("key"=>"a.IdQualification","value"=>"a.QualificationLevel"))
		->where('a.Active = ?',1)
                ->order("a.QualificationLevel");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

}