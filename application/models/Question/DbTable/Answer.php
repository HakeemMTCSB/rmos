<?php 
class App_Model_Question_DbTable_Answer extends Zend_Db_Table_Abstract
{
    protected $_name = 'q006_answer';
	protected $_primary = "id";
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('u'=>$this->_name))
	                 ->where('u.'.$this->_primary.' = ' .$id);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
        
		}else{
			$select = $db->select()
	                 ->from($this->_name);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	        $row =  $row->toArray();
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}
		
		return $row;
	}
	
	
	public function getTopic($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 echo  $select = $db->select()
		             ->from(array('s' => $this->_name))		
					 ->where('s.idPool='.$id);
	 
        $row = $db->fetchAll($select);
	      
		return $row;
	}
	
	public function getAnswerbyQuestion($idQuestion){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db->select()
		             ->from(array('a' => $this->_name))		
//					 ->where('a.question_main_id='.$idMainQues)
					 ->where('a.question_id='.$idQuestion);
	 
        $row = $db->fetchAll($select);
	      
		return $row;
	}
	
public function getCorrectAnswer($idQuestion){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db->select()
		             ->from(array('a' => $this->_name))		
					 //->where('a.question_main_id='.$idMainQues)
					 ->where('a.question_id='.$idQuestion)
					 ->where('a.correct_answer = 1');
	 
        $row = $db->fetchRow($select);
	      
		return $row;
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db ->select()
							->from($this->_name);
		
		return $selectData;
	}
	
	public function addData($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$this->insert($data);
		$id = $db->lastInsertId();
        
        return $id;
	}
	
	public function updateData($data,$id){
		
		$this->update($data, $this->_primary . " = '" . (int)$id ."'");
	}
	
	public function deleteData($id){
		$this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function deleteAnswer($qid){
		$this->delete('qid =' . (int)$qid);
	}
	

	
public function getAnswerValuebyQuestion($idGroup,$idIntake,$mainSetID,$poolID,$idQuestion){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		/*SELECT sum(d.value) as total  FROM `r024_student_course_registration` a 
join r015_student b on b.id = a.student_id
join q006_answersetstudent c on c.student_id = b.id

left join q006_answerstudent d on d.student_id = b.id and d.setanswer_id = c.id

WHERE a.IdCourseTaggingGroup = 20
and b.admission_intake_id = 2
and c.pool_id = 1
and c.sectiontagging_id = 51
and d.question_id = 1*/
		
		$select = $db->select()
				 ->from(array('a' => 'q006_answersetstudent'),array())
				 ->joinLeft(array('b'=>'q006_answerstudent'),'b.setanswer_id = a.id',array('total'=>'sum(b.value)'))	
				 ->join(array('c'=>'r015_student'),'c.id = a.student_id',array())
	             ->where('a.sectiontagging_id =?',$mainSetID)
	             ->where('a.pool_id =?',$poolID)
	             ->where('a.coursegroup_id =?',$idGroup)
	             ->where('b.question_id =?',$idQuestion)
	             ->where('c.admission_intake_id =?',$idIntake)
	             ->where('a.status = 1');
	             
	             $row = $db->fetchRow($select);
	             
	     $select2 = $db->select()
				 ->from(array('a' => 'q006_answersetstudent'),array())
				 ->joinLeft(array('b'=>'q006_answerstudent'),'b.setanswer_id = a.id',array('*'))	
				 ->join(array('c'=>'r015_student'),'c.id = a.student_id',array())
	             ->where('a.sectiontagging_id =?',$mainSetID)
	             ->where('a.pool_id =?',$poolID)
	             ->where('a.coursegroup_id =?',$idGroup)
	             ->where('b.question_id =?',$idQuestion)
	             ->where('c.admission_intake_id =?',$idIntake)
	             ->where('a.status = 1');
	             
			$row2 = $db->fetchAll($select2);	 
				 
         $dataRow = array(
         	'totalStud'=>count($row2),
         	'value'=>$row['total']
         );
         
//         echo "<pre>";
//         print_r($dataRow);
         
		return $dataRow;
	}
	
 	public function getAnswerEssay($idGroup,$idIntake,$idQuestion){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
                  ->from(array('a'=>'tbl_course_group_student_mapping'),array())
                  ->joinLeft(array('c'=>'r015_student'),'c.id = a.IdStudent',array())
                  ->joinLeft(array('d'=>'q006_answerstudent'),'d.student_id = c.id
                     AND d.coursegroup_id = a.IdCourseTaggingGroup')
 				  ->where('a.IdCourseTaggingGroup=?',$idGroup)
 				  ->where('d.question_id=?',$idQuestion)
				  ->where('c.admission_intake_id=?',$idIntake)
				  ->where('d.valueText!=?','tiada','Tiada','-tiada-','-Tiada-')
				  ->where('d.valueText!=?','no komen')
				  ->where('d.valueText!=?','-');
				  
				  
				 /* SELECT *
FROM tbl_course_group_student_mapping a
JOIN r015_student c ON c.id = a.IdStudent
JOIN q006_answerstudent d ON d.student_id = c.id
AND d.coursegroup_id = a.IdCourseTaggingGroup
WHERE c.admission_intake_id =3
AND a.IdCourseTaggingGroup =10
AND d.question_id =1*/

//		exit;


		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
}
?>