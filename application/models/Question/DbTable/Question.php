<?php 
Class App_Model_Question_DbTable_Question extends Zend_Db_Table_Abstract
{

    protected $_name = 'q003_question';
	protected $_primary = "id";

	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('u'=>$this->_name))
	                 ->where('u.'.$this->_primary.' = ' .$id);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
        
		}else{
			$select = $db->select()
	                 ->from($this->_name);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	        $row =  $row->toArray();
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}
//		echo "select";
		return $row;
	}
	
	//dapatkan soalan untuk display view question
	public function getQuestion($condition=null){	
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		  
		$select = $db->select()
		             ->from(array('q'=>'q003_question'))		
		             ->joinLeft(array('c' =>'q002_chapter'),'c.id = q.pool_id',array('topic_name'=>'c.name'))             		            
//		             ->joinLeft(array('qt'=>'q005_question_type'),'qt.id = c.question_type',array('question_name'=>'qt.name'))	
		             ->order('c.id asc');
		             
		if($condition!=null){
			if(isset($condition["pool_id"])){
				if($condition["pool_id"]!=""){
				$select->where('q.pool_id='.$condition["pool_id"]);
				}
			}
			
			if(isset($condition["question"]) ){	
				if($condition["question"]!="")
				$select -> where ('question='.$condition ["question"]);		
				
				}
			}
						
			if(isset($condition["qid"]) ){
				if($condition["qid"]!="")
				$select->where('q.id='.$condition["qid"]);
			}

		if(isset($condition["qid"])) {
			$row = $db->fetchRow($select);  
		}else{
        	$row = $db->fetchAll($select);  
		}      
		return $row;
	}
	
	//dapatkan soalan untuk manage questiontagging
	public function getmanageQuestion($condition=null){	
	$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
		             ->from(array('q'=>'q003_question'), array('*','qid'=>"q.id"))		
		             ->joinLeft(array('c' =>'q002_chapter'),'c.id = q.id',array('main_setID'=>"c.id")) 
		             ->order('c.id');
		             
		if($condition!=null){
			if(isset($condition["pool_id"])){
				if($condition["pool_id"]!=""){
				$select->where('q.pool_id='.$condition["pool_id"]);
				}
			}	
			
			if(isset($condition["question"]) ){	
				if($condition["question"]!="")
				$select -> where ('question='.$condition ["question"]);		
				
				}
			}
						
			if(isset($condition["qid"]) ){
				if($condition["qid"]!="")
				$select->where('q.id='.$condition["qid"]);
			}

		if(isset($condition["qid"])) {
			$row = $db->fetchRow($select);  
		}else{
        	$row = $db->fetchAll($select);  
		} 
//         echo $select;
		return $row;
	}
	
	public function getTotalQuestion($condition=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		  $select = $db->select()
		             ->from(array('q'=>'q003_question'))		
		             ->joinLeft(array('c' =>'q002_chapter'),'c.id = q.pool_id',array('topic_name'=>'c.name'));            		            
//		             ->joinLeft(array('qt'=>'q005_question_type'),'qt.id = q.rubric_id',array('question_name'=>'qt.name'))	;             		            
					
	 
		if($condition!=null){
			if(isset($condition["pool_id"])){
				$select->where('q.pool_id='.$condition["pool_id"]);
			}
			
			if(isset($condition["qid"])){
				$select->where('q.id='.$condition["qid"]);
			}
		}
		if(isset($condition["qid"])) {
			$row = $db->fetchRow($select);  
		}else{
        	$row = $db->fetchAll($select);  
		}      
	      
		return count($row);
	}
	
// guna dekat online survey
	public function getQuestionPool($pool_id,$type=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
		             ->from(array('q'=>'q003_question'))
		             ->joinLeft(array('p'=>'q001_pool'),'p.id = q.pool_id',array('pool_name'=>'p.name','pool_id'=>'p.id'))	
		             ->joinLeft (array('d'=>'q002_chapter'),'d.id = q.id',array('chapter_id'=>'d.name'))	            
					 ->where('q.pool_id='.$pool_id);
//		if($type){$select->where('d.question_type =?',$type);}
//	 echo $select;
	 
        $row = $db->fetchAll($select);
		return $row;
	}

	public function getPaginateData(){
		
		$db = Zend_Db_Table::getDefaultAdapter();

			 $select = $db ->select()
						  ->from('q001_pool');	
	 return $select;
	}
	
	
	public function addData($data){		
//		print_r($data);exit;
	    $db = Zend_Db_Table::getDefaultAdapter();
		$this->insert($data);
		$id = $db->lastInsertId();
        
        return $id;
	}
	
	public function updateData($data,$id){	
//	print_r($data);exit;	
		$this->update($data, $this->_primary .' =' . (int)$id);
	}
	
	public function deleteData($id){
//		print_r($id);exit;

		$this->delete($this->_primary .' =' . (int)$id);
	}
	
	
public function getDataByID($qid){
	
	   $db = Zend_Db_Table::getDefaultAdapter();
		
	   $select = $db->select()  
	                ->from(array('a'=>$this->_name),array("qid"=>"a.id","pool_id"=>"a.pool_id",'status'=>"a.status","question"=>"a.question"))
					->joinLeft(array('b'=>'q001_pool'),'b.id = a.pool_id',array('idValue'=>'b.id','poolname'=>'b.name'))				   
	                ->where("a.id = ?",$qid);
	                 
      return $row = $db->fetchRow($select);
	   
	}

//	public function getListQuestion($pool_id){	
//		
//		
//		$db = Zend_Db_Table::getDefaultAdapter();
//		
//		  
//		echo $select = $db->select()
//		             ->from(array('q'=>'q003_question'))		
////		             ->joinLeft(array('c' =>'q002_chapter'),'c.id = q.pool_id',array('topic_name'=>'c.name'))             		            
//		             ->where ('q.pool_id=?',$pool_id);
//	
//	}
}
?>
