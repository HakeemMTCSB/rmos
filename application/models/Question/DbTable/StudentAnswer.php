<?php 
class App_Model_Question_DbTable_StudentAnswer extends Zend_Db_Table_Abstract
{
    protected $_name = 'q010_student_answer';
	protected $_primary = "id";

	protected $_referenceMap    = array(
        'Student' => array(
            'columns'           => 'student_id',
            'refTableClass'     => 'App_Model_Record_DbTable_StudentRegistration',
            'refColumns'        => 'IdStudentRegistration'
        ),
        'Lecturer' => array(
            'columns'           => 'IdLecturer',
            'refTableClass'     => 'GeneralSetup_Model_DbTable_Staffmaster',
            'refColumns'        => 'IdStaff'
        ),
        'Course' => array(
            'columns'           => 'IdSubject',
            'refTableClass'     => 'GeneralSetup_Model_DbTable_Subjectmaster',
            'refColumns'        => 'IdSubject'
        )
    );
	
	public function get_answer( $main_set_id, $IdSubject, $student_id, $chapter_id, $question_id, $IdLecturer = null ) {
		$select = $this->select()
					->where('main_set_id = ?', $main_set_id)
					->where('IdSubject = ?', $IdSubject )
					->where('student_id = ?', $student_id )
					->where('chapter_id = ?', $chapter_id )
					->where('question_id = ?', $question_id )
					;
		if(!empty($IdLecturer)) {
			$select->where('IdLecturer = ?', $IdLecturer );
		}

		$answer = $this->fetchRow($select);
		return($answer);
					
	}

	public function save_answer( $main_set_id, $IdSubject, $student_id, $chapter_id, $question_id, $answer, $IdLecturer = null ) {
		$data['main_set_id'] = $main_set_id;
		$data['IdSubject'] = $IdSubject;
		$data['student_id'] = $student_id;
		$data['chapter_id'] = $chapter_id;
		$data['question_id'] = $question_id;
		$data['created_at'] = date('Y-m-d');
		if(!empty($IdLecturer)) {
			$data['IdLecturer'] = $IdLecturer;
		}

		if (is_numeric($answer)) {
			$data['answer'] = $answer;
		} else {
			$data['answer_text'] = $answer;
		}

		$data['created_by'] = 0;

		$this->insert($data);

	}

	public function get_answered($student_id, $main_set_id, $IdSubject, $IdLecturer = null) {
		$select = $this->select()
					->from($this->_name, array('main_set_id', 'IdSubject','IdLecturer'))
					->distinct()
					->where('student_id = ?', $student_id)
					->where('IdSubject = ?', $IdSubject)
					->where('main_set_id = ?', $main_set_id)
					;

		if(!empty($IdLecturer)) {
			$select->where('IdLecturer = ?', $IdLecturer );
		} else {
			$select->where('IdLecturer IS NULL');
		}
		$answers = $this->fetchAll($select);
		
		return($answers);
	}

	public function get_all_aswers($main_set_id, $pool_id=0) {
		$select = $this->select()
					->distinct()
					->from($this->_name, array( 'student_id', 'main_set_id', 'IdSubject','IdLecturer'))
					->where('main_set_id = ?', $main_set_id)
					//->where('pool_id = ?', $pool_id)
					;
		$answers = $this->fetchAll($select);
		
		return($answers);
	}

	public function answeredByStudent($student_id, $main_set_id, $pool_id) {
		$select = $this->select()
					->distinct()
					->from($this->_name, array( '*'))
					->where('main_set_id = ?', $main_set_id)
					->where('pool_id = ?', $pool_id)
					->where('student_id = ?', $student_id)
					;
		$answers = $this->fetchAll($select);
		
		return($answers);

	}
	
	public function setAnswerStudent($main_set_id, $pool_id,$semester_id, $lec, $sub) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
					->from(array('a'=>'q010_student_answer_main'),array("*",'idSet'=>'a.id'))
					->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName'))
					->joinLeft(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
					->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId'))
					->joinLeft(array('StudentProfile' => 'student_profile'), "srs.sp_id = StudentProfile.id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name",'StudentProfile.*' ))
					->where('main_set_id in (?)', explode(',', $main_set_id))
					->where('pool_id = ?', $pool_id)
					->where('semester_id = ?', $semester_id)
					;

		if ($lec != ''){
			$select->where('a.IdLecturer = ?', $lec);
		}

		if ($sub != ''){
			$select->where('a.IdSubject = ?', $sub);
		}
		//echo $select.'<br/><br/>';
		$answers = $db->fetchAll($select);
		
		return($answers);

	}
	
	public function getSearchDataReportByCourse($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$programscheme = $searchData['programscheme'];

		$selectData = $db->select()
						->from(array('a'=>'tbl_course_tagging_group'))
						->joinLeft(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
						->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName','IdSubject'))
						->joinLeft(array('sr'=>'tbl_studentregsubjects'),'sr.IdCourseTaggingGroup = a.IdCourseTaggingGroup',array())
						->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('registrationId','IdProgram'))
						->group('a.IdCourseTaggingGroup')
						//->group('srs.IdProgramScheme')
						->order('c.FirstName asc');

		if($program!='ALL' && $program!=''){
			$selectData->where("srs.IdProgram = ?",$program);
		}

		if($programscheme!='ALL' && $programscheme!=''){
			$selectData->where("srs.IdProgramScheme = ?",$programscheme);
		}

		if($semester!='ALL' && $semester!=''){
			$selectData->where("sr.IdSemesterMain = ?",$semester);
		}
		$row = $db->fetchAll($selectData);

		if($row){
			$rowData = array();
			$totalResponse = 0;
			foreach($row as $key=>$data){
				$data['IdSubject'] = $data['IdSubject']==null || $data['IdSubject']=='' ? 0:$data['IdSubject'];
				$rowData[$data['IdSubject']]['IdSubject'] = $data['IdSubject']==null || $data['IdSubject']=='' ? 0:$data['IdSubject'];
				$rowData[$data['IdSubject']]['SubCode'] = $data['SubCode'];
				$rowData[$data['IdSubject']]['SubjectName'] = $data['SubjectName'];
				
				//get total student registered
				$selectData2 = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
							->where("a.IdSubject = ?",$data['IdSubject'])
							->where("a.IdCourseTaggingGroup = ?",$data['IdCourseTaggingGroup']);

				if($program!='ALL' && $program!=''){
					$selectData2->where("srs.IdProgram = ?",$program);
				}

				if($semester!='ALL' && $semester!=''){
					$selectData2->where("a.IdSemesterMain = ?",$semester);
				}

				if($programscheme!='ALL' && $programscheme!=''){
					$selectData2->where("srs.IdProgramScheme = ?",$programscheme);
				}

				$row2 = $db->fetchAll($selectData2);
				$totalregistered = count($row2);
				$rowData[$data['IdSubject']]['total_registered'][$key] = $totalregistered;
				
				//get total student response
				$totalResponse = 0;
				$studentData = array();
				
				foreach($row2 as $reg){
					$studentData = array(
						'IdBranch'=> $reg['IdBranch'],
						'IdProgram'=> $reg['IdProgram'],
						'IdProgramScheme'=> $reg['IdProgramScheme'],
						'IdStudentRegistration'=> $reg['IdStudentRegistration'],
					);
		
				$checkCompleteTer = $this->getTerCompleteStatus($studentData,$semester);
				if($checkCompleteTer == 0){
					$totalResponse++;
				}

				$rowData[$data['IdSubject']]['total_response'][$key] = $totalResponse;
				}
				
			}
		}
					
		return $rowData;
	}
	
	public function getSearchDataReportSummary($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		$scheme = $searchData['scheme'];
	

		$selectData = $db->select()
							->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','SubCode'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )','SubjectName'))
							->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
							->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array('program_id'=>'GROUP_CONCAT( DISTINCT(cgp.program_id))','program_scheme_id'=>'GROUP_CONCAT( DISTINCT(cgp.program_scheme_id))'))
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
							->joinLeft(array('cdef'=>'tbl_definationms'),'cdef.IdDefinition = ps.mode_of_program',array('modeName'=>'cdef.DefinitionDesc'))
							->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'GROUP_CONCAT( DISTINCT(br.BranchCode) SEPARATOR "/" )'))
							->join(array('sc'=>'q020_sectiontagging'), 'sc.branch_id = tg.IdBranch AND sc.semester_id = tg.IdSemester AND sc.program_id =cgp.program_id AND sc.IdProgramScheme = cgp.program_scheme_id and parent_id !=0 ',array('parent_id'))
							//->group('sm.Group')
							//->order('c.FullName asc')
							;

			if(isset($program) && !empty($program)){
				$selectData->where("cgp.program_id IN (?)",$program);
			}
			
			if(isset($semester) && !empty($semester)){
				$selectData->where("tg.IdSemester IN (?)",$semester);
			}
			
			if(isset($branch) && !empty($branch)){
				$selectData->where("tg.IdBranch IN (?)",$branch);
			}
			
			if($mop != 'ALL'){
				$selectData->where("ps.mode_of_program = ?",$mop);
			}

		$selectData .= ' GROUP BY CASE sm.Group WHEN 0 THEN rand(sm.IdSubject) ELSE sm.Group END ORDER BY c.FullName asc';
		
//		echo $selectData.'<br>';
//		exit;
		$row = $db->fetchAll($selectData);
		//var_dump($row); //exit;
		//get total section
//		$totalSectionResult = $this->getSectionCategory($searchData);
//		$totalSection = count($totalSectionResult);
		
		if($row){
			
			foreach($row as $key=>$data){
				//dd($data);
				//get total student registered
					$selectData2 = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
							->where('a.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(a.exam_status,'x') NOT IN ('CT','EX','U')")
							->where("srs.IdProgram IN (?)", $program)
							//->where("ps.mode_of_program IN ($mop)")
							//->where("a.IdSemesterMain = ?",$semester)
							->where("a.IdSubject IN (?)", explode(',', $data['IdSubject']))
							->where("a.IdCourseTaggingGroup IN (?)", explode(',', $data['IdCourseTaggingGroup']));
                                        
                     $selectData5 = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
							->where('a.audit_program IS NOT NULL')
                                                        ->where('a.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(a.exam_status,'x') IN ('U')")
							->where("srs.IdProgram IN (?)", $program)
							//->where("ps.mode_of_program IN ($mop)")
							//->where("a.IdSemesterMain = ?",$semester)
							->where("a.IdSubject IN (?)", explode(',', $data['IdSubject']))
						 	->where("a.IdCourseTaggingGroup IN (?)", explode(',', $data['IdCourseTaggingGroup']));
                                        
                                        $select6 = $db->select()
                                            ->union(array($selectData2, $selectData5));
                                        
//					echo $selectData2;exit;
					$row2 = $db->fetchAll($select6);

					$row[$key]['total_registered'] = count($row2);

				/*if (trim($data['FullName'])=='Kartini Yusoff'){
					echo $selectData2;
					var_dump(explode(',', $data['IdSubject']));
					var_dump(explode(',', $data['IdCourseTaggingGroup']));
					var_dump($data);
					var_dump($row2); exit;
				}*/
					
					//get student list
//					$respondens = $this->getTotalResponseListStudent($searchData,$data,$totalSection);
//					
//					$forIn = implode(",",$respondens);
                                        //var_dump($row2);
					$totalResponse = 0;
					foreach($row2 as $reg){
						$selectPs = $db->select()
							->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
							->where('a.IdProgram IN (?)', $program)
							->where('a.mode_of_program = ?', $mop);

						$psresult = $db->fetchAll($selectPs);

						$psarr = array();
						//dd($psresult);
						if ($psresult){
							foreach ($psresult as $psresultLoop){
								$psarr[] = $psresultLoop['IdProgramScheme'];
							}
						}

						$ps = $reg['exam_status']=='U' ? $reg['audit_programscheme']:$reg['IdProgramScheme'];

						$studentData = array(
							'IdBranch'=> $reg['IdBranch'],
							//'IdProgram'=> $reg['exam_status']=='U' ? $reg['audit_program']:$reg['IdProgram'],
							'IdProgram'=> $program,
							//'IdProgramScheme'=> $reg['exam_status']=='U' ? $reg['audit_programscheme']:$reg['IdProgramScheme'],
							'IdProgramScheme'=> in_array($ps, $psarr) ? $ps:$psresult[0]['IdProgramScheme'],
							'IdStudentRegistration'=> $reg['IdStudentRegistration'],
						);

						//dd($studentData);

						/*$studentData = array(
							'IdBranch'=> $reg['IdBranch'],
							'IdProgram'=> $reg['exam_status']=='U' ? $reg['audit_program']:$reg['IdProgram'],
							'IdProgramScheme'=> $reg['exam_status']=='U' ? $reg['audit_programscheme']:$reg['IdProgramScheme'],
							'IdStudentRegistration'=> $reg['IdStudentRegistration'],
						);*/
                                                
                                               /* $studentData = array(
							'IdBranch'=> $branch,
							'IdProgram'=> $reg['IdProgram'],
							'IdProgramScheme'=> $reg['IdProgramScheme'],
							'IdStudentRegistration'=> $reg['IdStudentRegistration'],
						);*/
					
					$checkCompleteTer = $this->getTerCompleteStatus($studentData,$semester,$data['IdSubject'],$data['IdCourseTaggingGroup']);
                    if($checkCompleteTer == 0){
						$totalResponse++;
					}
					
				}
				
				$row[$key]['total_response'] = $totalResponse;
					
				$searchData = array(
					'program'=> $program,
					'semester'=> $semester,
					'scheme'=> $scheme,
					'mop'=> $mop,
				);
				$IdProgram= implode(',',$program);		
				$selectDataParent = $db->select()
							->from(array('a'=>'q020_sectiontagging'),array('parent_id'=>'group_concat(distinct parent_id)'))
//							->joinLeft(array('b'=>'tbl_course_tagging_group'),'a.branch_id = b.IdBranch and a.semester_id = b.IdSemester and ',array())
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = a.IdProgramScheme',array('mode_of_program'))
						
//							->where("b.IdSubject = ?",$data['IdSubject'])
							->where("ps.mode_of_program IN ($mop)")
							->where("a.semester_id IN (?)",$semester)
//							->where("a.program_id IN ($IdProgram)")
							->where('a.parent_id != 0');
//							->group('a.parent_id');
//							->where("b.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])");

							$poolData = $db->fetchRow( $selectDataParent );
//				exit;			
//				$poolData = $this->getPoolListParent($selectDataParent);
//				$parentid = array();
//				foreach($poolData as $m=>$pl){
//					$parentid[] = $pl['parent_id'];
//				}
				
				
				//get list pool
				$parentid =$poolData['parent_id'];
//				exit;
				$schemeId = $scheme;
				$mopId = $mop;
				
				$subjectId = $data['IdSubject'];
				$IdCourseTaggingGroup = $data['IdCourseTaggingGroup'];

				$row[$key]['parent_id'] = $parentid;
				$dataArray = $this->getTotalResponseData($parentid,$IdCourseTaggingGroup,$schemeId,$mopId,$IdProgram,$subjectId);

				//get set form
				/*$searchData = array(
					'program'=>$program,
					'branch'=>$branch,
					'mop'=>$mop,
					'semester'=> $semester
				);
				$setForm = $this->getSetForm($searchData);

				$main_set_id = array();
				foreach($setForm as $st){
					$main_set_id[] = $st['id'];
				}

				$totalAnswerSubmitted = $this->getAnswerSubmit($IdCourseTaggingGroup,$program, $branch,$semester,$subjectId, $main_set_id);
				$row[$key]['totalw'] = $totalAnswerSubmitted;*/

				$row[$key]['data'] = $dataArray['summary'];
//				$poolData = $this->getPoolList($searchData);
				
//				echo "<pre>";
//				print_r($poolData);
//				exit;
				
				/*$main_set_id = array();
				$k=0;
				foreach($poolData as $m=>$pl){
					
					/*$main_set_id[$k] = $pl['main_set_id'];
					
					$row[$key]['pool'][$pl['question_pool']]['pool_id']=$pl['question_pool'];
					$row[$key]['pool'][$pl['question_pool']]['poolname']=$pl['name'];
					$row[$key]['pool'][$pl['question_pool']]['main_set_id']=$main_set_id;
					
					$row[$key]['pool'][$m]['main_set_id'] = $pl['main_set_id'];
					
					$row[$key]['pool'][$m]['pool_id'] = $pl['question_pool'];
					$row[$key]['pool'][$m]['poolname'] = $pl['name'];
					
					
					
				/*$selectData = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'=>'SUM(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
//				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array())
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id',array())
				->join(array('srd'=>'tbl_studentregsubjects_detail'),'srd.student_id = a.student_id AND srd.regsub_id = sr.IdStudentRegSubjects AND srd.subject_id = a.IdSubject AND srd.semester_id = a.semester_id',array())
				->where('b.answer IS NOT NULL')
				->where("c.program_id IN (?)",$program)
				->where("c.branch_id IN (?)",$branch)
				->where("a.semester_id = ?",$semester)
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN (?)",$main_set_id)
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1');

				if($pool_id){
					$selectData->where("a.pool_id = ?",$pool_id);
				}
				
				if($registration_item_id){
					$selectData->where("srd.item_id = ?",$registration_item_id);
				}
				
				if($question_id){
					$selectData->where("b.question_id = ?",$question_id);
				}
				
				if($rubricWeightage){
					$selectData->where("b.answer = ?",$rubricWeightage);
				}
		
					
					//get total student response
					$selectData3 = $db->select()
							->from(array('a'=>'q010_student_answer_main'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId','IdProgram'))
							->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
							->where('sr.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
							->joinLeft(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'))
							->where("srs.IdProgram IN (?)",$program)
							->where('b.answer IS NOT NULL')
							->where("a.semester_id = ?",$semester)
							->where("a.IdSubject IN ($data[IdSubject])")
							->where("a.main_set_id IN ($pl[main_set_id])")
							->where("b.chapter_id IN ($pl[chapterid])")
							->where("sr.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])")
							->where("a.pool_id = ?",$pl['question_pool']);
//							->where("a.student_id IN (?)",$forIn);
                                        
                                        $selectData4 = $db->select()
							->from(array('a'=>'q010_student_answer_main'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId','IdProgram'))
							->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
							->where('sr.audit_program IS NOT NULL')
                                                        ->where('sr.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(sr.exam_status,'x') IN ('U')")
							->joinLeft(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'))
							->where("srs.IdProgram IN (?)",$program)
							->where('b.answer IS NOT NULL')
							->where("a.semester_id = ?",$semester)
							->where("a.IdSubject IN ($data[IdSubject])")
							->where("a.main_set_id IN ($pl[main_set_id])")
							->where("b.chapter_id IN ($pl[chapterid])")
							->where("sr.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])")
							->where("a.pool_id = ?",$pl['question_pool']);
                                        
                                        $select5 = $db->select()
				    		       ->union(array($selectData3, $selectData4));
                                        
					$row3 = $db->fetchAll($select5);
					
					$totalAnswer = count($row3);

					$sumAmount = 0;
					foreach($row3 as $rw){
						$sumAmount += $rw['answer'];
						
					}
					$weightage=0;
					if($totalAnswer > 0){
						$weightage = round($sumAmount/$totalAnswer,2);
					}
					$row[$key]['pool'][$m]['totalAnswer'] = $totalAnswer;
					$row[$key]['pool'][$m]['sumAmount'] = $sumAmount;
					$row[$key]['pool'][$m]['weightage'] = $weightage;
					
					$k++;
				}*/
				
//				$row[$key]['parent_id'] = $pl['parent_id'];
			}
		}
                
//		echo "<pre>";
//		print_r($row);
//		exit;
		return $row;
	}

	public function getSearchDataReportSummaryLecturer($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();

		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		$scheme = $searchData['scheme'];

		$auth = Zend_Auth::getInstance();
		$userid = $auth->getIdentity()->id;

		$sbmodel = new SurveyBank_Model_DbTable_ImportQuestion();

		$checkDean = $sbmodel->checkDean($userid);

		$prog = array(0);
		if ($checkDean){
			$getProgram = $sbmodel->getProgramBasedOnDept($checkDean['IdCollege']);

			if ($getProgram){
				foreach ($getProgram as $programLoop){
					array_push($prog, $programLoop['IdProgram']);
				}
			}
		}

		$selectData = $db->select()
			->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
			->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','SubCode'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )','SubjectName'))
			->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
			->join(array('user'=>'tbl_user'),'c.IdStaff = user.IdStaff',array())
			->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array('program_id'=>'GROUP_CONCAT( DISTINCT(cgp.program_id))','program_scheme_id'=>'GROUP_CONCAT( DISTINCT(cgp.program_scheme_id))'))
			->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
			->joinLeft(array('cdef'=>'tbl_definationms'),'cdef.IdDefinition = ps.mode_of_program',array('modeName'=>'cdef.DefinitionDesc'))
			->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
			->join(array('sc'=>'q020_sectiontagging'), 'sc.branch_id = tg.IdBranch AND sc.semester_id = tg.IdSemester AND sc.program_id =cgp.program_id AND sc.IdProgramScheme = cgp.program_scheme_id and parent_id !=0 ',array('parent_id'))
			->group('sm.Group')
			->order('c.FullName asc');

		if ($checkDean){
			$selectData->where('ps.IdProgram IN (?)', $prog);
		}else {
			$selectData->where('user.iduser = ?', $userid);
		}

		if(isset($program) && !empty($program)){
			$selectData->where("cgp.program_id IN (?)",$program);
		}

		if(isset($semester) && !empty($semester)){
			$selectData->where("tg.IdSemester = ?",$semester);
		}

		if(isset($branch) && !empty($branch)){
			$selectData->where("tg.IdBranch IN (?)",$branch);
		}

		if($mop != 'ALL'){
			$selectData->where("ps.mode_of_program = ?",$mop);
		}

//		echo $selectData.'<br>';
//		exit;
		$row = $db->fetchAll($selectData);
		//var_dump($row); exit;
		//get total section
//		$totalSectionResult = $this->getSectionCategory($searchData);
//		$totalSection = count($totalSectionResult);

		if($row){

			foreach($row as $key=>$data){

				//get total student registered
				$selectData2 = $db->select()
					->from(array('a'=>'tbl_studentregsubjects'))
					->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
					->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
					->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
					->where('a.Active NOT IN (3)')
					->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
					->where("IFNULL(a.exam_status,'x') NOT IN ('CT','EX','U')")
					//->where("srs.IdProgram IN ($data[program_id])")
					//->where("ps.mode_of_program IN ($mop)")
					//->where("a.IdSemesterMain = ?",$semester)
					->where("a.IdSubject IN ($data[IdSubject])")
					->where("a.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])");

				$selectData5 = $db->select()
					->from(array('a'=>'tbl_studentregsubjects'))
					->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
					->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
					->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
					->where('a.audit_program IS NOT NULL')
					->where('a.Active NOT IN (3)')
					->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
					->where("IFNULL(a.exam_status,'x') IN ('U')")
					//->where("srs.IdProgram IN ($data[program_id])")
					//->where("ps.mode_of_program IN ($mop)")
					//->where("a.IdSemesterMain = ?",$semester)
					->where("a.IdSubject IN ($data[IdSubject])")
					->where("a.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])");

				$select6 = $db->select()
					->union(array($selectData2, $selectData5));

//					echo $selectData2;exit;
				$row2 = $db->fetchAll($select6);

				$row[$key]['total_registered'] = count($row2);

				//get student list
//					$respondens = $this->getTotalResponseListStudent($searchData,$data,$totalSection);
//
//					$forIn = implode(",",$respondens);
				//var_dump($row2);
				$totalResponse = 0;
				foreach($row2 as $reg){
					$studentData = array(
						'IdBranch'=> $reg['IdBranch'],
						'IdProgram'=> $reg['exam_status']=='U' ? $reg['audit_program']:$reg['IdProgram'],
						'IdProgramScheme'=> $reg['exam_status']=='U' ? $reg['audit_programscheme']:$reg['IdProgramScheme'],
						'IdStudentRegistration'=> $reg['IdStudentRegistration'],
					);

					/* $studentData = array(
 'IdBranch'=> $branch,
 'IdProgram'=> $reg['IdProgram'],
 'IdProgramScheme'=> $reg['IdProgramScheme'],
 'IdStudentRegistration'=> $reg['IdStudentRegistration'],
);*/

					$checkCompleteTer = $this->getTerCompleteStatus($studentData,$semester,$data['IdSubject'],$data['IdCourseTaggingGroup']);
					if($checkCompleteTer == 0){
						$totalResponse++;
					}

				}

				$row[$key]['total_response'] = $totalResponse;

				$searchData = array(
					'program'=> $program,
					'semester'=> $semester,
					'scheme'=> $scheme,
					'mop'=> $mop,
				);
				$IdProgram= implode(',',$program);
				$selectDataParent = $db->select()
					->from(array('a'=>'q020_sectiontagging'),array('parent_id'=>'group_concat(distinct parent_id)'))
//							->joinLeft(array('b'=>'tbl_course_tagging_group'),'a.branch_id = b.IdBranch and a.semester_id = b.IdSemester and ',array())
					->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = a.IdProgramScheme',array('mode_of_program'))

//							->where("b.IdSubject = ?",$data['IdSubject'])
					->where("ps.mode_of_program IN ($mop)")
					->where("a.semester_id = ?",$semester)
//							->where("a.program_id IN ($IdProgram)")
					->where('a.parent_id != 0');
//							->group('a.parent_id');
//							->where("b.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])");

				$poolData = $db->fetchRow( $selectDataParent );
//				exit;
//				$poolData = $this->getPoolListParent($selectDataParent);
//				$parentid = array();
//				foreach($poolData as $m=>$pl){
//					$parentid[] = $pl['parent_id'];
//				}


				//get list pool
				$parentid =$poolData['parent_id'];
//				exit;
				$schemeId = $scheme;
				$mopId = $mop;

				$subjectId = $data['IdSubject'];
				$IdCourseTaggingGroup = $data['IdCourseTaggingGroup'];

				$row[$key]['parent_id'] = $parentid;
				$dataArray = $this->getTotalResponseData($parentid,$IdCourseTaggingGroup,$schemeId,$mopId,$IdProgram,$subjectId);

				$row[$key]['data'] = $dataArray['summary'];
//				$poolData = $this->getPoolList($searchData);

//				echo "<pre>";
//				print_r($poolData);
//				exit;

				/*$main_set_id = array();
				$k=0;
				foreach($poolData as $m=>$pl){

					/*$main_set_id[$k] = $pl['main_set_id'];

					$row[$key]['pool'][$pl['question_pool']]['pool_id']=$pl['question_pool'];
					$row[$key]['pool'][$pl['question_pool']]['poolname']=$pl['name'];
					$row[$key]['pool'][$pl['question_pool']]['main_set_id']=$main_set_id;

					$row[$key]['pool'][$m]['main_set_id'] = $pl['main_set_id'];

					$row[$key]['pool'][$m]['pool_id'] = $pl['question_pool'];
					$row[$key]['pool'][$m]['poolname'] = $pl['name'];



				/*$selectData = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'=>'SUM(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
//				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array())
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id',array())
				->join(array('srd'=>'tbl_studentregsubjects_detail'),'srd.student_id = a.student_id AND srd.regsub_id = sr.IdStudentRegSubjects AND srd.subject_id = a.IdSubject AND srd.semester_id = a.semester_id',array())
				->where('b.answer IS NOT NULL')
				->where("c.program_id IN (?)",$program)
				->where("c.branch_id IN (?)",$branch)
				->where("a.semester_id = ?",$semester)
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN (?)",$main_set_id)
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1');

				if($pool_id){
					$selectData->where("a.pool_id = ?",$pool_id);
				}

				if($registration_item_id){
					$selectData->where("srd.item_id = ?",$registration_item_id);
				}

				if($question_id){
					$selectData->where("b.question_id = ?",$question_id);
				}

				if($rubricWeightage){
					$selectData->where("b.answer = ?",$rubricWeightage);
				}


					//get total student response
					$selectData3 = $db->select()
							->from(array('a'=>'q010_student_answer_main'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId','IdProgram'))
							->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
							->where('sr.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
							->joinLeft(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'))
							->where("srs.IdProgram IN (?)",$program)
							->where('b.answer IS NOT NULL')
							->where("a.semester_id = ?",$semester)
							->where("a.IdSubject IN ($data[IdSubject])")
							->where("a.main_set_id IN ($pl[main_set_id])")
							->where("b.chapter_id IN ($pl[chapterid])")
							->where("sr.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])")
							->where("a.pool_id = ?",$pl['question_pool']);
//							->where("a.student_id IN (?)",$forIn);

                                        $selectData4 = $db->select()
							->from(array('a'=>'q010_student_answer_main'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId','IdProgram'))
							->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
							->where('sr.audit_program IS NOT NULL')
                                                        ->where('sr.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(sr.exam_status,'x') IN ('U')")
							->joinLeft(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'))
							->where("srs.IdProgram IN (?)",$program)
							->where('b.answer IS NOT NULL')
							->where("a.semester_id = ?",$semester)
							->where("a.IdSubject IN ($data[IdSubject])")
							->where("a.main_set_id IN ($pl[main_set_id])")
							->where("b.chapter_id IN ($pl[chapterid])")
							->where("sr.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])")
							->where("a.pool_id = ?",$pl['question_pool']);

                                        $select5 = $db->select()
				    		       ->union(array($selectData3, $selectData4));

					$row3 = $db->fetchAll($select5);

					$totalAnswer = count($row3);

					$sumAmount = 0;
					foreach($row3 as $rw){
						$sumAmount += $rw['answer'];

					}
					$weightage=0;
					if($totalAnswer > 0){
						$weightage = round($sumAmount/$totalAnswer,2);
					}
					$row[$key]['pool'][$m]['totalAnswer'] = $totalAnswer;
					$row[$key]['pool'][$m]['sumAmount'] = $sumAmount;
					$row[$key]['pool'][$m]['weightage'] = $weightage;

					$k++;
				}*/

//				$row[$key]['parent_id'] = $pl['parent_id'];
			}
		}

//		echo "<pre>";
//		print_r($row);
//		exit;
		return $row;
	}
	
	public function getPoolList( $searchData=null) {
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$IdProgramScheme = $searchData['mop'];
		$scheme = isset($searchData['scheme'])?$searchData['scheme']:0;

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q020_sectiontagging'),array('main_set_id'=>'GROUP_CONCAT( DISTINCT(a.id))','question_pool','parent_id'=>'GROUP_CONCAT( DISTINCT(a.parent_id))'))
				->join(array('b'=>'tbl_program_scheme'),'b.IdProgramScheme = a.IdProgramScheme',array())
				->join(array('c'=>'tbl_program'),'c.IdProgram = b.IdProgram',array())
				->join(array('d'=>'q002_chapter'),'d.main_set_id = a.id ',array('registration_item_id','name','chapterid'=>'GROUP_CONCAT( DISTINCT(d.id))'))
//				->where('c.program_id IN (?)',$program)
				->where('a.semester_id = ?',$semester)
				->where('a.status = 1')
				->where('d.status = 1')
				->group('d.pool_id')
				->group('d.registration_item_id')
				->order('d.pool_id asc')
				->order('d.order asc')
//				->where('b.question_type = 3')
				
				;
				
		if($scheme){
			$select->where('c.IdScheme = ?',$scheme);
		}
				
				/*SELECT GROUP_CONCAT(DISTINCT(d.id)),d.registration_item_id,d.name
FROM `q020_sectiontagging` a
join tbl_program_scheme b on b.IdProgramScheme = a.IdProgramScheme
join tbl_program c on c.IdProgram = b.IdProgram
join q002_chapter d on d.main_set_id = a.id and d.pool_id = a.question_pool and d.status = 1
where a.parent_id !=0 and c.IdScheme = 1
group by d.pool_id,d.registration_item_id*/
				
				
		if($IdProgramScheme!='ALL'){
			$select->where('b.mode_of_program IN (?)',$IdProgramScheme);
		}
		
//		echo $select;exit;
		$row = $db->fetchAll( $select );
		return $row;
	}
	
	private function getSectionCategory( $searchData=null) {
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$IdProgramScheme = $searchData['IdProgramScheme'];

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('c'=>'q020_sectiontagging'),array('*','main_set_id'=>'c.id'))
				->joinLeft(array('a'=>'q001_pool'),'a.id = c.question_pool')
				->where('c.program_id = ?',$program)
				->where('c.semester_id = ?',$semester)
				->where('c.status = 1')
				->where('a.type = 1')
				->group('a.name');
				//->group('c.id');
				
		if($IdProgramScheme != '')
			$select->where('c.IdProgramScheme IN (?)',$IdProgramScheme);
		
		//echo $select;exit;
		$row = $db->fetchAll( $select );
		return $row;
	}
	
	private function getTotalResponseListStudent(array $searchData, array $data, $total) {
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
//		$IdProgramScheme = $searchData['IdProgramScheme'];
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData3 = $db->select()
							->from(array('a'=>'q010_student_answer_main'),array('a.student_id'))
							->join(array('b'=> 'q010_student_answer_main'),'b.id = a.id',array('totalCount' => 'COUNT(*)'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array())
							->where("srs.IdProgram = ?",$program)
							->where("a.semester_id = ?",$semester)
							->where("a.IdSubject = ?",$data['IdSubject'])
							->group("a.student_id")
							->having('totalCount = ?',$total);
		//echo $selectData3;
		$result = $db->fetchAll($selectData3);

		$studentList = array();
		
		$i = 0;
		foreach ($result as $key => $value) {
			
			$studentList[$i] = $value['student_id'];
			$i++;
		}
		//print_r($studentList);
		return $studentList;
	}
	
	public function getReportByCourseLecturer($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		$scheme = $searchData['scheme'];
		
		$selectData = $db->select()
						->from(array('sc'=>'q020_sectiontagging'))
						->join(array('a'=>'q010_student_answer_main'), 'sc.id = a.main_set_id',array())
						->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId','IdProgram'))
						->joinLeft(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id AND sr.IdSubject = a.IdSubject AND sr.IdSemesterMain = sc.semester_id',array('IdCourseTaggingGroup'))
						->join(array('tg'=>'tbl_course_tagging_group'),'tg.IdCourseTaggingGroup = sr.IdCourseTaggingGroup',array('GroupName'))
						->join(array('s'=>'tbl_subjectmaster'), 's.IdSubject = tg.IdSubject',array('SubCode'=>'GROUP_CONCAT( DISTINCT(SubCode) SEPARATOR "/" )','SubjectName','IdSubject'))
						->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
						->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = sc.IdProgramScheme',array())
						->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = srs.IdBranch',array('BranchName'=>'BranchCode'))
						->joinLeft(array('cdef'=>'tbl_definationms'),'cdef.IdDefinition = ps.mode_of_program',array('modeName'=>'cdef.DefinitionDesc'))
//						->group('sc.id')
						->group('tg.IdCourseTaggingGroup')
						->group('tg.IdSubject')
						->group('s.Group')
						->order('c.FullName asc')
						->order('s.SubCode asc')
						;
						
		/*$selectData = $db->select()
						->from(array('a'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup','IdSubject'))
						->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
						->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName'))
						->joinLeft(array('sr'=>'tbl_studentregsubjects'),'sr.IdCourseTaggingGroup = a.IdCourseTaggingGroup',array())
						->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('registrationId','IdProgram'))
						->join(array('aps'=>'tbl_program_scheme'),'aps.IdProgramScheme = srs.IdProgramScheme',array('IdProgramScheme'))
						->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = srs.IdBranch',array('BranchName'=>'BranchCode'))
						->group('a.IdCourseTaggingGroup')
						->order('c.FirstName asc')
//						->limit(5);
						;*/

		if(isset($program) && !empty($program)){
			$selectData->where("sc.program_id IN (?)",$program);
		}
		
		if(isset($semester) && !empty($semester)){
			$selectData->where("a.semester_id = ?",$semester);
		}
		
		if(isset($branch) && !empty($branch)){
			$selectData->where("srs.IdBranch IN (?)",$branch);
		}
		
		if($mop != 'ALL'){
			$selectData->where("ps.mode_of_program = ?",$mop);
		}
		
		$row = $db->fetchAll($selectData);
//		echo $selectData .'<br />';
		if($row){
			foreach($row as $key=>$data){
				
				//get pool list
				$poolList = $this->getPoolList($searchData);
				
				$bil=0;
				foreach($poolList as $k=>$pl){
					
//					$row[$key]['pool'][$bil]['pool_id'] = $pl['question_pool'];
//					$row[$key]['pool'][$bil]['pool_name'] = $pl['name'];

					$searchData['pool_id'] = $pl['question_pool'];

//					$sectionList = $this->getSectionList($searchData);
//					$totalSection = count($sectionList);

					//get section list
//					foreach($sectionList as $m=>$sct){

						$row[$key]['chapter'][$k]['registration_item'] = $pl['name'];
						
						$rowTotal = $this->getAnswerSubmit($data['IdCourseTaggingGroup'],$program,$branch,$semester,$data['IdSubject'],$pl['main_set_id'],$pl['question_pool'],$pl['registration_item_id']);
							
						$row[$key]['chapter'][$k]['total'] = $rowTotal;
						
						$bil++;
//					}

				}
			}
		}
		
//				echo "<pre>";
//		print_r($row);	
//		exit;	
		return $row;
	}
	
	public function getAnswerSubmit($IdCourseTaggingGroup,$program, $branch,$semester,$subjectId, $main_set_id, $pool_id=0, $registration_item_id=0,$question_id=0,$rubricWeightage=0){ 
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		/*//get total student response
					$selectData3 = $db->select()
							->from(array('a'=>'q010_student_answer_main'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId','IdProgram'))
							->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
							->where('sr.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
							->joinLeft(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'))
							->where("srs.IdProgram IN (?)",$program)
							->where('b.answer IS NOT NULL')
							->where("a.semester_id = ?",$semester)
							->where("a.IdSubject IN ($data[IdSubject])")
							->where("a.main_set_id IN ($pl[main_set_id])")
							->where("b.chapter_id IN ($pl[chapterid])")
							->where("sr.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])")
							->where("a.pool_id = ?",$pl['question_pool']);
//							->where("a.student_id IN (?)",$forIn);
					$row3 = $db->fetchAll($selectData3);
					
					$totalAnswer = count($row3);

					$sumAmount = 0;
					foreach($row3 as $rw){
						$sumAmount += $rw['answer'];
						
					}
					$weightage=0;
					if($totalAnswer > 0){
						$weightage = round($sumAmount/$totalAnswer,2);
					}
					$row[$key]['pool'][$m]['totalAnswer'] = $totalAnswer;
					$row[$key]['pool'][$m]['sumAmount'] = $sumAmount;
					$row[$key]['pool'][$m]['weightage'] = $weightage;*/
					
					
					
					
					
					
		$selectData = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'=>'SUM(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array())
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id',array())
				->join(array('srd'=>'tbl_studentregsubjects_detail'),'srd.student_id = a.student_id AND srd.regsub_id = sr.IdStudentRegSubjects AND srd.subject_id = a.IdSubject AND srd.semester_id = a.semester_id',array())
				->where('b.answer IS NOT NULL')
				->where("srs.IdProgram IN (?)",$program)
				->where("c.branch_id IN (?)",$branch)
				->where("a.semester_id IN (?)",$semester)
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN (?)",$main_set_id)
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1');

		if($pool_id){
			$selectData->where("a.pool_id = ?",$pool_id);
		}
		
		if($registration_item_id){
			$selectData->where("srd.item_id = ?",$registration_item_id);
		}
		
		if($question_id){
			$selectData->where("b.question_id = ?",$question_id);
		}
		
		if($rubricWeightage){
			$selectData->where("b.answer = ?",$rubricWeightage);
		}
//	echo "<hr>";
//	echo $selectData;exit;
		$rowData = $db->fetchRow($selectData);
		$sumAmount = isset($rowData['answer'])?$rowData['answer']:0;
		
		$selectData2 = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('total'=>'COUNT(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array())
//				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = srs.IdStudentRegistration',array())
				->join(array('srd'=>'tbl_studentregsubjects_detail'),'srd.student_id = a.student_id AND srd.subject_id = a.IdSubject AND srd.semester_id = a.semester_id',array())
				->where('b.answer IS NOT NULL')
				->where("srs.IdProgram IN (?)",$program)
				->where("a.semester_id IN (?)",$semester)
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN (?)",$main_set_id)
				->where('a.status = 1');

		if($pool_id){
			$selectData2->where("a.pool_id = ?",$pool_id);
		}
		
		if($registration_item_id){
			$selectData2->where("srd.item_id = ?",$registration_item_id);
		}
		
		if($question_id){
			$selectData2->where("b.question_id = ?",$question_id);
		}
		
		if($rubricWeightage){
			$selectData2->where("b.answer = ?",$rubricWeightage);
		}
		
//		echo $selectData.'<br><br>';
		$rowData2 = $db->fetchRow($selectData2);
	    $totalAnswer = $rowData2['total'];

		$weightage=0;
		if($totalAnswer > 0){
			$weightage = round($sumAmount/$totalAnswer,2);
		}
		
		return array('totalAnswer'=>$totalAnswer,'weightage'=>$weightage,'sum'=>$sumAmount);
	}
							
	public function getTerCompleteStatus($student,$semesterID,$subject=0,$IdCourseTaggingGroup=0){ // function from student portal to check TER completed status
		$ter_completed = 0; //completed

		//get total evaluation form to be submmited
		$sectionTaggingDB = new App_Model_Question_DbTable_SectionTagging();
		$listEvaluation = $sectionTaggingDB->getListEvaluation($student['IdBranch'],$student['IdProgram'],$student['IdProgramScheme'],$semesterID);

		$jumlah_pool = 0;
		if(count($listEvaluation)>0){	
			$courseRegisterDb = new Registration_Model_DbTable_Studentregistration();
			foreach($listEvaluation as $key=>$sect){
				if($sect['type']==1){//by course
					if ($sect['target_user']==2){
						$courses = $courseRegisterDb->getCourseRegisteredBySemesterEvaluation($student['IdStudentRegistration'], $semesterID, NULL, $subject, $IdCourseTaggingGroup, $student['IdProgram']);
						if ($courses){
							foreach ($courses as $course){
								$lecturerList = $sectionTaggingDB->getLecturer($course['IdCourseTaggingGroup']);
								$jumlah_pool = $jumlah_pool+count($lecturerList);
							}
						}
					}else{
						$courses = $courseRegisterDb->getCourseRegisteredBySemesterEvaluation($student['IdStudentRegistration'], $semesterID, NULL, $subject, $IdCourseTaggingGroup, $student['IdProgram']);
						$jumlah_pool = count($courses) + $jumlah_pool;
					}
				}
			}					
		}
		
		$total_terform_submitted = $sectionTaggingDB->getTotalAnsweredByStudent($student['IdStudentRegistration'],$semesterID,$subject,$IdCourseTaggingGroup,$student['IdProgram']);

		if($total_terform_submitted < $jumlah_pool){
			//ter evaluation not completed. alert student to fill in the form	
			$ter_completed = 1;
		}
                
		if (!$listEvaluation){
			$ter_completed = 0;
		}
		
		return $ter_completed;
	}
	
	public function getSectionList( $searchData=null) {
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$IdProgramScheme = $searchData['mop'];
		$branch = $searchData['branch'];
		$poolID = $searchData['pool_id'];
		$scheme = isset($searchData['scheme'])?$searchData['scheme']:0;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		/*$select = $db ->select()
				->from(array('a'=>'q020_sectiontagging'),array('main_set_id'=>'GROUP_CONCAT( DISTINCT(a.id))','question_pool','parent_id'=>'GROUP_CONCAT( DISTINCT(a.parent_id))'))
				->join(array('b'=>'tbl_program_scheme'),'b.IdProgramScheme = a.IdProgramScheme',array())
				->join(array('c'=>'tbl_program'),'c.IdProgram = b.IdProgram',array())
				->join(array('d'=>'q002_chapter'),'d.main_set_id = a.id and d.pool_id = a.question_pool and d.status = 1',array('registration_item_id','name','chapterid'=>'GROUP_CONCAT( DISTINCT(d.id))'))
//				->where('c.program_id IN (?)',$program)
				->where('a.semester_id IN (?)',$semester)
				->where('a.status = 1')
				->group('d.pool_id')
				->group('d.registration_item_id')
				->order('d.pool_id asc')
				->order('d.order asc');*/

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q002_chapter'))
				->joinLeft(array('b'=>'q020_sectiontagging'),'b.id = a.main_set_id')
				->joinLeft(array('c'=>'tbl_definationms'),'c.IdDefinition = a.registration_item_id',array('item_name'=>'DefinitionDesc'))
//				->joinLeft(array('b'=>'q002_chapter'),'b.pool_id = a.id',array('chaptername'=>'b.name','chapterid'=>'b.id','question_type'=>'b.question_type', 'subsection_instruction' => 'b.subsection_instruction','registration_item_id'))
				->where('b.program_id IN (?)',$program)
				->where('b.branch_id IN (?)',$branch)
				->where('b.semester_id IN (?)',$semester)
				->where('b.question_pool IN (?)',$poolID)
				->where('a.status = 1')
				->where('a.question_type = 3')//likert only
				->group('a.registration_item_id')
				->order('a.order ASC');
				//->group('c.id');

		if($scheme){
			$select->where('c.IdScheme = ?',$scheme);
		}
			
		if($IdProgramScheme!='ALL'){
			$select->joinLeft(array('d'=>'tbl_program_scheme'),'b.IdProgramScheme = d.IdProgramScheme')
			->where('d.mode_of_program IN (?)',$IdProgramScheme);
		}
		
//		echo $select;exit;
		$row = $db->fetchAll( $select );
		return $row;
	}
	
	
	public function getRankCourse(array $searchData) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		
		$selectData = $db->select()
						->from(array('a'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup','IdSubject'))
						->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
						->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName'))
						->joinLeft(array('sr'=>'tbl_studentregsubjects'),'sr.IdCourseTaggingGroup = a.IdCourseTaggingGroup',array())
						->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('registrationId','IdProgram'))
						->join(array('aps'=>'tbl_program_scheme'),'aps.IdProgramScheme = srs.IdProgramScheme',array('IdProgramScheme'))
						->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = srs.IdBranch',array('BranchName'=>'BranchCode'))
						->group('a.IdCourseTaggingGroup')
						->order('c.FirstName asc')
//						->limit(5);
						;
							
		if(isset($program) && !empty($program)){
			$selectData->where("srs.IdProgram IN (?)",$program);
		}
		
		if(isset($semester) && !empty($semester)){
			$selectData->where("a.IdSemester = ?",$semester);
		}
		
		if(isset($branch) && !empty($branch)){
			$selectData->where("srs.IdBranch IN (?)",$branch);
		}
		
		if($mop != 'ALL'){
			$selectData->where("aps.mode_of_program = ?",$mop);
		}
		
		$row = $db->fetchAll($selectData);

		if($row){
			foreach($row as $key=>$data){
				
				//get pool list
				$poolList = $this->getPoolList($searchData);
				
				$bil=0;
				foreach($poolList as $k=>$pl){
				
					$searchData['pool_id'] = $pl['question_pool'];
					
					$sectionList = $this->getSectionList($searchData);
					$totalSection = count($sectionList);
					
					//get section list
					foreach($sectionList as $m=>$sct){
						
						if($totalSection == 1){
							$row[$key]['chapter'][$bil]['registration_item'] = $pl['name'];
						}else{
							$row[$key]['chapter'][$bil]['registration_item'] = $sct['item_name'];
						}
						
						$rowTotal = $this->getAnswerSubmit($data['IdCourseTaggingGroup'],$program,$branch,$semester,$data['IdSubject'],$sct['main_set_id'],$sct['pool_id'],$sct['registration_item_id']);	
						$row[$key]['chapter'][$bil]['total'] = $rowTotal;
						$row[$key]['chapter'][$bil]['total']['rating'] = $this->checkRating($rowTotal['weightage']);
						$bil++;
					}
						
				}
			}
			
			$ranks = $this->ranking($row);
		}
		
		return $row;
		
		
	}

	public function getRankCourseLecturer(array $searchData) {

		$db = Zend_Db_Table::getDefaultAdapter();

		$auth = Zend_Auth::getInstance();
		$userid = $auth->getIdentity()->id;

		$sbmodel = new SurveyBank_Model_DbTable_ImportQuestion();

		$checkDean = $sbmodel->checkDean($userid);

		$prog = array(0);
		if ($checkDean){
			$getProgram = $sbmodel->getProgramBasedOnDept($checkDean['IdCollege']);

			if ($getProgram){
				foreach ($getProgram as $programLoop){
					array_push($prog, $programLoop['IdProgram']);
				}
			}
		}

		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];

		$selectData = $db->select()
			->from(array('a'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup','IdSubject'))
			->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
			->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName'))
			->join(array('user'=>'tbl_user'),'c.IdStaff = user.IdStaff',array())
			->joinLeft(array('sr'=>'tbl_studentregsubjects'),'sr.IdCourseTaggingGroup = a.IdCourseTaggingGroup',array())
			->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('registrationId','IdProgram'))
			->join(array('aps'=>'tbl_program_scheme'),'aps.IdProgramScheme = srs.IdProgramScheme',array('IdProgramScheme'))
			->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = srs.IdBranch',array('BranchName'=>'BranchCode'))
			->group('a.IdCourseTaggingGroup')
			->order('c.FirstName asc');

		if ($checkDean){
			$selectData->where('aps.IdProgram IN (?)', $prog);
		}else {
			$selectData->where('user.iduser = ?', $userid);
		}

		if(isset($program) && !empty($program)){
			$selectData->where("srs.IdProgram IN (?)",$program);
		}

		if(isset($semester) && !empty($semester)){
			$selectData->where("a.IdSemester = ?",$semester);
		}

		if(isset($branch) && !empty($branch)){
			$selectData->where("srs.IdBranch IN (?)",$branch);
		}

		if($mop != 'ALL'){
			$selectData->where("aps.mode_of_program = ?",$mop);
		}

		$row = $db->fetchAll($selectData);

		if($row){
			foreach($row as $key=>$data){

				//get pool list
				$poolList = $this->getPoolList($searchData);

				$bil=0;
				foreach($poolList as $k=>$pl){

					$searchData['pool_id'] = $pl['question_pool'];

					$sectionList = $this->getSectionList($searchData);
					$totalSection = count($sectionList);

					//get section list
					foreach($sectionList as $m=>$sct){

						if($totalSection == 1){
							$row[$key]['chapter'][$bil]['registration_item'] = $pl['name'];
						}else{
							$row[$key]['chapter'][$bil]['registration_item'] = $sct['item_name'];
						}

						$rowTotal = $this->getAnswerSubmit($data['IdCourseTaggingGroup'],$program,$branch,$semester,$data['IdSubject'],$sct['main_set_id'],$sct['pool_id'],$sct['registration_item_id']);
						$row[$key]['chapter'][$bil]['total'] = $rowTotal;
						$row[$key]['chapter'][$bil]['total']['rating'] = $this->checkRating($rowTotal['weightage']);
						$bil++;
					}

				}
			}

			$ranks = $this->ranking($row);
		}

		return $row;


	}
	
	private function checkRating($point) {
		
		$exceptional = '4.25';
		$satisfactory= '3.5';
		
		if ($point < $satisfactory) {
			$rating = 'Unsatisfactory';
		}
		elseif ($point >= $exceptional) {
			$rating = 'Exceptional';
		}
		else {
			$rating = 'Satisfactory';
		}
		
		return $rating;
	}
	
	public function getAnswerByQuestionType($IdCourseTaggingGroup,$subjectId, $main_set_id,$questionId=0){ 
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'=>'answer_text'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = b.main_set_id',array())
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IdSubject = b.IdSubject',array())
				->where('b.answer_text IS NOT NULL')
				->where("b.answer_text LIKE '%_%'")
				->where("b.answer_text != ''")
				->where("b.answer_text NOT IN ('-','=')")
//				->where("c.program_id IN (?)",$program)
//				->where("c.branch_id IN (?)",$branch)
//				->where("a.semester_id = ?",$semester)
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN ($main_set_id)")
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
//				->where("b.chapter_id = ?",$chapter_id)
				->where('a.status = 1')
				->order('b.created_at asc');

		if($questionId){
			$selectData->where("b.question_id = ?",$questionId);
		}
		
//		echo  $selectData;
		$rowData = $db->fetchAll($selectData);
		return $rowData;
	}
	
	public function getTotalResponse($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram=0,$mopId=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();

		//get total student registered
					$selectData2 = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
							->where('a.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(a.exam_status,'x') NOT IN ('CT','EX','U')")
							->where("srs.IdProgram IN ($IdProgram)")
							//->where("ps.mode_of_program IN ($mop)")
							//->where("a.IdSemesterMain = ?",$semester)
							->where("a.IdSubject IN ($subjectId)")
							->where("a.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)");
                                        
                     $selectData5 = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
							->where('a.audit_program IS NOT NULL')
                            ->where('a.Active NOT IN (3)')
							->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
							->where("IFNULL(a.exam_status,'x') IN ('U')")
							->where("srs.IdProgram IN ($IdProgram)")
							//->where("ps.mode_of_program IN ($mop)")
							//->where("a.IdSemesterMain = ?",$semester)
							->where("a.IdSubject IN ($subjectId)")
							->where("a.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)");
                                        
                                        $select6 = $db->select()
                                            ->union(array($selectData2, $selectData5));
                                        
//					echo $selectData2;exit;
					$row2 = $db->fetchAll($select6);

					$row['total_registered'] = count($row2);

					$totalResponse = 0;
					foreach($row2 as $reg){
						$selectPs = $db->select()
							->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
							->where('a.IdProgram IN (?)', explode(',', $IdProgram))
							->where('a.mode_of_program = ?', $mopId);

						$psresult = $db->fetchAll($selectPs);

						$psarr = array();
						//dd($psresult);
						if ($psresult){
							foreach ($psresult as $psresultLoop){
								$psarr[] = $psresultLoop['IdProgramScheme'];
							}
						}

						$ps = $reg['exam_status']=='U' ? $reg['audit_programscheme']:$reg['IdProgramScheme'];

						$studentData = array(
							'IdBranch'=> $reg['IdBranch'],
							//'IdProgram'=> $reg['exam_status']=='U' ? $reg['audit_program']:$reg['IdProgram'],
							'IdProgram'=> explode(',', $IdProgram),
							//'IdProgramScheme'=> $reg['exam_status']=='U' ? $reg['audit_programscheme']:$reg['IdProgramScheme'],
							'IdProgramScheme'=> in_array($ps, $psarr) ? $ps:$psresult[0]['IdProgramScheme'],
							'IdStudentRegistration'=> $reg['IdStudentRegistration'],
						);

						//dd($studentData);

			$checkCompleteTer = $this->getTerCompleteStatus($studentData,$semester,$subjectId,$IdCourseTaggingGroup);

			if($checkCompleteTer == 0){
				$totalResponse++;
			}
		}

		$row['total_response'] = $totalResponse;
		return $row;
	}
	
	
	public function ranking(array $lecturers) {
		$groupChapter = false;
		foreach ($lecturers as $a => $lecturer) {
			//loop by chapter
			foreach ($lecturer['chapter'] as $b => $chapter) {
				$groupChapter[$chapter['registration_item']][$a] = sprintf("%.2f",$chapter['total']['weightage']);   
			}
		}

		if ($groupChapter) {
			foreach ($groupChapter as $c => $data) {
				$ranks = $this->findRank($data);
				$weightageRanks[$c] = $ranks;
			}
		}
		
		return $weightageRanks;

	}
	
	private function findRank(array $data) {
		
		$occurrences = array_count_values($data);
		krsort($occurrences);

		$position = 1;
		foreach ($occurrences as $score => $count) {
			$occurrences[$score] = $position;
			$position += $count;

		}

		return $occurrences;
	}
	
public function getReportByCourseOriginal($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		
		$selectSemester = $db->select()
						->from(array('s'=>'tbl_semestermaster'))
						->where('s.IdSemesterMaster IN (?)',$semester)
						;
		$rowSemester = $db->fetchAll($selectSemester);
		
		$dataArray = array();
		foreach($rowSemester as $s=>$sem){
			
			$selectData = $db->select()
							->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
							->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subcode_arr'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )'))
							->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
							->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array())
							->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
							->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
					
							->group('sm.Group')
							->order('c.FullName asc')
							;
							
			if(isset($program) && !empty($program)){
				$selectData->where("cgp.program_id IN (?)",$program);
			}
			
			if(isset($semester) && !empty($semester)){
				$selectData->where("tg.IdSemester = ?",$sem['IdSemesterMaster']);
			}
			
			if(isset($branch) && !empty($branch)){
				$selectData->where("tg.IdBranch = ?",$branch);
			}
			
			if(isset($mop) && !empty($mop)){
				$selectData->where("ps.mode_of_program = ?",$mop);
			}
			$row = $db->fetchAll($selectData);
			
			
			if($row){
				foreach ($row as $n=>$subj){
					$dataArray[$s]['semester']=$sem['SemesterMainName'];
					$dataArray[$s]['course'][$n]=$subj;
					
					$semester = $sem['IdSemesterMaster'];
					$searchData = array(
						'program'=> $program,
						'semester'=> $semester,
						'mop'=> $subj['mode_of_program'],
					);
							
					//get list pool
					$poolData = $this->getSetForm($searchData);
					
					$mainSetID = array();
					foreach($poolData as $m=>$pl){
						$mainSetID[]=$pl['id'];
					}
					
					$dataArray[$s]['course'][$n]['main_set_id']=$mainSetID;
					$idSubject = (int)$subj['IdSubject'];

					$rowTotal = $this->getAnswerSubmit($subj['IdCourseTaggingGroup'],$program,$branch,$semester,$idSubject,$mainSetID);
					
					$dataArray[$s]['course'][$n]['answer'] = $rowTotal;
					}
					
			}
			
		}
//		echo "<pre>";
//		print_r($dataArray);
//		echo $selectData .'<br />';exit;
		
		
//				echo "<pre>";
//		print_r($row);	
//		exit;	
		return $dataArray;
	}
	
	public function getReportByCourse($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		//var_dump($searchData); exit;
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		$scheme = $searchData['scheme'];

		$selectData = $db->select()
						->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
						->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subcode_arr'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )'))
						->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
						->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array())
						->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
						->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
				
						->group('sm.Group')
						->order('c.FullName asc')
						;
						
		if(isset($program) && !empty($program)){
			$selectData->where("cgp.program_id IN (?)",$program);
		}
		
		if(isset($semester) && !empty($semester)){
			$selectData->where("tg.IdSemester IN (?)",$semester);
		}
		
		if($branch != 'ALL'){
			$selectData->where("tg.IdBranch = ?",$branch);
		}
		
		if(isset($mop) && !empty($mop)){
			$selectData->where("ps.mode_of_program = ?",$mop);
		}
		
//		echo $selectData."<hr>";
//exit;
		$row = $db->fetchAll($selectData);
		
		$dataArray = array();
		if($row){
			
			foreach ($row as $s=>$subj){
				$dataArray[$s]['IdSubject'] = $subj['IdSubject'];
				$dataArray[$s]['SubCode'] = $subj['subcode_arr'];
				$dataArray[$s]['IdCourseTaggingGroup'] = $subj['IdCourseTaggingGroup'];
				
				//get semester
				
				$selectSemester = $db->select()
						->from(array('s'=>'tbl_semestermaster'))
						->where('s.IdSemesterMaster IN (?)',$semester)
						->order('s.AcademicYear desc')
						->order('s.SemesterMainStartDate desc')
						;
				$rowSemester = $db->fetchAll($selectSemester);
				
				foreach($rowSemester as $m=>$sem){
					$dataArray[$s]['answersemester'][$m]['IdSemester'] = $sem['IdSemesterMaster'];
					$dataArray[$s]['answersemester'][$m]['SemesterName'] = $sem['SemesterMainName'];
					
					//get set form
					$searchData = array(
						'program'=>$program,
						'branch'=>$branch,
						'mop'=>$mop,
						'semester'=> $sem['IdSemesterMaster']
					);
					$setForm = $this->getSetForm($searchData);

					//var_dump($setForm);

					$selectDataParent = $db->select()
						->from(array('a'=>'q020_sectiontagging'),array('parent_id'=>'group_concat(distinct parent_id)'))
//							->joinLeft(array('b'=>'tbl_course_tagging_group'),'a.branch_id = b.IdBranch and a.semester_id = b.IdSemester and ',array())
						->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = a.IdProgramScheme',array('mode_of_program'))

//							->where("b.IdSubject = ?",$data['IdSubject'])
						->where("ps.mode_of_program IN ($mop)")
						->where("a.semester_id IN (?)",$semester)
//							->where("a.program_id IN ($IdProgram)")
						->where('a.parent_id != 0');
//							->group('a.parent_id');
//							->where("b.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])");

					$poolData = $db->fetchRow( $selectDataParent );

					$parentid =$poolData['parent_id'];

					$weightage = $this->getTotalResponseData($parentid,$subj['IdCourseTaggingGroup'],$scheme,$mop,$program,$subj['IdSubject']);
					//var_dump($weightage);
					$dataArray[$s]['answersemester'][$m]['weightage']=$weightage['summary'];

					$main_set_id = array();
					foreach($setForm as $st){
						$main_set_id[] = $st['id'];
					}
					
					if($main_set_id){
					
						//get total answer submit
						$IdCourseTaggingGroup = $subj['IdCourseTaggingGroup'];
						$semesterID = $sem['IdSemesterMaster'];
						$subjectId = $subj['IdSubject'];
						
						
						$totalAnswerSubmitted = $this->getAnswerSubmit($IdCourseTaggingGroup,$program, $branch,$semesterID,$subjectId, $main_set_id);

						$dataArray[$s]['answersemester'][$m]['total'] = $totalAnswerSubmitted;
					}
				}
			}
		}
			
//		echo "<pre>";
//		print_r($dataArray);
//		exit;
		return $dataArray;
	}
	
	public function getSameCourseName($idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
		 					->from(array('s'=>$this->_name),array('IdSubject'=>'GROUP_CONCAT(IdSubject)','SubCode'=>'GROUP_CONCAT( DISTINCT(SubCode) SEPARATOR "/" )','SubjectName'=>'GROUP_CONCAT( DISTINCT(SubjectName) SEPARATOR "/" )'))	 				 
	 				 ->where("s.Group = ?",$idSubject);
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function getSetForm( $searchData=null) {
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$IdProgramScheme = $searchData['mop'];
		$branch = $searchData['branch'];

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('c'=>'q020_sectiontagging'),array('*','main_set_id'=>'c.id'))
				->where('c.program_id IN (?)',$program)
				->where('c.semester_id IN (?)',$semester)
				->where('c.branch_id IN (?)',$branch)
				->where('c.status = 1');
				
		if($IdProgramScheme!='ALL'){
			$select->joinLeft(array('d'=>'tbl_program_scheme'),'c.IdProgramScheme = d.IdProgramScheme',array())
			->where('d.mode_of_program IN (?)',$IdProgramScheme);
		}
		
//		echo $select."<br>";//exit;
		$row = $db->fetchAll( $select );
		return $row;
	}
	
	public function getReportByLecturer($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		$scheme = $searchData['scheme'];

		$selectData = $db->select()
						->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
						->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subcode_arr'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )'))
						->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
						->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array())
						->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
						->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
				
						->group('tg.IdLecturer')
						->order('c.FullName asc')
						;
						
		if(isset($program) && !empty($program)){
			$selectData->where("cgp.program_id IN (?)",$program);
		}
		
		if(isset($semester) && !empty($semester)){
			$selectData->where("tg.IdSemester IN (?)",$semester);
		}
		
		if($branch != 'ALL'){
			$selectData->where("tg.IdBranch = ?",$branch);
		}
		
		if(isset($mop) && !empty($mop)){
			$selectData->where("ps.mode_of_program = ?",$mop);
		}
		
//		echo $selectData."<hr>";exit;
		$row = $db->fetchAll($selectData);
		
		$dataArray = array();
		if($row){
			
			foreach ($row as $s=>$subj){
				$dataArray[$s]['FullName'] = $subj['FullName'];
				$dataArray[$s]['IdSubject'] = $subj['IdSubject'];
				$dataArray[$s]['IdCourseTaggingGroup'] = $subj['IdCourseTaggingGroup'];
				
				//get semester
				
				$selectSemester = $db->select()
						->from(array('s'=>'tbl_semestermaster'))
						->where('s.IdSemesterMaster IN (?)',$semester)
						->order('s.AcademicYear desc')
						->order('s.SemesterMainStartDate desc')
						;
				$rowSemester = $db->fetchAll($selectSemester);
				
				foreach($rowSemester as $m=>$sem){
					$dataArray[$s]['answersemester'][$m]['IdSemester'] = $sem['IdSemesterMaster'];
					$dataArray[$s]['answersemester'][$m]['SemesterName'] = $sem['SemesterMainName'];
					
					//get set form
					$searchData = array(
						'program'=>$program,
						'branch'=>$branch,
						'mop'=>$mop,
						'semester'=> $sem['IdSemesterMaster']
					);
					$setForm = $this->getSetForm($searchData);

					$selectDataParent = $db->select()
						->from(array('a'=>'q020_sectiontagging'),array('parent_id'=>'group_concat(distinct parent_id)'))
//							->joinLeft(array('b'=>'tbl_course_tagging_group'),'a.branch_id = b.IdBranch and a.semester_id = b.IdSemester and ',array())
						->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = a.IdProgramScheme',array('mode_of_program'))

//							->where("b.IdSubject = ?",$data['IdSubject'])
						->where("ps.mode_of_program IN ($mop)")
						->where("a.semester_id IN (?)",$semester)
//							->where("a.program_id IN ($IdProgram)")
						->where('a.parent_id != 0');
//							->group('a.parent_id');
//							->where("b.IdCourseTaggingGroup IN ($data[IdCourseTaggingGroup])");

					$poolData = $db->fetchRow( $selectDataParent );

					$parentid =$poolData['parent_id'];

					$weightage = $this->getTotalResponseData($parentid,$subj['IdCourseTaggingGroup'],$scheme,$mop,$program,$subj['IdSubject']);
					//var_dump($weightage);
					$dataArray[$s]['answersemester'][$m]['weightage']=$weightage['summary'];
					
					$main_set_id = array();
					foreach($setForm as $st){
						$main_set_id[] = $st['id'];
					}
					
					if($main_set_id){
					
						//get total answer submit
						$IdCourseTaggingGroup = $subj['IdCourseTaggingGroup'];
						$semesterID = $sem['IdSemesterMaster'];
						$subjectId = $subj['IdSubject'];
						
						
						$totalAnswerSubmitted = $this->getAnswerSubmit($IdCourseTaggingGroup,$program, $branch,$semesterID,$subjectId, $main_set_id);
						
						$dataArray[$s]['answersemester'][$m]['total'] = $totalAnswerSubmitted;
					}
				}
			}
		}
			
//		echo "<pre>";
//		print_r($dataArray);
		return $dataArray;
	}

	public function getReportByLecturer2($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();

		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];

		$auth = Zend_Auth::getInstance();
		$userid = $auth->getIdentity()->id;

		$sbmodel = new SurveyBank_Model_DbTable_ImportQuestion();

		$checkDean = $sbmodel->checkDean($userid);

		$prog = array(0);
		if ($checkDean){
			$getProgram = $sbmodel->getProgramBasedOnDept($checkDean['IdCollege']);

			if ($getProgram){
				foreach ($getProgram as $programLoop){
					array_push($prog, $programLoop['IdProgram']);
				}
			}
		}

		$selectData = $db->select()
			->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
			->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subcode_arr'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )'))
			->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
			->join(array('user'=>'tbl_user'),'c.IdStaff = user.IdStaff',array())
			->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array())
			->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
			->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
			->group('tg.IdLecturer')
			->order('c.FullName asc');

		if ($checkDean){
			$selectData->where('ps.IdProgram IN (?)', $prog);
		}else{
			$selectData->where('user.iduser = ?', $userid);
		}

		if(isset($program) && !empty($program)){
			$selectData->where("cgp.program_id IN (?)",$program);
		}

		if(isset($semester) && !empty($semester)){
			$selectData->where("tg.IdSemester IN (?)",$semester);
		}

		if($branch != 'ALL'){
			$selectData->where("tg.IdBranch = ?",$branch);
		}

		if(isset($mop) && !empty($mop)){
			$selectData->where("ps.mode_of_program = ?",$mop);
		}

//		echo $selectData."<hr>";exit;
		$row = $db->fetchAll($selectData);

		$dataArray = array();
		if($row){

			foreach ($row as $s=>$subj){
				$dataArray[$s]['FullName'] = $subj['FullName'];
				$dataArray[$s]['IdSubject'] = $subj['IdSubject'];
				$dataArray[$s]['IdCourseTaggingGroup'] = $subj['IdCourseTaggingGroup'];

				//get semester

				$selectSemester = $db->select()
					->from(array('s'=>'tbl_semestermaster'))
					->where('s.IdSemesterMaster IN (?)',$semester)
					->order('s.AcademicYear desc')
					->order('s.SemesterMainStartDate desc')
				;
				$rowSemester = $db->fetchAll($selectSemester);

				foreach($rowSemester as $m=>$sem){
					$dataArray[$s]['answersemester'][$m]['IdSemester'] = $sem['IdSemesterMaster'];
					$dataArray[$s]['answersemester'][$m]['SemesterName'] = $sem['SemesterMainName'];

					//get set form
					$searchData = array(
						'program'=>$program,
						'branch'=>$branch,
						'mop'=>$mop,
						'semester'=> $sem['IdSemesterMaster']
					);
					$setForm = $this->getSetForm($searchData);

					$main_set_id = array();
					foreach($setForm as $st){
						$main_set_id[] = $st['id'];
					}

					if($main_set_id){

						//get total answer submit
						$IdCourseTaggingGroup = $subj['IdCourseTaggingGroup'];
						$semesterID = $sem['IdSemesterMaster'];
						$subjectId = $subj['IdSubject'];


						$totalAnswerSubmitted = $this->getAnswerSubmit($IdCourseTaggingGroup,$program, $branch,$semesterID,$subjectId, $main_set_id);

						$dataArray[$s]['answersemester'][$m]['total'] = $totalAnswerSubmitted;
					}
				}
			}
		}

//		echo "<pre>";
//		print_r($dataArray);
		return $dataArray;
	}
	
	
	public function getAnswerSubmitBySet($IdCourseTaggingGroup,$main_set_id,$chapterId,$subjectId, $questionId, $rubricWeightage=0,$IdProgram=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentregistration = a.student_id',array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id' /*,array('answer'=>'SUM(answer)')*/)
				->join(array('qt'=>'q003_questiontagging'),'qt.IdQuestion = b.question_id and qt.main_set_id = b.main_set_id and qt.section_id = b.chapter_id',array())
//				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id',array())
//				->join(array('cp'=>'q002_chapter'),'cp.main_set_id = c.id and cp.id = b.chapter_id',array())
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IDSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
				->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=sr.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
				->where('sr.Active NOT IN (3)')
				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
				->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
				->where('b.answer IS NOT NULL')
				->where("srs.IdProgram IN ($IdProgram)")
				->where("b.chapter_id IN ($chapterId)")
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN ($main_set_id)")
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1')
				//->group('a.id', 'a.pool_id')
				->group('b.student_id')
;

		if($questionId){
			$selectData->where("b.question_id = ?",$questionId);
		}
		
		/*if($rubricWeightage){
			$selectData->where("b.answer IN ($rubricWeightage)");
		}*/
		
		/*if($IdProgram){
			$selectData->where("srs.IdProgram IN ($IdProgram)");
		}*/
	//echo "<hr>";
	//echo $selectData; //exit;
		$rowData = $db->fetchAll($selectData);

		$sumAmount = 0;
		if ($rowData){
			foreach ($rowData as $rawDataLoop){
				if ($rawDataLoop['answer']==$rubricWeightage) {
					$sumAmount = $sumAmount + $rawDataLoop['answer'];
				}
			}
		}

		//$sumAmount = isset($rowData['answer'])?$rowData['answer']:0;
		
		$selectData2 = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentregistration = a.student_id',array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id' /*,array('total'=>'COUNT(answer)')*/)
				->join(array('qt'=>'q003_questiontagging'),'qt.IdQuestion = b.question_id and qt.main_set_id = b.main_set_id and qt.section_id = b.chapter_id',array())
//				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
//				->join(array('cp'=>'q002_chapter'),'cp.main_set_id = c.id')
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id and sr.IDSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
				->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=sr.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
				->where('sr.Active NOT IN (3)')
				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
				->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
				->where('b.answer IS NOT NULL')
                ->where("srs.IdProgram IN ($IdProgram)")
				->where("b.chapter_id IN ($chapterId)")
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN ($main_set_id)")
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1')
			//->group('a.id', 'a.pool_id')
			->group('b.student_id')
				;

		
		if($questionId){
			$selectData2->where("b.question_id = ?",$questionId);
		}
		
		/*if($rubricWeightage){
			$selectData2->where("b.answer IN ($rubricWeightage)");
		}*/
		
		/*if($IdProgram){
			$selectData2->where("srs.IdProgram IN ($IdProgram)");
		}*/
		
		//echo $selectData2.'<br><br>';

		$rowData2 = $db->fetchAll($selectData2);

		$totalAnswer = 0;
		if ($rowData2){
			foreach ($rowData2 as $rawData2Loop){
				if ($rawData2Loop['answer']==$rubricWeightage) {
					$totalAnswer++;
				}
			}
		}

	    //$totalAnswer = $rowData2['total'];

		$weightage=0;
		if($totalAnswer > 0){
			$weightage = round($sumAmount/$totalAnswer,2);
		}
		
		return array('totalAnswer'=>$totalAnswer,'weightage'=>$weightage,'sum'=>$sumAmount);
	}
	
	public function getReportByCourseQuestionnaire($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		
		$dataArray = array();
		
		//get list of section
		$chapterList = $this->getPoolList($searchData);
		
		foreach($chapterList as $c=>$chp){
			
			$dataArray[$c]['chapter'] = $chp['name'];
			$dataArray[$c]['chapterId'] = $chp['chapterid'];
			
			$chapterId = $chp['chapterid'];
			$mainSetId = $chp['main_set_id'];
			
			//get list of question
			$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
			$setQuestion = $QuestionSurvey->getSetQuestion($mainSetId,$chapterId);
			
			foreach($setQuestion as $q=>$ques){
				
				$dataArray[$c]['listquestion'][$q]['question'] = $ques['question'];
				$dataArray[$c]['listquestion'][$q]['questionId'] = $ques['IdQuestion'];
				$dataArray[$c]['listquestion'][$q]['question_type'] = $ques['question_type'];
				
				$selectData = $db->select()
								->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
								->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subcode_arr'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )'))
								->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
								->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array())
								->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
								->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
						
								->group('sm.Group')
								->order('c.FullName asc')
								;
								
				if(isset($program) && !empty($program)){
					$selectData->where("cgp.program_id IN (?)",$program);
				}
				
				if(isset($semester) && !empty($semester)){
					$selectData->where("tg.IdSemester IN (?)",$semester);
				}
				
				if($branch != 'ALL'){
					$selectData->where("tg.IdBranch = ?",$branch);
				}
				
				if(isset($mop) && !empty($mop)){
					$selectData->where("ps.mode_of_program = ?",$mop);
				}
				
		//		echo $selectData."<hr>";exit;
				$row = $db->fetchAll($selectData);
				foreach($row as $m=>$data){
					
					$dataArray[$c]['listquestion'][$q]['courselist'][$m]['course'] = $data['subcode_arr'];
					$dataArray[$c]['listquestion'][$q]['courselist'][$m]['courseId'] = $data['IdSubject'];
					
					$IdCourseTaggingGroup = $data['IdCourseTaggingGroup'];
					$subjectId = $data['IdSubject'];
				
					//get total answered
					$totalAnswerArray = $this->getAnswerSubmitBySet($IdCourseTaggingGroup,$mainSetId,$chapterId,$subjectId,$ques['IdQuestion']);
					$dataArray[$c]['listquestion'][$q]['courselist'][$m]['total'] = $totalAnswerArray;
					
				}
				
				if($ques['question_type'] != 3){
					unset($dataArray[$c]['listquestion'][$q]);
				}
			}
			
		}
		
		
//		echo "<pre>";
//		print_r($dataArray);
//		exit;
		
		return $dataArray;
	}
	
	public function getReportAnalysis($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
		
		$program = $searchData['program'];
		$semester = $searchData['semester'];
		$branch = $searchData['branch'];
		$mop = $searchData['mop'];
		
		$dataArray = array();
		
		//get list of section
		$chapterList = $this->getPoolList($searchData);
		
		foreach($chapterList as $c=>$chp){
			
			$dataArray[$c]['chapter'] = $chp['name'];
			$dataArray[$c]['chapterId'] = $chp['chapterid'];
			
			$chapterId = $chp['chapterid'];
			$mainSetId = $chp['main_set_id'];
			
			$rubricList = $QuestionSurvey->getRubricDetails($chp['chapterid']);
			$endVal = end($rubricList);
			$maxWeightage =$endVal['ValueNumber'];
			$dataArray[0]['maxWeightage'] = $maxWeightage;
			//get list of question
			
			$setQuestion = $QuestionSurvey->getSetQuestion($mainSetId,$chapterId);
			
			foreach($setQuestion as $q=>$ques){
				
				$dataArray[$c]['listquestion'][$q]['question'] = $ques['question'];
				$dataArray[$c]['listquestion'][$q]['questionId'] = $ques['IdQuestion'];
				$dataArray[$c]['listquestion'][$q]['question_type'] = $ques['question_type'];
				
				$selectData = $db->select()
								->from(array('tg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'=>'GROUP_CONCAT(DISTINCT(tg.IdCourseTaggingGroup))'))
								->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=tg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subcode_arr'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )'))
								->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = tg.IdLecturer',array('FullName'))
								->join(array('cgp'=>'course_group_program'), 'cgp.group_id = tg.IdCourseTaggingGroup',array())
								->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = cgp.program_scheme_id',array('mode_of_program'))
								->join(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = tg.IdBranch',array('BranchName'=>'BranchCode'))
						
								->group('sm.Group')
								->order('c.FullName asc')
								;
								
				if(isset($program) && !empty($program)){
					$selectData->where("cgp.program_id IN (?)",$program);
				}
				
				if(isset($semester) && !empty($semester)){
					$selectData->where("tg.IdSemester IN (?)",$semester);
				}
				
				if($branch != 'ALL'){
					$selectData->where("tg.IdBranch = ?",$branch);
				}
				
				if(isset($mop) && !empty($mop)){
					$selectData->where("ps.mode_of_program = ?",$mop);
				}
				
		//		echo $selectData."<hr>";exit;
				$row = $db->fetchAll($selectData);
				foreach($row as $m=>$data){
					
					$dataArray[$c]['listquestion'][$q]['courselist'][$m]['course'] = $data['subcode_arr'];
					$dataArray[$c]['listquestion'][$q]['courselist'][$m]['courseId'] = $data['IdSubject'];
					
					$IdCourseTaggingGroup = $data['IdCourseTaggingGroup'];
					$subjectId = $data['IdSubject'];
					
					if($ques['question_type'] == 3){
		    			$rubricList = $QuestionSurvey->getAnalysisDetails($chapterId);
//		    			$arrayPool[$a]['chapter'][$c]['rubric'] = $rubricList;
//$dataArray[$c]['listquestion'][$q]['courselist'][$m]['rubric'] = $rubricList;
	    			}
				 
	    			$totalPercentage = 0;
	    			foreach($rubricList as $r=>$rb){
						//get total answered
						$totalAnswerArray = $this->getAnswerAnalysis($IdCourseTaggingGroup,$mainSetId,$chapterId,$subjectId,$ques['IdQuestion'],$rb['ValueNumber']);
						$dataArray[$c]['listquestion'][$q]['courselist'][$m]['rubric'][$r]['ValueNumber'] = $rb['ValueNumber'];
						$dataArray[$c]['listquestion'][$q]['courselist'][$m]['rubric'][$r]['analysisValue'] = $rb['analysisValue'];
						
						$dataArray[$c]['listquestion'][$q]['courselist'][$m]['rubric'][$r]['total'] = $totalAnswerArray;
						
						$totalPercentage = 0;
						if($totalAnswerArray['totalAnswerAll'] != 0){
							$totalPercentage = round(($totalAnswerArray['totalAnswer']/$totalAnswerArray['totalAnswerAll']) * 100,2);
						}
						
						$dataArray[$c]['listquestion'][$q]['courselist'][$m]['rubric'][$r]['percentage'] = $totalPercentage;
						
	    			}
						
				}
				
				if($ques['question_type'] != 3){
					unset($dataArray[$c]['listquestion'][$q]);
				}
			}
			
		}
		
		
//		echo "<pre>";
//		print_r($dataArray);
//		exit;
		
		return $dataArray;
	}
	
	public function getAnswerAnalysis($IdCourseTaggingGroup,$main_set_id,$chapterId,$subjectId, $questionId, $rubricWeightage=0){ 
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('answer'=>'SUM(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id',array())
				->where('b.answer IS NOT NULL')
				->where("b.chapter_id IN ($chapterId)")
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN ($main_set_id)")
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1');
		
		if($questionId){
			$selectData->where("b.question_id = ?",$questionId);
		}
		
		if($rubricWeightage){
			$selectData->where("b.answer IN ($rubricWeightage)");
		}
//	echo "<hr>";
//	echo $selectData;
		$rowData = $db->fetchRow($selectData);
		$sumAmount = isset($rowData['answer'])?$rowData['answer']:0;
		
		$selectData2 = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('total'=>'COUNT(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id',array())
				->where('b.answer IS NOT NULL')
				->where("b.chapter_id IN ($chapterId)")
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN ($main_set_id)")
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1');

		
		if($questionId){
			$selectData2->where("b.question_id = ?",$questionId);
		}
		
		if($rubricWeightage){
			$selectData2->where("b.answer IN ($rubricWeightage)");
		}
		
//		echo $selectData.'<br><br>';
		$rowData2 = $db->fetchRow($selectData2);
	    $totalAnswer = $rowData2['total'];

		$weightage=0;
		if($totalAnswer > 0){
			$weightage = round($sumAmount/$totalAnswer,2);
		}
		
		//get all answered
		$selectData3 = $db->select()
				->from(array('a'=>'q010_student_answer_main'),array())
				->join(array('b'=>'q010_student_answer'),'b.IdMain = a.id',array('total'=>'COUNT(answer)'))
				->join(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
				->join(array('sr'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration = a.student_id',array())
				->where('b.answer IS NOT NULL')
				->where("b.chapter_id IN ($chapterId)")
				->where("a.IdSubject IN ($subjectId)")
				->where("a.main_set_id IN ($main_set_id)")
				->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
				->where('a.status = 1');
		
		if($questionId){
			$selectData3->where("b.question_id = ?",$questionId);
		}
		
		$rowData3 = $db->fetchRow($selectData3);
	    $totalAllAnswer = $rowData3['total'];
	
		return array('totalAnswer'=>$totalAnswer,'weightage'=>$weightage,'sum'=>$sumAmount,'totalAnswerAll'=>$totalAllAnswer);
	}
	
	public function getResponseStudentList($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram=0,$mopId=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
					
		$selectData2 = $db->select()
					->from(array('a'=>'tbl_studentregsubjects'))
					->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
					->join(array('StudentProfile'=>'student_profile'),'StudentProfile.id = srs.sp_id',array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name" ))
					 ->joinLeft(array('b' => 'tbl_program'), "srs.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme",'ProgramCode'))
					 ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=srs.IdProgramScheme', array())
			            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
					->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=StudentProfile.appl_nationality',array('Nationality'))
					->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
					->where('a.Active NOT IN (3)')
					->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
					->where("IFNULL(a.exam_status,'x') NOT IN ('CT','EX','U')")
					->where("srs.IdProgram IN ($IdProgram)")
					//->where("a.IdSemesterMain = ?",$semester)
					->where("a.IdSubject IN ($subjectId)")
					->where("a.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
					->group('srs.IdStudentRegistration');
//					->limit(5);
	if($IdProgram){
		//$selectData2->where("srs.IdProgram IN ($IdProgram)");
	}
	if($mopId){
		//$selectData2->where("ps.mode_of_program IN ($mopId)");
	}

        $selectData3 = $db->select()
					->from(array('a'=>'tbl_studentregsubjects'))
					->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
					->join(array('StudentProfile'=>'student_profile'),'StudentProfile.id = srs.sp_id',array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name" ))
					 ->joinLeft(array('b' => 'tbl_program'), "srs.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme",'ProgramCode'))
					 ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=srs.IdProgramScheme', array())
			            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
					->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=StudentProfile.appl_nationality',array('Nationality'))
					->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
					->where('a.audit_program IS NOT NULL')
                                        ->where('a.Active NOT IN (3)')
					->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
					->where("IFNULL(a.exam_status,'x') IN ('U')")
					->where("srs.IdProgram IN ($IdProgram)")
					//->where("a.IdSemesterMain = ?",$semester)
					->where("a.IdSubject IN ($subjectId)")
					->where("a.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)")
					->group('srs.IdStudentRegistration')
//					->limit(5)
;
	if($IdProgram){
		//$selectData3->where("srs.IdProgram IN ($IdProgram)");
	}
	if($mopId){
		//$selectData3->where("ps.mode_of_program IN ($mopId)");
	}
        
        $select = $db->select()
            ->union(array($selectData2, $selectData3));
//			echo $selectData2;exit;
			$row = $db->fetchAll($select);	
			
			
		return $row;
	}

	public function getFeedbackStatus($studentId,$semester,$subjectId,$IdCourseTaggingGroup,$main_set_id,$IdProgram=0,$mopId=0){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData3 = $db->select()
	       				->from(array('a' => 'q010_student_answer_main'))
	       				->join(array('sr'=>'tbl_studentregsubjects'),'a.student_id = sr.IdStudentRegistration and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
	       				->join(array('srs'=>'tbl_studentregistration'),'a.student_id = srs.IdStudentRegistration',array())
	       				->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array())						
	       				->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
	       				->where('sr.Active NOT IN (3)')
	       				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
						->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
	                    ->where('semester_id =?', $semester) 
	                    ->where('student_id = ?',$studentId)
//	                    ->where('a.status = ?',1)
	                    ->group('a.id');
	        if($subjectId!=0){
	        	$selectData3->where("a.IdSubject IN ($subjectId)") ;
	        }
	        
			if($IdCourseTaggingGroup!=0){
	        	$selectData3->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)") ;
	        }
	        
			if($IdProgram){
	        	$selectData3->where("srs.IdProgram IN ($IdProgram)") ;
	        }
	        
			if($mopId){
	        	$selectData3->where("ps.mode_of_program IN ($mopId)") ;
	        }
	        
		$rowData3 = $db->fetchAll($selectData3);
		
		$totalSubmit = 0; //completed
		
		if($rowData3){
			foreach($rowData3 as $data){
				if($data['status'] == 0){
					$totalSubmit = 1;
				}
			}
		}else{
			$totalSubmit = 1;
		}
		
		return $totalSubmit;
	}
	
function getTotalResponseData($parentid,$IdCourseTaggingGroup,$schemeId,$mopId,$IdProgram,$subjectId){
//echo $parentid.' <hr> '.$IdCourseTaggingGroup.' <hr> '.$schemeId.' <hr> '.$mopId.' <hr> '.$IdProgram.' <hr> '.$subjectId;
//exit;
    	$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
		
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$dataGroup = $courseGroupDb->getInfoArray($IdCourseTaggingGroup);
		
    	$SectionData = $sectiontagging->getAnswerByID($parentid,$IdProgram);

   	 	$arrayPool = array();
		$arraySumInfo = array();
    	$a = 0;
		foreach($SectionData as $secData){
//			$arrayPool[$a]['branch'] = $secData['BranchName'];
    		$arrayPool[$a]['pool_name'] = $secData['question_pool_name'];
    		$arrayPool[$a]['main_set_id'] = $secData['id'];
    		
    		//get chapter list
    		$chapterList = $QuestionSurvey->getPoolBySetDistinct($secData['id'],$secData['question_pool']);

			//var_dump($chapterList);

    		foreach($chapterList as $c=>$chp){
//    			$arrayPool[$a]['chapter'][$c] = $chp; 

    			$arrayPool[$a]['chapter'][$c]['chaptername'] = $chp['chaptername']; 
//    			$arrayPool[$a]['chapter'][$c]['chapterinstruction'] = $chp['subsection_instruction'];
    			$arrayPool[$a]['chapter'][$c]['question_type'] = $chp['question_type'];
    			
    			if($chp['question_type'] == 3){
	    			$rubricList = $QuestionSurvey->getRubricDetails($chp['chapterid']);
	    			$arrayPool[$a]['chapter'][$c]['rubric'] = $rubricList;
    			}
    			
    			//get list of question based on main set id
	    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id'],$chp['chapterid']);
				//var_dump($setQuestion);
	    		$totalWeightage = 0;
    			$totalRespondent = 0;
    			$totalWeightageAll = 0;
    			
	    		foreach($setQuestion as $q=>$qs){
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['question_id'] = $qs['IdQuestion'];
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['subject_id'] = $dataGroup['IdSubject'];
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['question'] = $qs['question']; 
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['order'] = 'Q'.$qs['order'];

					//var_dump($qs['question']);

	    			$program = $secData['program_id'];
	    			$branch = $secData['branch_id'];
	    			$semester = $secData['semester_id'];
	    			$subjectId = $dataGroup['IdSubject'];
	    			$main_set_id = $secData['id'];
	    			$pool_id = $secData['question_pool'];
	    			$registration_item_id = $chp['registration_item_id'];
	    			$section_id = $chp['id'];
	    			$chapterid = $chp['chapterid'];

	    			//get total answer by question
	    			if($chp['question_type'] == 3){ //likert
	    				
	    				foreach($rubricList as $r=>$rb){
	    					
			    			$totalAnswerArray = $surveyDb->getAnswerSubmitBySet($IdCourseTaggingGroup,$main_set_id,$chapterid,$subjectId,$qs['IdQuestion'],$rb['ValueNumber'],$IdProgram);
							//var_dump($totalAnswerArray);
//			    			echo "<pre>";
//		print_r($totalAnswerArray);
			    			$arrayPool[$a]['chapter'][$c]['question'][$q]['rubric'][$r]['rubricName'] = $rb['description'];
			    			$arrayPool[$a]['chapter'][$c]['question'][$q]['rubric'][$r]['rubricValue'] = $rb['ValueNumber'];
			    			$arrayPool[$a]['chapter'][$c]['question'][$q]['rubric'][$r]['total'] = $totalAnswerArray;
			    			
			    			$totalRespondent += $totalAnswerArray['totalAnswer'];
			    			$totalWeightage += $totalAnswerArray['sum'];
			    			
			    			
			    			
	    				}
	    				if($totalRespondent > 0){
	    					$totalWeightageAll = round($totalWeightage/$totalRespondent,2);
	    				}
	    				
	    				$arrayPool[$a]['chapter'][$c]['totalRespondent'] = $totalRespondent;
	    				$arrayPool[$a]['chapter'][$c]['totalWeightage'] = $totalWeightage;
	    				$arrayPool[$a]['chapter'][$c]['totalWeightageAll'] = $totalWeightageAll;
	    				
	    				$arraySumInfo[$c]['chaptername'] = $chp['chaptername'];
	    				$arraySumInfo[$c]['totalRespondent'] = $totalRespondent;
	    				$arraySumInfo[$c]['totalWeightage'] = $totalWeightage;
	    				$arraySumInfo[$c]['totalWeightageAll'] = $totalWeightageAll;
    				
	    				
	    			}elseif($chp['question_type'] == 2){//essay
	    				$answerArray = $surveyDb->getAnswerByQuestionType($IdCourseTaggingGroup,$subjectId, $main_set_id, $qs['IdQuestion']);
	    				
	    				
		    			$arrayPool[$a]['chapter'][$c]['question'][$q]['answer_essay'] = $answerArray;
	    			}
	    			
	    			
	    			
	    			
	    		}
	    		
    		}
    		
    		$a++;
    	}
//    	exit;
    	return array('detail'=>$arrayPool, 'summary'=>array_values($arraySumInfo));
    }
}