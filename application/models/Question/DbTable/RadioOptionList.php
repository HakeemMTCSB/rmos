<?php 
class App_Model_Question_DbTable_RadioOptionList extends Zend_Db_Table_Abstract
{
    protected $_name = 'radio_option_list';
	
	public function getData($main_set_id,$section_id,$tagging_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
	                 ->from(array('r'=>$this->_name))
	                 ->where('r.main_set_id = ?', $main_set_id)
	                 ->where('r.section_id = ?', $section_id)
	                 ->where('r.question_tagging_id = ?', $tagging_id)
	                 ->order('r.DatetimeInsert ASC')
	                 ->order('r.id ASC');
		$row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function updateData($dataArray,$id){
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->update($this->_name, $dataArray, 'id='.$id);	
	}
	
	public function addData($data){
		$id = $this->insert($data);
		return $id;		
	}
	
	public function deleteData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->delete($this->_name, 'id='.$id);	
	}
}
?>
