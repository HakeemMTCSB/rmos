<?php 
Class App_Model_Question_DbTable_ManagePublish extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_publish';
	protected $_primary = "id_Publish";
	protected $_menu = "main_set_id";   //PK q020_sectiontagging
	
	
	public function addData($data)
	{	
		$db = Zend_Db_Table::getDefaultAdapter();
		$this->insert($data);
		$id = $db->lastInsertId();
        
        return $id;
	}
	
	//display at survey-bank/publish/view (questionpooltagging)
	public function getInfo($main_set_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                 ->from(array('a'=>$this->_name))
	                 ->joinLeft(array('d'=>'tbl_publishdetails'),'d.id_Publish=a.id_Publish',array('id_PoolPublish'=>'d.id_PoolPublish'))
	                 ->joinleft(array('c'=>'q020_sectiontagging'),'c.id = d.id_PoolPublish',array('question_poolname'=>'c.question_pool'))
	                 ->joinLeft(array('b'=>'q001_pool'),'b.id = c.question_pool',array('question_pool'=>'b.name'))
	                 ->where('a.main_set_id = ?',$main_set_id)
	                 ->where('c.question_pool!=\'\'')
	                 ->group('d.id_PoolPublish');
		$row = $db->fetchAll($select);
		return $row;
	}
	
	//display taggingpool
	public function getPublishTagging($main_set_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db->select()
	                 ->from(array('a'=>$this->_name, array ("StartDate"=>"a.start_date","EndDate"=>"a.end_date","Publish"=>"a.condition")))
	                 ->joinLeft(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
	                 ->where('main_set_id =?',$main_set_id);
	                   
	                 
	     $row = $db->fetchRow($select);
		 return $row;
	}
	
	public function getPublishData($main_set_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
					 ->from(array('a'=>$this->_name))
					 ->joinLeft(array('c'=>'tbl_publishcondition'),'c.id = a.condition',array('conditionname'=>'c.condition'))
					 ->where('a.main_set_id=?',$main_set_id);
		$row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getData(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                 ->from(array('a'=>$this->_name, array ("StartDate"=>"a.start_date","EndDate"=>"a.end_date","Publish"=>"a.condition")))
	                 ->joinLeft(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id')
	                 ->where('c.parent_id = 0');
	                 
	     $row = $db->fetchAll($select);
		 return $row;
	}
	
	public function deletePublish($main_set_id)
	{
       $this->delete("main_set_id =".(int)$main_set_id);
	}
}
?>