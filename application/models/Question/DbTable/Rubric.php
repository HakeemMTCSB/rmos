<?php 
class App_Model_Question_DbTable_Rubric extends Zend_Db_Table_Abstract
{
    protected $_name = 'q004_rubricdetails';
	protected $_primary = "id";
	protected $_main = "id_Section";
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('u'=>$this->_name))
	                 ->where('u.'.$this->_primary.' = ' .$id);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
        
		}else{
			$select = $db->select()
	                 ->from($this->_name);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	        $row =  $row->toArray();
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}
		
		return $row;
	}
	
      Public function getSectionID($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
		             ->from(array('x' => $this->_name))
					 ->where('x.id_Section=?',$id);
//	 exit;
        $row = $db->fetchRow($select);	      
		return $row;
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db ->select()
					->from(array('x'=>$this->_name),array("idpool"=>"x.id_Pool","id_Section"=>"x.id_Section"))
					->joinLeft(array('y'=>'q001_pool'),'y.id = x.idPool',array('idValue'=>'y.id','poolname'=>'y.name','*'))					
		            ->joinLeft(array('z'=>'q005_question_type'),'z.id = x.question_type',array('questiontype'=>'z.name'))
		            ->joinLeft(array('w'=>'q002_chapter'),'w.id = x.id_Section')
		;
		            return $selectData;
	}
	

     public function getRubricBySection($pid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
	    $select = $db->select()
		             ->from($this->_name)
					 ->where('id_Pool='.$pid);
	 
        $row = $db->fetchAll($select);
	      
		return $row;
	}
	
	
	
	public function addData($q){
		
		$id=$this->insert($q);
		return $id;
		
	}
	
    public function addData2($q,$id_Section){
		
//     	print_r($id_Section);exit;		
      $id=$this->insert($q,$id_Section);
//		$id=$this->add($this->_main.' =' . (int)$id_Section, " id_Pool =".(int)$id_Pool);
		return $id;
	}
	
	
	public function updateData($q,$id_Section){
//				print_r($q, $id_Section);exit;	             
		      
		$this->update($q,$this->_main.' =' . (int)$id_Section);
	}
	
//   public function updateData2($id_Section,$id_Pool){
////				print_r($q, $id_Section);exit;	             
//		      
//		$this->update($this->_main.' =' . (int)$id_Section, " id_Pool =".(int)$id_Pool);
//	}
	
	public function deleteData($id_Section){
		
//     	print_r($id_Section);exit;		
      
		$this->delete($this->_main.' =' . (int)$id_Section);
		
	}
	
	public function deleteData2($id_Section, $id_Pool){
																												
//     	print_r($id_Section);exit;		
      
		$this->delete($this->_main.' =' . (int)$id_Section, " id_Pool =".(int)$id_Pool);
		
	}
	
	public function deleteDataRubric($id_Section){
																												
//     	print_r($id_Section);exit;		
      
		$this->delete("id_Section =".(int)$id_Section);
		
	}
	
	
	public function getAnswerByID ($id_Section)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db ->select()
					->from(array('x'=>$this->_name),array("id_Section"=>"x.id_Section","ValueNumber1"=>"x.ValueNumber","description1"=>"x.description","weightage1"=>"x.weightage"))
//					->joinLeft(array('y'=>'q001_pool'),'y.id = x.id_Pool',array('idValue'=>'y.id','poolname'=>'y.name','*'))					
		            ->joinLeft(array('w'=>'q002_chapter'),'w.id = x.id_Section')
		            ->where("x.id_Section = ?",$id_Section);
		            
		             return $row = $db->fetchAll($selectData);
		            
		
	}
}
?>
