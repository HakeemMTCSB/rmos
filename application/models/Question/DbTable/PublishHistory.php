<?php 
Class App_Model_Question_DbTable_PublishHistory extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_publish_history';
	
	public function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function getData($main_set_id,$condition)
	{		
		$select = $this->_db->select()
						    ->from(array('a'=>$this->_name))
						    ->where('a.main_set_id=?',$main_set_id)
						    ->where('a.condition=?',$condition);
		$row = $this->_db->fetchAll($select);
		return $row;
	}
	
	public function addData($data)
	{	
		$this->insert($data);
		return $this->_db->lastInsertId();
	}
	
	public function updateData($dataArray, $where)
	{
		$this->update($dataArray, $where);
	}
}
?>