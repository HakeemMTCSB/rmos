<?php 
Class App_Model_Question_DbTable_PublishDetails extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_publishdetails';
	protected $_primary = "id";
	protected $_main = "id_Publish";	//PK tbl_publish
	protected $_menu = "main_set_id";   //PK q020_sectiontagging
	
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function addData($data)
	{	
		$db = Zend_Db_Table::getDefaultAdapter();
		$this->insert($data);
		$id = $db->lastInsertId();
        
        return $id;
	}
	
	public function getPublishTagging($main_set_id,$id_PoolPublish)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
	                 ->from(array('a'=>$this->_name))
	                 ->joinLeft(array('c'=>'q020_sectiontagging'),'c.id = a.main_set_id',array('question_poolname'=>'c.id'))
	                 ->join(array('d'=>'tbl_publish'),'d.id_Publish=a.id_Publish',array('startdate'=>'d.start_date','enddate'=>'d.end_date','condition'=>'d.condition'))
	                 ->where('a.main_set_id = ?',$main_set_id)
	                 ->where('a.id_PoolPublish = ?',$id_PoolPublish);
	                 
		$row = $db->fetchAll($select);  
		return $row;

	}
	
	public function PublishExist($main_set_id,$condition)
	{
		$db = Zend_Db_Table::getDefaultAdapter();		
		$select = $db->select()
					 ->from(array('a'=>'tbl_publish'))
					 ->where('a.main_set_id=?',$main_set_id)
					 ->where('a.condition=?',$condition);
		$row = $db->fetchAll($select);
		return $row;
	}
	
	/*public function update($dataArray, $id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();	
		$db->update('tbl_publish', $dataArray, array('id_Publish'=>$id));		
	}*/
	
	public function deletePublishData($main_set_id)
	{
       $this->delete("main_set_id =".(int)$main_set_id);
	}
}
?>