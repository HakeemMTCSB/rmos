<?php

Class App_Model_Question_DbTable_SectionTagging extends Zend_Db_Table_Abstract
{
    protected $_name = 'q020_sectiontagging';
    protected $_primary = "id";
    protected $_main = "parent_id";
    protected $_protected = "status";

    public function getData($id = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $id = (int)$id;

        if ($id != 0) {

            $select = $db->select()
                ->from(array('u' => $this->_name))
                ->where('u.' . $this->_primary . ' = ' . $id);

            $stmt = $db->query($select);
            $row = $stmt->fetch();

        } else {
            $select = $db->select()
                ->from($this->_name);

            $stmt = $db->query($select);
            $row = $stmt->fetchAll();
            $row = $row;
        }

        if (!$row) {
            throw new Exception("There is No Data");
        }
        return $row;
    }

    public function getPaginateData($searchData=array())
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        
		if ( !empty($searchData) )
		{
			$branch = $searchData['branch_id'];
			$semester = $searchData['semester_id'];
			$program = $searchData['program_id'];
			$scheme = $searchData['IdProgramScheme'];
		}

        $select = $db->select()
            ->from(array('f' => $this->_name),array('*','createdDate'=>'f.dateUpd'))
            ->joinLeft(array('n' => 'tbl_branchofficevenue'), 'n.IdBranch=f.branch_id', array('branch_name' => 'n.BranchName'))
            ->joinLeft(array('r' => 'tbl_semestermaster'), 'r.IdSemesterMaster=f.semester_id', array('semester_id' => 'r.SemesterMainCode'))
            ->join(array('m' => 'tbl_program'), 'm.IdProgram = f.program_id', array('program_id' => 'm.ProgramCode'))
            ->joinLeft(array('o' => 'q001_pool'), 'o.id = f.question_pool', array('question_pool' => 'o.name'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = f.IdProgramScheme', array('mode_of_program','mode_of_study','program_type'))
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
            ->joinLeft(array('pbh' => 'tbl_publish'), 'pbh.main_set_id = f.id AND pbh.enable=1', array('startdate' => 'pbh.start_date', 'enddate' => 'pbh.end_date','enable','condition_name' => 'GROUP_CONCAT( DISTINCT pbh.condition)'))
//            ->joinLeft(array('e' => 'tbl_publishcondition'), 'e.id=pbh.condition', array('condition_name' => 'GROUP_CONCAT( DISTINCT e.condition)'))
            ->joinLeft(array('u'=>'tbl_user'), 'u.iduser = f.updUser', array())
			->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
            ->where('parent_id= 0')
            ->order('f.semester_id desc')
            ->order('f.program_id desc')
            ->order('f.IdProgramScheme desc')
            ->order('f.dateUpd desc')
            ->group('f.id');

    	if(isset($branch) && !empty($branch)){
			$select->where("f.branch_id = ?",$branch);
		}
		
    	if(isset($semester) && !empty($semester)){
			$select->where("f.semester_id = ?",$semester);
		}
		
   		if(isset($program) && !empty($program)){
			$select->where("f.program_id = ?",$program);
		}
		
    	if(isset($scheme) && !empty($scheme)){
			$select->where("f.IdProgramScheme = ?",$scheme);
		}
        $row = $db->fetchAll($select);    
        return $row;

    }

    public function getInfo($parent_id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('f' => $this->_name, array('*')))
            ->join(array('o' => 'q001_pool'), 'o.id = f.question_pool', array('question_pool' => 'o.name'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = f.IdProgramScheme')
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
            ->where('parent_id = ?', $parent_id);

//		  return $select;
        $row = $db->fetchAll($select);
        return $row;
    }


    public function getSectionTagging($idParent, $idPool)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('f' => $this->_name, array('*')))
            ->where('parent_id = ?', $idParent)
            ->where('question_pool = ?', $idPool);

//		  return $select;
        $row = $db->fetchRow($select);
        return $row;
    }

    public function addData($data)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $this->insert($data);
        $id = $db->lastInsertId();

        return $id;
    }

    public function updateData($data, $id)
    {
        $this->update($data, $this->_primary . ' =' . (int)$id);
    }

    public function updateData2($q, $parent_id)
    {
        $this->update($q, $this->_main . ' =' . (int)$parent_id);
    }

    public function deleteData($id)
    {
        $this->delete($this->_primary . ' =' . (int)$id);
    }

    public function deleteItem($id)
    {
        $this->delete('parent_id =' . (int)$id);
    }

    public function deleteAnswer($parent_id)
    {
//     	print_r($id_Section);exit;		

        $this->delete("parent_id =" . (int)$parent_id);

    }

    public function getDataByParentID($id)
    {
        // get branch,intake,status,program
        $db = Zend_Db_Table::getDefaultAdapter();


        $select = $db->select()
            ->from(array('f' => $this->_name))
            ->joinLeft(array('n' => 'tbl_branchofficevenue'), 'n.IdBranch=f.branch_id', array('branch_name' => 'n.BranchName'))
            ->joinLeft(array('r' => 'tbl_semestermaster'), 'r.IdSemesterMaster=f.semester_id', array('semester_name' => 'r.SemesterMainName'))
            ->join(array('m' => 'tbl_program'), 'm.IdProgram = f.program_id', array('program_name' => 'm.ProgramName'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = f.IdProgramScheme')
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
//		             ->joinLeft(array('o'=>'q001_pool'),'o.id = f.question_pool',array('question_pool'=>'o.name'))
            ->where("f.parent_id=?", $id);

        return $row = $db->fetchAll($select);
    }

    public function getDataByID($id)
    {
        // get branch,intake,status,program
        $db = Zend_Db_Table::getDefaultAdapter();


        $select = $db->select()
            ->from(array('f' => $this->_name))
            ->joinLeft(array('n' => 'tbl_branchofficevenue'), 'n.IdBranch=f.branch_id', array('branch_name' => 'n.BranchName'))
            ->joinLeft(array('r' => 'tbl_semestermaster'), 'r.IdSemesterMaster=f.semester_id', array('semester_name' => 'r.SemesterMainCode'))
            ->join(array('m' => 'tbl_program'), 'm.IdProgram = f.program_id', array('program_name' => 'm.ProgramName'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = f.IdProgramScheme')
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
//		             ->joinLeft(array('o'=>'q001_pool'),'o.id = f.question_pool',array('question_pool'=>'o.name'))
            ->where("f.id=?", $id);

        return $row = $db->fetchRow($select);
    }

	public function getAnswerByIDAll($parent_id)
    {

        //get question pool
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('f' => $this->_name, array('*')))
            ->join(array('o' => 'q001_pool'), 'o.id = f.question_pool', array('question_pool_name' => 'o.name'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = f.IdProgramScheme')
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
            ->where('parent_id = ?', $parent_id);

//            echo "<hr>".$select;
        $rows = $db->fetchAll($select);
        return $rows;

    }

    public function getAnswerByID($parent_id,$IdProgram=array())
    {

        //get question pool
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('f' => $this->_name), array('*','id'=>'GROUP_CONCAT(DISTINCT(f.id))'))
            ->join(array('o' => 'q001_pool'), 'o.id = f.question_pool', array('question_pool_name' => 'o.name'))
            ->joinLeft(array('b'=>'q002_chapter'),'b.pool_id = f.question_pool and b.main_set_id = f.id',array())
            ->where("f.parent_id IN ($parent_id)");

//            if($IdProgram){
//            	$select->where("f.program_id IN ($IdProgram)");
//            } takyah check program - ni kes nk cater audit credit 05082015
//            echo "<hr>".$select;
        $rows = $db->fetchAll($select);
        return $rows;

    }

    public function getSetupTagging($branchID, $programID, $idSemester, $poolID = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('f' => $this->_name, array('*')))
            ->where('f.branch_id = ?', $branchID)
            ->where('f.program_id = ?', $programID);

        if ($idSemester > 0)
            $select->where('f.semester_id = ?', $idSemester);

        if ($poolID) {
            $select->where('f.question_pool = ?', $poolID);
            $row = $db->fetchRow($select);
        } else {
            $row = $db->fetchAll($select);
        }

//		  return $select;

        return $row;
    }
    
 	public function getSetupTaggingDetail($branchID, $idSemester, $programID, $programSchemeID)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('f' => $this->_name))
            ->where('f.branch_id = ?', $branchID)
            ->where('f.semester_id = ?', $idSemester)
            ->where('f.IdProgramScheme = ?', $programSchemeID)
            ->where('f.program_id = ?', $programID)
            ->where('f.parent_id = 0');

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getTaggingData($searchData=array())
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $branch   = isset($searchData['branch_id']) ? $searchData['branch_id'] : 0;
        $semester = isset($searchData['semester_id']) ? $searchData['semester_id'] : 0;
        $program  = isset($searchData['program_id']) ? $searchData['program_id'] : 0;
        $scheme   = isset($searchData['IdProgramScheme']) ? $$searchData['IdProgramScheme'] : 0;
      
        $select = $db->select()
            ->from(array('f' => $this->_name, array("status" => "f.status", "semester_id" => "f.semester_id")))
            ->joinLeft(array('n' => 'tbl_branchofficevenue'), 'n.IdBranch=f.branch_id', array('branch_name' => 'n.BranchName'))
            ->joinLeft(array('r' => 'tbl_semestermaster'), 'r.IdSemesterMaster=f.semester_id', array('semester_id' => 'r.SemesterMainCode'))
            ->join(array('m' => 'tbl_program'), 'm.IdProgram = f.program_id', array('program_id' => 'm.ProgramCode'))
            ->joinLeft(array('o' => 'q001_pool'), 'o.id = f.question_pool', array('question_pool' => 'o.name'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = f.IdProgramScheme', array('mode_of_program','mode_of_study','program_type'))
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
            
            ->joinLeft(array('pbh' => 'tbl_publish'), 'pbh.main_set_id = f.id AND pbh.enable=1', array('startdate' => 'pbh.start_date', 'enddate' => 'pbh.end_date'))
            ->joinLeft(array('e' => 'tbl_publishcondition'), 'e.id=pbh.condition', array('condition_name' => 'GROUP_CONCAT( DISTINCT e.condition)'))
            ->where('parent_id= 0')
            ->group('f.IdProgramScheme')
            ->group('pbh.main_set_id')
            ;
       
            
        $row = $db->fetchAll($select);
        return $row;
    }

    public function getSetPool($parent_id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'q020_sectiontagging'), array("*", 'idMain' => 'a.id'))
            ->joinLeft(array('o' => 'q001_pool'), 'o.id = a.question_pool', array('pool_name' => 'o.name', '*'))
            ->join(array('b' => 'q003_questiontagging'), 'b.main_set_id = a.id', array(''))
            ->where('a.parent_id = ?', $parent_id)
            ->group('b.main_set_id');
        $row = $db->fetchAll($select);
        return $row;
    }

    public function getListSemesterByPool($poolId)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'q020_sectiontagging'), array("*", 'idMain' => 'a.id'))
            ->joinLeft(array('o' => 'q001_pool'), 'o.id = a.question_pool', array('pool_name' => 'o.name', '*'))
            ->join(array('b' => 'q003_questiontagging'), 'b.main_set_id = a.id', array(''))
            ->join(array('r' => 'tbl_semestermaster'), 'r.IdSemesterMaster=a.semester_id', array('semester_name' => 'r.SemesterMainCode'))
            ->join(array('m' => 'tbl_program'), 'm.IdProgram = a.program_id', array('ProgramCode'))
            ->joinLeft(array('ProgramScheme' => 'tbl_program_scheme'), 'ProgramScheme.IdProgramScheme = a.IdProgramScheme', array('mode_of_program','mode_of_study','program_type'))
            ->joinLeft(array('ModeOfProgram' => 'tbl_definationms'), 'ModeOfProgram.idDefinition = ProgramScheme.mode_of_program', array('mode_of_program_name' => 'ModeOfProgram.DefinitionDesc'))
            ->joinLeft(array('ModeOfStudy' => 'tbl_definationms'), 'ModeOfStudy.idDefinition = ProgramScheme.mode_of_study', array('mode_of_studies_name' => 'ModeOfStudy.DefinitionDesc'))
            ->joinLeft(array('ModeOfProgramType' => 'tbl_definationms'), 'ModeOfProgramType.idDefinition = ProgramScheme.program_type', array('mode_of_program_type_name' => 'ModeOfProgramType.DefinitionDesc'))
            
            ->where('a.question_pool = ?', $poolId)
            ->group('b.main_set_id');
        $row = $db->fetchAll($select);
        return $row;
    }

    public function getListQuestion($idParent, $idSection)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'q003_questiontagging', array('*')))
            ->join(array('b' => 'q003_question'), 'b.id = a.IdQuestion', array('question' => 'b.question'))
            ->where('a.main_set_id = ?', $idParent)
            ->where('a.section_id = ?', $idSection);

//		  return $select;
        $row = $db->fetchAll($select);
        return $row;
    }
    
  public function getListEvaluation($branch_id, $program_id, $IdProgramScheme, $semester_id) {
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	 
  	 /*
		 * $selectData3 = $db->select()
	       				->from(array('a' => 'q010_student_answer_main'))
	       				->join(array('sr'=>'tbl_studentregsubjects'),'a.student_id = sr.IdStudentRegistration and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
	       				->join(array('srs'=>'tbl_studentregistration'),'a.student_id = srs.IdStudentRegistration',array())
	       						->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
	       				->where('sr.Active NOT IN (3)')
	       				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
						->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
	                    ->where('semester_id =?', $semester) 
	                    ->where('student_id = ?',$studentId)
//	                    ->where('a.status = ?',1)
	                    ->group('a.id');
	        if($subjectId!=0){
	        	$selectData3->where("a.IdSubject IN ($subjectId)") ;
	        }
	        
			if($IdCourseTaggingGroup!=0){
	        	$selectData3->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)") ;
	        }
	        
		if($IdProgram){
	        	$selectData3->where("srs.IdProgram IN ($IdProgram)") ;
	        }
		 */
  	 
       $select = $db->select()
       				->from(array('a' => 'q020_sectiontagging'))
       				->join(array('b' => 'q001_pool'), 'b.id = a.question_pool',array('type','target_user','name'))
                    ->where('branch_id =?', $branch_id)
                    ->where("program_id IN (?)",$program_id)
                    ->where("IdProgramScheme IN ($IdProgramScheme)" )
                    ->where('semester_id =?', $semester_id)
                    ;
//        echo $select."<hr>";exit;
        $question = $db->fetchAll($select);
        
        return($question);
    }
    
	public function getTotalAnsweredByStudent($student_id,$semester_id,$subject=0,$IdCourseTaggingGroup=0,$IdProgram=0) {
	  	   $db = Zend_Db_Table::getDefaultAdapter();
	  	   
	  	   /*
		 * $selectData3 = $db->select()
	       				->from(array('a' => 'q010_student_answer_main'))
	       				->join(array('sr'=>'tbl_studentregsubjects'),'a.student_id = sr.IdStudentRegistration and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
	       				->join(array('srs'=>'tbl_studentregistration'),'a.student_id = srs.IdStudentRegistration',array())
	       						->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
	       				->where('sr.Active NOT IN (3)')
	       				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
						->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
	                    ->where('semester_id =?', $semester) 
	                    ->where('student_id = ?',$studentId)
//	                    ->where('a.status = ?',1)
	                    ->group('a.id');
	        if($subjectId!=0){
	        	$selectData3->where("a.IdSubject IN ($subjectId)") ;
	        }
	        
			if($IdCourseTaggingGroup!=0){
	        	$selectData3->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)") ;
	        }
	        
		if($IdProgram){
	        	$selectData3->where("srs.IdProgram IN ($IdProgram)") ;
	        }
		 */
	  	   
	  	   //get active only
	       $select = $db->select()
	       				->from(array('a' => 'q010_student_answer_main'))
	       				->join(array('sr'=>'tbl_studentregsubjects'),'a.student_id = sr.IdStudentRegistration and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
	       				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())
	       				->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode')) 
	       				->where('sr.Active NOT IN (3)')
	       				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
						->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
	                    //->where('semester_id =?', $semester_id) 
	                    ->where('student_id = ?',$student_id)
	                    ->where('a.status = ?',1)
	                    ->group('a.id');
	        if($subject!=0){
	        	$select->where("a.IdSubject IN ($subject)") ;
	        }
	        
			if($IdCourseTaggingGroup!=0){
	        	$select->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)") ;
	        }
	        
			if($IdProgram){
	        	//$select->where("srs.IdProgram IN ($IdProgram)") ;
	        }
                
                $select2 = $db->select()
	       				->from(array('a' => 'q010_student_answer_main'))
	       				->join(array('sr'=>'tbl_studentregsubjects'),'a.student_id = sr.IdStudentRegistration and sr.IdSemesterMain = a.semester_id and sr.IdSubject = a.IdSubject',array())
	       				->join(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())
	       				->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode')) 
	       				->where('sr.Active NOT IN (3)')
	       				->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
						->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
	                    //->where('semester_id =?', $semester_id) 
	                    ->where('student_id = ?',$student_id)
	                    ->where('a.status = ?',1)
	                    ->group('a.id');
	        if($subject!=0){
	        	$select2->where("a.IdSubject IN ($subject)") ;
	        }
	        
			if($IdCourseTaggingGroup!=0){
	        	$select2->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)") ;
	        }
	        
			if($IdProgram){
	        	//$select2->where("srs.IdProgram IN ($IdProgram)") ;
	        }
//	                     echo $select."<hr>";	
                
                $select3 = $db->select()
                    ->union(array($select, $select2));
                
	        $question = $db->fetchAll($select3);	        
	        return count($question);
    }
    
    
	public function getAnsweredStudent($student_id,$semester_id,$subject=0,$IdCourseTaggingGroup=0,$questionId) {
	  	   $db = Zend_Db_Table::getDefaultAdapter();
	  	   
	  	   //get active only
	       $select = $db->select()
	       				->from(array('a' => 'q010_student_answer'))
	       				->join(array('b'=>'q010_student_answer_main'),'b.id = a.IdMain',array('*','statusMain'=>'status'))
	       				->join(array('sr'=>'tbl_studentregsubjects'),'b.student_id = sr.IdStudentRegistration and sr.IdSemesterMain = b.semester_id and sr.IdSubject = b.IdSubject',array())
	       				->where('sr.Active NOT IN (3)')
						->where("IFNULL(sr.exam_status,'x') NOT IN ('CT','EX','U')")
	                    ->where('sr.IdSemesterMain =?', $semester_id) 
	                    ->where('b.student_id = ?',$student_id)
//	                    ->where('b.status = ?',1)
	                    ->where('a.question_id = ?',$questionId)
//	                    ->where('a.chapter_id = ?',$chapterId)
	                    ->group('a.id');
	        if($subject!=0){
	        	$select->where("b.IdSubject IN ($subject)") ;
	        }
	        
			if($IdCourseTaggingGroup!=0){
	        	$select->where("sr.IdCourseTaggingGroup IN ($IdCourseTaggingGroup)") ;
	        }
//	                     echo $select."<hr>";	    exit;   
	        $question = $db->fetchRow($select);	        
	        return $question;
    }

    public function getLecturer($group){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array(
                'lecturerId'=>'a.IdLecturer',
                'lecturername'=>'b.FullName'
            ))
            ->join(array('b'=>'tbl_staffmaster'), 'b.IdStaff = a.IdLecturer', array())
            ->where('a.IdCourseTaggingGroup = ?', $group);

        $select2 = $db->select()
            ->from(array('a'=>'course_group_schedule'), array(
                'lecturerId'=>'a.IdLecturer',
                'lecturername'=>'b.FullName'
            ))
            ->join(array('b'=>'tbl_staffmaster'), 'b.IdStaff = a.IdLecturer', array())
            ->where('a.idGroup = ?', $group);

        $select3 = $db->select()
            ->from(array('a'=>'tbl_schedule_details'), array(
                'lecturerId'=>'a.tsd_lecturer',
                'lecturername'=>'b.FullName'
            ))
            ->join(array('b'=>'tbl_staffmaster'), 'b.IdStaff = a.tsd_lecturer', array())
            ->where('a.tsd_groupid = ?', $group);

        //echo $select1.'<br />';
        //echo $select2.'<br />';
        //echo $select3.'<br />'; exit;

        $select = $db->select()
            ->union(array($select1, $select2, $select3))
            ->group('lecturerId');

        $result = $db->fetchAll($select);
        return $result;
    }

}

/*

//to get section by scheme
SELECT GROUP_CONCAT(DISTINCT(d.id)),d.registration_item_id,d.name
FROM `q020_sectiontagging` a
join tbl_program_scheme b on b.IdProgramScheme = a.IdProgramScheme
join tbl_program c on c.IdProgram = b.IdProgram
join q002_chapter d on d.main_set_id = a.id and d.pool_id = a.question_pool and d.status = 1
where a.parent_id !=0 and c.IdScheme = 1
group by d.pool_id,d.registration_item_id

 * 
 */

?>
