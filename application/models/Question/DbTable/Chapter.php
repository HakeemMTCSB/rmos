<?php 
class App_Model_Question_DbTable_Chapter extends Zend_Db_Table_Abstract
{
    protected $_name = 'q002_chapter';
	protected $_primary = "id";
	protected $_main = "status";
	
   public function init()
    {
    	$this->lobjdb = Zend_Db_Table::getDefaultAdapter();
    }
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('u'=>$this->_name))
	                 ->where('u.'.$this->_primary.' = ' .$id);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
        
		}else{
			$select = $db->select()
	                 ->from($this->_name);
			                     
	        $stmt = $db->query($select);
	        $row  = $stmt->fetchAll();
	       // $row  =  $row->toArray();
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}
//		echo $select;
		return $row;
	}
	
	//display question
	public function getTopicbyPool($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
	    $select = $db->select()
		             ->from($this->_name)
					 ->where('id='.$id);
	 
        $row = $db->fetchAll($select);
        
		return $row;
	}
	
	
//   public function getQuestionTypebySection($id){
//		
//		$db = Zend_Db_Table::getDefaultAdapter();
//		
//	    $select = $db->select()
//		             ->from(array('a'=>$this->_name))
//		             ->joinLeft(array('e'=>'q005_question_type'),'e.id = a.question_type',array('questiontype'=>'e.name'))
//					 ->where('pool_id='.$id);
//	 
//        $row = $db->fetchAll($select);
//      
//		return $row;
//	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();		
	    $selectData = $db ->select()
					->from(array('a'=>$this->_name),array("id"=>"a.id","id_Pool"=>"a.pool_id","idChapter"=>"a.id","chaptername"=>"a.name"))
					->joinLeft(array('b'=>'q001_pool'),'b.id = a.pool_id',array('idValue'=>'b.id','poolname'=>'b.name','*'))
					->joinLeft(array('c'=>'q005_question_type'),'c.id = a.question_type',array('questiontype'=>'c.name'));

					return $selectData;
	}
	
	public function addData($data){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$this->insert($data);
		$id = $db->lastInsertId();
        
        return $id;
	}
	
	public function updateData($data,$id){
		$this->update($data,$this->_primary.' =' . (int)$id);
	}
	
	public function deleteData($id){
//		print_r($id);exit;
		
		$this->delete($this->_primary .' =' . (int)$id);		
	}
	
	public function getDataByID($id){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  $selectData = $db ->select()
		  
					->from(array('a'=>$this->_name),array("idChapter"=>"a.id","mainset"=>"a.main_set_id","chaptername"=>"a.name",'status'=>'a.status','question_type_id'=>'a.question_type', 'subsection_instruction' => 'a.subsection_instruction','registration_item_id','order'))
					->joinLeft(array('b'=>'q001_pool'),'b.id=a.pool_id',array('poolname'=>'b.name'))
					->joinLeft(array('c'=>'q005_question_type'),'c.id = a.question_type',array('questiontype'=>'c.name'))
					->where("a.id= ?",$id);
//					->where("a.main_set_id=?",$main_set_id);
//					exit;
					
		 return $row = $db->fetchRow($selectData);
	}
	
	//display at views (sectiontagging)
	public function getTopicbySet($id,$idChapter=0){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  $selectData = $db ->select()
		  
					->from(array('a'=>$this->_name),array('id','mainset'=>"a.main_set_id","idChapter"=>"a.id","chaptername"=>"a.name",'status'=>'a.status','question_type_id'=>'a.question_type','order','subsection_instruction'))
					->joinLeft(array('c'=>'q005_question_type'),'c.id = a.question_type',array('questiontype'=>'c.name'))
					->joinLeft(array('e'=>'q020_sectiontagging'),'e.id=a.main_set_id',array('questionpool'=>'e.question_pool'))
//					->joinLeft(array('g'=>'q003_questiontagging'),array('totalquestion'=>'count(g.IdQuestion)'))
					->joinLeft(array('d'=>'q001_pool'),'d.id=e.question_pool',array('pool'=>'d.name'))
					->joinLeft(array('i'=>'tbl_definationms'), 'a.registration_item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))
					->where("a.main_set_id = ?",$id)
					->order('a.order ASC')
					;
		if($idChapter){	
			$selectData->where("a.id = ?",$idChapter);
			$row = $db->fetchRow($selectData);
		}else{
			 $row = $db->fetchAll($selectData);
		}		
				return $row;
	}
	
	//untuk manage question popup
    public function getSectionByPool($id){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  $selectData = $db ->select()
		  
					->from(array('a'=>$this->_name),array('mainset'=>"a.main_set_id","idChapter"=>"a.id","chaptername"=>"a.name",'status'=>'a.status','question_type_id'=>'a.question_type'))
					->joinLeft(array('c'=>'q005_question_type'),'c.id = a.question_type',array('questiontype'=>'c.name'))
					->joinLeft(array('e'=>'q020_sectiontagging'),'e.id=a.main_set_id',array('questionpool'=>'e.question_pool'))
					->joinLeft(array('d'=>'q001_pool'),'d.id=e.question_pool',array('pool'=>'d.name'))
					->where("a.main_set_id = ?",$id);
					
							 return $row = $db->fetchAll($selectData);
	}

	public function update_order($id, $order) {
		$data['order'] = $order;
		$this->update($data,  'id = '. (int) $id);
		return(true);
	}
	
	
	

//     public function getDataByQT($question_type){
     	
//		$lstrselect = $this->lobjdb->select()
//    						->from(array('a'=>'q002_chapter'),array("*",'question_type_id'=>'a.question_type'))
//    						->where('a.question_type =?',$question_type);
////    						->group('a.topic_id');
//    	$lobjresultset = $this->lobjdb->fetchAll($lstrselect);
//    	return $lobjresultset;
		
//	}
}
?>
