<?php 
Class App_Model_Question_DbTable_QuestionTagging extends Zend_Db_Table_Abstract
{
    protected $_name = 'q003_questiontagging';
	protected $_primary = "id_questiontagging";
	protected $_main = "main_set_id";
	protected $_menu = "section_id";
//	protected $_protected = "";

	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('u'=>$this->_name))
	                 ->where('u.'.$this->_primary.' = ' .$id);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
        
		}else{
			$select = $db->select()
	                 ->from($this->_name);
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	        $row =  $row;
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}
		"select";
		return $row;
	}
	
	public function getPaginateData()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db ->select()
							->from($this->_name);
		
		return $selectData;
	}
	
	//soalan yang ditag
 	public function getQuestionTagging($main_set_id,$id)
 	{
		  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db ->select()
		  
					->from(array('a'=>$this->_name))
					->join(array('b'=>'q003_question'),'b.id = a.IdQuestion')
					->where("a.main_set_id=?",$main_set_id)
					->where("a.section_id= ?",$id);
					
		 return $row = $db->fetchAll($select);
	}
	
   public function addData($data)
	{	
		$db = Zend_Db_Table::getDefaultAdapter();
		$this->insert($data);
		$id = $db->lastInsertId();
        
        return $id;
	}
	
//	public function updateData()
//	{	
//          $this->update($data, $this->_primary .' =' . (int)$id);
//	}
	 
	public function deleteQuestion($section_id)
	{
       $this->delete("section_id =".(int)$section_id);
	}
	
	//display question yg ditag
public function getDisplayQuestion($main_set_id,$id)
 	{
		  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db ->select()
		  
					->from(array('a'=>$this->_name))
					->join(array('b'=>'q003_question'),'b.id = a.IdQuestion')
					->where("a.main_set_id=?",$main_set_id)
					->where("a.section_id= ?",$id)
					->order('order ASC');
					
		 return $row = $db->fetchAll($select);
	}
	//guna dekat manage question
public function getManageTagging($main_set_id,$section_id,$id)
 	{
		  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db ->select()
		  
					->from(array('a'=>$this->_name))
					->join(array('b'=>'q003_question'),'b.id = a.IdQuestion',array('question_id'=>'b.id'))
					->where("a.main_set_id=?",$main_set_id)
					->where ("a.section_id=?",$section_id)
					->where("b.id= ?",$id);
					
		 return $row = $db->fetchAll($select);
}

public function update_order($id, $order) {
	$data['order'] = $order;
	$a = $this->update($data,  'id_questiontagging = '. (int) $id);
	return(true);
}
	
	
}
