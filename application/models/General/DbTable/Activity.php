<?php
class App_Model_General_DbTable_Activity extends Zend_Db_Table { 	
	protected $_name = 'tbl_activity'; // table name
	private $lobjDbAdpt;
	protected $addDropId = '18';

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}	
	
	public function fnaddActivity($larrformData) {
		$this->insert($larrformData);
	}

	public function fngetActivityDetails() { //Function to get the Activity details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $lobjDbAdpt->select()
					  ->from(array("a"=>"tbl_activity"),array("a.*"))
					  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=a.ActivityType',array('ActivityTypeName'=>'DefinitionCode'));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnupdateActivity($formData,$lintIdActivity) { //Function for updating the Activity details
		unset ( $formData ['Save'] );
		$where = 'idActivity = '.$lintIdActivity;
		$this->update($formData,$where);
	}

	public function fnDeleteActivity($idActivity) {  // function to delete activity details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_activity';
		$where = $db->quoteInto('idActivity = ?', $idActivity);
		$db->delete('tbl_activity', $where);
	}

	public function fnSearchActivity($post = array()) { //Function for searching the Activity details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					  ->from(array("a"=>"tbl_activity"),array("a.*"))
					  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=a.ActivityType',array('ActivityTypeName'=>'DefinitionCode'))
					  ->where('a.ActivityName like "%" ? "%"',$post['field3'])
					  ->order("a.ActivityName");
				  
		if(isset($post['field5']) && $post['field5']!=''){
			$lstrSelect->where('a.ActivityType = ?',$post['field5']);
		}

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

		return $larrResult;
	}

	public function checkColor($color='',$id='')
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
					  ->from(array("a"=>"tbl_activity"),array("a.*"))
					  ->where('a.ActivityColorCode = ?',$color)
					  ->order("a.ActivityName");
				  
		if ($id != '')
		{
			$lstrSelect->where('a.idActivity != ?',$id);
		}

		$larrResult = $db->fetchRow($lstrSelect);

		return !empty($larrResult) ? false : true;
	}


  public function fngetActivityList(){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()->from(array('a' => $this->_name),array("key" => "a.idActivity" , "value" => "IFNULL(a.ActivityName,'')"));
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  // Function to add calender entry for semester
  public function fnaddCalenderActivity($formData){
    
  	$auth = Zend_Auth::getInstance();
  	
    $idSem = explode('_',$formData['IdSemester']);
    if($idSem['1']=='detail') { 
        $IdSemester = $idSem['0'];
        $IdSemesterMain = NULL;
    } else {
        $IdSemesterMain = $idSem['0'];
        $IdSemester = NULL;
    } 
    $activityMappingtable = new Zend_Db_Table('tbl_activity_calender');
    $data = array('IdSemester'=> $IdSemester,
                  'IdSemesterMain'=> $IdSemesterMain,
                  'IdActivity' => $formData['IdActivity'],
                  'StartDate' => $formData['StartDate'],
                  'EndDate' => $formData['EndDate'],
                  'createdby' => $auth->getIdentity()->iduser,
     			  'createddt' => date('Y-m-d H:i:s')
            );
    
    if($activityMappingtable->insert($data)){
      return true;
    }
    return false;
  }

  // Function to get calender and semester mapping entries according to semester id
  public function getcalenderMappingDetail($semId){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.IdSemester = ?',$semId);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  // Function to get the calender detatil according to id
  public function getcalenderMappingDetailById($Id){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.id = ?',$Id);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  // Function to get calender and semester mapping entries according to semester id
  public function getActivityDetailById($Id){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity'))->where('a.idActivity = ?',$Id);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  //Function to check semester mapping exist in activity semester mapping table
  public function checkcalenderexist($semId){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.IdSemester = ?',$semId);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    if(!empty($larrResult)){
      return $larrResult;
    }
    return false;
  }
  
  
  //Function to check semester mapping exist in activity semester mapping table
  public function checkcalenderexistmain($semId){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.IdSemesterMain = ?',$semId);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    if(!empty($larrResult)){
      return $larrResult;
    }
    return false;
  }
  

  public function getcalenderArray($calender,$activityDetail){
    $tem = array();
    $tem['activityid'] = $calender['IdActivity'];
    $tem['activityname'] = $activityDetail['ActivityName'];
    $tem['colorcode'] = $activityDetail['ActivityColorCode'];
    $tem['calenderId'] = $calender['id'];
    $tem['calenderstartdate'] = $calender['StartDate'];
    $tem['calenderenddate'] = $calender['EndDate'];
    return $tem;
  }

  public function fnDeletecalender($id) {  // function to delete activity details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = 'tbl_activity_calender';
    $where = $db->quoteInto('id = ?', $id);
    $db->delete('tbl_activity_calender', $where);
  }

  public function fnupdatecalender($formData) { //Function for updating the Activity details
    unset($formData['Save']);
    $calenderId = $formData['id'];
    unset($formData['id']);    
    $where = 'id = '.$calenderId;
    $db = Zend_Db_Table::getDefaultAdapter();
    $db->update('tbl_activity_calender' , $formData , $where);
  }


  public function getcalenderDetails($id){
    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $db->select()
              ->from(array('a' => 'tbl_activity_calender'))
              ->joinLeft(array("b"=>"tbl_semester"),'a.IdSemester = b.IdSemester', array('b.SemesterCode as SemesterCodeDetail'))
              ->joinLeft(array("d"=>"tbl_semestermaster"),'a.IdSemesterMain = d.IdSemesterMaster', array('d.SemesterMainCode as SemesterCodeMain'))
              ->join(array("c"=>"tbl_activity"),'a.IdActivity = c.idActivity')
              ->where('a.id = '.$id);
    $result = $db->fetchAll($select);
    return $result;
  }
  
    /*
    * GET activity for add and drop subject
    */ 
    public function getaddDrop($id,$programId = 0) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from('tbl_activity_calender')
                ->where('IdSemesterMain = ?', (int)$id);
               // ->where('IdActivity = ?', (int)$this->addDropId);
        if($programId != 0)
        {
            $select->where('IdProgram = ?', (int)$programId);
        }
        else
        {
            $select->where('IdProgram IS NULL');
        }
        //echo $select;
        $result = $db->fetchAll($select);

        return $result;

    }
  
    public function fnainsertCalenderActivity($formData){
        
        $newCalender = new Zend_Db_Table('tbl_activity_calender');
        $data = array(
        'IdSemesterMain'=> $formData['IdSemesterMaster'],
        'IdActivity' => $formData['IdActivity'],
        'type' => 1,
        'StartDate' => $formData['StartDate'],
        'EndDate' => $formData['EndDate']
        );
        
        if(isset($formData['IdProgram']))
        {
            $data['IdProgram'] = $formData['IdProgram'];
        }
        
        $newCalender->insert($data);
    }
    
    public function fngetActivityActive(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('a' => $this->_name),array("key" => "a.idActivity" , "value" => "IFNULL(a.ActivityName,'')"))
            ->where('a.status = 1');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
   
    
    public function getEventList($start_date=null,$end_date=null,$activity_type =null,$order=null,$limit=null, $scheme = null, $semester = null)
	{    		
        $auth = Zend_Auth::getInstance();
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

    	$lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'),array("*","DATEDIFF(ac.EndDate, ac.StartDate) as duration"))
            ->join(array('a' => 'tbl_activity'),'a.idActivity=ac.IdActivity',array('ActivityName','ActivityColorCode'))
			->join(array("d"=>"tbl_semestermaster"),'ac.IdSemesterMain = d.IdSemesterMaster', array('d.SemesterMainCode as SemesterCodeMain'));
		
		if ( $start_date != '' && $end_date != '' )
		{
			$lstrSelect->where("ac.StartDate >= ?", $start_date)
				->where("ac.EndDate <= ?", $end_date);
		}
		else if ( $start_date != '' && $end_date == '' )
		{
			$lstrSelect->where("ac.StartDate >= ?", $start_date);
		}
		else if ( $start_date == '' && $end_date != '' )
		{
			$lstrSelect->where("ac.EndDate <= ?", $end_date);
		}

        if ($semester != null && $semester !=''){
            $lstrSelect->where("ac.IdSemesterMain = ?", $semester);
        }

		
        if(isset($activity_type) && $activity_type!=0){
        	$lstrSelect->where('a.ActivityType = ?',$activity_type);
        }

		if ( $order != '')
		{
			$lstrSelect->order($order);
		}

		if ( $limit != null )
		{
			$lstrSelect->limit($limit);
		}

		if ( $scheme != null )
		{
			$lstrSelect->where('d.IdScheme = ?', $scheme);
		}
		
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
       
        return $larrResult;
    }
    
 	public function checkExistActivity($idActivity){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'))
            ->where('ac.IdActivity = ?',$idActivity);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

	public function ActivitiyPeriod($semester_id,$activity)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'),array('DATE(StartDate) as StartDate', 'DATE(EndDate) as EndDate'))
            ->where('ac.IdActivity = ?',$activity)
			->where('ac.IdSemesterMain = ?',$semester_id);

        $result = $lobjDbAdpt->fetchRow($lstrSelect);
		
		if ( empty($result) )
		{
			return false;
		}
		else
		{
			if ( ( strtotime(date('Y-m-d')) >= strtotime($result['StartDate']) ) && ( strtotime(date('Y-m-d')) <= strtotime($result['EndDate']) ) )
			{			
				return true;
			}
		}
        
		return false;
	}

	public function canAddDrop($semester_id,$activity)
	{
		//$registration_act = 46;
		//$addrop = 40;

		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'),array('DATE(EndDate) as EndDate'))
            ->where('ac.IdActivity = ?',$activity)
			->where('ac.IdSemesterMain = ?',$semester_id);

        $result = $lobjDbAdpt->fetchRow($lstrSelect);
		
		if ( empty($result) )
		{
			return false;
		}
		else
		{
			if ( strtotime(date('Y-m-d')) > strtotime($result['EndDate']) )
			{
				return false;
			}
		}
        
		return true;
	}
	
 	public function getCurrencyActivityPaymentReminder($idSemestet){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_activity_calender'))
            ->join(array('b'=>'comm_template_condition'),'b.semester_id = a.IdSemesterMain AND b.activity_id = a.IdActivity',array('*'))
            ->where('b.semester_id = ?',$idSemestet)
            ->where('b.publish = 1')
            ->where('DATE(a.StartDate) <= CURDATE()')
            ->where('DATE(a.EndDate) >= CURDATE()');
        $result = $db->fetchRow($select);
        return $result;
    }
    
	public function isRegistrationOpen($semester_id)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		//Early Registration
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'),array('DATE(StartDate) as StartDate', 'DATE(EndDate) as EndDate'))
            ->where('ac.IdActivity = ?',2)
			->where('ac.IdSemesterMain = ?',$semester_id);
			
        $result = $lobjDbAdpt->fetchRow($lstrSelect);
        
        //Registration
        $lstrSelect2 = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'),array('DATE(StartDate) as StartDate', 'DATE(EndDate) as EndDate'))
            ->where('ac.IdActivity = ?',46)
			->where('ac.IdSemesterMain = ?',$semester_id);
			
        $result2 = $lobjDbAdpt->fetchRow($lstrSelect2);
        
		
		if ( empty($result) || empty($result2))
		{
			return false;
		}
		else
		{
			if ( ( strtotime(date('Y-m-d')) >= strtotime($result['StartDate']) ) && ( strtotime(date('Y-m-d')) <= strtotime($result2['EndDate']) ) )
			{			
				return true;
			}
		}
        
		return false;
	}
    
    public function getStudentSubjectHaveClass($id, $semester = null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
           ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
           ->where('a.IdStudentRegistration = ?', $id)
           //->where('a.IdCourseTaggingGroup = ?', 1893);
           ->where('a.IdCourseTaggingGroup <> ?', 0)
           ->where('a.Active <> ?', 3);

        if ($semester != null && $semester != ''){
            $select->where('a.IdSemesterMain = ?', $semester);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getItem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->where('a.regsub_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
	
    public function classList($id, $type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.idGroup = ?', $id)
            ->where('a.idClassType IN (?)', $type);

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function classInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->where('a.IdCourseTaggingGroup = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getAttendanceDate($groupId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ctd_status=b.idDefinition')
            ->where('a.ctd_groupid = ?', $groupId)
            ->group('a.ctd_scdate');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSchedule3($groupid, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_schedule_details'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.tsd_lecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.tsd_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.tsd_groupid = ?', $groupid)
            ->where('a.tsd_scdate = ?', $date);
        //echo $select.'<br/>'; //exit;
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getActivityDrop($semester){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_activity_calender'), array('value'=>'*'))
            ->where('a.IdActivity IN (40, 50)')
            ->where('a.IdSemesterMain = ?', $semester)
            ->where('DATE(a.StartDate) <= CURDATE()')
            ->where('DATE(a.EndDate) >= CURDATE()');

        $result = $db->fetchRow($select);
        return $result;
    }
}