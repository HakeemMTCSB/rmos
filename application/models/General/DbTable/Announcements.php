<?php 
class App_Model_General_DbTable_Announcements extends Zend_Db_Table_Abstract
{
    protected $_name = 'announcement';
	protected $_file = 'announcement_files';
	protected $_primary = "id";

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	public function getStudentAnnouncements($program_id=0, $semester_id=0,$type='sms',$student_id=0, $paginate=0)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('in'=>'tbl_intake'),'in.IdIntake=a.intake_id', array('in.IntakeDesc as intake_name'))
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=a.program_id', array('p.ProgramName as program_name'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
					->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as created_name')
					->order('a.sticky DESC')
					->order('a.id DESC')
					->where('FIND_IN_SET (?, a.type)', $type);

		if ($program_id!=0)
		{
			$data = array(0,$program_id);
			$select->where('a.program_id IN(?) OR a.program_id IS NULL', $data);
		}
		
		if ($semester_id!=0)
		{
			$data = array(0,$semester_id);
			$select->where('a.semester_id IN(?) OR a.semester_id IS NULL', $data);
		}
		
		
		if ( $student_id )
		{
			$select	->where('a.id NOT IN ( SELECT announcement_id FROM `announcement_read` WHERE user_id = ? AND announcement_type=\'sms\')', $student_id)
					->where('a.sticky = 1');
		}
		
		$select->where('a.active = 1');

		if ( $paginate == 0 )
		{
			$row = $db->fetchAll($select);
		}
		else
		{
			$row = $select;
		}
		
		
		return $row;
	}
	
	public function getData($id=0,$paginate=0, $where=null)
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('in'=>'tbl_intake'),'in.IdIntake=a.intake_id', array('in.IntakeDesc as intake_name'))
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=a.program_id', array('p.ProgramName as program_name'))
					->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=a.semester_id', array('concat(s.SemesterMainName, " - ", s.SemesterMainCode) as semester_name'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
					->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as created_name')
					->order('a.id DESC');

		if  ( isset($where['title']) && $where['title'] != '' )
		{
			$select->where('a.title  LIKE ?', '%'.$where['title'].'%');
		}

		if  ( isset($where['intake_id']) && $where['intake_id'] != '' )
		{
			$select->where('a.intake_id = ?', $where['intake_id']);
		}

		if  ( isset($where['program_id']) && $where['program_id'] != '' )
		{
			$select->where('a.program_id = ?', $where['program_id']);
		}

		if  ( isset($where['semester_id']) && $where['semester_id'] != '' )
		{
			$select->where('a.semester_id = ?', $where['semester_id']);
		}

		if ( isset($where['types']) && !empty($where['types']) )
		{
			$wheretype = array();
			
			if ( count($where['types']) == 1 )
			{
				$select->where( 'a.type = ?', $where['types'][0] );
			}
			else
			{
				foreach ( $where['types'] as $type )
				{
					$wheretype[] = $db->quoteInto('FIND_IN_SET (?, a.type)', $type);
				}
				
				$select->where( new Zend_Db_Expr( implode(' OR ', $wheretype) ) );
			}
		}
		
		$id = (int)$id;

		if ($id!=0)
		{
			$select->where('a.'.$this->_primary.' = ?',$id);
		}

		if ( $paginate == 0 )
		{
			$row = $id > 0 ? $db->fetchRow($select) : $db->fetchAll($select);
		}
		else
		{
			$row = $select;
		}
		
		return $row;
	}
	
	public function add($data)
	{
		$db = $this->db;
		
		$db->insert($this->_name, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}

	public function updateAnnouncement($data, $id)
	{
		$db = $this->db;
		$db->update($this->_name, $data, 'id='.(int)$id);
	}
	
	public static function showTypes($types='')
	{
		$types = explode(',', $types);
		$new = array();

		$realtype = array(
							'sms'			=> 'SMS',
							'onapp'			=>	'Online Application',
							'studentportal'	=> 'Student Portal',
							'thesis'		=> 'Thesis Portal'
						);

		foreach ( $types as $type )
		{
			
			$new[] = $realtype[$type];
		}

		if ( count($new) == 4 )
		{
			return 'All';
		}

		return implode(', ', $new);
	}

	/*
	*	Files
	*/
	public function addFile($data)
	{
		$db = $this->db;
		
		$db->insert($this->_file, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}
	
	public function getFile($id)
	{
		$db = $this->db;
		$select = $db->select()
								->from(array("a"=>$this->_file))
								->where('a.id = ?',$id);

		$result = $db->fetchRow($select);	
		return $result;
	}

	public function getFiles($pid)
	{
		$db = $this->db;
		$select = $db->select()
								->from(array("a"=>$this->_file))
								->where('a.announcement_id = ?',$pid);

		$result = $db->fetchAll($select);	
		return $result;
	}

	public function deleteFile($id)
	{
		$db = $this->db;

		$db->delete($this->_file, 'id='.(int)$id);
	}

        static function getProgName($id){
            $db = Zend_Db_Table::getDefaultAdapter();;
            $select = $db->select()
                ->from(array('a'=>'tbl_program'), array('a.ProgramName'))
                ->where('a.IdProgram = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getIntakeName($id){
            $db = Zend_Db_Table::getDefaultAdapter();;
            $select = $db->select()
                ->from(array('a'=>'tbl_intake'), array('a.IntakeDesc'))
                ->where('a.IdIntake = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getSemName($id){
            $db = Zend_Db_Table::getDefaultAdapter();;
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('concat(a.SemesterMainName, " - ", a.SemesterMainCode)'))
                ->where('a.IdSemesterMaster = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
}
?>