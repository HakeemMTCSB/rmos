<?php 
class App_Model_General_DbTable_Utility extends Zend_Db_Table_Abstract
{
	public function autocompleteStudent($term=null, $searchby=1, $limit=10)
	{
		$db = getDB();

		$select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.IdStudentRegistration as key','sa.registrationId as value'))     
		->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = sa.profileStatus')  		
		->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT(CONCAT_WS(" ",appl_fname,appl_mname,appl_lname)," - ",registrationId," - ",df.DefinitionDesc) as label', 'CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as sname'))
		->joinLeft(array('a'=>'tbl_program'), 'sa.IdProgram = a.IdProgram', array('programname'=>'a.ProgramName'))
		->joinLeft(array('b'=>'tbl_program_scheme'), 'sa.IdProgramScheme = b.IdProgramScheme')
		->joinLeft(array('h'=>'tbl_definationms'), 'b.mode_of_program = h.idDefinition', array('mop'=>'h.DefinitionDesc'))
		->joinLeft(array('i'=>'tbl_definationms'), 'b.mode_of_study = i.idDefinition', array('mos'=>'i.DefinitionDesc'))
		->joinLeft(array('j'=>'tbl_definationms'), 'b.program_type = j.idDefinition', array('pt'=>'j.DefinitionDesc'));
//						  ->where('sa.ProfileStatus = 92'); //active only

		if ( $term != '' )
		{
			//1 = name
			if ( $searchby == 1 )
			{
				$select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?','%'.$term.'%');
			}
			else
			{
				$select->where('sa.registrationId LIKE ?','%'.$term.'%');
			}
		}

		$select->limit($limit);

		$results = $db->fetchAll($select);
		return $results;
	}

	public function autocompleteApplicant($term=null, $searchby=1, $limit=10,$withid=1)
	{
		$db = getDB();
		
		if ( $withid )
		{
			$select = $db->select()->from(array('at' => 'applicant_transaction'), array('at.at_trans_id as key','at.at_pes_id as value')) 
			->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = at.at_status')       		
						  ->join(array('ap'=>'applicant_profile'),'at.at_appl_id=ap.appl_id',array('CONCAT(CONCAT_WS(" ",ap.appl_fname,ap.appl_mname,ap.appl_lname)," - ",at.at_pes_id," - ",DefinitionDesc) as label'));
		}
		else
		{
			$select = $db->select()->from(array('at' => 'applicant_transaction'), array('at.at_trans_id as key','at.at_pes_id as value'))   
			->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = at.at_status')     		
						  ->join(array('ap'=>'applicant_profile'),'at.at_appl_id=ap.appl_id',array('CONCAT(at.at_pes_id," - ",DefinitionDesc) as label'));
		}
		

		if ( $term != '' )
		{
			//1 = name
			if ( $searchby == 1 )
			{
				$select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?','%'.$term.'%');
			}
			else
			{
				$select->where('at.at_pes_id LIKE ?','%'.$term.'%');
			}
		}
		
		$select->order('at.at_trans_id DESC');
		$select->limit($limit);

		$results = $db->fetchAll($select);
		return $results;
	}	
	
	public function autocompleteReceipt($term=null, $limit=10)
	{
		$db = getDB();

		$select = $db->select()->from(array('a' => 'receipt'), array('a.rcp_id as key','a.rcp_no as value'))
						  ->join(array('ap'=>'applicant_profile'),'a.rcp_appl_id=ap.appl_id',array('CONCAT(CONCAT_WS(" ",a.rcp_no," - ",ap.appl_fname,ap.appl_mname,ap.appl_lname)) as label'));

		$select->where('a.rcp_no LIKE ?','%'.$term.'%');
		
		$select->limit($limit);
		
		$results = $db->fetchAll($select);
		return $results;
	}

}

?>