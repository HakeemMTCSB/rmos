<?php 
class App_Model_General_DbTable_Qualificationmaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_qualificationmaster';
	protected $_primary = "IdQualification";
	protected $_subname = 'tbl_qualificationsub';
	protected $_subprimary = "id";
	
	public function getDatabyId ($id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('q'=>$this->_name))					
					  ->where($this->_primary." = ?",$id);					  
		
		$row = $db->fetchRow($select);		
		return $row;
	}	
	
	public function getDataTest(){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('q'=>$this->_name))					
					  ->where("qualification_type_id = 608");					  
		
		$row = $db->fetchAll($select);		
		return $row;
	}	
	
	public function getSubDatabyId ($id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('qs'=>$this->_subname))					
					  ->where($this->_subprimary." = ?",$id);					  
		
		$row = $db->fetchRow($select);		
		return $row;
	}

	public function getCerts() {
		$select = $this->select()
			->where('qualification_type_id = ?', 607)
			->order('QualificationRank ASC')
		;

		$qualifications = $this->fetchAll($select);
		return($qualifications);

	}
	
}