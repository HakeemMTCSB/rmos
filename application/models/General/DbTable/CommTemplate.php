<?php 
class App_Model_General_DbTable_CommTemplate extends Zend_Db_Table_Abstract
{
    protected $_name = 'comm_template';
	protected $_primary = "tpl_id";
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('s'=>$this->_name))
	                 ->where('s.'.$this->_primary.' = ' .$id);
	                 
					                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
			if(!$row){
				throw new Exception("There is No Data");
			}
        
		}else{
			$select = $db->select()
	                 ->from(array('s'=>$this->_name));                 
			
	        $row = $db->fetchAll($select);
	        
		}
		
		return $row;
	}

	public function retrieve_template($tpl_pname) {
		//TODO: get locale
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
                    ->from(array('CommTemplate' => $this->_name))
                    ->joinLeft( array('TemplateContent' => 'comm_template_content'),'TemplateContent.tpl_id = CommTemplate.tpl_id', array('TemplateContent.*') )
                    ->where('TemplateContent.locale = ?', 'en_US' )
                    ->where('CommTemplate.tpl_pname = ?', $tpl_pname);
		$template = $db->fetchRow($select);
		return($template);
	}
        
        public function retrieve_template_v2($programSchemeId, $sdtCtgy){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a' => 'tbl_application_initialconfig'))
                ->join(array('b' => 'tbl_application_item_tagging'),'a.id = b.idTagSection')
                ->where('a.programID = ?',$programSchemeId)
                ->where('a.sectionID = ?',17)
                ->where('a.sdtCtgy = ?',$sdtCtgy)
                ->where('b.idItem = ?', 140);
            $template = $db->fetchRow($select);
            return($template);
        }
        
	public function getDocumentDownload($tpl_id,$locale='en_US'){
				
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
	                ->from(array('ct'=>$this->_name))
	                ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id = ct.tpl_id')	              
	                ->where('ct.tpl_id = ?',$tpl_id)
	                ->where('ctc.locale =?',$locale);
	                
	    $row = $db->fetchRow($select);
		return $row;
	}
	
	
	
}
?>