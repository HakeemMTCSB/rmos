<?php 
class App_Model_General_DbTable_Scholarship extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_scholarship_sch';
	protected $_primary = "sch_Id";
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                 ->from(array('s'=>$this->_name))
	                 ->where('s.'.$this->_primary.' = ' .$id);
	                 
					                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
			if(!$row){
				throw new Exception("There is No Data");
			}
        
		}else{
			$select = $db->select()
	                 ->from(array('s'=>$this->_name));                 
			
	        $row = $db->fetchAll($select);
	        
		}
		
		return $row;
	}
	
	
	public function getScholarship($program_scheme_id='',$intake='',$appl_cat){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		/*
		 * SELECT sch.*,sct.* 
FROM `tbl_scholarship_sch` as sch
JOIN tbl_scholarshiptagging_sct as sct ON sct.sct_schId = sch.sch_Id
WHERE 	sct_intake = ''
AND sct_studentcategor = ''
AND 	sct_programscheme = '';

		 */
		
	    $select = $db->select()
	                ->from(array('sch'=>$this->_name))
	                ->join(array('sct'=>'tbl_scholarshiptagging_sct'),'sct.sct_schId = sch.sch_Id')
	                ->where('sct.sct_programscheme = ?',$program_scheme_id)
	                ->where('sct.sct_studentcategory = ?',$appl_cat)
	                ->where('sct.sct_intake = ?',$intake);
	                
	    $row = $db->fetchAll($select);    
		
		return $row;
	}
	
	
	
}
?>