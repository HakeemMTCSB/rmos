<?php 
class App_Model_General_DbTable_EmailTemplate extends Zend_Db_Table_Abstract
{
  
    protected $_name = 'tbl_statustemplate_stp';
	protected $_primary = "stp_Id";
	protected $_subname = 'tbl_status_sts';
	protected $_subprimary = "sts_Id";
	
	
			
	public function getEmailTemplate($status,$scheme,$appl_cat){ //status =>DRAFT
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('stp'=>$this->_name))
	                ->join(array('sts'=>$this->_subname),'sts.`sts_emailNotification`=stp.`stp_Id`')
	                ->where('stp.stp_tplType = 604')
	                ->where('sts.sts_name = ?',$status)
	                ->where('sts.sts_programScheme = ?',$scheme)
	                ->where('sts.sts_stdCtgy  = ?',$appl_cat);
	   
	    $row = $db->fetchRow($select);	
		return $row;           
	}
	
	public function getMessageTemplate($status,$scheme,$appl_cat){ //status =>DRAFT
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('stp'=>$this->_name))
	                ->join(array('sts'=>$this->_subname),'sts.`sts_message`=stp.`stp_Id`')
	                ->where('stp.stp_tplType = 603')
	                ->where('sts.sts_name = ?',$status)
	                ->where('sts.sts_programScheme = ?',$scheme)
	                ->where('sts.sts_stdCtgy  = ?',$appl_cat);
	   
	    $row = $db->fetchRow($select);	
		return $row;           
	}
	
	public function checkExistMessageAcceptance($scheme,$appl_cat){ 
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()	            
	                ->from(array('sts'=>$this->_subname))	            
	                ->where('(sts.sts_name = 599 OR sts.sts_name = 600)') //accept or decline
	                ->where('sts.sts_programScheme = ?',$scheme)
	                ->where('sts.sts_stdCtgy  = ?',$appl_cat);
	   
	    $row = $db->fetchRow($select);	
	    
	    if($row)
			return $row;  
		else
			return null;    
	}
	
	
	public function addData($data){
		
		$this->insert($data);
	}
	
	public function updateData($data,$id){
		
		$this->update($data, $this->_primary . ' = ' . (int)$id);
	}
	
	public function deleteData($id){
		$this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getEmailTemplatebyProgram($status,$program){ //status =>DRAFT
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('stp'=>$this->_name))
	                ->join(array('sts'=>$this->_subname),'sts.`sts_emailNotification`=stp.`stp_Id`')
	                ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=sts.sts_programScheme',array())
	                ->where('stp.stp_tplType = 604')
	                ->where('sts.sts_name = ?',$status)
	                ->where('ps.IdProgram  = ?',$program);
	   
	    $row = $db->fetchRow($select);	
		return $row;           
	}
	
	public function getEmailAttachment($stp_id,$locale){
		
		/*
		 *  SELECT sth.*,ct.*
			FROM tbl_statusattachment_sth as sth
			JOIN comm_template as ct ON ct.tpl_id = sth.sth_tplId 
			JOIN comm_template_content as ctc ON ctc.tpl_id=ct.tpl_id
			WHERE sth.sth_stpId = 1
			AND ctc.locale = ''
		 */
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('sth'=>'tbl_statusattachment_sth'))
	                ->join(array('ct'=>'comm_template'),'ct.tpl_id = sth.sth_tplId')
	                ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id=ct.tpl_id')	
                        ->joinLeft(array("b"=>"tbl_definationms"), "sth.sth_type = b.idDefinition", array('typeName'=>'DefinitionDesc'))
	                ->where('sth.sth_stpId  = ?',$stp_id)
	                ->where('ctc.locale = ?',$locale);
	   
	    $row = $db->fetchAll($select);	
		return $row;      
	}
	
	
}
?>