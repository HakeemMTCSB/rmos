<?php 
class App_Model_General_DbTable_Branch extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_branchofficevenue';
	protected $_primary = "IdBranch";
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = (int)$id;
		
		if($id!=0){

	        $select = $db->select()
	                ->from(array('b'=>$this->_name))
	                ->where('b.'.$this->_primary.' = ' .$id)
	                ->where('b.Active =?', 1)
					;
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
			if(!$row){
				throw new Exception("There is No Data");
			}
        
		}else{
			$select = $db->select()
	                ->from(array('b'=>$this->_name))
	                ->where('b.Active =?', 1)
					;
			                     
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	        
	        if(!$row){
	        	$row =  $row->toArray();
	        }
		}
		
		
		
		return $row;
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('b'=>$this->_name))
					;
		
		return $select;
	}
	
	public function addData($data){
		$data = friendly_columns($this->_name, $data);
			
		$this->insert($data);
	}
	
	public function updateData($data,$id){
	
		$data = friendly_columns($this->_name, $data);
		$this->update($data, $this->_primary . ' = ' . (int)$id);
	}
	
	public function deleteData($id){
		$this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getBranchArray(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectCountry = $db ->select()
							->from(array('b'=>$this->_name));
							
		$stmt = $db->query($selectCountry);
        $row = $stmt->fetchAll();
	    
		$officeDB = new App_Model_General_DbTable_Office();
		
        $i=0;
        foreach ($row as $branch){
	    	$row[$i]['office'] = $officeDB->getOfficeFromBranch($branch['id']);
	    	$i++;	    	
	    }
	    
        return $row;
	}
	
	
	public function getBranchNoProgram($program_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selbranch  = $db->select()
				         ->from(array('bp'=>'r040_program_branch'),array('branch_id'=>'bp.branch_id'))
				         ->where('bp.program_id ='.$program_id);
				         //->where('bp.branch_id ='.$branch_id);
		
		$select = $db->select()
				->from(array('b'=>$this->_name))
				->where("b.id NOT IN (?)",$selbranch);
				
	    $row = $db->fetchAll($select);
	    
	    return $row;
				
	}
}
?>