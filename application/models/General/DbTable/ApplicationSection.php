<?php 


class App_Model_General_DbTable_ApplicationSection extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_application_section';
	protected $_primary = "id";
        
        public function getSection($programID=0,$scheme,$student_cat){	
			
		    $db = Zend_Db_Table::getDefaultAdapter();
		    
		    $sql = $db->select()
		              ->from(array("a" => "tbl_application_initialconfig")) 
		              ->join(array('s'=>'tbl_application_section'),'s.id = a.sectionID',array('main_section_id'=>'id','name','instruction','status'))     
		              ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = a.programID')   
		              ->where("ps.IdProgram  = ?", $programID)      
		              ->where("a.programID = ?", $scheme) //scheme sebenarnya
		              ->where("a.sdtCtgy  = ?", $student_cat)  
		              ->order("a.seq_order");
	      
		   return $sidebarList = $db->fetchAll($sql);
		    
	}
	
	public function getData($sectionID){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("id = ?", $sectionID) ;

		$row = $db->fetchRow($select);	
		 
		return $row;
	}
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getSectionList(){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name);

		$row = $db->fetchAll($select);	
		 
		return $row;
	}
	
	public function migrateSectionData($student_id,$txn_id,$section,$stack_failed){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		
		$select = $db ->select()
					  ->from($section['table_name'])
					  ->where($section['table_id_column'].'='.$txn_id);

		$row_data = $db->fetchAll($select);	

		if(count($row_data)>0){			
			   	$migrate = array();		
				foreach($row_data as $row){	
					try{
						$row[$section['table_primary_id']]='';
						$row['sp_id']=$student_id;							
						$db->insert($section['student_table_name'],$row);
						
					}catch (Exception $em){						
						array_push($migrate,$message='Fail migrating data to table '.$section['student_table_name'].' '.$em->getMessage());
					}
				}
				array_push($stack_failed,$migrate);
		}
		
		 
	}
	
	function migrateDocuments($txn_id,$student_id,$at_repository,$sp_repository){

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		
		//get documents		
		$select = $db ->select()
					  ->from('applicant_documents')
					  ->where('ad_txn_id = ?',$txn_id);
		$documents = $db->fetchAll($select);
						
		if(count($documents)>0){ 
			
    		foreach($documents as $doc){  
    			
    			//get document type id
    			$select_checklist = $db ->select()
										->from(array('dcl'=>'tbl_documentchecklist_dcl'))
										->where('dcl.dcl_Id = ?',$doc['ad_dcl_id']);
				$dcl_info = $db->fetchRow($select_checklist);
    			
				if(!$dcl_info['dcl_uplType']){
					$dcl_info['dcl_uplType']=0;
				}
    			$file = array(
    			        'sp_id'=>$student_id,						
						'ad_dcl_id' => $doc['ad_dcl_id'],
    			        'ad_ads_id' => $doc['ad_ads_id'],
						'ad_section_id' => $doc['ad_section_id'],
						'ad_table_name' => $doc['ad_table_name'],
					    'ad_table_id'  => $doc['ad_table_id'],
						'ad_filename'  => $doc['ad_filename'], 		
    				    'ad_ori_filename'  => $doc['ad_ori_filename'], 				
    				    'ad_filepath'  => $sp_repository.'/'.$doc['ad_filename'], 
						'ad_createddt' => date("Y-m-d h:i:s"),
						'ad_createdby' => $auth->getIdentity()->iduser	,
    					'ad_type' => $dcl_info['dcl_uplType']				
				);
				  						
				$db->insert('student_documents',$file);	
    		}//end foreach doc
    		    		
    		//copy physical file
			try {
				$this->copy_file(DOCUMENT_PATH.$at_repository,DOCUMENT_PATH.$sp_repository,$txn_id);
			}catch (Exception $e) {
	    				//save error message
	    				$sysErDb = new App_Model_General_DbTable_SystemError();
	    				$msg['se_txn_id'] = $txn_id;
	    				$msg['se_title'] = 'Error copy documents';
	    				$msg['se_message'] = $e->getMessage();
	    				$msg['se_createdby'] = $auth->getIdentity()->iduser;
	    				$msg['se_createddt'] = date("Y-m-d H:i:s");
	    				$sysErDb->addData($msg);
	    	}
    	
    		
    
    	}	//end if count doc	
	}
	
	
	function copy_file($src,$dst){
						    	
		if (file_exists($src)) {
			if ($handle = opendir($src) ) {
				
			     //echo "Directory handle: $handle\n";
			     //echo "Entries:\n";
			    
			    //create destination dir		     
			    //@mkdir($dst);
			
			    /* This is the correct way to loop over the directory. */
			    while (false !== ($file = readdir($handle))) {
			        //echo "$file\n";
			        
			        if (( $file != '.' ) && ( $file != '..' )) { 
			            if ( is_dir($src . '/' . $file) ) { 
			               // recurse_copy($src . '/' . $file,$dst . '/' . $file); 
			            } 
			            else { 
			            	// echo "copy \n";
			                copy($src . '/' . $file,$dst . '/' . $file); 
			            } 
			        } 
	        
			    }			
			    		
			    closedir($handle);
			}else{
				throw new Exception('Cannot open folder directory. Directory folder does not exist!');
			}
		} else{
				throw new Exception('Directory folder does not exist!');
		}
			
	
	}
	
	
}
?>