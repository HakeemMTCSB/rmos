<?php 
class App_Model_General_DbTable_SystemError extends Zend_Db_Table_Abstract
{
    protected $_name = 'system_error';
	protected $_primary = "se_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"ad_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getDatabyId ($id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('q'=>$this->_name))					
					  ->where($this->_primary." = ?",$id);					  
		
		$row = $db->fetchRow($select);		
		return $row;
	}	
}

?>