<?php 
class App_Model_General_DbTable_Definationms extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_definationms';
	protected $_primary = "idDefinition";
	
	public function getData($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getDataByType($idType=0){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where("d.idDefType = '".$idType."'")
					->where("d.Status = ?", 1)
					->order('d.defOrder ASC');
						                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getDataByTypeDocumentType( $idScheme, $studentcategory){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                 ->joinLeft(array('a' => 'tbl_documentchecklist_dcl'), 'a.dcl_uplType = d.idDefinition', array(''))
	                ->where("d.idDefType = 10")
					->where("d.Status = ?", 1)
					->where("a.dcl_finance = ?", 1)
					->where("a.dcl_programScheme = ?",$idScheme)
					->where("a.dcl_stdCtgy = ?", $studentcategory)
					->order('d.defOrder ASC');
						                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}

    public function getByCode($definition) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('Definitions'=>$this->_name) )
            ->joinLeft(array('DefinitionType' => 'tbl_definationtypems'), 'DefinitionType.idDefType = Definitions.idDefType', array('BahasaIndonesiaType' => 'DefinitionType.BahasaIndonesia'))
            ->where("DefinitionType.defTypeDesc = '".$definition."'")
			->order('Definitions.defOrder ASC');

        $row = $db->fetchAll($select);
        return $row;
    }

	/*
	* getIdByDefType('DefCode','DefTypeName');
	*/
	public function getIdByDefType($defTypeDesc, $DefinitionCode){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()
						->from(array('a' => 'tbl_definationtypems'),array(''))
						->join(array('b' => 'tbl_definationms'), " a.idDefType = b.idDefType "  ,array('key' => 'b.idDefinition','value' => 'b.DefinitionCode'))
						->where("a.defTypeDesc= ?",$defTypeDesc)
						->where("b.DefinitionCode= ?",$DefinitionCode)
						->order('b.defOrder ASC');
		$larrResult = $db->fetchRow($lstrSelect);
		
		return $larrResult;
	}

    public function getByCodeAsList($definition) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $db->select()
            ->from(array('Definitions'=>$this->_name),null, array())
            ->joinLeft(array('DefinitionType' => 'tbl_definationtypems'), 'DefinitionType.idDefType = Definitions.idDefType',array("key"=>"Definitions.idDefinition","value"=>"Definitions.DefinitionDesc") )
            ->where("DefinitionType.defTypeDesc = ?",$definition)
            ->where("Definitions.Status = ?", 1)
			->order('Definitions.defOrder ASC');
        $result = $db->fetchAll($lstrSelect);
        return $result;

    }
    
	public function getDataByDefCode($type,$code){
			
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where('d.DefinitionCode = ?',$code)
	                ->where('d.idDefType = ?',$type);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}

    public function getDataByTypeCode($code=0){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('b'=>'tbl_definationms') )
            ->join(array('a' => 'tbl_definationtypems'), " a.idDefType = b.idDefType "  ,array('key' => 'b.idDefinition','value' => 'b.DefinitionCode'))
            ->where("a.defTypeDesc = ?",$code);

        $row = $db->fetchAll($select);

        return $row;

    }
	
	
	
}
?>