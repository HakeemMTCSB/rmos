<?php

class App_Model_Survey_DbTable_QuestionSurvey extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'q003_question';
	protected $_primary = "id";
	
	
	public function getPool($id=0){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q001_pool'),array("poolname"=>"a.name","poolid"=>"a.id"));
//				->joinLeft(array('c'=>'q006_answersetstudent'),'c.pool_id = a.id',array('idSetAnswer'=>'c.id','statusSurvey'=>'c.status','*'))
//				->group('a.id');
					
		if($id!=0){
			$select->joinLeft(array('b'=>'q002_chapter'),'a.id = b.pool_id',array('chaptername'=>'b.name','chapterid'=>'b.id','question_type'=>'b.question_type'));
			$select->where('a.ID = '.$id);
		}
		
		$row = $db->fetchAll($select);
		return $row;
	}
	
	public function getPoolBySem($id=0,$sem=0){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q001_pool'),array("poolname"=>"a.name","poolid"=>"a.id","idPool"=>"a.id"))
				->joinLeft(array('b'=>'q002_chapter'),'b.pool_id = a.id',array('chaptername'=>'b.name','chapterid'=>'b.id','question_type'=>'b.question_type', 'subsection_instruction' => 'b.subsection_instruction'))
				->joinLeft(array('c'=>'q020_sectiontagging'),'c.id = b.main_set_id',array('*'))
				->where('a.id = '.$id)
				->where('c.semester_id = ?',$sem);
		
		$row = $db->fetchAll($select);
		return $row;
	}
	
public function getPoolBySet( $id=0, $poolid=0 ) {

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
				->from(array('a'=>'q020_sectiontagging'))
//				->join(array('c'=>'q001_pool'),'c.id = a.question_pool',array())
				->join(array('b'=>'q002_chapter'),'b.pool_id = a.question_pool and b.main_set_id = a.id',array('chaptername'=>'b.name','chapterid'=>'GROUP_CONCAT( DISTINCT(b.id))','question_type'=>'b.question_type', 'subsection_instruction' => 'b.subsection_instruction','registration_item_id','order_chp'=>'order'))
				->where("a.id IN ($id)")
//				->where('b.pool_id = ?',$poolid)
				->where('b.status = 1')
				->group('b.id')
				/*
				 * alter on 23-07-2015
				 * ->group('b.registration_item_id')
				->group('a.question_pool')
				->group('b.question_type')*/
				
				
				->order('b.pool_id asc')
				->order('b.order asc')
				->order('b.question_type desc')
//				->order('a.id asc')
				
				;
		
		$row = $db->fetchAll( $select );
		return $row;
	}
	
	
public function getPoolBySetDistinct( $id=0, $poolid=0 ) {

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
				->from(array('a'=>'q020_sectiontagging'))
//				->join(array('c'=>'q001_pool'),'c.id = a.question_pool',array())
				->join(array('b'=>'q002_chapter'),'b.pool_id = a.question_pool and b.main_set_id = a.id',array('chaptername'=>'b.name','chapterid'=>'GROUP_CONCAT( DISTINCT(b.id))','question_type'=>'b.question_type', 'subsection_instruction' => 'b.subsection_instruction','registration_item_id','order_chp'=>'order'))
				->where("a.id IN ($id)")
//				->where("a.question_pool IN ($poolid)")
				->where('b.status = 1')
				->where('b.id != 712')
//				->group('b.id')
				->group('a.question_pool')
				->group('b.name')
				
				
				->order('b.pool_id asc')
				->order('b.order asc')
				->order('b.question_type desc')
//				->order('a.id asc')
				
				;
//		echo $select;exit
		$row = $db->fetchAll( $select );
		return $row;
	}
	

	public function getOnePoolBySem($id=0,$sem=0,$main_set_id=0){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('c'=>'q020_sectiontagging'))
				->join(array('a'=>'q001_pool'),'a.id = c.question_pool',array("poolname"=>"a.name","poolid"=>"a.id","idPool"=>"a.id"))
				->joinLeft(array('b'=>'q002_chapter'),'b.pool_id = a.id',array('chaptername'=>'b.name','chapterid'=>'b.id','question_type'=>'b.question_type'))
//				->joinLeft(array('c'=>'q020_sectiontagging'),'c.id = b.main_set_id',array('*'))
				->where('c.question_pool = ?', $id)
				->where('c.semester_id = ?',$sem)
				->where('c.id = ?',$main_set_id);
		
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function getSetPool($id=0){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('c'=>'q020_sectiontagging'))
				->join(array('a'=>'q001_pool'),'a.id = c.question_pool',array("poolname"=>"a.name","poolid"=>"a.id","idPool"=>"a.id"))
				->joinLeft(array('b'=>'q002_chapter'),'b.pool_id = a.id',array('chaptername'=>'b.name','chapterid'=>'b.id','question_type'=>'b.question_type'))
//				->joinLeft(array('c'=>'q020_sectiontagging'),'c.id = b.main_set_id',array('*'))
				->where('c.id = ?',$id);
		
		$row = $db->fetchRow($select);
		return $row;
	}

	public function getStatusSurvey($student_id,$id=0){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q006_answersetstudent'),array("*"))
				->where('a.student_id = '.$student_id)
				->where('a.pool_id = '.$id);
				
		
		
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function getCourseGroup($curSem,$courseid){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'tbl_course_tagging_group'),array("*"))
				->where('a.IdSemester = '.$curSem)
				->where('a.IdSubject = '.$courseid);
				
		
		
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function getQuestion($poolId,$chapterid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('a'=>'q003_question'),array("idQues"=>"a.id","*"))
					->where('a.pool_id = ?',$poolId)
					->where('a.chapter_id = ?',$chapterid)
					->order('a.id asc');
					
		
		$row = $db->fetchAll($select);
//		$arraySet = $row->toArray();
	return $row;
	}
	
public function getQuestionByChapter( $chapterid ) {

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
		->from( array( 'a'=>'q003_questiontagging' ), array( "*", 'idQues'=>'a.IdQuestion' ) )
		->joinLeft( array( 'b'=>'q003_question' ), 'b.id = a.IdQuestion', array( '*' ) )
		->where( 'a.section_id = ?', $chapterid )
		->order( 'a.order asc' );

		$row = $db->fetchAll( $select );
		return $row;

	}
	
//	public function getQuestionByChapter($chapterid,$main_set_id){
//		
//		$db = Zend_Db_Table::getDefaultAdapter();
//		echo $select = $db ->select()
//					->from(array('a'=>'q003_questiontagging'),array("*",'idQues'=>'a.IdQuestion'))
//					->joinLeft(array('b'=>'q003_question'),'b.id = a.IdQuestion',array('*'))
//					->where('a.section_id = ?',$chapterid)
//					->where('a.main_set_id = ?',$main_set_id)
//					->order('a.idQuestion asc');
//		
//		$row = $db->fetchAll($select);
//		return $row;
//		
//	}
	public function getQuestionById($questionID){

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('q'=>$this->_name))
					->where('q.id = ?',$questionID);
		$row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getRubricValue($chapterid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('a'=>'q006_rubric'))
					->where('a.chapter_id = ?',$chapterid)
					;
		$row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getRubricDetails($chapterid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('a'=>'q004_rubricdetails'))
					->where("a.id_Section IN ($chapterid)")
					->where('a.ValueNumber != 0')
					->group('a.ValueNumber');
		$row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getAnalysisDetails($chapterid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('a'=>'q004_rubricdetails'),array('ValueNumber'=>'GROUP_CONCAT(DISTINCT a.ValueNumber)','analysisValue'))
					->where("a.id_Section IN ($chapterid)")
					->where('a.ValueNumber != 0')
					->group('a.analysisValue')
					->order('a.ValueNumber asc');
		$row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getRadioValue($chapterid,$rubricid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('a'=>'q006_rubric'))
					->joinLeft(array('b'=>'q006_rubric_value'),'a.id = b.rubric_id',array('idValue'=>'b.id','*'))
					->where('a.chapter_id = ?',$chapterid)
					->where('b.rubric_id = ?',$rubricid)
					;
		$row = $db->fetchAll($select);
	return $row;
	}	
	
	public function getDateExam($program,$intake,$course){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					->from(array('a'=>'r029_course_offer'))
					->where('a.program_id = ?',$program)
					->where('a.semester_id = ?',$intake)
					->where('a.course_id = ?',$course)
					;
		$row = $db->fetchRow($select);
	return $row;
	}
	
	public function getListofStudentSemSubject($studMatric,$sem,$course){	

		echo $studMatric." + ". $sem." + ". $course;
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			
			 echo $select = $lobjDbAdpt ->select()
						->from(array('a'=>'r015_student'),array("idStud"=>"a.id"))
						->joinLeft(array('b'=>'r024_student_course_registration'),'a.id = b.student_id',array('idCourseRegis'=>'b.id','*'))
						->where("a.matric_no_old LIKE '%$studMatric%'")
						->where('a.admission_intake_id = ?',$sem)
						->where('b.course_id = ?',$course);
			$larrResult = $lobjDbAdpt->fetchRow($select);
			return $larrResult;
			
	}
	
	public function addData($larrformData,$table,$primary=null){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
//		echo "<pre>";
//    	print_r($larrformData);
//    	echo "</pre>";
    	
		$lastinsertid = $lobjDbAdpt->insert($table,$larrformData);
		
		if($primary != null){
			$lastid  = $lobjDbAdpt->lastInsertId($table,$primary);	
			return $lastid;
		}else{
			return $lastinsertid;
		}
		
	}
	
	public function updateDataAnswer($larrformData,$idAnswer){  		
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "q006_answerstudent";
		$lobjDbAdpt->update($table,$larrformData, ' id= '. (int)$idAnswer);
		
		//$lobjDbAdpt->update($table,$larrformData,$where);
	}
	
	public function updateSetAnswer($larrformData,$id){  		
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "q006_answersetstudent";
		
		$where = "id = ".$id;
		
		$lobjDbAdpt->update($table,$larrformData,$where);
	}
	
	public function getRegisteredCourse($studentid,$sem,$course=0){	

		//echo $studMatric." + ". $sem." + ". $course;
			
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $lobjDbAdpt ->select()
					->from(array('a'=>'r015_student'),array("idStud"=>"a.id"))
					->joinLeft(array('b'=>'r024_student_course_registration'),'a.id = b.student_id',array('idCourseRegis'=>'b.id','*'))
					->joinLeft(array('d'=>'tbl_course_tagging_group'),'d.IdSubject = b.course_id ', array('*'))
					->join(array('f'=>'tbl_course_group_student_mapping'),'f.IdStudent = a.id AND f.IdCourseRegistration = b.id and f.IdCourseTaggingGroup = d.IdCourseTaggingGroup', array('*'))
					->join(array('c'=>'r010_course'),'c.id = b.course_id and d.IdSubject = c.id', array('course_name'=>'c.name','course_code'=>'c.code','course_credit_hour'=>'c.credit_hour'))
					->joinLeft(array('g'=>'q006_answersetstudent'),'g.coursegroup_id = d.IdCourseTaggingGroup AND g.student_id = a.id', array('statusSurvey'=>'g.status'))
					->join(array('e'=>'tbl_staffmaster'),'e.IdStaff = d.IdLecturer', array('lecturername'=>'FullName'))
					->where('b.semester_id = ?',$sem)
					->where('a.id = ?',$studentid)
					;
					
			if($course != 0){
				$select->where('b.course_id = ?',$course);
				
				$larrResult = $lobjDbAdpt->fetchRow($select);
			}else{
				$larrResult = $lobjDbAdpt->fetchAll($select);
			}
//			echo $select;
		return $larrResult;
			
	}
	
	public function getListRegisteredCourse($studentid,$sem){	

		//echo $studMatric." + ". $sem." + ". $course;
			
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $lobjDbAdpt ->select()
					->from(array('a'=>'r015_student'),array("idStud"=>"a.id"))
					->joinLeft(array('b'=>'r024_student_course_registration'),'a.id = b.student_id',array('idCourseRegis'=>'b.id','*'))
					->join(array('c'=>'r010_course'),'c.id = b.course_id', array('course_name'=>'c.name','course_code'=>'c.code','course_credit_hour'=>'c.credit_hour'))
					->where('b.semester_id = ?',$sem)
					->where('a.id = ?',$studentid)
					;
		
			$larrResult = $lobjDbAdpt->fetchAll($select);
//			echo $select;
		return $larrResult;
			
	}
	
	public function getGroupTagging($studentid,$sem){	

		//echo $studMatric." + ". $sem." + ". $course;
			
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $lobjDbAdpt ->select()
					->from(array('a'=>'tbl_course_group_student_mapping'),array("*"))
					->joinLeft(array('b'=>'tbl_course_tagging_group'),'b.IdCourseTaggingGroup = a.IdCourseTaggingGroup',array('*'))
					->where('a.IdStudent = ?',$studentid)
					->where('b.IdSemester = ?',$sem);
					
		$larrResult = $lobjDbAdpt->fetchAll($select);
		return $larrResult;
			
	}
	
	public function getGroupStudent($studentid,$sem,$course){	

		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $lobjDbAdpt ->select()
					->from(array('a'=>'tbl_course_group_student_mapping'),array("*"))
					->joinLeft(array('b'=>'tbl_course_tagging_group'),'b.IdCourseTaggingGroup = a.IdCourseTaggingGroup',array('*'))
					->where('a.IdStudent = ?',$studentid)
					->where('b.IdSemester = ?',$sem)
					->where('b.IdSubject = ?',$course);
					
		$larrResult = $lobjDbAdpt->fetchRow($select);
		return $larrResult;
			
	}
	
	public function getSetAnswer($studentid,$pool_id,$coursegroup=0,$questionid=0){	
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			
			 $select = $lobjDbAdpt ->select()
						->from(array('a'=>'q006_answersetstudent'),array("setanswer_id"=>"a.id","*"))
						->joinLeft(array('b'=>'q006_answerstudent'),'b.setanswer_id = a.id',array('idAnswer'=>'b.id','*'))
						->where('a.student_id = ?',$studentid)
						->where('a.pool_id = ?',$pool_id);
						
							
			if($coursegroup != 0){
				$select->where('a.coursegroup_id = ?',$coursegroup);
			}
			
			if($questionid != 0){
				$select->where('b.question_id = ?',$questionid);
				$larrResult = $lobjDbAdpt->fetchRow($select);
			}else{
				$larrResult = $lobjDbAdpt->fetchAll($select);
			}
//			echo $select;
//			exit;
			return $larrResult;
			
	}
	
	
	public function getSetAnswer2($studentid,$pool_id,$coursegroup=0){	
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			
			 $select = $lobjDbAdpt ->select()
						->from(array('a'=>'q006_answersetstudent'),array("*"))
						//->joinLeft(array('b'=>'q006_answerstudent'),'b.setanswer_id = a.id',array('idAnswer'=>'b.id','*'))
						->where('a.student_id = ?',$studentid)
						->where('a.pool_id = ?',$pool_id);
							
			if($coursegroup != 0){
				$select->where('a.coursegroup_id = ?',$coursegroup);
			}
			
				$larrResult = $lobjDbAdpt->fetchAll($select);
			
			return $larrResult;
			
	}
	
	public function getAnswerByQuestion($studentid,$questionid,$id,$main_set_id){	

		//echo $studMatric." + ". $sem." + ". $course;
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			
			 $select = $lobjDbAdpt ->select()
						->from(array('a'=>'q010_student_answer'))
						->where('a.student_id = ?',$studentid)
						->where('a.IdMain = ?',$id)
						->where('a.question_id = ?',$questionid)
						->where('a.main_set_id = ?',$main_set_id);
			$larrResult = $lobjDbAdpt->fetchRow($select);
			return $larrResult;
			
	}
	
	public function getListStudentByIntake($sem){	

		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$select = $lobjDbAdpt ->select()
					->from(array('a'=>'r015_student'),array("idStudent"=>"a.id","*"))
					->joinLeft(array('c'=>'r016_student_status'),'c.id = a.student_status_id',array('statusStudent'=>'c.name'))
					->joinLeft(array('b'=>'q006_answersetstudent'),'b.student_id = a.id',array('idSetAnswer'=>'b.id'))
					->where('a.admission_intake_id = ?',$sem)
					->group('a.id')
					->order('a.fullname asc')
					;
					
		$larrResult = $lobjDbAdpt->fetchAll($select);
		return $larrResult;
			
	}
	
	public function getAnswerStudent($studentID=0){	

		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$select = $lobjDbAdpt ->select()
					->from(array('a'=>'q006_answersetstudent'),array("*"));
					if($studentID){
					$select->where('a.student_id = ?',$studentID);
					}
					
		$larrResult = $lobjDbAdpt->fetchAll($select);
		return $larrResult;
			
	}
	
	public function getStatusSurveybyCourseGroupId($student_id,$coursegroup_id){
		
		$id = (int)$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q006_answersetstudent'),array("*"))
				->where('a.student_id = '.$student_id)
				->where('a.coursegroup_id = '.$coursegroup_id);
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function getRadioOption($main_set_id,$section_id,$tagging_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
					 ->from(array('r'=>'radio_option_list'))
					 ->where('r.main_set_id = ?', $main_set_id)
	                 ->where('r.section_id = ?', $section_id)
					 ->where('r.question_tagging_id = ?', $tagging_id);
		$row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getSetQuestion($main_set_id,$section_id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q003_questiontagging'),array("*"))
				->joinLeft(array('c'=>'q003_question'),'c.id = a.IdQuestion')
				->joinLeft(array('b'=>'q002_chapter'),'b.id = a.section_id',array('question_type'))
				->where("a.main_set_id IN ($main_set_id)")
				->group('a.IdQuestion')
				->order('a.order asc');
				
		if($section_id){
			$select->where("a.section_id IN ($section_id)");
		}
//		echo $select."<hr>";
		$row = $db->fetchAll($select);
		return $row;
	}
	
	public function getListLecturer($branch_id,$semester_id, $lec, $sub){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'tbl_course_tagging_group'))
				->joinLeft(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
				->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName'))
				
				->where('a.IdBranch = ?',$branch_id)
				->where('a.IdSemester = ?',$semester_id);

		if ($lec != ''){
			$select->where('a.IdLecturer = ?', $lec);
		}

		if ($sub != ''){
			$select->where('a.IdSubject = ?', $sub);
		}

		$row = $db->fetchAll($select);
		return $row;
	}
	
	public function getSetAnswerMain($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
				->from(array('a'=>'q010_student_answer_main'))
				->joinLeft(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('FullName'))
				->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('SubCode','SubjectName'))
				->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.student_id',array('registrationId'))
					->joinLeft(array('StudentProfile' => 'student_profile'), "srs.sp_id = StudentProfile.id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name",'StudentProfile.*' ))
				->where('a.id = ?',$id);
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function searchList($branch_id,$semester_id, $type = 0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
			->from(array('a'=>'tbl_course_tagging_group'))
			->where('a.IdBranch = ?',$branch_id)
			->where('a.IdSemester = ?',$semester_id);

		if ($type == 0) {
			$select->join(array('c'=>'tbl_staffmaster'),'c.IdStaff = a.IdLecturer',array('c.FullName'));
			$select->where('a.IdLecturer != ?', 0);
			$select->where('a.IdLecturer IS NOT NULL');
			$select->group('a.IdLecturer');
			$select->order('c.FullName ASC');
		}else{
			$select->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.IdSubject',array('s.SubCode','s.SubjectName'));
			$select->where('a.IdSubject != ?', 0);
			$select->where('a.IdSubject IS NOT NULL');
			$select->group('a.IdSubject');
			$select->order('s.SubCode ASC');
		}

		$row = $db->fetchAll($select);
		return $row;
	}
}

