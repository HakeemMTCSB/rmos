<?php

class Application_Model_DbTable_Programentry2 extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_programentry';
  private $lobjDbAdpt;

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  /* public function fnViewProgramEntry($lintIdProgramEntry ) { // Function to view the Purchase Order details based on id
    $result = $this->fetchRow( "IdProgramEntry  = '$lintIdProgramEntry'") ;
    return $result->toArray();
    } */

   public function fnViewProgramEntryreq($Id) { // Function to view the Purchase Order details based on id
     $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_programentryrequirement"))
                    ->joinLeft(array('b' => 'tbl_qualificationmaster'), 'a.EntryLevel = b.IdQualification',array('b.QualificationLevel'))
                    ->where("a.IdProgramEntryReq = ?", $Id);
    //echo $lstrSelect;die();
    $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
    }

  /* public function fnViewProgramEntry($lintIdProgramEntry) { //Function to get the Program Branch details
    $select = $this->select()
    ->setIntegrityCheck(false)
    ->join(array('a' => 'tbl_programentry'),array('IdProgramEntry'))
    ->join(array('b'=>'tbl_programentrylearningmode'),'a.IdProgramEntry  = b.IdProgramEntry')
    ->where('a.IdProgramEntry = '.$lintIdProgramEntry);;
    $result = $this->fetchAll($select);
    return $result->toArray();
    } */

  public function fnViewProgramEntry($lintIdProgramEntry) { //Function to get the Program Branch details
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_programentry"))
                    ->where("a.IdProgramEntry = ?", $lintIdProgramEntry);
    //echo $lstrSelect;die();
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  /* public function fngetProgramEntryDetails() { //Function to get the Program Branch details
    $select = $this->select()
    ->setIntegrityCheck(false)
    ->join(array('a' => 'tbl_programentry'),array('IdProgramEntry'))
    ->join(array('b'=>'tbl_program'),'a.IdProgram  = b.IdProgram')
    ->join(array('c'=>'tbl_programentrylearningmode'),'a.IdProgramEntry  = c.IdProgramEntry')
    ->join(array('d'=>'tbl_definationms'),'c.LearningMode = d.idDefinition')
    ->order("b.ProgramName");
    $result = $this->fetchAll($select);
    return $result->toArray();
    } */

  public function fngetProgramEntryDetails() { //Function to get the Program Branch details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->join(array('a' => 'tbl_programentry'), array('IdProgramEntry'))
                    ->join(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram')
                    ->order("b.ProgramName");
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  public function fnSearchProgramEntry($post = array()) { //Function for searching the user details
    $db = Zend_Db_Table::getDefaultAdapter();
    $field7 = "a.Active = " . $post["field7"];
    if (empty($post['field1'])) {
      $select = $this->select()
                      ->setIntegrityCheck(false)
                      ->join(array('a' => 'tbl_programentry'), array('IdProgramEntry'))
                      ->join(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram')
                      ->where($db->quoteInto("a.Active = ?", $post["field7"]))
                      ->order("b.ProgramName");
    } else {
      $select = $this->select()
                      ->setIntegrityCheck(false)
                      ->join(array('a' => 'tbl_programentry'), array('IdProgramEntry'))
                      ->join(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram')
                      ->where($db->quoteInto('a.IdProgram = ?', $post['field1']))
                      ->where($db->quoteInto("a.Active = ?", $post["field7"]))
                      ->order("b.ProgramName");
    }
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  /* public function fnSearchProgramEntry($post = array()) { //Function for searching the user details
    $db = Zend_Db_Table::getDefaultAdapter();

    $field7 = "a.Active = ".$post["field7"];
    $select = $this->select()
    ->setIntegrityCheck(false)
    ->join(array('a' => 'tbl_programentry'),array('IdProgramEntry'))
    ->join(array('b'=>'tbl_program'),'a.IdProgram  = b.IdProgram')
    ->join(array('c'=>'tbl_programentrylearningmode'),'a.IdProgramEntry  = c.IdProgramEntry')
    ->join(array('d'=>'tbl_definationms'),'c.LearningMode = d.idDefinition');
    if(isset($post['field5']) && !empty($post['field5'])){
    $select->where("c.LearningMode = ?",$post['field5']);

    }
    $select  ->where('b.ProgramName like "%" ? "%"',$post['field3'])
    ->where($field7)
    ->order("b.ProgramName");
    // echo $select;
    $result = $this->fetchAll($select);
    return $result->toArray();
    } */


  /* public function fnSearchProgramEntry($post = array()) { //Function for searching the user details
    $db = Zend_Db_Table::getDefaultAdapter();

    $field7 = "a.Active = ".$post["field7"];
    $select = $this->select()
    ->setIntegrityCheck(false)
    ->join(array('a' => 'tbl_programentry'),array('IdProgramEntry'))
    ->join(array('b'=>'tbl_program'),'a.IdProgram  = b.IdProgram')
    ->join(array('c'=>'tbl_programentrylearningmode'),'a.IdProgramEntry  = c.IdProgramEntry')
    ->join(array('d'=>'tbl_definationms'),'c.LearningMode = d.idDefinition');


    for($i = 0 ;$i<count($post['field22']); $i++) {
    if(isset($post['field22'][$i]) && !empty($post['field22'][$i])){
    if($i == 0)$select->where("c.LearningMode = ?",$post['field22'][$i]);
    else $select->orWhere("c.LearningMode = ?",$post['field22'][$i]);
    }
    }
    $select  ->where('b.ProgramName like "%" ? "%"',$post['field3'])
    ->where($field7)
    ->order("b.ProgramName");
    // echo $select;
    $result = $this->fetchAll($select);
    return $result->toArray();
    }
   */

  public function fnAddPOEntry($post) { // Function to insert the data to Purchase Order master table
    unset($post['Group']);
    unset($post['EntryLevel']);
    unset($post['IdSpecialization']);
    unset($post['Validity']);
    unset($post['Item']);
    unset($post['Condition']);
    unset($post['Value']);
    unset($post['Save']);
    unset($post['groupgrid']);
    unset($post['EntryLevelgrid']);
    unset($post['Specializationgrid']);
    unset($post['Validitygrid']);
    unset($post['Itemgrid']);
    unset($post['Conditiongrid']);
    unset($post['Valuegrid']);
    $lastinsertid = $this->insert($post);
    return $lastinsertid;
  }

  public function fnAddLearningModeDetails($IdProgramEntry, $larrformData) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    for ($i = 0; $i < count($larrformData['LearningMode']); $i++) {
      $lstrTable = "tbl_programentrylearningmode";
      $larrInsertData = array('IdProgramEntry' => $IdProgramEntry,
          'LearningMode' => $larrformData["LearningMode"][$i],
      );
      $lobjDbAdpt->insert($lstrTable, $larrInsertData);
    }
  }

  public function fnAddPOEntryDetails($larrprogramresult, $formData) {  // function to insert po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_programentryrequirement";
    $programcount = count($formData['Itemgrid']);
    for ($i = 0; $i < $programcount; $i++) {
      $larrcourse = array('IdProgramEntry' => $larrprogramresult,
          'GroupId' => $formData['groupgrid'][$i],
          'EntryLevel' => $formData['EntryLevelgrid'][$i],
          'IdSpecialization' => $formData['Specializationgrid'][$i],
          'Validity' => $formData['Validitygrid'][$i],
          'Item' => $formData['Itemgrid'][$i],
          'Condition' => $formData['Conditiongrid'][$i],
          'Value' => $formData['Valuegrid'][$i],
          'UpdDate' => $formData['UpdDate'],
          'UpdUser' => $formData['UpdUser']
      );
      $db->insert($table, $larrcourse);
    }
  }

  public function fnCheckExistingEntry($idprogram, $idlearningmode) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_programentry'), array('a.*'))
                    ->join(array("b" => "tbl_programentrylearningmode"), 'a.IdProgramEntry = b.IdProgramEntry')
                    ->where("a.IdProgram  = $idprogram")
                    ->where("b.LearningMode  = $idlearningmode");
    $result = $db->fetchAll($sql);
    return $result->toArray();
  }

  public function fndeleteProgramEntryLearningmode($lintIdProgramEntry) { //Function for Delete Purchase order terms
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_programentrylearningmode";
    $where = $db->quoteInto("IdProgramEntry = $lintIdProgramEntry");
    $db->delete($table, $where);
  }

  public function fnGetProgramEditEntryDetails($lintIdProgramEntry) { // Function to edit Purchase order details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->join(array('a' => 'tbl_programentryrequirement'), array('a.*'))
                    ->where("a.IdProgramEntry = '$lintIdProgramEntry'");
    $result = $this->fetchAll($select);
    return $result;
  }

  public function fninserttempprogramentryrequriments($larrmainprogramresult, $idpoentery) {  // function to insert po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempprogramentryrequirement";
    $sessionID = Zend_Session::getId();
    foreach ($larrmainprogramresult as $formData) {
      $larrtepprogrmentryreq = array(
          'Item' => $formData['Item'],
          'Condition' => $formData['Condition'],
          'Value' => $formData['Value'],
          'Validity' => $formData['Validity'],
          'EntryLevel' => $formData['EntryLevel'],
          'IdSpecialization' => $formData['IdSpecialization'],
          'GroupId' => $formData['GroupId'],
          'UpdDate' => $formData['UpdDate'],
          'UpdUser' => $formData['UpdUser'],
          'unicode' => $idpoentery,
          'Date' => date("Y-m-d"),
          'sessionId' => $sessionID,
          'idExists' => $formData['IdProgramEntryReq'],
          'deleteFlag' => '1'
      );
      $db->insert($table, $larrtepprogrmentryreq);
    }
    //print_r($larrcourse);die();
  }

  public function fnGetTempprogramentryReqDetails($lintIdProgramentry) { // Function to edit Purchase order details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->joinLeft(array('a' => 'tbl_tempprogramentryrequirement'), array('a.IdTemp'))
                    ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
                    ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
                    ->joinLeft(array('e' => 'tbl_qualificationmaster'), 'a.EntryLevel=e.IdQualification', array('e.QualificationLevel as EntryLevelname'))
                    ->joinLeft(array('f' => 'tbl_definationms'), 'a.GroupId=f.idDefinition', array('f.DefinitionDesc as GroupName'))
                    ->joinLeft(array('g' => 'tbl_specialization'), 'a.IdSpecialization=g.IdSpecialization OR a.IdSpecialization = 0	', array('g.Specialization as SpecializationName'))
                    ->where('a.deleteFlag =1')
                    ->where("a.unicode  = '$lintIdProgramentry'")
                    ->group("a.IdTemp");
    $result = $this->fetchAll($select);
    return $result;
  }


  public function fnGetprogramentryReqDetails($lintIdProgramentry) { // Function to edit Purchase order details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->joinLeft(array('a' => 'tbl_programentryrequirement'), array('a.IdProgramEntryReq'))
                    ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
                    ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
                    ->joinLeft(array('e' => 'tbl_qualificationmaster'), 'a.EntryLevel=e.IdQualification', array('e.QualificationLevel as EntryLevelname'))
                    ->joinLeft(array('f' => 'tbl_definationms'), 'a.GroupId=f.idDefinition', array('f.DefinitionDesc as GroupName'))
                    ->joinLeft(array('g' => 'tbl_specialization'), 'a.IdSpecialization=g.IdSpecialization', array('g.Specialization as SpecializationName'))
                    //->where('a.deleteFlag =1')
                    ->where("a.	IdProgramEntry  = '$lintIdProgramentry'");
    $result = $this->fetchAll($select);
    return $result;
  }

  public function fnUpdateTempEditProgramentryreqDetails($IdProgramEntry,$IdProgramReq,$IdSpecialization,$Item,$EntryLevel,$Condition,$Value,$Validity,$upddate,$upduser,$GroupId) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempprogramentryrequirement";
    $larridpo = array(
        'Item' => $Item,
        'IdSpecialization' => $IdSpecialization,
        'Condition' => $Condition,
        'Value' => $Value,
        'EntryLevel' => $EntryLevel,
        'Validity' => $Validity,
        'GroupId' => $GroupId,
        'UpdDate' => $upddate,
        'UpdUser' => $upduser);
    $where = "IdTemp = '$IdProgramReq'";
    $db->update($table, $larridpo, $where);
  }

  public function fnInsertNewTempProgramentryreqDetails($IdProgramEntry,$IdProgramReq,$IdSpecialization,$Item,$EntryLevel,$Condition,$Value,$Validity,$upddate,$upduser,$GroupId) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempprogramentryrequirement";
    $sessionID = Zend_Session::getId();
    $larridpo = array(
        'Item' => $Item,
        'IdSpecialization' => $IdSpecialization,
        'Condition' => $Condition,
        'Value' => $Value,
        'Validity' => $Validity,
        'EntryLevel' => $EntryLevel,
        'GroupId' => $GroupId,
        'UpdDate' => $upddate,
        'UpdUser' => $upduser,
        'unicode' => $IdProgramEntry,
        'Date' => date("Y-m-d"),
        'sessionId' => $sessionID,
        'idExists' => '0'
    );
    $db->insert($table, $larridpo);
  }

  public function fnUpdateProgramentry($idprogramentry, $post) { // Function to view the Purchase Order details based on id
    $db = Zend_Db_Table::getDefaultAdapter();
    unset($post['Item']);
    unset($post['Desc']);
    unset($post['IdProgramReq']);
    unset($post['IdProgramEntry']);
    unset($post['Group']);
    unset($post['EntryLevel']);
    unset($post['IdSpecialization']);
    unset($post['Validity']);
    unset($post['Condition']);
    //unset($post['LearningMode']);
    unset($post['Value']);
    unset($post['Save']);
    $where = 'IdProgramEntry = ' . $idprogramentry;
    $db->update('tbl_programentry', $post, $where);
  }

  public function fnGetProgramentryreqTemDetails($idprogramentry, $sessionID) { // Function to edit Purchase order details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->join(array('a' => 'tbl_tempprogramentryrequirement'), array('a.IdTemp'))
                    ->where("a.unicode = '$idprogramentry'")
                    ->where("a.sessionId = '$sessionID'");
    $result = $this->fetchAll($select);
    return $result;
  }

  public function fnviewLearningModeDetails($lintidProgram) { //Function to get the user details
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_definationms"), array("key" => "a.idDefinition", "value" => "a.DefinitionDesc"))
                    ->join(array('b' => 'tbl_programlearningmodedetails'), 'a.idDefinition = b.IdLearningMode')
                    ->where("b.IdProgram = ?", $lintidProgram);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnUpdateMainProgramentryreqDetails($transferedpodetails, $programentry) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_programentryrequirement";
    $where = "IdProgramEntryReq = '$programentry'";
    $db->update($table, $transferedpodetails, $where);
  }

  public function fnInsertMainProgramreqDetails($transferedinsertpodetails) {  // function to insert po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_programentryrequirement";
    $db->insert($table, $transferedinsertpodetails);
  }

  public function fnDeleteTempProgramentryreqDetails($programentry, $sessionID) { //Function for Delete Purchase order terms
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempprogramentryrequirement";
    $where = $db->quoteInto('unicode = ?', $programentry);
    $where = $db->quoteInto('sessionId = ?', $sessionID);
    $db->delete($table, $where);
  }

  public function fngetAllentry($IdProgramEntry) {
    $sql = $this->lobjDbAdpt->select()
                    ->from(array('a' => 'tbl_programentryrequirement'), array('a.*'))
                    ->where('IdProgramEntry = ?', $IdProgramEntry)
                    ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
                    ->joinLeft(array('c' => 'tbl_definationms'), 'a.GroupId =c.idDefinition', array('c.DefinitionDesc as Groupname'))
                    ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
                    ->joinLeft(array('e' => 'tbl_qualificationmaster'), 'a.EntryLevel=e.IdQualification	', array('e.QualificationLevel as EntryLevelname'));
    return $result = $this->lobjDbAdpt->fetchAll($sql);
  }

  public function fngetallsubjectentry($lintIdProgramEntry){
  		$db = Zend_Db_Table::getDefaultAdapter();
  		$sql = $db->select()
                    ->from(array('a' => 'tbl_group_subject'), array('a.*'))
                    ->join(array('b' => 'tbl_subject'),'a.IdSubject = b.IdSubject OR a.IdSubject = 0',array("b.SubjectName"))
                    ->joinLeft(array('c' => 'tbl_definationms'),'a.fieldofstudy = c.idDefinition',array("DefinitionDesc AS fosn"))
                    ->where('a.ProgramEntry = ?', $lintIdProgramEntry)
                    ->group('a.IdGroupSubject');
    return $result = $db->fetchAll($sql);
  }

  public function fnDeleteTempProgramentryDetailsBysession($sessionID) { //Function for Delete Purchase order terms
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempprogramentryrequirement";
    $where = $db->quoteInto('sessionId = ?', $sessionID);
    $db->delete($table, $where);
  }

  public function fnUpdateTempprogramentrydetails($lintidtemp) {  // function to update po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempprogramentryrequirement";
    $larramounts = array('deleteFlag' => '0');
    $where = "idTemp = '" . $lintidtemp . "'";
    $db->update($table, $larramounts, $where);
  }
  
  public function fndeletesubject($GroupId,$ProgramEntry,$IdSubject){
  	$db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_group_subject";
    $larramounts = array('deleteFlag' => '0');
    $where = "GroupId = '" . $GroupId . "' AND ProgramEntry = '" . $ProgramEntry . "' AND IdSubject = '" . $IdSubject . "'";
    $db->delete($table, $where);
    return;
  }

  public function fnCheckExistingItem($Item, $sessionID) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_tempprogramentryrequirement'), array('a.*'))
                    ->where("a.Item = $Item")
                    ->where("a.deleteFlag != 0")
                    ->where("a.sessionId = ?", $sessionID);
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function fnDeleteMainProgramreqDetails($IdProgramEntryReq) { //Function for Delete Purchase order terms
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_programentryrequirement";
    $where = $db->quoteInto('IdProgramEntryReq = ?', $IdProgramEntryReq);
    $db->delete($table, $where);
  }

  public function fngetprogramentry() {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_definationms"), array("key" => "a.idDefinition", "value" => "a.DefinitionDesc"))
                    ->join(array('b' => 'tbl_programentryrequirement'), 'a.idDefinition = b.Item');
    //->where("b.IdProgram = ?",$lintidProgram);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnAddSubjectGroup($data) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_group_subject";
    $db->insert($table, $data);
  }

  public function fngetgroupsubject($groupId, $IdprogramEntry, $IdQualification) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_group_subject'), array('a.*','a.IdSubject AS SubjectId'))
                    ->join(array('b' => 'tbl_subject'), 'a.IdSubject = b.IdSubject OR a.IdSubject = 0')
                    ->join(array('c' => 'tbl_qualificationmaster'), 'a.IdQualification = c.IdQualification',array('c.QualificationLevel'))
                    ->join(array('d' => 'tbl_definationms'), 'a.GroupId = d.idDefinition',array('d.DefinitionCode AS GroupName'))
                    ->join(array('e' => 'tbl_definationms'), 'a.fieldofstudy = e.idDefinition OR a.fieldofstudy = 0',array('e.DefinitionDesc AS FieldofstudyName'))
                    ->where("a.ProgramEntry = $IdprogramEntry")
                    ->where("a.GroupId = '" . $groupId . "'")
                    ->where("a.IdQualification ='".$IdQualification ."'")
                    ->group("IdGroupSubject"); 
    $result = $db->fetchAll($sql);
    return $result;
  }

  public function fndeleteSubjectGroup($groupId, $IdprogramEntry,$Idqualification) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_group_subject";
    $where = $db->quoteInto('ProgramEntry = ?', $IdprogramEntry);
    $where .= ' AND ' .$db->quoteInto('GroupId = ?', $groupId);
    $where .= ' AND ' .$db->quoteInto('IdQualification = ?', $Idqualification);
    //$where = $db->quoteInto('IdSubject != ?',0);
    $db->delete($table, $where);
  }

}

?>