<?php
class App_Model_Finance_DbTable_FeeStructureItemLevel extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_structure_item_level';
	protected $_primary = "fsis_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsis'=>$this->_name))
					->where("fsis.fsis_id = '".$id."'");
			
		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getStructureItemData($fee_structure_item_id, $level=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsis'=>$this->_name))
					->where("fsis.fsis_item_id = '".$fee_structure_item_id."'");
					
		if($level!=null){
			$selectData->where('fsis.fsis_level =?', $level);
			
			$row = $db->fetchRow($selectData);
		}else{
			$row = $db->fetchAll($selectData);	
		}
		
		if($row){
			return $row;
		}else{
			return null;
		}	
	}
		
	public function addData($postData){
		
		$data = array(
		        'fsis_item_id' => $postData['fsis_item_id'],
				'fsis_level' => $postData['fsis_level']				
		);
			
		$this->insert($data);
	}		
		

	public function updateItemData($postData){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$db->beginTransaction();
		try {
			//delete all item semester
			if( isset($postData['fsis_item_id']) && $postData['fsis_item_id'] !=""  ){
				$result = $db->delete('fee_structure_item_level', 'fsis_item_id = '.$postData['fsis_item_id']);
			}
						
			//add new item semester
			foreach ($postData['fsis_level'] as $lvl) {
				$data = array( 
					'fsis_item_id' => $postData['fsis_item_id'],
					'fsis_level' => $lvl
				);
				
				$db->insert('fee_structure_item_level', $data);
			}
			
	
			$this->_db->commit();
			
		} catch (Exception $e) {
     		// rollback
     		$this->_db->rollback();
     		echo $e->getMessage();
     		
     		exit;
		}
	}
	
	public function deleteData($id=null){
		if($id!=null){
			$data = array(
				'fi_active' => 0				
			);
				
			$this->update($data, "fi_id = '".$id."'");
		}
	}
}

