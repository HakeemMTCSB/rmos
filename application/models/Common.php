<?php
	class App_Model_Common {

		public function fnPagination($larrresult,$page,$lintpagecount) { // Function for pagination
			$paginator = Zend_Paginator::factory($larrresult); //instance of the pagination
			$paginator->setItemCountPerPage($lintpagecount);
			$paginator->setCurrentPageNumber($page);
			return $paginator;
		}

		//Get Student Id
		public function fnGetStudentId($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_studentapplication"),array("StudentId"))
    	   							->where("a.IDApplication = ?",$IdStudent)
    	   							->where("a.Termination = 0")
    	   							->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['StudentId'];
		}

		//Get Student Name
		public function fnGetStudentNamebyid($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_studentapplication"),array("CONCAT(a.FName,' ', IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS StudentName"))
    	   							->where("a.IDApplication = ?",$IdStudent)
    	   							->where("a.Termination = 0")
    	   							->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['StudentName'];
		}

		//Get Student Details
		public function fnGetStudentDetailsByid($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_studentapplication"),array("CONCAT(a.FName,' ', IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS StudentName","a.StudentId"))
    	   							->where("a.IDApplication = ?",$IdStudent)
    	   							->where("a.Termination = 0")
    	   							->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		//Get Registration Offer Template
		public function fnGetRegistrationOffer(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE ?","%"."Student Registration");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}

			//Get Registration Email Template
		public function  fnGetRegistrationEmailTemplate(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE ?","%"."Portal Login Template");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}

		//Function To Get SMTP Settings From Initial Config
		public function fnGetInitialConfigDetails($iduniversity){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_config"),array("a.SMTPServer","a.SMTPUsername","a.SMTPPassword","a.SMTPPort","a.SSL","a.DefaultEmail") )
       								->where("a.idUniversity = ?",$iduniversity);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		//Get List Of States From Country's Id
		public function fnGetCountryStateList($idCountry){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"StateName"))
					 				 ->where("a.idCountry = ?",$idCountry)
					 				 ->where("a.Active = 1")
					 				 ->order("a.StateName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		public function fnGetCityList($lintidState){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_city"),array("key"=>"a.idCity","value"=>"a.CityName"))
				 				  ->where("a.idState= ?",$lintidState)
				 				 ->where("a.Active = 1")
				 				 ->order("a.CityName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		//Get Countries List
		public function fnGetCountryList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_countries"),array("key"=>"a.idCountry","value"=>"CountryName"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.CountryName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		//Get School Namres

	public function fnGetSchoolList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_schoolmaster"),array("key"=>"a.idSchool","value"=>"SchoolName"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.SchoolName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}

		//Get All Active Student Names List
		public function fnGetAllActiveStudentNamesList() {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IDApplication","value"=>"CONCAT(a.fName,' ', IFNULL(a.mName,' '),' ',IFNULL(a.lName,' '))") )
					 				->where("a.Active = 1")
					  				->where("a.Termination = 0");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}

		//Get All Active Student Ids List
		public function fnGetAllActiveStudentIdsList() {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IDApplication","value"=>"a.StudentId") )
					 				->where("a.Active = 1")
					  				->where("a.Termination = 0");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}

		//Get State List
		public function fnGetStateList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"StateName"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.StateName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}


		//Function To Reset The Array ie., from name to key
		public function fnResetArrayFromNamesToValues($OrginialArray){
			$larrNewArr = array();
			$OrgnArray = @array_values($OrginialArray);
			for($lintI=0;$lintI<count($OrgnArray);$lintI++){
				$larrNewArr[$lintI]["value"] = $OrgnArray[$lintI]["name"];
				$larrNewArr[$lintI]["key"] = $OrgnArray[$lintI]["key"];
			}
			return $larrNewArr;
		}

		//Function To Reset The Array ie., from Value to Name
		public function fnResetArrayFromValuesToNames($OrginialArray){
			$larrNewArr = array();
			$OrgnArray = @array_values($OrginialArray);
			for($lintI=0;$lintI<count($OrgnArray);$lintI++){
				$larrNewArr[$lintI]["name"] = $OrgnArray[$lintI]["value"];
				$larrNewArr[$lintI]["key"] = $OrgnArray[$lintI]["key"];
			}
			return $larrNewArr;
		}

		public function fngetLanguage($lintIdCountry){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_countries"),array("a.DefaultLanguage"))
       								->join(array('b' => 'tbl_definationms'),'a.DefaultLanguage = b.idDefinition',array('b.DefinitionDesc'))
       								->where('a.idCountry = ?',$lintIdCountry)
       								->order("b.DefinitionDesc");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}


		//Function To Reset The Array ie., from Value to Name Along With Status
		public function fnResetArrayFromValuesToNamesWithStatus($OrginialArray){
			$larrNewArr = array();
			$OrgnArray = @array_values($OrginialArray);
			for($lintI=0;$lintI<count($OrgnArray);$lintI++){
				$larrNewArr[$lintI]["key"] = $OrgnArray[$lintI]["key"];
				$larrNewArr[$lintI]["name"] = $OrgnArray[$lintI]["value"];
				$larrNewArr[$lintI]["Status"] = $OrgnArray[$lintI]["Status"];
			}
			return $larrNewArr;
		}

		public function fnGetRoleDetails() {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()
				->join(array('a' => 'tbl_definationms'),array('idDefinition'))
				->join(array('b' => 'tbl_definationtypems'),'a.idDefType = b.idDefType',array('b.idDefType'))
				->where('b.defTypeDesc = "Role"')
				->where('a.Status = 1')
                ->where('b.Active = 1')
                ->order("b.defTypeDesc");
			$result = $lobjDbAdpt->fetchAll($select);
			return $result;
		}

		public function fnGetdocumentuploadDetails() {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()
				->join(array('a' => 'tbl_definationms'),array('idDefinition'))
				->join(array('b' => 'tbl_definationtypems'),'a.idDefType = b.idDefType',array('b.idDefType'))
				->where('b.defTypeDesc = "Documents Upload Type"')
				->where('a.Status = 1')
                ->where('b.Active = 1')
                ->order("b.defTypeDesc");
			$result = $lobjDbAdpt->fetchAll($select);
			return $result;
		}

		public function fnGetRoleName($idrole){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationms"),array("a.DefinitionDesc") )
       								->where('a.idDefinition = ?',$idrole);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		public function fnGetStaff($idStaff){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_staffmaster"),array("a.StaffType","a.IdCollege","a.FullName") )
       								->where('a.IdStaff = ?',$idStaff);
       								
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		public function fnGetStaffpd($idStaff){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				->from(array("a"=>"tbl_staffmaster"),array("a.StaffType","a.IdCollege","a.FullName") )
				->join(array('b' => 'tbl_chiefofprogramlist'),'a.IdStaff = b.IdStaff',array('b.Active'))
				->where('a.IdStaff = ?',$idStaff)
				->order('b.IdChiefOfProgramList DESC')
			;

			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		public function getUser($iduser)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									->from(array('b'=>'tbl_user'))
       								->join(array("a"=>"tbl_staffmaster"),'b.IdStaff=a.IdStaff',array("a.StaffType","a.IdCollege","a.FullName") )
       								->where('b.iduser = ?',$iduser);
       								
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		public function fnGetUniversity($idCollege){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_collegemaster"),array("a.AffiliatedTo"))
       								->where('a.IdCollege = ?',$idCollege);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}


		public function fnGetStaffDetails($idStaff){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_staffmaster"),array("a.StaffType","a.IdCollege"))
       								->where('a.IdStaff = ?',$idStaff);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		public function fnGetCollegeDetails($idCollege){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_collegemaster"),array("a.AffiliatedTo"))
       								->where('a.IdCollege = ?',$idCollege);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}

		public function fnGetApplicationEmailTemplate(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE ?","%"."Student Approval");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}
		
		public function fnGetCitybyCountryList($idCountry){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
			
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("c"=>"tbl_countries"),array(''))
       								->join(array('s' => 'tbl_state'),'s.idCountry = c.idCountry',array(''))
       								->join(array('ct' => 'tbl_city'),'ct.idState = s.idState',array("key"=>"idCity","value"=>"CityName"))
       								->where('c.idCountry = ?',$idCountry);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}



	}