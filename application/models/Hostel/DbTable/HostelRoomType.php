<?php 

class App_Model_Hostel_DbTable_HostelRoomType extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_hostel_room_type';
	protected $_primary = "IdHostelRoomType";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"ad_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getRoomList($type='263'){ //default 263=>student
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('rt'=>$this->_name))
					  ->join(array('rc'=>'tbl_room_charges'),'rc.IdHostelRoom=rt.IdHostelRoomType',array('Rate'))
					  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=rt.OccupancyType',array('OccupancyType'=>'DefinitionDesc'))
					  ->where("rc.CustomerType = ?", $type);  //student	
		 $row = $db->fetchAll($select);	
		
		 return $row;
	}
	
}

?>