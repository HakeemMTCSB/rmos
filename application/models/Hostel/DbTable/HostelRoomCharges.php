<?php 

class App_Model_Hostel_DbTable_HostelRoomCharges extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_room_charges';
	protected $_primary = "IdRoomCharges";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"ad_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getData($txn_id,$type){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					 // ->where("ad_type = '".$type."'")
					 // ->where("ad_txn_id='".$txn_id."'")
					  ->order('ad_id desc limit 1');
		 $row = $db->fetchRow($select);	
		
		 return $row;
	}
	
}

?>