<?php

class App_Model_Audit extends Zend_Db_Table 
{

	protected $_name = 'audit_log';

	public function init()
	{
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	/*
		log_id
		log_module
		log_identity
		log_type
		log_type_id
		log_ip
		log_data
		log_user
		log_date
	*/

	//App_Model_Audit::recordLog('Personal Details',$data);
	
	public function compareChanges($refarr, $data=array(), $skip=array())
	{
		$newdata = array();
		
		foreach ( $refarr as $ref_key => $ref_val )
		{
			
			if ( !in_array($ref_key, $skip) )
			{
				if ( empty($data) )
				{
					$newdata[] = array(	
										'type' => 'field',
										'variable' => $ref_key,
										'value' => $ref_val,
										'prev_value' => ''
										);
				}
				else
				{
					//workaround
					if ( $data[$ref_key] == 0 && $ref_val == '' )
					{
						$ref_val = 0; 
					}
					
					//add
					if ( isset($data[$ref_key] ) && $ref_val != $data[$ref_key] )
					{
						$newdata[] = array(	
											'type' => 'field',
											'variable' => $ref_key,
											'value' => $ref_val,
											'prev_value' => $data[$ref_key]
											);	
					}
				}
			}
		}

		return $newdata;
		
	}

	public function recordLog($type='', $type_id=0, $identity, $data = array(), $userId, $module )
	{
		$logdata = array(
							'log_module'	=>  $module,
							'log_identity'	=>	$identity,
							'log_type'		=>	$type,
							'log_type_id'	=>	$type_id,
							'log_data'		=>	json_encode($data),
							'log_user'		=>	$userId,
							'log_date'		=> 	new Zend_Db_Expr('NOW()'),
							'log_ip'		=>	ip2long( get_ip() )
						);
		
		$this->addLog($logdata);
	}

	public function addLog($data)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lobjDbAdpt->insert($this->_name, $data );
	}
	
	public function getDataById($id)
	{	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('a' => $this->_name) )
					  ->joinLeft(array('b'=>'tbl_user'), 'b.iduser=a.log_user', array('b.loginName'))
					  ->where("a.log_id= ?",$id);
		 $row = $db->fetchRow($select);	
		
		 return $row;
	}

	public function getDataByUser($id)
	{	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('a' => $this->_name))
					  ->joinLeft(array('b'=>'tbl_user'), 'b.iduser=a.log_user', array('b.loginName'))
					  ->where("a.log_identity= ?",$id)
					  ->order('a.log_id DESC');
		 $rows = $db->fetchAll($select);	
		
		 return $rows;
	}
}