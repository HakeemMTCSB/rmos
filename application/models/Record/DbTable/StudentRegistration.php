<?php


class App_Model_Record_DbTable_StudentRegistration extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregistration';
	protected $_primary = "IdStudentRegistration";


    protected $_referenceMap    = array(
        'StudentProfile' => array(
            'columns'           => 'sp_id',
            'refTableClass'     => 'Records_Model_DbTable_Studentprofile',
            'refColumns'        => 'id'
        ),
    );
	
	public function getData($appl_id=0){
		if($appl_id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('sr'=>$this->_name))
					->where('sr.IdApplication = ?',$appl_id)
					->where('sr.profileStatus = 92'); //Active

			$stmt = $db->query($select);
			$row = $stmt->fetch();
		}else{
			$row = $this->fetchAll();
			$row=$row->toArray();
		}
		
		if(!$row){
			throw new Exception("There is No Information Found");
		}
		return $row;
	}
	
	
	public function getStudentInfo($id=0){
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('sr'=>$this->_name))
				->join(array('ap'=>'student_profile'),'sr.sp_id=ap.id',array('appl_nationality','name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
				->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ArabicName','ProgramName','ProgramCode','IdScheme'))
				->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDesc','IntakeId', 'sem_seq'))
				->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('StudentStatus'=>'DefinitionDesc'))
				->joinLeft(array('pm'=>'tbl_programmajoring'),'pm.IDProgramMajoring=sr.IDProgramMajoring',array('majoring'=>'BahasaDescription'))
				->joinLeft(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=sr.IdBranch',array('BranchName','invoice'))
				->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('CollegeName'=>'ArabicName','c.IdCollege'))
				->joinLeft(array('sm'=>'tbl_staffmaster'),'sm.IdStaff=sr.AcademicAdvisor',array('AcademicAdvisor'=>'FullName',"FrontSalutation","BackSalutation"))
				->join(array("aps" => "tbl_program_scheme"),'aps.IdProgramScheme=sr.IdProgramScheme', array('*'))
				->where('sr.IdStudentRegistration = ?',$id);//takde checking student status 31072015
				//->where('sr.profileStatus = 92'); //Active
				
		$row = $db->fetchRow($select);						
		
		return $row;
	}
	
	
/*
	 * This function to get course registered by semester.
	 */
	public function getCourseRegisteredBySemester($registrationId,$idSemesterMain,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                     
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain);   
                                           
        if(isset($idBlock) && $idBlock != ''){ //Block 
        	$sql->where("srs.IdBlock = ?",$idBlock);
        	$sql->order("srs.BlockLevel");
        }  

     
             
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	
	/*
	 * This function to get course registered by semester.
	 */
	public function getCourseRegisteredByBlock($registrationId,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		 $sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')   
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                  
                        ->where('sr.IdStudentRegistration  = ?', $registrationId)
                        ->where("srs.IdBlock = ?",$idBlock)
                        ->order("srs.BlockLevel");
                      
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function getStudentLevelByProgram($program_id=0, $intake=array(),$level_min=null,$studentstatus=92){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$sql = $db->select()
		->from(array('sr' => 'tbl_studentregistration'), array('student_id'=>'IdStudentRegistration','nim'=>'registrationId','intake_id'=>'IdIntake'))
		->joinLeft(array('ap'=>'student_profile'),'ap.appl_id=sr.IdApplication',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )"), 'nationality'=>'appl_nationality'))
		->joinLeft(array('itk'=>'tbl_intake'),'itk.IdIntake = sr.IdIntake', array('intake_name'=>'IntakeId'))
				->where('sr.profileStatus  = ?',$studentstatus)
				->order("sr.registrationId");
	
		if($program_id){
		  $sql->where('sr.IdProgram =?',$program_id);
		}
		
		if($intake){
		  $sql->where('sr.IdIntake in ('.implode(",",$intake).')');
		}
		
		$result = $db->fetchAll($sql);
	
		return $result;
	
	}

    public function getProfile($IdStudentRegistration) {
        $student_registrations = $this->find($IdStudentRegistration);

        if($student_registrations->count() > 0) {
            $student_registration = $student_registrations->current();
            $student_profile = $student_registration->findParentRow('Records_Model_DbTable_Studentprofile');
            if(!empty($student_registration)) {
                return($student_profile);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
	public function getStudentByIntake ($intake=array(),$program_id=0,$studentstatus=92){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$sql = $db->select()
		->from(array('sr' => 'tbl_studentregistration'), array('student_id'=>'IdStudentRegistration','nim'=>'registrationId','intake_id'=>'IdIntake','*'))
		->joinLeft(array('ap'=>'student_profile'),'ap.appl_id=sr.IdApplication',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )"), 'nationality'=>'appl_nationality'))
		->join(array('itk'=>'tbl_intake'),'itk.IdIntake = sr.IdIntake', array('intake_name'=>'IntakeId'))
				->where('sr.profileStatus  = ?',$studentstatus)
				->order("sr.registrationId");
	
		if($program_id){
		  $sql->where('sr.IdProgram =?',$program_id);
		}
		
		if($intake){
		  $sql->where('sr.IdIntake IN (?)',$intake);
		}
		$result = $db->fetchAll($sql);
	
		return $result;
	
	}
}