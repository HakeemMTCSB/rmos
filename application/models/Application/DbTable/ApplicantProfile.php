<?php 

class App_Model_Application_DbTable_ApplicantProfile extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_profile';
	protected $_primary = "appl_id";
	protected $_db;
	
	/*function App_Model_Application_DbTable_ApplicantProfile(){
		$this->_db = Zend_Registry::get('dbapp');
	}*/
	
	public function addData($data){
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function uniqueEmail($email){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->where("appl_email = ?", $email);

		$row = $this->_db->fetchRow($select);	
		 
		if($row){
		 	return false;
		}else{
			return true;	
		}
	}
	
	public function verifyData($username,$password){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->where("appl_email = ?", $username)
					  ->where("appl_password = ?", $password);
					  
		 $row = $this->_db->fetchRow($select);	
		 return $row;
	}
	
	public function getForgotPasswordData($email,$dob){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->where("appl_email = ?", $email)
					  ->where("appl_dob = ?", $dob);
					  
		 $row = $this->_db->fetchRow($select);	
		 
		 if($row){
		 	return $row;
		 }else{
		 	return null;	
		 }
		 
	}
	
	public function getDataBy($id='', $field='appl_id')
	{
		$select = $this->_db->select()
				  ->from(array('a'=>$this->_name));

		
		 $select->where("a.".$field." = ?", $id);
		 $row = $this->_db->fetchRow($select);	
			 
		return $row;
	}
	
	public function getData($id="", $branch=0){
	
		$select = $this->_db->select()
					  ->from(array('a'=>$this->_name));
		if ( $branch )
		{
			$select->joinLeft(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=a.branch_id', array('b.BranchName'));
		}
		
		if($id)	{			
			 $select->where("a.appl_id ='".$id."'");
			 $row = $this->_db->fetchRow($select);	
			 
		}	else{			
			$row = $this->_db->fetchAll($select);	
		}	  
		
		 return $row;
	}
	
	
	public function getProfile ($txn_id=""){
	
		 $select = $this->_db->select()
		            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
		            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
		            ->join(array('c'=>'applicant_program'), 'a.at_trans_id = c.ap_at_trans_id')
		            ->joinLeft(array('d'=>'tbl_program'), 'c.ap_prog_id = d.IdProgram')
		            ->joinLeft(array('da'=>'tbl_award_level'), 'da.Id = d.Award', array('level_of_study'=>'GradeDesc'))
		            ->joinLeft(array('e'=>'tbl_program_scheme'), 'c.ap_prog_scheme = e.IdProgramScheme')
		            ->joinLeft(array('f'=>'registry_values'), 'e.mode_of_program = f.id', array('mop'=>'f.name'))
		            ->joinLeft(array('g'=>'registry_values'), 'e.mode_of_study = g.id', array('mos'=>'g.name'))
		            ->joinLeft(array('h'=>'registry_values'), 'e.program_type = h.id', array('pt'=>'h.name'))
		            ->joinLeft(array('i'=>'registry_values'), 'b.appl_category = i.id', array('StudentCategory'=>'i.name'))
		            ->joinLeft(array('j'=>'tbl_intake'), 'a.at_intake=j.IdIntake',array('IntakeDesc'))
		            ->joinLeft(array('k'=>'registry_values'), 'a.at_status = k.id', array('applicant_status'=>'k.name'))
		            ->joinLeft(array('l'=>'registry_values'), 'b.appl_idnumber_type = l.id', array('idtype'=>'l.name'))
		            ->joinLeft(array('m'=>'tbl_countries'), 'b.appl_nationality = m.idCountry', array('nationality'=>'m.CountryName'))
		            ->joinLeft(array('n'=>'registry_values'), 'b.appl_religion = n.id', array('religion'=>'n.name'))
		            ->joinLeft(array('o'=>'registry_values'), 'b.appl_race = o.id', array('race'=>'o.name'))
		            ->joinLeft(array('p'=>'registry_values'), 'b.appl_marital_status = p.id', array('marital_status'=>'p.name'))
		            ->joinLeft(array('city'=>'tbl_city'),'city.idCity=b.appl_city',array('CityName'))
		            ->joinLeft(array('state'=>'tbl_state'),'state.IdState=b.appl_state',array('StateName'))
		            ->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=b.appl_country',array('CountryName'))
		            ->joinLeft(array('ccity'=>'tbl_city'),'ccity.idCity=b.appl_ccity',array('CCityName'=>'CityName'))
		            ->joinLeft(array('cstate'=>'tbl_state'),'cstate.IdState=b.appl_cstate',array('CStateName'=>'StateName'))
		            ->joinLeft(array('cctrs'=>'tbl_countries'),'cctrs.idCountry=b.appl_ccountry',array('CCountryName'=>'CountryName'))
		            ->joinLeft(array('q'=>'registry_values'), 'b.appl_bumiputera = q.id', array('appl_bumiputera'=>'q.name'))
		            ->joinLeft(array('r'=>'registry_values'), 'b.appl_race = r.id', array('appl_race'=>'r.name'))
		            ->joinLeft(array('br'=>'tbl_branchofficevenue'), 'br.IdBranch = a.branch_id', array('BranchName'))
					->where("a.	at_trans_id = ?",$txn_id);
		$row = $this->_db->fetchRow($select);	
		return $row;
	}
	
	public function search($name="", $id="", $id_type=0, $program_id=0){
		
		$select = $this->_db->select()
						->from(array('a'=>$this->_name))
						->where("a.ARD_PROGRAM != 0 and a.ARD_OFFERED = 1")
						->join(array('p'=>'r006_program'),'p.id = a.ARD_PROGRAM',array('program_code'=>'code'))
						->join(array('mp'=>'r005_program_main'),'mp.id = p.program_main_id',array('main_name'=>'name'));
						
		if($name!=""){
			$select->where("ARD_NAME like '%".$name."%'");	
		}						
		
		if($id!=""){
			$select->where("ARD_IC like '%".$id."%'");	
		}
		
		if($id_type!=0){
			$select->where("ARD_TYPE_IC = ".$id_type);	
		}
		
		if($program_id!=0){
			$select->where("ARD_PROGRAM = ".$program_id);	
		}
		
		$stmt = $this->_db->query($select);
	    $row = $stmt->fetchAll();
	    
	    return $row;
	}
	
	public function getPaginateData(){
		
		 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')));
						
		return $select;
	}
	
	public function verify($transaction_id,$billing_no,$pin_no){
		
			 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))	
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')				 
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id')	
					  ->joinLeft(array('apb'=>'appl_pin_to_bank'),'apb.billing_no=apt.apt_bill_no')				  
					  ->where("at.at_trans_id ='".$transaction_id."'")
					  ->where("apt.apt_bill_no = '".$billing_no."'")
				      ->where("apb.REGISTER_NO = '".$pin_no."'");	//	entry yg belum pakai								
       
        $row = $this->_db->fetchRow($select);
		return $row;
	}
	
		public function viewkartu($transaction_id){
		
			 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))	
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')				 
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id')	
					  ->joinLeft(array('apb'=>'appl_pin_to_bank'),'apb.billing_no=apt.apt_bill_no')				  
					  ->where("at.at_trans_id ='".$transaction_id."'");	//	entry yg belum pakai								
       
        $row = $this->_db->fetchRow($select);
		return $row;
	}

public function getAllProfile ($id=""){
	
		 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('admission_type'=>'at_appl_type'))
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')))	
					  ->joinleft(array('p'=>'tbl_city'),'p.idCity=ap.appl_city',array('CityName'=>'p.CityName'))
					  ->joinleft(array('s'=>'tbl_state'),'s.idState=ap.appl_state',array('StateName'=>'s.StateName'))					  			
					  ->where("at.at_trans_id ='".$id."'");
		$row = $this->_db->fetchRow($select);	
		return $row;
	}
	
	public function getPaginateDatabyProgram($condition=null){
		
		 $select = $this->_db->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'school_master'),'sm.sm_id=ae.ae_institution',array('school'=>'sm.sm_name'));
					   
					   if($condition!=null){
					   		if($condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					  		if($condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					   		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");	
							}
					   }
					   
		//echo $select;			  
		return $select;
	}
	
	public function getDatabyProgram($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'));
					   
					   if($condition!=null){
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}
					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}
					   		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$period = explode('/',$condition["period"]);
					   							   		
								$select->where("MONTH(at.at_submit_date) = '".$period[0]."'");
								$select->where("YEAR(at.at_submit_date) = '".$period[1]."'");
					  		}
					   }
					   
		
		// echo $select;
		 
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getDeanSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->where("at.at_selection_status = 0");
					  
					  
					   
					   if($condition!=null){
					   	
					   		if(isset($condition["faculty_id"]) && $condition["faculty_id"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty_id"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}
					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}					   	
					   		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					  	   
					   }
					   
		
		
		// echo $select;
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getRectorSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->where("at.at_selection_status = 1");
					   
					   if($condition!=null){
					  		if(isset($condition["faculty_id"]) && $condition["faculty_id"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty_id"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}					   	
					  	 	if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					   }
					   
					   
		//echo $select; 
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getApprovalSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->where("at.at_selection_status = 2");
					   
					   if($condition!=null){
					   		if(isset($condition["faculty_id"]) && $condition["faculty_id"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty_id"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}
					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}
					   		
					       if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					  	   
					   }
					   
		
		
	  //echo $select;
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getResultSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('faculty'=>'c.ArabicName'))
					   ->where("at.at_selection_status = 3");
					   
					   if($condition!=null){
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}
					   		/*if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$period = explode('/',$condition["period"]);
					   							   		
								$select->where("MONTH(at.at_submit_date) = '".$period[0]."'");
								$select->where("YEAR(at.at_submit_date) = '".$period[1]."'");
					  		}*/
					  		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					  	   
					   }
			   
		$row = $db->fetchAll($select);	  
		return $row;
	}
	
	public function getAgentPaginateData($condition=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')));
					  
					  
	  					if($condition!=null){
					   		if(isset($condition["agent_id"]) && $condition["agent_id"]!=''){
					   			$select->where("at.agent_id ='".$condition["agent_id"]."'");
					   		}
					   		
					  	   
					   }			 
						
		return $select;
	}
	
	public function getAgentData($condition=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')));
					  
					  
	  					if($condition!=null){
					   		if(isset($condition["agent_id"]) && $condition["agent_id"]!=''){
					   			$select->where("at.agent_id ='".$condition["agent_id"]."'");
					   		}
					   		
					  	   
					   }			 
						
		return $select;
	}
	
	
	public function getStatusSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','selection_status'=>'at.at_selection_status','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('faculty'=>'c.ArabicName'))
					   ->order("at.at_pes_id DESC");
					  
					   
					   if($condition!=null){
					   		if(isset($condition["faculty"]) && $condition["faculty"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}					  		
					  		
					  		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}					  		
					  		if(isset($condition["selection_status"]) && $condition["selection_status"]!=''){
								$select->where("at.at_selection_status  = '".$condition["selection_status"]."'");
					  		}
					   	
					  		
					  	   
					   }
		//echo $select;	   
		$row = $db->fetchAll($select);	  
		return $row;
	}
	
	public function getTransProfile ($id="",$transid=""){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id AND at.at_trans_id='.$transid,array('admission_type'=>'at_appl_type'))
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('sd'=>'school_discipline'),'sd.smd_code=ae.ae_discipline_code',array('discipline'=>'sd.smd_desc'))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')))
					  //->where("at.at_status = 'APPLY'")
					  ->where("ap.appl_id ='".$id."'");

		$row = $db->fetchRow($select);	
		return $row;
	}

	public function mergeProfile($mginfo){
		  
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dataap["at_appl_id"]=$mginfo["maintain_appl_id"];	
		$sqlt = "Update applicant_transaction set at_appl_id='".$mginfo["maintain_appl_id"]."' where at_trans_id = '".$mginfo["chg_trans_id"]."'"; 
		$db->query($sqlt);
		//$db->update('applicant_transaction',$dataap, "at_trans_id = ".$mginfo["chg_trans_id"]);
		//echo $sql."<hr>";
		
		$data_advpayment["advpy_appl_id"]=$mginfo["maintain_appl_id"];
		$sqlap = "Update advance_payment set advpy_appl_id='".$mginfo["maintain_appl_id"]."' where advpy_fomulir = '".$mginfo["chg_formulir"]."'"; 
		$db->query($sqlap);
		//$db->update("advance_payment",$data_advpayment,"advpy_fomulir = ".$mginfo["chg_formulir"]);
		//echo $sql."<hr>";
		
		$data_invoice["appl_id"]=$mginfo["maintain_appl_id"];
		$sql1 = "Update invoice_main set appl_id='".$mginfo["maintain_appl_id"]."' where no_fomulir = '".$mginfo["chg_formulir"]."'"; 
		$db->query($sql1);
		//$db->update("invoice_main",$data_invoice,"no_fomulir = ".$mginfo["chg_formulir"]);
		//echo $sql."<hr>";
		
		$data_payment["appl_id"]=$mginfo["maintain_appl_id"];
		$sql2 = "Update payment_main set appl_id='".$mginfo["maintain_appl_id"]."' where payer = '".$mginfo["chg_formulir"]."'"; 
		$db->query($sql2);
		//$db->update("payment_main",$data_payment,"payer = ".$mginfo["chg_formulir"]);
		//echo $sql."<hr>";
		
		$data_cn["appl_id"]=$mginfo["maintain_appl_id"];
		$sql3 = "Update credit_note set appl_id='".$mginfo["maintain_appl_id"]."' where cn_fomulir = '".$mginfo["chg_formulir"]."'"; 
		$db->query($sql3);
		//$db->update("credit_note",$data_cn,"cn_fomulir = ".$mginfo["chg_formulir"]);
		//echo $sql."<hr>";
		
		$data_refund["rfd_appl_id"]=$mginfo["maintain_appl_id"];
		$sql4 = "Update refund set rfd_appl_id='".$mginfo["maintain_appl_id"]."' where rfd_fomulir = '".$mginfo["chg_formulir"]."'"; 
		$db->query($sql4);
		//$db->update("refund",$data_refund,"rfd_fomulir = ".$mginfo["chg_formulir"]);
		//echo $sql."<hr>";
		
		//Utk Disabled Profile tu//
		/*$sql5="Update applicant_profile set appl_email = '".$mginfo["mail_archive"]."_disabled' where appl_id=".$mginfo["archive_appl_id"];
		$db->query($sql5); */
		
		$log["m_formulir1"]=$mginfo["chg_formulir"];
		$log["m_formulir2"]=$mginfo["maintain_formulir"];
		$log["mail_maintain"]=$mginfo["mail_maintain"];
		$log["mail_archive"]=$mginfo["mail_archive"];
		$log["from_appl_id"]=$mginfo["archive_appl_id"];
		$log["to_appl_id"]=$mginfo["maintain_appl_id"];
		$log["mergedate"]=$mginfo["mergedate"];
		$log["mergeby"]=$mginfo["mergeby"];
		
		$db->insert("merge_log",$log); 
		
	} 
	
	public function getProfileByFormulir($noformulir){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('admission_type'=>'at_appl_type','at_trans_id','at_pes_id','at_status'))
					  ->joinleft(array('af'=>'applicant_family'),'af.af_appl_id=ap.appl_id and af_relation_type=21',array('af_name'))
					  ->where("at.at_pes_id =?",$noformulir)
					  ; 
			//echo $select."<br>";
			$row = $db->fetchRow($select);	
			return $row;
	}

	public function getProfileProgram($transaction_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
	      $select = $db ->select()
					->from(array('ap'=>'applicant_program'))	
					->join(array('at'=>'applicant_transaction'),'at.at_trans_id = ap.ap_at_trans_id')
				    ->join(array('apr'=>'applicant_profile'),'apr.appl_id = at.at_appl_id')		
				    ->joinLeft(array('st'=>'tbl_state'),'st.idState=apr.appl_state',array('StateName'))	
				    ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=st.idState',array('CityName'))	
				    ->joinLeft(array('nt' => 'tbl_countries'),'nt.idCountry=apr.appl_country',array('CountryName'))
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=ap.ap_prog_id',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
                    ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_study', array('StudyMode'=>'ds.DefinitionDesc'))
                      ->joinLeft(array("dss" => "tbl_definationms"),'dss.idDefinition=ap.program_mode', array('ProgramMode'=>'dss.DefinitionDesc'))
                    ->joinLeft(array("salute" => "tbl_definationms"), 'apr.appl_salutation=salute.idDefinition', array('Salutation'=>'DefinitionDesc'))
                      ->joinLeft(array("intake" => "tbl_intake"), 'intake.IdIntake=at.at_intake', array('Semester'=>'IntakeDesc'))
					->where("at.at_trans_id  = '".$transaction_id."'")				
					->order("ap.ap_preference Asc");
					
//        $stmt = $db->query($select);
//        $row = $stmt->fetchAll();
        
        $row = $db->fetchRow($select);
        
        if($row){
        	return $row;	
        }else{
        	return null;
        }
	}
        
        public function getApplicantProfile ($txn_id=""){
	
            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db ->select()
                                      ->from(array('ap'=>$this->_name))
                                      ->join(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('at_trans_id','at_intake','at_repository','at_submit_date','at_pes_id','at_status','at_default_qlevel'))					  
                                  ->joinLeft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id',array('IdProgramScheme'=>'ap_prog_scheme','program_code'=>'ap_prog_code','ap_prog_scheme'))
                                  ->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=apr.ap_prog_id',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
                                      ->joinLeft(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=apr.ap_prog_scheme',array(''))	
                                      ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=apr.program_mode', array('ProgramMode'=>'DefinitionDesc'))
                                  ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=apr.mode_study', array('StudyMode'=>'DefinitionDesc'))
                                      ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=apr.program_type', array('ProgramType'=>'DefinitionDesc'))					 
                                      ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=ap.appl_category', array('student_category'=>'DefinitionDesc'))
                                      ->joinLeft(array("Salutation" => "tbl_definationms"),'Salutation.idDefinition=ap.appl_salutation', array('salutation_def'=>'DefinitionDesc'))
                                      ->joinLeft(array("Marriage" => "tbl_definationms"),'Marriage.idDefinition=ap.appl_marital_status', array('marriage_desc'=>'DefinitionDesc'))
                                      ->joinLeft(array("Religion" => "tbl_definationms"),'Religion.idDefinition=ap.appl_religion', array('religion_desc'=>'DefinitionDesc'))
                                      ->joinLeft(array("Nationality" => "tbl_countries"),'Nationality.idCountry=ap.appl_nationality', array('nationality_desc'=>'CountryName'))
                                      ->joinLeft(array("Country" => "tbl_countries"),'Country.idCountry=ap.appl_country', array('country_perm_desc'=>'CountryName'))
                                      ->joinLeft(array("CountryCorr" => "tbl_countries"),'CountryCorr.idCountry=ap.appl_ccountry', array('country_corr_desc'=>'CountryName'))
                                      ->joinLeft(array("State" => "tbl_state"),'State.idState=ap.appl_state', array('state_perm_desc'=>'StateName'))
                                      ->joinLeft(array("StateCorr" => "tbl_state"),'StateCorr.idState=ap.appl_cstate', array('state_corr_desc'=>'StateName'))
                                      ->joinLeft(array("City" => "tbl_city"),'City.idCity=ap.appl_city', array('city_perm_desc'=>'CityName'))
                                      ->joinLeft(array("CityCorr" => "tbl_city"),'CityCorr.idCity=ap.appl_ccity', array('city_corr_desc'=>'CityName'))
                                      ->joinLeft(array("IDtype" => "tbl_definationms"),'IDtype.idDefinition=ap.appl_idnumber_type', array('appl_id_type'=>'DefinitionDesc'))

                                      ->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake=at.at_intake',array('IntakeDesc','IntakeDefaultLanguage'))					
                                      ->where("at.at_trans_id ='".$txn_id."'");					 
            $row = $db->fetchRow($select);	
            return $row;
	}
}
?>