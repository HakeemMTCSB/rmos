<?php 

class App_Model_Application_DbTable_ApplicantQualification extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_qualification';
	protected $_primary = "ae_id";

    protected $_referenceMap    = array(
        'ApplicantProfile' => array(
            'columns'           => 'ae_appl_id',
            'refTableClass'     => 'Application_Model_DbTable_ApplicantProfile',
            'refColumns'        => 'appl_id'
        ),
    );
	
	public function addData($data){	
	   $this->insert($data);
	   
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id=$db->lastInsertId();
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getTransData ($id="",$transid=""){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ae'=>$this->_name))
					  ->joinLeft(array('qm'=>'tbl_qualificationmaster'),('qm.IdQualification=ae.ae_qualification'),array('QualificationLevel'))
					  ->joinLeft(array('d'=>'tbl_definationms'),('d.idDefinition=ae.ae_class_degree'),array('class_degree'=>'DefinitionDesc'))
					  ->joinLeft(array('i'=>'tbl_institution'),('i.idInstitution=ae.ae_institution'),array('InstitutionName'))
					  ->joinLeft(array('c'=>'tbl_countries'),('c.idCountry=ae.ae_institution_country'),array('CountryName'))					
					  ->where("ae.ae_transaction_id = ?",$transid)
					  ->order("ae_order asc");
					  
		if(isset($id) && $id!=''){
			  $select->where("ae.ae_appl_id = ?",$id);
		}			
	
		$row = $db->fetchAll($select);			
		return $row;
	}	
        
        public function getEngPro($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('engpro'=>'applicant_english_proficiency'), array('value'=>'*'))
                ->joinLeft(array('qualmas'=>'tbl_qualificationmaster'), 'engpro.ep_test=qualmas.IdQualification', array('engproname'=>'qualmas.QualificationLevel'))
                ->where('engpro.ep_transaction_id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
	
        public function getEmp($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            
            $select = $db->select()
                ->from(array('appemp'=>'applicant_employment'), array('value'=>'*'))
                ->joinLeft(array('empdef'=>'tbl_definationms'), 'appemp.ae_status=empdef.idDefinition', array('empstatusname'=>'empdef.DefinitionDesc'))
                ->where('appemp.ae_trans_id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getVisa($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            
            $select = $db->select()
                ->from(array('a'=>'applicant_visa'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_definationms'), 'a.av_status=b.idDefinition', array('visastatusname'=>'b.DefinitionDesc'))
                ->where('a.av_trans_id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
		
	public function getQualification($id=""){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('ae'=>$this->_name))
					  ->joinLeft(array('qm'=>'tbl_qualificationmaster'),('qm.IdQualification=ae.ae_qualification'),array('QualificationLevel'))
					  ->joinLeft(array('d'=>'tbl_definationms'),('d.idDefinition=ae.ae_class_degree'),array('class_degree'=>'DefinitionDesc'))
					  ->joinLeft(array('i'=>'tbl_institution'),('i.idInstitution=ae.ae_institution'),array('InstitutionName'))
					  ->joinLeft(array('c'=>'tbl_countries'),('c.idCountry=ae.ae_institution_country'),array('CountryName'))
					  ->order("ae_order asc")	;				
					
					  
		if($id)	{			
			 $select->where("ae_id ='".$id."'");
			 $row = $db->fetchRow($select);	
			 
		}	else{			
			$row = $db->fetchAll($select);	
		}	  
		
		 return $row;
	}
	

	
}
?>