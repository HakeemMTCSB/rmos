<?php 

class App_Model_Application_DbTable_ApplicantSeqno extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_id_seqno';
	protected $_primary = "	seq_id";
	
	
	
	public function getData($seq_year){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('seq'=>$this->_name))	
	                  ->where('seq.seq_year   = ?',$seq_year);
		 $row = $db->fetchRow($select);			
		 return $row;
	}
	
	
	public function updateData($data,$id){		
		 $this->update($data,"seq_id =".(int)$id); 		 
	}
	
public function checkduplicate($pesid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
			->where('a.at_pes_id = ?', $pesid);

		$result = $db->fetchAll($select);
		return $result;
	}
	
}
?>