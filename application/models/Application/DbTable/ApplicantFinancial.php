<?php 

class App_Model_Application_DbTable_ApplicantFinancial extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_financial';
	protected $_primary = "af_id";
	
	public function addData($data){	
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id = $db->lastInsertId();
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getDataByTxn ($txn_id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('af'=>$this->_name))
					  ->joinLeft(array('FinanceMethod' => 'registry_values'),'FinanceMethod.id = af.af_method' ,array('name as financial_method'))
					  ->joinLeft(array('FinanceType' => 'registry_values'),'FinanceType.id = af.af_type_scholarship' ,array('name as financial_type_scholarship'))
					  //->joinLeft(array('scholarship' => 'tbl_scholarship_sch'),'scholarship.sch_Id = af.af_scholarship_apply',array('sch_name'))
					  ->joinLeft(array('sct'=>'tbl_scholarshiptagging_sct'), 'af.af_scholarship_apply = sct.sct_Id', array())
					  ->joinLeft(array('sch'=>'tbl_scholarship_sch'), 'sct.sct_schId = sch.sch_Id', array('sch.sch_name'))
					  ->joinLeft(array('sa'=>'sponsor_application'), 'af.af_id = sa.sa_af_id', array('sa.sa_status'))
					  ->where("af.af_trans_id = ?",$txn_id);					  
		
		$row = $db->fetchRow($select);		
		return $row;
	}	

	public function addSponserData($data){	
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $db->insert('sponsor_application',$data);	   
	  
	}
	
	public function getDataById ($id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('af'=>$this->_name))					
					  ->where("af.af_id = ?",$id);					  
		
		$row = $db->fetchRow($select);		
		return $row;
	}

	public function getDataFinancial ($txn_id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('af'=>$this->_name),array('table_id'=>'af_id'))		
					  ->joinLeft(array('df' => 'tbl_definationms'),'df.idDefinition=af.af_method' ,array('column1'=>'DefinitionDesc','column2'=>''))			
					  ->where("af.af_trans_id = ?",$txn_id);					  
		
		$row = $db->fetchAll($select);		//DO NOT CHNAGE TO FETCH ROW
		return $row;
	}	
	
	public function getSponsorshipStatus($txn_id){
		 $db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('af'=>$this->_name),array(''))		
					  ->joinLeft(array('sa' => 'sponsor_application'),'sa.sa_af_id=af.af_id')			
					  ->where("af.af_trans_id = ?",$txn_id);
		 $row = $db->fetchRow($select);
		 return $row;
		
	}		
	
	
}
?>