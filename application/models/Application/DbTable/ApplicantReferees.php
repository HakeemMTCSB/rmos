<?php 

class App_Model_Application_DbTable_ApplicantReferees extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_referees';
	protected $_primary = "r_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"r_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getData($txn_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->joinLeft(array("Country" => "tbl_countries"),'Country.idCountry=' .$this->_name. '.r_ref1_country', array('r_ref1_country_name'=>'CountryName'))
					  ->joinLeft(array("Country2" => "tbl_countries"),'Country2.idCountry=' .$this->_name. '.r_ref2_country', array('r_ref2_country_name'=>'CountryName'))
					  ->joinLeft(array("State" => "tbl_state"),'State.idState=' .$this->_name. '.r_ref1_state', array('r_ref1_state_name'=>'StateName'))
					  ->joinLeft(array("State2" => "tbl_state"),'State2.idState=' .$this->_name. '.r_ref2_state', array('r_ref2_state_name'=>'StateName'))	
					  ->where("r_txn_id='".$txn_id."'");
		 $row = $db->fetchRow($select);	
		
		 return $row;
	}
}
?>