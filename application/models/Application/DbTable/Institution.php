<?php

class App_Model_Application_DbTable_Institution extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_institution';
	protected $_primary = "idInstitution";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$row = $this->fetchRow($this->_primary.' = ' .$id);
		}else{
			$row = $this->fetchAll();
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}

		return $row->toArray();
	}
	
	public function getDatabyCountry($idCountry=0){
	
		 $db = Zend_Db_Table::getDefaultAdapter();
	  
		 $select = $db ->select()
					   ->from(array('i'=>'tbl_institution'))
					   ->where('i.Country = ?',$idCountry)
					   ->order('InstitutionName asc');
		 $row = $db->fetchAll($select);
		 

		return $row;
	}
}

