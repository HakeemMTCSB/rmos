<?php 

class App_Model_Application_DbTable_ApplicantEnglishProficiency extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_english_proficiency';
	protected $_primary = "ep_id";
	
	public function addData($data){	
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id=$db->lastInsertId();
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getData ($txn_id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		  $select = $db ->select()
					  ->from(array('ep'=>$this->_name), array('value'=>'*'))	
					  ->join(array('qm'=>'tbl_qualificationmaster'),'qm.IdQualification=ep.ep_test',array('QualificationLevel'))				
					  ->where("ep.ep_transaction_id = ?",$txn_id);
                                          //->where("ep.ep_test != ?", 24);					  
		
		$row = $db->fetchAll($select);		
		return $row;
	}
	
	public function getDataAll ($txn_id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		  $select = $db ->select()
					  ->from(array('ep'=>$this->_name))					
					  ->where("ep.ep_transaction_id = ?",$txn_id);					  
		
		$row = $db->fetchAll($select);		
		return $row;
	}	
	
	
}
?>