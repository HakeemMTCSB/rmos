<?php 

class App_Model_Application_DbTable_ApplicantVisa extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_visa';
	protected $_primary = "av_id";
	
	public function addData($data){	
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getTransData ($transid){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('av'=>$this->_name))
					  ->joinLeft(array('VisaStatus' => 'tbl_definationms'),'VisaStatus.idDefinition = av.av_status' ,array('DefinitionDesc as visa_status'))
					  ->where("av.av_trans_id = ?",$transid);
					  
		$row = $db->fetchRow($select);
			
		return $row;
	}		
	
	
}
?>