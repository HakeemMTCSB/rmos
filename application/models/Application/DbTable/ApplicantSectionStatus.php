<?php 

class App_Model_Application_DbTable_ApplicantSectionStatus extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_section_status';
	protected $_primary = "id";
	
	public function getStatus($transID,$sectionID){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("appl_trans_id = ?", $transID)
					  ->where("idSection = ?", $sectionID);
					  
		$row = $db->fetchRow($select);	

		return $row;
	}
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	
}
?>