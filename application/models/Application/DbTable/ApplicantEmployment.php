<?php 

class App_Model_Application_DbTable_ApplicantEmployment extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_employment';
	protected $_primary = "ae_id";


    protected $_referenceMap    = array(
        'ApplicantProfile' => array(
            'columns'           => 'ae_appl_id',
            'refTableClass'     => 'Application_Model_DbTable_ApplicantProfile',
            'refColumns'        => 'appl_id'
        ),
    );

	public function addData($data){	
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getTransData ($id="",$transid="",$order=0){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinLeft(array('d' => 'tbl_definationms'),'d.idDefinition=ap.ae_status' ,array('status'=>'DefinitionDesc'))
					  ->joinLeft(array('df' => 'tbl_definationms'),'df.idDefinition=ap.ae_industry' ,array('industry'=>'DefinitionDesc'))
					  ->joinLeft(array('dfn' => 'tbl_definationms'),'dfn.idDefinition=ap.ae_position' ,array('position'=>'DefinitionDesc'))
					  ->where("ap.ae_trans_id = ?",$transid);
					  
		if(isset($id)  && $id!=''){
			$select->where("ap.ae_appl_id = ?",$id);
		}			  
		
		$row = $db->fetchAll($select);
			
		return $row;
	}		
	
	
	public function getEmployement($id=""){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name);
					  
		if($id)	{			
			 $select->where("ae_id ='".$id."'");
			 $row = $db->fetchRow($select);	
			 
		}else{			
			$row = $db->fetchAll($select);	
		}	  
		
		 return $row;
	}
	
	
	
}
?>