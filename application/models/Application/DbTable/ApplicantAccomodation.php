<?php 

class App_Model_Application_DbTable_ApplicantAccomodation extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_accomodation';
	protected $_primary = "acd_id";
	
	public function addData($data){		
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id=$db->lastInsertId();
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"acd_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getDatabyTxn($txn_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)				
					  ->where("acd_trans_id= ?",$txn_id);
		 $row = $db->fetchRow($select);	
		
		 return $row;
	}
	
	
}	
?>