<?php 

class App_Model_Application_DbTable_ApplicantDocument extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_documents';
	protected $_primary = "ad_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"ad_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getData($txn_id,$type){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					 // ->where("ad_type = '".$type."'")
					  ->where("ad_txn_id='".$txn_id."'")
					  ->order('ad_id desc limit 1');
		 $row = $db->fetchRow($select);	
		
		 return $row;
	}
	
	public function getDocument($ad_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					   ->from($this->_name)				
					   ->where("ad_id = ?",$ad_id);					  
		
		$row = $db->fetchRow($select);		
		return $row;
	}
	
	public function getDataArray($txn_id,$tbl_id,$tbl_name){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ad'=>$this->_name))	
					  ->joinleft(array('dcl'=>'tbl_documentchecklist_dcl'),'dcl.dcl_Id=ad.ad_dcl_id',array('dcl_name'))				
					  ->where("ad.ad_table_id = '".$tbl_id."'")
					  ->where("ad.ad_table_name = '".$tbl_name."'")
					  ->where("ad.ad_txn_id='".$txn_id."'");
		 $row = $db->fetchAll($select);	
		
		 return $row;
	}
	
	
	public function getDataArrayBySection($txn_id,$section,$type=''){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ad'=>$this->_name))	
					  ->joinleft(array('dcl'=>'tbl_documentchecklist_dcl'),'dcl.dcl_Id=ad.ad_dcl_id',array('dcl_name'))				
					  ->where("ad.ad_section_id = '".$section."'")					  
					  ->where("ad.ad_txn_id='".$txn_id."'");
		 if ( $type != '' )
		 {
			 $select->where('dcl.dcl_name = ?',$type);
		 }

		 $row = $db->fetchAll($select);	
		
		 if($row){
		 	return $row;
		 }else{
		 	return null;
		 }
	}
        
        public function getApplicantPhoto($txnId){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $lstrSelect = $lobjDbAdpt->select()
                ->from(array("a"=>"applicant_documents"),array("value"=>"a.*"))
                ->join(array('b'=>'tbl_documentchecklist_dcl'), 'a.ad_dcl_id = b.dcl_Id')
                ->where('a.ad_txn_id = ?',$txnId)
                ->where('b.dcl_uplType = 67 or b.dcl_uplType = 65')
                //->orWhere('b.dcl_uplType = ?', 65)
                ->where('a.ad_section_id = ?', 2)
                ->order('a.ad_id DESC');
            //echo $lstrSelect;
            
            $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
            return $larrResult;
        }
        
        
	public function getDataArrayByTxn($txn_id,$section_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ad'=>$this->_name))	
					  ->joinleft(array('dcl'=>'tbl_documentchecklist_dcl'),'dcl.dcl_Id=ad.ad_dcl_id',array('dcl_name'))		
					  ->where("ad.ad_txn_id='".$txn_id."'")
					  ->where('ad.ad_section_id = ?', $section_id);
		
		 $row = $db->fetchAll($select);	
		
		 if($row){
		 	return $row;
		 }else{
		 	return null;
		 }
	}
	
}	
?>