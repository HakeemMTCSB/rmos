<?php 

class App_Model_Application_DbTable_ApplicantHealthCondition extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_health_condition';
	protected $_primary = "ah_id";
	
	public function addData($data){	
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function deleteInfo($txnid){		
	  $this->delete('ah_trans_id =' . (int)$txnid);
	}
	
	public function getRowData ($transid,$status){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ah'=>$this->_name))					
					  ->where("ah.ah_trans_id = ?",$transid)
					  ->where("ah.ah_status = ?",$status);
					  
		$row = $db->fetchRow($select);
			
		return $row;
	}		
	
	public function getData ($transid){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ah'=>$this->_name))
					  ->joinLeft(array('HealthStatus' => 'tbl_definationms'),'HealthStatus.idDefinition = ah.ah_status' ,array('DefinitionDesc as health_status'))
					  ->where("ah.ah_trans_id = ?",$transid);
					  
		$row = $db->fetchAll($select);
			
		return $row;
	}		
}
?>