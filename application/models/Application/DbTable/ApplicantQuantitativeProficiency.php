<?php 

class App_Model_Application_DbTable_ApplicantQuantitativeProficiency extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_quantitative_proficiency';
	protected $_primary = "qp_id";
	
	public function addData($data){	
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id=$db->lastInsertId();
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
		
	
	public function getData($id=""){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name);
					  
		if($id)	{			
			 $select->where("qp_id ='".$id."'");
			 $row = $db->fetchRow($select);	
			 
		}else{			
			$row = $db->fetchAll($select);	
		}	  
		
		 return $row;
	}
	
	public function getTransData ($transid){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('qp'=>$this->_name))					 
					  ->where("qp.qp_trans_id = ?",$transid);
					  
		$row = $db->fetchAll($select);
			
		return $row;
	}	
	
	public function getQuantitativeData ($transid){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('qp'=>$this->_name),array('value'=>'qp.*'))					 
					  ->where("qp.qp_trans_id = ?",$transid);
					  
		$row = $db->fetchAll($select);
			
		return $row;
	}
	
}
?>