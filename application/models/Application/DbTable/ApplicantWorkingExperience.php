<?php 

class App_Model_Application_DbTable_ApplicantWorkingExperience extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_working_experience';
	protected $_primary = "aw_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function deletebyTxn($txn_id){		
	  $this->delete('aw_trans_id =' . (int)$txn_id);
	}
	
	public function getData ($transid="",$ind_id){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('aw'=>$this->_name))		
					  ->where("aw.aw_industry_id = ?",$ind_id)			 
					  ->where("aw.aw_trans_id = ?",$transid);
					  
		$row = $db->fetchRow($select);
			
		return $row;
	}	

	
	public function getInfo ($transid=""){
	 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('aw'=>$this->_name))
					  ->join(array('Industry' => 'tbl_definationms'), 'Industry.idDefinition = aw.aw_industry_id', array('industry_name' => 'Industry.DefinitionDesc'))
					  ->join(array('ExYear' => 'tbl_definationms'), 'ExYear.idDefinition = aw.aw_years', array('exp_year' => 'ExYear.DefinitionDesc'))
					  ->where("aw.aw_trans_id = ?",$transid);
					  
		$row = $db->fetchAll($select);
			
		return $row;
	}	
}
?>