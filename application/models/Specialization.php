<?php
class Application_Model_Specialization extends Zend_Db_Table {
	protected $_name = 'tbl_specialization';
	private $lobjDbAdpt;
	/**
	 *
	 * @see Zend_Db_Table_Abstract::init()
	 */
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fngetSpecializationList() { //Function to get the Soecialization details
		$lstrSelect = $this->lobjDbAdpt->select()->from($this->_name);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetSpecialization(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_specialization"),array("key"=>"a.IdSpecialization","value"=>"a.Description"))
		->order("a.Specialization");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchSpecialization($post = array()) { //Function for searching the Soecialization details

		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("is"=>"tbl_specialization"),array("is.*"))
		->where('is.Specialization like "%" ? "%"',$post['field3'])
		->where('is.Description like "%" ? "%"',$post['field2'])
		->order("is.Specialization");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnaddSpecialization($data) {
		$this->insert($data);
	}

	public function fnupdateSpecialization($data,$IdSpecialization) {
		$where = 'IdSpecialization = '.$IdSpecialization;
		$this->update($data,$where);
	}

	public function fndeleteSpecialiation($IdSpecialization) {
		$where = $this->lobjDbAdpt->quoteInto('IdSpecialization= ?', $IdSpecialization);
		$this->lobjDbAdpt->delete($this->_name, $where);
	}
	
	
}