<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 29/10/2015
 * Time: 3:36 PM
 */
class App_Model_Profile extends Zend_Db_Table {

    public function getProfile($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_user'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdStaff = b.IdStaff')
            ->joinLeft(array('c'=>'tbl_definationms'), 'b.FrontSalutation = c.idDefinition', array('salutationname'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.IdRole = d.idDefinition', array('rolename'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.Religion = e.idDefinition', array('religionname'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'b.IdLevel = f.idDefinition', array('levelname'=>'f.DefinitionDesc'))
            ->where('a.iduser = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
}