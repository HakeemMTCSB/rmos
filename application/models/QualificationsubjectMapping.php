<?php
class App_Model_QualificationsubjectMapping extends Zend_Db_Table {
	protected $_name = 'tbl_qualification_subject_mapping';
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnadd($data) {
		$this->insert($data);
	}
	
	public function fngetmappings($IdQualification) {
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_qualificationmaster'),'',array())
		->join(array('b'=>$this->_name),'a.IdQualification = b.IdQualification',array())
		->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
		->where("b.IdQualification = ?",$IdQualification);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fndeletemappings($IdQualification) {
		$where = $this->lobjDbAdpt->quoteInto('IdQualification = ?', $IdQualification);
		$this->lobjDbAdpt->delete($this->_name, $where);
	}


}