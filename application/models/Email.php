<?php

class App_Model_Email extends Zend_Db_Table 
{

	protected $_name = 'email_que';
	protected $_attach = 'email_que_attachment';
	protected $_comm_tpl = 'comm_template';
	protected $_comm_tpl_content = 'comm_template_content';
	protected $_comm_rec = 'comm_compose_recipients';

	public function init()
	{
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	/* add into email_que */
	public function add($data)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lobjDbAdpt->insert($this->_name, $data);

		return $lobjDbAdpt->lastInsertId();
	}
	
	/* add attachment */
	public function addAttachment($data)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lobjDbAdpt->insert($this->_attach, $data);

		return $lobjDbAdpt->lastInsertId();
	}


	public function getTemplate($value='',$field='tpl_pname',$lang='en_US')
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				->from(array("a"=>$this->_comm_tpl ),array("value"=>"a.*"))
				->joinLeft(array('b'=>$this->_comm_tpl_content),'a.tpl_id=b.tpl_id',array('b.tpl_content'))
				->where('a.'.$field.' = ?', $value);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);

		return $larrResult;
	}
}