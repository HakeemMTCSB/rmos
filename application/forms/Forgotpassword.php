<?php

class App_Form_Forgotpassword extends Zend_Form
{
	public function init()
    {
    	
		$email = new Zend_Form_Element_Text('email');
		$email			 
				->setAttrib('maxlength','50')
				->setAttrib('required','required')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		$email->class = "input-txt";
		  
		$password = new Zend_Form_Element_Password('password');
		$password			 
			->setAttrib('maxlength','50')      
			->setAttrib('required','required')
			->removeDecorator("DtDdWrapper")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag');
		$password->class = "input-txt";

		$confirmpass = new Zend_Form_Element_Password('confirmpass');
		$confirmpass			 
			->setAttrib('maxlength','50')      
			->setAttrib('required','required')
			->removeDecorator("DtDdWrapper")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag');
		$confirmpass->class = "input-txt";

		$submit = $this->createElement('submit','submit');
		$submit->dojotype="dijit.form.Button";
		$submit->label = "Retrieve Password";	
		$submit->removeDecorator("DtDdWrapper");
		$submit->class = "btn-submit";

		$this->addElements(array($submit,$email,$password, $confirmpass));
	}
    


}

