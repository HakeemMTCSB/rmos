<?php
class Application_Form_Manualapplication extends Zend_Dojo_Form {

	public function init() {
		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		$lobjstate = new GeneralSetup_Model_DbTable_State();
		$lobjcity = new GeneralSetup_Model_DbTable_City();
		$lobjcountry = new GeneralSetup_Model_DbTable_Country();
		$larrcountrylist = $lobjcountry->fnGetCountryDetails();
		$larrstatelist = $lobjstate->fnGetStateDetails();



		$month = date("m"); // Month value
		$day = date("d"); //today's date
		$year = date("Y"); // Year value
		$yesterdaydate = date('Y-m-d', mktime(0, 0, 0, $month, ($day -1), $year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";

		$StudentId = new Zend_Form_Element_Text('StudentId');
		$StudentId->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('readonly', true)->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$StudentCollegeId = new Zend_Form_Element_Text('StudentCollegeId');
		$StudentCollegeId->setAttrib('maxlength', '50')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$FName = new Zend_Form_Element_Text('FName');
		$FName->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");
		//New Addition place of birth
		$PlaceOfBirth = new Zend_Form_Element_Text('PlaceOfBirth');
		$PlaceOfBirth->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		//Religion
		$Religion = new Zend_Dojo_Form_Element_FilteringSelect('Religion');
		$Religion->setAttrib('required', "false");
		$Religion->removeDecorator("DtDdWrapper");
		$Religion->removeDecorator("Label");
		$Religion->removeDecorator('HtmlTag');
		$Religion->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$BloodGroup = new Zend_Dojo_Form_Element_FilteringSelect('BloodGroup');
		$BloodGroup->setAttrib('required', "false");
		$BloodGroup->removeDecorator("DtDdWrapper");
		$BloodGroup->removeDecorator("Label");
		$BloodGroup->removeDecorator('HtmlTag');
		$BloodGroup->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$MName = new Zend_Form_Element_Text('MName');
		$MName->setAttrib('maxlength', '50')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$LName = new Zend_Form_Element_Text('LName');
		$LName->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$FourthName = new Zend_Form_Element_Text('FourthName');
		$FourthName->setAttrib('required', "False")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$ThirdName = new Zend_Form_Element_Text('ThirdName');
		$ThirdName->setAttrib('required', "False")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$FullName = new Zend_Form_Element_Text('FullName');
		$FullName->setAttrib('required', "True")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '80')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$FullArabicName = new Zend_Form_Element_Text('FullArabicName');
		//$FullArabicName->setAttrib('required', "True")
		$FullArabicName->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '80')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$ConfirmDeclaration = new Zend_Form_Element_Checkbox('ConfirmDeclaration');
		$ConfirmDeclaration->setChecked(true)->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$NameField1 = new Zend_Form_Element_Text('NameField1');
		$NameField1->setAttrib('dojoType', "dijit.form.ValidationTextBox")
		->setAttrib('maxlength', '50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
		$NameField1->class = "namefield";

		$NameField2 = new Zend_Form_Element_Text('NameField2');
		$NameField2->setAttrib('dojoType', "dijit.form.ValidationTextBox")
		->setAttrib('maxlength', '50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
		$NameField2->class = "namefield";

		$NameField3 = new Zend_Form_Element_Text('NameField3');
		$NameField3->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		$NameField3->class = "namefield";

		$NameField4 = new Zend_Form_Element_Text('NameField4');
		$NameField4->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		$NameField4->class = "namefield";

		$NameField5 = new Zend_Form_Element_Text('NameField5');
		$NameField5->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '50')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		$NameField5->class = "namefield";
		
		$ExtraIdField1 = new Zend_Form_Element_Text('ExtraIdField1');
		 $ExtraIdField1->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->setAttrib('required','true')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField2 = new Zend_Form_Element_Text('ExtraIdField2');
		$ExtraIdField2->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField3 = new Zend_Form_Element_Text('ExtraIdField3');
		$ExtraIdField3->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField4 = new Zend_Form_Element_Text('ExtraIdField4');
		$ExtraIdField4->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField5 = new Zend_Form_Element_Text('ExtraIdField5');
		$ExtraIdField5->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField6 = new Zend_Form_Element_Text('ExtraIdField6');
		$ExtraIdField6->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField7 = new Zend_Form_Element_Text('ExtraIdField7');
		$ExtraIdField7->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField8 = new Zend_Form_Element_Text('ExtraIdField8');
		$ExtraIdField8->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField9 = new Zend_Form_Element_Text('ExtraIdField9');
		$ExtraIdField9->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField10 = new Zend_Form_Element_Text('ExtraIdField10');
		$ExtraIdField10->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		/*$ExtraIdField11 = new Zend_Form_Element_Text('ExtraIdField11');
		$ExtraIdField11->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField12 = new Zend_Form_Element_Text('ExtraIdField12');
		$ExtraIdField12->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField13 = new Zend_Form_Element_Text('ExtraIdFieldd13');
		$ExtraIdField13->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField14 = new Zend_Form_Element_Text('ExtraIdField14');
		$ExtraIdField14->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField15 = new Zend_Form_Element_Text('ExtraIdField15');
		$ExtraIdField15->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField16 = new Zend_Form_Element_Text('ExtraIdField16');
		$ExtraIdField16->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField17 = new Zend_Form_Element_Text('ExtraIdField17');
		$ExtraIdField17->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField18 = new Zend_Form_Element_Text('ExtraIdField18');
		$ExtraIdField18->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField19 = new Zend_Form_Element_Text('ExtraIdField19');
		$ExtraIdField19->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ExtraIdField20 = new Zend_Form_Element_Text('ExtraIdField20');
		$ExtraIdField20->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','50')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag'); */

		$TypeofId = new Zend_Dojo_Form_Element_FilteringSelect('TypeofId');
		$TypeofId->setAttrib('required', "false");
		$TypeofId->removeDecorator("DtDdWrapper");
		$TypeofId->removeDecorator("Label");
		$TypeofId->removeDecorator('HtmlTag');
		$TypeofId->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$IdNo = new Zend_Form_Element_Text('IdNo');
		$IdNo->setAttrib('required', "false");
		$IdNo->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$IdNo->removeDecorator("DtDdWrapper");
		$IdNo->removeDecorator("Label");
		$IdNo->removeDecorator('HtmlTag');

		$DefmodelObj = new App_Model_Definitiontype();
		$raceList = $DefmodelObj->fnGetDefinationMs('Race');
		$Race = new Zend_Dojo_Form_Element_FilteringSelect('Race');
		$Race->setAttrib('required', "false");
		$Race->removeDecorator("DtDdWrapper");
		$Race->removeDecorator("Label");
		$Race->removeDecorator('HtmlTag');
		$Race->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$Race->addMultiOptions($raceList);

		$SpecialTreatmentList = $DefmodelObj->fnGetDefinationMs('SpecialTreatment');
		$SpecialTreatment = new Zend_Dojo_Form_Element_FilteringSelect('SpecialTreatment');
		$SpecialTreatment->setAttrib('required', "false");
		$SpecialTreatment->removeDecorator("DtDdWrapper");
		$SpecialTreatment->removeDecorator("Label");
		$SpecialTreatment->removeDecorator('HtmlTag');
		$SpecialTreatment->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$SpecialTreatment->addMultiOptions($SpecialTreatmentList);

		$SpecialTreatmentTypeList = $DefmodelObj->fnGetDefinationMs('SpecialTreatmentType');
		$SpecialTreatmentType = new Zend_Dojo_Form_Element_FilteringSelect('SpecialTreatmentType');
		$SpecialTreatmentType->setAttrib('required', "false");
		$SpecialTreatmentType->removeDecorator("DtDdWrapper");
		$SpecialTreatmentType->removeDecorator("Label");
		$SpecialTreatmentType->removeDecorator('HtmlTag');
		$SpecialTreatmentType->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$SpecialTreatmentType->addMultiOptions($SpecialTreatmentTypeList);

		$DateOfBirth = new Zend_Dojo_Form_Element_DateTextBox('DateOfBirth');
		$DateOfBirth->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "$dateofbirth")->setAttrib('title', "dd-mm-yyyy")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$Gender = new Zend_Form_Element_Radio('Gender');
		$Gender->addMultiOptions(array (
				'1' => 'Male',
				'0' => 'Female'
		))->setvalue('1')->setSeparator('&nbsp;')->setAttrib('dojoType', "dijit.form.RadioButton")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$MaritalStatus = new Zend_Form_Element_Select('MaritalStatus');
		$MaritalStatus->setAttrib('required', "true");
		$MaritalStatus->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		//Type of residing in Jakarta
		$TypeOfResidence = new Zend_Form_Element_Select('TypeOfResidence');
		$TypeOfResidence->setAttrib('required', "true");
		$TypeOfResidence->addMultiOptions(array (
				'0' => 'Family House',
				'1' => 'RentHouse'
		))->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$NoOfChildren = new Zend_Form_Element_Text('NoOfChildren', array (
				'regExp' => '[\0-9]+'
		));
		$NoOfChildren->setAttrib('maxlength', '2')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Nationality = new Zend_Dojo_Form_Element_FilteringSelect('Nationality');
		$Nationality->removeDecorator("DtDdWrapper");
		$Nationality->setAttrib('required', "true");
		$Nationality->removeDecorator("Label");
		$Nationality->removeDecorator('HtmlTag');
		$Nationality->setRegisterInArrayValidator(false);
		$Nationality->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$IdType = new Zend_Dojo_Form_Element_FilteringSelect('IdType');
		$IdType->setAttrib('required', "false");
		$IdType->removeDecorator("DtDdWrapper");
		$IdType->removeDecorator("Label");
		$IdType->removeDecorator('HtmlTag');
		$IdType->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$IssuePlace = new Zend_Form_Element_Text('IssuePlace');
		$IssuePlace->setAttrib('maxlength', '50')->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$IssueDate = new Zend_Dojo_Form_Element_DateTextBox('IssueDate');
		$IssueDate->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "$dateofbirth")->setAttrib('title', "dd-mm-yyyy")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$ExpiryDate = new Zend_Dojo_Form_Element_DateTextBox('ExpiryDate');
		$ExpiryDate->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "$dateofbirth")->setAttrib('title', "dd-mm-yyyy")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$PermAddressDetails = new Zend_Form_Element_Text('PermAddressDetails');
		$PermAddressDetails->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$PermAddressDetails2 = new Zend_Form_Element_Text('PermAddressDetails2');
		$PermAddressDetails2->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$PermCountry = new Zend_Form_Element_Select('PermCountry');
		$PermCountry->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', "fnGetPermCountryStateList(this,'PermState')")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PermState = new Zend_Form_Element_Select('PermState');
		$PermState->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->setRegisterInArrayValidator(false)->setAttrib('OnChange', 'fnGetStateCityListPerm')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PermCity = new Zend_Dojo_Form_Element_FilteringSelect('PermCity');
		$PermCity->removeDecorator("DtDdWrapper");
		$PermCity->removeDecorator("Label");
		$PermCity->removeDecorator('HtmlTag');
		$PermCity->setAttrib('required', "true");
		$PermCity->setRegisterInArrayValidator(false);
		$PermCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$PermZip = new Zend_Form_Element_Text('PermZip');
		$PermZip->setAttrib('required', "true")->setAttrib('maxlength', '10')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CorrsAddressDetails = new Zend_Form_Element_Text('CorrsAddressDetails');
		$CorrsAddressDetails->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$CorrsAddressDetails2 = new Zend_Form_Element_Text('CorrsAddressDetails2');
		$CorrsAddressDetails2->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$CorrsCity = new Zend_Dojo_Form_Element_FilteringSelect('CorrsCity');
		$CorrsCity->removeDecorator("DtDdWrapper");
		$CorrsCity->removeDecorator("Label");
		$CorrsCity->removeDecorator('HtmlTag');
		$CorrsCity->setAttrib('required', "false");
		$CorrsCity->setRegisterInArrayValidator(false);
		$CorrsCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$CorrsState = new Zend_Form_Element_Select('CorrsState');
		$CorrsState->setRegisterInArrayValidator(false)->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', 'fnGetStateCityListCorrs')->setAttrib('required', "false")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CorrsCountry = new Zend_Form_Element_Select('CorrsCountry');
		$CorrsCountry->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', "fnGetCorrsCountryStateList(this,'CorrsState')")->setAttrib('required', "false")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CorrsZip = new Zend_Form_Element_Text('CorrsZip');
		$CorrsZip->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$OutcampusAddressDetails = new Zend_Form_Element_Text('OutcampusAddressDetails');
		$OutcampusAddressDetails->setAttrib('maxlength', '100');
		$OutcampusAddressDetails->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$OutcampusAddressDetails->removeDecorator("Label");
		$OutcampusAddressDetails->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$OutcampusAddressDetails2 = new Zend_Form_Element_Text('OutcampusAddressDetails2');
		$OutcampusAddressDetails2->setAttrib('maxlength', '100');
		$OutcampusAddressDetails2->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails2->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$OutcampusAddressDetails2->removeDecorator("Label");
		$OutcampusAddressDetails2->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails2->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$OutcampusCity = new Zend_Dojo_Form_Element_FilteringSelect('OutcampusCity');
		$OutcampusCity->removeDecorator("DtDdWrapper");
		$OutcampusCity->removeDecorator("Label");
		$OutcampusCity->removeDecorator('HtmlTag');
		$OutcampusCity->setAttrib('required', "false");
		$OutcampusCity->setRegisterInArrayValidator(false);
		$OutcampusCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$OutcampusState = new Zend_Form_Element_Select('OutcampusState');
		$OutcampusState->setRegisterInArrayValidator(false);
		$OutcampusState->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OutcampusState->setAttrib('OnChange', 'fnGetStateCityListOutcampus');
		$OutcampusState->setAttrib('required', "false");
		$OutcampusState->removeDecorator("DtDdWrapper");
		$OutcampusState->removeDecorator("Label");
		$OutcampusState->removeDecorator('HtmlTag');

		$OutcampusCountry = new Zend_Form_Element_Select('OutcampusCountry');
		$OutcampusCountry->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OutcampusCountry->setAttrib('OnChange', "fnGetOutcampusCountryStateList(this,'OutcampusState')");
		$OutcampusCountry->setAttrib('required', "false");
		$OutcampusCountry->removeDecorator("DtDdWrapper");
		$OutcampusCountry->removeDecorator("Label");
		$OutcampusCountry->removeDecorator('HtmlTag');

		$OutcampusZip = new Zend_Form_Element_Text('OutcampusZip');
		$OutcampusZip->setAttrib('maxlength', '20');
		$OutcampusZip->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$OutcampusZip->removeDecorator("DtDdWrapper");
		$OutcampusZip->removeDecorator("Label");
		$OutcampusZip->removeDecorator('HtmlTag');

		$HomePhonecountrycode = new Zend_Form_Element_Text('HomePhonecountrycode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$HomePhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomePhonecountrycode->setAttrib('maxlength', '3');
		$HomePhonecountrycode->setAttrib('style', 'width:30px');
		$HomePhonecountrycode->removeDecorator("DtDdWrapper");
		$HomePhonecountrycode->removeDecorator("Label");
		$HomePhonecountrycode->removeDecorator('HtmlTag');

		$HomePhonestatecode = new Zend_Form_Element_Text('HomePhonestatecode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$HomePhonestatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomePhonestatecode->setAttrib('maxlength', '2');
		$HomePhonestatecode->setAttrib('style', 'width:30px');
		$HomePhonestatecode->removeDecorator("DtDdWrapper");
		$HomePhonestatecode->removeDecorator("Label");
		$HomePhonestatecode->removeDecorator('HtmlTag');

		$HomePhone = new Zend_Form_Element_Text('HomePhone', array (
				'regExp' => '[0-9]+'
		));
		$HomePhone->setAttrib('style', 'width:93px')->setAttrib('maxlength', '9')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CellPhonecountrycode = new Zend_Form_Element_Text('CellPhonecountrycode', array (
				'regExp' => "[0-9+]+",
				'invalidMessage' => "Only digits"
		));
		$CellPhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$CellPhonecountrycode->setAttrib('maxlength', '4');
		$CellPhonecountrycode->setAttrib('style', 'width:50px');
		$CellPhonecountrycode->removeDecorator("DtDdWrapper");
		$CellPhonecountrycode->removeDecorator("Label");
		$CellPhonecountrycode->removeDecorator('HtmlTag');

		$CellPhone = new Zend_Form_Element_Text('CellPhone', array (
				'regExp' => '[\0-9()+-]+'
		));
		$CellPhone->setAttrib('style', 'width:109px')->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Faxcountrycode = new Zend_Form_Element_Text('Faxcountrycode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$Faxcountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Faxcountrycode->setAttrib('maxlength', '3');
		$Faxcountrycode->setAttrib('style', 'width:30px');
		$Faxcountrycode->removeDecorator("DtDdWrapper");
		$Faxcountrycode->removeDecorator("Label");
		$Faxcountrycode->removeDecorator('HtmlTag');

		$Faxstatecode = new Zend_Form_Element_Text('Faxstatecode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$Faxstatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Faxstatecode->setAttrib('maxlength', '2');
		$Faxstatecode->setAttrib('style', 'width:30px');
		$Faxstatecode->removeDecorator("DtDdWrapper");
		$Faxstatecode->removeDecorator("Label");
		$Faxstatecode->removeDecorator('HtmlTag');

		$Fax = new Zend_Form_Element_Text('Fax', array (
				'regExp' => '[0-9]+'
		));
		$Fax->setAttrib('style', 'width:93px')->setAttrib('maxlength', '9')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$EmailAddress = new Zend_Form_Element_Text('EmailAddress', array (
				'regExp' => "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",
				'invalidMessage' => "Not a valid email"
		));
		$EmailAddress->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$EmailAddress->setAttrib('required', "true")->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$idCollege = new Zend_Form_Element_Select('idCollege');
		$idCollege->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")
		//->setAttrib('OnChange','GetCoursebyCollegeId')
		->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$idsponsor = new Zend_Form_Element_Select('idsponsor');
		$idsponsor->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->setAttrib('required', "false")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$InternationalStd = new Zend_Form_Element_Checkbox('InternationalStd');
		$InternationalStd->setAttrib('onClick', 'showpassportdetails(this.checked)')->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Email = new Zend_Form_Element_Text('Email', array (
				'regExp' => "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",
				'invalidMessage' => "Not a valid email"
		));
		$Email->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Email->setAttrib('required', "true")->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$SameCorrespAddr = new Zend_Form_Element_Checkbox('SameCorrespAddr');
		$SameCorrespAddr->setAttrib('onClick', 'showcorrespondanceaddr(this.checked)')->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$SameOutcampusAddr = new Zend_Form_Element_Checkbox('SameOutcampusAddr');
		$SameOutcampusAddr->setAttrib('onClick', 'showOutcampusaddr(this.checked)')->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$PPNos = new Zend_Form_Element_Text('PPNos');
		$PPNos->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PPIssueDt = new Zend_Dojo_Form_Element_DateTextBox('PPIssueDt');
		$PPIssueDt->removeDecorator("Label")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "$dateofbirth")->setAttrib('onChange', "dijit.byId('PPExpDt').constraints.min = arguments[0];")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$PPExpDt = new Zend_Dojo_Form_Element_DateTextBox('PPExpDt');
		$PPExpDt->removeDecorator("Label")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")->setAttrib('onChange', "dijit.byId('PPIssueDt').constraints.max = arguments[0];")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$PPField1 = new Zend_Form_Element_Text('PPField1');
		$PPField1->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PPField2 = new Zend_Form_Element_Text('PPField2');
		$PPField2->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PPField3 = new Zend_Form_Element_Text('PPField3');
		$PPField3->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PPField4 = new Zend_Form_Element_Text('PPField4');
		$PPField4->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PPField5 = new Zend_Form_Element_Text('PPField5');
		$PPField5->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$VisaDetailField1 = new Zend_Form_Element_Text('VisaDetailField1');
		$VisaDetailField1->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$VisaDetailField2 = new Zend_Form_Element_Text('VisaDetailField2');
		$VisaDetailField2->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$VisaDetailField3 = new Zend_Form_Element_Text('VisaDetailField3');
		$VisaDetailField3->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$VisaDetailField4 = new Zend_Form_Element_Text('VisaDetailField4');
		$VisaDetailField4->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$VisaDetailField5 = new Zend_Form_Element_Text('VisaDetailField5');
		$VisaDetailField5->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Update = new Zend_Form_Element_Hidden('UpdDate');
		$Update->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->setAttrib('id', 'UpdUser')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$Active = new Zend_Form_Element_Checkbox('Active');
		$Active->setChecked(true)->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$hobbies = new Zend_Form_Element_Textarea('hobbies');
		$hobbies->setAttrib('cols', '40')->setAttrib('rows', '5')->setAttrib('maxlength', '250')->setAttrib('dojoType', "dijit.form.SimpleTextarea")->setAttrib('style', 'margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$othercurricularactivity = new Zend_Form_Element_Textarea('othercurricularactivity');
		$othercurricularactivity->setAttrib('cols', '40')->setAttrib('rows', '5')->setAttrib('maxlength', '250')->setAttrib('dojoType', "dijit.form.SimpleTextarea")->setAttrib('style', 'margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PreffredHostel = new Zend_Form_Element_Select('PreffredHostel');
		$PreffredHostel->addMultiOption('0', 'Select')->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', 'GetHostelDescription')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PreferredBlock = new Zend_Form_Element_Select('PreferredBlock');
		$PreferredBlock->addMultiOptions(array (
				"0" => "Select"
		))->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', 'GetFloorDetails')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PreffredFloor = new Zend_Form_Element_Select('PreffredFloor');
		$PreffredFloor->addMultiOption('0', 'Select')->setAttrib('dojoType', "dijit.form.FilteringSelect")
		//->setAttrib('OnChange','GetDormitoryDetails')
		->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PreffredroomType = new Zend_Form_Element_Select('PreffredroomType');
		$PreffredroomType->addMultiOption('0', 'Select')->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', 'GetBedDetails')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PrefferedApartment = new Zend_Form_Element_Select('PrefferedApartment');
		$PrefferedApartment->addMultiOption('0', 'Select')->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', 'GetDormitoryDetails')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PreffredBed = new Zend_Form_Element_Select('PreffredBed');
		$PreffredBed->addMultiOption('0', 'Select')->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->setAttrib('OnChange', 'fnCheckBedAvailability')->removeDecorator("Label")->removeDecorator('HtmlTag');

		$bedAllotedto = new Zend_Form_Element_Radio('bedAllotedto');
		$bedAllotedto->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('dojoType', "dijit.form.RadioButton")->addMultiOptions(array (
				'0' => 'Single Bed',
				'1' => 'All Beds',
				'2' => 'Multi-Select'
		))->setValue('0')->setAttrib('onClick', 'ToggleBedSelectionType(this.value)')->setSeparator('');

		$RoomPrefredFrom = new Zend_Dojo_Form_Element_DateTextBox('RoomPrefredFrom');
		$RoomPrefredFrom->removeDecorator("Label")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")->setAttrib('onChange', "dijit.byId('RoomPrefredto').constraints.min = arguments[0];")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$RoomPrefredto = new Zend_Dojo_Form_Element_DateTextBox('RoomPrefredto');
		$RoomPrefredto->removeDecorator("Label")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")->setAttrib('onChange', "dijit.byId('RoomPrefredFrom').constraints.max = arguments[0];")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$currentjobfromdate = new Zend_Dojo_Form_Element_DateTextBox('currentjobfromdate');
		$currentjobfromdate->setAttrib('class', 'txt_put')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")->setAttrib('onChange', "dijit.byId('currentjobtodate').constraints.min = arguments[0];")->removeDecorator('HtmlTag');

		$currentjobtodate = new Zend_Dojo_Form_Element_DateTextBox('currentjobtodate');
		$currentjobtodate->setAttrib('class', 'txt_put')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.DateTextBox")->setAttrib('title', "dd-mm-yyyy")->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")->setAttrib('onChange', "dijit.byId('currentjobfromdate').constraints.max = arguments[0];")->removeDecorator('HtmlTag');

		$currentjobtitle = new Zend_Form_Element_Text('currentjobtitle');
		$currentjobtitle->setAttrib('class', 'txt_put')->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$currentjoborganizationtype = new Zend_Form_Element_Select('currentjoborganizationtype');
		$currentjoborganizationtype->setAttrib('class', 'txt_put')->addMultiOptions(array (
				"0" => "Select"
		))->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->setRegisterInArrayValidator(false)->removeDecorator('HtmlTag');

		$currentjobemployer = new Zend_Form_Element_Text('currentjobemployer');
		$currentjobemployer->setAttrib('class', 'txt_put')->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$currentjobemployeraddress = new Zend_Form_Element_Text('currentjobemployeraddress');
		$currentjobemployeraddress->setAttrib('class', 'txt_put')->setAttrib('maxlength', '250')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$currentjobsalary = new Zend_Form_Element_Text('currentjobsalary', array (
				'regExp' => '[\0-9.,]+'
		));
		$currentjobsalary->setAttrib('maxlength', '50')->setAttrib('class', 'txt_put')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$IDCourse = new Zend_Form_Element_Select('IDCourse');
		$IDCourse->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', "fnGetPlacementtest(this);fnGetSubjectlist(this);fnGetProgchecklist(this)")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$IdApplication = new Zend_Form_Element_Hidden('IdApplication');
		$IdApplication->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype = "dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class', 'NormalBtn');
		$Save->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Close = new Zend_Form_Element_Button('Close');
		$Close->dojotype = "dijit.form.Button";
		$Close->label = $gstrtranslate->_("Close");
		$Close->setAttrib('class', 'NormalBtn')->setAttrib('onclick', 'fnCloseLyteBox()')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('OnClick', 'addEducationDetails()')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		$Add->dojotype = "dijit.form.Button";
		$Add->setAttrib('class', 'NormalBtn');
		$Add->label = $gstrtranslate->_("Add");

		$InstitutionName = new Zend_Form_Element_Text('InstitutionName');
		$InstitutionName->setAttrib('maxlength', '50')->setAttrib('class', 'txt_put')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');
		$InstitutionNameSelect = new Zend_Form_Element_Select('InstitutionNameSelect');
		$InstitutionNameSelect //->setAttrib('required',"true")
		->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$UniversityName = new Zend_Form_Element_Text('UniversityName');
		$UniversityName->setAttrib('maxlength', '150')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$StudyPlace = new Zend_Form_Element_Text('StudyPlace');
		$StudyPlace->setAttrib('maxlength', '150')->setAttrib('class', 'txt_put')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$MajorFiledOfStudy = new Zend_Form_Element_Text('MajorFiledOfStudy');
		$MajorFiledOfStudy->setAttrib('maxlength', '150')->setAttrib('class', 'txt_put')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		/* 	$YearOfStudyFrom  = new Zend_Dojo_Form_Element_DateTextBox('YearOfStudyFrom');
		 $YearOfStudyFrom ->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->setAttrib('dojoType',"dijit.form.DateTextBox")
		->setAttrib('title',"dd-mm-yyyy")
		->setAttrib('constraints', "{datePattern:'yyyy'}")
		->removeDecorator('HtmlTag');

		$YearOfStudyTo  = new Zend_Dojo_Form_Element_DateTextBox('YearOfStudyTo');
		$YearOfStudyTo ->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->setAttrib('dojoType',"dijit.form.DateTextBox")
		->setAttrib('title',"dd-mm-yyyy")
		->setAttrib('constraints', "{datePattern:'yyyy'}")
		->removeDecorator('HtmlTag'); */

		$YearOfStudyFrom = new Zend_Form_Element_Text('YearOfStudyFrom');
		$YearOfStudyFrom->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$YearOfStudyTo = new Zend_Form_Element_Text('YearOfStudyTo');
		$YearOfStudyTo->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$DegreeType = new Zend_Form_Element_Select('DegreeType');
		$DegreeType->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$GradeOrCGPA = new Zend_Form_Element_Select('GradeOrCGPA');
		$GradeOrCGPA->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$ICNumber = new Zend_Form_Element_Text('ICNumber');
		$ICNumber->setAttrib('dojoType', "dijit.form.TextBox");
		$ICNumber->setAttrib('required', "true")->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PersonalID = new Zend_Form_Element_Text('PersonalID');
		$PersonalID->setAttrib('dojoType', "dijit.form.TextBox");
		$PersonalID->setAttrib('required', "true")->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Registered = new Zend_Form_Element_Hidden('Registered');
		$Registered->removeDecorator("DtDdWrapper")->setValue(0)->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Offered = new Zend_Form_Element_Hidden('Offered');
		$Offered->removeDecorator("DtDdWrapper")->setValue(1)->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Accepted = new Zend_Form_Element_Hidden('Accepted');
		$Accepted->removeDecorator("DtDdWrapper")->setValue(1)->removeDecorator("Label")->removeDecorator('HtmlTag');

		$ApplicationType = new Zend_Form_Element_Hidden('ApplicationType');
		$ApplicationType->removeDecorator("DtDdWrapper")->setValue(0)->removeDecorator("Label")->removeDecorator('HtmlTag');

		$IdPlacementtest = new Zend_Form_Element_Select('IdPlacementtest');
		$IdPlacementtest->setRegisterInArrayValidator(false)->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('required', "false")->setAttrib('OnChange', "fnGetPlacementtesttime(this)")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$IdStudEduDtl = new Zend_Form_Element_Hidden('IdStudEduDtl');
		$IdStudEduDtl->setAttrib('dojoType', "dijit.form.TextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$ApplicationDate = new Zend_Form_Element_Hidden('ApplicationDate');
		$ApplicationDate->setAttrib('dojoType', "dijit.form.TextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$FileName = new Zend_Form_Element_File('FileName');
		$FileName->setAttrib('dojoType', "dijit.form.TextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$DocumentType = new Zend_Form_Element_Select('DocumentType');
		$DocumentType->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$NameOfFather = new Zend_Form_Element_Text('NameOfFather');
		$NameOfFather->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '100')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$NameOfMother = new Zend_Form_Element_Text('NameOfMother');
		$NameOfMother->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '100')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$CircOfFather = new Zend_Form_Element_Radio('CircOfFather');
		$CircOfFather->addMultiOptions(array (
				'1' => 'Alive',
				'0' => 'Deceased'
		))->setvalue('1')->setSeparator('&nbsp;')->setAttrib('dojoType', "dijit.form.RadioButton")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CircOfMother = new Zend_Form_Element_Radio('CircOfMother');
		$CircOfMother->addMultiOptions(array (
				'1' => 'Alive',
				'0' => 'Deceased'
		))->setvalue('1')->setSeparator('&nbsp;')->setAttrib('dojoType', "dijit.form.RadioButton")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$ParentAddressDetails = new Zend_Form_Element_Text('ParentAddressDetails');
		$ParentAddressDetails->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '255')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('propercase',"true");

		$PostCode = new Zend_Form_Element_Text('PostCode');
		$PostCode->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$City = new Zend_Dojo_Form_Element_FilteringSelect('City');
		$City->removeDecorator("DtDdWrapper");
		$City->removeDecorator("Label");
		$City->removeDecorator('HtmlTag');
		$City->setAttrib('required', "true");
		$City->setRegisterInArrayValidator(false);
		$City->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Telephone = new Zend_Form_Element_Text('Telephone');
		$Telephone->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Telephone->setAttrib('maxlength', '20');
		$Telephone->removeDecorator("DtDdWrapper");
		$Telephone->removeDecorator("Label");
		$Telephone->removeDecorator('HtmlTag');

		$Handphone = new Zend_Form_Element_Text('Handphone');
		$Handphone->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Handphone->setAttrib('maxlength', '20');
		$Handphone->removeDecorator("DtDdWrapper");
		$Handphone->removeDecorator("Label");
		$Handphone->removeDecorator('HtmlTag');

		$PEmail = new Zend_Form_Element_Text('PEmail', array (
				'regExp' => "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",
				'invalidMessage' => "Not a valid email"
		));
		$PEmail->setAttrib('required', "true");
		$PEmail->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$PEmail->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$ParentJob = new Zend_Form_Element_Select('ParentJob');
		$ParentJob->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$ParentEducation = new Zend_Form_Element_Text('ParentEducation');
		$ParentEducation->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$ParentEducation->setAttrib('maxlength', '255');
		$ParentEducation->removeDecorator("DtDdWrapper");
		$ParentEducation->removeDecorator("Label");
		$ParentEducation->removeDecorator('HtmlTag');

		$Referrel = new Zend_Form_Element_Select('Referrel');
		$Referrel->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$JacketSize = new Zend_Form_Element_Select('JacketSize');
		$JacketSize->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$TypeOfSchool = new Zend_Form_Element_Select('TypeOfSchool');
		$TypeOfSchool->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$StatusOfSchool = new Zend_Form_Element_Radio('StatusOfSchool');
		$StatusOfSchool->addMultiOptions(array (
				'1' => 'Public',
				'0' => 'Private'
		))->setvalue('1')->setSeparator('&nbsp;')->setAttrib('dojoType', "dijit.form.RadioButton")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$TypeOfStud = new Zend_Form_Element_Checkbox('TypeOfStud');
		$TypeOfStud->setAttrib('dojoType', "dijit.form.CheckBox");
		$TypeOfStud->setAttrib('onClick', 'toggletextdropdown(this.checked)');
		$TypeOfStud->setAttrib('readonly', "true")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$HomeTownSchoolDD = new Zend_Form_Element_Select('HomeTownSchoolDD');
		$HomeTownSchoolDD->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$HomeTownSchoolTB = new Zend_Form_Element_Select('HomeTownSchoolTB');
		$HomeTownSchoolTB->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomeTownSchoolTB->setAttrib('maxlength', '50');
		$HomeTownSchoolTB->removeDecorator("DtDdWrapper");
		$HomeTownSchoolTB->removeDecorator("Label");
		$HomeTownSchoolTB->removeDecorator('HtmlTag');

		$CreditTransferFromDD = new Zend_Form_Element_Select('CreditTransferFromDD');
		$CreditTransferFromDD->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CreditTransferFromTB = new Zend_Form_Element_Select('CreditTransferFromTB');
		$CreditTransferFromTB->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$CreditTransferFromTB->setAttrib('maxlength', '50');
		$CreditTransferFromTB->removeDecorator("DtDdWrapper");
		$CreditTransferFromTB->removeDecorator("Label");
		$CreditTransferFromTB->removeDecorator('HtmlTag');

		$LocalPlacementTest = new Zend_Form_Element_Checkbox('LocalPlacementTest');
		$LocalPlacementTest->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$LocalCertification = new Zend_Form_Element_Checkbox('LocalCertification');
		$LocalCertification->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$InternationalPlacementTest = new Zend_Form_Element_Checkbox('InternationalPlacementTest');
		$InternationalPlacementTest->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$InternationalCertification = new Zend_Form_Element_Checkbox('InternationalCertification');
		$InternationalCertification->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Completionofyesr = new Zend_Form_Element_Checkbox('Completionofyesr');
		$Completionofyesr->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$SubjectId = new Zend_Form_Element_Select('SubjectId');
		$SubjectId->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->setAttrib('onChange', 'fnGetSubjectCode(this.value)')->removeDecorator("Label")->removeDecorator('HtmlTag');

		$SubjectMark = new Zend_Form_Element_Select('SubjectMark');
		$SubjectMark->setAttrib('dojoType', "dijit.form.NumberTextBox");
		$SubjectMark->setAttrib('maxlength', '3');
		$SubjectMark->removeDecorator("DtDdWrapper");
		$SubjectMark->removeDecorator("Label");
		$SubjectMark->removeDecorator('HtmlTag');

		$ProgCheckListName = new Zend_Form_Element_Select('ProgCheckListName');
		$ProgCheckListName->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$countrycode = new Zend_Form_Element_Text('countrycode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$countrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$countrycode->setAttrib('maxlength', '4');
		$countrycode->setAttrib('style', 'width:50px');
		$countrycode->removeDecorator("DtDdWrapper");
		$countrycode->removeDecorator("Label");
		$countrycode->removeDecorator('HtmlTag');

		$ContactNumber = new Zend_Form_Element_Text('ContactNumber', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$ContactNumber->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$ContactNumber->setAttrib('required', "true");
		$ContactNumber->setAttrib('maxlength', '20');
		$ContactNumber->setAttrib('style', 'width:109px');
		$ContactNumber->removeDecorator("DtDdWrapper");
		$ContactNumber->removeDecorator("Label");
		$ContactNumber->removeDecorator('HtmlTag');

		$AwardLevel = new Zend_Dojo_Form_Element_FilteringSelect('AwardLevel');
		$AwardLevel->setAttrib('required', "true");
		$AwardLevel->removeDecorator("DtDdWrapper");
		$AwardLevel->removeDecorator("Label");
		$AwardLevel->removeDecorator('HtmlTag');
		$AwardLevel->setAttrib('dojoType', "dijit.form.FilteringSelect");

		//$applicantmodelObj = new App_Model_Applicant();
		//$IntakeList = $applicantmodelObj->getIntake();
		$Intake = new Zend_Dojo_Form_Element_FilteringSelect('Intake');
		$Intake->setAttrib('required', "true");
		$Intake->removeDecorator("DtDdWrapper");
		//$Intake->setAttrib('OnChange', "getProgramLevel(this.value,this.id)");
		$Intake->removeDecorator("Label");
		$Intake->removeDecorator('HtmlTag');
		$Intake->setAttrib('dojoType', "dijit.form.FilteringSelect");
		//$Intake->addMultiOptions($IntakeList);

		$awardLevelObj = new App_Model_Applicant();
		$awardList = $awardLevelObj->fnGetDefination('Award');
		$IdProgramLevel = new Zend_Dojo_Form_Element_FilteringSelect('IdProgramLevel');
		$IdProgramLevel->setAttrib('required', "true");
		$IdProgramLevel->removeDecorator("DtDdWrapper");
		$IdProgramLevel->setAttrib('OnChange', "getProgram(this.value,this.id)");
		$IdProgramLevel->removeDecorator("Label");
		$IdProgramLevel->removeDecorator('HtmlTag');
		$IdProgramLevel->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$IdProgramLevel->addMultiOptions($awardList);

		$IdProgram = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
		$IdProgram->setAttrib('required', "true");
		$IdProgram->removeDecorator("DtDdWrapper");
		$IdProgram->setAttrib('OnChange', "getBranch(this.value,this.id)");
		$IdProgram->removeDecorator("Label");
		$IdProgram->removeDecorator('HtmlTag');
		$IdProgram->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$IdBranch = new Zend_Dojo_Form_Element_FilteringSelect('IdBranch');
		$IdBranch->setAttrib('required', "true");
		$IdBranch->removeDecorator("DtDdWrapper");
		//$IdBranch->setAttrib('OnChange', "getScheme(this.value,this.id)");
		$IdBranch->removeDecorator("Label");
		$IdBranch->removeDecorator('HtmlTag');
		$IdBranch->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$IdScheme = new Zend_Dojo_Form_Element_FilteringSelect('IdScheme');
		$IdScheme->setAttrib('required', "true");
		$IdScheme->removeDecorator("DtDdWrapper");
		//$IdScheme->setAttrib('OnChange', "getProgramLevel(this.value,this.id)");
		$IdScheme->removeDecorator("Label");
		$IdScheme->removeDecorator('HtmlTag');
		$IdScheme->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$lobjsubject = new Application_Model_DbTable_Subjectsetup();
		$lobjsubjectgrade = new Application_Model_DbTable_Subjectgradetype();
		$larrsubjectlist = $lobjsubject->fngetsubjectList();
		$larrsubjectgradelist = $lobjsubjectgrade->fngetsubjectgradeList();

		$Subject = new Zend_Dojo_Form_Element_FilteringSelect('Subject');
		$Subject->removeDecorator("DtDdWrapper");
		$Subject->setAttrib('required', "true");
		$Subject->removeDecorator("Label");
		$Subject->removeDecorator('HtmlTag');
		$Subject->setRegisterInArrayValidator(false);
		$Subject->setAttrib('dojoType', "dijit.form.FilteringSelect");
		//$Subject->addMultiOptions($larrsubjectlist);

		$SubjectGrade = new Zend_Dojo_Form_Element_FilteringSelect('SubjectGrade');
		$SubjectGrade->removeDecorator("DtDdWrapper");
		$SubjectGrade->setAttrib('required', "true");
		$SubjectGrade->removeDecorator("Label");
		$SubjectGrade->removeDecorator('HtmlTag');
		$SubjectGrade->setRegisterInArrayValidator(false);
		$SubjectGrade->setAttrib('dojoType', "dijit.form.FilteringSelect");
		//$SubjectGrade->addMultiOptions($larrsubjectgradelist);

		$lobjqualification = new Application_Model_DbTable_Qualificationsetup();
		$larrqualificationlist = $lobjqualification->fngetQualificationList();
		$QualificationLevel = new Zend_Dojo_Form_Element_FilteringSelect('QualificationLevel');
		$QualificationLevel->removeDecorator("DtDdWrapper");
		$QualificationLevel->setAttrib('required', "true");
		$QualificationLevel->removeDecorator("Label");
		$QualificationLevel->removeDecorator('HtmlTag');
		$QualificationLevel->setRegisterInArrayValidator(false);
		$QualificationLevel->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$QualificationLevel->addMultiOptions($larrqualificationlist);

		$lobjspecialization = new Application_Model_DbTable_Specialization();
		$larrspecializationlist = $lobjspecialization->fngetSpecialization();
		//Code to bring General Label up in specialization
		$i = 0;
		foreach($larrspecializationlist as $certificatelist){
			if(strtolower($certificatelist['value']) == "general"){
				$larrlist[0]['key'] = $certificatelist['key'];
				$larrlist[0]['value'] = $certificatelist['value'];
				unset($larrspecializationlist[$i]);
			}
			$i++;
		}
		$larrspecializationlist = array_merge($larrlist,$larrspecializationlist);
		$IdSpecialization = new Zend_Dojo_Form_Element_FilteringSelect('IdSpecialization');
		$IdSpecialization->removeDecorator("DtDdWrapper");
		$IdSpecialization->setAttrib('required', "false");
		$IdSpecialization->removeDecorator("Label");
		$IdSpecialization->removeDecorator('HtmlTag');
		$IdSpecialization->setRegisterInArrayValidator(false);
		$IdSpecialization->setAttrib('onchange', "toggleSP(this.value)");
		$IdSpecialization->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$IdSpecialization->addMultiOptions($larrspecializationlist);
		$IdSpecialization->addMultiOptions(array('0'=>'Others'));





		$lobjinstitution = new Application_Model_DbTable_Institutionsetup();
		$larrinstitutionlist = $lobjinstitution->fnGetInstitutionList();
		$Institution = new Zend_Dojo_Form_Element_ComboBox('Institution');
		$Institution->removeDecorator("DtDdWrapper");
		$Institution->setAttrib('required', "true");
		//$Institution->setAttrib('onChange',"loadInstitutionAddress()") ;
		$Institution->removeDecorator("Label");
		$Institution->removeDecorator('HtmlTag');
		$Institution->setAutocomplete(true);
		$Institution->setAttrib('OnChange', "getInstituteCountry(this.value)");
		$Institution->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$Institution->addMultiOptions($larrinstitutionlist);
		$Institution->addMultiOptions(array('0'=>'Others'));

		$YearGraduated = new Zend_Form_Element_Textarea('YearGraduated', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Numbers Only"
		));
		$YearGraduated->setAttrib('invalidMessage', 'Please enter atleast 4 digits');
		$YearGraduated->setAttrib('constraints', '{min:1900,max:' . $year . ',pattern:"####"}');
		$YearGraduated->removeDecorator("DtDdWrapper");
		$YearGraduated->setAttrib('required', "true");
		$YearGraduated->setAttrib('maxlength', "4");
		$YearGraduated->removeDecorator("Label");
		$YearGraduated->removeDecorator('HtmlTag');
		$YearGraduated->setAttrib('dojoType', "dijit.form.NumberTextBox");

		$Result = new Zend_Form_Element_Text('Result');
		$Result->setAttrib('required', "false")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$lobjaward = new GeneralSetup_Model_DbTable_Awardlevel();
		$larrprogramentrylist = $lobjaward->fnGetDefinations("Result Item");
		$ResultItem = new Zend_Dojo_Form_Element_FilteringSelect('ResultItem');
		$ResultItem->removeDecorator("DtDdWrapper");
		$ResultItem->setAttrib('required', "false");
		$ResultItem->removeDecorator("Label");
		$ResultItem->removeDecorator('HtmlTag');
		$ResultItem->setRegisterInArrayValidator(false);
		$ResultItem->setAttrib('dojoType', "dijit.form.FilteringSelect");
		foreach ($larrprogramentrylist as $larrdefmsresult) {
			$ResultItem->addMultiOption($larrdefmsresult['idDefinition'], $larrdefmsresult['DefinitionDesc']);
		}

		$Country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
		$Country->removeDecorator("DtDdWrapper");
		$Country->setAttrib('required', "true");
		$Country->removeDecorator("Label");
		$Country->removeDecorator('HtmlTag');
		$Country->setRegisterInArrayValidator(false);
		$Country->setAttrib('OnChange', "fnGetCountryStateList(this,'Province')");
		$Country->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$Country->addMultiOptions($larrcountrylist);

		$Province = new Zend_Dojo_Form_Element_FilteringSelect('Province');
		$Province->removeDecorator("DtDdWrapper");
		$Province->setAttrib('required', "false");
		$Province->removeDecorator("Label");
		$Province->removeDecorator('HtmlTag');
		$Province->setRegisterInArrayValidator(false);
		$Province->setAttrib('OnChange', 'fnGetStateCityList');
		$Province->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$InstCity = new Zend_Dojo_Form_Element_FilteringSelect('InstCity');
		$InstCity->removeDecorator("DtDdWrapper");
		$InstCity->setAttrib('required', "false");
		$InstCity->removeDecorator("Label");
		$InstCity->removeDecorator('HtmlTag');
		$InstCity->setRegisterInArrayValidator(false);
		$InstCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$showOtherSchool = new Zend_Form_Element_Text('showOtherSchool');
		$showOtherSchool->removeDecorator("DtDdWrapper");
		$showOtherSchool->setAttrib('required', "false");
		$showOtherSchool->removeDecorator("Label");
		$showOtherSchool->removeDecorator('HtmlTag');
		$showOtherSchool->setAttrib('dojoType', "dijit.form.ValidationTextBox");

		$showOS = new Zend_Form_Element_Text('showOS');
		$showOS->removeDecorator("DtDdWrapper");
		$showOS->setAttrib('required', "false");
		$showOS->setValue('General');
		$showOS->removeDecorator("Label");
		$showOS->removeDecorator('HtmlTag');
		$showOS->setAttrib('dojoType', "dijit.form.ValidationTextBox");


		//$Province->addMultiOptions($larrstatelist);



		$this->addElements(array (
				$Country,
				$Province,
				$InstCity,
				$showOtherSchool,
				$showOS,
				$ThirdName,
				$AwardLevel,
				$countrycode,
				$ContactNumber,
				$Email,
				$PersonalID,
				$IssueDate,
				$ExpiryDate,
				$IssuePlace,
				$IdType,
				$FullArabicName,
				$FullName,
				$FourthName,
				$LocalPlacementTest,
				$LocalCertification,
				$InternationalPlacementTest,
				$InternationalCertification,
				$SubjectId,
				$SubjectMark,
				$StudentId,
				$StudentCollegeId,
				$FName,
				$MName,
				$Completionofyesr,
				$LName,
				$NameField1,
				$NameField2,
				$NameField3,
				$NameField4,
				$NameField5,
				$DateOfBirth,
				$Gender,
				$MaritalStatus,
				$NoOfChildren,
				$Nationality,
				$PermAddressDetails,
				$PermCity,
				$PermState,
				$PermCountry,
				$PermZip,
				$CorrsAddressDetails,
				$CorrsCity,
				$CorrsState,
				$CorrsCountry,
				$CorrsZip,
				$HomePhonecountrycode,
				$HomePhonestatecode,
				$HomePhone,
				$CellPhonecountrycode,
				$CellPhone,
				$Faxcountrycode,
				$Faxstatecode,
				$Fax,
				$EmailAddress,
				$idCollege,
				$idsponsor,
				$InternationalStd,
				$PPNos,
				$PPIssueDt,
				$PPExpDt,
				$PPField1,
				$PPField2,
				$PPField3,
				$PPField4,
				$PPField5,
				$VisaDetailField1,
				$VisaDetailField2,
				$VisaDetailField3,
				$VisaDetailField4,
				$VisaDetailField5,
				$Update,
				$UpdUser,
				$Active,
				$hobbies,
				$othercurricularactivity,
				$PreffredHostel,
				$PreferredBlock,
				$PreffredFloor,
				$PreffredroomType,
				$PreffredBed,
				$bedAllotedto,
				$RoomPrefredFrom,
				$RoomPrefredto,
				$currentjobfromdate,
				$currentjobtodate,
				$currentjobtitle,
				$currentjoborganizationtype,
				$currentjobemployer,
				$currentjobemployeraddress,
				$currentjobsalary,
				$IDCourse,
				$IdApplication,
				$Save,
				$Close,
				$Add,
				$InstitutionName,
				$InstitutionNameSelect,
				$UniversityName,
				$StudyPlace,
				$MajorFiledOfStudy,
				$YearOfStudyFrom,
				$YearOfStudyTo,
				$DegreeType,
				$GradeOrCGPA,
				$ICNumber,
				$Registered,
				$Offered,
				$Accepted,
				$ApplicationType,
				$IdPlacementtest,
				$IdStudEduDtl,
				$FileName,
				$DocumentType,
				$ApplicationDate,
				$PlaceOfBirth,
				$Religion,
				$BloodGroup,
				$TypeOfResidence,
				$SameCorrespAddr,
				$NameOfFather,
				$NameOfMother,
				$CircOfFather,
				$CircOfMother,
				$ParentAddressDetails,
				$PostCode,
				$City,
				$Telephone,
				$Handphone,
				$ParentJob,
				$ParentEducation,
				$Referrel,
				$JacketSize,
				$TypeOfSchool,
				$StatusOfSchool,
				$HomeTownSchoolDD,
				$HomeTownSchoolTB,
				$CreditTransferFromDD,
				$CreditTransferFromTB,
				$TypeOfStud,
				$PEmail,
				$ProgCheckListName,
				$TypeofId,
				$ConfirmDeclaration,
				$IdNo,
				$Race,
				$SpecialTreatment,
				$SpecialTreatmentType,
				$OutcampusAddressDetails,
				$OutcampusCity,
				$OutcampusState,
				$OutcampusCountry,
				$OutcampusZip,
				$SameOutcampusAddr,
				$Intake,
				$IdProgramLevel,
				$IdScheme,
				$IdBranch,
				$IdProgram,
				$Subject,
				$SubjectGrade,
				$QualificationLevel,
				$Institution,
				$YearGraduated,
				$ResultItem,
				$Result,$IdSpecialization,
				$PermAddressDetails2,
				$CorrsAddressDetails2,
				$OutcampusAddressDetails2,
				$ExtraIdField1,
				$ExtraIdField2,
				$ExtraIdField3,
				$ExtraIdField4,
				$ExtraIdField5,
				$ExtraIdField6,
				$ExtraIdField7,
				$ExtraIdField8,
				$ExtraIdField9,
				$ExtraIdField10
		));
	}

}
?>