<?php 

class Examination_Model_DbTable_ExamCenterSetup extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_center_setup';
	protected $_primary = "ecs_id";
	
	
	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($where){
		 $this->delete( $where );
	}
	
}

?>