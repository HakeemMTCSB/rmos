<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 15/12/2015
 * Time: 4:15 PM
 */
class Examination_Model_DbTable_TerStatus extends Zend_Db_Table {

    public function getTerCompleteStatus($student,$semester){

        $ter_completed = 0; //completed

        //get total evaluation form to be submmited
        $listEvaluation = $this->getListEvaluation($student['IdBranch'],$student['IdProgram'],$student['IdProgramScheme'],$semester["IdSemesterMaster"]);

        $jumlah_pool = 0;
        if(count($listEvaluation)>0){
            foreach($listEvaluation as $key=>$sect){
                if($sect['type']==1){//by course
                    if ($sect['target_user']==2){
                        $courses = $this->getCourseRegisteredBySemesterEvaluation($student['IdStudentRegistration'],$semester["IdSemesterMaster"]);

                        if ($courses){
                            foreach ($courses as $course){
                                $lecturerList = $this->getLecturer($course['IdCourseTaggingGroup']);
                                $jumlah_pool = $jumlah_pool+count($lecturerList);
                            }
                        }
                    }else{
                        $courses = $this->getCourseRegisteredBySemesterEvaluation($student['IdStudentRegistration'],$semester["IdSemesterMaster"]);
                        $jumlah_pool = $jumlah_pool+count($courses);
                    }
                }
            }
        }

        $total_terform_submitted = $this->getTotalAnsweredByStudent($student['IdStudentRegistration'],$semester['IdSemesterMaster']);

        //echo '-->'.$total_terform_submitted.'<'.$jumlah_pool;
        //checking submission form
        if($total_terform_submitted < $jumlah_pool){
            //ter evaluation not completed. alert student to fill in the form
            $ter_completed = 1;
        }

        return $ter_completed;
    }

    public function getListEvaluation($branch_id, $program_id, $IdProgramScheme, $semester_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'q020_sectiontagging'))
            ->join(array('b' => 'q001_pool'), 'b.id = a.question_pool',array('type','target_user','name'))
            ->where('branch_id =?', $branch_id)
            ->where('program_id=?', $program_id)
            ->where('IdProgramScheme=?', $IdProgramScheme)
            ->where('semester_id =?', $semester_id)
        ;

        $question = $db->fetchAll($select);

        return($question);
    }

    public function getCourseRegisteredBySemesterEvaluation($registrationId,$idSemesterMain,$idBlock=null){

        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->joinLeft(array('cg'=>'tbl_course_tagging_group'),'cg.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array())
            ->joinLeft(array('u'=>'tbl_staffmaster'),'u.IdStaff = cg.IdLecturer ',array('lecturername'=>'FullName','IdStaff'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            //->where("srs.subjectlandscapetype != 2")
            ->where('srs.IdSemesterMain = ?',$idSemesterMain)
            ->where('srs.Active NOT IN (3)')
//			->where('sm.CourseType NOT IN (3,19,2)') // coursetype GD & PPP & articleship
            ->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
            ->where("IFNULL(srs.exam_status,'x') NOT IN ('CT','EX','U')");

        if(isset($idBlock) && $idBlock != ''){ //Block
            $sql->where("srs.IdBlock = ?",$idBlock);
            $sql->order("srs.BlockLevel");
        }

        $sql2 = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->joinLeft(array('cg'=>'tbl_course_tagging_group'),'cg.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array())
            ->joinLeft(array('u'=>'tbl_staffmaster'),'u.IdStaff = cg.IdLecturer ',array('lecturername'=>'FullName','IdStaff'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            //->where("srs.subjectlandscapetype != 2")
            ->where('srs.audit_program IS NOT NULL')
            ->where('srs.IdSemesterMain = ?',$idSemesterMain)
            ->where('srs.Active NOT IN (3)')
//			->where('sm.CourseType NOT IN (3,19,2)') // coursetype GD & PPP & articleship
            ->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
            ->where("IFNULL(srs.exam_status,'x') IN ('U')");

        if(isset($idBlock) && $idBlock != ''){ //Block
            $sql2->where("srs.IdBlock = ?",$idBlock);
            $sql2->order("srs.BlockLevel");
        }

        $select = $db->select()
            ->union(array($sql, $sql2));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTotalAnsweredByStudent($student_id,$semester_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'q010_student_answer_main'))
            ->where('semester_id =?', $semester_id)
            ->where('student_id = ?',$student_id)
            ->where('status = ?',1);
        $question = $db->fetchAll($select);
        return count($question);
    }

    public function getLecturer($group){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array(
                'lecturerId'=>'a.IdLecturer',
                'lecturername'=>'b.FullName'
            ))
            ->join(array('b'=>'tbl_staffmaster'), 'b.IdStaff = a.IdLecturer', array())
            ->where('a.IdCourseTaggingGroup = ?', $group);

        $select2 = $db->select()
            ->from(array('a'=>'course_group_schedule'), array(
                'lecturerId'=>'a.IdLecturer',
                'lecturername'=>'b.FullName'
            ))
            ->join(array('b'=>'tbl_staffmaster'), 'b.IdStaff = a.IdLecturer', array())
            ->where('a.idGroup = ?', $group);

        $select3 = $db->select()
            ->from(array('a'=>'tbl_schedule_details'), array(
                'lecturerId'=>'a.tsd_lecturer',
                'lecturername'=>'b.FullName'
            ))
            ->join(array('b'=>'tbl_staffmaster'), 'b.IdStaff = a.tsd_lecturer', array())
            ->where('a.tsd_groupid = ?', $group);

        $select = $db->select()
            ->union(array($select1, $select2, $select3))
            ->group('lecturerId');

        $result = $db->fetchAll($select);
        return $result;
    }
}