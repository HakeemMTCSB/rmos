<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/9/2015
 * Time: 4:12 PM
 */
class Examination_Model_DbTable_ExamCalendar extends Zend_Db_Table_Abstract {

    public function getExamCalendarList($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_calendar'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.ec_idSemester = b.IdSemesterMaster')
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.ec_idActivity = c.idDefinition');

        if ($search != false){
            if (isset($search['ec_idSemester']) && $search['ec_idSemester']!=''){
                $select->where('a.ec_idSemester = ?', $search['ec_idSemester']);
            }
            if (isset($search['ec_idActivity']) && $search['ec_idActivity']!=''){
                $select->where('a.ec_idActivity = ?', $search['ec_idActivity']);
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExamCalendarInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_calendar'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.ec_idSemester = b.IdSemesterMaster')
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.ec_idActivity = c.idDefinition')
            ->where('a.ec_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSemesterList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.display = ?', 1);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkDuplicateEntry($semid, $actid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_calendar'), array('value'=>'*'))
            ->where('a.ec_idSemester = ?', $semid)
            ->where('a.ec_idActivity = ?', $actid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertCalendarActivity($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('exam_calendar', $data);
        $id = $db->lastInsertId('exam_calendar', 'ec_id');
        return $id;
    }

    public function updateCalendarActivity($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('exam_calendar', $data, 'ec_id = '.$id);
        return $update;
    }

    public function deleteCalendarActivity($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('exam_calendar', 'ec_id = '.$id);
        return $delete;
    }
}