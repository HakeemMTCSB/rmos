<?php
class Examination_Model_DbTable_StudentMarkEntry extends Zend_Db_Table { //Model Class for Users Details
	
	protected $_name = 'tbl_student_marks_entry';
	protected $_primary = 'IdStudentMarksEntry';

	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
    public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function updateData($data,$id){
		//dd($data); exit;
		 $this->update($data, "IdStudentMarksEntry = '".(int)$id."'");
	}
	
	
	public function checkMarkEntry($IdStudentRegistration,$IdMarksDistributionMaster,$IdStudentRegSubjects,$idSemester){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
			
		 //check ada x mark entry sebelum ni
	 	 $select_mark = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
	 	 				  ->where('sme.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
	 	 				  ->where('sme.IdStudentRegSubjects = ?',$IdStudentRegSubjects)
	 	 				  ->where('sme.IdSemester = ?',$idSemester);
	 	 				  
	     return $entry_list = $db->fetchRow($select_mark);	
	}
	
	public function getMark($IdStudentRegistration,$IdMarksDistributionMaster,$IdStudentRegSubjects,$idSemester){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
			
		 //check ada x mark entry sebelum ni
	 	 $select_mark = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
	 	 				  ->joinLeft(array('s'=>'tbl_staffmaster'),'s.IdStaff=sme.ApprovedBy',array('ApprovedBy'=>'FullName'))
	 	 				  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sme.MarksEntryStatus',array('BahasaIndonesia','DefinitionDesc'))
	 	 				  ->where('sme.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
	 	 				  ->where('sme.IdStudentRegSubjects = ?',$IdStudentRegSubjects)
	 	 				  ->where('sme.IdSemester = ?',$idSemester);
	 	 				  
	     return $entry_list = $db->fetchRow($select_mark);	
	}
	
	
	public function getItemMark($IdStudentMarksEntry,$itemId){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
			
		 //check ada x mark entry sebelum ni
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_detail_marks_entry'))
	 	 				  ->where('sme.IdStudentMarksEntry = ?',$IdStudentMarksEntry)
	 	 				  ->where('sme.ComponentDetail = ?',$itemId);
	 	 				  
	 	 return $row = $db->fetchRow($select);	
	}
	
	
	public function getStudentTotalMarkOri($semester_id,$program_id,$subject_id,$IdStudentRegistration,$IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$sql =  $db->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array('IdMarksDistributionMaster'))							
							    ->where("mdm.semester = ?",$semester_id)
								->where("mdm.IdProgram = ?",$program_id)
								->where("mdm.IdCourse = ?",$subject_id);
		
		 
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_marks_entry'),array('FinalTotalMarkObtained'))
	 	 				  ->where('sme.IdMarksDistributionMaster IN (?)',$sql)
	 	 				  ->where('sme.IdStudentRegistration = ?',$IdStudentRegistration);
		
		 $rows = $db->fetchAll($select);	
		 
		
		 if(is_array($rows)){
		 	 $grand_total_mark=0;
			 foreach($rows as $row){		 	
			 	$grand_total_mark = abs($grand_total_mark) + abs($row["FinalTotalMarkObtained"]);
			 }
			 
			 //mappkan markah dgn grade
			 $cms_calculation = new Cms_ExamCalculation();
		     $grade	= $cms_calculation->getGradeInfo($semester_id,$program_id,$subject_id,$grand_total_mark);			
			 
		     if(isset($grade["Pass"])){
			     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
			     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
		     	 else $grade_status='';
		     }else{
		     	 $grade_status='';
		     }
			 
			 $data["final_course_mark"]=$grand_total_mark;
			 $data["grade_point"]=$grade["GradePoint"];
			 $data["grade_name"]=$grade["GradeName"];
			 $data["grade_desc"]=$grade["GradeDesc"];
			 $data["grade_status"]=$grade_status; //status should consider on mark distribution component grade status for now belum checking lagi just amik status di grade setup (22/11/2013)

			
			 //save dalam studentregsubject
			 $subRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			 $subRegDB->updateData($data,$IdStudentRegSubjects);
		 }
	}
	
	
	public function getComponentMark($IdStudentRegistration,$IdMarksDistributionMaster,$idSemester,$idCourse){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select_mark = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
	 	 				  ->where('sme.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
	 	 				  ->where('sme.Course = ?',$idCourse)
	 	 				  ->where('sme.IdSemester = ?',$idSemester);
	 	 		  
	     return $entry_list = $db->fetchRow($select_mark);	
	}
	
	public function getStudentTotalMark($semester_id,$program_id,$subject_id,$IdStudentRegistration,$IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$sql =  $db->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array('IdMarksDistributionMaster'))							
							    ->where("mdm.semester = ?",$semester_id)
								->where("mdm.IdProgram = ?",$program_id)
								->where("mdm.IdCourse = ?",$subject_id);
		
		 $mdm = $db->fetchAll($sql);
		 
		 $grand_total_mark = null;
		 foreach ($mdm as $m){
 			$select = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_marks_entry'),array('FinalTotalMarkObtained','IdStudentMarksEntry'))
	 	 				  ->where('sme.IdMarksDistributionMaster = ?',$m["IdMarksDistributionMaster"])
	 	 				  ->where('sme.IdStudentRegSubjects = ?',$IdStudentRegSubjects);
			$dmark = $db->fetchRow($select);	
			
			if(is_array($dmark)){
				//delete mark yang sama distribution (overcome multiple mark for the same distribution)
				$sqld="Delete from tbl_student_marks_entry where 
						IdMarksDistributionMaster = ".$m["IdMarksDistributionMaster"]."
						and IdStudentRegSubjects = ".$IdStudentRegSubjects."
						and IdStudentMarksEntry != ".$dmark["IdStudentMarksEntry"];
				
				$db->query($sqld);
				$grand_total_mark = $grand_total_mark + $dmark["FinalTotalMarkObtained"];
			}
		 }
		 //mappkan markah dgn grade
		 if($grand_total_mark!=""){
			 $cms_calculation = new Cms_ExamCalculation();
		     $grade	= $cms_calculation->getGradeInfo($semester_id,$program_id,$subject_id,$grand_total_mark);			
			 
		     if(isset($grade["Pass"])){
			     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
			     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
		     	 else $grade_status='';
		     }else{
		     	 $grade_status='';
		     }
			 
			 $data["final_course_mark"]=$grand_total_mark;
			 $data["grade_point"]=$grade["GradePoint"];
			 $data["grade_name"]=$grade["GradeName"];
			 $data["grade_desc"]=$grade["GradeDesc"];
			 $data["grade_status"]=$grade_status; //status should consider on mark distribution component grade status for now belum checking lagi just amik status di grade setup (22/11/2013)
		 }else{
			 $data["final_course_mark"]=NULL;
			 $data["grade_point"]=NULL;
			 $data["grade_name"]="";
			 $data["grade_desc"]="";
			 $data["grade_status"]=""; //status should consider on mark distribution component grade status for now belum checking lagi just amik status di grade setup (22/11/2013)		 	
		 }
		
		 //save dalam studentregsubject
		 $subRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
		 $subRegDB->updateData($data,$IdStudentRegSubjects); 
		 
		 return $grand_total_mark;	 
	}
	
	public function saveStudentSubjectMark($IdStudentRegSubjects,$semester_id,$program_id,$subject_id,$grand_total_mark=null){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $message = '';
	 	//mappkan markah dgn grade
		 if($grand_total_mark!=""){
		 	
			 	try{
	    			
			 		 $cms_calculation = new Cms_ExamCalculation();
				     $grade	= $cms_calculation->getGradeInfo($semester_id,$program_id,$subject_id,$grand_total_mark);					    
					 
				     if(isset($grade["Pass"])){
					     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
					     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
				     	 else $grade_status='';
				     }else{
				     	 $grade_status='';
				     }
				 
					 $data["final_course_mark"]=$grand_total_mark;
					 $data["grade_point"]=$grade["GradePoint"];
					 $data["grade_name"]=$grade["GradeName"];
					 $data["grade_desc"]=$grade["GradeDesc"];
					 $data["grade_status"]=$grade_status; 
					
					 
	    		}catch (Exception $e) {
	    			
	   				// echo 'Caught exception: ',  $e->getMessage(), "\n";
	   				 $data["final_course_mark"]=NULL;
					 $data["grade_point"]=NULL;
					 $data["grade_name"]="";
					 $data["grade_desc"]="";
					 $data["grade_status"]=""; 
					 $message = $e->getMessage();
				}
				
			
			 
		 }else{
			 $data["final_course_mark"]=NULL;
			 $data["grade_point"]=NULL;
			 $data["grade_name"]="";
			 $data["grade_desc"]="";
			 $data["grade_status"]=""; 	
			 	
		 }
		 
		 
		 //save dalam studentregsubject
		 $subRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
		 $subRegDB->updateData($data,$IdStudentRegSubjects); 
		 
		 $data['message'] = $message;
		 return $data;
		
		
	}
	
	
	public function saveStudentSubjectMarkCe($IdStudentRegSubjects,$markObtained=''){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
					
    	$markDB = new Examination_Model_DbTable_StudentMarkEntry();		
    	$component_mark_entry = $markDB->getStudentMarkAndPassMark($IdStudentRegSubjects);
    		     	
    		
	    	$pass = true;
	    	
	    	if(count($component_mark_entry)>0){
	    		
		    	foreach($component_mark_entry as $component){   	    		
		    		if( round($component['FinalTotalMarkObtained']) < $component['ce_pass_mark']){
		    			$pass = false;
		    		}
		    	}    	
		    	
		    	if($pass === false){
		    			    		
		    		$data["final_course_mark"]=NULL;
		    		$data["grade_point"]=NULL;
		    		$data["grade_name"]="NP";
		    		$data["grade_desc"]="Fail";
		    		$data["grade_status"]="Fail"; 
		    		$data["grade_id"]=778;
		    		
		    	}else{
		    		
		    		$data["final_course_mark"]=NULL;
				 	$data["grade_point"]=NULL;
				 	$data["grade_name"]="P";
				 	$data["grade_desc"]="Pass";
			    	$data["grade_status"]="Pass"; 
			    	$data["grade_id"]=777;
		    	}
		    	
	    	}else{	    		
	    			$data["final_course_mark"]=NULL;
				 	$data["grade_point"]=NULL;
				 	$data["grade_name"]="";
				 	$data["grade_desc"]="";
			    	$data["grade_status"]=""; 
			    	$data["grade_id"]=NULL;
	    	}
	    	
    			
			 //save dalam studentregsubject
			 $subRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			 $subRegDB->updateData($data,$IdStudentRegSubjects); 
			 
			 return $data;
		
		
	}
	
	public function getMarkById($IdStudentMarksEntry){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>'tbl_student_detail_marks_entry'))
	 	 				  ->where('sme.IdStudentMarksEntry = ?',$IdStudentMarksEntry);	 	 		  
	     return $row = $db->fetchAll($select);	
	}
	
	
	public function getData($IdStudentMarksEntry){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>$this->_name))
	 	 				  ->where('sme.IdStudentMarksEntry = ?',$IdStudentMarksEntry);	 	 		  
	     return $row = $db->fetchRow($select);	
	}
	
	public function getDataByStudentRegSubjectId($IdStudentRegSubjects){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>$this->_name))
	 	 				  ->where('sme.IdStudentRegSubjects = ?',$IdStudentRegSubjects);	 	 		  
	     return $row = $db->fetchAll($select);	
	}
	
	public function copyHistory($IdStudentMarksEntry){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		
		$select = $db->select()
	 	 				  ->from(array('sme'=>$this->_name))
	 	 				  ->where('sme.IdStudentMarksEntry = ?',$IdStudentMarksEntry);
	 	$rows = $db->fetchAll($select);	
	 	
	 	foreach($rows as $row){
	 		
	 		$row['message'] = 'Exam Scaling';
		 	$row['createddt'] = date('Y-m-d H:i:s');
		 	$row['createdby'] = $auth->getIdentity()->iduser;
	 	
	 		$db->insert('tbl_student_marks_entry_history',$row);
	 	}
		
		
	}
	
	public function getAllMarkEntryByMainComponent($IdStudentRegSubjects,$idMaster){
				
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>$this->_name))
	 	 				  ->join(array('smed'=>'tbl_student_detail_marks_entry'),'smed.`IdStudentMarksEntry` = sme.`IdStudentMarksEntry`')
	 	 				  ->where('sme.IdStudentRegSubjects = ?',$IdStudentRegSubjects)
	 	 				  ->where('sme.IdMarksDistributionMaster = ?',$idMaster);	 	 		  
	     return $row = $db->fetchAll($select);	
	}
	
	public function getStudentMarkAndPassMark($IdStudentRegSubjects){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>$this->_name))
	 	 				  ->join(array('mdm'=>'tbl_marksdistributionmaster'),'mdm.IdMarksDistributionMaster = sme.IdMarksDistributionMaster',array('ce_pass_mark'))
	 	 				  ->where('sme.IdStudentRegSubjects = ?',$IdStudentRegSubjects);	 	 		  
	     return $row = $db->fetchAll($select);	
	}
	
	/*
	 * To check if lecturer has already do mark entry
	 */
	public function isEntered($IdMarksDistributionMaster){
		 $db = Zend_Db_Table::getDefaultAdapter();
		
	 	 $select = $db->select()
	 	 				  ->from(array('sme'=>$this->_name))
	 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster);	 	 		  
	     return $row = $db->fetchRow($select);	
	}
}
?>