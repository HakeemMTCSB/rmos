<?php 

class Examination_Model_DbTable_ExamGroup extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_group';
	protected $_primary = "eg_id";
	

	public function getData($idGroup){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
		->from(array('eg'=>$this->_name))
		->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=eg.eg_sub_id',array('subject_code'=>'SubCode','subject_name'=>'BahasaIndonesia','faculty_id'=>'IdFaculty'))
		->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=eg.eg_sem_id',array('semester_name'=>'SemesterMainName'))
		->joinLeft(array('r'=>'appl_room'),'r.av_id=eg.eg_room_id')
		->join(array('eat'=>'tbl_examination_assessment_type'), 'eat.IdExaminationAssessmentType = eg.eg_assessment_type', array('eg_exam_name'=>'DescriptionDefaultlang'))
		->where('eg.eg_id = ?',$idGroup);
		$row = $db->fetchRow($select);
		
		return $row;
	}
	
	public function getGroupList($idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('eg'=>$this->_name))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=eg.eg_sub_id',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage','faculty_id'=>'IdFaculty'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=eg.eg_sem_id',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('r'=>'appl_room'),'r.av_id=eg.eg_room_id')
					  ->join(array('eat'=>'tbl_examination_assessment_type'), 'eat.IdExaminationAssessmentType = eg.eg_assessment_type', array('eg_exam_name'=>'DescriptionDefaultlang'))
					  ->where('eg.eg_sub_id = ?',$idSubject)
					  ->where('eg.eg_sem_id = ?',$idSemester);
							  
		 $row = $db->fetchAll($select);
		 	
		 return $row;
	}
	
	public function getTotalGroupByCourse($idCourse,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("eg_sub_id = ?",$idCourse)
					  ->where('eg_sem_id = ?',$idSemester);					  
		 $row = $db->fetchAll($select);	
		 
		 if($row)
		 	return count($row);
		 else
		 return 0;
	}
	
	public function insert(array $data){
	
		if( !isset($data['eg_create_by']) ){
			$auth = $auth = Zend_Auth::getInstance();
				
			$data['eg_create_by'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['eg_create_date']) ){
			$data['eg_create_date'] = date('Y-m-d H:i:a');
		}
	
		return parent::insert($data);
	}
	
	public function getListGroup($formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('eg'=>$this->_name))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=eg.eg_sub_id',array('subject_code'=>'SubCode','subject_name'=>'SubjectName','faculty_id'=>'IdFaculty'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=eg.eg_sem_id',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('r'=>'appl_room'),'r.av_id=eg.eg_room_id')
					  ->join(array('eat'=>'tbl_examination_assessment_type'), 'eat.IdExaminationAssessmentType = eg.eg_assessment_type', array('eg_exam_name'=>'DescriptionDefaultlang'));
					  
					  if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
					  	 $select->where('eg.eg_sem_id = ?',$formData['IdSemester']);
					  }
					  
	 				  if(isset($formData['subject_code']) && $formData['subject_code']!=''){
					  	 $select->where('sm.SubCode LIKE ?','%'.$formData['subject_code'].'%');
					  }
					  
					  if(isset($formData['subject_name']) && $formData['subject_name']!=''){
					  	 $select->where('sm.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
					  }
					 
					 
							  
		 $row = $db->fetchAll($select);
		 	
		 return $row;
	}
	
}