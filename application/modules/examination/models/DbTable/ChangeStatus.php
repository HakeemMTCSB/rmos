<?php 

class Examination_Model_DbTable_ChangeStatus extends Zend_Db_Table_Abstract {
	
	protected $_name = 'change_status';
	protected $_primary = "cs_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
    public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function updateData($data,$id){
		
	  $this->update($data, "cs_id = '".(int)$id."'");
	}
	
}