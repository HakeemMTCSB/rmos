<?php 
class Examination_Model_DbTable_ExamSchedule extends Zend_Db_Table_Abstract
{
    protected $_name = 'exam_schedule';
	protected $_primary = "es_id";
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"es_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("es_id ='".$add_id."'");
	}
	
	
	public function getScheduleByExamCenter($formData){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name))
	                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=es.es_course',array('IdSubject','subjectMainDefaultLanguage','SubCode','SubjectName'))
	                ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=es.es_semester',array('IdSemesterMaster','SemesterMainName','SemesterMainDefaultLanguage'))
	                ->order('es.es_date desc');
	                

	                if(isset($formData['IdSemester']) && $formData['IdSemester']!= ''){
	    				$select->where('es_semester = ?',$formData['IdSemester']);
	                }	

	                if(isset($formData['IdSubject']) && $formData['IdSubject']!= ''){
	    				$select->where('es_course = ?',$formData['IdSubject']);
	                }	
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getScheduleBySubject($idSemester,$idSubject,$ec_id=0,$student_type=740){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name))
	                ->where('es.es_course = ?',$idSubject);
	    
		if($student_type==741){
			//VISITING NO NEED TO QUERY  by semester
			$select->where('es.es_semester IN (?)',$idSemester);
		}else{
			$select->where('es.es_semester IN (?)',$idSemester);
		}
		
	    if(isset($ec_id) && $ec_id!=0){           
	   	 	$select->where('es.es_exam_center = ?',$ec_id);
	    }
	     
        $row = $db->fetchRow($select);
        
        if(!$row){
        	
        	$select2 = $db->select()
	                ->from(array('es'=>$this->_name))  
	                ->where('es.es_course = ?',$idSubject);
	     
	        if($student_type==741){
				//VISITING NO NEED TO QUERY  by semester
				$select2->where('es.es_semester IN (?)',$idSemester);
			}else{
				$select2->where('es.es_semester IN (?)',$idSemester);
			}
		
       		$row2 = $db->fetchRow($select2);
       		$row = $row2;
        }
        
		return $row;
		
	}
	
	/*public function getScheduleBySubject($idSemester,$idSubject,$ec_id=0,$student_type=740){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name))	              
	                ->where('es.es_semester = ?',$idSemester)
	                ->where('es.es_course = ?',$idSubject);
	    
	    if(isset($ec_id) && $ec_id!=0){           
	   	 	$select->where('es.es_exam_center = ?',$ec_id);
	    }
	     
        $row = $db->fetchRow($select);
        
        if(!$row){
        	
        	$select2 = $db->select()
	                ->from(array('es'=>$this->_name))	              
	                ->where('es.es_semester = ?',$idSemester)
	                ->where('es.es_course = ?',$idSubject);
	     
       		$row2 = $db->fetchRow($select2);
       		$row = $row2;
        }
        
		return $row;
		
	}*/
	
}

?>