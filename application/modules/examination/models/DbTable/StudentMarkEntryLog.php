<?php
class Examination_Model_DbTable_StudentMarkEntryLog extends Zend_Db_Table_Abstract
{
	
	protected $_name = 'tbl_student_marks_entry_log';
	protected $_primary = 'log_id';

    function getData($where = array())
    {
        $db = getDB();

        $select = $db->select()
            ->from(array('a'=>$this->_name))
            ->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName');

        if ( !empty($where) )
        {
            foreach ( $where as $what => $val )
            {
                $select->where($what,$val);
            }
        }

        $select->order('a.log_id DESC');

        $results = $db->fetchAll($select);

        return $results;
    }
}