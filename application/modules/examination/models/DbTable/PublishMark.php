<?php
class Examination_Model_DbTable_PublishMark extends Zend_Db_Table { 
	
	protected $_name = 'tbl_publish_mark';
	protected $_primary = 'pm_id';
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getData($idProgram,$idSemester,$idSubject,$idGroup,$idMaster,$idDetail,$type) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select 	= $db->select()
								->from($this->_name)
								->where('pm_idProgram = ?',$idProgram)
								->where('pm_idSemester = ?',$idSemester)
								->where('pm_idSubject = ?',$idSubject)
								->where('pm_idGroup = ?',$idGroup)
								->where('IdMarksDistributionMaster = ?',$idMaster)
								->where('IdMarksDistributionDetails = ?',$idDetail)
								->where('pm_type = ?',$type);
		return $result = $db->fetchRow($select);
	}
	
	public function getPublishResult($idProgram,$idSemester,$category=0) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select 	= $db->select()
								->from($this->_name)
								->where('pm_idProgram = ?',$idProgram)
								->where('pm_idSemester = ?',$idSemester)								
								->where('pm_type = 2');
								
								if(isset($category) && $category!=0){
									$select->where('pm_category = ?',$category);
								}else{
									$select->where('pm_category = ?',0);
								}
		return $result = $db->fetchRow($select);
	}
	
	
	public function getPublishResultData($idProgram=null,$idSemester) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select 	= $db->select()
								->from(array('pm'=>$this->_name))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=pm.pm_idProgram',array('ProgramCode','ProgamName'=>'ProgramName'))
								->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=pm.pm_idSemester',array('SemesterName'=>'SemesterMainName'))
								->where('pm_idSemester = ?',$idSemester)								
								->where('pm_type = 2');
								
		if(isset($idProgram) && $idProgram!=''){
			$select->where('pm_idProgram = ?',$idProgram);
		}
		
		
		return $result = $db->fetchAll($select);
	}
	
	public function isPublish($idProgram,$idSemester){
		 $db = Zend_Db_Table::getDefaultAdapter();
		$select 	= $db->select()
								->from(array('a'=>$this->_name))								
								->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=a.pm_idSemester',array('IdSemesterMaster','SemesterMainName'))
								->where('pm_idProgram = ?',$idProgram)	
								->where('pm_idSemester = ?',$idSemester)							
								->where('pm_type = 2')
								->where('pm_category = 0')
								->where('DATE(pm_date) <= CURDATE()')
								->where('TIME(pm_time) <= CURTIME()');
		$result = $db->fetchRow($select);
		if($result){
			return true;
		}else{
			return false;
		}
	}
	
}
?>