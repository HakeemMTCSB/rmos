<?php
class Examination_Model_DbTable_CourseGroupStudentAttendance extends Zend_Db_Table_Abstract {
	
	protected $_name = 'course_group_attendance';
	protected $_primary = "id";
	
	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						->from(array('cga'=>$this->_name));
			
		if($id!=null){
			$selectData->where("cga.id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
		}else{
			$row = $db->fetchAll($selectData);
		}
			
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getPaginateData($group_id){
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
		->from(array('cga'=>$this->_name))
		->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cga.create_by', array())
		->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>"FullName"))
		->joinLeft(array('tsm'=>'tbl_staffmaster'),'tsm.IdStaff = cga.lecturer_id', array('FullName'))
		->where('cga.group_id =?',$group_id)
		->order('cga.class_date desc');

		return $selectData;
	}
	
	public function insert(array $data){
	
		if( !isset($data['create_by']) ){
			$auth = $auth = Zend_Auth::getInstance();
				
			$data['create_by'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['create_date']) ){
			$data['create_date'] = date('Y-m-d H:i:a');
		}
	
		return parent::insert($data);
	}
	
	public function getAttendanceByGroup($group_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('cga'=>$this->_name))
		->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cga.create_by', array())
		->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>"FullName"))
		->joinLeft(array('tsm'=>'tbl_staffmaster'),'tsm.IdStaff = cga.lecturer_id', array('FullName'))
		->where('cga.group_id =?',$group_id);
			
		
		$row = $db->fetchAll($selectData);
		
			
		if($row){
			return $row;
		}else{
			return null;
		}
	}
}
?>