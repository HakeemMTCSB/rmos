<?php 

class Examination_Model_DbTable_ExamConfiguration extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_configuration';
	protected $_primary = "ec_id";
	

	
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}	
	
	public function addData($data) {		
		$this->insert($data);
	}
	
	public function getData(){
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('ec'=>$this->_name))	;				 
					 	
		//echo $select;
		$row = $db->fetchRow($select);
		return $row;
	}
	
	
	
	
	
}

?>