<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/3/2016
 * Time: 11:08 AM
 */
class Examination_Model_DbTable_CeReport extends Zend_Db_Table {

    public function searchStudentCeEntryMark($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db ->select()
            ->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgram'))
            ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby','final_course_mark','grade_point','grade_name','mark_approval_status','grade_desc'))
            ->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
            ->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = sr.IdStudentRegistration AND er.er_idSemester = srs.IdSemesterMain AND er.er_idSubject=srs.IdSubject',array('er_attendance_status'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance_status'=>'DefinitionDesc'))
            ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject',array('SubCode','SubjectName','subjectMainDefaultLanguage'))
            ->joinLeft(array('sem'=>'tbl_semestermaster'),'sem.IdSemesterMaster = srs.IdSemesterMain')
            ->where('srs.IdSubject = ?', 126)
            ->order("sem.SemesterMainStartDate DESC")
            ->order('sp.appl_fname');

        if ($search != false){
            if (isset($search['year']) && count($search['year']) >0){
                $select ->where('sem.AcademicYear IN (?)', $search['year']);
            }

            if (isset($search['semester']) && count($search['semester']) >0){
                $select ->where('srs.IdSemesterMain IN (?)', $search['semester']);
            }
        }

        $rows = $db->fetchAll($select);
        return $rows;
    }

    public function fnGetSemesterList($scheme=null, $groupByYear = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $db->select()
            ->from(array("a"=>"tbl_semestermaster"))
            ->joinLeft(array('s'=>'tbl_scheme'),'s.IdScheme=a.IdScheme',array('SchemeCode'))
            ->where('a.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption")')
            ->order("SemesterMainStartDate DESC");

        if(isset($scheme) && $scheme!=''){
            $lstrSelect->where('a.IdScheme = ?',$scheme);
        }

        if ($groupByYear != false){
            $lstrSelect->group('a.AcademicYear');
        }

        $results = $db->fetchAll($lstrSelect);
        return $results;
    }

    function getMyListMainComponent($search = false){

        $auth = Zend_Auth::getInstance();

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db ->select()
            ->from(array('mdm'=>'tbl_marksdistributionmaster'))
            ->join(array('mdd'=>'tbl_marksdistributiondetails'),'mdd.IdMarksDistributionMaster=mdm.IdMarksDistributionMaster',array())
            ->join(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array('component_name'=>'Description','component_name_my'=>'Description','FinalExam'))
            ->joinLeft(array('sem'=>'tbl_semestermaster'),'sem.IdSemesterMaster = mdm.semester')
            //->where('mdm.semester = ?',$idSemester)
            ->where("mdm.IdCourse = ?", 126)
            ->where("mdm.Type= 1")
            ->order('eat.sorting')
            ->group('mdm.IdComponentType');

        if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){
            $select->where('mdd.IdLecturer = ?',$auth->getIdentity()->IdStaff);
        }

        if ($search != false){
            if (isset($search['year']) && count($search['year']) >0){
                $select ->where('sem.AcademicYear IN (?)', $search['year']);
            }

            if (isset($search['semester']) && count($search['semester']) >0){
                $select ->where('mdm.semester IN (?)', $search['semester']);
            }
        }

        $result = $db->fetchAll($select);

        return $result;
    }
}