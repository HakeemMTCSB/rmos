<?php
class Examination_Model_DbTable_MarkDistributionSetup extends Zend_Db_Table_Abstract { //Model Class for Users Details

	protected $_name = 'tbl_markdistribution_setup';	
	protected $_primary = 'mds_id';
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
    public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function updateData($data,$id){		
		 $this->update($data, "mds_id = '".(int)$id."'");
	}
	
	public function getListData($user_type,$formData=null) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			 ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=mds.mds_course',array("IdSubject","SubjectName","subjectMainDefaultLanguage",'SubCode'))
				 			 ->where('mds_user_type = ?',$user_type)
				 			 ->group('mds_semester')
				 			 ->group('mds_course');
				 				 
		if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
	 		$select->where('mds_semester = ?',$formData['IdSemester']);
	 	} 
	 	
	 	if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
	 		$select->where('mds_course = ?',$formData['IdSubject']);
	 	} 
	 	
	 
		$rows = $db->fetchAll($select);
		return $rows;
     }
     
	public function getDuplicateData($user_type,$formData=null) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			 ->where('mds_semester = ?',$formData['IdSemester'])
				 			 ->where('mds_course = ?',$formData['IdSubject'])
				 			 ->where('mds_user_type = ?',$user_type);
				 			 
		if($user_type==2){ //HOP
			 $select->where('mds_program = ?',$formData['IdProgram']);
		}		 			 
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
     
	public function getExistData($user_type,$formData=null) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//get seleted semester start date 
		$select_sem = $db->select()
				 	     ->from(array('sm'=>'tbl_semestermaster'),array('sm.SemesterMainStartDate','IdSemesterMaster'))
				 	     ->where('IdSemesterMaster = ?',$formData['IdSemester']);
	    $row_sem = $db->fetchRow($select_sem);	
		
		
		$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			 ->where('sm.SemesterMainStartDate <= ?',$row_sem['SemesterMainStartDate'])
				 			 ->where('mds_course = ?',$formData['IdSubject'])	
				 			 ->where('mds_program = ?',$formData['IdProgram'])				 			
				 			 ->where('mds_user_type = ?',$user_type)
				 			 ->order('sm.SemesterMainStartDate desc')
				 			 ->group('mds.mds_semester');
				 			 
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
 	public function getLatestSemesterDetails($user_type,$idAssessment,$idSemester,$idSubject,$idProgram=null){
     	
     	$db = Zend_Db_Table::getDefaultAdapter();
     	
     	$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			 ->where('mds_assessmenttypeid = ?',$idAssessment)
				 			 ->where('mds_semester = ?',$idSemester)
				 			 ->where('mds_course = ?',$idSubject)
				 			 ->where('mds_user_type = ?',$user_type);
				 			 				 			 
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
     public function getDetails($user_type,$idAssessment,$idSemester,$idSubject,$idProgram){
     	
     	$db = Zend_Db_Table::getDefaultAdapter();
     	
     	$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			 ->where('mds_assessmenttypeid = ?',$idAssessment)
				 			 ->where('mds_semester = ?',$idSemester)
				 			 ->where('mds_course = ?',$idSubject)
				 			 ->where('mds_user_type = ?',$user_type)
				 			 ->where('mds_program = ?',$idProgram);
			
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
	public function getExistDataByHOP($idSemester,$idProgram,$idSubject) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			// ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			// ->where('sm.SemesterMainStartDate <= (SELECT SemesterMainStartDate FROM tbl_semestermaster WHERE IdSemesterMaster = ?)',$idSemester)
				 			 ->where('mds_semester = ?',$idSemester)
				 			 ->where('mds_course = ?',$idSubject)	
				 			 ->where('mds_program = ?',$idProgram)			 			
				 			 ->where('mds_user_type = 2')
				 			// ->order('sm.SemesterMainStartDate desc')
				 			 ->group('mds.mds_semester');
				 			 
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
     
 	public function getAssessment($user_type,$idAssessment,$idSemester,$idSubject,$idProgram=null){
     	
     	$db = Zend_Db_Table::getDefaultAdapter();
     	
     	$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 //->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =mds.mds_semester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
				 			 ->where('mds_assessmenttypeid = ?',$idAssessment)
				 			 ->where('mds_semester = ?',$idSemester)
				 			 ->where('mds_course = ?',$idSubject)
				 			 ->where('mds_user_type = ?',$user_type);
		if($user_type==2){ //HOP
			 $select->where('mds_program = ?',$idProgram);
		}		
		
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
     
     public function searchMarkDistSetupList($user_type,$formData=null){
     	
     	$db = Zend_Db_Table::getDefaultAdapter();
     		
     	$progDB= new GeneralSetup_Model_DbTable_Program();
		$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		
     	$activeLandscape=$landscapeDB->getAllActiveLandscape($formData['IdProgram']);
     	
     	$rows = array();
     	$allsemlandscape = array();
     	
     	//buat macam ni nak cater multiple landscape type (in future)
		foreach($activeLandscape as $i=>$actl){					
			if($actl["LandscapeType"]==43 || $actl["LandscapeType"]==42){
				$allsemlandscape[$i] = $actl["IdLandscape"];
				$i++;
			}
		}
		
		if(is_array($allsemlandscape)){
			    	
	    	  $lstrSelect = $db->select()
	    	  				->distinct()
			 				 ->from(array("ls"=>'tbl_landscapesubject'),array())		
			 				 ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject ',array('IdSubject','SubjectName','SubCode','CreditHours','key'=>'IdSubject','name'=>'subjectMainDefaultLanguage'))			 				
	                         ->join(array("ld"=>"tbl_landscape"),"ld.IdLandscape=ls.IdLandscape",array("ProgramDescription"))	                        
			 				 ->group("ls.IdSubject")
			 				 ->order("s.SubCode");			 				 
			 				 
			 	if( (isset($formData['subject_code']) && $formData['subject_code']!='') || (isset($formData['subject_name']) && $formData['subject_name']!='')){
			 		if(isset($formData['subject_code']) && $formData['subject_code']!=''){
						$lstrSelect->where('s.SubCode LIKE ?','%'.$formData['subject_code'].'%');
					}
					
					if(isset($formData['subject_name']) && $formData['subject_name']!=''){
						$lstrSelect->where('s.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
					}
			 	}else{
			 		foreach ($allsemlandscape as $landscape) {
				 		$lstrSelect->orwhere("ls.IdLandscape = ?",$landscape);
					} 
			 	}
								
				//echo $lstrSelect;
			 	$rows = $db->fetchAll($lstrSelect);
			 	
			 	if(count($rows)>0){
					foreach($rows as $index=>$row){
				
					$select_mds = $db->select()
					 			 ->from(array('mds'=>$this->_name))
					 			 ->where('mds_user_type = ?',$user_type)
					 			 ->where('mds_semester = ?',$formData['IdSemester'])
					 			 ->where('mds_course = ?',$row['IdSubject'])
					 			 ->group('mds_semester')
					 			 ->group('mds_course');
	
					 			 //for Course Coordinator Only
					 			// if($user_type==2){
										//if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
											$select_mds->where('mds.mds_program = ?',$formData['IdProgram']);
										//}
					 			// } 		
						 
					$rows_mds = $db->fetchRow($select_mds);
					
					if($rows_mds){
						$rows[$index]['status']=1; //true
						$rows[$index]['mds_course']=$rows_mds['mds_course'];
						$rows[$index]['mds_semester']=$rows_mds['mds_semester'];
						$rows[$index]['mds_program']=$rows_mds['mds_program'];
					}else{
						$rows[$index]['status']=0; //false
					}
				}
		}
		}
		
		return $rows;
     		
				
     }
     
	public function searchtMarkDistributionList($user_type,$formData=null){
     	
     	$db = Zend_Db_Table::getDefaultAdapter();
     	
     	$select = $db->select() ->from(array('sm'=>'tbl_subjectmaster'),array('IdSubject','SubCode','SubjectName','subjectMainDefaultLanguage'))->where('CourseType!=20')->where('Active=1')->order('SubCode');
				 			
		if(isset($formData)){
			
			if(isset($formData['IdCollege']) && $formData['IdCollege']!=''){
				$select->where('sm.IdFaculty = ?',$formData['IdCollege']);
			}

			if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
				$select->where('sm.IdSubject = ?',$formData['IdSubject']);
			}
			
			if(isset($formData['subject_code']) && $formData['subject_code']!=''){
				$select->where('sm.SubCode LIKE ?','%'.$formData['subject_code'].'%');
			}
			
			if(isset($formData['subject_name']) && $formData['subject_name']!=''){
				$select->where('sm.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
			}
		}
		
		$rows = $db->fetchAll($select);
		
		
		foreach($rows as $index=>$row){
			
				$select_mds = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->where('mds_user_type = ?',$user_type)
				 			 ->where('mds_semester = ?',$formData['IdSemester'])
				 			 ->where('mds_course = ?',$row['IdSubject'])
				 			 ->group('mds_semester')
				 			 ->group('mds_course');

				 			 //for Course Coordinator Only
				 			 if($user_type==2){
									if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
										$select_mds->where('mds.mds_program = ?',$formData['IdProgram']);
									}
				 			 } 		
					 
				$rows_mds = $db->fetchRow($select_mds);
				
				if($rows_mds){
					$rows[$index]['status']=1; //true
					$rows[$index]['mds_course']=$rows_mds['mds_course'];
					$rows[$index]['mds_semester']=$rows_mds['mds_semester'];
					$rows[$index]['mds_program']=$rows_mds['mds_program'];
				}else{
					$rows[$index]['status']=0; //false
				}
		}
		
		return $rows;
     }
     
     
     public function searchMarkDistSetupListCc($formData=null){
     		
     	$db = Zend_Db_Table::getDefaultAdapter();
     		$auth = Zend_Auth::getInstance();
     		
     		$select = $db->select() 
     					->from(array('cc'=>'tbl_subjectcoordinatorlist'))
			     		->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cc.IdSubject',array('IdSubject','SubCode','SubjectName','subjectMainDefaultLanguage'))
			     		->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = cc.IdSemester',array("IdSemesterMaster","SemesterMainName","SemesterMainDefaultLanguage"))
			     		->where('sm.CourseType!=20')
			     		->where('sm.Active=1')	
			     		->group('sm.IdSubject')		     		
			     		->order('sm.SubCode');
			     		
			if(isset($formData)){				
				
				if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
					$select->where('cc.IdSemester = ?',$formData['IdSemester']);
				}
				
				if(isset($formData['subject_code']) && $formData['subject_code']!=''){
					$select->where('sm.SubCode LIKE ?','%'.$formData['subject_code'].'%');
				}
				
				if(isset($formData['subject_name']) && $formData['subject_name']!=''){
					$select->where('sm.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
				}
			}
			
      		if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin
      			$select->where('cc.IdStaff = ?',$auth->getIdentity()->IdStaff);	
		 	}
			
			//echo $select;
			$rows = $db->fetchAll($select);
		
     		if(count($rows)>0){
					foreach($rows as $index=>$row){
				
					$select_mds = $db->select()
					 			 ->from(array('mds'=>$this->_name))
					 			 ->where('mds_user_type = ?',2)
					 			 ->where('mds_course = ?',$row['IdSubject'])
					 			 ->group('mds_semester')
					 			 ->group('mds_course');	
					
					 if(isset($formData)){				
				
						if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
							$select_mds->where('mds_program = ?',$formData['IdProgram']);
						}

					 	if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
							$select_mds->where('mds_semester = ?',$formData['IdSemester']);
						}
					} 
					
					$rows_mds = $db->fetchRow($select_mds);
					
					if($rows_mds){
						$rows[$index]['status']=1; //true
						$rows[$index]['mds_course']=$rows_mds['mds_course'];
						$rows[$index]['mds_semester']=$rows_mds['mds_semester'];
						$rows[$index]['mds_program']=$rows_mds['mds_program'];
					}else{
						$rows[$index]['status']=0; //false
					}
				}
			}
			
			return $rows;
     }
     
	public function getExistHOPByGroup($idSemester,$idSubject,$idGroup) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				 			 ->from(array('mds'=>$this->_name))
				 			 ->where('mds_semester = ?',$idSemester)
				 			 ->where('mds_course = ?',$idSubject)	
				 			 ->where('mds_program IN (SELECT program_id FROM course_group_program WHERE group_id = ?)',$idGroup)			 			
				 			 ->where('mds_user_type = 2')
				 			 ->group('mds.mds_semester');
				 			 
		$row = $db->fetchRow($select);
		
		return $row;
     }
     
    
}

?>