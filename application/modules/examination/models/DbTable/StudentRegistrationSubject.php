<?php 
class Examination_Model_DbTable_StudentRegistrationSubject extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'tbl_studentregsubjects';
	protected $_primary = "IdStudentRegSubjects";
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
		
	public function getSemesterStatus($idSemester,$IdStudentRegistration){
		$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
		 	 				  ->from(array('sss'=>'tbl_studentsemesterstatus'))
		 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=sss.studentsemesterstatus',array('defination'=>'DefinitionDesc'))
		 	 				  ->where('sss.IdSemesterMain = ?',$idSemester)
		 	 				  ->where('sss.IdStudentRegistration = ?',$IdStudentRegistration); 	 				  
		 	return $row = $db->fetchRow($select);	 				 
	}
	
	
	public function getSemesterSubjectStatus($idSemester,$IdStudentRegistration,$idSubject){
		$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
		 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))
		 	 				  ->where('srs.IdSemesterMain = ?',$idSemester)
		 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
		 	 				  ->where('srs.IdSubject = ?',$idSubject); 	 				  
		 	return $row = $db->fetchRow($select);	 				 
	}
	
     /*
	 * This function to get course registered by semester.
	 */
	public function getListCourseRegisteredBySemester($registrationId,$idSemesterMain){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName')) 
                        ->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idProgram=sr.IdProgram AND er.er_idSubject=srs.IdSubject')
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)
                        ->where('er.er_attendance_status != ?',396)        
                       // ->where('srs.subjectlandscapetype != 2')                
                        ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5 or srs.Active=6');   //Status => 1:Register 4:Repeat 5:Refer
						            
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function getCourseRegisteredBySemester($registrationId,$idSemesterMain,$combineBestPembaikan=false){
		/*
		 * Dapatkan yang terbaik daripada Regular dan Pembaikan + subject tambahan dari pembaikan
		 * 
		 */
		$db = Zend_Db_Table::getDefaultAdapter();
		$sql1=$db->select()
				->from("tbl_semestermaster")
				->where("idsemestermaster = ?",$idSemesterMain);
		
		$sem = $db->fetchRow($sql1);

		if($combineBestPembaikan){
		
    		$sql="
    		SELECT  srs.*,CreditHours,SubCode,BahasaIndonesia as SubName,sm.semesterfunctiontype FROM `tbl_studentregsubjects` AS `srs` 
    		INNER JOIN `tbl_semestermaster` AS `sm` ON sm.idsemestermaster=srs.idsemestermain
    		INNER JOIN  tbl_subjectmaster as sbm ON  sbm.IdSubject=srs.IdSubject
    		inner join(
    		select idsubject,max(grade_point) as maxgrade from tbl_studentregsubjects AS `srs` 
    		INNER JOIN `tbl_semestermaster` AS `sm` ON sm.idsemestermaster=srs.idsemestermain 
    		WHERE (srs.IdStudentRegistration = '$registrationId') AND (idacadyear = '".$sem["idacadyear"]."') AND (srs.subjectlandscapetype != 2)		 
    		AND (semestercounttype = '".$sem["SemesterCountType"]."') 
    		group by srs.idsubject
    		
    		) maxtable on maxtable.idsubject = srs.idsubject and maxtable.maxgrade = srs.grade_point
    		WHERE 
    		(srs.IdStudentRegistration = '$registrationId') 
    		AND (idacadyear = '".$sem["idacadyear"]."')
    		AND (semestercounttype = '".$sem["SemesterCountType"]."')
    		GROUP BY srs.idsubject    
    		";
		}else{
    		  $sql="
    		  SELECT  srs.*,CreditHours,SubCode,BahasaIndonesia as SubName FROM `tbl_studentregsubjects` AS `srs`
    		  INNER JOIN `tbl_semestermaster` AS `sm` ON sm.idsemestermaster=srs.idsemestermain
    		  INNER JOIN  tbl_subjectmaster as sbm ON  sbm.IdSubject=srs.IdSubject
    		  WHERE srs.IdStudentRegistration = '$registrationId' AND srs.subjectlandscapetype != 2
        	  AND srs.idSemesterMain = '".$idSemesterMain."'
        	  GROUP BY srs.idsubject    
        	  ";
		}
		//AND (semesterfunctiontype = 1 OR semesterfunctiontype = 0) line 86 and 79
		//
		/*$sql ="
		SELECT srs.*,CreditHours,SubCode,BahasaIndonesia as SubName,sm.semesterfunctiontype FROM `tbl_studentregsubjects` AS `srs` 
		INNER JOIN `tbl_semestermaster` AS `sm` ON sm.idsemestermaster=srs.idsemestermain
		INNER JOIN  tbl_subjectmaster as sbm ON  sbm.IdSubject=srs.IdSubject
		inner join(
		select idsubject,max(grade_point) as maxgrade from (SELECT idsubject, grade_point

          FROM tbl_studentregsubjects AS srs
          INNER JOIN tbl_semestermaster AS sm ON sm.idsemestermaster = srs.idsemestermain
          WHERE (
          srs.IdStudentRegistration ='$registrationId'
          )
          AND (
          idacadyear ='".$sem["idacadyear"]."'
          )
          AND (
          semestercounttype ='".$sem["SemesterCountType"]."'
          )
          AND (
          semesterfunctiontype =1
          )
          AND idsubject
          IN (
          
          SELECT idsubject
          
          FROM tbl_studentregsubjects AS srs
          INNER JOIN tbl_semestermaster AS sm ON sm.idsemestermaster = srs.idsemestermain
          WHERE (
          srs.IdStudentRegistration ='$registrationId'
          )
          AND (
          idacadyear ='".$sem["idacadyear"]."'
          )
          AND (
          semestercounttype ='".$sem["SemesterCountType"]."'
          )
          AND (
          semesterfunctiontype =0
          )
          )
          UNION 
          SELECT idsubject, grade_point
          
          FROM tbl_studentregsubjects AS srs 
          INNER JOIN tbl_semestermaster AS sm ON sm.idsemestermaster = srs.idsemestermain
          WHERE (
          srs.IdStudentRegistration ='$registrationId'
          )
          AND (
          idacadyear ='".$sem["idacadyear"]."'
          )
          AND (
          semestercounttype ='".$sem["SemesterCountType"]."'
          )
          AND (
          semesterfunctiontype =0
          )
          ) as a group by idsubject
		) maxtable on maxtable.idsubject = srs.idsubject and maxtable.maxgrade = srs.grade_point
		WHERE 
		(srs.IdStudentRegistration = '$registrationId') AND (idacadyear = '".$sem["idacadyear"]."') 
		AND (semestercounttype = '".$sem["SemesterCountType"]."') AND 
		(semesterfunctiontype = 1 OR semesterfunctiontype = 0)   
		group by srs.IdSubject    
		";*/
		//echo $sql;
		//exit;         
        $result = $db->fetchAll($sql);
        return $result;
	}	
	public function getCategoryCourseRegistered($IdStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array())	 
	 	 				  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('idCategory'=>'category'))  
	 	 				  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sm.category',array('category'=>'DefinitionDesc','kategori'=>'BahasaIndonesia'))	 	
	 	 				  ->where('srs.exam_status = "C"')			 
	 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->group('sm.category');
	 	 				  				  
	 	return $row = $db->fetchAll($select);	 	
	}
	
	public function getCourseRegistered($IdStudentRegistration,$idCategory=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_name','grade_point'))	 
	 	 				  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName','NamaSubjek'=>'BahasaIndonesia','category'))  	 	
	 	 				  ->where('srs.exam_status = "C"')			 
	 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->group('srs.IdSubject');
	 	 				  
	 	if(isset($idCategory) && $idCategory!=''){
	 	 	 $select ->where('sm.category = ?',$idCategory);
	 	}			  		
	 	 		  
	 	return $row = $db->fetchAll($select);	 	
	}
	
	public function getHighestMarkOld($IdStudentRegistration,$IdSubject,$semester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
    	$select_max = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('max(grade_point)'))
	 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array())	 
	 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('srs.IdSubject = ?',$IdSubject)
	 	 				  ->where('sm.SemesterMainStartDate <= ?',$semester["SemesterMainStartDate"]);
	 	 				  
	 	 $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	 	 				  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName','NamaSubjek'=>'BahasaIndonesia','category')) 
	 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('srs.IdSubject = ?',$IdSubject)
	 	 				  ->where('srs.grade_point = (?)',$select_max);
	 				  
	 	return $row = $db->fetchRow($select);	 	
	}
	
	public function getHighestMark($IdStudentRegistration,$IdSubject,$semester){
		
	   $db = Zend_Db_Table::getDefaultAdapter();
		 				  
	   $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName','NamaSubjek'=>'BahasaIndonesia','category')) 
 						  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array())
	 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('srs.IdSubject = ?',$IdSubject)
	 	 				  ->where('sm.SemesterMainStartDate <= ?',$semester["SemesterMainStartDate"])
	 	 				  ->order('srs.grade_point desc');
	 				  
	 	return $row = $db->fetchRow($select);	 	
	}
	
	public function getData($IdStudentRegSubjects){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	 	 				  ->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration')  
	 	 				  ->join(array('sm' => 'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CourseType','CreditHours')) 
	 	 				  ->where('srs.IdStudentRegSubjects= ?',$IdStudentRegSubjects);	 	 							  
	 	return $row = $db->fetchRow($select);	 				 
	}
	
		
	public function getHighestMarkofAllSemester($IdStudentRegistration,$IdSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 				  
	    $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName','NamaSubjek'=>'BahasaIndonesia','category')) 
 				          ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 	 				  ->where('srs.IdSubject = ?',$IdSubject)	 	 				
	 	 				  ->order('srs.final_course_mark desc');
	 				  
	 	return $row = $db->fetchRow($select);	 
	}
	
	public function getRegDataParent($IdStudentRegistration,$parentId){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects'))	
	 	 				  ->join(array('lbs' => 'tbl_landscapeblocksubject'),'lbs.subjectid = srs.IdSubject')  
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('CreditHours'))
	 	 				  ->where('srs.subjectlandscapetype= 2')
	 	 				  ->where('srs.IdStudentRegistration= ?',$IdStudentRegistration)
	 	 				  ->where('lbs.IdLandscapeblocksubject= ?',$parentId);	 	 							  
	 	return $row = $db->fetchRow($select);	 				 
	}
	
	
	//utk update run
	public function getSubjectParent(){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	 	 				  ->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('IdLandscape'))  	 				    
	 	 				  ->where('srs.subjectlandscapetype= 2')
	 	 				  ->where('srs.final_course_mark > 0')	
	 	 				  ->group('srs.IdStudentRegistration')
	 	 				  ->group('srs.IdSubject') ;	
	 	 				  //->limit(50,0);				
	 	 				  
	 	 				  						  
	 	return $row = $db->fetchAll($select);	 				 
	}
	
	public function updateChildMark($data,$IdStudentRegistration, $idSubject, $IdSemesterMain){
		 $db = Zend_Db_Table::getDefaultAdapter();
		  echo '<br>';
		 echo 'IdStudentRegistration = '.$IdStudentRegistration.' AND IdSubject = '.$idSubject.' AND IdSemesterMain = '.$IdSemesterMain;
		 echo '<br>';
		 //$db->update($data,'IdStudentRegistration = '.$IdStudentRegistration.' AND IdSubject = '.$idSubject.'IdBlock = '.$idBlock);
	}
	
	
	//run update child mark
	public function getStudentRegSubjectInfo($idSemester,$IdStudentRegistration,$idSubject){
		$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
		 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))
		 	 				  ->where('srs.IdSemesterMain = ?',$idSemester)
		 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)		 	 				  
		 	 				  ->where('srs.IdSubject = ?',$idSubject); 	 				  
		 	return $row = $db->fetchRow($select);	 				 
	}
	
	//run child 
	public function getSemesterSubjectStatusDua($IdStudentRegistration,$idSubject){
		$db = Zend_Db_Table::getDefaultAdapter();
			 $select = $db->select()
		 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))		 	 				
		 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
		 	 				  ->where('srs.IdSubject = ?',$idSubject)
		 	 				  ->where('srs.grade_point IS NOT NULL')
		 	 				  ->where('srs.final_course_mark IS NULL OR srs.final_course_mark=0'); 	 				  
		 	return $row = $db->fetchRow($select);	 				 
	}
	
	public function getStudentDataLandscape($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	 	 				  ->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration')  
	 	 				  ->join(array('l' => 'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('LandscapeType'))  
	 	 				  ->where('srs.IdStudentRegSubjects= ?',$IdStudentRegSubjects);	 	 							  
	 	return $row = $db->fetchRow($select);	 				 
	}
	
	function getSemesterRegular($idsemmain,$idstudreg){
		$db = Zend_Db_Table::getDefaultAdapter();
		$sql = "SELECT * FROM `tbl_semestermaster` sm 
		inner join tbl_studentsemesterstatus as sss on sss.IdSemesterMain=sm.IdSemesterMaster 
		WHERE 
		sss.IdStudentRegistration = $idstudreg 
		";
		$row = $db->fetchRow($sql);
		return $row;
	}
		
	
	public function getPrevSubjectRegInfo($IdStudentRegistration,$IdSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql = "SELECT SemesterMainStartDate FROM tbl_semestermaster WHERE IdSemesterMaster = $idSemester ";
		$row_sem = $db->fetchRow($sql);
		
		$select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_point'))		 
	 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array())
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('CreditHours'))    	 				  
	 	 				  ->where('srs.IdStudentRegistration= ?',$IdStudentRegistration)
	 	 				  ->where('srs.IdSubject = ?',$IdSubject)
	 	 				  ->where('sm.SemesterMainStartDate < (?)',$row_sem['SemesterMainStartDate']);	 	 							  
	 	return $row = $db->fetchAll($select);
		
	}
	
	public function getRepeatSubjectRegInfo($IdStudentRegistration,$IdSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql = "SELECT SemesterMainStartDate FROM tbl_semestermaster WHERE IdSemesterMaster = $idSemester ";
		$row_sem = $db->fetchRow($sql);
		
		$select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_point'))		 
	 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array())
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('CreditHours'))
						  ->joinLeft(array('c'=>'exam_registration'), 'srs.IdSemesterMain = c.er_idSemester AND srs.IdStudentRegistration = c.er_idStudentRegistration AND srs.IdSubject = c.er_idSubject')
	 	 				  ->where('srs.IdStudentRegistration= ?',$IdStudentRegistration)
	 	 				  //->where('srs.Active != ?',3)
	 	 				  ->where('srs.IdSubject = ?',$IdSubject)
						  ->where('c.er_attendance_status != ?', 396)
	 	 				  ->where('sm.SemesterMainStartDate < (?)',$row_sem['SemesterMainStartDate'])
	 	 				  ->order('sm.SemesterMainStartDate desc');
	 	return $row = $db->fetchRow($select);
		
	}
	
	public function getPrevReplaceSubjectInfo($IdStudentRegistration,$IdStudentRegSubjects,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql = "SELECT SemesterMainStartDate FROM tbl_semestermaster WHERE IdSemesterMaster = $idSemester ";
		$row_sem = $db->fetchRow($sql);
		
		$select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_point'))		 
	 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array())
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('CreditHours'))
						  ->joinLeft(array('c'=>'exam_registration'), 'srs.IdSemesterMain = c.er_idSemester AND srs.IdStudentRegistration = c.er_idStudentRegistration AND srs.IdSubject = c.er_idSubject')
	 	 				  ->where('srs.IdStudentRegistration= ?',$IdStudentRegistration)
						  ->where('c.er_attendance_status != ?', 396)
						  //->where('srs.Active != ?',3)
	 	 				  ->where('srs.IdStudentRegSubjects = ?',$IdStudentRegSubjects)
	 	 				  ->where('sm.SemesterMainStartDate < (?)',$row_sem['SemesterMainStartDate']);	 	 							  
	 	
	 	 return $row = $db->fetchRow($select);
		
	}
	
	 /*
	 * Function to get student list for remarking application.
	 */
	public function getStudent($formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr'=>'tbl_studentregistration'), array('registrationId'))  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects'))
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName','subjectMainDefaultLanguage'))
                        ->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id',array('appl_fname','appl_lname'))
                        ->where('srs.IdSemesterMain = ?',$formData['IdSemester'])  
                        ->where('srs.Active != 3')
                        ->where('srs.IdStudentRegSubjects NOT IN (SELECT sa_idStudentRegSubject FROM tbl_student_appeal)')
                        ->order('sp.appl_fname')
                        ->order('sp.appl_lname');   //Status => 1:Register 4:Repeat 5:Refer

                        if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
                        	$sql->where('srs.IdSubject = ?',$formData['IdSubject']);
                        }
                        
						if(isset($formData['student_name']) && $formData['student_name']!=''){
                        	$sql->where('sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
                        	$sql->whereor('sp.appl_lname LIKE ?','%'.$formData['student_name'].'%');
                        }
                        
	 					if(isset($formData['student_id']) && $formData['student_id']!=''){
                        	$sql->where('sr.registrationId = ?',$formData['student_id']);
                        }
                        
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	
	public function getStudentExamScaling($scale,$scale_mark){
	
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr'=>'tbl_studentregistration'), array('registrationId'))  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','final_course_mark'))
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName','subjectMainDefaultLanguage'))
                        ->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id',array('appl_fname','appl_lname'))
                        ->join(array('sme'=>'tbl_student_marks_entry'),'sme.IdStudentRegSubjects=srs.IdStudentRegSubjects',array('final_exam_mark'=>'FinalTotalMarkObtained','IdStudentMarksEntry'))
                        ->join(array('mdm'=>'tbl_marksdistributionmaster'),'mdm.`IdMarksDistributionMaster` =  sme.`IdMarksDistributionMaster`',array())
                        ->join(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType = mdm.IdComponentType',array())
                        ->where('eat.FinalExam = ?',1)                                          
                        ->where('srs.IdSubject = ?',$scale['es_subject'])  
                        ->where('srs.Active != ?',3)
                        ->where('sr.profileStatus = ?',92)
                        ->where('srs.final_course_mark != ""')
                        ->where('srs.exam_scaling_mark != ?',1)
                        ->where('srs.mark_approval_status = ?',2)
                        ->where('srs.IdCourseTaggingGroup = ?',$scale['es_group']) //approve
                        ->order('sp.appl_fname')
                        ->order('sp.appl_lname')
                        ->group('srs.IdStudentRegSubjects');
                        
	                    if(isset($scale_mark) && $scale_mark!=''){
	                    	
	                    	if($scale['es_mark_type']==788){  // Final Exam
	                    		$sql->where('sme.FinalTotalMarkObtained IN (?)',$scale_mark);	                    		
	                    	}
	                    	
	                    	if($scale['es_mark_type']==789){  // Course Mark
	                    		$sql->where('srs.final_course_mark IN (?)',$scale_mark);	                    		
	                    	}
	                    	
	                    }
	           
        $result = $db->fetchAll($sql);
        return $result;
   
	}
	
	public function copyHistory($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$auth = Zend_Auth::getInstance();
		
	    $select = $db->select()
	  				  ->from(array('srs'=>'tbl_studentregsubjects'))	
	  				  ->where('srs.IdStudentRegSubjects= ?',$IdStudentRegSubjects);	 	 							  
	 	$row = $db->fetchRow($select);	

	 	$row['message'] = 'Exam Scaling';
	 	$row['createddt'] = date('Y-m-d H:i:s');
	 	$row['createdby'] = $auth->getIdentity()->iduser;	
	 	
	 	$db->insert('tbl_studentregsubjects_history',$row);
	 	 	
		
	}
	
/*
	 * This function to get course registered by semester with attendance status yg hadir dan x hadir sahaja diaplay for transcrip.
	 */
	public function getListCourseRegisteredBySemesterWithAttendanceStatus($registrationId,$idSemesterMain,$idProgram=null,$type=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId','IdLandscape'))  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName','CourseType')) 
                        ->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject')
                        ->where('(CASE WHEN er.er_attendance_status IS NULL OR er.er_attendance_status = "" OR er.er_attendance_status = 0 THEN 0 ELSE er.er_attendance_status END) != 396')
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)         
                        ->where('srs.Active !=3') //Status => 3:Withdraw
                        ->where('srs.audit_program IS NULL') //exclude audit credit
                        //->where('sm.CourseType != ?',20) disabled on 8-7-2015 as per requested by munirah refer email support x calculate tapi nak display
						->where('srs.mark_approval_status = ?', 2)
                        ->where('srs.exam_status!= ?','I'); // kalo exam_status I&U=incomplete jgn calculate subject ni

						if ($type == 0){
							$sql->where('srs.IdSubject != ?', 126);
						}
                     
                        //utk transcript
                        /*if(isset($idProgram) && $idProgram==5){
                        	/*$sql->join(array('l'=>'tbl_landscape'),array())
                        		->joinLeft(array('ls'=>'tbl_landscapesubject'),'ls.IdLandscape = l.IdLandscape AND ls.IdSubject = srs.IdSubject',array())
                        		->joinLeft(array('lb'=>'tbl_landscapeblock'),'lb.idblock = ls.IdLevel AND lb.idlandscape = l.IdLandscape',array('block','blockname'))
                        		->group('srs.IdStudentRegSubjects')
                        		->order('lb.block');*/
                        	/*$sql->joinLeft(array('eq'=>'tbl_subjectequivalent'), 'eq.idsubject = srs.IdSubject',array());
				  			$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape AND (ls.IdSubject = srs.IdSubject or eq.idsubjectequivalent = ls.IdSubject)',array());
							//$sql->join(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape  AND ls.IdSubject = srs.IdSubject',array());
				  			$sql->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array('block','blockname'));
				  			$sql->joinLeft(array('lsr' => 'tbl_studentregsubjects'),'lsr.IdSubject = ls.IdSubject',array())
				  			->group('srs.IdStudentRegSubjects')
				  			->group('eq.idsubject')
                        		->order('lsb.block');
	
                        }*/
                          
        $result = $db->fetchAll($sql);
        
        
          if(isset($idProgram) && $idProgram==5){
          	
	          	foreach($result as $index=>$row){
	          		
	          		//ini tritu academic progress  4-6-2015
		          	$select2 = $db->select()
					     		->from(array("ls"=>"tbl_landscapesubject"),array())
					     		//->joinLeft(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array())
					     		//->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array())
					     		//->joinLeft(array("pr"=>"tbl_programrequirement"),'pr.IdProgramReq=ls.IdProgramReq',array())
					     		->joinLeft(array("lb"=>"tbl_landscapeblock"),'ls.idlevel=lb.idblock',array("block",'blockname'))
					     		//->where("ls.IdProgram = ?",$idProgram)
					     		->where("ls.IdLandscape = ?",$row['IdLandscape'])
					     		->where("ls.IdSubject = ?",$row['IdSubject']);
					   $result_block = $db->fetchRow($select2);
					   
					    if(!$result_block){
					    	
            				//$equivid=$recordDb->checkEquivStatus($student['IdLandscape'],$course["IdSubject"]);
            						
								$sqle =   $db->select()
											->from(array("a"=>"tbl_landscapesubject"),array())
											->join(array("b"=>"tbl_subjectequivalent"),"a.idsubject=b.idsubjectequivalent",array('idsubjectequivalent'))
											->where("a.idlandscape = ?",$row['IdLandscape'])
											->where("b.idsubject =?",$row['IdSubject']);
								$result_e = $db->fetchRow($sqle);
										            	
				            	if($result_e){
				            			//ini tritu academic progress  4-6-2015 dekat library icampus
				            			$select3 = $db->select()
										     		->from(array("ls"=>"tbl_landscapesubject"),array())
										     		->joinLeft(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array())
										     		->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array())
										     		->joinLeft(array("pr"=>"tbl_programrequirement"),'pr.IdProgramReq=ls.IdProgramReq',array())
										     		->joinLeft(array("lb"=>"tbl_landscapeblock"),'ls.idlevel=lb.idblock',array("block",'blockname'))
										     		->where("ls.IdProgram = ?",$idProgram)
										     		->where("ls.IdLandscape = ?",$row['IdLandscape'])
										     		->where("s.IdSubject = ?",$result_e['idsubjectequivalent'])
										     		->where("IDProgramMajoring = 0");
										   $result_block3 = $db->fetchRow($select3);	
				            			   //$course_status = $landscapeSubjectDb->getCommonSubjectStatus($student['IdProgram'],$student['IdLandscape'],$equivid);
				            			   
										   if($result_block3){
										   		$result[$index]['block']=$result_block3['block'];
           										$result[$index]['blockname']=$result_block3['blockname'];
										   }else{
										   		
										   		$result[$index]['block']='x';
           										$result[$index]['blockname']='x';
										   }
				            	}else{
				            		$result[$index]['block']='x';
           							$result[$index]['blockname']='x';
				            	}
           				}else{
           					$result[$index]['block']=$result_block['block'];
           					$result[$index]['blockname']=$result_block['blockname'];
           				}                                
	          	}
        }
        
        return $result;
	}
	
	public function getPartListPerSemester($registrationId,$idSemesterMain){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//Mencari equivalent. tak tau la mana datang query ni, buat tengah mamai
		$regsql = $db->select()
					->from(array("srs"=>'tbl_studentregsubjects'),array("srs.IdSubject"))
					->where("srs.IdStudentRegistration = ?", $registrationId)
					->where("srs.IdSemesterMain = ?", $idSemesterMain);

		$select_eqv = $db->select()
				->distinct()
				->from(array("e"=>"tbl_subjectequivalent"),array("idsubject"))
				->where("e.idsubjectequivalent IN ? ",$regsql);
		
		//main query
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array())  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array()) 
                        ->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idProgram=sr.IdProgram AND er.er_idSubject=srs.IdSubject AND er.er_attendance_status !=396',array())
                        ->join(array('l'=>'tbl_landscape'),'l.IdLandscape = sr.IdLandscape',array())
                        ->join(array('ls'=>'tbl_landscapesubject'),$db->quoteInto('ls.IdLandscape = l.IdLandscape AND ( ls.IdSubject = srs.IdSubject OR ls.IdSubject IN (?))', $select_eqv),array())
                        ->join(array('lb'=>'tbl_landscapeblock'),'lb.idblock = ls.IdLevel AND lb.idlandscape = l.IdLandscape',array('block','blockname'=>'blockname','block',"blockname_desc"=>new Zend_Db_Expr('CASE WHEN block=1 THEN "Building Knowledge" WHEN block =2 THEN "Building Skills" END')))
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
						->where('srs.mark_approval_status = ?', 2)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)         
                        ->where('srs.Active !=3') //Status => 3:Withdraw
                        ->where('srs.exam_status!= ?','I')
                        ->where('lb.block != ?',3)
                        ->group('lb.block')
                        ->order('lb.block');                        
       
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function getListCourseRegisteredBySemesterWithoutAttendance($registrationId,$idSemesterMain, $type = 0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName','CourseType'))                        
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)
						->where('srs.mark_approval_status = ?', 2)
                        ->where('srs.Active != 3 ')  //Status => 3:Withdraw
                        ->where('srs.audit_program IS NULL')  //exclude audit credit
                        ->where('srs.exam_status!= ?','I'); // kalo exam_status I=incomplete jgn display subject ni

		if ($type == 0){
			$sql->where('srs.IdSubject != ?', 126);
		}
						            
        $result = $db->fetchAll($sql);
        return $result;
	}
        
        public function getRetakeSubject($IdStudentRegSubjects, $IdStudentRegistration){
            
        	$db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
                ->where('a.replace_subject = ?', $IdStudentRegSubjects)
                ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        //cek thesis
        public function isEndorsed($subject){
        	
        	$db = Zend_Db_Table::getDefaultAdapter();
        	
            $select = $db->select()
		                ->from(array('srs'=>'tbl_studentregsubjects'))
		                ->where('srs.IdSubject = ?', $subject['IdSubject'])
		                ->where('srs.IdStudentRegistration = ?', $subject['IdStudentRegistration'])
		                ->where('srs.exam_status IN ("C","P")');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
		public function getSubjectPart($idLandscape,$idSubject){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
      				 ->distinct()
	 				 ->from(array("ls"=>"tbl_landscapesubject"),array())
	 				 ->join(array('lb'=>'tbl_landscapeblock'),'lb.idblock = ls.IdLevel',array('SubjectTypeName'=>'blockname'))
					 ->where('ls.IdLandscape = ?',$idLandscape)
					 ->where('ls.IdSubject = ?',$idSubject);
		return $rowSet = $db->fetchRow($select);
	}
       
	public function getSubjectPart3($registrationId){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName','CourseType'))                        
                        ->join(array('l'=>'tbl_landscape'),'l.IdLandscape = sr.IdLandscape',array())
                        ->join(array('ls'=>'tbl_landscapesubject'),'ls.IdLandscape = l.IdLandscape AND ls.IdSubject = srs.IdSubject',array())
                        ->join(array('lb'=>'tbl_landscapeblock'),'lb.idblock = ls.IdLevel AND lb.idlandscape = l.IdLandscape',array())
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
						->where('srs.mark_approval_status = ?', 2)
                        ->where('lb.block = ?',3)      
                        ->where('srs.Active !=3') //Status => 3:Withdraw
                        ->where('srs.IdSubject IN (84,85)')
                        ->group('srs.IdStudentRegSubjects')
                        ->order('lb.block');
                                              
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	
	public function getInfoSubjectReg($IdStudentRegSubjects){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
  				  	->from(array('srs'=>'tbl_studentregsubjects'))	
  				  	->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('IdProgram','registrationId')) 
  				  	->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('AssessmentMethod')) 
  				  	->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
					->joinLeft(array('er'=>'exam_registration'),'er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject AND er.er_idSemester=srs.IdSemesterMain',array('er_attendance_status'))
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance'=>'DefinitionDesc'))
  				  	->where('srs.IdStudentRegSubjects= ?',$IdStudentRegSubjects);	 	 							  
	 	return $row = $db->fetchRow($select);	 				 
	}
	
	public function gettotalch($IdStudentRegSubjects,$idSubject){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	     $select = $db->select()
  				  		->from(array('srs'=>'tbl_studentregsubjects'),array('total_credit_hour_registered'=>'SUM(credit_hour_registered)'))	
  				  		->where('srs.IdStudentRegistration = ?',$IdStudentRegSubjects)
  				  		->where('srs.IdSubject = ?',$idSubject)
  				  		->where('srs.Active != ?',3);

	 	return $row = $db->fetchRow($select);
	}
	
	/*
	 * To get details for item = exam
	 */
	public function getItemDetail($IdStudentRegSubjects,$item_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()
	 				 ->from(array('srsd'=>'tbl_studentregsubjects_detail'))		
	 				 ->where('srsd.regsub_id = ?',$IdStudentRegSubjects)
	 				 ->where('srsd.item_id = ?',$item_id);
	 				 
	 	$row = $db->fetchRow($select);
	 	
	 	if(isset($row)){
	 		$select =$db->select()
	 				 ->from(array('srsd'=>'tbl_studentregsubjects_detail'))		
	 				 ->where('srsd.regsub_id = ?',$IdStudentRegSubjects)
	 				 ->where('srsd.item_id IN (889,890)');
	 				 
	 		$row = $db->fetchRow($select);
	 	}
	 	
	 	if($row)
	 	return $row;
	 	else
	 	return null;
				 
	}
}
	

?>