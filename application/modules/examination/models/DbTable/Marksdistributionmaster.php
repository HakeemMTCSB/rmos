<?php

class Examination_Model_DbTable_Marksdistributionmaster extends Zend_Db_Table {

	protected $_name = 'tbl_marksdistributionmaster';
	protected $_primary = 'IdMarksDistributionMaster';

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnGetMarksDistributionMaster() { //Function for the view user
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.MarksApplicationCode","a.IdCourse", "a.IdProgram", "a.IdMarksDistributionMaster", "a.semester","a.IdScheme","a.IdFaculty"))
		->joinLeft(array("b" => 'tbl_subjectmaster'), 'a.IdCourse = b.IdSubject', array('b.SubjectName'))
		->joinLeft(array("c" => 'tbl_collegemaster'), 'a.IdFaculty = c.IdCollege', array('c.CollegeName'))
		->joinLeft(array("d" => 'tbl_program'), 'a.IdProgram = d.IdProgram', array('d.ProgramName','d.ProgramCode'))
		->joinLeft(array("e" => 'tbl_scheme'), 'a.IdScheme = e.IdScheme', array('e.EnglishDescription AS schemename'))
		->joinLeft(array("f" => "tbl_definationms"), 'a.status = f.idDefinition', array('f.DefinitionDesc AS status'))
		//->where("a.Status = 193 OR a.Status = 243 ")
		//->group('a.IdCourse')
		//->group('a.IdProgram')
		//->group('a.semester')
		//->group('a.IdFaculty')
		//->group('a.IdScheme')
		->group('a.MarksApplicationCode');
		;
		/*$auth = Zend_Auth::getInstance();
		$loggeduserId = $auth->getIdentity()->iduser;
		if($loggeduserId!=1){
			$select->where("a.UpdUser =?",$loggeduserId);
		}*/

		return $result = $lobjDbAdpt->fetchAll($select);
	}

	public function fnSearchMarksDistributionMaster($post = array()) { //Function for searching the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.MarksApplicationCode","a.IdCourse", "a.IdProgram", "a.IdMarksDistributionMaster", "a.semester","a.IdScheme","a.IdFaculty"))
		->joinLeft(array("b" => 'tbl_subjectmaster'), 'a.IdCourse = b.IdSubject', array('b.SubjectName'))
		->joinLeft(array("c" => 'tbl_collegemaster'), 'a.IdFaculty = c.IdCollege', array('c.CollegeName'))
		->joinLeft(array("d" => 'tbl_program'), 'a.IdProgram = d.IdProgram', array('d.ProgramName','d.ProgramCode'))
		->joinLeft(array("e" => 'tbl_scheme'), 'a.IdScheme = e.IdScheme', array('e.EnglishDescription AS schemename'))
		->joinLeft(array("f" => "tbl_definationms"), 'a.status = f.idDefinition', array('f.DefinitionDesc AS status'))
		//->group('a.IdCourse')
		//->group('a.IdProgram')
		//->group('a.semester')
		//->group('a.IdFaculty')
		//->group('a.IdScheme');
		->group('a.MarksApplicationCode');


		if (isset($post['field5']) && !empty($post['field5'])) {
			$select = $select->where("a.IdFaculty = ?", $post['field5']);
		}
		if (isset($post['field23']) && !empty($post['field23'])) {
			$select = $select->where("a.IdScheme = ?", $post['field23']);

		}

		if (isset($post['field24']) && !empty($post['field24'])) {
			$select = $select->where('a.semester like  "%" ? "%"', $post['field24']);
		}

		if (isset($post['field8']) && !empty($post['field8'])) {
			$select = $select->where("a.IdProgram = ?", $post['field8']);
		}

		if (isset($post['field20']) && !empty($post['field20'])) {
			$select = $select->where("b.IdSubject = ?", $post['field20']);
		}
		//echo $select;die();
		//$select->where("a.Status = 193 OR a.Status = 243 ");
		/*$auth = Zend_Auth::getInstance();
		$loggeduserId = $auth->getIdentity()->iduser;
		if($loggeduserId!=1){
			$select->where("a.UpdUser =?",$loggeduserId);
		}*/
		$larrResult = $lobjDbAdpt->fetchAll($select);

		return $larrResult;
	}

	public function fnGetMarksDistributionMasterByCourse($Idcourse,$IdScheme,$IdFaculty,$IdProgram,$appcode) { //Function for the view user
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.*"))
		->joinLeft(array("b" => 'tbl_subjectmaster'), 'a.IdCourse = b.IdSubject', array('b.SubjectName'))
		->joinLeft(array("c" => 'tbl_collegemaster'), 'a.IdFaculty = c.IdCollege', array('c.CollegeName'))
		->joinLeft(array("d" => 'tbl_program'), 'a.IdProgram = d.IdProgram', array('d.ProgramName','d.IdProgram as program'))
		->joinLeft(array("e" => 'tbl_scheme'), 'a.IdScheme = e.IdScheme', array('e.EnglishDescription AS schemename'))
		->joinLeft(array("f" => "tbl_definationms"), 'a.status = f.idDefinition', array('f.DefinitionDesc AS status'))
		->joinLeft(array("g" => "tbl_examination_assessment_type"), 'a.IdComponentType = g.IdExaminationAssessmentType', array('g.Description AS component'))
		->joinLeft(array("h" => "tbl_examination_assessment_item"), 'a.IdComponentItem = h.IdExaminationAssessmentType', array('h.Description AS componentitem'))
		->where("a.IdCourse = ?", $Idcourse)
		->where("a.IdScheme = ?", $IdScheme)
		->where("a.IdFaculty = ?", $IdFaculty)
		->where("a.IdProgram = ?", $IdProgram)
		->where("a.MarksApplicationCode = ?", $appcode)
		//->where("a.Status = 193 OR a.Status = 243 ")
		//->group('a.IdComponentType')
		->order('a.IdMarksDistributionMaster');
		return $result = $lobjDbAdpt->fetchAll($select);
	}


	public function fnGetMarksDistributionMasterById($Id) { //Function for the view user
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.*"))
		->joinLeft(array("b" => 'tbl_subjectmaster'), 'a.IdCourse = b.IdSubject', array('b.SubjectName'))
		->joinLeft(array("c" => 'tbl_collegemaster'), 'a.IdFaculty = c.IdCollege', array('c.CollegeName'))
		->joinLeft(array("d" => 'tbl_program'), 'a.IdProgram = d.IdProgram', array('d.ProgramName','d.IdProgram as program'))
		->joinLeft(array("e" => 'tbl_scheme'), 'a.IdScheme = e.IdScheme', array('e.EnglishDescription AS schemename'))
		->joinLeft(array("f" => "tbl_definationms"), 'a.status = f.idDefinition', array('f.DefinitionDesc AS status'))
		->joinLeft(array("g" => "tbl_examination_assessment_type"), 'a.IdComponentType = g.IdExaminationAssessmentType', array('g.Description AS component'))
		->joinLeft(array("h" => "tbl_examination_assessment_item"), 'a.IdComponentItem = h.IdExaminationAssessmentType', array('h.Description AS componentitem'))
		->where("a.IdMarksDistributionMaster = ?", $Id);
		return $result = $lobjDbAdpt->fetchRow($select);
	}


	public function fncheckexistmasterentry($program,$course,$semester,$IdComponentType,$IdComponentItem,$appcode){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.IdMarksDistributionMaster"))
		->where("a.IdProgram = ?", $program)
		->where("a.IdCourse = ?", $course)
		->where("a.semester = ?", $semester)
		->where("a.IdComponentType = ?", $IdComponentType)
		->where("a.IdComponentItem = ?", $IdComponentItem)
		->where("a.MarksApplicationCode = ?", $appcode)
		->order('a.IdMarksDistributionMaster');
		return $result = $lobjDbAdpt->fetchAll($select);
	}



	public function fnGetSubjectName() { //function to get subject name
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("key" => "a.IdMarksDistributionMaster", "value" => "a.Name"))
		->where("a.Active = 1")
		->order("a.Name");

		return $result = $lobjDbAdpt->fetchAll($select);
	}

	public function fnAddMarksDisributionMaster($larrformData) { //Function for adding the user details to the table
		$this->insert($larrformData);
		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'IdMarksDistributionMaster');
		return $insertId;
	}

	public function fnupdateMarksDisributionMaster($data,$IdMarksDistributionMaster){
		$where = 'IdMarksDistributionMaster = ' . $IdMarksDistributionMaster;
		$this->update($data, $where);
	}

	public function fncheckduplicateentry($testArray){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.IdMarksDistributionMaster"));

		if($testArray['course']!=0){
			$select->where("a.IdCourse = ?", $testArray['course']);
		}

		if($testArray['faculty']!=0){
			$select->where("a.IdFaculty = ?", $testArray['faculty']);
		}

		if($testArray['scheme']!=0){
			$select->where("a.IdScheme = ?", $testArray['scheme']);
		}

		if($testArray['program']!=0){
			$select->where("a.IdProgram = ?", $testArray['program']);
		}
		//echo $select;
		//        if($testArray['semester']!=0){
		//            $select->where('a.semester like  "%" ? "%"', $testArray['semester']);
		//        }

		return $result = $lobjDbAdpt->fetchAll($select);
	}

	public function fncheckexistentry($testArray){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.*"));

		if($testArray['course']!=0){
			$select->where("a.IdCourse = ?", $testArray['course']);
		}

		if($testArray['program']!=0){
			$select->where("a.IdProgram = ?", $testArray['program']);
		}

		if($testArray['semester']!=0){
			$select->where('a.semester like  "%" ? "%"', $testArray['semester']);
		}

		return $result = $lobjDbAdpt->fetchAll($select);
	}







	public function fnEditMarksDisributionMaster($IdMarksDistributionMaster) { //Function for the view user
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.*"))
		->where("a.IdMarksDistributionMaster = ?", $IdMarksDistributionMaster);
		return $result = $lobjDbAdpt->fetchRow($select);
	}

	//    public function fnupdateMarksDisributionMaster($IdMarksDistributionMaster, $larrformData) { //Function for updating the user
	//        $where = 'IdMarksDistributionMaster = ' . $IdMarksDistributionMaster;
	//        $this->update($larrformData, $where);
	//    }

	public function fnGetCourseList($IdProgram) { //Function for the view user
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_landscape"), array("key" => "d.IdSubject", "value" => "CONCAT_WS('-',IFNULL(d.SubjectName,''),IFNULL(d.SubCode,''))"))
		->join(array("b" => "tbl_landscapeblocksubject"), 'a.IdLandscape = b.IdLandscape', array(''))
		->joinLeft(array("d" => "tbl_subjectmaster"), 'b.subjectid = d.IdSubject', array(''))
		->where('a.Active =?',123);
		if($IdProgram != 0){
			$select->where("a.IdProgram = ?", $IdProgram);
		}
		$select->order("d.SubjectName");
		return $result = $lobjDbAdpt->fetchAll($select);
	}

	public function fnGetCourseListsub($IdProgram) { //Function for the view user
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_landscape"), array("key" => "d.IdSubject", "value" => "CONCAT_WS('-',IFNULL(d.SubjectName,''),IFNULL(d.SubCode,''))"))
		->join(array("c" => "tbl_landscapesubject"), 'a.IdLandscape = c.IdLandscape', array(''))
		->joinLeft(array("d" => "tbl_subjectmaster"), 'c.IdSubject  = d.IdSubject', array(''))
		->where('a.Active =?',123);

		if($IdProgram != 0){
			$select->where("a.IdProgram = ?", $IdProgram);
		}
		$select->order("d.SubjectName");
		return $result = $lobjDbAdpt->fetchAll($select);
	}

	public function fnSearchMarksEntry($post = array()) { //Function for searching the user details
		$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "a.Active = " . $post["field7"];
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
		->where('a.IdSubject = ?', $post['field5'])
		->where('a.BahasaIndonesia like  "%" ? "%"', $post['field3'])
		->where($field7)
		->order('a.SubjectName');
		$result = $this->fetchAll($select);
		return $result->toArray();
	}


	public function fncheckduplicateByCourse($Idcourse){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.IdMarksDistributionMaster"))
		->where("a.IdCourse = ?", $Idcourse);
		return $result = $lobjDbAdpt->fetchAll($select);
	}


	public function fngetProgramexistinmarkdistribution(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array(""))
		->joinLeft(array("b" => "tbl_program"),'a.IdProgram = b.IdProgram',array('key' => 'b.IdProgram','value' => 'b.ProgramName'))
		->group('b.ProgramName');
		return $result = $lobjDbAdpt->fetchAll($select);
	}


	public function fngetfrommarksetupCourse(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.IdCourse"))
		->group('a.IdCourse');
		return $result = $lobjDbAdpt->fetchAll($select);
	}

	public function fngetSemesterexistinmarkdistribution(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array('key' => 'a.semester','value' => 'a.semester'))
		->group('a.semester');
		return $result = $lobjDbAdpt->fetchAll($select);
	}


	public function fncheckMaxEntry($larrformData) {
		$errMsg  =  '0';  // CAN ADD
		// COUNT Approve
		$appealsql3 =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array('Count(mdm.IdMarksDistributionMaster) as totalApprove'))
		->where("mdm.IdScheme = ?",$larrformData['IdScheme'])
		->where("mdm.IdFaculty = ?",$larrformData['IdFaculty'])
		->where("mdm.IdProgram = ?",$larrformData['IdProgram'])
		->where("mdm.IdCourse = ?",$larrformData['IdCourse'])
		->where("mdm.semester = ?",$larrformData['semestercode'])
		->where(" mdm.Status = 243 OR mdm.Status = 193 ");

		$result3 = $this->lobjDbAdpt->fetchAll($appealsql3);
		$totalApprove =  $result3[0]['totalApprove'];
		if($totalApprove=='0') {
			$errMsg  =  '0';  // CAN ADD
		} else {
			$errMsg  =  '1';  // cannot add
		}
		return $errMsg;
	}



	public function fngetLastRecord() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.MarksApplicationCode","a.IdMarksDistributionMaster"))
		->order("a.IdMarksDistributionMaster DESC")->limit("1");
		return $result = $lobjDbAdpt->fetchAll($select);
	}
	
	
	public function fngetmarksdistr($semcode,$IdProgram,$IdSubject){		
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_marksdistributionmaster"), array("a.*"))
		->where("a.IdProgram = ?",$IdProgram)
		->orwhere("a.IdProgram = ?",0)
		->where("a.IdCourse = ?",$IdSubject)
		->orwhere("a.IdCourse = ?",0)
		->where("a.semester = ?",$semcode);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}
	
	
	public function checkExistEntry($idSemester,$idSubject,$idProgram,$idGroup) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array('Count(*) as Total'))
							    ->where("mdm.semester = ?",$idSemester)
								//->where("mdm.IdProgram = ?",$idProgram) 31-3-2015 mark dist by group x perlu tag program
								->where("mdm.IdCourseTaggingGroup = ?",$idGroup)	
								->where("mdm.IdCourse = ?",$idSubject)
								->where("mdm.Type = 0 ");

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result['Total'];
	}
	
	public function checkCeExistEntry($idSemester,$idSubject) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array('Count(*) as Total'))
							    ->where("mdm.semester = ?",$idSemester)							
								->where("mdm.IdCourse = ?",$idSubject)
								->where("mdm.IdProgram = ?",0)
								->where("mdm.IdCourseTaggingGroup = ?",0)								
								->where("mdm.Type = 1 ");

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result['Total'];
	}
	
	public function addData($data){	
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $this->insert($data);
	   return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
    public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup,$type=0) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))
								->join(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array('component_name'=>'Description','component_name_my'=>'Description','FinalExam','IdExaminationAssessmentType','Description'))
							    ->where("mdm.semester = ?",$idSemester)							    
								->where("mdm.IdCourse = ?",$idSubject)
								->order('eat.sorting');
								
								if($type==1){
									$sql->where("mdm.Type= 1");
								}else{
									$sql->where("mdm.Type= 0");
									//$sql->where("mdm.IdProgram = ?",$idProgram);//31-3-2015 mark dist by group x perlu tag program
									$sql->where("mdm.IdCourseTaggingGroup = ?",$idGroup);
								}
	
		$result = $this->lobjDbAdpt->fetchAll($sql);
		
		return $result;
	}
	
	public function getTotalMark($idSemester,$idProgram,$idSubject) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array('TotalMark'=>'SUM(Marks)'))
								->joinLeft(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array())
							    ->where("mdm.semester = ?",$idSemester)
								//->where("mdm.IdProgram = ?",$idProgram)
								->where("mdm.IdCourse = ?",$idSubject)
								->where("mdm.Type= 0")				
								->where(" mdm.Status = 243 OR mdm.Status = 193 "); //Entry or approved

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result["TotalMark"];
	}
	
	
	
	public function getInfoComponent($id) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))	
										->joinLeft(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array('component_name'=>'DescriptionDefaultlang'))							
							    		->where("mdm.IdMarksDistributionMaster = ?",$id);

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
	}
	
	public function getListMainComponentSubject($idSemester,$idProgram) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))
								->joinLeft(array('c'=>'tbl_subjectmaster'),'c.IdSubject=mdm.IdCourse',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
							    ->where("mdm.semester = ?",$idSemester)
								//->where("mdm.IdProgram = ?",$idProgram)	
								->where("mdm.Type= 0")
								->group("mdm.IdCourse"); 

		$result = $this->lobjDbAdpt->fetchAll($sql);
		
		return $result;
	}
	
	
	public function getComponent($idSemester,$idProgram,$idSubject,$idGroup) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))
							    ->where("mdm.semester = ?",$idSemester)
								//->where("mdm.IdProgram = ?",$idProgram)
								->where("mdm.Type= 0")
								->where("mdm.IdCourse = ?",$idSubject)	
								->where("mdm.IdCourseTaggingGroup = ?",$idGroup);

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
	}
	
	
	public function getListCopyComponent($idSemester,$idProgram,$idSubject,$idGroup) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()
								->from(array("mdm" =>"tbl_marksdistributionmaster"))
								->join(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array('FinalExam'))
							    ->where("mdm.semester = ?",$idSemester)
								//->where("mdm.IdProgram = ?",$idProgram)
								->where("mdm.Type= 0")
								->where("mdm.IdCourse = ?",$idSubject)	
								->where("mdm.IdCourseTaggingGroup = ?",$idGroup);

		$result = $this->lobjDbAdpt->fetchAll($sql);
		
		return $result;
	}
	
	public function getListByLecturer($idSemester,$idProgram,$idSubject,$formData=null) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		 $sql =  $db->select()
						->from(array("mdm" =>"tbl_marksdistributionmaster"))
						->join(array('mdd'=>'tbl_marksdistributiondetails'),'mdd.IdMarksDistributionMaster=mdm.IdMarksDistributionMaster',array('UpdUser'))
						->join(array('u'=>'tbl_user'),'u.IdUser=mdd.UpdUser',array())
						->join(array('s'=>'tbl_staffmaster'),'s.IdStaff=u.IdStaff',array('FullName'))											
					    ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=mdm.IdScheme')
                        ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
					    ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
					    ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
                   
					    ->where("mdm.semester = ?",$idSemester)
						//->where("mdm.IdProgram = ?",$idProgram)
						->where("mdm.Type= 0")
						->where("mdm.IdCourse = ?",$idSubject)														
						->where("mdm.Status = 243 OR mdm.Status = 193 ")//Entry or approved											
						->group("mdm.IdProgram")
						->group("mdm.IdCourse")
						->group("mdm.semester")
						->group("mdm.IdScheme")
						->group("mdd.UpdUser"); 
											
						if(isset($formData['IdProgramScheme']) && $formData['IdProgramScheme']!=''){
							$sql->where("mdm.IdScheme = ?",$formData['IdProgramScheme']);
						}
		$result = $db->fetchAll($sql);
		//echo $sql;
		return $result;
	}
	
	
	public function getStatusComponent($idSemester,$idProgram,$idSubject,$idGroup) {
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
	
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))	
		  							      // ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=mdm.Status', array('DefinitionDesc','BahasaIndonesia'))						
							    		   ->where("mdm.IdCourseTaggingGroup = ?",$idGroup)
							    		  // ->where("mdm.IdProgram = ?",$idProgram)							    		  
							    		   ->where("mdm.IdCourse = ?",$idSubject)
							    		   ->where("mdm.semester = ?",$idSemester)
							    		   ->group('mdm.IdProgram')							    		  
							    		   ->group('IdCourseTaggingGroup');
							    		   
		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
	}
	
	public function getComponentByAssessmentId($IdComponentType,$idGroup,$idSemester,$idProgram,$idSubject) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))								
							    ->where("mdm.IdComponentType = ?",$IdComponentType)
							    ->where("mdm.semester = ?",$idSemester)
								//->where("mdm.IdProgram = ?",$idProgram)
								->where("mdm.Type= 0")
								->where("mdm.IdCourse = ?",$idSubject)
								->where("mdm.IdCourseTaggingGroup = ?",$idGroup);

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
	}
	
	
	public function getCeComponentByAssessmentId($IdComponentType,$idSemester,$idSubject) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))								
							    ->where("mdm.IdComponentType = ?",$IdComponentType)
							    ->where("mdm.semester = ?",$idSemester)								
								->where("mdm.IdCourse = ?",$idSubject);

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
	}
	
	public function updateApprovalData($data,$where){
		
		$this->update($data, $where);
	}
	
	public function searchMarkDistSubjectCe($formData=null){		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('sm'=>'tbl_subjectmaster'),array('IdSubject','SubCode','SubjectName','subjectMainDefaultLanguage'))
					  ->join(array('mdm'=>'tbl_marksdistributionmaster'),'mdm.IdCourse=sm.IdSubject',array())					
					  ->where('sm.CourseType = 20')
					  ->where('mdm.Type = 1')					 				
					  ->group('mdm.semester')
					  ->group('mdm.IdCourse');

		 if(isset($formData['Status']) && $formData['Status']!=''){
		 	
		 	if($formData['Status']=='Entry'){
		 		$select->where('mdm.Status = 0');
		 	}else{
		 		$select->where('mdm.Status = ?',$formData['Status']);
		 	}
		 } 
		 
		 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
		 			 $select->where('mdm.semester = ?',$formData['IdSemester']);
		 } 
		 
		 if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
		 			 $select->where('mdm.IdCourse = ?',$formData['IdSubject']);
		 }
		 
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getCeInfo($idSemester,$idSubject) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))
							    ->where("mdm.semester = ?",$idSemester)							
								->where("mdm.IdCourse = ?",$idSubject)
								->where("mdm.IdProgram = ?",0)
								->where("mdm.IdCourseTaggingGroup = ?",0)								
								->where("mdm.Type = 1 ")
								->group("mdm.semester")
								->group("mdm.IdCourse");

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
	}

	function getSubjectAssignLecture($formData=null){
		/*
		 * SELECT mdm.*, mdd.* FROM `tbl_marksdistributionmaster` as mdm JOIN tbl_marksdistributiondetails as mdd ON mdd.`IdMarksDistributionMaster`=mdm.`IdMarksDistributionMaster` WHERE mdm.`IdCourse` = 3668 AND mdm.`semester` = 3 AND IdLecturer=3195 
		 */
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('mdm'=>'tbl_marksdistributionmaster'),array())
					  ->join(array('mdd'=>'tbl_marksdistributiondetails'),'mdd.IdMarksDistributionMaster=mdm.IdMarksDistributionMaster',array())
					  ->join(array('sm'=>'tbl_subjectmaster'),'mdm.IdCourse=sm.IdSubject',array('IdSubject','SubCode','SubjectName','subjectMainDefaultLanguage'))					
					  ->where('sm.CourseType = 20')
					  ->where('mdm.Type = 1')					 				
					  ->where('mdm.semester = ?',$formData['IdSemester'])					  
					  //->where('mdd.IdLecturer = 3195')
					  ->group("mdm.semester")
					  ->group("mdm.IdCourse");
					  
					  if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){
					  		$select->where('mdd.IdLecturer = ?',$auth->getIdentity()->IdStaff);
					  }
					  
		//echo $select;			  
		$result = $this->lobjDbAdpt->fetchAll($select);
		
		return $result;
	}
	
	
	function getMyListMainComponent($idSemester,$idSubject){
		
		//IMPORTANT: FOR CE ONLY
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('mdm'=>'tbl_marksdistributionmaster'))
					  ->join(array('mdd'=>'tbl_marksdistributiondetails'),'mdd.IdMarksDistributionMaster=mdm.IdMarksDistributionMaster',array())
					  ->join(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array('component_name'=>'Description','component_name_my'=>'Description','FinalExam'))
					  ->where('mdm.semester = ?',$idSemester)		
					  ->where("mdm.IdCourse = ?",$idSubject)
					  ->where("mdm.Type= 1")
					  ->order('eat.sorting')
					  ->group('mdd.IdMarksDistributionMaster');
					  
	 				  /*if($auth->getIdentity()->IdRole==400){
					  	$select->where('mdd.IdLecturer = ?',$auth->getIdentity()->IdStaff);
					  }*/
					  
						if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){
					  		$select->where('mdd.IdLecturer = ?',$auth->getIdentity()->IdStaff);
					  }
					  
		$result = $this->lobjDbAdpt->fetchAll($select);
		
		return $result;
	}
	
	function getDeanApprovalListMainComponent($idSemester,$idSubject){
		
		//IMPORTANT: FOR CE ONLY
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('mdm'=>'tbl_marksdistributionmaster'))
					  ->join(array('mdd'=>'tbl_marksdistributiondetails'),'mdd.IdMarksDistributionMaster=mdm.IdMarksDistributionMaster',array())
					  ->join(array('eat'=>'tbl_examination_assessment_type'),'eat.IdExaminationAssessmentType=mdm.IdComponentType',array('component_name'=>'Description','component_name_my'=>'Description','FinalExam'))
					  ->where('mdm.semester = ?',$idSemester)		
					  ->where("mdm.IdCourse = ?",$idSubject)
					  ->where("mdm.Type= 1")
					  ->order('eat.sorting')
					  ->group('mdd.IdMarksDistributionMaster');
					
					  
		$result = $this->lobjDbAdpt->fetchAll($select);
		
		return $result;
	}

	public function getListMainComponentByGroup($idGroup) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"))
										   ->where('mdm.IdCourseTaggingGroup = ?',$idGroup);

		$result = $this->lobjDbAdpt->fetchAll($sql);
		
		return $result;
	}
	
	public function revertStatus($data,$idGroup){
		 $this->update($data, 'IdCourseTaggingGroup = '. (int)$idGroup);
	}
	
	public function addHistory($data){	
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $db->insert('tbl_marksdistributionmaster_history',$data);
	}
	
	public function isMarkEntered($idGroup){
		
		$sql =  $this->lobjDbAdpt->select()->from(array("mdm" =>"tbl_marksdistributionmaster"),array())
										   ->join(array('sme'=>'tbl_student_marks_entry'),'mdm.IdMarksDistributionMaster = sme.IdMarksDistributionMaster')
										   ->where('mdm.IdCourseTaggingGroup = ?',$idGroup);

		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return $result;
		
	}
}