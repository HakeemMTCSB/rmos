<?php 
class Examination_Model_DbTable_StudentRegistration extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'tbl_studentregistration';
	protected $_primary = "";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getSemesterByProgramIntake($idProgram,$idIntake){	
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db->select()
					  ->from(array('sr'=>$this->_name),array())
					  ->joinLeft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sr.IdSemesterMain',array('IdSemesterMaster','SemesterMainName'))
					  ->where('sr.IdProgram = ?',$idProgram)
					  ->where('sr.IdIntake = ?',$idIntake)
					  ->group('sm.IdSemesterMaster');					 
					  
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	
	public function getSubjectByProgramIntake($idProgram,$idIntake,$idSemester){	
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		 $select = $db->select()
					  ->from(array('sr'=>$this->_name),array())
					  ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration',array('IdSubject'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName'=>'subjectMainDefaultLanguage'))
					  ->where('sr.IdProgram = ?',$idProgram)
					  ->where('sr.IdIntake = ?',$idIntake)
					  ->where('sr.IdSemesterMain = ?',$idSemester)
					  ->group('srs.IdSubject');					 
					  
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getStudentByProgramIntakeSemester($idProgram,$idIntake,$idSemester){	
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db->select()
					  ->from(array('sr'=>$this->_name),array())
					  ->joinLeft(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
					  ->joinLeft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sr.IdSemesterMain',array('IdSemesterMaster','SemesterMainName'))
					  ->where('sr.IdProgram = ?',$idProgram)
					  ->where('sr.IdIntake = ?',$idIntake)
					  ->group('sm.IdSemesterMaster');					 
					  
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getStudentListbyGroup($idSemester,$idProgram,$idSubject,$idGroup,$formData=null){
		
			$db = Zend_Db_Table::getDefaultAdapter();	
			
			//list all student dalam course group
			$select = $db ->select()
						->from(array('gsm'=>'tbl_course_group_student_mapping'))					
						->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = gsm.IdStudent',array('IdStudentRegistration','registrationId'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->where('sr.IdProgram = ?',$idProgram)					
						->where('gsm.IdCourseTaggingGroup = ?',$idGroup)
						->where('srs.IdSubject = ?',$idSubject)
						->where('srs.IdSemesterMain = ?',$idSemester)
						->order('sr.registrationId');
			
		 	if(isset($formData)){
			 	
			 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
			 		$select->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
			 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
			 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
			 	}
			 	
			    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
			 		$select->where("sr.registrationId = ?",$formData["student_id"]);
			 	}
			 }
			 
			$rows = $db->fetchAll($select);
			
			
					
				/*echo '<pre>';
				print_r($rows);
				echo '</pre>';*/
			return $rows;
			
	}
	
	
	public function getStudentEntrybyGroup($idSemester,$idProgram,$idSubject,$idGroup,$formData=null, $check = true){
		
			 $db = Zend_Db_Table::getDefaultAdapter();

			 $auth = Zend_Auth::getInstance();  
			 
			 $semDB = new Registration_Model_DbTable_ExamRegistration();
			 $semester_all = $semDB->getSemester($idSemester);
						 
			//list all student dalam course group
			$sql1 = $db ->select()
						//->from(array('gsm'=>'tbl_course_group_student_mapping'))					
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('AssessmentMethod'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approval_status','mark_approveby','final_course_mark','grade_point','grade_name','approval_remarks','exam_status','audit_program'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->joinLeft(array('er'=>'exam_registration'),'er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject AND er.er_idSemester=srs.IdSemesterMain',array('er_attendance_status'))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance'=>'DefinitionDesc'))						
						->where('srs.IdCourseTaggingGroup = ?',$idGroup)
						->where('srs.IdSubject = ?',$idSubject)
						->where('srs.IdSemesterMain = ?',$idSemester)
						//->where('sr.profileStatus = ?',92)
						->where('srs.Active!=3')
						->where('sr.registrationId != ?',"1409999")
					 	->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT","U")');


			if ($check == true){
				$sql1->where('sr.profileStatus = ?',92);
			}
						
						
			 //audit program			
			 $sql2 = $db ->select()
						//->from(array('gsm'=>'tbl_course_group_student_mapping'))					
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('AssessmentMethod'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approval_status','mark_approveby','final_course_mark','grade_point','grade_name','approval_remarks','exam_status','audit_program'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->joinLeft(array('er'=>'exam_registration'),'er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject AND er.er_idSemester=srs.IdSemesterMain',array('er_attendance_status'))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance'=>'DefinitionDesc'))						
						->where('srs.IdCourseTaggingGroup = ?',$idGroup)
						->where('srs.IdSubject = ?',$idSubject)
						->where('srs.IdSemesterMain IN (?)',$semester_all)
						->where('srs.audit_program IS NOT NULL')    
						//->where('sr.profileStatus = ?',92)
						->where('srs.Active!=3')
						->where('sr.registrationId != ?',"1409999")
					 	->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")');

			if ($check == true){
				$sql2->where('sr.profileStatus = ?',92);
			}
					 	
			
		 	if(isset($formData)){
			 	
			 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
			 		$sql1->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
			 		$sql1->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
			 		$sql2->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
			 		$sql2->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
			 	}
			 	
			    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
			 		$sql1->where("sr.registrationId = ?",$formData["student_id"]);
			 		$sql2->where("sr.registrationId = ?",$formData["student_id"]);
			 	}
			 }
			 
			 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin		
			 	$sql1->join(array('g'=>'tbl_course_tagging_group'),'g.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array()); 	
			 	$sql1->where('g.IdLecturer = ?',$auth->getIdentity()->IdStaff);
			 	$sql2->join(array('g'=>'tbl_course_tagging_group'),'g.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array()); 	
			 	$sql2->where('g.IdLecturer = ?',$auth->getIdentity()->IdStaff);
			 }
			 
				// echo $select;
			   $select = $db->select()->union(array($sql1, $sql2))
	    					->order('appl_fname')
						 	->order('appl_lname')
							->order('registrationId');
                  
				$rows = $db->fetchAll($select);
			
			return $rows;
			
	}
	
	public function getApprovalStudentListbyGroup($idSemester,$idProgram,$idSubject,$idGroup,$formData=null){
		
			$db = Zend_Db_Table::getDefaultAdapter();	
			
			//list all student dalam course group
			$select = $db ->select()					
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby','final_course_mark','grade_point','grade_name','mark_approval_status','audit_program','exam_status'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject AND er.er_idSemester = srs.IdSemesterMain',array('er_attendance_status'))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance'=>'DefinitionDesc'))	
						->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('AssessmentMethod'))				
						//->where('sr.IdProgram = ?',$idProgram)
						->where('sr.profileStatus = ?',92)
						->where('srs.Active!=3')
						->where('sr.registrationId != ?',"1409999")					
						->where('srs.IdCourseTaggingGroup = ?',$idGroup)
						//->where('srs.IdSubject = ?',$idSubject)
						//->where('srs.IdSemesterMain = ?',$idSemester)						
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
			
		 	if(isset($formData)){
			 	
			 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
			 		$select->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
			 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
			 	}
			 	
			    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
			 		$select->where("sr.registrationId = ?",$formData["student_id"]);
			 	}
			 	
		 		if(isset($formData["status"]) && $formData["status"]!=''){
			 		$select->where("srs.mark_approval_status = ?",$formData["status"]);
			 	}
			 	
			 }else{
			 	$select->where('srs.mark_approval_status = 1');
			 }
			 
			$rows = $db->fetchAll($select);
			
			return $rows;
			
	}
	
	
	public function getStudentWithApprovedMark($idSemester,$idProgram,$idSubject,$idGroup,$formData=null){
		
			$db = Zend_Db_Table::getDefaultAdapter();	
					
			$select = $db ->select()					
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby','final_course_mark','grade_point','grade_name','mark_approval_status','audit_program','exam_status'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject AND er.er_idSemester = srs.IdSemesterMain',array('er_attendance_status'))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance'=>'DefinitionDesc'))	
						->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('AssessmentMethod'))				
						//->where('sr.IdProgram = ?',$idProgram)
						//->where('sr.profileStatus = ?',92)
						->where('srs.Active!=3')
						->where('sr.registrationId != ?',"1409999")					
						->where('srs.IdCourseTaggingGroup = ?',$idGroup)
						->where('srs.mark_approval_status = 2')
						//->where('srs.IdSubject = ?',$idSubject)
						//->where('srs.IdSemesterMain = ?',$idSemester)						
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
						
		 	if(isset($formData)){
			 	
			 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
			 		$select->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");			 		
			 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
			 	}
			 	
			    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
			 		$select->where("sr.registrationId = ?",$formData["student_id"]);
			 	}
			 }
			 
			$rows = $db->fetchAll($select);
			
			return $rows;
			
	}
	
	//nak dapatkan student yg register subject ni dan active pada semester tersebut dan kalo dah ada mark entry amik info marks dia
	public function getStudentList($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster,$formData=null){	
	
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
					  ->from(array('sr'=>$this->_name),array('IdStudentRegistration','registrationId'))
					  ->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration = sr.IdStudentRegistration',array())
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects'))
					  ->joinLeft(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName'=>'BahasaIndonesia','SubCode'))
					 // ->joinLeft(array('sme'=>'tbl_student_marks_entry'),'sme.IdStudentRegSubjects = srs.IdStudentRegSubjects',array('IdStudentMarksEntry','TotalMarkObtained','IdMarksDistributionMaster','MarksEntryStatus'))
					  ->where('sr.profileStatus=92')  // Active (refer defination table  type=20)
					  ->where('sss.studentsemesterstatus=130')  //Register pada semester tersebut
					  ->where('srs.Active=1') //Subject Status 1:Register 2:Add&Drop 3:Withdraw
					  ->where('sr.IdProgram = ?',$idProgram)
					  ->where('sss.IdSemesterMain = ?',$idSemester)
					  ->where('srs.IdSubject = ?',$idSubject)					 
					 // ->where("sme.MarksEntryStatus IS NULL OR sme.MarksEntryStatus = '407'") //407=>ENTRY 409=>SUBMITTED 411=>APPROVED					
					  ->order("sr.registrationId");
					  
		 if(isset($formData)){
		 	
		 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
		 		$select->where("sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
		 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
		 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%'");
		 	}
		 	
		    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
		 		$select->where("sr.registrationId = ?",$formData["student_id"]);
		 	}
		 }
		 
		 
		 $rows = $db->fetchAll($select);	
		
		     //ini nak filter jika dah ada masuk markah amik yg status entry sahaja dan amik info markah yg berdasasrkan markdistribution component
		     $i=0;
			 foreach($rows as $key => $row){

			 	    $rows[$i]['IdStudentMarksEntry']='';
			        $rows[$i]['TotalMarkObtained']='';
			        
				 	//check ada x mark entry
				 	$select_mark = $db->select()
				 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
				 	 				  ->where('sme.IdStudentRegistration = ?',$row["IdStudentRegistration"])
				 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
				 	 				  ->where('sme.IdStudentRegSubjects = ?',$row["IdStudentRegSubjects"])
				 	 				  ->where('sme.IdSemester = ?',$idSemester);
				 	 				  
				 	$entry_list = $db->fetchRow($select_mark);	
				 	
				 
				 	if(isset($entry_list["IdStudentMarksEntry"]) && $entry_list["IdStudentMarksEntry"]!=''){
				 		
			             //check mark entry status	407=>ENTRY 409=>SUBMITTED 411=>APPROVED					    		
		    			if($entry_list["MarksEntryStatus"]==409 || $entry_list["MarksEntryStatus"]==411 ){		    				
		    				unset($rows[$key]);			    			
			    		}else{
			    			$rows[$i]['IdStudentMarksEntry']=$entry_list["IdStudentMarksEntry"];
			    			$rows[$i]['TotalMarkObtained']=$entry_list["TotalMarkObtained"];
			    		}//end if
				 	}
			 
			 $i++;
			 }//end foreach
			 
			// print_r($rows);
			
		return $rows;
			 
	}
	
	
	
	
	public function getStudentListMarkSubmitted($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster,$idGroup,$formData=null){	
	
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		//list all student dalam course group
		$select = $db ->select()
					->from(array('gsm'=>'tbl_course_group_student_mapping'))
					->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = gsm.IdStudent',array('IdStudentRegistration','registrationId'))
					->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects'))
					->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
					->where('sr.IdProgram = ?',$idProgram)					
					->where('gsm.IdCourseTaggingGroup = ?',$idGroup)
					->where('srs.IdSubject = ?',$idSubject)
					->order('sr.registrationId');
		
	 	if(isset($formData)){
		 	
		 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
		 		$select->where("sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
		 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
		 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%'");
		 	}
		 	
		    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
		 		$select->where("sr.registrationId = ?",$formData["student_id"]);
		 	}
		 }
		 		
		$rows = $db->fetchAll($select);
			
		
	
		
		//unset student yg tak attend exam		
		foreach($rows as $key=>$student){
						
			//cari exam group student ni
			$examGroupDb =new Examination_Model_DbTable_ExamGroupStudent();
			$exam_group = $examGroupDb->checkStudentGroup($student["IdStudentRegistration"],$idSubject,$idSemester);
			
			if(is_array($exam_group)){
				
				//get exam attendance
		    	$examAttendanceDb = new Examination_Model_DbTable_ExamGroupAttendance();
		    	$attendance = $examAttendanceDb->getData($exam_group["egst_group_id"],$student["IdStudentRegistration"]);
				
		    	//jika x hadir atau x ada 
		    	if($attendance["ega_status"]!='395' || !$attendance){ //Defination type=31 Hadir=395
		    		unset($rows[$key]);		    		
		    		
		    	}else{
		    		
		    		    $rows[$key]['IdStudentMarksEntry']='';
				        $rows[$key]['TotalMarkObtained']='';
				        
					 	//check ada x mark entry
					 	$select_mark = $db->select()
					 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
					 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])
					 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
					 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"])
					 	 				  ->where('sme.IdSemester = ?',$idSemester)
					 	 				  ->where('sme.MarksEntryStatus = 411');
					 	 				  
					 	$entry_list = $db->fetchRow($select_mark);	
					 	
					 
					 	if(isset($entry_list["IdStudentMarksEntry"])){
					 		
					 		$rows[$key]['MarksEntryStatus']=$entry_list["MarksEntryStatus"];					 		
					 		$rows[$key]['IdStudentMarksEntry']=$entry_list["IdStudentMarksEntry"];
				    		$rows[$key]['TotalMarkObtained']=$entry_list["TotalMarkObtained"];
				    		
					 	}else{
					 			unset($rows[$key]);	
					 	}
		    	}
			
			}else{
				//jika tiada exam group consider student tu tak attend any exam
				unset($rows[$key]);					
			}//end exam group exist
			
		}//end foreacch
		
			
		/*echo '<pre>';
		print_r($rows);
		echo '</pre>';*/
		
		
		return $rows;
	}
	
	
	
	//nak dapatkan student yg register subject ni dan active pada semester tersebut dan nak mapping utk omr batch upload
	public function getStudentMapping($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster,$nim){	
	
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
					  ->from(array('sr'=>$this->_name),array('IdStudentRegistration','registrationId'))
					  ->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration = sr.IdStudentRegistration',array())
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects'))
					  ->joinLeft(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
					  //->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName'=>'BahasaIndonesia','SubCode'))
					 // ->joinLeft(array('sme'=>'tbl_student_marks_entry'),'sme.IdStudentRegSubjects = srs.IdStudentRegSubjects',array('IdStudentMarksEntry','TotalMarkObtained','IdMarksDistributionMaster','MarksEntryStatus'))
					  ->where('sr.profileStatus=92')  // Active (refer defination table  type=20)
					  ->where('sss.studentsemesterstatus=130')  //Register
					  ->where('srs.Active=1') //1:Register 2:Add&Drop 3:Withdraw
					  ->where('sr.IdProgram = ?',$idProgram)
					  ->where('sss.IdSemesterMain = ?',$idSemester)
					  ->where('srs.IdSubject = ?',$idSubject)	
					  ->where('sr.registrationId = ?',$nim)					 
					 // ->where("sme.MarksEntryStatus IS NULL OR sme.MarksEntryStatus = '407'") //407=>ENTRY 409=>SUBMITTED 411=>APPROVED					
					  ->order("sr.registrationId");					  
		 
		 
		 $row = $db->fetchRow($select);		 		 
		
		 
		 if(is_array($row)){
		
		 	//check ada x mark entry sebelum ni
		 	$select_mark = $db->select()
		 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
		 	 				  ->where('sme.IdStudentRegistration = ?',$row["IdStudentRegistration"])
		 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
		 	 				  ->where('sme.IdStudentRegSubjects = ?',$row["IdStudentRegSubjects"])
		 	 				  ->where('sme.IdSemester = ?',$idSemester);
		 	 				  
		   $entry_list = $db->fetchRow($select_mark);		 	
		 	
			if(isset($entry_list["IdStudentMarksEntry"]) && $entry_list["IdStudentMarksEntry"]!=''){
				$row['IdStudentMarksEntry']=$entry_list["IdStudentMarksEntry"];
		    	$row['TotalMarkObtained']=$entry_list["TotalMarkObtained"];
		    	$row['MarksEntryStatus']=$entry_list["MarksEntryStatus"];
			}else{
		 	
			 	$row['IdStudentMarksEntry']=null;
			    $row['TotalMarkObtained']=null;
			    $row['MarksEntryStatus']=null;
		    	
		 	}
			
		 }
		 		 	
		 return $row;	
	}
	
	
	//ini nak dapatkan senarai student yg register pada semester tersebut active sahaja
	public function getStudent($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster,$formData=null){	
	
				$db = Zend_Db_Table::getDefaultAdapter();
				
				$select = $db->select()
					  ->from(array('sr'=>$this->_name),array('IdStudentRegistration','registrationId'))
					  ->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration = sr.IdStudentRegistration',array())
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects'))
					  ->joinLeft(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('student_name'=>"CONCAT(appl_fname,' ',appl_mname,' ',appl_lname)"))
					  //->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName'=>'BahasaIndonesia','SubCode'))					 
					  ->where('sr.profileStatus=92')  // Active (refer defination table  type=20)
					  ->where('sss.studentsemesterstatus=130')  //Register pada semester tersebut
					  ->where('srs.Active=1') //Subject Status 1:Register 2:Add&Drop 3:Withdraw
					  ->where('sr.IdProgram = ?',$idProgram)
					  ->where('sss.IdSemesterMain = ?',$idSemester)
					  ->where('srs.IdSubject = ?',$idSubject)	
					  ->order("sr.registrationId");
					  
				 if(isset($formData)){
				 	
				 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
				 		$select->where("sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
				 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
				 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%'");
				 	}
				 	
				    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
				 		$select->where("sr.registrationId = ?",$formData["student_id"]);
				 	}
				 }
				 
				 
				 $rows = $db->fetchAll($select);

				 return $rows;
	}
	
	
	public function getRegistrationInfo($nim){
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
			         ->from(array('sr'=>$this->_name))
			         ->where('sr.registrationId = ?',$nim);
			  
		$row = $db->fetchRow($select);

		return $row;	  
	}
	
	
	public function getAllStudentRegisterSubject($idSemester,$idSubject,$formData=null){	
	
				$db = Zend_Db_Table::getDefaultAdapter();
				
				$select = $db->select()
					  ->from(array('sr'=>$this->_name),array('IdStudentRegistration','registrationId','IdProgram'))
					  ->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration = sr.IdStudentRegistration',array())
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects'))
					  ->joinLeft(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('student_name'=>"CONCAT(appl_fname,' ',appl_mname,' ',appl_lname)"))
					  ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('program_name'=>"CONCAT(ArabicName,' - ',ProgramCode)"))
					  //->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName'=>'BahasaIndonesia','SubCode'))					 
					  ->where('sr.profileStatus=92')  // Active (refer defination table  type=20)
					  ->where('sss.studentsemesterstatus=130')  //Register pada semester tersebut
					  ->where('srs.Active=1') //Subject Status 1:Register 2:Add&Drop 3:Withdraw					
					  ->where('sss.IdSemesterMain = ?',$idSemester)
					  ->where('srs.IdSubject = ?',$idSubject)	
					  ->order("sr.registrationId");
					  
				 if(isset($formData)){
				 	
				 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
				 		$select->where("sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
				 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
				 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%'");
				 	}
				 	
				    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
				 		$select->where("sr.registrationId = ?",$formData["student_id"]);
				 	}
				 }
				 
				 
				 $rows = $db->fetchAll($select);

				 return $rows;
	}
	
	
	public function getRegistrationSubject($idSemester,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
		 		     ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects'))
		 		     ->where('srs.IdSemesterMain = ?',$idSemester)
		 		     ->where('srs.IdSubject = ?',$idSubject);
	}
	
	
	
	public function getStudentAttendExambyCourseGroup($idProgram,$idGroup,$idSubject,$idSemester,$IdMarksDistributionMaster,$formData=null){
			
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		//list all student dalam course group
		$select = $db ->select()
					->from(array('gsm'=>'tbl_course_group_student_mapping'))					
					->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = gsm.IdStudent',array('IdStudentRegistration','registrationId'))
					->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby'))
					->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
					->where('sr.IdProgram = ?',$idProgram)					
					->where('gsm.IdCourseTaggingGroup = ?',$idGroup)
					->where('srs.IdSubject = ?',$idSubject)
					->where('srs.IdSemesterMain = ?',$idSemester)
					->order('sr.registrationId');
		
	 	if(isset($formData)){
		 	
		 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
		 		$select->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
		 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
		 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
		 	}
		 	
		    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
		 		$select->where("sr.registrationId = ?",$formData["student_id"]);
		 	}
		 }
		 
		$rows = $db->fetchAll($select);
			
		
	
		//print_r($rows);
		
		foreach($rows as $key=>$student){
			
			
			//cari exam group student ni
			$examGroupDb =new Examination_Model_DbTable_ExamGroupStudent();
			$exam_group = $examGroupDb->checkStudentGroup($student["IdStudentRegistration"],$idSubject,$idSemester,$IdMarksDistributionMaster);
			
			if(is_array($exam_group)){
				
				$rows[$key]['exam_group_id']=$exam_group["egst_group_id"];
								        
				//get exam attendance
		    	$examAttendanceDb = new Examination_Model_DbTable_ExamGroupAttendance();
		    	$attendance = $examAttendanceDb->getData($exam_group["egst_group_id"],$student["IdStudentRegistration"]);
				
		    	//jika x hadir atau x ada record  set status tak hadir
		    	if($attendance["ega_status"]!='395' || !$attendance){ //Defination type=31 Hadir=395
		    			
		    			$rows[$key]['attendance']='Not Attend';
		    		
		    	}else{		    		
		    		  
		    		    $rows[$key]['attendance']='Attend';
		    	}	    
			
			}else{
				//jika tiada exam group consider student tu tak attend any exam				
				$rows[$key]['exam_group_id']='';
				$rows[$key]['attendance']='No Exam Group';
			}//end exam group exist
			
			
			
			//check ada x mark entry
		 	$select_mark = $db->select()
		 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
		 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])
		 	 				  ->where('sme.IdMarksDistributionMaster = ?',$IdMarksDistributionMaster)
		 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"])
		 	 				  ->where('sme.IdSemester = ?',$idSemester);					 	 				  
		 	$entry_list = $db->fetchRow($select_mark);	
		 	
		 
		 	if(isset($entry_list["IdStudentMarksEntry"]) && $entry_list["IdStudentMarksEntry"]!=''){					 		
		 		$rows[$key]['MarksEntryStatus']=$entry_list["MarksEntryStatus"];					 		
		 		$rows[$key]['IdStudentMarksEntry']=$entry_list["IdStudentMarksEntry"];
	    		$rows[$key]['TotalMarkObtained']=$entry_list["TotalMarkObtained"];
	    		$rows[$key]['FinalTotalMarkObtained']=$entry_list["FinalTotalMarkObtained"];
		 	}else{
		 		
		 		//defaultkan mark entry info null
				$rows[$key]['IdStudentMarksEntry']='';
			    $rows[$key]['TotalMarkObtained']='';
			    $rows[$key]['FinalTotalMarkObtained']='';
		        
						
		 	}
			
		}//end foreacch
		
			
		/*echo '<pre>';
		print_r($rows);
		echo '</pre>';*/
		
		
		return $rows;
	}
	
	
	public function getRegisterSubject($idCourseGroup,$IdStudentRegistration){
		$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
		 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))
		 	 				  ->where('srs.IdCourseTaggingGroup = ?',$idCourseGroup)
		 	 				  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration); 	 				  
		 	return $row = $db->fetchRow($select);	 				 
	}
	
	
	public function getDatabyId($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
			         ->from(array('sr'=>$this->_name))
			         ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('IdScheme'))
			         ->where('sr.IdStudentRegistration = ?',$id);
			  
		$row = $db->fetchRow($select);

		return $row;	  
	}
	
	
	public function getStudentRegistration($formdata=null){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
			         ->from(array('sr'=>$this->_name))
			         ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('StudentName'=>"CONCAT_WS(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname)"))
					 ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('status'=>'DefinitionDesc'))
					 ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName'))
					 ->order('appl_fname')
					 ->order('appl_lname');
					
		if(isset($formdata)){	

			if(isset($formdata["idIntake"]) && $formdata["idIntake"]!=''){
			  $select->where("sr.IdIntake = ?",$formdata["idIntake"]);
			}
			
			if(isset($formdata["IdProgram"]) && $formdata["IdProgram"]!=''){
			  $select->where("sr.IdProgram = ?",$formdata["IdProgram"]);
			}
			
			if(isset($formdata["IdProgramScheme"]) && $formdata["IdProgramScheme"]!=''){
			  $select->where("sr.IdProgramScheme = ?",$formdata["IdProgramScheme"]);
			}
						
			if(isset($formdata["idProfileStatus"]) && $formdata["idProfileStatus"]!=''){
			  $select->where("sr.profileStatus = ?",$formdata["idProfileStatus"]);
			}
			
			if(isset($formdata["student_name"]) && $formdata["student_name"]!=''){
			   $select->where("(sp.appl_fname LIKE '%".$formdata["student_name"]."%'");
			   $select->orwhere("sp.appl_lname LIKE '%".$formdata["student_name"]."%')");
			}
			
			if(isset($formdata["Student"]) && $formdata["Student"]!=''){
			     $select->where("sr.registrationId = ? ",$formdata["Student"]);
			}
			
			
			if(isset($formdata["IdConvo"]) && $formdata["IdConvo"]!=''){
			  $select->join(array('pl'=>'pregraduate_list'),'pl.idStudentRegistration=sr.IdStudentRegistration',array())
			  		 ->where("pl.c_id = ?",$formdata["IdConvo"]);
			}
		}	         
			 
		//echo $select;
		$row = $db->fetchAll($select);

		return $row;	  
	}
	
	public function getSemesterStatus($idStudentReg,$idSemester){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
								->from(array('sss'=>'tbl_studentsemesterstatus')	)
								->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sss.IdSemesterMain')	 							
								->where('sss.IdStudentRegistration = ?',$idStudentReg)
								->where('sss.IdSemesterMain = ?',$idSemester)
								->where('sss.studentsemesterstatus = 130 or studentsemesterstatus=229'); //Register & Completed
		
		return $row = $db->fetchRow($select);
	}
	
	public function getPreviousSemester($idStudentReg,$level){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
								->from('tbl_studentsemesterstatus')								
								->where('IdStudentRegistration = ?',$idStudentReg)
								->where('Level = ?',$level)
								->where('studentsemesterstatus = 130 or studentsemesterstatus=229'); //Register & Completed
		
		return $row = $db->fetchRow($select);
		
		
	
		
	}
	
	
	public function getStudentInfo($id=0){
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('sr'=>$this->_name))
				->join(array('ap'=>'student_profile'),'ap.id=sr.sp_id')
				->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ArabicName','ProgramName','ProgramCode','IdScheme','AwardName'))
				->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('LandscapeType','TotalCreditHours','AssessmentMethod'))			
				->join(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDefaultLanguage','IntakeDesc','sem_year'))
				->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sr.IdProgramScheme')
				->joinLeft(array('ctr'=>'tbl_countries'),'ctr.idCountry = ap.appl_country',array('CountryName'))
				->joinLeft(array('sts'=>'tbl_state'),'sts.idState = ap.appl_state',array('StateName'))
				->joinLeft(array('ct'=>'tbl_city'),'ct.idCity = ap.appl_city',array('CityName'))
                ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
                ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
                ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
                ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('StudentStatus'=>'DefinitionDesc'))
				->joinLeft(array('pm'=>'tbl_programmajoring'),'pm.IDProgramMajoring=sr.IDProgramMajoring',array('majoring'=>'BahasaDescription','majoring_english'=>'EnglishDescription'))
				->join(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=sr.IdBranch',array('BranchName'))
				->join(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('NamaKolej'=>'ArabicName','c.IdCollege','CollegeName'))
				->joinLeft(array('sm'=>'tbl_staffmaster'),'sm.IdStaff=sr.AcademicAdvisor',array('AcademicAdvisor'=>'FullName',"FrontSalutation","BackSalutation"))
				->where('sr.IdStudentRegistration = ?',$id);
				
		$row = $db->fetchRow($select);						
		
		return $row;
	}
	
	/*
	 * This function to get course registered by semester.
	 */
	public function getCourseRegisteredBySemester($registrationId,$idSemesterMain,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                     
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain);   
                                           
        if(isset($idBlock) && $idBlock != ''){ //Block 
        	$sql->where("srs.IdBlock = ?",$idBlock);
        	$sql->order("srs.BlockLevel");
        }  
    
             
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	/*
	 * This function to get course registered by semester.
	 */
	public function getCourseRegisteredBySemesterBlock($registrationId,$idSemester,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		 $sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')   
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                  
                        ->where('sr.IdStudentRegistration  = ?', $registrationId)
                        ->where("srs.IdSemesterMain = ?",$idSemester)
                        ->where("srs.subjectlandscapetype != 2")
                        //->where("srs.IdBlock = ?",$idBlock)
                        ->order("srs.BlockLevel");
                      
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	
	public function getStudentLandscapeInfo($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
			         ->from(array('sr'=>$this->_name))
			         ->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('IdLandscape','LandscapeType'))
			         ->where('sr.IdStudentRegistration = ?',$id);
			  
		$row = $db->fetchRow($select);

		return $row;	  
	}
	
	
	public function getStudentSemesterInfo($idStudentReg,$idSemester,$row_sem){
		$db =  Zend_Db_Table::getDefaultAdapter();
						
		$select = $db->select()
								->from(array('sss'=>'tbl_studentsemesterstatus'),array())		
								->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sss.IdSemesterMain',array('IdSemesterMaster'))													
								->where('sss.IdStudentRegistration = ?',$idStudentReg)															
								->where('sss.studentsemesterstatus = 130 or sss.studentsemesterstatus=229') //Register & Completed
								->where('sss.Level != 0')
								->where('sm.SemesterMainStartDate <= ?',$row_sem["SemesterMainStartDate"]);
	
		//$semester = $db->fetchAll($select);
		
		$select_subject = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'))	 
	 	 				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject =srs.IdSubject',array('CreditHours','SubCode','SubjectName','NamaSubjek'=>'BahasaIndonesia','category'))
	 	 				  ->where('srs.IdStudentRegistration = ?',$idStudentReg)
	 	 				  ->where('(srs.subjectlandscapetype =1 OR srs.subjectlandscapetype =2)')	 	 				
	 	 				  ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5') //Status => 1:Register 4:Repeat 5:Refer
	 	 				  //->where('(srs.exam_status != "IN" OR srs.exam_status != "FR" )') //jika exam status IN & FR Credit Hour Not Count
	 	 				  ->where('srs.IdSemesterMain IN (?)',$select)
	 	 				  ->group('srs.IdSubject');
	 	 				  
		return $subjects = $db->fetchAll($select_subject);
		
	}
	
	
	public function SearchRegisterStudent($formdata=null){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
			         ->from(array('sr'=>$this->_name))
			         ->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration=sr.IdStudentRegistration',array('idstudentsemsterstatus','IdSemesterMain'))
			         ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('StudentName'=>"CONCAT_WS(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname)"))
					 ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('status'=>'DefinitionDesc'))
					 ->order('sr.registrationId');
					 					
		if(isset($formdata)){	

			if($formdata["idIntake"]!=''){
			  $select->where("sr.IdIntake = ?",$formdata["idIntake"]);
			}
			
			if($formdata["IdProgram"]!=''){
			  $select->where("sr.IdProgram = ?",$formdata["IdProgram"]);
			}
			
			if($formdata["IdSemester"]!=''){
			  $select->where("sss.IdSemesterMain = ?",$formdata["IdSemester"]);
			}
			
			if(isset($formdata["idProfileStatus"]) && $formdata["idProfileStatus"]!=''){
			  $select->where("sr.profileStatus = ?",$formdata["idProfileStatus"]);
			}
			
			if(isset($formdata["start_nim"]) && $formdata["start_nim"]!=''){			
			  $select->where("sr.registrationId BETWEEN '".$formdata["start_nim"]."' AND '".$formdata["end_nim"]."'");
			}
		}	         
			  
		
		$row = $db->fetchAll($select);

		return $row;	  
	}
	
	
	public function getStudentRegistrationLandscape($formdata=null){		
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$select = $db->select()
			         ->from(array('sr'=>$this->_name))
			         ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ArabicName','ProgramName','ProgramCode','IdScheme'))
			         ->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array())
			         ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('StudentName'=>"CONCAT_WS(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname)"))
					 ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('status'=>'DefinitionDesc'))
					 ->where('l.AssessmentMethod = "point" ');
					
					
		if(isset($formdata)){	
			
			if($formdata["idIntake"]!=''){
			  $select->where("sr.IdIntake = ?",$formdata["idIntake"]);
			}
			
			if($formdata["IdProgram"]!=''){
			  $select->where("sr.IdProgram = ?",$formdata["IdProgram"]);
			}
			
			if(isset($formdata["idProfileStatus"]) && $formdata["idProfileStatus"]!=''){
			  $select->where("sr.profileStatus = ?",$formdata["idProfileStatus"]);
			}
			
			if(isset($formdata["student_name"]) && $formdata["student_name"]!=''){
			  $select->where("(sp.appl_fname LIKE '%".$formdata["student_name"]."%'");
			    $select->orwhere("sp.appl_lname LIKE '%".$formdata["student_name"]."%')");
			}
			
			if(isset($formdata["Student"]) && $formdata["Student"]!=''){
			     $select->where("sr.registrationId LIKE '%".$formdata["Student"]."%'");
			}
		}	         
			  
		
		$row = $db->fetchAll($select);

		return $row;	  
	}
	
	
	public function searchStudentChangeStatus($formData=null){
		
			$db = Zend_Db_Table::getDefaultAdapter();	
			
			//list all student dalam course group
			$select = $db ->select()									
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby','final_course_mark','grade_point','grade_name'))
						->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject =srs.IdSubject',array('SubCode','SubjectName','subjectMainDefaultLanguage'))
						->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = sr.IdStudentRegistration AND er.er_idSemester=srs.IdSemesterMain AND er.er_idSubject=srs.IdSubject',array('er_attendance_status'))
						->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=er.er_attendance_status', array('attendance'=>'IFNULL(DefinitionDesc,"-")'))
						->where('srs.IdSemesterMain = ?',$formData['IdSemester'])
						->where('srs.mark_approval_status = 2')
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
			
		 	if(isset($formData)){
			 	
		 		if(isset($formData["IdSubject"]) && $formData["IdSubject"]!=''){
			 		$select->where("srs.IdSubject = ?",$formData["IdSubject"]);
			 	}
			 	
			 	if(isset($formData["student_name"]) && $formData["student_name"]!=''){
			 		$select->where("(sp.appl_fname  LIKE '%".$formData["student_name"]."%'");
			 		$select->orwhere("sp.appl_mname  LIKE '%".$formData["student_name"]."%'");
			 		$select->orwhere("sp.appl_lname  LIKE '%".$formData["student_name"]."%')");
			 	}
			 	
			    if(isset($formData["student_id"]) && $formData["student_id"]!=''){
			 		$select->where("sr.registrationId = ?",$formData["student_id"]);
			 	}
			 }
			// echo  $select;
			$rows = $db->fetchAll($select);
			
			return $rows;
			
	}
	
	public function searchStudentCeEntryMark($idSemester,$idSubject,$mark_approval_status=null,$formData=null){
		
			$db = Zend_Db_Table::getDefaultAdapter();	
			
			//list all student dalam course group
			$select = $db ->select()									
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgram'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby','final_course_mark','grade_point','grade_name','mark_approval_status','grade_desc'))
						->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = sr.IdStudentRegistration AND er.er_idSemester = srs.IdSemesterMain AND er.er_idSubject=srs.IdSubject',array('er_attendance_status'))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=er.er_attendance_status',array('attendance_status'=>'DefinitionDesc'))
						->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject =srs.IdSubject',array('SubCode','SubjectName','subjectMainDefaultLanguage'))
						->where('srs.IdSemesterMain = ?',$idSemester)	
						->where('srs.IdSubject = ?',$idSubject)
						->where('srs.Active != ?',3)
						->order('sr.registrationId');
						
			
			if(isset($formData) && $formData!=null){
				if(isset($formData['student_name']) && $formData['student_name']!=''){
					$select->where('sp.appl_fname LIKE ?',"%".$formData['student_name']."%");
				}
				
				if(isset($formData['student_name']) && $formData['student_name']!=''){
					$select->where('sr.registrationId LIKE ?',"%".$formData['student_id']."%");
				}
				
				if(isset($formData['status']) && $formData['status']!=''){
					$select->where('srs.mark_approval_status = ?',$formData['status']);
				}
				
			}else{
				if(isset($mark_approval_status) && $mark_approval_status!=''){
					$select->where('srs.mark_approval_status = ?',$mark_approval_status);
				}					
			}
			
			$rows = $db->fetchAll($select);
			
			return $rows;	
	}
	
	
	public function getCeStudentBySemester($idSemester,$idSubject,$status=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
			
			//list all student dalam course group
			$select = $db ->select()
						  ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects'))
						  ->where('srs.IdSemesterMain = ?',$idSemester)
						  ->where('srs.IdSubject = ?',$idSubject);	

						  if(isset($status) && $status!=''){
						  	$select->where('srs.mark_approval_status = ?',$status);
						  }
						  
			$rows = $db->fetchAll($select);
			
			return $rows;
		
	}
	
	public function getStudentRegThesis($formData=null){
		
			$db = Zend_Db_Table::getDefaultAdapter();	
			$auth = Zend_Auth::getInstance();
			
			$deanDB = new GeneralSetup_Model_DbTable_Chiefofprogram();
			$dean_program = $deanDB->isDean($auth->getIdentity()->IdStaff);

			//list all student dalam course group
			$select = $db ->select()					
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approveby','final_course_mark','grade_point','grade_name','mark_approval_status','audit_program','exam_status'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
						->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('AssessmentMethod'))		
						->where('sr.profileStatus IN (92,248,96)')
						->where('srs.Active!=3')
						->where('sr.registrationId != ?',"1409999")	
						->where('srs.IdSubject = ?',$formData['IdSubject'])
						->where('srs.IdSemesterMain = ?',$formData['IdSemester'])						
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');

			if(isset($formData) && $formData!=null){
				if(isset($formData['student_name']) && $formData['student_name']!=''){
					$select->where('(sp.appl_fname LIKE ?',"%".$formData['student_name']."%");
					$select->orwhere('sp.appl_lname LIKE ?)',"%".$formData['student_name']."%");
				}
				
				if(isset($formData['student_id']) && $formData['student_id']!=''){
					$select->where('sr.registrationId LIKE ?',"%".$formData['student_id']."%");
				}				
				
			}
			
			if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){
					//nothing appear all.
			}else{
				$select->where('sr.IdProgram IN (?)',$dean_program);
			}
			
			$rows = $db->fetchAll($select);
			return $rows;
	}
	public function getStudentbyGroupId($idGroup){
		
			 $db = Zend_Db_Table::getDefaultAdapter();

			 $auth = Zend_Auth::getInstance();  
			
			//list all student dalam course group
			$sql1 = $db ->select()
						//->from(array('gsm'=>'tbl_course_group_student_mapping'))					
						->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId'))
						->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','mark_approval_status','mark_approveby','final_course_mark','grade_point','grade_name','approval_remarks','exam_status','audit_program'))
						->join(array('sp'=>'student_profile'), 'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))					
						->where('srs.IdCourseTaggingGroup = ?',$idGroup)
						->where('sr.profileStatus = ?',92)
						->where('srs.Active!=3')
						->where('sr.registrationId != ?',"1409999")
					 	->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
					 	->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
						
				$rows = $db->fetchAll($sql1);
			
			return $rows;
			
	}

	public function getTemplate(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'transcript_template'), array('value'=>'*'));

		$result = $db->fetchRow($select);
		return $result;
	}

	public function insertTemplate($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert('transcript_template', $data);
	}

	public function updateTemplate($data, $id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update('transcript_template', $data, 'trt_id = '.$id);
	}
}
	

?>