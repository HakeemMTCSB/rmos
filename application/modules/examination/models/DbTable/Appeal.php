<?php 

class Examination_Model_DbTable_Appeal extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_student_appeal';
	protected $_primary = "sa_id";
	protected $_appeal_mark = 'tbl_student_appeal_mark';
	protected $_appeal_mark_id = "sam_id";
	

	public function getListApplication($formdata=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('sa'=>$this->_name))
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=sa.sa_idStudentRegSubject',array('IdStudentRegSubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('IdStudentRegistration','registrationId'))
					  ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname'))
					  ->joinLeft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sa.sa_idSemester',array('IdSemesterMaster','SemesterMainName','SemesterMainDefaultLanguage'))
					  ->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=sa.sa_idSubject',array('SubCode','SubjectName','subjectMainDefaultLanguage'))
					  ->order('sp.appl_fname')
					  ->order('sp.appl_lname');
					  
		$rows = $db->fetchAll($select);
		
		if(isset($formdata)){
			
			if(isset($formdata["idIntake"]) && $formdata["idIntake"]!=''){
				$select->where("sr.IdIntake = ?",$formdata["idIntake"]);
			}
			
			if(isset($formdata["IdProgram"]) && $formdata["IdProgram"]!=''){
				$select->where("sr.IdProgram = ?",$formdata["IdProgram"]);
			}
			
			if(isset($formdata["IdSemester"]) && $formdata["IdSemester"]!=''){
				$select->where("sa.sa_idSemester = ?",$formdata["IdSemester"]);
			}
			
			if(isset($formdata["IdSubject"]) && $formdata["IdSubject"]!=''){
				$select->where("sa.sa_idSubject = ?",$formdata["IdSubject"]);
			}
						
			if(isset($formdata["status"]) && $formdata["status"]!=''){
				$select->where("sa.sa_status = ?",$formdata["status"]);
			}
			
			if(isset($formdata["student_name"]) && $formdata["student_name"]!=''){
				$select->where("((sp.appl_fname  LIKE '%".$formdata["student_name"]."%'");		 		
		 		$select->orwhere("sp.appl_lname  LIKE '%".$formdata["student_name"]."%')");
			}
			
			if(isset($formdata["student_id"]) && $formdata["student_id"]!=''){
				$select->where("(sr.registrationId LIKE '%".$formdata["student_id"]."%'");	
			}			
			
			if(isset($formdata["Student"]) && $formdata["Student"]!=''){
				$select->where("((sp.appl_fname  LIKE '%".$formdata["Student"]."%'");		 		
		 		$select->orwhere("sp.appl_lname  LIKE '%".$formdata["Student"]."%')");
		 		$select->orwhere("sr.registrationId  LIKE '%".$formdata["Student"]."%')");
			}
		}
		
		//echo $select;
		$rows = $db->fetchAll($select);
		
		return $rows;
	}
	
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('sa'=>$this->_name))
					  ->where("sa_id = ?",$id);
		
		//echo $select;
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	
	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	
	public function addAppealMark($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_appeal_mark, $data);
		return $db->lastInsertId();
	}
	
	
	public function getAppealInfo($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		 $select = $db ->select()
		 			  ->from(array('sa'=>$this->_name))
					  ->join(array('sam'=>$this->_appeal_mark),'sam.sam_sa_id=sa.sa_id')	
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=sa.sa_idStudentRegSubject',array('IdStudentRegSubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('IdStudentRegistration','registrationId'))
					  ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram', array('ProgramName'))
					  ->joinLeft(array('sm1'=>'tbl_staffmaster'),'sm1.IdStaff=sam.sam_examiner1',array('Examiner1'=>'FullName'))
					 // ->joinLeft(array('df1'=>'tbl_definationms'),'df1.idDefinition=sm1.FrontSalutation',array('FS1'=>'DefinitionCode'))
	 				//  ->joinLeft(array('df2'=>'tbl_definationms'),'df2.idDefinition=sm1.BackSalutation',array('BS1'=>'DefinitionCode'))
	 				  ->joinLeft(array('sm2'=>'tbl_staffmaster'),'sm2.IdStaff=sam.sam_examiner2',array('Examiner2'=>'FullName'))
					 // ->joinLeft(array('dfn1'=>'tbl_definationms'),'dfn1.idDefinition=sm2.FrontSalutation',array('FS2'=>'DefinitionCode'))
	 				  //->joinLeft(array('dfn2'=>'tbl_definationms'),'dfn2.idDefinition=sm2.BackSalutation',array('BS2'=>'DefinitionCode'))
	 				  ->joinLeft(array('sm3'=>'tbl_staffmaster'),'sm3.IdStaff=sam.sam_examiner3',array('Examiner3'=>'FullName'))
					 // ->joinLeft(array('d1'=>'tbl_definationms'),'d1.idDefinition=sm3.FrontSalutation',array('FS3'=>'DefinitionCode'))
	 				 // ->joinLeft(array('d2'=>'tbl_definationms'),'d2.idDefinition=sm3.BackSalutation',array('BS3'=>'DefinitionCode'))
					  ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname'))
					
					  ->where("sa.sa_id = ?",$id);
		
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	public function updateAppealMark($data,$id){
		$db = Zend_Db_Table::getDefaultAdapter();	
		$db->update($this->_appeal_mark,$data,'sam_id = '. (int)$id);
		
	}
	
	
	public function getStudentData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('sa'=>$this->_name))
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=sa.sa_idStudentRegSubject',array('IdStudentRegSubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('IdStudentRegistration','registrationId','IdProgram'))
					  ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname'))	
					   ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram', array('ProgramName'))				
					  ->where("sa_id = ?",$id);
		
		//echo $select;
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	
	
}

?>