<?php 

class Examination_Model_DbTable_ExamSlipRelease extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_slip_release';
	protected $_primary = "esr_id";
	
	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
			->from(array('esr'=>$this->_name));
		
		if($id){
			$select->where('ega.ega_student_nim =?',$student_nim);
			$row = $db->fetchRow($select);
		}else{
			$row = $db->fetchAll($select);
		}
		
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getReleaseData($semesterId,$assessmentTypeId){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
		->from(array('esr'=>$this->_name))
		->where('esr_semester_id = ?', $semesterId)
		->where('esr_assessment_type_id = ?', $assessmentTypeId);
	
		$row = $db->fetchRow($select);
	
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getAssessmentStatus($semesterId,$assessmentTypeId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('esr'=>$this->_name),array('esr_status'))
						->where('esr_semester_id = ?', $semesterId)
						->where('esr_assessment_type_id = ?', $assessmentTypeId);
		
		$row = $db->fetchRow($select);
		
		if($row){
			return $row['esr_status'];
		}else{
			return null;
		}
	}
	
		
	public function insertData($data=array()){
	
		if( !isset($data['esr_last_edit_by']) ){
			$auth = $auth = Zend_Auth::getInstance();
				
			$data['esr_last_edit_by'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['esr_last_edit_date']) ){
			$data['esr_last_edit_date'] = date('Y-m-d H:i:a');
		}
	
		return parent::insert($data);
	}
	
	public function updateData($data=array(),$where){
		if( !isset($data['esr_last_edit_by']) ){
			$auth = $auth = Zend_Auth::getInstance();
		
			$data['esr_last_edit_by'] = $auth->getIdentity()->iduser;
		}
		
		if( !isset($data['esr_last_edit_date']) ){
			$data['esr_last_edit_date'] = date('Y-m-d H:i:a');
		}
		
		return parent::update($data,$where);
	}

	public function getExamCenter(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_exam_center'), array('value'=>'*'))
			->where('a.ec_active = ?', 1);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function getExamCenterSoe($idsem){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_exam_center'), array('value'=>'*'))
			->join(array('b'=>'exam_center_setup'), 'a.ec_id = b.ec_id')
			->where('b.idSemester = ?', $idsem)
			->where('a.ec_active = ?', 1);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function examCenterSlipRelease($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert('exam_slip_release_exam_center', $data);
		$id = $db->lastInsertId('exam_slip_release_exam_center', 'rec_id');
		return $id;
	}

	public function getCourse($semester){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_registration'), array('value'=>'*'))
			->where('a.er_idSemester = ?', $semester)
			->where('a.er_status = ?', 764)
			->group('a.er_idSubject');

		$result = $db->fetchAll($select);
		return $result;
	}

	public function courseSlipRelease($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert('exam_slip_release_course', $data);
		$id = $db->lastInsertId('exam_slip_release_course', 'rcr_id');
		return $id;
	}

	public function getExamCenterSlipRelease($id, $idsem){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_exam_center'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_exam_center'), 'a.rec_ecid = b.ec_id')
			->join(array('c'=>'exam_center_setup'), 'b.ec_id = c.ec_id')
			->where('c.idSemester = ?', $idsem)
			->where('a.rec_esrid = ?', $id);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function slipReleaseInfo($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_semestermaster'), 'a.esr_semester_id = b.IdSemesterMaster')
			->joinLeft(array('c'=>'tbl_definationms'), 'a.esr_assessment_type_id = c.idDefinition', array('assessment_name'=>'c.DefinitionDesc'))
			->where('a.esr_id = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function updateStatusEc($arr, $id){
		$db = Zend_Db_Table::getDefaultAdapter();

		$in = '';
		$count = count($arr);
		$i = 0;
		foreach ($arr as $value){
			$i++;
			$in .= $value;

			if ($count != $i){
				$in .= ', ';
			}
		}

		$db->update('exam_slip_release_exam_center', array('rec_status'=>1), 'rec_id IN ('.$in.') AND rec_esrid = '.$id);
		$db->update('exam_slip_release_exam_center', array('rec_status'=>0), 'rec_id NOT IN ('.$in.') AND rec_esrid = '.$id);
	}

	public function getCourseSlipRelease($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_course'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.rcr_subjectid = b.IdSubject')
			->where('a.rcr_recid = ?', $id)
			->where('a.rcr_subjectid != ?', 84)
			->where('a.rcr_subjectid != ?', 85)
			->where('a.rcr_subjectid != ?', 25)
			->where('a.rcr_subjectid != ?', 62)
			->where('a.rcr_subjectid != ?', 61)
			->where('a.rcr_subjectid != ?', 24);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function examCenterInfo($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_exam_center'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_exam_center'), 'a.rec_ecid = b.ec_id')
			->where('a.rec_id = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function updateStatusCourse($arr, $id){
		$db = Zend_Db_Table::getDefaultAdapter();

		$in = '';
		$count = count($arr);
		$i = 0;
		foreach ($arr as $value){
			$i++;
			$in .= $value;

			if ($count != $i){
				$in .= ', ';
			}
		}

		$db->update('exam_slip_release_course', array('rcr_status'=>1), 'rcr_id IN ('.$in.') AND rcr_recid = '.$id);
		$db->update('exam_slip_release_course', array('rcr_status'=>0), 'rcr_id NOT IN ('.$in.') AND rcr_recid = '.$id);
	}

	public function getExamSlipRelease(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release'));

		$result = $db->fetchAll($select);
		return $result;
	}

	public function checkExamCenter($esrid, $ecid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_exam_center'), array('value'=>'*'))
			->where('a.rec_esrid = ?', $esrid)
			->where('a.rec_ecid = ?', $ecid);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function checkCourse($ecid, $subjectid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_course'), array('value'=>'*'))
			->where('a.rcr_recid = ?', $ecid)
			->where('a.rcr_subjectid = ?', $subjectid);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function checkExamCenterActive($esrid, $ecid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_exam_center'), array('value'=>'*'))
			->where('a.rec_status = ?', 1)
			->where('a.rec_esrid = ?', $esrid)
			->where('a.rec_ecid = ?', $ecid);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function checkCourseActive($ecid, $subjectid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'exam_slip_release_course'), array('value'=>'*'))
			->where('a.rcr_status = ?', 1)
			->where('a.rcr_recid = ?', $ecid)
			->where('a.rcr_subjectid = ?', $subjectid);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function getReleaseSoe($semesterId, $type){
		//SOE Final Exam only

		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db ->select()
			->from(array('esr'=>'exam_slip_release'))
			->where('esr_semester_id = ?', $semesterId)
			->where('esr_assessment_type_id = ?', $type)
			->where('CONCAT_WS(" ",esr_date,esr_time) <= ?',date('Y-m-d H:i:s'));

		$row = $db->fetchRow($select);

		return $row;
	}

	public function getExamCenterAdd($esrid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_exam_center'), array('value'=>'*'))
			->where('a.ec_active = ?', 1)
			->where('a.ec_id NOT IN (SELECT b.rec_ecid FROM exam_slip_release_exam_center as b WHERE b.rec_esrid = ?)', $esrid)
			->order('a.ec_name ASC');

		$result = $db->fetchAll($select);
		return $result;
	}

	public function getSubjectAdd($recid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_subjectmaster'), array('value'=>'*'))
			->where('a.IdSubject NOT IN (SELECT b.rcr_subjectid FROM exam_slip_release_course as b WHERE b.rcr_recid = ?)', $recid)
			->order('a.SubCode ASC')
			->where('a.IdSubject != ?', 84)
			->where('a.IdSubject != ?', 85)
			->where('a.IdSubject != ?', 25)
			->where('a.IdSubject != ?', 62)
			->where('a.IdSubject != ?', 61)
			->where('a.IdSubject != ?', 24);

		$result = $db->fetchAll($select);
		return $result;
	}
}