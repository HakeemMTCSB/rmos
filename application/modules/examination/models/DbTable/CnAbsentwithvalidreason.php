<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 27/5/2016
 * Time: 3:36 PM
 */
class Examination_Model_DbTable_CnAbsentwithvalidreason extends Zend_Db_Table_Abstract {

    public function getSemester(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->order('a.SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getStudentList($semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.er_idStudentRegistration = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'tbl_program'), 'b.IdProgram = c.IdProgram')
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'b.IdProgramScheme = d.IdProgramScheme', array('d.mode_of_program'))
            ->where('a.er_idSemester = ?', $semesterid)
            //->where('a.er_idStudentRegistration = ?', 2607)
            ->where('a.er_attendance_status = ?', 396);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubject($student_id, $subject_id, $semester_id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentRegistration = ?', $student_id)
            ->where('a.IdSubject = ?', $subject_id)
            ->where('a.IdSemesterMain = ?', $semester_id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getItem($regsub_id, $item_id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'))
            ->where('a.regsub_id = ?', $regsub_id)
            ->where('a.item_id IN (?)', $item_id)
            ->where('a.invoice_id IS NOT NULL');

        $result = $db->fetchRow($select);
        return $result;
    }
}