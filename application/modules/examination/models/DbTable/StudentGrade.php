<?php
class Examination_Model_DbTable_StudentGrade extends Zend_Db_Table { 
	
	protected $_name = 'tbl_student_grade';
	protected $_primary = 'sg_id';
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
    public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function updateData($data,$id){		
		 $this->update($data, "sg_id = '".(int)$id."'");
	}
	
	public function checkStudent($idStudentReg,$idSemester){
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
								->from($this->_name)
								->where('sg_IdStudentRegistration = ?',$idStudentReg)
								->where('sg_semesterId = ?',$idSemester);
		
		return $row = $db->fetchRow($select);
	}
	

	function getGradebySemester($IdStudentRegistration,$idstudentsemsterstatus){
		
		$select = $this->select()
					   ->from($this->_name)
					   ->where('sg_IdStudentRegistration = ?',$IdStudentRegistration)
					   ->where('sg_idstudentsemsterstatus = ?',$idstudentsemsterstatus);
		
		return $rowSet = $this->fetchRow($select);
		
	}
	
	
	function getSemesterEndResult($IdStudentRegistration){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select_level = $db->select()
					   ->from('tbl_studentsemesterstatus',array('mx_level'=>'MAX(level)'))					 
					   ->where('IdStudentRegistration = ?',$IdStudentRegistration);
		$level = $db->fetchRow($select_level);
					   
		$select = $db->select()
					   ->from(array('sg'=>$this->_name))	
					   ->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.idstudentsemsterstatus=sg.sg_idstudentsemsterstatus',array())			 
					   ->where('sg_IdStudentRegistration = ?',$IdStudentRegistration)
					   ->where('sss.Level = ?',$level["mx_level"]);
					   
		
		return $rowSet = $db->fetchRow($select);
		
	}
	
	function getStudentGrade($IdStudentRegistration,$idSemester){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					   ->from($this->_name)
					   ->where('sg_IdStudentRegistration = ?',$IdStudentRegistration)
					   ->where('sg_semesterId = ?',$idSemester);
		
		return $rowSet = $db->fetchRow($select);
		
	}
	
	
	
	
	
}
?>