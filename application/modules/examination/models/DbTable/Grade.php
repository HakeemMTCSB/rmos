<?php
class Examination_Model_DbTable_Grade extends Zend_Db_Table { 
	
	protected $_name = 'tbl_gradesetup_main';
	protected $_primary = '';
	
	
	public function getGrade($semester_id,$program_id,$subject_id,$mark_obtained){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  		  
		  //round 
		  $mark_obtained = round($mark_obtained);
		  
		  $sql = $db->select()
					->from(array('sm'=>'tbl_semestermaster'),array('SemesterMainStartDate'))
				    ->where('sm.IdSemesterMaster = ?',$semester_id);
		  $sem_grade = $db->fetchRow($sql);
				  
		  
		  	   //1st : check setup yang base on effective semester, program & subject
		 	   $select_grade = $db->select()
	 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
			 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass','Grade'))
			 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
			 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
			 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])	
			 	 				  ->where('gsm.BasedOn = 1 ') //program	& subject	
			 	 				  ->where('gsm.IdProgram = ?',$program_id)
			 	 				  ->where('gsm.IdSubject = ?',$subject_id)
			 	 				  ->where("g.MinPoint <= ?", $mark_obtained)
				 	 			  ->where('g.MaxPoint >= ?', $mark_obtained)
				 	 			  ->order('sm.SemesterMainStartDate desc');
			 	 				  
			 	 				  /*if($mark_obtained==100){
			 	 				  		$select_grade->where("g.MinPoint <= ".$mark_obtained." AND ".$mark_obtained." <= MaxPoint");
			 	 				  }else{
			 	 				  		$select_grade->where("g.MinPoint <= ".$mark_obtained." AND ".$mark_obtained." < MaxPoint");
			 	 				  }	*/ 	 
		
		   		$grade = $db->fetchRow($select_grade);
		   		   
		   
	       if(!$grade){		  
	       		       	
	       	  	 //check setup yang base on effective semester, program
		 		 $select_grade2 = $db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass','Grade'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
				 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
				 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdProgram = ?',$program_id)
				 	 				  ->where("g.MinPoint <= ?", $mark_obtained)
				 	 			  	  ->where('g.MaxPoint >= ?', $mark_obtained)
				 	 			  	  ->order('sm.SemesterMainStartDate desc');						 	 				
				 	 				  
					
				 $grade2 = $db->fetchRow($select_grade2);				
				
					if(!$grade2){
						
							
							//get progrm award
							$programDB = new GeneralSetup_Model_DbTable_Program();
							$program = $programDB->fngetProgramData($program_id);
						
							 //check setup yang base on effective semester, award
					 		 $select_grade3 = $db->select()
							 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
									 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass','Grade'))
									 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
 													      ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
									 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])	
									 	 				  ->where('gsm.BasedOn = 0 ') //award	
									 	 				  ->where('gsm.IdAward = ?',$program['Award'])
									 	 				  ->where("g.MinPoint <= ?", $mark_obtained)
				 	 			  						  ->where('g.MaxPoint >= ?', $mark_obtained)
				 	 			  						  ->order('sm.SemesterMainStartDate desc');						 	 				
									 	 				  
							$grade3 = $db->fetchRow($select_grade3);
							$grade = $grade3;
							
				
					}else{
						$grade = $grade2;
					}
	       	       
	       }
	       
	 	   //throw exception if grade never been setup
		   if(!$grade){
		   	throw new exception('There is no grade setup for subject id: ('.$subject_id.') ('.$semester_id.') ('.$program_id.')');
		   }
	       return $grade;
	}
	
	public function getGradeOld($semester_id,$program_id,$subject_id,$mark_obtained){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		
     			  
		  echo '1<br>';
		  
		  //check setup yang base on selected semester, program & subject
		  $select_grade = $db->select()
	 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
			 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeName','GradeDesc','GradePoint','Pass','Grade'))
			 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
			 	 				  ->where('gsm.IdSemester = ?',$semester_id)
			 	 				  ->where('gsm.IdProgram = ?',$program_id)
			 	 				  ->where('gsm.IdSubject = ?',$subject_id)
			 	 				  ->where("g.MinPoint <= '.$mark_obtained.' AND '.$mark_obtained.' <= MaxPoint");	 	 
			
		   $grade = $db->fetchRow($select_grade);
		   
		   
		   
	       if(!$grade){		  
	     	
	       	        echo '2<br>';
	       	        //get grade based on latest semester, program & subject
	       		 	$select_latest_grade1 = $db->select()
					 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
					 	 				  		  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
					 	 				  		  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeName','GradeDesc','GradePoint','Pass','Grade'))
							 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
							 	 				  ->where('gsm.IdProgram = ?',$program_id)
							 	 				  ->where('gsm.IdSubject = ?',$subject_id)
							 	 				  ->where("g.MinPoint <= '".$mark_obtained."' AND '".$mark_obtained."' <= MaxPoint")
							 	 				  ->where('sm.SemesterMainStartDate < (SELECT SemesterMainStartDate FROM `tbl_semestermaster` WHERE `IdSemesterMaster`=(?))',$semester_id)			 	 				
							 	 				  ->order('SemesterMainStartDate desc');	
							 	 				   	
				    $grade_latest = $db->fetchRow($select_latest_grade1);	
				    	
				    
				    if(!$grade_latest){
				     	
				    	//check setup yang based on selected semester and program
				    	 echo '3<br>';
				    	$select_grade = $db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeName','GradeDesc','GradePoint','Pass','Grade'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
				 	 				  ->where('gsm.IdSemester = ?',$semester_id)
				 	 				  ->where('gsm.BasedOn = 2')
				 	 				  ->where('gsm.IdProgram = ?',$program_id)
				 	 				  ->where("g.MinPoint <= '".$mark_obtained."' AND '".$mark_obtained."' <= MaxPoint");
		 	 								
			   			$grade_program = $db->fetchRow($select_grade);	
			   			
			   			
			   			if(!$grade_program){
			   				
			   				 echo '4<br>';
			   				 //get grade based on latest semester & program 
					 	 	 $select_latest_grade2 = $db->select()
						 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
						 	 				  		  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
						 	 				  		  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeName','GradeDesc','GradePoint','Pass','Grade'))
								 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
								 	 				  ->where('gsm.IdProgram = ?',$program_id)							 	 				 
								 	 				  ->where("g.MinPoint <= '".$mark_obtained."' AND '".$mark_obtained."' <= MaxPoint")
								 	 				  ->where('sm.SemesterMainStartDate < (SELECT SemesterMainStartDate FROM `tbl_semestermaster` WHERE `IdSemesterMaster`=(?))',$semester_id)			 	 				
								 	 				  ->order('SemesterMainStartDate desc');	
								 	 				   	
					 	 	$grade_latest2 = $db->fetchRow($select_latest_grade2);	
					 	 	
					 	 	
					 	 	if(!$grade_latest2){
					 	 		
					 	 		//get university grade based on selected semester
					 	 		echo '5<br>';
					 	 		$select_univ = $db->select()
					 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
					 	 				  		  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
					 	 				  		  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeName','GradeDesc','GradePoint','Pass','Grade'))
							 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
							 	 				  ->where('gsm.BasedOn = 3')
							 	 				  ->where('gsm.IdSemester = ?',$semester_id)							 	 				 
							 	 				  ->where("g.MinPoint <= '".$mark_obtained."' AND '".$mark_obtained."' <= MaxPoint")
							 	 				  ->where('sm.SemesterMainStartDate < (SELECT SemesterMainStartDate FROM `tbl_semestermaster` WHERE `IdSemesterMaster`=(?))',$semester_id)			 	 				
							 	 				  ->order('SemesterMainStartDate desc');	
								 	 				   	
					 	 		$grade_univ = $db->fetchRow($select_univ);	
					 	 		
					 	 		
					 	 		if(!$grade_univ){
					 	 			
					 	 			//get university grade based on latest semester
					 	 			echo '6<br>';
					 	 			$select_univ2 = $db->select()
					 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
					 	 				  		  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
					 	 				  		  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeName','GradeDesc','GradePoint','Pass','Grade'))
							 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
							 	 				  ->where('gsm.BasedOn = 3')							 	 				  						 	 				 
							 	 				  ->where("g.MinPoint <= '".$mark_obtained."' AND '".$mark_obtained."' <= MaxPoint")
							 	 				  ->where('sm.SemesterMainStartDate < (SELECT SemesterMainStartDate FROM `tbl_semestermaster` WHERE `IdSemesterMaster`=(?))',$semester_id)			 	 				
							 	 				  ->order('SemesterMainStartDate desc');	
								 	 				   	
					 	 			$grade_univ2 = $db->fetchRow($select_univ2);
					 	 			$grade = $grade_univ2;
					 	 				
					 	 		}else{
			   						$grade = $grade_univ;
			   					}
					 	 		
					 	 	}else{
			   					$grade = $grade_latest2;
			   				}
				 	 	
			   			}else{
			   				$grade = $grade_program;
			   			}
			   			
				    }else{				    	
				    	$grade = $grade_latest;
				    }
	       }
	       
	 	   //throw exception if grade never been setup
		   if(!$grade){
		   	throw new exception('There is no grade setup for subject id: '.$subject_id);
		   }
	       return $grade;
	}
	
		
	
	
	public function listsubjectreg(){
		$sql="
				SELECT IdStudentRegSubjects, a.IdSemesterMain, IdProgram, IdSubject, final_course_mark
				FROM `tbl_studentregsubjects` a
				INNER JOIN tbl_studentregistration AS b ON a.IdStudentRegistration = b.IdStudentRegistration
				WHERE a.`IdSemesterMain` =1
				AND final_course_mark IS NOT NULL 
			";
		$db = Zend_Db_Table::getDefaultAdapter();
		$rows=$db->fetchAll($sql);
		return $rows;
	}
	
	
	public function getGradeSetup($program_id,$semester_id,$subject_id=null){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  		  
		  $sql = $db->select()
					->from(array('sm'=>'tbl_semestermaster'),array('SemesterMainStartDate'))
				    ->where('sm.IdSemesterMaster = ?',$semester_id);
		  $sem_grade = $db->fetchRow($sql);

		  $grade = null;
		  
		  if(isset($subject_id) && $subject_id!=null){
		  
		  	   //1st : check setup yang base on effective semester, program & subject
		 	   $select_grade = $db->select()
	 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
			 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
			 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])	
			 	 				  ->where('gsm.BasedOn = 1 ') //program	& subject	
			 	 				  ->where('gsm.IdProgram = ?',$program_id)
			 	 				  ->where('gsm.IdSubject = ?',$subject_id)
				 	 			  ->order('sm.SemesterMainStartDate desc');		
		   	   $grade = $db->fetchRow($select_grade);
		   		   
		  }
		   
	       if(!$grade){		  
	       		       	
	       	  	 //check setup yang base on effective semester, program
		 		 $select_grade2 = $db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
				 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdProgram = ?',$program_id)
				 	 			  	  ->order('sm.SemesterMainStartDate desc');	
				 $grade2 = $db->fetchRow($select_grade2);				
				
					if(!$grade2){
						
							
							//get progrm award
							$programDB = new GeneralSetup_Model_DbTable_Program();
							$program = $programDB->fngetProgramData($program_id);
						
							 //check setup yang base on effective semester, award
					 		 $select_grade3 = $db->select()
							 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
									 	 			      ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
									 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])	
									 	 				  ->where('gsm.BasedOn = 0 ') //award	
									 	 				  ->where('gsm.IdAward = ?',$program['Award'])
				 	 			  						  ->order('sm.SemesterMainStartDate desc');						 	 				
									 	 				  
							$grade3 = $db->fetchRow($select_grade3);
							$grade = $grade3;
							
				
					}else{
						$grade = $grade2;
					}
	       	       
	       }
	       
	 	   //throw exception if grade never been setup
		   if(!$grade){
		   	//throw new exception('There is no grade setup for subject id: '.$subject_id);
			   $grade_detail = false;
		   }else{
		   	
		   		//get grade detail		   	 
		 	   $select_grade_detail = $db->select()
			 	 				  ->from(array('g'=>'tbl_gradesetup'),array('Grade'))
			 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
			 	 				  ->where('g.IdGradeSetUpMain = ?',$grade['IdGradeSetUpMain'])
				 	 			  ->order('g.Rank');			 	 				 
		
		   		$grade_detail = $db->fetchAll($select_grade_detail);
		   }
		   
		   if($grade_detail){
	       		return $grade_detail;
		   }else{
		   		return null;
		   }
	}
	
	
	public function getGradeCTEX($semester_id,$program_id,$subject_id,$grade_obtained){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  		  
		  //trim 
		  $grade_obtained = trim($grade_obtained);
		  
		  $sql = $db->select()
					->from(array('sm'=>'tbl_semestermaster'),array('SemesterMainStartDate'))
				    ->where('sm.IdSemesterMaster = ?',$semester_id);
		  $sem_grade = $db->fetchRow($sql);
				  
		  
		  	   //1st : check setup yang base on effective semester, program & subject
		 	   $select_grade = $db->select()
	 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
			 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass','Grade'))
			 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
			 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
			 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])	
			 	 				  ->where('gsm.BasedOn = 1 ') //program	& subject	
			 	 				  ->where('gsm.IdProgram = ?',$program_id)
			 	 				  ->where('gsm.IdSubject = ?',$subject_id)
			 	 				  ->where("d.DefinitionCode = ?", $grade_obtained) 
				 	 			  ->order('sm.SemesterMainStartDate desc');
			 	 				  
			 	 				  /*if($mark_obtained==100){
			 	 				  		$select_grade->where("g.MinPoint <= ".$mark_obtained." AND ".$mark_obtained." <= MaxPoint");
			 	 				  }else{
			 	 				  		$select_grade->where("g.MinPoint <= ".$mark_obtained." AND ".$mark_obtained." < MaxPoint");
			 	 				  }	*/ 	 
		
		   		$grade = $db->fetchRow($select_grade);
		   		   
		   
	       if(!$grade){		  
	       		       	
	       	  	 //check setup yang base on effective semester, program
		 		 $select_grade2 = $db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass','Grade'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
				 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
				 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdProgram = ?',$program_id) 
				 	 				  ->where("d.DefinitionCode = ?", $grade_obtained) 
				 	 			  	  ->order('sm.SemesterMainStartDate desc');						 	 				
				 	 				  
					
				 $grade2 = $db->fetchRow($select_grade2);				
				
					if(!$grade2){
						
							
							//get progrm award
							$programDB = new GeneralSetup_Model_DbTable_Program();
							$program = $programDB->fngetProgramData($program_id);
						
							 //check setup yang base on effective semester, award
					 		 $select_grade3 = $db->select()
							 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
									 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass','Grade'))
									 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
 													      ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
									 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])	
									 	 				  ->where('gsm.BasedOn = 0 ') //award	
									 	 				  ->where('gsm.IdAward = ?',$program['Award']) 
									 	 				  ->where("d.DefinitionCode = ?", $grade_obtained) 
				 	 			  						  ->order('sm.SemesterMainStartDate desc');						 	 				
									 	 				  
							$grade3 = $db->fetchRow($select_grade3);
							$grade = $grade3;
							
				
					}else{
						$grade = $grade2;
					}
	       	       
	       }
	       
	 	   //throw exception if grade never been setup
		   if(!$grade){
		   	throw new exception('There is no grade setup for subject id: '.$subject_id);
		   }
	       return $grade;
	}
}
?>