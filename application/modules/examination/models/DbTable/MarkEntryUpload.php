<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/11/2016
 * Time: 9:48 AM
 */
class Examination_Model_DbTable_MarkEntryUpload extends Zend_Db_Table {

    public function getStudentInfo($registrationId, $groupId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectGroup = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('a.IdStudentRegistration'))
            ->where('a.IdCourseTaggingGroup = ?', $groupId);

        $selectGroupList = $db->fetchAll($selectGroup);

        if ($selectGroupList) {
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregistration'))
                ->where('a.registrationId = ?', $registrationId)
                ->where('a.IdStudentRegistration IN ('.$selectGroup.')');

            $result = $db->fetchRow($select);
        }else{
            $result = false;
        }

        return $result;
    }

    public function getStudentRegSubject($studentId, $groupId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentregistration = ?', $studentId)
            ->where('a.IdCourseTaggingGroup = ?', $groupId);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getMarkEntry($idSubject, $idsmester, $idstudent, $component){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_student_marks_entry'))
            ->where('a.Course = ?', $idSubject)
            ->where('a.IdSemester = ?', $idsmester)
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.Component = ?', $component);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getMarkEntryDetail($id, $componentDet){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_student_detail_marks_entry'))
            ->where('a.IdStudentMarksEntry = ?', $id)
            ->where('a.ComponentDetail = ?', $componentDet);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getExamRegistration($semesterId, $studentId, $subjectId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_registration'))
            ->where('a.er_idSemester IN (?)', $semesterId)
            ->where('a.er_idStudentRegistration = ?', $studentId)
            ->where('a.er_idSubject = ?', $subjectId);

        $result = $db->fetchRow($select);
        return $result;
    }
}