<?php 
class Examination_Model_DbTable_ExamRegistration extends Zend_Db_Table_Abstract
{
    protected $_name = 'exam_registration';
	protected $_primary = "er_id";
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"er_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("er_id ='".$add_id."'");
	}
	
	
	public function getStudentRegister($ecid,$idSemester,$idSubject){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semesterDB = new Registration_Model_DbTable_Semester();
		$semester_arr = $semesterDB->getEqSemesterOpen($idSemester);
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
	                ->where('er.er_ec_id = ?',$ecid)
	                ->where('srs.Active != ?',3)
	                ->where('sr.profileStatus = ?',92)
	                ->where('er.er_idSemester IN (?)',$semester_arr)
	                ->where('er.er_idSubject = ?',$idSubject);
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	
	
	public function getRegisteredNoEc($formData=null){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semesterDB = new Registration_Model_DbTable_Semester();
		$semester_arr = $semesterDB->getEqSemesterOpen($formData['IdSemester']);

		$select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName','DefaultLanguage'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	                ->join(array('cm'=>'tbl_subjectmaster'),'cm.IdSubject=er.er_idSubject',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
	                ->where('er_ec_id IS NULL')
	                ->where('er_idSemester IN (?) OR er_idSemester = '.$formData['IdSemester'].'',$semester_arr)
	                ->where('srs.Active != ? ',3)
	                ->where('sr.profileStatus = ?',92)
	                ->group('er_idCountry')
	                ->group('er_idSemester')
	                ->group('er_idSubject')	             
	                ->group('er_idCity')
	                ->order('er.er_idCountry')
	                ->order('er.er_idCity');

	  				if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
	                	$select->where('er_idSubject = ?',$formData['IdSubject']);
	                }
	                
	                if(isset($formData['idCountry']) && $formData['idCountry']!=''){
	                	$select->where('er_idCountry = ?',$formData['idCountry']);
	                }
	                
	                if(isset($formData['idCity']) && $formData['idCity']!=''){
	                	$select->where('er_idCity = ?',$formData['idCity']);
	                }
	                
      
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getTotalRegisteredNoEc($idCountry,$idCity,$idSemester,$idProgram,$idSubject){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semesterDB = new Registration_Model_DbTable_Semester();
		$semester_arr = $semesterDB->getEqSemesterOpen($idSemester);
		
		 $select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName','DefaultLanguage'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	                ->join(array('cm'=>'tbl_subjectmaster'),'cm.IdSubject=er.er_idSubject',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
	                ->where('er.er_ec_id IS NULL ')
	                ->where('srs.Active != ? ',3)
	                ->where('sr.profileStatus = ?',92)
	                ->where('er.er_idSemester IN (?)',$semester_arr)
	                ->where('er.er_idSubject = ?',$idSubject)  ;           
	                                    
      
	                if(isset($idCountry) && $idCountry!=''){
	                	$select ->where('er_idCountry = ?',$idCountry);
	                }
	                
	                if(isset($idCity) && $idCity!=''){
	                	$select->where('er_idCity = ?',$idCity);
	                }
	                
	     //echo $select.'<hr>';           
        $row = $db->fetchAll($select);
        
        if(count($row)>0){
        	return count($row);
        }else{
			return 0;
        }
		
	}
	
	public function updateCenterData($data,$where){
		$this->update($data,$where);
	}
	
	
	public function getCourseRegExambyStudent($idSemester,$idStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('cm'=>'tbl_subjectmaster'),'cm.IdSubject=er.er_idSubject',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
	                ->joinLeft(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name'))	               
	                ->where('er_idSemester = ?',$idSemester)
	                ->where('er_idStudentRegistration = ?',$idStudentRegistration);
        
        $row = $db->fetchAll($select);
		return $row;
	}
	
	
	/*
	 * get exam center address by exam registered by student
	 */
	public function getExamCenterbyStudent($idSemester,$idStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))	              
	                ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name'))	       
	                ->join(array('add'=>'tbl_address'),'add.add_org_id=ec.ec_id') 
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=add.add_country',array('CountryName'))
	                ->join(array('s'=>'tbl_state'),'s.idState=add.add_state',array('StateName'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=add.add_city',array('CityName'))
	                ->where('add.add_org_name = "tbl_exam_center"')   
	                ->where('add.add_address_type = ?',615)   //permenant address
	                ->where('er_idSemester = ?',$idSemester)
	                ->where('er_idStudentRegistration = ?',$idStudentRegistration)
	                ->group('er.er_ec_id');
        
        $row = $db->fetchAll($select);
		return $row;
	}
	
	
	public function getStudentNoEc($idCountry,$idCity,$idSemester,$idProgram,$idSubject){
				
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semesterDB = new Registration_Model_DbTable_Semester();
		$semester_arr = $semesterDB->getEqSemesterOpen($idSemester);
		
		
		 $select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName','DefaultLanguage'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	                ->join(array('cm'=>'tbl_subjectmaster'),'cm.IdSubject=er.er_idSubject',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
	                ->where('er.er_ec_id IS NULL ')
	                ->where('sr.profileStatus = ?',92)
	                 ->where('srs.Active != ?', 3)
	                ->where('er.er_idCountry = ?',$idCountry)
	                ->where('er.er_idCity = ?',$idCity)
	                ->where('er.er_idSemester IN (?)',$semester_arr)
	                ->where('er.er_idSubject = ?',$idSubject)
	                ->order('sp.appl_fname');		                     
      
	        	                
        $row = $db->fetchAll($select);
        return $row;
		
	}
	
	public function getTotalAssignedEc($idCountry,$idCity,$idSemester,$idProgram,$idSubject){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semesterDB = new Registration_Model_DbTable_Semester();
		$semester_arr = $semesterDB->getEqSemesterOpen($idSemester);
	
		
		 $select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName','DefaultLanguage'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	                ->join(array('cm'=>'tbl_subjectmaster'),'cm.IdSubject=er.er_idSubject',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
	                ->where('srs.Active != ?', 3)
	                ->where('sr.profileStatus = ?',92)
	                ->where('er.er_ec_id IS NOT NULL ')
	                ->where('er.er_idSemester IN (?)',$semester_arr)
	                ->where('er.er_idSubject = ?',$idSubject);                     
      
	                if(isset($idCountry) && $idCountry!=''){
	                	$select ->where('er_idCountry = ?',$idCountry);
	                }
	                
	                if(isset($idCity) && $idCity!=''){
	                	$select->where('er_idCity = ?',$idCity);
	                }
	                
	   //  echo $select.'<hr>';           
        $row = $db->fetchAll($select);
        
        if(count($row)>0){
        	return count($row);
        }else{
			return 0;
        }
		
	}
	
	
	public function getStudentNoExamCenter($idCountry,$idCity,$idSemester,$ec_city_others=null){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName','DefaultLanguage'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))	               
	                ->where('er_ec_id IS NULL ')
	                ->where('er_idCountry = ?',$idCountry)
	                ->where('er_idCity = ?',$idCity)
	                ->where('er_idSemester = ?',$idSemester);

	                if($idCity==99){
	                	$select->where('er_idCityOthers = ?',$ec_city_others);
	                }
	           
        $row = $db->fetchAll($select);
        return $row;
		
	}
	
	
	public function getStudentByEc($ec_id,$idSemester){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->where('er.er_ec_id = ?',$ec_id)
	                ->where('er.er_idSemester = ?',$idSemester)
	                ->group('er.er_idStudentRegistration');
					//->group('er.er_idSemester');
					//->group('er.er_idSubject');
	    
        $row = $db->fetchAll($select);
        return $row;
		
	}
	
	
	
}
?>