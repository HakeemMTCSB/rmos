<?php 

class Examination_Model_DbTable_ExamScaling extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_scaling';
	protected $_primary = "es_id";
	protected $_name_detail = 'exam_scaling_detail';
	protected $_primary_detail = "esd_id";
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"es_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("es_id ='".$add_id."'");
	}
	
	
	public function addDetailData($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert($this->_name_detail,$data);		
		return $db->lastInsertId();
	}
	
	public function updateDetailData($data,$id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update($this->_name_detail,$data,"esd_id ='".$id."'");
	}
	
	public function deleteDetailData($add_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->delete($this->_name_detail,"esd_id ='".$add_id."'");
	}
	
	public function getGroupData($IdCourseTaggingGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('es'=>$this->_name))
					  ->where("es_group = ?",$IdCourseTaggingGroup);		
		
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	public function getData($semester,$program,$subject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('es'=>$this->_name))
					  ->where("es_semester = ?",$semester)
					  ->where("es_program = ?",$program)
					  ->where("es_subject = ?",$subject);
		
		
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	public function getExamScaling($es_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('es'=>$this->_name))
					  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=es.es_semester',array('IdSemesterMaster','SemesterMainName','SemesterMainDefaultLanguage'))
					  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=es.es_subject',array('SubCode','SubjectName','subjectMainDefaultLanguage'))
					  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=es.es_mark_type',array('mark_type'=>'DefinitionDesc'))	
					  ->join(array('g'=>'tbl_course_tagging_group'),'g.IdCourseTaggingGroup = es.es_group',array('GroupName','GroupCode'))	
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=g.IdLecturer',array('FrontSalutation','coordinator'=>'FullName','BackSalutation'))	  
					  ->where("es_id = ?",$es_id);
		
		//echo $select;
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	public function getExamScalingMark($es_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('esd'=>$this->_name_detail),array('esd_mark'))
					  ->where("es_id = ?",$es_id);
		
		//echo $select;
		$row = $db->fetchAll($select);
		
		return $row;
		
	}
	
	
}

?>