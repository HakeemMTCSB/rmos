<?php 

class Examination_Model_DbTable_StudentMarkEntryHistory extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_student_marks_entry_history';
	protected $_primary = 'id';
	protected $_item_name = 'tbl_student_detail_marks_entry_history';
	protected $_item_primary = 'id';
	protected $_cs_name = 'tbl_student_mark_change_status';
	protected $_cs_primary = 'cs_id';
	
		
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('sa'=>$this->_name))		
					  ->where("id = ?",$id);
		
		//echo $select;
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	public function addData($data){		
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id = $db->insert($this->_name,$data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function addItemData($data){
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $id = $db->insert($this->_item_name, $data);
		 return $id;
	}

	public function addChangeStatus($data){
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $id = $db->insert($this->_cs_name, $data);
		 return $id;
	}
	
}

?>