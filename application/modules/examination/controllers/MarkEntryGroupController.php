<?php

class examination_MarkEntryGroupController extends Base_Base { //Controller for the User Module

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Mark Entry - Search Course");
    	
    	$form = new Examination_Form_MarkEntrySearchCourse();
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$progDB= new GeneralSetup_Model_DbTable_Program();
				$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
				$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
				$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
				
				$i=0;
				$j=0;
				$allsemlandscape =null;
				$allblocklandscape = null;
				
			    $activeLandscape=$landscapeDB->getAllActiveLandscape($formData["IdProgram"]);
					
			        foreach($activeLandscape as $actl){
						if($actl["LandscapeType"]==43){
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}elseif($actl["LandscapeType"]==44){
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				
				if(is_array($allsemlandscape))
					$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,$formData);
					
				//echo count($subjectsem);
				
				if(is_array($allblocklandscape))
					$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,$formData);
					
				//echo count($subjectblock);
				
				if(is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=array_merge( $subjectsem , $subjectblock );
				}else{
					if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
						$subjects=$subjectsem;
					}
					elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
						$subjects=$subjectblock;
					}		
				}//if else
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($subjects));
				$paginator->setItemCountPerPage(50);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				
				$this->view->paginator = $paginator;
				
				//print_r($subjects);
				
			}//if form valid
    	}//if post	
    }
    
    
}