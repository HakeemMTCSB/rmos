<?php

class examination_StudentassessmentController extends Base_Base { //Controller for the User Module

	private $lobjStudentAssessmentForm;
	private $lobjChargemaster;
	private $lobjdeftype;
	private $_gobjlog;
	private $lobjsemesterModel;
	private $lobjschemeModel;
	private $lobjprogramModel;
	private $lobjstudentassessmentModel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjStudentAssessmentForm = new Examination_Form_Studentassessment();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjDeparmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster();
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
		$this->lobjschemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjstudentregModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjstudentassessmentModel = new Examination_Model_DbTable_Studentassessment();
		
	}

	public function indexAction() {		
		$this->view->lobjStudentAssessmentForm = $this->lobjStudentAssessmentForm;
	}
	
	//Function to Fetch the student name
	public function getstudentdetAction(){
		$studentDet = array();
		$this->_helper->layout->disableLayout();	
		//$this->_helper->viewRenderer->setNoRender();
		$IdStudent = '';	
		$IdStudent = $this->getRequest()->getParam('IdStudent');
		$scheme = $this->getRequest()->getParam('scheme');
		$program = $this->getRequest()->getParam('program');
		if($IdStudent!=''){			
			$studentDet = $this->lobjstudentregModel->fngetstudentnames($IdStudent);	
		}else{			
			$studentDet = $this->lobjstudentregModel->fngetstudentnamebyschemeprg($scheme,$program);	
		}
		if(count($studentDet) > 0){
			$this->view->studdet = $studentDet;
		}
	}
	
	
	
	public function studentassessmentdetAction(){
		$lintstudentId = ( int ) $this->_getParam ( 'id' );
		$studentDet = $this->lobjstudentregModel->fetchStudentRegDetails('',$lintstudentId);		
		if(count($studentDet) > 0){
			$this->view->studdet = $studentDet;
		}
		//Now get student semesters and semester marks entry and gpa and cgpa detail
		$larrstassessmentDet = '';
		$larrstassessmentDet = $this->lobjstudentassessmentModel->fngetStudentassessmentdet($lintstudentId);
		
		if(count($larrstassessmentDet) > 0){
			$this->view->studentgpadet = $larrstassessmentDet;
		}
	}
	
	
	
	public function studentassesmentAction(){		
		$this->_helper->layout->disableLayout();
		$IdStudent = $this->_getParam('Idstudent');
		$semcode = $this->_getParam('semcode');
		$studentDet = $this->lobjstudentregModel->fetchStudentRegDetails('',$IdStudent);		
		
		if(count($studentDet) > 0){
			$this->view->studdet = $studentDet;
		}
		
		//Now first get the students subjects list for the semester
		$studentregsubject = $this->lobjstudentassessmentModel->fetchStudentRegSubList($studentDet['IdStudentRegistration'],$studentDet['IdSemester'],$studentDet['IdSemesterMain']);
		
		$finalstudentregsubjects = array();		
		foreach($studentregsubject as $det){
			 $marksdata = $this->lobjstudentassessmentModel->fngetsetudentcoursemarks($det['IdStudentRegistration'],$det['IdStudentRegSubjects']);
			 $det['IdGrade'] = $marksdata['IdGrade'];
			 $det['Totalmarks'] = $marksdata['Totalmarks'];
			 $det['GradePoint'] = $marksdata['GradePoint'];
			 $finalstudentregsubjects[] = $det;
		}
		
		
		
		/*$studregcoursegpa = array();
		foreach($studentregsubject as $det){
			$det['IdScheme'] = $studentDet['IdScheme'];
			$det['IdProgram'] = $studentDet['IdProgram'];
			$studregcoursegpa = $this->lobjstudentassessmentModel->fncalculatestudentcourseGpa($det);
						
			$det['marks'] = $studregcoursegpa;
			$det['gpaGradepoint'] = '';
			$det['gpaGradevalue'] = '';
			$det['cgpaGradepoint'] = '';
			$det['cgpaGradevalue'] = '';
			
			$semcode = '';
			
			if($det['IdSemesterDetails'] != ''){				
				$larrsem = $this->lobjsemesterModel->getsemDetail($det['IdSemesterDetails']);
				if(!empty($larrsem)){
					$semcode = $larrsem[0]['SemesterCode'];
				}
			}else{
				$larrsemmain = $this->lobjsemesterModel->getsemMainDet($det['IdSemesterMain']);				
				if(!empty($larrsemmain)){
					$semcode = $larrsemmain[0]['SemesterMainCode'];
				}
			}
			
			$gpadet = $this->lobjstudentassessmentModel->fngetgpadet($semcode,$det['IdScheme'],$det['IdProgram']);
			if(!empty($gpadet)){
					foreach($gpadet as $dat){						
						if(($dat['Minimum'] <= floatval($studregcoursegpa)) && (floatval($studregcoursegpa) <= floatval($dat['Maximum']))  ){
							$det['gpaGradepoint'] = $dat['Gradepoint'];
							$det['gpaGradevalue'] = $dat['Gradevalue'];
						}
						
						if(($dat['Minimum'] <= floatval($studregcoursegpa)) && (floatval($studregcoursegpa) <= $dat['Maximum'])  ){
							$det['cgpaGradepoint'] = $dat['Gradepoint'];
							$det['cgpaGradevalue'] = $dat['Gradevalue'];
						}
					}
				}
				
			// Now check the student attend this exam or not
			$ret = $this->lobjstudentassessmentModel->fncheckstudentcoursemarks($semcode,$det['IdStudentRegistration'],$det['IdSubject']);
			if(!$ret){
				$det['gpaGradepoint'] = '';
				$det['gpaGradevalue'] = '';
				$det['cgpaGradepoint'] = '';
				$det['cgpaGradevalue'] = '';
				$det['marks'] = '';
			}
				
			$finalstudentregsubjects[] = $det;
		}	*/
	
		
		$this->view->stdcoursemarksdet = $finalstudentregsubjects;		
	}
}