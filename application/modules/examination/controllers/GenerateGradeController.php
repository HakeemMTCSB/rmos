<?php

class examination_GenerateGradeController extends Base_Base { //Controller for the User Module

	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function groupListAction() {
		
		$msg = $this->_getParam('msg', null);
			
		//title
		$this->view->title= $this->view->translate("Generate Grade - Section List");
		 
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Complete Generate Grade");
		}
	
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();
	}
	
	
	
	public function studentListAction(){
		
			//title
		$this->view->title= $this->view->translate("Generate Grade - Student List");
		
		$id = $this->_getParam('id', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idProgram = $this->_getParam('idProgram', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$student = $this->_getParam('student',null);
	
		
		$this->view->idGroup = $id;
		$this->view->idSemester = $idSemester;
		$this->view->idProgram = $idProgram;
		$this->view->idSubject = $idSubject;
				
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
		
	
		//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($id);				
		$this->view->group = $group;
		
		//student list
		$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$studentList = $courseGroupStudentDb->getStudentbyGroup($id,$student);
		
        //GET SEMESTER INFO
        $SemesterMaster = new GeneralSetup_Model_DbTable_Semestermaster();
        $semester_master = $SemesterMaster->fnGetSemestermaster($idSemester);
	  	
        $disableGenerate = 'false';
        //echo $semester_master['SemesterMainEndDate'];
        if($semester_master['SemesterMainEndDate'] < '2013-08-20')
        {
            $disableGenerate = 'true';
        }
        
        $this->view->disableGenerate = $disableGenerate;
        
        if(count($studentList)>0){
			foreach($studentList as $index=>$student){
				
				//get subject registration info
				$subjectRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
				$subject  =  $subjectRegDB->getSemesterSubjectStatus($idSemester,$student["IdStudentRegistration"],$idSubject);
				$studentList[$index]["IdStudentRegSubjects"] = $subject["IdStudentRegSubjects"];
				$studentList[$index]["student_mark"] = $subject["final_course_mark"];
				$studentList[$index]["grade_point"] = $subject["grade_point"];
				$studentList[$index]["grade_desc"] = $subject["grade_desc"];
				$studentList[$index]["grade_status"] = $subject["grade_status"];
				$studentList[$index]["exam_status"] = $subject["exam_status"];
				$studentList[$index]["approvedby"] = $subject["mark_approveby"];
				$studentList[$index]["approveddt"] = $subject["mark_approvedt"];				
			}
	  	}
		$this->view->student_list = $studentList;
	}
	
	
	function searchCourseGroupAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$iduser = $auth->getIdentity()->iduser;
		
		
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			/*
			 * Search Group
			*/		
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getMarkEntryGroupList($subject_id,$semester_id);
			
			
			$i=0;
			foreach($groups as $group){
					
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
					
				$group["total_student"] = $total_student;
				$groups[$i]=$group;
					
				$i++;
			}
			/*
			 * End search group
			*/
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($groups);
		
			echo $json;
			exit();
		
		}
	}
	
	
public function searchCourseAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);

			
		if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Subject
			*/
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
			//Subject list base on Program Landscape from the selected faculty
			//$programs = $progDB->fngetProgramDetails($faculty_id);
				
				
			$allsemlandscape = null;
			$allblocklandscape = null;
			$i=0;
			$j=0;
			//foreach ($programs as $key => $program){
				$activeLandscape=$landscapeDB->getAllActiveLandscape($program_id);
				foreach($activeLandscape as $actl){
					if($actl["LandscapeType"]==43){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}elseif($actl["LandscapeType"]==44){
						$allblocklandscape[$j] = $actl["IdLandscape"];
						$j++;
					}
				}
			//}
				
				
			$subjectsem = null;
			$subjectblock = null;
				
			if(is_array($allsemlandscape))
				$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,null,$semester_id);
			if(is_array($allblocklandscape))
				$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,null,$semester_id);
				
			$subjects = null;
			if(is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=array_merge( $subjectsem , $subjectblock );
			}else{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=$subjectblock;
				}
			}
			//end subject list
			
			$i=0;
			foreach($subjects as $subject){
					
				//get total student register this subject
				$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
				$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$semester_id);
				$subject["total_student"] = $total_student;
					
				//get total group creates
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$semester_id);
				$subject["total_group"] = $total_group;
				$subject["IdSemester"] = $semester_id;				
					
				$subjects[$i]=$subject;
					
				$i++;
			}
				
			foreach ($subjects as $index => $subject){
				if($subject["total_student"]>0 && $subject["total_group"]>0){
						
				}else{
					unset($subjects[$index]);
				}
			}
			/*
			 * End search subject
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($subjects);
	
			echo $json;
			exit();
		}
	}
	
	
	function generateAction(){
		
		/*if($this->_getparam("gen",0)=="ALL"){
			echo "gen all<br>";
			$gradeDB = new Examination_Model_DbTable_Grade();
			$meDB = new Examination_Model_DbTable_StudentMarkEntry();
			$subregs = $gradeDB->listsubjectreg();
			//echo count($subregs);
			foreach($subregs as $subj){
				$meDB->getStudentTotalMark($subj['IdSemesterMain'],$subj['IdProgram'],$subj['IdSubject'],0,$subj["IdStudentRegSubjects"]);
			}
			echo "Done"; 
			exit;
		}*/  
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			$academicDB = new Examination_Model_DbTable_Academicstatus();
		    $gradeDB = new Examination_Model_DbTable_Grade();
			$subjectRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			$landscapeBlockSubjectDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();  
			$meDB = new Examination_Model_DbTable_StudentMarkEntry();
			   	    			    		
    		for($i=0; $i<count($formData['idStudentRegSub']); $i++){

	    			$IdStudentRegSubjects =  $formData['idStudentRegSub'][$i];
	    			//$student_mark         =  $formData['student_mark'][$IdStudentRegSubjects];
	    			
	    			$student_mark = $meDB->getStudentTotalMark($formData['idSemester'],$formData['idProgram'],$formData['idSubject'],0,$IdStudentRegSubjects);
		    		 //mappkan markah dgn grade				
				     /*$grade	= $gradeDB->getGrade($formData['idSemester'],$formData['idProgram'],$formData['idSubject'],$student_mark);			
					
				     if(isset($grade["Pass"])){
					     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
					     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
				     	 else $grade_status='';
				     }else{
				     	 $grade_status='';
				     }
					 
					 $data["final_course_mark"]=$student_mark;
					 $data["grade_point"]=$grade["GradePoint"];
					 $data["grade_name"]=$grade["GradeName"];
					 $data["grade_desc"]=$grade["GradeDesc"];
					 $data["grade_status"]=$grade_status; //status should consider on mark distribution component grade status for now belum checking lagi just amik status di grade setup (22/11/2013)
		
		    		 $subjectRegDB->updateData($data,$IdStudentRegSubjects);*/
		    		
		    		
		    		
		    		//update parent info
		    		$subject = $subjectRegDB->getData($IdStudentRegSubjects);
		    		
	    			if($subject["subjectlandscapetype"]==3){ //anak
									
		    				//sekiranya anak kene dapatkan sape bapak dia
		    				$subject_block_info = $landscapeBlockSubjectDb->getLandscapeSubjectInfo($subject['IdLandscape'],$formData['idSubject']);
							$parentId = $subject_block_info["parentId"];
							
							//cari anak-anak yg lain
							$children = $landscapeBlockSubjectDb->getChildByParentId($parentId);
	
							$total_credit_hour = 0;
		    				$child_point =0 ;
							$child_total_grade_point = 0;
							
							foreach($children as $child){
																	
								//amik the highest mark
								$child_mark  = $subjectRegDB->getHighestMarkofAllSemester($subject["IdStudentRegistration"],$child["subjectid"]);
								$child_point = $child_mark["CreditHours"] * $child_mark["grade_point"];
								$child_total_grade_point = abs($child_total_grade_point) + abs($child_point);

								$total_credit_hour = $total_credit_hour+$child_mark["CreditHours"];
								
							}//end foreach
							
							$parent_grade_point = $child_total_grade_point/$total_credit_hour;
								
						
					}//end if anak
				
    		}//end for
    		   		
    	}//end if
    		
    	
    	//redirect
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'generate-grade', 'action'=>'group-list','msg'=>1),'default',true));
		  
		
	}
	
	function updateChildMarkAction(){
		
		//nak update child mark sebab ada child ada grade xde mark
		
		//get all parent from studentregsubjects
		$subjectRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
		$landscapeBlockSubjectDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		
		$subject_register = $subjectRegDB->getSubjectParent();
				
		foreach($subject_register as $index=>$subreg){
			//get higherst mark
			$subject_highest_mark = $subjectRegDB->getHighestMarkofAllSemester($subreg['IdStudentRegistration'],$subreg['IdSubject']);
			$subject_register[$index]['highest_mark']=$subject_highest_mark["final_course_mark"];			
		}
		
		
		echo '<pre>';
		//print_r($subject_register);
		echo '</pre>';
				
		
		$update =1;
		foreach ($subject_register as $index=>$subject){	

			$final_course_mark = $subject['highest_mark'];
			
			//get block info
			$children = $landscapeBlockSubjectDb->getChild($subject["IdSubject"],$subject["IdBlock"],$subject["IdLandscape"]);			
			
			echo '<table border=1>
				  <tr><td colspan=3>'.$index.') '.$subject['IdStudentRegistration'].'</td></tr>
				  <tr><td>Parent/Student ID</td><td>Parent/Student Mark</td><td>&nbsp;Grade Point</td><tr>
				  <tr bgcolor="#F7F2E0"><td>'.$subject["IdSubject"].'</td><td>'.$final_course_mark.'</td><td>&nbsp;</td><tr>';
			
			$c=1;
			
			foreach($children as $child){
				
				//get info
				$subject_status = $subjectRegDB->getSemesterSubjectStatusDua($subject['IdStudentRegistration'],$child["subjectid"]);

				//update child
				if(is_array($subject_status)){	

					echo '<tr><td>'.$child["subjectid"].'</td><td>'.$subject_status["final_course_mark"].'&nbsp;</td><td>'.$subject_status["grade_point"].'&nbsp;</td></tr>';
					
					echo '<tr><td colspan=3>Update =>'.$update.'</td></tr>';
					$update++;	
					//$subjectRegDB->updateChildMark(array('final_course_mark'=>$final_course_mark),$subject['IdStudentRegistration'],$subject["IdSubject"],$subject['IdSemesterMain']);
					
				}else{
					echo '<tr><td colspan=3>NOT</td></tr>';
				}
		
				
			$c++;
			
			}
				echo '</table><br>';
		}
		
		exit;
	}
	
}

?>