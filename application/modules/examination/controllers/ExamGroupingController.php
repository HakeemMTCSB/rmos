<?php
/**
 *  @author alif 
 *  @date Oct 09, 2013
 */
 
class Examination_ExamGroupingController extends Base_Base {

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function groupAction() {
		
		//title
		$this->view->title= $this->view->translate("Exam Grouping");
		 
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		
		$semid = $this->_getParam('semid', null);
		$this->view->semid = $semid;
		
		$facid = $this->_getParam('facid', null);
		$this->view->facid = $facid;
		
		$subid = $this->_getParam('subid', null);
		$this->view->subid = $subid;
		
		
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		//faculty
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
		
		if($this->_sis_session->IdRole == 1){
			$collegeList = $collegeDb->getCollege();
		}else{
			$this->view->default_faculty = $this->_sis_session->idCollege;
			$collegeList = array('0'=>$collegeDb->fngetCollegemasterData($this->_sis_session->idCollege));
		}
		
		$this->view->college_list = $collegeList;
		
	}
	
	public function groupDetailAction(){
		
		$id = $this->_getParam('id', 0);
		$this->view->group_id = $id;
		
		$subject_id = $this->_getParam('subject_id', 0);
		$this->view->subject_id = $subject_id;
		
		//title
		$this->view->title= $this->view->translate("Exam Grouping : Detail");
		
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();
		
		//group data
		$group = $examGroupDb->getData($id);
		$this->view->group = $group;
										
		
		//group program
		$examGroupProgramDb = new Examination_Model_DbTable_ExamGroupProgram();
		$this->view->program = $examGroupProgramDb->getGroupData($id);
										
		//faculty list
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$facultyList = $facultyDb->getAllFaculty();
		$this->view->faculty_list = $facultyList;
		
		//program for each faculty
		$progDB= new GeneralSetup_Model_DbTable_Program();
		$program_list = $facultyList;
		foreach ($facultyList as $index=>$faculty){
			$program_list[$index]['programs'] = $progDB->fngetProgramDetails($faculty['IdCollege']);
		}
		$this->view->program_list = $program_list;
		
		//supervisor data
		$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
		$this->view->supervisor_list = $examSupervisorDb->getSupervisorList($id);

		//student data
		$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$this->view->student_list = $examGroupStudentDb->getStudentList($id,$subject_id,$group['eg_sem_id']);
		
		//list course group yg ada dalam student list
		$this->view->student_course_group = $examGroupStudentDb->getListCourseGroup($id,$subject_id,$group['eg_sem_id']);
		
		//class group
		$classGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$this->view->class_group_list = $classGroupDb->getGroupList($group['eg_sub_id'],$group['eg_sem_id']);
		
		

	}
	
	public function groupAddAction(){
		
		$semester_id = $this->_getParam('semester_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		
		//title
		$this->view->title= $this->view->translate("Exam Grouping : Add");
		
		//faculty list
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$this->view->faculty_list = $facultyDb->getFaculty();
		 
		//program list
		$programDb = new App_Model_Record_DbTable_Program();
		$program = array();
		foreach ($this->view->faculty_list as $faculty){
		  $where = array(
		      'IdCollege = ?' => $faculty['IdCollege'],
		      'Active = ?' => 1
		  );
		  $programList = $programDb->fetchAll($where);
		   
		  $program[] = array(
		      'faculty' => $faculty,
		      'program' => $programList->toArray()
		  );
		}
		$this->view->program_list = $program;
		
		$form = new Examination_Form_ExamGroup(array('semester_id'=>$semester_id, 'subject_id'=>$subject_id));
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
				
				$examGroupDb = new Examination_Model_DbTable_ExamGroup();
				$data = array(
					'eg_sem_id' => $formData['eg_sem_id'],
					'eg_sub_id'	=> $formData['eg_sub_id'],
					'eg_assessment_type' => $formData['eg_assessment_type'],
					'eg_repeat_status' => $formData['eg_repeat_status'],
					'eg_group_name'	=> $formData['eg_group_name'],
					'eg_group_code'	=> $formData['eg_group_code'],
					'eg_room_id'	=> $formData['eg_room_id'],
					'eg_date' => date('Y-m-d', strtotime($formData['eg_date'])),
					'eg_start_time'	=> $formData['eg_start_time'],
					'eg_end_time'	=> $formData['eg_end_time'],
					'eg_capacity' => $formData['eg_capacity']
				);
				
				$idGroup = $examGroupDb->insert($data);
				
				//course group program
				if( isset($formData['program']) ){
				  $examGroupProgramDb = new Examination_Model_DbTable_ExamGroupProgram();
				  foreach($formData['program'] as $program){
				    $dt = array(
				        'egp_eg_id' => $idGroup,
				        'egp_program_id' => $program
				    );
				     
				    $examGroupProgramDb->insert($dt);
				  }
				}
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'group','group'=>$formData['group_id']),'default',true));
				
			}else{
				$form->populate($formData);
			}
		}
		
		$this->view->form = $form;
		
	}
	
	public function groupEditAction(){
		$group_id = $this->_getParam('gid', null);
		
		//title
		$this->view->title= $this->view->translate("Exam Grouping : Edit");
		
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();
		$group = $examGroupDb->getData($group_id);
		$group['eg_date'] = date('d-m-Y',strtotime($group['eg_date']));
		
		$form = new Examination_Form_ExamGroup(array('semester_id'=>$group['eg_sem_id'], 'subject_id'=>$group['eg_sub_id']));
		
		//faculty list
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$this->view->faculty_list = $facultyDb->getFaculty();
		
		//program list
		$programDb = new App_Model_Record_DbTable_Program();
		$program = array();
		foreach ($this->view->faculty_list as $faculty){
		  $where = array(
		      'IdCollege = ?' => $faculty['IdCollege'],
		      'Active = ?' => 1
		  );
		  $programList = $programDb->fetchAll($where);
		   
		  $program[] = array(
		      'faculty' => $faculty,
		      'program' => $programList->toArray()
		  );
		}
		$this->view->program_list = $program;
		
		
		//group program
		$examGroupProgramDb = new Examination_Model_DbTable_ExamGroupProgram();
		$data_program = $examGroupProgramDb->getGroupData($group_id);
		$this->view->data_program = $data_program;
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				$examGroupDb = new Examination_Model_DbTable_ExamGroup();
				$data = array(
						'eg_sem_id' => $formData['eg_sem_id'],
						'eg_sub_id'	=> $formData['eg_sub_id'],
						'eg_assessment_type'	=> $formData['eg_assessment_type'],
						'eg_repeat_status' => $formData['eg_repeat_status'],
						'eg_group_name'	=> $formData['eg_group_name'],
						'eg_group_code'	=> $formData['eg_group_code'],
						'eg_room_id'	=> $formData['eg_room_id'],
						'eg_date' => date('Y-m-d', strtotime($formData['eg_date'])),
						'eg_start_time'	=> $formData['eg_start_time'],
						'eg_end_time'	=> $formData['eg_end_time'],
						'eg_capacity' => $formData['eg_capacity']
				);
		
				$examGroupDb->update($data,'eg_id = '.$group['eg_id']);
				
				$examGroupProgramDb = new Examination_Model_DbTable_ExamGroupProgram();
				//group program
				if(isset($formData['program_remove'])){
				  foreach ($formData['program_remove'] as $id_to_remove){
				    $examGroupProgramDb->delete('egp_id = '.$id_to_remove);
				  }
				}
				 
				if(isset($formData['program_add'])){
				  foreach ($formData['program_add'] as $id_program_to_add){
				    	
				    $dt = array(
				        'egp_eg_id' => $group['eg_id'],
				        'egp_program_id' => $id_program_to_add
				    );
				     
				    $examGroupProgramDb->insert($dt);
				  }
				}
		
				//redirect
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'group-detail','id'=>$group_id),'default',true));
		
			}else{
				$form->populate($formData);
			}
			
		}else{
			$form->populate($group);
		}
		
		$this->view->form = $form;
	}
	
	public function searchCourseAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
	
		$sis_session = new Zend_Session_Namespace('sis');
		
		if($faculty_id!=null){
			$ses_att_pdf = new Zend_Session_Namespace('att_pdf_ses');
			$ses_att_pdf->faculty_id = $faculty_id;
		}
	
		if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Subject
			*/
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
			//Subject list base on Program Landscape from the selected faculty
			$programs = $progDB->fngetProgramDetails($faculty_id);
				
				
			$allsemlandscape = null;
			$allblocklandscape = null;
			$i=0;
			$j=0;
			foreach ($programs as $key => $program){
				$activeLandscape=$landscapeDB->getAllActiveLandscape($program["IdProgram"]);
				foreach($activeLandscape as $actl){
					if($actl["LandscapeType"]==43){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}elseif($actl["LandscapeType"]==44){
						$allblocklandscape[$j] = $actl["IdLandscape"];
						$j++;
					}
				}
			}
				
				
			$subjectsem = null;
			$subjectblock = null;
				
			if(is_array($allsemlandscape))
				$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape);
			if(is_array($allblocklandscape))
				$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape);
				
			$subjects = null;
			if(is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=array_merge( $subjectsem , $subjectblock );
			}else{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=$subjectblock;
				}
			}
				
				
			//end subject list
			if(count($subjects)==0){
				//Original from yati
				$subjectDb = new  GeneralSetup_Model_DbTable_Subjectmaster();
				$subjects = $subjectDb->getSubjectByCollegeId( array('IdCollege'=>$faculty_id,'IdSemester'=>$semester_id,'SubjectCode'=>null) );
			}
			$i=0;
			foreach($subjects as $subject){
					
				//get total student register this subject
				$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
				$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$semester_id);
				$subject["total_student"] = $total_student;
					
				//get total group creates
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$semester_id);
				$subject["total_group"] = $total_group;
				$subject["IdSemester"] = $semester_id;
					
				//get total no of student has been assigned
				$total_assigned = $subjectRegDB->getTotalAssigned($subject["IdSubject"],$semester_id);
				$total_unassigned = $subjectRegDB->getTotalUnAssigned($subject["IdSubject"],$semester_id);
				$subject["total_assigned"] = $total_assigned;
				$subject["total_unassigned"] = $total_unassigned;
					
				$subjects[$i]=$subject;
					
				$i++;
			}

			//Request from pak agung to open because of some faculty will insert exam schedule first
			/*foreach ($subjects as $index => $subject){
				if($subject["total_student"]>0 && $subject["total_group"]>0){
						
				}else{
					unset($subjects[$index]);
				}
			}*/
			
			/*
			 * End search subject
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($subjects);
	
			echo $json;
			exit();
		}
	}
	
	public function searchExamGroupAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
		$subject_id = $this->_getParam('subject_id', null);
	
		$sis_session = new Zend_Session_Namespace('sis');
	
		if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Group
			*/
			//get Subject Info
			$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			$subject = $subjectDb->getData($subject_id);
			$this->view->subject = $subject;
				
			$courseGroupDb = new Examination_Model_DbTable_ExamGroup();
			$groups = $courseGroupDb->getGroupList($subject_id,$semester_id);
				
			
			foreach($groups as $index=>$group){

				$total_student = 0;
				
				$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
				$total_student = $examGroupStudentDb->getTotalStudentAssigned($group["eg_id"]);
					
				$group["total_student"] = $total_student;
				
				
				//group program
				$examGroupProgramDb = new Examination_Model_DbTable_ExamGroupProgram();
				$group["program"] = $examGroupProgramDb->getGroupData($group["eg_id"]);
				
				$groups[$index]=$group;
			}
			/*
			 * End search group
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($groups);
	
			echo $json;
			exit();
	
		}
	}
	
	public function getExamRoomAction(){
		$this->_helper->layout()->disableLayout();
	
		$location_id = $this->_getParam('location_id', null);
		
		$room_id = $this->_getParam('room_id', null);
	
		if ($this->getRequest()->isPost()) {
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			$roomDb = new App_Model_Application_DbTable_PlacementTestRoom();
			
			if($room_id!=null){
				$rooms = $roomDb->getData($room_id);
			}else{
				$rooms = $roomDb->getLocationVenue($location_id);
			}
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($rooms);
	
			echo $json;
			exit();
	
		}
	}
	
	public function ajaxGetStaffListAction(){
	
		$idCollege = $this->_getParam('idCollege',null);
		 
		$this->_helper->layout->disableLayout();
	
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$db = Zend_Db_Table::getDefaultAdapter();
       
	    $select = $db->select()
	 				 ->from(array("sm"=>"tbl_staffmaster"))
	 				 ->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=sm.IdCollege',array('College'=>'ArabicName'))
	 				 ->where("sm.Active = 1")
	 				 ->order('sm.FullName');
	 				
	 	if($idCollege){
	 		$select->where("sm.IdCollege = ?",$idCollege);	
	 	}

	 	//echo $select;
		$row = $db->fetchAll($select);
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($row);
	
		echo $json;
		exit();
	}
	
	public function addSupervisorAction(){
	
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
			
			$data = array(
				'egs_eg_id' => $formData['group_id'],
				'egs_staff_id' => $formData['supervisor']
			);
			
			$examSupervisorDb->insert($data);
	
		}
	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'group-detail','id'=>$formData['group_id']),'default',true));
	
	}
	
	public function removeSupervisorAction(){
		
		$group_id = $this->_getParam('gid',null);
		$supervisor_id = $this->_getParam('sid',null);
		
		if($group_id!=null && $supervisor_id!=null){
			$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
			$examSupervisorDb->delete('egs_id = '.$supervisor_id.' and egs_eg_id = '.$group_id);
		}
		
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'group-detail','id'=>$group_id),'default',true));
	}
	
	public function addStudentAction(){
		
		
		$idSubject = $this->_getParam('subject_id',null);
		$idSemester = $this->_getParam('semester_id',null);
		$idProgram = $this->_getParam('program_id',0);
		$classgroup_id = $this->_getParam('classgroup_id',0);
		$assessment_type_id = $this->_getParam('assessment_type', null);
			
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
			
			if( isset($formData['student']) ){
				foreach ($formData['student'] as $std_index){
					$data = array(
						'egst_subject_id' =>$formData['subject_id'],
						'egst_semester_id' =>$formData['semester_id'],
						'egst_group_id' =>$formData['group_id'],
						'egst_student_id' =>$formData['student_id'][$std_index],
						'egst_student_nim' =>$formData['student_nim'][$std_index],
					);
					
					$examGroupStudentDb->insert($data);
					
				}
			}
			
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'group-detail','id'=>$formData['group_id']),'default',true));
		}else{
			
			$this->_helper->layout->disableLayout();

			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			
			//get student who registered to subject and not assigned to exam group
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$sub_select = $db->select()
					->from(array('egst'=>'exam_group_student'),array('egst_student_id'))
					->join(array('eg'=>'exam_group'),'eg.eg_id = egst.egst_group_id', array())
					->where('egst.egst_semester_id =?',$idSemester)
					->where('egst.egst_subject_id =?',$idSubject)
					->where('eg.eg_assessment_type =?',$assessment_type_id);
			
			$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgram'))
	 				 ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration',array('IdSubject','IdSemesterMain'))
	 				 ->join(array('ctg'=>'tbl_course_tagging_group'),' ctg.IdSubject = '.$idSubject.' and ctg.IdSemester = '.$idSemester, array('GroupName', 'GroupCode'))
	 				 ->join(array('cgsm'=>'tbl_course_group_student_mapping'),'cgsm.IdCourseTaggingGroup = ctg.IdCourseTaggingGroup and cgsm.IdStudent = sr.IdStudentRegistration', array())
	 				 ->joinLeft(array('ap'=>'student_profile'),'ap.appl_id=sr.IdApplication',array('appl_fname','appl_mname','appl_lname'))
	 				 ->joinLeft(array('prg'=>'tbl_program'),'prg.IdProgram = sr.IdProgram', array('ProgramName'=>'ArabicName','ProgramCode'=>'ProgramCode'))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where("sr.IdStudentRegistration NOT IN ?", $sub_select)
	 				 ->order("registrationId");

			if($idProgram!=0){
				$select->where('sr.IdProgram =?',$idProgram);
			}
			
			if($classgroup_id!=0){
				$select->where('ctg.IdCourseTaggingGroup =?',$classgroup_id);
			}
			
			$row = $db->fetchAll($select);
			
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($row);
			
			echo $json;
			exit();
		}
		
		exit();
	}
	
	public function removeStudentAction(){
		
		$group_id = $this->_getParam('gid',null);
		$id = $this->_getParam('sid',null);
		
		if($group_id!=null && $id!=null){
			$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
			$examGroupStudentDb->delete('egst_id = '.$id.' and egst_group_id = '.$group_id);
		}
		
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'group-detail','id'=>$group_id),'default',true));
	}
	
	public function reportAttendancePdfAction(){
		
		$group_id = $this->_getParam('gid',null);
		$course_group_id = $this->_getParam('cgid',null);
		$sem_id = $this->_getParam('sem_id',null);
		$subject_id = $this->_getParam('subject_id',null);
		
		//faculty data
		$ses_att_pdf = new Zend_Session_Namespace('att_pdf_ses');
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDb->getData($ses_att_pdf->faculty_id);
		Global $faculty_data;
		$faculty_data = $faculty;
		
		//group data
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();
		$group = $examGroupDb->getData($group_id);
		
		$examStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$group['total_student'] = $examStudentDb->getTotalStudentAssigned($group_id);
		
		Global $group_data;
		$group_data = $group;
		
		
		//supervisor data
		$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
		$supervisor_list = $examSupervisorDb->getSupervisorList($group_id);
		
		Global $supervisorlist;
		$supervisorlist = $supervisor_list;
		

		//student data
		$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$student_list = $examGroupStudentDb->getStudentList($group_id,$subject_id,$sem_id,$course_group_id);

		//get course group info
		$course_group_list = $examGroupStudentDb->getListCourseGroup($group_id,$subject_id,$sem_id,$course_group_id);
		Global $coursegrouplist;
		$coursegrouplist = $course_group_list;
				
		
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$documentDb = new App_Model_Application_DbTable_ApplicantUploadFile();
		
		foreach ($student_list as $index=>$student){
			
			//financial status
			$student_list[$index]['invoice'] = $invoiceMainDb->getApplicantInvoiceData($student['appl_id']);
			$student_list[$index]['invoice_balance'] = $invoiceMainDb->getApplicantInvoiceBalanceAmount($student['appl_id']);
			
			if( $student_list[$index]['invoice_balance']>0 ){
				$student_list[$index]['payment_status'] = "BL";
			}else{
				$student_list[$index]['payment_status'] = "L";
			}
			
			//photo
			$photo = $documentDb->getTxnFile($student['transaction_id'],51);
			$student_list[$index]['photo_raw'] = $photo;
			
			$fnImage = new icampus_Function_General_Image();
			$student_list[$index]['photo'] = $fnImage->getImagePath($photo['pathupload'],93,100);

		}
		
		
		Global $studentlist;
		$studentlist = $student_list;
		
		
		
		/*
		 * PDF Generation
		*/
		
		require_once 'dompdf_config.inc.php';
		
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		$html_template_path = DOCUMENT_PATH."/template/ExamAttendanceReport.html";
		
		$html = file_get_contents($html_template_path);
		
	
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
		
		
		$dompdf->stream("DaftarHadir_".date('Ymd_Hi').".pdf");
		exit;
	}
	
	public function reportGradePdfAction(){
	
		$group_id = $this->_getParam('gid',null);
	    $course_group_id = $this->_getParam('cgid',null);
	 	$sem_id = $this->_getParam('sem_id',null);
		$subject_id = $this->_getParam('subject_id',null);
		
		//faculty data
		$ses_att_pdf = new Zend_Session_Namespace('att_pdf_ses');
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDb->getData($ses_att_pdf->faculty_id);
		Global $faculty_data;
		$faculty_data = $faculty;
	
		//group data
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();
		$group = $examGroupDb->getData($group_id);
	
		$examStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$group['total_student'] = $examStudentDb->getTotalStudentAssigned($group_id);
	
		Global $group_data;
		$group_data = $group;
	
	
		//supervisor data
		$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
		$supervisor_list = $examSupervisorDb->getSupervisorList($group_id);
		Global $supervisorlist;
		$supervisorlist = $supervisor_list;
	
		//student data
		$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$student_list = $examGroupStudentDb->getStudentList($group_id,$subject_id,$sem_id,$course_group_id);

		//get course group info
		$course_group_list = $examGroupStudentDb->getListCourseGroup($group_id,$subject_id,$sem_id,$course_group_id);
		Global $coursegrouplist;
		$coursegrouplist = $course_group_list;
	
	
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$documentDb = new App_Model_Application_DbTable_ApplicantUploadFile();
	
		foreach ($student_list as $index=>$student){
				
			//financial status
			$student_list[$index]['invoice'] = $invoiceMainDb->getApplicantInvoiceData($student['appl_id']);
			$student_list[$index]['invoice_balance'] = $invoiceMainDb->getApplicantInvoiceBalanceAmount($student['appl_id']);
				
			if( $student_list[$index]['invoice_balance']>0 ){
				$student_list[$index]['payment_status'] = "BL";
			}else{
				$student_list[$index]['payment_status'] = "L";
			}
				
			//photo
			$photo = $documentDb->getTxnFile($student['transaction_id'],51);
			$student_list[$index]['photo_raw'] = $photo;
				
			$fnImage = new icampus_Function_General_Image();
			$student_list[$index]['photo'] = $fnImage->getImagePath($photo['pathupload'],133,100);
	
		}
	
	
		Global $studentlist;
		$studentlist = $student_list;
	
	
	
		/*
		 * PDF Generation
		*/
	
		require_once 'dompdf_config.inc.php';
	
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
	
		$html_template_path = DOCUMENT_PATH."/template/ExamGradeReport.html";
	
		$html = file_get_contents($html_template_path);

		
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
	
	
		$dompdf->stream("DaftarNilai_".date('Ymd_Hi').".pdf");
		exit;
	}
	
	public function reportEventPdfAction(){
	
		$group_id = $this->_getParam('gid',null);
	
		//faculty data
		$ses_att_pdf = new Zend_Session_Namespace('att_pdf_ses');
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDb->getData($ses_att_pdf->faculty_id);
		Global $faculty_data;
		$faculty_data = $faculty;
	
		//group data
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();
		$group = $examGroupDb->getData($group_id);
	
		$examStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$group['total_student'] = $examStudentDb->getTotalStudentAssigned($group_id);
	
		Global $group_data;
		$group_data = $group;
	
	
		//supervisor data
		$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
		$supervisor_list = $examSupervisorDb->getSupervisorList($group_id);
		Global $supervisorlist;
		$supervisorlist = $supervisor_list;
	
		//student data
		$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$student_list = $examGroupStudentDb->getStudentList($group_id);
	
	
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$documentDb = new App_Model_Application_DbTable_ApplicantUploadFile();
	
		foreach ($student_list as $index=>$student){
	
			//financial status
			$student_list[$index]['invoice'] = $invoiceMainDb->getApplicantInvoiceData($student['appl_id']);
			$student_list[$index]['invoice_balance'] = $invoiceMainDb->getApplicantInvoiceBalanceAmount($student['appl_id']);
	
			if( $student_list[$index]['invoice_balance']>0 ){
				$student_list[$index]['payment_status'] = "BL";
			}else{
				$student_list[$index]['payment_status'] = "L";
			}
	
			//photo
			$photo = $documentDb->getTxnFile($student['transaction_id'],51);
			$student_list[$index]['photo_raw'] = $photo;
	
			$fnImage = new icampus_Function_General_Image();
			$student_list[$index]['photo'] = $fnImage->getImagePath($photo['pathupload'],133,100);
	
		}
	
	
		Global $studentlist;
		$studentlist = $student_list;
	
	
	
		/*
		 * PDF Generation
		*/
	
		require_once 'dompdf_config.inc.php';
	
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
	
		$html_template_path = DOCUMENT_PATH."/template/ExamEventReport.html";
	
		$html = file_get_contents($html_template_path);
	
		//echo $html;
		//exit;
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
	
	
		$dompdf->stream("BeritaAcara_".date('Ymd_Hi').".pdf");
		exit;
	}
	
	public function groupAttendanceAction(){
		//title
		$this->view->title= $this->view->translate("Exam Grouping Attendance");
		 
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		//faculty
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
		
		if($this->_sis_session->IdRole == 1){
			$collegeList = $collegeDb->getCollege();
		}else{
			$this->view->default_faculty = $this->_sis_session->idCollege;
			$collegeList = array('0'=>$collegeDb->fngetCollegemasterData($this->_sis_session->idCollege));
		}
		
		$this->view->college_list = $collegeList;
	}
	
	public function groupAttendanceDetailAction(){
		
		$id = $this->_getParam('id', 0);
		$this->view->group_id = $id;
		
		//title
		$this->view->title= $this->view->translate("Exam Grouping Attendance");
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
						
			$examGroupAttendanceDb = new Examination_Model_DbTable_ExamGroupAttendance();
			for($i=0; $i<sizeof($formData['registration_id']); $i++){
				
				if($formData['status'][$i]!=0){
					$cur_data = $examGroupAttendanceDb->getData($formData['group_id'],$formData['registration_id'][$i],$formData['nim'][$i]);
					
					if($cur_data){
						$data = array(
								'ega_status'	=> $formData['status'][$i],
						);
						
						$examGroupAttendanceDb->update($data, 'ega_id = '.$cur_data['ega_id']);
						
					}else{
						$data = array(
								'ega_eg_id' => $formData['group_id'],
								'ega_student_id' => $formData['registration_id'][$i],
								'ega_student_nim' => $formData['nim'][$i],
								'ega_status'	=> $formData['status'][$i]
						);
						
						$examGroupAttendanceDb->insert($data);
					}
				}
			}
			
			$this->view->noticeSuccess = $this->view->translate("Attendance Saved");
			
		}
		
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();
		
		//group data
		$group = $examGroupDb->getData($id);
		$this->view->group = $group;
		
		//student data
		$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$student_list = $examGroupStudentDb->getStudentListAttendance($id);
		
		$this->view->student_list = $student_list;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		$this->view->attendanceStatusList = $attendanceStatusList;
		
		/*echo "<pre>";
		print_r($student_list);
		echo "</pre>";
		exit;*/
	}
	
	public function kpuAction(){
		//title
		$this->view->title= $this->view->translate("Exam Grouping - Exam Slip");
		
		$form = new Examination_Form_SearchStudentSemester();
		$infoPath = $this->getRequest()->getPathInfo();
	    $basePath = $this->getRequest()->getBaseUrl();
	    $form->setAction($basePath . $infoPath."#tabs-2");
		
		$paginator_ses = new Zend_Session_Namespace('paginator_ses');
		
		
		if ($this->getRequest()->isPost()) {
				
			if($form->isValid($this->getRequest()->getPost())){
				
				$formData = $this->getRequest()->getPost();
				$form->populate($formData);
					
				$paginator_ses->form_data = $formData;
				
				$student_list = $this->getStudentSemesterList($formData);
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage($this->gintPageCount);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				$this->view->paginator = $paginator;
			}else{
				$formData = $this->getRequest()->getPost();
				$form->populate($formData);
			}
			
		}else{
			if( isset($paginator_ses->form_data) && $this->_getParam('page',null)!=null ){
				
				$formData = $paginator_ses->form_data;
				$form->populate($formData);
				
				$student_list = $this->getStudentSemesterList($formData);
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage($this->gintPageCount);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				$this->view->paginator = $paginator;
			}
		}
		
		$this->view->form = $form;
		
		
		//semester list
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		
		//assessment type list
		$assessmentTypeDb = new Examination_Model_DbTable_Assessmenttype();
		$assessmentList = $assessmentTypeDb->getdropdownforasseementtype();
		
		//get release status
		$examSlipDb = new Examination_Model_DbTable_ExamSlipRelease();
		
		foreach ($semesterList as $index=>$semester){
			$ass_type = $assessmentList;
			
			foreach ($ass_type as $index2 => $assessment){
				$ass_type[$index2]['release_data'] = $examSlipDb->getReleaseData($semester['key'],$assessment['key']);
			}
			
			$semesterList[$index]['assessment_type'] = $ass_type; 
		}
		$this->view->exam_release = $semesterList;
				
	}

	private function getStudentSemesterList($post){
		$session = new Zend_Session_Namespace('sis');
		$auth = Zend_Auth::getInstance();
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))
		->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration = sa.IdStudentRegistration and sss.IdSemesterMain = '.$post['semester'].' and sss.studentsemesterstatus = 130' )
		->join(array('at'=>'applicant_transaction'),'at.at_trans_id=sa.transaction_id',array('at_pes_id'))
		->joinLeft(array('p'=>'student_profile'),'p.appl_id=sa.IdApplication',array('appl_fname','appl_mname','appl_lname','appl_religion'))
		->joinLeft(array('defination' => 'tbl_definationms'), 'defination.idDefinition=sa.profileStatus', array('profileStatus'=>'DefinitionCode')) //Application STtsu
		->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('prg.ArabicName','ProgramName'))
		//->where("sa.OldIdStudentRegistration IS NULL")//ini letak siap2 ikut team india
		->order("sa.registrationId ASC");
		
		if($session->IdRole == 311 || $session->IdRole == 298){
			$lstrSelect->where("prg.IdCollege =?",$session->idCollege);
		}else{
				
			if(isset($post['IdCollege']) && !empty($post['IdCollege'])){
				$lstrSelect->where("prg.IdCollege =?",$post["IdCollege"]);
			}
		}
		 	
		
		if (isset($post['applicant_name']) && !empty($post['applicant_name'])) {
			 
			$lstrSelect->where("(p.appl_fname LIKE '%". $post['applicant_name']."%'");
			$lstrSelect->orwhere("p.appl_mname LIKE '%". $post['applicant_name']."%'");
			$lstrSelect->orwhere("p.appl_lname LIKE '%". $post['applicant_name']."%')");
		}
		
				
		if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {
			$lstrSelect->where("sa.IdProgram = ?",$post['IdProgram']);
		}
		
			
		
		if (isset($post['student_id']) && !empty($post['student_id'])) {
			$lstrSelect->where("sa.registrationId = ?",$post['student_id']);
		}
		
		//echo $lstrSelect;
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
	
	public function kpuPrintAction(){
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			//get student info
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($formData['sid']);
			
			
			//photo
			$documentDb = new App_Model_Application_DbTable_ApplicantUploadFile();
			$photo = $documentDb->getTxnFile($studentdetails['transaction_id'],51);
			$studentdetails['photo_raw'] = $photo;
				
			$fnImage = new icampus_Function_General_Image();
			$studentdetails['photo'] = $fnImage->getImagePath($photo['pathupload'],150,192);
			
			//academic advisor
			$staffMasterDb = new GeneralSetup_Model_DbTable_Staffmaster();
			$academicAdvisor = $staffMasterDb->fnviewStaffDetails($studentdetails['AcademicAdvisor']);

			// semester
			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDB->fnGetSemestermaster($formData['semid']);
			
			// get course registed in semester selected
			$courseRegisterDb = new Registration_Model_DbTable_Studentregistration();
			$courses = $courseRegisterDb->getCourseRegisteredBySemester($formData['sid'],$formData['semid']);
			
			//find exam schedule
			$examStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
			foreach ($courses as $index=>$course){
				$schedule = $examStudentDb->getExamGroupSchedule($formData['sid'],$formData['semid'],$course['IdSubject'],$formData['ass_type']);
				if($schedule){
					$courses[$index]['exam'] = $schedule[0];
				}
			}
			
			//class group
			$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
			
			//class attendance
			$courseGroupStudentAttendanceDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();
			foreach ($courses as $index=>$course){
				
				$classGroup = $courseGroupStudentDb->checkStudentCourseGroup($formData['sid'],$formData['semid'],$course['IdSubject']);
				
				$classGroup['class_session'] = $courseGroupStudentAttendanceDb->getAttendanceSessionCount($classGroup['IdCourseTaggingGroup'],$formData['sid']);
				$classGroup['class_attended'] = $courseGroupStudentAttendanceDb->getAttendanceStatusCount($classGroup['IdCourseTaggingGroup'],$formData['sid'],395);
				$classGroup['class_attendance_percentage'] = ($classGroup['class_attended']/$classGroup['class_session'] )*100;
				
				$courses[$index]['class_group'] = $classGroup;
			}
			
			//assessment type
			$assessmentTypeDb = new Examination_Model_DbTable_Assessmenttype();
			$assessmentType = $assessmentTypeDb->fnGetAssesmentTypeNamebyID($formData['ass_type']);
			
			//program
			$programDb = new App_Model_Record_DbTable_Program();
			$program = $programDb->getData($studentdetails['IdProgram']);
			
			//faculty data
			$facultyDb = new App_Model_General_DbTable_Collegemaster();
			$faculty = $facultyDb->getData($program['IdCollege']);
			
			Global $semester_data;
			$semester_data = $semester;
			
			Global $faculty_data;
			$faculty_data = $faculty;
						
			Global $student_data;
			$student_data = $studentdetails;
			
			Global $academic_advisor;
			$academic_advisor = $academicAdvisor;

			Global $courses_data;
			$courses_data = $courses;

			Global $assessment_type;
			$assessment_type = $assessmentType[0];
			
			/*
			 * PDF Generation
			*/
			
			require_once 'dompdf_config.inc.php';
			
			$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
			$autoloader->pushAutoloader('DOMPDF_autoload');
			
			$html_template_path = DOCUMENT_PATH."/template/ExamSlip.html";
			
			$html = file_get_contents($html_template_path);
			
			//echo $html;
			//exit;
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->set_paper('a4', 'portrait');
			$dompdf->render();
			
			
			$dompdf->stream("ExamSlip_".$formData['nid']."_".date('Ymd_Hi').".pdf");

		}
		
		
		
		
		exit;
	}
	
	public function examSlipReleaseAction(){
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$examSlipDb = new Examination_Model_DbTable_ExamSlipRelease();
			
			$release_data = $examSlipDb->getReleaseData($formData['esr_semester_id'],$formData['esr_assessment_type_id']);
			
			if($release_data){
				$data = array(
						'esr_status' => $formData['esr_status']
				);
				
				$examSlipDb->update($data, 'esr_id = '.$release_data['esr_id']);
				
			}else{
				$data = array(
						'esr_semester_id' => $formData['esr_semester_id'],
						'esr_assessment_type_id' => $formData['esr_assessment_type_id'],
						'esr_status' => $formData['esr_status']
				);
				
				$examSlipDb->insert($data);
			}
			
		}
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grouping', 'action'=>'kpu'),'default',true));
		
	}
	
}
 ?>
