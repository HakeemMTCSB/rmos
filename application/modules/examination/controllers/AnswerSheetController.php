<?php



class Examination_AnswerSheetController extends Zend_Controller_Action {

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Answer Sheet - Search Course");
    	
    	$form = new Examination_Form_SearchAssessmentComponent();
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$progDB= new GeneralSetup_Model_DbTable_Program();
				$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
				$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
				$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
				
				$i=0;
				$j=0;
				$allsemlandscape = null;
				$allblocklandscape = null;
				
			    $activeLandscape=$landscapeDB->getAllActiveLandscape($formData["IdProgram"]);
					
			        foreach($activeLandscape as $actl){
						if($actl["LandscapeType"]==43){
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}elseif($actl["LandscapeType"]==44){
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				
				if(is_array($allsemlandscape))
					$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,$formData);
					
				//echo count($subjectsem);
				
				if(is_array($allblocklandscape))
					$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,$formData);
					
				//echo count($subjectblock);
				
				if(is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=array_merge( $subjectsem , $subjectblock );
				}else{
					if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
						$subjects=$subjectsem;
					}
					elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
						$subjects=$subjectblock;
					}		
				}//if else
				
				
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($subjects));
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				
				$this->view->list_subject = $subjects;
				
				//print_r($subjects);
				
			}//if form valid
    	}//if post		
    }
    
    
	public function viewAction(){
    	
    	$this->view->title=$this->view->translate("Answer Sheet - Mark Distribution");
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
		
    	
    	//check kalo belum ada keluarkan button add
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$total = $markDistributionDB->checkExistEntry($idSemester,$idProgram,$idSubject);
    	$this->view->total = $total;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	
    	//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject);	    
    	$this->view->rs_component = $list_component;
    	//print_r($list_component);
    }
    
    public function examListAction(){
    	
    	$this->view->title=$this->view->translate("Answer Sheet - Exam List/Schedule");
    	
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->IdMarksDistributionDetails = $IdMarksDistributionDetails;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
		
    }
    
    
	public function roomDetailAction(){
    	
    	$this->view->title=$this->view->translate("Answer Sheet - Room Detail");
    	
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->IdMarksDistributionDetails = $IdMarksDistributionDetails;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	$form = new Examination_Form_AnswerSheetUpload(array('idSemesterx'=>$idSemester,'idProgramx'=>$idProgram,'idSubjectx'=>$idSubject,'idMaster'=>$IdMarksDistributionMaster,'idDetail'=>$IdMarksDistributionDetails));
		$this->view->form = $form;
    }
    
    
    public function uploadOmrAction(){
    	
    		
    	$this->view->title=$this->view->translate("Answer Sheet - Upload OMR");
    	
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->IdMarksDistributionDetails = $IdMarksDistributionDetails;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	$form = new Examination_Form_AnswerSheetUpload(array('idSemesterx'=>$idSemester,'idProgramx'=>$idProgram,'idSubjectx'=>$idSubject,'idMasterx'=>$IdMarksDistributionMaster,'idDetailx'=>$IdMarksDistributionDetails));
		$this->view->form = $form;
    	
    	if ($this->_request->isPost()) {
	
	       		$formData = $this->_request->getPost();	
    	
	       		//get info semester
		    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		    	$this->view->semester = $semesterDB->fngetSemestermainDetails($formData["idSemester"]);
		    	
		    	//get info program
		    	$programDB = new GeneralSetup_Model_DbTable_Program();
		    	$this->view->program = $programDB->fngetProgramData($formData["idProgram"]);
		    	
		    	//get info subject
		    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		    	$this->view->subject = $subjectDB->getData($formData["idSubject"]);
	    	       		
		    	
	       		//print_r($formData);
	       		
		    	
	       		
	       		  // success - do something with the uploaded file
	                $uploadedData = $form->getValues();
	                $fullFilePath = $form->file->getFileName();
					
	                $filename=explode("/",$fullFilePath);
	                $filename=end($filename);

	               // Zend_Debug::dump($uploadedData, '$uploadedData');
	                //Zend_Debug::dump($fullFilePath, '$fullFilePath');
	                
		            $schemaFile = $fullFilePath;
					$lines = file($schemaFile);
				
					$file = fopen($fullFilePath, "r") or exit("Unable to open file!");

					$total_question = 100;
					
					//Output a line of the file until the end is reached
					$i=0;
					$arr_sets = array();
					
					while(!feof($file)){
						$line_data = fgets($file);	
						$arr_sets[$i]["set_code"]=str_replace(" ","",substr($line_data, 0,6));							
						$arr_sets[$i]["student_nim"]=substr($line_data, 6,12);
						$arr_sets[$i]["student_name"]=substr($line_data, 21,30);
						$arr_sets[$i]["student_answer_raw"]=substr($line_data, 51,$total_question);
						
					$i++;
					}
					
					fclose($file);

					//print_r($arr_sets);
									
                	//remove file
					unlink($fullFilePath);
					
					
					
					
    			   //save as raw
				   foreach($arr_sets as $set){				   	
				   	   
					   $tmpData["tmp_name"]=$set["student_name"];
					   $tmpData["tmp_IdMarksDistributionMaster"]=$IdMarksDistributionMaster;
					   $tmpData["tmp_IdMarksDistributionDetails"]=$IdMarksDistributionDetails;
					   $tmpData["tmp_set_code"]=$set["set_code"];
					   $tmpData["tmp_answers_raw"]=$set["student_answer_raw"];
					   $tmpData["tmp_file"]=$filename;
					   
					   $markRawDb = new Examination_Model_DbTable_StudentMarkRaw();
					   $markRawDb->addData($tmpData);					   
				   }
				   
				   					
				   $cont=0;
				   foreach ($arr_sets as $key=>$line){										
					
					   if( $line['set_code'] != "" ){					
							
						    //1st: search and map student by NIM
					   		$studentDB = new Examination_Model_DbTable_StudentRegistration();
		    	        	$student = $studentDB->getStudentMapping($formData["idSemester"],$formData["idProgram"],$formData["idSubject"],$formData["idMaster"],$line["student_nim"]);
					   	
			    	        if(is_array($student)){
			    	        	
			    	        	$arr_sets[$cont]["IdStudentRegistration"]=$student["IdStudentRegistration"];
			    	        	$arr_sets[$cont]["IdStudentRegSubjects"]=$student["IdStudentRegSubjects"];
			    	        	$arr_sets[$cont]["name"]=$student["appl_fname"].' '.$student["appl_mname"].' '.$student["appl_lname"];
			    	        	$arr_sets[$cont]['IdStudentMarksEntry']=$student["IdStudentMarksEntry"];
	    						$arr_sets[$cont]['TotalMarkObtained']=$student["TotalMarkObtained"];
	    						$arr_sets[$cont]['MarksEntryStatus']=$student["MarksEntryStatus"];
			    	        	
			    	        }else{
			    	        	
			    	        	$arr_sets[$cont]["IdStudentRegistration"]='';
			    	        	$arr_sets[$cont]["IdStudentRegSubjects"]='';
			    	        	$arr_sets[$cont]["name"]='';
			    	        	$arr_sets[$cont]['IdStudentMarksEntry']='';
	    						$arr_sets[$cont]['TotalMarkObtained']='';
	    						$arr_sets[$cont]['MarksEntryStatus']='';
			    	        }
			    	        
			    	        
			    	        
			    	        
			    	        //2nd : check if answer scheme is exist/uploaded
			    	        $ansSchemeDb   = new Examination_Model_DbTable_StudentAnswerScheme();
				   			$scheme_exist = $ansSchemeDb->checkExist($formData["idMaster"],$formData["idDetail"],$line["set_code"]);

				   			if(isset($scheme_exist)){
				   				$arr_sets[$cont]['scheme_exist_status']='';
				   			}else{
				   				$arr_sets[$cont]['scheme_exist_status']='No Uploaded Scheme';
				   			}
				   			
				   			
				   			
														
						}else{						
							unset($arr_sets[$key]);
						}
				   $cont++;
				   }
				   
				    $this->view->arr_sets = $arr_sets;
				   // print_r($arr_sets);
				  
				 	
				    
				    //save as raw
				   foreach($arr_sets as $set){				   	
				   	   
					   $tmpData["tmp_name"]=$set["student_name"];
					   $tmpData["tmp_IdMarksDistributionMaster"]=$IdMarksDistributionMaster;
					   $tmpData["tmp_IdMarksDistributionDetails"]=$IdMarksDistributionDetails;
					   $tmpData["tmp_set_code"]=$set["set_code"];
					   $tmpData["tmp_answers_raw"]=$set["student_answer_raw"];
					   $tmpData["tmp_file"]=$filename;
					   
					   $markRawDb = new Examination_Model_DbTable_StudentMarkRaw();
					   $markRawDb->addData($tmpData);					   
				   }
				   
				
    	}
    	
    }
    
    
    public function mapStudentListAction(){
    	
    	
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);    
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    	// disable layouts for this action:
        $this->_helper->layout->disableLayout();
        
        $form = new Examination_Form_MarkEntrySearchStudent();
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudent($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster,$formData);
	    	
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudent($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster);
	    		
    	} //end if
    	
    	$this->view->student_list = $students;
    }
    
     
    public function saveMappingAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	$stack_failed = array();
    	
    	 if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//print_r($formData);
			
			for($i=1; $i<=count($formData["IdStudentRegistration"]); $i++){
				
				$IdStudentRegistration = $formData["IdStudentRegistration"][$i];
				
				//check if schema jawapan utk set ni belum ada jangan save
				if($formData["scheme_exist_status"]!=''){
					
						if($IdStudentRegistration!=''){
						
								//mapping match and save info
								
								$soa["soa_IdStudentRegistration"]=$IdStudentRegistration;
								$soa["soa_IdMarksDistributionMaster"]=$formData["IdMarksDistributionMaster"];
								$soa["soa_IdMarksDistributionDetails"]=$formData["IdMarksDistributionDetails"];
								$soa["soa_set_code"]=$formData["set_code"][$i];
								$soa["soa_status"]=1;
								$soa["soa_createddt"]=date('Y-m-d H:i:s');
								$soa["soa_createdby"]=$auth->getIdentity()->iduser;
								
								$omrDB = new Examination_Model_DbTable_StudentOmrAnswer();
								$soa_id = $omrDB->addData($soa);
								
								
								
								 /* get answer for each question*/					
								   	
								$tempAns = array();
								$j = strlen($formData['student_answer_raw'][$i]);
								
								for ($k = 0; $k < $j; $k++) {
									$char = substr($formData['student_answer_raw'][$i], $k, 1);
									// do stuff with $char
									$tempAns[$k] = $char;
								}	
							
								/*end get answer*/
								
								
								$quest_no =1;
								foreach($tempAns as  $key=>$value){
									
									// check jawapan betul atau salah
									// get schema jawapan 
									$ansSchemeDb = new Examination_Model_DbTable_StudentAnswerScheme();
									$result = $ansSchemeDb->getAnswer($formData["IdMarksDistributionMaster"],$formData["IdMarksDistributionDetails"],$soa["soa_set_code"],$quest_no,$value);
									
									$soad["soad_soa_id"]=$soa_id;
									$soad["soad_ques_no"]=$quest_no;
									$soad["soad_student_ans"]=$value;
									$soad["soad_status_ans"]=$result;						
									
									$omrDetailDB = new Examination_Model_DbTable_StudentOmrAnswerDetail();
									$omrDetailDB->addData($soad);
									
									//print_r($soad);
									
								$quest_no++;
								}
								/*end get answer*/
							
						}else{
							array_push($stack_failed,array('idStudentRegistration'=>$IdStudentRegistration,'comments'=>'No Match Student ID'));
						}//end soa id reg
				
				}else{
						array_push($stack_failed,array('idStudentRegistration'=>$IdStudentRegistration,'comments'=>'No Answer Scheme Uploaded'));
						
				}//end if scheme exist
								
			}//end for 
			
			
			//$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'answer-sheet', 'action'=>'add-student-list','idSubject'=>$formData["idSubject"],'subjectcode'=>$formData["subjectcode"],'idSemester'=>$formData["idSemester"],'msg'=>1),'default',true));
    	 }
    			
    }
    
    
	public function studentListAction(){
    	
    	
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->IdMarksDistributionDetails = $IdMarksDistributionDetails;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    
    	 //get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);

    	
        $form = new Examination_Form_MarkEntrySearchStudent();
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentDB = new Examination_Model_DbTable_StudentOmrAnswer();
	    	$students = $studentDB->getStudentList($IdMarksDistributionMaster,$IdMarksDistributionDetails,$formData);
	    	
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentOmrAnswer();
	    	$students = $studentDB->getStudentList($IdMarksDistributionMaster,$IdMarksDistributionDetails);
	    		
    	} //end if
    	
    	$this->view->student_list = $students;
    }
    
    
	public function studentAnswerAction(){
    	
		// disable layouts for this action:
        $this->_helper->layout->disableLayout();
        
    	$soa_id = $this->_getParam('soa_id');
    	$IdMarksDistributionMaster= $this->_getParam('IdMarksDistributionMaster');
    	
    	
    	
    	$studentAnswerDB = new Examination_Model_DbTable_StudentOmrAnswerDetail();
    	$students = $studentAnswerDB->getData($soa_id,$IdMarksDistributionMaster);   
    	
    	$this->view->student_answer = $students;
       
    }
    
    
}

?>