<?php
class examination_GpastudentsController extends Base_Base { //Controller for the User Module
	private $lobjGpastudentsForm;
	private $lobjChargemaster;

	public function init() { //initialization function
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjGpastudentsForm = new Examination_Form_Gpastudents();
		$this->lobjGpastudents = new Examination_Model_DbTable_Gpastudents();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$lobjStudentslist= $this->lobjGpastudents->fngetStudentslist();
		$lobjform->field5->addMultiOptions($lobjStudentslist);

		$lobjSemesterNameList = $this->lobjGpastudents->fnGetSemesterNameList();
		$lobjform->field8->addMultiOptions($lobjSemesterNameList);

	 if(!$this->_getParam('search'))
	 	unset($this->gobjsessionsis->Gpastudentspaginatorresult);
		$larrresult = $this->lobjGpastudents->fnGetGpaStudentsList(); //get user details

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Gpastudentspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Gpastudentspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjGpastudents ->fnSearchGpastudentsDetails( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Gpastudentspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/gpastudents/index');
		}
	}

	public function newgpastudentsAction() { //Action for creating the new user
		$this->view->lobjGpastudentsForm = $this->lobjGpastudentsForm;
		$IdStudentRegistration= ( int ) $this->_getParam ( 'id' );
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjGpastudentsForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjGpastudentsForm->IdStudentRegistration->setValue ( $IdStudentRegistration );
		$this->view->lobjGpastudentsForm->IdStudentRegistration->setAttrib ('readonly','true');
		$this->view->lobjGpastudentsForm->AcademicStatus->setAttrib ('readonly','true');

		$larrsemester = $this->lobjGpastudents ->fnGetSemester($IdStudentRegistration);

		$this->view->lobjGpastudentsForm->IdSemester->setValue ( $larrsemester['Sem']);
		$this->view->lobjGpastudentsForm->Intake->setValue ( $larrsemester['IdSemester']);
		$this->view->lobjGpastudentsForm->Intake->setAttrib ('readonly','true');

		$auth = Zend_Auth::getInstance();
		$this->view->lobjGpastudentsForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		//$larrresult = $this->lobjGpastudents ->fnSearchGpaStudentDetails($IdApplication);
		//echo "<PRE>";
		//print_r($larrresult);die();
		//$this->view->larrresult=$larrresult;
		//print_r($larrresult);die();

		//$lintIdGpastudents = ( int ) $this->_getParam ( 'IdGpaStudents' );


		//$this->view->lobjGpastudentsForm->IdGpaStudents->setValue ($lintIdGpastudents);

		$larrViewresult = $this->lobjGpastudents->fnviewGpastudents($IdStudentRegistration);
		if($larrViewresult){
			$this->view->lobjGpastudentsForm->IdGpaStudents->setValue ($larrViewresult['IdGpaStudents']);
			$lobjStudentslist= $this->lobjGpastudents->fngetStudentslist();
			$this->lobjGpastudentsForm->IdStudentRegistration->addMultiOptions($lobjStudentslist);

			$lobjSemesterNameList = $this->lobjGpastudents->fnGetSemesterNameList();
			$this->lobjGpastudentsForm->Intake->addMultiOptions($lobjSemesterNameList);

			$this->lobjGpastudentsForm->populate($larrViewresult);
		}
			


		$lobjStudentslist= $this->lobjGpastudents->fngetStudentslist();
		$this->lobjGpastudentsForm->IdStudentRegistration->addMultiOptions($lobjStudentslist);


		$lobjSemesterNameList = $this->lobjGpastudents->fnGetSemesterNameList();
		$this->lobjGpastudentsForm->Intake->addMultiOptions($lobjSemesterNameList);


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);die();

			unset ( $larrformData ['Save'] );
			if ($this->lobjGpastudentsForm->isValid ( $larrformData )) {
				//echo '<PRE>';
				//print_r($larrformData);die();
				if($larrformData ['IdGpaStudents']){
					unset($larrformData['Intake']);
					$lintiIdGpastudents = $larrformData ['IdGpaStudents'];
					$this->lobjGpastudents->fnupdateGpastudents($lintiIdGpastudents, $larrformData );
				}else{
					unset($larrformData['IdGpaStudents']);
					unset($larrformData['Intake']);
					$result = $this->lobjGpastudents->fnAddGpastudents($larrformData); //instance for adding the lobjuserForm values to DB
				}
				//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/examination/gpastudents/index');

				//$this->_redirect( $this->baseUrl . '/examination/gpastudents/newgpastudents/id/'.$larrformData['IdApplication'].'/Sem/'.$larrformData['IdSemester']);
			}
		}

	}

	public function GpastudentslistAction() { //Action for the updation and view of the  details

		$this->view->lobjGpastudentsForm = $this->lobjGpastudentsForm;
			
		$lintIdGpastudents = ( int ) $this->_getParam ( 'id' );
		$this->view->IdGpastudents = $lintIdGpastudents;


		$larrresult = $this->lobjGpastudents->fnviewGpastudents($lintIdGpastudents);

		$lobjProgramNameList = $this->lobjGpastudents->fnGetProgramNameList();
		$this->lobjGpastudentsForm->IdProgram->addMultiOptions($lobjProgramNameList);

		$lobjSemesterNameList = $this->lobjGpastudents->fnGetSemesterNameList();
		$this->lobjGpastudentsForm->IdSemester->addMultiOptions($lobjSemesterNameList);


		$this->lobjGpastudentsForm->populate($larrresult);

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjGpastudentsForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjGpastudentsForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjGpastudentsForm->isValid ( $larrformData )) {

					$lintiIdCharges = $larrformData ['IdCharges'];
					$this->lobjGpastudents->fnupdateCharges($lintiIdCharges, $larrformData );
					//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
					$this->_redirect( $this->baseUrl . '/examination/Gpastudents/index');
				}
			}
		}
		$this->view->lobjGpastudentsForm = $this->lobjGpastudentsForm;
	}


}