<?php

class examination_MarksdistributionmasterController extends Base_Base { //Controller for the User Module

	private $lobjMarksdistributionmasterForm;
	private $lobjSubjectmaster;
	private $lobjmarksentrysetup;
	private $lobjAcademicstatus;
	private $lobjmarksdistributionmaster;
	private $lobjSchemeModel;
	private $lobjprogramModel;
	private $lobjsemesterModel;
	private $lobjsubjectmasterModel;
	private $lobjmarksdetailForm;
	private $lobjmarksditrbutiondetail;
	private $lobjmarksHistoryModel;

	public function init() { //initialization function
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjMarksdistributionmasterForm = new Examination_Form_Marksdistributionmaster();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjmarksdistributionmaster = new Examination_Model_DbTable_Marksdistributionmaster();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();
		$this->lobjSchemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksdetailForm = new Examination_Form_Marksdistributiondetails();
		$this->lobjmarksditrbutiondetail = new Examination_Model_DbTable_Marksdetails();
		$this->lobjmarksHistoryModel = new Examination_Model_DbTable_Markshistory();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$facultyList = $this->lobjcollegemaster->fnGetListofCollege();
		if(!empty($facultyList)){
			$this->view->lobjform->field5->addMultiOptions($facultyList);
		}

		$schemeList = $this->lobjSchemeModel->fngetSchemes();
		if(!empty($schemeList)){
			$this->view->lobjform->field23->addMultiOptions($schemeList);
		}

		$semList = $this->lobjsemesterModel->getAllsemesterListCode();
		if(!empty($semList)){
			$this->view->lobjform->field24->addMultiOptions($semList);
		}

		$programList = $this->lobjprogramModel->fnGetProgramList();
		if(!empty($programList)){
			$this->view->lobjform->field8->addMultiOptions($programList);
		}

		$courseList = $this->lobjsubjectmasterModel->fnGetSubjectList();
		if(!empty($programList)){
			$this->view->lobjform->field20->addMultiOptions($courseList);
		}

		if (!$this->_getParam('search'))
			unset($this->gobjsessionsis->Marksentrypaginatorresult);
		$larrresult = $this->lobjmarksdistributionmaster->fnGetMarksDistributionMaster(); //get user details
		//        echo "<pre>";
		//        print_r($larrresult);
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset($this->gobjsessionsis->Marksentrypaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksentrypaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjmarksdistributionmaster->fnSearchMarksDistributionMaster($lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->Marksentrypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {

			$this->_redirect($this->baseUrl . '/examination/marksdistributionmaster');
		}
	}

	public function newmarksdistributionmasterAction() { //Action for creating the new user
		$this->view->lobjMarksdistributionmasterForm = $this->lobjMarksdistributionmasterForm;
		$lobjSubjectList = $this->lobjSubjectmaster->fnGetSubjectList();
		$auth = Zend_Auth::getInstance();
		$this->view->lobjMarksdistributionmasterForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjMarksdistributionmasterForm->UpdDate->setValue($ldtsystemDate);
		$copyProgram = $this->lobjmarksdistributionmaster->fngetProgramexistinmarkdistribution();
		$copyprogramlist = array();
		foreach($copyProgram as $list){
			if(($list['key'] == '') && ($list['value'] == '')){
				$list['key'] = 0;
				$list['value'] = 'All';
			}
			$copyprogramlist[] = $list;
		}
		if(!empty($copyprogramlist)){
			$this->view->lobjMarksdistributionmasterForm->FromProgram->addMultiOptions($copyprogramlist);
		}

		$copysemester = $this->lobjmarksdistributionmaster->fngetSemesterexistinmarkdistribution();
		$this->view->lobjMarksdistributionmasterForm->FromSemester->addMultiOptions($copysemester);

		$toprogramList = $this->lobjprogramModel->fnGetProgramList();
		$this->view->lobjMarksdistributionmasterForm->ToProgram->addMultiOptions($toprogramList);


		//$tosemList = $this->lobjsemesterModel->getAllsemesterListCode();
		$tosemList = $this->lobjsemesterModel->getListSemester();
		$this->view->lobjMarksdistributionmasterForm->ToSemester->addMultiOptions($tosemList);

		if ($this->_request->isPost() && $this->_request->getPost('Copy')) {
			$larrformData = $this->_request->getPost();
			$toprogramId =  $larrformData['ToProgram'];
			//$ret = $this->lobjprogramModel->fnViewProgramquota($toprogramId);
			$ret = $this->lobjprogramModel->getschemesofprogram($toprogramId);
			$schemelistofprogram = $this->flatten_array($ret);

			//echo '<pre>';
			//print_r($ret);
			//$selectedprogramscheme = $ret['IdScheme'];
			$facultylist = $this->lobjprogramModel->fnfetchProgramFaculty($toprogramId);

			//now check the existing marks entry according to filters from from detail
			$testarray = array('course' => $larrformData['FromCourse'],'program' => $larrformData['FromProgram'],
					'semester' => $larrformData['FromSemester'],
			);
			$ret = $this->lobjmarksdistributionmaster->fncheckexistentry($testarray);
			$data = array();
			$existingcomponentarray = array();
			$flag = true;


			foreach($ret as $list){

				if($list['IdFaculty'] == 0 && $list['IdScheme'] == 0){
					$flag = true;
				}else if($list['IdFaculty'] != $facultylist[0]['IdCollege']){
					$flag = false;
				}else if(!in_array($list['IdScheme'],$schemelistofprogram)){
					$flag = false;
				}
				//                else if($list['IdScheme'] != $selectedprogramscheme){
				//                    $flag = false;
				//                }
				if($flag){
					$list['IdProgram'] = $larrformData['ToProgram'];
					$list['IdCourse'] = $larrformData['ToCourse'];
					$list['semester'] = $larrformData['ToSemester'];
					$list['UpdUser'] = $auth->getIdentity()->iduser;
					$list['UpdDate'] = $ldtsystemDate;
					$data[] = $list;
				}else{
					$this->view->msgcopy = '1';
					return $this->render("newmarksdistributionmaster");
				}
			}

			foreach($data as $dat){
				$existingcomponentarray[] = $dat['IdComponentType'];
			}
			$componentList = $this->lobjSubjectmaster->fngetcourseComponentlist($larrformData['ToCourse']);
			//            echo "<pre>";
			//            print_r($componentList);
			$tocoursecomp = array();
			$fl = true;
			$existcomparray = $this->flatten_array($existingcomponentarray);
			// check if tocourse component not exist
			if(empty($componentList)){
				$this->view->msgcopycomponentnotexist = '1';
				return $this->render("newmarksdistributionmaster");
			}else{
				foreach($componentList as $comp){
					$tocoursecomp[] = $comp['Idcomponents'];
				}
				$reslt = array_intersect($existcomparray,$tocoursecomp);
				if(count($reslt) != count($existcomparray)){
					$this->view->msgcopycomponentnotmatch = '1';
					return $this->render("newmarksdistributionmaster");
				}
			}
			foreach($componentList as $lis){
				if(!in_array($lis['Idcomponents'],$existcomparray)){
					$fl = false;
				}
			}
			if($fl){
				$testarray = array('course' => $data[0]['IdCourse'],'faculty' => $data[0]['IdFaculty'],
						'scheme' => $data[0]['IdScheme'],'program' => $data[0]['IdProgram'],
						'semester' => $data[0]['semester'],
				);
				$ret = $this->lobjmarksdistributionmaster->fncheckduplicateentry($testarray);

				if(count($ret)>0){
					$this->view->msgscheme = '1';
					return $this->render("newmarksdistributionmaster");
				}else{
					$i = 0;
					$historyArray = array();
					foreach($data as $dat){
						$IdMarksDistributionMaster = $dat['IdMarksDistributionMaster'];
						unset($dat['IdMarksDistributionMaster']);
						$dat['Status'] = 193;
						$masterId = $this->lobjmarksdistributionmaster->fnAddMarksDisributionMaster($dat);
						if($i == 0){
							$historyArray['IdMarksDistributionMaster'] = $masterId;
							$historyArray['Activity'] = 'COPY';
							$historyArray['OldStatus'] = '';
							$historyArray['NewStatus'] = 193;
							$historyArray['UpdatedOn'] = $ldtsystemDate;
							$historyArray['UpdatedBy'] = $auth->getIdentity()->iduser;
							$ret = $this->lobjmarksHistoryModel->addMarksHistory($historyArray);
						}
						$detailmarks = $this->lobjmarksditrbutiondetail->fngetentryBymarksmasterId($IdMarksDistributionMaster);
						foreach($detailmarks as $det){
							unset($det['IdMarksDistributionDetails']);
							$det['UpdUser'] = $auth->getIdentity()->iduser;
							$det['UpdDate'] = $ldtsystemDate;
							$det['IdMarksDistributionMaster'] = $masterId;
							$this->lobjmarksditrbutiondetail->fnAddMarksDisributionDetail($det);
						}
						$i++;
					}
					$this->_redirect($this->baseUrl . '/examination/marksdistributionmaster');

				}
			}else{
				$this->view->msgcopy = '1';
				return $this->render("newmarksdistributionmaster");
			}




			//            echo "<pre>";
			//            print_r($componentList);
			//            print_r($larrformData);
			//            print_r($data);
			//            print_r($existingcomponentarray);
			//            print_r($componentList);

		}

		
		
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();


			// check if any of ahe application is approved, no more distribution can be saved for same combination.
			$checkStatus = $this->lobjmarksdistributionmaster->fncheckMaxEntry($larrformData);
		
			if($checkStatus=='0') {
				$componentList = $this->lobjSubjectmaster->fngetcourseComponentlist($larrformData['IdCourse']);
				$this->view->componentList = $componentList;
				$this->view->semester = $larrformData['semestercode'];
				$this->view->program = $larrformData['IdProgram'];
				$this->view->faculty = $larrformData['IdFaculty'];
				$this->view->scheme = $larrformData['IdScheme'];
				$this->view->lobjMarksdistributionmasterForm->populate($larrformData);
			} else {
				$this->view->semester = $larrformData['semestercode'];
				$this->view->program = $larrformData['IdProgram'];
				$this->view->faculty = $larrformData['IdFaculty'];
				$this->view->scheme = $larrformData['IdScheme'];
				$this->view->lobjMarksdistributionmasterForm->populate($larrformData);
				$this->view->errMsgMDMEntry = '1';
			}
		}
		
		
		
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post
			//asd($larrformData);
			// For duplicate test create an array
			$testarray = array('course' => $larrformData['course'],'faculty' => $larrformData['faculty'],
					'scheme' => $larrformData['scheme'],'program' => $larrformData['program'],
					'semester' => $larrformData['semester'],
			);
			//            $ret = $this->lobjmarksdistributionmaster->fncheckduplicateentry($testarray);
			//
			//            if(count($ret)>0){
			//                $this->view->msgscheme = '1';
			//                return $this->render("newmarksdistributionmaster");
			//            }
			$componentcount = $larrformData['count'];

			$data = array();
			$AppCode = array();
			$historyArray = array();


			// get the last record for AppCode
			$lastRecord = $this->lobjmarksdistributionmaster->fngetLastRecord();
			$markAppCode = explode('MD00',$lastRecord[0]['MarksApplicationCode']);
			$incrementedCode = $markAppCode[1]+1;
			$marksAppID = 'MD00'.$incrementedCode;

			for($i=0; $i < $componentcount; $i++){
				$compkey = 'componenttype_'.$i;
				$compitemkey = 'componentitem_'.$i;
				$markskey = 'marks_'.$i;
				$percentagekey = 'percentage_'.$i;
				$data['IdComponentType'] = $larrformData[$compkey];
				$data['IdComponentItem'] = $larrformData[$compitemkey];
				$data['Marks'] = $larrformData[$markskey];
				$data['Percentage'] = $larrformData[$percentagekey];
				$data['TotalMark'] = $larrformData['totalmarks'];
				//$data['IdProgram'] = $larrformData[$compkey];
				$data['IdCourse'] = $larrformData['course'];
				$data['Status'] = 193;
				$data['UpdUser'] = $larrformData['UpdUser'];
				$data['UpdDate'] = $larrformData['UpdDate'];
				$data['semester'] = $larrformData['semester'];
				$data['IdProgram'] = $larrformData['program'];
				$data['IdScheme'] = $larrformData['scheme'];
				$data['IdFaculty'] = $larrformData['faculty'];
				$masterId = $this->lobjmarksdistributionmaster->fnAddMarksDisributionMaster($data);


				$AppCode  = array('MarksApplicationCode'=>$marksAppID);
				$this->lobjmarksdistributionmaster->fnupdateMarksDisributionMaster($AppCode,$masterId);

				if($i == 0){
					$historyArray['IdMarksDistributionMaster'] = $masterId;
					$historyArray['Activity'] = 'ADD';
					$historyArray['OldStatus'] = '';
					$historyArray['NewStatus'] = 193;
					$historyArray['UpdatedOn'] = $ldtsystemDate;
					$historyArray['UpdatedBy'] = $auth->getIdentity()->iduser;
				}
			}

			$ret = $this->lobjmarksHistoryModel->addMarksHistory($historyArray);
			if($ret){
				$this->_redirect($this->baseUrl . '/examination/marksdistributionmaster');
			}

		}
	}

	public function editmarksdistributionmasterAction() { //Action for creating the new user
		$this->view->lobjMarksdistributionmasterForm = $this->lobjMarksdistributionmasterForm;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjMarksdistributionmasterForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjMarksdistributionmasterForm->UpdDate->setValue($ldtsystemDate);
		$Idcourse = (int) $this->_getParam('id');
		$IdScheme = (int) $this->_getParam('scheme');
		$IdFaculty = (int) $this->_getParam('faculty');
		$IdProgram = (int) $this->_getParam('program');
		$appcode = $this->_getParam('appcode');
		$mastermarksdistribution = $this->lobjmarksdistributionmaster->fnGetMarksDistributionMasterByCourse($Idcourse,$IdScheme,$IdFaculty,$IdProgram,$appcode);
		$this->view->allmaster = $mastermarksdistribution;
		$this->lobjMarksdistributionmasterForm->populate($mastermarksdistribution[0]);
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

			$componentcount = $larrformData['count'];
			// Now check the component count with the total of detail marks distribution count
			//            for($j=0;$j<$componentcount;$j++){
			//                $key = 'componentid_'.$j;
			//                $markskey = 'marks_'.$j;
			//                $IdMarksDistributionmaster = $larrformData[$key];
			//                $totalwightagelist = $this->lobjmarksditrbutiondetail->fngetstotalmarksofcomponent($IdMarksDistributionmaster);
			//                if($totalwightagelist['sumweightage'] != ''){
			//                    if(floatval($larrformData[$markskey]) != floatval($totalwightagelist['sumweightage'])){
			//                        $this->view->marksdifferentmessage = '1';
			//                        return $this->render("editmarksdistributionmaster");
			//                    }
			//                }
			//            }
			$data = array();
			for($i=0;$i<$componentcount;$i++){
				$compkey = 'componenttype_'.$i;
				$markskey = 'marks_'.$i;
				$compitemkey = 'componentitem_'.$i;
				$percentagekey = 'percentage_'.$i;
				$data['IdComponentType'] = $larrformData[$compkey];
				$data['IdComponentItem'] = $larrformData[$compitemkey];
				$data['Marks'] = $larrformData[$markskey];
				$data['Percentage'] = $larrformData[$percentagekey];
				$data['TotalMark'] = $larrformData['totalmarks'];
				//$data['IdProgram'] = $larrformData[$compkey];
				$data['IdCourse'] = $larrformData['course'];
				$data['Status'] = 193;
				$data['UpdUser'] = $larrformData['UpdUser'];
				$data['UpdDate'] = $larrformData['UpdDate'];
				$data['semester'] = $larrformData['semester'];
				$data['IdProgram'] = $larrformData['program'];

				// now check component is exist in database
				$return = $this->lobjmarksdistributionmaster->fncheckexistmasterentry($larrformData['program'],$larrformData['course'],$larrformData['semester'],$data['IdComponentType'],$data['IdComponentItem'],$appcode);

				if(count($return) > 0){
					$this->lobjmarksdistributionmaster->fnupdateMarksDisributionMaster($data,$return[0]['IdMarksDistributionMaster']);
					if($i == 0){
						$rethistorydata = $this->lobjmarksHistoryModel->getHistory($return[0]['IdMarksDistributionMaster']);
						$historydatalen = count($rethistorydata);
						$historyArray['IdMarksDistributionMaster'] = $return[0]['IdMarksDistributionMaster'];
						$historyArray['Activity'] = 'UPDATE';
						$historyArray['OldStatus'] = $rethistorydata[$historydatalen - 1]['NewStatus'];
						$historyArray['NewStatus'] = 193;
						$historyArray['UpdatedOn'] = $ldtsystemDate;
						$historyArray['UpdatedBy'] = $auth->getIdentity()->iduser;
						$ret = $this->lobjmarksHistoryModel->addMarksHistory($historyArray);
					}
				}else{
					$this->lobjmarksdistributionmaster->fnAddMarksDisributionMaster($data);
				}
			}
			$this->_redirect($this->baseUrl . '/examination/marksdistributionmaster');
		}
	}

	public function addmarksdistributiondetailAction(){
		$Idmarksdistributionmaster = (int) $this->_getParam('id');
		$mastermarksdistribution = $this->lobjmarksdistributionmaster->fnGetMarksDistributionMasterById($Idmarksdistributionmaster);

		$this->view->lobjmarksdetailForm = $this->lobjmarksdetailForm;

		$detailmarks = $this->lobjmarksditrbutiondetail->fnGetMarksDistributiondetailById($Idmarksdistributionmaster);

		if(!empty($detailmarks)){
			$this->view->detailmarks = $detailmarks;
		}

		if(count($mastermarksdistribution) > 0){
			$this->view->detailmaster = $mastermarksdistribution;
		}
		//        echo "<pre>";
		//        print_r($mastermarksdistribution);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjmarksdetailForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjmarksdetailForm->UpdDate->setValue($ldtsystemDate);
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			if(!empty($detailmarks)){
				$this->lobjmarksditrbutiondetail->fndeleteMarksDistributiondetailById($Idmarksdistributionmaster);
			}
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

			$len = count($larrformData['componenttypegrid']);
			$data = array();
			for($i=0;$i<$len;$i++){
				$data['IdMarksDistributionMaster'] = $Idmarksdistributionmaster;
				$data['IdComponentType'] = $larrformData['componenttypegrid'][$i];
				$data['IdComponentItem'] = $larrformData['componentitemgrid'][$i];
				$data['status'] = 193;
				$data['Weightage'] = $larrformData['markgrid'][$i];
				//$data['Percentage'] = $larrformData['percentagegrid'][$i];
				$data['TotalMark'] = $larrformData['totalmarks'];
				$data['UpdDate'] = $larrformData['UpdDate'];
				$data['UpdUser'] = $larrformData['UpdUser'];
				$this->lobjmarksditrbutiondetail->fnAddMarksDisributionDetail($data);
			}

			$url = "id/".$mastermarksdistribution['IdCourse'];
			$url = $url."/scheme/".$mastermarksdistribution['IdScheme'];
			$url = $url."/faculty/".$mastermarksdistribution['IdFaculty'];
			$url = $url."/program/".$mastermarksdistribution['IdProgram'];
			$url = $url."/appcode/".$mastermarksdistribution['MarksApplicationCode'];
			$this->_redirect($this->baseUrl . '/examination/marksdistributionmaster/editmarksdistributionmaster/'.$url);
		}

	}


	public function getschemelistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdFaculty = $this->_getParam('IdFaculty');
		$result = $this->lobjSchemeModel->getSchemeByFaculty($IdFaculty);
		if(!empty($result)){
			$temp = array('key' => '0', 'name'=> 'All');
			$result[] = $temp;
		}
		
		echo Zend_Json_Encoder::encode($result);
	}

	public function getprogramlistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdFaculty = $this->_getParam('IdFaculty');
		$IdScheme = $this->_getParam('IdScheme');
		$result = $this->lobjprogramModel->getProgramByfacultyandscheme($IdFaculty,$IdScheme);
		if(!empty($result)){
			$temp = array('key' => '0', 'name'=> 'All');
			$result[] = $temp;
		}
		echo Zend_Json_Encoder::encode($result);
	}



	public function getcourselistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$IdProgram = $this->_getParam('IdProgram');
		
		/*$result = $this->lobjmarksdistributionmaster->fnGetCourseList($IdProgram);
		$resultsub = $this->lobjmarksdistributionmaster->fnGetCourseListsub($IdProgram);
		
		$arrayresult = array_merge($result, $resultsub);
		$arrayresultsub = array_intersect_key($arrayresult, array_unique(array_map('serialize', $arrayresult)));
		$larrsubDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($arrayresultsub);*/
		
	    $landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		
		$i=0;
		$j=0;
		$allsemlandscape =array();
		$allblocklandscape = array();
		
		$activeLandscape=$landscapeDB->getAllActiveLandscape($IdProgram);
		
			foreach($activeLandscape as $actl){
				if($actl["LandscapeType"]==43){
					$allsemlandscape[$i] = $actl["IdLandscape"];						
					$i++;
				}elseif($actl["LandscapeType"]==44){
					$allblocklandscape[$j] = $actl["IdLandscape"];
					$j++;
				}
			}
		
		if(is_array($allsemlandscape)){
			$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape);
		}
			
		if(is_array($allblocklandscape)){
			$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape);
		}
		
		if(is_array($allsemlandscape) && is_array($allblocklandscape)){
			$subjects=array_merge( $subjectsem , $subjectblock );
		}else{
			if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
				$subjects=$subjectsem;
			}
			elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=$subjectblock;
			}		
		}		
		
		//print_r($subjects);
	    $i=0;
		foreach($subjects as $subject){
			$data[$i]["key"]=$subject["IdSubject"];
			$data[$i]["name"]=utf8_encode($subject["SubjectName"]);
			if($i>50)break;
		$i++;
		}
		
		echo Zend_Json_Encoder::encode($data);
	}

	public function getfromcourselistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdProgram = $this->_getParam('IdProgram');
		$result = $this->lobjmarksdistributionmaster->fnGetCourseList($IdProgram);
		$resultsub = $this->lobjmarksdistributionmaster->fnGetCourseListsub($IdProgram);
		$arrayresult = array_merge($result, $resultsub);
		$arrayresultsub = array_intersect_key($arrayresult, array_unique(array_map('serialize', $arrayresult)));
		$larrsubDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($arrayresultsub);
		//print_r($larrsubDetails);
		$existingcourselist = $this->lobjmarksdistributionmaster->fngetfrommarksetupCourse();
		//print_r($existingcourselist);
		$data = array();
		$onedArray = $this->flatten_array($existingcourselist);

		foreach($larrsubDetails as $list){
			if(in_array($list['key'],$onedArray)){
				$data[] = $list;
			}
		}
		echo Zend_Json_Encoder::encode($data);
	}

	private function flatten_array($mArray) {
		$sArray = array();

		foreach ($mArray as $row) {
			if (!(is_array($row))) {
				if ($sArray[] = $row) {

				}
			} else {
				$sArray = array_merge($sArray, self::flatten_array($row));
			}
		}
		return $sArray;
	}




}