<?php
class examination_MarkDistributionApprovalController extends Base_Base { //Controller for the User Module

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution Confirmation - Search Course");
    	
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkDistributionApprovalSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			//$this->view->idCollege = $formData["IdCollege"];
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			//$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$formData['Status']=3;
				$groups = $courseGroupDb->getCourseGroupDistList($formData);												
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}//if post	
    }
    
     public function approveAction(){
     
   		$this->view->title=$this->view->translate("Mark Distribution Confirmation");
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
		
		$auth = Zend_Auth::getInstance();
		
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			//echo '<pre>'; 
			//print_r($formData);
			//exit;
			$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			
			for($i=0; $i<count( $formData['IdCourseTaggingGroup'] ); $i++){
				
				$idGroup =  $formData['IdCourseTaggingGroup'][$i];	
				$idSubject = $formData['idSubject'][$idGroup];	
				$idSemester = $formData['idSemester'][$idGroup];	
				$idProgram = $formData['idProgram'][$idGroup];
								
				$data['Status']=$formData['status'];
				$data['Remarks']=$formData['remarks'];
				
				if($formData['status']==1) {
					$status='approved';
					$data['ApprovedBy']=$auth->getIdentity()->iduser;
					$data['ApprovedOn']=date ( 'Y-m-d H:i:s');
				}
				
				if($formData['status']==2) {
					$status='rejected';
					$data['RejectedBy']=$auth->getIdentity()->iduser;
					$data['RejectedOn']=date ( 'Y-m-d H:i:s');
				}
							
			    //$where = 'IdProgram = '.$idProgram.' AND semester='.$idSemester.' AND IdCourse='.$idSubject.' AND IdCourseTaggingGroup='.$idGroup;
			    $where = 'IdCourseTaggingGroup='.$idGroup;
			    $markDistributionDB->updateApprovalData($data,$where);
			    
			    $this->sendMail(array('idSubject'=>$idSubject,'idGroup'=>$idGroup));
			}
			
			
				
			
			$this->gobjsessionsis->flash = array('message' => 'Mark distribution has been '.$status, 'type' => 'success');	
			
			if($auth->getIdentity()->IdRole == 1){
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-approval', 'action'=>'index'),'default',true));
			}else{
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-approval', 'action'=>'index-my'),'default',true));
			}	
    	}
		
     }
    
   
    
    public function viewAction(){
    	
    	$this->_helper->layout->disableLayout();
    	
    	$this->view->title=$this->view->translate("Mark Distribution Confirmation");
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);    	
        
    	//get group info
    	$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    	$this->view->group = $groupDB->getInfo($idGroup);
    	
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();    	
    	//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
    
    	//print_r($list_component);
    	
    	$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	
    }
    
    
	public function indexCeAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution Confirmation - Search Course");
    	
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkDistCeSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();	
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    $this->view->idSemester = $formData['IdSemester'];	
				$formData['Status']='Entry';
				
				//get subject list status ENTRY Only
				$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();		
				$subjects = $markDistributionDB->searchMarkDistSubjectCe($formData);

				$this->view->subjects = $subjects;
								
			}//if form valid
    	}//if post	
    }
    
    
 	public function approveCeAction(){
     
   		
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
		
		$auth = Zend_Auth::getInstance();
		
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			//echo '<pre>'; 
			//print_r($formData);
			//exit;
			$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			
			for($i=0; $i<count( $formData['IdSubject'] ); $i++){

				$idSubject = $formData['IdSubject'][$i];	
				$idSemester = $formData['idSemester'];	
								
				$data['Status']=$formData['status'];
				$data['Remarks']=$formData['remarks'];
				
				if($formData['status']==1) {
					$status='approved';
					$data['ApprovedBy']=$auth->getIdentity()->iduser;
					$data['ApprovedOn']=date ( 'Y-m-d H:i:s');
				}
				
				if($formData['status']==2) {
					$status='rejected';
					$data['RejectedBy']=$auth->getIdentity()->iduser;
					$data['RejectedOn']=date ( 'Y-m-d H:i:s');
				}
							
			   $where = 'IdProgram = 0 AND semester='.$idSemester.' AND IdCourse='.$idSubject.' AND IdCourseTaggingGroup = 0 AND Type=1'; //CE
			   $markDistributionDB->updateApprovalData($data,$where);
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Mark distribution has been '.$status, 'type' => 'success');	
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-approval', 'action'=>'index-ce'),'default',true));		
    	}
		
     }
     
 	public function viewCeAction(){
    	
    	$this->_helper->layout->disableLayout();
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idSubject = $idSubject;

    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);    	
        
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();    	
    		//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,null,$idSubject,null,1);	    
    
    	//print_r($list_component);
    	
    	$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    
    	
    	
    }
    
    
	public function indexMyAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution Confirmation - Search Course");
    	
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MyGroupSearch(array('locale'=>$locale));
    	$this->view->form = $form;

   		$semesterDB = new Registration_Model_DbTable_Semester();
    	$semester = $semesterDB->getAllCurrentSemester();		    	    	
		
    	$cur_sem = array();
    	foreach($semester as $sem){
    		array_push($cur_sem,$sem['IdSemesterMaster']);
    	}
    	
    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idSemester = $formData["IdSemester"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group		
			    $formData['Status']=3;		
				$groups = $courseGroupDb->getGroupDistByCoordinator($formData);												
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}else{
    		
    		//display current semester
    		$formData['IdSemesterArray'] = $cur_sem;
    		$groups = $courseGroupDb->getGroupDistByCoordinator($formData);												
			$this->view->groups = $groups;
    		
    	}//if post	
    }
    
    
    public function sendMail($formData){
    	
    		//Send mail
			//coordinator profile
    		$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    		$lecturer = $groupDB->getCoordinatorProfile($formData["idSubject"]);    				
    		
    		//get group info			
    		$group = $groupDB->getInfo($formData["idGroup"]);
    		
    		//lecturer profile  
    		$userDb = new GeneralSetup_Model_DbTable_User();			  				
    		$recepient = $userDb->getstaffdetails($group['IdLecturer']);
    		
    		
    		$data['semester'] = $group['semester_name'];
    		$data['subject_code'] = $group['subject_code'];
    		$data['subject_name'] = $group['subject_name'];
    		$data['group'] = $group['GroupCode'].' - '.$group['GroupName'];
    		
    		$cms = new Cms_SendMail();
	 		$cms->ExamSendMail(119,array('FullName'=>$lecturer['FullName']),$recepient,$data);	
    }
}