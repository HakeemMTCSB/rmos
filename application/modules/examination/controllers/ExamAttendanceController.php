<?php 

class Examination_ExamAttendanceController extends Base_Base {

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Attendance");
		
		$session = Zend_Registry::get('sis');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;

		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {    	
			unset($session->search_attendance);
			unset($session->search_attendance_result);
    	}
    	
		$form = new Examination_Form_ExamAttendanceSearch(array ('locale' => $locale));
		$this->view->form = $form;
		
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
		$examRegDB = new Examination_Model_DbTable_ExamRegistration();
						
			if ($this->_request->isPost() && $this->_request->getPost('Search') ){
			
				if($form->isValid($_POST)) {
					
					$formData = $this->_request->getPost ();
					//var_dump($formData); ///exit;
					$examRegDb = new Registration_Model_DbTable_ExamRegistration();
					
					$session->search_attendance = $formData;
					
					$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];	
					$this->view->idSubject = $formData['IdSubject'];	
					$this->view->ec_id = $formData['ec_id'];					
				
					
					$subject_list = $this->getSubjectList($formData);
					//var_dump($subject_list); exit;
					if($subject_list){
						
						$subject_arr = array();
						
						if($formData['IdSubject']==''){
							foreach($subject_list as $subject){							
								array_push($subject_arr,$subject['IdSubject']);
							}
						}else{
								array_push($subject_arr,$formData['IdSubject']);
						}

						$ec_list = $examCenterDB->getExamCenterList($formData,$subject_arr, 'ajax2');
						
						foreach($ec_list as $index=>$center){
							
							//$student = $examRegDB->getStudentRegister($center['ec_id'],$center['er_idSemester'],$center['er_idSubject']);
							//get list student register exam center							
							$student_list = $examRegDb->getStudentListAttendance($center['ec_id'],$center['er_idSemester'],$center['er_idSubject']);

							if ($student_list){
								foreach ($student_list as $key => $student_loop){
									if ($student_loop['exam_status']=='EX'){
										unset($student_list[$key]);
									}
								}
							}
							
							$total_updated = 0;
							if(count($student_list)>0){
								$total_student = count($student_list);
								
								foreach($student_list as $student){
									if($student['er_attendance_status']!=''){
										$total_updated++;
									}
								}
							}else{
								$total_student= 0;
							}
							$ec_list[$index]['total_student'] = $total_student;
							$ec_list[$index]['total_updated'] = $total_updated;
						}
						$this->view->ec_list = $ec_list;
						
						$session->search_attendance_result = $ec_list;
						
					}
				}
				
			}else{
				
				//populate by session 
		    	if (isset($session->search_attendance)) {  
		    		  	
		    		$form->populate($session->search_attendance);
		    		$formData = $session->search_attendance;			
		    		
		    		$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];	
					$this->view->idSubject = $formData['IdSubject'];	
					$this->view->ec_id = $formData['ec_id'];					
				
					$this->view->ec_list = $session->search_attendance_result;
							
		    	}
			}
	}
	
	
	public function listStudentAction() {
		
		$this->view->title = $this->view->translate("Exam Attendance Details");

		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$ecid = $this->_getParam('ecid', 0);
		
		$this->view->idSemester= $idSemester;
		$this->view->cur_ecid = $ecid;
		$this->view->idSubject = $idSubject;
		
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);

    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    
    	//exam center
    	$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
    	$this->view->ec = $examCenterDB->getDatabyId($ecid);    	
		$this->view->country = $examCenterDB->getExamCenterCountry();
    	
		//get list student register exam center
		$examRegDb = new Registration_Model_DbTable_ExamRegistration();
		$student_list = $examRegDb->getStudentListAttendance($ecid,$idSemester,$idSubject);

		if ($student_list){
			foreach ($student_list as $key => $student_loop){
				if ($student_loop['exam_status']=='EX'){
					unset($student_list[$key]);
				}
			}
		}

		$this->view->student_list = $student_list;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		$this->view->attendanceStatusList = $attendanceStatusList;
		
		//exam status
		$examStatusList = $lkpTbl->getDataByType(157);
		$this->view->examStatusList = $examStatusList;
		
		$studentRegSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			
       
		if ($this->getRequest()->isPost()) {
				
			$auth = Zend_Auth::getInstance();
		
			$formData = $this->getRequest()->getPost();
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
			
			for($i=0; $i<sizeof($formData['er_id']); $i++){
				
				//update attendance
				if($formData['status'][$i]!=0){	
					
					$IdStudentRegSubjects = $formData['IdStudentRegSubjects'][$i];
					$exam_status = $formData['exam_status'][$IdStudentRegSubjects];
					
						
					$attendance = array('er_attendance_status'=>$formData['status'][$i],
										'mc_start_date'=>(isset($formData['mc_start_date'][$i]) && $formData['mc_start_date'][$i]!='') ? date('Y-m-d',strtotime($formData['mc_start_date'][$i])):null,
										'mc_end_date'=>(isset($formData['mc_end_date'][$i]) && $formData['mc_end_date'][$i]!='') ? date('Y-m-d',strtotime($formData['mc_end_date'][$i])):null,
										'mc_reason'=>$formData['mc_reason'][$i],
										'er_attendance_by'=>$auth->getIdentity()->iduser,
										'er_attendance_dt'=>date('Y-m-d H:i:s'));			
					$examRegDb->updateData($attendance,$formData['er_id'][$i]);

					//cancel CN exam fee if absent with valid reason
					/*if($formData['status'][$i]==396){
						//get subject reg detail
						$item = $studentRegSubjectDB->getItemDetail($IdStudentRegSubjects,879); //879=>exam

						if(isset($item) && $item['invoice_id']!=''){
							$invoiceClass->generateCreditNote($item['student_id'],$idSubject,$item['invoice_id'],$item['item_id'],100);
						}
					}*/
					
					//update exam status					
					if(isset($exam_status) && $exam_status!=''){
						//echo 'update exam status';
						$data = array('exam_status'=>$exam_status,'exam_status_updateby'=>$auth->getIdentity()->iduser,'exam_status_updatedt'=>date('Y-m-d H:i:s'));
					    $studentRegSubjectDB->updateData($data,$IdStudentRegSubjects);
					}
				}								
				
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-attendance', 'action'=>'list-student','idSemester'=>$formData['idSemester'],'idSubject'=>$formData['idSubject'],'ecid'=>$formData['ec_id']),'default',true));
						
		}
		
		//echo '<pre>';
		//print_r($student_list);
	}
	
	
	public function getExamCenterAction()
	{	
    	$IdSemester = $this->_getParam('IdSemester', 0);
    	$IdProgram = $this->_getParam('IdProgram', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
        $result = $examCenterDB->getExamCenterList(array('IdSemester'=>$IdSemester,'IdProgram'=>$IdProgram),'ajax');
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
    
    public function getSubjectAction(){
    	
    	$IdSemester = $this->_getParam('IdSemester', 0);
    	$IdProgram = $this->_getParam('IdProgram', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $result = $this->getSubjectList(array('IdSemester'=>$IdSemester,'IdProgram'=>$IdProgram));
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
		
    }
    
    function getSubjectList($formData){
    	
    	
    		 $semesterDB = new Registration_Model_DbTable_Semester();
			 $landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			 $landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			 $landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			 $sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();

			 $semester = $semesterDB->getData($formData['IdSemester']);
			 
			 $progDB= new GeneralSetup_Model_DbTable_Program();
             $programs = $progDB->getProgramByScheme($semester['IdScheme'], $formData['IdProgram']);

				$i=0;
				$j=0;
				$allblocklandscape=null;
				$allsemlandscape=null;
			
				if (count($programs))
				{
					foreach ($programs as $key => $program)
					{
						$activeLandscape=$landscapeDB->getAllLandscape($program["IdProgram"]);
						
						foreach($activeLandscape as $actl)
						{
							if($actl["LandscapeType"]!=44)
							{
								$allsemlandscape[$i] = $actl["IdLandscape"];						
								$i++;
							}
							elseif($actl["LandscapeType"]==44)
							{
								$allblocklandscape[$j] = $actl["IdLandscape"];
								$j++;
							}
						}
					}
				}
				else 
				{
					$subjects = FALSE;
				}

				if(is_array($allsemlandscape))
				{
					$subjectsem=$sofferedDB->getMultiLandscapeCourseOffer($allsemlandscape, $formData,$formData['IdSemester']);
				}
	
				if(is_array($allblocklandscape))
				{
					$subjectblock=$sofferedDB->getMultiBlockLandscapeCourseOffer($allblocklandscape, $formData,$formData['IdSemester']);
				}
	
				if(is_array($allsemlandscape) && is_array($allblocklandscape))
				{
					$subjects=array_merge( $subjectsem , $subjectblock );
				}
				else
				{
					if(is_array($allsemlandscape) && !is_array($allblocklandscape))
					{
						$subjects=$subjectsem;
					}
					elseif(!is_array($allsemlandscape) && is_array($allblocklandscape))
					{
						$subjects=$subjectblock;
					}
					else
					{
						$subjects=FALSE;
					}
				}

			return $subjects;
    }
	
}

?>