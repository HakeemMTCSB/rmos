<?php
class examination_GeneratedeanlistController extends Base_Base { //Controller for the User Module


	private $lobjprogram;
	private $lobjsemester;
	private $_gobjlog;
	private $lobjdeftype;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjgendeanlist = new Examination_Model_DbTable_Generatedeanlist();
		$this->lobjdeftype = new App_Model_Definitiontype();
	}


	/**
	 * Function to search students
	 * @author: VT
	 */
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		$this->view->UpdUser = $auth->getIdentity()->iduser;
		$this->view->UpdDate = date('Y-m-d H:i:s');

		// SHOW THE Condition
		$larrSAS = $this->lobjdeftype->fnGetDefinationMs('Condition');
		foreach($larrSAS as $larrsemvalues) {
			$this->view->lobjform->field23->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}

		// SHOW THE SEMESTER
		//$larrsemresult =  $this->lobjStudentRegModel->fetchAllSemMaster();
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->deanlistpaginatorresult);

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->deanlistpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->deanlistpaginatorresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )!='' ) {
			$larrformData = $this->_request->getPost ();
			$this->lobjgendeanlist->fnprocessDean($larrformData);
			$larrresultFinal =  $this->lobjgendeanlist->fngetDeanList($larrformData);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresultFinal,$lintpage,$lintpagecount);
			$this->gobjsessionsis->deanlistpaginatorresult = $larrresultFinal;
		}



		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			unset($this->gobjsessionsis->deanlistpaginatorresult);
			$this->_redirect( $this->baseUrl . '/examination/generatedeanlist');
		}
	}



	public function exportdeanlistAction(){
		$semCode = 'Semester';
		$result  = $this->gobjsessionsis->deanlistpaginatorresult;
		if(count($result)>0) {
			$semestName = $result[0]['IdSemester'];
			if($semestName!='') {
				$semCode = $semestName;
			}
			$this->view->larrresult = $result;
			$html =  $this->render('semesterdeanlistsexcel');

			$listName = $semCode.'_Dean_List.xls';
			$this->getResponse()->setRawHeader( "Content-Type: application/vnd.ms-excel; charset=UTF-8" )
			->setRawHeader( "Content-Disposition: attachment; filename=".$listName )
			->setRawHeader( "Content-Transfer-Encoding: binary" )
			->setRawHeader( "Expires: 0" )
			->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
			->setRawHeader( "Pragma: public" )
			->sendResponse();
			echo $html;
		}
		exit();

	}




}
