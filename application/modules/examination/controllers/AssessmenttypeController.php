<?php

class examination_AssessmenttypeController extends Base_Base { //Controller for the User Module

	private $lobjdeftype;
	private $lobjassessmentForm;
	private $lobjassessmenttypeModel;
	private $lobjAssessmentItemModel;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjassessmentForm = new Examination_Form_Assessment();
		$this->lobjassessmenttypeModel = new Examination_Model_DbTable_Assessmenttype();
		$this->lobjAssessmentItemModel = new Examination_Model_DbTable_Assessmentitem();
	}

	
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate('Assessment Type Setup');
		
		$session = Zend_Registry::get('sis');
		
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
				
		$searchForm = new Examination_Form_SearchAssessmentType();
		$this->view->form = $searchForm;
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
    		
			unset($session->result);
    	}
    	
    	
		if ($this->getRequest()->isPost() && $this->_request->getPost('save')) {
	
			$formData = $this->_request->getPost();
			
			$session->result = $formData;
			
			if ($searchForm->isValid($formData)) {
		
				$this->view->assessmenttypeList = $this->lobjassessmenttypeModel->searchAssessmentType($formData);
		
			}			
		}else{
			
			//populate by session 
	    	if (isset($session->result)) {    	
	    		$searchForm->populate($session->result);
	    		$formData = $session->result;			
	    		$this->view->assessmenttypeList = $this->lobjassessmenttypeModel->searchAssessmentType($formData);
	    	}
		}
	}
	
	
	function addAction(){
		
		$this->view->title = $this->view->translate('Add Assessment Type');
			
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		
		$this->view->lobjassessmentForm = $this->lobjassessmentForm;
	
		$auth = Zend_Auth::getInstance();
		
		if ($this->getRequest()->isPost()) {
	
			$formData = $this->_request->getPost();
			
			if ($this->lobjassessmentForm->isValid($formData)) {
				
					//get total added data -> for sorting/ordering purposes
					//$total = $this->lobjassessmenttypeModel->getTotalAssessmentType($formData['IdSemester'],$formData['Type']);
				
					$temp['sorting'] = $formData['Sorting'];
					$temp['IdDescription'] = $formData['IdDescription'];
					$temp['Type'] = $formData['Type'];
					$temp['FinalExam'] = $formData['Exam'];
					$temp['IdSemester'] = $formData['IdSemester'];
					$temp['Description'] = $formData['Description'];
					$temp['DescriptionDefaultlang'] = $formData['DescriptionDefaultlang'];
					$temp['UpdDate'] = date('Y-m-d H:i:s');
					$temp['UpdUser'] = $auth->getIdentity()->iduser;
					$this->lobjassessmenttypeModel->fnaddAssessmenttype($temp);
					
					$this->gobjsessionsis->flash = array('message' => 'Information has been saved', 'type' => 'success');
					$this->_redirect( $this->baseUrl . '/examination/assessmenttype');
			}
		}
	}
	
	function deleteAction(){
	
		 $id = $this->_getParam('id');
		
		 $this->lobjassessmenttypeModel->deleteData($id);
		 $this->gobjsessionsis->flash = array('message' => 'Information has been deleted', 'type' => 'success');
		 $this->_redirect( $this->baseUrl . '/examination/assessmenttype');
	}

}