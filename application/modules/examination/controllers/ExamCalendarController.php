<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/9/2015
 * Time: 4:06 PM
 */
class Examination_ExamCalendarController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Examination_Model_DbTable_ExamCalendar();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate("Exam Calendar");

        $form = new Examination_Form_ExamCalendarSearch();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])){
                $list = $this->model->getExamCalendarList($formData);
                $form->populate($formData);
            }else{
                $list = $this->model->getExamCalendarList();
            }
        }else{
            $list = $this->model->getExamCalendarList();
        }

        $this->view->form = $form;
        $this->view->list = $list;
    }

    public function addAction(){
        $this->view->title = $this->view->translate("Add Exam Calendar");

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $form = new Examination_Form_ExamCalendarAdd();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData);
            //exit;

            $check = $this->model->checkDuplicateEntry($formData['ec_idSemester'], $formData['ec_idActivity']);

            if ($check){
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('error' => 'Duplicate Data'));
                $this->_redirect($this->baseUrl . '/examination/exam-calendar/add/');
            }

            $data = array(
                'ec_idActivity'=>$formData['ec_idActivity'],
                'ec_idSemester'=>$formData['ec_idSemester'],
                'ec_start_date'=>date('Y-m-d', strtotime($formData['ec_start_date'])),
                'ec_end_date'=>date('Y-m-d', strtotime($formData['ec_end_date'])),
                'ec_createdt'=>date('Y-m-d H:i:s'),
                'ec_createdby'=>$userId
            );
            $id = $this->model->insertCalendarActivity($data);

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Data Saved'));
            $this->_redirect($this->baseUrl . '/examination/exam-calendar/edit/id/'.$id);
        }
    }

    public function editAction(){
        $this->view->title = $this->view->translate("Edit Exam Calendar");
        $id = $this->_getParam('id', 0);

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $info = $this->model->getExamCalendarInfo($id);

        $form = new Examination_Form_ExamCalendarAdd();
        $form->populate($info);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData);
            //exit;

            $data = array(
                //'ec_idActivity'=>$formData['ec_idActivity'],
                //'ec_idSemester'=>$formData['ec_idSemester'],
                'ec_start_date'=>date('Y-m-d', strtotime($formData['ec_start_date'])),
                'ec_end_date'=>date('Y-m-d', strtotime($formData['ec_end_date'])),
                'ec_createdt'=>date('Y-m-d H:i:s'),
                'ec_createdby'=>$userId
            );
            $this->model->updateCalendarActivity($data, $id);

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Data Saved'));
            $this->_redirect($this->baseUrl . '/examination/exam-calendar/edit/id/'.$id);
        }
    }

    public function deleteAction(){
        $id = $this->_getParam('id', 0);

        $this->model->deleteCalendarActivity($id);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Data Deleted'));
        $this->_redirect($this->baseUrl . '/examination/exam-calendar/index/');
    }
}