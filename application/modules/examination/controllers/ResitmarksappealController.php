<?php

class examination_ResitmarksappealController extends Base_Base { //Controller for the User Module

	private $lobjsemesterModel;
	private $lobjAcademicstatus;
	private $lobjSubjectmaster;
	private $lobjstatus;
	private $lobjresitmarksappealForm;
	private $lobjresitmarksentryModel;
	private $lobjresitmarksentryapplicationModel;
	private $lobjmarksentryModel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjstatus = new Examination_Model_DbTable_Remarkingconfig();
		$this->lobjresitmarksappealForm = new Examination_Form_Resitmarksappeal();
		$this->lobjresitmarksentryModel = new Examination_Model_DbTable_Resitmarksentry();
		$this->lobjresitmarksentryapplicationModel = new Examination_Model_DbTable_Resitmarksentryapplication();
		$this->lobjmarksentryModel = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
	}

	public function indexAction() {
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform;
		// Mode
		$modelist = array('MANUAL' => 'MANUAL', 'ONLINE' => 'ONLINE');
		$this->view->lobjform->field20->addmultioptions($modelist);

		// semester
		$semesterlist = $this->lobjsemesterModel->getAllsemesterListCode();
		$this->view->lobjform->field8->addmultioptions($semesterlist);

		//program
		$lobjProgramNameList = $this->lobjAcademicstatus->fnGetProgramNameList();
		$this->view->lobjform->field5->addMultiOptions($lobjProgramNameList);

		//course
		$larrresultCourses = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field27->addMultiOptions($larrresultCourses);

		$larrstatusresult = $this->lobjstatus->showstatus();
		foreach ($larrstatusresult as $larrstatusvalues) {
			$this->view->lobjform->field10->addMultiOption($larrstatusvalues['key'], strtoupper($larrstatusvalues['value']));
		}

		if (!$this->_getParam('search'))
			unset($this->gobjsessionsis->Marksentrypaginatorresult);
		$larrresult = $this->lobjresitmarksentryapplicationModel->fnAllapplication(); //get user details
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset($this->gobjsessionsis->Marksentrypaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksentrypaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjresitmarksentryapplicationModel->fnSearchapplication($lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->Marksentrypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {

			$this->_redirect($this->baseUrl . '/examination/resitmarksappeal');
		}
	}

	public function addresitmarksappealAction() {
		$this->view->lobjresitmarksappealForm = $this->lobjresitmarksappealForm;
		$larrformData = $this->_request->getPost();
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjresitmarksappealForm->UpdatedBy->setValue($auth->getIdentity()->iduser);
		$this->view->lobjresitmarksappealForm->ProcessedBy->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjresitmarksappealForm->UpdatedOn->setValue($ldtsystemDate);
		$this->view->lobjresitmarksappealForm->ProcessedOn->setValue($ldtsystemDate);

		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost();

			if ($larrformData['formsave'] == 'searchdata') {
				$this->view->studentid = $larrformData['StudentCode'];
				$this->view->errMsgPS = '0';
				$getProfileStatus = $this->lobjStudentRegModel->fnStudentProfileStatus($larrformData['StudentCode']);
				if($getProfileStatus=='92' || $getProfileStatus=='248' || $getProfileStatus=='253' ) { // active, defer, dormant
				$this->view->lobjresitmarksappealForm->StudentCode->setValue($larrformData['StudentCode']);
				$programdet = $this->lobjresitmarksentryModel->getstudentprogram($larrformData['StudentCode']);

					if (!empty($programdet)) {
						$this->view->lobjresitmarksappealForm->IdProgram->setValue($programdet['IdProgram']);
						$this->view->programdet = $programdet['ProgramName'];
						$semlist = $this->lobjsemesterModel->getsemesterofstudent($programdet['IdStudentRegistration']);

						$this->view->lobjresitmarksappealForm->IdStudent->setValue($programdet['IdStudentRegistration']);
						// get course list of student
						$courselist = $this->lobjresitmarksentryModel->getstudentcourses($programdet['IdStudentRegistration']);
						$this->view->lobjresitmarksappealForm->IdCourse->addmultioptions($courselist);
						$this->view->lobjresitmarksappealForm->SemesterCode->addmultioptions($semlist);
					}
				} else {
					$this->view->errMsgPS = '1';
				}
			}

			if (isset($larrformData['Save'])) {
				$programdet = $this->lobjresitmarksentryModel->getstudentprogram($larrformData['StudentCode']);
				$len = count($larrformData['coursegrid']);
				$data = array();
				$marksdata = array();
				$retarray = $this->lobjresitmarksentryapplicationModel->fnAllapplication();
				if (empty($retarray)) {
					$data['ExaminationResitApplicationCode'] = 'APP1';
				} else {

					$data['ExaminationResitApplicationCode'] = 'APP' . intval(count($retarray) + 1);
				}

				//$data['ExaminationResitApplicationCode'] = 'APP001';
				$data['IdStudent'] = $programdet['IdStudentRegistration'];
				$data['StudentCode'] = $larrformData['StudentCode'];
				$data['SemesterCode'] = $larrformData['SemesterCode'];
				$data['IdProgram'] = $larrformData['IdProgram'];
				$data['ProcessedBy'] = $larrformData['ProcessedBy'];
				$data['ProcessedOn'] = $larrformData['ProcessedOn'];
				$data['Status'] = 192;
				$data['mode'] = 'MANUAL';
				$data['UpdatedOn'] = $larrformData['UpdatedOn'];
				$data['UpdatedBy'] = $larrformData['UpdatedBy'];
				$data['IdStudent'] = $larrformData['IdStudent'];


				//Check Config Files
				$getMASemester = $this->lobjresitmarksentryapplicationModel->getMaxAppealResitperSemester($IdUniversity);
				if (count($getMASemester) == 0) {
					$maxAppeal = '2';
				} else {
					$maxAppeal = $getMASemester[0]['ApplicationCount'];
				}

				$larrconfig = $this->lobjresitmarksentryapplicationModel->totalAppealResitMax($larrformData['IdStudent'], $larrformData['SemesterCode'], $maxAppeal);


				if ($larrconfig == '0') {
					$retId = $this->lobjresitmarksentryapplicationModel->addresitmarksentryapplication($data);

					for ($i = 0; $i < $len; $i++) {
						$marksdata['IdExaminationResitApplication'] = $retId;
						$marksdata['IdCourse'] = $larrformData['coursegrid'][$i];
						$marksdata['IdComponent'] = $larrformData['componentgrid'][$i];
						$marks = $this->lobjmarksentryModel->getstudentcoursecomponent($programdet['IdStudentRegistration'], $larrformData['coursegrid'][$i], $larrformData['componentgrid'][$i]);

						if (count($marks) > 0) {
							$oldmarks = 0;
							$totalmarks = 0;
							foreach ($marks as $values) {

								//$idmarksentry = $values['IdStudentMarksEntry'];
								//$totalmarksarray = $this->lobjmarksentryModel->getdetailcomponentMarks($idmarksentry, $larrformData['componentgrid'][$i]);

								//if(count($totalmarksarray)>0) {
									//$oldmarks = 0;
									//$totalmarks = 0;
									//foreach ($totalmarksarray as $det) {
									//	$oldmarks = $oldmarks + $det['MarksObtained'];
									//	$totalmarks = $totalmarks + $det['TotalMarks'];
									//}

									$oldmarks = $values['TotalMarkObtained'];
									$totalmarks = $values['MarksTotal'];
									$marksdata['IdComponentDetail'] = '0';
									$marksdata['ComponentItem'] = $values['ComponentItem'];
									$marksdata['OldMarks'] = $oldmarks;
									$marksdata['NewMarks'] = 0;
									$marksdata['TotalMarks'] = $totalmarks;
									$marksdata['Status'] = 192;
									$marksdata['EntryRemark'] = '';
									$marksdata['ProcessingRemark'] = '';
									$marksdata['IdExaminer1'] = 0;
									$this->lobjresitmarksentryapplicationModel->addresitmarksentryapplicationmarks($marksdata);
								//}
							}
						}
					}
					$this->_redirect($this->baseUrl . '/examination/resitmarksappeal');
				} else {
					$this->view->errMsg = '1';
				}
			}
		}
		//B-Acc0001_This is just a test
	}

	public function getcoursecomponentAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idcourse = $this->_getParam('Idcourse');
		$result = $this->lobjresitmarksentryModel->getfinalcomponets($Idcourse);
		echo Zend_Json_Encoder::encode($result);
	}

	public function editresitmarksappealAction() {
		$this->view->lobjresitmarksappealForm = $this->lobjresitmarksappealForm;
		$appId = (int) $this->_getParam('id');
		$ret = $this->lobjresitmarksentryapplicationModel->getapplicationdate($appId);
		$this->view->status = $ret[0]['Status'];
		$this->view->lobjresitmarksappealForm->Studentcode->setValue($ret[0]['StudentCode']);

		$courselist = $this->lobjresitmarksentryModel->getstudentcourses($ret[0]['IdStudent']);
		$this->view->lobjresitmarksappealForm->IdCourse->addmultioptions($courselist);

		$this->view->appdata = $ret;
		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost();
			//asd($larrformData);
			$unarr = array_unique($larrformData['coursegrid']);
			$len = count($unarr);

			$this->lobjresitmarksentryapplicationModel->deleteresitmarksentryapplicationmarks($appId);
			for ($i = 0; $i < $len; $i++) {
				$marksdata['IdExaminationResitApplication'] = $appId;
				$marksdata['IdCourse'] = $larrformData['coursegrid'][$i];
				$marksdata['IdComponent'] = $larrformData['componentgrid'][$i];
				$marks = $this->lobjmarksentryModel->getstudentcoursecomponent($ret[0]['IdStudent'], $larrformData['coursegrid'][$i], $larrformData['componentgrid'][$i]);


				if (count($marks) > 0) {
					$oldmarks = 0;
					$totalmarks = 0;
					foreach ($marks as $values) {

						$idmarksentry = $values['IdStudentMarksEntry'];
						$totalmarksarray = $this->lobjmarksentryModel->getdetailcomponentMarks($idmarksentry, $larrformData['componentgrid'][$i]);
						//$oldmarks = 0;
						//$totalmarks = 0;
						//foreach ($totalmarksarray as $det) {
						//	$oldmarks = $oldmarks + $det['MarksObtained'];
						//	$totalmarks = $totalmarks + $det['TotalMarks'];
						//}
						$oldmarks = $values['TotalMarkObtained'];
						$totalmarks = $values['MarksTotal'];
						$marksdata['IdComponentDetail'] = '0';
						$marksdata['ComponentItem'] = $values['ComponentItem'];
						$marksdata['OldMarks'] = $oldmarks;
						$marksdata['NewMarks'] = 0;
						$marksdata['TotalMarks'] = $totalmarks;
						$marksdata['Status'] = 192;
						$marksdata['EntryRemark'] = '';
						$marksdata['ProcessingRemark'] = '';
						$marksdata['IdExaminer1'] = 0;
						$this->lobjresitmarksentryapplicationModel->addresitmarksentryapplicationmarks($marksdata);
					}
				}



				//                if (count($marks) > 0) {
				//                    $idmarksentry = $marks[0]['IdStudentMarksEntry'];
				//                    $totalmarksarray = $this->lobjmarksentryModel->getdetailcomponentMarks($idmarksentry, $larrformData['componentgrid'][$i]);
				//                    $oldmarks = 0;
				//                    $totalmarks = 0;
				//                    foreach ($totalmarksarray as $det) {
				//                        $oldmarks = $oldmarks + $det['MarksObtained'];
				//                        $totalmarks = $det['TotalMarks'];
				//                    }
				//                    $marksdata['IdComponentDetail'] = 0;
				//                    $marksdata['OldMarks'] = $oldmarks;
				//                    $marksdata['NewMarks'] = 0;
				//                    $marksdata['TotalMarks'] = $totalmarks;
				//                    $marksdata['Status'] = 192;
				//                    $marksdata['EntryRemark'] = '';
				//                    $marksdata['ProcessingRemark'] = '';
				//                    $marksdata['IdExaminer1'] = 0;
				//                    $this->lobjresitmarksentryapplicationModel->addresitmarksentryapplicationmarks($marksdata);
				//                }
			}
			$this->_redirect($this->baseUrl . '/examination/resitmarksappeal');
		}
	}


	public function getmarkentrydetAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idcourse = $this->_getParam('IdCourse');
		$IdComponent = $this->_getParam('IdComponent');
		$studentcode = $this->_getParam('studentcode');
		$programdet = $this->lobjresitmarksentryModel->getstudentprogram($studentcode);
		if(!empty($programdet)){
			$marks = $this->lobjmarksentryModel->getstudentcoursecomponent($programdet['IdStudentRegistration'], $Idcourse, $IdComponent);
			if(count($marks) > 0 ){
				echo '1';
			}else{
				echo 0;
			}
		}else{
			echo '2';
		}
	}

}

?>
