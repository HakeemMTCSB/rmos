<?php

class examination_MarkApprovalController extends Base_Base { //Controller for the User Module

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Mark Approval");
		 		
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkApprovalSearchCourseGroup(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	$auth = Zend_Auth::getInstance();
    	
    	
    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
    	//1st : get info adakah dia dean?
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			//$this->view->idCollege = $formData["IdCollege"];
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			//$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				
				
				if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){
					$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
				}else{									
					$groups = $courseGroupDb->getDeanCourseGroup($formData);
				}
				
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();				
				foreach($groups as $i=>$group){
						
				
					$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"], false);
					$total_approved = $courseGroupStudent->getTotalStudentMarkApproved	( $group["IdCourseTaggingGroup"], false);
					$total_pending = $courseGroupStudent->getTotalStudentMarkSubmitApproval	( $group["IdCourseTaggingGroup"], false);
					
					$groups[$i]["total_student"] = $total_student;
					$groups[$i]["total_mark_approved"] = $total_approved;
					$groups[$i]["total_pending"] = $total_pending;
					
				}
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
				
				
								
			}//if form valid
    	}else{
    		
    			if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){
    				
    				$semesterDB = new Registration_Model_DbTable_Semester();
			    	$semester = $semesterDB->getAllCurrentSemester();		    	    	
					
			    	$cur_sem = array();
			    	foreach($semester as $sem){
			    		array_push($cur_sem,$sem['IdSemesterMaster']);
			    	}
    	
			    	$formData['IdSemesterArray'] = $cur_sem;
					$groups = $courseGroupDb->getDeanCourseGroup($formData);
					
					$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();				
					foreach($groups as $i=>$group){
							
					
						$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"], false);
						$total_approved = $courseGroupStudent->getTotalStudentMarkApproved	( $group["IdCourseTaggingGroup"], false);
						$total_pending = $courseGroupStudent->getTotalStudentMarkSubmitApproval	( $group["IdCourseTaggingGroup"], false);
						
						$groups[$i]["total_student"] = $total_student;
						$groups[$i]["total_mark_approved"] = $total_approved;
						$groups[$i]["total_pending"] = $total_pending;
						
					}
					$this->view->groups = $groups;
				}
    		
    	}//if post	
    	
		
	}
	
	
	
	
 	public function componentListAction(){
    	
 		$auth = Zend_Auth::getInstance();
    	$this->view->role = $auth->getIdentity()->IdRole;
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
 		
    	$this->view->title=$this->view->translate("Mark Verification");
    	
    	if($this->_getParam('msg')==1){
    		$this->view->noticeSuccess = $this->view->translate("Mark has been verified");
    	}
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('id');
    	$idScheme = $this->_getParam('idScheme');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
    	$this->view->idProgramScheme = $idScheme;
		    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
 	//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);			
		$this->view->group = $group;
		
		//get scheme
		$groupProgramDB = new GeneralSetup_Model_DbTable_CourseGroupProgram();
		$this->view->scheme = $groupProgramDB->getListScheme($idGroup,$idProgram);
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->idProgramScheme = $formData['IdProgramScheme'];
			
			//get component
	    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
			$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$formData['IdProgramScheme']);	    
			
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	    	foreach($list_component as $index=>$component){
	    		
			  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			  	  $list_component[$index]['component_item'] = $component_item;
	    	}
	    	$this->view->rs_component = $list_component;
	    	
	    	//print_r($list_component);
			
		}
    	
    	
    }
    
	public function studentListAction(){
    	
    	$this->view->title=$this->view->translate("Mark Approval");
    
    	$auth = Zend_Auth::getInstance();
    	$this->view->role = $auth->getIdentity()->IdRole;    	
    	
    	$idGroup = $this->_getParam('id');
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');    	
    	    	   	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
    	    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get info group
    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$this->view->group = $courseGroupDb->getInfo($idGroup);
			    	
    		
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
		
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	//get list student yg register  semester,program,subject di atas
    	//keluarkan aje dulu list student filter by group or lecturer later
    	
        $form = new Examination_Form_MarkEntrySearchStudent(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject));
     	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			$this->view->formData = $formData;
			
			$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getApprovalStudentListbyGroup($idSemester,$idProgram,$idSubject,$idGroup,$formData, false);
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getApprovalStudentListbyGroup($idSemester,$idProgram,$idSubject,$idGroup, null, false);
	    	
    	} 
    	
    	
    	$db = Zend_Db_Table::getDefaultAdapter();	
    	
    	foreach($students as $key=>$student){
    		
    		if($student['exam_status']=='U' && $student['audit_program']==''){
    			unset($students[$key]);
    		}else{
	    		foreach($list_component as $c=>$component){
		    			
		    			$select_main_mark = $db->select()
						 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
						 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
						 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
						 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
					 	$entry_main = $db->fetchAll($select_main_mark);	
					 	
					 	foreach($entry_main as $m=>$main){
					 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
					 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
					 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
					 		
					 		//get item
				 			$select_item_mark = $db->select()
					 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
					 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
					 		$entry_item = $db->fetchAll($select_item_mark);	
					 		
					 		foreach($entry_item as $i=>$item){
					 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
					 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
					 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
					 		}
					 	}
		    		}
    		}	
    		
    	}
    	
		array_values($students);
    	//echo '<pre>';
    	//print_r($students);
		$this->view->students = $students;
		
    }
    
	
	
	
 	public function saveApprovalAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	
    	
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			  	    			    		
    		for($i=0; $i<count($formData['IdStudentMarksEntry']); $i++){

    			$IdStudentMarksEntry =  $formData['IdStudentMarksEntry'][$i];
    			    			
    			$data["MarksEntryStatus"]=411; //Approved
    			$data["ApprovedBy"]=$formData["ApprovedBy"];
	    		$data["ApprovedOn"]=date('Y-m-d H:i:s');  
    			
    			$studentMarkDb = new Examination_Model_DbTable_StudentMarkEntry();
    			//$studentMarkDb->updateData($data,$IdStudentMarksEntry);
    		}
    		   		
    	}
    		
    	//redirect
    	$this->gobjsessionsis->flash = array('message' => 'Mark has been verified', 'type' => 'success');
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-approval', 'action'=>'student-list','id'=>$formData["idGroup"],'idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idScheme'=>$formData['idScheme']),'default',true));
		  
    }
    
    
	public function searchCourseAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);

			
		if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Subject
			*/
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
			//Subject list base on Program Landscape from the selected faculty
			//$programs = $progDB->fngetProgramDetails($faculty_id);
				
				
			$allsemlandscape = null;
			$allblocklandscape = null;
			$i=0;
			$j=0;
			//foreach ($programs as $key => $program){
				$activeLandscape=$landscapeDB->getAllActiveLandscape($program_id);
				foreach($activeLandscape as $actl){
					if($actl["LandscapeType"]==43){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}elseif($actl["LandscapeType"]==44){
						$allblocklandscape[$j] = $actl["IdLandscape"];
						$j++;
					}
				}
			//}
				
				
			$subjectsem = null;
			$subjectblock = null;
				
			if(is_array($allsemlandscape))
				$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,null,$semester_id);
			if(is_array($allblocklandscape))
				$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,null,$semester_id);
				
			$subjects = null;
			if(is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=array_merge( $subjectsem , $subjectblock );
			}else{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=$subjectblock;
				}
			}
			//end subject list
			
			$i=0;
			foreach($subjects as $subject){
					
				//get total student register this subject
				$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
				$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$semester_id);
				$subject["total_student"] = $total_student;
					
				//get total group creates
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$semester_id);
				$subject["total_group"] = $total_group;
				$subject["IdSemester"] = $semester_id;				
					
				$subjects[$i]=$subject;
					
				$i++;
			}
				
			foreach ($subjects as $index => $subject){
				if($subject["total_student"]>0 && $subject["total_group"]>0){
						
				}else{
					unset($subjects[$index]);
				}
			}
			/*
			 * End search subject
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($subjects);
	
			echo $json;
			exit();
		}
	}
	
	function searchCourseGroupAction(){
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			/*
			 * Search Group
			*/		
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getMarkApprovalGroupList($subject_id,$semester_id);
			
			
			$i=0;
			foreach($groups as $group){
					
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
					
				$group["total_student"] = $total_student;
				$groups[$i]=$group;
					
				$i++;
			}
			/*
			 * End search group
			*/
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($groups);
		
			echo $json;
			exit();
		
		}
	}
	
	public function groupListAction(){
		
		$this->view->title = "Mark Verification - Course Group List";	
		
    	$programDb = new Registration_Model_DbTable_Program();
    	$this->view->program = $programDb->getData();
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->getData();    	

    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			$idProgram = $formData["idProgram"];
			$this->view->idProgram = $idProgram;
			
			$idSemester = $formData["idSemester"];
			$this->view->idSemester = $idSemester;
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getMarkApprovalGroupList('',$idSemester);			
			
			foreach($groups as $i=>$group){				
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getStudentbyGroup($group["IdCourseTaggingGroup"]);				
				$groups[$i]["total_student"]=count($total_student);				
			}		
			$this->view->list_groups = $groups;
			
			/*echo '<pre>';
	    	print_r($groups);
	    	echo '</pre>';*/
    	}
		
    	
	}
	
	
  
	   
	   
	   public function approvedListAction(){
    	
    	$this->view->title=$this->view->translate("Mark Entry Status : Approved : Student List");
    
    	$auth = Zend_Auth::getInstance();
    	$this->view->role = $auth->getIdentity()->IdRole;    	
    	
    	$this->view->tf = $this->_getParam('tf');
    	$idGroup = $this->_getParam('id');
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');    	
    	    	   	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
    	    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get info group
    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$this->view->group = $courseGroupDb->getInfo($idGroup);
			    	
    		
			
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
		
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	//get list student yg register  semester,program,subject di atas
    	//keluarkan aje dulu list student filter by group or lecturer later
    	
        $form = new Examination_Form_MarkEntrySearchStudent(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject));
     	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudentWithApprovedMark($idSemester,$idProgram,$idSubject,$idGroup,$formData);
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudentWithApprovedMark($idSemester,$idProgram,$idSubject,$idGroup);
	    	
    	} 
    	
    	
    	$db = Zend_Db_Table::getDefaultAdapter();	
    	
    	foreach($students as $key=>$student){
    		
    		foreach($list_component as $c=>$component){
    			
    			$select_main_mark = $db->select()
				 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
				 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
				 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
				 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
			 	$entry_main = $db->fetchAll($select_main_mark);	
			 	
			 	foreach($entry_main as $m=>$main){
			 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
			 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
			 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
			 		
			 		//get item
		 			$select_item_mark = $db->select()
			 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
			 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
			 		$entry_item = $db->fetchAll($select_item_mark);	
			 		
			 		foreach($entry_item as $i=>$item){
			 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
			 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
			 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
			 		}
			 	}
    		}
    		
    	}
    	

    	//echo '<pre>';
    	//print_r($students);
		$this->view->students = $students;
    }
    
    
	public function studentCeAction()
    {
    	$this->view->title=$this->view->translate("Mark Entry Approval - CE");
    	
    	$auth = Zend_Auth::getInstance();
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
		
		$deanDb = new GeneralSetup_Model_DbTable_Chiefofprogram();
		
    	//1st : get info adakah dia dean?    	
    	if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){
			//admin
		}else{				
							
			$isDean = $deanDb->isGSDean();
			$this->view->isDean = $isDean;
		}

		
		
		if(isset($isDean) && $isDean==true){
			
			$form = new Examination_Form_MarkEntryCeSearch(array('locale'=>$locale));
	    	$this->view->form = $form;
	    
	    	$studentSubjectDB = new Examination_Model_DbTable_StudentRegistration();
	    	
	    	if ($this->getRequest()->isPost()) {
				
				$formData = $this->getRequest()->getPost();			
			
				$this->view->idSemester = $formData["IdSemester"];
					
				if ($form->isValid($formData)) {
					
					$form->populate($formData);
					
					//get subject where mark dist is assigned to the lecturer
					$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
				    $subjects = $subjectDB->getListSubjectCe();
				   
				    
				    foreach($subjects as $index=>$subject){
				    	//get total student by semester
				    	$student = $studentSubjectDB->getCeStudentBySemester($formData["IdSemester"],$subject['IdSubject'],2);
				    	$subjects[$index]['total']=count($student);
				    }
					 $this->view->subjects = $subjects;
									
				}//if form valid
	    	}//if post
	    	
		}else{
			
		}
		
		
    		
    	
    }
    
    public function viewCeAction()
    {
    	$this->view->title=$this->view->translate("Mark Entry Approval - CE");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
    	$deanDb = new GeneralSetup_Model_DbTable_Chiefofprogram();
		
    	//1st : get info adakah dia dean?    	
    	$isDean = false;
    	if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){
			//admin
		}else{			
			$isDean = $deanDb->isGSDean();			
		}
		
		$idSemester = $this->_getParam('idSemester');    	  	
    	$idSubject = $this->_getParam('idSubject');    	
    	
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject; 
		
    	$this->view->form = new Examination_Form_MarkApprovalSearchForm(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject));
    	
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);    	
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
	
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
    	
    	if(isset($isDean) && $isDean==true){
    		$list_component = $markDistributionDB->getDeanApprovalListMainComponent($idSemester,$idSubject);
    	}else{
			$list_component = $markDistributionDB->getMyListMainComponent($idSemester,$idSubject);
    	}	    
		
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getMyListComponentItem($component["IdMarksDistributionMaster"],$isDean);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();		
    	}else{
    		$formData = array();
    	}
    	
    	//get student list
    	$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
    	$students = $studentRegDB->searchStudentCeEntryMark($idSemester,$idSubject,1,$formData);
    	
    	
    	$db = Zend_Db_Table::getDefaultAdapter();	
    	
		foreach($students as $key=>$student){
    		
    		foreach($list_component as $c=>$component){
    			
    			$select_main_mark = $db->select()
				 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
				 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
				 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
				 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
			 	$entry_main = $db->fetchAll($select_main_mark);	
			 	
			 	foreach($entry_main as $m=>$main){
			 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
			 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
			 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
			 		
			 		//get item
		 			$select_item_mark = $db->select()
			 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
			 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
			 		$entry_item = $db->fetchAll($select_item_mark);	
			 		
			 		foreach($entry_item as $i=>$item){
			 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
			 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
			 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
			 		}
			 	}
    		}
    		
    	}
    	
    	$this->view->students = $students;
    }
    
    
    public function changeStatusAction(){
    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;
	    	
	    	if ($this->getRequest()->isPost()) {
			
	    		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();	    		
	    		$markEntryDB = new Examination_Model_DbTable_StudentMarkEntry();
	    		$markEntryHistoryDB = new Examination_Model_DbTable_StudentMarkEntryHistory();
	    		$markEntryDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
	    		$grade_calculation = new Cms_ExamCalculation();
	    			    		
	    		$formData = $this->getRequest()->getPost();
	    		$student_arr = array();
	    		
	    		for($i=0; $i<count($formData['IdRegSub']); $i++){
	    			
	    			$IdStudentRegSubjects = $formData['IdRegSub'][$i];
	    			
	    			//get student name & ID
	    			$student = $subjectRegDB->getStudentData($IdStudentRegSubjects);

	    			if($formData['mark_approval_status']==2 && trim($student['exam_status'])!='U'){
	    				$data['exam_status']='C';
	    			}
	    			$data['mark_approval_status']=$formData['mark_approval_status'];
	    			$data['approval_remarks']=$formData['mark_approval_remarks'];
	    			$data["mark_approveby"]=$auth->getIdentity()->iduser;
	    			$data["mark_approvedt"]=date('Y-m-d H:i:s');  
	    			$subjectRegDB->updateData($data,$IdStudentRegSubjects);
	    			
	    			//ini mcam audit trail la nak keep track changes 
    			    $status['IdStudentRegSubjects'] = $IdStudentRegSubjects;
    			    $status['cs_status'] = $formData['mark_approval_status'];
    			    $status['cs_remarks'] = $formData['mark_approval_remarks'];
    			    $status["cs_createdby"] = $auth->getIdentity()->iduser;
    			    $status["cs_createddt"] = date('Y-m-d H:i:s');
    			    $markEntryHistoryDB->addChangeStatus($status);
    			    
		    		if($formData['mark_approval_status']==3){
	    				
	    				//keep history for any changes made through mark entry
	    				$info_list = $markEntryDB->getDataByStudentRegSubjectId($IdStudentRegSubjects);
	    				
	    				//email purpose
	    				$student_arr[$i]=$student;
	    				
	    				foreach($info_list as $info){
	    					
		    				$info['createddt']=date('Y-m-d H:i:s');  
		    				$info['createdby']=$auth->getIdentity()->iduser;  
		    				$info['message']='Keep history upon mark entry rejected';
		    				$markEntryHistoryDB->addData($info);
		    				
		    				$item_list = $markEntryDetailsDB->getListComponentMark($info['IdStudentMarksEntry']);
		    				foreach($item_list as $item){
		    					$markEntryHistoryDB->addItemData($item);
		    				}	
		    			}
	    			}
	    			
	    			
	    			
	    			/*---------------------------------------------------------------------------------------
	    			 * generate gpa/cgpa as per request by user based on sign off session - added on 24-6-2015
	    			 ---------------------------------------------------------------------------------------*/
	    			if($formData['mark_approval_status']==2){	    				
	    				$grade_calculation->generateGrade($student['IdStudentRegistration']);
	    			}
	    			/* ===========
	    			 * END
	    			   ===========*/
	    		}
	    		
	    		
	    		$this->sendMail(array('idSubject'=>$formData['idSubject'],'idGroup'=>$formData['idGroup'],'status'=>$formData['mark_approval_status']),$student_arr);
	    			    		
	    		$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-approval', 'action'=>'approved-list','id'=>$formData['idGroup'],'idProgram'=>$formData['idProgram'],'idSemester'=>$formData['idSemester'],'idSubject'=>$formData['idSubject']),'default',true));
		   
	    	}
	    	
	    	exit;
	   }
	   
	   
	public function changeStatusThesisAction(){
    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;
	    	
	    	if ($this->getRequest()->isPost()) {
			
	    		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();	    		
	    		$markEntryDB = new Examination_Model_DbTable_StudentMarkEntry();
	    		$markEntryHistoryDB = new Examination_Model_DbTable_StudentMarkEntryHistory();
	    		$markEntryDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
	    		$subRegSubDB = new Examination_Model_DbTable_StudentRegistrationSubject();
	    		$grade_calculation = new Cms_ExamCalculation();
	    			    		
	    		$formData = $this->getRequest()->getPost();	
	    		$student_arr = array();
	    		
	    		for($i=0; $i<count($formData['IdRegSub']); $i++){
	    			
	    			$IdStudentRegSubjects = $formData['IdRegSub'][$i];
	    			
	    			//get student name & ID
	    			$student = $subjectRegDB->getStudentData($IdStudentRegSubjects);

	    			//foreach student get info subject
	    			$regsub = $subRegSubDB->getData($IdStudentRegSubjects);
	    			
	    			if($formData['mark_approval_status']==2){
	    					    				
		    			if($regsub['CourseType']==3){
		    				//Thesis
		    				//cek if student have complete total ch
		    				$result = $subRegSubDB->gettotalch($student['IdStudentRegistration'],$regsub['IdSubject']);
							//var_dump($result);
							//var_dump($regsub); exit;
		    				if($result['total_credit_hour_registered'] >= $regsub['CreditHours'] && $regsub['exam_status']!='IP'){
		    					$data['exam_status']='P';
		    				}else{
		    					$data['exam_status']='IP';
		    				}
		    				
		    			}else if($regsub['CourseType']==2 || $regsub['CourseType']==9){
		    					$data['exam_status']='C';
		    			}
	    			}
	    			
	    			$data['mark_approval_status']=$formData['mark_approval_status'];
	    			$data['approval_remarks']=$formData['mark_approval_remarks'];
	    			$data["mark_approveby"]=$auth->getIdentity()->iduser;
	    			$data["mark_approvedt"]=date('Y-m-d H:i:s');  	    			
	    			$subjectRegDB->updateData($data,$IdStudentRegSubjects);
	    			
	    			//ini mcam audit trail la nak keep track changes 
    			    $status['IdStudentRegSubjects'] = $IdStudentRegSubjects;
    			    $status['cs_status'] = $formData['mark_approval_status'];
    			    $status['cs_remarks'] = $formData['mark_approval_remarks'];
    			    $status["cs_createdby"] = $auth->getIdentity()->iduser;
    			    $status["cs_createddt"] = date('Y-m-d H:i:s');
    			    $markEntryHistoryDB->addChangeStatus($status);
    			    
		    		if($formData['mark_approval_status']==3){
	    				
	    				//keep history for any changes made through mark entry
	    				$info_list = $markEntryDB->getDataByStudentRegSubjectId($IdStudentRegSubjects);

	    				//email purpose
	    				$student_arr[$i]=$student;
	    				
	    				foreach($info_list as $info){
	    					
		    				$info['createddt']=date('Y-m-d H:i:s');  
		    				$info['createdby']=$auth->getIdentity()->iduser;  
		    				$info['message']='Keep history upon mark entry rejected';
		    				$markEntryHistoryDB->addData($info);
		    				
		    				$item_list = $markEntryDetailsDB->getListComponentMark($info['IdStudentMarksEntry']);
		    				foreach($item_list as $item){
		    					$markEntryHistoryDB->addItemData($item);
		    				}	
		    			}
	    			}
	    			
	    			
	    			/*---------------------------------------------------------------------------------------
	    			 * generate gpa/cgpa as per request by user based on sign off session - added on 24-6-2015
	    			 ---------------------------------------------------------------------------------------*/
	    			if($formData['mark_approval_status']==2){	    				
	    				$grade_calculation->generateGrade($student['IdStudentRegistration']);
	    			}
	    			/* ===========
	    			 * END
	    			   ===========*/
	    			
	    		}
	    		
	    		
	    		//$this->sendMail(array('idSubject'=>$formData['idSubject'],'idGroup'=>$formData['idGroup'],'status'=>$formData['mark_approval_status']),$student_arr);
	    			    		
	    		$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-approval', 'action'=>'search-thesis'),'default',true));
		   
	    	}
	    	
	    	exit;
	   }
	   
	   
 	public function sendMail($formData,$student_arr=null){
    	
			//dean profile
    		$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    		
    		//get group info			
    		$group = $groupDB->getInfo($formData["idGroup"]);
    		    		
    		//dean profile
    		$dean = $groupDB->getDeanProfile($formData['idGroup']);    
    		
    		//lecturer profile  
    		$userDb = new GeneralSetup_Model_DbTable_User();			  				
    		$recepient = $userDb->getstaffdetails($group['IdLecturer']);
    		
    		
    		$data['semester'] = $group['semester_name'];
    		$data['subject_code'] = $group['subject_code'];
    		$data['subject_name'] = $group['subject_name'];
    		$data['group'] = $group['GroupCode'].' - '.$group['GroupName'];
    		
    		if(isset($student_arr)){
    			$list = '<table cellpadding="5" cellspacing=0 border=1>';
    			$list .= '<tr><th>Student ID</th><th>Name</th></tr>';
    			
    			foreach($student_arr as $student){
    				$list .= '<tr>';
    				$list .= '<td>'.$student['registrationId'].'</td><td>'.$student['student_name'].'</td>';
    				$list .= '</tr>';
    			}
    			$list .= '</table>';
    		}
    		
    		$data['revise_student']=$list;
    		
    		$cms = new Cms_SendMail();
    		
    		if($formData['status']==3){
    			//reject
    			$cms->ExamSendMail(122,array('FullName'=>$dean['FullName']),$recepient,$data);
    		}else{
    			//approve
	 			$cms->ExamSendMail(121,array('FullName'=>$dean['FullName']),$recepient,$data);
    		}	
    }
	   
		
	//FOR CE   
	public function changeApprovalStatusAction(){
    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;
	    	
	    	if ($this->getRequest()->isPost()) {
			
	    		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
	    		$markEntryDB = new Examination_Model_DbTable_StudentMarkEntry();
	    		$markEntryHistoryDB = new Examination_Model_DbTable_StudentMarkEntryHistory();
	    		$markEntryDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
	    		
	    		$formData = $this->getRequest()->getPost();		
	    		
	    		for($i=0; $i<count($formData['IdRegSub']); $i++){
	    			
	    			$IdStudentRegSubjects = $formData['IdRegSub'][$i];
	    			
	    			$data['mark_approval_status']=$formData['mark_approval_status'];
	    			$data['approval_remarks']=$formData['remarks'];
	    			$data["mark_approveby"]=$auth->getIdentity()->iduser;
	    			$data["mark_approvedt"]=date('Y-m-d H:i:s');  
	    			$subjectRegDB->updateData($data,$IdStudentRegSubjects);
	    			
	    			
    				//ini mcam audit trail la nak keep track changes 
    			    $status['IdStudentRegSubjects'] = $IdStudentRegSubjects;
    			    $status['cs_status'] = $formData['mark_approval_status'];
    			    $status['cs_remarks'] = $formData['remarks'];
    			    $status["cs_createdby"] = $auth->getIdentity()->iduser;
    			    $status["cs_createddt"] = date('Y-m-d H:i:s');
    			    $markEntryHistoryDB->addChangeStatus($status);
	    			
	    			if($formData['mark_approval_status']==3){	    			    
	    				
	    				//keep history for any changes made through mark entry
	    				$info_list = $markEntryDB->getDataByStudentRegSubjectId($IdStudentRegSubjects);
	    				
	    				foreach($info_list as $info){
	    						    					
		    				$info['createddt']=date('Y-m-d H:i:s');  
		    				$info['createdby']=$auth->getIdentity()->iduser;  
		    				$info['message']='Keep history upon mark entry rejected';
		    				$markEntryHistoryDB->addData($info);
		    				
		    				$item_list = $markEntryDetailsDB->getListComponentMark($info['IdStudentMarksEntry']);
		    				foreach($item_list as $item){
		    					$markEntryHistoryDB->addItemData($item);
		    				}	
		    			}
	    			}
	    		}
	    		
	    		$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-approval', 'action'=>'view-ce','idSemester'=>$formData['idSemester'],'idSubject'=>$formData['idSubject']),'default',true));
		   
	    	}
	    	
	    	exit;
	   }
	   
	   
	   public function searchThesisAction(){
	   		
		   	//title
			$this->view->title= $this->view->translate("Mark Approval - Thesis");
			 		
	    	$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale  = $locale;
		    	
	    	$form = new Examination_Form_SearchStudentResearch(array('locale'=>$locale));
	    	$this->view->form = $form;
	    
	    	$auth = Zend_Auth::getInstance();
    	
	    	
	    	if ($this->getRequest()->isPost()) {
				
				$formData = $this->getRequest()->getPost();			
							
				if ($form->isValid($formData)) {
					
					$form->populate($formData);
																
					//display student dia					
			    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
					$student = $studentDB->getStudentRegThesis($formData);
					$this->view->student_list = $student;
					
					
				}
				
				
	    	}		
    
	   }
}