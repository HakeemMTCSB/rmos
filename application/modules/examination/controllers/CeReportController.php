<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/3/2016
 * Time: 11:03 AM
 */
class Examination_CeReportController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Examination_Model_DbTable_CeReport();
    }

    public function indexAction(){
        $form = new Examination_Form_CeReport();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form->populate($formData);

            $component = $this->model->getMyListMainComponent($formData);
            $this->view->component = $component;

            $students = $this->model->searchStudentCeEntryMark($formData);

            if ($students) {
                foreach ($students as $key => $student) {
                    $markDB = new Examination_Model_DbTable_StudentMarkEntry();
                    $component_mark_entry = $markDB->getStudentMarkAndPassMark($student['IdStudentRegSubjects']);

                    $pass = true;

                    if ($component_mark_entry){
                        foreach ($component_mark_entry as $component_mark_entryLoop){

                            if(round($component_mark_entryLoop['FinalTotalMarkObtained']) < $component_mark_entryLoop['ce_pass_mark']){
                                $gred = 'Fail';
                                $pass = false;
                            }else{
                                $gred = 'Pass';
                            }

                            $students[$key][$component_mark_entryLoop['Component']] = $gred;
                        }

                        $students[$key]['overall'] = $pass == false ? 'Fail':'Pass';
                    }
                }
            }

            $this->view->students = $students;
        }
    }

    public function exportAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $component = $this->model->getMyListMainComponent($formData);
            $this->view->component = $component;

            $students = $this->model->searchStudentCeEntryMark($formData);

            if ($students) {
                foreach ($students as $key => $student) {
                    $markDB = new Examination_Model_DbTable_StudentMarkEntry();
                    $component_mark_entry = $markDB->getStudentMarkAndPassMark($student['IdStudentRegSubjects']);

                    $pass = true;

                    if ($component_mark_entry){
                        foreach ($component_mark_entry as $component_mark_entryLoop){

                            if(round($component_mark_entryLoop['FinalTotalMarkObtained']) < $component_mark_entryLoop['ce_pass_mark']){
                                $gred = 'Fail';
                                $pass = false;
                            }else{
                                $gred = 'Pass';
                            }

                            $students[$key][$component_mark_entryLoop['Component']] = $gred;
                        }

                        $students[$key]['overall'] = $pass == false ? 'Fail':'Pass';
                    }
                }
            }

            $this->view->students = $students;
        }

        $this->view->filename = date('Ymd').'_ce_report.xls';
    }
}