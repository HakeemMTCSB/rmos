<?php

class examination_MarkEntryStatusController extends Base_Base { //Controller for the User Module

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Change Status");
		 
		$session =  new Zend_Session_Namespace('sis');
		 
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
		$studentDB = new Examination_Model_DbTable_StudentRegistration();
		
    	$form = new Examination_Form_ChangeStatusSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    	
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$session->result = $formData;
				
				$this->view->idSemester = $formData['IdSemester'];		    	
		    	$this->view->idSubject = $formData['IdSubject'];		    
    					
	    		$students = $studentDB->searchStudentChangeStatus($formData);
	    		$this->view->students = $students; 
			}
			
    	}else{
    		
    		//populate by session 
	    	if (isset($session->result)) {    	
	    		$form->populate($session->result);
	    		$formData = $session->result;			
	    		$students = $studentDB->searchStudentChangeStatus($formData);
	    		$this->view->students = $students; 
	    	}
    	}
    	   	
    	
		
	}
	
	
	
  	public function changeStatusAction(){
    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;
	    	
	    	if ($this->getRequest()->isPost()) {
			
	    		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();	  
	    		$markEntryDB = new Examination_Model_DbTable_StudentMarkEntry(); 		
	    		$markEntryHistoryDB = new Examination_Model_DbTable_StudentMarkEntryHistory();
	    		$markEntryDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
	    		
	    		$formData = $this->getRequest()->getPost();		
	    		//print_r($formData);
	    		
	    		for($i=0; $i<count($formData['IdRegSub']); $i++){
	    			
	    			$IdStudentRegSubjects = $formData['IdRegSub'][$i];
	    			
	    			$data['mark_approval_status']=0; //ENtru
	    			$data["mark_approveby"]=$auth->getIdentity()->iduser;
	    			$data["mark_approvedt"]=date('Y-m-d H:i:s');  
	    			$subjectRegDB->updateData($data,$IdStudentRegSubjects);
	    			
	    			//ini mcam audit trail la nak keep track changes 
    			    $status['IdStudentRegSubjects'] = $IdStudentRegSubjects;
    			    $status['cs_status'] = 0;
    			    $status['cs_remarks'] = 'Change Status Approved to Entry';
    			    $status["cs_createdby"] = $auth->getIdentity()->iduser;
    			    $status["cs_createddt"] = date('Y-m-d H:i:s');
    			    $markEntryHistoryDB->addChangeStatus($status);
    			    
    				//keep history for any changes made through mark entry
    				$info_list = $markEntryDB->getDataByStudentRegSubjectId($IdStudentRegSubjects);
    				
    				foreach($info_list as $info){
    					
	    				$info['createddt']=date('Y-m-d H:i:s');  
	    				$info['createdby']=$auth->getIdentity()->iduser;  
	    				$info['message']='Keep history upon change status from approved to entry';
	    				$markEntryHistoryDB->addData($info);
	    				
	    				$item_list = $markEntryDetailsDB->getListComponentMark($info['IdStudentMarksEntry']);
	    				foreach($item_list as $item){
	    					$markEntryHistoryDB->addItemData($item);
	    				}	
	    			}	    			
	    		}
	    		
	    		$this->gobjsessionsis->flash = array('message' => 'Student mark status has been changed', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry-status', 'action'=>'index'),'default',true));
		   
	    	}
	    	
	    	exit;
	}
	   		
}