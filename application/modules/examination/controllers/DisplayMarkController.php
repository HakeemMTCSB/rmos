<?php

class examination_DisplayMarkController extends Base_Base { //Controller for the User Module

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Publish Assessment");
		 	
		$session = Zend_Registry::get('sis');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		
    	$form = new Examination_Form_SectionSearchForm(array('locale'=>$locale));
    	$this->view->form = $form;
    
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
    	if ($this->getRequest()->isPost()  && $this->_request->getPost('Search') ) {
			
    		unset($session->result);
    		
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idCollege = $formData["IdCollege"];
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group				
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);												
				$this->view->groups = $groups;
				$session->result = $formData;
				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}else{ 
    		
    		//populate by session 
	    	if (isset($session->result)) {    	
	    		$form->populate($session->result);			
	    		$groups = $courseGroupDb->getCourseGroupListByProgram($session->result);												
				$this->view->groups = $groups;		
	    	}
    		
    	}
	}
	
	public function componentListAction(){
    	
 		$auth = Zend_Auth::getInstance();
    	$this->view->role = $auth->getIdentity()->IdRole;
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
 		
    	$this->view->title=$this->view->translate("Publish Assessment");
    	
    	$publishDb = new Examination_Model_DbTable_PublishMark();
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
 		//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);			
		$this->view->group = $group;
		
    	    	
		//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);

		
		
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){ 
    		  if($component['IdComponentType']==18){
    		  	 unset($list_component[$index]);
    		  }else{
    		  
			  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			  	  
			  	  foreach($component_item as $key=>$item){
			  	  	
			  	  	$publish = $publishDb->getData($idProgram,$idSemester,$idSubject,$idGroup,$component["IdMarksDistributionMaster"],$item['IdMarksDistributionDetails'],1);
			  	  	//get publish mark
			  	  	$component_item[$key]['pm_id']=$publish['pm_id'];
			  	  	$component_item[$key]['pm_time']=$publish['pm_time'];
			  	  	$component_item[$key]['pm_date']=$publish['pm_date'];
			  	  }
		  	 	  $list_component[$index]['component_item'] = $component_item;
    		  }
    	}
    	$this->view->rs_component = $list_component;
    	
    	//echo '<pre>';
		//print_r($list_component);
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
			
			
			$data['pm_idProgram'] = $formData['idProgram'];
			$data['pm_idSemester'] = $formData['idSemester'];
			$data['pm_idSubject'] = $formData['idSubject'];
			$data['pm_idGroup'] = $formData['idGroup'];
			$data['pm_type'] = 1;
				
			for($i=0; $i<count( $formData['IdMarksDistributionMaster'] ); $i++){
				
				
				$IdMarksDistributionMaster = $formData['IdMarksDistributionMaster'][$i];
				$data['IdMarksDistributionMaster'] = $IdMarksDistributionMaster;
								
				for($m=0; $m<count($formData['IdMarksDistributionDetails'][$IdMarksDistributionMaster]); $m++){
					
						$IdMarksDistributionDetails = $formData['IdMarksDistributionDetails'][$IdMarksDistributionMaster][$m];
						$pm_id = $formData['pm_id'][$IdMarksDistributionMaster][$m];						
						$data['IdMarksDistributionDetails'] = $IdMarksDistributionDetails;
						
						if(isset($formData['publish_date_item'][$IdMarksDistributionDetails]) && $formData['publish_date_item'][$IdMarksDistributionDetails]!=''){
							$data['pm_date'] = date('Y-m-d',strtotime($formData['publish_date_item'][$IdMarksDistributionDetails]));
						}else{
							$data['pm_date']=null;
						}
						$data['pm_time'] = $formData['publish_time_item'][$IdMarksDistributionDetails];
										
						
						if(isset($pm_id) && $pm_id!=''){
							//echo 'updData';
							$publishDb->updateData($data,$pm_id);
						}else{
							//echo 'addData';
							$pmid = $publishDb->addData($data);
						}
				}
				
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');	
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'display-mark', 'action'=>'component-list','idGroup'=>$idGroup,'idProgram'=>$idProgram,'idSemester'=>$idSemester,'idSubject'=>$idSubject),'default',true));
    	}
    	
    }
    
	
    
	public function groupListAction(){
		
		$this->view->title = "Publish Assessment";	
		
    	$programDb = new Registration_Model_DbTable_Program();
    	$this->view->program = $programDb->getData();
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->getData();    	

    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			$idProgram = $formData["idProgram"];
			$this->view->idProgram = $idProgram;
			
			$idSemester = $formData["idSemester"];
			$this->view->idSemester = $idSemester;
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getCourseTaggingGroupList($idSemester);			
			
			foreach($groups as $i=>$group){				
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getStudentbyGroup($group["IdCourseTaggingGroup"]);				
				$groups[$i]["total_student"]=count($total_student);				
			}		
			$this->view->list_groups = $groups;
			
			/*echo '<pre>';
	    	print_r($groups);
	    	echo '</pre>';*/
    	}
		
    	
	}
	
		
}