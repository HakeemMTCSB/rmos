<?php

class examination_ExamGradeController extends Base_Base { //Controller for the User Module

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Generate GPA/CGPA - Student List");
		
		$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
		
		$msg = $this->_getParam('msg', null);
		
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Grade has been successfully generated");
		}
		
		$form = new Examination_Form_ExamGradeSearchForm();
		$this->view->form = $form;
		
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();	

		
		//program
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();	
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();	
			
			
			if ($form->isValid($formData)) {	
				
				$this->view->idProgram = $formData["IdProgram"];
				
				//get Student
				$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
				$student_list = $studentRegDB->getStudentRegistrationLandscape($formData);				
				$this->view->student_list = $student_list;
				
			}
		}
		
		
	}
	
	public function generateGradeAction(){
		
		
		 if ($this->getRequest()->isPost()) {
		
		 		$grade_calculation = new Cms_ExamCalculation();
		
				$formdata = $this->getRequest()->getPost();	 
				
				//print_r($formdata);
				
				//CALCULATE CGPA				
				for($i=0; $i<count($formdata["IdStudentRegistration"]); $i++){
		
						$IdStudentRegistration =$formdata["IdStudentRegistration"][$i];									
	  					
						$grade_calculation->generateGrade($IdStudentRegistration);
				}
				
				
				$this->gobjsessionsis->flash = array('message' => 'Grade has been generate successfully.', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grade', 'action'=>'index'),'default',true));
		 }
		 
		 
	
	}
	
	
	public function generateGradeOldAction(){
		
		//JANGAN MODIFY DISINI. function ni dah dipindahkan ke Cms_ExamCalculation() ->library 24-6-2015 yati
		
		$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
		$grade_calculation = new Cms_ExamCalculation();
		$StudentGradeDB = new Examination_Model_DbTable_StudentGrade();
		$studentRegistrationDB = new Examination_Model_DbTable_StudentRegistration();
		$systemErrorDB = new App_Model_General_DbTable_SystemError();
		$auth = Zend_Auth::getInstance();
		
        //echo '<pre>';
        
        if ($this->getRequest()->isPost()) {
		
				$formdata = $this->getRequest()->getPost();	 
				
				//print_r($formdata);
				
				//CALCULATE CGPA				
				for($i=0; $i<count($formdata["IdStudentRegistration"]); $i++){
		
						$IdStudentRegistration =$formdata["IdStudentRegistration"][$i];									
	  					
						//get student info
						$student = $studentRegistrationDB->getDatabyId($IdStudentRegistration);
						$idProgram = $student['IdProgram'];
						
						//get registered semester
						//$registered_semester = $semesterStatusDB->getCountableRegisteredSemester($IdStudentRegistration);
						$registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);
						//echo '<pre>';
						//print_r($registered_semester);
						$probation = 0;
						$cum_credit_hour = 0;
						$cum_grade_point = 0;
						$cum_credit_hour_deduction = 0; 
						
						foreach($registered_semester as $semester){
							
								$display  = '<table border=1>';
								$display .= '<tr>
								 				 <td>STATUS</td>
												 <td>SUB Code</td>
												 <td>EXAM STATUS</td>
												 <td>CREDIT HOUR</td>
												 <td>GRADE POINT</td> 
												 <td>GRADE POINT EARNED</td> 
												 <td>GRADE STATUS</td> 
											 </tr>';
			     
							
								//get subject registered in each semester
								if($semester["IsCountable"]==1){				
									$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($IdStudentRegistration,$semester['IdSemesterMain']);
								}else{			
									$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($IdStudentRegistration,$semester['IdSemesterMain']);
								}
								$total_credit_hour = 0;
								$total_credit_hour_ct = 0;
								$total_credit_hour_ex = 0;
								$total_credit_hour_u =0;
								$total_grade_point = 0;
								$total_credit_hour_deduction = 0;
								$total_grade_point_deduction = 0;
								$sem_credit_hour_deduction = 0; 
								
								
								foreach($subject_list as $subject){
									
									if($subject["CourseType"]==2 || $subject["CourseType"]==3 || $subject["CourseType"]==19 ){
										//cek is completed/endorsed
										$isCompleted = $regSubjectDB->isEndorsed($subject);
									}

									if( $subject["exam_status"] != 'I' && $subject["exam_status"] != 'U'){
										
												//get point earned
												if( $subject["exam_status"] == 'IP' || $subject["exam_status"] == 'P') {		
													
													$grade_point_earned = 0;
													if($isCompleted){
														
														//to get total  credit hour		  		
														$total_credit_hour=$total_credit_hour+$subject["credit_hour_registered"];														
														
														$subject["CreditHours"] = $subject["credit_hour_registered"];
														
														$subject["grade_point"] = '-';
														
														//to deduct for gpa calculation
														$sem_credit_hour_deduction = abs($sem_credit_hour_deduction) + abs($subject["credit_hour_registered"]);
													
														//to deduct for cgpa calculation
														$cum_credit_hour_deduction = abs($cum_credit_hour_deduction) +  abs($subject["credit_hour_registered"]);
													}
													
												}else{	
													
													/*if($subject["exam_status"]=="U" ){
														$total_credit_hour_u = $total_credit_hour_u+$subject["CreditHours"];
														$cum_credit_hour_deduction = abs($cum_credit_hour_deduction) + $subject["CreditHours"];
													}*/
													
													if($subject["exam_status"]=="CT" ){
														$total_credit_hour_ct = $total_credit_hour_ct+$subject["CreditHours"];
													}
													
													if($subject["exam_status"]=="EX" ){
														$total_credit_hour_ex = $total_credit_hour_ex+$subject["CreditHours"];
														$cum_credit_hour_deduction = abs($cum_credit_hour_deduction) + $subject["CreditHours"];
													}
													
													//to get total  credit hour	
													//if($subject["exam_status"]!="U" ){	  		
														$total_credit_hour=$total_credit_hour+$subject["CreditHours"];
													//}
													
													//get point earned
													if($subject["exam_status"]=="EX"){
														//$grade_point_earned = abs($subject["CreditHours"])*abs($subject["grade_point"]);
														$grade_point_earned=$grade_point_earned + 0;
													}else{
														$grade_point_earned = abs($subject["CreditHours"])*abs($subject["grade_point"]);										 
													}
												}
												
												if($subject["exam_status"]=="EX"){
													//$total_grade_point;
												}else{
												//get total grade point
													$total_grade_point = abs($total_grade_point)+abs($grade_point_earned);
												}
												
												
												if( ($subject["Active"]==4 || $subject["Active"]==9 || $subject["Active"]==6 || $subject["Active"]==10) && $subject['cgpa_calculation']==1){ // Repeat
													
													
													if($subject['replace_subject']!='' || $subject['replace_subject']!=0){
														
														
														$replace_subject = $regSubjectDB->getPrevReplaceSubjectInfo($IdStudentRegistration,$subject['replace_subject'],$semester['IdSemesterMain']);
													
														//deduction for cumulative calculation
														$total_credit_hour_deduction = $total_credit_hour_deduction + $replace_subject['CreditHours'];
														$total_grade_point_deduction = $total_grade_point_deduction + ($replace_subject['grade_point']*$replace_subject['CreditHours']);
														
													
													}else{
														
														
														//get subject exam grade info registered at previous semester
														$prev_reg_subjects = $regSubjectDB->getRepeatSubjectRegInfo($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMain']);
														
														//deduction for cumulative calculation
														$total_credit_hour_deduction = $total_credit_hour_deduction + $prev_reg_subjects['CreditHours'];
														$total_grade_point_deduction = $total_grade_point_deduction + ($prev_reg_subjects['grade_point']*$prev_reg_subjects['CreditHours']);
													}
													
													/*foreach($prev_reg_subjects as $prev){
														echo '<br>total_credit_hour_deduction:'.$total_credit_hour_deduction = $total_credit_hour_deduction + $prev['CreditHours'];
														echo '<br>total_credit_hour_deduction:'.$total_grade_point_deduction = $total_grade_point_deduction + ($prev['grade_point']*$prev['CreditHours']);
													}*/
													
												}//end repeat
												
												
												/*if($subject["Active"]==6  && $subject['cgpa_calculation']==1){ // Replace
													//get replace subject exam grade info registered at previous semester
													$replace_subject = $regSubjectDB->getPrevReplaceSubjectInfo($IdStudentRegistration,$subject['replace_subject'],$semester['IdSemesterMain']);
													
													//deduction for cumulative calculation
													$total_credit_hour_deduction = $total_credit_hour_deduction + $replace_subject['CreditHours'];
													$total_grade_point_deduction = $total_grade_point_deduction + ($replace_subject['grade_point']*$replace_subject['CreditHours']);
												}*/
												
												
												$display .= '<tr bgcolor="#D0F5A9">
		  														<td>'.$subject["Active"].'&nbsp;</td>
		  														<td>'.$subject["SubCode"].'&nbsp;</td>
														  		<td>'.$subject["exam_status"].'&nbsp;</td>
														  		<td>'.$subject["CreditHours"].'&nbsp;</td>
														  		<td>'.$subject["grade_point"].'&nbsp;</td>
														  		<td>'.$grade_point_earned.'-'.$total_grade_point_deduction.'</td>
														  		<td>'.$subject["grade_status"].'&nbsp;</td>
														  	</tr>';
									
									}else{
										
											
												$display .= '<tr bgcolor="#D0F5A9">
		  														<td>'.$subject["Active"].'&nbsp;</td>
		  														<td>'.$subject["SubCode"].'&nbsp;</td>
														  		<td>'.$subject["exam_status"].'&nbsp;</td>
														  		<td>'.$subject["CreditHours"].'&nbsp;</td>
														  		<td>'.$subject["grade_point"].'&nbsp;</td>
														  		<td>'.$grade_point_earned.'-'.$total_grade_point_deduction.'</td>
														  		<td>'.$subject["grade_status"].'&nbsp;</td>
														  	</tr>';
										
									}//end I									
										
									
								}//end subject
								
								
								//$gpa = $grade_calculation->calculateGPA($total_grade_point, ($total_credit_hour - $sem_credit_hour_deduction) );
								if($semester["SemesterFunctionType"]==2 || $semester["SemesterFunctionType"]==3){
									$gpa="-";
									$cum_credit_hour = (abs($cum_credit_hour) + abs($total_credit_hour)) - abs($total_credit_hour_deduction);
								}else{
									$gpa = $grade_calculation->calculateGPA($total_grade_point, ($total_credit_hour - $sem_credit_hour_deduction - $total_credit_hour_ex ) );
									
									//cumulative
									$cum_credit_hour = (abs($cum_credit_hour) + abs($total_credit_hour)) - abs($total_credit_hour_deduction);
									$cum_grade_point = (abs($cum_grade_point) + abs($total_grade_point)) - abs($total_grade_point_deduction);
									
									
									$cgpa = $grade_calculation->calculateCGPA($cum_grade_point,($cum_credit_hour  - $cum_credit_hour_deduction));
									//$cgpa = $grade_calculation->calculateCGPA($cum_grade_point,($cum_credit_hour  - $cum_credit_hour_deduction - $total_credit_hour_ex - $total_credit_hour_u));
								}
								$display .= "<tr> 
											    <td>&nbsp;</td>
												<td colspan=3>Total Credit Hours :$total_credit_hour</td>
												<td colspan=3>Cum. Credit Hours :$cum_credit_hour</td>
											</tr>";
	  							$display .= "<tr>
	  										    <td>&nbsp;</td>
	  											<td colspan=3>Total Grade Points :$total_grade_point</td>
	  											<td colspan=3>Cum. Grade Points  :$cum_grade_point</td>
	  										</tr>";
	  							$display .="<tr>
	  											<td>&nbsp;</td>
	  											<td colspan=3>Grade Point Average (GPA) :$gpa</td>
												<td colspan=3>Cum. GPA (CGPA) :$cgpa ($cum_credit_hour - $cum_credit_hour_deduction) </td> 
											</tr>";
			  					$display .= '</table><br><br>';
								
								echo 'Semester:'.$semester['SemesterMainName'];
								echo $display;
								
								//get cgpa status
								try{								
									$academic_status = $grade_calculation->getAcademicStatus($semester["IdSemesterMain"],$idProgram,1,null,$cgpa);
								}catch (Exception $event){
									
									$error['se_txn_id']=0;
									$error['se_IdStudentRegistration']=$IdStudentRegistration;
									$error['se_IdStudentRegSubjects']=0;
									$error['se_title']='Student Course Registration';
									$error['se_message']=$event->getMessage();
									$error['se_createddt']=date("Y-m-d H:i:s");
									$error['se_createdby']=$auth->getIdentity()->id;
									
									$systemErrorDB->addData($error);
								}
								
								if(isset($academic_status)){
								
									if($academic_status['Probation']==1){
										$probation = $probation+1;
										if($probation==1){
											$cgpa_status = '1st Probation';
										}else
										if($probation==2){
											$cgpa_status = '2nd Probation';
										}else{
											$cgpa_status = 'Probation';
										}	
									}else{
										$cgpa_status= 'Pass';
									}								
								}else{
									$cgpa_status ='';
								}
								
								
								//save section
								$grade_info["sg_IdStudentRegistration"]=$IdStudentRegistration;
					  			$grade_info["sg_semesterId"]=$semester["IdSemesterMain"];					  		
					  			$grade_info["sg_idstudentsemsterstatus"]=$semester["idstudentsemsterstatus"];
					  			
					  			$grade_info["sg_sem_credithour"]=$total_credit_hour;
					  			$grade_info["sg_sem_totalpoint"]=$total_grade_point;
					  			$grade_info["sg_gpa"]=$gpa;	
					  			
					  			$grade_info["sg_cum_credithour"]=$cum_credit_hour;
					  			$grade_info["sg_cum_totalpoint"]=$cum_grade_point;
					  			$grade_info["sg_cgpa"]=$cgpa;	
					  			$grade_info["sg_cgpa_status"]=$cgpa_status;				  			
					  								  			
					  					
									
								//check if student grade already exist
					  			$student = $StudentGradeDB->checkStudent($IdStudentRegistration,$semester["IdSemesterMain"]);
					  			
					  			if(is_array($student)){			  				
					  				//echo 'update';
					  				$StudentGradeDB->updateData($grade_info,$student["sg_id"]);
					  			}else{
					  				//echo 'add';
					  				$StudentGradeDB->addData($grade_info);
					  			}
								
						}//end semester
						
						
						
	  					
				}
				//END CGPA Calculation

				$this->gobjsessionsis->flash = array('message' => 'Grade has been generate successfully.', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-grade', 'action'=>'index'),'default',true));
						
        }
         
	}
	
	
	
	
}
?>