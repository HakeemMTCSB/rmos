<?php

class examination_MarkEntryController extends Base_Base { //Controller for the User Module

	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Mark Entry - Section List");
		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkEntrySearchCourseGroup(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();		
		
			
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			//$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
												
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				
				
				foreach($groups as $i=>$group){
				
					$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"], false);
					$total_submit = $courseGroupStudent->getTotalStudentMarkSubmitApproval($group["IdCourseTaggingGroup"], false);
					$total_approve = $courseGroupStudent->getTotalStudentMarkApproved($group["IdCourseTaggingGroup"], false);
					
					$groups[$i]["total_student"] = $total_student;
					$groups[$i]["total_submit"] = $total_submit;
					$groups[$i]["total_approve"] = $total_approve;
					
				}
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}//if post		
	}
	
	
	public function mySectionAction() {
		
		//title
		$this->view->title= $this->view->translate("Mark Entry - Section List");
		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MyGroupSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	$semesterDB = new Registration_Model_DbTable_Semester();
    	$semester = $semesterDB->getAllCurrentSemester();		    	    	
		
    	$cur_sem = array();
    	foreach($semester as $sem){
    		array_push($cur_sem,$sem['IdSemesterMaster']);
    	}
    	
		
    	//get group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();	
				
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();		
		
			$this->view->idSemester = $formData["IdSemester"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			 	$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
									
				foreach($groups as $i=>$group){
				
					$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"], false);
					$total_submit = $courseGroupStudent->getTotalStudentMarkSubmitApproval($group["IdCourseTaggingGroup"], false);
					$total_approve = $courseGroupStudent->getTotalStudentMarkApproved($group["IdCourseTaggingGroup"], false);
					
					$groups[$i]["total_student"] = $total_student;
					$groups[$i]["total_submit"] = $total_submit;
					$groups[$i]["total_approve"] = $total_approve;
					
				}
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}else{
    		
    		//display current semester
    		$formData['IdSemesterArray'] = $cur_sem;
    		$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
    		
    		foreach($groups as $i=>$group){
				
					$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"],$group['IdProgram']);
					$total_submit = $courseGroupStudent->getTotalStudentMarkSubmitApproval($group["IdCourseTaggingGroup"]);
					$total_approve = $courseGroupStudent->getTotalStudentMarkApproved($group["IdCourseTaggingGroup"]);
					
					$groups[$i]["total_student"] = $total_student;
					$groups[$i]["total_submit"] = $total_submit;
					$groups[$i]["total_approve"] = $total_approve;
					
			}
			$this->view->groups = $groups;
    		
    	}//if post		
	}
	
    public function viewComponentAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	$this->view->role = $auth->getIdentity()->IdRole;
    	
    	$this->view->title=$this->view->translate("Mark Entry - Component List");
    	
    	if($this->_getParam('msg')==1){
    		$this->view->noticeSuccess = $this->view->translate("Data has been saved");
    	}
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('id');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);			
		$this->view->group = $group;
		
		//get scheme
		$groupProgramDB = new GeneralSetup_Model_DbTable_CourseGroupProgram();
		$this->view->scheme = $groupProgramDB->getListScheme($idGroup,$idProgram);
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->idProgramScheme = $formData['IdProgramScheme'];
			
			//get component
	    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
			$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$formData['IdProgramScheme']);	    
			
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	    	foreach($list_component as $index=>$component){
	    		
			  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			  	  $list_component[$index]['component_item'] = $component_item;
	    	}
	    	$this->view->rs_component = $list_component;
	    	
	    	//print_r($list_component);
			
		}
    	
    	
    }

	public function historyAction()
	{
		$this->view->title=$this->view->translate("Mark Entry History");

		$idGroup = $this->_getParam('id');
		$idSemester = $this->_getParam('idSemester');
		$idProgram = $this->_getParam('idProgram');
		$idSubject = $this->_getParam('idSubject');
		$idLog = $this->_getParam('idLog');

		$this->view->idSemester = $idSemester;
		$this->view->idProgram = $idProgram;
		$this->view->idSubject = $idSubject;
		$this->view->idGroup = $idGroup;

		//get info
		$info = Cms_ExamMarkEntry::get($idGroup,$idProgram, $idSemester, $idSubject, true );

		if ( empty($info) )
		{
			throw new Exception('Invalid Mark Entry Parameters');
		}

		//get info semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semester = $semesterDB->fngetSemestermainDetails($idSemester);
		$this->view->semester = $semester;

		//get info program
		$programDB = new GeneralSetup_Model_DbTable_Program();
		$this->view->program = $programDB->fngetProgramData($idProgram);

		//get info subject
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->view->subject = $subjectDB->getData($idSubject);

		//get info group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$this->view->group = $courseGroupDb->getInfo($idGroup);

		//get component
		$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);

		if(count($list_component)>0){

			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();
			foreach($list_component as $index=>$component){

				$component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
				$list_component[$index]['component_item'] = $component_item;


				if($component['Status']!=1){
					$flag = false;
				}
			}
		}else{
			$flag = false;
		}

		$this->view->rs_component = $list_component;

		//logs
		$markLogDb = new Examination_Model_DbTable_StudentMarkEntryLog();

		$results = $markLogDb->getData(array('marks_entry_id = ?' => $info['marks_entry_id']));

		if ( empty($idLog) && !empty($results) )
		{
			$logid = $results[0]['log_id'];

			$this->redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry','action'=>'history','id'=>$idGroup,'idProgram'=>$idProgram,'idSemester'=>$idSemester,'idSubject'=>$idSubject,'idLog'=>$logid), 'default', true));
		}


		foreach ( $results as $result )
		{
			if ( $result['log_id'] == $idLog )
			{
				$logData = $result['logdata'];
			}
		}

		$this->view->idLog = $idLog;
		$this->view->logData = $logData;


		//students
		$studentDB = new Examination_Model_DbTable_StudentRegistration();
		$students = $studentDB->getStudentEntrybyGroup($idSemester,$idProgram,$idSubject,$idGroup);

		$db = Zend_Db_Table::getDefaultAdapter();
		$examRegDB = new Registration_Model_DbTable_ExamRegistration();
		$all_semester = $examRegDB->getSemester($idSemester);

		foreach($students as $key=>$student){

			if($student['exam_status']=='U' && $student['audit_program']==''){
				unset($students[$key]);
			}else{

				//is registered this subject at previous semester?
				//to cater for absent with valid reason -> to display previous mark

				$select_reg_prev = $db->select()
					->from(array('srs'=>'tbl_studentregsubjects'))
					->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = srs.IdStudentRegistration AND er.er_idSubject = srs.IdSubject AND er.er_idSemester=srs.IdSemesterMain',array())
					->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = srs.IdSemesterMain',array('prev_absent_sem'=>'SemesterMainName'))
					->where('srs.IdSemesterMain NOT IN (?)',$all_semester)
					->where('srs.Active != ?',3)
					->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
					->where('er.er_attendance_status = ?',396) //Absent With Valid Reason
					->where('srs.IdSubject = ?',$idSubject)
					->order('sm.SemesterMainStartDate DESC');
				$reg_prev = $db->fetchRow($select_reg_prev);

				if($reg_prev){
					$students[$key]['prev_absent_with_reason'] = $reg_prev;
				}else{
					$students[$key]['prev_absent_with_reason'] = false;
				}

				foreach($list_component as $c=>$component){

					$select_main_mark = $db->select()
						->from(array('sme'=>'tbl_student_marks_entry'))
						->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])
						->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
					// ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);
					$entry_main = $db->fetchAll($select_main_mark);

					foreach($entry_main as $m=>$main){
						$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
						$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
						$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];

						//get item
						$select_item_mark = $db->select()
							->from(array('smed'=>'tbl_student_detail_marks_entry'))
							->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);
						$entry_item = $db->fetchAll($select_item_mark);

						foreach($entry_item as $i=>$item){
							$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
							$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
							$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
						}//end foreach
					}//end foreach
				}//end foreach
			}//end else

		}

		array_values($students);

		//echo '<pre>';
		//print_r($students);
		$this->view->students = $students;

		$this->view->results = $results;
	}
 	
    
	public function studentListAction(){
    	
    	$this->view->title=$this->view->translate("Mark Entry");
    
    	$auth = Zend_Auth::getInstance();
    	$this->view->role = $auth->getIdentity()->IdRole;    

    	if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){ //admin	atau asad admin		
    		$this->view->urlBack = $this->view->url(array('module'=>'examination','controller'=>'mark-entry','action'=>'index'), 'default', true);
    	}else{
    		$this->view->urlBack = $this->view->url(array('module'=>'examination','controller'=>'mark-entry','action'=>'my-section'), 'default', true);
    	}
    	
    	$idGroup = $this->_getParam('id');
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');    	
    	    	   	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
    	
    	$flag = true;

		//get info
		$info = Cms_ExamMarkEntry::get($idGroup,$idProgram, $idSemester, $idSubject, true );

		$this->view->markinfo = $info;

    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	$this->view->semester = $semester;
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get info group
    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$this->view->group = $courseGroupDb->getInfo($idGroup);
		
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
		

		if(count($list_component)>0){			
		
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();
	    	foreach($list_component as $index=>$component){
	    		
			  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			  	  $list_component[$index]['component_item'] = $component_item;
			  	  
			  	  
			  	  if($component['Status']!=1){
			  	  	 $flag = false;
			  	  }
	    	}
		}else{
			$flag = false;
		}
    	
		$this->view->rs_component = $list_component;
    	$this->view->flag = $flag;
    	
    	//get list student yg register  semester,program,subject di atas
    	//keluarkan aje dulu list student filter by group or lecturer later
    	
        $form = new Examination_Form_MarkEntrySearchStudent(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject));
     	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudentEntrybyGroup($idSemester,$idProgram,$idSubject,$idGroup,$formData, false);
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudentEntrybyGroup($idSemester,$idProgram,$idSubject,$idGroup, null, false);
	    	
    	} 
    	
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$examRegDB = new Registration_Model_DbTable_ExamRegistration();	
    	$all_semester = $examRegDB->getSemester($idSemester);
    	
    	foreach($students as $key=>$student){
    		
    		if($student['exam_status']=='U' && $student['audit_program']==''){
    			unset($students[$key]);
    		}else{
    			
    			//is registered this subject at previous semester?
    			//to cater for absent with valid reason -> to display previous mark
    			
    			$select_reg_prev = $db->select()
    								  ->from(array('srs'=>'tbl_studentregsubjects'))
    								  ->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = srs.IdStudentRegistration AND er.er_idSubject = srs.IdSubject AND er.er_idSemester=srs.IdSemesterMain',array())
    								  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = srs.IdSemesterMain',array('prev_absent_sem'=>'SemesterMainName'))
    								  ->where('srs.IdSemesterMain NOT IN (?)',$all_semester)
    								  ->where('srs.Active != ?',3)
    								  ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
    								  ->where('er.er_attendance_status = ?',396) //Absent With Valid Reason
    								  ->where('srs.IdSubject = ?',$idSubject)
    								  ->order('sm.SemesterMainStartDate DESC');
    			$reg_prev = $db->fetchRow($select_reg_prev);
    			
    			if($reg_prev){
    				$students[$key]['prev_absent_with_reason'] = $reg_prev;
    			}else{
    				$students[$key]['prev_absent_with_reason'] = false;
    			}
    			
	    		foreach($list_component as $c=>$component){
	    			
	    			$select_main_mark = $db->select()
					 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
					 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
					 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
					 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
				 	$entry_main = $db->fetchAll($select_main_mark);	
				 	
				 	foreach($entry_main as $m=>$main){
				 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
				 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
				 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
				 		
				 		//get item
			 			$select_item_mark = $db->select()
				 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
				 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
				 		$entry_item = $db->fetchAll($select_item_mark);	
				 		
				 		foreach($entry_item as $i=>$item){
				 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
				 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
				 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
				 		}//end foreach
				 	}//end foreach
	    		}//end foreach
    		}//end else
    		
    	}
    	
		array_values($students);
		
    	//echo '<pre>';
    	//print_r($students);
		$this->view->students = $students;
		
    }
    
  
    
   
    
	public function groupListAction(){
		
		$this->view->title = "Mark Entry - Course Group List";	
		
    	$programDb = new Registration_Model_DbTable_Program();
    	$this->view->program = $programDb->getData();
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->getData();    	

    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			$idProgram = $formData["idProgram"];
			$this->view->idProgram = $idProgram;
			
			$idSemester = $formData["idSemester"];
			$this->view->idSemester = $idSemester;
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getCourseTaggingGroupList($idSemester);			
			
			foreach($groups as $i=>$group){				
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getStudentbyGroup($group["IdCourseTaggingGroup"]);				
				$groups[$i]["total_student"]=count($total_student);				
			}		
			$this->view->list_groups = $groups;
			
			/*echo '<pre>';
	    	print_r($groups);
	    	echo '</pre>';*/
    	}
		
    	
	}
	
	
	function ajaxSaveMarkAction(){
		
				$this->_helper->layout()->disableLayout();
				
				$auth = Zend_Auth::getInstance();
				
				$program_id = $this->_getParam('idProgram', null);
				$semester_id = $this->_getParam('idSemester', null);		
				$subject_id = $this->_getParam('idSubject', null);
				$idGroup = $this->_getParam('idGroup', null);
				
				$idMaster = $this->_getParam('idMaster', null);
				$IdMarksDistributionDetails = $this->_getParam('idDetail', null);
					
				$IdStudentRegistration = $this->_getParam('id', null);
				$IdStudentRegSubjects = $this->_getParam('idRegSub', null);
				
				$IdStudentMarksEntry = $this->_getParam('idMarkEntry', null);
				$IdStudentMarksEntryDetail = $this->_getParam('idMarkEntryDetail', null);
				
				$markObtained = $this->_getParam('markObtained', null); //entry mark
				$TotalItemRaw = $this->_getParam('TotalItemRaw', null); //total raw mark for each component
				$TotalItemPercent = $this->_getParam('TotalItemPercent', null);			
				$grandTotal = $this->_getParam('grandTotal', null); //total mark course based (after weightage)

				$formdata = $this->_getParam('formdata', null);
				
				$ajaxContext = $this->_helper->getHelper('AjaxContext');
				$ajaxContext->addActionContext('view', 'html');
				$ajaxContext->initContext();

				$infoId = Cms_ExamMarkEntry::update($idGroup,$program_id,$semester_id,$subject_id);

				if ( !empty($formdata) )
				{
					$formdata = json_decode($formdata, true);

					$marklogDb = new Examination_Model_DbTable_StudentMarkEntryLog();

					$marklogDb->insert(array(
												'marks_entry_id'	=> $infoId,
												'logdata'			=> json_encode($formdata),
												'created_by'		=> Zend_Auth::getInstance()->getIdentity()->iduser,
												'created_date'		=> new Zend_Db_Expr('NOW()')
					));
				}

    			
				//$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	  	    	//$item = $oCompitem->getDataComponentItem($IdMarksDistributionDetails);	  	     			
    			
	  	    	//get main component total mark after percentage
	  	    	//$FinalMarksObtained = $cms_calculation->calculateMark($markObtained,$item["Weightage"],$item["Percentage"]);				    			
	  	    	
				
				$cms_calculation = new Cms_ExamCalculation();
				$markDB = new Examination_Model_DbTable_StudentMarkEntry();
				$markDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
				$studentRegistarationDB = new Examination_Model_DbTable_StudentRegistration(); 
			    		
				//get main component info
	    		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
	    		$main_component = $markDistributionDB->getInfoComponent($idMaster);


	        	
			  
    			
    			$data["TotalMarkObtained"] = $TotalItemRaw; //Total Raw Mark each item++
    			$data["FinalTotalMarkObtained"]=$TotalItemPercent; //afrer calculation made
    			$data["UpdUser"] = $auth->getIdentity()->iduser;
		    	$data["UpdDate"] = date('Y-m-d H:i:s');	
		    	
		    	
		    	if(isset($IdStudentMarksEntry) && $IdStudentMarksEntry!=''){
		    		$markDB->updateData($data,$IdStudentMarksEntry);
		    	}else{
		    		if(isset($markObtained) && $markObtained!=''){
			    				
		    			$data["IdStudentRegistration"] = $IdStudentRegistration;
		    			$data["IdStudentRegSubjects"] =$IdStudentRegSubjects;  
			    		$data["IdSemester"] = $semester_id;
				    	$data["Course"] = $subject_id;   
				    	$data["IdMarksDistributionMaster"] = $idMaster;
				    	$data["MarksTotal"] = $main_component["Marks"]; 
				    	$data["Component"]   =  $main_component["IdComponentType"];
				    	$data["ComponentItem"]   =  ''; 				
				    	$data["Instructor"] = '';    		
				    	$data["AttendanceStatus"] = '';
				    	$data["MarksEntryStatus"] = 0; //entry
	    	
		    			$IdStudentMarksEntry = $markDB->addData($data);	
		    		}
		    	}
    			
    			
	    				
   		
				//2nd : Save Details Component Mark    			
    			
	  	  		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	  	    	$item = $oCompitem->getDataComponentItem($IdMarksDistributionDetails);	  	     			
    			
	  	    	//get main component total mark
	  	    	$FinalMarksObtained = $cms_calculation->calculateMark($markObtained,$item["Weightage"],$item["Percentage"]);				    			
	  	    	
		    	$dataItem["MarksObtained"] = $markObtained;
				$dataItem["FinalMarksObtained"] = $FinalMarksObtained;
		    	$dataItem["UpdUser"] = $auth->getIdentity()->iduser;
				$dataItem["UpdDate"] = date('Y-m-d H:i:s');
    			
				if(isset($IdStudentMarksEntryDetail) && $IdStudentMarksEntryDetail!=''){
					
					 if($markObtained==''){
					 	
					 	//delete sebab null ibarat no mark
					 	$markDetailsDB->deleteData($IdStudentMarksEntryDetail);
					 	$IdStudentMarksEntryDetail='x';
					 	
					 	// count item mark ada lain x					 					  	 
				  	    $item_entry = $markDB->getMarkById($IdStudentMarksEntry);
				  	    if(count($item_entry)>0){
				  	    	//do nothing
				  	    }else{
		  	   				//x ada item delete main	
		  	   				//echo 'masuk';	  	   				
		  	   				$markDB->deleteData($IdStudentMarksEntry);
				 		    $IdStudentMarksEntry='x';
		  	   			}
					 	
					 }else{
    					 $markDetailsDB->updateData($dataItem,$IdStudentMarksEntryDetail);	    					
					 }
    				 
    			}else{	    	

		    			$dataItem["IdStudentMarksEntry"] = $IdStudentMarksEntry;   
						$dataItem["Component"] = $main_component["IdComponentType"];   
						$dataItem["ComponentItem"]   =  '';
						$dataItem["ComponentDetail"] = $IdMarksDistributionDetails;				
				    	$dataItem["TotalMarks"] = $item["Weightage"]; //jumlah penuh soalan bukan pemberat
		    
    				 	$IdStudentMarksEntryDetail  = $markDetailsDB->addData($dataItem);
    			}   			
    		 				
    		    			    			
    		//nak update markah keseluruhan campur semua mark distribution component yg ada
    		//cari mark distribution bagi semester,subject,program
    		
    		$grade = $markDB->saveStudentSubjectMark($IdStudentRegSubjects,$semester_id,$program_id,$subject_id,$grandTotal);
    			
    		$result = array( 'IdStudentMarksEntry'=>$IdStudentMarksEntry,
    		 			     'IdStudentMarksEntryDetail'=>$IdStudentMarksEntryDetail,
    		 				 'grade'=>$grade['grade_name'],
    						 'grade_point'=>$grade['grade_point'],
    						 'message'=>$grade['message'],
			 				 'updated'=>date('d-m-Y H:i A'));
		
			$ajaxContext->addActionContext('view', 'html')
						->addActionContext('form', 'html')
						->addActionContext('process', 'json')
						->initContext();
		
			$json = Zend_Json::encode($result);
		
			echo $json;
			exit();
		
	}
	
	
public function searchCourseAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);

			
		if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Subject
			*/
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
			//Subject list base on Program Landscape from the selected faculty
			//$programs = $progDB->fngetProgramDetails($faculty_id);
				
				
			$allsemlandscape = null;
			$allblocklandscape = null;
			$i=0;
			$j=0;
			//foreach ($programs as $key => $program){
				$activeLandscape=$landscapeDB->getAllActiveLandscape($program_id);
				foreach($activeLandscape as $actl){
					if($actl["LandscapeType"]==43){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}elseif($actl["LandscapeType"]==44){
						$allblocklandscape[$j] = $actl["IdLandscape"];
						$j++;
					}
				}
			//}
				
				
			$subjectsem = null;
			$subjectblock = null;
				
			if(is_array($allsemlandscape))
				$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,null,$semester_id);
			if(is_array($allblocklandscape))
				$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,null,$semester_id);
				
			$subjects = null;
			if(is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=array_merge( $subjectsem , $subjectblock );
			}else{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=$subjectblock;
				}
			}
			//end subject list
			
			$i=0;
			foreach($subjects as $subject){
					
				//get total student register this subject
				$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
				$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$semester_id);
				$subject["total_student"] = $total_student;
					
				//get total group creates
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$semester_id);
				$subject["total_group"] = $total_group;
				$subject["IdSemester"] = $semester_id;				
					
				$subjects[$i]=$subject;
					
				$i++;
			}
				
			foreach ($subjects as $index => $subject){
				if($subject["total_student"]>0 && $subject["total_group"]>0){
						
				}else{
					unset($subjects[$index]);
				}
			}
			/*
			 * End search subject
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($subjects);
	
			echo $json;
			exit();
		}
	}
	
	function searchCourseGroupAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$iduser = $auth->getIdentity()->iduser;
		
		
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			/*
			 * Search Group
			*/		
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getMarkEntryGroupList($subject_id,$semester_id);
			
			
			$i=0;
			foreach($groups as $group){
					
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				//$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
				$total_student = $courseGroupStudent->getTotalStudentViaSubReg($group["IdCourseTaggingGroup"]);
					
				$group["total_student"] = $total_student;
				$groups[$i]=$group;
					
				$i++;
			}
			/*
			 * End search group
			*/
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($groups);
		
			echo $json;
			exit();
		
		}
	}
	
	
	function searchGroupCourseAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$iduser = $auth->getIdentity()->iduser;
		
		
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$program_id = $this->_getParam('program_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$course = $courseGroupDb->getCourseByLectGroup($semester_id,$program_id);			
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($course);
		
			echo $json;
			exit();
		
		}
	}
	
	   public function changeStatusAction(){
    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;
	    	
	    	if ($this->getRequest()->isPost()) {
			
				$formData = $this->getRequest()->getPost();			
		
	    		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
	    		$markEntryHistoryDB = new Examination_Model_DbTable_StudentMarkEntryHistory();
	    	
	    		for($i=0; $i<count($formData['IdRegSub']); $i++){
	    			
	    			$data['mark_approval_status']=1;
	    			$subjectRegDB->updateData($data,$formData['IdRegSub'][$i]);
	    			
	    			//ini mcam audit trail la nak keep track changes 
    			    $status['IdStudentRegSubjects'] = $formData['IdRegSub'][$i];
    			    $status['cs_status'] = 1;
    			    $status['cs_remarks'] = '';
    			    $status["cs_createdby"] = $auth->getIdentity()->iduser;
    			    $status["cs_createddt"] = date('Y-m-d H:i:s');
    			    $markEntryHistoryDB->addChangeStatus($status);
	    		}
	    		
	    		$this->sendMail(array('idSubject'=>$formData['idSubject'],'idGroup'=>$formData['idGroup']));
	    		
	    		$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');	    		
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry', 'action'=>'student-list','id'=>$formData['idGroup'],'idProgram'=>$formData['idProgram'],'idSemester'=>$formData['idSemester'],'idSubject'=>$formData['idSubject']),'default',true));
		   
	    	}
	    	
	    	exit;
	   }
	   
	   
	   public function sendMail($formData){

	   		
    		$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    		    		
    		//get group info			
    		$group = $groupDB->getInfo($formData["idGroup"]);
    		    		
    		//dean profile
    		$recepient = $groupDB->getDeanProfile($formData['idGroup']);    				
    		
    		//dean profile  
    		$userDb = new GeneralSetup_Model_DbTable_User();			  				
    		$lecturer = $userDb->getstaffdetails($group['IdLecturer']);    		
    		
    		$data['semester'] = $group['semester_name'];
    		$data['subject_code'] = $group['subject_code'];
    		$data['subject_name'] = $group['subject_name'];
    		$data['group'] = $group['GroupCode'].' - '.$group['GroupName'];
    		
    		$cms = new Cms_SendMail();
	 		$cms->ExamSendMail(120,array('FullName'=>$lecturer['FullName']),$recepient,$data);	
    }
	
	    public function printAction(){
	    	
			//require_once ('jpgraph/jpgraph.php');
			//require_once ('jpgraph/jpgraph_bar.php');

			$this->_helper->layout->disableLayout();
	    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;    	
	    	
	    	$idGroup = $this->_getParam('id');
	    	$idSemester = $this->_getParam('idSemester');
	    	$idProgram = $this->_getParam('idProgram');
	    	$idSubject = $this->_getParam('idSubject');    	
	    	    	   	
	    	//get info semester
	    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
	    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
	    	$this->view->semester = $semester;
	    	
	    	//get info program
	    	$programDB = new GeneralSetup_Model_DbTable_Program();
	    	$this->view->program = $programDB->fngetProgramData($idProgram);
	    	
	    	//get info subject
	    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
	    	$this->view->subject = $subjectDB->getData($idSubject);
	    	
	    	//get info group
	    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$this->view->group = $courseGroupDb->getInfo($idGroup);
    				    		    		
	    	//get component
	    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
			$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
			
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	    	foreach($list_component as $index=>$component){
	    		
			  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			  	  $list_component[$index]['component_item'] = $component_item;
	    	}
	    	$this->view->rs_component = $list_component;
	    	
	    	//get list student yg register  semester,program,subject di atas
	    	//keluarkan aje dulu list student filter by group or lecturer later
	    	
	       $studentDB = new Examination_Model_DbTable_StudentRegistration();
		   $students = $studentDB->getStudentEntrybyGroup($idSemester,$idProgram,$idSubject,$idGroup, null, false);
	    	
	    	$db = Zend_Db_Table::getDefaultAdapter();	
	    	
	    	foreach($students as $key=>$student){
	    		
	    		if($student['exam_status']=='U' && $student['audit_program']==''){
    				unset($students[$key]);
    			}else{
		    		foreach($list_component as $c=>$component){
		    			
		    			$select_main_mark = $db->select()
						 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
						 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
						 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
						 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
					 	$entry_main = $db->fetchAll($select_main_mark);	
					 	
					 	foreach($entry_main as $m=>$main){
					 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
					 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
					 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
					 		
					 		//get item
				 			$select_item_mark = $db->select()
					 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
					 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
					 		$entry_item = $db->fetchAll($select_item_mark);	
					 		
					 		foreach($entry_item as $i=>$item){
					 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
					 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
					 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
					 		}
					 	}
		    		}
    			}
	    	}
	    	array_values($students);
	
			$this->view->students = $students;
			$this->view->filename = date('Ymd').'_gradebook.xls';

			//graph test
			/*$data1y=array(47,80,40,116);
			$data2y=array(61,30,82,105);
			$data3y=array(115,50,70,93);


			// Create the graph. These two calls are always required
			$graph = new Graph(350,200,'auto');
			$graph->SetScale("textlin");

			$theme_class=new UniversalTheme;
			$graph->SetTheme($theme_class);

			$graph->yaxis->SetTickPositions(array(0,30,60,90,120,150), array(15,45,75,105,135));
			$graph->SetBox(false);

			$graph->ygrid->SetFill(false);
			$graph->xaxis->SetTickLabels(array('A','B','C','D'));
			$graph->yaxis->HideLine(false);
			$graph->yaxis->HideTicks(false,false);

			// Create the bar plots
			$b1plot = new BarPlot($data1y);
			$b2plot = new BarPlot($data2y);
			$b3plot = new BarPlot($data3y);

			// Create the grouped bar plot
			$gbplot = new GroupBarPlot(array($b1plot,$b2plot,$b3plot));
			// ...and add it to the graPH
			$graph->Add($gbplot);


			$b1plot->SetColor("white");
			$b1plot->SetFillColor("#cc1111");

			$b2plot->SetColor("white");
			$b2plot->SetFillColor("#11cccc");

			$b3plot->SetColor("white");
			$b3plot->SetFillColor("#1111cc");

			$graph->title->Set("Bar Plots");

			// Display the graph
			$graphDir = DOCUMENT_PATH.'/graph';
			if ( !is_dir( $graphDir ) )
			{
				if ( mkdir_p($graphDir) === false )
				{
					throw new Exception('Cannot create folder ('.$graphDir.')');
				}
			}

			$graphFile = md5(time()).'.png';
			$graph->Stroke($graphDir.'/'.$graphFile);
			

			//$this->view->graph1 = DOCUMENT_URL.'/graph/'.$graphFile;
			$this->view->graph1 = DOCUMENT_URL.'/graph/11e35d2e8d0a53c1e5cab9b869be38c8.png';*/
	    }

	public function printFormatAction(){

		$this->_helper->layout->disableLayout();

		$auth = Zend_Auth::getInstance();
		$this->view->role = $auth->getIdentity()->IdRole;

		$idGroup = $this->_getParam('id');
		$idSemester = $this->_getParam('idSemester');
		$idProgram = $this->_getParam('idProgram');
		$idSubject = $this->_getParam('idSubject');

		//get info semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semester = $semesterDB->fngetSemestermainDetails($idSemester);
		$this->view->semester = $semester;

		//get info program
		$programDB = new GeneralSetup_Model_DbTable_Program();
		$program = $this->view->program = $programDB->fngetProgramData($idProgram);

		//get info subject
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $this->view->subject = $subjectDB->getData($idSubject);

		//get info group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $this->view->group = $courseGroupDb->getInfo($idGroup);

		//get component
		$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);

		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();
		foreach($list_component as $index=>$component){

			$component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			$list_component[$index]['component_item'] = $component_item;
		}
		$rs_component = $this->view->rs_component = $list_component;

		//get list student yg register  semester,program,subject di atas
		//keluarkan aje dulu list student filter by group or lecturer later

		$studentDB = new Examination_Model_DbTable_StudentRegistration();
		$students = $studentDB->getStudentEntrybyGroup($idSemester,$idProgram,$idSubject,$idGroup, null, false);

		$db = Zend_Db_Table::getDefaultAdapter();

		foreach($students as $key=>$student){

			if($student['exam_status']=='U' && $student['audit_program']==''){
				unset($students[$key]);
			}else{
				foreach($list_component as $c=>$component){

					$select_main_mark = $db->select()
						->from(array('sme'=>'tbl_student_marks_entry'))
						->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])
						->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
					// ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);
					$entry_main = $db->fetchAll($select_main_mark);

					foreach($entry_main as $m=>$main){
						$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
						$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
						$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];

						//get item
						$select_item_mark = $db->select()
							->from(array('smed'=>'tbl_student_detail_marks_entry'))
							->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);
						$entry_item = $db->fetchAll($select_item_mark);

						foreach($entry_item as $i=>$item){
							$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
							$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
							$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
						}
					}
				}
			}
		}
		array_values($students);

		require_once ('PHPExcel/Classes/PHPExcel.php');

		$this->view->students = $students;
		$filename = $this->view->filename = date('Ymd').'_upload_gradebook.xls';

		$table = '<table class="table" width="600px" cellpadding="0" cellspacing="1" border="1">';
		$table .= '<tr>';
		$table .= '<th width="20%">Semester Name</th>';
		$table .= '<td colspan=5>'.$semester[0]['SemesterMainName'].'</td>';
		$table .= '</tr>';
		$table .= '<tr>';
		$table .= '<th>Course Name</th>';
		$table .= '<td colspan=5>'.$subject['SubjectName'].' ('.$subject['SubCode'].')'.'</td>';
		$table .= '</tr>';
		$table .= '<tr>';
		$table .= '<th>Section Name</th>';
		$table .= '<td colspan=5>'.$group["GroupName"].' - '.$group["GroupCode"].'</td>';
		$table .= '</tr>';
		$table .= '<tr>';
		$table .= '<th>Lecturer</th>';
		$table .= '<td colspan=5>'.$group['FullName'].'</td>';
		$table .= '</tr>';
		$table .= '</table>';
		$table .= '<br /><br />';
		$table .= '<table class="table" width="100%" cellpadding="5" cellspacing="2" border="1">';
		$table .= '<tr>';
		$table .= '<th rowspan="3" width="25px">No</th>';
		$table .= '<th rowspan="3" width="25px" style="vertical-align:middle;" >Status</th>';
		$table .= '<th rowspan="3" style="vertical-align:middle;">Student Name</th>';
		$table .= '<th rowspan="3" style="vertical-align:middle;">Student ID</th>';

		foreach($rs_component as $m=>$component){
			if($component['FinalExam']==1){
				$attendance = 1;
			}else{
				$attendance = 0;
			}

			if(count($component['component_item'])>0){
				$colspanx = abs(count($component['component_item']))+abs($attendance);
			}else{
				$colspanx = abs(count($component['component_item']))+abs($attendance);
			}

			$table .= '<th align="center" colspan="'.$colspanx.'">';
			$table .= $component['component_name'];
			$table .= '</th>';
		}

		$table .= '</tr>';
		$table .= '<tr>';
		for($m=0; $m<count($rs_component); $m++){
			for($j=0; $j<count($rs_component[$m]['component_item']); $j++){
				$table .= '<th colspan="1">';
				$table .= $rs_component[$m]['component_item'][$j]['ComponentName'];
				$table .= '</th>';
			}

			if($rs_component[$m]['FinalExam']==1){
				$table .= '<th rowspan="2">Attendance Status</th>';
			}
		}

		$table .= '</tr>';
		$table .= '<tr >';

		foreach($rs_component as $m=>$component){
			foreach($component['component_item'] as $n=>$item){
				$table .= '<th align="center">Raw<br />'.$item['Weightage'].'</th>';
			}
		}

		$table .= '</tr>';

		$i=1;
		$row_bgcolor = '';
		foreach($students as $student){

			if($student['AssessmentMethod']=='point'){
				$textbox = 'text';
			}else{
				$textbox = 'hidden';
			}

			$row_bgcolor = 'style="background-color: #FFFFFF"';

			if ($student['mark_approval_status']==1){
				$row_bgcolor = 'style="background-color: #F5F6CE"';
			}else
				if($student['mark_approval_status']==2){
					$row_bgcolor = 'style="background-color: #D8F6CE"';
				}else
					if($student['mark_approval_status']==3){
						$row_bgcolor = 'style="background-color: #F6CED8"';
					}else{
						$enable_entry = '';
					}

			$table .= '<tr '.$row_bgcolor.'>';
			$table .= '<td align="center" style="vertical-align:middle;">'.$i.'</td>';
			$table .= '<td align="center" style="vertical-align:middle;">';

			if($student['mark_approval_status']==0){
				$table .= 'Entry';
			} else {
				if ($student['mark_approval_status']==1){
					$table .= 'Waiting for approval';
				}else
					if($student['mark_approval_status']==2){
						$table .= 'Approved';
					}else
						if($student['mark_approval_status']==3){
							$table .= 'Rejected';
						}
			}

			$table .= '</td>';
			$table .= '<td align="left" style="vertical-align:middle;">'.$student["appl_fname"].' '.$student["appl_lname"].'</td>';
			$table .= '<td align="center" style="vertical-align:middle;">'.$student["registrationId"].'</td>';

			$j=0;

			foreach($rs_component as $component){
				//get main component info
				$k=0;
				$total_item = count($component['component_item']);
				foreach($component['component_item'] as $item){
					$table .= '<td align="center" style="vertical-align:middle;">';
					$table .= ((isset($student["r".$item['IdMarksDistributionDetails']])) ? $student["r".$item['IdMarksDistributionDetails']]:'');
					$table .= '</td>';
					$k++;
				}

				if($component['FinalExam']==1){
					$table .= '<td align="center" >'.$student['attendance'].'</td>';
				}

				$j++;
			}

			$table .= '</tr>';

			$i++;
		}

		$table .= '</table>';

		$tmpfile = tempnam(sys_get_temp_dir(), 'html');
		file_put_contents($tmpfile, $table);

		// save $table inside temporary file that will be deleted later
		$tmpfile = tempnam(sys_get_temp_dir(), 'html');
		file_put_contents($tmpfile, $table);

		// insert $table into $objPHPExcel's Active Sheet through $excelHTMLReader
		$objPHPExcel     = new PHPExcel();
		$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
		$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
		$objPHPExcel->getActiveSheet()->setTitle('Mark Entry');

		unlink($tmpfile);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); // header for .xlxs file
		header('Content-Disposition: attachment;filename='.$filename); // specify the download file name
		header('Cache-Control: max-age=0');

		// Creates a writer to output the $objPHPExcel's content
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$writer->save('php://output');
		exit;
	}
	    
	    public function viewPrevMarkAction(){
	    	
	    	$this->_helper->layout->disableLayout();
    	    	
	    	$db = Zend_Db_Table::getDefaultAdapter();
	    	
	    	$registry = Zend_Registry::getInstance();
			$this->view->locale = $registry->get('Zend_Locale');
	
			$IdStudentRegSubjects = $this->_getParam('id');
			
			$studentRegSubDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			$subreg_info = $studentRegSubDB->getInfoSubjectReg($IdStudentRegSubjects);
			
			//get info program
	    	$programDB = new GeneralSetup_Model_DbTable_Program();
	    	$this->view->program = $programDB->fngetProgramData($subreg_info['IdProgram']);
    	
			//populate component dulu
			
			//get component
	    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
			$list_component = $markDistributionDB->getListMainComponent($subreg_info['IdSemesterMain'],$subreg_info['IdProgram'],$subreg_info['IdSubject'],$subreg_info['IdCourseTaggingGroup']);	    
			
			$flag = true;
			if(count($list_component)>0){			
			
				$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();
		    	foreach($list_component as $index=>$component){
		    		
				  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
				  	  $list_component[$index]['component_item'] = $component_item;
				  	  
				  	  
				  	  if($component['Status']!=1){
				  	  	 $flag = false;
				  	  }
				  	  
				  	  
				  	  //mark section ----------------
				  	  	$select_main_mark = $db->select()
					 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
					 	 				  ->where('sme.IdStudentRegistration = ?',$subreg_info["IdStudentRegistration"])				 	 			
					 	 				  ->where('sme.IdStudentRegSubjects = ?',$subreg_info["IdStudentRegSubjects"]);
					 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
					 	$entry_main = $db->fetchAll($select_main_mark);	
					 	
					 	foreach($entry_main as $m=>$main){
					 		$subreg_info['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
					 		$subreg_info['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
					 		$subreg_info['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
					 		
					 		//get item
				 			$select_item_mark = $db->select()
					 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
					 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
					 		$entry_item = $db->fetchAll($select_item_mark);	
					 		
					 		foreach($entry_item as $i=>$item){
					 			$subreg_info['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
					 			$subreg_info['r'.$item['ComponentDetail']]=$item['MarksObtained'];
					 			$subreg_info['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
					 		}//end foreach
					 	}//end foreach
				  	  
				  	  //end mark section ------------
		    	}
			}else{
				$flag = false;
			}
	    	$this->view->rs_component = $list_component;
	    	$this->view->flag = $flag;
	    	$this->view->student = $subreg_info;
	    	
	    }
	
	public function getMarkFromMoodleAction() {
		
		$this->_helper->layout->disableLayout();
		
		$idGroup = $this->_getParam('id');
		$idDetail = $this->_getParam('idDetail');
		$j = $this->_getParam('j');
		$m = $this->_getParam('m');
		
		$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    $students = $studentDB->getStudentbyGroupId($idGroup);
		
		
		$mddb = new GeneralSetup_Model_DbTable_Moodle();
		$quiz = $mddb->checkQuizExist($idDetail);
		
		$i = 0;
		$forJSON = array();
		
		foreach($students as $key => $value) {
			
			$user_moodle = $mddb->getUserId($value['registrationId']);
			
			$marks = $mddb->getQuizMark($quiz['id'],$user_moodle['id']);
			
			if($marks)
			{
				$grade = number_format($marks['grade'], 2, '.', '');
				
			}
			else
			{
				$grade = '0.00';
			}
			
			$forJSON[$i]['idDetail'] = $idDetail;
			$forJSON[$i]['idStudent'] = $value['IdStudentRegistration'];
			$forJSON[$i]['registrationId'] = $value['registrationId'];
			$forJSON[$i]['rawMark'] = $grade;
			$forJSON[$i]['j'] = (int)$j;
			$forJSON[$i]['m'] = (int)$m;
			
			$i++;
		}
		
		echo json_encode($forJSON);
		
		exit;
	}

	public function uploadMarkAction(){
		$this->view->title = $this->view->translate('Upload Mark');

		$idGroup = $this->_getParam('id');
		$idSemester = $this->_getParam('idSemester');
		$idProgram = $this->_getParam('idProgram');
		$idSubject = $this->_getParam('idSubject');

		$this->view->idGroup = $idGroup;
		$this->view->idSemester = $idSemester;
		$this->view->idProgram = $idProgram;
		$this->view->idSubject = $idSubject;

		$auth = Zend_Auth::getInstance();

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$dir = APP_DOC_PATH.'/'.$idGroup.'/'.$idSemester.'/'.$idProgram.'/'.$idSubject;

			if ( !is_dir($dir) ){
				if ( mkdir_p($dir) === false )
				{
					throw new Exception('Cannot create attachment folder ('.$dir.')');
				}
			}

			$adapter = new Zend_File_Transfer_Adapter_Http();
			$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 2)); //maybe soon we will allow more?
			//$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
			$adapter->addValidator('Extension', false, array('extension' => 'xls,xlsx' , 'case' => false));
			//$adapter->addValidator('Extension', false, array('xls', 'xlsx'));
			$adapter->addValidator('NotExists', false, $dir);
			$adapter->setDestination($dir);

			$files = $adapter->getFileInfo();

			$status = false;

			if ($files){
				foreach ($files as $file=>$info){
					if ($adapter->isValid($file)){
						$fileOriName = $info['name'];
						$fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
						$filepath = $info['destination'].'/'.$fileRename;

						if ($adapter->isUploaded($file)){
							$adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
							if ($adapter->receive($file)){
								$status = true;
							}
						}else{
							$this->gobjsessionsis->flash = array('message' => 'File not upload', 'type' => 'error');
						}
					}else{
						$this->gobjsessionsis->flash = array('message' => 'File not upload. Invalid file', 'type' => 'error');
					}
				}
			}else{
				$this->gobjsessionsis->flash = array('message' => 'No file to upload', 'type' => 'error');
			}

			if ($status == false){
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry', 'action'=>'upload-mark','id'=>$idGroup, 'idProgram'=>$idProgram, 'idSemester'=>$idSemester, 'idSubject'=>$idSubject),'default',true));
			}

			if (!file_exists($filepath)){
				$this->gobjsessionsis->flash = array('message' => 'File not upload', 'type' => 'error');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry', 'action'=>'upload-mark','id'=>$idGroup, 'idProgram'=>$idProgram, 'idSemester'=>$idSemester, 'idSubject'=>$idSubject),'default',true));
			}

			//get component
			$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
			$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);

			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();
			foreach($list_component as $index=>$component){
				$component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
				$list_component[$index]['component_item'] = $component_item;
			}

			require_once ('PHPExcel/Classes/PHPExcel.php');
			$inputFileName = $filepath;

			//Read your Excel workbook
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$uploadModel = new Examination_Model_DbTable_MarkEntryUpload();

			//Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$examRegDB = new Registration_Model_DbTable_ExamRegistration();
			$all_semester = $examRegDB->getSemester($idSemester);

			$markDB = new Examination_Model_DbTable_StudentMarkEntry();
			$markDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();

			$componentArr = array();
			$rownext = false;
			$filestatus = false;

			//Loop through each row of the worksheet in turn
			for ($row = 1; $row <= $highestRow; $row++){
				//Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

				if ($rowData[0][0] == 'No'){
					if ($list_component){
						foreach ($list_component as $list_component_loop){
							if (!in_array($list_component_loop['component_name'], $rowData[0])){
								unlink($filepath);
								$this->gobjsessionsis->flash = array('message' => 'Invalid file format 1', 'type' => 'error');
								$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry', 'action'=>'upload-mark','id'=>$idGroup, 'idProgram'=>$idProgram, 'idSemester'=>$idSemester, 'idSubject'=>$idSubject),'default',true));
							}
						}
					}

					$rownext = $row+1;
					$filestatus = true;
				}else{
					$filestatus = false;
				}

				if ($rownext == $row && $rownext != false){
					if ($list_component){
						foreach ($list_component as $list_component_loop){
							if (isset($list_component_loop['component_item']) && count($list_component_loop['component_item']) > 0) {
								foreach ($list_component_loop['component_item'] as $component_item) {
									if (!in_array($component_item['ComponentName'], $rowData[0])) {
										unlink($filepath);
										$this->gobjsessionsis->flash = array('message' => 'Invalid file format 2', 'type' => 'error');
										$this->_redirect($this->view->url(array('module' => 'examination', 'controller' => 'mark-entry', 'action' => 'upload-mark', 'id' => $idGroup, 'idProgram' => $idProgram, 'idSemester' => $idSemester, 'idSubject' => $idSubject), 'default', true));
									}

									//echo $component_item['ComponentName'].' : '.array_search($component_item['ComponentName'],$rowData[0]).'<br />';
									$componentArr[$list_component_loop['component_name']][$component_item['ComponentName']] = array_search($component_item['ComponentName'],$rowData[0]);
								}
							}else{
								unlink($filepath);
								$this->gobjsessionsis->flash = array('message' => 'Invalid file format 3', 'type' => 'error');
								$this->_redirect($this->view->url(array('module' => 'examination', 'controller' => 'mark-entry', 'action' => 'upload-mark', 'id' => $idGroup, 'idProgram' => $idProgram, 'idSemester' => $idSemester, 'idSubject' => $idSubject), 'default', true));
							}
						}
					}

					$filestatus = true;
				}else{
					$filestatus = false;
				}

				if (count($componentArr) > 0) {
					if ($rowData[0][0] >= 1) {
						$studentInfo = $uploadModel->getStudentInfo($rowData[0][3], $idGroup);

						if ($studentInfo) {
							$regSubjectInfo = $uploadModel->getStudentRegSubject($studentInfo['IdStudentRegistration'], $idGroup);

							if ($regSubjectInfo['mark_approval_status'] == 0) {

								$grandTotal = 0.00;
								if ($list_component) {
									foreach ($list_component as $list_component_loop) {
										$examStatus = true;

										if ($list_component_loop["IdComponentType"] == 51) {
											$examInfo = $uploadModel->getExamRegistration($all_semester, $studentInfo['IdStudentRegistration'], $idSubject);

											if ($examInfo['er_attendance_status'] != 395) {
												$examStatus = false;
											}
										}

										if ($examStatus == true) {

											$data = array();
											$dataItem = array();
											$TotalItemRaw = 0.00;
											$TotalItemPercent = 0.00;
											if (isset($list_component_loop['component_item']) && count($list_component_loop['component_item']) > 0) {
												foreach ($list_component_loop['component_item'] as $component_item_key => $component_item) {
													$MarksObtained = $rowData[0][$componentArr[$list_component_loop['component_name']][$component_item['ComponentName']]];
													$MarksObtained = $MarksObtained == '' ? 0.00 : $MarksObtained;

													if ($MarksObtained > $component_item['Weightage']){
														unlink($filepath);
														$this->gobjsessionsis->flash = array('message' => 'Invalid mark', 'type' => 'error');
														$this->_redirect($this->view->url(array('module' => 'examination', 'controller' => 'mark-entry', 'action' => 'upload-mark', 'id' => $idGroup, 'idProgram' => $idProgram, 'idSemester' => $idSemester, 'idSubject' => $idSubject), 'default', true));
													}

													$FinalMarksObtained = round((($MarksObtained / $component_item['Weightage']) * $component_item['Percentage']), 2);

													$TotalItemRaw = $TotalItemRaw + $MarksObtained;
													$TotalItemPercent = $TotalItemPercent + $FinalMarksObtained;

													$dataItem[$component_item['IdMarksDistributionDetails']]["MarksObtained"] = $MarksObtained;
													$dataItem[$component_item['IdMarksDistributionDetails']]["FinalMarksObtained"] = $FinalMarksObtained;
													$dataItem[$component_item['IdMarksDistributionDetails']]["UpdUser"] = $auth->getIdentity()->iduser;
													$dataItem[$component_item['IdMarksDistributionDetails']]["UpdDate"] = date('Y-m-d H:i:s');
													$dataItem[$component_item['IdMarksDistributionDetails']]["Component"] = $list_component_loop["IdComponentType"];
													//$dataItem[$component_item['IdMarksDistributionDetails']]["ComponentItem"] = '';
													$dataItem[$component_item['IdMarksDistributionDetails']]["ComponentDetail"] = $component_item['IdMarksDistributionDetails'];
													$dataItem[$component_item['IdMarksDistributionDetails']]["TotalMarks"] = $component_item["Weightage"];
												}

												$grandTotal = $grandTotal+$TotalItemPercent;

												$data["TotalMarkObtained"] = $TotalItemRaw; //Total Raw Mark each item++
												$data["FinalTotalMarkObtained"] = $TotalItemPercent; //afrer calculation made
												$data["UpdUser"] = $auth->getIdentity()->iduser;
												$data["UpdDate"] = date('Y-m-d H:i:s');
												$data["IdStudentRegistration"] = $studentInfo['IdStudentRegistration'];
												$data["IdStudentRegSubjects"] = $regSubjectInfo['IdStudentRegSubjects'];
												$data["IdSemester"] = $idSemester;
												$data["Course"] = $idSubject;
												$data["IdMarksDistributionMaster"] = $list_component_loop['IdMarksDistributionMaster'];
												$data["MarksTotal"] = $list_component_loop["Marks"];
												$data["Component"] = $list_component_loop["IdComponentType"];
												//$data["ComponentItem"] = '';
												//$data["Instructor"] = '';
												//$data["AttendanceStatus"] = '';
												$data["MarksEntryStatus"] = 0; //entry

												$checkMarkEntry = $uploadModel->getMarkEntry($idSubject, $idSemester, $studentInfo['IdStudentRegistration'], $list_component_loop['IdComponentType']);

												if ($checkMarkEntry){ //update
													$markDB->updateData($data, $checkMarkEntry['IdStudentMarksEntry']);

													if ($dataItem){
														foreach ($dataItem as $dataItemLoop){
															$checkMarkEntryDetail = $uploadModel->getMarkEntryDetail($checkMarkEntry['IdStudentMarksEntry'], $dataItemLoop['ComponentDetail']);

															if ($checkMarkEntryDetail){ //update
																$IdStudentMarksEntryDetail = $checkMarkEntryDetail['IdStudentMarksEntryDetail'];
																$markDetailsDB->updateData($dataItemLoop, $IdStudentMarksEntryDetail);
															}else{ //insert
																$IdStudentMarksEntryDetail  = $markDetailsDB->addData($dataItemLoop);
															}
														}
													}
												}else{ //insert
													$IdStudentMarksEntry = $markDB->addData($data);

													if ($dataItem){
														foreach ($dataItem as $dataItemLoop){
															$dataItemLoop["IdStudentMarksEntry"] = $IdStudentMarksEntry;

															$IdStudentMarksEntryDetail  = $markDetailsDB->addData($dataItemLoop);
														}
													}
												}
											}
										}
									}
								}

								$grade = $markDB->saveStudentSubjectMark($regSubjectInfo['IdStudentRegSubjects'], $idSemester, $idProgram, $idSubject, $grandTotal);
							}
						}

						$filestatus = true;
					}else{
						$filestatus = false;
					}
				}else{
					$filestatus = false;
				}
			}

			if ($filestatus == false){
				unlink($filepath);
				$this->gobjsessionsis->flash = array('message' => 'Invalid file format 4', 'type' => 'error');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry', 'action'=>'upload-mark','id'=>$idGroup, 'idProgram'=>$idProgram, 'idSemester'=>$idSemester, 'idSubject'=>$idSubject),'default',true));
			}

			unlink($filepath);

			$this->gobjsessionsis->flash = array('message' => 'Mark successfully uploaded', 'type' => 'success');
			$this->_redirect($this->view->url(array('module' => 'examination', 'controller' => 'mark-entry', 'action' => 'student-list', 'id' => $idGroup, 'idProgram' => $idProgram, 'idSemester' => $idSemester, 'idSubject' => $idSubject), 'default', true));
		}
	}
}