<?php
class examination_GradesetupController extends Base_Base { //Controller for the User Module
	private $lobjGradesetupForm;
	private $lobjGradesetup;
	private $lobjdeftype;
	private $_gobjlog;
	private $lobjschemesetupmodel;
	private $Awardlevel;
	private $lobjStudentRegModel;
	private $lobjprogram;
	private $lobjsemesterModel;

	public function init() { //initialization function

		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjGradesetupForm = new Examination_Form_Gradesetup();
		$this->lobjGradesetup = new Examination_Model_DbTable_Gradesetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->Awardlevel = new GeneralSetup_Model_DbTable_Awardlevel();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();

	}

	public function indexAction() { // action for search and view
		
		$this->view->title = $this->view->translate('Grade Setup');
		
		$lobjform = new App_Form_Search(); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view


		//get intake
		$intakeDB = new GeneralSetup_Model_DbTable_Intake();
		$intake_list = $intakeDB->fngetIntakeList();
		$this->view->lobjform->IdIntake->addMultiOption('','All');
		foreach($intake_list as $intake) {
			$this->view->lobjform->IdIntake->addMultiOption($intake['IdIntake'],$intake['IntakeDesc']);			
		}
		
		$larrschemelist = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display scheme
		foreach($larrschemelist as $larrvalues) {
			$this->view->lobjform->field5->addMultiOption($larrvalues['IdScheme'],$larrvalues['EnglishDescription']);
		}
			
		$larrawardresult = $this->Awardlevel->fnGetDefinations('Award');//function to display award
		$this->view->lobjform->field19->addMultiOption('','All');
		foreach($larrawardresult as $larrawardvalues) {
			$this->view->lobjform->field19->addMultiOption($larrawardvalues['idDefinition'],$larrawardvalues['DefinitionDesc']);
		}
			
		$larrsemresult =  $this->lobjsemesterModel->fnSemesterList();
		$this->view->lobjform->field1->addMultiOption('','All');
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field1->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}
			
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		$this->view->lobjform->field20->addMultiOption('','All');
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field20->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
		}
			
		// SHOW THE COURSES
		$larrresultCourses = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field8->addMultiOption('','All');
		foreach($larrresultCourses as $course) {
			$this->view->lobjform->field8->addMultiOption($course['key'],$course['value']);
		}
			
		$larrresult = $this->lobjGradesetup ->fnSearchGradeSetup( $post=null);

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Academicstatuspaginatorresult);
		
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		//$this->view->paginator = $lobjPaginator->fnPagination($larrresultgrade,$lintpage,$lintpagecount);


		if(isset($this->gobjsessionsis->Academicstatuspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Academicstatuspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjGradesetup ->fnSearchGradeSetup( $lobjform->getValues ()); //searching the values for the user
				//print_r($larrresult);die();
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Academicstatuspaginatorresult = $larrresult;
			}
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/gradesetup/index');
		}
	}

	public function newgradesetupAction() { //Action for creating the new user
		$this->view->lobjGradesetupForm = $this->lobjGradesetupForm;
		$this->view->IdProgram = $idProgram = ( int ) $this->_getParam ( 'id' );
		$this->view->ProgramName  = ( string ) $this->_getParam ( 'name' );
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjGradesetupForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjGradesetupForm->IdProgram->setValue ( $idProgram );
		$lobjProgramName = $this->lobjGradesetup->fnGetProgramName($idProgram);

		$this->view->ProgramName=$lobjProgramName['ProgramName'];
		//$this->view->lobjGradesetupForm->IdProgram->setAttrib ('readonly','true');

		$this->view->lobjGradesetupForm->CopyFromIdProgram->setValue ( $idProgram );
		//$this->view->lobjGradesetupForm->CopyFromIdProgram->setAttrib ('readonly','true');

		$auth = Zend_Auth::getInstance();
		$this->view->lobjGradesetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$larrresult = $this->lobjGradesetup ->fnSearchGradeSetUpDetails($idProgram);

		$this->view->larrresult=$larrresult;
		//print_r($larrresult);die();

		$linIdGradeSetUp = ( int ) $this->_getParam ( 'IdGradeSetUp' );
		//echo "<pre>";
		//print_r($larrresult);die();
		if(!$linIdGradeSetUp) {
			foreach($larrresult as $larrresultDtls){
				$this->view->lobjGradesetupForm->MinPoint->setValue ( $larrresultDtls['MaxPoint']+1);
				//$this->view->lobjGradesetupForm->MinPoint->setAttrib ('readonly','true');
			}
		}

		if($linIdGradeSetUp){
			$this->view->IdAcademicStatus = $linIdGradeSetUp;
			$this->view->lobjGradesetupForm->IdGradeSetUp->setValue ($linIdGradeSetUp);
			$larrViewresult = $this->lobjGradesetup->fnviewGradeSetUp($linIdGradeSetUp);
			$lobjProgramNameList = $this->lobjGradesetup->fnGetProgramNameList();
			//$this->lobjGradesetupForm->IdProgram->addMultiOptions($lobjProgramNameList);


			$lobjSemesterNameList = $this->lobjGradesetup->fnGetSemesterNameList();
			$this->lobjGradesetupForm->IdSemester->addMultiOptions($lobjSemesterNameList);
			$this->lobjGradesetupForm->populate($larrViewresult);
		}
			


		$lobjProgramNameList = $this->lobjGradesetup->fnGetProgramNameList();
		//$this->lobjGradesetupForm->IdProgram->addMultiOptions($lobjProgramNameList);
		$this->lobjGradesetupForm->CopyFromIdProgram->addMultiOptions($lobjProgramNameList);
		$this->lobjGradesetupForm->CopyToIdProgram->addMultiOptions($lobjProgramNameList);
		//$this->lobjGradesetupForm->DescArabicName->addMultiOptions($larrdefmsStatusEnglishNamedtls['idDefinition'],$larrdefmsStatusEnglishNamedtls['DefinitionDesc']);
		//$this->lobjGradesetupForm->DescEnglishName->addMultiOptions($lobjProgramNameList);
		$larrdefmsStatusEnglishName = $this->lobjdeftype->fnGetDefinations('StatusEnglishName');

		foreach($larrdefmsStatusEnglishName as $larrdefmsStatusEnglishNamedtls) {
			$this->lobjGradesetupForm->DescEnglishName->addMultiOption($larrdefmsStatusEnglishNamedtls['idDefinition'],$larrdefmsStatusEnglishNamedtls['DefinitionDesc']);
			//$this->lobjGradesetupForm->CopyFromDescEnglishName->addMultiOption($larrdefmsStatusEnglishNamedtls['idDefinition'],$larrdefmsStatusEnglishNamedtls['DefinitionDesc']);
			//$this->lobjGradesetupForm->CopyToDescEnglishName->addMultiOption($larrdefmsStatusEnglishNamedtls['idDefinition'],$larrdefmsStatusEnglishNamedtls['DefinitionDesc']);
		}
		foreach($larrdefmsStatusEnglishName as $larrdefmsStatusArabicNamedtls) {
			$this->lobjGradesetupForm->DescArabicName->addMultiOption($larrdefmsStatusArabicNamedtls['idDefinition'],$larrdefmsStatusArabicNamedtls['Description']);
		}

		$lobjGradeValue= $this->lobjGradesetup->fnGetGrade();
		$this->lobjGradesetupForm->Grade->addMultiOptions($lobjGradeValue);

		//---------------



		//--------------



		$lobjSemesterNameList = $this->lobjGradesetup->fnGetSemesterNameList();
		$this->lobjGradesetupForm->IdSemester->addMultiOptions($lobjSemesterNameList);
		$this->lobjGradesetupForm->CopyFromIdSemester->addMultiOptions($lobjSemesterNameList);
		$this->lobjGradesetupForm->CopyToIdSemester->addMultiOptions($lobjSemesterNameList);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
			unset ( $larrformData ['Save'] );
			
			if ($this->lobjGradesetupForm->isValid ( $larrformData )) {
				
				if($larrformData ['BasedOn'] == 1){
					unset($larrformData['IdGradeSetUp']);

					$result = $this->lobjGradesetup->fnAddSubjectGradeSetUp($larrformData);
				}else{
					
					$result = $this->lobjGradesetup->fnGetCourseList($larrformData['IdProgram']);
					
					$resultsub = $this->lobjGradesetup->fnGetCourseListsub($larrformData['IdProgram']);
					
					$arrayresult=array_merge($result,$resultsub);
					
					$arrayresultsub = array_intersect_key($arrayresult, array_unique(array_map('serialize', $arrayresult)));
					
					unset($larrformData['IdGradeSetUp']);
					
					$result = $this->lobjGradesetup->fnAddProgramGradeSetUp($larrformData,$arrayresultsub);
				}
					
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'New Grade Setup Add Id=' . $larrformData['IdProgram'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/gradesetup/newgradesetup/id/'.$larrformData['IdProgram']);
			}
		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'CopySetup' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//echo "<PRE>";
			//print_r($larrformData);die();

			unset ( $larrformData ['Save'] );
			
			if ($this->lobjGradesetupForm->isValid ( $larrformData )) {
				
				$larrresult = $this->lobjGradesetup ->fnCopySearchGradeSetUpDetails($larrformData['CopyFromIdProgram'],$larrformData['CopyFromIdSemester']);
				
				foreach($larrresult as $larrresultUpdate){
					for($i=0;$i<count($larrresultUpdate['IdSemester']);$i++){
						$this->lobjGradesetup ->fnCopyAddGradeSetUpDetails($larrformData['CopyToIdProgram'],$larrformData['CopyToIdSemester'],$larrresultUpdate);
					}
				}

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Copy Grade Setup Add Id=' . $larrformData['CopyFromIdProgram'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				$this->_redirect( $this->baseUrl . '/examination/gradesetup/newgradesetup/id/'.$idProgram);
			}
		}


	}
	public function deletegradesetupAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Po details Id
		$IdGradeSetUp = $this->_getParam('IdGradeSetUp');
		$larrDelete = $this->lobjGradesetup->fndeletegradesetup($IdGradeSetUp);
		echo "1";
	}
	public function deleteallgradesetupAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdGradeSetUpMain = $this->_getParam('IdGradeSetUpMain');
		$larrDelete = $this->lobjGradesetup->fndeleteallgradesetup($IdGradeSetUpMain);
		echo "1";
	}


	public function getprogramsubjectlistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$lobjCommonModel = new App_Model_Common();

		$lintvalue= $this->_getParam('value');

		if($lintvalue == '0') {
			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjGradesetup->fnGetSubProgramList());
		}else{

			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjGradesetup->fnGetSubjectNameList());
		}
		echo Zend_Json_Encoder::encode($larrDetails);
	}
	public function academicstatuslistAction() { //Action for the updation and view of the  details

		$this->view->lobjGradesetupForm = $this->lobjGradesetupForm;
			
		$lintIdAcademicStatus = ( int ) $this->_getParam ( 'id' );
		$this->view->IdAcademicStatus = $lintIdAcademicStatus;


		$larrresult = $this->lobjGradesetup->fnviewAcademicStatus($lintIdAcademicStatus);

		$lobjProgramNameList = $this->lobjGradesetup->fnGetProgramNameList();
		$this->lobjGradesetupForm->IdProgram->addMultiOptions($lobjProgramNameList);

		$lobjSemesterNameList = $this->lobjGradesetup->fnGetSemesterNameList();
		$this->lobjGradesetupForm->IdSemester->addMultiOptions($lobjSemesterNameList);


		$this->lobjGradesetupForm->populate($larrresult);

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjGradesetupForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjGradesetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjGradesetupForm->isValid ( $larrformData )) {

					$lintiIdCharges = $larrformData ['IdCharges'];
					$this->lobjGradesetup->fnupdateCharges($lintiIdCharges, $larrformData );
					//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
					$this->_redirect( $this->baseUrl . '/examination/academicstatus/index');
				}
			}
		}
		$this->view->lobjGradesetupForm = $this->lobjGradesetupForm;
	}


	/**
	 * Function to ADD new GradeSetup
	 * @author: VT
	 */

	public function addgradesetupAction() {
		$this->view->title = $this->view->translate('Add Grade');
			
		$this->view->lobjGradesetupForm = $this->lobjGradesetupForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->lobjGradesetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->lobjGradesetupForm->UpdDate->setValue ( $ldtsystemDate );
			
		// get all grade desc for unique in DB
		//$this->view->gradereqresult = $this->lobjGradesetup->fngetAllgradeDesc();
			
		//get intake
		$intakeDB = new GeneralSetup_Model_DbTable_Intake();
		$intake_list = $intakeDB->fngetIntakeList();
		foreach($intake_list as $intake) {
			$this->lobjGradesetupForm->IdIntake->addMultiOption($intake['IdIntake'],$intake['IntakeDesc']);			
		}
		
		$larrschemelist = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display scheme
		foreach($larrschemelist as $larrvalues) {
			$this->lobjGradesetupForm->IdScheme->addMultiOption($larrvalues['IdScheme'],$larrvalues['EnglishDescription']);			
		}
			
		$larrawardresult = $this->Awardlevel->fnGetDefinations('Award');//function to display award
		foreach($larrawardresult as $larrawardvalues) {
			$this->lobjGradesetupForm->IdAward->addMultiOption($larrawardvalues['idDefinition'],$larrawardvalues['DefinitionDesc']);			
		}
			
		$larrsemresult =  $this->lobjsemesterModel->getListSemester();
		
		foreach($larrsemresult as $larrsemvalues) {
			$this->lobjGradesetupForm->IdSemester->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}
			
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->lobjGradesetupForm->IdProgram->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);			
		}
			
		$larrAllsubjDetails =$this->lobjGradesetup->fnGetSubjectNameList();
		foreach($larrAllsubjDetails as $larrsubjvalues) {
			$this->lobjGradesetupForm->IdSubject->addMultiOption($larrsubjvalues['key'],$larrsubjvalues['value']);
		}
			
		$lobjGradeValue= $this->lobjGradesetup->fnGetGrade();
		$this->lobjGradesetupForm->Grade->addMultiOptions($lobjGradeValue);
			
			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
			unset ( $larrformData ['Save'] );
			
			
			if ($this->lobjGradesetupForm->isValid ( $larrformData )) {

				unset($larrformData['IdGradeSetUp']);

				$BasedOn    = $larrformData['BasedOn'];
				$IdIntake = $larrformData['IdIntake'];
				$IdSemester = $larrformData['IdSemester'];
				$IdScheme   = $larrformData['IdScheme'];
				$IdAward    = $larrformData['IdAward'];
				$IdProgram  = $larrformData['IdProgram'];
				$IdSubject  = $larrformData['IdSubject'];

				// check for Duplicacy
				$resultDuplicate = $this->lobjGradesetup->fncheckDuplicate($BasedOn,$IdProgram,$IdSubject,$IdScheme,$IdAward,$IdSemester,$IdIntake);
               
				if(count($resultDuplicate)=='0') {

					$this->lobjGradesetup->fnAddSubjectGradeSetUp($larrformData);
						
					// Write Logs
					$priority=Zend_Log::INFO;
					$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
							'level' => $priority,
							'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
							'time' => date ( 'Y-m-d H:i:s' ),
							'message' => 'New Grade Setup Add',
							'Description' =>  Zend_Log::DEBUG,
							'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
					$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
					
					$this->gobjsessionsis->flash = array('message' => 'Information saved', 'type' => 'success');
					$this->_redirect( $this->baseUrl . '/examination/gradesetup/index');
				} else {
					$this->gobjsessionsis->flash = array('message' => 'Record already exist', 'type' => 'error');					
					$this->view->larrformDataM = $larrformData;
					$this->lobjGradesetupForm->populate($larrformData);
				}//end if
			}
		}


		

			


	}

	/**
	 * Function to EDIT new GradeSetup
	 * @author: VT
	 */

	public function editgradesetupAction() {
		
		$this->view->title = $this->view->translate('Edit Grade');
		
		$this->view->lobjGradesetupForm = $this->lobjGradesetupForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjGradesetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjGradesetupForm->UpdDate->setValue ( $ldtsystemDate );
			
			
		$lintIdGrade = ( int ) $this->_getParam ( 'id' );
		$this->view->lintIdGrade = $lintIdGrade ;
		$larrGradeDetails= $this->lobjGradesetup->fnviewGradeSetUpMain($lintIdGrade);
			
		// get all grade desc for unique in DB
		$this->view->gradereqresult = $this->lobjGradesetup->fngetAllgradeDesc($lintIdGrade);
		
		//get intake
		$intakeDB = new GeneralSetup_Model_DbTable_Intake();
		$intake_list = $intakeDB->fngetIntakeList();
		foreach($intake_list as $intake) {
			$this->lobjGradesetupForm->IdIntake->addMultiOption($intake['IdIntake'],$intake['IntakeDesc']);			
		}
			
		$larrschemelist = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display scheme
		foreach($larrschemelist as $larrvalues) {
			$this->lobjGradesetupForm->IdScheme->addMultiOption($larrvalues['IdScheme'],$larrvalues['EnglishDescription']);
		}
			
		$larrawardresult = $this->Awardlevel->fnGetDefinations('Award');//function to display award
		foreach($larrawardresult as $larrawardvalues) {
			$this->lobjGradesetupForm->IdAward->addMultiOption($larrawardvalues['idDefinition'],$larrawardvalues['DefinitionDesc']);
		}
			
		$larrsemresult =  $this->lobjsemesterModel->getListSemester();
		// convert double dimession array into single
		$array_double_tosingle  = array_unique($this->flatten_array($larrsemresult));
		// ends

			
		foreach($larrsemresult as $larrsemvalues) {
			$this->lobjGradesetupForm->IdSemester->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}
			
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->lobjGradesetupForm->IdProgram->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
		}
			
	    $larrAllsubjDetails =$this->lobjGradesetup->fnGetSubjectNameList();
		foreach($larrAllsubjDetails as $larrsubjvalues) {
			$this->lobjGradesetupForm->IdSubject->addMultiOption($larrsubjvalues['key'],$larrsubjvalues['value']);
		}
		
		$this->view->idsem = $this->view->idsch = $this->view->idawrd = $this->view->idprg = $this->view->idsub = NULL;
		if(count($larrGradeDetails)>0) {
			$this->view->larrGradeDetails = $larrGradeDetails;

			$array2 = array($larrGradeDetails['IdSemester']);
			$getFinalArr = array_intersect($array_double_tosingle,$array2);

			$this->view->lobjGradesetupForm->BasedOn->setValue ( $larrGradeDetails['BasedOn'] );
			
			if($larrGradeDetails['IdIntake']!='') {
				$this->view->idintake = $larrGradeDetails['IdIntake'];
			}
			
			if($larrGradeDetails['IdSemester']!='') {
				if(count($getFinalArr)>0) {
					$this->view->idsem = $larrGradeDetails['IdSemester'];
				}
				else {  $this->view->idsem = '';
				}

			}
			if($larrGradeDetails['IdScheme']!='' && $larrGradeDetails['IdScheme']!='0') {
				$this->view->idsch = $larrGradeDetails['IdScheme'];
			}
			if($larrGradeDetails['IdAward']!='' && $larrGradeDetails['IdAward']!='0') {
				$this->view->idawrd = $larrGradeDetails['IdAward'];
			}
			if($larrGradeDetails['IdProgram']!='' && $larrGradeDetails['IdProgram']!='0') {
				$this->view->idprg = $larrGradeDetails['IdProgram'];
			}
			if($larrGradeDetails['IdSubject']!='' && $larrGradeDetails['IdSubject']!='0') {
				$this->view->idsub = $larrGradeDetails['IdSubject'];
			}

		
			$larrGradeList = $this->lobjGradesetup->fnViewGradeList($lintIdGrade);
			$this->view->larrresult = $larrGradeList;

		}
			
		$lobjGradeValue= $this->lobjGradesetup->fnGetGrade();
		$this->lobjGradesetupForm->Grade->addMultiOptions($lobjGradeValue);

			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
				
			//echo '<pre>';
			//echo $lintIdGrade;
			//print_r($larrformData);		

			
			if ($this->lobjGradesetupForm->isValid ( $larrformData )) {
				
				$this->lobjGradesetup->fnUpdateSubjectGradeSetUp($larrformData,$lintIdGrade);
					
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Grade Setup Edit Id=' . $lintIdGrade,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				
				$this->gobjsessionsis->flash = array('message' => 'Date has been saved', 'type' => 'success');
				$this->_redirect( $this->baseUrl . '/examination/gradesetup/editgradesetup/id/'.$lintIdGrade);
				

			}
		}

	}


	public function getprogramlistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$lintIdScheme= $this->_getParam('IdScheme');
		$lintIdAward= $this->_getParam('IdAward');
		$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjGradesetup->fnListProgram($lintIdScheme,$lintIdAward));
		echo Zend_Json_Encoder::encode($larrDetails);
	}


	public function getsubjectlistAction(){
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
		
		$lintIdprogram= $this->_getParam('Idprogram');
		$IdSemester= $this->_getParam('IdSemester');
		
		//$lobjCommonModel = new App_Model_Common();		
		//$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjGradesetup->fnListSubject($lintIdprogram));
		//print_r($larrDetails);
		//exit;
		
		//Subject list base on Program Landscape from the selected faculty 
		
		$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		
		$i=0;
		$j=0;
		$allsemlandscape =null;
		$allblocklandscape = null;
		$subjects = array();
		
		$activeLandscape=$landscapeDB->getAllActiveLandscape($lintIdprogram);
		
			foreach($activeLandscape as $actl){
				if($actl["LandscapeType"]==43){
					$allsemlandscape[$i] = $actl["IdLandscape"];						
					$i++;
				}elseif($actl["LandscapeType"]==44){
					$allblocklandscape[$j] = $actl["IdLandscape"];
					$j++;
				}
			}
		
		if(is_array($allsemlandscape)){
			$subjectsem=$landsubDB->getAllLandscapeCourse($allsemlandscape,null,$IdSemester);
		}
			
		if(is_array($allblocklandscape)){
			//$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,null,$IdSemester);
			$subjectblock = array();
		}
		
		if(is_array($allsemlandscape) && is_array($allblocklandscape)){
			$subjects=array_merge( $subjectsem , $subjectblock );
		}else{
			if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
				$subjects=$subjectsem;
			}
			elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=$subjectblock;
			}		
		}		
		
		
	   
	    if(count($subjects)>0){

	    	$i=0;
	    	foreach($subjects as $subject){
				$data[$i]["key"]=utf8_encode($subject["IdSubject"]);
				$data[$i]["name"]=utf8_encode($subject["SubCode"]).' - '.utf8_encode($subject["SubjectName"]);
				
			$i++;
			}
			
	    }else{
	    	$data= array();
	    }
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();
        
		echo $data_json = Zend_Json_Encoder::encode($data);
		
		exit();		
		
	}

	private function flatten_array($mArray) {
		$sArray = array();

		foreach ($mArray as $row) {
			if (!(is_array($row))) {
				if ($sArray[] = $row) {

				}
			} else {
				$sArray = array_merge($sArray, self::flatten_array($row));
			}
		}
		return $sArray;
	}
	
	function copyGradeAction(){
		
		$this->view->title = $this->view->translate('Copy Grade');
			
		$this->view->lobjGradesetupForm = $this->lobjGradesetupForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->lobjGradesetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->lobjGradesetupForm->UpdDate->setValue ( $ldtsystemDate );
	
		$larrawardresult = $this->Awardlevel->fnGetDefinations('Award');//function to display award
		foreach($larrawardresult as $larrawardvalues) {
			$this->lobjGradesetupForm->IdAward->addMultiOption($larrawardvalues['idDefinition'],$larrawardvalues['DefinitionDesc']);
			$this->lobjGradesetupForm->CopyFromIdAward->addMultiOption($larrawardvalues['idDefinition'],$larrawardvalues['DefinitionDesc']);
			$this->lobjGradesetupForm->CopyToIdAward->addMultiOption($larrawardvalues['idDefinition'],$larrawardvalues['DefinitionDesc']);
		}
			
		//get intake
		$intakeDB = new GeneralSetup_Model_DbTable_Intake();
		$intake_list = $intakeDB->fngetIntakeList();
		foreach($intake_list as $intake) {
			$this->lobjGradesetupForm->CopyFromIdIntake->addMultiOption($intake['IdIntake'],$intake['IntakeDesc']);	
			$this->lobjGradesetupForm->CopyToIdIntake->addMultiOption($intake['IdIntake'],$intake['IntakeDesc']);			
		}
		
		//$larrsemresult =  $this->lobjsemesterModel->getAllsemesterListCode();
		$larrsemresult =  $this->lobjsemesterModel->getListSemester();
		
		foreach($larrsemresult as $larrsemvalues) {
			$this->lobjGradesetupForm->IdSemester->addMultiOption($larrsemvalues['key'],$larrsemvalues['value'].'-'.$larrsemvalues['code']);
			$this->lobjGradesetupForm->CopyFromIdSemester->addMultiOption($larrsemvalues['key'],$larrsemvalues['value'].'-'.$larrsemvalues['code']);
			$this->lobjGradesetupForm->CopyToIdSemester->addMultiOption($larrsemvalues['key'],$larrsemvalues['value'].'-'.$larrsemvalues['code']);
		}
			
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->lobjGradesetupForm->IdProgram->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
			$this->lobjGradesetupForm->CopyFromIdProgram->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
			$this->lobjGradesetupForm->CopyToIdProgram->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
		}
			
		$larrAllsubjDetails =$this->lobjGradesetup->fnGetSubjectNameList();
		foreach($larrAllsubjDetails as $larrsubjvalues) {
			$this->lobjGradesetupForm->IdSubject->addMultiOption($larrsubjvalues['key'],$larrsubjvalues['value']);
			$this->lobjGradesetupForm->CopyFromIdSubject->addMultiOption($larrsubjvalues['key'],$larrsubjvalues['value']);
			$this->lobjGradesetupForm->CopyToIdSubject->addMultiOption($larrsubjvalues['key'],$larrsubjvalues['value']);
		}
		
		
		
		// COPY GRADE SETUP
		if ($this->_request->isPost () && $this->_request->getPost ( 'CopySetup' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['CopySetup'] );

			$CopyBasedOn = $larrformData['CopyBasedOn'];
			$CopyFromIdSemester = $larrformData['CopyFromIdSemester'];
			$CopyFromIdIntake = $larrformData['CopyFromIdIntake'];
			//$CopyFromIdScheme = $larrformData['CopyFromIdScheme'];
			$CopyFromIdAward = $larrformData['CopyFromIdAward'];
			$CopyFromIdProgram = $larrformData['CopyFromIdProgram'];
			$CopyFromIdSubject = $larrformData['CopyFromIdSubject'];

			// check whether data is available or not
			$resultAvailabale = $this->lobjGradesetup->fncheckDuplicate($CopyBasedOn,$CopyFromIdProgram,$CopyFromIdSubject,0,$CopyFromIdAward,$CopyFromIdSemester,$CopyFromIdIntake);
			
			if(count($resultAvailabale)>0) {
					
				$CopyToIdIntake = $larrformData['CopyToIdIntake'];
				$CopyToIdSemester = $larrformData['CopyToIdSemester'];
				//$CopyToIdScheme = $larrformData['CopyToIdScheme'];
				$CopyToIdAward = $larrformData['CopyToIdAward'];
				$CopyToIdProgram = $larrformData['CopyToIdProgram'];
				$CopyToIdSubject = $larrformData['CopyToIdSubject'];

				// check for Duplicacy
				$resultDuplicate = $this->lobjGradesetup->fncheckDuplicate($CopyBasedOn,$CopyToIdProgram,$CopyToIdSubject,0,$CopyToIdAward,$CopyToIdSemester,$CopyToIdIntake);

				if(count($resultDuplicate)=='0') {

					$mainID = $resultAvailabale[0]['IdGradeSetUpMain'];
					$finalresult = $this->lobjGradesetup->fnViewGradeList($mainID);
					
					if(count($finalresult)>0) {
						$finalresultAdd = $this->lobjGradesetup->fnCopyGradelist($finalresult,$larrformData,$userId);
						
						$this->gobjsessionsis->flash = array('message' => 'Grade has been successfully copied', 'type' => 'success');
						$this->_redirect( $this->baseUrl . '/examination/gradesetup/index');
					} else { 
						$this->gobjsessionsis->flash = array('message' => 'No grade available', 'type' => 'error');						
					}
					
				} else { 
					$this->gobjsessionsis->flash = array('message' => 'Record already exist', 'type' => 'error');
					
				}
				
			} else{
				$this->gobjsessionsis->flash = array('message' => 'Record does not exist', 'type' => 'error');
				
			}

			$this->view->larrformData = $larrformData;
			$this->lobjGradesetupForm->populate($larrformData);

		}
		
	}

}