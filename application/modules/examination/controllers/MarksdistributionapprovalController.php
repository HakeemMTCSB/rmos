<?php

class examination_MarksdistributionapprovalController extends Base_Base { //Controller for the User Module

	private $lobjMarksdistributionmasterForm;
	private $lobjSubjectmaster;
	private $lobjmarksentrysetup;
	private $lobjAcademicstatus;
	private $lobjmarksdistributionmaster;
	private $lobjSchemeModel;
	private $lobjprogramModel;
	private $lobjsemesterModel;
	private $lobjsubjectmasterModel;
	private $lobjmarksdetailForm;
	private $lobjmarksditrbutiondetail;
	private $lobjmarksHistoryModel;

	public function init() { //initialization function
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjMarksdistributionmasterForm = new Examination_Form_Marksdistributionmaster();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjmarksdistributionmaster = new Examination_Model_DbTable_Marksdistributionmaster();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();
		$this->lobjSchemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksdetailForm = new Examination_Form_Marksdistributiondetails();
		$this->lobjmarksditrbutiondetail = new Examination_Model_DbTable_Marksdetails();
		$this->lobjmarksHistoryModel = new Examination_Model_DbTable_Markshistory();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$facultyList = $this->lobjcollegemaster->fnGetListofCollege();
		if(!empty($facultyList)){
			$this->view->lobjform->field5->addMultiOptions($facultyList);
		}

		$schemeList = $this->lobjSchemeModel->fngetSchemes();
		if(!empty($schemeList)){
			$this->view->lobjform->field23->addMultiOptions($schemeList);
		}

		$semList = $this->lobjsemesterModel->getAllsemesterListCode();
		if(!empty($semList)){
			$this->view->lobjform->field24->addMultiOptions($semList);
		}

		$programList = $this->lobjprogramModel->fnGetProgramList();
		if(!empty($programList)){
			$this->view->lobjform->field8->addMultiOptions($programList);
		}

		$courseList = $this->lobjsubjectmasterModel->fnGetSubjectList();
		if(!empty($programList)){
			$this->view->lobjform->field20->addMultiOptions($courseList);
		}

		if (!$this->_getParam('search'))
			unset($this->gobjsessionsis->Marksentrypaginatorresult);
		$larrresult = $this->lobjmarksdistributionmaster->fnGetMarksDistributionMaster(); //get user details
		//        echo "<pre>";
		//        print_r($larrresult);
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset($this->gobjsessionsis->Marksentrypaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksentrypaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjmarksdistributionmaster->fnSearchMarksDistributionMaster($lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->Marksentrypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/examination/marksdistributionapproval');
		}
	}



	public function editmarksdistributionmasterapprovalAction() { //Action for creating the new user
		$this->view->lobjMarksdistributionmasterForm = $this->lobjMarksdistributionmasterForm;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjMarksdistributionmasterForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjMarksdistributionmasterForm->UpdDate->setValue($ldtsystemDate);
		$this->view->lobjMarksdistributionmasterForm->Reject->setAttrib('OnClick','return checkRemark();');
		$Idcourse = (int) $this->_getParam('id');
		$IdScheme = (int) $this->_getParam('scheme');
		$IdFaculty = (int) $this->_getParam('faculty');
		$IdProgram = (int) $this->_getParam('program');
		$appcode = $this->_getParam('appcode');
		$mastermarksdistribution = $this->lobjmarksdistributionmaster->fnGetMarksDistributionMasterByCourse($Idcourse,$IdScheme,$IdFaculty,$IdProgram,$appcode);
		$this->view->allmaster = $mastermarksdistribution;
		$this->lobjMarksdistributionmasterForm->populate($mastermarksdistribution[0]);
		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

			if($this->_request->getPost('Approve')){
				$componentcount = $larrformData['count'];
				$data = array();
				for($i=0;$i<$componentcount;$i++){
					$compkey = 'componenttype_'.$i;
					$markskey = 'marks_'.$i;
					$compitemkey = 'componentitem_'.$i;
					$percentagekey = 'percentage_'.$i;
					$data['IdComponentType'] = $larrformData[$compkey];
					$data['IdComponentItem'] = $larrformData[$compitemkey];
					$data['Marks'] = $larrformData[$markskey];
					$data['Percentage'] = $larrformData[$percentagekey];
					$data['TotalMark'] = $larrformData['totalmarks'];
					//$data['IdProgram'] = $larrformData[$compkey];
					$data['IdCourse'] = $larrformData['course'];
					$data['Status'] = 243;
					$data['UpdUser'] = $larrformData['UpdUser'];
					$data['UpdDate'] = $larrformData['UpdDate'];
					$data['semester'] = $larrformData['semester'];
					$data['IdProgram'] = $larrformData['program'];
					$data['ApprovedBy'] = $auth->getIdentity()->iduser;
					$data['ApprovedOn'] = $ldtsystemDate;
					// now check component is exist in database
					$return = $this->lobjmarksdistributionmaster->fncheckexistmasterentry($larrformData['program'],$larrformData['course'],$larrformData['semester'],$data['IdComponentType'],$data['IdComponentItem'],$appcode);
					$this->lobjmarksdistributionmaster->fnupdateMarksDisributionMaster($data,$return[0]['IdMarksDistributionMaster']);
					if($i == 0){
						$rethistorydata = $this->lobjmarksHistoryModel->getHistory($return[0]['IdMarksDistributionMaster']);
						$historydatalen = count($rethistorydata);
						$historyArray['IdMarksDistributionMaster'] = $return[0]['IdMarksDistributionMaster'];
						$historyArray['Activity'] = 'APPROVED';
						$historyArray['OldStatus'] = $rethistorydata[$historydatalen - 1]['NewStatus'];
						$historyArray['NewStatus'] = 243;
						$historyArray['UpdatedOn'] = $ldtsystemDate;
						$historyArray['UpdatedBy'] = $auth->getIdentity()->iduser;
						$ret = $this->lobjmarksHistoryModel->addMarksHistory($historyArray);
					}
				}
			}else if($this->_request->getPost('Reject')){
				$componentcount = $larrformData['count'];
				$data = array();
				for($i=0;$i<$componentcount;$i++){
					$compkey = 'componenttype_'.$i;
					$markskey = 'marks_'.$i;
					$compitemkey = 'componentitem_'.$i;
					$percentagekey = 'percentage_'.$i;
					$data['IdComponentType'] = $larrformData[$compkey];
					$data['IdComponentItem'] = $larrformData[$compitemkey];
					$data['Marks'] = $larrformData[$markskey];
					$data['Percentage'] = $larrformData[$percentagekey];
					$data['TotalMark'] = $larrformData['totalmarks'];
					//$data['IdProgram'] = $larrformData[$compkey];
					$data['IdCourse'] = $larrformData['course'];
					$data['Status'] = 195;
					$data['UpdUser'] = $larrformData['UpdUser'];
					$data['UpdDate'] = $larrformData['UpdDate'];
					$data['semester'] = $larrformData['semester'];
					$data['IdProgram'] = $larrformData['program'];
					$data['Remarks'] = $larrformData['Remarks'];
					$data['RejectedBy'] = $auth->getIdentity()->iduser;
					$data['RejectedOn'] = $ldtsystemDate;
					// now check component is exist in database
					$return = $this->lobjmarksdistributionmaster->fncheckexistmasterentry($larrformData['program'],$larrformData['course'],$larrformData['semester'],$data['IdComponentType'],$data['IdComponentItem'],$appcode);
					$this->lobjmarksdistributionmaster->fnupdateMarksDisributionMaster($data,$return[0]['IdMarksDistributionMaster']);

					if($i == 0){
						$rethistorydata = $this->lobjmarksHistoryModel->getHistory($return[0]['IdMarksDistributionMaster']);
						$historydatalen = count($rethistorydata);
						$historyArray['IdMarksDistributionMaster'] = $return[0]['IdMarksDistributionMaster'];
						$historyArray['Activity'] = 'REJECT';
						$historyArray['OldStatus'] = $rethistorydata[$historydatalen - 1]['NewStatus'];
						$historyArray['NewStatus'] = 195;
						$historyArray['UpdatedOn'] = $ldtsystemDate;
						$historyArray['UpdatedBy'] = $auth->getIdentity()->iduser;
						$ret = $this->lobjmarksHistoryModel->addMarksHistory($historyArray);
					}
				}
			}
			$this->_redirect($this->baseUrl . '/examination/marksdistributionapproval');
		}
	}

	public function addmarksdistributionapprovedetailAction() {

		$Idmarksdistributionmaster = (int) $this->_getParam('id');
		$mastermarksdistribution = $this->lobjmarksdistributionmaster->fnGetMarksDistributionMasterById($Idmarksdistributionmaster);
		$this->view->lobjmarksdetailForm = $this->lobjmarksdetailForm;

		$detailmarks = $this->lobjmarksditrbutiondetail->fnGetMarksDistributiondetailById($Idmarksdistributionmaster);

		if(!empty($detailmarks)){
			$this->view->detailmarks = $detailmarks;
		}

		if(count($mastermarksdistribution) > 0){
			$this->view->detailmaster = $mastermarksdistribution;
		}
		//        echo "<pre>";
		//        print_r($mastermarksdistribution);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjmarksdetailForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjmarksdetailForm->UpdDate->setValue($ldtsystemDate);
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			if(!empty($detailmarks)){
				$this->lobjmarksditrbutiondetail->fndeleteMarksDistributiondetailById($Idmarksdistributionmaster);
			}
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post
			$len = count($larrformData['componenttypegrid']);
			$data = array();
			for($i=0;$i<$len;$i++){
				$data['IdMarksDistributionMaster'] = $Idmarksdistributionmaster;
				$data['IdComponentType'] = $larrformData['componenttypegrid'][$i];
				$data['IdComponentItem'] = $larrformData['componentitemgrid'][$i];
				$data['status'] = 193;
				$data['Weightage'] = $larrformData['markgrid'][$i];
				//$data['Percentage'] = $larrformData['percentagegrid'][$i];
				$data['TotalMark'] = $larrformData['totalmarks'];
				$data['UpdDate'] = $larrformData['UpdDate'];
				$data['UpdUser'] = $larrformData['UpdUser'];
				$this->lobjmarksditrbutiondetail->fnAddMarksDisributionDetail($data);
			}
			$url = "id/".$mastermarksdistribution['IdCourse'];
			$url = $url."/scheme/".$mastermarksdistribution['IdScheme'];
			$url = $url."/faculty/".$mastermarksdistribution['IdFaculty'];
			$url = $url."/program/".$mastermarksdistribution['IdProgram'];
			$url = $url."/appcode/".$mastermarksdistribution['MarksApplicationCode'];
			$this->_redirect($this->baseUrl . '/examination/marksdistributionapproval/editmarksdistributionmasterapproval/'.$url);
		}






	}



}