<?php
class examination_RemarkappealController extends Base_Base { //Controller for the User Module

	private $lobjSubjectmaster;
	private $lobjremarkentrysetup;
	private $lobjprogram;
	private $lobjStudentRegModel;
	private $lobjsemester;
	private $_gobjlog;
	private $lobjstatus;
	private $lobjdeftype;
	private $lobjformRemark;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjRemarkapplication =  new Examination_Model_DbTable_Remarkapplication();
		$this->lobjremarkconfig = new Examination_Model_DbTable_Remarkingconfig();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjstatus = new Examination_Model_DbTable_Remarkingconfig();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjformRemark = new Examination_Form_Remarking ();
	}


	/**
	 * Function to search Appeal
	 * @author: SJ
	 */
	public function indexAction() { // action for search and view
		//Intialize the Form
		$lobjform = new App_Form_Search ();
		$this->view->lobjform = $lobjform;

		// SHOW THE COURSES
		$larrresultCourses = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field23->addMultiOptions( $larrresultCourses );

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field24->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramName']);
		}

		// SHOW THE SEMESTER
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}

		//SHOW THE MODE
		$applicationsource = $this->lobjdeftype->fnGetDefinationMs('Source Of Application');
		$this->view->lobjform->field1->addMultiOptions($applicationsource);


		//SHOW THE STATUS
		$larrstatusresult = $this->lobjstatus->showstatus();
		foreach($larrstatusresult as $larrstatusvalues) {
			$this->view->lobjform->field25->addMultiOption($larrstatusvalues['key'],$larrstatusvalues['value']);
		}

		//Js
		//$this->view->lobjform->Search->setAttrib('Onclick','return formsubmit();');

		$larrresult = $this->lobjremarkconfig->fnSearchRemarkingEntry( $post = NULL);

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Remarkingpaginatorresult);


		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Remarkingpaginatorresult)) {

			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Remarkingpaginatorresult,$lintpage,$lintpagecount);
		} else {

			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);

		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ($larrformData)) {


				$larrresult = $this->lobjremarkconfig->fnSearchRemarkingEntry($larrformData);

				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Remarkingpaginatorresult = $larrresult;

			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/remarkappeal');
		}
	}


	/**
	 * Function to Add Appeal
	 * @author: SJ
	 */

	public function addremarkappealAction() {
		//Intialize Form
		$this->view->lobjform = $this->lobjformRemark;

		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->view->iduser = $auth->getIdentity()->iduser;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$UpdDate = date('Y-m-d H:i:s');

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field24->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramName']);
		}

		// SHOW THE SEMESTER
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}




		//Mode
		$DefmodelObj = new GeneralSetup_Model_DbTable_Maintenance();
		$mode = $DefmodelObj->fngetDefKeyValue('Source Of Application','MN');
		$mode = $mode[0]['key'];


		//Js Call
		$this->view->lobjform->field2->setAttrib('Onblur','showProgram();');
		$this->view->lobjform->field27->setAttrib('OnChange','showcourses();');
		$this->view->lobjform->field23->setAttrib('OnChange','showcomponent();');
		$this->view->lobjform->field25->setAttrib('OnChange','showdetailcomponent();');
		$this->view->lobjform->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjform->Save->setAttrib('OnClick','return checkSave();');

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if($larrformData['savedata']=='searchdata') {

				//Get Student Id
				$fetchIDStudentReg =   $this->lobjremarkconfig->fngetIdStudentregistration($larrformData['field2']);
				$getIDstudentReg = $fetchIDStudentReg[0]['IdStudentRegistration'];

				// Get Program code for that student ID
				$program  = $fetchIDStudentReg[0]['IdProgram'];

				//Get Semester Code
				$semester = $larrformData['field27'];//77_main
				$semester = $this->lobjRemarkapplication->getSemcode($semester);//MFS001

				//Get Course
				$idcourse  = $larrformData['field23'];
				//Get Student Id

				//Check Config Files
				$getMASemester = $this->lobjremarkconfig->getMaxAppealperUniversity($IdUniversity);
				if(count($getMASemester)==0) {
					$maxAppeal = '2';
				}
				else {$maxAppeal = $getMASemester[0]['MaxAppeal'];
				}

				$larrconfig = $this->lobjRemarkapplication->totalAppealMaxPerStudent($idcourse,$getIDstudentReg, $semester, $maxAppeal);

				// Add Appeal
				if($larrconfig=='0') {
					$this->lobjremarkconfig->addRemarkappeal($mode,$program,$semester,$larrformData,$getIDstudentReg,$UpdDate,$userId);
					$this->_redirect( $this->baseUrl . '/examination/remarkappeal/index');
				} else {
					$this->view->errMsg = '1';
				}

			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/remarkappeal/addremarkappeal');
		}

	}

	public function editremarkappealAction() {
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		#$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->view->iduser = $auth->getIdentity()->iduser;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$UpdDate = date('Y-m-d H:i:s');

		$idAppeal = $this->_getParam ( 'id' );

		$resultStatus = $this->lobjremarkconfig->fngetAppealStatus($idAppeal);
		if(count($resultStatus)>0){
			$this->view->appealStatus = $resultStatus[0]['Status'];
		}


		$this->view->lobjform = $this->lobjformRemark;

		//Show the student Id field1
		$resultall = $this->lobjremarkconfig->fngetdetails($idAppeal);
		$larrstudentidresult = $resultall[0]['IdStudent'];
		$this->view->lobjform->field1->setValue($larrstudentidresult);
		$this->view->lobjform->field1->setAttrib('readonly',true);


		// SHOW THE SEMESTER field2
		$larrsemresult = $resultall[0]['SemesterCode'];
		$this->view->lobjform->field2->setValue($larrsemresult);
		$this->view->lobjform->field2->setAttrib('readonly',true);

		//Show the course field3
		$larrcourseresult = $resultall[0]['SubjectName'];
		$this->view->lobjform->field3->setValue($larrcourseresult);
		$this->view->lobjform->field3->setAttrib('readonly',true);


		// SHOW THE PROGRAM field4
		$larrprogramresult = $resultall[0]['ProgramName'];
		if(isset($larrprogramresult)){
			$this->view->lobjform->field4->setValue($larrprogramresult);
			$this->view->lobjform->field4->setAttrib('readonly',true);
		}


		// Show the component field5
		$larrresult = $this->lobjremarkconfig->fngetdetailcomponent($idAppeal);
		$courseID  = $larrresult[0]['IdCourse'];
		$component = $larrresult[0]['IdComponent'].'-'.$larrresult[0]['IdComponentItem'];
		$componentitem = $larrresult[0]['IdComponentItem'];
		$this->view->courseID = $courseID;

		$larrcompresult = $this->lobjremarkconfig->showcomponent($courseID);
		foreach($larrcompresult as $values) {
			$this->view->lobjform->field25->addMultiOption($values['key'],$values['value']);
		}

		$this->view->lobjform->field25->setValue($component);
		$this->view->lobjform->field25->setAttrib('readonly',true);


		//Show detail Component
		$fetchIDStudentReg =   $this->lobjStudentRegModel->fngetIdStudentregistration($larrstudentidresult);
		$getIDstudentReg = $fetchIDStudentReg[0]['IdStudentRegistration'];

		//		$larrdetailcomponent = $this->lobjremarkconfig->showdetailcomponentitem($getIDstudentReg,$courseID,$component,$componentitem);
		//
		//		foreach($larrdetailcomponent as $values) {
		//			$this->view->lobjform->field26->addMultiOption($values['key'],$values['value']);
		//	        }

		//Other
		$this->view->lobjform->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjform->Save->setAttrib('OnClick','return checkSave();');

		$result = $this->lobjremarkconfig->getAppealDetailByID($idAppeal);
		$this->view->result = $result;

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {

			$larrformData = $this->_request->getPost ();

			if($larrformData['savedata']=='searchdata') {

				//Check Config Files
				//$semester = $larrsemresult;
				//$getMASemester = $this->lobjremarkconfig->getMaxAppealperSemester($IdUniversity,$semester);

				//if(count($getMASemester)==0) {$maxAppeal = '2';}
				//else {$maxAppeal = $getMASemester[0]['MaxAppeal'];}

				//$idcourse = $result[0][IdCourse];
				//$larrconfig = $this->lobjRemarkapplication->totalAppealMax($idcourse,$getIDstudentReg, $semester, $maxAppeal);

				// Add Appeal
				//if($larrconfig=='0') {
				$this->lobjremarkconfig->updateRemarkappeal($userId,$UpdDate,$courseID,$idAppeal,$larrformData,$getIDstudentReg);
				$this->_redirect( $this->baseUrl . '/examination/remarkappeal');
				//} else {
				//$this->view->errMsg = '1';
				//}

			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/remarkappeal/editremarkappeal/id/'.$idAppeal);
		}

	}


	public function getcourselistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$studentID =  $this->_getParam ( 'studentID' );
		$semester =  $this->_getParam ( 'semester' );
		//fetch the student id i.e IMF001
		$fetchIDStudentReg =   $this->lobjStudentRegModel->fngetIdStudentregistration($studentID);
		// fetch the studentID primary key
		$getIDstudentReg = $fetchIDStudentReg[0]['IdStudentRegistration'];
		//fetch the courses related to semester and studentID
		$larrresultCourses = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjRemarkapplication->fngetCourses($getIDstudentReg,$semester));
		echo Zend_Json_Encoder::encode($larrresultCourses);
	}

	public function getcomponentlistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$courseID =  $this->_getParam ( 'course' );
		$fetchComponent =  $this->lobjremarkconfig->showcomponent($courseID);
		$larrresultComponent = $lobjCommonModel->fnResetArrayFromValuesToNames($fetchComponent);
		echo Zend_Json_Encoder::encode($larrresultComponent);
	}

	public function getdetailcomponentlistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$resultnew = array();
		$studentID =  $this->_getParam ( 'studentID' );
		$component = $this->_getParam ( 'component' );
		$courseID =  $this->_getParam ( 'course' );
		$fetchIDStudentReg =   $this->lobjStudentRegModel->fngetIdStudentregistration($studentID);
		$getIDstudentReg = $fetchIDStudentReg[0]['IdStudentRegistration'];
		$fetchDetailComponent = $this->lobjremarkconfig->showdetailcomponent($getIDstudentReg,$courseID,$component);
		//$larrresultComponent = $lobjCommonModel->fnResetArrayFromValuesToNames($fetchDetailComponent);
		if(count($fetchDetailComponent)>0) {
			echo Zend_Json_Encoder::encode($fetchDetailComponent);
		}
		else {
			echo Zend_Json_Encoder::encode($resultnew);
		}
	}


	public function getprogramlistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$studentID =  $this->_getParam ( 'studentID' );
		$larrresultProgram = '';
		$getProfileStatus = $this->lobjStudentRegModel->fnStudentProfileStatus($studentID);
		if($getProfileStatus=='92' || $getProfileStatus=='248' || $getProfileStatus=='253' ) { // active, defer, dormant
			$fetchProgram = $this->lobjremarkconfig->showProgramName($studentID);
			$larrresultProgram = $lobjCommonModel->fnResetArrayFromValuesToNames($fetchProgram);
			echo Zend_Json_Encoder::encode($larrresultProgram);
		} else {
			echo Zend_Json_Encoder::encode($larrresultProgram);
		}

	}



}
