<?php
 
class Examination_ExamRecordController extends Base_Base {

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Exam Records - Student List");
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		//faculty
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
		
		if($this->_sis_session->IdRole == 1){
			$collegeList = $collegeDb->getCollege();
		}else{
			$this->view->default_faculty = $this->_sis_session->idCollege;
			$collegeList = array('0'=>$collegeDb->fngetCollegemasterData($this->_sis_session->idCollege));
		}
		
		$this->view->college_list = $collegeList;
		
	}
	
	function searchCourseGroupAction(){
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			/*
			 * Search Group
			*/
			//get Subject Info
			$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			$subject = $subjectDb->getData($subject_id);
			$this->view->subject = $subject;
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getGroupList($subject_id,$semester_id);
			
			
			$i=0;
			foreach($groups as $group){
					
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
					
				$group["total_student"] = $total_student;
				$groups[$i]=$group;
					
				$i++;
			}
			/*
			 * End search group
			*/
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($groups);
		
			echo $json;
			exit();
		
		}
	}
	
	
	function courseGroupStudentAction(){
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		$group_id = $this->_getParam('group_id', null);
		$StudentName = $this->_getParam('StudentName', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			/*
			 * Search Student by Course Group
			*/
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
			$student = $courseGroupDb->getStudentbyGroup($group_id,$StudentName);
			
			
			foreach($student as $key => $row){
				
				//get semester status utk student ni
				$subjectRegDb = new Examination_Model_DbTable_StudentRegistrationSubject();
				$sem_status = $subjectRegDb->getSemesterStatus($semester_id,$row["IdStudentRegistration"]);
				$sub_status = $subjectRegDb->getSemesterSubjectStatus($semester_id,$row["IdStudentRegistration"],$subject_id);
				
				//get subject status pada semester ni
				$student[$key]["semester_status"]=$sem_status["defination"];
				
				if($sub_status["Active"]==1){
					$subject_status = "Register";					
				}else if($sub_status["Active"]==2){
					$subject_status = "Add & Drop";
				}else if($sub_status["Active"]==3){
					$subject_status = "Withdrawal";
				}else{
					$subject_status = "Not Register";
				}
				$student[$key]["subject_status"] =$subject_status;	
				$student[$key]["IdStudentRegSubjects"] =$sub_status["IdStudentRegSubjects"];	
				
				
			}
		
			
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($student);
		
			echo $json;
			exit();
		
		}
	}
	
	
	public function viewMarkAction() {
		
		//title
		$this->view->title= $this->view->translate("Exam Records - Student Mark");
		
		$idSemester = $this->_getParam('semester_id', null);		
		$idSubject = $this->_getParam('course_id', null);
		$idGroup = $this->_getParam('group_id', null);
		$IdStudentRegistration = $this->_getParam('id', null);
		$IdStudentRegSubjects = $this->_getParam('regSubId', null);
		
		
		
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		$this->view->IdStudentRegistration = $IdStudentRegistration;
		$this->view->IdStudentRegSubjects = $IdStudentRegSubjects;
		
		//get student program
		$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
		$student = $studentRegDB->getDatabyId($IdStudentRegistration);
		
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($student["IdProgram"]);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    		
		//get component
		$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$student["IdProgram"],$idSubject);	    
    	$this->view->rs_component = $list_component;
    	
    	//echo'<pre>';
    	//print_r($list_component);
    	//echo'</pre>';
    	
	
	}
}

?>