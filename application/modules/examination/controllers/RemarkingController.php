<?php
class examination_remarkingController extends Base_Base { //Controller for the REcords Module

	private $_gobjlog;
	private $lobjsemester;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjremarkconfig = new Examination_Model_DbTable_Remarkingconfig();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();


	}


	/**
	 * Function to display the Examination Configuration setup form
	 * @author: Sourabh
	 */
	public function indexAction() {

		$lobjform = new Examination_Form_Remarking ();

		$this->view->lobjform = $lobjform;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
		$IdUniversity = $this->gobjsessionsis->idUniversity;


		//SHOW MARKS APPEAL
		//$larrComponents = $this->lobjdeftype->fnlistDefination('85');
		$DefmodelObj = new App_Model_Definitiontype();
		$raceList = $DefmodelObj->fnGetDefinationMs('Marks Appeal');

		foreach($raceList as $larrvalues) {
			$this->view->lobjform->ChooseMarks->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}

		// SHOW THE SEMESTER
		//        $larrsemresult =  $this->lobjsemester->getAllsemesterListCode();
		//
		//        foreach($larrsemresult as $larrsemvalues) {
		//				$this->view->lobjform->SemesterCode->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		//	        }

		$this->view->lobjform->MaxAppeal->setValue('1');
		$this->view->lobjform->UpdUser->setValue($userId);
		$this->view->lobjform->UpdDate->setValue($UpdDate);
		$this->view->lobjform->IdUniversity->setValue($IdUniversity);

		$getDetauilsConfig= $this->lobjremarkconfig->getConfigDetails($IdUniversity);
		if(count($getDetauilsConfig)>0) {
			$this->view->lobjform->populate($getDetauilsConfig[0]);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {


			$larrformData = $this->_request->getPost ();

			$errMsgInsert = array();
			$errMsgInsert = $this->lobjremarkconfig->getConfigDetails($IdUniversity);

			if(empty($errMsgInsert)){
				$this->lobjremarkconfig->addAppeal($larrformData);
					
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array (  'user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Re-examine Examination Setup Add UniversityID = '.$IdUniversity,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
					
					
					
			}else{
				$this->lobjremarkconfig->updateConfigDetails($larrformData,$IdUniversity);
					
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array (  'user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Re-examine Examination Setup Edit UniversityID = '.$IdUniversity,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
					
			}
			$this->_redirect( $this->baseUrl . '/examination/remarking');




		}




			
	}

}
?>