<?php

class examination_MarkDistributionController extends Base_Base { //Controller for the User Module

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution");
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkDistributionApprovalSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	$auth = Zend_Auth::getInstance();
    	
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			//$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);

				$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
				if(count($groups)>0){
					foreach($groups as $i=>$group){					
										    
							//check if exist
					    	$total = $markDistributionDB->checkExistEntry($group['IdSemester'],$group['IdSubject'],$group['IdProgram'],$group['IdCourseTaggingGroup']);
					    	$groups[$i]['total']=$total;

					    	//check status
					    	$status = $markDistributionDB->getStatusComponent($group['IdSemester'],$group['IdProgram'],$group['IdSubject'],$group['IdCourseTaggingGroup']) ;
					    	$groups[$i]['status']=$status['Status'];
					}
				}
				$this->view->groups = $groups;

				
								
			}//if form valid
    	}//if post		
    }
    
	public function indexMyAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution");
    	
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MyGroupSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	$semesterDB = new Registration_Model_DbTable_Semester();
    	$semester = $semesterDB->getAllCurrentSemester();		    	    	
		
    	
    	$cur_sem = array();
    	foreach($semester as $sem){
    		array_push($cur_sem,$sem['IdSemesterMaster']);
    	}
    	
    	//get group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			//$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			//$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);				
			    
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
				
				if(count($groups)>0){
					foreach($groups as $i=>$group){					
										    
							//check if exist
					    	$total = $markDistributionDB->checkExistEntry($group['IdSemester'],$group['IdSubject'],$group['IdProgram'],$group['IdCourseTaggingGroup']);
					    	$groups[$i]['total']=$total;

					    	//check status
					    	$status = $markDistributionDB->getStatusComponent($group['IdSemester'],$group['IdProgram'],$group['IdSubject'],$group['IdCourseTaggingGroup']) ;
					    	$groups[$i]['status']=$status['Status'];
					}
				}
				$this->view->groups = $groups;

				
								
			}//if form valid
    	}else{
    		
    		$formData['IdSemesterArray'] = $cur_sem;
    		$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
    		
    		if(count($groups)>0){
				foreach($groups as $i=>$group){					
									    
						//check if exist
				    	$total = $markDistributionDB->checkExistEntry($group['IdSemester'],$group['IdSubject'],$group['IdProgram'],$group['IdCourseTaggingGroup']);
				    	$groups[$i]['total']=$total;

				    	//check status
				    	$status = $markDistributionDB->getStatusComponent($group['IdSemester'],$group['IdProgram'],$group['IdSubject'],$group['IdCourseTaggingGroup']) ;
				    	$groups[$i]['status']=$status['Status'];
				}
			}
				
    		$this->view->groups = $groups;
    		
    	}//if post		
    }
    
    public function createAction(){
       
    		$auth = Zend_Auth::getInstance();
    	
    
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$this->view->idProgram = $idProgram;   
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		
    	
    		
		//check if setup  for hop  is exist
		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();			
		$main_status = $setupDb->getExistDataByHOP($idSemester,$idProgram,$idSubject);
		
		if($main_status){
			
				$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
				
				//get info semester
    			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    			$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    
    	
				//get component
		    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
				//$assessment_list = $AssessmenttypeDB->fngetAllassessmentType();	
				$assessment_list = $AssessmenttypeDB->getAssessmentbyType($type=0,$idSemester,$semester[0]['IdScheme']);

			
				$total_raw = 0;
				$mds_weightage = 0;
		
				foreach($assessment_list as $index=>$list){
					//get info mds from HOP
					$detail = $setupDb->getAssessment(2,$list['IdExaminationAssessmentType'],$main_status["mds_semester"],$idSubject,$idProgram);					
					
					if($detail){
						
						$total_raw = $total_raw + $detail['mds_raw'];
						$total_weightage = $mds_weightage + $detail['mds_weightage'];
					
						//insert into table					 		
						$data["semester"]  = $idSemester;
						$data["IdProgram"] = 0; //inceif x by program $idProgram;			
						$data["IdCourse"]  = $idSubject;				
						$data["IdCourseTaggingGroup"]  = $idGroup;			
						$data["Status"]    = 0;//Entry
						$data['UpdUser']   = $auth->getIdentity()->iduser;
						$data['UpdDate']   = date ( 'Y-m-d H:i:s');
						$data["TotalMark"] = 0;		
						$data["IdComponentType"]= $list['IdExaminationAssessmentType'];			 			 
					 	$data["Marks"]= 0;
					 	$data["Percentage"]= 0;
					 	$data["TotalMark"]= 0;
					 	$data['allow_appeal']= 1;
						$data['allow_resit']= 1;			
			    	
						
			    		//print_r($data);
						$markDistributionDB->addData($data);
					}
				}
				
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idSemester'=>$idSemester,'idProgram'=>$idProgram,'idSubject'=>$idSubject,'idGroup'=>$idGroup),'default',true));
		   
		
		}else{
		
			$this->gobjsessionsis->flash = array('message' => 'Main setup by HOP does not exist', 'type' => 'notice');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'index'),'default',true));
		}
		          
    }
    
    
    public function viewAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	$this->view->title=$this->view->translate("Mark Distribution");
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$this->view->sysmsg = $this->_getParam("msg");
    	$this->view->user = $auth->getIdentity()->iduser;
    	
    	if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){ //admin	atau asad admin	
    		$this->view->url = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution','action'=>'index'), 'default', true);
    	}else{
    		$this->view->url = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution','action'=>'index-my'), 'default', true);
    	}
    	
    	$this->view->idProgram = $idProgram;   
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	//$programDB = new GeneralSetup_Model_DbTable_Program();
    	//$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get group info
    	$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    	$this->view->group = $groupDB->getInfo($idGroup);
    	
    	//check if setup  for hop  is exist
    	$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
		$main_status = $setupDb->getExistHOPByGroup($idSemester,$idSubject,$idGroup);
		
		
		if($main_status){
			$flag = true;
			//check kalo belum ada keluarkan button add
	    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
	    	$total = $markDistributionDB->checkExistEntry($idSemester,$idSubject,$idProgram,$idGroup);
	    	$this->view->total = $total;	    	
	    	
	    	//get component
			$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
	    
	    	//print_r($list_component);
	    	
	    	$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	    	foreach($list_component as $index=>$component){
	    		
	    		  $this->view->status = $this->getStatus($component['Status']);
	    		
			  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
			  	  $list_component[$index]['component_item'] = $component_item;
	    	}
	    	$this->view->rs_component = $list_component;
	    	
		}else{
			$flag = false;
		}
		$this->view->flag = $flag;
    	
    	
    	
    }
    
    
 	public function entryAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution : Entry");
    	    	
    	$this->_helper->layout->disableLayout();
    	 
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$auth = Zend_Auth::getInstance();
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
 		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup(); 		
 		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
				
				
		//check if data already exist		
		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();		
		$status = $markDistributionDB->checkExistEntry($idSemester,$idSubject,$idProgram,$idGroup);

		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    
		//get component
    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
		//$assessment_list = $AssessmenttypeDB->fngetAllassessmentType();	
		$assessment_list = $AssessmenttypeDB->getAssessmentbyType($type=0,$idSemester,$semester[0]['IdScheme']);

		$total_raw = 0;
		$mds_weightage = 0;
		foreach($assessment_list as $index=>$list){
								
			if($status){
				
				//get mark each component
				$main_mark_distribution = $markDistributionDB->getComponentByAssessmentId($list['IdExaminationAssessmentType'],$idGroup,$idSemester,$idProgram,$idSubject);
				$assessment_list[$index]['IdMarksDistributionMaster']= $main_mark_distribution['IdMarksDistributionMaster'];
				$assessment_list[$index]['mds_raw']= $main_mark_distribution['Marks'];
				$assessment_list[$index]['mds_weightage']= $main_mark_distribution['Percentage'];
				$assessment_list[$index]['allow_appeal']= $main_mark_distribution['allow_appeal'];
				$assessment_list[$index]['allow_resit']= $main_mark_distribution['allow_resit'];
				
				$total_raw = $total_raw + $main_mark_distribution['Marks'];
				$total_weightage = $mds_weightage + $main_mark_distribution['Percentage'];
				
				
				//get item
				$component_item_list = $oCompitem->getListComponentItem($main_mark_distribution['IdMarksDistributionMaster']);
				$assessment_list[$index]['component_item']= $component_item_list;
				
			}else{
				
				//get info mds from HOP
				$detail = $setupDb->getDetails(2,$list['IdExaminationAssessmentType'],$main_status["mds_semester"],$idSubject,$idProgram);					
				$assessment_list[$index]['mds_raw']= $detail['mds_raw'];
				$assessment_list[$index]['mds_weightage']= $detail['mds_weightage'];	
				$assessment_list[$index]['allow_appeal']= 1;
				$assessment_list[$index]['allow_resit']= 1;
				
				$total_raw = $total_raw + $detail['mds_raw'];
				$total_weightage = $mds_weightage + $detail['mds_weightage'];
				
			}
								
			
		}
		$this->view->total_raw = $total_raw;
		$this->view->list_component = $assessment_list;  
		
		//echo '<pre>';
		//print_r($assessment_list);
			
			
    		
    	
    	
    }
    
    
    public function addAction(){
    	
    	$this->view->title=$this->view->translate("Add Mark Distribution");
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$auth = Zend_Auth::getInstance();
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idScheme = $this->_getParam('idScheme');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idScheme = $idScheme;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get Assessment Type (component)
    	$assessmentTypeDb = new Examination_Model_DbTable_Assessmenttype();
    	$assessment = $assessmentTypeDb->getdropdownforasseementtype();
    	$this->view->components = $assessment;
    	
    	//get program scheme
    	$programSchemeDb = new GeneralSetup_Model_DbTable_Programscheme();
        $this->view->scheme = $programSchemeDb->getSchemeById($idScheme);
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
						
			$iteration  		= $formData["iteration"];	
			
			$data["semester"]  = $formData["idSemester"];
			$data["IdProgram"] = $formData["idProgram"];			
			$data["IdCourse"]  = $formData["idSubject"];	
			$data["IdScheme"]  = $formData["idScheme"];			
			$data["Status"]    = 193;
			$data['UpdUser']   = $auth->getIdentity()->iduser;
			$data['UpdDate']   = date ( 'Y-m-d H:i:s');
			$data["TotalMark"] = $formData["total_mark"];
			
				 		 
			for($i=1; $i<=$iteration; $i++){
			 	
			 	$data["IdComponentType"]= $formData["component_id".$i];			 			 
			 	$data["Marks"]= $formData["component_total_mark".$i];
			 	$data["Percentage"]= $formData["component_percentage".$i];	
			 	
				if(isset($formData["appeal".$i])){
			 		$data["allow_appeal"] = $formData["appeal".$i];
			 	}
			 	
			 	if(isset($formData["resit".$i])){
			    	$data["allow_resit"] = $formData["resit".$i];
			 	}
			    
			 	//insert into table
			    $markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			    $markDistributionDB->addData($data);
			    
			   
		    }//end for
				
				
		  		//redirect
		  		$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idScheme'=>$formData["idScheme"]),'default',true));
		   
    	}//end if
    	
    	
    }
    
    
	public function editAction(){
    	
    	$this->view->title=$this->view->translate("Edit Main Component - Mark Distribution");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get list main component 
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$this->view->main_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject);
    	$this->view->TotalMark = $markDistributionDB->getTotalMark($idSemester,$idProgram,$idSubject);
    	      	
    	//get Assessment Type (component)
    	$assessmentTypeDb = new Examination_Model_DbTable_Assessmenttype();
    	$assessment = $assessmentTypeDb->getdropdownforasseementtype();
    	$this->view->components = $assessment;
    	
    	//print_r($this->view->main_component );
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$iteration  	   = $formData["iteration"];		
			$data["semester"]  = $formData["idSemester"];
			$data["IdProgram"] = $formData["idProgram"];			
			$data["IdCourse"]  = $formData["idSubject"];	
			$data["Status"]    = 193;	//ENTRY
			$data['UpdUser']   = $auth->getIdentity()->iduser;
			$data['UpdDate']   = date ( 'Y-m-d H:i:s');
			$data["TotalMark"] = $formData["total_mark"];
				 		 
			for($i=1; $i<=$iteration; $i++){
			 	
				$IdMarksDistributionMaster = $formData["IdMarksDistributionMaster".$i];	
				
			 	$data["IdComponentType"]= $formData["component_id".$i];			 			 
			 	$data["Marks"]= $formData["component_total_mark".$i];
			 	$data["Percentage"]= $formData["component_percentage".$i];	
			 	
			 	if(isset($formData["appeal".$i]) && $formData["appeal".$i]!=''){
			 		$data["allow_appeal"] = $formData["appeal".$i];
			 	}else{
			 		$data["allow_appeal"] = 0;
			 	}
			 	
			 	if(isset($formData["resit".$i]) && $formData["resit".$i]!=''){
			    	$data["allow_resit"] = $formData["resit".$i];
			 	}else{
			 		$data["allow_resit"] = 0;
			 	}
			
			 
			 	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			 	
				if(isset($IdMarksDistributionMaster)){
							//update
							$markDistributionDB->updateData($data,$IdMarksDistributionMaster);
				}else{ 			
					 	    //insert into table			   
			  				$markDistributionDB->addData($data);
				}//end if
			    
		    }//end for
			
		  
			 
		  	//redirect
		  	$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idScheme'=>$idScheme),'default',true));
		   
    	}//end if
    	
    	
    }
    
    
	
    
	public function editItemAction(){
    	
    	$this->view->title=$this->view->translate("Add & Edit Item - Mark Distribution");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$IdMarksDistributionMaster = $this->_getParam('id');
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get main component info
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$this->view->main_component = $markDistributionDB->getInfoComponent($IdMarksDistributionMaster);
    	//print_r($this->view->main_component);
    	
    	//get list component item    	
  	   $oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
  	   $this->view->component_item = $oCompitem->getListComponentItem($IdMarksDistributionMaster);
    	
    	//get Assessment Type (component)
    	$assessmentItemDb = new Examination_Model_DbTable_Assessmentitem();
    	$assessment = $assessmentItemDb->getdropdownforasseementitem();
    	$this->view->components = $assessment;
    	
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
						
			$iteration  	   = $formData["iteration"];
			
			$data["IdMarksDistributionMaster"]  = $formData["IdMarksDistributionMaster"];
			$data["idSemester"]  = $formData["idSemester"];
			$data['idStaff']   = $auth->getIdentity()->iduser;
			$data['UpdUser']   = $auth->getIdentity()->iduser;
			$data['UpdDate']   = date ( 'Y-m-d H:i:s');
			$data["TotalMark"]= $formData["total_mark"];
				 		 
			for($i=1; $i<=$iteration; $i++){
			 	
				$IdMarksDistributionDetails = $formData["IdMarksDistributionDetails".$i];	
				
			 	$data["ComponentName"]= $formData["component_name".$i];			 			 
			 	$data["Weightage"]= $formData["component_total_mark".$i];
			 	$data["Percentage"]= $formData["component_percentage".$i];	
			 
			 	$markDistributionDB = new Examination_Model_DbTable_Marksdistributiondetails();
			 	 
				if(isset($IdMarksDistributionDetails)){
							//update
							$markDistributionDB->updateData($data,$IdMarksDistributionDetails);
				}else{ 			
					 	    //insert into table			   
			  				$markDistributionDB->addData($data);
				}//end if
			   
		    }//end for
			
		   
		  	//redirect
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"]),'default',true));
		   
    	}//end if
    	
    	
    }
    
	function deleteAction(){
    	
    	if ($this->_request->isPost()) {   
    	 	  
    		 $formData = $this->getRequest()->getPost();
    		
			 for($i=0; $i < count($formData["id"]); $i++){
			 	
			 	$id =  $formData["id"][$i];
			 	
			 	//delete parent component
				$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    			$markDistributionDB->deleteData($id);
    			
    			//delete child     			
				$markDistributionDB = new Examination_Model_DbTable_Marksdistributiondetails();
    			$markDistributionDB->deleteComponentItem($id);
    		 }
    		
    		 //redirect			
		     $this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'edit','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"]),'default',true));
	    }
	    exit;
		   
    }
    
    function deleteItemAction(){
    	
    	if ($this->_request->isPost()) {   
    	 	  
    		 $formData = $this->getRequest()->getPost();
    		
    		 $markDistributionDB = new Examination_Model_DbTable_Marksdistributiondetails();
    		 
			 for($i=0; $i < count($formData["id"]); $i++){
			 	
			 	$id =  $formData["id"][$i];
			 	
			 	//delete component				
    			$markDistributionDB->deleteData($id);
    		 }
    		 
    		 
    		 //redirect	
    		 $this->gobjsessionsis->flash = array('message' => 'Data has been deleted', 'type' => 'success');		
		     $this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'entry','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idGroup'=>$formData["idGroup"]),'default',true));
	    }
	    exit;
		   
    }
    
    
    function copyAction(){
    	
    	$this->view->title=$this->view->translate("Copy Mark Distribution");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){ //admin	atau asad admin	
    		$this->view->url = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution','action'=>'index'), 'default', true);
    	}else{
    		$this->view->url = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution','action'=>'index-my'), 'default', true);
    	}
    	
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$this->view->idProgram = $idProgram;   
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get group info
    	$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    	$this->view->group = $groupDB->getInfo($idGroup);
    	
		
		
    	//get smester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->view->semestercopy = $semesterDB->getSemesterOnwards();
		
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->programcopy = $programDb->getData();
		
		//$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		//$this->view->subjectcopy = $subjectDB->searchSubject();
    	
		if ($this->_request->isPost()) {   
    	 	  
    		 $formData = $this->getRequest()->getPost(); 

    		//check if setup  for hop  is exist
    		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup(); 
			$main_status = $setupDb->getExistHOPByGroup($formData['idSemesterTo'],$formData['idSubjectTo'],$formData['idGroupTo']);
			
			if($main_status){
		
				 //check if selected mark distribution is available
	    		 $markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
	    		 $result = $markDistributionDB->getListCopyComponent($formData['idSemester'],$formData['idProgram'],$formData['idSubject'],$formData['idGroup']);
	    		 
	    		 if(count($result)>0){
	    		 	
	    		 	 //check if copy to combination already exist?
	    		 	 $result_to = $markDistributionDB->getComponent($formData['idSemesterTo'],$formData['idProgramTo'],$formData['idSubjectTo'],$formData['idGroupTo']);
	    		
	    		 	 if($result_to){
	    		 	 	//available cannot copy
	    		 	 	$this->gobjsessionsis->flash = array('message' => 'Data already exist.', 'type' => 'error');	
	    		 	 }else{
	    		 	 	
	    		 	 	//get semester to copy info
	    		 	 	$semesterToCopyDetail = $semesterDB->fngetSemestermainDetails($formData['idSemesterTo']);
	    		 	 	
	    		 	 	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
						
	    		 	 	//get item
	    		 	 	$markDistributionDetDB = new Examination_Model_DbTable_Marksdistributiondetails();
	    		 	 	
	    		 	 	foreach ($result as $r){
	    		 	 	
	    		 	 		    $assessment_component = $AssessmenttypeDB->getAssessmentbyTypeBySemester($type=0,$formData['idSemesterTo'],$semesterToCopyDetail[0]['IdScheme'],$r['FinalExam']);
								
	    		 	 			$IdMarksDistributionMaster = $r['IdMarksDistributionMaster'];
	    		 	 			
			    		 	 	//not exist start copy
			    		 	 	unset($r['IdMarksDistributionMaster']);		    		 	 	
			    		 	 	unset($r['ApprovedBy']);
			    		 	 	unset($r['ApprovedOn']);
			    		 	 	unset($r['RejectedBy']);
			    		 	 	unset($r['RejectedOn']);
			    		 	 	unset($r['FinalExam']);
			    		 	 	
			    		 	 	$r['IdComponentType']=$assessment_component['IdExaminationAssessmentType'];
			    		 	 	$r['semester']=$formData['idSemesterTo'];
			    		 	 	$r['IdProgram']=0; //$formData['idProgramTo'];
			    		 	 	$r['IdCourseTaggingGroup']=$formData['idGroupTo'];
			    		 	 	$r['IdCourse']=$formData['idSubjectTo'];
			    		 	 	$r['Status'] =0;
			    		 	 	$r['UpdUser']=$auth->getIdentity()->iduser;
			    		 	 	$r['UpdDate']= date ( 'Y-m-d H:i:s');
			    		 	 	
			    		 	 	//print_r($r);
			    		 	 	//insert main component
			    		 	 	$id = $markDistributionDB->addData($r);
			    		 	 	
			    		 	 	$item_list = $markDistributionDetDB->getListComponentItem($IdMarksDistributionMaster);
			    		 	 	
	    		 	 			//insert item component
			    		 	 	if(count($item_list)>0){
			    		 	 		foreach($item_list as $item){
			    		 	 			$item_to['IdMarksDistributionMaster']=$id;
			    		 	 			$item_to['idSemester']=$formData['idSemesterTo'];
			    		 	 			$item_to['ComponentName']=$item['ComponentName'];
			    		 	 			$item_to['Weightage']=$item['Weightage'];
			    		 	 			$item_to['TotalMark']=$item['TotalMark'];
			    		 	 			$item_to['Percentage']=$item['Percentage'];
			    		 	 			$item_to['Weightage']=$item['Weightage'];
			    		 	 			$item_to['idStaff']=$auth->getIdentity()->iduser;
			    		 	 			$item_to['UpdUser']=$auth->getIdentity()->iduser;
			    		 	 			$item_to['UpdDate']= date ( 'Y-m-d H:i:s');
			    		 	 			//print_r($item_to);
			    		 	 			$markDistributionDetDB->addData($item_to);
			    		 	 		}
			    		 	 	}
	    		 	 
	    		 	 	}
	    		 	 	
	    		 	 
	    		 	 	
	    		 	 	//redirect		
	    		 	 	$this->gobjsessionsis->flash = array('message' => 'Data has been copied', 'type' => 'success');	
	    		 	 	$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idGroup'=>$formData['idGroupTo'],'idProgram'=>$formData['idProgramTo'],'idSemester'=>$formData['idSemesterTo'],'idSubject'=>$formData['idSubjectTo']),'default',true));
			     		
	    		 	 }
	    		 }else{
	    		 	//No mark distribution available
	    		 	$this->gobjsessionsis->flash = array('message' => 'No mark distribution available', 'type' => 'notice');
	    		 		
	    		 }
	    		 
			}else{
				//redirect		
	    		$this->gobjsessionsis->flash = array('message' => 'Main setup by HOD for selected course does not exist.', 'type' => 'notice');
			}
    		
		}
    }
    
	public function getSubjectAction()
	{	
    	$idSemester = $this->_getParam('idSemester', 0);
    	$idProgram = $this->_getParam('idProgram', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
       	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
       	$result = $markDistributionDB->getListMainComponentSubject($idSemester,$idProgram,$idProgramScheme);
       	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
	public function ajaxGroupSubjectAction()
	{	
    	$idSemester = $this->_getParam('idSemester', 0);
    	$idProgram = $this->_getParam('idProgram', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
       	//get group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$result = $courseGroupDb->getCourseByLectGroup($idSemester,$idProgram);
				
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
	public function getGroupAction()
	{	
    	$idSemester = $this->_getParam('idSemester', 0);
    	$idProgram = $this->_getParam('idProgram', 0);
    	$idSubject = $this->_getParam('idSubject', 0);
    	$idGroupFrom = $this->_getParam('idGroupFrom', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
       	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$result = $courseGroupDb->getGroupCopy(array('IdSubject'=>$idSubject,'IdProgram'=>$idProgram,'IdSemester'=>$idSemester,'idGroupFrom'=>$idGroupFrom));
       	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
    
    function saveAction(){
    	$auth = Zend_Auth::getInstance();
    	
    	if ($this->getRequest()->isPost()) {
			
    		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup(); 		
 			$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
			
		
				$formData = $this->getRequest()->getPost();

				/*echo '<pre>';
			    echo print_r($formData);				
				exit;*/
				
				//delete checked
				if(isset($formData["idDel"])){
		    		 for($d=0; $d < count($formData["idDel"]); $d++){
					 	
					 	$idDel =  $formData["idDel"][$d];
					 	
					 	//delete component				
		    			$oCompitem->deleteData($idDel);
		    		 }
				}
				
				for($i=0; $i<count($formData['IdExaminationAssessmentType']); $i++){
				 	
					$IdExaminationAssessmentType = $formData['IdExaminationAssessmentType'][$i];
					
					$IdMarksDistributionMaster = $formData['IdMarksDistributionMaster'][$IdExaminationAssessmentType];
					
				 	$data["IdComponentType"]= $IdExaminationAssessmentType;			 			 
				 	$data["Marks"]= $formData["Raw"][$IdExaminationAssessmentType];
				 	$data["Percentage"]= $formData["Weightage"][$IdExaminationAssessmentType];	
				 	
				 	if($formData['btn-submit']=='Confirm'){
				 		$data['Status'] = 3; //Confirm
				 		
				 	}
				 	
					if(isset($formData["appeal"][$IdExaminationAssessmentType])){
				 		$data["allow_appeal"] = $formData["appeal"][$IdExaminationAssessmentType];
				 	}
				 	
				 	if(isset($formData["resit"][$IdExaminationAssessmentType])){
				    	$data["allow_resit"] = $formData["resit"][$IdExaminationAssessmentType];
				 	}				    
				 	
				 	
				 	if($IdMarksDistributionMaster){
				 		//echo 'update table	';			   
				    	$markDistributionDB->updateData($data,$IdMarksDistributionMaster);	
				 		
				 	}else{
				 		//insert into table					 		
						$data["semester"]  = $formData["idSemester"];
						$data["IdProgram"] = 0; //$formData["idProgram"];			
						$data["IdCourse"]  = $formData["idSubject"];	
						$data["IdCourseTaggingGroup"]  = $formData["idGroup"];			
						$data["Status"]    = 0;//Entry
						$data['UpdUser']   = $auth->getIdentity()->iduser;
						$data['UpdDate']   = date ( 'Y-m-d H:i:s');
						$data["TotalMark"] = $formData["total_mark"];				
				    	$IdMarksDistributionMaster = $markDistributionDB->addData($data);
				    	
				    } 
				   
				    //add item
		    		for($m=0; $m<count($formData['component_item_name'][$IdExaminationAssessmentType]); $m++){
		    			
		    			$IdDetails = $formData['IdMarksDistributionDetails'][$IdExaminationAssessmentType][$m];				    		
		    			$component_item_name = $formData['component_item_name'][$IdExaminationAssessmentType][$m];
		    							    			
		    			$data_item['ComponentName'] = $component_item_name;
		    			$data_item['IdMarksDistributionDetails'] = $IdDetails;
		    			$data_item['Weightage'] = $formData['itemRaw'][$IdExaminationAssessmentType][$m];
		    			$data_item['Percentage'] = $formData['itemWeightage'][$IdExaminationAssessmentType][$m];
		    			$data_item['TotalMark'] = $formData["Raw"][$IdExaminationAssessmentType];
		    			$data_item['UpdUser']   = $auth->getIdentity()->iduser;
						$data_item['UpdDate']   = date ( 'Y-m-d H:i:s');
						
						if(isset($formData["moodle_type"][$IdExaminationAssessmentType][$m]) && $formData["moodle_type"][$IdExaminationAssessmentType][$m]!=0){
							$data_item["moodle"] = 1;	
							$data_item['moodle_type']=$formData["moodle_type"][$IdExaminationAssessmentType][$m];	
						}else{
							$data_item["moodle"] = 0;
					 		$data_item['moodle_type']=0;
						}
			    		/*if(isset($formData["moodle"][$IdExaminationAssessmentType][$m])){
					 		$data_item["moodle"] = 1;					 		
					 		$data_item['moodle_type']=$formData["moodle_type"][$IdExaminationAssessmentType][$m];					 		
					 	}else{
					 		$data_item["moodle"] = 0;
					 		$data_item['moodle_type']=0;
					 	}*/
						
						if(isset($component_item_name) && $component_item_name!=''){
							if(isset($IdDetails) && $IdDetails!=''){
								echo 'updte';
								$data_item['IdMarksDistributionMaster']=$IdMarksDistributionMaster;	
								$oCompitem->updateData($data_item,$IdDetails);
							}else{
								echo 'add';
								$data_item['IdMarksDistributionMaster']=$IdMarksDistributionMaster;	
								$data_item['idSemester']=$formData["idSemester"];
			    				$oCompitem->addData($data_item);
			    			 	
							}	
							//print_r($data_item);			
						}		
		    		}//end for
				
				 
				   
			    }//end for
			   
			    
    			if($formData['btn-submit']=='Confirm'){
    				
    				
    				//coordinator profile
    				$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    				$recepient = $groupDB->getCoordinatorProfile($formData["idSubject"]);    				
    				
    				//get group info			
    				$group = $groupDB->getInfo($formData["idGroup"]);
    				
    				//lecturer profile  
    				$userDb = new GeneralSetup_Model_DbTable_User();			  				
    				$sender = $userDb->getstaffdetails($group['IdLecturer']);
    				
    				
    				$data['semester'] = $group['semester_name'];
    				$data['subject_code'] = $group['subject_code'];
    				$data['subject_name'] = $group['subject_name'];
    				$data['group'] = $group['GroupCode'].' - '.$group['GroupName'];
    				
    				$cms = new Cms_SendMail();
			 		$cms->ExamSendMail(118,$sender,$recepient,$data);			 		
			 	}
				
				
		  		//redirect
		  		$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idGroup'=>$formData["idGroup"]),'default',true));
		   
		
    		}//end if
    }
    
    
    public function viewAppAction(){
    	
    	$this->_helper->layout->disableLayout();
    	
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	$this->view->idGroup = $idGroup;
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);    	
        
    	//get group info
    	$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    	$this->view->group = $groupDB->getInfo($idGroup);
    	
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();    	
    	//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject,$idGroup);	    
    
    	//print_r($list_component);
    	
    	$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	
    }
    public function mdlpushAction(){
    	$moodledb = new GeneralSetup_Model_DbTable_Moodle();
    	$mddb = new Examination_Model_DbTable_Marksdistributiondetails();
    	$grpdb = new GeneralSetup_Model_DbTable_CourseGroup();
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	$idmarkdis = $this->_getParam('idmarkdis');
    	$mdtype = $this->_getParam('mdtype');
    	$idcomp= $this->_getParam('idcomp');
    	
    	$mdd=$mddb->getDataComponentItem($idmarkdis);
    	//print_r($mdd);exit;
    	$grp = $grpdb->getInfo($idGroup);
    	$mdgrpname = explode(" ",$grp["GroupName"]);
    	if($mdtype==1){
    		$moodledb->create_quiz($idSemester,$idSubject,$grp["IdLecturer"],$idGroup,$idmarkdis,$idcomp,$mdd["ComponentName"]." - ".$mdgrpname[0],"",$mdd["Weightage"],$mdd["Percentage"]);
    	}elseif($mdtype==2){
    		$moodledb->createAssignment($idSemester,$idSubject,$grp["IdLecturer"],$idGroup,$idmarkdis,$idcomp,$mdd["ComponentName"]." - ".$mdgrpname[0],"",$mdd["Weightage"],$mdd["Percentage"]);
    	}
    	$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'view','idSemester'=>$idSemester,'idProgram'=>$idProgram,'idSubject'=>$idSubject,'idGroup'=>$idGroup,"msg"=>"moodledone"),'default',true));
    	exit;
    }
    
    public function getStatus($status){
    	
    	switch ($status){
    		
    		case 0 : $status = 'Entry'; break;
    		case 1 : $status = 'Approved'; break;
    		case 2 : $status = 'Revise'; break;
    		case 3 : $status = 'Confirm'; break;
    	}
    	
    	return $status;
    }
    
    
    public function revertAction(){
    	
    	$this->view->title=$this->view->translate("Copy Mark Distribution");
    	
    	$auth = Zend_Auth::getInstance();
    	   	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idGroup = $this->_getParam('idGroup');
    	
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$markDistributionDetDB =  new Examination_Model_DbTable_Marksdistributiondetails();
    	
    	//check if group belong to this lecturer
    	if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin	  
    		
    		$groupDB = new GeneralSetup_Model_DbTable_CourseGroup();
    		$isMyGroup = $groupDB->isMyGroup($idGroup);
    		
    		if(!$isMyGroup){
    			$this->gobjsessionsis->flash = array('message' => 'You are not the owner of the course section.', 'type' => 'error');
    			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'index-my'),'default',true));
    		}
    	}
    	
    	
    	//check if already do mark entry
    	$entry = $markDistributionDB->isMarkEntered($idGroup);
    	if($entry){
    		$this->gobjsessionsis->flash = array('message' => 'You are not allowed to revert mark distribution.', 'type' => 'error');
    		if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin	  
    			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'index-my'),'default',true));
    		}else{
    			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'index'),'default',true));
    		}
    	}
    	
    	
    	//get mark distribution info
    	$list_component = $markDistributionDB->getListMainComponentByGroup($idGroup);
    	
    	foreach($list_component as $c){
    		//keep history    		
    		$c['createddt'] = date ( 'Y-m-d H:i:s');
    		$c['createdby'] = $auth->getIdentity()->iduser;
    		$markDistributionDB->addHistory($c);
    		
    		//get detailz
    		$item = $markDistributionDetDB->getListComponentItem($c['IdMarksDistributionMaster']);
    		
    		if(count($item)>0){
	    		foreach($item as $i){
	    			//keep history  
	    			unset($i['FullName']);
	    			$i['createddt'] = date ( 'Y-m-d H:i:s');
	    			$i['createdby'] = $auth->getIdentity()->iduser;	    			
	    		  	$markDistributionDetDB->addHistory($i);
	    		}
    		}
    	}
    	
    	//update
    	$data['Status'] = 0;
    	$data['ApprovedBy'] = null;
    	$data['ApprovedOn'] = null;
    	$markDistributionDB->revertStatus($data,$idGroup);
    
    	$this->gobjsessionsis->flash = array('message' => 'Revert successfull.', 'type' => 'success');
    	if($auth->getIdentity()->IdRole==1 || $auth->getIdentity()->IdRole==455){ //admin	atau asad admin	    		
    		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'index','idSemester'=>$idSemester,'idProgram'=>$idProgram,'idSubject'=>$idSubject,'idGroup'=>$idGroup),'default',true));
    	}else{
    		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution', 'action'=>'index-my','idSemester'=>$idSemester,'idProgram'=>$idProgram,'idSubject'=>$idSubject,'idGroup'=>$idGroup),'default',true));
    	}
    	
    }
   
}