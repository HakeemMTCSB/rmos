<?php 
class Examination_AssignExamCenterController extends Zend_Controller_Action {
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate("Exam Center : Assign Student to Exam Center");
			
		$session = Zend_Registry::get('sis');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
				
		$form = new Examination_Form_ExamCenterSearch(array ('locale' => $locale));
		$this->view->form = $form;
		
			//clear the session
	    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
				unset($session->result);
	    	}
		
			if ($this->_request->isPost()){
			
				if($form->isValid($_POST)) {
					
					$formData = $this->_request->getPost ();
					
					$session->result = $formData;
					
					$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];					
					//echo '<pre>';
					//print_r($formData);
					
					$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
					$examRegDB = new Registration_Model_DbTable_ExamRegistration();
					
					$ec_list = $examCenterDB->getExamCenterList($formData);
					
					foreach($ec_list as $index=>$center){
						
						$student = $examRegDB->getStudentRegister($center['ec_id'],$center['er_idSemester'],$center['er_idSubject']);
						
						if(count($student)>0){
							$total_student = count($student);
						}else{
							$total_student= 0;
						}
						$ec_list[$index]['total_student'] = $total_student;
					}
					$this->view->ec_list = $ec_list;
					
					//print_r($ec_list);
				}
				
			}
				
	}
	
	
	public function addAction() {
		
		$this->view->title = $this->view->translate("Assign Student to Exam Center : Search");
			
		$session = Zend_Registry::get('sis');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
				
		$form = new Examination_Form_SearchStudentByEc(array ('locale' => $locale));
		$this->view->form = $form;
		
			//clear the session
	    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
				unset($session->result_assign);
	    	}
    	
			if ($this->_request->isPost()){
			
				if($form->isValid($_POST)) {
					
					$formData = $this->_request->getPost ();
					
					$session->result_assign = $formData;
					
					$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];
					$this->view->idSubject = $formData['IdSubject'];
					$this->view->idCountry = $formData['idCountry'];
					$this->view->idCity = $formData['idCity'];
					
					$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
					$this->view->exam_center = $examCenterDB->getExamCenter($formData['idCountry'], $formData['idCity']);
		
					$examRegDB = new Examination_Model_DbTable_ExamRegistration();
	    			$center = $examRegDB->getRegisteredNoEc($formData);

	    			foreach($center as $index=>$c){
	    				//get total register student
	    				$total_student = $examRegDB->getTotalRegisteredNoEc($c['er_idCountry'],$c['er_idCity'],$c['er_idSemester'],$c['er_idProgram'],$c['er_idSubject']);
	    				$center[$index]['total_student']=$total_student;
	    				
	    				//get total register student with EC
	    				$total_assigned_student = $examRegDB->getTotalAssignedEc($c['er_idCountry'],$c['er_idCity'],$c['er_idSemester'],$c['er_idProgram'],$c['er_idSubject']);
	    				$center[$index]['total_assign_student']=$total_assigned_student;
	    			}
	    			$this->view->center_list = $center;
	    			
				}
				
			}else{
				
				//populate by session 
		    	if (isset($session->result_assign) && $session->result_assign['IdSemester']!='' && $session->result_assign['IdProgram']!='' && $session->result_assign['idCountry']!='' && $session->result_assign['idCity']!='') {    	
		    		
		    		$form->populate($session->result_assign);
		    		
		    		$formData = $session->result_assign;	
		    		
		    		$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];
					$this->view->idSubject = $formData['IdSubject'];
					$this->view->idCountry = $formData['idCountry'];
					$this->view->idCity = $formData['idCity'];
					
					$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
					$this->view->exam_center = $examCenterDB->getExamCenter($formData['idCountry'], $formData['idCity']);
		
					$examRegDB = new Examination_Model_DbTable_ExamRegistration();
	    			$center = $examRegDB->getRegisteredNoEc($formData);
	    			
	    			foreach($center as $index=>$c){
	    				//get total register student
	    				$total_student = $examRegDB->getTotalRegisteredNoEc($c['er_idCountry'],$c['er_idCity'],$c['er_idSemester'],$c['er_idProgram'],$c['er_idSubject']);
	    				$center[$index]['total_student']=$total_student;
	    				
	    				//get total register student with EC
	    				$total_assigned_student = $examRegDB->getTotalAssignedEc($c['er_idCountry'],$c['er_idCity'],$c['er_idSemester'],$c['er_idProgram'],$c['er_idSubject']);
	    				$center[$index]['total_assign_student']=$total_assigned_student;
	    			}
	    			$this->view->center_list = $center;
		    	}
			}
						
			
    		//echo '<pre>';
    		//print_r($center);
    		
			$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
			$this->view->country = $examCenterDB->getExamCenterCountry();
		
	}
	
	
	
	public function getSubjectAction()
	{	
    	$idProgram = $this->_getParam('idProgram', 0);
    	 	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $db = Zend_Db_Table::getDefaultAdapter();
        
		$progDB= new GeneralSetup_Model_DbTable_Program();
		$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		
     	$activeLandscape=$landscapeDB->getAllActiveLandscape($idProgram);
     	
     	$rows = array();
     	$allsemlandscape = array();
     	
     	//buat macam ni nak cater multiple landscape type (in future)
		foreach($activeLandscape as $i=>$actl){					
			if($actl["LandscapeType"]==43){
				$allsemlandscape[$i] = $actl["IdLandscape"];
				$i++;
			}
		}
		
		if(is_array($allsemlandscape)){
			    	
	    	  $lstrSelect = $db->select()
	    	  				->distinct()
			 				 ->from(array("ls"=>'tbl_landscapesubject'),array())		
			 				 ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject ',array('IdSubject','SubjectName','SubCode','CreditHours','key'=>'IdSubject','name'=>'subjectMainDefaultLanguage'))			 				
	                         ->join(array("ld"=>"tbl_landscape"),"ld.IdLandscape=ls.IdLandscape",array("ProgramDescription"))
			 				 ->group("ls.IdSubject")
			 				 ->order("s.SubCode");			 				 
			 				 
				foreach ($allsemlandscape as $landscape) {
				 	$lstrSelect->orwhere("ls.IdLandscape = ?",$landscape);
				} 				
				
			 	$rows = $db->fetchAll($lstrSelect);
		}
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($rows);
		
		echo $json;
		exit();
    }
    
    
	public function assignStudentAction() {
		
		$this->view->title = $this->view->translate("Assign Student");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		$idCountry = $this->_getParam('idCountry',null);
		$idCity = $this->_getParam('idCity',null);
		$idSemester = $this->_getParam('idSemester',null);
		$idProgram = $this->_getParam('idProgram',null);
		$idSubject = $this->_getParam('idSubject',null); 
				
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
		$this->view->exam_center = $examCenterDB->getExamCenter($idCountry, $idCity);
		
		$examRegDB = new Examination_Model_DbTable_ExamRegistration();
		$student_list = $examRegDB->getStudentNoEc($idCountry,$idCity,$idSemester,$idProgram,$idSubject);
		$this->view->student_list = $student_list;
		
		//echo '<pre>';
		//print_r($student_list);
	}
	
	
	public function saveAction() {
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();	

			//echo '<pre>';
			//print_r($formData);
			$examRegDB = new Examination_Model_DbTable_ExamRegistration();
			
			for($i=0; $i<count($formData['er_id']); $i++){
				$er_id = $formData['er_id'][$i];
				$where = 'er_id='.$er_id;
				$examRegDB->updateCenterData(array('er_ec_id'=>$formData['ec_id']),$where);
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');		
		}
	
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'assign-exam-center', 'action'=>'add'),'default',true));
		
	}
}

?>