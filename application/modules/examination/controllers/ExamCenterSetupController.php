<?php 
class Examination_ExamCenterSetupController extends Base_Base {

	    
	public function indexAction(){
			
		$this->view->title=$this->view->translate("Exam Center Setup");
    
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;	
		
		$form = new Examination_Form_ExamCenterSetup();
		$this->view->form = $form;
		
					
		$examSetupDB = new Examination_Model_DbTable_ExamCenterSetup();
		$examRegistrationDB = new Examination_Model_DbTable_ExamRegistration();
			
		
		//get semseter
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->view->semester_list = $semesterDB->fnGetSemesterList();
		
		if ($this->_request->isPost() && $this->_request->getPost ('Search')){
			
			$formData = $this->_request->getPost ();
			$this->view->data = $formData;

			if($form->isValid($formData)){
				
				$form->populate($formData);
				
				//search exam center
				$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
				$examcenter_list = $examCenterDB->searchSetupExamCenter($formData);

				if($formData['status']==2){
					foreach($examcenter_list as $index=>$eclist){
						//get total no of student have been assign
						$student_assigned = $examRegistrationDB->getStudentByEc($eclist['ec_id'],$formData['idSemester']);
						$examcenter_list[$index]['total']=count($student_assigned);
					}
				}
				$this->view->examcenter_list = $examcenter_list;
				
			}
			
		}
		
		
		if ($this->_request->isPost() && $this->_request->getPost ('save') ){
			
			$auth = Zend_Auth::getInstance();
			
			$formData = $this->_request->getPost ();	
			
			if(isset($formData['ec_id'])){				
			
				for($i=0; $i<count($formData['ec_id']); $i++){
					
					$ec_id = $formData['ec_id'][$i];
					$ec_country = $formData['ec_idCountry'][$ec_id];
					$ec_city = $formData['ec_idCity'][$ec_id];
					$ec_city_others = $formData['ec_cityOthers'][$ec_id];
					
					
					//assign to semester
					if($formData['Status']==1){
						
						//assigning exam center to semester
						$data['ecs_publish']=$formData['ecs_publish'][$ec_id];						
						$data['idSemester']=$formData['IdSemester'];
						$data['ec_id']=$ec_id;
						$data['ecs_createddt']=date ( 'Y-m-d H:i:s');
						$data['ecs_createdby']=$auth->getIdentity()->iduser;							
						$examSetupDB->addData($data);
						
						
											
						//assign student to exam center
						$student_list = $examRegistrationDB->getStudentNoExamCenter($ec_country,$ec_city,$formData['IdSemester'],$ec_city_others);
						if(count($student_list)>0){
							foreach($student_list as $student){
								$where = 'er_id='.$student['er_id'];
								$examRegistrationDB->updateCenterData(array('er_ec_id'=>$ec_id),$where);
							}
						}
						
					}
					
					
					//remove from semester
					if($formData['Status']==2){
						
						//un-assign student from exam center
						$student_list2 = $examRegistrationDB->getStudentByEc($ec_id,$formData['IdSemester']);
						if(count($student_list2)>0){
							foreach($student_list2 as $student2){
								$where = 'er_id='.$student2['er_id'];
								$examRegistrationDB->updateCenterData(array('er_ec_id'=>null),$where);
							}
						}
											
						//remove un-assign exam center from semester
						$where = 'idSemester='.$formData['IdSemester'].' AND ec_id='.$ec_id;
						$examSetupDB->deleteData($where);				
					}
					
				}//end for
			
			}//end if isset
			
			//Screen unassign/remove 
			if($formData['Status']==2){
				
				//tapi xnak remove just nak enable/disabled publish
				if(isset($formData['ecs_publish']) && count($formData['ecs_publish'])>0){
					foreach($formData['ecs_publish'] as $key=>$value){										
						$examSetupDB->updateData(array('ecs_publish'=>$value),$key);
					}
				}				
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been update', 'type' => 'success');
			
			$info = array('idSemester'=>$formData['IdSemester'],'idCountry'=>$formData['IdCountry'],'idCity'=>$formData['IdCity'],'status'=>$formData['Status']);
			$form->populate($info);
			$this->view->data = $info;
			
			//search exam center
			$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
			$examcenter_list = $examCenterDB->searchSetupExamCenter($info);		
			if(count($examcenter_list)>0){
				foreach($examcenter_list as $index=>$eclist){
					//get total no of student have been assign
					$student_assigned = $examRegistrationDB->getStudentByEc($eclist['ec_id'],$formData['IdSemester']);
					$examcenter_list[$index]['total']=count($student_assigned);
				}	
			}	
			$this->view->examcenter_list = $examcenter_list;
			
		}
		
	
	}
	
		
	public function getCityAction()
	{	
    	$idCountry = $this->_getParam('idCountry', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
        $result = $examCenterDB->getExamCenterCity($idCountry);
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
}