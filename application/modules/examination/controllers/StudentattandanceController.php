<?php
class Examination_StudentattandanceController extends Base_Base {

	private $lobjstudentattandance;
	private $lobjStudentattandanceForm;
	private $lobjsemestermaster;
	private $lobjinitialconfig;
	private $lobjstudentapplication;

	public function init() {
		$this->fnsetObj();
	}

	public function fnsetObj(){

		$this->lobjStudentattandanceForm = new Examination_Form_Studentattandance();
		$this->lobjstudentattandance = new Examination_Model_DbTable_Studentattandance();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster ();
	}

	public function indexAction() {

		$idUniversity =$this->gobjsessionsis->idUniversity;
		$this->view->config = $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);
		$this->view->lobjform = $this->lobjform;
		$this->view->results = $larrresult= array();
		$this->view->lobjStudentattandanceForm = $this->lobjStudentattandanceForm;



		$lobjsemester = $this->lobjsemestermaster->fnGetSemestermasterList();
		$this->view->lobjform->field8->addMultiOptions($lobjsemester);

		$larStudentNameCombo = $this->lobjstudentattandance->fngetStudentNameCombo();
		$this->view->lobjform->field1->addMultiOptions($larStudentNameCombo);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {

				$this->view->results = $larrresult = $this->lobjstudentattandance->fnSearchStudentAttandance( $this->lobjform->getValues ()); //searching the values for the user
					
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			$ldtsystemDate = date ( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance();
			$count=count($larrformData['IdStudentRegistration']);

			for($i=0;$i<$count;$i++) {

				$larrformDatas ['IdStudentRegistration'] = $larrformData['IdStudentRegistration'][$i];
				$larrformDatas ['IdSemester'] = $larrformData['IdSemester'];
				if(@$larrformData['attandance'][$larrformData['IdStudentRegistration'][$i]][$i])
					$larrformDatas ['AttandanceStatus'] = 1;
				$larrformDatas ['UpdDate'] = $ldtsystemDate;
				$larrformDatas ['UpdUser'] = $auth->getIdentity()->iduser;
					
				$larrstudentreg = $this->lobjstudentattandance->fnAddStudentAttandance($larrformDatas);
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/studentattandance/index');
		}

	}
}