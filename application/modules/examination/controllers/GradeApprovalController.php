<?php

class examination_GradeApprovalController extends Base_Base { //Controller for the User Module

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Grade Approval - Group List");
		 
	
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		//faculty
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
		
		if($this->_sis_session->IdRole == 1){
			$collegeList = $collegeDb->getCollege();
		}else{
			$this->view->default_faculty = $this->_sis_session->idCollege;
			$collegeList = array('0'=>$collegeDb->fngetCollegemasterData($this->_sis_session->idCollege));
		}
		
		$this->view->college_list = $collegeList;
		
	}
	
	
	public function studentListAction(){
		
			//title
		$this->view->title= $this->view->translate("Grade Approval - Student List");
		
		$id = $this->_getParam('id', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$student = $this->_getParam('student',null);
		$msg = $this->_getParam('msg', null);
		
		$this->view->idGroup = $id;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Mark has been successfully approved");
		}
		
		
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
		
		//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($id);			
		$this->view->group = $group;
		
		//student list
		$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$studentList = $courseGroupStudentDb->getStudentbyGroup($id,$student);
		
		//get academic staff for approval purpose
		$staffDb = new GeneralSetup_Model_DbTable_Staffmaster();
	  	$this->view->staff = $staffDb->getAcademicStaff();  
		
	  	if(count($studentList)>0){
	  		$subjectRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			foreach($studentList as $index=>$student){
				
				//get subject registration info				
				$subject  =  $subjectRegDB->getSemesterSubjectStatus($idSemester,$student["IdStudentRegistration"],$idSubject);
				
				$studentList[$index]["IdStudentRegSubjects"] = $subject["IdStudentRegSubjects"];
				$studentList[$index]["student_mark"] = $subject["final_course_mark"];
				$studentList[$index]["grade_point"] = $subject["grade_point"];
				$studentList[$index]["grade_name"] = $subject["grade_name"];
				$studentList[$index]["grade_desc"] = $subject["grade_desc"];
				$studentList[$index]["grade_status"] = $subject["grade_status"];
				$studentList[$index]["exam_status"] = $subject["exam_status"];
				$studentList[$index]["approvedby"] = $subject["mark_approveby"];
				$studentList[$index]["approveddt"] = $subject["mark_approvedt"];
				
			}
	  	}
		$this->view->student_list = $studentList;
	}
	
	
 	public function saveApprovalAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$subjectRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();    	    	
    	$examCms = new Cms_ExamCalculation();
    	$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			  	    			    		
    		for($i=0; $i<count($formData['idStudentRegSub']); $i++){

    			$IdStudentRegSubjects =  $formData['idStudentRegSub'][$i];
    			$exam_status =  $formData['exam_status'][$IdStudentRegSubjects];
    			      			
    			if($exam_status!='IN' && $exam_status!='MG' && $exam_status!='F' && $exam_status!='NR'){    				
	    			$data["exam_status"]='C';
	    			$data["exam_status_updateby"]=$auth->getIdentity()->iduser;
    				$data["exam_status_updatedt"]=date('Y-m-d H:i:s');  	
    			}
    			    			
    			$data["mark_approveby"]=$formData["ApprovedBy"];
	    		$data["mark_approvedt"]=date('Y-m-d H:i:s'); 
	    		$subjectRegDB->updateData($data, $IdStudentRegSubjects);
	    		
	    		
    			//update parent mark   
    			$student = $subjectRegDB->getStudentDataLandscape($IdStudentRegSubjects);    			
    			if($student["LandscapeType"]==44){//block    		    
	    			$info['IdLandscape']=$student["IdLandscape"];
	    			$info['IdSubject']=$student["IdSubject"];
	    			$info['IdStudentRegistration']=$student["IdStudentRegistration"];
	    			$info["IdProgram"]=$student["IdProgram"];
	    			$info["IdSemester"]=$student["IdSemester"];
	    						
	    			$parent_grade = $examCms->getParentGrade($info);
    			}
    		}
    		   		
    	}
    		
    	//redirect
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'grade-approval', 'action'=>'student-list','idSemester'=>$formData["idSemester"],'id'=>$formData["idGroup"],'idSubject'=>$formData["idSubject"],'msg'=>1),'default',true));
		  
    }
    
   
}