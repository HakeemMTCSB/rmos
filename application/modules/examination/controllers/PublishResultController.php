<?php

class examination_PublishResultController extends Base_Base { //Controller for the User Module

		
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Publish Exam Result");
		 
		$session = Zend_Registry::get('sis');
		
		$msg = $this->_getParam('msg', null);
				
		$publishDb = new Examination_Model_DbTable_PublishMark();
		
		$form = new Examination_Form_PublishResultSearchForm();
				
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$session->result = $formData;
			 
			if ($form->isValid($formData)) {
				$form->populate($formData);				
				$this->view->publish_data = $publishDb->getPublishResultData($formData["IdProgram"],$formData["IdSemester"]);
			}
			
		}else{ 
    		
    		//populate by session 
	    	if (isset($session->result)) {    	
	    		$form->populate($session->result);
	    		$formData = $session->result;			
	    		$this->view->publish_data = $publishDb->getPublishResultData($formData["IdProgram"],$formData["IdSemester"]);
	    	}
    		
    	}
		
		$this->view->form = $form;
		
		
		//program
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();	
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemesterList();
		$this->view->semester_list = $semesterList;
		
	}
	
	
	public function saveAction() {
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
						
			$data["pm_idSemester"] = $formData["idSemester"];			
			$data["pm_type"] = 2;
			$data["pm_category"] = $formData['category'];
			$data["pm_date"] = date('Y-m-d',strtotime($formData["DisplayDate"]));
			$data["pm_time"] = $formData["DisplayTime"];
			
			$publishDb = new Examination_Model_DbTable_PublishMark();
			$stack = array();
			
			for($i=0; $i<count($formData["idProgram"]); $i++){
				
				$idProgram = $formData["idProgram"][$i];
				
				$publish_data = $publishDb->getPublishResult($idProgram,$formData["idSemester"],$formData['category']);
				
				if(is_array($publish_data)){
					//echo 'duplicate';
					$flag = 1; //duplicate			
					array_push($stack,$idProgram);							
				}else{
					//echo 'add';
					//$msg =1;
					$data["pm_idProgram"] = $idProgram;	
					$publishDb->addData($data);
										
				}
			
			}
			
			if($flag){				
				$message = "Fail to add info for ".count($stack)." program(s).Duplicate entry is not allowed";
				$this->gobjsessionsis->flash = array('message' => $message, 'type' => 'success');
			}else{
				$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			}
			
			
			
		}
		
		
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'publish-result', 'action'=>'index'),'default',true));
	}		
		
	
	public function deleteAction(){
		
		$id = $this->_getParam('id', null);
		
		$publishDb = new Examination_Model_DbTable_PublishMark();
		$publishDb->deleteData($id);
		
		$this->gobjsessionsis->flash = array('message' => 'Data has been deleted', 'type' => 'success');
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'publish-result', 'action'=>'index','msg'=>3),'default',true));
	}
}