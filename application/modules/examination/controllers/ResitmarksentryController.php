<?php
class examination_ResitmarksentryController extends Base_Base { //Controller for the User Module

	private $lobjSubjectmaster;
	private $lobjprogram;
	private $lobjStudentRegModel;
	private $lobjsemester;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjResitmarksentry =  new Examination_Model_DbTable_Resitmarksentry();
		$this->lobjremarkconfig = new Examination_Model_DbTable_Remarkingconfig();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjstatus = new Examination_Model_DbTable_Remarkingconfig();
	}


	/**
	 * Function to search students
	 * @author: VT
	 */
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		// SHOW THE COURSES
		$larrresultCourses = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field23->addMultiOptions( $larrresultCourses );

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field24->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramName']);
		}

		// SHOW THE SEMESTER
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}

		//SHOW THE MODE
		$applicationsource = $this->lobjdeftype->fnGetDefinationMs('Source Of Application');
		$this->view->lobjform->field1->addMultiOptions($applicationsource);

		//SHOW THE STATUS
		$larrstatusresult = $this->lobjstatus->showstatus();
		foreach($larrstatusresult as $larrstatusvalues) {
			$this->view->lobjform->field25->addMultiOption($larrstatusvalues['key'],$larrstatusvalues['value']);
		}

		$this->view->lobjform->Search->setAttrib('Onclick','return formsubmit();');

		$larrresult = $this->lobjResitmarksentry->fnSearchResitMarksEntry( $post = NULL);

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Remarkingpaginatorresult);


		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Remarkingpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Remarkingpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjResitmarksentry->fnSearchResitMarksEntry( $larrformData );
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Remarkingpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/resitmarksentry');
		}
	}



	/**
	 * Function to ADD examiner1 marks
	 * @author: VT
	 */

	public function addresitmarksentryAction() {
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		$this->view->iduser = $auth->getIdentity()->iduser;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$getDetailsConfig = $this->lobjremarkconfig->getConfigDetails($IdUniversity);
		$IdExaminationResitApplication = $this->_getParam ( 'id' );

		$larrresult = $this->lobjResitmarksentry->getEntryDetailByID( $IdExaminationResitApplication );
		$this->view->resultResit = $larrresult;
		//$SemesterCode = $larrresult[0]['SemesterCode'];
		//$idstudent = $larrresult[0]['IdStudent'];
		//$IdProgram = $larrresult[0]['IdProgram'];
		$IdStudentRegID = $larrresult[0]['StudentCode'];
		$studentProfileStatus = $this->lobjStudentRegModel->fnStudentProfileStatus($IdStudentRegID);
		if($studentProfileStatus=='92' || $studentProfileStatus=='248' || $studentProfileStatus=='253') { //active, defer and dormant
			$this->view->disableSubmit = '1';
		} else {
			$this->view->disableSubmit = '0';
		}


		// get the all staffs
		$larrresultSubjectStaffs = $this->lobjResitmarksentry->fnViewstaffsubject( );
		foreach($larrresultSubjectStaffs as $larrstaffvalues) {
			$this->view->lobjform->field23->addMultiOption($larrstaffvalues['key'],$larrstaffvalues['value']);
		}

		$larrresultAppealComponentData = $this->lobjResitmarksentry->fnSearchMarksEntryDetails( $IdExaminationResitApplication );
		$this->view->finalResult = $larrresultAppealComponentData;

		// get the staff registered
		$larrResultStaffReg = $this->lobjResitmarksentry->fngetStaffReg( $IdExaminationResitApplication );
		$this->view->lobjform->field23->setValue($larrResultStaffReg[0]['IdExaminer1']);


		// get the status of Application
		$larrresultApplicationEntryStatus = $this->lobjResitmarksentry->fnApplicationEntryStatus( $IdExaminationResitApplication );
		if(count($larrresultApplicationEntryStatus)>0) {
			$this->view->larrresultApplicationEntryStatus = $larrresultApplicationEntryStatus;

			if($larrresultApplicationEntryStatus[0]['Status']!='192') {
				$this->view->lobjform->field23->setAttrib('readOnly','readOnly');
			}
		}


		if ($this->_request->isPost () ) {
			$larrformData = $this->_request->getPost ();
			$status = '193'; // Entry
			$this->lobjResitmarksentry->fnInsertmarks($larrformData,$status,$getDetailsConfig,$IdExaminationResitApplication);
			$this->_redirect( $this->baseUrl . '/examination/resitmarksentry');
		}

	}


}
