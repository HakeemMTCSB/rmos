<?php

class Examination_MarkDistributionSetupController extends Base_Base { //Controller for the User Module

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution Setup - Search Course");
    	
    	$session = Zend_Registry::get('sis');
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	
    	$form = new Examination_Form_MarkDistributionSetupSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    	
    	$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
    	
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	    	
    	//search
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Search') ) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->idSemester = $formData["IdSemester"];
			$this->view->idProgram = $formData["IdProgram"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);							
				
				//get subject under department selected	
				$data = $setupDb->searchMarkDistSetupList(1,$formData);				
				$this->view->list_data = $data;		
								
				$session->result = $formData;
			}//if form valid
			
    	}else{ 
    		
    		//populate by session 
	    	if (isset($session->result['IdProgram']) && isset($session->result['IdSemester'])) {    	
	    		$form->populate($session->result);		

	    		$this->view->idSemester = $session->result['IdSemester'];
				$this->view->idProgram = $session->result['IdProgram'];
			
	    		$data = $setupDb->searchMarkDistSetupList(1,$session->result);				
				$this->view->list_data = $data;			
	    	}
    		
    	}	
    	
    	
    	
    	
    }
    
	public function manageAction(){
    	
		$this->view->title=$this->view->translate("Mark Distribution Setup : Add");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$idSemester = $this->_getParam('idSemester');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idProgram = $this->_getParam('idProgram');
    	
    	$this->view->idSemester = $idSemester;    
    	$this->view->idSubject = $idSubject;
    	$this->view->idProgram = $idProgram;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	$this->view->semester = $semester;
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);    	
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    
		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
					
		//get component
    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
		$assessment_list = $AssessmenttypeDB->getAssessmentbyType($type=0,$idSemester,$semester[0]['IdScheme'])	;

		foreach($assessment_list as $index=>$list){
			//get info mds
			$detail = $setupDb->getLatestSemesterDetails(1,$list['IdExaminationAssessmentType'],$idSemester,$idSubject);					
			$assessment_list[$index]['mds_min_weightage']= $detail['mds_min_weightage'];
			$assessment_list[$index]['mds_max_weightage']= $detail['mds_max_weightage'];
		}		

		$this->view->list_component = $assessment_list;  
		
		//print_r($assessment_list);
		
		if ($this->getRequest()->isPost() && $this->_request->getPost('btn-submit')) {
		
    		$formData = $this->getRequest()->getPost();	    		
    	
    		$data['mds_user_type']=1;
			$data['mds_semester']=$formData['idSemester'];
			$data['mds_program']=$formData['idProgram'];	
			$data['mds_course']=$formData['idSubject'];			
			$data['mds_createdt']=date ( 'Y-m-d H:i:s');
			$data['mds_createdby']=$auth->getIdentity()->iduser;
			
			for($i=0; $i<count($formData['IdExaminationAssessmentType']); $i++){
				$IdExaminationAssessmentType = $formData['IdExaminationAssessmentType'][$i];
				
				$data['mds_assessmenttypeid']=$IdExaminationAssessmentType;				
				$data['mds_min_weightage']=$formData['MinWeightage'][$IdExaminationAssessmentType];
				$data['mds_max_weightage']=$formData['MaxWeightage'][$IdExaminationAssessmentType];
				
				//add				
				$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
				$setupDb->addData($data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup', 'action'=>'edit','idProgram'=>$formData["idProgram"],'idSemester'=>$formData["idSemester"],'idSubject'=>$formData["idSubject"]),'default',true));
    	}
    	
    }
    
    
	public function editAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution Setup : Edit");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idProgram = $this->_getParam('idProgram');
    	
    	$this->view->idSemester = $idSemester;    
    	$this->view->idSubject = $idSubject;
    	$this->view->idProgram = $idProgram;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	$this->view->semester = $semester;
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);   
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
    	//get component
    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
		//$assessment_list = $AssessmenttypeDB->fngetAllassessmentType();
		$assessment_list = $AssessmenttypeDB->getAssessmentbyType($type=0,$idSemester,$semester[0]['IdScheme']);	  
		
		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
		foreach($assessment_list as $index=>$list){
			//get info mds
			$detail = $setupDb->getDetails(1,$list['IdExaminationAssessmentType'],$idSemester,$idSubject,$idProgram);
			$assessment_list[$index]['mds_id']= $detail['mds_id'];
			$assessment_list[$index]['mds_min_weightage']= $detail['mds_min_weightage'];
			$assessment_list[$index]['mds_max_weightage']= $detail['mds_max_weightage'];
		}		
		$this->view->list_component = $assessment_list;  
    
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$data['mds_modifydt']=date ( 'Y-m-d H:i:s');
			$data['mds_modifyby']=$auth->getIdentity()->iduser;
			
			for($i=0; $i<count($formData['mds_id']); $i++){
				
				$mds_id = $formData['mds_id'][$i];
				$data['mds_min_weightage']=$formData['MinWeightage'][$mds_id];
				$data['mds_max_weightage']=$formData['MaxWeightage'][$mds_id];
				
				//add				
				$setupDb->updateData($data,$mds_id);
				
			}
			
				$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup', 'action'=>'edit','idProgram'=>$formData["idProgram"],'idSemester'=>$formData["idSemester"],'idSubject'=>$formData["idSubject"]),'default',true));
		}
    	
    }
    
    
	public function searchAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution Setup - Lead Lecturer : Search Course");
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	
    	$form = new Examination_Form_MarkDistributionSetupHop(array('locale'=>$locale));
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idSemester = $formData["IdSemester"];
			$this->view->idProgram = $formData["IdProgram"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get added mark distribution
			    $setupDb = new Examination_Model_DbTable_MarkDistributionSetup();					
				//$data = $setupDb->searchtMarkDistributionList(2,$formData);	
				$data = $setupDb->searchMarkDistSetupList(2,$formData);			
				$this->view->list_data = $data;
				
								
			}//if form valid
    	}//if post	
    	
    	
    	
    	
    }
    
    
	public function addAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution Setup -  Lead Lecturer : Add");
    	
    	$auth = Zend_Auth::getInstance();
    	
		if($auth->getIdentity()->IdRole==400){
			$user_type = 2; //CC
    		$this->view->backUrl = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup','action'=>'course-search'), 'default', true);
    	}else{
    		$user_type = 1; //AdMIn
    		$this->view->backUrl = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup','action'=>'search'), 'default', true);
    	}
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$idSemester = $this->_getParam('idSemester');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idProgram = $this->_getParam('idProgram');
    	
    	$this->view->idSemester = $idSemester;    
    	$this->view->idSubject = $idSubject;
    	$this->view->idProgram = $idProgram;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	$this->view->semester = $semester;
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);    
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram); 
    	
		$form = new Examination_Form_MarkDistributionSetupSearchHop(array('locale'=>$locale));
    	    	
    	
    	$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
    	
	 	//check if setup by asad/exam is exist
		$main_status = $setupDb->getExistData(1,array('IdSemester'=>$idSemester,'IdSubject'=>$idSubject,'IdProgram'=>$idProgram));
		
		if($main_status){
			
			//get component
	    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
			//$assessment_list = $AssessmenttypeDB->fngetAllassessmentType();
			$assessment_list = $AssessmenttypeDB->getAssessmentbyType(0,$idSemester,$semester[0]['IdScheme']);	
				  
			foreach($assessment_list as $index=>$list){
						
				//get info mds
				//$detail = $setupDb->getLatestSemesterDetails(1,$list['IdExaminationAssessmentType'],$main_status["IdSemesterMaster"],$idSubject,$idProgram);					
				//$assessment_list[$index]['mds_min_weightage']= $detail['mds_min_weightage'];
				//$assessment_list[$index]['mds_max_weightage']= $detail['mds_max_weightage'];
				
				//get info main weightage from asad
				$detail_main = $setupDb->getDetails(1,$list['IdExaminationAssessmentType'],$main_status["IdSemesterMaster"],$idSubject,$idProgram);					
				$assessment_list[$index]['mds_min_weightage']= $detail_main['mds_min_weightage'];
				$assessment_list[$index]['mds_max_weightage']= $detail_main['mds_max_weightage'];
			}		
	
			$this->view->list_component = $assessment_list;  
			
			//print_r($assessment_list);
		}else{
			
			$this->gobjsessionsis->flash = array('message' => 'Main setup by ASAD does not exist', 'type' => 'notice');
			
			if($auth->getIdentity()->IdRole==400){
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup', 'action'=>'course-search'),'default',true));
			}else{
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup', 'action'=>'search'),'default',true));
			}
		}
			
		/*if ($this->getRequest()->isPost()  && $this->_request->getPost('Search')) {
			
			$formData = $this->getRequest()->getPost();

			$form->populate($formData);
			
			$this->view->idCollege = $formData["IdCollege"];
			$this->view->idSemester = $formData["IdSemester"];				
			$this->view->idSubject = $formData["IdSubject"];
			$this->view->idProgram = $formData["IdProgram"];			
			
			$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
			
		}*/
		
		
		
		if ($this->getRequest()->isPost() && $this->_request->getPost('btn-submit')) {
		
    		$formData = $this->getRequest()->getPost();	
    		
    		$data['mds_user_type']=2;
			$data['mds_semester']=$formData['idSemester'];
			$data['mds_program']=$formData['idProgram'];	
			$data['mds_course']=$formData['idSubject'];			
			$data['mds_createdt']=date ( 'Y-m-d H:i:s');
			$data['mds_createdby']=$auth->getIdentity()->iduser;
			
			for($i=0; $i<count($formData['IdExaminationAssessmentType']); $i++){
				$IdExaminationAssessmentType = $formData['IdExaminationAssessmentType'][$i];
				
				$data['mds_assessmenttypeid']=$IdExaminationAssessmentType;				
				$data['mds_weightage']=$formData['Weightage'][$IdExaminationAssessmentType];
				
				//add				
				$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
				$setupDb->addData($data);
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup', 'action'=>'edit-data','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"]),'default',true));
    	}
		
    	
    }
    
	public function editDataAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution Setup -  Lead Lecturer : Edit");
    	
    	$auth = Zend_Auth::getInstance();
    		
    	if($auth->getIdentity()->IdRole==400){ //CC
    		$user_type = 2;
    		$this->view->backUrl = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup','action'=>'course-search'), 'default', true);
    	}else{
    		$user_type = 1;
    		$this->view->backUrl = $this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup','action'=>'search'), 'default', true);
    	}
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');    	
    	$idSubject = $this->_getParam('idSubject');
    	$idProgram = $this->_getParam('idProgram');
    	
    	$this->view->idSemester = $idSemester;    
    	$this->view->idSubject = $idSubject;
    	$this->view->idProgram = $idProgram;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	$this->view->semester = $semester;
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);    	
    	
    	//get component
    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
		//$assessment_list = $AssessmenttypeDB->fngetAllassessmentType();	
		$assessment_list = $AssessmenttypeDB->getAssessmentbyType($type=0,$idSemester,$semester[0]['IdScheme']);	  

		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
		
		//check if setup by asad/exam is exist
		$main_status = $setupDb->getExistData(1,array('IdSemester'=>$idSemester,'IdSubject'=>$idSubject,'IdProgram'=>$idProgram));
		
		
		foreach($assessment_list as $index=>$list){
			
			//get info mds from cc
			$detail = $setupDb->getDetails(2,$list['IdExaminationAssessmentType'],$idSemester,$idSubject,$idProgram);
			$assessment_list[$index]['mds_id']= $detail['mds_id'];
			$assessment_list[$index]['mds_weightage']= $detail['mds_weightage'];
			
			
			//get info main weightage from asad
			$detail_main = $setupDb->getDetails(1,$list['IdExaminationAssessmentType'],$main_status["IdSemesterMaster"],$idSubject,$idProgram);					
			$assessment_list[$index]['mds_min_weightage']= $detail_main['mds_min_weightage'];
			$assessment_list[$index]['mds_max_weightage']= $detail_main['mds_max_weightage'];
			
		}		
		$this->view->list_component = $assessment_list;  
    	
	   // echo '<pre>';
		//print_r($assessment_list);
		
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
						
			$data['mds_modifydt']=date ( 'Y-m-d H:i:s');
			$data['mds_modifyby']=$auth->getIdentity()->iduser;
			
			for($i=0; $i<count($formData['mds_id']); $i++){
				
				$mds_id = $formData['mds_id'][$i];
				$data['mds_weightage']=$formData['Weightage'][$mds_id];
				$data['mds_weightage']=$formData['Weightage'][$mds_id];
				
				//edit				
			    $setupDb->updateData($data,$mds_id);
				//print_r($data);
			}
			
			
				$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-setup', 'action'=>'edit-data','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"]),'default',true));
		}
    	
    }
    
 	public function addxAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution Setup");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    		    	
    	//get component
    	$Assessmenttype = new Examination_Model_DbTable_Assessmenttype();
		$this->view->list_component = $Assessmenttype->fngetAllassessmentType(fngetAllassessmentType);	    
    
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			
			$data['mds_semester']=$formData['idSemester'];
			$data['mds_course']=$formData['idSubject'];
			$data['mds_program']=$formData['idSemester'];
			$data['mds_createdt']=date ( 'Y-m-d H:i:s');
			$data['mds_createdby']=$auth->getIdentity()->iduser;
			
			
			
			$data['mds_min_weightage']=$formData['idSemester'];
			$data['mds_max_weightage']=$formData['idSemester'];
			
			/*
			 * [MinRaw] => Array
		        (
		            ["54"] => 70
		            ["55"] => 70
		        )

			    [MaxRaw] => Array
			        (
			            ["54"] => 80
			            ["55"] => 80
			        )
			
			    [MinWeightage] => Array
			        (
			            ["54"] => 70
			            ["55"] => 70
			        )
			
			    [MaxWeightage] => Array
			        (
			            ["54"] => 80
			            ["55"] => 80
		        )
			 */
		}
    	
    }
    
    
	public function searchCourseAction(){
	
		$idCollege = $this->_getParam('idCollege',null);
		 
		$this->_helper->layout->disableLayout();
	
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$db = Zend_Db_Table::getDefaultAdapter();
       
	    $select = $db->select()
	 				 ->from(array("sm"=>"tbl_subjectmaster"))	 				 
	 				 ->where("sm.Active = 1")
	 				 ->order('sm.SubCode');
	 				 
	 	if($idCollege){
	 		$select->where('sm.IdFaculty = ?',$idCollege);
	 	}
		$row = $db->fetchAll($select);
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($row);
	
		echo $json;
		exit();
	}
	
	public function courseSearchAction(){
		
		$this->view->title=$this->view->translate("Mark Distribution Setup :  Lead Lecturer : Search Course");
    		
		$auth = Zend_Auth::getInstance();
		$session = Zend_Registry::get('sis');
		//echo '<pre>';
		//print_r($auth->getIdentity());
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	
		$form = new Examination_Form_MarkDistributionSetupHop(array('locale'=>$locale));    	
    	$this->view->form = $form;
    	
    	$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();	
    	
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();

			$this->view->idSemester = $formData['IdSemester'];  
    		$this->view->idProgram = $formData['IdProgram']; 
			
			if ($form->isValid($formData)) {
				
				$session->result = $formData;
				
				$form->populate($formData);
				$data = $setupDb->searchMarkDistSetupListCc($formData);			
				
				$this->view->list_data = $data;
								
			}//if form valid
    	}
    	    	
	}
	
 	function runscriptAction(){
     	exit;
     	$db = Zend_Db_Table::getDefaultAdapter();
     		
     	$progDB= new GeneralSetup_Model_DbTable_Program();
		$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		
		$goods = array();
		
		$goods[0]['program']= 2;
		$goods[1]['program']= 5;
		$goods[2]['program']= 1;
		$goods[3]['program']= 3;
		$goods[4]['program']= 20;
		
		
		$goods[0]['semester']= 6;
		$goods[1]['semester']= 6;
		$goods[2]['semester']= 11;
		$goods[3]['semester']= 11;
		$goods[4]['semester']= 11;
		
		//add				
		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
		
								
		foreach($goods as $good){
			
				$activeLandscape=$landscapeDB->getAllActiveLandscape($good['program']);
		     	
		     	$rows = array();
		     	$allsemlandscape = array();
		     	
		     	//buat macam ni nak cater multiple landscape type (in future)
				foreach($activeLandscape as $i=>$actl){					
					if($actl["LandscapeType"]==43 || $actl["LandscapeType"]==42){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}
				}
				
				if(is_array($allsemlandscape)){
					    	
			    	  $lstrSelect = $db->select()
			    	  				->distinct()
					 				 ->from(array("ls"=>'tbl_landscapesubject'),array())		
					 				 ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject ',array('IdSubject','SubjectName','SubCode','CreditHours','key'=>'IdSubject','name'=>'subjectMainDefaultLanguage'))			 				
			                         ->join(array("ld"=>"tbl_landscape"),"ld.IdLandscape=ls.IdLandscape",array("ProgramDescription"))	                        
					 				 ->group("ls.IdSubject")
					 				 ->order("s.SubCode");			 				 
					 				 
							foreach ($allsemlandscape as $landscape) {
						 		$lstrSelect->orwhere("ls.IdLandscape = ?",$landscape);
							}
										
						//echo $lstrSelect;
					 	$rows = $db->fetchAll($lstrSelect);
					 	
					 	echo '<pre>';
					 	print_r($rows);
					 	
					 	echo '<pre>';
					 	if(count($rows)>0){
					 		
					 		foreach($rows as $row){
					 			
					 			$data['mds_user_type']=2;
								$data['mds_semester']=$good['semester'];
								$data['mds_program']=$good['program'];	
								$data['mds_course']=$row['IdSubject'];			
								$data['mds_createdt']=date ( 'Y-m-d H:i:s');
								$data['mds_createdby']=589;
									
								

								if($good['program'] ==2 || $good['program']==5){
									//ps
									$data['mds_weightage']=30;
									$data['mds_assessmenttypeid']=56;	
									
									
									$select1 =  $lstrSelect = $db->select()->from('tbl_markdistribution_setup_2')
																 ->where('mds_program = ?',$good['program'])
																 ->where('mds_semester = ?',$good['semester'])
																 ->where('mds_course = ?',$row['IdSubject'])
																 ->where('mds_assessmenttypeid = ?',56)
																 ->where('mds_user_type = ?',2);
									$row_select1 = $db->fetchRow($select1);		

									if(!$row_select1){
										print_r($data);	
										//$db->insert('tbl_markdistribution_setup_2',$data);								
										$setupDb->addData($data);
									}
									
									
									
									$data['mds_weightage']=70;
									$data['mds_assessmenttypeid']=51;
									
									$select2 =  $lstrSelect = $db->select()->from('tbl_markdistribution_setup_2')
																 ->where('mds_program = ?',$good['program'])
																 ->where('mds_semester = ?',$good['semester'])
																 ->where('mds_course = ?',$row['IdSubject'])
																 ->where('mds_assessmenttypeid = ?',51)
																 ->where('mds_user_type = ?',2);
									$row_select2 = $db->fetchRow($select2);		

									if(!$row_select2){
										print_r($data);		
										//$db->insert('tbl_markdistribution_setup_2',$data);								
										$setupDb->addData($data);
									}
									
								}else{
									//gs
									$data['mds_weightage']=30;
									$data['mds_assessmenttypeid']=57;
									
									$select3 =  $lstrSelect = $db->select()->from('tbl_markdistribution_setup_2')
																 ->where('mds_program = ?',$good['program'])
																 ->where('mds_semester = ?',$good['semester'])
																 ->where('mds_course = ?',$row['IdSubject'])
																 ->where('mds_assessmenttypeid = ?',57)
																 ->where('mds_user_type = ?',2);
									$row_select3 = $db->fetchRow($select3);		

									if(!$row_select3){
										print_r($data);	
										//$db->insert('tbl_markdistribution_setup_2',$data);									
										$setupDb->addData($data);
									}
									
									
									
									$data['mds_weightage']=70;
									$data['mds_assessmenttypeid']=58;
									
									$select4 =  $lstrSelect = $db->select()->from('tbl_markdistribution_setup_2')
																 ->where('mds_program = ?',$good['program'])
																 ->where('mds_semester = ?',$good['semester'])
																 ->where('mds_course = ?',$row['IdSubject'])
																 ->where('mds_assessmenttypeid = ?',58)
																 ->where('mds_user_type = ?',2);
									$row_select4 = $db->fetchRow($select4);		

									if(!$row_select4){
										print_r($data);	
										//$db->insert('tbl_markdistribution_setup_2',$data);									
										$setupDb->addData($data);
									}
									
								}
								
								
								echo '<hr>';
					 		}
					 	}
				}
			
			
		}//end godds
     	
		exit;
     }
     
}

?>