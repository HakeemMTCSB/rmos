<?php
class examination_MarksentrysetupController extends Base_Base { //Controller for the User Module
	private $lobjMarksentryForm;
	private $lobjSubjectmaster;
	private $lobjmarksentrysetup;
	private $lobjStaffmaster;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjMarksentryForm = new Examination_Form_Marksentry();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field5->addMultiOptions ( $larrresult );

	 if(!$this->_getParam('search'))
	 	unset($this->gobjsessionsis->Marksentrypaginatorresult);
		$larrresult = $this->lobjSubjectmaster->fnGetSubjectMasterList(); //get user details
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Marksentrypaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksentrypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				//echo "hello";die();
				$larrresult = $this->lobjmarksentrysetup ->fnSearchMarksEntry( $lobjform->getValues () ); //searching the values for the user
					
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Marksentrypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			echo "AS";
			$this->_redirect( $this->baseUrl . '/examination/marksentrysetup');
		}
	}

	public function markssetuplistAction() { //Action for creating the new user

		$this->view->lobjMarksentryForm = $this->lobjMarksentryForm;
		$idSubject = ( int ) $this->_getParam ( 'id' );
		//echo "<pre>";print_r($idSubject);die();
		$this->view->subjectName = ( string ) $this->_getParam ( 'name' );
		$this->view->lobjMarksentryForm->IdSubject->setValue ( $idSubject );
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjMarksentryForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjMarksentryForm->EffectiveDate->setValue ( $ldtsystemDate );
		$larrstafflist = $this->lobjStaffmaster->fngetStaffMasterListforDD();
		$this->view->lobjMarksentryForm->IdStaff->addMultiOptions ( $larrstafflist );

		$auth = Zend_Auth::getInstance();
		$this->view->lobjMarksentryForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$result = $this->lobjmarksentrysetup->fnEditMarksEntry($idSubject);
		/*print_r($result);
		 die();*/
		$jsonresult = Zend_Json_Encoder::encode($result);

		$jsondata = '{
				"label":"SubjectName",
				"identifier":"IdMarksEntrySetup",
				"items":'.$jsonresult.
				'}';

		$this->view->jsondata = $jsondata;


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			if ($this->lobjMarksentryForm->isValid ( $larrformData )) {
				if($larrformData ['IdMarksEntrySetup']){
					$lintIdMarksEntrySetup = $larrformData ['IdMarksEntrySetup'];
					$this->lobjmarksentrysetup->fnupdateMarksEntry($lintIdMarksEntrySetup, $larrformData );
				}else{
					unset($larrformData['IdMarksEntrySetup']);
					$result = $this->lobjmarksentrysetup->fnAddMarksEntry($larrformData); //instance for adding the lobjuserForm values to DB
				}


				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Marks Entry Setup Add Id=' . $larrformData['IdSubject'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/marksentrysetup');
			}
		}
	}

	public function getvalidstaffAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$staffid = $this->_getParam('staffid');
		$IdSubject = $this->_getParam('IdSubject');


		$larrDetails = $this->lobjmarksentrysetup->fngetstaffname($staffid,$IdSubject);
		echo $larrDetails['IdStaff'];

	}
}