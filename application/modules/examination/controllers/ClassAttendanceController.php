<?php
/**
 *  @author alif 
 *  @date Aug 25, 2013
 */
 
class Examination_ClassAttendanceController extends Base_Base {

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		//$db = new Studentfinance_Model_DbTable_Burekol();
		//$this->_DbObj = $db;
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Attendance - Search Group");
		 
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		//faculty
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
		
		if($this->_sis_session->IdRole == 1){
			$collegeList = $collegeDb->getCollege();
		}else{
			$this->view->default_faculty = $this->_sis_session->idCollege;
			$collegeList = array('0'=>$collegeDb->fngetCollegemasterData($this->_sis_session->idCollege));
		}
		
		$this->view->college_list = $collegeList;
		
	}
	
	public function searchCourseAction(){
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
				
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			 
			/*
			 * Search Subject
			 */
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			
			//Subject list base on Program Landscape from the selected faculty
			$programs = $progDB->fngetProgramDetails($faculty_id);
			
			
			$allsemlandscape = null;
			$allblocklandscape = null;
			$i=0;
			$j=0;
			foreach ($programs as $key => $program){
				$activeLandscape=$landscapeDB->getAllActiveLandscape($program["IdProgram"]);
				foreach($activeLandscape as $actl){
					if($actl["LandscapeType"]==43){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}elseif($actl["LandscapeType"]==44){
						$allblocklandscape[$j] = $actl["IdLandscape"];
						$j++;
					}
				}
			}
			
			
			$subjectsem = null;
			$subjectblock = null;
			
			if(is_array($allsemlandscape))
				$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape);
			if(is_array($allblocklandscape))
				$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape);
			
			$subjects = null;
			if(is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=array_merge( $subjectsem , $subjectblock );
			}else{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=$subjectblock;
				}
			}
			
			
			//end subject list
			if(count($subjects)==0){
				//Original from yati
				$subjectDb = new  GeneralSetup_Model_DbTable_Subjectmaster();
				$subjects = $subjectDb->getSubjectByCollegeId( array('IdCollege'=>$faculty_id,'IdSemester'=>$semester_id,'SubjectCode'=>null) );
			}
			$i=0;
			foreach($subjects as $subject){
			
				//get total student register this subject
				$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
				$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$semester_id);
				$subject["total_student"] = $total_student;
					
				//get total group creates
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$semester_id);
				$subject["total_group"] = $total_group;
				$subject["IdSemester"] = $semester_id;
					
				//get total no of student has been assigned
				$total_assigned = $subjectRegDB->getTotalAssigned($subject["IdSubject"],$semester_id);
				$total_unassigned = $subjectRegDB->getTotalUnAssigned($subject["IdSubject"],$semester_id);
				$subject["total_assigned"] = $total_assigned;
				$subject["total_unassigned"] = $total_unassigned;
					
				$subjects[$i]=$subject;
					
				$i++;
			}
			
			foreach ($subjects as $index => $subject){
				if($subject["total_student"]>0 && $subject["total_group"]>0){
					
				}else{
					unset($subjects[$index]);
				}
			}
			/*
			 * End search subject
			 */
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($subjects);
				
			echo $json;
			exit();
		}
	}
		
	function searchCourseGroupAction(){
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
		$subject_id = $this->_getParam('subject_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
			/*
			 * Search Group
			*/
			//get Subject Info
			$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			$subject = $subjectDb->getData($subject_id);
			$this->view->subject = $subject;
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$groups = $courseGroupDb->getGroupList($subject_id,$semester_id);
			
			
			$i=0;
			foreach($groups as $group){
					
				$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
					
				$group["total_student"] = $total_student;
				$groups[$i]=$group;
					
				$i++;
			}
			/*
			 * End search group
			*/
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($groups);
		
			echo $json;
			exit();
		
		}
	}
		
	public function studentAttendanceAction(){
		
		$group_id = $this->_getParam('group', null);
		$semester_id = $this->_getParam('sem', null);
		
		//title
		$this->view->title= $this->view->translate("Attendance Record");
			
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		//group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($group_id);
		
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$group['total_student'] = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
		$this->view->group = $group;
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($group['IdSubject']);
		$this->view->subject = $subject;
		
		//schedule
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($group["IdCourseTaggingGroup"]);
		$this->view->schedule = $schedule;
		
		//attendance
		$groupStudtAtt = new Examination_Model_DbTable_CourseGroupStudentAttendance();
		$attendanceList = $groupStudtAtt->getPaginateData($group["IdCourseTaggingGroup"]);
                
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($attendanceList));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		$this->view->paginator = $paginator;
		
		/*echo "<pre>";
		print_r($schedule);
		echo "</pre>";*/
		
	}
	
	public function studentAttendanceAddAction(){
		
		$group_id = $this->_getParam('group', null);
		$this->view->group_id = $group_id;
		
		$semester_id = $this->_getParam('sem', null);
		
		//title
		$this->view->title= $this->view->translate("Attendance  - Add");
			
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
				
			try {
				
				//add head
				if( !isset($formData['class_date']) || $formData['class_date']=="" || $formData['class_date']==null ){
					throw new Exception("Date cannot be empty");
				}
				$studAttDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
				$data = array(
					'group_id' => $formData['group_id'],
					'class_date' => date('Y-m-d', strtotime($formData['class_date'])),
					'class_time_start' => $formData['class_time_start'],
					'class_time_end' => $formData['class_time_end'],
					'remark' => $formData['remark'],
					'total_registered_student' => 	$formData['total_student'],
					'group_schedule_id' => 	$formData['schedule_id'],
					'lecturer_id' => $formData['lecturer_id'],
					'lecturer_activity' => 	$formData['lecturer_activity'],
					'student_assignment' => 	$formData['student_assignment'],
					'evaluation' => 	$formData['evaluation']
				);
				
			
				$head_id = $studAttDb->insert($data);
				
				
				//add detail
				for($i=0; $i<sizeof($formData['registration_id']); $i++){
					
					$studAttDetailDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();
					$data = array(
						'course_group_att_id' => $head_id,
						'student_id' => $formData['registration_id'][$i],
						'student_nim' => $formData['nim'][$i],
						'status' => ($formData['status'][$i]!=null && $formData['status'][$i]!=0)?$formData['status'][$i]:new Zend_Db_Expr('NULL')
					);
					
					
					
					$studAttDetailDb->insert($data);
					
				}
				
				
				$db->commit();
				//$db->rollBack();
						
				//redirect
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'class-attendance', 'action'=>'student-attendance','group'=>$formData['group_id']),'default',true));
						
			}catch (Exception $e) {
				$db->rollBack();
				
				echo "Error in Student Attendance. <br />";
				
				echo $e->getMessage();
				/*echo "<pre>";
				print_r($e->getTrace());
				echo "</pre>";*/
				
    			exit;
    			
			}

		}
		
		//group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($group_id);
				
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$group['total_student'] = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
		$this->view->group = $group;
				
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($group['IdSubject']);
		$this->view->subject = $subject;
		
		//schedule
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($group["IdCourseTaggingGroup"]);
		$this->view->schedule = $schedule;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		$this->view->attendanceStatusList = $attendanceStatusList;
		
		//student list
		$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$studentList = $courseGroupStudentDb->getStudent($group["IdCourseTaggingGroup"]);
		$this->view->studentList = $studentList;
		
		//academic staff
		$staffDb = new GeneralSetup_Model_DbTable_Staffmaster();
		$staff = $staffDb->getAcademicStaff();
		$this->view->academic_staff = $staff;
		
	}
	
	public function studentAttendanceEditAction(){
		
		$att_id = $this->_getParam('id', null);
		
		//title
		$this->view->title= $this->view->translate("Attendance  - Edit");
			
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
		
			try {
		
				//update head
				if( !isset($formData['class_date']) || $formData['class_date']=="" || $formData['class_date']==null ){
					throw new Exception("Date cannot be empty");
				}
				$studAttDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
				$data = array(
						'class_date' => date('Y-m-d', strtotime($formData['class_date'])),
						'class_time_start' => $formData['class_time_start'],
						'class_time_end' => $formData['class_time_end'],
						'remark' => $formData['remark'],
						'group_schedule_id' => 	$formData['schedule_id'],
						'lecturer_id' => $formData['lecturer_id'],
						'lecturer_activity' => 	$formData['lecturer_activity'],
						'student_assignment' => 	$formData['student_assignment'],
						'evaluation' => 	$formData['evaluation']
				);
				$head_id = $studAttDb->update($data,'id = '.$att_id);
		
		
				//update detail
				for($i=0; $i<sizeof($formData['registration_id']); $i++){
						
					$studAttDetailDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();
					$data = array(
							'status' => ($formData['status'][$i]!=null && $formData['status'][$i]!=0)?$formData['status'][$i]:new Zend_Db_Expr('NULL')
					);
						
						
					$studAttDetailDb->update($data, 'course_group_att_id = '.$att_id.' and student_id = '.$formData['registration_id'][$i].' and student_nim = '.$formData['nim'][$i]);
						
				}
		
		
				$db->commit();
				//$db->rollBack();
		
				//redirect
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'class-attendance', 'action'=>'student-attendance','group'=>$formData['group_id']),'default',true));
		
			}catch (Exception $e) {
				$db->rollBack();
		
				echo "Error in Student Attendance. <br />";
				echo $e->getMessage();
		
				/*echo "<pre>";
				print_r($e->getTrace());
				echo "</pre>";*/
		
				exit;
				 
			}
				
		}
		
		//attendance data
		$studAttDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
		$studAttData = $studAttDb->getData($att_id);
		$this->view->att_data = $studAttData;
		
		$studAttDetailDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();
		$studAttDataDetail = $studAttDetailDb->getDetailData($att_id);
		$this->view->studentList = $studAttDataDetail;
				
		//group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($studAttData["group_id"]);
				
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$group['total_student'] = $courseGroupStudent->getTotalStudent($studAttData["group_id"]);
		$this->view->group = $group;
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($group['IdSubject']);
		$this->view->subject = $subject;
		
		//schedule
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($group["IdCourseTaggingGroup"]);
		$this->view->schedule = $schedule;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		$this->view->attendanceStatusList = $attendanceStatusList;
		
		//academic staff
		$staffDb = new GeneralSetup_Model_DbTable_Staffmaster();
		$staff = $staffDb->getAcademicStaff();
		$this->view->academic_staff = $staff;
		
	}
	
	public function studentAttendanceDeleteAction(){
		
		$this->_helper->layout->disableLayout();
		
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$att_id = $this->_getParam('id', null);
		
		$status = false;
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
	
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
	
			try {
				
				if( !isset($formData['gid']) || $formData['gid']=="" || $formData['gid']==null || !isset($formData['id']) || $formData['id']=="" || $formData['id']==null ){
					throw new Exception("Unable to delete. Information not sufficient");
				}
	
				//delete detail
				$studAttDetailDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();
				$studAttDetailDb->delete('course_group_att_id = '.$formData['id']);
				
				//delete head
				$studAttDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
				$studAttDb->delete('group_id = '.$formData['gid']." and id = ".$formData['id']);
	
				$db->commit();
				//$db->rollBack();
	
				$status = true;
	
			}catch (Exception $e) {
				$db->rollBack();
	
				echo "Error in Delete Student Attendance. <br />";
				echo $e->getMessage();
	
				/*echo "<pre>";
					print_r($e->getTrace());
				echo "</pre>";*/
	
				exit;
					
			}
	
		}
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode(array('status'=>$status));
	
		echo $json;
		exit();
	
	}
	
	public function studentAttendanceDetailAction(){
		$group_id = $this->_getParam('group', null);
		$att_id = $this->_getParam('id', null);
		
		//title
		$this->view->title= $this->view->translate("Attendance  - Detail");
			
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		//group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($group_id);
		
		//check if not admin and different faculty in session throw exception
		try{
			if($this->_sis_session->IdRole != 1 && $this->_sis_session->idCollege != $group['faculty_id'] ){
				throw new Exception("Not allowed to view. Faculty not match");
			}
		}catch (Exception $e){
			echo $e->getMessage();
			exit;
		}
		
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$group['total_student'] = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
		$this->view->group = $group;
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($group['IdSubject']);
		$this->view->subject = $subject;
		
		//schedule
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($group["IdCourseTaggingGroup"]);
		$this->view->schedule = $schedule;
		
		//attendance data
		$studAttDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
		$studAttData = $studAttDb->getData($att_id);
		$this->view->att_data = $studAttData;
		
		$studAttDetailDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();
		$studAttDataDetail = $studAttDetailDb->getDetailData($att_id);
		$this->view->studentList = $studAttDataDetail;
		
		//academic staff
		$staffDb = new GeneralSetup_Model_DbTable_Staffmaster();
		$staff = $staffDb->getAcademicStaff();
		$this->view->academic_staff = $staff;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		$this->view->attendanceStatusList = $attendanceStatusList;
		
	}
	
	public function ajaxGetScheduleAction(){
	
		$sc_id = $this->_getParam('sc_id',null);
			
		$this->_helper->layout->disableLayout();
	
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getData($sc_id);
			
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($schedule);
	
		echo $json;
		exit();
	}
	
	public function studentAttendanceGroupReportAction(){
		
		$group_id = $this->_getParam('group', null);
		$this->view->group_id = $group_id;
		
		$this->view->title = $this->view->translate('Group Attendance Report');
		
		
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($group_id);
		
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$group['total_student'] = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
		$this->view->group = $group;
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($group['IdSubject']);
		$this->view->subject = $subject;
		
		//schedule
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($group["IdCourseTaggingGroup"]);
		$this->view->schedule = $schedule;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		$this->view->attendanceStatusList = $attendanceStatusList;
		
		//student list
		$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$studentList = $courseGroupStudentDb->getStudent($group["IdCourseTaggingGroup"]);
		$this->view->studentList = $studentList;

		//att 
		$attlDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
		$grpAtt = $attlDb->getAttendanceByGroup($group["IdCourseTaggingGroup"]);
		$this->view->group_att = $grpAtt;
		
	}
	
	public function studentAttendanceGroupReportPdfAction(){
		
		$group_id = $this->_getParam('group', null);
		Global $dt_group_id;
		$dt_group_id = $group_id;
		
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($group_id);
		
		$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$group['total_student'] = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
		Global $dt_group;
		$dt_group = $group;
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($group['IdSubject']);
		Global $dt_subject;
		$dt_subject = $subject;
		
		//schedule
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($group["IdCourseTaggingGroup"]);
		Global $dt_schedule;
		$dt_schedule = $schedule;
		
		//attendance status
		$lkpTbl = new App_Model_General_DbTable_Definationms();
		$attendanceStatusList = $lkpTbl->getDataByType(91);
		Global $dt_attendanceStatusList;
		$dt_attendanceStatusList = $attendanceStatusList;
		
		//student list
		$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$studentList = $courseGroupStudentDb->getStudent($group["IdCourseTaggingGroup"]);
		Global $dt_studentList;
		$dt_studentList = $studentList;
		
		//att
		$attlDb = new Examination_Model_DbTable_CourseGroupStudentAttendance();
		$grpAtt = $attlDb->getAttendanceByGroup($group["IdCourseTaggingGroup"]);
		Global $dt_grpAtt;
		$dt_grpAtt = $grpAtt;
		
		/*
		 * PDF Generation
		*/
		
		require_once 'dompdf_config.inc.php';
		
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		$html_template_path = DOCUMENT_PATH."/template/AttendanceGroupReport.html";
		
		$html = file_get_contents($html_template_path);
		
		//echo $html;
		//exit;
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'landscape');
		$dompdf->render();
		
		
		$dompdf->stream("AttendanceGroupReport.pdf");
		exit;
	}
}
 ?>
