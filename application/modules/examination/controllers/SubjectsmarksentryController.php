<?php
class Examination_SubjectsmarksentryController extends Base_Base {
	private $lobjSubjectsMarksEntrynModelbulk;
	private $lobjplacementtestmarksmodel;
	private $lobjinitialconfig;
	private $lobjstudentapplication;
	private $lobjStudentregistrationForm;
	private $_gobjlog;


	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj(){
		$this->lobjSubjectsMarksEntrynModelbulk = new Examination_Model_DbTable_Subjectsmarksentry();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistrationbulk();
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
	}

	public function indexAction() {
		-		$idUniversity =$this->gobjsessionsis->idUniversity;
		$this->view->config = $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);
		$this->view->lobjform = $this->lobjform;
		$this->view->results = $larrresult= array();
		$this->view->lobjStudentRegForm = $this->lobjStudentregistrationForm;

		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
		$this->view->lobjform->field8->addMultiOptions($larProgramNameCombo);

		$larSubjectNameCombo = $this->lobjSubjectsMarksEntrynModelbulk->fngetSubjectNameCombo();
		$this->view->lobjform->field1->addMultiOptions($larSubjectNameCombo);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->results = $larrresult = $this->lobjSubjectsMarksEntrynModelbulk->fnSearchStudentsubjects( $this->lobjform->getValues ()); //searching the values for the user
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post

			$ldtsystemDate = date ( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance();
			$UpdUser = $auth->getIdentity()->iduser;

			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Subject Marks Entry Edit' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log


			/*			$count=count($larrformData['IdStudentRegistration']);
			 for($i=0;$i<$count;$i++) {
			$larrformDatasDelete ['IdStudentRegistration'] = $larrformData['IdStudentRegistration'][$i];
			$larrformDatasDelete ['idStaff'] = $larrformData['idStaff'][$i];
			$larrformDatasDelete ['idSubject'] = $larrformData['idSubject'][$i];
			$larrformDatasDelete ['IdMarksDistributionDetails'] = $larrformData['IdMarksDistributionDetails'][$i];
			$larrDeleteSubjectMarks = $this->lobjSubjectsMarksEntrynModelbulk->fnDeleteSubjectMarks($larrformDatasDelete);
			}*/

			$count=count($larrformData['IdStudentRegistration']);
			for($i=0;$i<$count;$i++) {
				if($larrformData['idSubjectMarksEntry'][$i] == "") {
					$larrformDatas ['IdStudentRegistration'] = $larrformData['IdStudentRegistration'][$i];
					///////////Extrafields //////////////
					$larrformDatas ['Year'] = $larrformData['Year'][$i];
					$larrformDatas ['IdSemester'] = $larrformData['IdSemester'][$i];
					$larrformDatas ['StudentId'] = $larrformData['StudentId'][$i];
					$larrformDatas ['SubjectCode'] = $larrformData['SubjectCode'][$i];
					///////////Extrafields //////////////
					$larrformDatas ['idStaff'] = $larrformData['idStaff'][$i];
					$larrformDatas ['idSubject'] = $larrformData['idSubject'][$i];
					$larrformDatas ['subjectmarks'] = $larrformData['subjectmarks'][$i];
					$larrformDatas ['IdMarksDistributionDetails'] = $larrformData['IdMarksDistributionDetails'][$i];
					$larrformDatas ['Active'] = 1;
					$larrformDatas ['UpdDate'] = $ldtsystemDate;
					$larrformDatas ['UpdUser'] = $auth->getIdentity()->iduser;
					$larrstudentreg = $this->lobjSubjectsMarksEntrynModelbulk->fnAddSubjectMarks($larrformDatas);

				} else {
					$updatemarksentry = $this->lobjSubjectsMarksEntrynModelbulk->fnUpdateSubjectMarks($larrformData['idSubjectMarksEntry'][$i],$larrformData['subjectmarks'][$i]);
				}
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/subjectsmarksentry/index');
		}

	}


}