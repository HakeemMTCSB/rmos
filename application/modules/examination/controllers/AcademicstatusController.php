<?php

class examination_AcademicstatusController extends Base_Base { //Controller for the User Module

	private $lobjAcademicstatusForm;
	private $lobjChargemaster;
	private $lobjdeftype;
	private $_gobjlog;
	private $lobjsemesterModel;
	private $lobjschemeModel;
	private $lobjprogramModel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjAcademicstatusForm = new Examination_Form_Academicstatus();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjDeparmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster();
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
		$this->lobjschemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
	}

	public function indexAction() { // action for search and view
		$this->view->title = $this->view->translate('GPA/CGPA Setup');
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
		
		//program
		$lobjProgramNameList = $this->lobjAcademicstatus->fnGetProgramNameList();
		$lobjform->field5->addMultiOptions(array('' => 'All'));
		foreach($lobjProgramNameList as $program) {
			$lobjform->field5->addMultiOption($program['key'],$program['value']);
		}

		//semester
		$lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$lobjsemesterlist = $this->lobjsemesterModel->fnSemesterList();
		$lobjform->field8->addMultiOptions(array('' => 'All'));
		foreach($lobjsemesterlist as $semester) {
			$lobjform->field8->addMultiOption($semester['key'],$semester['value']);
		}
		
		//Award
		$lobjAwardNameList = $this->lobjdeftype->fnGetDefinations('Award');
		$lobjform->field20->addMultiOptions(array('' => 'All'));
		foreach($lobjAwardNameList as $larrdefmsresult) {
			$lobjform->field20->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}

		$lobjform->field27->addMultiOptions(array(null=>'All', '3' => 'GPA','1' => 'CGPA'));


		if (!$this->_getParam('search'))
			unset($this->gobjsessionsis->Academicstatuspaginatorresult);
			$larrresult = $this->lobjAcademicstatus->fnGetacademicsetupList(); //get user details

		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		
		if (isset($this->gobjsessionsis->Academicstatuspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Academicstatuspaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		
		
		
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {			
			
			$larrformData = $this->_request->getPost();
			//echo '<pre>';
			//print_r($larrformData);
			
			if ($lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjAcademicstatus->fnSearchAcademicStatusDetails($larrformData); //searching the values for the user				
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->Academicstatuspaginatorresult = $larrresult;
			}
		}
		
	}

	
	
	/*
	 * Action for creating new academic setup
	*/
	public function newacademicstatusAction() {
		$this->view->title = $this->view->translate('Add GPA/CGPA');
		$this->view->lobjAcademicstatusForm = $this->lobjAcademicstatusForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAcademicstatusForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjAcademicstatusForm->UpdDate->setValue($ldtsystemDate);
		
		
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {

			$larrformData = $this->_request->getPost();
		
			unset($larrformData ['Save']);

			if($larrformData['BasedOn'] == 'Award'){
				$ret = $this->lobjAcademicstatus->checkduplicate($larrformData['AcademicStatus'],$larrformData['IdSemester'],$larrformData['IdScheme'],$larrformData['IdAward']);
				if(count($ret) > 0){
					//$this->view->msgscheme = '1';
					$this->gobjsessionsis->flash = array('message' => 'Grade already exist', 'type' => 'error');
					return $this->render("newacademicstatus");
				}
			} else if($larrformData['BasedOn'] == 'Program'){
				
				if($larrformData['AcademicStatus']==2){
					
					
					for($x=0; $x<=2; $x++){
						$ret = $this->lobjAcademicstatus->checkduplicateByprogram($x,$larrformData['IdSemester'],$larrformData['IdProgram']);
						if(count($ret) > 0){
							//$this->view->msgscheme = '1';	
							$this->gobjsessionsis->flash = array('message' => 'Grade already exist', 'type' => 'error');					
							return $this->render("newacademicstatus");
						}//end if
					}//end for
					
				}else{
				
					$ret = $this->lobjAcademicstatus->checkduplicateByprogram($larrformData['AcademicStatus'],$larrformData['IdSemester'],$larrformData['IdProgram']);
				
					if(count($ret) > 0){
						//$this->view->msgscheme = '1';	
						$this->gobjsessionsis->flash = array('message' => 'Grade already exist', 'type' => 'error');					
						return $this->render("newacademicstatus");
					}//end if
				}
			}
			
			
			if(count($ret) == 0){
				$result = $this->lobjAcademicstatus->fnAddademicStatus($larrformData);
			}
			
			
			// Write Logs
			$priority = Zend_Log::INFO;
			$larrlog = array('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date('Y-m-d H:i:s'),
					'message' => 'GPA/CGPA Status Add Id=' . $larrformData['IdProgram'],
					'Description' => Zend_Log::DEBUG,
					'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
			
			$this->_gobjlog->write($larrlog); //insert to tbl_log
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect( $this->baseUrl . '/examination/academicstatus/index');
			
			
		}else if($this->_request->isPost() && $this->_request->getPost('CopySetup')){
			
			$larrformData = $this->_request->getPost();
			/*echo "<pre>";
			print_r($larrformData);
			echo "</pre>";*/
			
			
			$data = array();
			$flag = 1;
			
					
			/*check copy of from combination exist or not
			There is two case one is scheme & Award and Program */
			
			
			
			// First case : scheme & Award
			if($larrformData['CopyBasedOn'] == 'Scheme & Award'){
				$result = $this->lobjAcademicstatus->fngetasetupByschemeandaward($larrformData['CopyToIdSemester'],$larrformData['CopyToIdScheme'],$larrformData['CopyBasedOn']);
				if(count($result) > 0){
					//$this->view->errormsg = '1';
					$this->gobjsessionsis->flash = array('message' => 'Grade already exist', 'type' => 'error');	
					return $this->render("newacademicstatus");
				}else{
					$ret = $result = $this->lobjAcademicstatus->fngetasetupByschemeandaward($larrformData['CopyFromIdSemester'],$larrformData['CopyFromIdScheme'],$larrformData['CopyBasedOn']);
					if(count($ret) > 0){
						$flag = 1;
					}else{
						$flag = 0;
					}
				}
			}//end first case
			
			
			
            // Second case : Program
			if($larrformData['CopyBasedOn'] == 'Program'){
				
				//check grade for semester yg nak di add already exist or not
				$result = $this->lobjAcademicstatus->fngetasetupByprogram($larrformData['CopyToIdSemester'],$larrformData['CopyFromIdProgram'],$larrformData['CopyBasedOn']);
				
				if(count($result) > 0){
					//$this->view->errormsgbyprogram = '1';
					$this->gobjsessionsis->flash = array('message' => 'Grade already exist', 'type' => 'error');	
					return $this->render("newacademicstatus");
				}
				
				//check grade yg nak di copy ada atau tak
				$ret = $result = $this->lobjAcademicstatus->fngetasetupByprogram($larrformData['CopyFromIdSemester'],$larrformData['CopyFromIdProgram'],$larrformData['CopyBasedOn']);
				if(count($ret) > 0){
					$flag = 1; //ada
				}else{
					$flag = 0;//tak ada
				}
				
			}//end second case
			
		
			
			if($flag == 1){
				if(count($ret) > 0){
					foreach($ret as $list){
						
						if($larrformData['CopyFromIdProgram'] != ''){
							$data['IdProgram'] =  $larrformData['CopyFromIdProgram'];
						}else{
							$data['IdProgram'] =  $list['IdProgram'];
						}
						
						//$data['SemesterCode'] =  $larrformData['CopyToIdSemester'];
						$data['IdSemester'] =  $larrformData['CopyToIdSemester'];
						$data['IdAward'] =  $list['IdAward'];
						$data['IdScheme'] =  $larrformData['CopyToIdScheme'];
						$data['AcademicStatus'] =  $list['AcademicStatus'];
						$data['BasedOn'] =  $larrformData['CopyBasedOn'];
						$data['IdAcademicStatus'] =  $list['IdAcademicStatus'];
						//$data['MinimumTotalMarks'] =  $list['MinimumTotalMarks'];
						//$data['MaximumTotalMarks'] =  $list['MaximumTotalMarks'];
						//$data['Minimum'] =  $list['Minimum'];
						// $data['Maximum'] =  $list['Maximum'];
						$data['Active'] =  1;
						$data['UpdDate'] =  $ldtsystemDate;
						$data['UpdUser'] =  $auth->getIdentity()->iduser;
						
						
						$result = $this->lobjAcademicstatus->fnCopyAddademicStatus($data);
						
						
						$priority = Zend_Log::INFO;
						$larrlog = array('user_id' => $auth->getIdentity()->iduser,
								'level' => $priority,
								'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
								'time' => date('Y-m-d H:i:s'),
								'message' => 'GPA/CGPA Status Add Id=' . $larrformData['CopyFromIdProgram'],
								'Description' => Zend_Log::DEBUG,
								'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
						$this->_gobjlog->write($larrlog); //insert to tbl_log
						
						
					}//end foreach
					
					$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				}//end if count
				
			}else{
				$this->gobjsessionsis->flash = array('message' => 'Grade already exist', 'type' => 'error');	
				//$this->view->copynotpossibleerrormsg = '1';
				return $this->render("newacademicstatus");
			}
			
			$this->_redirect( $this->baseUrl . '/examination/academicstatus/index');
		}
			

	}

	// Action to open the academic status page in edit mode
	public function academicstatuslistAction() { //Action for the updation and view of the  details.
		$this->view->title = $this->view->translate('Edit GPA/CGPA');
		$this->view->lobjAcademicstatusForm = $this->lobjAcademicstatusForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$lintIdAcademicStatus = (int) $this->_getParam('id');
		$larrresult = $this->lobjAcademicstatus->fnviewAcademicStatus($lintIdAcademicStatus);
		
		$this->view->acadecmisetup = $larrresult;
		$this->lobjAcademicstatusForm->populate($larrresult);

		$larrresultDetails = $this->lobjAcademicstatus->fnviewAcademicStatusDetails($lintIdAcademicStatus);		
		$this->view->acadecmisetupDetails = $larrresultDetails;
	
	

		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjAcademicstatusForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAcademicstatusForm->UpdUser->setValue($auth->getIdentity()->iduser);

		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();
			
			if ($this->_request->isPost()) {
				$larrformData = $this->_request->getPost();
				unset($larrformData ['Save']);
				if ($this->lobjAcademicstatusForm->isValid($larrformData)) {
					//unset($larrformData ['Save']);
					if($larrformData['BasedOn'] == 'Award') {
						$ret = $this->lobjAcademicstatus->checkduplicate($larrformData['AcademicStatus'],$larrformData['IdSemester'],$larrformData['IdScheme'],$larrformData['IdAward'],$lintIdAcademicStatus);
						if(count($ret) > 0){
							$this->view->msgscheme = '1';
							return $this->render("academicstatuslist");
						}
					}

					if($larrformData['BasedOn'] == 'Program') {
						$ret = $this->lobjAcademicstatus->checkduplicateByprogram($larrformData['AcademicStatus'],$larrformData['IdSemester'],$larrformData['IdProgram'],$lintIdAcademicStatus);
						if(count($ret) >= 1){
							$this->view->msgscheme = '1';
							
							return $this->render("academicstatuslist");
						}
					}

					$this->lobjAcademicstatus->fnupdateAcademicStatus($lintIdAcademicStatus,$larrformData);
					$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
					$this->_redirect($this->baseUrl . '/examination/academicstatus/index');
				}
			}
		}
	}

	public function getprogramlistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idscheme = $this->_getParam('idscheme');
		$award = $this->_getParam('idaward');
		$programList = $this->lobjprogramModel->getProgramByschemeandaward($idscheme,$award);
		echo Zend_Json_Encoder::encode($programList);
	}

	function copyAcademicStatusAction(){
		
		$this->view->title = $this->view->translate('Copy GPA/CGPA');
		
		$this->view->lobjAcademicstatusForm = $this->lobjAcademicstatusForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAcademicstatusForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjAcademicstatusForm->UpdDate->setValue($ldtsystemDate);
		
		if($this->_request->isPost()){
			
			$larrformData = $this->_request->getPost();
			//echo '<pre>';
			//print_r($larrformData);
			
						
			// First case :  Award
			if($larrformData['CopyBasedOn'] == 'Award'){				
				//copy semester & award
				
				$this->copySemesterAward($larrformData);					
			}
			
			
			// Second case : Program
			if($larrformData['CopyBasedOn'] == 'Program'){
			
				if($larrformData['CopyFromIdProgram']!=''){					
					//copy semester & program
					$this->copySemesterProgram($larrformData);
				}else{
					$this->copySemester($larrformData);
				}
					
			}
	
		}
		
		
	}
	
	function copySemesterAward($larrformData){
			$auth = Zend_Auth::getInstance();
		
			//check grade yg nak di copy ada atau tak
			$result_data = $result = $this->lobjAcademicstatus->fngetasetupByschemeandaward($larrformData['CopyFromIdSemester'],null,$larrformData['CopyBasedOn']);
			if(count($result_data) > 0){
				
				foreach($result_data as $result){
					
					//check if alredy exist	
					$res = $this->lobjAcademicstatus->getExistSemesterAward($larrformData['CopyToIdSemester'],$result['IdAward'],$larrformData['CopyBasedOn'],$result['AcademicStatus']);
					
					if($res){
						//skip copy
						echo 'skip';
					}else{
						//start copy								
						$data['IdSemester'] =  $larrformData['CopyToIdSemester'];
						$data['IdProgram'] =  null;
						$data['IdAward'] =  $result['IdAward'];					
						$data['AcademicStatus'] =  $result['AcademicStatus'];
						$data['BasedOn'] =  $larrformData['CopyBasedOn'];
						$data['IdAcademicStatus'] = $result['IdAcademicStatus'];
						$data['Active'] =  1;
						$data['UpdDate'] =  date('Y-m-d H:i:s');
						$data['UpdUser'] =  $auth->getIdentity()->iduser;						
						
						$result = $this->lobjAcademicstatus->fnCopyAddademicStatus($data);
					}
				}
										
			}else{
				$flag = 0; //tak ada
			}
	}
	
	function copySemester($larrformData){
		
		$auth = Zend_Auth::getInstance();			
		
		//get all grade existing in selected semester
		$result = $this->lobjAcademicstatus->fngetademicStatus($larrformData['CopyFromIdSemester']);
		
		foreach($result as $row){
			
			//check if already exist
			$res = $this->lobjAcademicstatus->checkExistAcademicStatus($larrformData['CopyToIdSemester'],$row['IdProgram'],$larrformData['CopyBasedOn'],$row['AcademicStatus']);
			if($res){
				//already exist
				//skip copy
			}else{
				//start copy								
				$data['IdSemester'] =  $larrformData['CopyToIdSemester'];
				$data['IdProgram'] =  $row['IdProgram'];
				$data['IdAward'] =  null;					
				$data['AcademicStatus'] =  $row['AcademicStatus'];
				$data['BasedOn'] =  $larrformData['CopyBasedOn'];
				$data['IdAcademicStatus'] = $row['IdAcademicStatus'];
				$data['Active'] =  1;
				$data['UpdDate'] =  date('Y-m-d H:i:s');
				$data['UpdUser'] =  $auth->getIdentity()->iduser;						
				
				$result = $this->lobjAcademicstatus->fnCopyAddademicStatus($data);
			}
		}
	}
	
	function copySemesterProgram($larrformData){
		
			$auth = Zend_Auth::getInstance();
			
			//check grade yg nak di copy ada atau tak
			$return_list = $result = $this->lobjAcademicstatus->fngetasetupByprogram($larrformData['CopyFromIdSemester'],$larrformData['CopyFromIdProgram'],$larrformData['CopyBasedOn']);
			if(count($return_list) > 0){
				
				foreach($return_list as $ret){
										
					for($i=0; $i<count($larrformData['CopyToIdProgram']); $i++){
						
						$idProgram = $larrformData['CopyToIdProgram'][$i];
						
						//check if already exist
						$result = $this->lobjAcademicstatus->checkExistAcademicStatus($larrformData['CopyToIdSemester'],$idProgram,$larrformData['CopyBasedOn'],$ret['AcademicStatus']);
						if($result){
							//already exist
							//dont copy
						}else{
							//start copy								
							$data['IdSemester'] =  $larrformData['CopyToIdSemester'];
							$data['IdProgram'] =  $idProgram;
							$data['IdAward'] =  null;					
							$data['AcademicStatus'] =  $ret['AcademicStatus'];
							$data['BasedOn'] =  $larrformData['CopyBasedOn'];
							$data['IdAcademicStatus'] =  $ret['IdAcademicStatus'];
							$data['Active'] =  1;
							$data['UpdDate'] =  date('Y-m-d H:i:s');
							$data['UpdUser'] =  $auth->getIdentity()->iduser;						
							
							$result = $this->lobjAcademicstatus->fnCopyAddademicStatus($data);
						}						
					}
				}				
				
			}else{
				$flag = 0;//tak ada
			}
	}
		
		
	function copyAcademicStatusOldAction(){
		
		$this->view->title = $this->view->translate('Copy GPA/CGPA');
		
		$this->view->lobjAcademicstatusForm = $this->lobjAcademicstatusForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAcademicstatusForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjAcademicstatusForm->UpdDate->setValue($ldtsystemDate);
		
		if($this->_request->isPost()){
			
			$larrformData = $this->_request->getPost();
			echo '<pre>';
			print_r($larrformData);
			exit;
			$data = array();
			$flag = 1;
			
					
			/*check copy of from combination exist or not
			There is two case one is scheme & Award and Program */
			
			
			
			// First case : scheme & Award
			if($larrformData['CopyBasedOn'] == 'Award'){
				//check grade for semester yg nak di add already exist or not
				$result = $this->lobjAcademicstatus->fngetasetupByschemeandaward($larrformData['CopyToIdSemester'],$larrformData['CopyToIdScheme'],$larrformData['CopyBasedOn']);
				if(count($result) > 0){
					$this->view->errormsg = '1';
					return $this->render("newacademicstatus");
				}else{
					
					//check grade yg nak di copy ada atau tak
					$ret = $result = $this->lobjAcademicstatus->fngetasetupByschemeandaward($larrformData['CopyFromIdSemester'],$larrformData['CopyFromIdScheme'],$larrformData['CopyBasedOn']);
					if(count($ret) > 0){
						$flag = 1; //ada
					}else{
						$flag = 0; //tak ada
					}
				}
			}//end first case
			
			
			
            // Second case : Program
			if($larrformData['CopyBasedOn'] == 'Program'){
				
				//check grade for semester yg nak di add already exist or not
				$result = $this->lobjAcademicstatus->fngetasetupByprogram($larrformData['CopyToIdSemester'],$larrformData['CopyFromIdProgram'],$larrformData['CopyBasedOn']);
				
				if(count($result) > 0){
					$this->view->errormsgbyprogram = '1';
					return $this->render("newacademicstatus");
				}
				
				//check grade yg nak di copy ada atau tak
				$ret = $result = $this->lobjAcademicstatus->fngetasetupByprogram($larrformData['CopyFromIdSemester'],$larrformData['CopyFromIdProgram'],$larrformData['CopyBasedOn']);
				if(count($ret) > 0){
					$flag = 1; //ada
				}else{
					$flag = 0;//tak ada
				}
				
			}//end second case
			
		
			
			if($flag == 1){
				if(count($ret) > 0){
					foreach($ret as $list){
						
						if($larrformData['CopyFromIdProgram'] != ''){
							$data['IdProgram'] =  $larrformData['CopyFromIdProgram'];
						}else{
							$data['IdProgram'] =  $list['IdProgram'];
						}
						
						
						$data['IdSemester'] =  $larrformData['CopyToIdSemester'];
						$data['IdAward'] =  $list['IdAward'];					
						$data['AcademicStatus'] =  $list['AcademicStatus'];
						$data['BasedOn'] =  $larrformData['CopyBasedOn'];
						$data['IdAcademicStatus'] =  $list['IdAcademicStatus'];
						$data['Active'] =  1;
						$data['UpdDate'] =  $ldtsystemDate;
						$data['UpdUser'] =  $auth->getIdentity()->iduser;						
						
						$result = $this->lobjAcademicstatus->fnCopyAddademicStatus($data);
						
						
						$priority = Zend_Log::INFO;
						$larrlog = array('user_id' => $auth->getIdentity()->iduser,
								'level' => $priority,
								'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
								'time' => date('Y-m-d H:i:s'),
								'message' => 'GPA/CGPA Status Add Id=' . $larrformData['CopyFromIdProgram'],
								'Description' => Zend_Log::DEBUG,
								'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
						$this->_gobjlog->write($larrlog); //insert to tbl_log
						
					}//end foreach
				}//end if count
				
			}else{
				$this->gobjsessionsis->flash = array('message' => 'Unable to copy. Data grade not available.', 'type' => 'error');				
				return $this->render("copy-academic-status");
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been copied', 'type' => 'success');			
			$this->_redirect( $this->baseUrl . '/examination/academicstatus/index');
		}
	
			
	}
}