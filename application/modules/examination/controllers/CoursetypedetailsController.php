<?php
class examination_CoursetypedetailsController extends Base_Base { //Controller for the User Module
	private $lobjCoursetypedetailsForm;
	private $lobjCoursetypedetails;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjCoursetypedetailsForm = new Examination_Form_Coursetypedetails();
		$this->lobjCoursetypedetails = new Examination_Model_DbTable_Coursetypedetails();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$lobjCourseTypeList = $this->lobjCoursetypedetails->fnGetCourseTypeList();
		$lobjform->field5->addMultiOptions($lobjCourseTypeList);

	 if(!$this->_getParam('search'))
	 	unset($this->gobjsessionsis->Coursetypedetailsresult);
		$larrresult = $this->lobjCoursetypedetails->fngetCourseTypeDetails(); //get user details

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Coursetypedetailsresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Coursetypedetailsresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCoursetypedetails ->fnSearchCourseTypeDetails( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Coursetypedetailsresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/coursetypedetails/index');
		}
	}

	public function newcoursetypedetailsAction() { //Action for creating the new user

		$this->view->lobjCoursetypedetailsForm = $this->lobjCoursetypedetailsForm;
		$this->view->IdCourseType = $IdCourseType = ( int ) $this->_getParam ( 'id' );
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjCoursetypedetailsForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjCoursetypedetailsForm->IdCourseType->setValue ( $IdCourseType );
		$this->view->lobjCoursetypedetailsForm->IdCourseType->setAttrib ('readonly','true');
		$lobjCourseTypeList = $this->lobjCoursetypedetails->fnGetCourseTypeList();
		$this->lobjCoursetypedetailsForm->IdCourseType->addMultiOptions($lobjCourseTypeList);

		$auth = Zend_Auth::getInstance();
		$this->view->lobjCoursetypedetailsForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$larrresult = $this->lobjCoursetypedetails ->fnViewCourseTypeDetails($IdCourseType);
		$this->view->larrresult=$larrresult;
		//print_r($larrresult);die();

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);die();

			unset ( $larrformData ['Save'] );
			if ($this->lobjCoursetypedetailsForm->isValid ( $larrformData )) {

				unset($larrformData['IdCourseTypeDetails']);
				$result = $this->lobjCoursetypedetails->fnAddCourseTypeDetails($larrformData); //instance for adding the lobjuserForm values to DB

				//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
				//$this->_redirect( $this->baseUrl . '/examination/academicstatus/index');

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'New Couse Type Details Add Id=' . $larrformData['IdCourseType'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/coursetypedetails/newcoursetypedetails/id/'.$larrformData['IdCourseType']);
			}
		}

	}
	public function deletecoursetypedtlsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Po details Id
		$IdCourseTypeDetails = $this->_getParam('IdCourseTypeDetails');
		$larrDelete = $this->lobjCoursetypedetails->fndeleteCourseTypeDetails($IdCourseTypeDetails);
		echo "1";
	}

}