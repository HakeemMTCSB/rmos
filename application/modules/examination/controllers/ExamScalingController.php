<?php 
class Examination_ExamScalingController extends Base_Base {

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Exam Scaling");
    
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;	
		
			
    	$form = new  Examination_Form_ExamScalingSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    	
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();		
		
			
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			//$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
				
				$examScaleDB = new Examination_Model_DbTable_ExamScaling();
				foreach($groups as $index=>$group){
					$scale = $examScaleDB->getGroupData($group['IdCourseTaggingGroup']);
					if($scale){
						$groups[$index]['es_id']=$scale['es_id'];
					}
				}			
				$this->view->groups = $groups;
							
			}//if form valid
			
    	}//if post	
    	   	    	
    }
    
    
    public function addAction()
    {
    	$this->view->title=$this->view->translate("Exam Scaling : Add");
    
    	$this->view->form = new Examination_Form_ExamScalingAddForm();
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;	
		
		$auth = Zend_Auth::getInstance();    	   
		
		$IdCourseTaggingGroup = $this->_getParam('id');
		
		$idSemester = $this->_getParam('idSemester');    	  	
    	$idSubject = $this->_getParam('idSubject');    	
    	$idProgram = $this->_getParam('idProgram');    	
    	
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;
    	$this->view->idProgram = $idProgram;
    	
		//get info group
    	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($IdCourseTaggingGroup);
		$this->view->group = $group;
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($group['IdSemester']);    	
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	//$this->view->program = $programDB->fngetProgramData($group['IdSemester']);    	
    	    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($group['IdSubject']);
    	
    	
		
		
    	//get mark type
    	$defDB = new App_Model_General_DbTable_Definationms();
    	$this->view->mark_type = $defDB->getDataByType(156);
    	
		if ($this->getRequest()->isPost()) {    		
    
			$formData = $this->getRequest()->getPost();		
			
			$data['es_semester'] = $formData['idSemester'];
			$data['es_subject'] = $formData['idSubject'];
			$data['es_group'] = $formData['idGroup'];
			$data['es_mark_type'] = $formData['es_mark_type'];
			$data['es_operational'] = $formData['es_operational'];
			$data['es_rules'] = $formData['es_rules'];
			$data["es_createddt"]=date("Y-m-d H:i:s");
			$data["es_createdby"]=$auth->getIdentity()->id;
			
			$examScaleDB = new Examination_Model_DbTable_ExamScaling();
			$es_id = $examScaleDB->addData($data);
			
			for($i=0; $i<count($formData['es_mark']); $i++){
				
				$info['es_id'] = $es_id;
				$info['esd_mark'] = $formData['es_mark'][$i];
				$examScaleDB->addDetailData($info);
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-scaling', 'action'=>'process','es_id'=>$es_id),'default',true));
			
		}
    }
    
    
      public function processAction()
    {
    	$this->view->title=$this->view->translate("Exam Scaling : Process");
    
    	$this->view->form = new Examination_Form_ExamScalingAddForm();
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;	
		
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();    	   
		
		$examScaleDB = new Examination_Model_DbTable_ExamScaling();
		$studentRegSubDB = new  Examination_Model_DbTable_StudentRegistrationSubject();
		$studentMarkEntryDB = new Examination_Model_DbTable_StudentMarkEntry();
		$systemErrorDB = new App_Model_General_DbTable_SystemError();
		
		if ($this->getRequest()->isPost()) {    		
    
			$formData = $this->getRequest()->getPost();		
			
			$cms_calculation = new Cms_ExamCalculation();
			
			//get exam scale info
			$scale = $examScaleDB->getExamScaling($formData['es_id']);
				
			$stack_fail = array();
						
			for($i=0; $i<count($formData['IdStudentRegSubjects']); $i++){
				
					$IdStudentRegSubjects = $formData['IdStudentRegSubjects'][$i];
					$IdStudentMarksEntry = $formData['IdStudentMarksEntry'][$IdStudentRegSubjects];
					
					//get from tbl_studentregsubject 
					$subject_info = $studentRegSubDB->getData($IdStudentRegSubjects);
												
					$new_mark = 0;
					
					if($scale['es_mark_type']==788){ // Final Exam						
						//get from tbl_student_mark_entry
						$component_info = $studentMarkEntryDB->getDataByStudentRegSubjectId($IdStudentRegSubjects);
						
						foreach($component_info as $comp){
							if($IdStudentMarksEntry==$comp['IdStudentMarksEntry']){
								//process: get new mark based on operation and rule
								$new_final_exam_mark = $this->calculate($comp['FinalTotalMarkObtained'],$scale['es_operational'],$scale['es_rules']);
								$comp['FinalTotalMarkObtained'] = $new_final_exam_mark;
							}						
							$new_mark = abs($new_mark) + abs($comp['FinalTotalMarkObtained']);
						}						
					}
					
					
					if($scale['es_mark_type']==789){ // Total Mark												
						//process: get new mark based on operation and rule
						$new_mark = $this->calculate($subject_info['final_course_mark'],$scale['es_operational'],$scale['es_rules']);											
					}
					
					
					
					
					//get grade						
					$grade=$cms_calculation->getGradeInfo($scale['es_semester'],$subject_info['IdProgram'],$scale['es_subject'],$new_mark);
											
					if($grade){

						$db->beginTransaction();
						
						try {							
							
							if($scale['es_mark_type']==788){ // Final Exam							
							
								//1:Copy history subject
								$studentRegSubDB->copyHistory($IdStudentRegSubjects);
								
								//2:Copy history component final exam mark 
								$studentMarkEntryDB->copyHistory($IdStudentMarksEntry);
								
								//3:Update component mark perlu ske?
								$studentMarkEntryDB->updateData(array('FinalTotalMarkObtained'=>abs($new_final_exam_mark)),$IdStudentMarksEntry);
								
								//4:update course grade				
								$this->updateGrade($IdStudentRegSubjects,$new_mark,$grade);
								
								//5:generate gpa/cgpa
								$cms_calculation->generateGrade($subject_info['IdStudentRegistration']);
							}	
						
							
							if($scale['es_mark_type']==789){ // Total Mark	
								//1:Copy history subject
								$studentRegSubDB->copyHistory($IdStudentRegSubjects);
											
								//2:update course grade				
								$this->updateGrade($IdStudentRegSubjects,$new_mark,$grade);
								
								//3:generate gpa/cgpa
								$cms_calculation->generateGrade($subject_info['IdStudentRegistration']);
							}
							
							$db->commit();
 
						} catch (Exception $e) {
							
						    $db->rollBack();	
						    
						    $message = array('IdStudentRegSubjects'=>$IdStudentRegSubjects,'message'=>$e->getMessage());
						    array_push($stack_fail,$message);

						    $error['se_IdStudentRegSubjects']=$IdStudentRegSubjects;
							$error['se_title']='Exam Scaling Processing';
							$error['se_message']=$e->getMessage();
							$error['se_createddt']=date("Y-m-d H:i:s");
							$error['se_createdby']=$auth->getIdentity()->id;
							$systemErrorDB->addData($error);
				
						}//end try
				}					
			}
			
		
			if(count($stack_fail)>0){
				$data = '<ul>';
				foreach($stack_fail as $stack){
					$data .= '<li>Id :'.$stack['IdStudentRegSubjects'].'   Message : '.$stack['message'].'</li>';
				}
				$data .= '</ul>';
				$this->gobjsessionsis->flash = array('message' => 'Fail update exam scaling as below details:-<br><br>'.$data, 'type' => 'error');
			}else{
				$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			}
			
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-scaling', 'action'=>'process','es_id'=>$formData['es_id']),'default',true));
			
		}else{
			
				$es_id = $this->_getParam('es_id');   
				$this->view->es_id = $es_id;  
				
				
				$scale = $examScaleDB->getExamScaling($es_id);
				$this->view->scale = $scale;
				
				$scale_mark = $examScaleDB->getExamScalingMark($es_id);
				$this->view->scale_mark = $scale_mark;
				
				//get student list
				
				$students = $studentRegSubDB->getStudentExamScaling($scale,$scale_mark);
				$this->view->students = $students;
				
				//echo '<pre>';
				//print_r($students);
		}
    }
    
    function calculate($mark,$operation,$rules){
    	if($operation=='+'){
    		return $new_mark = abs($mark) + abs($rules);
    	}else
    	if($operation=='-'){
    		return $new_mark = abs($mark) - abs($rules);
    	}
    }
    
    function updateGrade($IdStudentRegSubjects,$new_mark,$grade){
    	
     		if(isset($grade["Pass"])){
			     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
			     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
		     	 else $grade_status='';
		     }else{
		     	 $grade_status='';
		     }
			     
			//update remarking mark grade info
			$mark['final_course_mark'] = $new_mark;
			$mark['grade_id'] = $grade['Grade'];
			$mark['grade_point'] = $grade['GradePoint'];
			$mark['grade_name'] = $grade['GradeName'];				
			$mark['grade_status'] = $grade_status;
			$mark['grade_desc'] = $grade['GradeDesc'];	
			$mark['exam_scaling_mark'] = 1;
			
			$studentRegSubDB = new  Examination_Model_DbTable_StudentRegistrationSubject();				
			$studentRegSubDB->updateData($mark,$IdStudentRegSubjects);
    }
}
?>