<?php
class examination_GeneratecompletelistController extends Base_Base { //Controller for the User Module

	private $_gobjlog;
	private $lobjStaffmaster;
	private $lobjprogram;
	private $lobjStudentRegModel;
	private $lobjdeftype;
	private $lobjsemester;
	private $lobjcollegemaster;
	private $lobjcompletlistModel;


	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){

		$this->lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();
		$this->lobjcompletlistModel = new Examination_Model_DbTable_Generatecompletelist();
	}


	/**
	 * Function to search students
	 * @author: VT
	 */
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		$this->view->UpdUser = $auth->getIdentity()->iduser;
		$this->view->UpdDate = date('Y-m-d H:i:s');

		$facultyList = $this->lobjcollegemaster->fnGetListofCollege();
		$this->view->lobjform->field5->addMultiOptions($facultyList);

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field24->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramName']);
		}

		// SHOW THE SEMESTER
		//$larrsemresult =  $this->lobjStudentRegModel->fetchAllSemMaster();
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}


		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->completelistpaginatorresult);

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->completelistpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->completelistpaginatorresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )!='' ) {
			$larrformData = $this->_request->getPost ();
			$this->lobjcompletlistModel->fnprocessCompleteStudent($larrformData);
			$larrresultFinal =  $this->lobjcompletlistModel->fngetCompleteStudentList($larrformData);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresultFinal,$lintpage,$lintpagecount);
			$this->gobjsessionsis->completelistpaginatorresult = $larrresultFinal;
		}



		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			unset($this->gobjsessionsis->completelistpaginatorresult);
			$this->_redirect( $this->baseUrl . '/examination/generatedeanlist');
		}
	}



	public function exportcompletelistAction(){
		$semCode = 'Semester';
		$result  = $this->gobjsessionsis->completelistpaginatorresult;
		if(count($result)>0) {
			$semestName = $result[0]['IdSemester'];
			if($semestName!='') {
				$semCode = $semestName;
			}
			$this->view->larrresult = $result;
			$html =  $this->render('semestercompletelistsexcel');

			$listName = $semCode.'_Complete_Student_List.xls';
			$this->getResponse()->setRawHeader( "Content-Type: application/vnd.ms-excel; charset=UTF-8" )
			->setRawHeader( "Content-Disposition: attachment; filename=".$listName )
			->setRawHeader( "Content-Transfer-Encoding: binary" )
			->setRawHeader( "Expires: 0" )
			->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
			->setRawHeader( "Pragma: public" )
			->sendResponse();
			echo $html;
		}
		exit();

	}




}
