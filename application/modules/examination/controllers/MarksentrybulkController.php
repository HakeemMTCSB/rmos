<?php
class examination_MarksentrybulkController extends Base_Base { //Controller for the User Module
	private $lobjMarksentryForm;
	private $lobjSubjectmaster;
	private $lobjmarksentrysetup;
	private $lobjStaffmaster;
	private $lobjprogram;
	private $lobjStudentRegModel;
	private $lobjschemesetupmodel;
	private $lobjdeftype;
	private $lobjsemester;
	private $lobjAssementType;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjMarksentryForm = new Examination_Form_Marksentry();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjlandscape = new GeneralSetup_Model_DbTable_Landscape();
		$this->lobjAssementType = new Examination_Model_DbTable_Assessmenttype();
		$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();
	}


	/**
	 * Function to search students
	 * @author: VT
	 */
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$auth = Zend_Auth::getInstance();
		$this->view->iduser = $auth->getIdentity()->iduser;
		$instructor = $auth->getIdentity()->iduser;

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field25->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
		}
		//$this->view->lobjform->field25->setAttrib('Onchange','showcourses(this.value);');

		// SHOW THE COURSES
		$larrresultCourses = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field23->addMultiOptions( $larrresultCourses );
		$this->view->lobjform->field23->setAttrib('Onchange','showcoursecomponents(this.value);');

		// SHOW THE SEMESTER
		//$larrsemresult =  $this->lobjStudentRegModel->fetchAllSemMaster();
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}

		// SHOW THE SCHEME
//		$larrschemelist = $this->lobjschemesetupmodel->fnGetSchemeDetails();
//		foreach($larrschemelist as $larrvalues) {
//			$this->view->lobjform->field26->addMultiOption($larrvalues['IdScheme'],$larrvalues['EnglishDescription']);
//		}
		
		// SHOW THE COURSES
		$data = $this->lobjStaffmaster->fngetStaffMasterListforDD();
		$this->view->lobjform->field29->addMultiOptions($data);
		
		$this->view->lobjform->field29->setAttrib('onchange','getCourses(this)');

		/* SHOW THE FACULTY
		$facultyList = $this->lobjcollegemaster->fnGetListofCollege();
		if(!empty($facultyList)){
			$this->view->lobjform->field29->addMultiOptions($facultyList);
		}*/

		// SHOW THE COMPONENTS
		//$larrComponents = $this->lobjdeftype->fnlistDefination('40');
		//$larrComponents = $this->lobjdeftype->fnGetDefinationMs('Subject Components');
		//foreach($larrComponents as $larrvalues) {
		//	$this->view->lobjform->field24->addMultiOption($larrvalues['key'],$larrvalues['value']);
		//}

		$this->view->lobjform->Search->setAttrib('Onclick','return formsubmit();');

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Marksentrypaginatorresult);


		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Marksentrypaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksentrypaginatorresult,$lintpage,$lintpagecount);
		}
		$i = '0';
		$larrresultFinal = array();
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();

			//if ($lobjform->isValid ( $larrformData )) {
			//asd($larrformData,false);
			$larrresultFinal = $this->lobjmarksentrysetup->fnSearchMarksEntryBulk( $larrformData ,$instructor);

			//$larrresultFinal[0]['IdSemester']= $larrformData['field27'];

			foreach ($larrresultFinal as $values) {

				$larrresultFinal[$i]= $values;
				$larrresultFinal[$i]['IdSemester']= $larrformData['field27'];
				$larrresultFinal[$i]['IdScheme']= $larrformData['field26'];
				$larrresultFinal[$i]['IdFaculty']= $larrformData['field29'];
				$i++;
			}


			//foreach ($larrresult as $values) {
			//                                $IdSubject = $values['IdSubject'];
			//                                $IdStudentRegistration = $values['IdStudentRegistration'];
			//                                $returnComp = $this->lobjmarksentrysetup->fnfetchAllComponents($IdSubject,$IdStudentRegistration);
			//                                if($returnComp!='') {
			//                                    $larrresultFinal[$i]= $values;
			//                                    $larrresultFinal[$i]['Idcomponents']= $returnComp;
			//                                    $i++;
			//                                }
			//                                }
			
			//echo '<pre>';
			//print_r($larrresultFinal);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresultFinal,$lintpage,$lintpagecount);
			$this->gobjsessionsis->Marksentrypaginatorresult = $larrresultFinal;
			//}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/marksentrybulk');
		}
	}


	/**
	 * Function to update marks for the student marks component
	 * @author: VT
	 */

	public function marksentrybulklistAction() {

		$this->view->lobjMarksentryForm = $this->lobjMarksentryForm;
		$idcomp = ( int ) $this->_getParam ( 'idcomp' );
		$idcompitem = ( int ) $this->_getParam ( 'idcompitem' );
		$idcourse = ( int ) $this->_getParam ( 'idcourse' );
		$idprogram = ( int ) $this->_getParam ( 'idprogram' );
		$idsem = $this->_getParam ( 'idsem' );
		$idScheme = ( int ) $this->_getParam ( 'idScheme' );
		$idFaculty = ( int ) $this->_getParam ( 'idFaculty' );
		$group = ( int ) $this->_getParam ( 'group' );
		$auth = Zend_Auth::getInstance();
		$this->view->iduser = $auth->getIdentity()->iduser;
		$instructor = $auth->getIdentity()->iduser;
		$this->view->instructor = $instructor;
		$this->view->course = $idcourse;
		$this->view->componentid = $idcomp;
		$this->view->componentitemid = $idcompitem;
		//$auth = Zend_Auth::getInstance();
		//$this->view->iduser = $auth->getIdentity()->iduser;


		$this->view->idstudentregistration = $this->view->coursename = $this->view->prgName = $this->view->componentname = $this->view->semName = NULL;

		// SHOW THE STUDENT ATTENDANCE STATUS
		//$larrSAS = $this->lobjdeftype->fnlistDefination('83');
		$larrSAS = $this->lobjdeftype->fnGetDefinationMs('Student Attendance Status');
		$this->view->attendancestatus = $larrSAS;

		if($idcourse!='') {
			$subjectdetails = $this->lobjSubjectmaster->fngetsubjectdetailsbyid($idcourse);
			if(count($subjectdetails)>0) {
				$this->view->coursename = $subjectdetails[0]['SubjectName'].'-'.$subjectdetails[0]['SubCode'];
			}
		}

		if($idcomp!='' && $idcompitem!='') {
			$larrComponents = $this->lobjAssementType->fnGetAssesmentTypeNamebyID($idcomp);
			$larrComponentsItem = $this->lobjAssementType->fnGetAssesmentItemNamebyID($idcompitem);
			if(count($larrComponents)>0) {
				$this->view->componentname = $larrComponents[0]['Description'].'-'.$larrComponentsItem[0]['Description'];
			}
		}

		if($idprogram!='') {
			$programdetails = $this->lobjprogram->fnfindprogram($idprogram);
			if(count($programdetails)>0) {
				$this->view->prgName = $programdetails[0]['ProgramName'];
			}
		}

		//get semester name
		if($idsem!='') {
			$semesterdetails = $this->lobjmarksentrysetup->getSemcode($idsem);
			$this->view->semName = $semesterdetails;
		}

		//$larrresultMES = $this->lobjmarksentrysetup->fngetMarksBulkStatus( $idcourse $idcomp);
		//if(count($larrresultMES)>0) {
		//$this->view->larrresultMES = $larrresultMES;
		//}

		// Show the component Details
		$larrresult = $this->lobjmarksentrysetup->fnSearchMarksEntryBulkDetails( $idScheme, $idFaculty, $idsem, $idprogram, $idcourse ,$idcomp, $idcompitem, $instructor,$group);
		
		$i = '0';
		$larrresultFinal = array();
		foreach ($larrresult as $values) {
			$IdMarksDistributionMaster = $values['IdMarksDistributionMaster'];
			$IdStudentRegistration = $values['IdStudentRegistration'];

			$IdSemesterDetails = $values['IdSemesterDetails'];
			$IdSemesterMain = $values['IdSemesterMain'];
			if($IdSemesterDetails!='' && $IdSemesterMain=='') {
				$semCode =  $IdSemesterDetails.'_detail';
			} elseif ($IdSemesterDetails=='' && $IdSemesterMain!='') {
				$semCode =  $IdSemesterMain.'_main';
			}
			if($semCode!='') {
				$semesterCodeName = $this->lobjmarksentrysetup->getSemcode($semCode);
			} else { $semesterCodeName = '';
			}

			$returnCompDetail = $this->lobjmarksentrysetup->fnfetchAllComponentsDetails($IdMarksDistributionMaster,$idcourse ,$idcomp, $idcompitem, $IdStudentRegistration);
			if($returnCompDetail!='') {
				$larrresultFinal[$i]= $values;
				$larrresultFinal[$i]['SemesterCode']= $semesterCodeName;
				$larrresultFinal[$i]['IdcomponentsDetails']= $returnCompDetail;
				$i++;
			}
		}

		$this->view->finalResult = $larrresultFinal;
//		echo '<pre>';
//		print_r($larrresultFinal);
		//asd($larrresultFinal);

		// get approved components only
		$larrresultApprComp = $this->lobjmarksentrysetup->getApprovedComponentsBulk( $idprogram, $idcourse ,$idcomp);
		if(count($larrresultApprComp)>0) {
			$this->view->larrresultApprCompStatus = $larrresultApprComp[0]['Status'];
		}

		//asd($larrresultFinal);
		if ($this->_request->isPost () ) {
			$larrformData = $this->_request->getPost ();


			$marksentrysttaus = '311';
			if($larrformData['savedata']=='savedata') {
				$marksentrysttaus = '311';
			} else if($larrformData['savedata']=='submitdata' ) {
				$marksentrysttaus = '312';
			}else if($larrformData['savedata']=='approvedata' ) {
				$marksentrysttaus = '313';
			}else if($larrformData['savedata']=='rejectdata' ) {
				$marksentrysttaus = '313';
			}


			$returnCompDetail = $this->lobjmarksentrysetup->fnInsertComponentBulk($larrformData,$marksentrysttaus,$instructor);
			//if($idsem!='') {
			$this->_redirect( $this->baseUrl . '/examination/marksentrybulk/marksentrybulklist/idcourse/'.$idcourse.'/idcomp/'.$idcomp.'/idcompitem/'.$idcompitem.'/idprogram/'.$idprogram.'/idsem/'.$idsem.'/idScheme/'.$idScheme.'/idFaculty/'.$idFaculty);
			// } else {
			//    $this->_redirect( $this->baseUrl . '/examination/marksentrybulk/marksentrybulklist/idcourse/'.$idcourse.'/idcomp/'.$idcomp.'/idprogram/'.$idprogram);
			//}

		}
	}



	public function getcomponentlistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$courseid = ( int ) $this->_getParam ( 'courseid' );
		$larrresultCourses = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjSubjectmaster->fngetComponentlist($courseid));
		echo Zend_Json_Encoder::encode($larrresultCourses);
	}


	public function getcourselistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$IdProgram = ( int ) $this->_getParam ( 'prgid' );
		$larrresultCourses = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjlandscape->fngetActiveLandscapeSubjects($IdProgram));
		echo Zend_Json_Encoder::encode($larrresultCourses);
	}
	
	
	public function searchStudentAction(){
		
		$this->view->title = "Mark Entry";
		
		$form = new Examination_Form_SearchStudent();
		$this->view->form = $form;
		
		if($this->getRequest()->isPost())
        {   
        	$formData = $this->getRequest()->getPost();           
            if($form->isValid($formData))
            {
            	$form->populate($formData);
            	print_r($formData);
            	
            	//get list student
            	
            }
        }
	}
	
	public function ajaxGetSemesterAction(){
		
    	$idProgram = $this->_getParam('idProgram',null);
    	$idIntake = $this->_getParam('idIntake',null);
    	
     	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
        if($idProgram && $idIntake){
		    $registrationDB = new Examination_Model_DbTable_StudentRegistration();       
		    $semester = $registrationDB->getSemesterByProgramIntake($idProgram,$idIntake);
		    
		    $ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

			$json = Zend_Json::encode($semester);
        }else{
        	$json = null;
        }
	  	
		
		
		echo $json;
		exit();
    }
    
    
	public function ajaxGetSubjectAction(){
		
    	$idProgram = $this->_getParam('idProgram',null);
    	$idIntake = $this->_getParam('idIntake',null);
    	$idSemester = $this->_getParam('idSemester',null);
    	
     	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
        if($idProgram && $idIntake && $idSemester){
		    $registrationDB = new Examination_Model_DbTable_StudentRegistration();       
		    $semester = $registrationDB->getSubjectByProgramIntake($idProgram,$idIntake,$idSemester);
		    
		    $ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

			$json = Zend_Json::encode($semester);
        }else{
        	$json = null;
        }
	  	
		
		
		echo $json;
		exit();
    }


}
