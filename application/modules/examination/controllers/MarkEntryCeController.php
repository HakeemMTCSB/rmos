<?php

class examination_MarkEntryCeController extends Base_Base { //Controller for the User Module

	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Mark Entry - CE");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkEntryCeSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idSemester = $formData["IdSemester"];
				
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				//get subject where mark dist is assigned to the lecturer
				$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
				$subjects = $markDistributionDB->getSubjectAssignLecture($formData);
			    $this->view->subjects = $subjects;
				
			    //echo '<pre>';
				//print_r($subjects);
								
			}//if form valid
    	}//if post		
	}
	
	public function entryAction(){
		
		//title
		$this->view->title= $this->view->translate("Mark Entry - Admin - CE");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;		
		
    	$idSemester = $this->_getParam('idSemester');    	  	
    	$idSubject = $this->_getParam('idSubject');    	
    	
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject; 
		
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);    	
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
	
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getMyListMainComponent($idSemester,$idSubject);	    
		
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getMyListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	/*echo '<pre>';
		print_r($list_component);
		
    	exit;*/
    	
    	//get student list
    	$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
    	$students = $studentRegDB->searchStudentCeEntryMark($idSemester,$idSubject);

    	$db = Zend_Db_Table::getDefaultAdapter();	
    	
		foreach($students as $key=>$student){
    		
    		foreach($list_component as $c=>$component){
    			
    			$select_main_mark = $db->select()
				 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
				 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
				 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
				 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
			 	$entry_main = $db->fetchAll($select_main_mark);	
			 	
			 	foreach($entry_main as $m=>$main){
			 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
			 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
			 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
			 		
			 		//get item
		 			$select_item_mark = $db->select()
			 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
			 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
			 		$entry_item = $db->fetchAll($select_item_mark);	
			 		
			 		foreach($entry_item as $i=>$item){
			 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
			 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
			 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
			 		}
			 	}
    		}
    		
    	}
		//var_dump($students);
    	$this->view->students = $students;
	}
	
	public function viewAction(){
		
		//title
		$this->view->title= $this->view->translate("Mark Entry - CE");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;		
		
    	$idSemester = $this->_getParam('idSemester');    	  	
    	$idSubject = $this->_getParam('idSubject');    	
    	
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject; 
		
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);    	
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
	
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getMyListMainComponent($idSemester,$idSubject);	    
		
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getMyListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	
    	//echo '<pre>';
		//print_r($list_component);
		
    	
    	//get student list
    	$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
    	$students = $studentRegDB->searchStudentCeEntryMark($idSemester,$idSubject);
    	
    	$db = Zend_Db_Table::getDefaultAdapter();	
    	
		foreach($students as $key=>$student){
    		
    		foreach($list_component as $c=>$component){
    			
    			$select_main_mark = $db->select()
				 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
				 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
				 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
				 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
			 	$entry_main = $db->fetchAll($select_main_mark);	
			 	
			 	foreach($entry_main as $m=>$main){
			 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
			 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
			 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
			 		
			 		//get item
		 			$select_item_mark = $db->select()
			 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
			 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
			 		$entry_item = $db->fetchAll($select_item_mark);	
			 		
			 		foreach($entry_item as $i=>$item){
			 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
			 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
			 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
			 		}
			 	}
    		}
    		
    	}
    	
    	$this->view->students = $students;
	}
	
	
	function ajaxSaveMarkAction(){
		
				$this->_helper->layout()->disableLayout();
				
				$auth = Zend_Auth::getInstance();
				
				$semester_id = $this->_getParam('idSemester', null);		
				$subject_id = $this->_getParam('idSubject', null);
				$program_id = $this->_getParam('idProgram', null);
				
				$idMaster = $this->_getParam('idMaster', null);
				$IdMarksDistributionDetails = $this->_getParam('idDetail', null);
					
				$IdStudentRegistration = $this->_getParam('id', null);
				$IdStudentRegSubjects = $this->_getParam('idRegSub', null);
				
				$IdStudentMarksEntry = $this->_getParam('idMarkEntry', null);
				$IdStudentMarksEntryDetail = $this->_getParam('idMarkEntryDetail', null);
				
				$markObtained = $this->_getParam('markObtained', null); //entry mark			
				$TotalItemRaw = $this->_getParam('TotalItemRaw', null); //total raw mark for each component
				$FinalMark = $this->_getParam('FinalMark', null);
					
				$ajaxContext = $this->_helper->getHelper('AjaxContext');
				$ajaxContext->addActionContext('view', 'html');
				$ajaxContext->initContext();
			    			
				$cms_calculation = new Cms_ExamCalculation();
				$markDB = new Examination_Model_DbTable_StudentMarkEntry();
				$markDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
				$studentRegistarationDB = new Examination_Model_DbTable_StudentRegistration(); 
				
				 		
				//get main component info
	    		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
	    		$main_component = $markDistributionDB->getInfoComponent($idMaster);
	        	
				
				$item_mark_entry = $markDB->getAllMarkEntryByMainComponent($IdStudentRegSubjects,$idMaster);
    											
		    			$data["TotalMarkObtained"] = $TotalItemRaw; //Total Raw Mark each item++
		    			$data["FinalTotalMarkObtained"]=$FinalMark; //Total Raw Mark each item++
		    			$data["UpdUser"] = $auth->getIdentity()->iduser;
				    	$data["UpdDate"] = date('Y-m-d H:i:s');	
    				    	
    				
			    		if(isset($IdStudentMarksEntry) && $IdStudentMarksEntry!=''){
			    				$markDB->updateData($data,$IdStudentMarksEntry);				    		   		  
			    		}else{	    

			    			if(isset($markObtained) && $markObtained!=''){
			    				
				    			$data["IdStudentRegistration"] = $IdStudentRegistration;
				    			$data["IdStudentRegSubjects"] =$IdStudentRegSubjects;  
					    		$data["IdSemester"] = $semester_id;
						    	$data["Course"] = $subject_id;   
						    	$data["IdMarksDistributionMaster"] = $idMaster;
						    	$data["MarksTotal"] = 0; 
						    	$data["Component"]   =  $main_component["IdComponentType"];
						    	$data["ComponentItem"]   =  ''; 				
						    	$data["Instructor"] = '';    		
						    	$data["AttendanceStatus"] = '';
						    	$data["MarksEntryStatus"] = 0; //entry
			    	
				    			$IdStudentMarksEntry = $markDB->addData($data);	
			    			}			    			
			    		}
				
			    		
			    		//2nd : Save Details Component Mark  
			  	  		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
			  	    	$item = $oCompitem->getDataComponentItem($IdMarksDistributionDetails);	  	     			
		    			
			  	    	//get main component total mark
			  	    	//$FinalMarksObtained = $cms_calculation->calculateMark($markObtained,$item["Weightage"],$item["Percentage"]);				    			
			  	      			
		    	
				    	$dataItem["MarksObtained"] = $markObtained;
						$dataItem["FinalMarksObtained"] = $markObtained;
				    	$dataItem["UpdUser"] = $auth->getIdentity()->iduser;
						$dataItem["UpdDate"] = date('Y-m-d H:i:s');
		    			
						if(isset($IdStudentMarksEntryDetail) && $IdStudentMarksEntryDetail!=''){
							
							 if($markObtained==''){
							 	
							 	//delete sebab null ibarat no mark
							 	$markDetailsDB->deleteData($IdStudentMarksEntryDetail);
							 	$IdStudentMarksEntryDetail='x';
							 	
							 	// count item mark ada lain x					 					  	 
						  	    $item_entry = $markDB->getMarkById($IdStudentMarksEntry);
						  	    if(count($item_entry)>0){
						  	    	//do nothing
						  	    }else{
				  	   				//x ada item delete main	
				  	   				//echo 'masuk';	  	   				
				  	   				$markDB->deleteData($IdStudentMarksEntry);
						 		    $IdStudentMarksEntry='x';
				  	   			}
							 	
							 }else{
		    					 $markDetailsDB->updateData($dataItem,$IdStudentMarksEntryDetail);	    					
							 }
		    				 
		    			}else{	    	
		    					
				    			$dataItem["IdStudentMarksEntry"] = $IdStudentMarksEntry;   
								$dataItem["Component"] = $main_component["IdComponentType"];   
								$dataItem["ComponentItem"]   =  '';
								$dataItem["ComponentDetail"] = $IdMarksDistributionDetails;				
						    	//$dataItem["TotalMarks"] = $item["Weightage"]; //jumlah penuh soalan bukan pemberat for CE x perlu
				    
		    				 	$IdStudentMarksEntryDetail  = $markDetailsDB->addData($dataItem);
		    			} 
    			
	    			
    		
    		$grade = $markDB->saveStudentSubjectMarkCe($IdStudentRegSubjects,$markObtained);
    		
    		$result = array( 'IdStudentMarksEntry'=>$IdStudentMarksEntry,
    		 			     'IdStudentMarksEntryDetail'=>$IdStudentMarksEntryDetail,
    		 				 'grade'=>$grade['grade_desc']);
			
			$ajaxContext->addActionContext('view', 'html')
						->addActionContext('form', 'html')
						->addActionContext('process', 'json')
						->initContext();
		
			$json = Zend_Json::encode($result);
		
			echo $json;
			exit();
		
	}
	
	
	function ajaxSaveMarkRoleAction(){
		
				$this->_helper->layout()->disableLayout();
				
				$auth = Zend_Auth::getInstance();
				
				$semester_id = $this->_getParam('idSemester', null);		
				$subject_id = $this->_getParam('idSubject', null);
				$program_id = $this->_getParam('idProgram', null);
				
				$idMaster = $this->_getParam('idMaster', null);
				$IdMarksDistributionDetails = $this->_getParam('idDetail', null);
					
				$IdStudentRegistration = $this->_getParam('id', null);
				$IdStudentRegSubjects = $this->_getParam('idRegSub', null);
				
				$IdStudentMarksEntry = $this->_getParam('idMarkEntry', null);
				$IdStudentMarksEntryDetail = $this->_getParam('idMarkEntryDetail', null);
				
				$markObtained = $this->_getParam('markObtained', null); //entry mark			
				//$TotalItemRaw = $this->_getParam('TotalItemRaw', null); //total raw mark for each component
				//$TotalItemPercent = $this->_getParam('TotalItemPercent', null);			
				//$grandTotal = $this->_getParam('grandTotal', null); //total mark course based (after weightage)
				
				$ajaxContext = $this->_helper->getHelper('AjaxContext');
				$ajaxContext->addActionContext('view', 'html');
				$ajaxContext->initContext();
			
    			
				$cms_calculation = new Cms_ExamCalculation();
				$markDB = new Examination_Model_DbTable_StudentMarkEntry();
				$markDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
				$studentRegistarationDB = new Examination_Model_DbTable_StudentRegistration(); 
				
				 		
				//get main component info
	    		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
	    		$main_component = $markDistributionDB->getInfoComponent($idMaster);
	        	
				
			/*	$item_mark_entry = $markDB->getAllMarkEntryByMainComponent($IdStudentRegSubjects,$idMaster);
    			
    			//$data["TotalMarkObtained"] = $TotalItemRaw; //Total Raw Mark each item++
    			//$data["FinalTotalMarkObtained"]=$TotalItemPercent; //afrer calculation made
    			$data["UpdUser"] = $auth->getIdentity()->iduser;
		    	$data["UpdDate"] = date('Y-m-d H:i:s');	
    			
    			if(isset($markObtained) && $markObtained!=''){
			    		if(isset($IdStudentMarksEntry) && $IdStudentMarksEntry!=''){
			    			$markDB->updateData($data,$IdStudentMarksEntry);				    		   		  
			    		}else{	    

			    			$data["IdStudentRegistration"] = $IdStudentRegistration;
			    			$data["IdStudentRegSubjects"] =$IdStudentRegSubjects;  
				    		$data["IdSemester"] = $semester_id;
					    	$data["Course"] = $subject_id;   
					    	$data["IdMarksDistributionMaster"] = $idMaster;
					    	$data["MarksTotal"] = $main_component["Marks"]; 
					    	$data["Component"]   =  $main_component["IdComponentType"];
					    	$data["ComponentItem"]   =  ''; 				
					    	$data["Instructor"] = '';    		
					    	$data["AttendanceStatus"] = '';
					    	$data["MarksEntryStatus"] = 0; //entry
		    	
			    			$IdStudentMarksEntry = $markDB->addData($data);				    			
			    		}
    			}*/
	    				
   		
	    		
				if(isset($markObtained) && $markObtained!=''){
			    		
						$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
			  	    	$item = $oCompitem->getDataComponentItem($IdMarksDistributionDetails);	  	     			
		    			
			  	    	//get main component total mark
			  	    	$FinalMarksObtained = $cms_calculation->calculateMark($markObtained,$item["Weightage"],$item["Percentage"]);				    			
			  	    	
			  	    	
						
		    			$data["UpdUser"] = $auth->getIdentity()->iduser;
				    	$data["UpdDate"] = date('Y-m-d H:i:s');	    			
		    	
						//1st : Save Main
						if(isset($IdStudentMarksEntry) && $IdStudentMarksEntry!=''){
							
							//check ada lagi tak item lain under mark entri ni
							$item_mark = $markDetailsDB->getListComponentMark($IdStudentMarksEntry);
							
							$itemMarkObtained = 0;
							$itemFinalMarksObtained = 0;
							foreach($item_mark as $item){
								$itemMarkObtained = abs($itemMarkObtained) + abs($item['MarksObtained']);
								$itemFinalMarksObtained = abs($itemFinalMarksObtained) + abs($item['FinalMarksObtained']);
							}
							
							$data["TotalMarkObtained"] = abs($itemMarkObtained) + abs($markObtained); //Total Raw Mark each item++
		    			    $data["FinalTotalMarkObtained"]= abs($itemFinalMarksObtained) + abs($FinalMarksObtained); //afrer calculation made
			    										
			    			$markDB->updateData($data,$IdStudentMarksEntry);				    		   		  
			    		}else{	    

			    			
			    			$data["TotalMarkObtained"] = $markObtained; //Total Raw Mark each item++
		    			    $data["FinalTotalMarkObtained"]=$FinalMarksObtained; //afrer calculation made
			    			$data["IdStudentRegistration"] = $IdStudentRegistration;
			    			$data["IdStudentRegSubjects"] =$IdStudentRegSubjects;  
				    		$data["IdSemester"] = $semester_id;
					    	$data["Course"] = $subject_id;   
					    	$data["IdMarksDistributionMaster"] = $idMaster;
					    	$data["MarksTotal"] = $main_component["Marks"]; 
					    	$data["Component"]   =  $main_component["IdComponentType"];
					    	$data["ComponentItem"]   =  ''; 				
					    	$data["Instructor"] = '';    		
					    	$data["AttendanceStatus"] = '';
					    	$data["MarksEntryStatus"] = 0; //entry
		    	
			    			$IdStudentMarksEntry = $markDB->addData($data);				    			
			    		}
			    		
			    		
			    		
			    		//2nd : Save Details Component Mark  
			  	  		
				    	$dataItem["MarksObtained"] = $markObtained;
						$dataItem["FinalMarksObtained"] = $FinalMarksObtained;
				    	$dataItem["UpdUser"] = $auth->getIdentity()->iduser;
						$dataItem["UpdDate"] = date('Y-m-d H:i:s');
		    			
						if(isset($IdStudentMarksEntryDetail) && $IdStudentMarksEntryDetail!=''){
							
							 if($markObtained==''){
							 	
							 	//delete sebab null ibarat no mark
							 	$markDetailsDB->deleteData($IdStudentMarksEntryDetail);
							 	$IdStudentMarksEntryDetail='x';
							 	
							 	// count item mark ada lain x					 					  	 
						  	    $item_entry = $markDB->getMarkById($IdStudentMarksEntry);
						  	    if(count($item_entry)>0){
						  	    	//do nothing
						  	    }else{
				  	   				//x ada item delete main	
				  	   				//echo 'masuk';	  	   				
				  	   				$markDB->deleteData($IdStudentMarksEntry);
						 		    $IdStudentMarksEntry='x';
				  	   			}
							 	
							 }else{
		    					 $markDetailsDB->updateData($dataItem,$IdStudentMarksEntryDetail);	    					
							 }
		    				 
		    			}else{	    	
		    					
				    			$dataItem["IdStudentMarksEntry"] = $IdStudentMarksEntry;   
								$dataItem["Component"] = $main_component["IdComponentType"];   
								$dataItem["ComponentItem"]   =  '';
								$dataItem["ComponentDetail"] = $IdMarksDistributionDetails;				
						    	$dataItem["TotalMarks"] = $item["Weightage"]; //jumlah penuh soalan bukan pemberat
				    
		    				 	$IdStudentMarksEntryDetail  = $markDetailsDB->addData($dataItem);
		    			} 
    			}
    			
				     			
	    		
    		
    		$grade = $markDB->saveStudentSubjectMarkCe($IdStudentRegSubjects);
    		
    		$result = array( 'IdStudentMarksEntry'=>$IdStudentMarksEntry,
    		 			     'IdStudentMarksEntryDetail'=>$IdStudentMarksEntryDetail,
    		 				 'grade'=>$grade['grade_name']);
			
			$ajaxContext->addActionContext('view', 'html')
						->addActionContext('form', 'html')
						->addActionContext('process', 'json')
						->initContext();
		
			$json = Zend_Json::encode($result);
		
			echo $json;
			exit();
		
	}
	
	public function changeStatusAction(){
    	
	    	$auth = Zend_Auth::getInstance();
	    	$this->view->role = $auth->getIdentity()->IdRole;
	    	
	    	if ($this->getRequest()->isPost()) {
			
				$formData = $this->getRequest()->getPost();			
		
	    		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
	    		$markEntryHistoryDB = new Examination_Model_DbTable_StudentMarkEntryHistory();
	    	
	    		for($i=0; $i<count($formData['IdRegSub']); $i++){
	    			
	    			$data['mark_approval_status']=1; //SUbmit For Approval
	    			$subjectRegDB->updateData($data,$formData['IdRegSub'][$i]);
	    			
	    			//ini mcam audit trail la nak keep track changes 
    			    $status['IdStudentRegSubjects'] = $formData['IdRegSub'][$i];
    			    $status['cs_status'] = 1;
    			    $status['cs_remarks'] = '';
    			    $status["cs_createdby"] = $auth->getIdentity()->iduser;
    			    $status["cs_createddt"] = date('Y-m-d H:i:s');
    			    $markEntryHistoryDB->addChangeStatus($status);
	    		}
	    		
	    		$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');	    		
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry-ce', 'action'=>'entry','idSemester'=>$formData['idSemester'],'idSubject'=>$formData['idSubject']),'default',true));
		   
	    	}
	    	
	    	exit;
	   }
	   
	   public function printCeAction(){
	   		   	
	   		$auth = Zend_Auth::getInstance();
	   		
	   		$this->_helper->layout->disableLayout();
	   		
	   		$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale  = $locale;		
			
	    	$idSemester = $this->_getParam('idSemester');    	  	
	    	$idSubject = $this->_getParam('idSubject');    	
	    	
	    	//get component
	    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
			$list_component = $markDistributionDB->getMyListMainComponent($idSemester,$idSubject);	    
			
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	    	foreach($list_component as $index=>$component){
	    		
			  	  $component_item = $oCompitem->getMyListComponentItem($component["IdMarksDistributionMaster"]);
			  	  $list_component[$index]['component_item'] = $component_item;
	    	}
	    	$this->view->rs_component = $list_component;
	    	
	    	//get student list
	    	$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentRegDB->searchStudentCeEntryMark($idSemester,$idSubject);
	    	
	    	$db = Zend_Db_Table::getDefaultAdapter();	
	    	
			foreach($students as $key=>$student){
	    		
	    		foreach($list_component as $c=>$component){
	    			
	    			$select_main_mark = $db->select()
					 	 				  ->from(array('sme'=>'tbl_student_marks_entry'))
					 	 				  ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])				 	 			
					 	 				  ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
					 	 				 // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);		  
				 	$entry_main = $db->fetchAll($select_main_mark);	
				 	
				 	foreach($entry_main as $m=>$main){
				 		$students[$key]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
				 		$students[$key]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
				 		$students[$key]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];
				 		
				 		//get item
			 			$select_item_mark = $db->select()
				 	 				  ->from(array('smed'=>'tbl_student_detail_marks_entry'))				 	 			
				 	 				  ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);		  
				 		$entry_item = $db->fetchAll($select_item_mark);	
				 		
				 		foreach($entry_item as $i=>$item){
				 			$students[$key]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
				 			$students[$key]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
				 			$students[$key]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
				 		}
				 	}
	    		}	    		
	    	}
	    	
	    	$this->view->students = $students;
	    	
	    	$this->view->filename = date('Ymd').'_gradebookce.xls';
	   		
	   }
}