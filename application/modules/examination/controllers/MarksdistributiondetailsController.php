<?php
class examination_MarksdistributiondetailsController extends Base_Base { //Controller for the User Module
	private $lobjMarksdistributiondetailsForm;
	private $lobjMarksdistributiondetails;
	private $lobjdeftype;
	private $_gobjlog;
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjMarksdistributiondetailsForm = new Examination_Form_Marksdistributiondetails();
		$this->lobjMarksdistributiondetails = new Examination_Model_DbTable_Marksdistributiondetails();
		$this->lobjdeftype = new App_Model_Definitiontype();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$lobjMarksDistributionList = $this->lobjMarksdistributiondetails->fnGetMarksDistributionList();
		$lobjform->field5->addMultiOptions($lobjMarksDistributionList);

		$lobjProgramNameList = $this->lobjMarksdistributiondetails->fnGetProgramList();
		$lobjform->field8->addMultiOptions($lobjProgramNameList);


	 if(!$this->_getParam('search'))
	 	unset($this->gobjsessionsis->Marksdistributiondetailsresult);
		$larrresult = $this->lobjMarksdistributiondetails->fngetMarksDistributionMasterDetails(); //get user details
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Marksdistributiondetailsresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksdistributiondetailsresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjMarksdistributiondetails ->fnSearchMarksDistributionMasterDetails( $lobjform->getValues () ); //searching the values for the user

				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Marksdistributiondetailsresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/marksdistributiondetails/index');
		}
	}

	public function newmarksdistributiondetailsAction() { //Action for creating the new user

		$this->view->lobjMarksdistributiondetailsForm = $this->lobjMarksdistributiondetailsForm;
		$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster = ( int ) $this->_getParam ( 'id' );
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjMarksdistributiondetailsForm->IdMarksDistributionMaster->setValue ( $IdMarksDistributionMaster );
		$this->view->lobjMarksdistributiondetailsForm->UpdDate->setValue ( $ldtsystemDate );
		$larrrProgramName = $this->lobjMarksdistributiondetails->fnGetProgramNameDetails($IdMarksDistributionMaster);
		$this->view->Name = $larrrProgramName['Name'];
		$this->view->ProgramName = $larrrProgramName['ProgramName'];
		$larrstaffdetails=$this->lobjMarksdistributiondetails->fnGetstaffdetails();
		// echo "<pre>";print_r($larrstaffdetails);die();
		$larrsemesterdetails=$this->lobjMarksdistributiondetails->fnGetsemesterdetails();
		//echo "<pre>";print_r($larrsemesterdetails);die();
		$this->lobjMarksdistributiondetailsForm->idStaff->addMultiOptions($larrstaffdetails);
		$this->lobjMarksdistributiondetailsForm->idSemester->addMultiOptions($larrsemesterdetails);
		$larrdefmsPassStatus = $this->lobjdeftype->fnGetDefinations('Pass Status');
		//echo "<pre>";print_r($larrdefmsPassStatus);die();
		foreach($larrdefmsPassStatus as $larrdefmsPassStatusRes) {
			$this->lobjMarksdistributiondetailsForm->PassStatus->addMultiOption($larrdefmsPassStatusRes['idDefinition'],$larrdefmsPassStatusRes['DefinitionDesc']);
		}

		$auth = Zend_Auth::getInstance();
		$this->view->lobjMarksdistributiondetailsForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$larrresult = $this->lobjMarksdistributiondetails ->fnViewMarksDistributionDetails($IdMarksDistributionMaster);
		//echo "<pre>";print_r($larrresult);die();
		$this->view->larrresult=$larrresult;



		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			if ($this->lobjMarksdistributiondetailsForm->isValid ( $larrformData )) {
				$result = $this->lobjMarksdistributiondetails->fnAddMarksDistributionDetails($larrformData); //instance for adding the lobjuserForm values to DB

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Marks Distribution Details Add Id=' . $larrformData['IdMarksDistributionMaster'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/marksdistributiondetails/newmarksdistributiondetails/id/'.$larrformData['IdMarksDistributionMaster']);
			}
		}

	}
	public function deletemarksdistributiondtlsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();


		$IdMarksDistributionDetails = $this->_getParam('IdMarksDistributionDetails');
		$larrDelete = $this->lobjMarksdistributiondetails->fndeletemarksdistributiondtls($IdMarksDistributionDetails);
		echo "1";
	}


}