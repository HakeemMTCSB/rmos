<?php
class examination_SubjectstaffverificationController extends Base_Base { //Controller for the User Module
	private $lobjSubjectstaffverificationForm;
	private $lobjSubjectmaster;
	private $lobjSubjectstaffverification;
	private $lobjStaffmaster;
	private $lobjSemestermaster;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjSubjectstaffverificationForm = new Examination_Form_Subjectstaffverification();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjSubjectstaffverification = new Examination_Model_DbTable_Subjectstaffverification();
		$this->lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster ();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$larrresultsubjlist = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field5->addMultiOptions ( $larrresultsubjlist );

		//$larrresult = $this->lobjSubjectmaster->fnGetSubjectMasterList();

	 if(!$this->_getParam('search'))
	 	unset($this->gobjsessionsis->Subjectstaffverificationpaginatorresult);
	 $larrresult = $this->lobjSubjectmaster->fnGetSubjectMasterList();//get user details

	 // print_r($larrresult);exit;
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Subjectstaffverificationpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Subjectstaffverificationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();

			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjSubjectstaffverification ->fnSearchMarksEntry( $lobjform->getValues () ); //searching the values for the user
				/*echo"<pre>";
				 print_r($larrresult);die();*/
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Subjectstaffverificationpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {

			$this->_redirect( $this->baseUrl . '/examination/subjectstaffverification/');
		}
	}

	public function subjectstaffverificationlistAction() { //Action for creating the new user

		$this->view->lobjSubjectstaffverificationForm = $this->lobjSubjectstaffverificationForm;

		$idSubject = ( int ) $this->_getParam ( 'id' );

		$this->view->subjectName = ( string ) $this->_getParam ( 'name' );

		$this->view->lobjSubjectstaffverificationForm->IdSubject->setValue ( $idSubject );

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );

		$this->view->lobjSubjectstaffverificationForm->UpdDate->setValue ( $ldtsystemDate );


		$larrstafflist = $this->lobjStaffmaster->fngetStaffMasterListforDD();
		$this->view->lobjSubjectstaffverificationForm->IdStaff->addMultiOptions ( $larrstafflist );

		$lobjsemester = $this->lobjsemestermaster->fnGetSemestermasterList();
		$this->view->lobjSubjectstaffverificationForm->IdSemester->addMultiOptions($lobjsemester);


		$auth = Zend_Auth::getInstance();
		$this->view->lobjSubjectstaffverificationForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$result = $this->lobjSubjectstaffverification->fnEditSubjectStaffverify($idSubject);

		if($result) {
			$this->lobjSubjectstaffverificationForm->populate($result);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );

			if($larrformData ['SameLecturer']==1)$larrformData ['IdStaff']=0;

			if ($this->lobjSubjectstaffverificationForm->isValid ( $larrformData )) {
				if($larrformData ['IdSubjectStaffVerification']){
					$lintIdSubjectStaffVerification = $larrformData ['IdSubjectStaffVerification'];
					$this->lobjSubjectstaffverification->fnupdateSubjectStaffVerification($lintIdSubjectStaffVerification, $larrformData );
				}else{
					unset($larrformData['IdSubjectStaffVerification']);
					$result = $this->lobjSubjectstaffverification->fnAddSubjectStaffVerification($larrformData); //instance for adding the lobjuserForm values to DB
				}

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Subject Staff Verification Edit Id=' . $larrformData['IdSubject'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/subjectstaffverification');
			}
		}
	}
}