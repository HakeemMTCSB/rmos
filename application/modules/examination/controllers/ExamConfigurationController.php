<?php 

class Examination_ExamConfigurationController extends Base_Base {

	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
			$this->view->title= $this->view->translate("Exam Configuration");
			
			$auth = Zend_Auth::getInstance();
			$examConfigDB = new Examination_Model_DbTable_ExamConfiguration();
				
			$data = $examConfigDB->getData();
			
			
			if ($this->getRequest()->isPost()) {
				
				$formData = $this->getRequest()->getPost();
								
				if(!isset($formData['cont_prob_sem'])){
					$formData['cont_prob_sem']=0;
				}
				
				if(!isset($formData['mark_dist_approval'])){
					$formData['mark_dist_approval']=0;
				}
				
				$formData['ec_updatedt']=date('Y-m-d H:i:s');  
				$formData['ec_updateby']=$auth->getIdentity()->iduser;  

				unset($formData['submit']);
				
				if(isset($formData['ec_id']) && $formData['ec_id']!=''){
					
					$examConfigDB->updateData($formData,$formData['ec_id']);
				}else{
					$examConfigDB->addData($formData);
				}
			
				$data = $examConfigDB->getData();
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			}
			
			$this->view->data = $data;
	}
	
	
}
?>