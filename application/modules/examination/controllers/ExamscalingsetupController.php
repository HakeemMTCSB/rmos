<?php

class examination_ExamscalingsetupController extends Base_Base { //Controller for the User Module

	private $lobjsemesterModel;
	private $lobjschemeModel;
	private $lobjformexamscaling;
	private $lobjcollegemasterModel;
	private $lobjsubjectmasterModel;
	private $lobjexamselingsetupModel;
	private $lobjexamselingdetailModel;
	private $lobjassessmenttypeModel;
	private $lobjprogram;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjschemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjformexamscaling = new Examination_Form_Examscaling();
		$this->lobjcollegemasterModel = new GeneralSetup_Model_DbTable_Collegemaster();
		$this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjexamselingsetupModel = new Examination_Model_DbTable_Examscalingsetup();
		$this->lobjexamselingdetailModel = new Examination_Model_DbTable_Examscalingdetail();
		$this->lobjassessmenttypeModel = new Examination_Model_DbTable_Assessmenttype();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();

	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$lobjsemesterlist = $this->lobjsemesterModel->getAllsemesterListCode();
		$lobjform->field8->addMultiOptions($lobjsemesterlist);
		
		$lobjprogram = $this->lobjprogram->fnGetProgramList();
		$lobjform->field5->addMultiOptions($lobjprogram);

		$schemeList = $this->lobjschemeModel->fnGetSchemeDetails();
		foreach ($schemeList as $larrschemearr) {
			$lobjform->field10->addMultiOption($larrschemearr['IdScheme'], $larrschemearr['EnglishDescription']);
		}
		if (!$this->_getParam('search'))
			unset($this->gobjsessionsis->Academicstatuspaginatorresult);
		$larrresult = $this->lobjexamselingsetupModel->fngetAllsetup(); //get user details

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset($this->gobjsessionsis->Academicstatuspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Academicstatuspaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjexamselingsetupModel->fnsearchAllsetup($lobjform->getValues()); //searching the values for the user
				//echo "<pre>";print_r($larrresult);die();
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->Academicstatuspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/examination/examscalingsetup/index');
		}

	}

	public function addexamscalingsetupAction() {
		$this->view->lobjformexamscaling = $this->lobjformexamscaling;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjformexamscaling->UpdUser->setValue($auth->getIdentity()->iduser);
		$this->view->lobjformexamscaling->CreatedBy->setValue($auth->getIdentity()->iduser);

		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjformexamscaling->UpdDate->setValue($ldtsystemDate);
		$this->view->lobjformexamscaling->CreatedDate->setValue($ldtsystemDate);

		$idUniversity = $this->gobjsessionsis->idUniversity;
		//Get faculty list
		$facultyList = $this->lobjcollegemasterModel->fnGetCollegeListByUniversity($idUniversity);
		$this->view->lobjformexamscaling->IdFaculty->addMultioptions($facultyList);

		//$lobjdeftype = new App_Model_Definitiontype();
		//$componentlist = $lobjdeftype->fnGetDefinationMs('Subject Components');
		$larrcomponentlist = $this->lobjassessmenttypeModel->getdropdownforasseementtype();
		$this->view->lobjformexamscaling->IdComponent->addMultiOptions($larrcomponentlist);

		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();
			// declaration of scale setup and detail array
			$examscalsetupdata =  array();
			$examscaledetail = array();
			// now set the value in scale setup array
			$examscalsetupdata['IdScheme'] = $larrformData['IdScheme'];
			$examscalsetupdata['semestercode'] = $larrformData['semestercode'];
			$examscalsetupdata['IdFaculty'] = $larrformData['IdFaculty'];
			$examscalsetupdata['CreatedDate'] = $larrformData['CreatedDate'];
			$examscalsetupdata['CreatedBy'] = $larrformData['CreatedBy'];
			$examscalsetupdata['UpdUser'] = $larrformData['UpdUser'];
			$examscalsetupdata['UpdDate'] = $larrformData['UpdDate'];
			// Now check duplicate
			$checkduplicate = $this->lobjexamselingsetupModel->fnCheckduplicateScalingSetup($larrformData['semestercode'],$larrformData['IdScheme']);
			if(empty($checkduplicate)){
				// Now insert the exam scaling setup
				$retId = $this->lobjexamselingsetupModel->fnExamScalingSetup($examscalsetupdata);

				// Now set the value in scale setup detail array
				$len = count($larrformData['coursegrid']);
				for($i = 0; $i < $len; $i++){
					$examscaledetail['IdExamScaling'] = $retId;
					$examscaledetail['IdComponent'] = $larrformData['componentgrid'][$i];
					$examscaledetail['IdCourse'] = $larrformData['coursegrid'][$i];
					$examscaledetail['Marks'] = $larrformData['marksgrid'][$i];
					if(isset($larrformData[$i-1])){
						$examscaledetail['Active'] = 1;
					}else{
						$examscaledetail['Active'] = 0;
					}
					$this->lobjexamselingdetailModel->fnaddscalesetpdetail($examscaledetail);
				}
				$this->_redirect( $this->baseUrl . '/examination/examscalingsetup');
			}else{
				$this->view->errormsg = '1';
			}
		}
	}


	public function editexamscalingsetupAction(){
		$this->view->lobjformexamscaling = $this->lobjformexamscaling;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjformexamscaling->UpdUser->setValue($auth->getIdentity()->iduser);
		$this->view->lobjformexamscaling->CreatedBy->setValue($auth->getIdentity()->iduser);

		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjformexamscaling->UpdDate->setValue($ldtsystemDate);
		$this->view->lobjformexamscaling->CreatedDate->setValue($ldtsystemDate);

		$lintIdexamscale = (int) $this->_getParam('id');
		$examscaledata = $this->lobjexamselingsetupModel->fngetexamscaledata($lintIdexamscale);
		$this->view->examscaledata = $examscaledata;
		$this->view->lobjformexamscaling->IdExamScaling->setValue($lintIdexamscale);
		//$lobjdeftype = new App_Model_Definitiontype();
		//$componentlist = $lobjdeftype->fnGetDefinationMs('Subject Components');
		$larrcomponentlist = $this->lobjassessmenttypeModel->getdropdownforasseementtype();
		$this->view->lobjformexamscaling->IdComponent->addMultiOptions($larrcomponentlist);
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();
			//delete records indetail
			$examscaledata = $this->lobjexamselingdetailModel->fndeletedetailsetup($larrformData['IdExamScaling']);
			// Now set the value in scale setup detail array
			$len = count($larrformData['coursegrid']);
			for($i = 0; $i < $len; $i++){
				$examscaledetail['IdExamScaling'] = $larrformData['IdExamScaling'];
				$examscaledetail['IdComponent'] = $larrformData['componentgrid'][$i];
				$examscaledetail['IdCourse'] = $larrformData['coursegrid'][$i];
				$examscaledetail['Marks'] = $larrformData['marksgrid'][$i];
				if(isset($larrformData[$i])){
					$examscaledetail['Active'] = 1;
				}else{
					$examscaledetail['Active'] = 0;
				}
				$this->lobjexamselingdetailModel->fnaddscalesetpdetail($examscaledetail);
			}
			$this->_redirect( $this->baseUrl . '/examination/examscalingsetup');
		}
	}

	public function getcourselistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idscheme = $this->_getParam('idscheme');
		$idcomponent = $this->_getParam('idcomponent');
		$courselist = $this->lobjsubjectmasterModel->fngetcoursebycomponentandscheme($idscheme, $idcomponent);
		echo Zend_Json_Encoder::encode($courselist);
	}

}

?>
