<?php
class Examination_SubjectsmarksverificationController extends Base_Base {
	private $lobjSubjectsMarksVerificationModelbulk;
	private $lobjplacementtestmarksmodel;
	private $lobjinitialconfig;
	private $lobjstudentapplication;
	private $lobjStudentregistrationForm;
	private $_gobjlog;
	private $_gobjlogger;


	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj(){
		$this->lobjSubjectsMarksVerificationModelbulk = new Examination_Model_DbTable_Subjectsmarksverification();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistrationbulk();
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjGpacalculation = new Examination_Model_DbTable_Gpacalculation();
	}

	public function indexAction() {

		$takemarks = $this->view->TakeMarks;
		$idUniversity =$this->gobjsessionsis->idUniversity;
		$this->view->config = $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);
		$this->view->lobjform = $this->lobjform;
		$this->view->results = $larrresult= array();
		$this->view->lobjStudentRegForm = $this->lobjStudentregistrationForm;

		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
		$this->view->lobjform->field8->addMultiOptions($larProgramNameCombo);

		$larSubjectNameCombo = $this->lobjSubjectsMarksVerificationModelbulk->fngetSubjectNameCombo();
		$this->view->lobjform->field1->addMultiOptions($larSubjectNameCombo);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->results = $larrresult = $this->lobjSubjectsMarksVerificationModelbulk->fnSearchStudentsubjects( $this->lobjform->getValues ()); //searching the values for the user

			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post

			/*
			 $count=count($larrformData['idSubjectMarksEntry']);
			for($i=0;$i<$count;$i++) {
			$larrformDatasDelete ['idSubjectMarksEntry'] = $larrformData['idSubjectMarksEntry'][$i];
			$larrformDatasDelete ['idverifier'] = $larrformData['idverifier'][$i];

			$larrDeleteSubjectverifiedMarks = $this->lobjSubjectsMarksVerificationModelbulk->fnDeleteSubjectVerifiedmarksMarks($larrformDatasDelete);
			}*/

			$ldtsystemDate = date ( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance();


			$count=count($larrformData['idSubjectMarksEntry']);
			for($i=0;$i<$count;$i++) {
				if($larrformData['idVerifierMarks'][$i] == "") {
					if($larrformData['Rank'][$i] == "") {
						$larrformDatasUpdate ['Rank'] = 0;
					} else {
						$larrformDatasUpdate ['Rank'] = $larrformData['Rank'][$i];
					}

					if($larrformData['idverifier'][$i] == "") {
						$larrformDatasUpdate ['idverifier'] = NULL;
					} else {
						$larrformDatasUpdate ['idverifier'] = $larrformData['idverifier'][$i];
					}
					$larrformDatasUpdate ['idSubjectMarksEntry'] = $larrformData['idSubjectMarksEntry'][$i];
					$larrformDatasUpdate ['verifiresubjectmarks'] = $larrformData['verifiresubjectmarks'][$i];
					$larrformDatasUpdate ['UpdDate'] = $ldtsystemDate;
					$larrformDatasUpdate ['UpdUser'] = $auth->getIdentity()->iduser;
					$larrformDatasUpdate ['Active'] = 1;
					$larrAddSubjectMarks = $this->lobjSubjectsMarksVerificationModelbulk->fnAddSubjectVerifiedMarks($larrformDatasUpdate);
				} else {
					$larrUpdateSubjectMarks = $this->lobjSubjectsMarksVerificationModelbulk->fnUpdateSubjectVerifiedMarks($larrformData['idVerifierMarks'][$i],$larrformData['verifiresubjectmarks'][$i]);
				}
				$IdStudentRegistration[] = $larrformData['idVerifierMarks'][$i];
			}


			//GPA calculation
			for($k=0;$k<count($larrformData['idStudentregistration']);$k++){
					
				for($l=0;$l<count($larrformData['idSubject'][$larrformData['idStudentregistration'][$k]]);$l++){


					$larrresult = $this->lobjGpacalculation->fnGetSubjectprerequisitsvalidation($larrformData['idSubject'][$larrformData['idStudentregistration'][$k]][$l],$larrformData['idStudentregistration'][$k],$this->view->TakeMarks);

					$sum=0;
					$totcredithrs=0;

					for($i=0;$i<count($larrresult);$i++){
							
						$IDgpacalculation = $larrresult[0]['Idgpacalculation'];
							
						$larrgradepoint=$this->lobjGpacalculation->fnGetGradepoints($larrresult[$i]['IDCourse'],$larrresult[$i]['IdSemestersyllabus'],$larrresult[$i]['IdSubject'],$larrresult[$i]['fullcalculatedmarks']);

						for($j=0;$j<count($larrgradepoint);$j++){

							if(($larrresult[$i]['fullcalculatedmarks'] >= $larrgradepoint[$j]['MinPoint']) && ($larrresult[$i]['fullcalculatedmarks'] <= $larrgradepoint[$j]['MaxPoint'])){

								$sum = $sum+$larrgradepoint[$j]['GradePoint']+$larrgradepoint[$j]['CreditHours'];
								$totcredithrs=$totcredithrs+$larrgradepoint[$j]['CreditHours'];
							}
						}
					}
					if($totcredithrs==0)$totcredithrs=1;
					$grossgpa=$sum/$totcredithrs;
					$Roundgrossgpa=round($grossgpa,2);

				}
				$ldtsystemDate = date ( 'Y-m-d H:i:s' );
				$auth = Zend_Auth::getInstance();

				$larrformDatagpa['IdApplication']=$larrformData['idStudentApplication'];
				$larrformDatagpa['UpdDate']=$ldtsystemDate;
				$larrformDatagpa['UpdUser']=$auth->getIdentity()->iduser;
				$larrformDatagpa['Gpa']=$Roundgrossgpa;
				$larrformDatagpa['Idgpacalculation']=$IDgpacalculation;
				$larrformDatagpa['IdStudentRegistration']=$larrformData['idStudentregistration'][$k];



				if(!$larrformDatagpa['Idgpacalculation']){
					$this->lobjGpacalculation->fnSavegpa($larrformDatagpa);
				}else{
					$this->lobjGpacalculation->fnUpdategpa($larrformDatagpa,$larrformDatagpa['IdApplication']);
				}

			}


			//**************************************************


			////////////////////////////////////////////////////////////////////////////////////////


			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Subject Marks Verification Edit' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			//log file
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Subject Marks Verification Edit"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);


			///////////////////////////////////////////////////////////////////////////////////////


		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/subjectsmarksverification/index');
		}

	}
}