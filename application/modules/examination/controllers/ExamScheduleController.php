<?php 
class Examination_ExamScheduleController extends Base_Base {
	
	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate("Exam Schedule");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
	}
	
	public function courseAction() {
		
		
		$this->view->title = $this->view->translate("Setup Exam Schedule > Exam Schedule Setup - by Course");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
				
		$session = Zend_Registry::get('sis');
		
		$subjectOffDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
		$form = new Examination_Form_SearchExamSchedule();
				
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
    	
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
				
				unset($session->result);
				
				$form->populate($formData);
				
				$this->view->idSemester = $formData['IdSemester'];
				$this->view->idCollege = $formData['IdCollege'];
				//$this->view->subname = $formData['subject_name'];
				//$this->view->subcode = $formData['subject_code'];
				
				//get list course offerrd 
				
				$subject_offered = $subjectOffDB->searchSubjectOffered($formData);
				$this->view->subject_offered = $subject_offered;
			
				$session->result = $formData;
				//echo '<pre>';
				//print_r($subject_offered);
			}
		}else{ 
    		
    		//populate by session 
	    	if (isset($session->result)) {    	    			
	    		
	    		$form->populate($session->result);
	    		$formData = $session->result;
				$this->view->idSemester = $formData['IdSemester'];
				$this->view->idCollege = $formData['IdCollege'];				
	    		$subject_offered = $subjectOffDB->searchSubjectOffered($formData);
	    		$this->view->subject_offered = $subject_offered;
	    	}
    		
    	}
		
    	
		
		$this->view->form = $form;
		
	}
	
	
	public function addScheduleCourseAction() {
				
		$auth = Zend_Auth::getInstance();
			
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			// echo '<pre>';
			// print_r($formData);
			// echo '</pre>';
			$examScheduleDB = new Examination_Model_DbTable_ExamSchedule();
			
			for($i=0; $i<count($formData['IdSubject']); $i++){
				
				$IdSubject = $formData['IdSubject'][$i];
				$es_id = $formData['es_id'][$IdSubject];
				
				$data['es_modifydt'] = date('Y-m-d H:i:s'); 
				$data['es_modifyby'] = $auth->getIdentity()->iduser;
								
				if($formData['es_date'][$IdSubject]=='' && $formData['es_start_time'][$IdSubject]=='' && $formData['es_end_time'][$IdSubject]==''){
					//no data dont add
				}else{
					
					if($formData['es_date'][$IdSubject]!=''){
						$data['es_date'] = date('Y-m-d',strtotime($formData['es_date'][$IdSubject]));
					}else{
						$data['es_date'] = null;
					}
					
					
					if($formData['es_start_time'][$IdSubject]!=''){
						$data['es_start_time'] = $formData['es_start_time'][$IdSubject];
					}else{
						$data['es_start_time'] = null;
					}
					
					
					if($formData['es_end_time'][$IdSubject]!=''){
						$data['es_end_time'] = $formData['es_end_time'][$IdSubject];
					}else{
						$data['es_end_time'] = null;
					}
					
					
					
					if(isset($es_id) && $es_id!=''){
						//echo 'update'.$es_id.'<br />';
						$examScheduleDB->updateData($data,$es_id);
					}else{
						//echo 'add'.$es_id.'<br />';	

						$data['es_semester'] = $formData['IdSemester'];
						$data['es_course']   = $IdSubject;
						//print_r($data);
						//die;
						$examScheduleDB->addData($data);
					}
					
				}
				unset($data);
			}
			
			//die;
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-schedule', 'action'=>'course'),'default',true));
			//$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-schedule', 'action'=>'course','idSemester'=>$formData['idSemester'],'idCollege'=>$formData['idCollege'],'subname'=>$formData['subname'],'subcode'=>$formData['subcode']),'default',true));
		}
		
		
	}
	
	
	public function examcenterAction() {
		
		$this->view->title = $this->view->translate("Setup Exam Schedule > Exam Schedule Setup - by Exam Center");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		
		$form = new Examination_Form_SearchExamCenterSchedule();
		$examScheduleDB = new Examination_Model_DbTable_ExamSchedule();
		
		if ($this->getRequest()->isPost() && $this->getRequest()->getPost('search')) {
			
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$this->view->idSemester = $formData['IdSemester'];			
				$this->view->idSubject = $formData['IdSubject'];
				$this->view->ec_id = $formData['ec_id'];			
		
				//get addedd schedule by exam center				
				$this->view->schedule = $examScheduleDB->getScheduleByExamCenter($formData);
			}			
			
		}
		
		$this->view->form = $form;
		
	}
	
	
	public function addScheduleEcAction() {
				
		$this->view->title = $this->view->translate("Setup Exam Schedule > Exam Schedule Setup - by Exam Center");	
			
		$auth = Zend_Auth::getInstance();
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
			
		$form = new Examination_Form_SearchAddExamCenterSchedule();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			$form->populate($formData);
			
			$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
			
			$formData['ec_country']  = $formData['idCountry'];
			$formData['ec_city']  = $formData['idCity'];			
			$this->view->idCountry = $formData['idCountry'];
			
    		$this->view->exam_center_list = $examCenterDB->getDataInfo($formData);    	
	
    		//echo '<pre>';
			//print_r($this->view->exam_center_list);
			
			//get info semester
    		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    		$this->view->semester = $semesterDB->fnGetSemesterList();
    		    		
		}
		
		$this->view->form = $form;
	}
	
	public function saveAction(){
		
		if ($this->getRequest()->isPost() ) {
			
			$auth = Zend_Auth::getInstance();
			
			$formData = $this->getRequest()->getPost();

			//echo '<pre>';
			//print_r($formData);
			
				
			if($formData['es_date']!=''){
				$data['es_date'] = date('Y-m-d',strtotime($formData['es_date']));
			}else{
				$data['es_date'] = null;
			}
			
			
			if($formData['es_start_time']!=''){
				$data['es_start_time'] = $formData['es_start_time'];
			}else{
				$data['es_start_time'] = null;
			}
			
			if($formData['es_end_time']!=''){
				$data['es_end_time'] = $formData['es_end_time'];
			}else{
				$data['es_end_time'] = null;
			}
				
			$data['es_semester'] = $formData['idSemester'];
			$data['es_course']   = $formData['idSubject'];
			$data['es_modifydt'] = date('Y-m-d H:i:s'); 
			$data['es_modifyby'] = $auth->getIdentity()->iduser;
									
			$examScheduleDB = new Examination_Model_DbTable_ExamSchedule();
						
			for($i=0; $i<count($formData['ec_id']); $i++){				
				$data['es_exam_center'] = $formData['ec_id'][$i];
				$examScheduleDB->addData($data);
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-schedule', 'action'=>'examcenter'),'default',true));
			//exit;
		}
	}
	
	
	public function editAction(){
		
		if ($this->getRequest()->isPost()) {
			
			$formdata = $this->getRequest()->getPost();
			
			$examScheduleDB = new Examination_Model_DbTable_ExamSchedule();
			
			if($formdata['es_date']!=''){
				$data['es_date'] = date('Y-m-d',strtotime($formdata['es_date']));
			}else{
				$data['es_date'] = null;
			}
			
			
			if($formdata['es_start_time']!=''){
				$data['es_start_time'] = $formdata['es_start_time'];
			}else{
				$data['es_start_time'] = null;
			}
			
			if($formdata['es_end_time']!=''){
				$data['es_end_time'] = $formdata['es_end_time'];
			}else{
				$data['es_end_time'] = null;
			}
				
			$examScheduleDB->updateData($data,$formdata['es_id']);
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-schedule', 'action'=>'examcenter'),'default',true));
		}
	}
	
	public function getCourseOfferedAction()
	{	
    	$idSemester = $this->_getParam('idSemester', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $subjectOffDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
		$result = $subjectOffDB->searchSubjectOffered(array('IdSemester'=>$idSemester));
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
    
	public function getDefaultAction()
	{	
    	$idSemester = $this->_getParam('idSemester', 0);
    	$idSubject = $this->_getParam('idSubject', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $examScheduleDB = new Examination_Model_DbTable_ExamSchedule();
		$result = $examScheduleDB->getScheduleBySubject($idSemester,$idSubject);
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
}


	

?>