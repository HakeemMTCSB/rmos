<?php
class Examination_GpacalculationController extends Base_Base {
	private $lobjplacementtestmarksmodel;
	private $lobjGpacalculation;
	private $lobjGpacalculationForm;
	private $_gobjlog;


	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj(){
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		$this->lobjGpacalculation = new Examination_Model_DbTable_Gpacalculation();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistrationbulk();
		$this->lobjGpacalculationForm = new Examination_Form_Gpacalculation();
		$this->lobjGradesetup = new Examination_Model_DbTable_Gradesetup();
	}

	public function indexAction() {
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view

		$lobjProgramNameList = $this->lobjGradesetup->fnGetProgramNameList();
		$this->view->lobjform->field8->addMultiOptions($lobjProgramNameList);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->view->lobjform->isValid ( $larrformData )) {
				$this->view->results = $larrresult = $this->lobjGpacalculation->fnSearchStudentsubjects( $this->view->lobjform->getValues ()); //searching the values for the user
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/gpacalculation/index');
		}

	}

	public function newgpacalculationAction() { //Action for creating the new user

		$this->view->lobjGpacalculationForm = $this->lobjGpacalculationForm;

		$IdApplication = ( int ) $this->_getParam ( 'id' );
		$Idregistration = (int) $this->_getParam ( 'regid' );
		$Intsubjectid = (int) $this->_getParam ( 'subjid' );

		$this->view->lobjGpacalculationForm->IdApplication->setValue ( $IdApplication );
		$this->view->lobjGpacalculationForm->IdApplication->setAttrib ('readonly','true');

		$lobjStudentsList = $this->lobjGpacalculation->fnGetStudentsList();
		$this->lobjGpacalculationForm->IdApplication->addMultiOptions($lobjStudentsList);


		$larrresult = $this->lobjGpacalculation->fnGetSubjectprerequisitsvalidation($Intsubjectid,$Idregistration,$this->view->TakeMarks);
			
		$sum=0;
		$totcredithrs=0;

		for($i=0;$i<count($larrresult);$i++){
			$this->view->lobjGpacalculationForm->Idgpacalculation->setValue( $larrresult[0]['Idgpacalculation'] );
			$larrgradepoint=$this->lobjGpacalculation->fnGetGradepoints($larrresult[$i]['IDCourse'],$larrresult[$i]['IdSemestersyllabus'],$larrresult[$i]['IdSubject'],$larrresult[$i]['fullcalculatedmarks']);


			for($j=0;$j<count($larrgradepoint);$j++){
					
				if(($larrresult[$i]['fullcalculatedmarks'] >= $larrgradepoint[$j]['MinPoint']) && ($larrresult[$i]['fullcalculatedmarks'] <= $larrgradepoint[$j]['MaxPoint'])){

					$sum = $sum+$larrgradepoint[$j]['GradePoint']+$larrgradepoint[$j]['CreditHours'];
					$totcredithrs=$totcredithrs+$larrgradepoint[$j]['CreditHours'];
				}
			}
		}
		if($totcredithrs==0)$totcredithrs=1;
		$grossgpa=$sum/$totcredithrs;
		$Roundgrossgpa=round($grossgpa,2);

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjGpacalculationForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjGpacalculationForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		$this->view->lobjGpacalculationForm->Gpa->setValue( $Roundgrossgpa );
		$this->view->lobjGpacalculationForm->IdStudentRegistration->setValue( $Idregistration );

			
		$this->view->larrresult=$larrresult;

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				echo "<pre>";
				print_r($larrformData);
				exit;
				if(!$larrformData['Idgpacalculation']){
					$this->lobjGpacalculation->fnSavegpa($larrformData);
				}else{
					$this->lobjGpacalculation->fnUpdategpa($larrformData,$larrformData['IdApplication']);
				}


				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'GPA Calculation Edit Id=' . $larrformData['IdStudentRegistration'],
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log


				$this->_redirect( $this->baseUrl . '/examination/gpacalculation/index');
			}

		}

	}
}