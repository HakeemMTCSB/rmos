<?php
class examination_MarksentryindividualController extends Base_Base { //Controller for the User Module
	private $lobjMarksentryForm;
	private $lobjSubjectmaster;
	private $lobjmarksentrysetup;
	private $lobjStaffmaster;
	private $lobjprogram;
	private $lobjStudentRegModel;
	private $lobjschemesetupmodel;
	private $lobjdeftype;
	private $lobjsemester;
	private $_gobjlog;
	private $lobjcoursetaggingModel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjMarksentryForm = new Examination_Form_Marksentry();
		$this->lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjmarksentrysetup = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjStudentRegModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjcoursetaggingModel = new GeneralSetup_Model_DbTable_Coursetagging();		
	}


	/**
	 * Function to search students
	 * @author: VT
	 */
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		$this->view->iduser = $auth->getIdentity()->iduser;
		$instructor = $auth->getIdentity()->iduser;
		
		// Listed down the lecturer
		$data = $this->lobjStaffmaster->fngetStaffMasterListforDD();
		$this->view->lobjform->field29->addMultiOptions($data);
		// SHOW THE COURSES
		$larrresultCourses = $this->lobjSubjectmaster->fnGetSubjectList();
		$this->view->lobjform->field23->addMultiOptions( $larrresultCourses );
		
		$this->view->lobjform->field29->setAttrib('onchange','getCourses(this)');
		$this->view->lobjform->field23->setAttrib('onchange','getGroup(this)');
		//$this->view->lobjform->field26->setAttrib('onchange','getLecturer(this)');

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field24->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
		}

		// SHOW THE SEMESTER
		//$larrsemresult =  $this->lobjStudentRegModel->fetchAllSemMaster();
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}

		// SHOW THE SCHEME
		$larrschemelist = $this->lobjschemesetupmodel->fnGetSchemeDetails();
		foreach($larrschemelist as $larrvalues) {
			$this->view->lobjform->field25->addMultiOption($larrvalues['IdScheme'],$larrvalues['EnglishDescription']);
		}

		// SHOW THE COMPONENTS
		//$larrComponents = $this->lobjdeftype->fnlistDefination('40');
		//foreach($larrComponents as $larrvalues) {
		//	$this->view->lobjform->field26->addMultiOption($larrvalues['idDefinition'],$larrvalues['DefinitionDesc']);
		//}

		$this->view->lobjform->Search->setAttrib('Onclick','return formsubmit();');

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Marksentrypaginatorresult);


		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Marksentrypaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Marksentrypaginatorresult,$lintpage,$lintpagecount);
		}
		$i = '0';
		$larrresultFinal = array();
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();			
			if ($lobjform->isValid ( $larrformData )) {			
				
				if($larrformData['field26'] != ''){									
					$larrresult = $this->lobjmarksentrysetup->fnSearchStudentofGroup($lobjform->getValues ());
				}else{					
					$larrresult = $this->lobjmarksentrysetup->fnSearchMarksEntry( $lobjform->getValues () ,$instructor);
				}
				

				//$larrresult = $this->lobjmarksentrysetup->fnSearchMarksEntry( $lobjform->getValues () ,$instructor);

				foreach ($larrresult as $values) {
					$IdSubject = $values['IdSubject'];
					$IdStudentRegistration = $values['IdStudentRegistration'];
					$IdStudentRegSubjects = $values['IdStudentRegSubjects'];
					$returnComp = $this->lobjmarksentrysetup->fnfetchAllComponents($IdSubject,$IdStudentRegistration,$IdStudentRegSubjects);
					if($returnComp!='') {
						$larrresultFinal[$i]= $values;
						$larrresultFinal[$i]['Idcomponents']= $returnComp;
						$i++;
					}
				}
								
				$this->view->paginator = $lobjPaginator->fnPagination($larrresultFinal,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Marksentrypaginatorresult = $larrresultFinal;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/marksentryindividual');
		}
	}


	/**
	 * Function to update marks for the student marks component
	 * @author: VT
	 */

	public function marksentrylistAction() {

		$this->view->lobjMarksentryForm = $this->lobjMarksentryForm;
		$idstudent = ( int ) $this->_getParam ( 'idstudent' );
		$idprogram = ( int ) $this->_getParam ( 'idprogram' );
		$idcourse = ( int ) $this->_getParam ( 'idcourse' );
		$IdStudentRegSubjects = ( int ) $this->_getParam ( 'id' );
		$auth = Zend_Auth::getInstance();
		$this->view->iduser = $auth->getIdentity()->iduser;
		$instructor = $auth->getIdentity()->iduser;
		$this->view->instructor = $instructor;
		$this->view->course = $idcourse;
		$this->view->IdStudentRegistration = $idstudent;
		$this->view->IdStudentRegSubjects = $IdStudentRegSubjects;
		//$auth = Zend_Auth::getInstance();
		//$this->view->iduser = $auth->getIdentity()->iduser;

		$this->view->idstudentregistration = $this->view->coursename = $this->view->prgName = $this->view->semName = NULL;

		// SHOW THE STUDENT ATTENDANCE STATUS

		$larrSAS = $this->lobjdeftype->fnGetDefinationMs('Student Attendance Status');		
		$this->view->attendancestatus = $larrSAS;

		$studentdetails = $this->lobjStudentRegModel->fnfetchStudentRegID($idstudent);
		$this->view->idstudentregistration = $studentdetails[0]['registrationId'];
		$studentProfileStatus = $studentdetails[0]['profileStatus'];
		if($studentProfileStatus=='92' || $studentProfileStatus=='248' || $studentProfileStatus=='253') { //active, defer and dormant
			$this->view->disbaleSubmit = '1';
		} else {
			$this->view->disbaleSubmit = '0';
		}



		$subjectdetails = $this->lobjSubjectmaster->fngetsubjectdetailsbyid($idcourse);
		$this->view->coursename = $subjectdetails[0]['SubjectName'].'-'.$subjectdetails[0]['SubCode'];

		$programdetails = $this->lobjprogram->fnfindprogram($idprogram);
		$this->view->prgName = $programdetails[0]['ProgramName'];
		$ret = $this->lobjprogram->fngetschemeofstudent($idstudent);
		$idScheme = $ret['IdScheme'];
		$idFaculty = $programdetails[0]['IdCollege'];

		$semester = '';
		$semresult =  $this->lobjStudentRegModel->fngetsemesterbystudcourse($idstudent,$idcourse);
		if(!empty($semresult)){
			$semester  = $semresult[0]['semCode'];
		}
		$this->view->semName = $semester;


		// Show the component Details
		$larrresult = $this->lobjmarksentrysetup->fnSearchMarksEntryDetails( $idcourse ,$idprogram, $semester ,$idstudent, $idScheme, $idFaculty, $instructor);

		$i = '0';
		$larrresultFinal = array();
		// check entry for IdStudentRegSubjects
		$larrresultEntry = $this->lobjmarksentrysetup->fngetRecordExist( $IdStudentRegSubjects, $idcourse );
		if(count($larrresultEntry)>0) {

			foreach ($larrresult as $values) {
				$IdMarksDistributionMaster = $values['IdMarksDistributionMaster'];
				$Idcmp = $values['IdComponentTypeMain'];
				$Idcmpitem = $values['IdComponentItemMain'];
				$returnCompDetail = $this->lobjmarksentrysetup->fnfetchAllComponentsDetailsIndividualAfterinsert($IdMarksDistributionMaster,$idcourse ,$Idcmp, $idstudent,$Idcmpitem);
				if($returnCompDetail!='') {
					$larrresultFinal[$i]= $values;
					$larrresultFinal[$i]['IdcomponentsDetails']= $returnCompDetail;
					$i++;
				}
			}
			$this->view->recordEx = '1';
		} else {

			foreach ($larrresult as $values) {
				$IdMarksDistributionMaster = $values['IdMarksDistributionMaster'];
				$Idcmp = $values['IdComponentTypeMain'];
				$Idcmpitem = $values['IdComponentItemMain'];
				$returnCompDetail = $this->lobjmarksentrysetup->fnfetchAllComponentsDetailsIndividual($IdMarksDistributionMaster,$idcourse ,$Idcmp, $idstudent, $Idcmpitem);
				if($returnCompDetail!='') {
					$larrresultFinal[$i]= $values;
					$larrresultFinal[$i]['IdcomponentsDetails']= $returnCompDetail;
					$i++;
				}
			}
			$this->view->recordEx = '0';
		}

		$this->view->finalResult = $larrresultFinal;		

		// get approved components only
		$larrresultApprComp = $this->lobjmarksentrysetup->getApprovedComponents( $idcourse ,$idprogram, $semester ,$idstudent, $idScheme, $idFaculty);
		if(count($larrresultApprComp)>0) {
			$this->view->larrresultApprCompStatus = $larrresultApprComp[0]['Status'];
		}

		// get marks status
		$larrresultMES = $this->lobjmarksentrysetup->fngetMarksStatus( $idcourse ,$idstudent);
		if(count($larrresultMES)>0) {
			$this->view->larrresultMES = $larrresultMES;
		}

		if ($this->_request->isPost () ) {
			$larrformData = $this->_request->getPost ();

			$marksentrysttaus = '311';
			if($larrformData['savedata']=='savedata') {
				$marksentrysttaus = '311';
			} else if($larrformData['savedata']=='submitdata' ) {
				$marksentrysttaus = '312';
			}else if($larrformData['savedata']=='approvedata' ) {
				$marksentrysttaus = '313';
			}else if($larrformData['savedata']=='rejectdata' ) {
				$marksentrysttaus = '311';
			}

			$returnCompDetail = $this->lobjmarksentrysetup->fnInsertComponentIndividual($larrformData,$marksentrysttaus,$IdStudentRegSubjects,$instructor);
			$this->_redirect( $this->baseUrl . '/examination/marksentryindividual/marksentrylist/id/'.$IdStudentRegSubjects.'/idstudent/'.$idstudent.'/idprogram/'.$idprogram.'/idcourse/'.$idcourse);

		}
	}
	
	public function getgroupAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();	
		$IdCourse = $this->_getParam('IdCourse');
		$larrDetails = $this->lobjcoursetaggingModel->getCoursetagGroup($IdCourse);
		if(count($larrDetails)>0){
			echo Zend_Json_Encoder::encode($larrDetails);
		}
	}
	
	public function getlecturerAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$IdCourse = $this->_getParam('courseId');
		$Idgroup = $this->_getParam('Idgroup');
		
		$larrDetails = $this->lobjcoursetaggingModel->getCoursetagLecturer($IdCourse,$Idgroup);
		if(count($larrDetails)>0){
			echo Zend_Json_Encoder::encode($larrDetails);
		}
	
	}
	
	public function getlecturercoursesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$IdLecturer = $this->_getParam('IdLecturer');		
		$larrDetails = $this->lobjcoursetaggingModel->fngetLecturerCourses($IdLecturer);
		if(count($larrDetails)>0){
			echo Zend_Json_Encoder::encode($larrDetails);
		}
	
	}
}
