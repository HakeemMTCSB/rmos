<?php

class examination_GenerateexamscalingController extends Base_Base {

	private $lobjgenerateexamscalingForm;
	private $lobjprogramModel;
	private $lobjstudentregistrationModel;
	private $lobjexamscalingModel;
	private $lobjmarksentrysetupModel;
	private $lobjgenerategradeModel;
	private $lobjremarkapplicationModel;
	private $lobjcollegemasterModel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjgenerateexamscalingForm = new Examination_Form_Generateexamscaling();
		$this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
		$this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjexamscalingModel = new Examination_Model_DbTable_Examscalingsetup();
		$this->lobjmarksentrysetupModel = new Examination_Model_DbTable_Marksentrysetup();
		$this->lobjgenerategradeModel = new Examination_Model_DbTable_Generategrade();
		$this->lobjremarkapplicationModel = new Examination_Model_DbTable_Remarkapplication();
		$this->lobjcollegemasterModel = new GeneralSetup_Model_DbTable_Collegemaster();
	}

	public function indexAction() {
		$this->view->lobjgenerateexamscalingForm = $this->lobjgenerateexamscalingForm;
		$auth = Zend_Auth::getInstance();
		$lIntidUniversity = $this->gobjsessionsis->idUniversity;
		$larrayFacultyList = $this->lobjcollegemasterModel->fnGetCollegeListByUniversity($lIntidUniversity);
		$this->view->lobjgenerateexamscalingForm->IdFaculty->addMultioptions($larrayFacultyList);
		$this->view->hideProcessScalingButtton = '0';

		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();

			$this->view->searchdata = $larrformData;
			// Now get the student list
			$larrstudentList = $this->lobjstudentregistrationModel->fngetstudentbyschemeprogramsemester($larrformData['IdScheme'],$larrformData['IdFaculty'],$larrformData['semestercode']);
			if(count($larrstudentList)==0) {
				$this->view->hideProcessScalingButtton = '1';
			}
			//$studentList = $this->lobjstudentregistrationModel->getstudentByschemeprogramsemester($larrformData['IdScheme'],$larrformData['IdProgram'],$larrformData['semestercode']);
			if(!empty($larrstudentList)){
				$this->view->studentlist = $larrstudentList;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();

			//Now check setup is exist for giving scheme,course and component combination
			$existindsetup = $this->lobjexamscalingModel->checkscalingsetups($larrformData['IdScheme'],$larrformData['IdCourse'],$larrformData['IdComponent']);


			if(count($existindsetup)>0){
				foreach($larrformData['student'] as $idstudent){
					// first update marks for component in tbl_student_marks_entry table
					$ret = $this->lobjmarksentrysetupModel->getmarksentry($larrformData['IdComponent'],$larrformData['IdCourse'],$idstudent);
					if(!empty($ret)){
						if($ret[0]['TotalMarkObtainedScaling'] == ''){
							$marksarray['TotalMarkObtainedScaling'] = $ret[0]['TotalMarkObtained'] + $existindsetup[0]['Marks'];

							$this->lobjmarksentrysetupModel->updatetmarksentry($marksarray,$ret[0]['IdStudentMarksEntry']);

							$larrstudentdet = $this->lobjstudentregistrationModel->fnfetchStudentProgSchmDetails($idstudent);

							if(!empty($larrstudentdet)){
								// Now calculate and update cpascaling and cgpascaling point
								$this->lobjgenerategradeModel->fnGetStudentsCGPAofrExamResitScaling( $larrformData['semestercode'], $idstudent, $larrstudentdet[0]['IdProgram'], $auth->getIdentity()->iduser );
								// Now update gradepointscasling
								$this->lobjremarkapplicationModel->fnupdateStudentGradePointScaling($larrformData['IdCourse'], $larrformData['semestercode'], $idstudent, $larrstudentdet[0]['IdProgram']);
							}
						}
					}
				}
			}
			$this->_redirect($this->baseUrl . '/examination/generateexamscaling');
		}
	}

	public function getprogramlistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idscheme = $this->_getParam('idscheme');
		$programList = $this->lobjprogramModel->fngetprogrambyScheme($idscheme);
		echo Zend_Json_Encoder::encode($programList);
	}

}

?>
