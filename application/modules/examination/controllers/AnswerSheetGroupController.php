<?php

class Examination_AnswerSheetGroupController extends Base_Base {

	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Upload OMR");
		 
		$msg = $this->_getParam('msg', null);
				
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Information saved successfully");
		}
		
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		//faculty
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
		
		if($this->_sis_session->IdRole == 1){
			$collegeList = $collegeDb->getCollege();
		}else{
			$this->view->default_faculty = $this->_sis_session->idCollege;
			$collegeList = array('0'=>$collegeDb->fngetCollegemasterData($this->_sis_session->idCollege));
		}
		
		$this->view->college_list = $collegeList;
		
	}
	
	public function groupDetailAction(){
		
		$id = $this->_getParam('id', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$msg = $this->_getParam('msg', null);
		
		$this->view->group_id = $id;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Information saved successfully");
		}
		
		//title
		$this->view->title= $this->view->translate("Exam Group");
		
		$examGroupDb = new Examination_Model_DbTable_ExamGroup();		
		//group data
		$group = $examGroupDb->getData($id);
		$this->view->group = $group;
						
		//supervisor data
		$examSupervisorDb = new Examination_Model_DbTable_ExamGroupSupervisor();
		$this->view->supervisor_list = $examSupervisorDb->getSupervisorList($id);

		//student data
		$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
		$this->view->student_list = $examGroupStudentDb->getStudentList($id);
	
		$form = new Examination_Form_AnswerSheetGroupUpload(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject,'id'=>$id));
		$this->view->form = $form;
		
	}
	
	
	
	public function searchCourseAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
	
		$sis_session = new Zend_Session_Namespace('sis');
		
		if($faculty_id!=null){
			$ses_att_pdf = new Zend_Session_Namespace('att_pdf_ses');
			$ses_att_pdf->faculty_id = $faculty_id;
		}
	
		if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Subject
			*/
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
			//Subject list base on Program Landscape from the selected faculty
			$programs = $progDB->fngetProgramDetails($faculty_id);
				
				
			$allsemlandscape = null;
			$allblocklandscape = null;
			$i=0;
			$j=0;
			foreach ($programs as $key => $program){
				$activeLandscape=$landscapeDB->getAllActiveLandscape($program["IdProgram"]);
				foreach($activeLandscape as $actl){
					if($actl["LandscapeType"]==43){
						$allsemlandscape[$i] = $actl["IdLandscape"];
						$i++;
					}elseif($actl["LandscapeType"]==44){
						$allblocklandscape[$j] = $actl["IdLandscape"];
						$j++;
					}
				}
			}
				
				
			$subjectsem = null;
			$subjectblock = null;
				
			if(is_array($allsemlandscape))
				$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape);
			if(is_array($allblocklandscape))
				$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape);
				
			$subjects = null;
			if(is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=array_merge( $subjectsem , $subjectblock );
			}else{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=$subjectblock;
				}
			}
				
				
			//end subject list
			if(count($subjects)==0){
				//Original from yati
				$subjectDb = new  GeneralSetup_Model_DbTable_Subjectmaster();
				$subjects = $subjectDb->getSubjectByCollegeId( array('IdCollege'=>$faculty_id,'IdSemester'=>$semester_id,'SubjectCode'=>null) );
			}
			$i=0;
			foreach($subjects as $subject){
					
				//get total student register this subject
				$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
				$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$semester_id);
				$subject["total_student"] = $total_student;
					
				//get total group creates
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$semester_id);
				$subject["total_group"] = $total_group;
				$subject["IdSemester"] = $semester_id;
					
				//get total no of student has been assigned
				$total_assigned = $subjectRegDB->getTotalAssigned($subject["IdSubject"],$semester_id);
				$total_unassigned = $subjectRegDB->getTotalUnAssigned($subject["IdSubject"],$semester_id);
				$subject["total_assigned"] = $total_assigned;
				$subject["total_unassigned"] = $total_unassigned;
					
				$subjects[$i]=$subject;
					
				$i++;
			}
				
			foreach ($subjects as $index => $subject){
				if($subject["total_student"]>0 && $subject["total_group"]>0){
						
				}else{
					unset($subjects[$index]);
				}
			}
			/*
			 * End search subject
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($subjects);
	
			echo $json;
			exit();
		}
	}
	
	public function searchExamGroupAction(){
		$this->_helper->layout()->disableLayout();
	
		$semester_id = $this->_getParam('semester_id', null);
		$faculty_id = $this->_getParam('faculty_id', null);
		$subject_id = $this->_getParam('subject_id', null);
	
		$sis_session = new Zend_Session_Namespace('sis');
	
		//if ($this->getRequest()->isPost()) {
	
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
	
			/*
			 * Search Group
			*/
			//get Subject Info
			$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			$subject = $subjectDb->getData($subject_id);
			$this->view->subject = $subject;
				
			$courseGroupDb = new Examination_Model_DbTable_ExamGroup();
			$groups = $courseGroupDb->getGroupList($subject_id,$semester_id);
				
			
			foreach($groups as $index=>$group){

				$total_student = 0;
				
				$examGroupStudentDb = new Examination_Model_DbTable_ExamGroupStudent();
				$total_student = $examGroupStudentDb->getTotalStudentAssigned($group["eg_id"]);
					
				$group["total_student"] = $total_student;
				$groups[$index]=$group;
			}
			/*
			 * End search group
			*/
	
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
	
			$json = Zend_Json::encode($groups);
	
			echo $json;
			
	
		//}
		exit();
	}
	
	
	
	
	 public function uploadOmrAction(){
    	
    		
    	$this->view->title=$this->view->translate("Answer Sheet - Upload OMR");
    
    	$id = $this->_getParam('id', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		
		$this->view->group_id = $id;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		
		$form = new Examination_Form_AnswerSheetGroupUpload(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject,'id'=>$id));
		$this->view->form = $form;
		
		
		if ($this->_request->isPost()) {
			
			//if ($form->isValid($_POST)) {
			
	       			$formData = $this->_request->getPost();	
	       			
	       			$this->view->group_id = $formData["idGroup"];
					$this->view->idSemester = $formData["idSemester"];
					$this->view->idSubject = $formData["idSubject"];
    	
	       		    // success - do something with the uploaded file
	                $uploadedData = $form->getValues();
	                $fullFilePath = $form->file->getFileName();
					
	                $filename=explode("/",$fullFilePath);
	                $filename=end($filename);

	               // Zend_Debug::dump($uploadedData, '$uploadedData');
	               // Zend_Debug::dump($fullFilePath, '$fullFilePath');
	                
		            $schemaFile = $fullFilePath;
					$lines = file($schemaFile);
				
					$file = fopen($fullFilePath, "r") or exit("Unable to open file!");

									
					//Output a line of the file until the end is reached
					$i=0;
					$arr_sets = array();
					
					while(!feof($file)){
						
						  $line_data = fgets($file);

						  $arr_sets[$i]["set_code"]=str_replace(" ","",substr($line_data, 0,6));	
						  $arr_sets[$i]["student_nim"]=substr($line_data, 6,12);
						  $arr_sets[$i]["student_name"]=substr($line_data, 21,30);
						 
							
						  /* Tujuan : Nak dapatkan total question
						   * Cara   : Cari dalam answer scheme based on Set Code
						   */
						   
						  //1st: Dapatkan program bagi student ni
						  $studentRegDB = new Examination_Model_DbTable_StudentRegistration();
						  $student_info = $studentRegDB->getRegistrationInfo($arr_sets[$i]["student_nim"]); 
													  
						  if(isset($student_info["registrationId"])){
						  	
						  	  //NIM match,student ini wujud dalam table student registration so dapatla program id dia
						  	  $arr_sets[$i]["student_nim_status"]='Nim Exist';
						  	  $arr_sets[$i]["student_idProgram"]=$student_info["IdProgram"];
						  	  
							  //2nd : check if answer scheme is exist/uploaded
			    	          $ansSchemeDb   = new Examination_Model_DbTable_StudentAnswerScheme();
				   			  $scheme_data = $ansSchemeDb->getAnswerSchemeData($formData["idSemester"],$student_info["IdProgram"],$formData["idSubject"],$arr_sets[$i]["set_code"]); 
			
				   			  if(is_array($scheme_data)){
				   					
				   			  		$total_question = $scheme_data["sas_total_quest"];
				   					$arr_sets[$i]["student_answer_raw"]=substr($line_data, 51,$total_question);
				   					$arr_sets[$i]["answer_schema_status"]='Exist';
				   					$arr_sets[$i]["IdMarksDistributionMaster"]= $scheme_data["sas_IdMarksDistributionMaster"];
				   					$arr_sets[$i]["IdMarksDistributionDetails"]=$scheme_data["sas_IdMarksDistributionDetails"];
				   					
				   					/* 3rd : Mapping Student Ke Program,Semester,Subject 
				   			  		 * Tujuan : Sebab nak make sure profile dia active, subject pada senester tersebut status register.
				   			         */
				   			  
				   			  		$studentDB = new Examination_Model_DbTable_StudentRegistration();
		    	        	 		$student = $studentDB->getStudentMapping($formData["idSemester"],$student_info["IdProgram"],$formData["idSubject"],$scheme_data["sas_IdMarksDistributionMaster"],$arr_sets[$i]["student_nim"]);
					  
						   			if(is_array($student)){
					    	        	
					    	        	$arr_sets[$i]["IdStudentRegistration"]=$student["IdStudentRegistration"];
					    	        	$arr_sets[$i]["IdStudentRegSubjects"]=$student["IdStudentRegSubjects"];
					    	        	$arr_sets[$i]["name"]=$student["appl_fname"].' '.$student["appl_mname"].' '.$student["appl_lname"];
					    	        	$arr_sets[$i]['IdStudentMarksEntry']=$student["IdStudentMarksEntry"];
			    						$arr_sets[$i]['TotalMarkObtained']=$student["TotalMarkObtained"];
			    						$arr_sets[$i]['MarksEntryStatus']=$student["MarksEntryStatus"];
			    						$arr_sets[$i]['student_semester_status']='Active & Register';
					    	        	
					    	        }else{
					    	        	
					    	        	$arr_sets[$i]["IdStudentRegistration"]='';
					    	        	$arr_sets[$i]["IdStudentRegSubjects"]='';
					    	        	$arr_sets[$i]["name"]='';
					    	        	$arr_sets[$i]['IdStudentMarksEntry']='';
			    						$arr_sets[$i]['TotalMarkObtained']='';
			    						$arr_sets[$i]['MarksEntryStatus']='';
			    						$arr_sets[$i]['student_semester_status']='Not Active or Not Register Subject';
					    	        }
				   					
				   				 
				   					
				   			  }else{
				   			  	    $total_question = $formData["total_quest"]; //use default total question from user input
				   					$arr_sets[$i]["student_answer_raw"]=substr($line_data, 51,$total_question);				   					
				   					$arr_sets[$i]["answer_schema_status"]='Answer Scheme Does Not Exist';
				   			  }
				   			  
				   			  
				   			  //4rd: Check Student ini dalam exam group ini atau tidak
				   			  $examGroupDb = new Examination_Model_DbTable_ExamGroupStudent();
				   			  $group = $examGroupDb->checkStudentGroup($student_info["IdStudentRegistration"],$formData["idSubject"],$formData["idSemester"]);
				   			
				   			  if(isset($group["egst_group_id"])){				   			  		
				   			  		$arr_sets[$i]["student_groupid"]=$group["egst_group_id"];
				   			  		$arr_sets[$i]["student_group"]=$group["eg_group_name"];				   			  	
				   			  }else{
				   			  		$arr_sets[$i]["student_group"]='';
				   			  }
				   			  
				   			  
						  }else{
						  	        $total_question = $formData["total_quest"]; //use default total question from user input
				   					$arr_sets[$i]["student_answer_raw"]=substr($line_data, 51,$total_question);		
						  	 		$arr_sets[$i]["student_nim_status"]='Nim Does Not Exist in Table Student Registration';
						  	 		$arr_sets[$i]["answer_schema_status"]='Fail to find Answer Scheme due to NIM does not Exist ';
						  }
					  
						
					$i++;
					}
					
					fclose($file);
					
                	//remove file
					unlink($fullFilePath);
					
					 
					//echo '<pre>';
					//print_r($arr_sets);
					//echo '</pre>';
					
					
				   $this->view->arr_sets = $arr_sets;
				   // print_r($arr_sets);
				
				   
					/*foreach($arr_sets as $set){				   	
				   	   
					   $tmpData["tmp_name"]=$set["student_name"];
					   $tmpData["tmp_IdMarksDistributionMaster"]=$IdMarksDistributionMaster;
					   $tmpData["tmp_IdMarksDistributionDetails"]=$IdMarksDistributionDetails;
					   $tmpData["tmp_set_code"]=$set["set_code"];
					   $tmpData["tmp_answers_raw"]=$set["student_answer_raw"];
					   $tmpData["tmp_file"]=$filename;
					   
					   $markRawDb = new Examination_Model_DbTable_StudentMarkRaw();
					   $markRawDb->addData($tmpData);					   
				   }*/
				  
	       		//}//form valid
			}//end post
	
	 }
	 
	 
	public function mapStudentListAction(){
    	
    	 
    	$idSemester = $this->_getParam('idSemester');    	
    	$idSubject = $this->_getParam('idSubject');    
    	$no = $this->_getParam('no');    
    	$set_code = $this->_getParam('set_code'); 
    	$group_id = $this->_getParam('group_id');    	
    		
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;
    	$this->view->no = $no;    	
    	$this->view->set_code = $set_code;
    	
    	// disable layouts for this action:
        $this->_helper->layout->disableLayout();
        
       // $form = new Examination_Form_MarkEntrySearchStudent();
       // $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getAllStudentRegisterSubject($idSemester,$idSubject,$formData);
	    	
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getAllStudentRegisterSubject($idSemester,$idSubject);
	    		
    	} //end if
    	
    	
    	//mapp kan student ni utk cari answer scheme exist x?
    	foreach($students as $key=>$student){
    		
    		 //Check Exam Group
			 $examGroupDb = new Examination_Model_DbTable_ExamGroupStudent();
			 $group = $examGroupDb->checkStudentGroup($student["IdStudentRegistration"],$idSubject,$idSemester);

    		 if(isset($group["egst_group_id"])){				   			  		
   			  		$students[$key]["student_groupid"]=$group["egst_group_id"];
   			  		$students[$key]["student_group"]=$group["eg_group_name"];				   			  	
   			  }else{
   			  		$students[$key]["student_group"]='';
   			  }
   			  
   			  
   			  //Check Course Group
   			  $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
   			  $courseGroup = $courseGroupDb->checkStudentCourseGroup($student["IdStudentRegistration"],$idSemester,$idSubject);
   			  $students[$key]["student_course_group"]=$courseGroup["GroupName"];
   			  
    		
    		  //Check Answer Scheme
    		  $ansSchemeDb   = new Examination_Model_DbTable_StudentAnswerScheme();
		      $scheme_data = $ansSchemeDb->getAnswerSchemeData($idSemester,$student["IdProgram"],$idSubject,$set_code); 
			
		       if(is_array($scheme_data)){				   					
	   			  		$students[$key]["scheme_status"]=1;
	   			  		$students[$key]["IdMarksDistributionMaster"]= $scheme_data["sas_IdMarksDistributionMaster"];
				   		$students[$key]["IdMarksDistributionDetails"]=$scheme_data["sas_IdMarksDistributionDetails"];
				   		
				   		//now cari IdMarksEntry				
					 	$markEntryDb = new Examination_Model_DbTable_StudentMarkEntry();
					 	$entry_list =  $markEntryDb->checkMarkEntry($student["IdStudentRegistration"],$scheme_data["sas_IdMarksDistributionMaster"],$student["IdStudentRegSubjects"],$idSemester);
					 	
						if(isset($entry_list["IdStudentMarksEntry"]) && $entry_list["IdStudentMarksEntry"]!=''){
							$students[$key]['IdStudentMarksEntry']=$entry_list["IdStudentMarksEntry"];
					    	$students[$key]['TotalMarkObtained']=$entry_list["TotalMarkObtained"];
					    	$students[$key]['MarksEntryStatus']=$entry_list["MarksEntryStatus"];
						}else{					 	
						 	$students[$key]['IdStudentMarksEntry']=null;
						    $students[$key]['TotalMarkObtained']=null;
						    $students[$key]['MarksEntryStatus']=null;					    	
					 	}
						
					  
		 
		       }else{
		       		    $students[$key]["scheme_status"]=null;
		       		    $students[$key]["IdMarksDistributionMaster"]= null;
				   		$students[$key]["IdMarksDistributionDetails"]= null;
		       }
    	}
    	
    	$this->view->student_list = $students;
    	
    }
	
    
   public function saveMappingAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	$stack_failed = array();
    	
    	$omrDB = new Examination_Model_DbTable_StudentOmrAnswer();
    	$omrDetailsDb = new Examination_Model_DbTable_StudentOmrAnswerDetail();
    	$markDB = new Examination_Model_DbTable_StudentMarkEntry();
		$markDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
		$markDistributionDetailsDB = new Examination_Model_DbTable_Marksdistributiondetails();	
						
    	 if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
		
		
			for($i=1; $i<=count($formData["IdStudentRegistration"]); $i++){
			 
			  	
						$IdStudentRegistration = $formData["IdStudentRegistration"][$i];
												    
			    
					    //mapping match and save info
						
						$soa["soa_IdStudentRegistration"]=$IdStudentRegistration;
						$soa["soa_exam_groupid"]=$formData["student_groupid"][$i];			
						$soa["soa_IdMarksDistributionMaster"]=$formData["idMaster"][$i];
						$soa["soa_IdMarksDistributionDetails"]=$formData["idDetails"][$i];			
						$soa["soa_set_code"]=$formData["set_code"][$i];
						$soa["soa_status"]=1;
						$soa["soa_createddt"]=date('Y-m-d H:i:s');
						$soa["soa_createdby"]=$auth->getIdentity()->iduser;
						
						
						//checking exist or not
										
						$omr= $omrDB->checkExist($IdStudentRegistration,$formData["set_code"][$i]);	
											
						if(is_array($omr)){
							//delete then add
							$omrDB->deleteData($omr["soa_id"]);							
							$omrDetailsDb->deleteDetailData($omr["soa_id"]);
						}
						$soa_id = $omrDB->addData($soa);
							
						
						 /* get answer for each question*/					
						   	
						$tempAns = array();
						$j = strlen($formData['student_answer_raw'][$i]);
						
						for ($k = 0; $k < $j; $k++) {
							$char = substr($formData['student_answer_raw'][$i], $k, 1);
							// do stuff with $char
							$tempAns[$k] = $char;
						}	
					
						/*end get answer*/
						
					
						$quest_no =1;
						$mark=0;
						foreach($tempAns as  $key=>$value){
							
							// check jawapan betul atau salah
							// get schema jawapan 
							$ansSchemeDb = new Examination_Model_DbTable_StudentAnswerScheme();
							$result = $ansSchemeDb->getAnswer($formData["idMaster"][$i],$formData["idDetails"][$i],$soa["soa_set_code"],$quest_no,$value);
							
							$soad["soad_soa_id"]=$soa_id;
							$soad["soad_ques_no"]=$quest_no;
							$soad["soad_student_ans"]=$value;
							$soad["soad_status_ans"]=$result;						
							
							$omrDetailDB = new Examination_Model_DbTable_StudentOmrAnswerDetail();
							$omrDetailDB->addData($soad);
							
							//kira markah
							if($result==1){
								$mark++;
							}
							
							//print_r($soad);
							
						$quest_no++;
						}
						
						
						
						/*  -------------------
						 *   Mark Entry Section 
						 *  ------------------- */
					  
						/*		
						
						//get info mark distribution master				
						$MarkDistributionMaster = $markDistributionDB->getInfoComponent($formData["idMaster"][$i]);
						
				       // echo '<pre>';
				        
						//1st: Save Main Mark
						
						$data["IdStudentRegistration"] = $IdStudentRegistration;
		    			$data["IdStudentRegSubjects"] = $formData["IdStudentRegSubjects"][$i];  
			    		$data["IdSemester"] = $formData["idSemester"];
				    	$data["Course"] = $formData["idSubject"];   
				    	$data["IdMarksDistributionMaster"] = $formData["idMaster"][$i];
				    	$data["TotalMarkObtained"] = $mark;
				    	$data["MarksTotal"] = $MarkDistributionMaster["Marks"]; 
				    	$data["Component"]   =  $MarkDistributionMaster["IdComponentType"]; 
				    	$data["ComponentItem"]   =  ''; 				
				    	$data["Instructor"] = '';    		
				    	$data["AttendanceStatus"] = '';
				    	$data["MarksEntryStatus"] = 407; //407=>ENTRY 409=>SUBMITTED 411=>APPROVED  		
				    	$data["UpdUser"] = $auth->getIdentity()->iduser;
				    	$data["UpdDate"] = date('Y-m-d H:i:s');
			    			
			    		//print_r($data);
			    		
		    		
						//check mark entry dah ada ke?
						$entry = $markDB->checkMarkEntry($IdStudentRegistration,$formData["idMaster"][$i],$formData["IdStudentRegSubjects"][$i],$formData["idSemester"]);		    					    		
			    		
						if(isset($entry["IdStudentMarksEntry"])){
							$IdStudentMarksEntry=$entry["IdStudentMarksEntry"];
			    			$markDB->updateData($data,$IdStudentMarksEntry);
			    			//echo '<br>update main<br>';
			    		}else{	    			
			    			$IdStudentMarksEntry = $markDB->addData($data);
			    			//echo '<br>save main<br>';
			    		}
				  
			    		
			    		
			    		
			    		
			    		//2nd : Save Details Component Mark
						//jika mark ini utk component item punya mark so kene masukkan mark dalam detail mark entry juga
						
						
			    		if(isset($formData["idDetails"][$i]) && $formData["idDetails"][$i]!=0){
			    			
				    			$MarkDistributionDetail = $markDistributionDetailsDB->getDataComponentItem($formData["idDetails"][$i]);
				    			
				    			
								$itemData["IdStudentMarksEntry"]=$IdStudentMarksEntry;
								$itemData["Component"]= $MarkDistributionMaster["IdComponentType"]; 
								$itemData["ComponentItem"]='';
								$itemData["ComponentDetail"]=$formData["idDetails"][$i]; //fk mark_distributiondetails
								$itemData["MarksObtained"]=$mark;
								$itemData["TotalMarks"]=$MarkDistributionDetail["Weightage"]; //ini sebenarnya x yah masukkan x per refer aje dari mark distribution
								$itemData["UpdUser"] = $auth->getIdentity()->iduser;
								$itemData["UpdDate"] = date('Y-m-d H:i:s');						
														
		    				
								//print_r($itemData);
								
								//check mark dah pernah entry ke belum?
				    			$markEntryDetails = $markDetailsDB->checkMarkEntry($IdStudentMarksEntry,$formData["idDetails"][$i]);
				    		
								if(isset($markEntryDetails["IdStudentMarksEntryDetail"]) && $markEntryDetails["IdStudentMarksEntryDetail"]!=''){
				    				 $markDetailsDB->updateData($itemData,$markEntryDetails["IdStudentMarksEntryDetail"]);
				    				// echo '<br>update details<br>';
				    			}else{	    			
				    				 $markDetailsDB->addData($itemData);
				    				// echo '<br>save details<br>';
				    			}
					  
				    		
		    				   
								
								
								
								// Function : Untuk update TotalMarkObtained dalam markdistribution master
					    		// Step: Kene check component item dia ada berapa? Kemudian jumlahkan	
								
					    		$itemsMark =  $markDetailsDB->getListComponentMark($IdStudentMarksEntry);					    				    		
					    		foreach($itemsMark as $item){
					    			$TotalMarkObtained=$TotalMarkObtained+$item["MarksObtained"];
					    		}		
		
				    			$markDB->updateData(array('TotalMarkObtained'=>$TotalMarkObtained),$IdStudentMarksEntry);
						
			    		}//end if details component
			    		
			    		
			    		*/
		    		
						//echo '</pre>';
	    		/*  -------------------
				 *  End Mark Entry Section 
				 *  ------------------- */
				
								
			}//end for 
			
			$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'answer-sheet-group', 'action'=>'index','msg'=>1),'default',true));
			//$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'answer-sheet-group', 'action'=>'group-detail','id'=>$formData["idGroup"],'idSubject'=>$formData["idSubject"],'idSemester'=>$formData["idSemester"],'msg'=>1),'default',true));
    	 }
    			
    }
    
    public function studentAnswerAction(){
    	
		// disable layouts for this action:
        $this->_helper->layout->disableLayout();
        
    	$group_id = $this->_getParam('gid');
    	$IdStudentRegistration= $this->_getParam('IdStudentRegistration');
    	    	
    	
    	$studentAnswerDB = new Examination_Model_DbTable_StudentOmrAnswerDetail();
    	$students = $studentAnswerDB->getAnswer($group_id,$IdStudentRegistration);   
    	
    	$this->view->student_answer = $students;
       
    }
    
    
	public function courseGroupDetailAction(){
		
		$id = $this->_getParam('id', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$msg = $this->_getParam('msg', null);
		
		$this->view->group_id = $id;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Information has been saved successfully");
		}
		
		//title
		$this->view->title= $this->view->translate("Course Group");
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
				
		$examGroupStudentDB = new Examination_Model_DbTable_ExamGroupStudent();
		$this->view->assessment = $examGroupStudentDB->getAssessment($idSubject,$idSemester,$id);
	
			
		//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($id);			
		$this->view->group = $group;
	
		$form = new Examination_Form_AnswerSheetGroupUpload(array('idSemesterx'=>$idSemester,'idSubjectx'=>$idSubject,'id'=>$id));
		$this->view->form = $form;
		
		
		
		 if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->egroupId = $formData["egroupId"]; //exam group id
			$this->view->assessmentId = $formData["assessmentId"];
			
			
			$examGroupStudentDB = new Examination_Model_DbTable_ExamGroupStudent();
			$this->view->exam_group = $examGroupStudentDB->getExamGroup($idSubject,$idSemester,$id,$formData["assessmentId"]);
		
			//student list
			$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
			$studentList = $courseGroupStudentDb->getStudentbyGroup($id);
	
				foreach($studentList as $key=>$value){
					
						//get omr info
						$omrDB = new Examination_Model_DbTable_StudentOmrAnswer();
						$omrData = $omrDB->getData($value["IdStudentRegistration"],$formData["egroupId"]);
						$studentList[$key]['set_code']=$omrData["soa_set_code"];
						$studentList[$key]['soa_id']=$omrData["soa_id"];
						$studentList[$key]['soa_status']=$omrData["soa_status"];
				
				}
			
			$this->view->student_list = $studentList;
			
			
		 }
		 
		 
		
		
		
	
		
		
		
	}
	
	public function markProcessingAction(){
		
		
		$auth = Zend_Auth::getInstance();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//print_r($formData);
			
			$omrDB = new Examination_Model_DbTable_StudentOmrAnswer();
    		$omrDetailDb = new Examination_Model_DbTable_StudentOmrAnswerDetail();
    		$markDB = new Examination_Model_DbTable_StudentMarkEntry();
			$markDetailsDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
			$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			$markDistributionDetailsDB = new Examination_Model_DbTable_Marksdistributiondetails();	
			$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
		
			//echo '<pre>';	
				  
			for($i=0; $i<count($formData["soa_id"]); $i++){					
						
				
				$soa_id = $formData["soa_id"][$i];
				
				
				$info = $omrDB->getDatabyid($soa_id);
				
				//print_r($info);
				
				$answers = $omrDetailDb->getOmrAnswer($soa_id);
				
				$mark=0;
				foreach($answers as $answer){					
					//jawapan betul tambah markah				
					if($answer["soad_status_ans"]==1){
							$mark++;
					}					
				}
			
						/*  -------------------
						 *   Mark Entry Section 
						 *  ------------------- */
				
						//get info mark distribution master	
						$MarkDistributionMaster = $markDistributionDB->getInfoComponent($info["soa_IdMarksDistributionMaster"]);
						
				        //get info subject registration				        
				        $subjectRegister = $studentRegDB->getRegisterSubject($formData["group_id"],$info["soa_IdStudentRegistration"]);
				        
						//1st: Save Main Mark
						
						$data["IdStudentRegistration"] = $info["soa_IdStudentRegistration"];
		    			$data["IdStudentRegSubjects"] = $subjectRegister["IdStudentRegSubjects"];  
			    		$data["IdSemester"] = $formData["idSemester"];
				    	$data["Course"] = $formData["idSubject"];   
				    	$data["IdMarksDistributionMaster"] = $info["soa_IdMarksDistributionMaster"];
				    	$data["TotalMarkObtained"] = $mark;
				    	$data["MarksTotal"] = $MarkDistributionMaster["Marks"]; 
				    	$data["Component"]   =  $MarkDistributionMaster["IdComponentType"]; 
				    	$data["ComponentItem"]   =  ''; 				
				    	$data["Instructor"] = '';    		
				    	$data["AttendanceStatus"] = '';
				    	$data["MarksEntryStatus"] = 407; //407=>ENTRY 409=>SUBMITTED 411=>APPROVED  		
				    	$data["UpdUser"] = $auth->getIdentity()->iduser;
				    	$data["UpdDate"] = date('Y-m-d H:i:s');
			    			
			    		//print_r($data);
			    		
		    		
						//check mark entry dah ada ke?
						$entry = $markDB->checkMarkEntry($info["soa_IdStudentRegistration"],$info["soa_IdMarksDistributionMaster"],$subjectRegister["IdStudentRegSubjects"],$formData["idSemester"]);		    					    		
			    		
						if(isset($entry["IdStudentMarksEntry"])){
							$IdStudentMarksEntry=$entry["IdStudentMarksEntry"];
			    			$markDB->updateData($data,$IdStudentMarksEntry);
			    			//echo '<br>update main<br>';
			    		}else{	    			
			    			$IdStudentMarksEntry = $markDB->addData($data);
			    			//echo '<br>save main<br>';
			    		}
				  
			    		
			    		
			    	
			    		
			    		//2nd : Save Details Component Mark
						//jika mark ini utk component item punya mark so kene masukkan mark dalam detail mark entry juga
						
						
			    		if(isset($info["soa_IdMarksDistributionDetails"]) && $info["soa_IdMarksDistributionDetails"]!=0){
			    			
				    			$MarkDistributionDetail = $markDistributionDetailsDB->getDataComponentItem($info["soa_IdMarksDistributionDetails"]);
				    			
				    			
								$itemData["IdStudentMarksEntry"]=$IdStudentMarksEntry;
								$itemData["Component"]= $MarkDistributionMaster["IdComponentType"]; 
								$itemData["ComponentItem"]='';
								$itemData["ComponentDetail"]=$info["soa_IdMarksDistributionDetails"]; //fk mark_distributiondetails
								$itemData["MarksObtained"]=$mark;
								$itemData["TotalMarks"]=$MarkDistributionDetail["Weightage"]; //ini sebenarnya x yah masukkan x per refer aje dari mark distribution
								$itemData["UpdUser"] = $auth->getIdentity()->iduser;
								$itemData["UpdDate"] = date('Y-m-d H:i:s');						
														
		    				
								//print_r($itemData);
								
								//check mark dah pernah entry ke belum?
				    			$markEntryDetails = $markDetailsDB->checkMarkEntry($IdStudentMarksEntry,$info["soa_IdMarksDistributionDetails"]);
				    		
								if(isset($markEntryDetails["IdStudentMarksEntryDetail"]) && $markEntryDetails["IdStudentMarksEntryDetail"]!=''){
				    				 $markDetailsDB->updateData($itemData,$markEntryDetails["IdStudentMarksEntryDetail"]);
				    				// echo '<br>update details<br>';
				    			}else{	    			
				    				 $markDetailsDB->addData($itemData);
				    				// echo '<br>save details<br>';
				    			}
					  
				    		
		    				   
								
								
								
								// Function : Untuk update TotalMarkObtained dalam markdistribution master
					    		// Step: Kene check component item dia ada berapa? Kemudian jumlahkan	
								$TotalMarkObtained=0;
					    		$itemsMark =  $markDetailsDB->getListComponentMark($IdStudentMarksEntry);					    				    		
					    		foreach($itemsMark as $item){
					    			$TotalMarkObtained=$TotalMarkObtained+$item["MarksObtained"];
					    		}		
		
				    			$markDB->updateData(array('TotalMarkObtained'=>$TotalMarkObtained),$IdStudentMarksEntry);
						
			    		}//end if details component
			    	
			    		
			    		
		    		
						
			    		/*  -------------------
						 *  End Mark Entry Section 
						 *  ------------------- */
				
				}//END FOR
				
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'answer-sheet-group', 'action'=>'course-group-detail','idSemester'=>$formData["idSemester"],'idSubject'=>$formData["idSubject"],'id'=>$formData["group_id"],'msg'=>1),'default',true));
		}
		
	}
	
	
	public function getExamGroupAction(){
		
		$assessmentId = $this->_getParam('assessmentId',null);
		$subject_id = $this->_getParam('idSubject',null);
		$semester_id = $this->_getParam('idSemester',null);
	    $idgroup = $this->_getParam('idgroup',null);
    	
	   
     	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
    
	    $examGroupStudentDB = new Examination_Model_DbTable_ExamGroupStudent();
		$group = $examGroupStudentDB->getExamGroup($subject_id,$semester_id,$idgroup,$assessmentId);
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($group);
		
		echo $json;
		exit();
	}
    
    
	
}
 ?>
