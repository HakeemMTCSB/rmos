<?php 
class Examination_AssigngradesController extends Base_Base {
	/**
	 * The default action - show the home page
	 */

	public function init()
	{
		$this->fnsetObj();

	}
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjplacementtestmodel = new Application_Model_DbTable_Placementtest();
		//$this->lobjplacementtestmarksbulkmodel = new Examination_Model_DbTable_Assigngrades();
		$this->lobjassigngradesmodel = new Examination_Model_DbTable_Assigngrades();
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
	}

	public function indexAction()
	{
		$this->view->lobjform = $this->lobjform;
		$this->view->IdPrgm = 0;
		// TODO Auto-generated PlacementtestmarksController::indexAction() default action
		$larrresult = array();//$this->lobjstudentapplication->fetchAll('Active = 1', "FName ASC");

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->placementmarkspaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		$larProgramNameCombo = $this->lobjassigngradesmodel->fngetSemesterNameCombo();
		$this->view->lobjform->field5->addMultiOptions($larProgramNameCombo);
		$this->view->lobjform->field5->setAttrib('required',"true");

		$larGradeNameCombo = $this->lobjassigngradesmodel->fngetGradeNameCombo();
		$this->view->gradeslist	= $larGradeNameCombo;

		/*	$this->view->lobjform->field8->addMultiOptions($larGradeNameCombo);
		 $this->view->lobjform->field8->setAttrib('required',"true");
		*/

		if(isset($this->gobjsessionsis->placementmarkspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->placementmarkspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {

				$larrresult = $this->lobjassigngradesmodel->fnSearchStudentlist( $this->lobjform->getValues ());

				if(count($larrresult) > 0 ) {

					$ArrIdStudentRegistrations['IdStudentRegistration'][0] = 0;
					$ArraIdStudentRegistration[0]['IdStudentRegistration'] =  0;
					$ArraIdStudentRegistration[0]['IdSemester'] =  0;
					$ArraIdStudentRegistration[0]['FName'] =  0;
					$ArraIdStudentRegistration[0]['SemsterCount'] =  0;
					$ArraIdStudentRegistration[0]['ProgramName'] =  0;
					$ArraIdStudentRegistration[0]['GradeDesc'] =  0;
					$ArraIdStudentRegistration[0]['SemesterCode'] =  0;
					$ArraIdStudentRegistration[0]['IdGrade'] =  0;

					$ArraIdSubjectLists['IdSubject'][0] =  0;
					$ArraIdSubjectList[0]['IdSubject'] =  0;
					$ArraIdSubjectList[0]['SubjectName'] =  0;

					$cnts  = 0;
					$cnts1 = 0;
					for($i=0;$i<count($larrresult);$i++){
							
						if(!in_array($larrresult[$i]['IdStudentRegistration'],$ArrIdStudentRegistrations['IdStudentRegistration'])){
							$ArrIdStudentRegistrations['IdStudentRegistration'][$cnts] = $larrresult[$i]['IdStudentRegistration'];
							$ArraIdStudentRegistration[$cnts]['IdStudentRegistration'] = $larrresult[$i]['IdStudentRegistration'];
							$ArraIdStudentRegistration[$cnts]['IdSemester'] = $larrresult[$i]['IdSemester'];
							$ArraIdStudentRegistration[$cnts]['FName'] = $larrresult[$i]['FName'];
							$ArraIdStudentRegistration[$cnts]['SemsterCount'] = $larrresult[$i]['SemsterCount'];
							$ArraIdStudentRegistration[$cnts]['ProgramName'] = $larrresult[$i]['ProgramName'];
							$ArraIdStudentRegistration[$cnts]['GradeDesc'] = $larrresult[$i]['GradeDesc'];
							$ArraIdStudentRegistration[$cnts]['SemesterCode'] = $larrresult[$i]['SemesterCode'];
							$ArraIdStudentRegistration[$cnts]['IdGrade'] = $larrresult[$i]['IdGrade'];
							$cnts++;
						}
						if(!in_array($larrresult[$i]['IdSubject'],$ArraIdSubjectLists['IdSubject'])){
							$ArraIdSubjectLists['IdSubject'][$cnts1] = $larrresult[$i]['IdSubject'];
							$ArraIdSubjectList[$cnts1]['IdSubject'] = $larrresult[$i]['IdSubject'];
							$ArraIdSubjectList[$cnts1]['SubjectName'] = $larrresult[$i]['SubjectName'];
							$cnts1++;
						}
					}
					$this->view->ArraSubjectList   = $ArraIdSubjectList;
					$larrresult	 = $ArraIdStudentRegistration;
					$lintpagecount= 1000;
					$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
					$this->gobjsessionsis->placementmarkspaginatorresult = $larrresult;
				}

			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();

			$larrformData['UpdUser']    =  1;
			$larrformData['UpdDate']    =  date('Y-m-d');

			$count = count($larrformData['studentid']);
			for($i = 0 ;$i<$count ; $i++) {
				$countsem = count($larrformData['semno'][$larrformData['studentid'][$i]]);
			 for($j=0;$j<$countsem;$j++){
			 	if($larrformData['subjid'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$j]=="")$larrformData['subjid'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$j]=0;
			 	if($larrformData['idgrade'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$larrformData['subjid'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$j]][$j]=="")$larrformData['idgrade'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$larrformData['subjid'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$j]][$j]=0;
			 	$larrFormdatainsert = array('IdStudent'=>$larrformData['studentid'][$i],
			 			'SemesterNumber'=>$larrformData['semno'][$larrformData['studentid'][$i]][$j],
			 			'IdSubject'=>$larrformData['subjid'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$j],
			 			'IdGrade'=>$larrformData['idgrade'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$larrformData['subjid'][$larrformData['studentid'][$i]][$larrformData['semno'][$larrformData['studentid'][$i]][$j]][$j]][$j],
			 			'UpdUser'=>$larrformData['UpdUser'],
			 			'UpdDate'=>$larrformData['UpdDate'],
			 	);
			 	$lastId=$this->lobjassigngradesmodel->fninsertGrades($larrFormdatainsert);
			 	$larrFormdatainsert['IdStudentGrade']=$lastId;
			 	$this->lobjassigngradesmodel->fninsertGradesSubjects($larrFormdatainsert);
			 }
			}
			$this->_redirect( $this->baseUrl . '/examination/assigngrades/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/assigngrades/index');
		}

	}

}

?>