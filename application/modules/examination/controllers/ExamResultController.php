<?php
 
class Examination_ExamResultController extends Base_Base {

	public function indexAction(){
		
		$this->view->title = "Examination Result";
		 
		$form = new Examination_Form_ExamResultSearchForm();		
		
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();	

		
		//program
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();	
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		
		if ($this->getRequest()->isPost()) {

			if($form->isValid($this->getRequest()->getPost())){
				
				$formData = $this->getRequest()->getPost();	
				$form->populate($formData);
				
				//get Student
				$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
				$student_list = $studentRegDB->getStudentRegistration($formData);
				$this->view->student_list = $student_list;
			}
			
		}
		$this->view->form = $form;
		
	}
	
	
     
     
	public function transcriptAction(){
		
		$this->view->title = "Transcript";
		 
		$form = new Examination_Form_ExamResultSearchForm();		
		
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();	

		
		//program
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();	
		
		//semester
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemestermasterList();
		$this->view->semester_list = $semesterList;
		
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();	

			$form->populate($formData);
			//get Student
			$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
			$student_list = $studentRegDB->getStudentRegistration($formData);
			$this->view->student_list = $student_list;
			
		}
		$this->view->form = $form;
		
	}
	
	public function viewTranscriptAction(){
		
            $this->view->title = "Transcript";

            $IdStudentRegistration = $this->_getParam('id',null);   	
            $this->view->id = 	$IdStudentRegistration;
		      
            $studentRegDB = new Examination_Model_DbTable_StudentRegistration();
            $semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
            $regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();   
            $studentGradeDB = new Examination_Model_DbTable_StudentGrade();	
            $cms = new Cms_ExamCalculation();	 
            $publishResultDB = new Examination_Model_DbTable_PublishMark(); 
			$semesterDB = new Registration_Model_DbTable_Semester();

            //To get Student Academic Info  
            $student = $studentRegDB->getStudentInfo($IdStudentRegistration);        
            $this->view->student = $student;
            
            if($student['IdProgram']==1 || $student['IdProgram']==3 || $student['IdProgram']==20){
        		$cms->generateGrade($IdStudentRegistration);
            } else if ($student['IdProgram']==2){
				$cms->generateGradePs($IdStudentRegistration);
			}
                        
            $current_semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));
    		
            $convoDB = new Graduation_Model_DbTable_Pregraduation();
	    	$this->view->convocation = $convoDB->getConvoInfo($IdStudentRegistration);
	        
        
            $scheme = '';
            if($student['IdScheme']==1){
                $scheme = '( '.$student['mop'].' '.$student['mos'].' '.$student['pt'].' )';    
            }    
            if($student['IdScheme']==11){ //GS
                if($student['pt']!='')
                $scheme = '( '.$student['pt'].' )';    
            }
            $this->view->program_scheme = $scheme;
          
            if($student['IdProgram']==5){ //CIFP NI SPECIAL  
            	//get part III subject
            	$this->view->part_three = $regSubjectDB->getSubjectPart3($IdStudentRegistration);                	
            }
             
            //get registered semester   		
            $registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);

            if ($registered_semester){
                $registered_semester=$this->display($registered_semester,$IdStudentRegistration,$student,$current_semester);               
            }

            //echo '<pre>';
            //print_r($registered_semester);
            $this->view->registered_semester = $registered_semester;
          
	}
	
	
	public function printTranscriptAction(){
		
		
		$IdStudentRegistration = $this->_getParam('id',null);   
		$type = $this->_getParam('type',1);   		
		      
        $studentRegDB = new Examination_Model_DbTable_StudentRegistration();
		$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
   		$regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();   
   		$studentGradeDB = new Examination_Model_DbTable_StudentGrade();		
		$publishResultDB = new Examination_Model_DbTable_PublishMark(); 
		$semesterDB = new Registration_Model_DbTable_Semester();
		$convoDB = new Graduation_Model_DbTable_Pregraduation();
		 
		//To get Student Academic Info  
        $student = $studentRegDB->getStudentInfo($IdStudentRegistration);
        
        $current_semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));
    	
	    $convocation = $convoDB->getConvoInfo($IdStudentRegistration);
	        
        Global $student_info;
        $student_info = $student;
        
        Global $part_three_info;
        
        Global $program_scheme;
        
        Global $convo;
        $convo = $convocation;
        	
        $scheme = '';
	 	if($student['IdScheme']==1){ //PS
       	 	$scheme = '( '.$student['mop'].' '.$student['mos'].' '.$student['pt'].' )';    
        }    
        if($student['IdScheme']==11){ //GS
        	$scheme = '( '.$student['pt'].' )';    
        }
        $program_scheme = $scheme;
           
	 	if($student['IdProgram']==5){ //CIFP NI SPECIAL  
            	//get part III subject
            	$part_three = $regSubjectDB->getSubjectPart3($IdStudentRegistration);             
       			$part_three_info = $part_three;
            	              	
        }
       
             
   		//get registered semester   		
		//$registered_semester = $semesterStatusDB->getCountableRegisteredSemester($IdStudentRegistration);
		$registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);
		       
 		if ($registered_semester){
               $registered_semester = $this->display($registered_semester,$IdStudentRegistration,$student,$current_semester,$type);
        }
		
		Global $semester_list;
		$semester_list = $registered_semester;
				
		$city = ($student['appl_city']!=99) ? $student['CityName']:$student['appl_city_others'];
		$state = ($student['appl_state']!=99) ? $student['StateName']:$student['appl_state_others'];
		
		$address  = (!empty($student['appl_address1'])) ? $student['appl_address1']:'';
		$address .= (!empty($student['appl_address2'])) ? '<br>'.$student['appl_address2']:'';
		$address .= (!empty($student['appl_address3'])) ? '<br>'.$student['appl_address3']:'';
		$address .= (!empty($student['appl_postcode'])) ? '<br>'.$student['appl_postcode'].' '.$city:'';
		$address .= (!empty($student['CountryName']))   ? '<br>'.$state.' '.$student['CountryName']:'<br>'.$state;
		 
		
		$fieldValues = array(
		    	 			 '$[STUDENT_NAME]'=>$student["appl_fname"].' '.$student["appl_lname"], 
							 '$[STUDENT_ID]'=> strtoupper( $student["registrationId"] ) ,
							 '$[ADDRESS]'=> $address,
							 '$[INTAKE]'=> ($type==2) ? strtoupper($student["sem_year"] ):strtoupper($student["IntakeDesc"]),
							 '$[PROGRAMME]'=> strtoupper( $student["ProgramName"].' '.$scheme ),
							 '$[LOGO]'=> "images/logo_text_high.jpg"
	    	  				);
		    	  				
		
		require_once 'dompdf_config.inc.php';
		error_reporting(0);	
		
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//template path	 
		if($type==2){
			//official
			$html_template_path = DOCUMENT_PATH."/template/official-transcript.html";
			
			//output filename
			$output_filename = $student["registrationId"]."-official-transcript.pdf";
			
		}else{
			//unofficial
			$html_template_path = DOCUMENT_PATH."/template/transcript.html";
			
			//output filename 
			$output_filename = $student["registrationId"]."-transcript.pdf";
				
		}

		
		$html = file_get_contents($html_template_path);			
    		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}


		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'potrait');
		$dompdf->render();
		
		//$dompdf = $dompdf->output();
		$dompdf->stream($output_filename);						
							
		//to rename output file		
		$output_file_path = DOCUMENT_PATH.$student['sp_repository'].'/'.$output_filename;				
	 		
		file_put_contents($output_file_path, $dompdf);
		
		$this->view->file_path = $output_file_path;
		exit;
    	
	}
	
	
	public function printOfficialTranscriptAction(){
		
		
		$IdStudentRegistration = $this->_getParam('id',null);   
		      
        $studentRegDB = new Examination_Model_DbTable_StudentRegistration();
		$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
   		$regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();   
   		$studentGradeDB = new Examination_Model_DbTable_StudentGrade();		
		$publishResultDB = new Examination_Model_DbTable_PublishMark(); 
		$semesterDB = new Registration_Model_DbTable_Semester();
		$convoDB = new Graduation_Model_DbTable_Pregraduation();
		 
		//To get Student Academic Info  
        $student = $studentRegDB->getStudentInfo($IdStudentRegistration);
        
        $current_semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));
    	
	    $convocation = $convoDB->getConvoInfo($IdStudentRegistration);
	        
        Global $student_info;
        $student_info = $student;
        
        Global $part_three_info;
        
        Global $program_scheme;
        
        Global $convo;
        $convo = $convocation;
        	
        $scheme = '';
	 	if($student['IdScheme']==1){ //PS
       	 	$scheme = '( '.$student['mop'].' '.$student['mos'].' '.$student['pt'].' )';    
        }    
        if($student['IdScheme']==11){ //GS
        	$scheme = '( '.$student['pt'].' )';    
        }
        $program_scheme = $scheme;
           
	 	if($student['IdProgram']==5){ //CIFP NI SPECIAL  
            	//get part III subject
            	$part_three = $regSubjectDB->getSubjectPart3($IdStudentRegistration);             
       			$part_three_info = $part_three;            	              	
        }
       
             
   		//get registered semester   		
		//$registered_semester = $semesterStatusDB->getCountableRegisteredSemester($IdStudentRegistration);
		$registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);
	           
 		foreach($registered_semester as $index=>$semester){
				
				//get subject registered in each semester
				if($semester["IsCountable"]==1){
					$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($IdStudentRegistration,$semester['IdSemesterMain'],$student['IdProgram']);
				}else{
					$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($IdStudentRegistration,$semester['IdSemesterMain']);
				}
								
             	
				$registered_semester[$index]['total_subjects']=count($subject_list);
				$registered_semester[$index]['subjects']=$subject_list;
				
				//get grade info
				$student_grade = $studentGradeDB->getStudentGrade($IdStudentRegistration,$semester['IdSemesterMain']);
				$registered_semester[$index]['grade'] = $student_grade;
				
				//cek publish status               
				$isPublish = $publishResultDB->isPublish($student['IdProgram'],$semester['IdSemesterMain'],0);
				
				if($current_semester['IdSemesterMaster']==$semester['IdSemesterMain']){
					//if eq current semester
					unset($registered_semester[$index]);
				}else{
					//default true sebab azila suh admin boleh tgk walau x publish
					$registered_semester[$index]['isPublish']=true;
					
					if($student['IdProgram']==5){ //CIFP NI SPECIAL                	
	                	$part_list = $regSubjectDB->getPartListPerSemester($IdStudentRegistration,$semester['IdSemesterMain']);  
	                	
	                	if(count($part_list)>0){
	                		//display
	                		//$registered_semester[$index]['parts']= $part_list;
	                	}else{
	                		unset($registered_semester[$index]);
	                	}		                	
		            }
	            
				}

				
				
             	if(count($subject_list)==1 ){
             		
             		//to cater for CE & Official Transcript
             		$ce_found = array_search(126, array_column($subject_list, 'IdSubject'));
             		if(false !== $ce_found){
             			unset($registered_semester[$index]);
             		}
             		
             		//to cater course type=3 Thesis/Dissertation
             		$ct3_key_found = array_search(3, array_column($subject_list, 'CourseType'));
             		if(false !== $ct3_key_found){
             			
             			if($subject_list[$ct3_key_found]['exam_status']=='IP'){
             				unset($registered_semester[$index]);
             			}             			
             		}             		
             	}else{
             			//to cater course type=3 Thesis/Dissertation
	             		$ct3_key_found = array_search(3, array_column($subject_list, 'CourseType'));
	             		if(false !== $ct3_key_found){	             			
	             			if($subject_list[$ct3_key_found]['exam_status']=='IP'){	             				
	             				unset($subject_list[$ct3_key_found]);
	             				$registered_semester[$index]['subjects']=$subject_list;
	             			}             			
	             		}  	
             		}
	        }	            
	        array_values($registered_semester);
	            
	        
	        
	 		if ($registered_semester){	 			
	 		 	$i = 0;
	            foreach ($registered_semester as $registered_semesterLoop){
	                                
	                    if (count($registered_semesterLoop['subjects']) > 0){
	                        
	                    	$k = 0;
	                        foreach ($registered_semesterLoop['subjects'] as $subjectLoop){
	
	                        $subArr = array();
	                        $subArr2 = array();
	                            $j = 0;
	                                                        
	                            foreach ($registered_semester as $registered_semesterLoop2){
	                            
	                                if ($j > $i){
	                                
	                                    foreach ($registered_semesterLoop2['subjects'] as $subjectLoop2){
	                                    
	                                    
	                                    if (in_array($subjectLoop2['exam_status'], array("U","I"))) {
	                                    //if( $subjectLoop2['exam_status']!='U' &&  $subjectLoop2['exam_status']!='I' ){
	                                        array_push($subArr2, $subjectLoop2['IdSubject']);
	                                    }else{
	                                    array_push($subArr,$subjectLoop2['IdSubject']);
	                                    }
	                                    }                                    
	                                }
	                                $j++;
	                            }
	                            
	                           
	                            if(in_array($subjectLoop['IdSubject'], $subArr)){
	                                $pemalar = '-';
	                                $pemalar2 = '-';
	                                $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
	                                $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
	                            }
	
	                            $checkRepeatRetake = $regSubjectDB->getRetakeSubject($subjectLoop['IdStudentRegSubjects'], $IdStudentRegistration);
	
	                            if ($checkRepeatRetake){
	                                $pemalar = '-';
	                                $pemalar2 = $subjectLoop['CreditHours'];
	                                $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
	                                $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
	                            }
	                            $k++;
	                        }
	                    }
	                    $i++;
	            }
            }//end if
 			
 			      
		Global $semester_list;
		$semester_list = $registered_semester;
				
		$city = ($student['appl_city']!=99) ? $student['CityName']:$student['appl_city_others'];
		$state = ($student['appl_state']!=99) ? $student['StateName']:$student['appl_state_others'];
		
		$address  = (!empty($student['appl_address1'])) ? str_replace(',','',$student['appl_address1']):'';
		$address .= (!empty($student['appl_address2'])) ? ', '.str_replace(',','',$student['appl_address2']):'';
		$address .= (!empty($student['appl_address3'])) ? ', '.str_replace(',','',$student['appl_address3']):'';
		$address .= (!empty($student['appl_postcode'])) ? ', '.$student['appl_postcode']:'';
		$address .= (!empty($city)) ? ' '.$city:'';
		$address .= (!empty($state))   					? ', '.$state:'';
		$address .= (!empty($student['CountryName']))   ? ', '.$student['CountryName']:'';
		
		$fieldValues = array(
		    	 			 '$[STUDENT_NAME]'=>$student["appl_fname"].' '.$student["appl_lname"], 
							 '$[STUDENT_ID]'=> strtoupper( $student["registrationId"] ) ,
							 '$[ADDRESS]'=> $address,
							 '$[INTAKE]'=> strtoupper($student["sem_year"] ),
							 '$[PROGRAMME]'=> strtoupper( $student["ProgramName"] ),
							 '$[LOGO]'=> "images/logo_text_high.jpg"
	    	  				);
		    	  				
		
		require_once 'dompdf_config.inc.php';
		error_reporting(0);	
		
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//official
		$html_template_path = DOCUMENT_PATH."/template/official-transcript.html";
		
		//output filename
		$output_filename = $student["registrationId"]."-official-transcript.pdf";

		
		$html = file_get_contents($html_template_path);			
    		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}
		
		
		
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'potrait');
		$dompdf->render();
		
		//$dompdf = $dompdf->output();
		$dompdf->stream($output_filename);						
							
		//to rename output file		
		$output_file_path = DOCUMENT_PATH.$student['sp_repository'].'/'.$output_filename;				
	 		
		file_put_contents($output_file_path, $dompdf);
		
		$this->view->file_path = $output_file_path;
		exit;
    	
	}
	
	public function searchAction(){
		
		$this->view->title = "Official Transcript";
		 
		$form = new Examination_Form_ExamResultSearchBulkForm();		
				
		//program
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();

		$templateModel = new Communication_Model_DbTable_Template();
		$templateList = $templateModel->getTemplates('examination', 587);
		$this->view->templateList = $templateList;
		
		if ($this->getRequest()->isPost()) {

			if($form->isValid($this->getRequest()->getPost())){
				
				$formData = $this->getRequest()->getPost();	
				$this->view->c_id = $formData['IdConvo'];
				$form->populate($formData);
				
				//get Student
				$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
				$student_list = $studentRegDB->getStudentRegistration($formData);
				$this->view->student_list = $student_list;
			}
			
		}
		$this->view->form = $form;
		
	}
	
	public function printBulkAction(){

		set_time_limit(0);
		ini_set('memory_limit', '-1');
	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
					//var_dump($formData); exit;
			$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
			$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
	   		$regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();   
	   		$studentGradeDB = new Examination_Model_DbTable_StudentGrade();		
			$publishResultDB = new Examination_Model_DbTable_PublishMark(); 
			$semesterDB = new Registration_Model_DbTable_Semester();
			 $cms = new Cms_ExamCalculation();	 
			 		
	        $student_list = array();
	        
	        $convoDB = new Graduation_Model_DbTable_Convocation();
	        $convocation = $convoDB->getData($formData['c_id']);
	        
			for($m=0; $m<count($formData['IdStudentRegistration']); $m++){
				
				$IdStudentRegistration = $formData['IdStudentRegistration'][$m];

								 
				//To get Student Academic Info  
		        $student = $studentRegDB->getStudentInfo($IdStudentRegistration);

		        if($student['IdProgram']==1 || $student['IdProgram']==3 || $student['IdProgram']==20){
					$cms->generateGrade($IdStudentRegistration); 
				} else if ($student['IdProgram']==2){
					$cms->generateGradePs($IdStudentRegistration);
				}
					
				
		        $current_semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));

				$scheme = '';
			 	if($student['IdScheme']==1){ //PS
		       	 	$scheme = '( '.$student['mop'].' '.$student['mos'].' '.$student['pt'].' )';    
		        }    
		        if($student['IdScheme']==11){ //GS
		        	$scheme = '( '.$student['pt'].' )';    
		        }
		        $program_scheme = $scheme;
		        
		        
		        if($student['IdProgram']==5){ //CIFP NI SPECIAL  
	            	//get part III subject
	            	$part_three = $regSubjectDB->getSubjectPart3($IdStudentRegistration);             
	       			$student_list[$m]['part_three']  = $part_three;          	              	
        		}

		           
		        $city = ($student['appl_city']!=99) ? $student['CityName']:$student['appl_city_others'];
				$state = ($student['appl_state']!=99) ? $student['StateName']:$student['appl_state_others'];
				
				$address  = (!empty($student['appl_address1'])) ? str_replace(',','',$student['appl_address1']):'';
				$address .= (!empty($student['appl_address2'])) ? ', '.str_replace(',','',$student['appl_address2']):'';
				$address .= (!empty($student['appl_address3'])) ? ', '.str_replace(',','',$student['appl_address3']):'';
				$address .= (!empty($city)) ? ' '.$city:'';
				$address .= (!empty($student['appl_postcode'])) ? ', '.$student['appl_postcode']:'';
				$address .= (!empty($state))   					? ', '.$state:'';
				$address .= (!empty($student['CountryName']))   ? ', '.$student['CountryName']:'';
				//$address .= (!empty($student['CountryName']))   ? ','.$state.' '.$student['CountryName']:','.$state;
				
				$student_list[$m]['student_name']=$student["appl_fname"].' '.$student["appl_lname"];
				$student_list[$m]['student_id']=strtoupper( $student["registrationId"] );
				$student_list[$m]['address']=$address;
				$student_list[$m]['intake']=strtoupper($student["sem_year"] );
				//$student_list[$m]['programme']= ($student['IdProgram']== 2 || $student['IdProgram']== 5) ? strtoupper($student["ProgramName"]).' ('.$student["ProgramCode"].')':strtoupper( $student["ProgramName"] );
				$student_list[$m]['programme']= strtoupper($student["ProgramName"]);
				$student_list[$m]['award_name']= $student["AwardName"].' '.$scheme;				
				$student_list[$m]['AssessmentMethod']=$student['AssessmentMethod'];
				//$student_list[$m]['AssessmentMethod']=$formData['asessmentmethod'];
				$student_list[$m]['IdProgram']=$student['IdProgram'];

				//$template = $studentRegDB->getTemplate();
				/*$template['trt_content'] = str_replace('[Student Name]', $student_list[$m]['student_name'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Address]', $student_list[$m]['address'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Student ID]', $student_list[$m]['student_id'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Year of Admission]', $student_list[$m]['intake'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Programme]', $student_list[$m]['programme'], $template['trt_content']);*/

		       		             
		   		//get registered semester
				$registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);
				
				foreach($registered_semester as $index=>$semester){
					
					//get subject registered in each semester
					if($semester["IsCountable"]==1){
						$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($IdStudentRegistration,$semester['IdSemesterMain'],$student['IdProgram']);
					}else{
						$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($IdStudentRegistration,$semester['IdSemesterMain']);
					}

					$registered_semester[$index]['subjects']=$subject_list;
					$registered_semester[$index]['total_subjects']=count($subject_list);
					
					//get grade info
					$student_grade = $studentGradeDB->getStudentGrade($IdStudentRegistration,$semester['IdSemesterMain']);
					$registered_semester[$index]['grade'] = $student_grade;
					
					//cek publish status               
					$isPublish = $publishResultDB->isPublish($student['IdProgram'],$semester['IdSemesterMain'],0);
					
					if($current_semester['IdSemesterMaster']==$semester['IdSemesterMain']){
						//if eq current semester
						unset($registered_semester[$index]);
					}else{
						//default true sebab azila suh admin boleh tgk walau x publish
						$registered_semester[$index]['isPublish']=true;
						
						if($student['IdProgram']==5){ //CIFP NI SPECIAL                	
		                	$part_list = $regSubjectDB->getPartListPerSemester($IdStudentRegistration,$semester['IdSemesterMain']);  
		                	
		                	if(count($part_list)>0){
		                		//display
		                		//$registered_semester[$index]['parts']= $part_list;
		                	}else{
		                		unset($registered_semester[$index]);
		                	}		                	
			            }
		            
					}

					
					//to cater for CE & Official Transcript
             		if(count($subject_list)==1 ){
             			$ce_found = array_search(126, array_column($subject_list, 'IdSubject'));
             			if(false !== $ce_found){
             				unset($registered_semester[$index]);
             			}
             			
             			//to cater course type=3 Thesis/Dissertation
	             		$ct3_key_found = array_search(3, array_column($subject_list, 'CourseType'));
	             		if(false !== $ct3_key_found){
	             			
	             			if($subject_list[$ct3_key_found]['exam_status']=='IP'){
	             				unset($registered_semester[$index]);
	             			}             			
	             		}  	
             		}else{
             			//to cater course type=3 Thesis/Dissertation
	             		$ct3_key_found = array_search(3, array_column($subject_list, 'CourseType'));
	             		if(false !== $ct3_key_found){	             			
	             			if($subject_list[$ct3_key_found]['exam_status']=='IP'){	             				
	             				unset($subject_list[$ct3_key_found]);
	             				$registered_semester[$index]['subjects']=$subject_list;
	             			}             			
	             		}  	
             		}
             		
             		
					
		        }	            
		        array_values($registered_semester);
		                 
		      
		 		if ($registered_semester){	 			
		 		 	$i = 0;
		            foreach ($registered_semester as $registered_semester_index => $registered_semesterLoop){
		                                
		                    if (count($registered_semesterLoop['subjects']) > 0){
		                        
		                    	$k = 0;
		                        foreach ($registered_semesterLoop['subjects'] as $subjectLoop){
		
		                        $subArr = array();
		                        $subArr2 = array();
		                            $j = 0;
		                                                        
		                            foreach ($registered_semester as $registered_semesterLoop2){
		                            
		                                if ($j > $i){

											if (isset($registered_semesterLoop2['subjects']) && count($registered_semesterLoop2['subjects'] > 0)) {
												foreach ($registered_semesterLoop2['subjects'] as $subjectLoop2) {
													if (isset($subjectLoop2['exam_status']) && isset($subjectLoop2['IdSubject'])) {
														if (in_array($subjectLoop2['exam_status'], array("U", "I"))) {
															//if( $subjectLoop2['exam_status']!='U' &&  $subjectLoop2['exam_status']!='I' ){
															array_push($subArr2, $subjectLoop2['IdSubject']);
														} else {
															array_push($subArr, $subjectLoop2['IdSubject']);
														}
													}
												}
											}
		                                }
		                                $j++;
		                            }
		                            
		                           
		                            if(in_array($subjectLoop['IdSubject'], $subArr)){
		                                $pemalar = '-';
		                                $pemalar2 = '-';
		                                $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
		                                $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
		                            }
		
		                            $checkRepeatRetake = $regSubjectDB->getRetakeSubject($subjectLoop['IdStudentRegSubjects'], $IdStudentRegistration);
		
		                            if ($checkRepeatRetake){
		                                $pemalar = '-';
		                                $pemalar2 = $subjectLoop['CreditHours'];
		                                $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
		                                $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
		                            }
		                            $k++;
		                        }
		                    }
		                    $i++;
		            }
	            }//end if
	            	
	            $student_list[$m]['semester_list']=$registered_semester;
				//var_dump($registered_semester[0]['subjects']); exit;
	            
			}//end for

			Global $convo;
			$convo = $convocation;
								
			Global $student_info;
	        $student_info = $student_list;

			$templateModel = new Communication_Model_DbTable_Template();
			$template = $templateModel->getTemplateContent($formData['template'], 'en_US');

			Global $infotemplate;
			$infotemplate = $template['tpl_content'];

			$infotemplateCombine = '';
			//dd($template['tpl_content']);
			if ($student_list){
				$no_student = 0;
				foreach ($student_list as $student){
					$no_student++;
					$infotemplatereplace = $template['tpl_content'];
					$infotemplatereplace = str_replace('[Student Name]', strtoupper($student['student_name']), $infotemplatereplace);
					$infotemplatereplace = str_replace('[Address]', strtoupper($student['address']), $infotemplatereplace);
					$infotemplatereplace = str_replace('[Student ID]', $student['student_id'], $infotemplatereplace);
					$infotemplatereplace = str_replace('[Year of Admission]', $student['intake'], $infotemplatereplace);
					$infotemplatereplace = str_replace('[Programme]', $student['programme'], $infotemplatereplace);
					$infotemplatereplace = str_replace('[Date]', ((isset($convo['c_date_from'])) ? date("j F Y",strtotime($convo['c_date_from'])):''), $infotemplatereplace);

					$result = '';
					if($student['AssessmentMethod']=='point') {
						$result = '<table class="" width="100%" border="0" cellpadding="5" cellspacing="0">';
						$result .= '<tr>';
						$result .= '<th width="10%" align="left">CODE</th>';
						$result .= '<th align="left">COURSE TITLE</th>';
						$result .= '<th width="15%">CREDIT HOUR</th>';
						$result .= '<th width="10%">GRADE</th>';
						$result .= '<th width="10%">GRADE POINT</th>';
						$result .= '<th width="10%">GPA</th>';
						$result .= '<th width="10%">CGPA</th>';
						$result .= '</tr>';

						$i=0;
						$j=1;
						$cumtotalrow = 0;
						$cch = 0;
						$cgpa = 0;
						$total_credit_hour_ct = 0;
						$total_credit_hour_ex = 0;
						$page = 1;
						foreach($student['semester_list'] as $sem_index => $semester) {
							$c = 0;
							$i++;

							if($semester['isPublish']) {
								$cch = $semester['grade']['sg_cum_credithour'];
								$cgpa = $semester['grade']['sg_cgpa'];

								if (isset($semester['subjects']) && count($semester['subjects']) > 0) {
									$result .= '<tr>';
									$result .= '<td colspan="7" align="left" class="textBold">' . $semester['SemesterMainName'] . '</td>';
									$result .= '</tr>';

									foreach ($semester['subjects'] as $key => $subject) {
										$key = $key + 1;
										$c++;

										$credit_hours = ($subject['credit_hour_registered'] != 0 || $subject['credit_hour_registered'] != '') ? $subject['credit_hour_registered'] : $subject['CreditHours'];
										if ($subject["exam_status"] == 'CT') {
											$total_credit_hour_ct = $total_credit_hour_ct + $credit_hours;
										} else
											if ($subject["exam_status"] == 'EX') {
												$total_credit_hour_ex = $total_credit_hour_ex + $credit_hours;
											}

										if ($subject["CourseType"] == 20 || ($subject["exam_status"] == 'P') || ($subject["exam_status"] == 'IP') || ($subject["exam_status"] == 'EX') || ($subject["exam_status"] == 'U')) {

											//exclude calculation

											if ($subject["CourseType"] == 20) { //CE

												$subject["CreditHours"] = '-';
												$subject["grade_point"] = '-';
												$grade_point_earned = '0.00';

											} else {

												if ($subject["exam_status"] == 'CT') {
													$subject["grade_name"] = $subject["grade_name"] . '(CT)';
												} else {
													$subject["grade_name"] = $subject["exam_status"];
												}

												$subject["grade_point"] = '-';
												$grade_point_earned = 0;

												if ($subject["exam_status"] == 'U') {
													$subject["CreditHours"] = '0';
												}

											}


										} else {
											//get point earned
											if ($subject["grade_point"] === '-') {
												$grade_point_earned = '-';
											} else {
												$grade_point_earned = abs($subject["CreditHours"]) * abs($subject["grade_point"]);
											}
										}

										$result .= '<tr>';
										$result .= '<td align="left">' . $subject["SubCode"] . '&nbsp;</td>';
										$result .= '<td align="left">' . $subject["SubName"] . '&nbsp;</td>';
										$result .= '<td align="center">';

										//official sahaja mcm ni accumulated credit hours. for coursetype=3
										if ($subject["credit_hour_registered"] != '') {
											if ($subject["CourseType"] == 3) {
												$result .= $subject["CreditHours"];
											} else {
												$result .= $subject["credit_hour_registered"];
											}
										} else {
											$result .= $subject["CreditHours"];
										}

										$result .= '</td>';
										$result .= '<td align="center">';

										if ($subject["exam_status"] == "U" || $subject["exam_status"] == "EX" || $subject["exam_status"] == "CT") {
											$grade = $subject["exam_status"];
											if ($subject["exam_status"] == "U") {
												$display = 'Off';
											}

										} else {
											$grade = $subject["grade_name"];
										}

										$result .= $grade . '&nbsp;</td>';

										$result .= '<td align="center">' . $subject["grade_point"] . '</td>';
										$result .= '<td align="center">' . (($semester['total_subjects'] != $key) ? '' : $semester['grade']['sg_gpa']) . '&nbsp;</td>';
										$result .= '<td align="center">' . (($semester['total_subjects'] != $key) ? '' : $semester['grade']['sg_cgpa']) . '&nbsp;</td>';
										$result .= '</tr>';
									}

									$totalrow = (2 + $c);
									$cumtotalrow = $cumtotalrow + $totalrow;

									if (count($student['semester_list']) != $i) {
										$result .= '<tr>';
										$result .= '<td align="center" colspan="7">';
										$result .= '<hr>';

										if (($page == 1 && $cumtotalrow > 23) || ($page > 1 && $cumtotalrow > 29)) {
											if ((count((isset($student['semester_list'][$sem_index + 1]['subjects']) ? $student['semester_list'][$sem_index + 1]['subjects'] : 0)) + 2) > 1) {
												$result .= '<div style="page-break-after: always;"></div>';
												//$result .= '<tr class="page-break"><td align="center" colspan="7"><p></p></td></tr>';
												//$result .= '<p>1:'.$page.'-'.$cumtotalrow.'-'.$semester['SemesterMainName'].'</p>';
												$result .= '<p></p>';
												//echo 'masuk 1 '.$semester['SemesterMainName'].'<br />';
												$cumtotalrow = 0;
												$page++;
											}
										}

										$result .= '</td>';
										$result .= '</tr>';

										/*if (($page == 1 && $cumtotalrow > 23) || ($page > 1 && $cumtotalrow > 29)) {
											if ((count((isset($student['semester_list'][$sem_index + 1]['subjects']) ? $student['semester_list'][$sem_index + 1]['subjects'] : 0)) + 2) > 1) {
												//$result .= '<div style="page-break-after: always;"></div>';
												//$result .= '<tr class="page-break"><td align="center" colspan="7"><p></p></td></tr>';
												//$result .= '<p>1:'.$page.'-'.$cumtotalrow.'-'.$semester['SemesterMainName'].'</p>';
												//echo 'masuk 1 '.$semester['SemesterMainName'].'<br />';
												$cumtotalrow = 0;
												$page++;
											}
										}*/
									} else {
										$cumtotalrow = $cumtotalrow + (count((isset($student['semester_list'][$sem_index]['subjects']) ? $student['semester_list'][$sem_index]['subjects'] : 0)) + 2);
									}
								}
							}
						}

						$result .= '<tr>';
						$result .= '<td align="center" colspan="7"><span class="textBold">END OF RECORD</span></td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td align="center" colspan="7"><hr></td>';
						$result .= '</tr>';
						$result .= '</table>';

						if( ($page==1 && $cumtotalrow > 18) || ($page>1 && $cumtotalrow > 29)){
							$result .= '<div style="page-break-after: always;"></div>';
							$cumtotalrow=0;
						}

						//exit;

						$result .= '<fieldset class="field_set">';
						$result .= '<table>';
						$result .= '<tr>';
						$result .= '<td>CCH TAKEN</td>';
						$result .= '<td>: '.($cch-($total_credit_hour_ex+$total_credit_hour_ct)).'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>CCH EXEMPTION</td>';
						$result .= '<td>: '.$total_credit_hour_ex.'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>CCH TRANSFERRED</td>';
						$result .= '<td>: '.$total_credit_hour_ct.'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>CGPA</td>';
						$result .= '<td>: '.$cgpa.'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>NOTE</td>';
						$result .= '<td>: Awarded the degree of '.$student['award_name'].'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>DATE</td>';
						$result .= '<td>: '.((isset($convo['c_date_from'])) ? date("j F Y",strtotime($convo['c_date_from'])):'').'</td>';
						$result .= '</tr>';
						$result .= '</table>';
						$result .= '</fieldset>';
					}else {
						$result = '<table class="" width="100%" border="0" cellpadding="5" cellspacing="0" >';
						$result .= '<tr>';
						$result .= '<th width="20%" align="left">CODE</th>';
						$result .= '<th>MODULE TITLE</th>';
						$result .= '<th width="20%" align="center">GRADE</th>';
						$result .= '</tr>';

						if (isset($student['part_three']) && count($student['part_three']) > 0) {
							$result .= '<tr>';
							$result .= '<td colspan="3" align="left" class="textBold">Articleship/PPP</td>';
							$result .= '</tr>';

							foreach ($student['part_three'] as $part3) {
								$result .= '<tr>';
								$result .= '<td align="left">' . $part3["SubCode"] . '&nbsp;</td>';
								$result .= '<td align="left">' . $part3["SubName"] . '&nbsp;</td>';
								$result .= '<td align="center">' . $part3['grade_name'] . '&nbsp;</td>';
								$result .= '</tr>';
							}

							$result .= '<tr>';
							$result .= '<td colspan="3"><hr></td>';
							$result .= '</tr>';
						}

						$i = 0;
						$j = 1;
						$rowcount = 0;
						$cumtotalrow = 0;
						$total_credit_hour_mifp = 0;
						$page = 1;
						foreach ($student['semester_list'] as $sem_index => $semester) {
							$i++;
							$c = 0;

							if (isset($semester['SemesterMainName'])){

								$result .= '<tr>';
								$result .= '<td colspan="3" align="left" class="textBold">' . $semester['SemesterMainName'] . ' Semester</td>';
								$result .= '</tr>';

								foreach ($semester['subjects'] as $key => $subject) {
									if ($subject['CourseType']!=19 && $subject['CourseType']!=2) {
										$c++;

										if ($subject['credit_hour_registered'] != 0 || $subject['credit_hour_registered'] != '') {
											$total_credit_hour_mifp = $total_credit_hour_mifp + $subject["credit_hour_registered"];
										} else {
											$total_credit_hour_mifp = $total_credit_hour_mifp + $subject["CreditHours"];
										}

										if ($subject["exam_status"] == 'P' || $subject["exam_status"] == 'IP' || $subject["exam_status"] == 'I') {
											$grade = $subject["exam_status"];
										} elseif ($subject["exam_status"] == "U" || $subject["exam_status"] == "EX") {
											$grade = $subject["exam_status"];
										} elseif ($subject["exam_status"] == "CT") {
											$grade = $subject["exam_status"];
										} else {
											$grade = $subject["grade_name"];
										}

										$result .= '<tr>';
										$result .= '<td align="left" width="20%">' . $subject["SubCode"] . '&nbsp;</td>';
										$result .= '<td align="left">' . $subject["SubName"] . '&nbsp;</td>';
										$result .= '<td align="center" width="20%">' . $grade . '&nbsp;</td>';
										$result .= '</tr>';
									}
								}

								$totalrow = (2 + $c);
								$cumtotalrow = $cumtotalrow + $totalrow;

								if (count($student['semester_list']) != $i) {
									$result .= '<tr>';
									$result .= '<td align="center" colspan="3">';
									$result .= '<hr>';
									if (($page == 1 && $cumtotalrow > 23) || ($page > 1 && $cumtotalrow > 30)) {
										//var_dump(1);
										if (isset($student['semester_list'][$sem_index + 1]['subjects']) && (count($student['semester_list'][$sem_index + 1]['subjects']) + 2) > 2) {
											//var_dump($semester);
											$result .= '<div style="page-break-after: always;"></div>';
											$result .= '<p></p>';
											//$result .= '<p>1:'.$page.'-'.$cumtotalrow.'-'.$semester['SemesterMainName'].'</p>';
											//$result .= 'break<br />';
											$cumtotalrow = 0;
											$page++;
											//break;
										}
									}
									$result .= '</td>';
									$result .= '</tr>';

									/*if (($page == 1 && $cumtotalrow > 23) || ($page > 1 && $cumtotalrow > 30)) {
										//var_dump(1);
										if ((count($student['semester_list'][$sem_index + 1]['subjects']) + 2) > 2) {
											//var_dump($semester);
											//$result .= '<div style="page-break-after: always;"></div>';
											//$result .= 'break<br />';
											$cumtotalrow = 0;
											$page++;
											//break;
										}
									}*/
								} else {
									$cumtotalrow = $cumtotalrow + ((isset($student['semester_list'][$sem_index]['subjects']) ? count($student['semester_list'][$sem_index]['subjects']) : 0) + 2);
								}
							}
						}

						$result .= '</table>';

						$result .= '<table class="" width="100%" border="0" cellpadding="5" cellspacing="0" >';
						if($student['IdProgram']==2){
							$result .= '<tr>';
							$result .= '<td align="left"><br /><p>The above mentioned student has completed '.$total_credit_hour_mifp.' credit hours and satisfied all requirements for the Masters in Islamic Finance Practice.</p></td>';
							$result .= '</tr>';
						}

						$result .= '<tr>';
						$result .= '<td align="center" class="textBold">END OF RECORD</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td align="center"><hr></td>';
						$result .= '</tr>';
						$result .= '</table>';
					}

					if( ($page==1 && $cumtotalrow > 23) || ($page>1 && $cumtotalrow > 30)){
						$result .= '<div style="page-break-after: always;"></div>';
						$cumtotalrow=0;
					}

					$result .= '<br />';
					if($student['IdProgram']==2 || $student['IdProgram']==5){
						//$result .= '<span class="textBold">DATE OF ISSUE</span> : '.((isset($convo['c_date_from'])) ? date("j F Y",strtotime($convo['c_date_from'])):'');
					}

					//$result .= '<br /><br /><br /><br />';
					/*$result .= '<div class="textBold">';
					$result .= '<p>Head<br />';
					$result .= 'Admission and Student Affairs Division</p>';
					$result .= '</div>';*/

					$infotemplatereplace = str_replace('[Result]', $result, $infotemplatereplace);

					if( $no_student != count($student_info) ){
						$infotemplatereplace .= '<div style="page-break-after: always;"></div>';
					}

					$infotemplateCombine .= $infotemplatereplace;
				}
			}

			$head = '<html>
					<head>
					 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					<style type="text/css">
					@page { margin: 80px 50px 50px 50px;}
					table { page-break-inside:auto }
					/*tr { page-break-inside:avoid; page-break-after:auto }*/


					/*@media print {
						tr.page-break  { display: block; page-break-before: always; }
					}*/

					body{
					font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
					font-size: 12px;
					padding:0;
					margin-top: 4.0em;
					margin-left: 0.6em;
					}
					.table{
					width: 100%;
					}

					.upper { text-transform:uppercase; }

					td.border-top{
					border-top: 1px solid #111111;
					}
					td.border-top-bottom{
					border-top: 1px solid #111111;
					border-bottom: 1px solid #111111;
					}
					td.bold-text{
					font-weight: bold;
					}

					.style1 {
					font-size: 18px;
					font-weight: bold;
					}
					.style2 {
					font-size: 16px;
					font-weight: bold;
					}
					.style3 {
					font-size: 14px;
					font-weight: bold;
					}
					.right{
						border-right: none;
					}
					#footer {
					 font-size: 8px;
					 position: fixed;
					 bottom: 0px;
					 left: 0px;
					 right: 0px;
					 height: 50px;
					 text-align: center;
					}
					hr {
						display: block;
						margin-top: 0.5em;
						margin-bottom: 0.5em;
						margin-left: auto;
						margin-right: auto;
						border-style: inset;
						border-width: 1px solid;
					}
					.top {
					  position: fixed;
					  top: 0;
					  left: 0;
					  z-index: 999;
					  width: 100%;
					  height: 23px;
					}

					/* Table */
					.gradetbl{
						font-family: Verdana,"Trebuchet Ms",Arial,Helvetica,"Bitstream Vera Sans",sans-serif;
						background-color: #ffffff;
						border-spacing: 0;
						border-collapse: collapse;

					}
					.gradetbl th{
						text-align:center;
						background-color: #767676;
						color:white;
						line-height: 10px;
						border:1px solid #000;
					}
					.gradetbl td{
						text-align:center;
						padding: 4px;
						border:1px solid #000;
						line-height: 10px;
					}

					.line {
						text-decoration:underline;
					}
					.field_set{
						border-color:#000;
						border-style: solid;
					}
					.textBold{
						 font-weight: bold;
					}
					.capitalize {
						text-transform: capitalize;
					}
					</style>
					</head>
					<body>

					<script type="text/php">
					if ( isset($pdf) ) {

					  $header = $pdf->open_object();
					  $font = Font_Metrics::get_font("Helvetica, Arial, sans-serif", "bold");
					  $color = array(0,0,0);
					 // $pdf->page_text(430, 20, "PRIVATE & CONFIDENTIAL", $font, 10, $color);
					  $pdf->close_object();
					  $pdf->add_object($header, "odd");
					}


									if ( isset($pdf) ) {

										  $footer = $pdf->open_object();

										  $font = Font_Metrics::get_font("Helvetica");
										  $size = 5;
										  $color = array(0,0,0);
										  $text_height = Font_Metrics::get_font_height($font, $size)+2;

										  $w = $pdf->get_width();
										  $h = $pdf->get_height();

										  // Draw a line along the bottom
										  //$y = $h - (2.5 * $text_height)-10;
										  //$pdf->line(10, $y, $w - 10, $y, $color, 1);

										  //1st row footer
										  $text = "";
										  $width = Font_Metrics::get_text_width($text, $font, $size);
										  $y = $h - (2 * $text_height);
										  $x = ($w - $width) / 2.0;

										  $pdf->page_text($x, $y, $text, $font, $size, $color);

										  //2nd row footer
										  $text = "";
										  $width = Font_Metrics::get_text_width($text, $font, $size);
										  $y = $h - (1 * $text_height);
										  $x = ($w - $width) / 2.0;

										  $pdf->page_text($x, $y, $text, $font, $size, $color);

										  $pdf->close_object();

										  $pdf->add_object($footer, "all");

										}
					</script>';

			$foot = '</body>
					</html>';

			$html = $head.$infotemplateCombine.$foot;

			//echo $html; exit;

			require_once 'dompdf_config.inc.php';
			error_reporting(0);	
			
			$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
			$autoloader->pushAutoloader('DOMPDF_autoload');
			
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->set_paper('a4', 'potrait');
			$dompdf->render();
		
			//output filename
			$output_filename = "official-transcript-bulk.pdf";
			
			//$dompdf = $dompdf->output();
			$dompdf->stream($output_filename);
		}
		exit;
	}

	public function printBulkWordAction(){

		set_time_limit(0);
		ini_set('memory_limit', '-1');

		$this->_helper->layout->disableLayout();

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			//var_dump($formData); exit;
			$studentRegDB = new Examination_Model_DbTable_StudentRegistration();
			$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
			$regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			$studentGradeDB = new Examination_Model_DbTable_StudentGrade();
			$publishResultDB = new Examination_Model_DbTable_PublishMark();
			$semesterDB = new Registration_Model_DbTable_Semester();
			$cms = new Cms_ExamCalculation();

			$student_list = array();

			$convoDB = new Graduation_Model_DbTable_Convocation();
			$convocation = $convoDB->getData($formData['c_id']);

			for($m=0; $m<count($formData['IdStudentRegistration']); $m++){

				$IdStudentRegistration = $formData['IdStudentRegistration'][$m];


				//To get Student Academic Info
				$student = $studentRegDB->getStudentInfo($IdStudentRegistration);

				if($student['IdProgram']==1 || $student['IdProgram']==3 || $student['IdProgram']==20){
					$cms->generateGrade($IdStudentRegistration);
				} else if ($student['IdProgram']==2){
					$cms->generateGradePs($IdStudentRegistration);
				}


				$current_semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));

				$scheme = '';
				if($student['IdScheme']==1){ //PS
					$scheme = '( '.$student['mop'].' '.$student['mos'].' '.$student['pt'].' )';
				}
				if($student['IdScheme']==11){ //GS
					$scheme = '( '.$student['pt'].' )';
				}
				$program_scheme = $scheme;


				if($student['IdProgram']==5){ //CIFP NI SPECIAL
					//get part III subject
					$part_three = $regSubjectDB->getSubjectPart3($IdStudentRegistration);
					$student_list[$m]['part_three']  = $part_three;
				}


				$city = ($student['appl_city']!=99) ? $student['CityName']:$student['appl_city_others'];
				$state = ($student['appl_state']!=99) ? $student['StateName']:$student['appl_state_others'];

				$address  = (!empty($student['appl_address1'])) ? trim(str_replace(',','',$student['appl_address1'])):'';
				$address .= (!empty($student['appl_address2'])) ? ', '.trim(str_replace(',','',$student['appl_address2'])):'';
				$address .= (!empty($student['appl_address3'])) ? ', '.trim(str_replace(',','',$student['appl_address3'])):'';
				$address .= (!empty($city)) ? ' '.trim($city):'';
				$address .= (!empty($student['appl_postcode'])) ? ', '.trim($student['appl_postcode']):'';
				$address .= (!empty($state))   					? ', '.trim($state):'';
				$address .= (!empty($student['CountryName']))   ? ', '.trim($student['CountryName']):'';
				//$address .= (!empty($student['CountryName']))   ? ','.$state.' '.$student['CountryName']:','.$state;

				$student_list[$m]['student_name']=$student["appl_fname"].' '.$student["appl_lname"];
				$student_list[$m]['student_id']=strtoupper( $student["registrationId"] );
				$student_list[$m]['address']=$address;
				$student_list[$m]['intake']=strtoupper($student["sem_year"] );
				//$student_list[$m]['programme']= ($student['IdProgram']== 2 || $student['IdProgram']== 5) ? strtoupper($student["ProgramName"]).' ('.$student["ProgramCode"].')':strtoupper( $student["ProgramName"] );
				$student_list[$m]['programme']= strtoupper($student["ProgramName"]);
				$student_list[$m]['award_name']= $student["AwardName"].' '.$scheme;
				$student_list[$m]['AssessmentMethod']=$student['AssessmentMethod'];
				//$student_list[$m]['AssessmentMethod']=$formData['asessmentmethod'];
				$student_list[$m]['IdProgram']=$student['IdProgram'];

				//$template = $studentRegDB->getTemplate();
				/*$template['trt_content'] = str_replace('[Student Name]', $student_list[$m]['student_name'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Address]', $student_list[$m]['address'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Student ID]', $student_list[$m]['student_id'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Year of Admission]', $student_list[$m]['intake'], $template['trt_content']);
				$template['trt_content'] = str_replace('[Programme]', $student_list[$m]['programme'], $template['trt_content']);*/


				//get registered semester
				$registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);

				foreach($registered_semester as $index=>$semester){

					//get subject registered in each semester
					if($semester["IsCountable"]==1){
						$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($IdStudentRegistration,$semester['IdSemesterMain'],$student['IdProgram']);
					}else{
						$subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($IdStudentRegistration,$semester['IdSemesterMain']);
					}

					$registered_semester[$index]['subjects']=$subject_list;
					$registered_semester[$index]['total_subjects']=count($subject_list);

					//get grade info
					$student_grade = $studentGradeDB->getStudentGrade($IdStudentRegistration,$semester['IdSemesterMain']);
					$registered_semester[$index]['grade'] = $student_grade;

					//cek publish status
					$isPublish = $publishResultDB->isPublish($student['IdProgram'],$semester['IdSemesterMain'],0);

					if($current_semester['IdSemesterMaster']==$semester['IdSemesterMain']){
						//if eq current semester
						unset($registered_semester[$index]);
					}else{
						//default true sebab azila suh admin boleh tgk walau x publish
						$registered_semester[$index]['isPublish']=true;

						if($student['IdProgram']==5){ //CIFP NI SPECIAL
							$part_list = $regSubjectDB->getPartListPerSemester($IdStudentRegistration,$semester['IdSemesterMain']);

							if(count($part_list)>0){
								//display
								//$registered_semester[$index]['parts']= $part_list;
							}else{
								unset($registered_semester[$index]);
							}
						}

					}


					//to cater for CE & Official Transcript
					if(count($subject_list)==1 ){
						$ce_found = array_search(126, array_column($subject_list, 'IdSubject'));
						if(false !== $ce_found){
							unset($registered_semester[$index]);
						}

						//to cater course type=3 Thesis/Dissertation
						$ct3_key_found = array_search(3, array_column($subject_list, 'CourseType'));
						if(false !== $ct3_key_found){

							if($subject_list[$ct3_key_found]['exam_status']=='IP'){
								unset($registered_semester[$index]);
							}
						}
					}else{
						//to cater course type=3 Thesis/Dissertation
						$ct3_key_found = array_search(3, array_column($subject_list, 'CourseType'));
						if(false !== $ct3_key_found){
							if($subject_list[$ct3_key_found]['exam_status']=='IP'){
								unset($subject_list[$ct3_key_found]);
								$registered_semester[$index]['subjects']=$subject_list;
							}
						}
					}



				}
				array_values($registered_semester);


				if ($registered_semester){
					$i = 0;
					foreach ($registered_semester as $registered_semester_index => $registered_semesterLoop){

						if (count($registered_semesterLoop['subjects']) > 0){

							$k = 0;
							foreach ($registered_semesterLoop['subjects'] as $subjectLoop){

								$subArr = array();
								$subArr2 = array();
								$j = 0;

								foreach ($registered_semester as $registered_semesterLoop2){

									if ($j > $i){

										if (isset($registered_semesterLoop2['subjects']) && count($registered_semesterLoop2['subjects'] > 0)) {
											foreach ($registered_semesterLoop2['subjects'] as $subjectLoop2) {
												if (isset($subjectLoop2['exam_status']) && isset($subjectLoop2['IdSubject'])) {
													if (in_array($subjectLoop2['exam_status'], array("U", "I"))) {
														//if( $subjectLoop2['exam_status']!='U' &&  $subjectLoop2['exam_status']!='I' ){
														array_push($subArr2, $subjectLoop2['IdSubject']);
													} else {
														array_push($subArr, $subjectLoop2['IdSubject']);
													}
												}
											}
										}
									}
									$j++;
								}


								if(in_array($subjectLoop['IdSubject'], $subArr)){
									$pemalar = '-';
									$pemalar2 = '-';
									$registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
									$registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
								}

								$checkRepeatRetake = $regSubjectDB->getRetakeSubject($subjectLoop['IdStudentRegSubjects'], $IdStudentRegistration);

								if ($checkRepeatRetake){
									$pemalar = '-';
									$pemalar2 = $subjectLoop['CreditHours'];
									$registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
									$registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
								}
								$k++;
							}
						}
						$i++;
					}
				}//end if

				$student_list[$m]['semester_list']=$registered_semester;
				//var_dump($registered_semester[0]['subjects']); exit;

			}//end for

			Global $convo;
			$convo = $convocation;

			Global $student_info;
			$student_info = $student_list;

			$templateModel = new Communication_Model_DbTable_Template();
			$template = $templateModel->getTemplateContent($formData['template'], 'en_US');

			Global $infotemplate;
			$infotemplate = $template['tpl_content'];

			$infotemplateCombine = '';

			if ($student_list){
				$no_student = 0;
				foreach ($student_list as $student){
					$no_student++;
					$infotemplatereplace = $template['tpl_content'];
					$infotemplatereplace = str_replace('[Student Name]', strtoupper($student['student_name']), $infotemplatereplace);
					$infotemplatereplace = str_replace('[Address]', strtoupper($student['address']), $infotemplatereplace);
					$infotemplatereplace = str_replace('[Student ID]', $student['student_id'], $infotemplatereplace);
					$infotemplatereplace = str_replace('[Year of Admission]', $student['intake'], $infotemplatereplace);
					$infotemplatereplace = str_replace('[Programme]', $student['programme'], $infotemplatereplace);
					$infotemplatereplace = str_replace('[Date]', ((isset($convo['c_date_from'])) ? date("j F Y",strtotime($convo['c_date_from'])):''), $infotemplatereplace);

					$result = '';
					if($student['AssessmentMethod']=='point') {
						$result = '<table class="" width="100%" border="0" cellpadding="5" cellspacing="0">';
						$result .= '<tr>';
						$result .= '<th width="10%" align="left">CODE</th>';
						$result .= '<th align="left">COURSE TITLE</th>';
						$result .= '<th width="15%">CREDIT HOUR</th>';
						$result .= '<th width="10%">GRADE</th>';
						$result .= '<th width="10%">GRADE POINT</th>';
						$result .= '<th width="10%">GPA</th>';
						$result .= '<th width="10%">CGPA</th>';
						$result .= '</tr>';

						$i=0;
						$j=1;
						$cumtotalrow = 0;
						$cch = 0;
						$cgpa = 0;
						$total_credit_hour_ct = 0;
						$total_credit_hour_ex = 0;
						$page = 1;
						foreach($student['semester_list'] as $sem_index => $semester) {
							$c = 0;
							$i++;

							if($semester['isPublish']) {
								$cch = $semester['grade']['sg_cum_credithour'];
								$cgpa = $semester['grade']['sg_cgpa'];

								$result .= '<tr>';
								$result .= '<td colspan="7" align="left" class="textBold">'.$semester['SemesterMainName'].'</td>';
								$result .= '</tr>';

								foreach($semester['subjects'] as $key=>$subject) {
									$key = $key + 1;
									$c++;

									$credit_hours = ($subject['credit_hour_registered'] != 0 || $subject['credit_hour_registered'] != '') ? $subject['credit_hour_registered'] : $subject['CreditHours'];
									if ($subject["exam_status"] == 'CT') {
										$total_credit_hour_ct = $total_credit_hour_ct + $credit_hours;
									} else
										if ($subject["exam_status"] == 'EX') {
											$total_credit_hour_ex = $total_credit_hour_ex + $credit_hours;
										}

									if ($subject["CourseType"] == 20 || ($subject["exam_status"] == 'P') || ($subject["exam_status"] == 'IP') || ($subject["exam_status"] == 'EX') || ($subject["exam_status"] == 'U')) {

										//exclude calculation

										if ($subject["CourseType"] == 20) { //CE

											$subject["CreditHours"] = '-';
											$subject["grade_point"] = '-';
											$grade_point_earned = '0.00';

										} else {

											if ($subject["exam_status"] == 'CT') {
												$subject["grade_name"] = $subject["grade_name"] . '(CT)';
											} else {
												$subject["grade_name"] = $subject["exam_status"];
											}

											$subject["grade_point"] = '-';
											$grade_point_earned = 0;

											if ($subject["exam_status"] == 'U') {
												$subject["CreditHours"] = '0';
											}

										}


									} else {
										//get point earned
										if ($subject["grade_point"] === '-') {
											$grade_point_earned = '-';
										} else {
											$grade_point_earned = abs($subject["CreditHours"]) * abs($subject["grade_point"]);
										}
									}

									$result .= '<tr>';
									$result .= '<td align="left">'.$subject["SubCode"].'&nbsp;</td>';
									$result .= '<td align="left">'.$subject["SubName"].'&nbsp;</td>';
									$result .= '<td align="center">';

									//official sahaja mcm ni accumulated credit hours. for coursetype=3
									if($subject["credit_hour_registered"]!=''){
										if($subject["CourseType"]==3){
											$result .= $subject["CreditHours"];
										}else{
											$result .= $subject["credit_hour_registered"];
										}
									}else{
										$result .= $subject["CreditHours"];
									}

									$result .= '</td>';
									$result .= '<td align="center">';

									if($subject["exam_status"]=="U" || $subject["exam_status"]=="EX" || $subject["exam_status"]=="CT"){
										$grade= $subject["exam_status"];
										if($subject["exam_status"]=="U")
										{
											$display = 'Off';
										}

									}else{
										$grade= $subject["grade_name"];
									}

									$result .= $grade.'&nbsp;</td>';

									$result .= '<td align="center">'.$subject["grade_point"].'</td>';
									$result .= '<td align="center">'.(($semester['total_subjects']!=$key) ? '':$semester['grade']['sg_gpa']).'&nbsp;</td>';
									$result .= '<td align="center">'.(($semester['total_subjects']!=$key) ? '':$semester['grade']['sg_cgpa']).'&nbsp;</td>';
									$result .= '</tr>';
								}

								$totalrow=(2+$c);
								$cumtotalrow = $cumtotalrow+ $totalrow;

								if(count($student['semester_list'])!=$i){
									$result .= '<tr>';
									$result .= '<td align="center" colspan="7">';
									$result .= '<hr>';

									$result .= '</td>';
									$result .= '</tr>';

									if(  ($page==1 && $cumtotalrow > 22) || ($page>1 && $cumtotalrow > 29) ){
										if((count((isset($student['semester_list'][$sem_index+1]['subjects']) ? $student['semester_list'][$sem_index+1]['subjects']:0)) + 2 ) > 1){
											$result .= '<div style="page-break-after: always;"></div>';
											$result .= '';
											$cumtotalrow=0;
											$page++;
										}
									}
								}else{
									$cumtotalrow=$cumtotalrow+(count((isset($student['semester_list'][$sem_index]['subjects']) ? $student['semester_list'][$sem_index]['subjects']:0)) + 2 );
								}
							}
						}

						$result .= '<tr>';
						$result .= '<td align="center" colspan="7"><span class="textBold">END OF RECORD</span></td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td align="center" colspan="7"><hr></td>';
						$result .= '</tr>';
						$result .= '</table>';

						if( ($page==1 && $cumtotalrow > 18) || ($page>1 && $cumtotalrow > 29)){
							$result .= '<div style="page-break-after: always;"></div>';
							$result .= '';
							$cumtotalrow=0;
						}

						$result .= '<fieldset class="field_set">';
						$result .= '<table>';
						$result .= '<tr>';
						$result .= '<td>CCH TAKEN</td>';
						$result .= '<td>: '.($cch-($total_credit_hour_ex+$total_credit_hour_ct)).'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>CCH EXEMPTION</td>';
						$result .= '<td>: '.$total_credit_hour_ex.'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>CCH TRANSFERRED</td>';
						$result .= '<td>: '.$total_credit_hour_ct.'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>CGPA</td>';
						$result .= '<td>: '.$cgpa.'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>NOTE</td>';
						$result .= '<td>: Awarded the degree of '.$student['award_name'].'</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td>DATE</td>';
						$result .= '<td>: '.((isset($convo['c_date_from'])) ? date("j F Y",strtotime($convo['c_date_from'])):'').'</td>';
						$result .= '</tr>';
						$result .= '</table>';
						$result .= '</fieldset>';
					}else {
						$result = '<table class="" width="100%" border="0" cellpadding="5" cellspacing="0" >';
						$result .= '<tr>';
						$result .= '<th width="20%" align="left">CODE</th>';
						$result .= '<th>MODULE TITLE</th>';
						$result .= '<th width="20%" align="center">GRADE</th>';
						$result .= '</tr>';

						if (isset($student['part_three']) && count($student['part_three']) > 0) {
							$result .= '<tr>';
							$result .= '<td colspan="3" align="left" class="textBold">Articleship/PPP</td>';
							$result .= '</tr>';

							foreach ($student['part_three'] as $part3) {
								$result .= '<tr>';
								$result .= '<td align="left">' . $part3["SubCode"] . '&nbsp;</td>';
								$result .= '<td align="left">' . $part3["SubName"] . '&nbsp;</td>';
								$result .= '<td align="center">' . $part3['grade_name'] . '&nbsp;</td>';
								$result .= '</tr>';
							}

							$result .= '<tr>';
							$result .= '<td colspan="3"><hr></td>';
							$result .= '</tr>';
						}

						$i = 0;
						$j = 1;
						$rowcount = 0;
						$cumtotalrow = 0;
						$total_credit_hour_mifp = 0;
						$page = 1;
						foreach ($student['semester_list'] as $sem_index => $semester) {
							$i++;
							$c = 0;

							if (isset($semester['SemesterMainName'])){

								$result .= '<tr>';
								$result .= '<td colspan="3" align="left" class="textBold">' . $semester['SemesterMainName'] . ' Semester</td>';
								$result .= '</tr>';

								foreach ($semester['subjects'] as $key => $subject) {
									if ($subject['CourseType']!=19 && $subject['CourseType']!=2) {
										$c++;

										if ($subject['credit_hour_registered'] != 0 || $subject['credit_hour_registered'] != '') {
											$total_credit_hour_mifp = $total_credit_hour_mifp + $subject["credit_hour_registered"];
										} else {
											$total_credit_hour_mifp = $total_credit_hour_mifp + $subject["CreditHours"];
										}

										if ($subject["exam_status"] == 'P' || $subject["exam_status"] == 'IP' || $subject["exam_status"] == 'I') {
											$grade = $subject["exam_status"];
										} elseif ($subject["exam_status"] == "U" || $subject["exam_status"] == "EX") {
											$grade = $subject["exam_status"];
										} elseif ($subject["exam_status"] == "CT") {
											$grade = $subject["exam_status"];
										} else {
											$grade = $subject["grade_name"];
										}

										$result .= '<tr>';
										$result .= '<td align="left" width="20%">' . $subject["SubCode"] . '&nbsp;</td>';
										$result .= '<td align="left">' . $subject["SubName"] . '&nbsp;</td>';
										$result .= '<td align="center" width="20%">' . $grade . '&nbsp;</td>';
										$result .= '</tr>';
									}
								}

								$totalrow = (2 + $c);
								$cumtotalrow = $cumtotalrow + $totalrow;

								if (count($student['semester_list']) != $i) {
									$result .= '<tr>';
									$result .= '<td align="center" colspan="3">';
									$result .= '<hr>';

									$result .= '</td>';
									$result .= '</tr>';

									if (($page == 1 && $cumtotalrow > 23) || ($page > 1 && $cumtotalrow > 30)) {
										//var_dump(1);
										if ((count($student['semester_list'][$sem_index + 1]['subjects']) + 2) > 2) {
											//var_dump($semester);
											$result .= '<div style="page-break-after: always;"></div>';
											$result .= '';
											//$result .= 'break<br />';
											$cumtotalrow = 0;
											$page++;
											//break;
										}
									}
								} else {
									$cumtotalrow = $cumtotalrow + ((isset($student['semester_list'][$sem_index]['subjects']) ? count($student['semester_list'][$sem_index]['subjects']) : 0) + 2);
								}
							}
						}

						$result .= '</table>';

						$result .= '<table class="" width="100%" border="0" cellpadding="5" cellspacing="0" >';
						if($student['IdProgram']==2){
							$result .= '<tr>';
							$result .= '<td align="left"><br /><p>The above mentioned student has completed '.$total_credit_hour_mifp.' credit hours and satisfied all requirements for the Masters in Islamic Finance Practice.</p></td>';
							$result .= '</tr>';
						}

						$result .= '<tr>';
						$result .= '<td align="center" class="textBold">END OF RECORD</td>';
						$result .= '</tr>';
						$result .= '<tr>';
						$result .= '<td align="center"><hr></td>';
						$result .= '</tr>';
						$result .= '</table>';
					}

					if( ($page==1 && $cumtotalrow > 23) || ($page>1 && $cumtotalrow > 30)){
						$result .= '<div style="page-break-after: always;"></div>';
						$result .= '';
						$cumtotalrow=0;
					}

					$result .= '<br />';
					if($student['IdProgram']==2 || $student['IdProgram']==5){
						//$result .= '<span class="textBold">DATE OF ISSUE</span> : '.((isset($convo['c_date_from'])) ? date("j F Y",strtotime($convo['c_date_from'])):'');
					}

					/*$result .= '<br /><br /><br /><br />';
					$result .= '<div class="textBold">';
					$result .= '<p>Head<br />';
					$result .= 'Admission and Student Affairs Division</p>';
					$result .= '</div>';

					if( $no_student != count($student_info) ){
						$result .= '<div style="page-break-after: always;"></div>';
						$result .= '';
					}*/

					$infotemplatereplace = str_replace('[Result]', $result, $infotemplatereplace);
					$infotemplateCombine .= $infotemplatereplace;
				}
			}

			$head = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">
					<head>
					 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					<style type="text/css">
					@page { margin: 80px 50px 50px 50px;}
					table { page-break-inside:auto }
					tr { page-break-inside:avoid; page-break-after:auto }

					body{
					font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
					font-size: 12px;
					padding:0;
					margin-top: 4.0em;
					margin-left: 0.6em;
					}
					.table{
					width: 100%;
					}

					.upper { text-transform:uppercase; }

					td.border-top{
					border-top: 1px solid #111111;
					}
					td.border-top-bottom{
					border-top: 1px solid #111111;
					border-bottom: 1px solid #111111;
					}
					td.bold-text{
					font-weight: bold;
					}

					.style1 {
					font-size: 18px;
					font-weight: bold;
					}
					.style2 {
					font-size: 16px;
					font-weight: bold;
					}
					.style3 {
					font-size: 14px;
					font-weight: bold;
					}
					.right{
						border-right: none;
					}
					#footer {
					 font-size: 8px;
					 position: fixed;
					 bottom: 0px;
					 left: 0px;
					 right: 0px;
					 height: 50px;
					 text-align: center;
					}
					hr {
						display: block;
						margin-top: -0.5em;
						margin-bottom: 0.5em;
						margin-left: auto;
						margin-right: auto;
						border-style: inset;
						/*border-width: 1px;*/
						border: 1px solid;
						color: black;
					}
					.top {
					  position: fixed;
					  top: 0;
					  left: 0;
					  z-index: 999;
					  width: 100%;
					  height: 23px;
					}

					/* Table */
					.gradetbl{
						font-family: Verdana,"Trebuchet Ms",Arial,Helvetica,"Bitstream Vera Sans",sans-serif;
						background-color: #ffffff;
						border-spacing: 0;
						border-collapse: collapse;

					}
					.gradetbl th{
						text-align:center;
						background-color: #767676;
						color:white;
						line-height: 10px;
						border:1px solid #000;
					}
					.gradetbl td{
						text-align:center;
						padding: 4px;
						border:1px solid #000;
						line-height: 10px;
					}

					.line {
						text-decoration:underline;
					}
					.field_set{
						border-color:#000;
						border-style: solid;
					}
					.textBold{
						 font-weight: bold;
					}
					.capitalize {
						text-transform: capitalize;
					}
					.pagebreak {
						page-break-before: always;
					}
					</style>
					</head>
					<body>';

			$foot = '</body>
					</html>';

			$html = $head.$infotemplateCombine.$foot;
			//$html = $infotemplateCombine;
			//echo $html; exit;
			$this->view->filename = date('Ymd').'_official-transcript-bulk.doc';
			$this->view->html = $html;

			//echo $html; exit;

			//New Word Document
			/*$PHPWord = new PhpOffice\PhpWord\PhpWord();

			//New portrait section
			$section = $PHPWord->createSection();

			//Add header
			$header = $section->createHeader();
			//$table = $header->addTable();
			//$table->addRow();
			//$table->addCell(4500)->addText('');

			//Add footer
			$footer = $section->createFooter();
			//$footer->addPreserveText('', array('align'=>'center'));

			//Write some text
			//$section->addTextBreak();
			//$section->addText('');

			\PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);

			//Save File
			$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
			$objWriter->save(APP_DOC_PATH.'/'.date('YmdHis').'_official-transcript-bulk.docx');*/

			/*$document = $PHPWord->loadTemplate(APP_DOC_PATH.'/official-transcript-bulk.docx');
			$toOpenXML = HTMLtoOpenXML::getInstance()->fromHTML($html);
			//echo $html;
			//echo $toOpenXML = str_replace('<w:p>', '<w:p xmlns:w="http://www.example.com">', $toOpenXML); exit;
			$toOpenXML = str_replace('<w:p>', '<w:p xmlns:w="http://www.example.com">', $toOpenXML);
			$toOpenXML = str_replace('dy>', '', $toOpenXML);
			$toOpenXML = str_replace('</td>', '', $toOpenXML);
			$toOpenXML = str_replace('</h3>', '', $toOpenXML);
			$cutValue = (strpos($toOpenXML, '</w:p>')+6);
			$toOpenXML = substr($toOpenXML, 0, $cutValue);
			$toOpenXML = '<?xml version="1.0" encoding="UTF-8"?>'.$toOpenXML;
			//echo $toOpenXML;
			echo $html;
			//exit;
			$document->setValue('Value1', $toOpenXML);

			$document->save(APP_DOC_PATH.'/'.date('YmdHis').'_Solarsystem.docx');*/
		}
		//exit;
	}
	
	
	public function display($registered_semester,$IdStudentRegistration,$student,$current_semester,$type=1){

			$regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			$studentRegDB = new Examination_Model_DbTable_StudentRegistration();	
			$studentGradeDB = new Examination_Model_DbTable_StudentGrade();	
			$publishResultDB = new Examination_Model_DbTable_PublishMark(); 
				
			foreach($registered_semester as $index=>$semester){

				$registered_semester[$index]['publish_official_transcript']=true; //set default
				
                //get subject registered in each semester
                if($semester["IsCountable"]==1){				
                    $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($IdStudentRegistration,$semester['IdSemesterMain'],$student['IdProgram'], 1);
                }else{			
                    $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($IdStudentRegistration,$semester['IdSemesterMain'], 1);
                }
                                
             	if(count($subject_list)>0){
             		
             		$registered_semester[$index]['total_subjects']=count($subject_list);
	                $registered_semester[$index]['subjects']=$subject_list;
	
	                //get grade info
	                $student_grade = $studentGradeDB->getStudentGrade($IdStudentRegistration,$semester['IdSemesterMain']);
	                $registered_semester[$index]['grade'] = $student_grade;
	                                
	                //cek publish status   --- yatie 5-6-2015            
					$isPublish = $publishResultDB->isPublish($student['IdProgram'],$semester['IdSemesterMain'],0);
					
					if($current_semester['IdSemesterMaster']==$semester['IdSemesterMain']){
						//if eq current semester
						unset($registered_semester[$index]);
					}else{
						//default true sebab azila suh admin boleh tgk walau x publish
						$registered_semester[$index]['isPublish']=true;
						
						if($student['IdProgram']==5){ //CIFP NI SPECIAL                	
		                	$part_list = $regSubjectDB->getPartListPerSemester($IdStudentRegistration,$semester['IdSemesterMain']);                	
		                	$registered_semester[$index]['parts']= $part_list;
		                }						
					}
					//------------------end publish
             	}else{
             		unset($registered_semester[$index]);
             	}  

				//to cater for CE & Official Transcript
				
             	if(count($subject_list)==1 ){
             		$ce_found = array_search(126, array_column($subject_list, 'IdSubject'));
             		if(false !== $ce_found){
             			
             			if($type==2){
             				//do not display ce in official transcript
             				unset($registered_semester[$index]);
             			}else{
             				//do not display ce in official transcript
             				$registered_semester[$index]['publish_official_transcript']=false;
             			}
             		}
             	}
            }
            
            //echo '<pre>';
            //print_r($registered_semester[4]['subjects']);
            
            array_values($registered_semester);
				
            //$i = 0;
            foreach ($registered_semester as $i => $registered_semesterLoop){
                                
                    if (count($registered_semesterLoop['subjects']) > 0){
                        
                    	$k = 0;
                        foreach ($registered_semesterLoop['subjects'] as $subjectLoop){

                        $subArr = array();
                        $subArr2 = array();
                            $j = 0;
                                                        
                            foreach ($registered_semester as $registered_semesterLoop2){
                            
                                if ($j > $i){
                                
                                    foreach ($registered_semesterLoop2['subjects'] as $subjectLoop2){  
	                                    if (in_array($subjectLoop2['exam_status'], array("U","I"))) {
	                                    //if( $subjectLoop2['exam_status']!='U' &&  $subjectLoop2['exam_status']!='I' ){
	                                        array_push($subArr2, $subjectLoop2['IdSubject']);
	                                    }else{
	                                    	array_push($subArr,$subjectLoop2['IdSubject']);
	                                    }
                                    }                                    
                                }
                                $j++;
                            }

                            if(in_array($subjectLoop['IdSubject'], $subArr)){
                                $pemalar = '-';
                                $pemalar2 = '-';
                                $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
                                $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
                            }

                            $checkRepeatRetake = $regSubjectDB->getRetakeSubject($subjectLoop['IdStudentRegSubjects'], $IdStudentRegistration);

							/*if ($subjectLoop['IdSubject'] == 1){
								var_dump(in_array($subjectLoop['IdSubject'], $subArr));
								var_dump($subjectLoop);
								var_dump($registered_semester[$i]['subjects'][$k]['grade_point']);
							}*/

                            if ($checkRepeatRetake){
                                $pemalar = '-';
                                $pemalar2 = $subjectLoop['CreditHours'];
                                $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
                                $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
                            }
                            $k++;
                        }
                    }
                    //$i++;
            }

		//echo '<pre>';
		//print_r($registered_semester[4]['subjects']);
            return $registered_semester;
	}

	public function templateSetupAction(){
		$this->view->title = $this->view->translate('Transcript Template Setup');

		$model = new Examination_Model_DbTable_StudentRegistration();
		$auth = Zend_Auth::getInstance();

		$template = $model->getTemplate();
		$this->view->template = $template;

		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$data = array(
				'trt_content'=>$formData['content'],
				'trt_updby'=>$auth->getIdentity()->iduser,
				'trt_upddate'=>date('Y-m-d H:i:s')
			);

			if ($template){
				$model->updateTemplate($data, $template['trt_id']);
			}else{
				$model->insertTemplate($data);
			}

			//redirect here
			$this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
			$this->_redirect($this->baseUrl . '/examination/exam-result/template-setup/');
		}
	}
}
?>