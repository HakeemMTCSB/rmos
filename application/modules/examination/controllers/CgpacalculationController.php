<?php
class Examination_CgpacalculationController extends Base_Base {
	private $lobjplacementtestmarksmodel;
	private $lobjGpacalculation;
	private $lobjGpacalculationForm;
	private $lobjCgpacalculationForm;

	public function init() {
		$this->fnsetObj();
	}

	public function fnsetObj(){
		$this->lobjCgpacalculationForm = new Examination_Form_Cgpacalculation();
		$this->lobjCgpacalculation = new Examination_Model_DbTable_Cgpacalculation();
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();

		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		$this->lobjGpacalculation = new Examination_Model_DbTable_Gpacalculation();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistrationbulk();
		$this->lobjGpacalculationForm = new Examination_Form_Gpacalculation();
	}

	public function indexAction() {

		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjCgpacalculation->fngetStudentApplicationDetails();

		$studentlist = $this->lobjStudentregistrationModel->fnGetApplicantNameList();
		$this->view->lobjform->field5->addMultiOptions($studentlist);

		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
		$this->view->lobjform->field8->addMultiOptions($larProgramNameCombo);

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->cgpacalculation);

		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->cgpacalculation)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->cgpacalculation,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();

			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCgpacalculation->fnSearchStudentApplication( $lobjform->getValues ()); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->cgpacalculation = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/cgpacalculation/index');
		}

	}

	public function newcgpacalculationAction() {

		$this->view->lobjCgpacalculationForm = $this->lobjCgpacalculationForm;

		$lintidapplicant = $this->_getParam('idapplicant');

		$larrresult = $this->lobjCgpacalculation->fnGetAvgmarks( $lintidapplicant );
		$this->view->studname = $larrresult['FName'].' '.$larrresult['MName'].' '.$larrresult['LName'];
		$this->view->Progname = $larrresult['ProgramName'];
		$this->view->cgpa = $larrresult['totalsubmarks'];
		echo "<pre>";
		print_r($larrresult);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjCgpacalculationForm->IdApplication->setValue ( $lintidapplicant );
		$this->view->lobjCgpacalculationForm->Cgpa->setValue ( $larrresult['totalsubmarks'] );
		$this->view->lobjCgpacalculationForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjCgpacalculationForm->UpdUser->setValue ( $auth->getIdentity()->iduser );





			
	}


}