<?php

class examination_MarkDistributionCeController extends Base_Base { //Controller for the User Module

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Mark Distribution - CE");
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
		$form = new Examination_Form_MarkDistCeSearch(array('locale'=>$locale));
		$this->view->form = $form;
        	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			if($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$this->view->idSemester = $formData['IdSemester'];
				$formData['CourseType']=20;
				
				//get subject list 
				$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
				$subjects = $subjectDB->searchSubject($formData);
				
				$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
				foreach($subjects as $index=>$subject){
					$info = $markDistributionDB->getCeInfo($formData['IdSemester'],$subject['IdSubject']);
					$subjects[$index]['detail']=$info;
				}
				$this->view->subjects = $subjects;
				
			}												
			
    	}//if post		
    }
    
    public function viewAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution - CE");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');    	  	
    	$idSubject = $this->_getParam('idSubject');    	
    	
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;
    	
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$semester = $semesterDB->fngetSemestermainDetails($idSemester);    	
    	$this->view->semester =  $semester;
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$info = $markDistributionDB->getCeInfo($idSemester,$idSubject);
    	$this->view->info = $info;	    

    	
    	if(!$info){
    		//create
    		$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();		
			$assessment_list = $AssessmenttypeDB->getAssessmentbyType(1,$idSemester,$semester[0]['IdScheme']);
		
			foreach($assessment_list as $index=>$list){
					
					
					//insert into table					 		
					$data["semester"]  = $idSemester;
					$data["IdProgram"] = 0;			
					$data["IdCourse"]  = $idSubject;				
					$data["IdCourseTaggingGroup"]  = 0;			
					$data["Status"]    = 0;//Entry
					$data['UpdUser']   = $auth->getIdentity()->iduser;
					$data['UpdDate']   = date ( 'Y-m-d H:i:s');
					$data["TotalMark"] = 0;		
					$data["IdComponentType"]= $list['IdExaminationAssessmentType'];			 			 
				 	$data["Marks"]= 0;
				 	$data["Percentage"]= 0;
				 	$data["TotalMark"]= 0;
				 	$data['allow_appeal']= 1;
					$data['allow_resit']= 1;
					$data['Type']= 1;		
		    						
		    		//print_r($data);
					$markDistributionDB->addData($data);
				
			}
    	}
    	
    	
    	//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,null,$idSubject,null,1);	    
    
    	//print_r($list_component);
    	
    	$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    	//echo '<pre>';
    	//print_r($list_component);
    	
    	
    }
    
    
    
	public function entryAction(){
    	
    	$this->view->title=$this->view->translate("Mark Distribution - CE : Entry");
    	
    	
    	$this->_helper->layout->disableLayout();
    	 
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$auth = Zend_Auth::getInstance();
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;    	
    	$this->view->idSubject = $idSubject;    	
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	//get ecturer
    	$staffDB = new GeneralSetup_Model_DbTable_Staffmaster();
    	$this->view->academic_staff = $staffDB->getAcademicStaff();
    	
    	
 		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup(); 		
 		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
		$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
			
		//get component
    	$AssessmenttypeDB = new Examination_Model_DbTable_Assessmenttype();
		//$assessment_list = $AssessmenttypeDB->getAssessmentbyType(1,$idSemester);

		$assessment_list = $markDistributionDB->getListMainComponent($idSemester,null,$idSubject,null,1);
			
		$total_raw = 0;
		$mds_weightage = 0;
		foreach($assessment_list as $index=>$list){								
				
				//get mark each component
				$main_mark_distribution = $markDistributionDB->getCeComponentByAssessmentId($list['IdExaminationAssessmentType'],$idSemester,$idSubject);
				$assessment_list[$index]['IdMarksDistributionMaster']= $main_mark_distribution['IdMarksDistributionMaster'];
				$assessment_list[$index]['mds_raw']= $main_mark_distribution['Marks'];
				$assessment_list[$index]['mds_weightage']= $main_mark_distribution['Percentage'];
				$assessment_list[$index]['allow_appeal']= $main_mark_distribution['allow_appeal'];
				$assessment_list[$index]['allow_resit']= $main_mark_distribution['allow_resit'];
				$assessment_list[$index]['ce_pass_mark']= $main_mark_distribution['ce_pass_mark'];
				
				$total_raw = $total_raw + $main_mark_distribution['Marks'];
				$total_weightage = $mds_weightage + $main_mark_distribution['Percentage'];
								
				//get item
				$component_item_list = $oCompitem->getListComponentItem($main_mark_distribution['IdMarksDistributionMaster']);
				$assessment_list[$index]['component_item']= $component_item_list;
				
	
			
		}
		$this->view->total_raw = $total_raw;
		$this->view->list_component = $assessment_list;  
		
		//echo '<pre>';
		//print_r($assessment_list);	
    		  
    }
    
    
 	function saveAction(){
    	$auth = Zend_Auth::getInstance();
    	
    	if ($this->getRequest()->isPost()) {
			
    		$setupDb = new Examination_Model_DbTable_MarkDistributionSetup(); 		
 			$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
			$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
				
		
				$formData = $this->getRequest()->getPost();

				//echo '<pre>';
			   // echo print_r($formData);				
				//exit;
				
				//delete checked
	    		 for($d=0; $d < count($formData["idDel"]); $d++){
				 	
				 	$idDel =  $formData["idDel"][$d];
				 	
				 	//delete component				
	    			$oCompitem->deleteData($idDel);
	    		 }
				
				for($i=0; $i<count($formData['IdExaminationAssessmentType']); $i++){
				 	
					$IdExaminationAssessmentType = $formData['IdExaminationAssessmentType'][$i];
					
					$IdMarksDistributionMaster = $formData['IdMarksDistributionMaster'][$IdExaminationAssessmentType];
					
				 	$data["IdComponentType"]= $IdExaminationAssessmentType;			 			 
				 	$data["Marks"]= $formData["Raw"][$IdExaminationAssessmentType];
				 	//$data["Percentage"]= $formData["Weightage"][$IdExaminationAssessmentType];
				 	$data["Percentage"]= 0 ;	
				 	$data["ce_pass_mark"]= $formData["ec_pass_mark"][$IdExaminationAssessmentType];	
				 	$data["ce_total_mark"]= $formData["ce_total_mark"][$IdExaminationAssessmentType];	
				 	
					if(isset($formData["appeal"][$IdExaminationAssessmentType])){
				 		$data["allow_appeal"] = $formData["appeal"][$IdExaminationAssessmentType];
				 	}
				 	
				 	if(isset($formData["resit"][$IdExaminationAssessmentType])){
				    	$data["allow_resit"] = $formData["resit"][$IdExaminationAssessmentType];
				 	}				    
				 	
				 	
				 	if($IdMarksDistributionMaster){
				 		//echo 'update table	';			   
				    	$markDistributionDB->updateData($data,$IdMarksDistributionMaster);	
				 		
				 	}else{
				 		//insert into table					 		
						$data["semester"]  = $formData["idSemester"];
						$data["IdProgram"] = 0;			
						$data["IdCourse"]  = $formData["idSubject"];	
						$data["IdCourseTaggingGroup"]  = 0;			
						$data["Status"]    = 0;//Entry
						$data["Type"]    = 1;
						$data['UpdUser']   = $auth->getIdentity()->iduser;
						$data['UpdDate']   = date ( 'Y-m-d H:i:s');
						$data["TotalMark"] = $formData["total_mark"];				
				    	$IdMarksDistributionMaster = $markDistributionDB->addData($data);
				    	
				    } 
				   
				    //add item
		    		for($m=0; $m<count($formData['component_item_name'][$IdExaminationAssessmentType]); $m++){
		    			
		    			$IdDetails = $formData['IdMarksDistributionDetails'][$IdExaminationAssessmentType][$m];				    		
		    			$component_item_name = $formData['component_item_name'][$IdExaminationAssessmentType][$m];
		    							    			
		    			$data_item['ComponentName'] = $component_item_name;
		    			$data_item['IdMarksDistributionDetails'] = $IdDetails;
		    			$data_item['Weightage'] = $formData['itemRaw'][$IdExaminationAssessmentType][$m];
		    			//$data_item['Percentage'] = $formData['itemWeightage'][$IdExaminationAssessmentType][$m];
		    			$data_item['Percentage'] = 0;
		    			$data_item['IdLecturer'] = $formData['IdLecturer'][$IdExaminationAssessmentType][$m];
		    			$data_item['TotalMark'] = $formData["Raw"][$IdExaminationAssessmentType];
		    			$data_item['UpdUser']   = $auth->getIdentity()->iduser;
						$data_item['UpdDate']   = date ( 'Y-m-d H:i:s');
												
						/*if(isset($formData["ec_compulsory"][$IdExaminationAssessmentType][$m]) && $formData["ec_compulsory"][$IdExaminationAssessmentType][$m]==1){
							$data_item['ec_compulsory'] = 1; 
						}else{
							$data_item['ec_compulsory'] = 0;
						}*/
						
						if(isset($component_item_name) && $component_item_name!=''){
							if(isset($IdDetails) && $IdDetails!=''){
								//echo 'updte';
								$data_item['IdMarksDistributionMaster']=$IdMarksDistributionMaster;	
								$oCompitem->updateData($data_item,$IdDetails);
							}else{
								//echo 'add';
								$data_item['IdMarksDistributionMaster']=$IdMarksDistributionMaster;	
								$data_item['idSemester']=$formData["idSemester"];
			    				$oCompitem->addData($data_item);
			    			 	
							}	
							//print_r($data_item);			
						}		
		    		}//end for
				
				 
				   
			    }//end for
				
				
		  		//redirect
		  		$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-distribution-ce', 'action'=>'view','idSemester'=>$formData["idSemester"],'idSubject'=>$formData["idSubject"]),'default',true));
		   
		
    		}//end if
    }
    
    
	public function searchCeCourseAction(){
	
		$idCollege = $this->_getParam('idCollege',null);
		 
		$this->_helper->layout->disableLayout();
	
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$db = Zend_Db_Table::getDefaultAdapter();
       
	    $select = $db->select()
	 				 ->from(array("sm"=>"tbl_subjectmaster"))	
	 				 ->where('sm.CourseType=20') 				 
	 				 ->where("sm.Active = 1")
	 				 ->order('sm.SubCode');
	 				 
	 	if($idCollege){
	 		$select->where('sm.IdFaculty = ?',$idCollege);
	 	}
		$row = $db->fetchAll($select);
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($row);
	
		echo $json;
		exit();
	}
	
	public function viewCeAction(){
    	
    	$this->_helper->layout->disableLayout();
    	
    	$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
    	$idSemester = $this->_getParam('idSemester');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idSubject = $idSubject;

    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);    	
        
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();    	
    		//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,null,$idSubject,null,1);	    
    
    	//print_r($list_component);
    	
    	$oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
    	foreach($list_component as $index=>$component){
    		
		  	  $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
		  	  $list_component[$index]['component_item'] = $component_item;
    	}
    	$this->view->rs_component = $list_component;
    
    	
    	
    }
}

?>