<?php
class examination_GeneratecgpaController extends Base_Base { //Controller for the User Module

	private $lobjprogram;
	private $lobjgengradesetup;
	private $lobjsemester;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjsemester =  new GeneralSetup_Model_DbTable_Semester();
		$this->lobjgengradesetup = new Examination_Model_DbTable_Generategrade();
	}


	/**
	 * Function to search students and update them with cgpa and gpa values
	 * @author: VT
	 */
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;

		// SHOW THE PROGRAM
		$larrprogramresult = $this->lobjprogram->fngetProgramDetails ();
		foreach($larrprogramresult as $larrprgmvalues) {
			$this->view->lobjform->field24->addMultiOption($larrprgmvalues['IdProgram'],$larrprgmvalues['ProgramCode'].'-'.$larrprgmvalues['ProgramName']);
		}

		// SHOW THE SEMESTER
		//$larrsemresult =  $this->lobjStudentRegModel->fetchAllSemMaster();
		$larrsemresult =  $this->lobjsemester->getAllsemesterListCodeID();
		foreach($larrsemresult as $larrsemvalues) {
			$this->view->lobjform->field27->addMultiOption($larrsemvalues['key'],$larrsemvalues['value']);
		}


		//$this->view->lobjform->Search->setAttrib('Onclick','return formsubmit();')->setLabel('Process Grade');
		if ($this->_request->isPost () && $this->_request->getParam ( 'Search' ) ) {
			$larrformData = $this->_request->getPost ();			
			$errMsgInsert = $this->lobjgengradesetup->fnGetStudentsCGPA($larrformData,$userId);			
			if($errMsgInsert == '1') {

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array (  'user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'CGPA and GPA values ',
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				//$this->view->errMsg = "1";
				$this->_redirect( $this->baseUrl . '/examination/generatecgpa');
			} else {
				//$this->view->errMsg = "0";
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/generatecgpa');
		}
	}


}
