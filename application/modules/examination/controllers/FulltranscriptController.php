<?php

class examination_FulltranscriptController extends Base_Base {

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjAcademicstatusForm = new Examination_Form_Academicstatus();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjDeparmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster();
		$this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
		$this->lobjschemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
		$this->lobjacademicprogress = new Records_Model_DbTable_Academicprogress();
		$this->lobjcompletlistModel = new Examination_Model_DbTable_Generatecompletelist();
	}

	public function indexAction(){
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjProgramNameList = $this->lobjAcademicstatus->fnGetProgramNameList();
		$lobjform->field27->addMultiOptions($lobjProgramNameList);
		$lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$lobjsemesterlist = $this->lobjsemesterModel->getAllsemesterListCode();
		$lobjform->field10->addMultiOptions($lobjsemesterlist);

		if (!$this->_getParam('search'))
		unset($this->gobjsessionsis->Partialtranspaginatorresult);
		$larrresult = $this->lobjcompletlistModel->fngetCompleteStudentListAll($post = array()); //get user details
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance
		if (isset($this->gobjsessionsis->Partialtranspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Partialtranspaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjcompletlistModel->fngetCompleteStudentListAll($larrformData	); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->Partialtranspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/examination/fulltranscript/index');
		}
	}

	public function downloadtranscriptAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintId = ( int ) $this->_getParam('idStudent');
		if(isset($lintId)){
			// create new PDF document
		   	$this->view->StudentData = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($lintId);

		   	$this->view->larrresult = $this->lobjAddDropSubjectModel->fngetRegisteredStudentDtls();

		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;

		// INSERT PROCESS FOR ACADEMIC PROGRESS STARTS
		// BEFORE INSERTING DELETE ALL Academic records based on studentID
		$this->lobjacademicprogress->deleteRecords($lintId);

		// FETCH STUDENT DETAILS
		$studentdetails = $this->lobjacademicprogress->fetchStudentRegDetails($lintId);
		//if(count($studentdetails)>0) { }
		$academicprogressID = $this->lobjacademicprogress->insertAcademicProgress($studentdetails);

		// get the courses based on landscape and program ID
		$landscapeID = $studentdetails['IdLandscape'];
		$programID = $studentdetails['IdProgram'];
		// hide $this->lobjacademicprogress->fetchCoursesInsert($lintidreg,$academicprogressID,$landscapeID,$programID,$studentdetails,$userId);
		$this->lobjacademicprogress->academicprogressCoursesInsert($lintId,$academicprogressID,$landscapeID,$programID,$studentdetails,$userId);
		// get the registeredCourses for studentID and set thier isregistered to '1'.
		// hide $this->lobjacademicprogress->fetchCoursesRegister($lintidreg);
		// INSERT PROCESS FOR ACADEMIC PROGRESS ENDS



		// finally, fetch the records to display based on landscape type whether it is level, block or semester.

		$this->view->studentdetails = $this->lobjacademicprogress->fetchStudentDetails($lintId);

		$this->view->minCreditGrad = $this->lobjacademicprogress->fetchTotalHours($landscapeID);
		$this->view->totalCreditTaken = $this->lobjacademicprogress->fetchTotalCreditHours($lintId,'pass','fail');
		$this->view->totalCreditEarn = $this->lobjacademicprogress->fetchTotalCreditHours($lintId,'pass','CT');
		$this->view->totalCreditToComplete = $this->lobjacademicprogress->fetchTotalCredit($lintId,'0');
		$this->view->totalCGPA = $this->lobjacademicprogress->fetchTotalCGPA($lintId);

			$semsters = $semstersunreg = array();
			$larrresult = $this->lobjacademicprogress->fetchRegisteredAcademicProgressSemester($lintId);
			$semsters = $returnCompDetail = array();
			$i = '0';
			foreach ($larrresult as $item) {
				$key = $item['codeSem'];
				$subjectID = $item['IdSubject'];
				if($subjectID!='') {
					$returnCompDetail = $this->lobjacademicprogress->fnfetchEquivalentSubjects($subjectID);
				}
				if (!isset($semsters[$key])) {
					$semsters[$key][] = $item;
					if(count($returnCompDetail)>0 && $subjectID!='') {
						$semsters[$key][$i]['equicourse'] = $returnCompDetail;
					}

				} else {
					$semsters[$key][] = $item;
					if(count($returnCompDetail)>0 && $subjectID!='') {
						$semsters[$key][$i]['equicourse'] = $returnCompDetail;
					}
				}
				$i++;
			}
			$this->view->larrsemesterlist = $semsters;

			$this->view->html =  $this->view->render('/fulltranscript/semesteracademicprogress.phtml');

			$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
			$filePath = $_SERVER['DOCUMENT_ROOT'].$baseUrl."/tcpdf/";
			require_once($filePath.'config/lang/eng.php');
			require_once($filePath.'tcpdf.php');

			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('AUCMS');
			$pdf->SetTitle('AUCMS - Academic Transcript');
			$pdf->SetSubject('AUCMS - Academic Transcript');
			$pdf->SetMargins(18,15,18,FALSE);
			$pdf->SetFont('times', '', 8, '', 'false');
			// set default header data
			$pdf->SetHeaderData('aucms-logo.jpg', PDF_HEADER_LOGO_WIDTH,'ACADEMIC TRANSCRIPT', array(0,64,255), array(0,64,128));
			$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));
			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			//set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			//set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			// add a new page
			$pdf->AddPage();
			//whole TCPDF's settings goes here
			//$html = $this->view->render('/partialtranscript/transcript.phtml');
			$htmlContents = $this->view->render('/fulltranscript/transcript.phtml');
			//echo $htmlContents;die;
			//$htmlContents;
			// output the HTML content
			$pdf->writeHTML($htmlContents, true, 0, true, 0);
			$pdf->Ln();
			$pdf->lastPage();
			$pdfName = 'Academic Transcript.pdf';
			ob_clean();
			$pdf->Output($pdfName, 'D');
		}
	}
}