<?php
class examination_ExaminationhistoryController extends Base_Base { //Controller for the User Module

	private $lobjmarksHistoryModel;

	public function init() { //initialization function
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjmarksHistoryModel = new Examination_Model_DbTable_Markshistory();
	}

	public function indexAction() {
		$history = $this->lobjmarksHistoryModel->getAllHistory();
		$data = array();
		if(!empty($history)){
			$i = 0;
			$j = 0;
			foreach($history as $list){
				if($i < 1){
					$detailtemp = array();
					$temp = array();
					$temp['no'] = $j;
					$temp['schemename'] = $list['schemename'];
					$temp['facultyname'] = $list['facultyname'];
					$temp['ProgramName'] = $list['ProgramName'];
					$temp['coursename'] = $list['coursename'];
					$temp['semester'] = $list['semester'];
					$detailtemp['OldStatus'] = $list['oldstatus'];
					$detailtemp['NewStatus'] = $list['newstatus'];
					$detailtemp['UpdatedOn'] = $list['UpdatedOn'];
					$detailtemp['UpdatedBy'] = $list['user'];
					$detailtemp['Activity'] = $list['Activity'];
					$temp['detail'][] = $detailtemp;
					$data[$j] = $temp;
				}else{
					if($history[$i-1]['IdMarksDistributionMaster'] == $list['IdMarksDistributionMaster']){
						$detailtemp = array();
						$detailtemp['OldStatus'] = $list['oldstatus'];
						$detailtemp['NewStatus'] = $list['newstatus'];
						$detailtemp['UpdatedOn'] = $list['UpdatedOn'];
						$detailtemp['UpdatedBy'] = $list['user'];
						$detailtemp['Activity'] = $list['Activity'];
						$data[$j]['detail'][] = $detailtemp;
					}else{
						$detailtemp = array();
						$temp = array();
						$j++;
						$temp['no'] = $j;
						$temp['schemename'] = $list['schemename'];
						$temp['facultyname'] = $list['facultyname'];
						$temp['ProgramName'] = $list['ProgramName'];
						$temp['coursename'] = $list['coursename'];
						$temp['semester'] = $list['semester'];
						$detailtemp['OldStatus'] = $list['oldstatus'];
						$detailtemp['NewStatus'] = $list['newstatus'];
						$detailtemp['UpdatedOn'] = $list['UpdatedOn'];
						$detailtemp['UpdatedBy'] = $list['user'];
						$detailtemp['Activity'] = $list['Activity'];
						$temp['detail'][] = $detailtemp;
						$data[$j] = $temp;
					}
				}
				$i++;
			}
			$this->view->historyArray = $data;
		}

	}




}