<?php 
class Examination_ExamSlipController extends Zend_Controller_Action {
	
	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate("Statement of Eligibility");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		//semester list
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemesterList();
		$this->view->semesterList = $semesterList;
			
		
		$IdSemester = $this->_getParam('IdSemester',null);
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$IdSemester = $formData['IdSemester'];
			$this->view->idSemester = $IdSemester;

			$defDB = new App_Model_General_DbTable_Definationms();
			$assessmentList = $defDB->getDataByType(151);

			$semesterList = $semesterDB->SearchSemester(array('IdSemester'=>$IdSemester));

			//get release status
			$examSlipDb = new Examination_Model_DbTable_ExamSlipRelease();

			foreach ($semesterList as $index=>$semester){
				$ass_type = $assessmentList;

				foreach ($ass_type as $index2 => $assessment){
					$release_data = $examSlipDb->getReleaseData($semester['key'],$assessment['idDefinition']);

					$ass_type[$index2]['esr_id'] = $release_data['esr_id'];
					$ass_type[$index2]['esr_date'] = $release_data['esr_date'];
					$ass_type[$index2]['esr_time'] = $release_data['esr_time'];
				}

				$semesterList[$index]['assessment_type'] = $ass_type;
			}

			//var_dump($semesterList[0]['assessment_type'][0]);
			//var_dump($semesterList[0]['assessment_type'][1]);
			$this->view->exam_release = $semesterList;
		}
	}
	
	
	public function examSlipReleaseAction(){
		
		$auth = Zend_Auth::getInstance();    	   
		
				
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$examSlipDb = new Examination_Model_DbTable_ExamSlipRelease();
			
			$release_data = $examSlipDb->getReleaseData($formData['esr_semester_id'],$formData['esr_assessment_type_id']);
			
			if($release_data){
				$data = array(
						'esr_date' => date('Y-m-d',strtotime($formData['esr_date'])),
						'esr_time' => $formData['esr_time'],
						'esr_last_edit_date' => date("Y-m-d H:i:s"),
						'esr_last_edit_by'=>$auth->getIdentity()->id
				);
				
				$examSlipDb->update($data, 'esr_id = '.$release_data['esr_id']);
				
			}else{
				$data = array(
						'esr_semester_id' => $formData['esr_semester_id'],
						'esr_assessment_type_id' => $formData['esr_assessment_type_id'],
						'esr_date' => date('Y-m-d',strtotime($formData['esr_date'])),
						'esr_time' => $formData['esr_time'],
						'esr_last_edit_date' => date("Y-m-d H:i:s"),
						'esr_last_edit_by'=>$auth->getIdentity()->id
				);
				$esr_id = $examSlipDb->insert($data);

				$ecList = $examSlipDb->getExamCenterSoe($formData['esr_semester_id']);

				if ($ecList){
					foreach ($ecList as $ecLoop){
						$ecData = array(
							'rec_esrid'=>$esr_id,
							'rec_ecid'=>$ecLoop['ec_id'],
							'rec_status'=>1,
							'rec_upddate'=>date('Y-m-d H:i:s'),
							'rec_updby'=>$auth->getIdentity()->id
						);
						$rec_id = $examSlipDb->examCenterSlipRelease($ecData);

						$courseList = $examSlipDb->getCourse($formData['esr_semester_id']);

						if ($courseList){
							foreach ($courseList as $courseLoop){
								$courseData = array(
									'rcr_recid'=>$rec_id,
									'rcr_subjectid'=>$courseLoop['er_idSubject'],
									'rcr_status'=>1,
									'rcr_upddate'=>date('Y-m-d H:i:s'),
									'rcr_updby'=>$auth->getIdentity()->id
								);
								$examSlipDb->courseSlipRelease($courseData);
							}
						}
					}
				}
			}
			
		}
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'exam-slip', 'action'=>'index','IdSemester'=>$formData['esr_semester_id']),'default',true));
		
	}
	
	public function viewAction(){
		
		$this->view->title = $this->view->translate("Statement of Eligibility");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
									
		$form = new Examination_Form_SearchStudentSemester();
		
		$paginator_ses = new Zend_Session_Namespace('paginator_ses');

		$publish_soe = false;

		if ($this->getRequest()->isPost()) {
				
			if( $this->_request->getPost ( 'Search' ) ) {
					if($form->isValid($this->getRequest()->getPost())){
						
						$formData = $this->getRequest()->getPost();
						$form->populate($formData);

						$examSlipDB = new Examination_Model_DbTable_ExamSlipRelease();
						$publish_soe = $examSlipDB->getReleaseSoe($formData['semester'], $formData['exam_type']);
						
						//print_r($formData);
						$this->view->exam_type = $formData['exam_type'];
							
						$paginator_ses->form_data = $formData;
						$this->view->formData = $formData;
						$student_list = $this->getStudentSemesterList($formData);
						
						$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
						$paginator->setItemCountPerPage(50);
						$paginator->setCurrentPageNumber($this->_getParam('page',1));
						$this->view->paginator = $paginator;
					}else{
						$formData = $this->getRequest()->getPost();
						$form->populate($formData);
					}
			}		
			
		}else{
			if( isset($paginator_ses->form_data) && $this->_getParam('page',null)!=null ){
				
				$formData = $paginator_ses->form_data;
				$form->populate($formData);

				$examSlipDB = new Examination_Model_DbTable_ExamSlipRelease();
				$publish_soe = $examSlipDB->getReleaseSoe($formData['semester'], $formData['exam_type']);
				
				$this->view->exam_type = $formData['exam_type'];
				$this->view->formData = $formData;
				$student_list = $this->getStudentSemesterList($formData);
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage(50);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				$this->view->paginator = $paginator;
			}
		}
		
		$this->view->publish_soe = $publish_soe;
		$this->view->form = $form;
	}
	
	
	public function viewSlipAction(){
		
		$this->_helper->layout->setLayout('preview');
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');	
	
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();

			//get student info
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$student_info = $studentRegistrationDB->fetchStudentHistoryDetails($formData['sid']);
			$this->view->student = $student_info;
						
			// semester
			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDB->fnGetSemestermaster($formData['semid']);
			$this->view->semester = $semester;
			
			// get course registed in semester selected
			$courseRegisterDb = new Registration_Model_DbTable_Studentregistration();
			$courses = $courseRegisterDb->getCourseRegisteredSOE($formData['sid'],$formData['semid'],$formData['ass_type'],$student_info);			
	
			//program
			$programDb = new App_Model_Record_DbTable_Program();
			$program = $programDb->getData($student_info['IdProgram']);
		
			//program
			$programDb = new App_Model_Record_DbTable_Program();
			$program = $programDb->getData($student_info['IdProgram']);
						
			//get subject register for exam
			$examRegDb = new Examination_Model_DbTable_ExamRegistration();			
			$address_list = $examRegDb->getExamCenterbyStudent($formData['semid'],$formData['sid']);
			
			$table_add = $this->address($address_list);			
			$this->view->address = $table_add;

			$examSlipDB = new Examination_Model_DbTable_ExamSlipRelease();
			$publish_soe = $examSlipDB->getReleaseSoe($semester['IdSemesterMaster'], $formData['ass_type']);

			if ($courses){
				foreach ($courses as $key => $course){
					if ($course['er_ec_id'] != null) {
						$checkEc = $examSlipDB->checkExamCenterActive($publish_soe['esr_id'], $course['er_ec_id']);

						if ($checkEc){
							$checkCourse = $examSlipDB->checkCourseActive($checkEc['rec_id'], $course['IdSubject']);
							//var_dump($checkCourse);
							if (!$checkCourse){
								unset($courses[$key]);
							}
						}else{
							unset($courses[$key]);
						}
					}else{
						unset($courses[$key]);
					}
				}
			}

			$table = $this->schedule($courses,$formData);
			$this->view->schedule = $table;
			
				
		}
	}
	
	public function printExamSlipAction(){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			//echo '<pre>';
			//print_r($formData);
			//exit;
			
			//get student info
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$student_info = $studentRegistrationDB->fetchStudentHistoryDetails($formData['sid']);
				
		
			// semester
			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDB->fnGetSemestermaster($formData['semid']);
			
			// get course registed in semester selected
			$courseRegisterDb = new Registration_Model_DbTable_Studentregistration();
			$courses = $courseRegisterDb->getCourseRegisteredSOE($formData['sid'],$formData['semid'],$formData['ass_type']);			
	
			
			//program
			$programDb = new App_Model_Record_DbTable_Program();
			$program = $programDb->getData($student_info['IdProgram']);
						
			//get subject register for exam
			$examRegDb = new Examination_Model_DbTable_ExamRegistration();			
			$address_list = $examRegDb->getExamCenterbyStudent($formData['semid'],$formData['sid']);
			
			$table_add = $this->address($address_list);

			$examSlipDB = new Examination_Model_DbTable_ExamSlipRelease();
			$publish_soe = $examSlipDB->getReleaseSoe($semester['IdSemesterMaster'], $formData['ass_type']);

			if ($courses){
				foreach ($courses as $key => $course){
					if ($course['er_ec_id'] != null) {
						$checkEc = $examSlipDB->checkExamCenterActive($publish_soe['esr_id'], $course['er_ec_id']);

						if ($checkEc){
							$checkCourse = $examSlipDB->checkCourseActive($checkEc['rec_id'], $course['IdSubject']);
							//var_dump($checkCourse);
							if (!$checkCourse){
								unset($courses[$key]);
							}
						}else{
							unset($courses[$key]);
						}
					}else{
						unset($courses[$key]);
					}
				}
			}
			
			$table = $this->schedule($courses,$formData);
			
			
			$fieldValues = array(
			    	 			 '$[STUDENT_NAME]'=>strtoupper( $student_info["appl_fname"].' '.$student_info["appl_lname"]) , 
								 '$[STUDENT_ID]'=> strtoupper( $student_info["registrationId"] ) ,
								 '$[STUDENT_IDNO]'=> strtoupper( $student_info["appl_idnumber"])  ,
								 '$[PROGRAMME]'=> strtoupper( $program["ProgramName"] ) , 
								 '$[SEMESTER]'=> strtoupper($semester["SemesterMainName"] ) ,
								 '$[SCHEDULE]'=>  $table ,
			 					 '$[ADDRESS]'=>  $table_add  ,
								 '$[LOGO]'=> "images/logo_text.png"  
		    	  				);
				
		    	  				
			require_once 'dompdf_config.inc.php';
		
			$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
			$autoloader->pushAutoloader('DOMPDF_autoload');
			
			//template path	 
			$html_template_path = DOCUMENT_PATH."/template/FinalExamSlip.html";
			
			$html = file_get_contents($html_template_path);			
	    		
			//replace variable
			foreach ($fieldValues as $key=>$value){
				$html = str_replace($key,$value,$html);	
			}
				
			
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->set_paper('a4', 'potrait');
			$dompdf->render();

			//output filename 
			$output_filename = $student_info["registrationId"]."-FinalExamSlip.pdf";
					
			//$dompdf = $dompdf->output();
			$dompdf->stream($output_filename);						
			
			//to rename output file						
		    $output_file_path = DOCUMENT_PATH.$student_info['sp_repository'].'/'.$output_filename;
			
			file_put_contents($output_file_path, $dompdf);
			
			$this->view->file_path = $output_file_path;
		
		}
		
		exit;
	}
	
	
	private function getStudentSemesterList($post=null){
		
		$session = new Zend_Session_Namespace('sis');
		$auth = Zend_Auth::getInstance();
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))
									->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration = sa.IdStudentRegistration' )
									->join(array('p'=>'student_profile'),'p.id = sa.sp_id',array('appl_fname','appl_lname'))
									->join(array('defination' => 'tbl_definationms'), 'defination.idDefinition=sa.profileStatus', array('profileStatus'=>'DefinitionCode')) //Application STtsu
									->join(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('prg.ArabicName','ProgramName','ProgramCode'))	
									->where('sss.IdSemesterMain = ?',$post['semester'])
									->where('sss.studentsemesterstatus = ?',130)
									->group('sa.IdStudentRegistration')
									->order("p.appl_fname")
									->order("p.appl_lname");
		
		if(isset($post['exam_type'])){
			
			$lstrSelect->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sa.IdStudentRegistration AND srs.IdSemesterMain=sss.IdSemesterMain')->where('srs.Active !=?',3);
			
			if($post['exam_type']==771){
				$lstrSelect->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject')->where('sm.CourseType!=20'); //FE
			}
			
			if($post['exam_type']==772){
				$lstrSelect->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject')->where('sm.CourseType=20'); //CE
			}
			
		}
		
		if($session->IdRole == 311 || $session->IdRole == 298){
			$lstrSelect->where("prg.IdCollege =?",$session->idCollege);
		}else{
				
			if(isset($post['IdCollege']) && !empty($post['IdCollege'])){
				$lstrSelect->where("prg.IdCollege =?",$post["IdCollege"]);
			}
		}
		 	
		
		if (isset($post['applicant_name']) && !empty($post['applicant_name'])) {
			 
			$lstrSelect->where("(p.appl_fname LIKE '%". $post['applicant_name']."%'");			
			$lstrSelect->orwhere("p.appl_lname LIKE '%". $post['applicant_name']."%')");
		}
		
				
		if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {
			$lstrSelect->where("sa.IdProgram = ?",$post['IdProgram']);
		}		
			
		
		if (isset($post['student_id']) && !empty($post['student_id'])) {
			$lstrSelect->where("sa.registrationId = ?",$post['student_id']);
		}
		
		//echo $lstrSelect; exit;
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
	
	function schedule($courses,$formData){
		
		$table = '<table width="100%" border=1 cellpadding="5px" cellspacing="0" class="infotable4">
						   <thead>
							<tr>
								<th width="5px">No.</th>
								<th width="25%">Paper</th>
								<th width="30%">Exam Center</th>
								<th width="20%">Date</th>
								<th width="25%">Time</th>
							</tr>
							</thead>';
					
				
				if(count($courses)>0){
					$table .= '<tbody>';
					$no = 0;
					foreach( $courses as $index=>$subject){
						$no++;
							
						if($subject['ecs_publish']==1) {
							
							
							if($subject['ec_name']!=''){
								
								$ec_name =  ($subject['ec_name']!='') ? $subject['ec_name']:'To be confirmed'; 
								
								$es_date =        (isset($subject['schedule_date']) && $subject['schedule_date']!='') ? date('j F Y',strtotime($subject['schedule_date'])):'';
								$es_start_time =  (isset($subject['schedule_start_time'])) ? date('h:i a',strtotime($subject['schedule_start_time'])):'';
								$es_end_time   =  (isset($subject['schedule_end_time'])) ? date('h:i a',strtotime($subject['schedule_end_time'])):'';
								
								if($es_start_time!=''){
									$es_time = $es_start_time.' to '.$es_end_time;
								}else{
									$es_time = '';
								}
								
							}else{
								
								$ec_name = 'To be confirmed';
								$es_date = '';
								$es_time = '';
							}
							
						}else{
							$ec_name = 'To be confirmed';
							$es_date = '';
							$es_time = '';
						}
						
						$table .= '<tr>';
							$table .= '<td align="center">'.$no.'</td>';
							$table .= '<td align="center">'.$subject["SubCode"].' <br> '.$subject["SubjectName"].'</td>';
							$table .= '<td align="center">'.$ec_name.'&nbsp;</td>';
							$table .= '<td align="center">'.$es_date.'&nbsp;</td>';
							$table .= '<td align="center">'.wordwrap($es_time).'&nbsp;</td>';							
						$table .= '</tr>';
					}
					$table .= '</tbody>';
				}//end count
				$table .= '</table>';
				
				return $table;
				
	}
	
	function address($address_list){
		
		$table_add= '';
			if(count($address_list)>0){
				$table_add = '<table class="tbl_address">';
				foreach($address_list as $add){		

					
					$city = ($add['add_city']!=99) ? $add['CityName']:$add['add_city_others'];
					$state = ($add['add_state']!=99) ? $add['StateName']:$add['add_state_others'];
		
								
					$table_add .= '<tr><td>';
						$table_add .= '<span class="title_add">'.strtoupper($add['ec_name']).'</span><br>';
						$table_add .= strtoupper($add['add_address1']).'<br>';
						$table_add .= strtoupper($add['add_address2']).'<br>';
						$table_add .= strtoupper($add['add_zipcode']).' '.strtoupper($city).'<br>';
						$table_add .= strtoupper($state).'<br>';
						$table_add .= strtoupper($add['CountryName']).'<br>';
					$table_add .= '</td></tr>';
					$table_add .= '<tr><td>&nbsp;</td></tr>';
				}
				$table_add .= '</table>';
			}
			return $table_add;
			
	}

	public function examCenterSetupAction(){
		$this->view->title = $this->view->translate('Statement of Eligibility (Exam Center Setup)');

		$id = $this->_getParam('id', 0);

		$model = new Examination_Model_DbTable_ExamSlipRelease();

		if ($id != 0){
			$info = $model->slipReleaseInfo($id);
			$ecList = $model->getExamCenterSlipRelease($id, $info['esr_semester_id']);
			//var_dump($info);
			$this->view->info = $info;
			$this->view->ecList = $ecList;
		}else{
			$this->_redirect($this->baseUrl . '/examination/exam-slip/index/');
		}

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$idarr = array(0);
			if (isset($formData['check']) && count($formData['check']) > 0){
				foreach ($formData['check'] as $key => $value){
					array_push($idarr, $key);
				}
			}

			$model->updateStatusEc($idarr, $id);

			//redirect here
			$this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
			$this->_redirect($this->baseUrl . '/examination/exam-slip/exam-center-setup/id/'.$id);
		}
	}

	public function addExamCenterAction(){
		$this->view->title = $this->view->translate('Add Exam Center');

		$id = $this->_getParam('id', 0);
		$model = new Examination_Model_DbTable_ExamSlipRelease();
		$auth = Zend_Auth::getInstance();

		if ($id != 0){
			$info = $model->slipReleaseInfo($id);
			$examCenter = $model->getExamCenterAdd($id);

			$this->view->info = $info;
			$this->view->examCenter = $examCenter;
		}else{
			$this->_redirect($this->baseUrl . '/examination/exam-slip/index/');
		}

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$ecData = array(
				'rec_esrid'=>$id,
				'rec_ecid'=>$formData['exam_center'],
				'rec_status'=>1,
				'rec_upddate'=>date('Y-m-d H:i:s'),
				'rec_updby'=>$auth->getIdentity()->id
			);
			$rec_id = $model->examCenterSlipRelease($ecData);

			$courseList = $model->getCourse($info['esr_semester_id']);

			if ($courseList){
				foreach ($courseList as $courseLoop){
					$courseData = array(
						'rcr_recid'=>$rec_id,
						'rcr_subjectid'=>$courseLoop['er_idSubject'],
						'rcr_status'=>1,
						'rcr_upddate'=>date('Y-m-d H:i:s'),
						'rcr_updby'=>$auth->getIdentity()->id
					);
					$model->courseSlipRelease($courseData);
				}
			}

			//redirect here
			$this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
			$this->_redirect($this->baseUrl . '/examination/exam-slip/exam-center-setup/id/'.$id);
		}
	}

	public function courseSetupAction(){
		$this->view->title = $this->view->translate('Statement of Eligibility (Course Setup)');

		$id = $this->_getParam('id', 0);

		$model = new Examination_Model_DbTable_ExamSlipRelease();

		if ($id != 0){
			$ecInfo = $model->examCenterInfo($id);
			$info = $model->slipReleaseInfo($ecInfo['rec_esrid']);
			$ecList = $model->getCourseSlipRelease($id);
			//var_dump($ecInfo);
			$this->view->ecInfo = $ecInfo;
			$this->view->info = $info;
			$this->view->ecList = $ecList;
		}else{
			//redirect here
			//$this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
			$this->_redirect($this->baseUrl . '/examination/exam-slip/index/');
		}

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			//var_dump($formData); exit;

			$idarr = array(0);
			if (isset($formData['check']) && count($formData['check']) > 0){
				foreach ($formData['check'] as $key => $value){
					array_push($idarr, $key);
				}
			}

			$model->updateStatusCourse($idarr, $id);

			//redirect here
			$this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
			$this->_redirect($this->baseUrl . '/examination/exam-slip/course-setup/id/'.$id);
		}
	}

	public function addCourseAction(){
		$this->view->title = $this->view->translate('Add Course');

		$id = $this->_getParam('id', 0);
		$model = new Examination_Model_DbTable_ExamSlipRelease();
		$auth = Zend_Auth::getInstance();

		if ($id != 0){
			$ecInfo = $model->examCenterInfo($id);
			$info = $model->slipReleaseInfo($ecInfo['rec_esrid']);
			$course = $model->getSubjectAdd($id);
			//var_dump($course);
			$this->view->ecInfo = $ecInfo;
			$this->view->info = $info;
			$this->view->course = $course;
		}else{
			$this->_redirect($this->baseUrl . '/examination/exam-slip/index/');
		}

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$courseData = array(
				'rcr_recid' => $id,
				'rcr_subjectid' => $formData['course'],
				'rcr_status' => 1,
				'rcr_upddate' => date('Y-m-d H:i:s'),
				'rcr_updby' => $auth->getIdentity()->id
			);
			$model->courseSlipRelease($courseData);

			//redirect here
			$this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
			$this->_redirect($this->baseUrl . '/examination/exam-slip/course-setup/id/'.$id);
		}
	}

	public function insertSetupDataAction(){
		//set unlimited
		set_time_limit(0);
		ini_set('memory_limit', '-1');

		$model = new Examination_Model_DbTable_ExamSlipRelease();
		$list = $model->getExamSlipRelease();

		$auth = Zend_Auth::getInstance();

		if ($list){
			foreach ($list as $loop){
				$ecList = $model->getExamCenterSoe($loop['esr_semester_id']);

				if ($ecList){
					foreach ($ecList as $ecLoop){
						$checkEc = $model->checkExamCenter($loop['esr_id'], $ecLoop['ec_id']);

						if (!$checkEc) {
							$ecData = array(
								'rec_esrid' => $loop['esr_id'],
								'rec_ecid' => $ecLoop['ec_id'],
								'rec_status' => 1,
								'rec_upddate' => date('Y-m-d H:i:s'),
								'rec_updby' => $auth->getIdentity()->id
							);
							$rec_id = $model->examCenterSlipRelease($ecData);
						}else{
							$rec_id = $checkEc['rec_id'];
						}

						$courseList = $model->getCourse($loop['esr_semester_id']);

						if ($courseList){
							foreach ($courseList as $courseLoop){
								$checkCourse = $model->checkCourse($rec_id, $courseLoop['er_idSubject']);

								if (!$checkCourse) {
									$courseData = array(
										'rcr_recid' => $rec_id,
										'rcr_subjectid' => $courseLoop['er_idSubject'],
										'rcr_status' => 1,
										'rcr_upddate' => date('Y-m-d H:i:s'),
										'rcr_updby' => $auth->getIdentity()->id
									);
									$model->courseSlipRelease($courseData);
								}
							}
						}
					}
				}
			}
		}

		exit;
	}

	public function getIncompleteTerAction(){
		$form = new Examination_Form_SearchStudentSemester();
		$this->view->form = $form;
	}

	public function downloadIncompleteTerAction(){
		//set unlimited
		set_time_limit(0);
		ini_set('memory_limit', '-1');

		$this->auth = Zend_Auth::getInstance();
		$this->_helper->layout->disableLayout();
		$this->view->role = $this->auth->getIdentity()->IdRole;

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			//dd($formData); exit;

			$student_list = $this->getStudentSemesterList($formData);

			//dd($student_list); exit;

			if ($student_list) {
				foreach ($student_list as $key => $student_loop) {
					$model = new Examination_Model_DbTable_TerStatus();
					$semeser['IdSemesterMaster'] = $formData['semester'];
					$terstatus = $model->getTerCompleteStatus($student_loop, $semeser);

					if ($terstatus == 0){
						unset($student_list[$key]);
					}else{
						$student_list[$key]['terstatus']=$terstatus;

						$db = Zend_Db_Table::getDefaultAdapter();

						$selectExam = $db->select()
							->from(array('a'=>'exam_registration'))
							->joinLeft(array('b'=>'tbl_exam_center'), 'a.er_ec_id = b.ec_id', array('b.ec_name'))
							->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.er_idSubject = c.IdSubject', array('c.SubjectName', 'c.SubCode'))
							->joinLeft(array('e'=>'tbl_definationms'), 'a.er_attendance_status = e.idDefinition', array('attStatus'=>'e.DefinitionDesc'))
							->where('a.er_idStudentregistration = ?', $student_loop['IdStudentRegistration'])
							->where('a.er_idSemester = ?', $formData['semester'])
							->where('a.er_attendance_status = ?', 395);

						$examList = $db->fetchAll($selectExam);

						if ($examList) {
							$student_list[$key]['examlist'] = $examList;
						}else{
							unset($student_list[$key]);
						}
					}
				}
			}
			//dd($student_list);
			//exit;
			$this->view->student_list = $student_list;
		}

		$this->view->filename = date('Ymd').'_ternotcomplete.xls';
	}
}
?>