<?php
class Examination_SubjectregistrationController extends Base_Base { //Controller for the User Module

	private $lobjSubjectregistrationForm;
	private $lobjSubjectregistration;
	private $lobjSubjectmaster;
	private $_gobjlog;
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjSubjectregistrationForm = new Examination_Form_Subjectregistration();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjSubjectregistration = new Examination_Model_DbTable_Subjectregistration();
		$this->lobjCommonDefinition=new App_Model_Definitiontype();
	}

	public function indexAction() { // action for search and view

		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$this->view->lobjSubjectregistrationForm = $this->lobjSubjectregistrationForm;


		$lobjProgramNameList = $this->lobjAcademicstatus->fnGetProgramNameList();
		$lobjform->field5->addMultiOptions($lobjProgramNameList);

		$lobjSemesterNameList = $this->lobjAcademicstatus->fnGetSemesterNameList();
		$lobjform->field8->addMultiOptions($lobjSemesterNameList);

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Subjectregistrationpaginatorresult);
		$larrresult = $this->lobjSubjectregistration->fngetProgramDetails(); //get user details

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Subjectregistrationpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Subjectregistrationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {

				$larrresult = $this->lobjSubjectregistration->fnSearchProgramDetails( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Subjectregistrationpaginatorresult = $larrresult;

			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/subjectregistration/index');
		}
	}

	public function newsubjectregistrationAction() { //Action for creating the new user

		$this->view->lobjSubjectregistrationForm = $this->lobjSubjectregistrationForm;

		$idProgram = ( int ) $this->_getParam ( 'id' );
		$ProgramName=$this->_getParam ( 'name' );
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjSubjectregistrationForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjSubjectregistrationForm->IdProgram->setValue ( $idProgram );
		$this->view->pgmid = $idProgram;
		$this->view->Progname = $ProgramName;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjSubjectregistrationForm->UpdUser->setValue( $auth->getIdentity()->iduser);

		$larrtermstatuslist=$this->lobjCommonDefinition->fnGetDefinationMs("Detain Action");
		$this->lobjSubjectregistrationForm->TerminateStatus->addMultiOptions($larrtermstatuslist);

		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		$larrresult = $this->lobjSubjectregistration->fnViewSubjectregistrationlist($idProgram);
		//echo "<pre>";print_r($larrresult);die();
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);

		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();
			if ($this->lobjSubjectregistrationForm->isValid($larrFormData)) {
				unset($larrFormData['Save']);
				$this->lobjSubjectregistration->fnDeleteSubjectregistrationpolicy($idProgram);
				$lastinsertId=$this->lobjSubjectregistration->fnaddSubjectregistrationpolicy($larrFormData);


				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Subject Registration Policy Add Id=' . $idProgram,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/subjectregistration/index');
			}

		}




	}


}