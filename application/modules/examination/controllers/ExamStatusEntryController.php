<?php

class examination_ExamStatusEntryController extends Base_Base { //Controller for the User Module

	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function groupListAction() {
		
		//title
		$this->view->title= $this->view->translate("Exam Status Entry - Section List");
		 
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_SectionSearchForm(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idCollege = $formData["IdCollege"];
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
												
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}//if post	
	}
	
	
	
	public function studentListAction(){
		
			//title
		$this->view->title= $this->view->translate("Exam Status Entry - Student List");
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    
		
		$id = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idProgram = $this->_getParam('idProgram', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$student = $this->_getParam('student',null);
	
		
		$this->view->idGroup = $id;
		$this->view->idSemester = $idSemester;
		$this->view->idProgram = $idProgram;
		$this->view->idSubject = $idSubject;
				
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
		
	
		//get course group
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($id);				
		$this->view->group = $group;
		
		//student list
		$courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		$studentList = $courseGroupStudentDb->getStudentbyGroup($id,$student);
		
			
	  	if(count($studentList)>0){
			foreach($studentList as $index=>$student){
				
				//get subject registration info
				$subjectRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
				$subject  =  $subjectRegDB->getSemesterSubjectStatus($idSemester,$student["IdStudentRegistration"],$idSubject);
				$studentList[$index]["IdStudentRegSubjects"] = $subject["IdStudentRegSubjects"];
				$studentList[$index]["student_mark"] = $subject["final_course_mark"];
				$studentList[$index]["grade_point"] = $subject["grade_point"];
				$studentList[$index]["grade_desc"] = $subject["grade_desc"];
				$studentList[$index]["grade_status"] = $subject["grade_status"];
				$studentList[$index]["exam_status"] = $subject["exam_status"];
				$studentList[$index]["approvedby"] = $subject["mark_approveby"];
				$studentList[$index]["approveddt"] = $subject["mark_approvedt"];				
			}
	  	}
		$this->view->student_list = $studentList;
	}
	
	
	public function saveStatusAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$this->_helper->layout()->disableLayout();
		
		$IdStudentRegSubjects = $this->_getParam('IdStudentRegSubjects', null);
		$exam_status = $this->_getParam('exam_status', null);
		
		
		if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
		
						
			$data = array('exam_status'=>$exam_status,'exam_status_updateby'=>$auth->getIdentity()->iduser,'exam_status_updatedt'=>date('Y-m-d H:i:s'));
			
			
			$studentRegSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
			$studentRegSubjectDB->updateData($data,$IdStudentRegSubjects);
			
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
		
			$json = Zend_Json::encode($exam_status);
		
			echo $json;
			exit();
		
		}
		
	}
	
	
}

?>