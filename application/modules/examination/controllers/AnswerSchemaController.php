<?php



class Examination_AnswerSchemaController extends Zend_Controller_Action {
	
	private $_DbObj;
	private $_config;
	
	public function init(){
		
		$sis_session = new Zend_Session_Namespace('sis');
		$configDb = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->_config = $configDb->fnGetInitialConfigDetails($sis_session->idUniversity);
		
		//$this->_DbObj = new Application_Model_DbTable_Placementtest();
	}
	
	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Answer Scheme - Search Course");
    	
    	$form = new Examination_Form_SearchAssessmentComponent();
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$progDB= new GeneralSetup_Model_DbTable_Program();
				$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
				$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
				$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				
				
				$i=0;
				$j=0;
				$allsemlandscape = null;
				$allblocklandscape = null;
				
			    $activeLandscape=$landscapeDB->getAllActiveLandscape($formData["IdProgram"]);
					
			        foreach($activeLandscape as $actl){
						if($actl["LandscapeType"]==43){
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}elseif($actl["LandscapeType"]==44){
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				
				if(is_array($allsemlandscape))
					$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,$formData);
					
				//echo count($subjectsem);
				
				if(is_array($allblocklandscape))
					$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,$formData);
					
				//echo count($subjectblock);
				
				if(is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=array_merge( $subjectsem , $subjectblock );
				}else{
					if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
						$subjects=$subjectsem;
					}
					elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
						$subjects=$subjectblock;
					}		
				}//if else
				
				
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($subjects));
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				
				$this->view->list_subject = $subjects;
				
				//print_r($subjects);
				
			}//if form valid
    	}//if post		
    }
    
    
	public function viewAction(){
    	
    	$this->view->title=$this->view->translate("Answer Scheme - Mark Distribution");
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
		
    	
    	//check kalo belum ada keluarkan button add
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$total = $markDistributionDB->checkExistEntry($idSemester,$idProgram,$idSubject);
    	$this->view->total = $total;
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	
    	//get component
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject);	    
    	$this->view->rs_component = $list_component;
    	//print_r($list_component);
    }
    
    
    
	public function detailAction(){
    	$this->view->title= $this->view->translate("Answer Schema - Detail");
    	
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
		$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->IdMarksDistributionDetails = $IdMarksDistributionDetails;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
		
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
		
    	
    	//get main component info
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$this->view->main_component = $markDistributionDB->getInfoComponent($IdMarksDistributionMaster);
    	//print_r($this->view->main_component);
    	
    	//get list component item    	
  	    $oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
  	    $this->view->component_item = $oCompitem->getDataComponentItem($IdMarksDistributionDetails);
  	    
  	    $form = new Examination_Form_AnswerSchemaUpload(array('idSemesterx'=>$idSemester,'idProgramx'=>$idProgram,'idSubjectx'=>$idSubject,'idMaster'=>$IdMarksDistributionMaster,'idDetail'=>$IdMarksDistributionDetails));
  	    $this->view->form = $form;
  	    
  	    $answerSchemeDB = new Examination_Model_DbTable_StudentAnswerScheme();
  	    $sets = $answerSchemeDB->getData($IdMarksDistributionMaster,$IdMarksDistributionDetails);
    	$this->view->sets = $sets;
    }
    
    
    public function uploadSchemaAction(){
    	
    	$this->view->title= $this->view->translate("Upload Answer Scheme");
    	
    	if ($this->_request->isPost()) {

    		$formData = $this->_request->getPost();
    		
    		$form = new Examination_Form_AnswerSchemaUpload();

            if ($form->isValid($formData)) {
            	
				$formData = $this->getRequest()->getPost();
				
				$this->view->idMaster = $formData["sas_IdMarksDistributionMaster"];	    	
		    	$this->view->idSemester = $formData["idSemester"];
		    	$this->view->idProgram = $formData["idProgram"];
		    	$this->view->idSubject = $formData["idSubject"];
				
				// success - do something with the uploaded file
                $uploadedData = $form->getValues();
                $fullFilePath = $form->file->getFileName();
                
                //Zend_Debug::dump($uploadedData, '$uploadedData');
               // Zend_Debug::dump($fullFilePath, '$fullFilePath');
                
	            $schemaFile = $fullFilePath;
				$lines = file($schemaFile);
				
				$file = fopen($fullFilePath, "r") or exit("Unable to open file!");
				
					$i=0;
				    $arr_sets = array();
           			 while(!feof($file)){
						$line_data = fgets($file);			
						//echo strlen($line_data)	;
						//echo '<br>';		
						$arr_sets[$i]["sas_IdMarksDistributionMaster"]=$formData["sas_IdMarksDistributionMaster"];
						$arr_sets[$i]["sas_IdMarksDistributionDetails"]=$formData["sas_IdMarksDistributionDetails"];
						$arr_sets[$i]["sas_set_code"]=str_replace(" ","",substr($line_data, 0,3));
						$arr_sets[$i]["sas_total_quest"]=$formData["sas_total_quest"];
						$arr_sets[$i]["sas_status"]=1;
						$arr_sets[$i]["answer_raw"]=str_replace(" ","",substr($line_data, 41,$formData["sas_total_quest"]));					
						$i++;
					}
				
				fclose($file);
				
				//print_r($arr_sets);
				
                //remove file
				unlink($fullFilePath);
				
				
				//pack to array
				$cont = 0;
				$data = array();				
				foreach ($arr_sets as $key=>$line){
					
					if( $line['sas_set_code'] != "" ){					
						
						$tempAns = array();
						$j = strlen($line['answer_raw']);
						
						for ($k = 0; $k < $j; $k++) {
							$char = substr($line['answer_raw'], $k, 1);
							// do stuff with $char
							$tempAns[$k] = $char;
						}			
								
						//$data[$cont]['sad_ques_ans'] =  $tempAns;
											
						//restructure array
						$arr_main[$cont]["sas_IdMarksDistributionMaster"]=$line['sas_IdMarksDistributionMaster'];
						$arr_main[$cont]["sas_IdMarksDistributionDetails"]=$line['sas_IdMarksDistributionDetails'];
						$arr_main[$cont]["sas_set_code"]=$line['sas_set_code'];
						$arr_main[$cont]["sas_total_quest"]=$line['sas_total_quest'];
						$arr_main[$cont]["sas_status"]=$line['sas_status'];
						$arr_main[$cont]["sas_answers"]=$tempAns;
						
					}else{						
						unset($arr_sets[$key]);
					}
					
					
				$cont++;
				}

				
				//print_r($arr_main);				
										
				$stack_failed = array();
				for($row=0; $row<count($arr_main); $row++){
					
					//check dah ada ke set code ini
					//set code x boleh sama dalam same Program,Semester and Subject but can be in diffferent program
					$answerSchemeDB = new Examination_Model_DbTable_StudentAnswerScheme();
					//$status = $answerSchemeDB->checkExist($formData["sas_IdMarksDistributionMaster"],$formData["sas_IdMarksDistributionDetails"],$arr_main[$row]["sas_set_code"]);
				    $status = $answerSchemeDB->checkDuplicate($formData["idSemester"],$formData["idProgram"],$formData["idSubject"],$arr_main[$row]["sas_set_code"]);
					
					
				    if($status!=1){
						$main["sas_IdMarksDistributionMaster"]  = $arr_main[$row]["sas_IdMarksDistributionMaster"];	
						$main["sas_IdMarksDistributionDetails"] = $arr_main[$row]["sas_IdMarksDistributionDetails"];
						$main["sas_set_code"]                   = $arr_main[$row]["sas_set_code"];
						$main["sas_total_quest"]                = $arr_main[$row]["sas_total_quest"];	
						$sas_id = $answerSchemeDB->addData($main);	
	
						$answers = $arr_main[$row]["sas_answers"];
						
						$quest_no =1;
						foreach($answers as  $value){
							
							$answer["sad_anscheme_id"]=$sas_id;
							$answer["sad_ques_no"]=$quest_no;
							$answer["sad_ques_ans"]=$value["sad_ques_ans"];
							
							$answerSchemeDetailDB = new Examination_Model_DbTable_StudentAnswerSchemeDetails();
							$answerSchemeDetailDB->addData($answer);
							
						$quest_no++;
						}
				    }else{//end add
				    	array_push($stack_failed,array('set_code'=>$arr_main[$row]["sas_set_code"],'comments'=>'Set Code already exist')); 								
						
				    }
				}
					
				if(count($stack_failed)>0){
					$this->view->noticeError = $this->view->translate("Failed Uploading Answer Scheme");
					$this->view->stack_failed = $stack_failed;
				}else{
					//redirect					
					$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'answer-schema', 'action'=>'detail','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idMaster'=>$formData["sas_IdMarksDistributionMaster"]),'default',true));
				}
            }
            
    	}
    	
    	
    }
    
    
    
    
	
	public function schemaDetailAction(){
    	$this->view->title= $this->view->translate("Answer Scheme - Schema Detail");
    	
		$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
		$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$sas_id = $this->_getParam('id');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->IdMarksDistributionDetails = $IdMarksDistributionDetails;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
		
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
		    	
    	//get main component info
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$this->view->main_component = $markDistributionDB->getInfoComponent($IdMarksDistributionMaster);
    	
    	$answerSchemeDetailDB = new Examination_Model_DbTable_StudentAnswerSchemeDetails();
        $this->view->schemaDetail = $answerSchemeDetailDB->getDetails($sas_id);
		
    }
    
	public function deleteAction($id = null){
		
    	$IdMarksDistributionMaster = $this->_getParam('idMaster',0);
    	$IdMarksDistributionDetails = $this->_getParam('idDetail',0);
		$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	$sas_id = $this->_getParam('id');
    	
    	if($id>0){
    		
    		$answerSchemeDB = new Examination_Model_DbTable_StudentAnswerScheme();
    		$answerSchemeDetailDB = new Examination_Model_DbTable_StudentAnswerSchemeDetails();
    		$answerSchemeDB->deleteData($sas_id);
    		$answerSchemeDetailDB->deleteDetailsData($sas_id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'answer-schema', 'action'=>'detail','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'idMaster'=>$formData["sas_IdMarksDistributionMaster"],'idDetail'=>$IdMarksDistributionDetails),'default',true));
    	
    }
}

