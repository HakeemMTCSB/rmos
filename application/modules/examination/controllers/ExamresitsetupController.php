<?php
class examination_ExamresitsetupController extends Base_Base { //Controller for the User Module
	private $lobjexamresitsetupForm;
	private $lobjexamresitsetupModel;

	public function init() { //initialization function
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjexamresitsetupForm = new Examination_Form_Examresitsetup();
		$this->lobjexamresitsetupModel = new Examination_Model_DbTable_Examinationresitsetup();
	}

	public function indexAction() {
		$this->view->lobjexamresitsetupForm = $this->lobjexamresitsetupForm;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
		$IdUniversity = $this->gobjsessionsis->idUniversity;

		$DefmodelObj = new App_Model_Definitiontype();
		$raceList = $DefmodelObj->fnGetDefinationMs('Marks Appeal');

		foreach($raceList as $larrvalues) {
			$this->view->lobjexamresitsetupForm->FinalMarks->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		$this->view->lobjexamresitsetupForm->UpdatedOn->setValue($UpdDate);
		$this->view->lobjexamresitsetupForm->UpdatedBy->setValue($userId);
		$this->view->lobjexamresitsetupForm->IdUniversity->setValue($IdUniversity);

		//now if the record is already exist for this university
		$configarray = $this->lobjexamresitsetupModel->getresitconfig($IdUniversity);
		if(!empty($configarray)){
			$this->view->lobjexamresitsetupForm->populate($configarray);
			$this->view->configarray = $configarray;
		}


		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();
			unset($larrformData ['Save']);
			if(!empty($configarray)){
				$this->lobjexamresitsetupModel->updateresitconfig($larrformData,$configarray['IdExaminationResitConfiguration']);
			}else{
				$this->lobjexamresitsetupModel->Addresitconfig($larrformData);
			}
			$this->_redirect( $this->baseUrl . '/examination/examresitsetup');
		}


	}
}
?>
