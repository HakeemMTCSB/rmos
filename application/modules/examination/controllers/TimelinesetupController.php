<?php
class examination_TimelinesetupController extends Base_Base { //Controller for the User Module

	//private $lobjdeftype;
	private $_gobjlog;
	private $lobjExamCalendarForm;
	private $lobjdeftype;
	public $lobjform;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjexamassementtypeModel = new Examination_Model_DbTable_Assessmenttype();
		$this->lobjexamassementitemModel = new Examination_Model_DbTable_Assessmentitem();
		$this->lobjExamMArksEntrytimelineModel = new Examination_Model_DbTable_Examinationtimeline();
		$this->lobjExamCalendarForm = new Examination_Form_Timelinesetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
	}

	public function indexAction() { // action for search and view
		//Form
		$this->view->lobjform = $this->lobjform;
		
		$larrresult = $this->lobjexamassementtypeModel->fnSearchSemester( $post = NULL);
		
		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->ExamCalendarpaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		$this->view->lobjform->field14->setAttrib('required','false');
		$this->view->lobjform->field15->setAttrib('required','false');
		
		if(isset($this->gobjsessionsis->ExamCalendarpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->ExamCalendarpaginatorresult,$lintpage,$lintpagecount);
		}
		else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
			$larrformData = $this->_request->getPost ();
			$larrresult = $this->lobjexamassementtypeModel->fnSearchSemester($larrformData);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->ExamCalendarpaginatorresult = $larrresult;
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/timelinesetup/');
		}
	}
	
	public function addAction(){
		//Form
		$this->view->lobjExamCalendarForm = $this->lobjExamCalendarForm;
		//Get Id
		$id = $this->_getParam ( 'id' );
		$lstrsemesterdetail = $this->lobjexamassementtypeModel->fngetsemesterid($id);
		
		if($lstrsemesterdetail){
			$lstrsemestercode = $lstrsemesterdetail[0]['SemesterMainCode'];
			$this->view->lobjExamCalendarForm->Semester->setValue($lstrsemesterdetail[0]['SemesterMainCode']);
			$this->view->lobjExamCalendarForm->Semester->setAttrib('readonly','true');
			$this->view->lobjExamCalendarForm->SemesterStartDate->setValue($lstrsemesterdetail[0]['SemesterMainStartDate']);
			$this->view->lobjExamCalendarForm->SemesterStartDate->setAttrib('readonly','true');
			$this->view->lobjExamCalendarForm->SemesterEndDate->setValue($lstrsemesterdetail[0]['SemesterMainEndDate']);
			$this->view->lobjExamCalendarForm->SemesterEndDate->setAttrib('readonly','true');
		}
		
		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		//Render to View
		$this->view->lobjExamCalendarForm->IdUniversity->setvalue ($IdUniversity);
		$this->view->lobjExamCalendarForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjExamCalendarForm->UpdUser->setValue ($userId);
		
		if($lstrsemesterdetail){
			$larrresult = $this->lobjexamassementtypeModel->fngetExamcalendarId($lstrsemestercode);
			if($larrresult){
				$this->view->result = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();	
			$this->lobjexamassementtypeModel->fnAddExamCalendar($larrformData,$id);
			
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'add exam calendar' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			$this->_redirect( $this->baseUrl . '/examination/timelinesetup/');
		}
	}
}

?>
