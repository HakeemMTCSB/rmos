<?php



class Examination_ResitController extends Zend_Controller_Action {

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Student Resit List");
 
    	$form = new  Examination_Form_ResitSearchForm();
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {    		
    
			$formData = $this->getRequest()->getPost();		
			$form->populate($formData);
			
	    	$resitDB = new Examination_Model_DbTable_Resit();
	    	$appealList = $resitDB->getListApplication($formData);
	    	    	
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($appealList));
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
					
			$this->view->paginator = $paginator;
    	}
    }
    
}

?>