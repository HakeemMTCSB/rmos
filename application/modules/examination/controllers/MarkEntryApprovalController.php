<?php

class examination_MarkEntryApprovalController extends Base_Base { //Controller for the User Module

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Mark Entry Approval");
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MarkEntrySearchCourseGroup(array('locale'=>$locale));
    	$this->view->form = $form;
    
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();			
		
			$this->view->idCollege = $formData["IdCollege"];
			$this->view->idProgram = $formData["IdProgram"];
			$this->view->idSemester = $formData["IdSemester"];
			$this->view->idSubject = $formData["IdSubject"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			    //get group
				$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
				$groups = $courseGroupDb->getCourseGroupListByProgram($formData);
												
				$this->view->groups = $groups;

				//echo '<pre>';
				//print_r($groups);
								
			}//if form valid
    	}//if post	
    	
    }
    
    public function viewComponentAction(){
    	
    	$this->view->title=$this->view->translate("Mark Entry - Component List");
    	
    	if($this->_getParam('msg')==1){
    		$this->view->noticeSuccess = $this->view->translate("Data has been saved");
    	}
    	
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
		    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
    	//get component
    	$markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
		$list_component = $markDistributionDB->getListMainComponent($idSemester,$idProgram,$idSubject);	    
    	$this->view->rs_component = $list_component;
    	
    	
    }
    
    public function studentListAction(){
    	
    	$this->view->title=$this->view->translate("Mark Entry - Add Mark");
    
    	$IdMarksDistributionMaster = $this->_getParam('id');
    	$idSemester = $this->_getParam('idSemester');
    	$idProgram = $this->_getParam('idProgram');
    	$idSubject = $this->_getParam('idSubject');
    	
    	$this->view->IdMarksDistributionMaster = $IdMarksDistributionMaster;
    	$this->view->idSemester = $idSemester;
    	$this->view->idProgram = $idProgram;
    	$this->view->idSubject = $idSubject;
		    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	
    	//get info program
    	$programDB = new GeneralSetup_Model_DbTable_Program();
    	$this->view->program = $programDB->fngetProgramData($idProgram);
    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	    	
    	//get main component info
    	$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	$this->view->main_component = $markDistributionDB->getInfoComponent($IdMarksDistributionMaster);
    	
    	
    	//get list component item    	
  	    $oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
  	    $component_item = $oCompitem->getListComponentItem($IdMarksDistributionMaster);
  	    $this->view->component_item = $component_item;
  	    $this->view->total_item = count($component_item);
    	
    	//get list student yg register  semester,program,subject di atas
    	//keluarkan aje dulu list student filter by group or lecturer later
    	
        $form = new Examination_Form_MarkEntrySearchStudent();
     	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudentListMarkSubmitted($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster,$formData);
	    	
    	}else{ 
    	
	    	$studentDB = new Examination_Model_DbTable_StudentRegistration();
	    	$students = $studentDB->getStudentListMarkSubmitted($idSemester,$idProgram,$idSubject,$IdMarksDistributionMaster);
	    		
    	} //end if
    	
    	
    	
    	
		$this->view->students = $students;
					
    	//echo '<pre>';
    	//print_r($students);
    	//echo '</pre>';
    	
    }
    
    
    public function saveMarkApprovalAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			
			//get main component info
    		$markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    		$main_component = $markDistributionDB->getInfoComponent($formData["IdMarksDistributionMaster"]);
    
    	
			//get list component item    	
	  	    $oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();	
	  	    $component_item = $oCompitem->getListComponentItem($formData["IdMarksDistributionMaster"]);
	  	   
  	    
			echo '<pre>';
    		print_r($formData);
    		echo '</pre>';
    		
    		for($i=0; $i<count($formData['IdStudentMarksEntry']); $i++){

    		}
    		
    		
    		
    		
    		
    	}
    			//redirect
				//$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'mark-entry', 'action'=>'view-component','idSemester'=>$formData["idSemester"],'idProgram'=>$formData["idProgram"],'idSubject'=>$formData["idSubject"],'msg'=>1),'default',true));
		   
    	
    }
    
    
	
}