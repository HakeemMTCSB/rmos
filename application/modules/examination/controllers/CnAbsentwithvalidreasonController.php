\\<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 27/5/2016
 * Time: 3:33 PM
 */
class Examination_CnAbsentwithvalidreasonController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Examination_Model_DbTable_CnAbsentwithvalidreason();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Credit Note Absent With Valid Reason');

        $invoiceClass = new icampus_Function_Studentfinance_Invoice();

        $semesterList = $this->model->getSemester();
        $this->view->semesterList = $semesterList;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->formData = $formData;

            $list = $this->model->getStudentList($formData['semester']);

            if ($list){
                foreach ($list as $loop){
                    if ($loop['IdScheme']==1 || $loop['IdScheme']==12){
                        if ($loop['mode_of_program'] == 578 || $loop['mode_of_program'] == 767){
                            $itemID = array(889, 890);
                        }else {
                            $itemID = array(879);
                        }
                    }else{
                        $itemID = array(889, 890);
                    }

                    $subject = $this->model->getSubject($loop['er_idStudentRegistration'], $loop['er_idSubject'], $loop['er_idSemester']);

                    $item = $this->model->getItem($subject['IdStudentRegSubjects'], $itemID);

                    if(isset($item) && $item['invoice_id']!=''){
                        $invoiceClass->generateCreditNote($item['student_id'], $loop['er_idSubject'], $item['invoice_id'], $item['item_id'], 100);
                    }
                }
            }
        }
    }
}