<?php



class Examination_AppealController extends Zend_Controller_Action {

	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Student Remarking List");
    
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;	
		
		$session = Zend_Registry::get('sis');
			
   	    //clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->remarking_search);
    	}
    	
    	$form = new  Examination_Form_AppealSearchForm(array('locale'=>$locale));
    	$appealDB = new Examination_Model_DbTable_Appeal();
    	
    	if ($this->getRequest()->isPost()) {    		
    
			$formData = $this->getRequest()->getPost();		
			$form->populate($formData);
			
			$session->remarking_search = $formData;
			
	    	$appealList = $appealDB->getListApplication($formData);
	    	    	
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($appealList));
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
					
			$this->view->paginator = $paginator;
			
    	}else{
    		
    		//populate by session 
	    	if (isset($session->remarking_search)) {    	
	    		$form->populate($session->remarking_search);
	    		$formData = $session->remarking_search;			
	    		
	    		$appealList = $appealDB->getListApplication($formData);
	    	    	
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($appealList));
				$paginator->setItemCountPerPage(100);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
						
				$this->view->paginator = $paginator;
	    	}
    	}
    	$this->view->form = $form;
    	
    }
    
     public function applyAction(){
     		
     		$auth = Zend_Auth::getInstance();    	          	 
	  
     		$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale  = $locale;	
		
     		$this->view->title=$this->view->translate("Apply Remarking");
    	
     		$form = new  Examination_Form_AppealAddForm(array('locale'=>$locale));
    		$this->view->form = $form;
    		
	     	$session = Zend_Registry::get('sis');
				
	   	    //clear the session
	    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
				unset($session->search_apply);
	    	}
    	
	    	
			$studentRegSubDB = new Examination_Model_DbTable_StudentRegistrationSubject();
					
    		if ($this->getRequest()->isPost()) {    		
    
				$formData = $this->getRequest()->getPost();	
		
				if ($form->isValid($formData)) {
				
					$form->populate($formData);
					
					$session->search_apply = $formData;
					
					$this->view->idSemester = $formData['IdSemester'];
					$this->view->idSubject = $formData['IdSubject'];
			
					$this->view->students = $studentRegSubDB->getStudent($formData);
					
				}
				
    		}else{

    			if(isset($session->search_apply)){
    				
    				$formData = $session->search_apply;
    				
    				$this->view->idSemester = $formData['IdSemester'];
					$this->view->idSubject = $formData['IdSubject'];
			
					$this->view->students = $studentRegSubDB->getStudent($formData);
    			}
    		}
     }
     
     public function saveApplyAction(){
     		
     		$auth = Zend_Auth::getInstance();    	          	 
	  
     		$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale  = $locale;	
			
			if ($this->getRequest()->isPost()) {    		
    
				$formData = $this->getRequest()->getPost();
		
				
				$appealDB = new Examination_Model_DbTable_Appeal();				
									
				$info["sa_idSemester"]=$formData["idSemester"];
				$info["sa_idSubject"]=$formData["idSubject"];				
				$info["sa_status"]=1; //Apply	
				$info["sa_applyDate"]=date("Y-m-d H:i:s");
				$info["sa_applyBy"]=$auth->getIdentity()->id;
				$info["sa_applyRole"]=$auth->getIdentity()->IdRole;
			
				for($i=0; $i<count($formData["IdStudentRegSubjects"]); $i++){
					$info["sa_idStudentRegSubject"]=$formData["IdStudentRegSubjects"][$i];					
					$appealDB->addData($info);
				}
				
				$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
				
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'appeal', 'action'=>'apply'),'default',true));
			}
			exit;
	
      }
    
     public function appealApprovalAction(){
    	
     		$auth = Zend_Auth::getInstance();    	          	 
	  
     		$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale  = $locale;	
		
     		$this->view->title=$this->view->translate("Remarking Approval");    	         	 
     		
	    	$form = new  Examination_Form_AppealAddForm(array('locale'=>$locale));
	    	$this->view->form = $form;
	    	
	    	if ($this->getRequest()->isPost()) {    		
	    
				$formData = $this->getRequest()->getPost();		
				
				if ($form->isValid($formData)) {
				
					$form->populate($formData);
				
					$formData['status']=1;
					
			    	$appealDB = new Examination_Model_DbTable_Appeal();
			    	$appealList = $appealDB->getListApplication($formData);		    	    	
					$this->view->list = $appealList;
				}
	    	}
     }
     
     public function saveApprovalAction(){
     	
     		$auth = Zend_Auth::getInstance();    	          	 
	  
     		$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale  = $locale;	
			
			if ($this->getRequest()->isPost()) {    		
    
				$formData = $this->getRequest()->getPost();			
				
				$appealDB = new Examination_Model_DbTable_Appeal();				
		
				for($i=0; $i<count($formData["sa_id"]); $i++){
					
					$sa_id=$formData["sa_id"][$i];					
					$info["sa_status"]=$formData['status']; 
					$info["sa_remarks"]=$formData['remarks'];  

					if($formData['status']==2){
						$info["sa_approvedDate"]=date("Y-m-d H:i:s");
						$info["sa_approvedBy"]=$auth->getIdentity()->id;
					}
					
					if($formData['status']==3){
						$info["sa_rejectDate"]=date("Y-m-d H:i:s");
						$info["sa_rejectBy"]=$auth->getIdentity()->id;
					}
				
					$appealDB->updateData($info,$sa_id);
				}				
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'appeal', 'action'=>'appeal-approval'),'default',true));
			}
     }
     
      public function viewAction(){
    
      	 $this->view->title=$this->view->translate("Remarking Details");   
      	
      	 $id = $this->_getParam('id',0);        
      	 
      	 $registry = Zend_Registry::getInstance();
		 $locale = $registry->get('Zend_Locale');
		 $this->view->locale  = $locale;	
			
			
         $appealDB = new Examination_Model_DbTable_Appeal();
		 $appeal = $appealDB->getAppealInfo($id);
		 $this->view->appeal = $appeal;
		        
         //get course info
    	 $courseDb= new GeneralSetup_Model_DbTable_Subjectmaster();
    	 $this->view->subject = $courseDb->getData($appeal["sa_idSubject"]);
    	
    	 //get semester info
    	 $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    	 $this->view->semester = $semesterDb->getData($appeal["sa_idSemester"]);
    	
    	 //echo '<pre>';
    	 //print_r($appeal);
      }
      
      
     public function viewOldAction(){
     	
     	 // disable layouts for this action:
         $this->_helper->layout->disableLayout();
        
         $id = $this->_getParam('id',0);
        
         $appealDB = new Examination_Model_DbTable_Appeal();
		 $appeal = $appealDB->getData($id);
		 $this->view->appeal = $appeal;
		        
         //get course info
    	 $courseDb= new GeneralSetup_Model_DbTable_Subjectmaster();
    	 $this->view->subject = $courseDb->getData($appeal["sa_idSubject"]);
    	
    	 //get semester info
    	 $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    	 $this->view->semester = $semesterDb->getData($appeal["sa_idSemester"]);
    	
    	 //get component info
    	 $MarkDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	 $component = $MarkDistributionDB->getInfoComponent($appeal["sa_idComponent"]);
    	 
    	  //get compinent info
    	 $MarkDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
    	 $component = $MarkDistributionDB->getInfoComponent($appeal["sa_idComponent"]);
		    	 
		    	 //get component item
		    	 $MarkDistributionDetailsDB = new Examination_Model_DbTable_Marksdistributiondetails();
		    	 $component_item = $MarkDistributionDetailsDB->getListComponentItem($appeal["sa_idComponent"]);
		    	 
		    	 if(count($component_item)>0){
			    	 foreach($component_item as $key=>$item){
			    	 	//get appeal mark
			    	 	$mark = $appealDB->getAppealMark($id,$appeal["sa_idComponent"],$item["IdMarksDistributionDetails"]);
			    	 	$component_item[$key]["mark"]=$mark["sam_appeal_mark"];
			    	 	$component_item[$key]["sam_id"]=$mark["sam_id"];
			    	 }
		    	 }else{		    	 	
		    	 		$mark = $appealDB->getAppealMark($id,$appeal["sa_idComponent"]);
		    	 		$component["mark"]=$mark["sam_appeal_mark"];
		    	 		$component["finalmark"]=$mark["sam_appeal_finalmark"];
		    	 }
		    	 
		    	 /*echo '<pre>';
		    	 print_r($component_item);
		    	  echo '</pre>';*/
		    	 
		    	 $component["item"]=$component_item;
		    	 $this->view->component = $component;
    	 
     }
     
     public function updateMarkAction(){
      
          	 $auth = Zend_Auth::getInstance();
     
          	 $registry = Zend_Registry::getInstance();
			 $locale = $registry->get('Zend_Locale');
			 $this->view->locale  = $locale;	
			
          	 $this->view->title=$this->view->translate("Remarking : Update Mark");    	         	 
     		
             $id = $this->_getParam('id',0);
      	     $this->view->id = $id;
         
	    	
	    	if ($this->getRequest()->isPost()) {    		
    
				$formData = $this->getRequest()->getPost();			
				
				$appealDB = new Examination_Model_DbTable_Appeal();	
				$appeal = $appealDB->getStudentData($formData['sam_sa_id']);
				 
				$studentRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
				$subject = $studentRegDB->getData($formData['idStudentRegSubject']);
				
				$idStudentRegSubject = $formData['idStudentRegSubject'];
				
				//update student_appeal status
				$status["sa_approvedDate"]=date("Y-m-d H:i:s");
				$status["sa_approvedBy"]=$auth->getIdentity()->id;
				$appealDB->updateData(array('sa_status'=>4),$formData['sam_sa_id']);
				
				//echo '<pre>';				
				//print_r($formData);				
				unset($formData['idStudentRegSubject']);	
				unset($formData['submit']);				
				
				//add appeal mark details
				$formData['sam_createddt']=date("Y-m-d H:i:s");
				$formData['sam_createdby']=$auth->getIdentity()->id;				
				$sam_id = $appealDB->addAppealMark($formData);
								
				//copy to as history
				$history['final_course_mark'] = $subject['final_course_mark'];
				$history['grade_point'] = $subject['grade_point'];
				$history['grade_name'] = $subject['grade_name'];				
				$history['grade_status'] = $subject['grade_status'];
				$appealDB->updateAppealMark($history,$sam_id);
				
				
				try{
						    	
					//get grade from actual mark given
					$cms_calculation = new Cms_ExamCalculation();
					$grade=$cms_calculation->getGradeInfo($appeal['sa_idSemester'],$appeal['IdProgram'],$appeal['sa_idSubject'],$formData['sam_actual_mark']);
					
	    	 		 if(isset($grade["Pass"])){
					     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
					     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
				     	 else $grade_status='';
				     }else{
				     	 $grade_status='';
				     }
					     
					//update remarking mark grade info
					$mark['final_course_mark'] = $formData['sam_actual_mark'];
					$mark['grade_point'] = $grade['GradePoint'];
					$mark['grade_name'] = $grade['GradeName'];				
					$mark['grade_status'] = $grade_status;
					$mark['grade_desc'] = $grade['GradeDesc'];	
					$mark['appeal_mark'] = 1;				
					$studentRegDB->updateData($mark,$idStudentRegSubject);
					
					$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
					
				}catch (Exception $e) {
					
					$this->_helper->flashMessenger->addMessage(array('error' => $e->getMessage()));
				}
								
				$this->_redirect($this->view->url(array('module'=>'examination','controller'=>'appeal', 'action'=>'view','id'=>$formData['sam_sa_id']),'default',true));
				
	    	}else{
	    		
	    		 $appealDB = new Examination_Model_DbTable_Appeal();
				 $appeal = $appealDB->getStudentData($id);
				 $this->view->appeal = $appeal;
				        
		         //get course info
		    	 $courseDb= new GeneralSetup_Model_DbTable_Subjectmaster();
		    	 $this->view->subject = $courseDb->getData($appeal["sa_idSubject"]);
		    	
		    	 //get semester info
		    	 $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		    	 $this->view->semester = $semesterDb->getData($appeal["sa_idSemester"]);
		    	 
		    	 //get ecturer
		    	 $staffDB = new GeneralSetup_Model_DbTable_Staffmaster();
		    	 $this->view->academic_staff = $staffDB->getAcademicStaff();
	    	
		    	 //get student reg subject info
		    	 $studentRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
				 $this->view->grade = $studentRegDB->getData($appeal['sa_idStudentRegSubject']);
				
	    	}
    	
	    	
		    
	 }
	 
     public function updateMarkOldAction(){
     	
     	 // disable layouts for this action:
         //$this->_helper->layout->disableLayout();
         
         $auth = Zend_Auth::getInstance();
         	
         	
         $cms_calculation = new Cms_ExamCalculation();
    	 $markEntryDB = new Examination_Model_DbTable_StudentMarkEntry();
    	 $DetailsMarkEntryDB = new Examination_Model_DbTable_StudentDetailsMarkEntry();
         $masterDB = new Examination_Model_DbTable_Marksdistributionmaster();
         $detailsDB = new Examination_Model_DbTable_Marksdistributiondetails();
         $markHistoryDB = new Examination_Model_DbTable_StudentMarkHistory();
         
    	  if ($this->getRequest()->isPost()) {    		
    
			$formData = $this->getRequest()->getPost();	
			
		    $appealDB = new Examination_Model_DbTable_Appeal();
		    
		    $data["sam_sa_id"] = $formData["sa_id"]; //appeal id
		    $data["sam_idComponent"]= $formData["id_component"]; //main component id
		   
		   	if(isset($formData["id_component_item"]) && count($formData["id_component_item"]>0)){
		   		
			    for($i=1; $i<=count($formData["id_component_item"]); $i++){
		   
			    	$data["sam_idItem"]=$formData["id_component_item"][$i];
				    $data["sam_appeal_mark"]=$formData["new_mark"][$i];
				    $data["sam_appeal_finalmark"]=$formData["finalmark"][$i];
			    
				    if(isset($formData["sam_id"][$i]) && $formData["sam_id"][$i]!=''){
				    	$appeal_id = $formData["sam_id"][$i];	
				    	$appealDB->updateAppealMark(array('sam_appeal_mark'=>$formData["new_mark"][$i],'sam_appeal_finalmark'=>$formData["finalmark"][$i]),$formData["sam_id"][$i]);
				    }else{
				    	$appeal_id = $appealDB->addAppealMark($data);
				    }
			    }
			    
		   	}else{	
		   		 	
		   		    $TotalMarkObtained=$formData["new_mark"]; 		   		      		
				    if($formData["sam_id"]!=''){		
				    	$appeal_id = $formData["sam_id"];		    	
				    	$appealDB->updateAppealMark(array('sam_appeal_mark'=>$formData["new_mark"],'sam_appeal_finalmark'=>$formData["finalmark"]),$formData["sam_id"]);
				    }else{	
		   		        $data["sam_appeal_mark"]=$formData["new_mark"];	
		   		        $data["sam_appeal_finalmark"]=$formData["finalmark"];
		   		        print_r($data);
				    	$appeal_id = $appealDB->addAppealMark($data);
				    }
		   	}		 
		    
		
		    
		    $info["sa_verify_status"] = $formData["verify_status"];
		    
		    
		    if($formData["verify_status"]==1){
		    	
		    	//get main component info		    	
		    	$component = $masterDB->getInfoComponent($formData["id_component"]);
		    	$main_mark = $markEntryDB->getComponentMark($formData["idStudentRegistration"],$formData["id_component"],$formData["idSemester"],$formData["idSubject"]);
		        /*
		         * print_r($main_mark);
		    	exit;*/
		    	//copy (keep as history old mark)		    	
		    	$history["IdStudentMarksEntry"]=$main_mark["IdStudentMarksEntry"];
		    	$history["StudentMark"]=$main_mark["TotalMarkObtained"];
		    	$history["StudentMarkPercent"]=$main_mark["FinalTotalMarkObtained"];	
		    	$history["exam_group_id"]=$main_mark["exam_group_id"];	
		    	$history["category"]=2;	
		    	$history["category_ref_id"]=$appeal_id;		    			  
		    	$history["smh_createddt"]=date("Y-m-d H:i:s");
		    	$history["smh_createdby"]=$auth->getIdentity()->id;
		    	$idHistory = $markHistoryDB->addData($history);
		    	
		    	
		    	//replace old mark with new mark
		    	if(isset($formData["id_component_item"]) && count($formData["id_component_item"]>0)){
		    		
		    		$TotalMarkObtained = '';
		    	  	for($i=1; $i<=count($formData["id_component_item"]); $i++){
				    
		    	  		$ItemMarkObtained = $formData["new_mark"][$i];
		    	  		
		    	  		$item = $detailsDB->getDataComponentItem($formData["id_component_item"][$i]);
		    	  		$item_percentage = $item["Percentage"];
			    		$item_fullmark   = $item["Weightage"];
			    			
		    			if(isset($ItemMarkObtained) && $ItemMarkObtained!=''){
		    				if($TotalMarkObtained=='') {			    								    					
		    					$TotalMarkObtained = $cms_calculation->calculateMark($ItemMarkObtained,$item_fullmark,$item_percentage);
		    						    				
		    				}else{			    					
		    					$ItemMarkObtained = $cms_calculation->calculateMark($ItemMarkObtained,$item_fullmark,$item_percentage);
		    					$TotalMarkObtained = $TotalMarkObtained+$ItemMarkObtained;
		    				}
		    				
		    				//to get item punya percentage
		    				$FinalMarksObtained = $cms_calculation->calculateMark($ItemMarkObtained,$item["Weightage"],$item["Percentage"]);			    			
		    				$item_mark = $DetailsMarkEntryDB->getComponentItemMark($main_mark["IdStudentMarksEntry"],$formData["id_component_item"][$i]);		    				
		    				$DetailsMarkEntryDB->updateData(array('MarksObtained'=>$ItemMarkObtained,'FinalMarksObtained'=>$FinalMarksObtained),$item_mark["IdStudentMarksEntryDetail"]);
		    					
		    				$itemhistory["smh_id"]=$idHistory;
		    				$itemhistory["IdStudentMarksEntry"]=$main_mark["IdStudentMarksEntry"];		    			
					    	$itemhistory["StudentItemMark"]=$item_mark["MarksObtained"];
					    	$itemhistory["StudentItemPercentMark"]=$item_mark["FinalMarksObtained"];	  
					    	$itemhistory["simh_createddt"]=date("Y-m-d H:i:s");
		    				$itemhistory["simh_createdby"]=$auth->getIdentity()->id;
		    	
		    				$markHistoryDB->addItemData($itemhistory);
		    						    				
    					}//end if	
    					
    					
				    }//end for
			    
				    $FinalTotalMarkObtained=$cms_calculation->calculateMark($TotalMarkObtained,$component["Marks"],$component["Percentage"]);
				    $markEntryDB->updateData(array('TotalMarkObtained'=>$TotalMarkObtained,'FinalTotalMarkObtained'=>$FinalTotalMarkObtained,'category'=>2,'category_ref_id'=>$appeal_id),$main_mark["IdStudentMarksEntry"]);
		    		
		    	}else{
		    		
		    		$FinalTotalMarkObtained=$cms_calculation->calculateMark($TotalMarkObtained,$component["Marks"],$component["Percentage"]);		    		
		    	    $markEntryDB->updateData(array('TotalMarkObtained'=>$TotalMarkObtained,'FinalTotalMarkObtained'=>$FinalTotalMarkObtained,'category'=>2,'category_ref_id'=>$appeal_id),$main_mark["IdStudentMarksEntry"]);
		    	}
		    
		    	
		    	//update appeal info
		    	$infox["sa_verify_status"] = 1;	
		    	$infox["sa_verify_date"] = date("Y-m-d H:i:s");		    	   	
				$appealDB->updateData($infox,$formData["sa_id"]);
		    			    	
		    }//end verify
		    	    
		   
			//exit;
		    $this->_redirect($this->view->url(array('module'=>'examination','controller'=>'appeal', 'action'=>'index','msg'=>1),'default',true));

    	  }else{
    	  	
    	  	     $id = $this->_getParam('id',0);
    	  	     $this->view->id = $id;
         
	          	 $appealDB = new Examination_Model_DbTable_Appeal();
				 $appeal = $appealDB->getData($id);
				 $this->view->appeal = $appeal;
				        
		         //get course info
		    	 $courseDb= new GeneralSetup_Model_DbTable_Subjectmaster();
		    	 $this->view->subject = $courseDb->getData($appeal["sa_idSubject"]);
		    	
		    	 //get semester info
		    	 $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		    	 $this->view->semester = $semesterDb->getData($appeal["sa_idSemester"]);
		    	
		    	 //get compinent info
		    	 $MarkDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
		    	 $component = $MarkDistributionDB->getInfoComponent($appeal["sa_idComponent"]);
		    	 
		    	 //get component item
		    	 $MarkDistributionDetailsDB = new Examination_Model_DbTable_Marksdistributiondetails();
		    	 $component_item = $MarkDistributionDetailsDB->getListComponentItem($appeal["sa_idComponent"]);
		    	 
		    	 if(count($component_item)>0){
			    	 foreach($component_item as $key=>$item){
			    	 	//get appeal mark
			    	 	$mark = $appealDB->getAppealMark($id,$appeal["sa_idComponent"],$item["IdMarksDistributionDetails"]);
			    	 	$component_item[$key]["mark"]=$mark["sam_appeal_mark"];
			    	 	$component_item[$key]["finalmark"]=$mark["sam_appeal_finalmark"];
			    	 	$component_item[$key]["sam_id"]=$mark["sam_id"];
			    	 }
		    	 }else{		    	 	
		    	 		$mark = $appealDB->getAppealMark($id,$appeal["sa_idComponent"]);
		    	 		$component["mark"]=$mark["sam_appeal_mark"];
		    	 		$component["finalmark"]=$mark["sam_appeal_finalmark"];
		    	 		$component["sam_id"]=$mark["sam_id"];
		    	 }
		    	 
		    	 /*echo '<pre>';
		    	 print_r($component_item);
		    	  echo '</pre>';*/
		    	 
		    	 $component["item"]=$component_item;
		    	 $this->view->component = $component;
    	
    	  }
    
    	
     }
    
     
	public function searchCourseAction(){
	
		$idCollege = $this->_getParam('idCollege',null);
		 
		$this->_helper->layout->disableLayout();
	
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$db = Zend_Db_Table::getDefaultAdapter();
       
	    $select = $db->select()
	 				 ->from(array("sm"=>"tbl_subjectmaster"))	 				 
	 				 ->where("sm.Active = 1")
	 				 ->order('sm.SubCode');
	 				 
	 	if($idCollege){
	 		$select->where('sm.IdFaculty = ?',$idCollege);
	 	}
		$row = $db->fetchAll($select);
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($row);
	
		echo $json;
		exit();
	}
}

?>