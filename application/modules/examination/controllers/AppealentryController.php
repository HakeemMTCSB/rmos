<?php
class examination_AppealentryController extends Base_Base { //Controller for the User Module
	private $lobjAppealentry;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjAppealentry = new Examination_Model_DbTable_Appealentry();
		$this->lobjAppealEntryForm = new Examination_Form_Appealentry();
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lintpagecount = $this->gintPageCount;
			
		$larrresult = $this->lobjAppealentry->fnGetAppeal();


		if(isset($this->gobjsessionsis->MarksentryAppealpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->MarksentryAppealpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjAppealentry ->fnSearchAppeal( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->MarksentryAppealpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/appealentry');
		}
	}

	public function editmarksappealAction() {


		$idAppeal = ( int ) $this->_getParam ( 'id' );


		$this->view->appealcode = ( string ) $this->_getParam ( 'appealcode' );
		$this->view->subjectname = ( string ) $this->_getParam ( 'subjectname' );
		$this->view->CompNameMaster = ( string ) $this->_getParam ( 'CompNameMaster' );
		$this->view->CompNameDetails = ( string ) $this->_getParam ( 'CompNameDetails' );
		$this->view->previousmarks = ( string ) $this->_getParam ( 'previousmarks' );
		$this->view->comments = ( string ) $this->_getParam ( 'comments' );
		$idverifiermarks = ( string ) $this->_getParam ( 'Idverifiermarks' );
		$prevmarks=( string ) $this->_getParam ( 'previousmarks' );
		$this->view->lobjAppealEntryForm = $this->lobjAppealEntryForm;

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjAppealEntryForm->DateOfRemarks->setValue ( $ldtsystemDate );

		$this->view->lobjAppealEntryForm->Idverifiermarks->setValue ( $idverifiermarks );
		$this->view->lobjAppealEntryForm->prevmarks->setValue ( $prevmarks );

		$auth = Zend_Auth::getInstance();
		$this->view->lobjAppealEntryForm->RemarkedBy->setValue ( $auth->getIdentity()->iduser);
		$this->view->lobjAppealEntryForm->IdAppeal->setValue ($idAppeal);

		$result = $this->lobjAppealentry->fnEditMarksEntryAppeal($idAppeal);
		if($result) {
			$this->lobjAppealEntryForm->populate($result);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			if ($this->lobjAppealEntryForm->isValid ( $larrformData )) {

				if($this->view->markdentrytype=0){
					$this->lobjAppealentry->fnupdateVerifierMarks($larrformData['NewMarks'],$larrformData['Idverifiermarks']);
				}else if($this->view->markdentrytype=1){
					$higher= max($larrformData['NewMarks'],$larrformData['prevmarks']);
					$this->lobjAppealentry->fnupdateVerifierMarks($higher,$larrformData['Idverifiermarks']);
				}

				if($larrformData ['IdAppealEntry']){
					unset($larrformData['Idverifiermarks']);
					unset($larrformData['prevmarks']);
					$lintIdAppealEntry = $larrformData ['IdAppealEntry'];
					$this->lobjAppealentry->fnupdateMarksEntryAppeal($lintIdAppealEntry, $larrformData );
				}else{
					unset($larrformData['IdAppealEntry']);
					unset($larrformData['Idverifiermarks']);
					unset($larrformData['prevmarks']);
					$result = $this->lobjAppealentry->fnAddMarksEntryAppeal($larrformData); //instance for adding the lobjuserForm values to DB
				}


				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Remarking Against Appeal Edit Id=' . $idAppeal,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/examination/appealentry');
			}
		}
	}
}