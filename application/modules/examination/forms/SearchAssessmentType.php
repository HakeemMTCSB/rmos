<?php

class Examination_Form_SearchAssessmentType extends Zend_Form
{
		
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
			
	 	//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Effective Semester'),
			'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"].' - '.$semester['SemesterMainCode']);
		}
		
	   //Type
		$this->addElement('select','Type', array(
			'label'=>$this->getView()->translate('Type'),
			'required'=>true
		));	   			
		
		$this->Type->addMultiOption(null,"-- Please Select --");
		$this->Type->addMultiOption(1,"Standard");
		$this->Type->addMultiOption(2,"Comprehensive Exam");		
				
		
		$this->addElement('text','Description', array(
			'label'=>$this->getView()->translate('Description'),
		  'class'=>'input-txt'
		));	
		
		
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		//button
		$this->addElement('submit', 'Clear', array(
				'label'=>'Clear',
				'decorators'=>array('ViewHelper')
		));
		
		
		$this->addDisplayGroup(array('save','Clear'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>