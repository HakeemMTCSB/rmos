<?php 

class Examination_Form_MyGroupSearch extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
			
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
		
		
						
		//Subject Code
		$this->addElement('text','subject_code', array(
			'label'=>$this->getView()->translate('Course Code')
		));	
		
		$this->addElement('text','subject_name', array(
			'label'=>$this->getView()->translate('Course Name')
		));
		
		
		//Course
		/*$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course')
		));
		$this->IdSubject->addMultiOption(null,"-- All --");	
		$this->IdSubject->disableInArrayValidator = true;
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}*/
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
}
?>