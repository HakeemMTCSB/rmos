<?php

class Examination_Form_AppealSearchForm extends Zend_Form
{
		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
		//intake
		/*$this->addElement('select','idIntake', array(
			'label'=>'Intake'
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->idIntake->addMultiOption(null,"-- All --");
		foreach ($intakeList as $list){
			if($this->_locale=='ms_MY'){
				$this->idIntake->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
			}else{
				$this->idIntake->addMultiOption($list['IdIntake'],$list['IntakeDesc']);
			}
		}*/
		
	  	//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}	
		
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
			'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}					
				
		//Course
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course'),
		    'required'=>true
		));
		$this->IdSubject->addMultiOption(null,"-- Please Select --");	
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}
		
		 //Status
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Status')
		));
		
		$this->status->addMultiOption(null,"-- All --");
		$this->status->addMultiOption(1,"Apply");
		$this->status->addMultiOption(2,"Approved");
		$this->status->addMultiOption(3,"Rejected");
		
		//Student Name/NIM
		$this->addElement('text','Student', array(
			'label'=>$this->getView()->translate('Student Name/ID')
		));	
		
		
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('save'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>