<?php

class Examination_Form_AppealAddForm extends Zend_Form
{
		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchAddForm');
		
			
	  	
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
			'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}					
		
		//Program
		/*$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
		}		*/
		
		/*	//College
		$this->addElement('select','IdCollege', array(
			'label'=>$this->getView()->translate('Department'),
		    'onchange'=>'search_subject();',
		    'required'=>false
		));
		
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();		
		
		$this->IdCollege->addMultiOption(null,"-- All --");		
		foreach($collegeDb->fnGetCollegeList() as $college){
			if($this->_locale=='ms_MY'){
				$this->IdCollege->addMultiOption($college["key"],$college["ArabicName"]);
			}else{
				$this->IdCollege->addMultiOption($college["key"],$college["value"]);
			}
		}*/
		
		
		//Course
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course'),
		    'required'=>true
		));
		$this->IdSubject->addMultiOption(null,"-- Please Select --");	
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}
		
		
		//Student Name/ID
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));	
		
		//Student Name/ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID')
		));	
		
		
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('save'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>