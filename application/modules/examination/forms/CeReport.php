<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 13/4/2016
 * Time: 11:31 AM
 */
class Examination_Form_CeReport extends Zend_Dojo_Form {

    public function init(){

        $model = new Examination_Model_DbTable_CeReport();

        //year
        $year = new Zend_Form_Element_Select('year');
        $year->removeDecorator("DtDdWrapper");
        $year->setAttrib('class', 'select');
        //$year->setAttrib('required', '');
        $year->setAttrib('multiple', true);
        $year->removeDecorator("Label");

        $yearList = $model->fnGetSemesterList(11, true);

        if ($yearList){
            foreach ($yearList as $yearLoop){
                $year->addMultiOption($yearLoop['AcademicYear'], $yearLoop['AcademicYear']);
            }
        }

        //semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        //$semester->setAttrib('required', '');
        $semester->setAttrib('multiple', true);
        $semester->removeDecorator("Label");

        $semesterList = $model->fnGetSemesterList(11);

        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
            }
        }

        //form elements
        $this->addElements(array(
            $year,
            $semester
        ));
    }
}