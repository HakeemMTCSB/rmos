<?php 

class Examination_Form_SearchStudentSemester extends Zend_Form
{
		
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','form_student_semester');

		//exam type
		$this->addElement('select','exam_type', array(
				'label'=>$this->getView()->translate('Exam Type'),
				'required'=>true
		));
		
		$defDB = new App_Model_General_DbTable_Definationms();
		$assessmentList = $defDB->getDataByType(151);		
		$this->exam_type->addMultiOption(null,"-- Please select --");
		foreach($assessmentList as $assessment){
			$this->exam_type->addMultiOption($assessment["idDefinition"],$assessment["DefinitionDesc"]);
		}
		
		//Semester
		$this->addElement('select','semester', array(
			'label'=>$this->getView()->translate('Semester'),
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDB->fnGetSemesterList();
		
		$this->semester->addMultiOption(null,"-- Please select --");		
		foreach($semesterList as $semester){
			$this->semester->addMultiOption($semester["key"],$semester["value"]);
		}
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme'),
		    'required'=>false
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData()  as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}
						
		//Applicant Name
		$this->addElement('text','applicant_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));
		
		//Student ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID')
		));
				
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>