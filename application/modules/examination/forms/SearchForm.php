<?php 
class Examination_Form_SearchForm extends Zend_Form
{		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
		  //Semester
		$this->addElement('select','IdSemester');
	
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- All --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
			}
		}
		
		$this->IdSemester->removeDecorator("DtDdWrapper")						
						 ->removeDecorator("Label")
						 ->removeDecorator('HtmlTag');
		
		
		//Program
		$this->addElement('select','IdProgram');
	
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}	
		
		$this->IdProgram->removeDecorator("DtDdWrapper")						
						 ->removeDecorator("Label")
						 ->removeDecorator('HtmlTag');
	
						 
						 
	   $this->addElement('text','subject_code');
	   $this->subject_code->removeDecorator("DtDdWrapper")
						 ->removeDecorator("Label")
						 ->removeDecorator('HtmlTag');
						 
	   $this->addElement('submit','submit');	  
	   $this->submit->removeDecorator("DtDdWrapper")
						 ->removeDecorator("Label")
						 ->removeDecorator('HtmlTag');
	
	}
}

?>