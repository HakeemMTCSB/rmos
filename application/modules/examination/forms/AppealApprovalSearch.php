<?php

class Examination_Form_AppealSearchForm extends Zend_Form
{
		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester')
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- All --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}					
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('save'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>