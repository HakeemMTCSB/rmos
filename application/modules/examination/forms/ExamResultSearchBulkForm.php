<?php

class Examination_Form_ExamResultSearchBulkForm extends Zend_Form
{
		
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
			
		//Convo
		$this->addElement('select','IdConvo', array(
			'label'=>$this->getView()->translate('Convocation'),
		 	'required'=>true
		));
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
	
		$this->IdConvo->addMultiOption(null,"-- Plese Select --");		
		foreach($convocationDb->getConvocationList()as $convo){
			$this->IdConvo->addMultiOption($convo["c_id"],'Year: '.$convo["c_year"].' Session: '.$convo["c_session"].' Date: '.date("d-m-Y",strtotime($convo['c_date_from'])).' to '.date("d-m-Y",strtotime($convo['c_date_to'])) );
		}	
		
		
	  	//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme'),
			'required'=>true,
            'onchange'=>'getProgramScheme()'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}		
		
				
		
	 	//Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme')
		));		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));		
		$this->IdProgramScheme->setRegisterInArrayValidator(false);		
					
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));	
		
		//Student ID
		$this->addElement('text','Student', array(
			'label'=>$this->getView()->translate('Student ID')
		));	
		
		
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('save'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>
