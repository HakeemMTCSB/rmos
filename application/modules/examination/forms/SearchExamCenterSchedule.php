<?php

class Examination_Form_SearchExamCenterSchedule extends Zend_Form
{
		
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
			
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
			 'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"].'-'.$semester['SemesterMainCode']);
		}	

		//College
		$this->addElement('select','IdCollege', array(
			'label'=>$this->getView()->translate('Department'),
		    'onchange'=>'search_subject();',
		    'required'=>false
		));
		
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();		
		
		$this->IdCollege->addMultiOption(null,"-- All --");		
		foreach($collegeDb->fnGetCollegeList() as $college){
			if($this->_locale=='ms_MY'){
				$this->IdCollege->addMultiOption($college["key"],$college["ArabicName"]);
			}else{
				$this->IdCollege->addMultiOption($college["key"],$college["value"]);
			}
		}
		
		//Course
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course')
		));
		$this->IdSubject->addMultiOption(null,"-- All --");	
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}
		
		 //Exam Center
		$this->addElement('select','ec_id', array(
			'label'=>$this->getView()->translate('Exam Center'),
			 'required'=>false
		));
		
		$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
		
		$this->ec_id->addMultiOption(null,"-- All --");		
		foreach($examCenterDB->getData() as $examcenter){
			$this->ec_id->addMultiOption($examcenter["ec_id"],$examcenter["ec_name"]);
		}	
		
		
	
		
		//button
		$this->addElement('submit', 'search', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('search'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>