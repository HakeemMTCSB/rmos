<?php

class Examination_Form_SearchExamSchedule extends Zend_Form
{
		
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
			
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
			 'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"].' - '.$semester['SemesterMainCode']);
		}					
		
		
		//College
		$this->addElement('select','IdCollege', array(
			'label'=>$this->getView()->translate('Department'),
		    'onchange'=>'search_subject();',
		    'required'=>true
		));
		
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();		
		
		$this->IdCollege->addMultiOption(null,"-- Please Select --");		
		foreach($collegeDb->fnGetCollegeList() as $college){
			if($this->_locale=='ms_MY'){
				$this->IdCollege->addMultiOption($college["key"],$college["ArabicName"]);
			}else{
				$this->IdCollege->addMultiOption($college["key"],$college["value"]);
			}
		}
		
		
		//Course
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course')
		));
		$this->IdSubject->addMultiOption(null,"-- All --");	
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}
		
		/*//Subject Name
		$this->addElement('text','subject_name', array(
			'label'=>$this->getView()->translate('Course Name'),
		    'required'=>false,
		));	
		
		//Subject Code
		$this->addElement('text','subject_code', array(
			'label'=>$this->getView()->translate('Course Code'),
		    'required'=>false,
		));	
		*/
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
		//button
		$this->addElement('submit', 'Clear', array(
				'label'=>'Clear',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('save','Clear'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>