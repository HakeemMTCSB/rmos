<?php

class Examination_Form_ExamScalingAddForm extends Zend_Form
{
		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchAddForm');
		
			
	  	
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
			'required'=>true
		));
		
		$this->IdSemester->removeDecorator("DtDdWrapper");	
		$this->IdSemester->removeDecorator("Label");
		$this->IdSemester->removeDecorator('HtmlTag');
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}					
		
		

		
	}
}
?>