<?php 

class Examination_Form_MarkDistributionSetupSearchHop extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
			
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Effective Semester'),
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
				
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program Name'),
		    'required'=>false
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}		
	
		
		//College
		$this->addElement('select','IdCollege', array(
			'label'=>$this->getView()->translate('Department'),
		    'onchange'=>'search_subject();',
		    'required'=>false
		));
		
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();		
		
		$this->IdCollege->addMultiOption(null,"-- All --");		
		foreach($collegeDb->fnGetCollegeList() as $college){
			if($this->_locale=='ms_MY'){
				$this->IdCollege->addMultiOption($college["key"],$college["ArabicName"]);
			}else{
				$this->IdCollege->addMultiOption($college["key"],$college["value"]);
			}
		}
		
		
		//Course
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course'),
		    'required'=>true
		));
		$this->IdSubject->addMultiOption(null,"-- Please Select --");	
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
}
?>