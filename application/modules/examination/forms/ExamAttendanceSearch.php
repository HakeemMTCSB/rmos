<?php

class Examination_Form_ExamAttendanceSearch extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true,
			'onchange'=>'getSubject();'
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
		
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),
		    'required'=>true,
			'onchange'=>'getSubject();'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		//Exam Center
		$ecDB = new GeneralSetup_Model_DbTable_ExamCenter();
		
		$this->addElement('select','ec_id', array(
			'label'=>$this->getView()->translate('Exam Center')
		));
		
		$this->ec_id->addMultiOption(null,"-- All --");		
		foreach($ecDB->getData() as $center){
			if($this->_locale=='ms_MY'){
				$this->ec_id->addMultiOption($center["ec_id"],$center["ec_code"].' - '.$center["ec_name"]);
			}else{
				$this->ec_id->addMultiOption($center["ec_id"],$center["ec_code"].' - '.$center["ec_name"]);
			}
		}
		
		
		
		/*$this->addElement('text','ec_id', array(
			'label'=>$this->getView()->translate('Exam Center')
		));	*/
		
		
		//Subject Code
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course')
		));	
		$this->IdSubject->addMultiOption(null,"-- All --");
		$this->IdSubject->setRegisterInArrayValidator(false);
		
		
		
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>