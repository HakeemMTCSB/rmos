<?php 

class Examination_Form_SearchStudentByEc extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),
		    'required'=>true
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		//Subject Code
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course Name')
		));	
		$this->IdSubject->addMultiOption(null,"-- All --");	
		$this->IdSubject->setRegisterInArrayValidator(false);
	
		
	    //Country
		$this->addElement('select','idCountry', array(
			'label'=>$this->getView()->translate('Country')
		));
		
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
		
		$this->idCountry->addMultiOption(null,"-- All --");		
		foreach($examCenterDB->getExamCenterCountry() as $country){
			if($this->_locale=='ms_MY'){
				$this->idCountry->addMultiOption($country["idCountry"],$country["DefaultLanguage"]);
			}else{
				$this->idCountry->addMultiOption($country["idCountry"],$country["CountryName"]);
			}
		}
		
		  //Country
		$this->addElement('select','idCity', array(
			'label'=>$this->getView()->translate('City')
		));
		$this->idCity->addMultiOption(null,"-- All --");		
	    $this->idCity->setRegisterInArrayValidator(false);
	
		
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>