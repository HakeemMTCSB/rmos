<?php 

class Examination_Form_ChangeStatusSearch extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
			
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
				
		//Course
		$this->addElement('select','IdSubject', array(
			'label'=>$this->getView()->translate('Course'),
		    'required'=>false
		));
		$this->IdSubject->addMultiOption(null,"-- All --");	
		
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		foreach($subjectDB->searchSubject() as $subject){
			if($this->_locale=='ms_MY'){
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["subjectMainDefaultLanguage"]);
			}else{
				$this->IdSubject->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
			}
		}
		
		//Student Name
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));
		
		//Student ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID')
		));
		
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
}
?>