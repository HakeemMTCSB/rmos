<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/9/2015
 * Time: 4:49 PM
 */
class Examination_Form_ExamCalendarSearch extends Zend_Dojo_Form { //Formclass for the user module

    public function init(){
        $this->setMethod('post');
        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $model = new Examination_Model_DbTable_ExamCalendar();

        //semester
        $ec_idSemester = new Zend_Form_Element_Select('ec_idSemester');
        $ec_idSemester->removeDecorator("DtDdWrapper");
        $ec_idSemester->setAttrib('class', 'select');
        $ec_idSemester->removeDecorator("Label");

        $ec_idSemester->addMultiOption('', '-- Select --');

        $semList = $model->getSemesterList();

        if ($semList){
            foreach ($semList as $semLoop){
                $ec_idSemester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName'].' '.$semLoop['SemesterMainCode']);
            }
        }

        //activity
        $ec_idActivity = new Zend_Form_Element_Select('ec_idActivity');
        $ec_idActivity->removeDecorator("DtDdWrapper");
        $ec_idActivity->setAttrib('class', 'select');
        $ec_idActivity->removeDecorator("Label");

        $ec_idActivity->addMultiOption('', '-- Select --');

        $actList = $model->getDefination(177);

        if ($actList){
            foreach ($actList as  $actLoop){
                $ec_idActivity->addMultiOption($actLoop['idDefinition'], $actLoop['DefinitionDesc']);
            }
        }

        $this->addElements(array(
            $ec_idSemester,
            $ec_idActivity
        ));
    }
}