<?php 

class Examination_Form_ExamCenterSetup extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//Semester
		$this->addElement('select','idSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->idSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->idSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->idSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
		  //Country
		$this->addElement('select','idCountry', array(
			'label'=>$this->getView()->translate('Country'),
			'onchange'=>'getCity(this)'
		));
		
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
		
		$this->idCountry->addMultiOption(null,"-- All --");		
		foreach($examCenterDB->getExamCenterCountry() as $country){
			if($this->_locale=='ms_MY'){
				$this->idCountry->addMultiOption($country["idCountry"],$country["DefaultLanguage"]);
			}else{
				$this->idCountry->addMultiOption($country["idCountry"],$country["CountryName"]);
			}
		}
		
		  //City
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
        
		$this->addElement('select','idCity', array(
			'label'=>$this->getView()->translate('City')
		));
		$this->idCity->addMultiOption(null,"-- All --");	
		foreach($examCenterDB->getExamCenterCity() as $city){
			if($this->_locale=='ms_MY'){
				$this->idCity->addMultiOption($city["idCity"],$city["CityName"]);
			}else{
				$this->idCity->addMultiOption($city["idCity"],$city["CityName"]);
			}
		}	
	    $this->idCity->setRegisterInArrayValidator(false);
	    
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Status'),
		 	'required'=>true
		));
		$this->status->addMultiOption(1,"Not Assigned");
		$this->status->addMultiOption(2,"Assigned");
	
		
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        /*//button
		$this->addElement('reset', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));*/
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>