<?php

class Examination_Form_SearchAddExamCenterSchedule extends Zend_Form
{
		
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','SearchForm');
		
			
		//Country
		$this->addElement('select','idCountry', array(
			'label'=>$this->getView()->translate('Country'),
			 'required'=>false
		));
		
		$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
		
		$this->idCountry->addMultiOption(null,"-- All --");		
		foreach($examCenterDB->getExamCenterCountry() as $examcenter){
			$this->idCountry->addMultiOption($examcenter["idCountry"],$examcenter["CountryName"]);
		}	
		
		
		//City
		$this->addElement('select','idCity', array(
			'label'=>$this->getView()->translate('City'),
			 'required'=>false
		));
		
		$this->idCity->addMultiOption(null,"-- All --");		
			
		
	 
		 //Exam Center
		$this->addElement('select','ec_id', array(
			'label'=>$this->getView()->translate('Exam Center'),
			 'required'=>false
		));
		
		$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
		
		$this->ec_id->addMultiOption(null,"-- All --");		
		foreach($examCenterDB->getData() as $examcenter){
			$this->ec_id->addMultiOption($examcenter["ec_id"],$examcenter["ec_name"]);
		}	
		
		
	
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
	
		
		$this->addDisplayGroup(array('save'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>