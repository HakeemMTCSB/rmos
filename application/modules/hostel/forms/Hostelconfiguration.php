<?php
class Hostel_Form_Hostelconfiguration extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Hostel Configuration SETUP
	 * @author: Vipul
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');

		$chargesRate = new Zend_Dojo_Form_Element_FilteringSelect('chargesRate');
		$chargesRate->removeDecorator("DtDdWrapper");
		$chargesRate->removeDecorator("Label");
		$chargesRate->removeDecorator('HtmlTag');
		$chargesRate->setAttrib('required', "false");
		$chargesRate->setRegisterInArrayValidator(false);
		$chargesRate->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$approvalHostelProcess = new Zend_Form_Element_Checkbox('Applicableforapproval');
		$approvalHostelProcess->setAttrib('dojoType', "dijit.form.CheckBox");
		$approvalHostelProcess->setvalue('0');
		$approvalHostelProcess->removeDecorator("DtDdWrapper");
		$approvalHostelProcess->removeDecorator("Label");
		$approvalHostelProcess->removeDecorator('HtmlTag');

		$revertHostelProcess = new Zend_Form_Element_Checkbox('Applicableforrevert');
		$revertHostelProcess->setAttrib('dojoType', "dijit.form.CheckBox");
		$revertHostelProcess->setvalue('0');
		$revertHostelProcess->removeDecorator("DtDdWrapper");
		$revertHostelProcess->removeDecorator("Label");
		$revertHostelProcess->removeDecorator('HtmlTag');

		$eServicesHostelProcess = new Zend_Form_Element_Checkbox('Applicableforeservice');
		$eServicesHostelProcess->setAttrib('dojoType', "dijit.form.CheckBox");
		$eServicesHostelProcess->setvalue('0');
		$eServicesHostelProcess->removeDecorator("DtDdWrapper");
		$eServicesHostelProcess->removeDecorator("Label");
		$eServicesHostelProcess->removeDecorator('HtmlTag');

		$generateHostelBill = new Zend_Form_Element_Checkbox('Applicableforgenbill');
		$generateHostelBill->setAttrib('dojoType', "dijit.form.CheckBox");
		$generateHostelBill->setvalue('0');
		$generateHostelBill->removeDecorator("DtDdWrapper");
		$generateHostelBill->removeDecorator("Label");
		$generateHostelBill->removeDecorator('HtmlTag');

		$extendStayPeriod = new Zend_Form_Element_Checkbox('Applicableforextend');
		$extendStayPeriod->setAttrib('dojoType', "dijit.form.CheckBox");
		$extendStayPeriod->setvalue('0');
		$extendStayPeriod->removeDecorator("DtDdWrapper");
		$extendStayPeriod->removeDecorator("Label");
		$extendStayPeriod->removeDecorator('HtmlTag');
			
		$Add = new Zend_Form_Element_Submit('Add');
		$Add->label = $gstrtranslate->_("Save");
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType', "dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearpageAdd()');
		$clear->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		
		$Block_Label = new Zend_Form_Element_Text('Block_Label');
		$Block_Label->removeDecorator("DtDdWrapper");
		$Block_Label->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Block_Label->setAttrib('required',"false")->setAttrib('trim',"true");
		$Block_Label->setAttrib('maxlength','50');
		$Block_Label->setAttrib('propercase',"true");
		$Block_Label->removeDecorator("Label");
		$Block_Label->removeDecorator('HtmlTag');
		
		$Level_Label = new Zend_Form_Element_Text('Level_Label');
		$Level_Label->removeDecorator("DtDdWrapper");
		$Level_Label->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Level_Label->setAttrib('required',"false")->setAttrib('trim',"true");
		$Level_Label->setAttrib('maxlength','50');
		$Level_Label->setAttrib('propercase',"true");
		$Level_Label->removeDecorator("Label");
		$Level_Label->removeDecorator('HtmlTag');
		
		$Room_Label = new Zend_Form_Element_Text('Room_Label');
		$Room_Label->removeDecorator("DtDdWrapper");
		$Room_Label->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Room_Label->setAttrib('required',"false")->setAttrib('trim',"true");
		$Room_Label->setAttrib('maxlength','50');
		$Room_Label->setAttrib('propercase',"true");
		$Room_Label->removeDecorator("Label");
		$Room_Label->removeDecorator('HtmlTag');

		$terms = new Zend_Form_Element_File('terms');


		$this->setAttrib('enctype', 'multipart/form-data');
		
		$this->addElements(array(
				$chargesRate, $approvalHostelProcess, $revertHostelProcess,
				$eServicesHostelProcess, $generateHostelBill, $extendStayPeriod,
				$Add,$clear,$Block_Label,$Level_Label,$Room_Label, $terms
		));
	}

}

?>
