<?php
class Hostel_Form_Roomreg extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Itemregister SETUP
	 * @author: Vipul
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		//$month= date("m"); // Month value
		//$day= date("d"); //today's date
		//$year= date("Y"); // Year value
		//$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateformat = "{datePattern:'dd-MM-yyyy'}";

		$IdHostelRoom = new Zend_Form_Element_Hidden('IdHostelRoom');
		$IdHostelRoom->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		//		$SemesterCode = new Zend_Form_Element_Text('SemesterCode');
		//                $SemesterCode->setAttrib('dojoType',"dijit.form.TextBox");
		//		$SemesterCode   ->setAttrib('readonly',"false")
		//				->removeDecorator("DtDdWrapper")
		//				->removeDecorator("Label")
		//				->removeDecorator('HtmlTag');

		$SemesterCode = new Zend_Dojo_Form_Element_FilteringSelect('SemesterCode',array('promptMessage'=>"please select semester"));
		$SemesterCode->removeDecorator("DtDdWrapper");
		$SemesterCode->setAttrib('required',"false") ;
		$SemesterCode->removeDecorator("Label");
		$SemesterCode->removeDecorator('HtmlTag');
		$SemesterCode->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$Room = new Zend_Form_Element_Text('Room',array('promptMessage'=>"please select room"));
		$Room->setAttrib('dojoType',"dijit.form.TextBox");
		$Room            ->setAttrib('readonly',"false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		// $todaysDate = new Date();

		$CheckInDate = new Zend_Form_Element_Text('CheckInDate',array('promptMessage'=>"please select check in date"));
		$CheckInDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$CheckInDate->setAttrib('onChange',"showkeytakendate(this.value)");
		//$CheckInDate->setAttrib('onChange',"dijit.byId('CheckOutDate').constraints.min = arguments[0] ;");
		$CheckInDate->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$CheckOutDate = new Zend_Form_Element_Text('CheckOutDate',array('promptMessage'=>"please select check out date"));
		$CheckOutDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$CheckOutDate->setAttrib('onChange',"showkeyreturndate(this.value)");
		//$CheckOutDate->setAttrib('onChange',"dijit.byId('CheckInDate').constraints.max = arguments[0];") ;
		$CheckOutDate->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$RoomKeyTakenDate = new Zend_Form_Element_Text('RoomKeyTakenDate');
		$RoomKeyTakenDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$RoomKeyTakenDate->setAttrib('onChange',"dijit.byId('SemesterMainEndDate').constraints.min = arguments[0];");
		$RoomKeyTakenDate->setAttrib('required',"false")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$RoomKeyReturnDate = new Zend_Form_Element_Text('RoomKeyReturnDate');
		$RoomKeyReturnDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$RoomKeyReturnDate->setAttrib('onChange',"dijit.byId('SemesterMainEndDate').constraints.min = arguments[0];");
		$RoomKeyReturnDate->setAttrib('required',"false")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');



		$CheckInbutton = new Zend_Form_Element_Button('CheckIn');
		$CheckInbutton->label = $gstrtranslate->_("Check In");
		$CheckInbutton->dojotype="dijit.form.Button";
		$CheckInbutton->removeDecorator("DtDdWrapper");
		$CheckInbutton->setAttrib('onClick',"savecheckin();");
		$CheckInbutton->removeDecorator('HtmlTag');
		//->class = "NormalBtn";


		$CheckOutbutton = new Zend_Form_Element_Button('CheckOut');
		$CheckOutbutton->label = $gstrtranslate->_("Check Out");
		$CheckOutbutton->dojotype="dijit.form.Button";
		$CheckOutbutton->removeDecorator("DtDdWrapper");
		$CheckOutbutton->setAttrib('onClick',"savecheckout();");
		$CheckOutbutton->removeDecorator('HtmlTag');
		//->class = "NormalBtn";


		$ChangeRoom = new Zend_Form_Element_Button('ChangeRoom');
		$ChangeRoom->label = $gstrtranslate->_("Change Room");
		$ChangeRoom->dojotype="dijit.form.Button";
		$ChangeRoom->removeDecorator("DtDdWrapper");
		$ChangeRoom->setAttrib('onClick',"savechangeroom();");
		$ChangeRoom->removeDecorator('HtmlTag');


		$OutCampus = new Zend_Form_Element_Button('OutCampus');
		$OutCampus->label = $gstrtranslate->_("Out Campus");
		$OutCampus->dojotype="dijit.form.Button";
		$OutCampus->removeDecorator("DtDdWrapper");
		$OutCampus->setAttrib('onClick',"showhideoca();");
		$OutCampus->removeDecorator('HtmlTag');
		//->class = "NormalBtn";


		$CancelOutCampus = new Zend_Form_Element_Button('CancelOutCampus');
		$CancelOutCampus->label = $gstrtranslate->_("Cancel Out Campus");
		$CancelOutCampus->dojotype="dijit.form.Button";
		$CancelOutCampus->removeDecorator("DtDdWrapper");
		$CancelOutCampus->setAttrib('onClick',"savecanceloutcampus();");
		$CancelOutCampus->removeDecorator('HtmlTag');
		//->class = "NormalBtn";

		$PrintSlip = new Zend_Form_Element_Button('PrintSlip');
		$PrintSlip->label = $gstrtranslate->_("Print Slip");
		$PrintSlip->dojotype="dijit.form.Button";
		$PrintSlip->removeDecorator("DtDdWrapper");
		$PrintSlip->setAttrib('onClick',"saveprintslip();");
		$PrintSlip->removeDecorator('HtmlTag');
		//->class = "NormalBtn";


		$checkinCB = new Zend_Form_Element_Checkbox('checkinCB');
		$checkinCB->setAttrib('dojoType', "dijit.form.CheckBox");
		$checkinCB->setAttrib('onChange',"getcurrentDateForCIN();");
		$checkinCB->setvalue('0');
		$checkinCB->removeDecorator("DtDdWrapper");
		$checkinCB->removeDecorator("Label");
		$checkinCB->removeDecorator('HtmlTag');


		$checkoutCB = new Zend_Form_Element_Checkbox('checkoutCB');
		$checkoutCB->setAttrib('dojoType', "dijit.form.CheckBox");
		$checkoutCB->setvalue('0');
		$checkoutCB->setAttrib('onChange',"getcurrentDateForCOUT();");
		$checkoutCB->removeDecorator("DtDdWrapper");
		$checkoutCB->removeDecorator("Label");
		$checkoutCB->removeDecorator('HtmlTag');

		$IdStudentRegistration = new Zend_Form_Element_Hidden('IdStudentRegistration');
		$IdStudentRegistration->removeDecorator("DtDdWrapper");
		$IdStudentRegistration->removeDecorator("Label");
		$IdStudentRegistration->removeDecorator('HtmlTag');

		$OutcampusAddressDetails = new Zend_Form_Element_Text('OutcampusAddressDetails');
		$OutcampusAddressDetails->setAttrib('maxlength', '100');
		$OutcampusAddressDetails->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails->setAttrib('required', "false")->setAttrib('trim', "true");
		$OutcampusAddressDetails->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$OutcampusAddressDetails->removeDecorator("Label");
		$OutcampusAddressDetails->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails->removeDecorator('HtmlTag');

		$OutcampusCity = new Zend_Dojo_Form_Element_FilteringSelect('OutcampusCity');
		$OutcampusCity->removeDecorator("DtDdWrapper");
		$OutcampusCity->removeDecorator("Label");
		$OutcampusCity->removeDecorator('HtmlTag');
		$OutcampusCity->setAttrib('required', "false");
		$OutcampusCity->setRegisterInArrayValidator(false);
		$OutcampusCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$OutcampusState = new Zend_Form_Element_Select('OutcampusState');
		$OutcampusState->setRegisterInArrayValidator(false);
		$OutcampusState->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OutcampusState->setAttrib('OnChange', 'fnGetStateCityListPerm');
		$OutcampusState->setAttrib('required', "false");
		$OutcampusState->removeDecorator("DtDdWrapper");
		$OutcampusState->removeDecorator("Label");
		$OutcampusState->removeDecorator('HtmlTag');

		$OutcampusCountry = new Zend_Form_Element_Select('OutcampusCountry');
		$OutcampusCountry->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OutcampusCountry->setAttrib('OnChange', "fnGetPermCountryStateList(this,'OutcampusState')");
		$OutcampusCountry->setAttrib('required', "false");
		$OutcampusCountry->removeDecorator("DtDdWrapper");
		$OutcampusCountry->removeDecorator("Label");
		$OutcampusCountry->removeDecorator('HtmlTag');

		$OutcampusZip = new Zend_Form_Element_Text('OutcampusZip');
		$OutcampusZip->setAttrib('maxlength', '20');
		$OutcampusZip->setAttrib('dojoType', "dijit.form.TextBox");
		$OutcampusZip->removeDecorator("DtDdWrapper");
		$OutcampusZip->removeDecorator("Label");
		$OutcampusZip->removeDecorator('HtmlTag');


		

		$HomePhone = new Zend_Form_Element_Text('HomePhone');
		$HomePhone->setAttrib('style', 'width:93px')->setAttrib('maxlength', '30')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		

		$CellPhone = new Zend_Form_Element_Text('CellPhone');
		$CellPhone->setAttrib('style', 'width:109px')->setAttrib('maxlength', '30')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');





		$SaveAddress = new Zend_Form_Element_Button('SaveAddress');
		$SaveAddress->label = $gstrtranslate->_("Save Address");
		$SaveAddress->dojotype="dijit.form.Button";
		$SaveAddress->setAttrib('onClick',"saveoutcampusaddress();");
		$SaveAddress->removeDecorator("DtDdWrapper");
		$SaveAddress->removeDecorator('HtmlTag');

		$Item = new Zend_Dojo_Form_Element_FilteringSelect('Item');
		$Item->removeDecorator("DtDdWrapper");
		$Item->removeDecorator("Label");
		$Item->removeDecorator('HtmlTag');
		$Item->setAttrib('required',"true");
		$Item->setRegisterInArrayValidator(false);
		$Item->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Quantity = new Zend_Form_Element_Text('Quantity',array('regExp'=>"^[1-9]\d{0,2}(\d*|(,\d{3})*)$",'invalidMessage'=>"Value should be numeric, min 1 "));
		$Quantity->removeDecorator("DtDdWrapper");
		$Quantity->setAttrib('required',"true") ;
		$Quantity->setAttrib('maxlength',"2") ;
		$Quantity->setValue("1") ;
		$Quantity->removeDecorator("Label");
		$Quantity->removeDecorator('HtmlTag');
		$Quantity->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$PrintData = new Zend_Form_Element_Button('PrintData');
		$PrintData->label = $gstrtranslate->_("Print");
		$PrintData->dojotype="dijit.form.Button";
		$PrintData->setAttrib('onClick',"PrintDiv();");
		$PrintData->removeDecorator("DtDdWrapper");
		$PrintData->removeDecorator('HtmlTag');


		$RemarkChangeRoom = new Zend_Form_Element_Text('RemarkChangeRoom',array('promptMessage'=>"please enter remark for Change Room"));
		$RemarkChangeRoom->setAttrib('dojoType',"dijit.form.ValidationTextBox")
							->setAttrib('required', "false")
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label")
							->removeDecorator('HtmlTag');

		$ChargingPerRoom = new Zend_Form_Element_Checkbox('ChargingPerRoom');
		$ChargingPerRoom->setAttrib('dojoType', "dijit.form.CheckBox");
		$ChargingPerRoom->setCheckedValue('1') ;
		$ChargingPerRoom->setUnCheckedValue('0') ;
		$ChargingPerRoom->setChecked(true);
		$ChargingPerRoom->removeDecorator("DtDdWrapper");
		$ChargingPerRoom->removeDecorator("Label");
		$ChargingPerRoom->removeDecorator('HtmlTag');

		$this->addElements(array(
				$SemesterCode, $Room, $CheckInDate , $CheckOutDate, $RoomKeyTakenDate, $RoomKeyReturnDate,
				$CheckInbutton, $CheckOutbutton, $ChangeRoom, $OutCampus,$CancelOutCampus,
				$PrintSlip, $checkinCB, $checkoutCB, $IdStudentRegistration,
				$OutcampusAddressDetails, $OutcampusCity, $OutcampusState, 
				$OutcampusCountry, $OutcampusZip, $SaveAddress,
				$HomePhone, $CellPhone,$Item, $Quantity,
				$IdHostelRoom , $PrintData, $RemarkChangeRoom, $ChargingPerRoom
		));
	}

}

?>
