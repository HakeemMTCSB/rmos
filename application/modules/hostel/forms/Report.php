<?php
class Hostel_Form_Report extends Zend_Dojo_Form {
	
	public function init() 
	{
		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		
	
		$ResBlock = new Zend_Dojo_Form_Element_FilteringSelect('ResBlock');
		$ResBlock->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('onchange', "getLevel(this,'ResLevel')")
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$ResLevel = new Zend_Dojo_Form_Element_FilteringSelect('ResLevel');
		$ResLevel->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('onchange', "getRoom('ResBlock','ResLevel','ResRoom')")
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$ResRoom = new Zend_Dojo_Form_Element_FilteringSelect('ResRoom');
		$ResRoom->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$ResGender = new Zend_Dojo_Form_Element_FilteringSelect('ResGender');
		$ResGender->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		
		//REGISTRATIONS
		$RegBlock = new Zend_Dojo_Form_Element_FilteringSelect('RegBlock');
		$RegBlock->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('onchange', "getLevel(this,'RegLevel')")
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$RegLevel = new Zend_Dojo_Form_Element_FilteringSelect('RegLevel');
		$RegLevel->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('onchange', "getRoom('RegBlock','RegLevel','RegRoom')")
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$RegRoom = new Zend_Dojo_Form_Element_FilteringSelect('RegRoom');
		$RegRoom->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$RegGender = new Zend_Dojo_Form_Element_FilteringSelect('RegGender');
		$RegGender->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		
		// GENDER
		$genderlist = array(
								array('key' => 0, 'value' => $gstrtranslate->translate('Female')),
								array('key' => 1, 'value' => $gstrtranslate->translate('Male'))
							);
							
		$ResGender->addMultioptions($genderlist);
		$RegGender->addMultioptions($genderlist);
		
		$this->addElements(array(
				$ResBlock,
				$ResLevel,
				$ResRoom,
				$ResGender,
				
				$RegBlock,
				$RegLevel,
				$RegRoom,
				$RegGender
		));

	}
}