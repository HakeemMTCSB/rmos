<?php
class Hostel_Form_Chargessetup extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Hostel Reservation SETUP
	 * @author: Dushyant
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		$lobjCurrency = new Hostel_Model_DbTable_Hostelconfig();
		
		$IdHostelRoomType  = new Zend_Form_Element_Hidden('IdHostelRoomType');
		$IdHostelRoomType->removeDecorator("DtDdWrapper");
		$IdHostelRoomType->setAttrib('dojoType',"dijit.form.TextBox");
		$IdHostelRoomType->removeDecorator("Label");
		$IdHostelRoomType->removeDecorator('HtmlTag');

		$RoomType  = new Zend_Form_Element_Text('RoomType');
		$RoomType->removeDecorator("DtDdWrapper");
		$RoomType->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomType->setAttrib('required',"true");
		$RoomType->setAttrib('trim',"true");
		$RoomType->removeDecorator("Label");
		$RoomType->removeDecorator('HtmlTag');
		$RoomType->setAttrib('propercase',"true");

		$RoomCode = new Zend_Form_Element_Text('RoomCode');
		$RoomCode->removeDecorator("DtDdWrapper");
		$RoomCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomCode->setAttrib('required',"true")->setAttrib('trim',"true");
		$RoomCode->removeDecorator("Label");
		$RoomCode->removeDecorator('HtmlTag');

		$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->removeDecorator("DtDdWrapper")->setAttrib('trim',"true");
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ShortName->setAttrib('required',"true");
		$ShortName->removeDecorator("Label");
		$ShortName->removeDecorator('HtmlTag');
		$ShortName->setAttrib('propercase',"true");

		$DefaultLang = new Zend_Form_Element_Text('DefaultLang');
		$DefaultLang->removeDecorator("DtDdWrapper");
		$DefaultLang->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$DefaultLang->setAttrib('required',"false");
		$DefaultLang->removeDecorator("Label");
		$DefaultLang->removeDecorator('HtmlTag');

		$SemesterCode = new Zend_Dojo_Form_Element_FilteringSelect('SemesterCode');
		$SemesterCode->removeDecorator("DtDdWrapper");
		$SemesterCode->setAttrib('required', "true");
		$SemesterCode->setAttrib('onchange', "getdates(this)");
		$SemesterCode->removeDecorator("Label");
		$SemesterCode->removeDecorator('HtmlTag');
		$SemesterCode->setRegisterInArrayValidator(false);
		$SemesterCode->setAttrib('dojoType', "dijit.form.FilteringSelect");


		$IdIntake = new Zend_Dojo_Form_Element_FilteringSelect('IdIntake');
		$IdIntake->removeDecorator("DtDdWrapper");
		$IdIntake->setAttrib('required', "true");
		$IdIntake->removeDecorator("Label");
		$IdIntake->removeDecorator('HtmlTag');
		$IdIntake->setRegisterInArrayValidator(false);
		$IdIntake->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Rate = new Zend_Form_Element_Text('Rate');
		$Rate->removeDecorator("DtDdWrapper");
		$Rate->setAttrib('required',"true") ;
		$Rate->setAttrib('constraints', '{min:1,max:9999,pattern:"#.##"}');
		$Rate->setAttrib('invalidMessage', 'Only numbers greater than/equal to 1 upto two decimal places is allowed');
		$Rate->removeDecorator("Label");
		$Rate->removeDecorator('HtmlTag');
		$Rate->setAttrib('dojoType',"dijit.form.NumberTextBox");

		$Deposit = new Zend_Form_Element_Text('Deposit');
		$Deposit->removeDecorator("DtDdWrapper");
		$Deposit->setAttrib('required',"true") ;
		$Deposit->setAttrib('constraints', '{min:1,max:99999999,pattern:"#.##"}');
		$Deposit->setAttrib('invalidMessage', 'Only numbers greater than/equal to 1 upto two decimal places is allowed');
		$Deposit->removeDecorator("Label");
		$Deposit->removeDecorator('HtmlTag');
		$Deposit->setAttrib('dojoType',"dijit.form.NumberTextBox");

		$Admin = new Zend_Form_Element_Text('Admin');
		$Admin->removeDecorator("DtDdWrapper");
		$Admin->setAttrib('required',"true") ;
		$Admin->setAttrib('constraints', '{min:1,max:99999999,pattern:"#.##"}');
		$Admin->setAttrib('invalidMessage', 'Only numbers greater than/equal to 1 upto two decimal places is allowed');
		$Admin->removeDecorator("Label");
		$Admin->removeDecorator('HtmlTag');
		$Admin->setAttrib('dojoType',"dijit.form.NumberTextBox");


		$Definition = new App_Model_General_DbTable_Definationms();
		$tenancy_period = new Zend_Dojo_Form_Element_FilteringSelect('tenancy_period');
		$tenancy_period->removeDecorator("DtDdWrapper")
			->setAttrib('required', "true")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag')
			->setRegisterInArrayValidator(false)
			->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$tenancy_periods = array_merge(array(array('key' => '', 'value' => $gstrtranslate->_('Select...') )),  $Definition->getByCodeAsList('Hostel Configuration Charges Rate'));
		foreach($tenancy_periods as $t_period) {
			$tenancy_period->addMultiOption($t_period['key'], $t_period['value']);
		}


		$Definition = new App_Model_General_DbTable_Definationms();
		$OccupancyType = new Zend_Dojo_Form_Element_FilteringSelect('OccupancyType');
		$OccupancyType->removeDecorator("DtDdWrapper")
			->setAttrib('required', "true")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag')
			->setRegisterInArrayValidator(false)
			->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OccupancyTypes = array_merge(array(array('key' => '', 'value' => $gstrtranslate->_('Select...') )),  $Definition->getByCodeAsList('Occupancy Type'));
		foreach($OccupancyTypes as $t_period) {
			$OccupancyType->addMultiOption($t_period['key'], $t_period['value']);
		}

		$Definition = new App_Model_General_DbTable_Definationms();
		$RoomFeature = new Zend_Dojo_Form_Element_FilteringSelect('RoomFeature');
		$RoomFeature->removeDecorator("DtDdWrapper")
			->setAttrib('required', "true")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag')
			->setRegisterInArrayValidator(false)
			->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$RoomFeatures = array_merge(array(array('key' => '', 'value' => $gstrtranslate->_('Select...') )),  $Definition->getByCodeAsList('Room Type'));
		foreach($RoomFeatures as $t_period) {
			$RoomFeature->addMultiOption($t_period['key'], $t_period['value']);
		}


		$CustomerType = new Zend_Dojo_Form_Element_FilteringSelect('CustomerType');
		$CustomerType->removeDecorator("DtDdWrapper");
		$CustomerType->setAttrib('required', "true");
		$CustomerType->removeDecorator("Label");
		$CustomerType->removeDecorator('HtmlTag');
		$CustomerType->setRegisterInArrayValidator(false);
		$CustomerType->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper");
		$UpdUser->removeDecorator("Label");
		$UpdUser->removeDecorator('HtmlTag');

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType', "dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearpageAdd()');
		$clear->removeDecorator("Label");
		$clear->removeDecorator("DtDdWrapper");
		$clear->removeDecorator('HtmlTag');

		$Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('class', 'NormalBtn');
		$Add->setAttrib('dojoType', "dijit.form.Button");
		$Add->setAttrib('OnClick', 'addchargesetupinfo()');
		$Add->removeDecorator("Label");
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype = "dijit.form.Button";
		$Save->setAttrib('OnClick', 'return validateform()');
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag');
		$Save->class = "NormalBtn";


		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateformat = "{datePattern:'dd-MM-yyyy'}";

		$StartDate = new Zend_Form_Element_Text('StartDate');
		$StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$StartDate->setAttrib('onChange',"dijit.byId('EndDate').constraints.min = arguments[0];");
		$StartDate->setAttrib('required',"true")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$EndDate = new Zend_Form_Element_Text('EndDate');
		$EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$EndDate->setAttrib('onChange',"dijit.byId('StartDate').constraints.max = arguments[0];") ;
		$EndDate->setAttrib('required',"false")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		$Currency = new Zend_Form_Element_Select('Currency');
		$Currency->removeDecorator("DtDdWrapper");
		$Currency->setAttrib('required',"false");
		$Currency->removeDecorator("Label");
		$Currency->removeDecorator('HtmlTag');
		$Currency->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$deafult = "";
		$lobjcurrency = $lobjCurrency->fnGetCurrency($deafult);
		foreach($lobjcurrency as $larrvalues) {
			$Currency->addMultiOption($larrvalues['cur_id'],$larrvalues['cur_desc']);
		}

		$this->addElements(array(
				$IdHostelRoomType,
				$RoomType,
				$RoomCode,
				$ShortName,
				$DefaultLang,
				$SemesterCode,$IdIntake,
				$Rate,
				$CustomerType,
				$UpdDate,
				$UpdUser,
				$clear,
				$Currency,
				$Add,
				$tenancy_period,
				$OccupancyType,
				$RoomFeature,
				$Deposit,
				$Admin,
				$Save,$StartDate,$EndDate
		));

	}
}