<?php
class Hostel_Form_Reservationsetup extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Hostel Reservation SETUP
	 * @author: Dushyant
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');

		$IdReservation  = new Zend_Form_Element_Hidden('IdReservation');
		$IdReservation->removeDecorator("DtDdWrapper");
		$IdReservation->setAttrib('dojoType',"dijit.form.TextBox");
		$IdReservation->removeDecorator("Label");
		$IdReservation->removeDecorator('HtmlTag');

		$SemesterCode = new Zend_Dojo_Form_Element_FilteringSelect('SemesterCode');
		$SemesterCode->removeDecorator("DtDdWrapper");
		$SemesterCode->setAttrib('required', "true");
		$SemesterCode->removeDecorator("Label");
		$SemesterCode->removeDecorator('HtmlTag');
		$SemesterCode->setRegisterInArrayValidator(false);
		$SemesterCode->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Priority = new Zend_Dojo_Form_Element_FilteringSelect('Priority');
		$Priority->removeDecorator("DtDdWrapper");
		$Priority->setAttrib('required', "true");
		$Priority->removeDecorator("Label");
		$Priority->removeDecorator('HtmlTag');
		$Priority->setRegisterInArrayValidator(false);
		$Priority->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$ValueType = new Zend_Dojo_Form_Element_FilteringSelect('ValueType');
		$ValueType->removeDecorator("DtDdWrapper");
		$ValueType->setAttrib('required', "true");
		$ValueType->setAttrib('onchange', 'getvalueId()');
		$ValueType->removeDecorator("Label");
		$ValueType->removeDecorator('HtmlTag');
		$ValueType->setRegisterInArrayValidator(false);
		$ValueType->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Condition = new Zend_Dojo_Form_Element_FilteringSelect('Condition');
		$Condition->removeDecorator("DtDdWrapper");
		$Condition->setAttrib('required', "true");
		$Condition->removeDecorator("Label");
		$Condition->removeDecorator('HtmlTag');
		$Condition->setRegisterInArrayValidator(false);
		$Condition->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$ValueId = new Zend_Dojo_Form_Element_FilteringSelect('ValueId');
		$ValueId->removeDecorator("DtDdWrapper");
		$ValueId->setAttrib('required', "true");
		$ValueId->removeDecorator("Label");
		$ValueId->removeDecorator('HtmlTag');
		$ValueId->setRegisterInArrayValidator(false);
		$ValueId->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper");
		$UpdUser->removeDecorator("Label");
		$UpdUser->removeDecorator('HtmlTag');

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType', "dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearpageAdd()');
		$clear->removeDecorator("Label");
		$clear->removeDecorator("DtDdWrapper");
		$clear->removeDecorator('HtmlTag');

		$Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('class', 'NormalBtn');
		$Add->setAttrib('dojoType', "dijit.form.Button");
		$Add->setAttrib('OnClick', 'addReservationinfo()');
		$Add->removeDecorator("Label");
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype = "dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag');
		$Save->class = "NormalBtn";

		$Back = new Zend_Form_Element_Button('Back');
		$Back->label = $gstrtranslate->_("Back");
		$Back->dojotype = "dijit.form.Button";
		$Back->setAttrib('OnClick', 'goback()');
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag');
		$Back->class = "NormalBtn";

		$this->addElements(array(
				$IdReservation,
				$SemesterCode,
				$Priority,
				$ValueType,
				$Condition,
				$ValueId,
				$UpdDate,
				$UpdUser,
				$clear,
				$Add,
				$Save,$Back
		));

	}
}