<?php
class Hostel_Form_Hostelinfo extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Itemregister SETUP
	 * @author: Vipul
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');

		$Name = new Zend_Form_Element_Text('Name');
		$Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Name->setAttrib('required',"true")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('trim',"true")->setAttrib('propercase', "true");

		$IdHostelRoom = new Zend_Form_Element_Hidden('IdHostelRoom');
		$IdHostelRoom->setAttrib('dojoType',"dijit.form.TextBox");
		$IdHostelRoom->setAttrib('required',"true")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Code = new Zend_Form_Element_Text('Code');
		$Code->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Code->setAttrib('required',"true")
		//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('trim',"true");


		$DefaultLang = new Zend_Form_Element_Text('DefaultLang');
		$DefaultLang->setAttrib('dojoType',"dijit.form.TextBox");
		$DefaultLang             //->setAttrib('required',"true")
		//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ShortName->setAttrib('required',"true")//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('trim',"true")->setAttrib('propercase', "true");
			
			
		$Address1 = new Zend_Form_Element_Text('Address1');
		$Address1->setAttrib('required', "true")
		->setAttrib('dojoType', "dijit.form.ValidationTextBox")
		->setAttrib('maxlength', '100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('trim',"true")->setAttrib('propercase', "true");
			

		$Address2 = new Zend_Form_Element_Text('Address2');
		$Address2->setAttrib('dojoType', "dijit.form.TextBox")
		->setAttrib('maxlength', '100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('propercase', "true");


		$Country = new Zend_Form_Element_Select('Country');
		$Country->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('required',"false")
		->setAttrib('OnChange', "fnGetPermCountryStateList(this,'State')")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$State = new Zend_Form_Element_Select('State');
		$State->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('required',"false")
		->setRegisterInArrayValidator(false)
		->setAttrib('OnChange', 'fnGetStateCityListPerm')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$City = new Zend_Dojo_Form_Element_FilteringSelect('City');
		$City->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");


		$Zipcode = new Zend_Form_Element_Text('Zipcode');
		$Zipcode->setAttrib('maxlength', '10')
		->setAttrib('dojoType', "dijit.form.TextBox")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		//		$Phone = new Zend_Form_Element_Text('Phone', array ('regExp' => "[0-9]+",'invalidMessage' => "Only digits"
		//		));
		//		$Phone->setAttrib('maxlength', '20')->setAttrib('required',"true")
		//		->setAttrib('dojoType', "dijit.form.ValidationTextBox")
		//		->removeDecorator("DtDdWrapper")
		//		->removeDecorator("Label")
		//		->removeDecorator('HtmlTag');



		$HomePhonecountrycode = new Zend_Form_Element_Text('HomePhonecountrycode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$HomePhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomePhonecountrycode->setAttrib('maxlength', '3')->setAttrib('required',"true")->setAttrib('trim',"true");
		$HomePhonecountrycode->setAttrib('style', 'width:30px');
		$HomePhonecountrycode->removeDecorator("DtDdWrapper");
		$HomePhonecountrycode->removeDecorator("Label");
		$HomePhonecountrycode->removeDecorator('HtmlTag');

		$HomePhonestatecode = new Zend_Form_Element_Text('HomePhonestatecode', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$HomePhonestatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomePhonestatecode->setAttrib('maxlength', '2')->setAttrib('required',"true")->setAttrib('trim',"true");
		$HomePhonestatecode->setAttrib('style', 'width:30px');
		$HomePhonestatecode->removeDecorator("DtDdWrapper");
		$HomePhonestatecode->removeDecorator("Label");
		$HomePhonestatecode->removeDecorator('HtmlTag');

		$HomePhone = new Zend_Form_Element_Text('HomePhone', array (
				'regExp' => '[0-9]+'
		));
		$HomePhone->setAttrib('style', 'width:93px')->setAttrib('maxlength', '9')->setAttrib('required',"true")->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag')->setAttrib('trim',"true");



		$faxcountrycode = new Zend_Form_Element_Text('faxcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$faxcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$faxcountrycode->setAttrib('maxlength','3');
		$faxcountrycode->setAttrib('style','width:30px');
		$faxcountrycode->removeDecorator("DtDdWrapper");
		$faxcountrycode->removeDecorator("Label");
		$faxcountrycode->removeDecorator('HtmlTag');

		$faxstatecode = new Zend_Form_Element_Text('faxstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$faxstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$faxstatecode->setAttrib('maxlength','5');
		$faxstatecode->setAttrib('style','width:30px');
		$faxstatecode->removeDecorator("DtDdWrapper");
		$faxstatecode->removeDecorator("Label");
		$faxstatecode->removeDecorator('HtmlTag');

		$Fax = new Zend_Form_Element_Text('Fax',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Fax"));
		$Fax->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Fax->setAttrib('style','width:93px');
		$Fax->setAttrib('maxlength','20');
		$Fax->removeDecorator("DtDdWrapper");
		$Fax->removeDecorator("Label");
		$Fax->removeDecorator('HtmlTag');

		$Gender = new Zend_Form_Element_Select('Gender');
		$Gender->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Gender->removeDecorator("DtDdWrapper");
		$Gender->setAttrib('required',"true");
		$Gender->removeDecorator("Label");
		$Gender->removeDecorator('HtmlTag');
		$Gender->setAttrib('dojoType',"dijit.form.FilteringSelect");


        $PropertyOwner = new Zend_Form_Element_Text('PropertyOwner');
        $PropertyOwner->setAttrib('dojoType', "dijit.form.TextBox")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');
		//		$Fax = new Zend_Form_Element_Text('Fax');
		//		$Fax->setAttrib('maxlength', '30')
		//		->setAttrib('dojoType', "dijit.form.TextBox")
		//		->removeDecorator("DtDdWrapper")
		//		->removeDecorator("Label")
		//		->removeDecorator('HtmlTag');
		//
			
			


		$Search = new Zend_Form_Element_Submit('Search');
		$Search->label = $gstrtranslate->_("Search");
		$Search->dojotype="dijit.form.Button";
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->setAttrib('OnClick', 'addrecords()');
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType', "dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearpageAdd()');
		$clear->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		
		$RoomName = new Zend_Form_Element_Text('RoomName');
		$RoomName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomName->setAttrib('required',"true")->setAttrib('trim',"true")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('propercase', "true");

		$RoomCode = new Zend_Form_Element_Text('RoomCode');
		$RoomCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomCode->setAttrib('required',"true")->setAttrib('trim',"true")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Level = new Zend_Dojo_Form_Element_FilteringSelect('Level');
		$Level->removeDecorator("DtDdWrapper")->setAttrib('required',"true")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Block = new Zend_Dojo_Form_Element_FilteringSelect('Block');
		$Block->removeDecorator("DtDdWrapper")->setAttrib('required',"true")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('onchange', "getLevel(this)")
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Capacity = new Zend_Form_Element_Text('Capacity',array('regExp'=>"^[1-9]\d{0,2}(\d*|(,\d{3})*)$",'invalidMessage'=>"Only digits"));
		$Capacity->removeDecorator("DtDdWrapper");
		$Capacity->setAttrib('required',"true")->setAttrib('trim',"true") ;
		$Capacity->setValue("1") ;
		$Capacity->setAttrib('maxlength','1');
		$Capacity->removeDecorator("Label");
		$Capacity->removeDecorator('HtmlTag');
		$Capacity->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$RoomType = new Zend_Dojo_Form_Element_FilteringSelect('RoomType');
		$RoomType->removeDecorator("DtDdWrapper")->setAttrib('required',"true")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$InventoryName = new Zend_Dojo_Form_Element_FilteringSelect('InventoryName');
		$InventoryName->removeDecorator("DtDdWrapper")->setAttrib('required',"true")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$Quantity = new Zend_Form_Element_Text('Quantity',array('regExp'=>"^[1-9]\d{0,2}(\d*|(,\d{3})*)$",'invalidMessage'=>"Only digits"));
		$Quantity->removeDecorator("DtDdWrapper");
		$Quantity->setAttrib('required',"true")->setAttrib('trim',"true") ;
		$Quantity->setValue("1") ;
		$Quantity->setAttrib('maxlength','2');
		$Quantity->removeDecorator("Label");
		$Quantity->removeDecorator('HtmlTag');
		$Quantity->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

		$Save1 = new Zend_Form_Element_Submit('Save1');
		$Save1->label = $gstrtranslate->_("Save");
		$Save1->dojotype="dijit.form.Button";
		$Save1->removeDecorator("DtDdWrapper");
		$Save1->setAttrib('onclick',"return validateform()") ;
		$Save1->removeDecorator('HtmlTag')
		->class = "NormalBtn";


			
		$this->addElements(array(
				$Name, $Code, $ShortName,
				$DefaultLang, $Search,
				$Add, $Save, $clear,
				$Address1, $Address2, $Country, $State, $City, $Zipcode, $HomePhonecountrycode, $HomePhonestatecode, $HomePhone,
				$faxcountrycode, $faxstatecode, $Fax, $RoomName,$Gender, $PropertyOwner,
				$RoomCode, $Level, $Block, $Capacity, $RoomType, $InventoryName, $Quantity, $Save1, $IdHostelRoom
				,$Active
		));
	}

}

?>
