<?php
class Hostel_Form_Inventorysetup extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Inventory SETUP
	 * @author: Vipul
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');

		$Name = new Zend_Form_Element_Text('Name');
		$Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Name->setAttrib('required',"true")
		//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('propercase', "true");
			
		$InvCode = new Zend_Form_Element_Hidden('InvCode');
		$InvCode->setAttrib('dojoType',"dijit.form.TextBox");
		$InvCode->setAttrib('required',"false")
		//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$Code = new Zend_Form_Element_Text('Code');
		$Code->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Code->setAttrib('required',"true")
		//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$DefaultLang = new Zend_Form_Element_Text('DefaultLang');
		$DefaultLang->setAttrib('dojoType',"dijit.form.TextBox");
		$DefaultLang             //->setAttrib('required',"true")
		//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ShortName->setAttrib('required',"true")//->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')->setAttrib('propercase', "true");


		$Search = new Zend_Form_Element_Submit('Search');
		$Search->label = $gstrtranslate->_("Search");
		$Search->dojotype="dijit.form.Button";
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->setAttrib('OnClick', 'addrecords()');
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType', "dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearpageAdd()');
		$clear->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');


		$this->addElements(array(
				$Name, $Code, $ShortName,
				$DefaultLang, $Search,
				$Add,$Save,$clear,$InvCode
		));
	}

}

?>
