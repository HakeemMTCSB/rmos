<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hostel_Form_ReportHostelSearch extends Zend_Dojo_Form {
    
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        $blockid = $this->getAttrib('blockid');
        
        $model = new Hostel_Model_DbTable_ReportHostel();
        
        //block
        $block = new Zend_Form_Element_Select('block');
	$block->removeDecorator("DtDdWrapper");
        $block->setAttrib('class', 'select');
        $block->setAttrib('required', true);
	$block->removeDecorator("Label");
        $block->setAttrib('onchange', 'getLevel(this.value);');
        
        $block->addMultiOption('', '-- Select --');
        
        $blockList = $model->getBlockList();
        
        if ($blockList){
            foreach ($blockList as $blockLoop){
                $block->addMultiOption($blockLoop['IdHostelBlock'], $blockLoop['Name']);
            }
        }
        
        //level
        $level = new Zend_Form_Element_Select('level');
	$level->removeDecorator("DtDdWrapper");
        $level->setAttrib('class', 'select');
	$level->removeDecorator("Label");
        //$level->setAttrib('onchange', 'getLevel(this.value);');
        
        $level->addMultiOption('', '-- Select --');
        
        if ($blockid){
            $levelList = $model->getLevel($blockid);
            
            if ($levelList){
                foreach ($levelList as $levelLoop){
                    $level->addMultiOption($levelLoop['IdHostelLevel'], $levelLoop['Name']);
                }
            }
        }
        
        //status
        $status = new Zend_Form_Element_Select('status');
	$status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'select');
	$status->removeDecorator("Label");
        
        ///$status->addMultiOption('', '-- Select --');
        $status->addMultiOption(1, 'Check In');
        $status->addMultiOption(2, 'Check Out');
        
        //date from
        $DateFrom = new Zend_Form_Element_Text('DateFrom');
	$DateFrom->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //date to
        $DateTo = new Zend_Form_Element_Text('DateTo');
	$DateTo->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $this->addElements(array(
            $block,
            $level,
            $status
        ));
    }
}