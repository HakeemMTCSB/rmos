<?php
class Hostel_Form_Vacantroomsearch extends Zend_Dojo_Form {
	
	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		$lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();

		$RoomType  = new Zend_Form_Element_Text('RoomType');
		$RoomType->removeDecorator("DtDdWrapper");
		$RoomType->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomType->setAttrib('required',"true");
		$RoomType->setAttrib('trim',"true");
		$RoomType->removeDecorator("Label");
		$RoomType->removeDecorator('HtmlTag');
		$RoomType->setAttrib('propercase',"true");

		$RoomCode = new Zend_Form_Element_Text('RoomCode');
		$RoomCode->removeDecorator("DtDdWrapper");
		$RoomCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomCode->setAttrib('required',"true")->setAttrib('trim',"true");
		$RoomCode->removeDecorator("Label");
		$RoomCode->removeDecorator('HtmlTag');

		$Block = new Zend_Dojo_Form_Element_FilteringSelect('Block');
		$Block->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('onchange', "getLevel(this)")
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$Level = new Zend_Dojo_Form_Element_FilteringSelect('Level');
		$Level->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		$clear = new Zend_Form_Element_Submit('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType', "dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->removeDecorator("Label");
		$clear->removeDecorator("DtDdWrapper");
		$clear->removeDecorator('HtmlTag');

		$Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('class', 'NormalBtn');
		$Add->setAttrib('dojoType', "dijit.form.Button");
		$Add->setAttrib('OnClick', 'addchargesetupinfo()');
		$Add->removeDecorator("Label");
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag');

		$Search = new Zend_Form_Element_Submit('Search');
		$Search->label = $gstrtranslate->_("Search");
		$Search->dojotype = "dijit.form.Button";
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator('HtmlTag');
		
		$StudentId = new Zend_Dojo_Form_Element_FilteringSelect('StudentId');
		$StudentId->removeDecorator("DtDdWrapper")->setAttrib('required',"false")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		
		
		$select[-1] = array('key'=>'','value'=>'Select'); 
		$lobjsemesterlist = $lobjSemesterModel->getList();
		$lobjsemesterlist = $select + $lobjsemesterlist;
		
		$IdSemester  = new Zend_Dojo_Form_Element_FilteringSelect('IdSemester');
		$IdSemester->removeDecorator("DtDdWrapper");
		$IdSemester->setAttrib('required',"false") ;
		$IdSemester->removeDecorator("Label");
		$IdSemester->removeDecorator('HtmlTag');
		$IdSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$IdSemester->setAttrib('onchange','getdates(this)');
		$IdSemester->addMultiOptions($lobjsemesterlist);
		
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateformat = "{datePattern:'dd-MM-yyyy'}";

		$SemesterStartDate = new Zend_Form_Element_Text('SemesterStartDate');
		$SemesterStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$SemesterStartDate->setAttrib('onChange',"dijit.byId('SemesterEndDate').constraints.min = arguments[0];");
		$SemesterStartDate->setAttrib('required',"true")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$SemesterEndDate = new Zend_Form_Element_Text('SemesterEndDate');
		$SemesterEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$SemesterEndDate->setAttrib('onChange',"dijit.byId('SemesterStartDate').constraints.max = arguments[0];") ;
		$SemesterEndDate->setAttrib('required',"true")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$room_type_id = new Zend_Form_Element_Select('room_type_id');
		$room_type_id->removeDecorator("DtDdWrapper");
		$room_type_id->removeDecorator("Label");
		$room_type_id->removeDecorator('HtmlTag');
		$room_type_id->class = "NormalBtn";
		$room_type_id->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$this->addElements(array(
				$RoomType,
				$RoomCode,
				$Block,
				$Level,
				$clear,
				$Add,
				$Search,
				$StudentId,
				$IdSemester,
				$SemesterStartDate,
				$SemesterEndDate,
				$room_type_id
				
		));

	}
}