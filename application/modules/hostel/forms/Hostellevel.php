<?php
class Hostel_Form_Hostellevel extends Zend_Dojo_Form {
	/**
	 * @see Zend_Form::init()
	 */

	/**
	 * FORM CLASS for Hostel Configuration SETUP
	 * @author: Vipul
	 */

	public function init() {

		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		$hostelBlockModel = new Hostel_Model_DbTable_Blockinfo();
		$hostelBlockList = $hostelBlockModel->fngetListofBlocks();


		$IdHostelBlock = new Zend_Dojo_Form_Element_FilteringSelect('IdHostelBlock');
		$IdHostelBlock->removeDecorator("DtDdWrapper");
		$IdHostelBlock->removeDecorator("Label");
		$IdHostelBlock->removeDecorator('HtmlTag');
		$IdHostelBlock->setAttrib('required', "true");
		$IdHostelBlock->setRegisterInArrayValidator(false);
		$IdHostelBlock->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$IdHostelBlock->addMultioptions($hostelBlockList);


		$Name = new Zend_Form_Element_Text('Name');
		$Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Name->setAttrib('required',"true")->setAttrib('trim',"true");
		$Name->setAttrib('trim',"true");
		$Name->removeDecorator("DtDdWrapper");
		$Name->removeDecorator("Label");
		$Name->removeDecorator('HtmlTag')->setAttrib('propercase', "true");

		$Code = new Zend_Form_Element_Text('Code');
		$Code->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Code->setAttrib('required',"true")->setAttrib('trim',"true");
		$Code->setAttrib('trim',"true");
		$Code->removeDecorator("DtDdWrapper");
		$Code->removeDecorator("Label");
		$Code->removeDecorator('HtmlTag');


		$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ShortName->setAttrib('required',"true");
		$ShortName->setAttrib('trim',"true");
		$ShortName->removeDecorator("DtDdWrapper");
		$ShortName->removeDecorator("Label");
		$ShortName->removeDecorator('HtmlTag')->setAttrib('propercase', "true");

        $InventoryName = new Zend_Dojo_Form_Element_FilteringSelect('InventoryName');
        $InventoryName->removeDecorator("DtDdWrapper")->setAttrib('required',"true")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setRegisterInArrayValidator(false)
            ->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $Quantity = new Zend_Form_Element_Text('Quantity',array('regExp'=>"^[1-9]\d{0,2}(\d*|(,\d{3})*)$",'invalidMessage'=>"Only digits"));
        $Quantity->removeDecorator("DtDdWrapper");
        $Quantity->setAttrib('required',"true")->setAttrib('trim',"true") ;
        $Quantity->setValue("1") ;
        $Quantity->setAttrib('maxlength','2');
        $Quantity->removeDecorator("Label");
        $Quantity->removeDecorator('HtmlTag');
        $Quantity->setAttrib('dojoType',"dijit.form.ValidationTextBox");

        $Add = new Zend_Form_Element_Button('Add');
        $Add->label = $gstrtranslate->_("Add");
        $Add->dojotype="dijit.form.Button";
        $Add->setAttrib('OnClick', 'addrecords()');
        $Add->removeDecorator("DtDdWrapper");
        $Add->removeDecorator('HtmlTag')
            ->class = "NormalBtn";


        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

		$DefaultLang = new Zend_Form_Element_Text('DefaultLang');
		$DefaultLang->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$DefaultLang->setAttrib('required',"false");
		$DefaultLang->removeDecorator("DtDdWrapper");
		$DefaultLang->removeDecorator("Label");
		$DefaultLang->removeDecorator('HtmlTag');

		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
			

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag');
		$Save->class = "NormalBtn";


		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper");
		$UpdUser->removeDecorator("Label");
		$UpdUser->removeDecorator('HtmlTag');

		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');


		$this->addElements(array(
				$IdHostelBlock, $Name, $Code,
				$ShortName, $DefaultLang,
                $InventoryName, $Quantity,
                $Add, $clear,
				$Save,$UpdDate,$UpdUser, $Active
		));
	}

}

?>

