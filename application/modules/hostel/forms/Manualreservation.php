<?php
class Hostel_Form_Manualreservation extends Zend_Dojo_Form { //Formclass for the user module
	public function init() {
		$gstrtranslate = Zend_Registry :: get('Zend_Translate');
		$lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
		
		$hostelBlockModel = new Hostel_Model_DbTable_Blockinfo();
		$tem['key'] = '';
		$tem['value'] = 'Select';
		$blockList = $hostelBlockModel->fngetListofBlocks();
		array_unshift($blockList, $tem);

		$IdHostelBlock = new Zend_Form_Element_Select('IdHostelBlock');
		$IdHostelBlock->removeDecorator("DtDdWrapper");
		$IdHostelBlock->removeDecorator("Label");
		$IdHostelBlock->removeDecorator('HtmlTag');
		$IdHostelBlock->addMultioptions($blockList);
		$IdHostelBlock->setAttrib('onChange',"getHostelLevel(this.value)");
		$IdHostelBlock->class = "NormalBtn";


		$level[0]['key'] = '';
		$level[0]['value'] = 'Select';
		$IdHostelLevel = new Zend_Form_Element_Select('IdHostelLevel');
		$IdHostelLevel->removeDecorator("DtDdWrapper");
		$IdHostelLevel->removeDecorator("Label");
		$IdHostelLevel->removeDecorator('HtmlTag');
		$IdHostelLevel->setAttrib('required',"false");
		$IdHostelLevel->addMultioptions($level);


		$RoomCode = new Zend_Form_Element_Text('RoomCode');
		$RoomCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomCode->setAttrib('required',"true");
		$RoomCode->setAttrib('trim',"true");
		$RoomCode->setAttrib('maxlength','100');
		$RoomCode->removeDecorator("DtDdWrapper");
		$RoomCode->removeDecorator("Label");
		$RoomCode->removeDecorator('HtmlTag');

		$RoomId = new Zend_Form_Element_Hidden('RoomId');
		$RoomId->setAttrib('required',"false");
		$RoomId->setAttrib('maxlength','50');
		$RoomId->removeDecorator("DtDdWrapper");
		$RoomId->removeDecorator("Label");
		$RoomId->removeDecorator('HtmlTag');

		$SeatNo = new Zend_Form_Element_Hidden('SeatNo');
		$SeatNo->setAttrib('required',"false");
		$SeatNo->setAttrib('maxlength','50');
		$SeatNo->removeDecorator("DtDdWrapper");
		$SeatNo->removeDecorator("Label");
		$SeatNo->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("RESERVE ROOM");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag');

		$ChangeReserve = new Zend_Form_Element_Submit('ChangeReserve');
		$ChangeReserve->label = $gstrtranslate->_("CHANGE RESERVE ROOM");
		$ChangeReserve->dojotype="dijit.form.Button";
		$ChangeReserve->removeDecorator("DtDdWrapper");
		$ChangeReserve->removeDecorator('HtmlTag');

		$UpdateReserve = new Zend_Form_Element_Submit('UpdateReserve');
		$UpdateReserve->label = $gstrtranslate->_("Update Reservation");
		$UpdateReserve->dojotype="dijit.form.Button";
		$UpdateReserve->removeDecorator("DtDdWrapper");
		$UpdateReserve->removeDecorator('HtmlTag');

		$CancelReserve = new Zend_Form_Element_Submit('CancelReserve');
		$CancelReserve->label = $gstrtranslate->_("CANCEL RESERVATION");
		$CancelReserve->dojotype="dijit.form.Button";
		$CancelReserve->removeDecorator("DtDdWrapper");
		$CancelReserve->removeDecorator('HtmlTag');

		$Approve = new Zend_Form_Element_Submit('Approve');
		$Approve->label = $gstrtranslate->_("Approve");
		$Approve->dojotype="dijit.form.Button";
		$Approve->removeDecorator("DtDdWrapper");
		$Approve->removeDecorator('HtmlTag');

		$Disapprove = new Zend_Form_Element_Submit('Disapprove');
		$Disapprove->label = $gstrtranslate->_("Revert");
		$Disapprove->dojotype="dijit.form.Button";
		$Disapprove->removeDecorator("DtDdWrapper");
		$Disapprove->removeDecorator('HtmlTag');

		$Remarks = new Zend_Form_Element_Textarea('Remarks');
		$Remarks->setAttrib('dojoType',"dijit.form.Textarea");
		$Remarks->setAttrib('required',"false")
		//$Remarks->setAttrib('maxlength','100');
		->setAttrib('rows', '4')
		->setAttrib('height', '200px');
		$Remarks->removeDecorator("DtDdWrapper");
		$Remarks->removeDecorator("Label");
		$Remarks->removeDecorator('HtmlTag');


		$block = new Zend_Form_Element_Hidden('block');
		$block->setAttrib('required',"false");
		$block->setAttrib('maxlength','50');
		$block->removeDecorator("DtDdWrapper");
		$block->removeDecorator("Label");
		$block->removeDecorator('HtmlTag');


		$level = new Zend_Form_Element_Hidden('level');
		$level->setAttrib('required',"false");
		$level->setAttrib('maxlength','50');
		$level->removeDecorator("DtDdWrapper");
		$level->removeDecorator("Label");
		$level->removeDecorator('HtmlTag');

		$setroom = new Zend_Form_Element_Hidden('setroom');
		$setroom->setAttrib('required',"false");
		$setroom->setAttrib('maxlength','50');
		$setroom->removeDecorator("DtDdWrapper");
		$setroom->removeDecorator("Label");
		$setroom->removeDecorator('HtmlTag');
		
		$select[-1] = array('key'=>'','value'=>'Select'); 
		$lobjsemesterlist = $lobjSemesterModel->getList();
		$lobjsemesterlist = $select + $lobjsemesterlist;
		
		$IdSemester  = new Zend_Dojo_Form_Element_FilteringSelect('IdSemester');
		$IdSemester->removeDecorator("DtDdWrapper");
		$IdSemester->setAttrib('required',"false") ;
		$IdSemester->removeDecorator("Label");
		$IdSemester->removeDecorator('HtmlTag');
		$IdSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$IdSemester->setAttrib('onchange','getdates(this)');
		$IdSemester->addMultiOptions($lobjsemesterlist);

		$lobjdefination = new App_Model_Definitiontype();
        $StatusList = $lobjdefination->fnGetDefinationMs( "Hostel Reservation Status" );
        $Status = new Zend_Dojo_Form_Element_FilteringSelect( 'Status' );
        $Status->removeDecorator( "DtDdWrapper" );
        $Status->removeDecorator( "Label" );
        $Status->removeDecorator( 'HtmlTag' );
        $Status->setAttrib( 'required', "false" );
        $Status->setRegisterInArrayValidator( false );
        $Status->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        $Status->addMultiOptions( $StatusList );

        //$TenantTypeList = $lobjdefination->fnGetDefinationMs( "Customer Type", true);
        $Definition = new App_Model_General_DbTable_Definationms();
        $TenantTypeList = $Definition->getByCodeAsList('Customer Type');
        $TenantType = new Zend_Dojo_Form_Element_FilteringSelect( 'tenant_type' );
        $TenantType->removeDecorator( "DtDdWrapper" );
        $TenantType->removeDecorator( "Label" );
        $TenantType->removeDecorator( 'HtmlTag' );
        $TenantType->setAttrib( 'required', "false" );
        $TenantType->setRegisterInArrayValidator( false );
        $TenantType->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        $TenantType->addMultiOptions( $TenantTypeList );

        
        $registration_application_number = new Zend_Dojo_Form_Element_ComboBox( 'registration_application_number' );
        $registration_application_number->removeDecorator( "DtDdWrapper" );
        $registration_application_number->removeDecorator( "Label" );
        $registration_application_number->removeDecorator( 'HtmlTag' );
        $registration_application_number->setAttrib( 'required', "false" );
        $registration_application_number->setRegisterInArrayValidator( false );
        $registration_application_number->setAttrib('onchange','get_student_applicant_id(this)');
        $registration_application_number->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        

        $CustTypeList = array( 1 => 'Student', 2 => 'Applicant');
        $CustType = new Zend_Dojo_Form_Element_RadioButton( 'cust_type' );
        $CustType->removeDecorator( "DtDdWrapper" );
        $CustType->removeDecorator( "Label" );
        $CustType->removeDecorator( 'HtmlTag' );
        $CustType->setSeparator( PHP_EOL );
        $CustType->setAttrib( 'required', "false" );
        //$CustType->setRegisterInArrayValidator( false );
        //$CustType->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        $CustType->addMultiOptions( $CustTypeList );
		
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateformat = "{datePattern:'dd-MM-yyyy'}";

		$SemesterStartDate = new Zend_Form_Element_Text('SemesterStartDate');
		$SemesterStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$SemesterStartDate->setAttrib('onChange',"dijit.byId('SemesterEndDate').constraints.min = arguments[0];");
		$SemesterStartDate->setAttrib('required',"true")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$SemesterEndDate = new Zend_Form_Element_Text('SemesterEndDate');
		$SemesterEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$SemesterEndDate->setAttrib('onChange',"dijit.byId('SemesterStartDate').constraints.max = arguments[0];") ;
		$SemesterEndDate->setAttrib('required',"true")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		

		$this->addElements(array(
				$RoomCode,$IdHostelLevel,$IdHostelBlock,$RoomId,$SeatNo,$Save,$ChangeReserve, $UpdateReserve, $Status,
				$CancelReserve,$Approve,$Disapprove,$Remarks,$block,$level,$setroom,$IdSemester,$SemesterStartDate
				,$SemesterEndDate, $TenantType, $CustType, $registration_application_number
		));
	}
}
?>

