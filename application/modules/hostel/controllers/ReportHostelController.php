<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hostel_ReportHostelController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Hostel_Model_DbTable_ReportHostel();
        $this->lobjHostelroomregModel = new Hostel_Model_DbTable_Roomregistration();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Hostel Report');
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['search'])){
                $form = new Hostel_Form_ReportHostelSearch(array('blockid'=>$formData['block']));
                $this->view->form = $form;
                $form->populate($formData);
                
                //get block information
                $blockInfo = $this->model->getBlock($formData['block']);
                $this->view->blockInfo = $blockInfo;
                
                //get report data
                $levelList = $this->model->getLevelById($formData);
                
                if ($levelList){
                    
                    foreach ($levelList as $key => $levelLoop){
                        $roomList = $this->model->getRoom($levelLoop['IdHostelLevel']);
                        $levelList[$key]['roomlist']=$roomList;
                        if ($roomList){
                            $levelList[$key]['genderroom']=$roomList[0]['Gender'];
                        }else{
                            $levelList[$key]['genderroom']=1;
                        }
                        
                        $rowspan1 = 0;
                        
                        $capacity = 0;
                        
                        if ($roomList){
                            
                            foreach ($roomList as $key2=>$roomLoop){
                                $studentList = $this->model->getReportHostelList($roomLoop['IdHostelRoom'], $roomLoop['Capacity'], $formData['status']);
                                $levelList[$key]['roomlist'][$key2]['studentlist']=$studentList;
                                
                                $rowspan2 = 0;
                                
                                if ($studentList){
                                    foreach ($studentList as $key3 => $studentLoop){
                                        //$roomrate = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($roomLoop['IdHostelRoom'], 263);
                                        $roomrate = $this->model->getRoomRate($roomLoop['RoomTypeId'], $studentLoop['IdIntake'], $studentLoop['CheckInDate']);
                                        $levelList[$key]['roomlist'][$key2]['studentlist'][$key3]['roomrate']=$roomrate;
                                        
                                        $date = date('Y-m-d', strtotime($studentLoop['CheckInDate']));
                                        
                                        $semestersession = $this->model->getSemesterSession($date, $studentLoop['IdScheme']);
                                        $levelList[$key]['roomlist'][$key2]['studentlist'][$key3]['semestersession']=$semestersession;
                                        
                                        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                                        $taggingFinancialAid=$studentRegistrationDb->getFinancialAidTagging($studentLoop['IdStudentRegistration']);
                                        $levelList[$key]['roomlist'][$key2]['studentlist'][$key3] = array_merge($levelList[$key]['roomlist'][$key2]['studentlist'][$key3],$taggingFinancialAid);
                                    
                                        if ($formData['status']!=1){
                                            if ($studentLoop['checkinDate2']==null){
                                                $checkindate = $this->lobjHostelroomregModel->getCheckinDate($studentLoop['IdStudentRegistration']);
                                                $levelList[$key]['roomlist'][$key2]['studentlist'][$key3]['CheckInDate'] = $checkindate['checkinDate'];
                                            }
                                        }
                                        
                                        $rowspan2++;
                                    }
                                }else{
                                    if ($formData['status']!=1){
                                        $roomrate = $this->model->getRoomRate($roomLoop['RoomTypeId']);

                                        if ($roomrate){
                                            $rate = number_format($roomrate['Rate'], 2, '.', ',');
                                        }else{
                                            $rate = '0.00';
                                        }
                                        
                                        $addData = array(
                                            'appl_fname'=>'No Checkout History',
                                            'appl_lname'=>'',
                                            'appl_gender'=>'',
                                            'registrationId'=>'',
                                            'ProgramCode'=>'',
                                            'CheckInDate'=>null,
                                            'IdScheme'=>null,
                                            'CheckInDate'=>null,
                                            'CheckOutDate'=>null,
                                            'IdHostelRoom'=>null,
                                            'roomrate'=>$roomrate,
                                            //'availablehostel'=>1,
                                            'appl_phone_home'=>'',
                                            'appl_email_personal'=>'',
                                            'IntakeDesc'=>''
                                        );
                                        array_push($levelList[$key]['roomlist'][$key2]['studentlist'], $addData);
                                        $rowspan2++;
                                    }
                                }
                                
                                if ($formData['status']==1){
                                    if (count($studentList) < $roomLoop['Capacity']){
                                        $capacityAvailable = $roomLoop['Capacity']-count($studentList);

                                        //$roomrate = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($roomLoop['IdHostelRoom'], 263);
                                        $roomrate = $this->model->getRoomRate($roomLoop['RoomTypeId']);

                                        if ($roomrate){
                                            $rate = number_format($roomrate['Rate'], 2, '.', ',');
                                        }else{
                                            $rate = '0.00';
                                        }

                                        $i = 1;

                                        while ($i <= $capacityAvailable){
                                            $addData = array(
                                                'appl_fname'=>'available-RM'.$rate,
                                                'appl_lname'=>'',
                                                'appl_gender'=>$roomLoop['Gender'],
                                                'registrationId'=>'',
                                                'ProgramCode'=>'',
                                                'CheckInDate'=>null,
                                                'IdScheme'=>null,
                                                'CheckInDate'=>null,
                                                'CheckOutDate'=>null,
                                                'IdHostelRoom'=>null,
                                                'availablehostel'=>1,
                                                'appl_phone_home'=>'',
                                                'appl_email_personal'=>'',
                                                'IntakeDesc'=>''
                                            );
                                            array_push($levelList[$key]['roomlist'][$key2]['studentlist'], $addData);
                                            $i++;
                                        }
                                    }
                                }
                                
                                //rowspan 2
                                if ($formData['status']==1){
                                    $levelList[$key]['roomlist'][$key2]['rowspan2']=$roomLoop['Capacity'];
                                }else{
                                    $levelList[$key]['roomlist'][$key2]['rowspan2']=$rowspan2;
                                }
                                
                                //capacity
                                $capacity = $roomLoop['Capacity']+$capacity;
                                $rowspan1 = $rowspan2+$rowspan1;
                            }
                        }
                        
                        if ($formData['status']==1){
                            $levelList[$key]['rowspan1']=$capacity;
                        }else{
                            $levelList[$key]['rowspan1']=$rowspan1;
                        }
                    }
                }
                
                //echo "<pre>";
		//print_r($levelList);
		//exit;
                
                $this->view->list = $levelList;
            }else{
                $form = new Hostel_Form_ReportHostelSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Hostel_Form_ReportHostelSearch();
            $this->view->form = $form;
        }
    }
    
    public function printAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $form = new Hostel_Form_ReportHostelSearch(array('blockid'=>$formData['block']));
            $this->view->form = $form;
            $form->populate($formData);

            //get block information
            $blockInfo = $this->model->getBlock($formData['block']);
            $this->view->blockInfo = $blockInfo;

            //get report data
            $levelList = $this->model->getLevelById($formData);

            if ($levelList){

                foreach ($levelList as $key => $levelLoop){
                    $roomList = $this->model->getRoom($levelLoop['IdHostelLevel']);
                    $levelList[$key]['roomlist']=$roomList;
                    if ($roomList){
                        $levelList[$key]['genderroom']=$roomList[0]['Gender'];
                    }else{
                        $levelList[$key]['genderroom']=1;
                    }

                    $rowspan1 = 0;

                    $capacity = 0;

                    if ($roomList){

                        foreach ($roomList as $key2=>$roomLoop){
                            $studentList = $this->model->getReportHostelList($roomLoop['IdHostelRoom'], $roomLoop['Capacity'], $formData['status']);
                            $levelList[$key]['roomlist'][$key2]['studentlist']=$studentList;

                            $rowspan2 = 0;

                            if ($studentList){
                                foreach ($studentList as $key3 => $studentLoop){
                                    //$roomrate = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($roomLoop['IdHostelRoom'], 263);
                                    $roomrate = $this->model->getRoomRate($roomLoop['RoomTypeId'], $studentLoop['IdIntake'], $studentLoop['CheckInDate']);
                                    $levelList[$key]['roomlist'][$key2]['studentlist'][$key3]['roomrate']=$roomrate;

                                    $date = date('Y-m-d', strtotime($studentLoop['CheckInDate']));

                                    $semestersession = $this->model->getSemesterSession($date, $studentLoop['IdScheme']);
                                    $levelList[$key]['roomlist'][$key2]['studentlist'][$key3]['semestersession']=$semestersession;

                                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                                    $taggingFinancialAid=$studentRegistrationDb->getFinancialAidTagging($studentLoop['IdStudentRegistration']);
                                    $levelList[$key]['roomlist'][$key2]['studentlist'][$key3] = array_merge($levelList[$key]['roomlist'][$key2]['studentlist'][$key3],$taggingFinancialAid);

                                    if ($formData['status']!=1){
                                        if ($studentLoop['checkinDate2']==null){
                                            $checkindate = $this->lobjHostelroomregModel->getCheckinDate($studentLoop['IdStudentRegistration']);
                                            $levelList[$key]['roomlist'][$key2]['studentlist'][$key3]['CheckInDate'] = $checkindate['checkinDate'];
                                        }
                                    }

                                    $rowspan2++;
                                }
                            }else{
                                if ($formData['status']!=1){
                                    $roomrate = $this->model->getRoomRate($roomLoop['RoomTypeId']);

                                    if ($roomrate){
                                        $rate = number_format($roomrate['Rate'], 2, '.', ',');
                                    }else{
                                        $rate = '0.00';
                                    }

                                    $addData = array(
                                        'appl_fname'=>'No Checkout History',
                                        'appl_lname'=>'',
                                        'appl_gender'=>'',
                                        'registrationId'=>'',
                                        'ProgramCode'=>'',
                                        'CheckInDate'=>null,
                                        'IdScheme'=>null,
                                        'CheckInDate'=>null,
                                        'CheckOutDate'=>null,
                                        'IdHostelRoom'=>null,
                                        'roomrate'=>$roomrate,
                                        //'availablehostel'=>1,
                                        'appl_phone_home'=>'',
                                        'appl_email_personal'=>'',
                                        'IntakeDesc'=>''
                                    );
                                    array_push($levelList[$key]['roomlist'][$key2]['studentlist'], $addData);
                                    $rowspan2++;
                                }
                            }

                            if ($formData['status']==1){
                                if (count($studentList) < $roomLoop['Capacity']){
                                    $capacityAvailable = $roomLoop['Capacity']-count($studentList);

                                    //$roomrate = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($roomLoop['IdHostelRoom'], 263);
                                    $roomrate = $this->model->getRoomRate($roomLoop['RoomTypeId']);

                                    if ($roomrate){
                                        $rate = number_format($roomrate['Rate'], 2, '.', ',');
                                    }else{
                                        $rate = '0.00';
                                    }

                                    $i = 1;

                                    while ($i <= $capacityAvailable){
                                        $addData = array(
                                            'appl_fname'=>'available-RM'.$rate,
                                            'appl_lname'=>'',
                                            'appl_gender'=>$roomLoop['Gender'],
                                            'registrationId'=>'',
                                            'ProgramCode'=>'',
                                            'CheckInDate'=>null,
                                            'IdScheme'=>null,
                                            'CheckInDate'=>null,
                                            'CheckOutDate'=>null,
                                            'IdHostelRoom'=>null,
                                            'availablehostel'=>1,
                                            'appl_phone_home'=>'',
                                            'appl_email_personal'=>'',
                                            'IntakeDesc'=>''
                                        );
                                        array_push($levelList[$key]['roomlist'][$key2]['studentlist'], $addData);
                                        $i++;
                                    }
                                }
                            }

                            //rowspan 2
                            if ($formData['status']==1){
                                $levelList[$key]['roomlist'][$key2]['rowspan2']=$roomLoop['Capacity'];
                            }else{
                                $levelList[$key]['roomlist'][$key2]['rowspan2']=$rowspan2;
                            }

                            //capacity
                            $capacity = $roomLoop['Capacity']+$capacity;
                            $rowspan1 = $rowspan2+$rowspan1;
                        }
                    }

                    if ($formData['status']==1){
                        $levelList[$key]['rowspan1']=$capacity;
                    }else{
                        $levelList[$key]['rowspan1']=$rowspan1;
                    }
                }
            }

            //echo "<pre>";
            //print_r($levelList);
            //exit;

            $this->view->list = $levelList;
        }
        
        $this->view->filename = date('Ymd').'_accommodation_report.xls';
    }
    
    public function getLevelAction(){
        $id = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $levelList = $this->model->getLevel($id);
        
        $json = Zend_Json::encode($levelList);
		
	echo $json;
	exit();
    }
}