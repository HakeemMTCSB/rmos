<?php
class Hostel_ReservationsetupController extends Base_Base {
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjreservationsetup = new Hostel_Form_Reservationsetup();
		$this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$this->lobjreservation = new Hostel_Model_DbTable_Reservationsetup();
	}

	public function indexAction(){
		$this->view->lobjreservationsetup = $this->lobjreservationsetup;
		$lobjsemesterlist = $this->lobjSemesterModel->getAllsemesterList();
		$i =0;

		//asd($_SERVER,false);
		$request = $this->getRequest();
		$this->view->lastpage = $request->getHeader('referer');


		foreach($lobjsemesterlist as $semester){
			$finalsemester[$i]['key'] = $lobjsemesterlist[$i]['value'];
			$finalsemester[$i]['value'] = $lobjsemesterlist[$i]['value'];
			$i++;
		}
		$this->lobjreservationsetup->SemesterCode->addMultiOptions($finalsemester);

		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Reservation Priority');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjreservationsetup->Priority->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}

		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Value Type');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjreservationsetup->ValueType->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}

		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Hostel Reservation Condition');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjreservationsetup->Condition->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}

		$this->view->reservationdata = $this->lobjreservation->fngetreservationdata();
		//		echo "<pre>";
		//		print_r($this->view->reservationdata);


		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();
			$auth = Zend_Auth :: getInstance();
			$userId = $auth->getIdentity()->iduser;
			$this->lobjreservation->fndeletereservationdata();
			$this->lobjreservation->fninsertreservationdata($larrformData,$userId);
			$this->_redirect($this->baseUrl . '/hostel/reservationsetup/index');
		}
	}

	public function getintakelistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrintakelist = $this->lobjintake->fngetallIntakelist();
		echo Zend_Json_Encoder::encode($larrintakelist);
	}
}
