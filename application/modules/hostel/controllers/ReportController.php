<?php
class Hostel_ReportController extends Base_Base 
{
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() 
	{
		$this->lobjhostelroom = new Hostel_Model_DbTable_Hostelroom();
		$this->lobjchagessetupForm = new Hostel_Form_Chargessetup();
		$this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$this->lobjhostellevelModel = new Hostel_Model_DbTable_Hostellevel();
		$this->lobjhostelreservationModel = new Hostel_Model_DbTable_Hostelreservation();
		$this->lobjReportForm = new Hostel_Form_Report();
		$this->lobjroomregistration = new Hostel_Model_DbTable_Roomregistration();
		$this->lobjroominfo = new Hostel_Model_DbTable_Roominfo();
		
		$this->public = $this->getPublic();
	}

	public function indexAction()
	{
		
        //labels
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$this->view->curtab = $this->_getParam('tab', 1); 
		
		//start
		$hostelBlockModel = new Hostel_Model_DbTable_Blockinfo();
		$blockList = $hostelBlockModel->fngetListofBlocks();
		
			
		/* ---------------------------------------
		 *  	INIT AND RESERVATION
		 *  
		 *  - 6 Jan 2014
		 *  - initially this was only for reservation so the code is a bit messed up 
		 *  -------------------------------------- 
		 */
		
		$resList =  $this->lobjhostelreservationModel->getAllReservation();
		
		
		//filter fields
		$this->lobjBlockinfoModel = new Hostel_Model_DbTable_Blockinfo();
		$blockList = $this->lobjBlockinfoModel->fngetListofBlocks();
		$this->lobjReportForm->ResBlock->addMultioptions($blockList);
		$this->lobjReportForm->RegBlock->addMultioptions($blockList);
		
		$this->view->lobjReportForm = $this->lobjReportForm;
		$this->view->blockList = $blockList;
		
		//process filter
		if ($this->_request->isPost()) 
		{
			$formData = $this->_request->getPost();
			
			if ( $formData['filtertype'] == 'reservation' )
			{
				$filter = array(
									'Block' 	=> $formData['ResBlock'],
									'Level'		=> $formData['ResLevel'],
									'Room'		=> $formData['ResRoom'],
									'Gender'	=> $formData['ResGender']
								);
								
				$resList =  $this->lobjhostelreservationModel->getAllReservation($filter);
		
			}
		
			
		}
	
		
		
			
		//pagi2 buta nyanyuk punya code
		$levels = array();
		$levelcount = array();
		$resbyblock = array();
		$resbyblocklevel = array(); 
		
		foreach ( $resList as $res )
		{
			$resbyblock[$res['BlockId']][] = $res;
		}
		
		$this->view->resbyblock = $resbyblock;
		$this->view->resList = $resList;

		//process blocks
		foreach ( $blockList as $block )
		{
			$levelcounter = 0;
			
			$levelList = $this->lobjhostellevelModel->getAllLevelList($block['key']);
			foreach ($levelList as $level)
			{
				$levels[$block['key']][$level['key']] = $level['name'];
				
				if ( !empty($resbyblock[$block['key']]) )
				{
					foreach ( $resbyblock[$block['key']] as $resblock )
					{
						if ( $resblock['BlockId'] == $block['key'] && $resblock['LevelName'] == $level['name'] )
						{
							$resbyblocklevel[$block['key']][$level['key']][] = $resblock;
							$levelcount[$block['key']][$level['name']]++;		
						}
					}
				}
			}
		
		}
		
		
		//process room by hostel and level
		$this->view->resbyblocklevel = $resbyblocklevel;
	
		$this->view->levels = $levels;
		$this->view->levelcount = $levelcount;
		
		
		/* ---------------------------------------
		 *  	REGISTRATION 
		 *  -------------------------------------- 
		 */
		
		$regList =  $this->lobjroomregistration->getAllRegistration();
		
		
		//process filter
		if ($this->_request->isPost()) 
		{
			$formData = $this->_request->getPost();
			
		
			if ( $formData['filtertype'] == 'registration' )
			{
				$filter = array(
									'Block' 	=> $formData['RegBlock'],
									'Level'		=> $formData['RegLevel'],
									'Room'		=> $formData['RegRoom'],
									'Gender'	=> $formData['RegGender']
								);
								
				$regList = $this->lobjroomregistration->getAllRegistration($filter);
		
			}
			
		}
		
		$reglevels = array();
		$reglevelcount = array();
		$regbyblock = array();
		$regbyblocklevel = array(); 
		
		foreach ( $regList as $reg )
		{
			$regbyblock[$reg['BlockId']][] = $reg;
		}
		
		$this->view->regbyblock = $regbyblock;
		$this->view->regList = $regList;

		//process blocks
		foreach ( $blockList as $block )
		{
			$levelcounter = 0;
			
			$levelList = $this->lobjhostellevelModel->getAllLevelList($block['key']);
			
			foreach ($levelList as $level)
			{
				$reglevels[$block['key']][$level['key']] = $level['name'];
				
				if ( !empty($regbyblock[$block['key']]) )
				{
				
					foreach ( $regbyblock[$block['key']] as $regblock )
					{
						if ( $regblock['BlockId'] == $block['key'] && $regblock['LevelName'] == $level['name'] )
						{
							
							$regbyblocklevel[$block['key']][$level['key']][] = $regblock;
							$reglevelcount[$block['key']][$level['name']]++;
						}
					}
				}
			}
			
			
		}
		
	
		
		//process room by hostel and level
		$this->view->regbyblocklevel = $regbyblocklevel;
		
		$this->view->reglevels = $reglevels;
		$this->view->reglevelcount = $reglevelcount;
		
		
	}
	
	protected function getPublic()
	{
		chdir(APPLICATION_PATH);
    	return realpath("../public");
	}
    
    public function chartAction()
    {
    	if ( $this->getRequest()->isXmlHttpRequest() ) 
    	{
    		$this->_helper->layout->disableLayout();
    	}
    	
        //$this->_helper->viewRenderer->setNoRender();
    	include $this->public.'/lib/Chart.php';
    	
    	$chart = new Chart('ColumnChart');
		$this->view->chart = array();
	
		//CHARTS DATA
		// get all hostel
		$this->lobjBlockinfoModel = new Hostel_Model_DbTable_Blockinfo();
		$HostelBlocks = $this->lobjBlockinfoModel->fnfetchAllBlockinfo();
		$this->view->HostelBlocks = $HostelBlocks;
			
	
    	//MAIN - OCCUPANCY
    	$TotalCapacity = $this->lobjroominfo->getCapacityByHostel();
    	$TotalOccupancy = $this->lobjroominfo->getOccupiedByHostel();
    	
        
        /**
         * Jasdy modified balik array bagi sesuai dengan cara call dekat tempat lain
         */
        //Rearrange capacity
        $hostelCapacity = array();
        foreach($TotalCapacity as $n => $l)
        {
            $hostelCapacity[$l['Block']]['Total'] = $l['Total']; 
            $hostelCapacity[$l['Block']]['Block'] = $l['Block']; 
        }
        $TotalCapacity = $hostelCapacity;
        
        //Rearrange occupancy
        $hostelOccupied = array();
        foreach($TotalOccupancy as $p => $m)
        {
            $hostelOccupied[$m['Block']]['Total'] = $m['Total']; 
            $hostelOccupied[$m['Block']]['Block'] = $m['Block']; 
        }
        
        $TotalOccupancy = $hostelOccupied;
    	/**
         * Tamat rearrange
         */
        
        $this->view->TotalCapacity = $TotalCapacity;
    	$this->view->TotalOccupancy = $hostelOccupied;
    	
    	$data = array(
		        'cols' => array(
		                array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string'),
		                array('id' => '', 'label' => $this->view->translate('Occupied'), 'type' => 'number'),
		                array('id' => '', 'label' => $this->view->translate('Available'), 'type' => 'number'),

		        ),
		        'rows' => array(
		        
		        )
		);

		$x=0;
    	foreach ( $HostelBlocks as $hostel)
		{
			$occupied = intval($TotalOccupancy[$hostel['IdHostelBlock']]['Total']);
			$available = intval($TotalCapacity[$hostel['IdHostelBlock']]['Total']-$TotalOccupancy[$hostel['IdHostelBlock']]['Total']);
			$occupied_percent = $TotalCapacity[$hostel['IdHostelBlock']]['Total'] == 0 ? 0 : round(($occupied/$TotalCapacity[$hostel['IdHostelBlock']]['Total']) * 100);
			$available_percent = $TotalCapacity[$hostel['IdHostelBlock']]['Total'] == 0 ? 0 : round(($available/$TotalCapacity[$hostel['IdHostelBlock']]['Total']) * 100); 
			
			$data['rows'][$x] = array( 'c' => array(array('v' => $hostel['Name'])));
			//occupied
			array_push($data['rows'][$x]['c'], array('v' => $occupied_percent ));
			//available
			array_push($data['rows'][$x]['c'], array('v' => $available_percent));
			
			$x++;
		}
    	// echo '<pre>';
        // print_r($data);
        // echo '</pre>';
    	/*$data = array(
		        'cols' => array(
		                array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string'),
		                array('id' => '', 'label' => $this->view->translate('Occupied'), 'type' => 'number'),
		                array('id' => '', 'label' => $this->view->translate('Available'), 'type' => 'number'),

		        ),
		        'rows' => array(
		                array('c' => array(array('v' => $this->view->translate('Putra Villa')), array('v' => 52), array('v' => 48))),
		                array('c' => array(array('v' => $this->view->translate('Teachers Quaters')), array('v' => 33), array('v' => 67))),
		                array('c' => array(array('v' => $this->view->translate('Taman Sri Serdang')), array('v' => 44), array('v' => 56)))
		        )
		);*/
    	
		$chart->load(json_encode($data));
		$chart->add('var formatter = new google.visualization.NumberFormat({ fractionDigits: 0, suffix: \'%\' }); formatter.format(data, 2); formatter.format(data, 1);');
		
		$options = array( 
							'title' => 'Occupancy',
							'width' => 700,
							'height' =>500,
							'colors' => array('red','green'),
							'fontName' => 'Verdana',
							'vAxis' => array('format'=>'#\'%\''), 
							'hAxis' => array(	
												'title' => $this->view->translate('Hostel'),
												'titleTextStyle' => array('bold' => 'true', 'italic' => 'false')
											),
							'chartArea' => array(
													'backgroundColor' => array(
																					'stroke' => '#ccc',
																					'strokeWidth' => 1
																			  )
											
											)
  
						);
		$this->view->chart['main'] = $chart->draw('chart_main', $options);
		
		//GENDER
		$TotalGender = $this->lobjroominfo->getGenderByHostel();
		$TotalGender = $this->formatInvTotal($TotalGender,'Gender');
		$this->view->TotalGender = $TotalGender;
		
	
   	 	$data = array(
		        'cols' => array(
		                array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string'),
		                array('id' => '', 'label' => $this->view->translate('Male'), 'type' => 'number'),
		                array('id' => '', 'label' => $this->view->translate('Female'), 'type' => 'number'),

		        ),
		        'rows' => array(
		        
		        )
		);

		$x=0;
    	foreach ( $HostelBlocks as $hostel)
		{
			$male = intval($TotalGender[$hostel['IdHostelBlock']][1]);
			$female = intval($TotalGender[$hostel['IdHostelBlock']][0]);
			$male_percent = round( ($male / ($male+$female)) * 100 );
			$female_percent = round( ($female / ($male+$female)) * 100 );
			
			
			$data['rows'][$x] = array( 'c' => array(array('v' => $hostel['Name'])));
			//male
			array_push($data['rows'][$x]['c'], array('v' => $male_percent ));
			
			//female
			array_push($data['rows'][$x]['c'], array('v' => $female_percent ));
			$x++;
		}
		
		
		
    	/*$data = array(
		        'cols' => array(
		                array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string'),
		                array('id' => '', 'label' => $this->view->translate('Male'), 'type' => 'number'),
		                array('id' => '', 'label' => $this->view->translate('Female'), 'type' => 'number'),

		        ),
		        'rows' => array(
		                array('c' => array(array('v' => $this->view->translate('Putra Villa')), array('v' => 19), array('v' => 81))),
		                array('c' => array(array('v' => $this->view->translate('Teachers Quaters')), array('v' => 27), array('v' => 73))),
		                array('c' => array(array('v' => $this->view->translate('Taman Sri Serdang')), array('v' => 70), array('v' => 30)))
		        )
		);*/
	
		$chart->load(json_encode($data));
		$chart->add('var formatter = new google.visualization.NumberFormat({ fractionDigits: 0, suffix: \'%\' }); formatter.format(data, 2); formatter.format(data, 1);');
		
		$options = array( 
							'title' => 'Gender',
							'width' => 1000,
							'height' =>500,
							'fontName' => 'Verdana',
							'fontSize' => 11,
							'colors' => array('lightblue','pink'),
							'vAxis' => array('format'=>'#\'%\''), 
							'hAxis' => array(	
												'title' => $this->view->translate('Hostel'),
												'titleTextStyle' => array('bold' => 'true', 'italic' => 'false')
											),
							'chartArea' => array(
													'backgroundColor' => array(
																					'stroke' => '#ccc',
																					'strokeWidth' => 1
																			  )
											
											)
  
						);
		$this->view->chart['gender'] = $chart->draw('chart_gender', $options);
    	
    	//INVENTORY
			
		// get all inventory per hostel
		if ( !empty($HostelBlocks) )
		{
			$InvTypes = $this->lobjroominfo->getInventoryTypes();
			$InvTotal = $this->lobjroominfo->getInventoriesByHostel();
			$InvTotal = $this->formatInvTotal($InvTotal);
			
			$this->view->InvTypes = $InvTypes;
			$this->view->InvTotal = $InvTotal;
		}
	
		
		$data = array(
		        'cols' => array(
		                array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string')
		        ),
		        'rows' => array(
		        )
		);

		$x=0;
    	foreach ( $HostelBlocks as $hostel)
		{
			$data['rows'][$x] = array( 'c' => array(array('v' => $hostel['Name'])));
			foreach ( $InvTypes as $inv )
			{
				array_push($data['rows'][$x]['c'], array('v' => intval($InvTotal[$hostel['IdHostelBlock']][$inv['IdHostelInventory']])));
			}
			$x++;
		}
		
		foreach ( $InvTypes as $inv )
		{
			$data['cols'][] = array('id' => '', 'label' => $inv['Name'], 'type' => 'number');
		}

		// array('c' => array(array('v' => 'Putra Villa'), array('v' => 1000), array('v' => 1000), array('v' => 1000), array('v' => 1000))),

		$chart->load(json_encode($data));
		
		$options = array( 
							'title' => 'Inventory',
							'width' => 1000,
							'height' =>500,
							'fontName' => 'Verdana',
							'fontSize' => 11,
							'vAxis' => array('format'=>'#'), 
							'hAxis' => array(	
												'title' => $this->view->translate('Hostel'),
												'titleTextStyle' => array('bold' => 'true', 'italic' => 'false')
											),
							'chartArea' => array(
													'backgroundColor' => array(
																					'stroke' => '#ccc',
																					'strokeWidth' => 1
																			  )
											
											)
  
						);
		$this->view->chart['inventory'] = $chart->draw('chart_inventory', $options);
    
    }
    
    private function formatInvTotal($data=array(),$name='InventoryName')
    {
    	if ( !empty($data) )
    	{
    		$newdata = array();
    		foreach ( $data as $row )
    		{
    			//newdata[blockid][InventoryName] = Total
    			$newdata[$row['Block']][$row[$name]] = $row['Total'];
    		}
    		
    	
    		return $newdata;
    	}
    }
    
    public function downloadAction()
    {
        
        $downloadType = $this->_getParam('file',null);
        $type = $this->_getParam('type',null);
        //echo 'not-null';
        if($downloadType != null)
        {
            
            //CHARTS DATA
            // get all hostel
            $this->lobjBlockinfoModel = new Hostel_Model_DbTable_Blockinfo();
            $HostelBlocks = $this->lobjBlockinfoModel->fnfetchAllBlockinfo();
            $this->view->HostelBlocks = $HostelBlocks;
                
        
            //MAIN - OCCUPANCY
            $TotalCapacity = $this->lobjroominfo->getCapacityByHostel();
            $TotalOccupancy = $this->lobjroominfo->getOccupiedByHostel();
            
            
           
            //Rearrange capacity
            $hostelCapacity = array();
            foreach($TotalCapacity as $n => $l)
            {
                $hostelCapacity[$l['Block']]['Total'] = $l['Total']; 
                $hostelCapacity[$l['Block']]['Block'] = $l['Block']; 
            }
            $TotalCapacity = $hostelCapacity;
            
            //Rearrange occupancy
            $hostelOccupied = array();
            foreach($TotalOccupancy as $p => $m)
            {
                $hostelOccupied[$m['Block']]['Total'] = $m['Total']; 
                $hostelOccupied[$m['Block']]['Block'] = $m['Block']; 
            }
            
            $TotalOccupancy = $hostelOccupied;
            /**
             * Tamat rearrange
             */
            
            $this->view->TotalCapacity = $TotalCapacity;
            $this->view->TotalOccupancy = $hostelOccupied;
            
            $data = array(
                    'cols' => array(
                            array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string'),
                            array('id' => '', 'label' => $this->view->translate('Occupied'), 'type' => 'number'),
                            array('id' => '', 'label' => $this->view->translate('Available'), 'type' => 'number'),

                    ),
                    'rows' => array(
                    
                    )
            );

            $x=0;
            foreach ( $HostelBlocks as $hostel)
            {
                
                
                $occupied = intval($TotalOccupancy[$hostel['IdHostelBlock']]['Total']);
                $available = intval($TotalCapacity[$hostel['IdHostelBlock']]['Total']-$TotalOccupancy[$hostel['IdHostelBlock']]['Total']);
                $occupied_percent = $TotalCapacity[$hostel['IdHostelBlock']]['Total'] == 0 ? 0 : round(($occupied/$TotalCapacity[$hostel['IdHostelBlock']]['Total']) * 100);
                $available_percent = $TotalCapacity[$hostel['IdHostelBlock']]['Total'] == 0 ? 0 : round(($available/$TotalCapacity[$hostel['IdHostelBlock']]['Total']) * 100); 
                
                $data['rows'][$x] = array( 'c' => array(array('v' => $hostel['Name'])));
                // occupied
                array_push($data['rows'][$x]['c'], array('v' => $occupied_percent ));
                // available
                array_push($data['rows'][$x]['c'], array('v' => $available_percent));
                
                $x++;
            }
            
            $occupancyHostel = array();
           
            foreach($HostelBlocks as $h => $j)
            {
                $occupiedRoom  = intval($TotalOccupancy[$j['IdHostelBlock']]['Total']);
                $availableRoom = intval($TotalCapacity[$j['IdHostelBlock']]['Total']-$TotalOccupancy[$j['IdHostelBlock']]['Total']);
                $totalRoom     = $TotalCapacity[$j['IdHostelBlock']]['Total'];
                
                $occupied_percent = $TotalCapacity[$j['IdHostelBlock']]['Total'] == 0 ? 0 : round(($occupiedRoom/$TotalCapacity[$j['IdHostelBlock']]['Total']) * 100);
                $available_percent = $TotalCapacity[$j['IdHostelBlock']]['Total'] == 0 ? 0 : round(($availableRoom/$TotalCapacity[$j['IdHostelBlock']]['Total']) * 100); 
                
                $occupancyHostel['table'][$j['IdHostelBlock']]['occupied']  = $occupiedRoom;
                $occupancyHostel['table'][$j['IdHostelBlock']]['available'] = $availableRoom;
                $occupancyHostel['table'][$j['IdHostelBlock']]['total']     = $totalRoom;
                $occupancyHostel['graph'][$j['IdHostelBlock']]['occupied']  = $occupied_percent;
                $occupancyHostel['graph'][$j['IdHostelBlock']]['available'] = $available_percent;
                //$occupancyHostel['graph'][$j['Block']]['total']     = $totalRoom;
            }
            
            // echo '<pre>';
            // print_r($occupancyHostel);
            // echo '</pre>';
            
            
            //GENDER
            $TotalGender = $this->lobjroominfo->getGenderByHostel();
            $TotalGender = $this->formatInvTotal($TotalGender,'Gender');
            $this->view->TotalGender = $TotalGender;
            
        
            $data = array(
                    'cols' => array(
                            array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string'),
                            array('id' => '', 'label' => $this->view->translate('Male'), 'type' => 'number'),
                            array('id' => '', 'label' => $this->view->translate('Female'), 'type' => 'number'),

                    ),
                    'rows' => array(
                    
                    )
            );

            $x=0;
            
            $genderHostel = array();
            foreach ( $HostelBlocks as $hostel)
            {
                $male = intval($TotalGender[$hostel['IdHostelBlock']][1]);
                $female = intval($TotalGender[$hostel['IdHostelBlock']][0]);
                $male_percent = round( ($male / ($male+$female)) * 100 );
                $female_percent = round( ($female / ($male+$female)) * 100 );
                
                $genderHostel['table'][$hostel['IdHostelBlock']]['male']   = $male;
                $genderHostel['table'][$hostel['IdHostelBlock']]['female'] = $female;
                $genderHostel['graph'][$hostel['IdHostelBlock']]['male']   = $male_percent;
                $genderHostel['graph'][$hostel['IdHostelBlock']]['female'] = $female_percent;
                
                
                $data['rows'][$x] = array( 'c' => array(array('v' => $hostel['Name'])));
                //male
                array_push($data['rows'][$x]['c'], array('v' => $male_percent ));
                
                //female
                array_push($data['rows'][$x]['c'], array('v' => $female_percent ));
                $x++;
            }
            
            
            
            //INVENTORY
                
            // get all inventory per hostel
            if ( !empty($HostelBlocks) )
            {
                $InvTypes = $this->lobjroominfo->getInventoryTypes();
                $jastotal = $InvTotal = $this->lobjroominfo->getInventoriesByHostel();
                $InvTotal = $this->formatInvTotal($InvTotal);
               
            }
        
            //print_r($InvTypes);
            $data = array(
                    'cols' => array(
                            array('id' => '', 'label' => $this->view->translate('Hostel'), 'type' => 'string')
                    ),
                    'rows' => array(
                    )
            );

            $x=0;
            $inventoryList = array();
            foreach ( $HostelBlocks as $hostel)
            {   
                $inventoryList['table'][$hostel['IdHostelBlock']]['name'] = $hostel['Name'];
                
                foreach ( $InvTypes as $inv )
                {
                    $inventoryList['table'][$hostel['IdHostelBlock']][$inv['IdHostelInventory']] = intval($InvTotal[$hostel['IdHostelBlock']][$inv['IdHostelInventory']]);
                }
                $x++;
            }
            
            foreach ( $InvTypes as $inv )
            {
                $data['cols'][] = array('id' => '', 'label' => $inv['Name'], 'type' => 'number');
            }

           
            if ($downloadType == 'pdf')
            {
                if($type == 'occupied')
                {
                    /**
                    * JANA OCCUPIED
                    */
                   
                    $yData = array();
                    
                    $x = 0;
                    foreach ($HostelBlocks as $key => $value)
                    {
                        $yData['occupied'][$x]  = $occupancyHostel['table'][$value['IdHostelBlock']]['occupied'];
                        $yData['available'][$x] = $occupancyHostel['table'][$value['IdHostelBlock']]['available'];
                        $yData['total'][$x]     = $occupancyHostel['table'][$value['IdHostelBlock']]['total'];
                        $yData['name'][$x]      = $value['Name'];
                        
                        $x++;
                    }
                   
                    $this->generateGraph($yData,'occupancy');
                    
                    //GENERATE PDF
                    
                    include $this->public.'/MPDF57/mpdf.php';

                    $html = '<img src="'.$this->public.'/graph/occupancy.jpg" />';
                    $html.= '<br /><table width="100%" border="1" cellspacing="2">
                                <tr>
                                    <th></th>
                                    <th>Total</th>
                                    <th>Occupied</th>
                                    <th>Available</th>
                                </tr>';
                                
                    foreach ($HostelBlocks as $hostel)
                    {
                        $html.= '<tr><td>'.$hostel['Name'].'</td><td align="center">'.$occupancyHostel['table'][$hostel['IdHostelBlock']]['total'].'</td><td align="center">'.$occupancyHostel['table'][$hostel['IdHostelBlock']]['occupied'].'</td><td align="center">'.$occupancyHostel['table'][$hostel['IdHostelBlock']]['available'].'</td></tr>';
                    }
                    $html.='</table>';
                    ob_end_clean();
                    $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

                    $mpdf->SetDisplayMode('fullpage');

                    $mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

                    $mpdf->WriteHTML($html,2);
                    
                    ob_end_clean();
                    $mpdf->Output('occupancy.pdf','D');
                   
                    //DELETE GRAPH AFTER GENERATE PDF
                    unlink($this->public.'/graph/occupancy.jpg');
                    /**
                     *END OCCUPIED
                     */
                }
                elseif($type == 'gender')
                {
                    /**
                    * JANA OCCUPIED
                    */
                   
                    $yData = array();
                    
                    $x = 0;
                    foreach ($HostelBlocks as $key => $value)
                    {
                        $yData['male'][$x]  = $genderHostel['table'][$value['IdHostelBlock']]['male'];
                        $yData['female'][$x] = $genderHostel['table'][$value['IdHostelBlock']]['female'];
                        $yData['name'][$x]      = $value['Name'];
                       
                        $x++;
                    }
                   
                    $this->generateGraph($yData,'gender');
                    
                    //GENERATE PDF
                    
                    include $this->public.'/MPDF57/mpdf.php';

                    $html = '<img src="'.$this->public.'/graph/gender.jpg" />';
                    $html.= '<br /><table width="100%" border="1" cellspacing="2">
                                <tr>
                                    <th></th>
                                    <th>Male</th>
                                    <th>Female</th>
                                </tr>';
                                
                    foreach ($HostelBlocks as $hostel)
                    {
                        $html.= '<tr><td>'.$hostel['Name'].'</td><td align="center">'.$genderHostel['table'][$hostel['IdHostelBlock']]['male'].'</td><td align="center">'.$genderHostel['table'][$hostel['IdHostelBlock']]['female'].'</td></tr>';
                    }
                    $html.='</table>';
                    ob_end_clean();
                    $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

                    $mpdf->SetDisplayMode('fullpage');

                    $mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

                    $mpdf->WriteHTML($html,2);
                    
                    ob_end_clean();
                    $mpdf->Output('gender.pdf','D');
                   
                    //DELETE GRAPH AFTER GENERATE PDF
                    unlink($this->public.'/graph/gender.jpg');
                    /**
                     *END OCCUPIED
                     */
                }
                elseif($type == 'inventory')
                {
                    /**
                    * JANA INVENTORY
                    */
                   
                    $yData = array();
                    
                    $x = 0;
                    foreach ($HostelBlocks as $key => $value)
                    {
                        $yData['bed'][$x]  = $inventoryList['table'][$value['IdHostelBlock']][1];
                        $yData['studytable'][$x] = $inventoryList['table'][$value['IdHostelBlock']][2];
                        $yData['name'][$x]      = $value['Name'];
                       
                        $x++;
                        
                    }
                   
                    $this->generateGraph($yData,'inventory');
                    
                    //GENERATE PDF
                    
                    include $this->public.'/MPDF57/mpdf.php';

                    $html = '<img src="'.$this->public.'/graph/inventory.jpg" />';
                    $html.= '<br /><table width="100%" border="1" cellspacing="2">
                                <tr>
                                    <th></th>
                                    <th>Bed</th>
                                    <th>Study Table</th>
                                </tr>';
                                
                    foreach ($HostelBlocks as $hostel)
                    {
                        $html.= '<tr><td>'.$hostel['Name'].'</td><td align="center">'.$inventoryList['table'][$hostel['IdHostelBlock']][1].'</td><td align="center">'.$inventoryList['table'][$hostel['IdHostelBlock']][2].'</td></tr>';
                    }
                    $html.='</table>';
                    ob_end_clean();
                    $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

                    $mpdf->SetDisplayMode('fullpage');

                    $mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

                    $mpdf->WriteHTML($html,2);
                    
                    ob_end_clean();
                    $mpdf->Output('gender.pdf','D');
                   
                    //DELETE GRAPH AFTER GENERATE PDF
                    unlink($this->public.'/graph/inventory.jpg');
                    /**
                     *END INVENTORY
                     */
                }
                exit;
            }
            elseif ($downloadType == 'excel')
            {
                include $this->public.'/PHPExcelpdf/Classes/PHPExcel.php';
                
                $start_table = 2;
                
                if($type == 'occupied')
                {
                    /**
                    * JANA OCCUPIED
                    */
                    
                    $objPHPExcel = new PHPExcel();

                    // Set document properties
                    $objPHPExcel->getProperties()
                        ->setCreator("AUCMS System")
                        ->setLastModifiedBy("AUCMS System")
                        ->setTitle("Report")
                        ->setSubject("Summary Report")
                        ->setDescription("Summary Report")
                        ->setKeywords("Summary Report")
                        ->setCategory("Summary Report");


                   
                    $objWorksheet = $objPHPExcel->getActiveSheet();
                    //$objWorksheet->fromArray(
                    
                    $chartOccupancy = array(
                        array('','Total','Occupied','Available')
                    );
                    
                    $x = $start_table;
                    foreach($HostelBlocks as $hostel)
                    {
                        $pushChart = array(
                            $hostel['Name'],
                            $occupancyHostel['table'][$hostel['IdHostelBlock']]['total'],
                            $occupancyHostel['table'][$hostel['IdHostelBlock']]['occupied'],
                            $occupancyHostel['table'][$hostel['IdHostelBlock']]['available']
                        );
                        
                        array_push($chartOccupancy,$pushChart);
                        
                        $x++;
                    }
                    
                    $objWorksheet->fromArray($chartOccupancy);
                    
                    $dataseriesLabels = array(
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1),	//	'Occupied'
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$D$1', NULL, 1),	//	'Available'
                       
                    );
                    
                    //	Set the X-Axis Labels
                    //		Datatype
                    //		Cell reference for data
                    //		Format Code
                    //		Number of datapoints in series
                    //		Data values
                    //		Data Marker
                    $xAxisTickValues = array(
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$'.$start_table.':$A$'.$x, NULL, 100),	
                    );
                    
                    //	Set the Data values for each data series we want to plot
                    //		Datatype
                    //		Cell reference for data
                    //		Format Code
                    //		Number of datapoints in series
                    //		Data values
                    //		Data Marker
                    $dataSeriesValues = array(
                        new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$'.$start_table.':$C$'.$x, NULL, 12),
                        new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$D$'.$start_table.':$D$'.$x, NULL, 12),
                        //new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$E$2:$E$13', NULL, 12),
                    );

                    //	Build the dataseries
                    $series = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
                        PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                        range(0, count($dataSeriesValues)-1),			// plotOrder
                        $dataseriesLabels,								// plotLabel
                        $xAxisTickValues,								// plotCategory
                        $dataSeriesValues								// plotValues
                    );
                    
                    //	Set additional dataseries parameters
                    //		Make it a vertical column rather than a horizontal bar graph
                    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                    //	Set the series in the plot area
                    $plotarea = new PHPExcel_Chart_PlotArea(NULL, array($series));
                    //	Set the chart legend
                    $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, false);

                    $title = new PHPExcel_Chart_Title('Hostel Room Occupancy');
                    $xAxisLabel = new PHPExcel_Chart_Title('Hostel Block');
                    $yAxisLabel = new PHPExcel_Chart_Title('No. of Room');


                    //	Create the chart
                    $chart = new PHPExcel_Chart(
                        'chart1',		// name
                        $title,			// title
                        $legend,		// legend
                        $plotarea,		// plotArea
                        true,			// plotVisibleOnly
                        0,				// displayBlanksAs
                        $xAxisLabel,	// xAxisLabel
                        $yAxisLabel		// yAxisLabel
                    );

                    //	Set the position where the chart should appear in the worksheet
                    $chart->setTopLeftPosition('G2');
                    $chart->setBottomRightPosition('P20');

                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                    ob_end_clean();
                
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="summary.xlsx"');
                    header('Cache-Control: max-age=0');

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->setIncludeCharts(TRUE);
                    ob_end_clean();
                    $objWriter->save('php://output');
                    /*
                     * FINISH OCCUPIED
                     */
                }
                elseif ($type == 'gender')
                {
                    /*
                     * START GENDER
                     */
                     
                    $objPHPExcel = new PHPExcel();

                    // Set document properties
                    $objPHPExcel->getProperties()
                        ->setCreator("AUCMS System")
                        ->setLastModifiedBy("AUCMS System")
                        ->setTitle("Report")
                        ->setSubject("Summary Report")
                        ->setDescription("Summary Report")
                        ->setKeywords("Summary Report")
                        ->setCategory("Summary Report");


                   
                    $objWorksheet = $objPHPExcel->getActiveSheet();
                    //$objWorksheet->fromArray(
                    
                    $chartGender = array(
                        array('','Male','Female')
                    );
                    
                    
                    $x = $start_table;
                    foreach($HostelBlocks as $hostel)
                    {
                        $pushChart = array($hostel['Name'],$genderHostel['table'][$hostel['IdHostelBlock']]['male'],$genderHostel['table'][$hostel['IdHostelBlock']]['female']);
                        
                        array_push($chartGender,$pushChart);
                        $x++;
                    }
                  
                    $objWorksheet->fromArray($chartGender);
                    $dataseriesLabels = array(
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$B$1', NULL, 1),	//	'Male'
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1),	//	'Female'
                        
                    );
                    //	Set the X-Axis Labels
                    //		Datatype
                    //		Cell reference for data
                    //		Format Code
                    //		Number of datapoints in series
                    //		Data values
                    //		Data Marker
                    $xAxisTickValues = array(
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$'.$start_table.':$A$'.$x, NULL, 100),	
                    );
                    //	Set the Data values for each data series we want to plot
                    //		Datatype
                    //		Cell reference for data
                    //		Format Code
                    //		Number of datapoints in series
                    //		Data values
                    //		Data Marker
                    $dataSeriesValues = array(
                        new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$'.$start_table.':$B$'.$x, NULL, 12),
                        new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$'.$start_table.':$C$'.$x, NULL, 12),
                        
                    );

                    //	Build the dataseries
                    $series = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
                        PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                        range(0, count($dataSeriesValues)-1),			// plotOrder
                        $dataseriesLabels,								// plotLabel
                        $xAxisTickValues,								// plotCategory
                        $dataSeriesValues								// plotValues
                    );
                    //	Set additional dataseries parameters
                    //		Make it a vertical column rather than a horizontal bar graph
                    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                    //	Set the series in the plot area
                    $plotarea = new PHPExcel_Chart_PlotArea(NULL, array($series));
                    //	Set the chart legend
                    $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, false);

                    $title = new PHPExcel_Chart_Title('Hostel Occupancy By Gender');
                    $xAxisLabel = new PHPExcel_Chart_Title('Hostel Block');
                    $yAxisLabel = new PHPExcel_Chart_Title('No. Gender');


                    //	Create the chart
                    $chart = new PHPExcel_Chart(
                        'chart1',		// name
                        $title,			// title
                        $legend,		// legend
                        $plotarea,		// plotArea
                        true,			// plotVisibleOnly
                        0,				// displayBlanksAs
                        $xAxisLabel,	// xAxisLabel
                        $yAxisLabel		// yAxisLabel
                    );

                    //	Set the position where the chart should appear in the worksheet
                    $chart->setTopLeftPosition('G2');
                    $chart->setBottomRightPosition('P20');

                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                    
                    ob_end_clean();
                
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="summary.xlsx"');
                    header('Cache-Control: max-age=0');

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->setIncludeCharts(TRUE);
                    ob_end_clean();
                    $objWriter->save('php://output');
                    /*
                     * END GENDER
                     */
                }
                elseif ($type == 'inventory')
                {
                     /*
                     * START INVENTORY
                     */
                    $objPHPExcel = new PHPExcel();

                    // Set document properties
                    $objPHPExcel->getProperties()
                        ->setCreator("AUCMS System")
                        ->setLastModifiedBy("AUCMS System")
                        ->setTitle("Report")
                        ->setSubject("Summary Report")
                        ->setDescription("Summary Report")
                        ->setKeywords("Summary Report")
                        ->setCategory("Summary Report");
                    
                    
                    $objWorksheet = $objPHPExcel->getActiveSheet();
                    $x = $start_table;
                    
                    $chartInventory = array(
                        array('','Bed','Study Table')
                    );
                    
                    foreach($HostelBlocks as $hostel)
                    {
                        
                        $pushChart = array($hostel['Name'],$inventoryList['table'][$hostel['IdHostelBlock']][1],$inventoryList['table'][$hostel['IdHostelBlock']][2]);
                        
                        array_push($chartInventory,$pushChart);
                        $x++;
                    }
                  
                    $objWorksheet->fromArray($chartInventory);
                    $dataseriesLabels = array(
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$B$1', NULL, 1),	//	'Male'
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1),	//	'Female'
                        
                    );
                    //	Set the X-Axis Labels
                    //		Datatype
                    //		Cell reference for data
                    //		Format Code
                    //		Number of datapoints in series
                    //		Data values
                    //		Data Marker
                    $xAxisTickValues = array(
                        new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$'.$start_table.':$A$'.$x, NULL, 100),	
                    );
                    //	Set the Data values for each data series we want to plot
                    //		Datatype
                    //		Cell reference for data
                    //		Format Code
                    //		Number of datapoints in series
                    //		Data values
                    //		Data Marker
                    $dataSeriesValues = array(
                        new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$'.$start_table.':$B$'.$x, NULL, 12),
                        new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$'.$start_table.':$C$'.$x, NULL, 12),
                        
                    );

                    //	Build the dataseries
                    $series = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
                        PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                        range(0, count($dataSeriesValues)-1),			// plotOrder
                        $dataseriesLabels,								// plotLabel
                        $xAxisTickValues,								// plotCategory
                        $dataSeriesValues								// plotValues
                    );
                    //	Set additional dataseries parameters
                    //		Make it a vertical column rather than a horizontal bar graph
                    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                    //	Set the series in the plot area
                    $plotarea = new PHPExcel_Chart_PlotArea(NULL, array($series));
                    //	Set the chart legend
                    $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, false);

                    $title = new PHPExcel_Chart_Title('Hostel Occupancy By Gender');
                    $xAxisLabel = new PHPExcel_Chart_Title('Hostel Block');
                    $yAxisLabel = new PHPExcel_Chart_Title('No. Gender');


                    //	Create the chart
                    $chart = new PHPExcel_Chart(
                        'chart1',		// name
                        $title,			// title
                        $legend,		// legend
                        $plotarea,		// plotArea
                        true,			// plotVisibleOnly
                        0,				// displayBlanksAs
                        $xAxisLabel,	// xAxisLabel
                        $yAxisLabel		// yAxisLabel
                    );

                    //	Set the position where the chart should appear in the worksheet
                    $chart->setTopLeftPosition('G2');
                    $chart->setBottomRightPosition('P20');

                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                    
                    ob_end_clean();
                
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="summary.xlsx"');
                    header('Cache-Control: max-age=0');

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->setIncludeCharts(TRUE);
                    ob_end_clean();
                    $objWriter->save('php://output');
                    
                    
                    /*
                     * END INVENTORY
                     */
                }
                
               
            }
        }
        
        exit;
    }
    
    private function generateGraph($data,$type)
    {
        include $this->public.'/jpgraph/src/jpgraph.php';
        include $this->public.'/jpgraph/src/jpgraph_bar.php';
        
        $path = $this->public.'/graph/';
        
        // Create the graph. These two calls are always required
        ob_end_clean();
        
        $graph = new Graph(650,400,'auto');
        $graph->SetScale("textlin");

        $theme_class=new UniversalTheme;
        $graph->SetTheme($theme_class);
        //$graph->yaxis->SetTickPositions(array(0,30,60,90,120,150), array(15,45,75,105,135));
        $graph->SetBox(false);
        
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels($data['name']);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);
        
        if($type == 'occupancy')
        {
            $data1y = $data['occupied'];
            $data2y = $data['available'];
           //Create the bar plots
            $b1plot = new BarPlot($data1y);
            $b2plot = new BarPlot($data2y);

            //Create the grouped bar plot
            $gbplot = new GroupBarPlot(array($b1plot,$b2plot));
            //...and add it to the graPH
            $graph->Add($gbplot);
            $b1plot->SetColor("white");
            $b1plot->SetFillColor("#cc1111");

            $b2plot->SetColor("white");
            $b2plot->SetFillColor("#11cccc");
            
            $graph->title->Set("Occupancy");
            
            $filename = 'occupancy.jpg';
            
        }
        elseif($type == 'gender')
        {
            $data1y = $data['male'];
            $data2y = $data['female'];
           //Create the bar plots
            $b1plot = new BarPlot($data1y);
            $b2plot = new BarPlot($data2y);

            //Create the grouped bar plot
            $gbplot = new GroupBarPlot(array($b1plot,$b2plot));
            //...and add it to the graPH
            $graph->Add($gbplot);
            $b1plot->SetColor("white");
            $b1plot->SetFillColor("#cc1111");

            $b2plot->SetColor("white");
            $b2plot->SetFillColor("#11cccc");
            
            $graph->title->Set("Gender");
            
            $filename = 'gender.jpg';
        }
        elseif($type == 'inventory')
        {
            $data1y = $data['bed'];
            $data2y = $data['studytable'];
           //Create the bar plots
            $b1plot = new BarPlot($data1y);
            $b2plot = new BarPlot($data2y);

            //Create the grouped bar plot
            $gbplot = new GroupBarPlot(array($b1plot,$b2plot));
            //...and add it to the graPH
            $graph->Add($gbplot);
            $b1plot->SetColor("white");
            $b1plot->SetFillColor("#cc1111");

            $b2plot->SetColor("white");
            $b2plot->SetFillColor("#11cccc");
            
            $graph->title->Set("Gender");
            
            $filename = 'inventory.jpg';
        }
        
        // Display the graph
        $graph->img->SetImgFormat('jpeg');
        ob_end_clean();
        
        //$graph->Stroke();
        $graph->Stroke($path.$filename);
        
    }
  
}
?>