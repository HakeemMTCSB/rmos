<?php
class Hostel_HostelconfigurationController extends Base_Base { //Controller for the REcords Module

	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjhostelConfigurationForm = new Hostel_Form_Hostelconfiguration();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
		//$this->lobjhostelconfigModel = Hostel_Model_DbTable_Hostelconfig::singleton();
	}

	/**
	 * Function to display the hostel Configuration setup form
	 * @author: Vipul
	 */
	public function indexAction() {
		$this->view->lobjhostelConfigurationForm = $this->lobjhostelConfigurationForm;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;

		// GET THE charges Rate list for hostel Configuration setup
		$defName = 'Hostel Configuration Charges Rate';
		$chargeRateData = $this->lobjdeftype->fnGetDefinationsStatus($defName);
		$this->view->lobjhostelConfigurationForm->chargesRate->addMultiOptions($chargeRateData);
		$IdUniversity = $this->gobjsessionsis->idUniversity;

		if ($this->_request->isPost () && $this->_request->getPost( 'Add' )) {
			$larrformData = $this->_request->getPost ();

			$larrformData['IdUniversity'] = $IdUniversity;
			$hostelconfigDetail = array();
			$hostelconfigDetail = $this->lobjhostelconfigModel->fnfetchConfiguration($IdUniversity);

			if(empty($hostelconfigDetail)) {
				$this->lobjhostelconfigModel->fnsaveHostelConfig($larrformData,$userId);
			}else{
				$this->lobjhostelconfigModel->fnupdateHostelConfig($larrformData,$IdUniversity);
			}
			$this->_redirect( $this->baseUrl . '/hostel/hostelconfiguration');
		}

		// FETCH and POPULATE the resulted Data into form.
		$resultData = $this->lobjhostelconfigModel->fnfetchConfiguration($IdUniversity);
		$this->view->hostelconfig = $resultData[0];

		if(!empty($resultData)) {
			$this->view->chargesListData = $resultData[0]['chargesRate'];
			$this->view->lobjhostelConfigurationForm->populate($resultData[0]);
		}
	}


}
?>