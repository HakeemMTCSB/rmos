<?php

class Hostel_HostelinformationController extends Base_Base {

	private $_gobjlog;
	private $lobjhostellevelForm;
	private $lobjhostelLevelModel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjhostelHostelinfoForm = new Hostel_Form_Hostelinfo();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjBlockinfoModel = new Hostel_Model_DbTable_Blockinfo();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjCommonModel = new App_Model_Common();
		$this->lobjhostellevelForm = new Hostel_Form_Hostellevel();
		$this->lobjhostelLevelModel = new Hostel_Model_DbTable_Hostellevel();
		$this->lobjhostelroom = new Hostel_Model_DbTable_Hostelroom();
		$this->lobjinventoryconfigModel = new Hostel_Model_DbTable_Inventorysetup();
		$this->lobjroominfo = new Hostel_Model_DbTable_Roominfo();
		$this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
	}

	/**
	 * Function to display the hostel Blockinfo setup form and SEARCH
	 * @author: Vipul
	 */
	public function indexAction() {
		$this->view->lobjform = $this->lobjhostelHostelinfoForm;
		$larrresult = $this->lobjBlockinfoModel->fnfetchAllBlockinfo($post = NULL);
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		if (!$this->_getParam('search')) {
			unset($this->gobjsessionsis->progstudentregistrationpaginatorresult);
		}
		
		$IdUniversity = $this->gobjsessionsis->idUniversity; 
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;

		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page', 1); // Paginator instance
		if (isset($this->gobjsessionsis->progstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progstudentregistrationpaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
		}


		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();

			//var_dump($larrformData); exit;

			//$larrformData['Name'] = $this->

			$larrresult = $this->lobjBlockinfoModel->fnfetchAllBlockinfo($larrformData); //searching the values for the user
			if(count($larrresult)==0) {
				$this->view->blankMsg = '1';
			}
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
			$this->gobjsessionsis->progstudentregistrationpaginatorresult = $larrresult;
		}
			



	}


	/**
	 * Function to ADD BLOCK and UPDATE
	 * @author: Vipul
	 */
	public function addblockinfoAction() {
		$this->view->lobjform = $this->lobjhostelHostelinfoForm;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->view->lobjform->Country->addMultiOptions($lobjcountry);

		$blockid = $this->_getParam('blockid');
		$this->view->blockid = $blockid;

		$this->view->lobjform->Name->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_block',
				'field' => 'Name',
				'exclude' => array('field' => 'IdHostelBlock','value' => $this->_getParam('blockid', 0))
		));
		$this->view->lobjform->Code->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_block',
				'field' => 'Code',
				'exclude' => array('field' => 'IdHostelBlock','value' => $this->_getParam('blockid', 0))
		));
		$this->view->lobjform->Code->getValidator('Db_NoRecordExists')->setMessage("Code already exists. Please try a different.");
		$this->view->lobjform->Name->getValidator('Db_NoRecordExists')->setMessage("Name already exists. Please try a different.");




		if ($this->_request->isPost() && $this->_request->getPost('Save')) {

			$larrformData = $this->_request->getPost();
			if ($this->view->lobjform->isValid($larrformData))
			{
				$this->lobjBlockinfoModel->fnsaveblock($larrformData, $userId, $blockid);
				$this->_redirect($this->baseUrl . '/hostel/hostelinformation/index');
			} else {
				$this->view->lobjform->populate($larrformData);
			}
		}

		if($blockid!='') {
			$where = "a.IdHostelBlock = '".$blockid."' ";
			$larrresult = $this->lobjBlockinfoModel->fnfetchBlockByID( $where );
			if(count($larrresult)>0) {

				$getLevels =  $this->lobjhostelLevelModel->fnCheckLevel($blockid);

				if(count($getLevels)==0) {
					$this->view->delMsg = '1'; // delete the block
				} else {
					$this->view->delMsg = '0'; // donot delete the block
				}
				$this->view->lobjform->populate($larrresult[0]);
				$this->view->larrresult = $larrresult[0];
				$this->view->lobjform->HomePhone->setValue ( $larrresult[0] ['Phone'] );
				$this->view->lobjform->Fax->setValue ( $larrresult[0] ['Fax'] );


				if (isset($larrresult[0]['Country']) && $larrresult[0]['Country'] != ''){
					$PermState = $this->lobjCommonModel->fnGetCountryStateList($larrresult[0]['Country']);
					foreach ($PermState as $PermStateresult) {
						$this->view->lobjform->State->addMultiOption($PermStateresult['key'], $PermStateresult['value']);
					}
				}
				if (isset($larrresult[0]['State']) && $larrresult[0]['State'] != ''){
					$larrStateCityList = $this->lobjCommonModel->fnGetCityList($larrresult[0]['State']);
					foreach ($larrStateCityList as $PermCityresult) {
						$this->view->lobjform->City->addMultiOption($PermCityresult['key'], $PermCityresult['value']);
					}
				}

			}
		}
	}

	/**
	 * Function to DELETE Block
	 * @author: Vipul
	 */
	public function delblockAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('id');
		$result = $this->lobjBlockinfoModel->fndeleteblock($id);
		if ($result) {
			echo '1';
			die;
		}
	}

	/*
	 *  Function to list the hostel level
	*/

	public function hostellevelAction(){
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform;
		$blockList = $this->lobjBlockinfoModel->fngetListofBlocks();
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjform->field8->addMultioptions($blockList);
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		if (!$this->_getParam('search')){
			unset($this->gobjsessionsis->changestatuspaginatorresult);
			$larrresult = $this->lobjhostelLevelModel->getAllLevel();
		}
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset($this->gobjsessionsis->changestatuspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->changestatuspaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}

		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			$larrresult = $this->lobjhostelLevelModel->fnSearchhostelLevel($larrformData);
			if(count($larrresult)==0) {
				$this->view->blankMsg = '1';
			}
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
			$this->gobjsessionsis->changestatuspaginatorresult = $larrresult;
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostellevel');
		}


		if (isset($this->gobjsessionsis->changestatuspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->changestatuspaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
	}

	public function addhostellevelAction(){
		$this->view->lobjhostellevelForm = $this->lobjhostellevelForm;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$date = date("Y-m-d");
		$this->view->lobjhostellevelForm->UpdDate->setValue($date);
		$auth = Zend_Auth::getInstance();
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$this->view->lobjhostellevelForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $larrresult = $this->lobjinventoryconfigModel->fnfetchInventoryList();
        $this->view->lobjhostellevelForm->InventoryName->addMultioptions($larrresult);
		/*$this->view->lobjhostellevelForm->Name->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_level',
				'field' => 'Name',
				'exclude' => array(
						'field' => 'IdHostelLevel',
						'value' => $this->_getParam('id', 0)
				)
		)
		);
		$this->view->lobjhostellevelForm->Code->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_level',
				'field' => 'Code',
				'exclude' => array(
						'field' => 'IdHostelLevel',
						'value' => $this->_getParam('id', 0)
				)
		)
		);
		$this->view->lobjhostellevelForm->Name->getValidator('Db_NoRecordExists')->setMessage("Name already exists");
		$this->view->lobjhostellevelForm->Code->getValidator('Db_NoRecordExists')->setMessage("Code already exists");
		*/
		
		if ($this->_request->isPost()) 
		{
			$formData = $this->_request->getPost();
			if ($this->lobjhostellevelForm->isValid($formData)) 
			{
                $auth = Zend_Auth :: getInstance();
                $userId = $auth->getIdentity()->iduser;
				if ( $this->lobjhostelLevelModel->checkDuplicate($formData) || 1 )
				{
                    $levelId = $this->lobjhostelLevelModel->saveHostelLevel($formData);
                    if(isset($formData['InventoryNamegrid']))
                    {
                        $this->lobjhostelLevelModel->fnsavelevelinventoryinfo($formData,$userId,$levelId);
                    }
					
					$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostellevel');
				}
				else
				{
					$this->view->errMSg = 1;
				}
			}
		}

	}

	public function edithostellevelAction(){
		$this->view->lobjhostellevelForm = $this->lobjhostellevelForm;
		$IdLevel = $this->_getParam('id');
	    $this->view->IdHostelLevel = $IdLevel;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$levelDet = $this->lobjhostelLevelModel->getLevelDet($IdLevel);
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjhostellevelForm->populate($levelDet);


		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            $IdLevel = $formData['IdHostelLevel'];
            $this->view->lobjhostellevelForm->Name->addValidator('Db_NoRecordExists', true, array(
                    'table' => 'tbl_hostel_level',
                    'field' => 'Name',
                    'exclude' => array(
                        'field' => 'IdHostelLevel',
                        'value' => $IdLevel
                    )
                )
            );
            $this->view->lobjhostellevelForm->Name->getValidator('Db_NoRecordExists')->setMessage("Name already exists");

            $this->view->lobjhostellevelForm->Code->addValidator('Db_NoRecordExists', true, array(
                    'table' => 'tbl_hostel_level',
                    'field' => 'Code',
                    'exclude' => array(
                        'field' => 'IdHostelLevel',
                        'value' => $IdLevel
                    )
                )
            );
            $this->view->lobjhostellevelForm->Code->getValidator('Db_NoRecordExists')->setMessage("Code already exists");

			if ($this->lobjhostellevelForm->isValid($formData)) {
				$this->lobjhostelLevelModel->UpdateHostelLevel($formData,$IdLevel);

                $this->lobjhostelLevelModel->fndeletelevelinventoryinfo($IdLevel);
                if(isset($formData['InventoryNamegrid']))
                {
                    $this->lobjhostelLevelModel->fnsavelevelinventoryinfo($formData,$userId,$IdLevel);
                }

				$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostellevel');
			}
		}

        $this->view->inventoryinfo = $this->lobjhostelLevelModel->fngetlevelinventoryinfo($IdLevel);
        $larrresult = $this->lobjinventoryconfigModel->fnfetchInventoryList();
        $this->view->lobjhostellevelForm->InventoryName->addMultioptions($larrresult);
	}

	//	public function addhostellevelAction(){
	//		$this->view->lobjhostellevelForm = $this->lobjhostellevelForm;
	//		$date = date("Y-m-d");
	//		$this->view->lobjhostellevelForm->UpdDate->setValue($date);
	//		$auth = Zend_Auth::getInstance();
	//
	//		$this->view->lobjhostellevelForm->UpdUser->setValue($auth->getIdentity()->iduser);
	//		if ($this->_request->isPost()) {
	//			$formData = $this->_request->getPost();
	//			$this->lobjhostelLevelModel->saveHostelLevel($formData);
	//			$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostellevel');
	//		}
	//
	//	}
	//
	//	public function edithostellevelAction(){
	//		$this->view->lobjhostellevelForm = $this->lobjhostellevelForm;
	//		$IdLevel = $this->_getParam('id');
	//		$levelDet = $this->lobjhostelLevelModel->getLevelDet($IdLevel);
	//		$this->view->lobjhostellevelForm->populate($levelDet);
	//		if ($this->_request->isPost()) {
	//			$formData = $this->_request->getPost();
	//			$this->lobjhostelLevelModel->UpdateHostelLevel($formData,$IdLevel);
	//			$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostellevel');
	//		}
	//
	//	}

	public function hostelroomAction(){
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform;
		$blockList = $this->lobjBlockinfoModel->fngetListofBlocks();
		$this->view->lobjform->field10->addMultioptions($blockList);
		$levelList = $this->lobjhostelLevelModel->fngetAllLevelList();
		$this->view->lobjform->field8->addMultioptions($levelList);
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		if (!$this->_getParam('search')){
			unset($this->gobjsessionsis->roompaginatorresult);
			$larrresult = $this->lobjroominfo->fngetallroominfo();
		}
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset($this->gobjsessionsis->roompaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->roompaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			$larrresult = $this->lobjroominfo->fnSearchhostelroom($larrformData);
			if(count($larrresult)==0) {
				$this->view->blankMsg = '1';
			}
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
			$this->gobjsessionsis->roompaginatorresult = $larrresult;
		}
		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostelroom');
		}
			
	}


	public function addhostelroomAction(){
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjhostelHostelinfoForm = $this->lobjhostelHostelinfoForm;
		$blockList = $this->lobjBlockinfoModel->fngetListofBlocks();
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$this->view->lobjhostelHostelinfoForm->Gender->addMultiOptions(array('1'=>'Male',
				'2'=>'Female'));
		
		$this->view->lobjhostelHostelinfoForm->Block->addMultioptions($blockList);
		$roomtypelist = $this->lobjhostelroom->fngetListofroomtypes();
		$this->view->lobjhostelHostelinfoForm->RoomType->addMultioptions($roomtypelist);
		$larrresult = $this->lobjinventoryconfigModel->fnfetchInventoryList();
		$this->view->lobjhostelHostelinfoForm->InventoryName->addMultioptions($larrresult);
		/*$this->lobjhostelHostelinfoForm->RoomName->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room',
				'field' => 'RoomName',
				'exclude' => array('field' => 'IdHostelRoom','value' => $this->_getParam('id', 0))
		));
		$this->lobjhostelHostelinfoForm->RoomCode->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room',
				'field' => 'RoomCode',
				'exclude' => array('field' => 'IdHostelRoom','value' => $this->_getParam('id', 0))
		));
		$this->lobjhostelHostelinfoForm->RoomCode->getValidator('Db_NoRecordExists')->setMessage("Code already exists. Please try a different.");
		$this->lobjhostelHostelinfoForm->RoomName->getValidator('Db_NoRecordExists')->setMessage("Name already exists. Please try a different.");
		*/
		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
			if ($this->lobjhostelHostelinfoForm->isValid($formData))
			{
		
				$auth = Zend_Auth :: getInstance();
				$userId = $auth->getIdentity()->iduser;
				
				if ( $this->lobjroominfo->checkDuplicate($formData) )
				{
					$roomId = $this->lobjroominfo->fnsaveroominfo($formData,$userId);
				
					if(isset($formData['InventoryNamegrid']))
					{
						$this->lobjroominfo->fnsaveroominventoryinfo($formData,$userId,$roomId);
					}
				
					$this->_redirect($this->baseUrl . '/hostel/hostelinformation/hostelroom');
				}
				else
				{
					
					$this->view->errMSg = 1;
				}
				
				
			}
			else 
			{
				$this->lobjhostelHostelinfoForm->populate($formData);
			}

		}
			
	}

	public function edithostelroomAction(){
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjhostelHostelinfoForm = $this->lobjhostelHostelinfoForm;
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$this->view->lobjhostelHostelinfoForm->Gender->addMultiOptions(array('1'=>'Male',
				'2'=>'Female'));
		
		$blockList = $this->lobjBlockinfoModel->fngetListofBlocks();
		$this->view->lobjhostelHostelinfoForm->Block->addMultioptions($blockList);
		$roomtypelist = $this->lobjhostelroom->fngetListofroomtypes();
		$this->view->lobjhostelHostelinfoForm->RoomType->addMultioptions($roomtypelist);
		$larrresult = $this->lobjinventoryconfigModel->fnfetchInventoryList();
		$this->view->lobjhostelHostelinfoForm->InventoryName->addMultioptions($larrresult);
		$roomId = $this->_getParam("id");
		$roominfo = $this->lobjroominfo->fngetroominfo($roomId);
		
		$this->view->GenderV = '';
		if($roominfo){
			$this->view->lobjhostelHostelinfoForm->IdHostelRoom->setValue($roominfo[0]['IdHostelRoom']);
			//$this->view->lobjhostelHostelinfoForm->Gender->setValue($roominfo[0]['Gender']);
			$this->view->GenderV = $roominfo[0]['Gender'];
			$levelList = $this->lobjhostelLevelModel->getAllLevelList($roominfo[0]['Block']);
		}
		foreach($levelList as $level_list) {
			$finalLevelList[] = array(
					'key' => $level_list['key'],
					'value' => $level_list['name']
				);

		}
		
		$this->view->lobjhostelHostelinfoForm->Level->addMultioptions($finalLevelList);
		$this->lobjhostelHostelinfoForm->populate($roominfo[0]);
		$this->view->inventoryinfo = $this->lobjroominfo->fngetinventoryinfo($roomId);
		/*$this->lobjhostelHostelinfoForm->RoomName->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room',
				'field' => 'RoomName',
				'exclude' => array('field' => 'IdHostelRoom','value' => $this->_getParam('id', 0))
		));*/
		$this->lobjhostelHostelinfoForm->RoomCode->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room',
				'field' => 'RoomCode',
				'exclude' => array('field' => 'IdHostelRoom','value' => $this->_getParam('id', 0))
		));
		$this->lobjhostelHostelinfoForm->RoomCode->getValidator('Db_NoRecordExists')->setMessage("Code already exists. Please try a different.");
		//$this->lobjhostelHostelinfoForm->RoomName->getValidator('Db_NoRecordExists')->setMessage("Name already exists. Please try a different.");

		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
                        //var_dump($formData); exit;
			if ($this->lobjhostelHostelinfoForm->isValid($formData))
			{
				$auth = Zend_Auth :: getInstance();
				$userId = $auth->getIdentity()->iduser;
				$this->lobjroominfo->fnupdatehostelroom($formData,$userId);
				$this->lobjroominfo->fndeleteroominventoryinfo($formData['IdHostelRoom']);
				if(isset($formData['InventoryNamegrid'])){
					$this->lobjroominfo->fnsaveroominventoryinfo($formData,$userId,$roomId);
				}
				$this->redirect($this->baseUrl . '/hostel/hostelinformation/hostelroom');
			}
			else{
				$this->lobjhostelHostelinfoForm->populate($formData);
			}
		}

	}

	public function getlevellistAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idblock = $this->_getParam('Idblock');
		$levelList = $this->lobjhostelLevelModel->getAllLevelList($Idblock);
		echo Zend_Json_Encoder::encode($levelList);
	}
	
	public function getroomlistAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idblock = $this->_getParam('Idblock');
		$Idlevel = $this->_getParam('Idlevel');
		
		$roomList = $this->lobjhostelroom->fnGetRooms($Idblock,$Idlevel);
		echo Zend_Json_Encoder::encode($roomList);
	}
}

?>
