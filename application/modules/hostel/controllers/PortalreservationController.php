<?php

class Hostel_PortalreservationController extends Base_Base 
{
	public function init() {

		$this->RoomType = new Hostel_Model_DbTable_Hostelroom();
		$this->StudentReservation = new Hostel_Model_DbTable_Hostelreservation();

	}
	
	public function newAction() {

		$post_data = $this->_request->getPost();

		if(!empty($post_data)) {
			$new_reservation['IdHostelRoomType'] = $post_data['IdHostelRoomType'];
			$current_charge = $this->RoomType->get_roomtype_active_charges($post_data['IdHostelRoomType']);
			/*if(isset($current_charge['IdRoomCharges'])) {
				$new_reservation['charge_id'] = $current_charge['IdRoomCharges'];
			}*/
			//$new_reservation['duration'] =  $post_data['duration'];
			$auth = Zend_Auth::getInstance();

			$new_reservation['IdStudentRegistration'] =  $auth->getIdentity()->iduser;
			$new_reservation['cust_type'] = 1;
			$new_reservation['Active'] = 1;
			$new_reservation['Status'] = 265;
			$new_reservation['duration'] =  $post_data['duration'];
			$new_reservation['Remarks'] = 'Student application via student portal';
			$new_reservation['UpdUser'] = $auth->getIdentity()->iduser;
			$new_reservation['UpdDate'] = date('Y-m-d H:i:s');

			$this->StudentReservation->save($new_reservation);
			$this->gobjsessionsis->flash = array('message' => 'Reservation made.', 'type' => 'success');
			//TODO:redirect somewhere;
		}

		$roomtypes = $this->RoomType->fngethostelroomdetail();
		foreach($roomtypes as $key => $roomtype) {
			$roomtypes[$key] = $this->RoomType->get_roomtype_active_charges($roomtype['IdHostelRoomType']);
		}
				
		$this->view->roomtypes = $roomtypes;

	}
	
}
