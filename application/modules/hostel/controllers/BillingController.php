<?php
/**
	invoice are stored in invoice and invoice details table.
	some invoice needs to be triggered at various places
	advance payment is a place to store returnable deposits.
	create table to mark invoice created for that month so we know the ones that has been invoiced
**/


class Hostel_BillingController extends Base_Base {

	public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array() ) {
		parent::__construct($request, $response, $invokeArgs);

		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object


		$this->HostelRoom = new Hostel_Model_DbTable_Hostelroom();
		$this->RoomRegistration = new Hostel_Model_DbTable_Roomregistration();
		$this->StudentRegistration = new Registration_Model_DbTable_Studentregistration();
		$this->HostelRoomType = new Hostel_Model_DbTable_Hostelroom();

		$this->Semester = new GeneralSetup_Model_DbTable_Semester();
		$this->DefinitionType = new App_Model_Definitiontype();
		$this->HostelConfig = new Hostel_Model_DbTable_Hostelconfig();
		$this->Intake = new GeneralSetup_Model_DbTable_Intake();
		$this->HostelFee = new Hostel_Model_DbTable_HostelFeeInvoice();
	}


	/*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['invoice_no'];
	}

	public function indexAction() {
		

		$this->view->fee_items = Studentfinance_Model_DbTable_FeeItem::getListByCodes("REN");
		
		//if there's a search
		if ($this->_request->isPost()) {
			$form_data = $this->_request->getPost();
		} else {
			$form_data = array();
		}

		$hostel_fees_select = $this->HostelFee->getHostelFees($form_data);
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($hostel_fees_select));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
		$this->view->hostel_fees = $paginator;

	}

	public function addAction() {
		$month = $this->_getParam('month', date('m'));
		$year = $this->_getParam('year', date('Y'));
		$monthyear = $month . '/' . $year;

		$page_count = $this->gintPageCount;
		$cur_page = $this->_getParam('page', 1);
	
		$form_data = array();
		$registrations = $this->RoomRegistration->getActiveRegistrations($form_data);

		if(count($registrations)==0) {
			$this->view->blankMsg = '1';
		}
		
		//get the ones we haven't invoiced yet
		$this->view->notyetinvoiceds = $this->HostelFee->findNotYetInvoiced($monthyear);

		$this->view->roomtypes = $this->HostelRoomType->fngetListofroomtypes();
		

		$this->view->fee_items = Studentfinance_Model_DbTable_FeeItem::getListByCodes("Ren");
		$this->view->hostel_registrations = $this->lobjCommon->fnPagination($registrations, $cur_page, $page_count);
	}


	/**
	*
	* this generates invoice from the billing only
	* too fat. put this bugger on diet
	**/
	public function generateInvoiceAction() {
		$auth = Zend_Auth :: getInstance();
		$userId = $auth->getIdentity()->iduser;
		$StudentFinance = new Studentfinance_Model_DbTable_Currency();
		$default_cur = $StudentFinance->getDefaultCurrency();

		if ($this->_request->isPost()) {
			$form_data = $this->_request->getPost();
                        
			$auth = Zend_Auth :: getInstance();
			$userId = $auth->getIdentity()->iduser;
			$monthyear = $form_data['fee_month'] . '/' . $form_data['fee_year'];

			$StudentFinanceFeeCategory = new Studentfinance_Model_DbTable_FeeCategory();
			$fee_category = $StudentFinanceFeeCategory->getByCode('Ren');
			$fee_category_id = $fee_category['fc_id'];

			foreach($form_data['IdHostelregistration'] as $IdHostelregistration) {
			
			//LoooOoOOoOOOng one.
			//get RoomRegistration
				$hostel_registration = $this->RoomRegistration->getRegistration($IdHostelregistration);
				$room_type_id = $hostel_registration['IdHostelRoomType'];

				//263 is of type student
				$cust_type = 263;
				$check_in = $hostel_registration['CheckInDate'];
				//get student appl
				$IdStudentRegistration = $hostel_registration['IdStudentRegistration'];
				$student = $this->StudentRegistration->getData($IdStudentRegistration);

				$at_trans_id = $student['transaction_id'];
				$appl_id = $student['appl_id'];
				$ap_prog_id = $student['IdProgram'];
				$at_fs_id= $student['fs_id'];

				//TODO: remove. this is test
				if(empty($at_fs_id)) {
					$at_fs_id = 0;
				}
				
				$date = date('Y-m-d', strtotime('01-'.$form_data['fee_month'].'-'.$form_data['fee_year']));
				//var_dump($date); exit;
				$calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
				
				
				$invoice_desc = $form_data['description'];//get current currency rate 
				$currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
				$currencyRate = $currencyRateDB->getCurrentExchangeRate($calculated_charges['currency_id']);
				$exchangeRate = $currencyRate['cr_id'];
				
				//$FeeStructure = new Studentfinance_Model_DbTable_FeeStructure();
				//$feeStructureData = $FeeStructure->fetchRow('fs_id = '.$student['at_fs_id'])->toArray();

				//semester
					$semesterDb = new Registration_Model_DbTable_Semester();
					$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $student['IdScheme']));
					
				//add main
				$Invoice = new Studentfinance_Model_DbTable_InvoiceMain();
				$bill_no = $this->getBillSeq(2, date('Y'));

				if ( $form_data['fee_item_id'] ==  Studentfinance_Model_DbTable_FeeItem::getFeeItemIdByCode('REN-MTH') ) {
				
					$calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
					
					
					$data = array(
							'bill_number' => $bill_no,
							'appl_id' => $appl_id,
							'trans_id' => $at_trans_id,
							'IdStudentRegistration' => $IdStudentRegistration,
							//'academic_year' => $semester['AcademicYear'],
							'semester' => $semester['IdSemesterMaster'],
							'bill_amount' => $calculated_charges['default_currency_amount'],
							'bill_balance' => $calculated_charges['default_currency_amount'],
							//'bill_description' => $invoice_desc . '-' . $monthyear,
                                                        'bill_description' => $invoice_desc,
							'program_id' => $ap_prog_id,
							'invoice_date' => $date,
							'creator' => $userId,
							'fs_id' => $at_fs_id,
							'status' => 'A',
							'currency_id' => $calculated_charges['currency_id'],
							'bill_amount_default_currency' => $calculated_charges['default_currency_amount'],
							'exchange_rate' => $exchangeRate,
							//'fee_category' => $fee_category_id
					);

					$main_id = $Invoice->insert($data);
				
					
					//add invoice detail. 1 to 1 thing

					//get fee item
					$FeeItem = new Studentfinance_Model_DbTable_FeeItem();
					$fee_item = $FeeItem->getData($form_data['fee_item_id']);
						
					$data_detail = array(
										'invoice_main_id' => $main_id,
										'fi_id' => $form_data['fee_item_id'],
										'fee_item_description' => $fee_item['fi_name'],
                                                                                //'fee_item_description' => $fee_item['fi_name'] . ' - ' . $monthyear,
										'cur_id' => $calculated_charges['currency_id'],
										'amount' => $calculated_charges['amount'],
										'balance' => $calculated_charges['amount'],
										'amount_default_currency' => $calculated_charges['default_currency_amount'],
										'exchange_rate' => $exchangeRate,
									);
					

					$InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					$InvoiceDetailDb->insert($data_detail);

				
				} else if ($form_data['fee_item_id'] ==  Studentfinance_Model_DbTable_FeeItem::getFeeItemIdByCode('REN-PEN')) {
					
					$calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
					

					$data = array(
							'bill_number' => $bill_no,
							'appl_id' => $appl_id,
							'trans_id' => $at_trans_id,
							'IdStudentRegistration' => $IdStudentRegistration,
							//'academic_year' => $semester['AcademicYear'],
							'semester' => $semester['IdSemesterMaster'],
							'bill_amount' => $form_data['fee_amount'],
							'bill_balance' => $form_data['fee_amount'],
							'bill_description' => $invoice_desc,
							'program_id' => $ap_prog_id,
							'creator' => $userId,
							'fs_id' => $at_fs_id,
							'status' => 'A',
							'currency_id' => $calculated_charges['currency_id'],
							'bill_amount_default_currency' => $form_data['fee_amount'],
							'exchange_rate' => $exchangeRate,
							'invoice_date' => $date,	
							//'invoice_type' => 'PROCESSING',
							//'fee_category' => $fee_category_id
					);

					$main_id = $Invoice->insert($data);
				
				
					//add invoice detail. 1 to 1 thing

					//get fee item
					$FeeItem = new Studentfinance_Model_DbTable_FeeItem();
					$fee_item = $FeeItem->getData($form_data['fee_item_id']);
						
					$data_detail = array(
										'invoice_main_id' => $main_id,
										'fi_id' => $form_data['fee_item_id'],
										'fee_item_description' => $fee_item['fi_name'],
										'cur_id' => $calculated_charges['currency_id'],
										'amount' => $form_data['fee_amount'],
										'balance' => $form_data['fee_amount'],
										'amount_default_currency' =>  $form_data['fee_amount'],
										'exchange_rate' => $exchangeRate,
									);
					

					$InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					$InvoiceDetailDb->insert($data_detail);

				}  else if ($form_data['fee_item_id'] ==  Studentfinance_Model_DbTable_FeeItem::getFeeItemIdByCode('REN-UTI')) {

					$calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
					
					$data = array(
						'bill_number' => $bill_no,
						'appl_id' => $appl_id,
						'trans_id' => $at_trans_id,
						'IdStudentRegistration' => $IdStudentRegistration,
						//'academic_year' => $semester['AcademicYear'],
						'semester' => $semester['IdSemesterMaster'],
						'bill_amount' => $form_data['fee_amount'],
						'bill_balance' => $form_data['fee_amount'],
						'bill_description' => $invoice_desc,
						'program_id' => $ap_prog_id,
						'creator' => $userId,
						'fs_id' => $at_fs_id,
						'status' => 'A',
						'currency_id' => $calculated_charges['currency_id'],
						'bill_amount_default_currency' => $form_data['fee_amount'],
						'invoice_date' => $date,
						'exchange_rate' => $exchangeRate,
						//'invoice_type' => 'PROCESSING',
						//'fee_category' => $fee_category_id
					);

					$main_id = $Invoice->insert($data);


					//add invoice detail. 1 to 1 thing

					//get fee item
					$FeeItem = new Studentfinance_Model_DbTable_FeeItem();
					$fee_item = $FeeItem->getData($form_data['fee_item_id']);

					$data_detail = array(
						'invoice_main_id' => $main_id,
						'fi_id' => $form_data['fee_item_id'],
						'fee_item_description' => $fee_item['fi_name'],
						'cur_id' => $calculated_charges['currency_id'],
						'amount' => $form_data['fee_amount'],
						'balance' => $form_data['fee_amount'],
						'amount_default_currency' =>  $form_data['fee_amount'],
						'exchange_rate' => $exchangeRate,
					);


					$InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					$InvoiceDetailDb->insert($data_detail);

				} else if ($form_data['fee_item_id'] ==  Studentfinance_Model_DbTable_FeeItem::getFeeItemIdByCode('REN-PF')) {
					

					$admin_charge = $this->calculate_hostel_admin($room_type_id, $cust_type, $date);
					$data = array(
							'bill_number' => $bill_no,
							'appl_id' => $appl_id,
							'trans_id' => $at_trans_id,
							'IdStudentRegistration' => $IdStudentRegistration,
							//'academic_year' => $semester['AcademicYear'],
							'semester' => $semester['IdSemesterMaster'],
							'bill_amount' => $admin_charge['amount'],
							'bill_balance' => $admin_charge['amount'],
							'bill_description' => $invoice_desc,
							'program_id' => $ap_prog_id,
							'creator' => $userId,
							'fs_id' => $at_fs_id,
							'status' => 'A',
							'currency_id' => $admin_charge['currency_id'],
							'bill_amount_default_currency' => $admin_charge['default_currency_amount'],
							'exchange_rate' => $exchangeRate,
							'invoice_date' => $date,
							//'invoice_type' => 'PROCESSING',
							//'fee_category' => $fee_category_id
					);

					$main_id = $Invoice->insert($data);
				
				
					//add invoice detail. 1 to 1 thing

					//get fee item
					$FeeItem = new Studentfinance_Model_DbTable_FeeItem();
					$fee_item = $FeeItem->getData($form_data['fee_item_id']);
						
					$data_detail = array(
										'invoice_main_id' => $main_id,
										'fi_id' => $form_data['fee_item_id'],
										'fee_item_description' => $fee_item['fi_name'],
										'cur_id' => $admin_charge['currency_id'],
										'amount' => $admin_charge['amount'],
										'balance' => $admin_charge['amount'],	
										'amount_default_currency' =>  $admin_charge['default_currency_amount'],
										'exchange_rate' => $exchangeRate,
					
									);
					

					$InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					$InvoiceDetailDb->insert($data_detail);

				} else if ($form_data['fee_item_id'] ==  Studentfinance_Model_DbTable_FeeItem::getFeeItemIdByCode('REN-DEP')) {

					//$calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
					$deposit = $this->calculate_hostel_deposit($room_type_id, $cust_type, $date);
					$data = array(
							'bill_number' => $bill_no,
							'appl_id' => $appl_id,
							'trans_id' => $at_trans_id,
							'IdStudentRegistration' => $IdStudentRegistration,
							//'academic_year' => $semester['AcademicYear'],
							'semester' => $semester['IdSemesterMaster'],
							'bill_amount' => $deposit['amount'],
							'bill_balance' => $deposit['amount'],
							'bill_description' => $invoice_desc,
							'program_id' => $ap_prog_id,
							'creator' => $userId,
							'fs_id' => $at_fs_id,
							'status' => 'A',
							'currency_id' => $deposit['currency_id'],
							'bill_amount_default_currency' => $deposit['default_currency_amount'],
							'exchange_rate' => $exchangeRate,
							'invoice_date' => $date,
							//'invoice_type' => 'PROCESSING',
							//'fee_category' => $fee_category_id
					);

					$main_id = $Invoice->insert($data);
				
				
					//add invoice detail. 1 to 1 thing

					//get fee item
					$FeeItem = new Studentfinance_Model_DbTable_FeeItem();
					$fee_item = $FeeItem->getData($form_data['fee_item_id']);
						
					$data_detail = array(
										'invoice_main_id' => $main_id,
										'fi_id' => $form_data['fee_item_id'],
										'fee_item_description' => $fee_item['fi_name'],
										'cur_id' =>$deposit['currency_id'],
										'amount' => $deposit['amount'],
										'balance' => $deposit['amount'],
										'amount_default_currency' =>  $deposit['default_currency_amount'],
										'exchange_rate' => $exchangeRate,
									);
					

					$InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					$InvoiceDetailDb->insert($data_detail);

					//then create charges advance payment
					//advpy_id
					/**/
					$adv_pay['advpy_appl_id'] = $student['appl_id'];
					//$adv_pay['advpy_acad_year_id'] = ;
					//$adv_pay['advpy_sem_id'] = ;
					//$adv_pay['advpy_prog_code'] = ;	
					//$adv_pay['advpy_fomulir'] = ;
					$adv_pay['advpy_invoice_no'] = $bill_no;	
					$adv_pay['advpy_invoice_id'] = $main_id;	
					//$adv_pay['advpy_payment_id'] = ;	
					//$adv_pay['advpy_refund_id'] = ;	
					$adv_pay['advpy_description'] = $invoice_desc;	
					$adv_pay['advpy_amount'] = $deposit['amount'];	
					//$adv_pay['advpy_total_paid'] = ;	
					$adv_pay['advpy_total_balance'] = $deposit['amount'];	
					$adv_pay['advpy_status'] = 'A';
					$adv_pay['advpy_creator'] = $userId;
					$adv_pay['advpy_create_date'] = date('Y-m-d H:i:s');

					$AdvancePayment = new Studentfinance_Model_DbTable_AdvancePayment();
//					$adv_id = $AdvancePayment->insert($adv_pay); //su hide on 06/03/2015

					//and deposit
					//advpydet_id	
					$adv_paydtl['advpydet_advpy_id'] = $adv_id;
					//$adv_paydtl['advpydet_bill_no'] = $bill_no;
					//$adv_paydtl['advpydet_refund_cheque_id'];
					//$adv_paydtl['advpydet_refund_id'];
					//$adv_paydtl['advpydet_total_paid'];

					$AdvancePaymentDetail = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

//					$advdtl_id = $AdvancePaymentDetail->insert($adv_paydtl);//su hide on 06/03/2015

				} else if ($form_data['fee_item_id'] ==  Studentfinance_Model_DbTable_FeeItem::getFeeItemIdByCode('REN-ADV')) {

					//$calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
					$deposit = $this->calculate_hostel_deposit($room_type_id, $cust_type, $date);
					$data = array(
						'bill_number' => $bill_no,
						'appl_id' => $appl_id,
						'trans_id' => $at_trans_id,
						'IdStudentRegistration' => $IdStudentRegistration,
						//'academic_year' => $semester['AcademicYear'],
						'semester' => $semester['IdSemesterMaster'],
						//'bill_amount' => $deposit['amount'],
						//'bill_balance' => $deposit['amount'],
                                                'bill_amount' => $form_data['fee_amount'],
                                                'bill_balance' => $form_data['fee_amount'],
						'bill_description' => $invoice_desc,
						'program_id' => $ap_prog_id,
						'creator' => $userId,
						'fs_id' => $at_fs_id,
						'status' => 'A',
						'currency_id' => $deposit['currency_id'],
						'bill_amount_default_currency' => $deposit['default_currency_amount'],
						'exchange_rate' => $exchangeRate,
						'invoice_date' => $date,
						//'invoice_type' => 'PROCESSING',
						//'fee_category' => $fee_category_id
					);

					$main_id = $Invoice->insert($data);


					//add invoice detail. 1 to 1 thing

					//get fee item
					$FeeItem = new Studentfinance_Model_DbTable_FeeItem();
					$fee_item = $FeeItem->getData($form_data['fee_item_id']);

					$data_detail = array(
						'invoice_main_id' => $main_id,
						'fi_id' => $form_data['fee_item_id'],
						'fee_item_description' => $fee_item['fi_name'],
						'cur_id' => $deposit['currency_id'],
						//'amount' => $deposit['amount'],
						//'balance' => $deposit['amount'],
                                                'amount' => $form_data['fee_amount'],
                                                'balance' => $form_data['fee_amount'],
						'amount_default_currency' =>  $deposit['default_currency_amount'],
						'exchange_rate' => $exchangeRate,
					);


					$InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					$InvoiceDetailDb->insert($data_detail);

					//then create charges advance payment
					//advpy_id
					/**/
					$adv_pay['advpy_appl_id'] = $student['appl_id'];
					//$adv_pay['advpy_acad_year_id'] = ;
					//$adv_pay['advpy_sem_id'] = ;
					//$adv_pay['advpy_prog_code'] = ;
					//$adv_pay['advpy_fomulir'] = ;
					$adv_pay['advpy_invoice_no'] = $bill_no;
					$adv_pay['advpy_invoice_id'] = $main_id;
					//$adv_pay['advpy_payment_id'] = ;
					//$adv_pay['advpy_refund_id'] = ;
					$adv_pay['advpy_description'] = $invoice_desc;
					//$adv_pay['advpy_amount'] = $deposit['amount'];
					//$adv_pay['advpy_total_paid'] = ;
					//$adv_pay['advpy_total_balance'] = $deposit['amount'];
                                        $adv_pay['advpy_amount'] = $form_data['fee_amount'];
                                        $adv_pay['advpy_total_balance'] = $form_data['fee_amount'];
					$adv_pay['advpy_status'] = 'A';
					$adv_pay['advpy_creator'] = $userId;
					$adv_pay['advpy_create_date'] = date('Y-m-d H:i:s');

					$AdvancePayment = new Studentfinance_Model_DbTable_AdvancePayment();
//					$adv_id = $AdvancePayment->insert($adv_pay);//su hide on 06/03/2015

					//and deposit
					//advpydet_id
					$adv_paydtl['advpydet_advpy_id'] = $adv_id;
					$adv_paydtl['advpydet_bill_no'] = $bill_no;
					//$adv_paydtl['advpydet_refund_cheque_id'];
					//$adv_paydtl['advpydet_refund_id'];
					//$adv_paydtl['advpydet_total_paid'];

					$AdvancePaymentDetail = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
//					$advdtl_id = $AdvancePaymentDetail->insert($adv_paydtl);//su hide on 06/03/2015

				}else{
                                    $calculated_charges = $this->calculate_hostel_fee_charge($room_type_id, $cust_type, $check_in, $date);
					
                                    $data = array(
                                            'bill_number' => $bill_no,
                                            'appl_id' => $appl_id,
                                            'trans_id' => $at_trans_id,
                                            'IdStudentRegistration' => $IdStudentRegistration,
                                            //'academic_year' => $semester['AcademicYear'],
                                            'semester' => $semester['IdSemesterMaster'],
                                            'bill_amount' => $form_data['fee_amount'],
                                            'bill_balance' => $form_data['fee_amount'],
                                            'bill_description' => $invoice_desc,
                                            'program_id' => $ap_prog_id,
                                            'creator' => $userId,
                                            'fs_id' => $at_fs_id,
                                            'status' => 'A',
                                            'currency_id' => $calculated_charges['currency_id'],
                                            'bill_amount_default_currency' => $form_data['fee_amount'],
                                            'invoice_date' => $date,
                                            'exchange_rate' => $exchangeRate,
                                            //'invoice_type' => 'PROCESSING',
                                            //'fee_category' => $fee_category_id
                                    );

                                    $main_id = $Invoice->insert($data);


                                    //add invoice detail. 1 to 1 thing

                                    //get fee item
                                    $FeeItem = new Studentfinance_Model_DbTable_FeeItem();
                                    $fee_item = $FeeItem->getData($form_data['fee_item_id']);

                                    $data_detail = array(
                                            'invoice_main_id' => $main_id,
                                            'fi_id' => $form_data['fee_item_id'],
                                            'fee_item_description' => $fee_item['fi_name'],
                                            'cur_id' => $calculated_charges['currency_id'],
                                            'amount' => $form_data['fee_amount'],
                                            'balance' => $form_data['fee_amount'],
                                            'amount_default_currency' =>  $form_data['fee_amount'],
                                            'exchange_rate' => $exchangeRate,
                                    );


                                    $InvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
                                    $InvoiceDetailDb->insert($data_detail);
                                }

				
				
				//Now create the link
				$hf_invoice_data['invoice_id'] = $main_id;
				$hf_invoice_data['IdStudentRegistration'] = $IdStudentRegistration;
				$hf_invoice_data['IdHostelregistration'] = $IdHostelregistration;
				$hf_invoice_data['monthyear'] = $monthyear;
				$hf_invoice_data['note'] = $main_id;
				$hf_invoice_data['created_at'] = date('Y-m-d H:i:s');
				$hf_invoice_data['created_by'] = $userId;

				$FeeInvoice = new Hostel_Model_DbTable_HostelFeeInvoice();
				$FeeInvoice->insert($hf_invoice_data);

			}
			
		}

		$this->_redirect( $this->baseUrl . '/hostel/billing/index');
	}


	/**
	* calculates charge
	* returns an array(
	*	'currency_id' =>$currency_id,
	*	'amount' => $amount,
	*	'default_currency_id' => $default_currency_id,
	*	'default_currency_amount' => $default_currency_amount
	* 	)
	**/
	public function calculate_hostel_fee_charge($room_type_id, $customer_type = 263, $check_in, $date_check) {

		//first we check if this is the same month as the checkin
		$is_same_month = date('Y-m', strtotime($check_in)) == date('Y-m', strtotime($date_check));
                
                if($is_same_month) {
			//then we check if its below 15 days

			//$completed_time= "2012-05-22 05:02:00";

			$checkin = new DateTime($check_in);
			$datecheck = new DateTime($date_check);
			$interval = $datecheck->diff($checkin);
			$total_days = (int) $interval->format('%d');
		} else {

			//let see if date_check is 15 days since day 1 (simple je khannnnn).
			$total_days = (int) date('d', strtotime($date_check));
		}
                $checkinday = intval(date('d', strtotime($check_in)));
                $charge = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($room_type_id, $customer_type, $date_check = null);
		
		$rate = $charge['Rate'];
		
		//calculate based on rate and charges
		//254 Daily
		//255 Monthly
		//256 Semester
		$type = $charge['tenancy_period'];
		$total_charge = 0;
		if ($type == 254) {
			$total_charge = $rate * $total_days;
		} else if ($type == 255) {
			$total_charge = $rate;
		} else {
			//i haz no ideaz
			$total_charge = $rate;
		}
                
                if ($checkinday >= 15 && $is_same_month){
                    $total_charge = ($total_charge/2);
                }

		//and then comes the forex exchange
		$Currency = new Studentfinance_Model_DbTable_Currency();
		$currency = $Currency->fetchRow('cur_id = '. $charge['Currency'])->toArray();

		$current_default_rate = $Currency->getDefaultCurrency();
		
		if($currency['cur_default'] == 'N'){
			//get exchange rate
			$CurrencyRate = new Studentfinance_Model_DbTable_CurrencyRate();
			$curr_rate = $CurrencyRate->getCurrentExchangeRate($charge['Currency']);
			
			$total_invoice_amount_default_currency = $total_charge * $curr_rate['cr_exchange_rate'];
			$calculated_charge = array(
				'currency_id' =>$charge['Currency'],
				'amount' => $total_charge,
				'default_currency_id' => $current_default_rate['cur_id'],
				'default_currency_amount' => $total_invoice_amount_default_currency
			 	);
		}else{
			$total_invoice_amount_default_currency = $total_charge;

			$calculated_charge = array(
				'currency_id' =>$charge['Currency'],
				'amount' => $total_charge,
				'default_currency_id' => $current_default_rate['cur_id'],
				'default_currency_amount' => $total_invoice_amount_default_currency
			 	);
		}
		//phew!
		return ($calculated_charge);
	}

	/**
	* calculates charge
	* returns an array(
	*	'currency_id' =>$currency_id,
	*	'amount' => $amount,
	*	'default_currency_id' => $default_currency_id,
	*	'default_currency_amount' => $default_currency_amount
	* 	)
	**/
	public function calculate_hostel_deposit($room_type_id, $customer_type = 263, $date_check) {

		$charge = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($room_type_id, $customer_type, $date_check = null);
		
		$deposit = $charge['Deposit'];

		//and then comes the forex exchange
		$Currency = new Studentfinance_Model_DbTable_Currency();
		$currency = $Currency->fetchRow('cur_id = '. $charge['Currency'])->toArray();

		$current_default_rate = $Currency->getDefaultCurrency();
		
		if($currency['cur_default'] == 'N'){
			//get exchange rate
			$CurrencyRate = new Studentfinance_Model_DbTable_CurrencyRate();
			$curr_rate = $CurrencyRate->getCurrentExchangeRate($charge['Currency']);
			
			$total_invoice_amount_default_currency = $deposit * $curr_rate['cr_exchange_rate'];
			$calculated_charge = array(
				'currency_id' =>$charge['Currency'],
				'amount' => $deposit,
				'default_currency_id' => $current_default_rate['cur_id'],
				'default_currency_amount' => $total_invoice_amount_default_currency
			 	);
		}else{
			$total_invoice_amount_default_currency = $deposit;

			$calculated_charge = array(
				'currency_id' =>$charge['Currency'],
				'amount' => $deposit,
				'default_currency_id' => $current_default_rate['cur_id'],
				'default_currency_amount' => $total_invoice_amount_default_currency
			 	);
		}
		//phew!
		return ($calculated_charge);
	}

	public function calculate_hostel_admin($room_type_id, $customer_type = 263, $date_check) {

		$charge = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($room_type_id, $customer_type, $date_check = null);
		
		$amount_admin = $charge['Admin'];

		//and then comes the forex exchange
		$Currency = new Studentfinance_Model_DbTable_Currency();
		$currency = $Currency->fetchRow('cur_id = '. $charge['Currency'])->toArray();

		$current_default_rate = $Currency->getDefaultCurrency();
		
		if($currency['cur_default'] == 'N'){
			//get exchange rate
			$CurrencyRate = new Studentfinance_Model_DbTable_CurrencyRate();
			$curr_rate = $CurrencyRate->getCurrentExchangeRate($charge['Currency']);
			
			$total_invoice_amount_default_currency = $amount_admin * $curr_rate['cr_exchange_rate'];
			$calculated_charge = array(
				'currency_id' =>$charge['Currency'],
				'amount' => $amount_admin,
				'default_currency_id' => $current_default_rate['cur_id'],
				'default_currency_amount' => $total_invoice_amount_default_currency
			 	);
		} else {
			$total_invoice_amount_default_currency = $amount_admin;

			$calculated_charge = array(
				'currency_id' =>$charge['Currency'],
				'amount' => $amount_admin,
				'default_currency_id' => $current_default_rate['cur_id'],
				'default_currency_amount' => $total_invoice_amount_default_currency
			 	);
		}
		//phew!
		return ($calculated_charge);
	}

	public function viewInvoiceAction() {
		$hostel_fee_id = $this->_getParam('hostel_fee_id');

		$this->view->hostel_fee = $this->HostelFee->getHostelFee($hostel_fee_id);
	}

	public function getroomtypeAction() {
		
		$this->_helper->layout->disableLayout();

		$month = $this->_getParam('month', date('m'));
		$year = $this->_getParam('year', date('Y'));
		$monthyear = $month . '/' . $year;

		$page_count = $this->gintPageCount;
		$cur_page = $this->_getParam('page', 1);
	
		if ($this->_request->isPost()) {
			$form_data = $this->_request->getPost();
		} else {
			$form_data = array();
		}
		
		$this->view->hostel_registrations = $this->RoomRegistration->getActiveRegistrations($form_data);

	}


}