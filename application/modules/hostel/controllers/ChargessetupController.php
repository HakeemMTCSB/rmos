<?php
class Hostel_ChargessetupController extends Base_Base {
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get( 'log' ); //instantiate log object
		$this->lobjhostelroom = new Hostel_Model_DbTable_Hostelroom();
		$this->lobjchagessetupForm = new Hostel_Form_Chargessetup();
		$this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
	}


	public function indexAction() {

		$this->view->lobjform = $this->lobjform;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->hostelroominfo = $this->lobjhostelroom->fngethostelroomdetail();

		$this->view->hostelrooms = array();
		foreach ( $this->view->hostelroominfo as $hostelroom ) {
			$this->view->hostelrooms[$hostelroom['IdHostelRoomType']] = $hostelroom;
		}


		if ( $this->_request->isPost () && $this->_request->getPost ( 'Search' ) ) {
			$larrformData = $this->_request->getPost ();
			if ( $this->lobjform->isValid ( $larrformData ) ) {
				$this->view->hostelroominfo = $this->lobjhostelroom->fnSearchhostelroom( $larrformData ); //searching the values for the user
				if ( count( $this->view->hostelroominfo )==0 ) {
					$this->view->blankMsg = '1';
				}
			}
		}

		if ( $this->_request->isPost () && $this->_request->getPost ( 'Clear' ) ) {
			$this->_redirect( $this->baseUrl . '/hostel/chargessetup/index' );
		}

	}

	public function newchargessetupAction() {
		
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjchagessetupForm =  $this->lobjchagessetupForm;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$this->view->hostelconfig = $this->lobjhostelconfigModel->fnfetchConfiguration( $IdUniversity );

		$deafult = 'Y';
		$lobjcurrency = $this->lobjhostelconfigModel->fnGetCurrency( $deafult );
		if ( $lobjcurrency[0] ) {
			$this->view->currency = $lobjcurrency[0]['cur_id'];
		}else {
			$this->view->currency = '';
		}

		$finalIntakeValues = $this->lobjintake->fngetallIntake();
		$this->lobjchagessetupForm->IdIntake->addMultiOptions( $finalIntakeValues );
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations( 'Customer Type' );
		foreach ( $larrdefmsresultset as $larrdefmsresult ) {
			$this->lobjchagessetupForm->CustomerType->addMultiOption( $larrdefmsresult['idDefinition'], $larrdefmsresult['DefinitionDesc'] );
		}


		$this->lobjchagessetupForm->RoomType->addValidator( 'Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room_type',
				'field' => 'RoomType',
				'exclude' => array( 'field' => 'IdHostelRoomType', 'value' => $this->_getParam( 'id', 0 ) )
			) );
		$this->lobjchagessetupForm->RoomCode->addValidator( 'Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room_type',
				'field' => 'RoomCode',
				'exclude' => array( 'field' => 'IdHostelRoomType', 'value' => $this->_getParam( 'id', 0 ) )
			) );
		$this->lobjchagessetupForm->RoomCode->getValidator( 'Db_NoRecordExists' )->setMessage( "Code already exists. Please try a different." );
		$this->lobjchagessetupForm->RoomType->getValidator( 'Db_NoRecordExists' )->setMessage( "Name already exists. Please try a different." );



		if ( $this->_request->isPost() && $this->_request->getPost( 'Save' ) ) {
			$larrformData = $this->_request->getPost();
			if ( $this->lobjchagessetupForm->isValid( $larrformData ) || 1) {
				$auth = Zend_Auth :: getInstance();
				$userId = $auth->getIdentity()->iduser;
				$checkDuplicate = $this->lobjhostelroom->checkDuplicateRoomType($larrformData['RoomCode']);
				//($checkDuplicate); exit;
				if ($checkDuplicate){
					$this->_helper->flashMessenger->addMessage(array('error' => "Add Data failed. Duplicate Roomtype Code"));
					$this->_redirect( $this->baseUrl . '/hostel/chargessetup/index' );
				}

				$idroomType = $this->lobjhostelroom->fninserthostelroom( $larrformData, $userId );
				$this->lobjhostelroom->fninsertchargesdetail( $idroomType, $larrformData, $userId );

				//TODO: handle file uploads!
                $dir = APP_DOC_PATH. '/hostel_pics';
                
	            if ( !is_dir($dir) ){
	                if ( mkdir_p($dir) === false )
	                {
	                    throw new Exception('Cannot create attachment folder ('.$dir.')');
	                }
	            }

				$RoomtypePhoto = new Hostel_Model_DbTable_RoomtypePhoto();
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dir);
				$files  = $upload->getFileInfo();
				$hostel_photo = array();

				foreach($files as $file => $fileInfo) {

				    if ($upload->isUploaded($file)) {
				    		$originalFilename = pathinfo($fileInfo['name']);
						    $newFilename =  $idroomType . '-' . uniqid() . '.' . $originalFilename['extension'];
						    $upload->addFilter('Rename', array( 'target' =>  $dir . '/'.  $newFilename, 'overwrite' => true));
				            if ($upload->receive($file)) {
				                $info = $upload->getFileInfo($file);
				                $hostel_photo['name'] = $upload->getFileName($file, false);
				                $hostel_photo['room_type_id'] = $idroomType;
				                $RoomtypePhoto->save($hostel_photo);
				            }
				     }
				}
				
				$this->_redirect( $this->baseUrl . '/hostel/chargessetup/index' );
			} else {
				$this->lobjchagessetupForm->populate( $larrformData );
			}
			exit;
		}
	}

	public function editchargesetupAction() {

		$Idroomtype = $this->_getParam( 'id' );
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$this->view->hostelconfig = $this->lobjhostelconfigModel->fnfetchConfiguration( $IdUniversity );
		$this->view->lobjchagessetupForm =  $this->lobjchagessetupForm;

		$deafult = 'Y';
		$lobjcurrency = $this->lobjhostelconfigModel->fnGetCurrency( $deafult );
		if ( $lobjcurrency[0] ) {
			$this->view->currency = $lobjcurrency[0]['cur_id'];
		}
		else {
			$this->view->currency = '';
		}
		$finalIntakeValues = $this->lobjintake->fngetallIntake();
		$this->lobjchagessetupForm->IdIntake->addMultiOptions( $finalIntakeValues );
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations( 'Customer Type' );
		foreach ( $larrdefmsresultset as $larrdefmsresult ) {
			$this->lobjchagessetupForm->CustomerType->addMultiOption( $larrdefmsresult['idDefinition'], $larrdefmsresult['DefinitionDesc'] );
		}


		$this->lobjchagessetupForm->RoomType->addValidator( 'Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room_type',
				'field' => 'RoomType',
				'exclude' => array( 'field' => 'IdHostelRoomType', 'value' => $this->_getParam( 'id', 0 ) )
			) );
		$this->lobjchagessetupForm->RoomCode->addValidator( 'Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room_type',
				'field' => 'RoomCode',
				'exclude' => array( 'field' => 'IdHostelRoomType', 'value' => $this->_getParam( 'id', 0 ) )
			) );
		$this->lobjchagessetupForm->RoomCode->getValidator( 'Db_NoRecordExists' )->setMessage( "Code already exists. Please try a different." );
		$this->lobjchagessetupForm->RoomType->getValidator( 'Db_NoRecordExists' )->setMessage( "Name already exists. Please try a different." );



		$roomtypeinfo =  $this->lobjhostelroom->fngetroomtypedetails( $Idroomtype );
		$this->lobjchagessetupForm->populate( $roomtypeinfo[0] );
		$this->view->lobjchagessetupForm->IdHostelRoomType->setValue( $roomtypeinfo[0]['IdHostelRoomType'] );
		$this->view->roomtypechargeinfo =  $this->lobjhostelroom->fngetroomtypechargedetails( $Idroomtype );

		if ( $this->_request->isPost() && $this->_request->getPost( 'Save' ) ) {
			$larrformData = $this->_request->getPost();


			if ( $this->lobjchagessetupForm->isValid( $larrformData ) ) {
				$auth = Zend_Auth :: getInstance();
				$userId = $auth->getIdentity()->iduser;
				$this->lobjhostelroom->fnupdatehostelroom( $larrformData, $userId );
				$this->lobjhostelroom->fndeleteroomchargesinfo( $larrformData['IdHostelRoomType'] );
				$this->lobjhostelroom->fninsertchargesdetail( $larrformData['IdHostelRoomType'], $larrformData, $userId );

				

				$this->_redirect( $this->baseUrl . '/hostel/chargessetup/index' );
			} else {
				$this->lobjchagessetupForm->populate( $roomtypeinfo[0] );
			}

		}

		$this->view->chargetype = $roomtypeinfo[0];

	}

	public function editmainchargeAction() {
		$Idroomtype = $this->_getParam( 'id' );
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$this->view->hostelconfig = $this->lobjhostelconfigModel->fnfetchConfiguration( $IdUniversity );
		$this->view->lobjchagessetupForm =  $this->lobjchagessetupForm;


		$this->lobjchagessetupForm->RoomType->addValidator( 'Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room_type',
				'field' => 'RoomType',
				'exclude' => array( 'field' => 'IdHostelRoomType', 'value' => $this->_getParam( 'id', 0 ) )
			) );
		$this->lobjchagessetupForm->RoomCode->addValidator( 'Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_room_type',
				'field' => 'RoomCode',
				'exclude' => array( 'field' => 'IdHostelRoomType', 'value' => $this->_getParam( 'id', 0 ) )
			) );
		$this->lobjchagessetupForm->RoomCode->getValidator( 'Db_NoRecordExists' )->setMessage( "Code already exists. Please try a different." );
		$this->lobjchagessetupForm->RoomType->getValidator( 'Db_NoRecordExists' )->setMessage( "Name already exists. Please try a different." );



		$roomtypeinfo =  $this->lobjhostelroom->fngetroomtypedetails( $Idroomtype );
		$this->lobjchagessetupForm->populate( $roomtypeinfo[0] );
		$this->view->lobjchagessetupForm->IdHostelRoomType->setValue( $roomtypeinfo[0]['IdHostelRoomType'] );
		$this->view->roomtypechargeinfo =  $this->lobjhostelroom->fngetroomtypechargedetails( $Idroomtype );

		


		if ( $this->_request->isPost() && $this->_request->getPost( 'Save' ) ) {
			$larrformData = $this->_request->getPost();
			if ( $this->lobjchagessetupForm->isValid( $larrformData ) ) {
				$auth = Zend_Auth :: getInstance();
				$userId = $auth->getIdentity()->iduser;

				$checkDuplicate = $this->lobjhostelroom->checkDuplicateRoomType($larrformData['RoomCode'], $larrformData['IdHostelRoomType']);
				
				if ($checkDuplicate){
					$this->_helper->flashMessenger->addMessage(array('error' => "Add Data failed. Duplicate Roomtype Code"));
					$this->_redirect( $this->baseUrl . '/hostel/chargessetup/index' );
				}

				$this->lobjhostelroom->fnupdatehostelroom( $larrformData, $userId );


				$dir = APP_DOC_PATH. '/hostel_pics';
                
	            if ( !is_dir($dir) ){
	                if ( mkdir_p($dir) === false )
	                {
	                    throw new Exception('Cannot create attachment folder ('.$dir.')');
	                }
	            }

				$RoomtypePhoto = new Hostel_Model_DbTable_RoomtypePhoto();
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dir);
				$files  = $upload->getFileInfo();
				$hostel_photo = array();

				foreach($files as $file => $fileInfo) {

				    if ($upload->isUploaded($file)) {
				    		$originalFilename = pathinfo($fileInfo['name']);
				    		
						    $newFilename =  $larrformData['IdHostelRoomType'] . '-' . uniqid() . '.' . $originalFilename['extension'];
						    $upload->addFilter('Rename', array( 'target' =>  $dir . '/'.  $newFilename, 'overwrite' => true));
				            if ($upload->receive($fileInfo['name'])) {
				                $info = $upload->getFileInfo($file);
				                $hostel_photo['name'] = $upload->getFileName($file, false);
				                $hostel_photo['room_type_id'] = $larrformData['IdHostelRoomType'];
				                $RoomtypePhoto->save($hostel_photo);
				            }
				     }
				}
				

				$this->_redirect( $this->baseUrl . '/hostel/chargessetup/index' );
			} else {
				$this->lobjchagessetupForm->populate( $roomtypeinfo[0] );
			}
		}

		$this->view->filelist =  $this->lobjhostelroom->getImages($Idroomtype);
	}

	public function getsemesterdatesAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idsemester = $this->_getParam( 'Idsemester' );
		$SemesterCode = $this->_getParam( 'SemesterCode' );
		if ( isset( $Idsemester ) && $Idsemester != '' ) {
			$lobjsemesterdates = $this->lobjSemesterModel->fngetsemesterdates( $Idsemester );
		}else if ( isset( $SemesterCode ) && $SemesterCode != '' ) {
				$lobjsemesterdates = $this->lobjSemesterModel->fngetsemesterdatesBycode( $SemesterCode );
			}
		if ( isset( $SemesterCode ) && $SemesterCode != '' && count( $lobjsemesterdates ) != 0 ) {
			$lobjsemesterdates[0]['StartDate_prop'] = $lobjsemesterdates[0]['StartDate'];
			$lobjsemesterdates[0]['StartDate'] = date( 'd-m-Y', strtotime( $lobjsemesterdates[0]['StartDate'] ) );


		}
		echo Zend_Json_Encoder::encode( $lobjsemesterdates );
	}

	public function deletefileAction() {
	
		if($this->_request->isPost()) {
			$post_data = $this->_request->getPost();
			if(!empty($post_data['id'])) {
				$this->lobjhostelroom->deletefile( $post_data['id'] );
			}
		}
		
		exit;
		return(true);
		
	}
}
