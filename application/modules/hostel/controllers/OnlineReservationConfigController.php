<?php 

class Hostel_OnlineReservationConfigController extends Base_Base {

	public function init() {
		$this->_gobjlog = Zend_Registry::get('log');
		$this->OnlineReservationConfig = new Hostel_Model_DbTable_OnlineReservationConfig();
	}

	public function indexAction() {
		$this->view->configs = $this->OnlineReservationConfig->fetchAll();

		$id = $this->view->id = $this->_getParam('id',1);
		$where = $this->OnlineReservationConfig->select()->where('id = ?', $id);
		$this->view->section = $this->OnlineReservationConfig->fetchRow($where);
	}

	public function editAction() {
		$id = $this->_getParam('id');

		if ($this->_request->isPost()) {
			$form_data = $this->_request->getPost();
			$auth = Zend_Auth::getInstance();

			$section_data['section_name'] = $form_data['section_name'];
			$section_data['section_name_malay'] = $form_data['section_name_malay'];
			$section_data['instruction'] = $form_data['instruction'];
			$section_data['status'] = $form_data['status'];
			$section_data['updated_by'] = $auth->getIdentity()->iduser;
			$section_data['updated_date'] = new Zend_Db_Expr('NOW()');

			$this->OnlineReservationConfig->update($section_data, 'id = ' . $form_data['id']);

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/hostel/online-reservation-config/index/id/'.$form_data['id']);

		} else {
			$form_data = array();
		}

		$where = $this->OnlineReservationConfig->select()->where('id = ?', $id);
		$this->view->section = $this->OnlineReservationConfig->fetchRow($where);

	}

}