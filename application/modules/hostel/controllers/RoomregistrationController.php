<?php

class Hostel_RoomregistrationController extends Base_Base { 

	private $Log;
	private $lobjhostelRegistrationModel;

	public function init() { //initialization function
		$this->Log = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjhostelHostelinfoForm = new Hostel_Form_Hostelinfo();
		$this->lobjcredittransferForm = new Records_Form_Credittransferapp();
		$this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjHostelroomreg = new Hostel_Form_Roomreg();
		$this->lobjHostelroomregModel = new Hostel_Model_DbTable_Roomregistration();
		$this->lobjplacementtest = new Application_Model_DbTable_Placementtest();
		$this->lobjhostelBlockModel = new Hostel_Model_DbTable_Blockinfo();
		$this->lobjhostellevelModel = new Hostel_Model_DbTable_Hostellevel();
		$this->lobjroomtypelevelModel = new Hostel_Model_DbTable_Hostelroom();
		$this->lobjhostelreservationModel = new Hostel_Model_DbTable_Hostelreservation();
		$this->lobjStudentApplicationForm = new Application_Form_Manualapplication();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjhostelitem = new Hostel_Model_DbTable_Itemregistersetup();
		$this->lobjmanualReservationForm = new Hostel_Form_Manualreservation();
		$this->lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
		$this->lobjhostelRegistrationModel = new Hostel_Model_DbTable_Roomregistration();
		$this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
	}


	public function indexAction() {
		$this->view->lobjform = $this->lobjform;

		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;

		// List data of student for Room Reservation
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($this->lobjform->isValid($larrformData)) {
				$larrresult = $this->lobjHostelroomregModel->searchStudentforHostelRegistration($larrformData);
				if(count($larrresult)==0) {
					$this->view->blankMsg = '1';
				}
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->progseniorstudentregistrationpaginatorresult = $larrresult;
			} 
		} else {
			$larrresult = $this->lobjHostelroomregModel->searchStudentforHostelRegistration();
			if(count($larrresult)==0) {
				$this->view->blankMsg = '1';
			}
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		//list ends

		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/hostel/roomregistration');
		}

		// create program dropdown
		$ProgramList = $this->lobjplacementtest->fnGetProgramMaterList();
		$this->lobjform->field1->addMultiOptions($ProgramList);

		//create block dropdown
		$blockList = $this->lobjhostelBlockModel->fngetListofBlocks();
		$this->lobjform->field5->addMultiOptions($blockList);

		//create level dropdown
		$levelList = $this->lobjhostellevelModel->fngetAllLevelList();
		$this->lobjform->field8->addMultiOptions($levelList);

		//create room type dropdown
		$roomtypeList = $this->lobjroomtypelevelModel->fngetListofroomtypes();
		$this->lobjform->field20->addMultiOptions($roomtypeList);
	}


	/**
	 * Function to display the hostel room registration
	 * @author: Vipul
	 */
	public function roominfoAction() {
		//$this->view->lobjcredittransferForm = $this->lobjcredittransferForm;
		$this->view->lobjStudentApplicationForm = $this->lobjStudentApplicationForm;
		$this->view->lobjHostelroomreg = $this->lobjHostelroomreg;
		//$this->view->lobjcredittransferForm->IdStudent->setAttrib('onChange',"getstudentdetail(this.value);") ;
		$this->view->style =  "style='display:none;'";
		$this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
		$idUniversity =$this->gobjsessionsis->idUniversity;
		$this->view->UnversityName = $this->gobjsessionsis->universityname;
		$initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);
		$this->view->univDetails = $this->lobjinitialconfig->fngetunivdetails($idUniversity);
		// Get country list
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjHostelroomreg->OutcampusCountry->addMultiOptions($lobjcountry);

		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;


		$studentId = $this->_getParam('IdStudent');
		$this->view->IdStudent = $studentId;
		if(isset($studentId) && $studentId!='') {

			$studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($studentId);

			$this->view->studDet = $studentDett[0];
			$this->view->style =  "style='display:block;'";

			$UserRoomDetail = $this->lobjHostelroomregModel->fnagetReservationDetail($studentId);

			if(count($UserRoomDetail)>0) {
				$this->view->lobjmanualReservationForm->block->setValue($UserRoomDetail[0]['Block']);
				$this->view->lobjmanualReservationForm->level->setValue($UserRoomDetail[0]['Level']);
				$this->view->lobjmanualReservationForm->setroom->setValue($UserRoomDetail[0]['RoomId']."_".$UserRoomDetail[0]['RoomCode']."_".$UserRoomDetail[0]['SeatNo']);
			}

			//Room RESERVATION
			$studentRoomReservationData = $this->lobjhostelreservationModel->fngetStudentRoomReservationDetail($studentId);
			$studentRoomRegDetail = $this->lobjHostelroomregModel->fngetStudentRoomRegDetail($studentId);
			$this->view->itemTabApperance = $studentRoomRegDetail;

			//asd($studentRoomReservationData,false);
			//asd($studentRoomRegDetail);

			if(count($studentRoomRegDetail)==0 ) {
				if(count($studentRoomReservationData) > 0 ) {
					$this->view->lobjHostelroomreg->Room->setValue($studentRoomReservationData[0]['RoomName']);
					$this->view->lobjHostelroomreg->IdHostelRoom->setValue($studentRoomReservationData[0]['IdHostelRoom']);
					$this->view->lobjmanualReservationForm->SeatNo->setValue($studentRoomReservationData[0]['SeatNo']);
					$this->view->lobjHostelroomreg->IdStudentRegistration->setValue($studentId);
				} else {
					$this->view->lobjHostelroomreg->IdStudentRegistration->setValue($studentId);
				}
				// ENABLE CHECKIN buuton and its fields
				$this->view->enableData = '0';
				$this->view->lobjHostelroomreg->CheckOut->setAttrib('disable',"true"); // button
				$this->view->lobjHostelroomreg->ChangeRoom->setAttrib('disable',"true"); // button
				$this->view->lobjHostelroomreg->OutCampus->setAttrib('disable',"true"); // button
				$this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true"); // button
				$this->view->lobjHostelroomreg->PrintSlip->setAttrib('disable',"true"); // button
				$this->view->lobjHostelroomreg->checkoutCB->setAttrib('disable',"true"); // checkbox
				$this->view->lobjHostelroomreg->CheckOutDate->setAttrib('readonly',"true"); // Datetype textbox
				$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true"); //  Datetype textbox
				$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox

			} else  {
				$this->view->lobjHostelroomreg->IdStudentRegistration->setValue($studentId);
				// FIRST TAB -- Room Registration
				$studentRoomData = $this->lobjHostelroomregModel->fngetroomdetails($studentId);
				//asd($studentRoomData,false);
				$this->view->displayStudentRoomData =  $studentRoomData[0];
				$studentRoomData[0]['CheckInDate'] = substr($studentRoomData[0]['CheckInDate'],0,10);
				$studentRoomData[0]['RoomKeyTakenDate'] = substr($studentRoomData[0]['RoomKeyTakenDate'],0,10);
				$studentRoomData[0]['RoomKeyReturnDate'] = substr($studentRoomData[0]['RoomKeyReturnDate'],0,10);
				$studentRoomData[0]['CheckOutDate'] = substr($studentRoomData[0]['CheckOutDate'],0,10);
				$this->view->checkindate = $studentRoomData[0]['CheckInDate'];
				if($studentRoomData[0]['CheckInDate']!='') {
					$checkinDate = explode('-',$studentRoomData[0]['CheckInDate']);
					$this->view->checkindateY =  $checkinDate[0];
					$this->view->checkindateM =  $checkinDate[1]-1;
					$this->view->checkindateD =  $checkinDate[2];
				}
				$StudentStatus =  $studentRoomData[0]['StudentStatus'];
				if($StudentStatus=='') {  // FIRST TIME ENTRY
					// ENABLE checkin, rest disable
					$this->view->lobjHostelroomreg->CheckOut->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->ChangeRoom->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->OutCampus->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->PrintSlip->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->checkoutCB->setAttrib('disable',"true"); // checkbox
					$this->view->lobjHostelroomreg->CheckOutDate->setAttrib('readonly',"true"); // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true"); //  Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					//$this->view->lobjHostelroomreg->RemarkChangeRoom->setAttrib('readonly',"true"); //  RemarkChangeRoom textbox
				} else if($StudentStatus=='1') {  // status is CHECKIN
					// disable checkin, canceloutcampus
					$this->view->enableData = '1';
					$this->view->lobjHostelroomreg->CheckIn->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true"); // button
					//$this->view->lobjHostelroomreg->checkinCB->setAttrib('disable',"true");// checkbox
					//$this->view->lobjHostelroomreg->CheckInDate->setAttrib('readonly',"true"); // Datetype textbox
					//$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true"); //  Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					//$this->view->lobjHostelroomreg->RemarkChangeRoom->setAttrib('readonly',"true"); //  RemarkChangeRoom textbox
				}
				else if($StudentStatus=='2') {  // status is CHECKOUT
					$this->view->enableData = '2';
					//$this->view->lobjHostelroomreg->CheckIn->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->CheckOut->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->ChangeRoom->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->OutCampus->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true");  // button
					//$this->view->lobjHostelroomreg->PrintSlip->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->checkoutCB->setAttrib('disable',"true");  // button
					//$this->view->lobjHostelroomreg->checkinCB->setAttrib('disable',"true");// checkbox
					$this->view->lobjHostelroomreg->CheckOutDate->setAttrib('readonly',"true");      // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true");// Datetype textbox
					//$this->view->lobjHostelroomreg->CheckInDate->setAttrib('readonly',"true");      // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true");// Datetype textbox
					//$this->view->lobjHostelroomreg->RemarkChangeRoom->setAttrib('readonly',"true"); //  RemarkChangeRoom textbox
				}else if($StudentStatus=='3') {  // status is ROOMCHANGE
					// disable checkin, canceloutcampus
					$this->view->enableData = '3';
					$this->view->lobjHostelroomreg->CheckIn->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true"); // button
					//$this->view->lobjHostelroomreg->checkinCB->setAttrib('disable',"true");// checkbox
					//$this->view->lobjHostelroomreg->CheckInDate->setAttrib('readonly',"true"); // Datetype textbox
					//$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true"); //  Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					//$this->view->lobjHostelroomreg->RemarkChangeRoom->setAttrib('readonly',"false"); //  RemarkChangeRoom textbox
				}
				else if($StudentStatus=='4') { // status is OUTCAMPUS
					// disable checkin, checkout, roomchange
					$this->view->enableData = '4';
					$this->view->lobjHostelroomreg->CheckIn->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->CheckOut->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->ChangeRoom->setAttrib('disable',"true"); // button
					$this->view->lobjHostelroomreg->OutCampus->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->checkinCB->setAttrib('disable',"true"); // checkbox
					$this->view->lobjHostelroomreg->checkoutCB->setAttrib('disable',"true"); // checkbox
					$this->view->lobjHostelroomreg->CheckInDate->setAttrib('readonly',"true"); // Datetype textbox
					$this->view->lobjHostelroomreg->CheckOutDate->setAttrib('readonly',"true");     // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true");   // Datetype textbox
					$studentAddressData = $this->lobjHostelroomregModel->fngetcampusaddressdetail($studentId);
					//$this->view->lobjHostelroomreg->RemarkChangeRoom->setAttrib('readonly',"true"); //  RemarkChangeRoom textbox

					$this->view->studentAddressData =  $studentAddressData[0];
					$lobjCommonModel = new App_Model_Common();

					/*if (isset($studentAddressData[0]['HomePhone']) && $studentAddressData[0]['HomePhone'] != '') {
						$phonepices = explode('-', $studentAddressData[0]['HomePhone']);
						$studentAddressData[0]['HomePhonecountrycode'] = $phonepices[0];
						$studentAddressData[0]['HomePhonestatecode'] = $phonepices[1];
						$studentAddressData[0]['HomePhone'] = $phonepices[2];
					}

					if ( isset($studentAddressData[0]['CellPhone']) && $studentAddressData[0]['CellPhone'] !='' ) {
						$phonecell = explode('-', ($studentAddressData[0]['CellPhone']));
						$studentAddressData[0]['CellPhonecountrycode'] = $phonecell[0];
						$studentAddressData[0]['CellPhone'] = $phonecell[1];
					}*/

					$this->view->lobjHostelroomreg->populate($studentAddressData[0]);
					//
					if (isset($studentAddressData[0]['OutcampusCountry']) && $studentAddressData[0]['OutcampusCountry'] != ''){
						$OutcampusState = $lobjCommonModel->fnGetCountryStateList($studentAddressData[0]['OutcampusCountry']);
						foreach ($OutcampusState as $OutcampusStateresult) {
							$this->lobjHostelroomreg->OutcampusState->addMultiOption($OutcampusStateresult['key'], $OutcampusStateresult['value']);
						}
					}

					if (isset($studentAddressData[0]['OutcampusState']) && $studentAddressData[0]['OutcampusState'] != ''){
						$larrStateCityList = $lobjCommonModel->fnGetCityList($studentAddressData[0]['OutcampusState']);
						foreach ($larrStateCityList as $OutcampusCityresult) {
							$this->lobjHostelroomreg->OutcampusCity->addMultiOption($OutcampusCityresult['key'], $OutcampusCityresult['value']);
						}
					}


				}else if($StudentStatus=='5') { // status is OUTCAMPUS CANCEL
					// disable checkout, roomchange, canceloutcampus
					$this->view->enableData = '5';
					$this->view->lobjHostelroomreg->CheckOut->setAttrib('disable',"true");// button
					$this->view->lobjHostelroomreg->ChangeRoom->setAttrib('disable',"true");  // button
					$this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true");   // button
					$this->view->lobjHostelroomreg->checkoutCB->setAttrib('disable',"true");   // checkbox
					$this->view->lobjHostelroomreg->CheckOutDate->setAttrib('readonly',"true");  // Datetype textbox
					//$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true");// Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true"); //  Datetype textbox
					$this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true"); // Datetype textbox
					//$this->view->lobjHostelroomreg->RemarkChangeRoom->setAttrib('readonly',"true"); //  RemarkChangeRoom textbox

				} //}
				//asd($studentRoomData[0],false);
				if($studentRoomData[0]['SemesterCode']!='') {
					$this->view->semID = $studentRoomData[0]['SemesterCode'];
				}
                                
                                if ($studentRoomData[0]['StudentStatus']==1 || $studentRoomData[0]['StudentStatus']==3){
                                    $this->view->lobjHostelroomreg->populate($studentRoomData[0]);
                                }
				$this->view->lobjHostelroomreg->Room->setValue($studentRoomData[0]['RoomName']);
				$this->view->lobjHostelroomreg->IdStudentRegistration->setValue($studentRoomData[0]['IdStudentRegistration']);
				$this->view->lobjHostelroomreg->IdHostelRoom->setValue($studentRoomData[0]['IdHostelRoom']);
				$this->view->lobjmanualReservationForm->SeatNo->setValue($studentRoomData[0]['SeatNo']);

			}
			//           else if (count($studentRoomReservationData)==0 && count($studentRoomRegDetail)==0)  {
			//                 $this->view->ErrMsg = '1';
			//                 $this->view->lobjHostelroomreg->CheckIn->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->CheckOut->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->ChangeRoom->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->OutCampus->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->CancelOutCampus->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->PrintSlip->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->checkoutCB->setAttrib('disable',"true");  // button
			//                 $this->view->lobjHostelroomreg->checkinCB->setAttrib('disable',"true");// checkbox
			//                 $this->view->lobjHostelroomreg->CheckOutDate->setAttrib('readonly',"true");      // Datetype textbox
			//                 $this->view->lobjHostelroomreg->RoomKeyReturnDate->setAttrib('readonly',"true");// Datetype textbox
			//                 $this->view->lobjHostelroomreg->CheckInDate->setAttrib('readonly',"true");      // Datetype textbox
			//                 $this->view->lobjHostelroomreg->RoomKeyTakenDate->setAttrib('readonly',"true");// Datetype textbox
			//        }


			// FIRST TAB ENDS

			//Second Tab -- Item Register

			$itemlist = $this->lobjhostelitem->fngetitemlist();
			$this->lobjHostelroomreg->Item->addMultiOptions($itemlist);
			$this->view->iteminfo = $this->lobjhostelitem->getaddediteminfo($studentId);
		}

		//Second Tab Ends

		//Third Tab -- History
		$this->view->historyinfo = $this->lobjHostelroomregModel->fngethistory($studentId);
		if ($this->_request->isPost()) {
			$larrformdata = $this->_request->getPost();
			$auth = Zend_Auth :: getInstance();
			$userId = $auth->getIdentity()->iduser;
			$this->lobjHostelroomregModel->fnupdatehistory($larrformdata,$userId);
			$this->_redirect($this->baseUrl . '/hostel/roomregistration/index');
		}
		//Third Tab Ends



	}

	public function detailstudentAction() {
		//$this->_helper->viewRenderer->setNoRender();
		$studentId = $this->_getParam('IdStudent');
		$studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($studentId);
		$this->view->studDet = $studentDett[0];
	}

	public function addroomitemsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idstudent = $this->_getParam('IdStudent');
		if ($this->_request->isPost()) { // save opeartion
			$lobjFormData = $this->_request->getPost();
			$auth = Zend_Auth :: getInstance();
			$userId = $auth->getIdentity()->iduser;
			$this->lobjhostelitem->fndeletehostelitems($Idstudent);
			$this->lobjhostelitem->fninsertitemdetails($Idstudent,$lobjFormData,$userId);
			echo "1";
		}

	}

	/**
	 * Functo save CHECKIN DATA
	 * @author: VT
	 */
	public function savecheckinAction () {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		$incheck = $this->_getParam('incheck');
		$RoomKeyTakenDate = $this->_getParam('RoomKeyTakenDate');
		$IdHostelRoom = $this->_getParam('IdHostelRoom');
		$SeatNo = $this->_getParam('SeatNo');
		$SemCode = $this->_getParam('SemCode');
		$ChargingPerRoom = $this->_getParam('ChargingPerRoom');
		$this->lobjHostelroomregModel->fnupdateCheckInDetails($IdStudentRegistration,$incheck,$RoomKeyTakenDate,$IdHostelRoom,$SeatNo,$SemCode,$ChargingPerRoom,$userId);
		echo 'ok';
		die;
	}


	/**
	 * Functo save OUTCAMPUS ADDRESS DATA
	 * @author: VT
	 */
	public function saveoutcampusaddressAction () {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		$OutcampusAddressDetails = $this->_getParam('OutcampusAddressDetails');
		$homePhone = $this->_getParam('homePhone');
		$homecountrycode = $this->_getParam('homecountrycode');
		$homestatecode = $this->_getParam('homestatecode');
		$OutcampusCity = $this->_getParam('OutcampusCity');
		$OutcampusState = $this->_getParam('OutcampusState');
		$OutcampusCountry = $this->_getParam('OutcampusCountry');
		$OutcampusZip = $this->_getParam('OutcampusZip');
		$CellPhonecountrycode = $this->_getParam('CellPhonecountrycode');
		$CellPhone = $this->_getParam('CellPhone');
		$IdHostelRoom = $this->_getParam('IdHostelRoom');
		$SemCode = $this->_getParam('SemCode');
		$ChargingPerRoom = $this->_getParam('ChargingPerRoom');

		$this->lobjHostelroomregModel->fnupdateOutCampusDetails($IdStudentRegistration,$OutcampusAddressDetails,$homePhone,$OutcampusCity,$OutcampusState,$OutcampusCountry,$OutcampusZip,
				$CellPhone,$IdHostelRoom,$SemCode,$ChargingPerRoom,$userId);
		echo 'DONE'; die;
	}


	/**
	 * Functo save CANCElOUTCAMPUS  DATA
	 * @author: VT
	 */
	public function savecanceloutcampusAction () {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		$IdHostelRoom = $this->_getParam('IdHostelRoom');
		$SemCode = $this->_getParam('SemCode');
		$ChargingPerRoom = $this->_getParam('ChargingPerRoom');
		$this->lobjHostelroomregModel->fnupdateCancelOutCampusDetails($IdStudentRegistration,$IdHostelRoom,$SemCode,$ChargingPerRoom,$userId);
		echo 'DONE'; die;
	}

	/**
	 * Functo save SAVE CHECKOUT  DATA
	 * @author: VT
	 */
	public function savecheckoutAction () {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		$outcheck = $this->_getParam('outcheck');
                //$incheck = $this->_getParam('incheck');
                $reservationInfo = $this->lobjHostelroomregModel->fnagetReservationDetail($IdStudentRegistration);
                
		$RoomKeyReturnDate = $this->_getParam('RoomKeyReturnDate');
		$IdHostelRoom = $this->_getParam('IdHostelRoom');
		$SemCode = $this->_getParam('SemCode');
		$ChargingPerRoom = $this->_getParam('ChargingPerRoom');
		$this->lobjHostelroomregModel->fnupdateCheckOutDetails($IdStudentRegistration,$outcheck,$reservationInfo[0]['CheckInDate'],$RoomKeyReturnDate,$IdHostelRoom,$SemCode,$ChargingPerRoom,$userId);
		echo 'DONE'; die;
	}


	public function savechangeroomAction() {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		$IdHostelRoom = $this->_getParam('IdHostelRoom');
		$SeatNo = $this->_getParam('SeatNo');
		$incheck = $this->_getParam('incheck');
		$RoomKeyTakenDate = $this->_getParam('RoomKeyTakenDate');
		$SemCode = $this->_getParam('SemCode');
		$RemarkChangeRoom = $this->_getParam('RemarkChangeRoom');
		$ChargingPerRoom = $this->_getParam('ChargingPerRoom');
                $UserRoomDetail = $this->lobjHostelroomregModel->fnagetReservationDetail($IdStudentRegistration);
                //var_dump($UserRoomDetail); exit;
                if ($UserRoomDetail[0]['RoomId']!=$IdHostelRoom){
                    $this->lobjHostelroomregModel->fnupdateChangeRoom($IdStudentRegistration,$IdHostelRoom,$SeatNo,$incheck,$RoomKeyTakenDate,$SemCode,$RemarkChangeRoom,$ChargingPerRoom,$userId);
                }
                echo 'DONE'; die;

	}


	public function hostelreservationroomAction(){
		$this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
		$this->_helper->layout->disableLayout();
		$IdStudent = $this->_getParam('Idstudent');
		$UserRoomDetail = $this->lobjhostelreservationModel->fnagetReservationDetail($IdStudent);
                //var_dump($UserRoomDetail);
		if(!empty($UserRoomDetail)){
			//$this->view->BlockId = $UserRoomDetail['Block'];
			//$this->view->LevelId = $UserRoomDetail['Level'];
		}
		$studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($IdStudent);
		$this->view->studentname = '';
		if(!empty($studentDett)){
			$this->view->studentname = $studentDett[0]['name'];
		}
	}

	public function getroomallocationAction(){

		$this->_helper->layout->disableLayout();
		$blockId = $this->_getParam('blockId');
		$levelId = $this->_getParam('levelId');
		$cust_type = $this->_getParam('cust_type');
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		// get the gender of student and then filter the room based on it.

		$studentDett = $this->lobjstudentregistrationModel->getStudentByRegDetail($IdStudentRegistration);
		
		$getGender = $studentDett['appl_gender'];
		
		//$getGender = $this->lobjhostelRegistrationModel->fngetStudentGender($IdStudentRegistration);

		$this->view->gender = $getGender;
		$roomList = $this->lobjhostelRoomModel->fngethostelroom($blockId, $levelId,$getGender);
		$this->view->roomList = '';

		if(!empty($roomList)){
			$this->view->roomList = $roomList;
		}

		$occupiedReservedRoomList =  $this->lobjhostelreservationModel->fngetSeatsOccupied();
		$this->view->occupiedRoomList = $occupiedReservedRoomList;
		$occupiedRegisteredRoomList = $this->lobjhostelRegistrationModel->fngetSeatsRegistered();
		$this->view->occupiedRoomList = array_merge($occupiedReservedRoomList,$occupiedRegisteredRoomList);

		$status = NULL;
		$UserRoomDetail = $this->lobjhostelreservationModel->fnagetReservationDetail($IdStudentRegistration);

		if(!empty($UserRoomDetail)){
			$status = $UserRoomDetail['Status'];
			$this->view->studentRoomId = str_replace('_', '',$UserRoomDetail['RoomId']).'_'.$UserRoomDetail['SeatNo'];
		}
		$this->view->status = $status;
	}

        public function printHistorySlipAction(){
            $this->_helper->layout->disableLayout();
            $id = $this->_getParam('id');
            $printInfo = $this->lobjHostelroomregModel->getPrintSlipInfo($id);
            
            if ($printInfo['Activity']=='Check Out'){
                if ($printInfo['checkinDate2']==null){
                    $checkindate = $this->lobjHostelroomregModel->getCheckinDate($printInfo['IdStudentRegistration']);
                    $printInfo['checkinDate2'] = $checkindate['checkinDate'];
                }
            }
            
            global $globalprintinfo;
            $globalprintinfo = $printInfo;
            
            $fieldValues = array(
                '$[LOGO]'=> "images/logo_text_high.jpg"
            );

			//var_dump($fieldValues); exit;


            require_once 'dompdf_config.inc.php';
            //error_reporting(0);	

            $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
            $autoloader->pushAutoloader('DOMPDF_autoload');

            //template path	 
            $html_template_path = DOCUMENT_PATH."/template/accommodationslip.html";

            $html = file_get_contents($html_template_path);			

            //replace variable
            foreach ($fieldValues as $key=>$value){
                    $html = str_replace($key,$value,$html);	
            }
            
            //echo $html; exit;

            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->set_paper('a4', 'potrait');
            $dompdf->render();

            //output filename 
            $output_filename = $printInfo["registrationId"]."-accommodation-slip.pdf";

            //$dompdf = $dompdf->output();
            $dompdf->stream($output_filename);
            exit;
        }
}

?>
