<?php
class Hostel_InventorysetupController extends Base_Base { //Controller for the REcords Module

	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjhostelInventoryForm = new Hostel_Form_Inventorysetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjinventoryconfigModel = new Hostel_Model_DbTable_Inventorysetup();
		$this->lobjSubjectsetup = new Application_Model_DbTable_Subjectsetup();
		//$this->lobjhostelconfigModel = Hostel_Model_DbTable_Hostelconfig::singleton();
	}


	/**
	 * Function to display the hostel inventory setup form and SEARCH
	 * @author: Vipul
	 */
	public function indexAction() {
		$this->view->lobjform = $this->lobjhostelInventoryForm;
		$larrresult = $this->lobjinventoryconfigModel->fnfetchAllInventory($post = NULL);
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		if(!$this->_getParam('search')) {
			unset($this->gobjsessionsis->progstudentregistrationpaginatorresult);
		}


		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->progstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progstudentregistrationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$larrresult = $this->lobjinventoryconfigModel->fnfetchAllInventory( $larrformData ); //searching the values for the user
			if(count($larrresult)==0) {
				$this->view->blankMsg = '1';
			}
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->progstudentregistrationpaginatorresult = $larrresult;

		}

	}


	/**
	 * Function to ADD INVENTORY and UPDATE
	 * @author: Vipul
	 */
	public function addinventoryAction() {
		$this->view->lobjform = $this->lobjhostelInventoryForm;
		$idUniversity =$this->gobjsessionsis->idUniversity;
		$configArray = $this->lobjSubjectsetup->getConfigDetail($idUniversity);
		$this->view->confdet = $configArray;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$invtid = $this->_getParam('invtid');
		$this->view->invtid = $invtid;
		$this->view->lobjform->Name->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_inventory',
				'field' => 'Name',
				'exclude' => array('field' => 'IdHostelInventory','value' => $this->_getParam('invtid', 0))
		));
		$this->view->lobjform->Code->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_inventory',
				'field' => 'Code',
				'exclude' => array('field' => 'IdHostelInventory','value' => $this->_getParam('invtid', 0))
		));
		$this->view->lobjform->Code->getValidator('Db_NoRecordExists')->setMessage("Code already exists. Please try a different.");
		$this->view->lobjform->Name->getValidator('Db_NoRecordExists')->setMessage("Name already exists. Please try a different.");
			

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {

			$larrformData = $this->_request->getPost ();
			if ($this->view->lobjform->isValid($larrformData))
			{
				if(isset($larrformData['InvCode']) && $larrformData['InvCode'] == ''){
					if($configArray[0]['inventoryIdType'] == 1){
						if($larrformData)
							$codegenObj = new Cms_CodeGeneration();
						$inventoryCode = $codegenObj->InventoryCodeGenration($idUniversity, $configArray[0]);
						$larrformData['Code'] = $inventoryCode;
					}
				}else if(isset($larrformData['InvCode']) && $larrformData['InvCode'] != ''){
					$larrformData['Code'] = $larrformData['InvCode'];
					unset($larrformData['InvCode']);
				}else{
					$larrformData['Code'] = $larrformData['Code'];
				}
				$this->lobjinventoryconfigModel->fnsaveinventory( $larrformData ,$userId, $invtid);
				$this->_redirect( $this->baseUrl . '/hostel/inventorysetup/index');
			}
			else {  $this->view->lobjform->populate($larrformData);
			}
		}

		if($invtid!='') {
			$where = "a.IdHostelInventory = '".$invtid."' ";
			$larrresult = $this->lobjinventoryconfigModel->fnfetchInventoryByID( $where );
			if(count($larrresult)>0) {
				$this->view->lobjform->populate($larrresult[0]);
				if($configArray[0]['inventoryIdType'] == 1){
					$this->view->lobjform->InvCode->setValue($larrresult[0]['Code']);
				}
				$this->view->invtid = $invtid;
			}
		}


	}

	/**
	 * Function to DELETE INVENTORY
	 * @author: Vipul
	 */
	public function delinventoryAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('id');
		$result = $this->lobjinventoryconfigModel->fndeleteinvnt($id);
		if($result) {
			echo '1'; die;
		}
	}


}
?>