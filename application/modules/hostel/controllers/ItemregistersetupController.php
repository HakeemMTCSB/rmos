<?php
class Hostel_ItemregistersetupController extends Base_Base { //Controller for the REcords Module

	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjhostelItemregisterForm = new Hostel_Form_Itemregistersetup();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjItemregisterconfigModel = new Hostel_Model_DbTable_Itemregistersetup();
		//$this->lobjItemregisterconfigModel = Hostel_Model_DbTable_Itemregistersetup::singleton();
	}


	/**
	 * Function to display the hostel Itemregister setup form and SEARCH
	 * @author: Vipul
	 */
	public function indexAction() {
		$this->view->lobjform = $this->lobjhostelItemregisterForm;
		$larrresult = $this->lobjItemregisterconfigModel->fnfetchAllItemregister($post = NULL);
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		if(!$this->_getParam('search')) {
			unset($this->gobjsessionsis->progstudentregistrationpaginatorresult);
		}


		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->progstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progstudentregistrationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$larrresult = $this->lobjItemregisterconfigModel->fnfetchAllItemregister( $larrformData ); //searching the values for the user
			if(count($larrresult)==0) {
				$this->view->blankMsg = '1';
			}
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->progstudentregistrationpaginatorresult = $larrresult;

		}

	}


	/**
	 * Function to ADD Itemregister and UPDATE
	 * @author: Vipul
	 */
	public function additemregisterAction() {
		$this->view->lobjform = $this->lobjhostelItemregisterForm;
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$itemregid = $this->_getParam('itemregid');
		$this->view->itemregid = $itemregid;
		$this->view->lobjform->Name->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_item',
				'field' => 'Name',
				'exclude' => array('field' => 'IdHostelItem','value' => $this->_getParam('itemregid', 0))
		));
		$this->view->lobjform->Code->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_hostel_item',
				'field' => 'Code',
				'exclude' => array('field' => 'IdHostelItem','value' => $this->_getParam('itemregid', 0))
		));
		$this->view->lobjform->Code->getValidator('Db_NoRecordExists')->setMessage("Code already exists. Please try a different.");
		$this->view->lobjform->Name->getValidator('Db_NoRecordExists')->setMessage("Name already exists. Please try a different.");

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {

			$larrformData = $this->_request->getPost ();
			if ($this->view->lobjform->isValid($larrformData))
			{
				$this->lobjItemregisterconfigModel->fnsaveitemregister( $larrformData ,$userId, $itemregid);
				$this->_redirect( $this->baseUrl . '/hostel/itemregistersetup/index');
			}
			else { $this->view->lobjform->populate($larrformData);
			}
		}

		if($itemregid!='') {
			$where = "a.IdHostelItem = '".$itemregid."' ";
			$larrresult = $this->lobjItemregisterconfigModel->fnfetchItemregisterByID( $where );
			if(count($larrresult)>0) {
				$this->view->lobjform->populate($larrresult[0]);
				$this->view->itemregid = $itemregid;
			}
		}


	}

	/**
	 * Function to DELETE Itemregister
	 * @author: Vipul
	 */
	public function delitemregisterAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('id');
		$result = $this->lobjItemregisterconfigModel->fndeleteitem($id);
		if($result) {
			echo '1'; die;
		}
	}


}
?>
