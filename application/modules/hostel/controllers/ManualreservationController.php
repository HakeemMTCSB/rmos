<?php

class Hostel_manualreservationController extends Base_Base { //Controller for the REcords Module

	private $_gobjlog;
	private $lobjmanualReservationForm;
	private $lobjstudentregistrationModel;
	private $lobjplacementtest;
	private $lobjhostelBlockModel;
	private $lobjhostellevelModel;
	private $lobjroomtypelevelModel;
	private $lobjhostelreservation;
	private $lobjhostelRoomModel;
	private $lobjhostelconfigModel;
	private $lobjhostelRegistrationModel;
	private $lobjhostelreservationHistory;

	//STATUSES
	//265 - Entry
	//266 - Approved
	//267 - Disapproved
	//268 - Revert


	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjform = new App_Form_Search ();
		
		$this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjplacementtest = new Application_Model_DbTable_Placementtest();
		$this->lobjhostelBlockModel = new Hostel_Model_DbTable_Blockinfo();
		$this->lobjhostellevelModel = new Hostel_Model_DbTable_Hostellevel();
		$this->lobjroomtypelevelModel = new Hostel_Model_DbTable_Hostelroom();
		$this->lobjhostelreservation = new Hostel_Model_DbTable_Hostelreservation();
		$this->lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
		$this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
		$this->lobjhostelRegistrationModel = new Hostel_Model_DbTable_Roomregistration();
		$this->lobjhostelreservationHistory = new Hostel_Model_DbTable_Hostelreservationhistory();
		$this->lobjHostelroomregModel = new Hostel_Model_DbTable_Roomregistration();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->ApplicantTransaction = new Registration_Model_DbTable_ApplicantTransaction();

		$this->lobjHostelroomreg = new Hostel_Form_Roomreg();
		$this->lobjmanualReservationForm = new Hostel_Form_Manualreservation();
	
	}


	public function indexAction() {
		//$this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
		$this->view->lobjform = $this->lobjform;

		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;

		// create program dropdown
		$ProgramList = $this->lobjplacementtest->fnGetProgramMaterList();
		$this->lobjform->field1->addMultiOptions($ProgramList);

		//create block dropdown
		$blockList = $this->lobjhostelBlockModel->fngetListofBlocks();
		$this->lobjform->field5->addMultiOptions($blockList);

		//create level dropdown
		$levelList = $this->lobjhostellevelModel->fngetAllLevelList();
		$this->lobjform->field8->addMultiOptions($levelList);

		//create room type dropdown
		$roomtypeList = $this->lobjroomtypelevelModel->fngetListofroomtypes();
		$this->lobjform->field20->addMultiOptions($roomtypeList);


		$lintpagecount = $this->gintPageCount;		
		$lintpage = $this->_getParam('page', 1);


		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();

			$larrformData['field4'] = $this->view->escape(strip_tags($larrformData['field4']));

			if ($this->lobjform->isValid($larrformData)) {

				$larrresult = $this->lobjhostelreservation->searchStudentforHostelReservation($larrformData); //searching the values for the user
				if(count($larrresult)==0) {
					$this->view->blankMsg = '1';
				}
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->progseniorstudentregistrationpaginatorresult = $larrresult;
			}
		} else {
			$this->lobjhostelreservation = new Hostel_Model_DbTable_Hostelreservation();
			$larrresult = $this->lobjhostelreservation->searchStudentforHostelReservation();

			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
		}
		$room_charge = array();

		foreach($this->view->paginator as $key => $reservation) {
			if(!empty($reservation['AppointedRoomTypeId'])) {
				$room_charge[$reservation['IdHostelReservation']] = Hostel_Model_DbTable_Hostelroom::getRoomTypeCharges($reservation['AppointedRoomTypeId'], $reservation['tenant_type'], $reservation['SemesterStartDate']);
			}
		}		
		$this->view->room_charge = $room_charge;

		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/hostel/manualreservation');
		}

	}


	public function hostelreservationAction(){

        $IdUniversity = $this->gobjsessionsis->idUniversity;
        $larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
        if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
        if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
        if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
        $this->view->labels = $larrlabels;

		$this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
		$this->view->lobjHostelroomreg = $this->lobjHostelroomreg;
		$this->view->lobjmanualReservationForm->RoomCode->setAttrib('readonly',"true");
		$this->view->UnversityName = $this->gobjsessionsis->universityname;
		$IdHostelReservation = $this->_getParam('IdHostelReservation',0);

		$UserRoomDetail = $hostel_reservation = $this->lobjhostelreservation->getHostelReservation($IdHostelReservation);
		$this->view->UserRoomDetail = $UserRoomDetail;
		
		if($UserRoomDetail['tenant_type'] == 263) {
			$this->view->studDet = $studentDett = $this->lobjhostelreservation->getStudentDetail($UserRoomDetail['IdStudentRegistration'], $UserRoomDetail['cust_type']);
		} else {
			$this->view->studDet = $studentDett = array();
		}

		//this part for the slip
		$idUniversity =$this->gobjsessionsis->idUniversity;
		
		$this->view->univDetails = $this->lobjinitialconfig->fngetunivdetails($idUniversity);

		
 		$this->view->IdSemester = $hostel_reservation['IdSemester'];//$UserRoomDetail['IdSemester'];
		if(!empty($UserRoomDetail)){

			$this->view->lobjmanualReservationForm->block->setValue($UserRoomDetail['BlockName']);
			$this->view->lobjmanualReservationForm->level->setValue($UserRoomDetail['LevelName']);
			$this->view->lobjmanualReservationForm->setroom->setValue($UserRoomDetail['RoomId']."_".$UserRoomDetail['RoomCode']."_".$UserRoomDetail['SeatNo']);
		}

		$this->view->reservationstatus = '';
		
		
		if(!empty($UserRoomDetail)){
			$this->view->lobjmanualReservationForm->populate($UserRoomDetail);
			$this->view->lobjmanualReservationForm->Save->setAttrib('disable',"true");
			$this->view->reservationstatus = $UserRoomDetail['Status'];
		}else{
			$this->view->lobjmanualReservationForm->ChangeReserve->setAttrib('disable',"true");
			$this->view->lobjmanualReservationForm->CancelReserve->setAttrib('disable',"true");
		}
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$configDetail = $this->lobjhostelconfigModel->fnfetchConfiguration($IdUniversity);

		if(!empty($configDetail)){
			$this->view->revertReject = $configDetail[0]['Applicableforrevert'];
			$this->view->configdet = $configDetail;
		}
		
		//For Print Button
		//$studentRoomData = $this->lobjHostelroomregModel->fngetroomdetails($IdStudent);
		$studentRoomData = $this->lobjhostelreservation->fngetroomdetailByReservation($IdHostelReservation);
		if(count($studentRoomData)>0){
			$this->view->displayStudentRoomData =  $studentRoomData[0];
		}

		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost();
			
			$auth = Zend_Auth :: getInstance();
			$userId = $auth->getIdentity()->iduser;
			$data = array();
			if(!empty($larrformData['IdHostelReservation'])) {
				$data['IdHostelReservation'] = $larrformData['IdHostelReservation'];	
			}

			$data['IdStudentRegistration'] = $larrformData['IdStudentRegistration'];
			$data['IdHostelRoom'] = $larrformData['RoomId'];
			$data['SeatNo'] = $larrformData['SeatNo'];
			$data['IdSemester'] = $larrformData['IdSemester'];
			$data['SemesterStartDate'] = $larrformData['SemesterStartDate'];
			$data['SemesterEndDate'] = $larrformData['SemesterEndDate'];
			$data['tenant_type'] = $larrformData['tenant_type'];
			$data['Active'] = 1;
			$data['UpdUser'] = $userId;
			$data['UpdDate'] = date("Y-m-d");
			$data['Status'] = $larrformData['Status'];
			$data['Remarks'] = $larrformData['Remarks'];
			

			// Now declare the history array
			$historyArray = array();
			$historyArray['IdStudentRegistration'] =  $larrformData['IdStudentRegistration'];
			$historyArray['IdHostelRoom'] =  $larrformData['RoomId'];
			$historyArray['IpAddress'] =  $this->getRequest()->getServer('REMOTE_ADDR');
			$historyArray['Date'] =  date ( 'Y-m-d H:i:s' );
			$historyArray['Remarks'] =  '';
			$historyArray['Upduser'] =  $userId;
			$historyArray['UpdDate'] =  date ( 'Y-m-d H:i:s' );
			// now check history exist or not for this student
			$studentHistoryDet = $this->lobjhostelreservationHistory->fngetstudentHistoryDetail($larrformData['IdStudentRegistration']);
			if (!empty($studentHistoryDet)) {
				$len = count($studentHistoryDet);
				$historyArray['OldValue'] = $studentHistoryDet[$len-1]['NewValue'];
			} else {
				$historyArray['OldValue'] =  '';
			}

			//this thing here needs a lil looking up
			if( $data['Status'] == 265 ) {

				//remove previous reservation
				if(empty($data['IdHostelReservation'])) {
                    
                    //get student currently active reservation
                    $previous_reservation = $this->lobjhostelreservation->getCurrentReservation($data['IdStudentRegistration'], $cust_type );
                    //totally removes the IdHostelReservation to make sure we don't accidentally do weird stuff
                    unset($data['IdHostelReservation']);
                    $tosave = array_merge($previous_reservation, $data);
					$this->lobjhostelreservation->save($tosave);
				} else {
					
					$this->lobjhostelreservation->save($data);
				}
				
				//$this->lobjhostelRoomModel->updateOccuipedSeats($data['IdHostelRoom']);
				$historyArray['Activity'] =  'Update Reservation';
				$historyArray['NewValue'] =  265;
				$this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);

			} else if($data['Status'] == 268 ) {
				//cancels reservation

				// first delete previous reservation
				if(empty($data['IdHostelReservation'])){
					//get student currently active reservation
                    $previous_reservation = $this->lobjhostelreservation->getCurrentReservation($data['IdStudentRegistration'], $cust_type );
                    //totally removes the IdHostelReservation to make sure we don't accidentally do weird stuff
                    unset($data['IdHostelReservation']);
                    $tosave = array_merge($previous_reservation, $data);
					$this->lobjhostelreservation->save($tosave);
				} else {
					$this->lobjhostelreservation->save($data);
				}

				$historyArray['Activity'] =  'Cancel Reservation';
				$historyArray['NewValue'] =  268;
				$historyArray['Remarks'] =  $larrformData['Remarks'];
				$this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);

			} else if($data['Status'] == 267) {
				$dataArray['IdHostelReservation'] = $data['IdHostelReservation'];
				$dataArray['Status'] = '267';
				$this->lobjhostelreservation->save($dataArray);
				$historyArray['Activity'] =  'Rejected';
				$historyArray['NewValue'] =  267;
				$this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);

			} else if($data['Status'] == 266) {

				$historyArray['Activity'] =  'Approved';
				$this->lobjhostelreservation->save($data);

				$historyArray['NewValue'] =  266;
				$this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);

			}

			$this->_redirect($this->baseUrl . '/hostel/manualreservation');
		}

	}

	public function newhostelreservationAction(){
		$this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
		$this->view->lobjHostelroomreg = $this->lobjHostelroomreg;
		$this->view->lobjmanualReservationForm->RoomCode->setAttrib('readonly',"true");
		$this->view->UnversityName = $this->gobjsessionsis->universityname;
		
		$this->view->lobjmanualReservationForm->ChangeReserve->setAttrib('disable',"true");
		$this->view->lobjmanualReservationForm->CancelReserve->setAttrib('disable',"true");

		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost();
			$cust_type = $larrformData['cust_type'];

			$auth = Zend_Auth :: getInstance();
			$userId = $auth->getIdentity()->iduser;
			$data = array();

            if(!empty($larrformData['IdStudentRegistration'])) {
                $data['IdStudentRegistration'] = $larrformData['IdStudentRegistration'];
            }

            if(!empty($larrformData['agent_id'])) {
                $data['agent_id'] = $larrformData['agent_id'];
            }

			$data['IdHostelRoom'] = $larrformData['RoomId'];
			$data['SeatNo'] = $larrformData['SeatNo'];
			$data['IdSemester'] = $larrformData['IdSemester'];
			$data['SemesterStartDate'] = $larrformData['SemesterStartDate'];
			$data['SemesterEndDate'] = $larrformData['SemesterEndDate'];
			$data['cust_type'] = $larrformData['cust_type'];
            $data['tenant_type'] = $larrformData['tenant_type'];

			if(!empty($larrformData['sd']) && !empty($larrformData['IdSemester']) ) {
				$data['SemesterStartDate'] = $larrformData['sd'];
			}

			if( !empty($larrformData['ed'])  && !empty($larrformData['IdSemester']) ) {
				$data['SemesterEndDate'] = $larrformData['ed'];
			}
			
			$data['Active'] = 1;
			$data['UpdUser'] = $userId;
			$data['UpdDate'] = date("Y-m-d");
			$data['Status'] = $larrformData['Status'];
			$data['Remarks'] = $larrformData['Remarks'];
			

			// Now declare the history array
			$historyArray = array();
			$historyArray['IdStudentRegistration'] =  $larrformData['IdStudentRegistration'];
			$historyArray['IdHostelRoom'] =  $larrformData['RoomId'];
			$historyArray['IpAddress'] =  $this->getRequest()->getServer('REMOTE_ADDR');
			$historyArray['Date'] =  date ( 'Y-m-d H:i:s' );
			$historyArray['Remarks'] =  '';
			$historyArray['Upduser'] =  $userId;
			$historyArray['UpdDate'] =  date ( 'Y-m-d H:i:s' );
            $historyArray['NewValue'] = $data['Status'];

			// now check history exist or not for this student
			$studentHistoryDet = $this->lobjhostelreservationHistory->fngetstudentHistoryDetail($larrformData['IdStudentRegistration']);
			if (!empty($studentHistoryDet)) {
				$len = count($studentHistoryDet);
				$historyArray['OldValue'] = $studentHistoryDet[$len-1]['NewValue'];
			} else {
				$historyArray['OldValue'] =  '';
			}

			//this thing here needs a lil looking up
            if($data['tenant_type'] == 263) {
            	$active_reservation = $this->lobjhostelreservation->getActiveReservation($data['IdStudentRegistration'], $data['cust_type']);
            	if(!empty($active_reservation)) {
            		$this->_helper->flashMessenger->addMessage(array('error' => "Student Already Has An Active Reservation."));
            		$this->_redirect($this->baseUrl . '/hostel/manualreservation');
            	}

            }
            $this->lobjhostelreservation->fnaddReservation($data);

            //Process history log
			if( $data['Status'] == 265 ) {
				//$this->lobjhostelRoomModel->updateOccuipedSeats($data['IdHostelRoom']);
				$historyArray['Activity'] =  'Update Reservation';

			} else if($data['Status'] == 268 ) {
				//cancels reservation
				$historyArray['Activity'] =  'Cancel Reservation';
				$historyArray['Remarks'] =  $larrformData['Remarks'];

			} else if($data['Status'] == 267) {

				$historyArray['Activity'] =  'Rejected';

			} else if($data['Status'] == 266) {

				$historyArray['Activity'] =  'Approved With New Reservation Record';

			}

            $this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);
			$this->_redirect($this->baseUrl . '/hostel/manualreservation');

		}

		$idUniversity = $this->gobjsessionsis->idUniversity;
		$this->view->univDetails = $this->lobjinitialconfig->fngetunivdetails($idUniversity);
		

	}

	public function hostelreservationroomAction()
	{
		$this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
		$this->_helper->layout->disableLayout();
		
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
		if($larrlabels['Block_Label']==''){$larrlabels['Block_Label'] = 'Block';}
		if($larrlabels['Level_Label']==''){$larrlabels['Level_Label'] = 'Level';}
		if($larrlabels['Room_Label']==''){$larrlabels['Room_Label'] = 'Room';}
		$this->view->labels = $larrlabels;
		
		$IdStudent = $this->_getParam('Idstudent');
		$cust_type = $this->_getParam('cust_type');
		$tenant_type = $this->_getParam('tenant_type');
		$UserRoomDetail = $this->lobjhostelreservation->fnagetReservationDetail($IdStudent, $cust_type);
		if(!empty($UserRoomDetail)){
			$this->view->BlockId = $UserRoomDetail['Block'];
			$this->view->LevelId = $UserRoomDetail['Level'];
			$this->view->dateApplied = $UserRoomDetail['applied_date'];
			$this->view->roomType = $UserRoomDetail['RoomType'];
		}

		
		if($cust_type == 1) {
			$studentDett = $this->lobjstudentregistrationModel->getStudentByRegDetail($IdStudent);
		} else {
			$studentDett = $this->ApplicantTransaction->getApplicantRegistrationDetail($IdStudent);
		}
		$this->view->cust_type = $cust_type;
		$this->view->gender = $studentDett['appl_gender'];
		$this->view->studentname = '';
		if(!empty($studentDett)){
			$this->view->studentname = $studentDett['name'];
		}
	}

	public function gethostellevelAction(){
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$blockId = $this->_getParam('blockId');
		$levelList = $this->lobjhostellevelModel->getAllLevelList($blockId);
		$IdStudent = $this->_getParam('IdStudentRegistration');
		$this->view->levelList = $levelList;
	}

	public function getroomallocationAction() {
		//No, i don't know why they don't just send the gender directly here.
		$this->_helper->layout->disableLayout();
		$blockId = $this->_getParam('blockId');
		$levelId = $this->_getParam('levelId');
		$cust_type = $this->_getParam('cust_type');
		$IdStudentRegistration = $this->_getParam('IdStudentRegistration');
		if($cust_type == 1) {
			$studentDett = $this->lobjstudentregistrationModel->getStudentByRegDetail($IdStudentRegistration);	
			$getGender = $studentDett['appl_gender'];
		} else if ($cust_type == 2){
			$studentDett = $this->ApplicantTransaction->getApplicantRegistrationDetail($IdStudentRegistration);
			$getGender = $studentDett['appl_gender'];
		} else {
			$getGender = '';
		}

		$this->view->gender = $getGender;
		$roomList = $this->lobjhostelRoomModel->fngethostelroom($blockId, $levelId, $getGender);
		$this->view->roomList = '';
		
		if(!empty($roomList)) {
			$this->view->roomList = $roomList;
		}
		
		$occupiedReservedRoomList =  $this->lobjhostelreservation->fngetSeatsOccupied();
	
		
		$this->view->occupiedRoomList = $occupiedReservedRoomList;
		$occupiedRegisteredRoomList = $this->lobjhostelRegistrationModel->fngetSeatsRegistered();
		$this->view->occupiedRoomList = array_merge($occupiedReservedRoomList,$occupiedRegisteredRoomList);

		$status = NULL;
		$UserRoomDetail = $this->lobjhostelreservation->fnagetReservationDetail($IdStudentRegistration);

		if(!empty($UserRoomDetail)){
			$status = $UserRoomDetail['Status'];
			$this->view->studentRoomId = $UserRoomDetail['RoomId'].'_'.$UserRoomDetail['SeatNo'];
		}
		$this->view->status = $status;
	}

	public function findstudentAction() {

		$cust_type = $this->_getParam('cust_type_id', 1);
		$number_part = $this->_getParam('term', '');
        $gender_id = $this->_getParam('gender_id', 1);

		$student = array();
		if( $cust_type == 1 ) {
			$where_exist = $this->lobjstudentregistrationModel->select()
								->setIntegrityCheck(false)
								->from(array('HostelReservation' => 'tbl_hostel_reservation'), array('HostelReservation.IdStudentRegistration'))
								->where('HostelReservation.cust_type = ?', $cust_type)
								->where('HostelReservation.status IN (265,266)' )
								;
			$where = $this->lobjstudentregistrationModel->select()
						->setIntegrityCheck(false)
						->from(array('StudentProfile' => 'student_profile'))
						->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'),'StudentRegistration.IdApplication = StudentProfile.appl_id', array('StudentRegistration.*', 'StudentRegistration.IdStudentRegistration as StudentID'))
							->where('StudentRegistration.registrationId Like ?', "%" .$number_part . "%")
                            ->where('StudentProfile.appl_gender = ?', $gender_id)
							->where('StudentRegistration.IdStudentRegistration NOT IN (' . $where_exist. ')' )

							;
			
			$students = $this->lobjstudentregistrationModel->fetchAll($where);
			
			foreach($students as $student) {

				$student_array[] = array(
						'id' => $student['StudentID']
						,'label' => $student['registrationId'] . '-' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
						,'value' => $student['registrationId'] . '-' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
				);
			}
			
			
		} else if( $cust_type == 2 ) {
			$where_exist = $this->lobjstudentregistrationModel->select()
								->setIntegrityCheck(false)
								->from(array('HostelReservation' => 'tbl_hostel_reservation'), array('HostelReservation.IdStudentRegistration'))
								->where('HostelReservation.cust_type = ?', $cust_type)
								->where('HostelReservation.status  IN (265,266)' )
								;

			$where = $this->lobjstudentregistrationModel->select()
						->setIntegrityCheck(false)
						->from(array('ApplicantTransaction' => 'applicant_transaction'), array('ApplicantTransaction.*'))
						->joinLeft(array('ApplicantProfile' => 'applicant_profile' ), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id', array('ApplicantProfile.*'))
                        ->where('ApplicantProfile.appl_gender = ?', $gender_id)
						->where('ApplicantTransaction.at_pes_id Like ?', "%" . $number_part . "%")
						->where('ApplicantTransaction.at_trans_id NOT IN (' . $where_exist. ')' )
						;
			 
			$students = $this->lobjstudentregistrationModel->fetchAll($where);
			foreach($students as $student) {
				$student_array[] = array(
						'id' => $student['at_trans_id']
						,'label' => $student['at_pes_id'] . '-' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
						,'value' => $student['at_pes_id'] . '-' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
				);
			}
		}
		if(!empty($student_array)) {
			echo Zend_Json::encode($student_array);
		} else {
			echo Zend_Json::encode(array());
		}
		
		exit;
	}

}
?>