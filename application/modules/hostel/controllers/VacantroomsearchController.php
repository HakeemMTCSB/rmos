<?php

class Hostel_VacantroomsearchController extends Base_Base
{

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array() )
    {
        parent::__construct($request, $response, $invokeArgs);
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object

        $this->lobjhostelroom = new Hostel_Model_DbTable_Hostelroom();
        $this->lobjVacantRoomSearchForm = new Hostel_Form_Vacantroomsearch();
        $this->lobjBlockinfoModel = new Hostel_Model_DbTable_Blockinfo();
        $this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
        $this->lobjroominfoModel = new Hostel_Model_DbTable_Roominfo();
        $this->lobjmanualReservationForm = new Hostel_Form_Manualreservation();
        $this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
        $this->lobjhostelreservationHistory = new Hostel_Model_DbTable_Hostelreservationhistory();
        $this->lobjhostelreservationModel = new Hostel_Model_DbTable_Hostelreservation();
        $this->lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
        $this->lobjroomtypelevelModel = new Hostel_Model_DbTable_Hostelroom();

    }


    public function indexAction()
    {

        $this->view->lobjVacantRoomSearchForm = $this->lobjVacantRoomSearchForm;
        $blockList = $this->lobjBlockinfoModel->fngetListofBlocks();
        $this->view->lobjVacantRoomSearchForm->Block->addMultioptions($blockList);
        $larrresult = $this->lobjhostelroom->fnSearchVacantRoom($post = NULL);

        $this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();
        $IdUniversity = $this->gobjsessionsis->idUniversity;
        $larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
        if ($larrlabels['Block_Label'] == '') {
            $larrlabels['Block_Label'] = 'Block';
        }
        if ($larrlabels['Level_Label'] == '') {
            $larrlabels['Level_Label'] = 'Level';
        }
        if ($larrlabels['Room_Label'] == '') {
            $larrlabels['Room_Label'] = 'Room';
        }
        $this->view->labels = $larrlabels;

        if (!$this->_getParam('search'))
            unset($this->gobjsessionsis->Vacantroompaginatorresult);
        $lintpagecount = $this->gintPageCount;
        $lobjPaginator = new App_Model_Common(); // Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance
        if (isset($this->gobjsessionsis->Vacantroompaginatorresult)) {
            $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Vacantroompaginatorresult, $lintpage, $lintpagecount);
        } else {
            $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
        }
        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();
            if ($larrformData) {
                $larrresult = $this->lobjhostelroom->fnSearchVacantRoom($larrformData);

                if (!$larrresult) {
                    $this->view->Errmsg = 1;
                } else {
                    $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
                    $this->gobjsessionsis->Vacantroompaginatorresult = $larrresult;
                }
            }
        }
        if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
            $this->redirect($this->baseUrl . '/hostel/vacantroomsearch/index');
        }

        //create room type dropdown
        $roomtypeList = $this->lobjroomtypelevelModel->fngetListofroomtypes();
        $this->lobjVacantRoomSearchForm->room_type_id->addMultiOptions($roomtypeList);

    }

    public function reserveAction()
    {
        $roomId = $this->_getParam('roomid');
        $IdStudentRegistration = $this->_request->getQuery('IdStudentRegistration');

        $cust_type = $this->_getParam('cust_type');

        $this->view->roomId = $roomId;
        $this->view->IdStudentRegistration = $IdStudentRegistration;

        $StudInfo = array();
        if ($IdStudentRegistration != '') {
            if ($cust_type == 1) {
                $StudInfo = $this->lobjstudentregistrationModel->getTheStudentRegistrationDetail($IdStudentRegistration);
            } else {
                $StudInfo = $this->lobjstudentregistrationModel->getApplicantDetailByTransaction($IdStudentRegistration);
            }
        }

        $this->view->StudInfo = $StudInfo;
        $this->view->cust_type = $cust_type;
        $this->view->lobjVacantRoomSearchForm = $this->lobjVacantRoomSearchForm;
        $this->view->lobjmanualReservationForm = $this->lobjmanualReservationForm;
        $roominfo = $this->lobjroominfoModel->fngetroominfodetail($roomId);

        $studentlist = $this->lobjroominfoModel->getstudentlist();
        //$this->view->lobjVacantRoomSearchForm->StudentId->addmultioptions($studentlist);

        $this->view->roominfo = $roominfo;

        $this->lobjhostelconfigModel = new Hostel_Model_DbTable_Hostelconfig();


        //labels
        $IdUniversity = $this->gobjsessionsis->idUniversity;
        $larrlabels = $this->lobjhostelconfigModel->fnGetLabels($IdUniversity);
        if ($larrlabels['Block_Label'] == '') {
            $larrlabels['Block_Label'] = 'Block';
        }
        if ($larrlabels['Level_Label'] == '') {
            $larrlabels['Level_Label'] = 'Level';
        }
        if ($larrlabels['Room_Label'] == '') {
            $larrlabels['Room_Label'] = 'Room';
        }
        $this->view->labels = $larrlabels;

        if ($this->_request->isPost()) {
            $larrformData = $this->_request->getPost();

            $UserRoomDetail = $this->lobjhostelreservationModel->fnagetReservationDetail($larrformData['IdStudentRegistration']);


            $IdUniversity = $this->gobjsessionsis->idUniversity;
            $configDetail = $this->lobjhostelconfigModel->fnfetchConfiguration($IdUniversity);
            $auth = Zend_Auth :: getInstance();
            $userId = $auth->getIdentity()->iduser;

            $data = array();
            $data['cust_type'] = 1;

            $data['IdStudentRegistration'] = $larrformData['IdStudentRegistration'];
            $data['IdHostelRoom'] = $larrformData['RoomId'];
            $data['SeatNo'] = $larrformData['SeatNo'];
            $data['IdSemester'] = $larrformData['IdSemester'];
            if (!empty($larrformData['sd'])) {
                $larrformData['SemesterStartDate'] = $larrformData['sd'];
            }

            if (!empty($larrformData['ed'])) {
                $larrformData['SemesterEndDate'] = $larrformData['ed'];
            }

            $data['SemesterStartDate'] = $larrformData['SemesterStartDate'];
            $data['SemesterEndDate'] = $larrformData['SemesterEndDate'];
            $data['Status'] = 265;
            $data['Active'] = 1;
            $data['UpdUser'] = $userId;
            $data['UpdDate'] = date("Y-m-d");
            $data['cust_type'] = $larrformData['cust_type'];
            /*if(!empty($configDetail) && $configDetail[0]['Applicableforapproval'] == 0){
                $data['Status'] = 266;
            }else{
                $data['Status'] = 265;
            }
            if(isset($larrformData['StatusApproved'])){
                $data['Status'] = 266;
            }*/


            // Now declare the history array
            $historyArray = array();
            $historyArray['IdStudentRegistration'] = $larrformData['IdStudentRegistration'];
            $historyArray['IdHostelRoom'] = $larrformData['RoomId'];
            $historyArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
            $historyArray['Date'] = date('Y-m-d H:i:s');
            $historyArray['Remarks'] = '';
            $historyArray['Upduser'] = $userId;
            $historyArray['UpdDate'] = date('Y-m-d H:i:s');
            // now check history exist or not for this student
            $studentHistoryDet = $this->lobjhostelreservationHistory->fngetstudentHistoryDetail($larrformData['IdStudentRegistration']);
            if (!empty($studentHistoryDet)) {
                $len = count($studentHistoryDet);
                $historyArray['OldValue'] = $studentHistoryDet[$len - 1]['NewValue'];
            } else {
                $historyArray['OldValue'] = '';
            }
            if (isset($larrformData['Save'])) {
                $this->lobjhostelreservationModel->fnaddReservation($data);
                $this->lobjhostelRoomModel->updateOccuipedSeats($data['IdHostelRoom']);
                $historyArray['Activity'] = 'Add Reservation';
                $historyArray['NewValue'] = 265;
                $this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);
            } elseif (isset($larrformData['ChangeReserve'])) {
                
                //get student currently active reservation
                $previous_reservation = $this->lobjhostelreservationModel->getCurrentReservation($data['IdStudentRegistration'], $data['cust_type'] );
                //totally removes the IdHostelReservation to make sure we don't accidentally do weird stuff
                unset($data['IdHostelReservation']);
                $tosave = array_merge($previous_reservation, $data);
                $this->lobjhostelreservationModel->save($tosave);

                //$this->lobjhostelreservationModel->fndeleteReservation($data['IdStudentRegistration']);
                // decrease occupied capacity of the room
                $this->lobjhostelRoomModel->decreaseOccuipedSeats($UserRoomDetail['RoomId']);
                // Now add new reservation
                //$this->lobjhostelreservationModel->fnaddReservation($data);
                // Now updateoccupied capacity
                $this->lobjhostelRoomModel->updateOccuipedSeats($data['IdHostelRoom']);
                $historyArray['Activity'] = 'Change Program';
                $historyArray['NewValue'] = 265;
                $this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);
            } else if (isset($larrformData['CancelReserve'])) {
                // first delete previous reservation
                $previous_reservation = $this->lobjhostelreservationModel->getCurrentReservation($data['IdStudentRegistration'], $data['cust_type'] );
                //totally removes the IdHostelReservation to make sure we don't accidentally do weird stuff
                $new_status['Status'] = 268;
                $tosave = array_merge($previous_reservation, $new_status);
                $this->lobjhostelreservationModel->save($tosave);

                //$this->lobjhostelreservationModel->fndeleteReservation($data['IdStudentRegistration']);
                // decrease occupied capacity of the room
                $this->lobjhostelRoomModel->decreaseOccuipedSeats($UserRoomDetail['RoomId']);
                $historyArray['Activity'] = 'Cancel Reservation';
                $historyArray['NewValue'] = '';
                $historyArray['Remarks'] = $larrformData['Remarks'];
                $this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);
            } else if (isset($larrformData['Disapprove'])) {
                $dataArray['Status'] = '265';
                $this->lobjhostelreservationModel->fnupdatereservationstatus($dataArray, $data['IdStudentRegistration']);
                $historyArray['Activity'] = 'Revert';
                $historyArray['NewValue'] = 268;
                $this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);
            } else if (isset($larrformData['Approve'])) {
                $dataArray['Status'] = '266';
                $this->lobjhostelreservationModel->fnupdatereservationstatus($dataArray, $data['IdStudentRegistration']);
                $historyArray['Activity'] = 'Approved';
                $historyArray['NewValue'] = 266;
                $this->lobjhostelreservationHistory->fnAddHostelReservationHistory($historyArray);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
        } else {

        }

        $this->view->reservations = $this->lobjhostelreservationModel->getReservationsByRoom($roomId);

    }

    public function deleteAction()
    {
        $IdHostelReservation = $this->getParam('IdHostelReservation');

        $hostel_reservation = $this->lobjhostelreservationModel->find($IdHostelReservation);

        if (count($hostel_reservation) > 0) {

            $reservation = $hostel_reservation->current();
            $room_id = $reservation['IdHostelRoom'];

            $where = $this->lobjhostelreservationModel->getAdapter()->quoteInto('IdHostelReservation = ?', $IdHostelReservation);
            $this->lobjhostelreservationModel->delete($where);
        } else {
            $this->_helper->flashMessenger->addMessage(array('error' => "Could not find record"));
            $this->redirect($this->baseUrl . '/hostel/vacantroomsearch');

        }


        $this->_helper->flashMessenger->addMessage(array('success' => "Record updated successfully"));
        $this->redirect($this->baseUrl . '/hostel/vacantroomsearch/reserve/roomid/' . $room_id);
    }


}