<?php
class Hostel_Model_DbTable_Roomregistration extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_registration';
	private $lobjDbAdpt;
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	/**
	 * Functio to get detals of hostel romm registraton
	 */
	public function fngetroomdetails( $studentID ) {
		$sql = $this->lobjDbAdpt->select()
		->from( array( 'a'=>'tbl_hostel_registration' ), array( 'a.*' ) )
		->joinLeft( array( 'b' => 'tbl_hostel_room' ), "a.IdHostelRoom = b.IdHostelRoom", array( 'b.RoomCode as Room', 'b.RoomName as RoomName' ) )
		->joinLeft( array( 'c' => 'tbl_hostel_room_type' ), "b.RoomType = c.IdHostelRoomType", array( 'c.RoomType as RoomType' ) )
		->joinLeft( array( 'd' => 'tbl_hostel_block' ), "d.IdHostelBlock = b.Block", array( 'd.Name as BlockName' ) )
		->joinLeft( array( 'e' => 'tbl_hostel_level' ), "e.IdHostelLevel = b.Level", array( 'e.Name as LevelName' ) )
		->joinLeft( array( 'hb' => 'tbl_hostel_block' ), 'b.Block = hb.IdHostelBlock', array( 'CONCAT_WS(" ",hb.Address1,hb.Address2) as address', 'hb.Zipcode', 'hb.Phone' ) )
		->joinLeft( array( 'ci' => 'tbl_city' ), 'hb.City = ci.IdCity', array( 'ci.CityName' ) )
		->joinLeft( array( 'st' => 'tbl_state' ), 'hb.State = st.IdState', array( 'st.StateName' ) )
		->joinLeft( array( 'co' => 'tbl_countries' ), 'hb.Country = co.idCountry', array( 'co.CountryName' ) )
		->where( 'a.IdStudentRegistration = ?', $studentID );
		$result = $this->lobjDbAdpt->fetchAll( $sql );
		return $result;
	}

	/**
	 * Function for CHECKIN DETAILS FOR A STUDENT FOR THE ROOM
	 */
	public function fnupdateCheckInDetails( $IdStudentRegistration, $incheck, $RoomKeyTakenDate, $IdHostelRoom, $SeatNo, $SemCode, $ChargingPerRoom, $userId ) {

		// INSERT INTO history TAB
		$f =  explode( '-', $incheck ); //14-09-2012
		$incheck = "$f[2]-$f[1]-$f[0]";
		$checkDate = $incheck;
		$this->fnUpdateHostelHistory( 'Check In', $IdStudentRegistration, $checkDate, $IdHostelRoom, $userId );


		$g =  explode( '-', $RoomKeyTakenDate );
		$RoomKeyTakenDate = "$g[2]-$g[1]-$g[0]";
		$formData = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'IdHostelRoom' => $IdHostelRoom,
			'SemesterCode' => $SemCode,
			'SeatNo' => $SeatNo,
			'CheckInDate'  => $incheck,
			'CheckOutDate'  => NULL,
			'RoomKeyReturnDate'  => NULL,
			'RoomKeyTakenDate' => $RoomKeyTakenDate,
			'StudentStatus' => '1',
			'ChargingPerRoom' => $ChargingPerRoom,
			'UpdUser' => $userId,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);

		$checkprevData = $this->fngetStudentRoomRegDetail( $IdStudentRegistration );
		if ( count( $checkprevData )==0 ) {

			// if the reservation room id is same as selected room id then do nothing
			// else decrement 1 from reservation room id and increment 1 to selected roomid ofr registration
			$lobjReservationModel = new Hostel_Model_DbTable_Hostelreservation();
			$lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
			$checkRoomResData = $lobjReservationModel->fngetStudentRRDetail( $IdStudentRegistration );
			if ( count( $checkRoomResData )>0 && $checkRoomResData[0]['IdHostelRoom']!=$IdHostelRoom ) {
				// decreemnt the seat for the room
				$lobjhostelRoomModel->decreaseOccuipedSeats( $checkRoomResData[0]['IdHostelRoom'] );
				// icreemnt the seat for the room
				$lobjhostelRoomModel->updateOccuipedSeats( $IdHostelRoom );
			}


			//reservation is considered processed and therefore we'll mark it as processed
			if ( count( $checkRoomResData )>0 ) {
				//1 is student. registration only allow students
				$lobjReservationModel->markAsProcessed( $IdStudentRegistration, 1 );
			}


			$this->insert( $formData );

		}
		else {
			// icrement the seat for the room
			$lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
			$lobjhostelRoomModel->updateOccuipedSeats( $IdHostelRoom );
			// UPDATE the room reg
			$this->update( $formData, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );
		}

	}

	/**
	 * Function for OUTCAMPUS DETAILS FOR A STUDENT
	 */
	public function fnupdateOutCampusDetails( $IdStudentRegistration, $OutcampusAddressDetails, $homePhone, $OutcampusCity, $OutcampusState, $OutcampusCountry, $OutcampusZip, $CellPhone, $IdHostelRoom, $SemCode, $ChargingPerRoom, $userId ) {

		// INSERT INTO history TAB
		$checkDate = date( 'Y-m-d H:i:s' );
		$this->fnUpdateHostelHistory( 'Out Campus', $IdStudentRegistration, $checkDate, $IdHostelRoom, $userId );

		// DECREMENt the seat for the room
		$lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
		$lobjhostelRoomModel->decreaseOccuipedSeats( $IdHostelRoom );

		$formData = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'StudentStatus' => '4',
			'SemesterCode' => $SemCode,
			'CheckOutDate'=>date( 'Y-m-d H:i:s' ),
			'RoomKeyReturnDate'=>date( 'Y-m-d H:i:s' ),
			'ChargingPerRoom' => $ChargingPerRoom,
			'UpdUser' => $userId,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);
		$this->update( $formData, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );

		$HomePhoneData = $homePhone;
		$CellPhoneData = $CellPhone;
		$formDataStudent = array(
			'OutcampusAddressDetails' => $OutcampusAddressDetails,
			'OutcampusCountry' => $OutcampusCountry,
			'OutcampusState' => $OutcampusState,
			'OutcampusCity' => $OutcampusCity,
			'OutcampusZip' => $OutcampusZip,
			'HomePhone' => $HomePhoneData,
			'CellPhone' => $CellPhoneData,
		);
		$this->lobjDbAdpt->update( 'tbl_studentregistration', $formDataStudent, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );


		// DELETE THE ITEM REGISTERED FOR THE STUDENT
		$lobjItemModdel = new Hostel_Model_DbTable_Itemregistersetup();
		$lobjItemModdel->fndeletehostelitems( $IdStudentRegistration );

		// free the room
		$formDataFree = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'StudentStatus' => '4',
			'IdHostelRoom'=>NULL,
			'SeatNo' => NULL,
			'CheckInDate'=>NULL,
			'CheckOutDate'=>date( 'Y-m-d H:i:s' ),
			'RoomKeyReturnDate'=>date( 'Y-m-d H:i:s' ),
			'RoomKeyTakenDate'=>NULL,
			'ChargingPerRoom' => $ChargingPerRoom,
			'UpdUser' => $userId,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);
		$this->update( $formDataFree, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );

	}

	/**
	 * Function to get outcampus address FOR A STUDENT
	 */
	public function fngetcampusaddressdetail( $IdStudentRegistration ) {
		$wh =  "IdStudentRegistration = '".$IdStudentRegistration."'  ";
		$lstrSelect = $this->lobjDbAdpt->select()->from( array( "sa" => "tbl_studentregistration" ), array( "sa.OutcampusAddressDetails", "sa.HomePhone", "sa.CellPhone",
				"sa.OutcampusCountry", "sa.OutcampusState", "sa.OutcampusCity", "sa.OutcampusZip" ) );
		$lstrSelect->where( $wh );
		$larrResult = $this->lobjDbAdpt->fetchAll( $lstrSelect );
		return $larrResult;
	}

	/**
	 * Function to CANCEL OUT CAMPUS FOR A STUDENT
	 */
	public function fnupdateCancelOutCampusDetails( $IdStudentRegistration, $IdHostelRoom=NULL, $SemCode=NULL, $ChargingPerRoom, $userId ) {

		// INSERT INTO history TAB
		$checkDate = date( 'Y-m-d H:i:s' );
		$this->fnUpdateHostelHistory( 'Out Campus Cancel', $IdStudentRegistration, $checkDate, $IdHostelRoom, $userId );

		$formData = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'StudentStatus' => '5',
			'UpdUser' => $userId,
			'CheckInDate'=>NULL,
			'CheckOutDate'=>NULL,
			'RoomKeyReturnDate'=>NULL,
			'RoomKeyTakenDate'=>NULL,
			'ChargingPerRoom' => $ChargingPerRoom,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);
		$this->update( $formData, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );

	}

	/**
	 * Function to CHECKOUT FOR A STUDENT
	 */
	public function fnupdateCheckOutDetails( $IdStudentRegistration, $outcheck, $incheck, $RoomKeyReturnDate, $IdHostelRoom, $SemCode, $ChargingPerRoom, $userId ) {

		// INSERT INTO history TAB
		$f =  explode( '-', $outcheck ); //14-09-2012
		$outcheck = "$f[2]-$f[1]-$f[0]";
		$checkDate = $outcheck;
		$this->fnUpdateHostelHistory( 'Check Out', $IdStudentRegistration, $checkDate, $IdHostelRoom, $userId, null, $incheck);

		// DECEREMNET the seat for the room
		$lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();
		$lobjhostelRoomModel->decreaseOccuipedSeats( $IdHostelRoom );

		//$f =  explode('-',$outcheck); //14-09-2012
		// $outcheck = "$f[2]-$f[1]-$f[0]";
		$g =  explode( '-', $RoomKeyReturnDate );
		$RoomKeyReturnDate = "$g[2]-$g[1]-$g[0]";
		$formData = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'CheckOutDate'  => $outcheck,
			'RoomKeyReturnDate' => $RoomKeyReturnDate,
			'StudentStatus' => '2',
			'SemesterCode' => $SemCode,
			'IdHostelRoom'=>NULL,
			//'CheckInDate'=>$incheck,
			'RoomKeyTakenDate'=>NULL,
			'ChargingPerRoom' => $ChargingPerRoom,
			'SeatNo' => NULL,
			'UpdUser' => $userId,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);
		$this->update( $formData, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );

		// DELETE THE ITEM REGISTERED FOR THE STUDENT
		$lobjItemModdel = new Hostel_Model_DbTable_Itemregistersetup();
		$lobjItemModdel->fndeletehostelitems( $IdStudentRegistration );

	}

	/**
	 * Function to MAINTAIN HOSTEL HISTORY FOR A STUDENT
	 */

	public function fnUpdateHostelHistory( $type, $IdStudentRegistration, $checkDate=NULL, $IdHostelRoom, $userId, $remark=null , $checkindate=null) {
		$resultData  =  $this->fngetroomstatus( $IdStudentRegistration );
		if ( ! empty( $resultData[0]['StudentStatus'] ) ) {
			$StudentStatus = $resultData[0]['StudentStatus'];
			if ( $StudentStatus=='1' ) {
				$oldstatusName = 'CheckIn';
			}
			else if ( $StudentStatus=='2' ) {
					$oldstatusName = 'CheckOut';
				}
			else if ( $StudentStatus=='3' ) {
					$oldstatusName = 'Change Room';
				}
			else if ( $StudentStatus=='4' ) {
					$oldstatusName = 'Out Campus';
				}
			else if ( $StudentStatus=='5' ) {
					$oldstatusName = 'Out Campus Cancel';
				} else {
				$oldstatusName = '';
			}
		} else {
			$oldstatusName = '';
		}

		$formData = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'IdHostelRoom' => $IdHostelRoom,
			'ActivityDate'  =>  date( 'Y-m-d H:i:s' ),
			'Activity' => $type,
			'checkinDate' => $checkDate,
                        'checkinDate2' => $checkindate,
			'OldValue' => $oldstatusName,
			'NewValue' => $type,
			'Remarks' => $remark,
			'UpdUser' => $userId,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);

		$this->lobjDbAdpt->insert( 'tbl_hostel_history', $formData );

	}

	/**
	 * Functio to get room Status
	 */
	public function fngetroomstatus( $studentID ) {
		$sql = $this->lobjDbAdpt->select()
		->from( array( 'a'=>'tbl_hostel_registration' ), array( 'a.StudentStatus' ) )
		->where( 'a.IdStudentRegistration = ?', $studentID );
		$result = $this->lobjDbAdpt->fetchAll( $sql );
		return $result;
	}

	public function fngethistory( $studentId ) {
		$sql = $this->lobjDbAdpt->select()
		->from( array( 'a'=>'tbl_hostel_history' ), array( 'a.*' ) )
		->joinLeft( array( 'c' => 'tbl_hostel_room' ), "a.IdHostelRoom = c.IdHostelRoom", array( 'c.RoomCode as Room', 'c.RoomName as RoomName' ) )
		->joinLeft( array( 'd' => 'tbl_hostel_block' ), "c.Block = d.IdHostelBlock", array( 'd.Name as BlockName', 'd.Code as BlockCode' ) )
		->joinLeft( array( 'e' => 'tbl_hostel_level' ), "c.Level = e.IdHostelLevel", array( 'e.Name as LevelName', 'e.Code as LevelCode' ) )
		->joinLeft( array( 'f' => 'tbl_hostel_room_type' ), "c.RoomType = f.IdHostelRoomType", array( 'f.RoomType as TypeName', 'f.RoomCode as TypeCode' ) )
		->joinLeft( array( 'b' => 'tbl_user' ), "a.UpdUser = b.iduser", array( 'b.loginName' ) )
		->where( 'a.IdStudentRegistration = ?', $studentId )
                ->order('a.ActivityDate DESC');
		$result = $this->lobjDbAdpt->fetchAll( $sql );
		return $result;
	}

	public function fnupdatehistory( $larrformdata, $userId ) {

		if ( isset( $larrformdata['HistoryIdgrid'] ) ) {
			for ( $i=0;$i<count( $larrformdata['HistoryIdgrid'] );$i++ ) {
				$formData = array(
					'Remarks' => $larrformdata['Remarksgrid'][$i],
					//'UpdUser' => $userId,
					//'UpdDate' => date( 'Y-m-d H:i:s' ),
				);
				$wh =  "IdHistory = '".$larrformdata['HistoryIdgrid'][$i]."'  ";
				$this->lobjDbAdpt->update( 'tbl_hostel_history', $formData, $wh );
			}

		}

	}

	/**
	 * Function to get roomregistration FOR A STUDENT
	 */
	public function fngetStudentRoomRegDetail( $idStudent ) {
		$sql = $this->lobjDbAdpt->select()
		->from( array( 'HostelRegistration'=>'tbl_hostel_registration' ), array( 'HostelRegistration.*' ) )
		->where( 'HostelRegistration.IdStudentRegistration = ?', $idStudent )
		;
		$result = $this->lobjDbAdpt->fetchAll( $sql );
		return $result;
	}
	//function to search
	public function searchStudentforHostelRegistration( $post = array() ) {
		//var_dump($post); exit;
		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a'=>'tbl_studentregistration' ), array( "a.IdStudentRegistration as StudentId", "a.*" ) )
		->joinLeft( array( 'StudentProfile'=>'student_profile' ), 'a.sp_id = StudentProfile.id', array( "CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name", "a.registrationId" ) )
		->joinLeft( array( 'b'=>'tbl_hostel_registration' ), 'a.IdStudentRegistration = b.IdStudentRegistration', array( 'b.IdHostelregistration', 'hostelregistrationIdstudentRegistration' => 'b.IdStudentRegistration', 'b.StudentStatus' ) )
		->joinLeft( array( 'c'=>'tbl_hostel_room' ), 'b.IdHostelRoom = c.IdHostelRoom', array( 'c.RoomName' ) )
		->joinLeft( array( 'RoomType' => 'tbl_hostel_room_type' ), 'c.RoomType = RoomType.IdHostelRoomType', 'RoomType.*' )
                ->joinLeft(array('pr'=>'tbl_program'), 'a.IdProgram=pr.IdProgram', array('pr.ProgramName'))
                ->joinLeft(array('df'=>'tbl_definationms'), 'a.profileStatus=df.idDefinition', array('statusName'=>'df.DefinitionDesc'));
		//->where( "a.profileStatus = 92 OR a.profileStatus = 253" );


		if ( isset( $post['field2'] ) && $post['field2']!='' ) {
			$lstrSelect = $lstrSelect->where( 'StudentProfile.appl_fname like "%" ? "%"', $post['field2'] )
			->orwhere( 'StudentProfile.appl_mname like "%" ? "%"', $post['field2'] )
			->orwhere( 'StudentProfile.appl_lname like "%" ? "%"', $post['field2'] );
		}

		if ( isset( $post['field3'] ) && $post['field3']!='' ) {
			$lstrSelect = $lstrSelect->where( 'a.registrationId like "%" ? "%"', $post['field3'] );
		}

		if ( isset( $post['field1'] ) && $post['field1']!='' ) {
			$lstrSelect = $lstrSelect->where( 'a.IdProgram =?', $post['field1'] );
		}

		if ( isset( $post['field5'] ) && $post['field5']!='' ) {
			$lstrSelect = $lstrSelect->where( 'c.Block =?', $post['field5'] );
		}

		if ( isset( $post['field8'] ) && $post['field8']!='' ) {
			$lstrSelect = $lstrSelect->where( 'c.Level = ?', $post['field8'] );
		}

		if ( isset( $post['field20'] ) && $post['field20']!='' ) {
			$lstrSelect = $lstrSelect->where( 'c.RoomType =?', $post['field20'] );
		}

		if ( isset( $post['field14'] ) && $post['field14']!='' ) {
			$post['field14']= substr( $post['field14'], 0, 10 );
			$lstrSelect->where( 'b.CheckInDate LIKE?', '%'.$post['field14'].'%' );
		}

		if ( isset($post['getapprovedreservation']) && !empty( $post['getapprovedreservation'] ) ) {
			$reservation_select = $this->lobjDbAdpt->select()
			->from( array( 'StudentRegistration' => 'tbl_studentregistration' ), array( 'StudentRegistration.IdStudentRegistration' ) )
			->join( array( 'HostelReservation' => 'tbl_hostel_reservation' ), 'HostelReservation.IdStudentRegistration = StudentRegistration.IdStudentRegistration', array() )
			->where( 'HostelReservation.Status = ?', 266 )
			->where( 'HostelReservation.cust_type = 1' )
			;

			$lstrSelect->where( "a.IdStudentRegistration IN (?)", $reservation_select );
		}
		//echo $lstrSelect; exit;
		$result = $this->lobjDbAdpt->fetchAll( $lstrSelect );

		return $result;
	}

	//function to search
	public function getApprovedReservationStudent( $search = null ) {
		//fix this
		//if $search has array key('field2','field3') or is null
		//266 being approved
		$reservation_select = $this->lobjDbAdpt->select()
		->from( array( 'StudentRegistration' => 'tbl_studentregistration' ), array( 'StudentRegistration.IdStudentRegistration' ) )
		->join( array( 'HostelReservation' => 'tbl_hostel_reservation' ), 'HostelReservation.IdStudentRegistration = StudentRegistration.IdStudentRegistration', array( '*' ) )
		->where( 'HostelReservation.Status = ?', 266 )
		->where( 'HostelReservation.cust_type = 1' )
		;
		//first select all students from reservation
		//else
		//then get already students in registrations
		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a'=>'tbl_studentregistration' ), array( "a.*" , "a.IdStudentRegistration as StudentId" ) )
		->joinLeft( array( 'StudentProfile'=>'student_profile' ), 'a.sp_id = StudentProfile.id', array( "CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name", "a.registrationId" ) )
		->joinLeft( array( 'b'=>'tbl_hostel_registration' ), 'a.IdStudentRegistration = b.IdStudentRegistration', array( 'b.IdHostelregistration', 'b.*' ) )
		->joinLeft( array( 'c'=>'tbl_hostel_room' ), 'b.IdHostelRoom = c.IdHostelRoom', array( 'c.RoomName' ) )
		->joinLeft( array( 'RoomType' => 'tbl_hostel_room_type' ), 'c.RoomType = RoomType.IdHostelRoomType', 'RoomType.*' )
		->where( "a.profileStatus = 92 OR a.profileStatus = 253" )
		->where( "a.IdStudentRegistration IN (?)", $reservation_select )
		;

		if ( isset( $search['field2'] ) && !empty( $search['field2'] ) ) {
			$lstrSelect = $lstrSelect->where( 'StudentProfile.appl_fname like "%" ? "%"', $search['field2'] )
			->orwhere( 'StudentProfile.appl_mname like "%" ? "%"', $search['field2'] )
			->orwhere( 'StudentProfile.appl_lname like "%" ? "%"', $search['field2'] );
		}

		if ( isset( $search['field3'] ) && !empty( $search['field3'] ) ) {
			$lstrSelect = $lstrSelect->where( 'a.registrationId like "%" ? "%"', $search['field3'] );
		}

		if ( isset( $search['field1'] ) && !empty( $search['field1'] ) ) {
			$lstrSelect = $lstrSelect->where( 'a.IdProgram =?', $search['field1'] );
		}

		if ( isset( $search['field5'] ) && !empty( $search['field5'] ) ) {
			$lstrSelect = $lstrSelect->where( 'c.Block =?', $search['field5'] );
		}

		if ( isset( $search['field8'] ) && !empty( $search['field8'] ) ) {
			$lstrSelect = $lstrSelect->where( 'c.Level =?', $search['field1'] );
		}

		if ( isset( $search['field20'] ) && !empty( $search['field20'] ) ) {
			$lstrSelect = $lstrSelect->where( 'c.RoomType =?', $search['field20'] );
		}

		if ( isset( $post['field14'] ) && !empty( $post['field14'] ) ) {
			$post['field14']= substr( $post['field14'], 0, 10 );
			$lstrSelect->where( 'b.CheckInDate LIKE?', '%'.$post['field14'].'%' );
		}

		// echo  $lstrSelect ;die;
		$result = $this->lobjDbAdpt->fetchAll( $lstrSelect );
		return $result;
	}



	public function fnupdateChangeRoom( $IdStudentRegistration, $IdHostelRoom, $SeatNo, $incheck, $RoomKeyTakenDate, $SemCode, $RemarkChangeRoom, $ChargingPerRoom, $userId ) {
		// INSERT INTO history TAB
		$f =  explode( '-', $incheck ); //14-09-2012
		$incheck = "$f[2]-$f[1]-$f[0]";
		$checkDate = $incheck;
		$g =  explode( '-', $RoomKeyTakenDate );
		$RoomKeyTakenDate = "$g[2]-$g[1]-$g[0]";

		$this->fnUpdateHostelHistory( 'Room Change', $IdStudentRegistration, $checkDate, $IdHostelRoom, $userId, $RemarkChangeRoom );
		$lobjhostelRoomModel = new Hostel_Model_DbTable_Hostelsroom();

		$getDetail = $this->fngetStudentRoomRegDetail( $IdStudentRegistration );
		if ( count( $getDetail )>0 ) {
			$oldRoomID =  $getDetail[0]['IdHostelRoom'];
			// DECEREMNET the seat for the room
			$lobjhostelRoomModel->decreaseOccuipedSeats( $oldRoomID );
		}

		$formData = array(
			'IdStudentRegistration' => $IdStudentRegistration,
			'IdHostelRoom' => $IdHostelRoom,
			'SeatNo' => $SeatNo,
			'SemesterCode' => $SemCode,
			'CheckInDate'  => $incheck,
			'CheckOutDate'  => NULL,
			'RoomKeyTakenDate' => $RoomKeyTakenDate,
			'RoomKeyReturnDate' => NULL,
			'RemarkChangeRoom' => $RemarkChangeRoom,
			'ChargingPerRoom' => $ChargingPerRoom,
			'StudentStatus' => '3',
			'UpdUser' => $userId,
			'UpdDate' => date( 'Y-m-d H:i:s' ),
		);
		// increment the seat for the room
		$lobjhostelRoomModel->updateOccuipedSeats( $IdHostelRoom );
		// UPDATE the room reg
		$this->update( $formData, array( 'IdStudentRegistration = ?' => $IdStudentRegistration ) );
	}


	public function fngetSeatsRegistered() {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a' => 'tbl_hostel_registration' ), array( "CONCAT_WS('_',IFNULL(a.IdHostelRoom,''),IFNULL(a.SeatNo,'')) as seatname" ) )
		->joinLeft( array( 'b' => 'tbl_studentregistration' ), 'a.IdStudentRegistration = b.IdStudentRegistration' )
		->joinLeft( array( 'StudentProfile'=>'student_profile' ), 'b.sp_id = StudentProfile.id', array( "CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as studentname",  "b.registrationId", "b.IdStudentRegistration", 'StudentProfile.appl_gender as Gender' ) )
		->where( 'a.SeatNo !=?', 0 );
		$result = $this->lobjDbAdpt->fetchAll( $lstrSelect );
		return $result;
	}

	public function fnagetReservationDetail( $IdStudent ) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a' => 'tbl_hostel_registration' ), array( "a.IdHostelRoom AS RoomId", "a.SeatNo", "a.CheckInDate") )
		->joinLeft( array( 'b' => 'tbl_hostel_room' ), "a.IdHostelRoom = b.IdHostelRoom", array( "b.RoomCode", "b.Block", "b.Level" ) )
		->where( "a.IdStudentRegistration =?", $IdStudent );
		//->where("a.Active =?", 1);
		$result = $this->lobjDbAdpt->fetchAll( $lstrSelect );

		if ( count( $result )==0 ) {
			//$lobjmodelRoomReserv = new Hostel_Model_DbTable_Hostelreservation();
			//$result = $lobjmodelRoomReserv->fnagetReservationDetail($IdStudent);
			$lstrSelect = $this->lobjDbAdpt->select()
			->from( array( 'a' => 'tbl_hostel_reservation' ), array( "a.IdHostelRoom AS RoomId", "a.SeatNo", "a.Status", "CheckInDate"=>"a.SemesterStartDate") )
			->joinLeft( array( 'b' => 'tbl_hostel_room' ), "a.IdHostelRoom = b.IdHostelRoom", array( "b.RoomCode", "b.Block", "b.Level" ) )
			->where( "a.IdStudentRegistration =?", $IdStudent )
			->where( "a.Active =?", 1 );
			$result = $this->lobjDbAdpt->fetchAll( $lstrSelect );
		}

		return $result;
	}
	//TODO:and get this right also
	public function fnagetReservationDetail2( $IdStudentRegistration, $cust_type = 1 ) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a' => 'tbl_hostel_reservation' ), array( "a.IdHostelRoom AS RoomId", "a.SeatNo", "a.Status", "a.IdSemester", "a.SemesterStartDate", "a.SemesterEndDate", "a.*" ) )
		->joinLeft( array( 'TenantType' => 'tbl_definationms' ), 'a.tenant_type = TenantType.idDefinition', array( 'TenantType.DefinitionCode AS tenant_type_name' ) )
		->joinLeft( array( 'b' => 'tbl_hostel_room' ), "a.IdHostelRoom = b.IdHostelRoom", array( "b.RoomCode", "b.Block", "b.Level" ) )
		->where( "a.IdStudentRegistration = ?", $IdStudentRegistration )
		->where( "a.Active =?", 1 );

		//student
		if ( $cust_type == 1 ) {
			//for student
			$lstrSelect = $lstrSelect->joinLeft( array( 'StudentRegistration' => 'tbl_studentregistration' ), 'StudentRegistration.IdStudentRegistration = a.IdStudentRegistration', array( "StudentRegistration.registrationId" ) )
			->joinLeft( array( 'StudentProfile' => 'student_profile' ), 'StudentProfile.appl_id =  StudentRegistration.IdApplication' , array( "CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as student_name", 'StudentProfile.appl_id' ) )
			;
		} else if ( $cust_type == 2 ) {
				//for applicant
				$lstrSelect = $lstrSelect->joinLeft( array( 'ApplicantTransaction' => 'applicant_transaction' ), 'ApplicantTransaction.at_trans_id = a.IdStudentRegistration', array( 'ApplicantTransaction.at_pes_id' ) )
				->joinLeft( array( 'ApplicantProfile' => 'applicant_profile' ), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id',
					array( "CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name", 'ApplicantProfile.appl_id' ) )
				;

			}

		$result = $this->lobjDbAdpt->fetchRow( $lstrSelect );
		return $result;
	}

	public function getAllRegistration( $post='' ) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a' => 'tbl_hostel_registration' ), array( "a.*" ) )
		//->joinLeft(array('b' => 'tbl_hostel_room'), "a.IdHostelregistration = b.IdHostelRoom", array('b.RoomCode as Room', 'b.RoomName as RoomName'))
		->joinLeft( array( 'b' => 'tbl_hostel_room' ), "a.IdHostelRoom = b.IdHostelRoom", array( 'b.RoomCode as Room', 'b.RoomName as RoomName' ) )
		->joinLeft( array( 'c' => 'tbl_hostel_room_type' ), "b.RoomType = c.IdHostelRoomType", array( 'c.RoomType as RoomType' ) )
		->joinLeft( array( 'd' => 'tbl_hostel_block' ), "d.IdHostelBlock = b.Block", array( 'd.Name as BlockName', 'd.IdHostelBlock as BlockId' ) )
		->joinLeft( array( 'e' => 'tbl_hostel_level' ), "e.IdHostelLevel = b.Level", array( 'e.Name as LevelName' ) )
		->join( array( 'f' => 'tbl_studentregistration' ), 'a.IdStudentRegistration = f.IdStudentRegistration', array( "f.registrationId", "f.CellPhone", "f.Gender", "CONCAT_WS('',IFNULL(f.FName,' '),IFNULL(f.MName,' '),IFNULL(f.LName,' ')) as studentname" ) );

		if ( $post['Block'] != '' ) {
			$lstrSelect->where( 'b.Block = ?', $post['Block'] );
		}

		if ( $post['Level'] != '' ) {
			$lstrSelect->where( 'b.Level = ?', $post['Level'] );
		}

		if ( $post['Room'] != '' ) {
			$lstrSelect->where( 'b.IdHostelRoom = ?', $post['Room'] );
		}

		if ( $post['Gender'] != '' ) {
			$lstrSelect->where( 'f.Gender = ?', $post['Gender'] );
		}

		$lstrSelect->order( 'LevelName ASC' );

		$result = $this->lobjDbAdpt->fetchAll( $lstrSelect );
		return $result;
	}

	function getActiveRegistrations($search_data) {
		$sql = $this->lobjDbAdpt->select()
		->from( array( 'HostelRegistration'=>'tbl_hostel_registration' ), array( 'HostelRegistration.*' ) )
		->joinLeft( array( 'StudentRegistration'=>'tbl_studentregistration' ), 'HostelRegistration.IdStudentRegistration = StudentRegistration.IdStudentRegistration', array( 'StudentRegistration.*' ) )
		->joinLeft( array( 'StudentProfile'=>'student_profile' ), 'StudentRegistration.sp_id = StudentProfile.id', array( "CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name", "StudentRegistration.registrationId", 'StudentProfile.appl_gender as Gender' ) )
		->joinLeft( array( 'HostelRoom' => 'tbl_hostel_room' ), "HostelRegistration.IdHostelRoom = HostelRoom.IdHostelRoom", array( 'HostelRoom.RoomCode as Room', 'HostelRoom.RoomName as RoomName' ) )
		->joinLeft( array( 'RoomType' => 'tbl_hostel_room_type' ), "HostelRoom.RoomType = RoomType.IdHostelRoomType", array( 'HostelRoom.RoomType as RoomType' ) )
		->joinLeft( array( 'HostelBlock' => 'tbl_hostel_block' ), "HostelBlock.IdHostelBlock = HostelRoom.Block", array( 'HostelBlock.Name as BlockName', 'CONCAT_WS(" ",HostelBlock.Address1,HostelBlock.Address2) as address', 'HostelBlock.Zipcode', 'HostelBlock.Phone' ) )
		->joinLeft( array( 'HostelLevel' => 'tbl_hostel_level' ), "HostelLevel.IdHostelLevel = HostelRoom.Level", array( 'HostelLevel.Name as LevelName' ) )
		->joinLeft( array( 'City' => 'tbl_city' ), 'HostelBlock.City = City.IdCity', array( 'City.CityName' ) )
		->joinLeft( array( 'State' => 'tbl_state' ), 'HostelBlock.State = State.IdState', array( 'State.StateName' ) )
		->joinLeft( array( 'Country' => 'tbl_countries' ), 'HostelBlock.Country = Country.idCountry', array( 'Country.CountryName' ) )
		->where( 'HostelRegistration.StudentStatus IN (1,3)')
		//su tmbah 3 (change room) 09-07-2015
		;

		if(!empty($search_data['room_type_id'])) {
			$sql->where('RoomType.IdHostelRoomType = ?', $search_data['room_type_id']);
		}
		
		$result = $this->lobjDbAdpt->fetchAll( $sql );
		return $result;
	}


	/**
	 * get student gender value
	 */
	public function fngetStudentGender( $IdStudentRegistration ) {

		$lstrSelect = $this->lobjDbAdpt->select()
		->from( array( 'a' => 'tbl_studentregistration' ) )
		->joinLeft( array( 'StudentProfile'=>'student_profile' ), 'a.sp_id = StudentProfile.id', array( "CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name", "a.registrationId", "a.IdStudentRegistration", 'StudentProfile.appl_gender as Gender' ) )
		->where( "a.IdStudentRegistration =?", $IdStudentRegistration );
		$result = $this->lobjDbAdpt->fetchRow( $lstrSelect );
		return $result['Gender'];

	}

	public function getRegistration( $IdHostelregistration ) {
		$where = $this->lobjDbAdpt->select()
		->from( array( 'HostelRegistration'=>'tbl_hostel_registration' ), array( 'HostelRegistration.*' ) )
		->joinLeft( array( 'HostelRoom'=>'tbl_hostel_room' ), 'HostelRegistration.IdHostelRoom = HostelRoom.IdHostelRoom', array( 'HostelRoom.RoomName' ) )
		->joinLeft( array( 'RoomType' => 'tbl_hostel_room_type' ), 'HostelRoom.RoomType = RoomType.IdHostelRoomType', 'RoomType.*' )
		->where( 'HostelRegistration.IdHostelregistration = ?', $IdHostelregistration );
		$room_registration = $this->lobjDbAdpt->fetchRow( $where );
		return $room_registration;
	}
        
        public function getPrintSlipInfo($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a' => 'tbl_hostel_history'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_hostel_room'), 'a.IdHostelRoom = b.IdHostelRoom')
                ->joinLeft(array( 'c' => 'tbl_hostel_room_type' ), "b.RoomType = c.IdHostelRoomType", array( 'c.RoomType as RoomType' ))
		->joinLeft(array( 'd' => 'tbl_hostel_block' ), "d.IdHostelBlock = b.Block", array( 'd.Name as BlockName' ))
		->joinLeft(array( 'e' => 'tbl_hostel_level' ), "e.IdHostelLevel = b.Level", array( 'e.Name as LevelName' ))
		->joinLeft(array( 'hb' => 'tbl_hostel_block' ), 'b.Block = hb.IdHostelBlock', array( 'CONCAT_WS(" ",hb.Address1,hb.Address2) as address', 'hb.Zipcode', 'hb.Phone' ))
		->joinLeft(array( 'ci' => 'tbl_city' ), 'hb.City = ci.IdCity', array( 'ci.CityName' ))
		->joinLeft(array( 'st' => 'tbl_state' ), 'hb.State = st.IdState', array( 'st.StateName' ))
		->joinLeft(array( 'co' => 'tbl_countries' ), 'hb.Country = co.idCountry', array( 'co.CountryName' ))
                ->joinLeft(array('f'=>'tbl_studentregistration'), 'a.IdStudentRegistration=f.IdStudentRegistration')
                ->joinLeft(array('g'=>'student_profile'), 'f.sp_id=g.id', array('genderid'=>'g.appl_gender', 'studentName'=>'concat(g.appl_fname, " ", g.appl_lname)'))
                ->joinLeft(array('h'=>'tbl_program'), 'f.IdProgram=h.IdProgram', array('h.ProgramName'))
                ->where('a.IdHistory =?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getCheckinDate($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a' => 'tbl_hostel_history'), array('value'=>'*'))
                ->where('a.Activity = ?', 'Check In')
                ->where('a.IdStudentRegistration = ?', $id)
                ->order('a.IdHistory DESC');
                    
            $result = $db->fetchRow($select);
            return $result;
        }
}
