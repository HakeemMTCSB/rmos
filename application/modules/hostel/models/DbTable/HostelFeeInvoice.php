<?php
class Hostel_Model_DbTable_HostelFeeInvoice extends Zend_Db_Table_Abstract {

	protected $_name = 'hostel_fee_invoices';
	

	public function init() {
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}

	/**
	* retrieve from tbl_hostel_registration with these syarat
	* 	- StudentStatus = 1, which means he checked in
	*	
	*
	* $monthyear = '2014-03'
	*
	**/
	public function findNotYetInvoiced($monthyear) {
		

		//get list of HostelRegistrationIds that already have generated invoices for $monthyear
		$generated_where = $this->db->select()
							->from(array('HostelFeeInvoices' => 'hostel_fee_invoices'), 'HostelFeeInvoices.IdHostelregistration')
							->where('HostelFeeInvoices.monthyear = ?', $monthyear );

		//take this where and put it in this query below

		$date = $monthyear . '-01';
		$where = $this->db->select()
					->from(array('HostelRegistration' => 'tbl_hostel_registration'))
					->where('CheckInDate <= ? AND HostelRegistration.StudentStatus = 1', $date)
					->where('HostelRegistration.IdHostelregistration NOT IN (?)', $generated_where)					
					;

		$needing_invoice = $this->db->fetchAll($where);

		return($needing_invoice);

	}

	public function getHostelFees($search_data = null) {
		$generatedfee_where = $this->db->select()
							->from(array('HostelFeeInvoices' => 'hostel_fee_invoices'), array('HostelFeeInvoices.IdHostelregistration', 'HostelFeeInvoices.created_at','HostelFeeInvoices.monthyear', 'HostelFeeInvoices.id as hostel_fee_id'))
							->joinLeft( array('Invoice' => 'invoice_main'), 'Invoice.id = HostelFeeInvoices.invoice_id', array('Invoice.*') )
							->joinLeft( array('InvoiceDetail' => 'invoice_detail'), 'InvoiceDetail.invoice_main_id = Invoice.id')
							->joinLeft( array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = HostelFeeInvoices.IdStudentRegistration')
							->joinLeft( array('StudentProfile' => 'student_profile'), 'StudentProfile.id = StudentRegistration.sp_id', array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name","StudentProfile.*"))
							;

		if(!empty($search_data['fee_item_id'])) {
			$generatedfee_where->where( 'InvoiceDetail.fi_id = ?', $search_data['fee_item_id'] );
		}

		if(!empty($search_data['StartDate']))  {
			$generatedfee_where->where('HostelFeeInvoices.created_at >= ?', $search_data['StartDate']);
		}

		if(!empty($search_data['EndDate']))  {
			$generatedfee_where->where('HostelFeeInvoices.created_at <= ?', $search_data['EndDate']);
		}

		if(!empty($search_data['customer_id'])) {
			$generatedfee_where->where('StudentRegistration.registrationId = ?', $search_data['customer_id']);
		}
		
		if(!empty($search_data['customer_name'])) {
			$generatedfee_where->orWhere("StudentProfile.appl_fname LIKE ?", "%{$search_data['customer_name']}%");
			$generatedfee_where->orWhere("StudentProfile.appl_mname LIKE ?", "%{$search_data['customer_name']}%");
			$generatedfee_where->orWhere("StudentProfile.appl_lname LIKE ?", "%{$search_data['customer_name']}%");


		}

		return($generatedfee_where);
	}

	function getHostelFee($hostel_fee_id) {
		$generatedfee_where = $this->db->select()
							->from(array('HostelFeeInvoices' => 'hostel_fee_invoices'), array('HostelFeeInvoices.IdHostelregistration', 'HostelFeeInvoices.created_at','HostelFeeInvoices.monthyear', 'HostelFeeInvoices.id as hostel_fee_id'))
							->joinLeft( array('Invoice' => 'invoice_main'), 'Invoice.id = HostelFeeInvoices.invoice_id', array('Invoice.*') )
							->joinLeft( array('InvoiceDetail' => 'invoice_detail'), 'InvoiceDetail.invoice_main_id = Invoice.id')
							->joinLeft( array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = HostelFeeInvoices.IdStudentRegistration')
							->joinLeft( array('StudentProfile' => 'student_profile'), 'StudentProfile.id = StudentRegistration.sp_id', array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name","StudentProfile.*"))
							->where('HostelFeeInvoices.id = ?', $hostel_fee_id)
							;

		return($this->db->fetchRow($generatedfee_where));
	}

	

}
