<?php
class Hostel_Model_DbTable_Hostelsroom extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_room';
	private $lobjDbAdpt;


	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fngethostelroom($blockId,$levelId,$getGender=NULL){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('a.*'))
		->joinLeft(array('b'=> 'tbl_hostel_block'),'a.Block = b.IdHostelBlock',array('b.Name As blockName'))
		->joinLeft(array('c'=>'tbl_hostel_level'),'a.Level = c.IdHostelLevel',array('c.Name As levelName'))
		->joinLeft(array('d'=>'tbl_hostel_room_type'),'a.RoomType = d.IdHostelRoomType',array('d.RoomType As RoomTypeName'))
		->where('a.Block =?',$blockId)
		->where('a.Level =?',$levelId);
		if($getGender!='') {
			$sql->where('a.Gender =?',$getGender);
		}
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function updateOccuipedSeats($roomId){
		$getNoOcpdseats = $this->fngetOccupiedSeats($roomId);
		if(count($getNoOcpdseats)>0) {
			$occpdSeats = $getNoOcpdseats[0]['OccupiedCapacity'];
			if($occpdSeats>=0) {
				$table     = 'tbl_hostel_room';
				$data      = array('OccupiedCapacity' => new Zend_Db_Expr('OccupiedCapacity + 1'));
				$where[] = $this->lobjDbAdpt->quoteInto('IdHostelRoom = ?',$roomId);
				$this->lobjDbAdpt->update($table, $data, $where);
			}
		}
	}

	public function decreaseOccuipedSeats($roomId){
		$getNoOcpdseats = $this->fngetOccupiedSeats($roomId);
		if(count($getNoOcpdseats)>0) {
			$occpdSeats = $getNoOcpdseats[0]['OccupiedCapacity'];
			if($occpdSeats>0) {
				$table     = 'tbl_hostel_room';
				$data      = array('OccupiedCapacity' => new Zend_Db_Expr('OccupiedCapacity - 1'));
				$where[] = $this->lobjDbAdpt->quoteInto('IdHostelRoom = ?',$roomId);
				$this->lobjDbAdpt->update($table, $data, $where);
			}
		}
	}

	/**
	 * Function to Get occupied seats
	 * @param int $roomId
	 * @author: VT
	 */
	public function fngetOccupiedSeats($roomId) {
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('a.OccupiedCapacity'))
		->where('a.IdHostelRoom =?',$roomId);
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}


}
