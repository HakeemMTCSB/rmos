<?php
class Hostel_Model_DbTable_Blockinfo extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_block';
	private $lobjDbAdpt;
	// Hold an instance of the class
	//private static $instance;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	//    public static function singleton()
	//    {
	//        if (!isset(self::$instance)) {
	//            $c = __CLASS__; //late static binding
	//            self::$instance = new $c;
	//        }
	//        //asd(self::$instance);
	//        return self::$instance;
	//    }


	/**
	 * Function to SAVE Blockinfo setup
	 * @param array $data
	 * @param int $userId
	 * @author: Vipul
	 */
	public function fnsaveblock($data,$userId,$id=NULL) {

		$gatheredHomePhone =  $data['HomePhone'];
		$gatheredFax=  $data['Fax'];
			
		$formData = array(
				'Name' => $data['Name'],
				'Code'  => $data['Code'],
				'ShortName' => $data['ShortName'],
				'DefaultLang' => $data['DefaultLang'],
				'Address1' => $data['Address1'],
				'Address2' => $data['Address2'],
				'City' => $data['City'],
				'State' => $data['State'],
				'Zipcode' => $data['Zipcode'],
				'Country' => $data['Country'],
				'Phone' => $gatheredHomePhone,
				'Fax' => $gatheredFax,
				'UpdUser' => $userId,
				'UpdDate' => date('Y-m-d H:i:s'),
				'Active' => $data['Active'],
		);

		if( $id === null ) {
			$this->insert($formData);
		}
		else
		{
			$this->update($formData, array('IdHostelBlock = ?' => $id));
		}
	}


	/**
	 * Function to FETCH Blockinfo SETUP
	 * @author: Vipul
	 */
	public function fnfetchAllBlockinfo($post = array()) {
		$lstrSelect = $this->lobjDbAdpt->select()->from(array("sa" => "tbl_hostel_block"), array("sa.*"));

		if(isset($post['Name']) && !empty($post['Name']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.Name LIKE ?", '%'.$post['Name'].'%'));
		}
		if(isset($post['Code']) && !empty($post['Code']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.Code LIKE ?", '%'.$post['Code'].'%'));
		}
		if(isset($post['ShortName']) && !empty($post['ShortName']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.ShortName LIKE ?", '%'.$post['ShortName'].'%'));
		}
		if(isset($post['DefaultLang']) && !empty($post['DefaultLang']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.DefaultLang LIKE ?", '%'.$post['DefaultLang'].'%'));
		}
//echo $lstrSelect;
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


	/**
	 * Function to FETCH Block SETUP by condition
	 * @author: Vipul
	 */
	public function fnfetchBlockByID($condition) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_hostel_block"),array("a.*"))
		->where($condition);
		//echo $lstrSelect;
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}





	/**
	 * Function to delete Block
	 * @author: vipul
	 * overwritten by the one who's name shall not be spoken
	 */

	public function fndeleteblock($id) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();


		$where = $lobjDbAdpt->quoteInto('IdHostelBlock = ?', $id);
		$formData['Active'] = 0;
		$this->update( $formData, $where );

		return 1;
	}


	/*
	 *  Function to create the list of blocks
	*/

	public function fngetListofBlocks(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_hostel_block"), array("key" => 'a.IdHostelBlock','value' => 'a.Name'));
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}



}
?>
