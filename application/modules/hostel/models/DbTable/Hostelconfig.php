<?php
class Hostel_Model_DbTable_Hostelconfig extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_config';
	private $lobjDbAdpt;
	// Hold an instance of the class
	//private static $instance;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	//    public static function singleton()
	//    {
	//        if (!isset(self::$instance)) {
	//            $c = __CLASS__; //late static binding
	//            self::$instance = new $c;
	//        }
	//        //asd(self::$instance);
	//        return self::$instance;
	//    }


	/**
	 * Function to SAVE hostel Configuration setup
	 * @param array $data
	 * @param int $userId
	 * @author: Vipul
	 */
	public function fnsaveHostelConfig($data,$userId) {
		$formData = array(
				'Applicableforapproval' => $data['Applicableforapproval'],
				'Applicableforrevert'  => $data['Applicableforrevert'],
				'Applicableforeservice' => $data['Applicableforeservice'],
				'Applicableforextend' => $data['Applicableforextend'],
				'Applicableforgenbill' => $data['Applicableforgenbill'],
				'IdUniversity' => $data['IdUniversity'],
				'Block' => $data['Block'],
				'Level' => $data['Level'],
				'Room' => $data['Room'],
				'chargesRate' => $data['chargesRate'],
				'UpdUser' => $userId,
				'UpdDate' => date('Y-m-d H:i:s'),
		);
		$this->insert($formData);
		$getlID = $this->lobjDbAdpt->lastInsertId();
		return $getlID;
	}


	/**
	 * Function to FETCH hostel Configuration setup
	 * @author: Vipul
	 */
	public function fnfetchConfiguration($IdUniversity) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_hostel_config"), array("a.*"))
		->where('a.IdUniversity =?',$IdUniversity);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


	/**
	 * Function to DELETE hostel Configuration setup
	 * @author: Vipul
	 */
	public function fndeleteHostelConfig() {
		//$this->lobjDbAdpt->query("DELETE FROM tbl_hostel_config");
		$stmt = $this->lobjDbAdpt->prepare("TRUNCATE TABLE tbl_hostel_config");
		$stmt->execute();
	}

	/*
	 * Function to update hostel configuration
	*/

	public function fnupdateHostelConfig($larrformData,$IdUniversity) {
		$larrformData = friendly_columns($this->_name, $larrformData);
		$where = 'IdUniversity = '.$IdUniversity;
		$this->update($larrformData,$where);
	}
	
	public function fnGetLabels($IdUniversity){
		$lstrSelect = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_hostel_config"), array("a.IdUniversity","a.Block_Label","a.Level_Label","a.Room_Label"))
					->where('a.IdUniversity =?',$IdUniversity);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetCurrency($deafult){
		if($deafult==''){
			$lstrSelect = $this->lobjDbAdpt->select()
			->from(array("a" => "tbl_currency"), array("a.cur_id","a.cur_code","a.cur_desc"));
			$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}else{
			$lstrSelect = $this->lobjDbAdpt->select()
			->from(array("a" => "tbl_currency"), array("a.cur_id","a.cur_code","a.cur_desc","a.cur_default"))
			->where('a.cur_default =?',$deafult);
			$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
	}
}
?>
