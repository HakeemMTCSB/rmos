<?php
class Hostel_Model_DbTable_Itemregistersetup extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_item';
	private $lobjDbAdpt;
	// Hold an instance of the class
	//private static $instance;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	//    public static function singleton()
	//    {
	//        if (!isset(self::$instance)) {
	//            $c = __CLASS__; //late static binding
	//            self::$instance = new $c;
	//        }
	//        //asd(self::$instance);
	//        return self::$instance;
	//    }


	/**
	 * Function to SAVE Itemregister setup
	 * @param array $data
	 * @param int $userId
	 * @author: Vipul
	 */
	public function fnsaveitemregister($data,$userId,$id=NULL) {
		$formData = array(
				'Name' => $data['Name'],
				'Code'  => $data['Code'],
				'ShortName' => $data['ShortName'],
				'DefaultLang' => $data['DefaultLang'],
				'UpdUser' => $userId,
				'UpdDate' => date('Y-m-d H:i:s'),
		);

		if(null === $id ) {
			$this->insert($formData);
		}
		else
		{
			$this->update($formData, array('IdHostelItem = ?' => $id));
		}
	}


	/**
	 * Function to FETCH Itemregister SETUP
	 * @author: Vipul
	 */
	public function fnfetchAllItemregister($post = array()) {
		$lstrSelect = $this->lobjDbAdpt->select()->from(array("sa" => "tbl_hostel_item"), array("sa.*"));

		if(isset($post['Name']) && !empty($post['Name']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.Name LIKE ?", "%".$post['Name']."%"));
		}
		if(isset($post['Code']) && !empty($post['Code']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.Code LIKE ?", "%".$post['Code']."%"));
		}
		if(isset($post['ShortName']) && !empty($post['ShortName']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.ShortName LIKE ?", "%".$post['ShortName']."%"));
		}
		if(isset($post['DefaultLang']) && !empty($post['DefaultLang']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.DefaultLang LIKE ?", "%".$post['DefaultLang']."%"));
		}

		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


	/**
	 * Function to FETCH Itemregister SETUP by condition
	 * @author: Vipul
	 */
	public function fnfetchItemregisterByID($condition) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_hostel_item"),array("a.*"))
		->where($condition);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}





	/**
	 * Function to delete Itemregister
	 * @author: vipul
	 */

	public function fndeleteitem($id){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = $lobjDbAdpt->quoteInto('IdHostelItem = ?', $id);
		$lobjDbAdpt->delete('tbl_hostel_item', $where);
		return 1;
	}

	public function fngetitemlist(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_hostel_item"),array('key'=>'a.IdHostelItem','value'=>'a.Name'));
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fninsertitemdetails($Idstudent,$larrformData,$userId){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_hostel_item_register';
		if(isset($larrformData['ItemNamegrid'])){
			for($i=0;$i<count($larrformData['ItemNamegrid']);$i++){
				$formData = array(
						'IdStudentRegistration'  => $Idstudent,
						'IdHostelItem' => $larrformData['ItemNamegrid'][$i],
						'Quantity' => $larrformData['QuantityNamegrid'][$i],
						'UpdUser' => $userId,
						'UpdDate' => date('Y-m-d H:i:s'),
				);
				$lobjDbAdpt->insert($table,$formData);
			}
		}
	}

	public function fndeletehostelitems($Idstudent){
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_hostel_item_register';
		$where = $db->quoteInto('IdStudentRegistration = ?', $Idstudent);
		$db->delete($table, $where);
	}

	public function getaddediteminfo($studentId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_hostel_item_register"),array('a.Quantity','a.IdHostelItem'))
		->joinLeft(array('b'=>'tbl_hostel_item'),'a.IdHostelItem = b.IdHostelItem',array('b.Name'))
		->where('a.IdStudentRegistration = ?',$studentId);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}


}
?>
