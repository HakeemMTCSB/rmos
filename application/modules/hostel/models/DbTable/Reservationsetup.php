<?php
class Hostel_Model_DbTable_Reservationsetup extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_reservation_setup';
	private $lobjDbAdpt;


	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fninsertreservationdata($data,$userId) {
		for($i=0;$i<count($data['SemesterCodeNamegrid']);$i++){
			$formData = array(
					'SemesterCode' => $data['SemesterCodeNamegrid'][$i],
					'Priority'  => $data['PriorityNamegrid'][$i],
					'ValueType' => $data['ValueTypeNamegrid'][$i],
					'Condition' => $data['ConditionNamegrid'][$i],
					'ValueId'	=> $data['ValueIdNamegrid'][$i],
					'UpdUser' => $userId,
					'UpdDate' => date('Y-m-d H:i:s'),
			);
			$this->insert($formData);
		}
		return ;
	}

	public function fngetreservationdata(){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_reservation_setup'),array('a.*'))
		->joinLeft(array('b'=>'tbl_definationms'),'a.Priority = b.idDefinition',('b.DefinitionDesc AS priority'))
		->joinLeft(array('c'=>'tbl_definationms'),'a.ValueType = c.idDefinition',('c.DefinitionDesc AS valueType'))
		->joinLeft(array('d'=>'tbl_definationms'),'a.Condition = d.idDefinition',('d.DefinitionDesc AS condition'))
		->joinLeft(array('e'=>'tbl_intake'),'a.ValueId = e.IdIntake OR a.ValueId = 0',('e.IntakeId'))
		->group('a.IdReservation');
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fndeletereservationdata(){
		$stmt = $this->lobjDbAdpt->prepare("TRUNCATE TABLE tbl_hostel_reservation_setup");
		$stmt->execute();
	}
}
