<?php
class Hostel_Model_DbTable_Hostellevel extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_level';
	private $lobjDbAdpt;


	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function saveHostelLevel($post){
        $post = friendly_columns($this->_name, $post);
         $this->insert($post);
        $getID = $this->getAdapter()->lastInsertId();
        return($getID);
	}

    public function fnsavelevelinventoryinfo($larrformData,$userId,$levelId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $table = 'tbl_hostel_level_inventory';
        if(isset($larrformData['InventoryNamegrid'])){
            for($i=0;$i<count($larrformData['InventoryNamegrid']);$i++){
                $formData = array(
                    'IdHostelLevel' => $levelId,
                    'InventoryName'  => $larrformData['InventoryNamegrid'][$i],
                    'Quantity' => $larrformData['QuantityNamegrid'][$i],
                    'UpdUser' => $userId,
                    'UpdDate' => date('Y-m-d H:i:s'),
                );
                $lobjDbAdpt->insert($table,$formData);
            }
        }
    }
	
	public function checkDuplicate($post)
	{
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('a.IdHostelLevel'))
		->where('IdHostelBlock = ?',$post['IdHostelBlock'])
		->where('Name = \''.$post['Name'].'\' OR Code = \''.$post['Code'].'\'');
		
		
		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return empty($result) ? true : false;
	}

	public function getAllLevel(){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('a.IdHostelLevel','a.Name AS levelname','a.DefaultLang AS defaultlevel'))
		->joinLeft(array('b' => 'tbl_hostel_block'),"a.IdHostelBlock = b.IdHostelBlock",array('b.Name AS blockname'));
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function getAllLevelList($Idblock){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('key'=>'a.IdHostelLevel','name'=>'a.Name'))
		->where('a.IdHostelBlock = ?',$Idblock);
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fngetAllLevelList(){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('key'=>'a.IdHostelLevel','value'=>'a.Name'));
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fnSearchhostelLevel($post){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('a.IdHostelLevel','a.Name AS levelname','a.DefaultLang AS defaultlevel'))
		->joinLeft(array('b' => 'tbl_hostel_block'),"a.IdHostelBlock = b.IdHostelBlock",array('b.Name AS blockname'));

		if (isset($post['field2']) && !empty($post['field2'])) {
			$lstrSelect = $lstrSelect->where('a.Name like "%" ? "%"', $post['field2']);
		}

		if (isset($post['field4']) && !empty($post['field4'])) {
			$lstrSelect = $lstrSelect->where('a.Code like "%" ? "%"', $post['field4']);
		}

		if (isset($post['field6']) && !empty($post['field6'])) {
			$lstrSelect = $lstrSelect->where('a.DefaultLang like "%" ? "%"', $post['field6']);
		}

		if (isset($post['field8']) && !empty($post['field8'])) {
			$lstrSelect = $lstrSelect->where('b.IdHostelBlock =?', $post['field8']);
		}
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}



	public function getLevelDet($id){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('a.*'))
		->joinLeft(array('b' => 'tbl_hostel_block'),"a.IdHostelBlock = b.IdHostelBlock",array('b.IdHostelBlock'))
		->where('a.IdHostelLevel =?',$id);
		$result = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $result;
	}

	public function UpdateHostelLevel($post,$id){
        $post = friendly_columns($this->_name, $post);
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = $lobjDbAdpt->quoteInto('IdHostelLevel = ?', $id);
		$lobjDbAdpt->update('tbl_hostel_level', $post,$where);
	}

	/**
	 * Function to check whether a block has hostel level or not.
	 * @author VT
	 */
	public function fnCheckLevel($idBlock) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_level'),array('a.*'))
		->where('a.IdHostelBlock =?',$idBlock);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

    public function fngetlevelinventoryinfo($Idlevel) {
        $sql = $this->lobjDbAdpt->select()
            ->from(array('a'=>'tbl_hostel_level_inventory'),array('a.Quantity','a.InventoryName'))
            ->joinLeft(array('b'=>'tbl_hostel_inventory'),'a.InventoryName = b.IdHostelInventory',array('b.Name'))
            ->where('a.IdHostelLevel = ?',$Idlevel);
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
    }

    public function fndeletelevelinventoryinfo($IdHostelLevel){
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = 'tbl_hostel_level_inventory';
        $where = $db->quoteInto('IdHostelLevel = ?', $IdHostelLevel);
        $db->delete($table, $where);
        return(true);
    }

}
?>

