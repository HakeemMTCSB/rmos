<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hostel_Model_DbTable_ReportHostel extends Zend_Db_Table_Abstract {
    
    public function getBlockList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_hostel_block'))
            ->order('a.Name');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getBlock($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_hostel_block'))
            ->where('a.IdHostelBlock = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getLevel($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_hostel_level'), array('value'=>'*'))
            ->where('a.IdHostelBlock = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getLevelById($formData = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_hostel_level'), array('value'=>'*'));
        
        if ($formData != false){
            if (isset($formData['block']) && $formData['block']!=''){
                $select->where('a.IdHostelBlock = ?', $formData['block']);
            }
            if (isset($formData['level']) && $formData['level']!=''){
                $select->where('a.IdHostelLevel = ?', $formData['level']);
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getRoom($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('f'=>'tbl_hostel_room'), array('value'=>'*', 'RoomTypeId'=>'RoomType'))
            ->joinLeft(array('a'=>'tbl_hostel_room_type'), 'f.RoomType = a.IdHostelRoomType')
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.OccupancyType = b.IdDefinition', array('occupancyname'=>'b.DefinitionDesc'))
            ->where('f.Level = ?', $id)
            ->order('f.IdHostelRoom');
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getReportHostelList($id, $limit, $status){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        
        if ($status == 1){
            $select->from(array('a'=>'tbl_hostel_registration'), array('value'=>'*'));
        }else{
            $select->from(array('a'=>'tbl_hostel_history'), array('value'=>'*', 'CheckOutDate'=>'a.checkinDate', 'CheckInDate'=>'a.checkinDate2'));
        }
            
        $select->joinLeft(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration=b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_intake'), 'b.IdIntake=e.IdIntake')
            ->where('a.idHostelRoom = ?', $id);
                
        if ($status == 1){
            $select->limit($limit);
        }else{
            $select->where('a.Activity = ?', 'Check Out');
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getRoomRate($id, $IdIntake=false, $CheckInDate=false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            /*->from(array('a'=>'tbl_room_charges'), array('value'=>'*'))
            ->where('a.IdHostelRoom = ?', $id)
            ->where('a.CustomerType = ?', 263);*/
            ->from(array('a'=>'tbl_room_charges'),array('StartDate'=>'DATE(StartDate)','Rate','Currency'))
            ->where('a.IdHostelRoom = ?',$id);
        
            if ($IdIntake != false){
                $select->where('a.IdIntake = ?',$IdIntake);
            }
                
            if ($CheckInDate != false){
                $select->where("'$CheckInDate' >= DATE(a.StartDate)");
                $select->where("CURDATE() >= DATE(a.StartDate)");
            }
            
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemesterSession($date, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->where('"'.$date.'" BETWEEN a.SemesterMainStartDate AND a.SemesterMainEndDate')
            ->where('IdScheme = ?', $scheme);

        $result = $db->fetchRow($select);
        return $result;
    }
}