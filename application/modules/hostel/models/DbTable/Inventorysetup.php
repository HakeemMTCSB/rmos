<?php
class Hostel_Model_DbTable_Inventorysetup extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_inventory';
	private $lobjDbAdpt;
	// Hold an instance of the class
	//private static $instance;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	//    public static function singleton()
	//    {
	//        if (!isset(self::$instance)) {
	//            $c = __CLASS__; //late static binding
	//            self::$instance = new $c;
	//        }
	//        //asd(self::$instance);
	//        return self::$instance;
	//    }


	/**
	 * Function to SAVE inventory  setup
	 * @param array $data
	 * @param int $userId
	 * @author: Vipul
	 */
	public function fnsaveinventory($data,$userId,$id=NULL) {
		$formData = array(
				'Name' => $data['Name'],
				'Code'  => $data['Code'],
				'ShortName' => $data['ShortName'],
				'DefaultLang' => $data['DefaultLang'],
				'UpdUser' => $userId,
				'UpdDate' => date('Y-m-d H:i:s'),
		);

		if(null === $id ) {
			$this->insert($formData);
		}
		else
		{
			$this->update($formData, array('IdHostelInventory = ?' => $id));
		}
	}


	/**
	 * Function to FETCH Inventory SETUP
	 * @author: Vipul
	 */
	public function fnfetchAllInventory($post = array()) {
		$lstrSelect = $this->lobjDbAdpt->select()->from(array("sa" => "tbl_hostel_inventory"), array("sa.*"));

		if(isset($post['Name']) && !empty($post['Name']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.Name LIKE ?", "%".$post['Name']."%"));
		}
		if(isset($post['Code']) && !empty($post['Code']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.Code LIKE ?", "%".$post['Code']."%"));
		}
		if(isset($post['ShortName']) && !empty($post['ShortName']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.ShortName LIKE ?", "%".$post['ShortName']."%"));
		}
		if(isset($post['DefaultLang']) && !empty($post['DefaultLang']) ){
			$lstrSelect->where($this->lobjDbAdpt->quoteInto("sa.DefaultLang LIKE ?", "%".$post['DefaultLang']."%"));
		}

		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	/**
	 * Function to FETCH Inventory SETUP by condition
	 * @author: Dushyant
	 */
	public function fnfetchInventoryList() {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_hostel_inventory"),array("key"=>"a.IdHostelInventory","value"=>"a.Name"));
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}


	/**
	 * Function to FETCH Inventory SETUP by condition
	 * @author: Vipul
	 */
	public function fnfetchInventoryByID($condition) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_hostel_inventory"),array("a.*"))
		->where($condition);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}





	/**
	 * Function to delete Inventory
	 * @author: vipul
	 */

	public function fndeleteinvnt($id){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = $lobjDbAdpt->quoteInto('IdHostelInventory = ?', $id);
		$lobjDbAdpt->delete('tbl_hostel_inventory', $where);
		return 1;
	}

	public function fntotalInventories(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->select()->from(array('a' => 'tbl_hostel_inventory',array('a.IdHostelInventory')));
		$result = $db->fetchAll($sql);
		$totalVal = count($result);
		return $totalVal;
	}



}
?>
