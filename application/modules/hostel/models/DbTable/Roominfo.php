<?php
class Hostel_Model_DbTable_Roominfo extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_room';
	private $lobjDbAdpt;
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function checkDuplicate($post)
	{
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_hostel_room'),array('a.IdHostelRoom'))
		->where('Block = ?',$post['Block'])
		->where('Level = ?', $post['Level'])
		->where('RoomName = \''.$post['RoomName'].'\' OR RoomCode = \''.$post['RoomCode'].'\'');

		
		$result = $this->lobjDbAdpt->fetchRow($sql);
		
		return empty($result) ? true : false;
	}
	
	public function getInventoryTypes()
	{
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_inventory'),array('a.IdHostelInventory','a.Name'));
		$result = $this->lobjDbAdpt->fetchAll($sql);
	
		return $result;
	}
	
	public function getOccupiedByHostel()
	{
		/*
		 	SELECT COUNT(a.IdHostelRegistration) as Total, b.Block FROM `tbl_hostel_registration` a 
			INNER JOIN `tbl_hostel_room` b ON (a.IdHostelRoom=b.IdHostelRoom)
			WHERE 
			a.StudentStatus=1
			GROUP BY b.Block
		 */
		
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_registration'),array('COUNT(a.IdHostelRegistration) as Total'))
		->join(array('b'=>'tbl_hostel_room'),'a.IdHostelRoom=b.IdHostelRoom',array('b.Block'))
		->where('a.StudentStatus=?',1)
		->group('b.Block');
		//echo $sql;
		$result = $this->lobjDbAdpt->fetchAll($sql);
		//print_r($result);
		return $result;
		
	}
	
	public function getCapacityByHostel()
	{
		/*
		 	SELECT SUM( b.Capacity ) AS Total, b.Block
			FROM `tbl_hostel_room` b
			GROUP BY b.Block
		*/
		
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('SUM(a.Capacity) as Total','a.Block'))
		->group('a.Block');
		
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	
	}
	
	public function getInventoriesByHostel()
	{
		/*
		 	SELECT e.amount, e.Name, e.blockname
			FROM (
			SELECT SUM( a.Quantity ) AS amount, b.Name AS Name, c.Name as blockname
			FROM tbl_hostel_room_inventory a
			JOIN tbl_hostel_inventory b ON (a.InventoryName = b.IdHostelInventory)
			JOIN tbl_hostel_room d
			RIGHT JOIN (
			SELECT f.IdHostelBlock AS IdHostelBlock, f.Name AS Name FROM tbl_hostel_block f
			)c ON (c.IdHostelBlock = d.Block)
			
			AND d.IdHostelRoom = a.IdHostelRoom
			GROUP BY b.Name, c.Name
			)e
		*/
			
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room_inventory'),array('SUM(a.Quantity) as Total','a.InventoryName'))
		->join(array('c'=>'tbl_hostel_inventory'), 'a.InventoryName=c.IdHostelInventory', array('c.Name'))
		->join(array('b'=>'tbl_hostel_room'), 'a.IdHostelRoom=b.IdHostelRoom', array('b.Block'))
		->group('b.Block')
		->group('c.Name');
	
		
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}
	
	public function getGenderByHostel()
	{
		/*SELECT COUNT( a.IdHostelregistration ) AS Total, b.Gender, c.Block
		FROM `tbl_hostel_registration` a
		INNER JOIN `tbl_studentregistration` b ON ( a.IdStudentRegistration = b.IdStudentRegistration )
		INNER JOIN `tbl_hostel_room` c ON ( a.IdHostelRoom = c.IdHostelRoom )
		GROUP BY b.Gender, c.Block */
		
		/* Gender 1 is male */
		
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_registration'),array('COUNT(a.IdHostelregistration) as Total'))
		->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration ', array('b.Gender'))
		->join(array('c'=>'tbl_hostel_room'), 'a.IdHostelRoom=c.IdHostelRoom', array('c.Block'))
		->group('b.Gender')
		->group('c.Block');
	
		//echo $sql;
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}
	
	public function fnsaveroominfo($data,$userId){
		$formData = array(
				'RoomName' => $data['RoomName'],
				'RoomCode'  => $data['RoomCode'],
				'Block' => $data['Block'],
				'DefaultLang' => $data['DefaultLang'],
				'Level' => $data['Level'],
				'Capacity' => $data['Capacity'],
				'Gender' => $data['Gender'],
                'PropertyOwner' => $data['PropertyOwner'],
				'RoomType' => $data['RoomType'],
				'Active'	=> "1",
				'UpdUser' => $userId,
				'UpdDate' => date('Y-m-d H:i:s'),
		);
		$this->insert($formData);
		$getlID = $this->lobjDbAdpt->lastInsertId();
		return $getlID;
	}

	public function fnsaveroominventoryinfo($larrformData,$userId,$roomId){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_hostel_room_inventory';
		if(isset($larrformData['InventoryNamegrid'])){
			for($i=0;$i<count($larrformData['InventoryNamegrid']);$i++){
				$formData = array(
						'IdHostelRoom' => $roomId,
						'InventoryName'  => $larrformData['InventoryNamegrid'][$i],
						'Quantity' => $larrformData['QuantityNamegrid'][$i],
						'UpdUser' => $userId,
						'UpdDate' => date('Y-m-d H:i:s'),
				);
				$lobjDbAdpt->insert($table,$formData);
			}
		}
	}

	public function fngetallroominfo(){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('a.IdHostelRoom','a.RoomName','a.RoomCode','a.Capacity'))
		->joinLeft(array('b'=>'tbl_hostel_room_type'),'a.RoomType = b.IdHostelRoomType',array('b.RoomType'))
		->joinLeft(array('c'=>'tbl_hostel_block'),'a.Block = c.IdHostelBlock',array('c.Name AS Block'))
		->joinLeft(array('d'=>'tbl_hostel_level'),'a.Level = d.IdHostelLevel',array('d.Name AS Level'));
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fngetroominfo($roomId){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('a.*'))
		->where('a.IdHostelRoom = ?',$roomId);
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}
	
	public function fngetroominfodetail($roomId){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('a.*'))
		->joinLeft(array('b'=>'tbl_hostel_room_type'),'a.RoomType = b.IdHostelRoomType',array('b.RoomType'))
		->joinLeft(array('c'=>'tbl_hostel_block'),'a.Block = c.IdHostelBlock',array('c.Name AS BlockName'))
		->joinLeft(array('d'=>'tbl_hostel_level'),'a.Level = d.IdHostelLevel',array('d.Name AS LevelName'))
		
		->where('a.IdHostelRoom = ?',$roomId);
		$result = $this->lobjDbAdpt->fetchRow($sql);
		return $result;
	}
	
	public function getstudentlist() {
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_studentregistration'))
		->join(array('StudentProfile' => 'student_profile'), 'StudentProfile.id = a.sp_id',  array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as value", "a.IdStudentRegistration as key"))
		->where("a.IdStudentRegistration NOT IN ( Select IdStudentRegistration from tbl_hostel_registration where IdHostelRoom != '')")
		->where("a.profileStatus = 92 OR a.profileStatus = 253");
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fngetinventoryinfo($roomId){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room_inventory'),array('a.Quantity','a.InventoryName'))
		->joinLeft(array('b'=>'tbl_hostel_inventory'),'a.InventoryName = b.IdHostelInventory',array('b.Name'))
		->where('a.IdHostelRoom = ?',$roomId);
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fnupdatehostelroom($data,$userId){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = 'IdHostelRoom = '.$data['IdHostelRoom'];
		$table = 'tbl_hostel_room';
		$formData = array(
				'RoomName' => $data['RoomName'],
				'RoomCode'  => $data['RoomCode'],
				'Block' => $data['Block'],
				'DefaultLang' => $data['DefaultLang'],
				'Level' => $data['Level'],
				'Gender' => $data['Gender'],
                'PropertyOwner' => $data['PropertyOwner'],
				'Capacity' => $data['Capacity'],
				'RoomType' => $data['RoomType'],
				'Active'	=> "1",
				'UpdUser' => $userId,
				'UpdDate' => date('Y-m-d H:i:s'),
		);
		$lobjDbAdpt->update($table,$formData,$where);
	}

	public function fndeleteroominventoryinfo($IdHostelRoom){
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_hostel_room_inventory';
		$where = $db->quoteInto('IdHostelRoom = ?', $IdHostelRoom);
		$db->delete($table, $where);
	}

	public function fnSearchhostelroom($post){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room'),array('a.IdHostelRoom','a.RoomName','a.RoomCode','a.Capacity'))
		->joinLeft(array('b'=>'tbl_hostel_room_type'),'a.RoomType = b.IdHostelRoomType',array('b.RoomType'))
		->joinLeft(array('c'=>'tbl_hostel_block'),'a.Block = c.IdHostelBlock',array('c.Name AS Block'))
		->joinLeft(array('d'=>'tbl_hostel_level'),'a.Level = d.IdHostelLevel',array('d.Name AS Level'));

		if (isset($post['field2']) && !empty($post['field2'])) {
			$lstrSelect = $lstrSelect->where('a.RoomName like "%" ? "%"', $post['field2']);
		}

		if (isset($post['field4']) && !empty($post['field4'])) {
			$lstrSelect = $lstrSelect->where('a.RoomCode like "%" ? "%"', $post['field4']);
		}

		if (isset($post['field8']) && !empty($post['field8'])) {
			$lstrSelect = $lstrSelect->where('a.Level like "%" ? "%"', $post['field8']);
		}

		if (isset($post['field10']) && !empty($post['field10'])) {
			$lstrSelect = $lstrSelect->where('a.Block =?', $post['field10']);
		}
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
}