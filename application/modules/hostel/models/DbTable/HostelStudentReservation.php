<?php
class Hostel_Model_DbTable_HostelStudentReservation extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_student_reservation';

	public function save(array $data){
		
		if(isset($data['id'])) {
			$where  = $this->getAdapter()->quoteInto('id = ?', $data['id']);
			$this->update($data, $where);
			$id = $data['id'];
		} else {
			$id = $this->insert($data);	
		}

		return $id;
	}



}