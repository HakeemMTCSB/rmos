<?php
class Hostel_Model_DbTable_Hostelreservationhistory extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_reservation_history';
	private $lobjDbAdpt;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnAddHostelReservationHistory($data){
		$this->insert($data);
	}

	public function fngetstudentHistoryDetail($IdStudentRegistration){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_reservation_history'),array('a.*'))
		->where('a.IdStudentRegistration =?',$IdStudentRegistration);
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}



}