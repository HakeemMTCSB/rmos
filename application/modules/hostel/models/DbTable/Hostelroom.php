<?php
class Hostel_Model_DbTable_Hostelroom extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_room_type';
	private $lobjDbAdpt;


	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fngethostelroomdetail(){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room_type'),array('a.*'));
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fnSearchhostelroom($larrformData){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room_type'),array('a.*'));
		if(!empty($larrformData['field3']) && $larrformData['field3'] != ''){
			$sql = $sql->where('a.RoomType like "%" ? "%"',$larrformData['field3']);
		}
		if(!empty($larrformData['field2']) && $larrformData['field2'] != ''){
			$sql = $sql->where('a.RoomCode like "%" ? "%"',$larrformData['field2']);
		}
		if(!empty($larrformData['field4']) && $larrformData['field4'] != ''){
			$sql = $sql->where('a.ShortName like "%" ? "%"',$larrformData['field4']);
		}
		if(!empty($larrformData['field6']) && $larrformData['field6'] != ''){
			$sql = $sql->where('a.DefaultLang like "%" ? "%"',$larrformData['field6']);
		}
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fninserthostelroom($data,$userid){

		$data['UpdUser'] = $userid;
		$data['UpdDate'] = date('Y-m-d H:i:s');
		$data['Active'] = "1";

		$formData = friendly_columns($this->_name, $data);

		$this->insert($formData);
		$id = $this->lobjDbAdpt->lastInsertId();
		return $id;
	}

	public function checkDuplicateRoomType($code, $id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_hostel_room_type'))
			->where('a.RoomCode = ?', $code);

		if ($id != 0){
			$select->where('a.IdHostelRoomType != ?', $id);
		}

		$result = $db->fetchAll($select);
		return $result;
	}

	public function fnupdatehostelroom($data,$userId){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = 'IdHostelRoomType = '.$data['IdHostelRoomType'];
		$table = 'tbl_hostel_room_type';
		
		//additional fields
		$data['UpdUser'] = $userId;
		$data['UpdDate'] = date('Y-m-d H:i:s');
		$data['Active'] = "1";

		$formData = friendly_columns($this->_name, $data);
		$lobjDbAdpt->update($table,$formData,$where);

	}

	public function fninsertchargesdetail($idroomType,$larrformData,$userId){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_room_charges';
		if(isset($larrformData['IdIntakeNamegrid'])){
			for($i=0;$i<count($larrformData['IdIntakeNamegrid']);$i++) {
				$formData = array(
						'IdHostelRoom' => $idroomType,
						'IdIntake'  => $larrformData['IdIntakeNamegrid'][$i],
						'StartDate' => splitAndFormatDojodate($larrformData['StartDateNamegrid'][$i]),
						'Rate' => $larrformData['RateNamegrid'][$i],
						'Deposit' => $larrformData['Depositgrid'][$i],
						'Admin' => $larrformData['Admingrid'][$i],
						'CustomerType' => $larrformData['CustomerTypeNamegrid'][$i],
						'Currency' => $larrformData['CurrencyNamegrid'][$i],
						'tenancy_period' => $larrformData['TenancyPeriod'][$i],
						'UpdUser' => $userId,
						'UpdDate' => date('Y-m-d H:i:s'),
				);
				$lobjDbAdpt->insert($table,$formData);
			}
		}
	}

	public function fngetroomtypedetails($Idroomtype) {
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room_type'),array('a.*'))
		->joinLeft(array('Occupancy'=>'tbl_definationms'),'Occupancy.idDefinition = a.OccupancyType',array('Occupancy.DefinitionDesc AS occupancy_type'))
		->joinLeft(array('RoomFeature'=>'tbl_definationms'),'RoomFeature.idDefinition = a.RoomFeature',array('RoomFeature.DefinitionDesc AS room_feature'))
		->where('a.IdHostelRoomType =?',$Idroomtype);
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	public function fngetroomtypechargedetails($Idroomtype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$sql = $lobjDbAdpt->select()
		->from(array('a'=>'tbl_room_charges'),array('a.*', 'datechargecreated'=>'a.UpdDate'))
		
		->joinLeft(array('b'=>'tbl_definationms'),'b.idDefinition = a.CustomerType',array('b.DefinitionDesc AS Customer'))
		->joinLeft(array('Definitions'=>'tbl_definationms'),'Definitions.idDefinition = a.tenancy_period',array('Definitions.DefinitionDesc AS tenancy_period_name'))
		->joinLeft(array('c'=>'tbl_intake'),'c.IdIntake = a.IdIntake',array('c.*'))
		->joinLeft(array('d'=>'tbl_currency'),'d.cur_id = a.Currency',array('d.cur_desc'))
		->joinLeft(array('e'=>'tbl_user'), 'a.UpdUser = e.iduser', array('e.loginName'))
		->where('a.IdHostelRoom =?',$Idroomtype);
		$result = $lobjDbAdpt->fetchAll($sql);
		return $result;
	}


	public function fndeleteroomchargesinfo($IdHostelRoomType){
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_room_charges';
		$where = $db->quoteInto('IdHostelRoom = ?', $IdHostelRoomType);
		$db->delete($table, $where);
	}

	public function fngetListofroomtypes(){
		$sql = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_hostel_room_type'),array('key'=>'a.IdHostelRoomType','value'=>'a.RoomType'))
		->where('a.Active = ?',1)
		;
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}
	
	public function fnSearchVacantRoom($post = array()) {
		$hostelreserve = new Hostel_Model_DbTable_Hostelreservation();
		$select = $this->lobjDbAdpt->select()
				->from(array("a" => "tbl_hostel_room"), array("a.*"))
				;
		if(isset($post['Block']) && !empty($post['Block']) ) {
			$select = $select->where("a.Block =?",$post['Block']);
		}
		if(isset($post['Level']) && !empty($post['Level']) ) {
			$select = $select->where("a.Level =?",$post['Level']);
		}

		$result = $this->lobjDbAdpt->fetchAll($select);
		foreach($result as $cur_index => $room) {
				$result[$cur_index]['OccupantCount'] = count($hostelreserve->getReservationsByRoom($room['IdHostelRoom']));
		}

		return $result;
	}
	
	public function fnGetRooms($blockid,$levelid) 
	{
		$select = $this->lobjDbAdpt->select()
				->from(array("a" => "tbl_hostel_room"), array("a.IdHostelRoom as key","a.RoomName as name"));
		if($blockid != '')
		{
			$select = $select->where("a.Block =?",$blockid);
		}
		
		if( $levelid != '' )
		{
			$select = $select->where("a.Level =?",$levelid);
		}
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function get_room_charges() {
		$select = $this->lobjDbAdpt->select()
					->from( array('RoomType' => 'tbl_hostel_room_type' ) )
					->joinLeft(array('RoomCharge' => 'tbl_room_charges'), 'RoomType.IdHostelRoomType = RoomCharge.IdHostelRoom')
					->joinLeft(array('Intake' => 'tbl_intake'), 'Intake.IdIntake = RoomCharge.IdIntake')
					->joinLeft(array('Definition' => 'tbl_definationms'), 'Definition.idDefinition = RoomCharge.CustomerType')
					->joinLeft(array('Definitions'=>'tbl_definationms'),'Definitions.idDefinition = RoomCharge.tenancy_period',array('Definitions.DefinitionDesc AS tenancy_period'));

		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}
	
	public function get_roomtype_active_charges($hostel_room_type_id, $cust_type = 263) {
		$select = $this->lobjDbAdpt->select()
					->from( array('RoomType' => 'tbl_hostel_room_type' ) )
					->joinLeft(array('RoomCharge' => 'tbl_room_charges'), 'RoomType.IdHostelRoomType = RoomCharge.IdHostelRoom')
					->joinLeft(array('Definition' => 'tbl_definationms'), 'Definition.idDefinition = RoomCharge.CustomerType')
					->joinLeft(array('Definitions'=>'tbl_definationms'),'Definitions.idDefinition = RoomCharge.tenancy_period',array('Definitions.DefinitionDesc AS tenancy_period'))
					->where('RoomCharge.CustomerType = ?', $cust_type)
					->where('RoomType.StartDate <= ?', date('Y-m-d'))
					->where('RoomType.IdHostelRoomType = ?', $hostel_room_type_id)
					->order('RoomCharge.StartDate Desc')
					->limit(1);
					;
		$current_charge = $this->lobjDbAdpt->fetchRow($select);
		return($current_charge);
	}
	
	public function getRoomList($type='263'){ //default 263=>student
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('rt'=>$this->_name))
					  ->join(array('rc'=>'tbl_room_charges'),'rc.IdHostelRoom=rt.IdHostelRoomType',array('Rate'))
                                          ->joinLeft(array('df'=>'tbl_definationms'), 'rt.OccupancyType = df.idDefinition', array('occTypeName'=>'df.DefinitionDesc'))
					  ->where("rc.CustomerType = ?", $type);  //student	
		 $row = $db->fetchAll($select);	
		
		 return $row;
	}

	//263 = student
	static public function getRoomTypeCharges($room_type_id, $cust_type = 263, $date = null) {
		if(empty($date)) {
			$date = date('Y-m') . '-01';
		}

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
					->from( array('RoomCharge' => 'tbl_room_charges' ))
					->joinLeft(array('Intake' => 'tbl_intake'), 'Intake.IdIntake = RoomCharge.IdIntake')
					->joinLeft(array('Definition' => 'tbl_definationms'), 'Definition.idDefinition = RoomCharge.CustomerType')
					->joinLeft(array('Definitions'=>'tbl_definationms'),'Definitions.idDefinition = RoomCharge.tenancy_period',array('Definitions.DefinitionDesc AS tenancy_period_name'))
					->where('RoomCharge.CustomerType = ?', $cust_type)
					->where('RoomCharge.IdHostelRoom = ?', $room_type_id)
					->where('RoomCharge.StartDate <= ?', $date)
					->order('RoomCharge.StartDate DESC')
					;
		
		$result = $db->fetchRow($select);
		return $result;
	}

	public function getImages($id) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
					->from(array('RoomPhoto' => 'tbl_hostel_roomtype_photos'))
					->where('room_type_id = ?', $id);
		$fotos = $db->fetchAll($select);
		return($fotos);
	}

	public function deletefile($id) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = 'id = ' .(int) $id;
		$db->delete('tbl_hostel_roomtype_photos', $select);
		return(true);
	}
	
}