<?php

class Hostel_Model_DbTable_Hostelreservation extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_reservation';
	protected $_primary = 'IdHostelReservation';
	private $db;

	public function init() {
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}


	public function fngetroomdetailByReservation($IdHostelReservation) {
		
		$sql = $this->db->select()
		->from(array('a'=>'tbl_hostel_reservation'),array('a.*'))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_trans_id = a.IdStudentRegistration', array('ApplicantTransaction.at_trans_id'))
		->joinLeft(array('b' => 'tbl_hostel_room'),"a.IdHostelRoom = b.IdHostelRoom",array('b.RoomCode as Room','b.RoomName as RoomName', 'b.RoomType as roomtypeid'))
		->joinLeft(array('c' => 'tbl_hostel_room_type'),"b.RoomType = c.IdHostelRoomType",array('c.RoomType as RoomType'))
		->joinLeft(array('d' => 'tbl_hostel_block'),"d.IdHostelBlock = b.Block",array('d.Name as BlockName'))
		->joinLeft(array('e' => 'tbl_hostel_level'),"e.IdHostelLevel = b.Level",array('e.Name as LevelName'))
		->joinLeft(array('hb' => 'tbl_hostel_block'),'b.Block = hb.IdHostelBlock',array('CONCAT_WS(" ",hb.Address1,hb.Address2) as address','hb.Zipcode','hb.Phone'))
		->joinLeft(array('ci' => 'tbl_city'),'hb.City = ci.IdCity',array('ci.CityName'))
		->joinLeft(array('st' => 'tbl_state'),'hb.State = st.IdState',array('st.StateName'))
		->joinLeft(array('co' => 'tbl_countries'),'hb.Country = co.idCountry',array('co.CountryName'))
		->where('a.IdHostelReservation = ?',$IdHostelReservation);
		$result = $this->db->fetchAll($sql);

		return $result;
	}

	public function fngetroomdetails($at_appl_id) {
		
		$sql = $this->db->select()
		->from(array('a'=>'tbl_hostel_reservation'),array('a.*'))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_trans_id = a.IdStudentRegistration', array('ApplicantTransaction.at_trans_id'))
		->joinLeft(array('b' => 'tbl_hostel_room'),"a.IdHostelRoom = b.IdHostelRoom",array('b.RoomCode as Room','b.RoomName as RoomName'))
		->joinLeft(array('c' => 'tbl_hostel_room_type'),"b.RoomType = c.IdHostelRoomType",array('c.RoomType as RoomType'))
		->joinLeft(array('d' => 'tbl_hostel_block'),"d.IdHostelBlock = b.Block",array('d.Name as BlockName'))
		->joinLeft(array('e' => 'tbl_hostel_level'),"e.IdHostelLevel = b.Level",array('e.Name as LevelName'))
		->joinLeft(array('hb' => 'tbl_hostel_block'),'b.Block = hb.IdHostelBlock',array('CONCAT_WS(" ",hb.Address1,hb.Address2) as address','hb.Zipcode','hb.Phone'))
		->joinLeft(array('ci' => 'tbl_city'),'hb.City = ci.IdCity',array('ci.CityName'))
		->joinLeft(array('st' => 'tbl_state'),'hb.State = st.IdState',array('st.StateName'))
		->joinLeft(array('co' => 'tbl_countries'),'hb.Country = co.idCountry',array('co.CountryName'))
		->where('ApplicantTransaction.at_appl_id = ?',$at_appl_id);
		$result = $this->db->fetchAll($sql);

		return $result;
	}

	public function searchStudentforHostel() {
		
		$sql = $this->db->select()
		->from(array('b' => 'tbl_hostel_reservation'), array('b.IdHostelRoom', "b.Active","b.Status","b.IdHostelReservation","b.cust_type", 'b.IdStudentRegistration as IdCustomerId'))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_appl_id = b.IdStudentRegistration', array('ApplicantTransaction.at_trans_id'))
		->joinLeft(array('a' => 'tbl_studentregistration'),'a.IdStudentRegistration = b.IdStudentRegistration', array("CONCAT_WS(' ',IFNULL(a.FName,''),IFNULL(a.MName,''),IFNULL(a.LName,'')) as name", "a.registrationId", "a.IdStudentRegistration"))
		->joinLeft(array('c' => 'tbl_hostel_room'), 'b.IdHostelRoom = c.IdHostelRoom', array('c.RoomName', 'c.RoomCode'))
		->joinLeft(array('d' => 'tbl_definationms'), 'b.Status = d.idDefinition', array('d.DefinitionCode AS status'))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'b.IdStudentRegistration = ApplicantTransaction.at_trans_id', array('ApplicantTransaction.at_appl_id'))
		->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantTransaction.at_appl_id = ApplicantProfile.appl_id', array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name"))
		->where("a.IdStudentRegistration NOT IN ( Select IdStudentRegistration from tbl_hostel_registration where IdHostelRoom != '')")
		->where("a.profileStatus = 92 OR a.profileStatus = 253")
		;
		$result = $this->db->fetchAll($sql);
		return $result;
		
	}

	public function getHostelReservation($IdHostelReservation) {
		$lstrSelect = $this->db->select()
			->from(array('HostelReservation' => 'tbl_hostel_reservation'), array('HostelReservation.IdHostelRoom', "HostelReservation.Active","HostelReservation.cust_type", 'HostelReservation.IdStudentRegistration as IdCustomerId', "HostelReservation.IdHostelReservation", "HostelReservation.IdHostelRoom", "HostelReservation.tenant_type", 'HostelReservation.IdSemester', 'HostelReservation.*' ))
			//for applicant
			->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'HostelReservation.IdStudentRegistration = ApplicantTransaction.at_trans_id', array('ApplicantTransaction.at_trans_id', 'ApplicantTransaction.at_pes_id'))
			->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id', 
					array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name", 'ApplicantProfile.appl_id', 'ApplicantProfile.*' ))

			//for student
			->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = HostelReservation.IdStudentRegistration', array("StudentRegistration.registrationId"))
			->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.appl_id =  StudentRegistration.IdApplication' ,array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as student_name", 'StudentProfile.appl_id', 'StudentProfile.*' ))
			
			//and reservation details			
			->joinLeft(array('RoomType' => 'tbl_hostel_room_type'), 'RoomType.IdHostelRoomType = HostelReservation.IdHostelRoomType', array('RoomType.RoomType','RoomType.RoomCode as RoomTypeCode'))
			->joinLeft(array('TenantType' => 'tbl_definationms'), 'HostelReservation.tenant_type = TenantType.idDefinition', array('TenantType.DefinitionCode AS tenant_type_name'))
			->joinLeft(array('c' => 'tbl_hostel_room'), 'HostelReservation.IdHostelRoom = c.IdHostelRoom', array('c.RoomName', 'c.RoomCode', 'c.IdHostelRoom as RoomId'))
			->joinLeft(array('d' => 'tbl_definationms'), 'HostelReservation.Status = d.idDefinition', array('d.DefinitionCode AS status'))
			->joinLeft(array('e' => 'tbl_hostel_room_type'), "c.RoomType = e.IdHostelRoomType", array('e.RoomType as RoomType'))
			->joinLeft(array('f' => 'tbl_hostel_block'), "f.IdHostelBlock = c.Block", array('f.Name as BlockName'))
			->joinLeft(array('g' => 'tbl_hostel_level'), "g.IdHostelLevel = c.Level", array('g.Name as LevelName'))
			->where('HostelReservation.IdHostelReservation = ?', $IdHostelReservation)
		;
		
		$result = $this->db->fetchRow($lstrSelect);
		return($result);

	}

	public function searchStudentforHostelReservation($post = null) {
		//select from student profile,
		//foreach student get their application id
		//get transaction id based on application id
		//and lastly get the awesomest hostel reservation
		$lstrSelect = $this->db->select()
			->from(array('HostelReservation' => 'tbl_hostel_reservation'), array('HostelReservation.IdHostelRoom', "HostelReservation.Active","HostelReservation.cust_type", 'HostelReservation.IdStudentRegistration as IdCustomerId', "HostelReservation.IdHostelReservation", "HostelReservation.IdHostelRoom", "HostelReservation.tenant_type", 'HostelReservation.SemesterStartDate', 'HostelReservation.SemesterEndDate', 'HostelReservation.agent_id', 'HostelReservation.IdStudentRegistration','HostelReservation.applied_date' ))
						
			//for applicant
			->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'HostelReservation.IdStudentRegistration = ApplicantTransaction.at_trans_id and cust_type = 2', array('ApplicantTransaction.at_trans_id', 'ApplicantTransaction.at_pes_id'))
			->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id', 
					array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name", 'ApplicantProfile.appl_id' ))

			//for student
			->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = HostelReservation.IdStudentRegistration and cust_type = 1', array("StudentRegistration.registrationId"))
			->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.id =  StudentRegistration.sp_id' ,array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as student_name", 'StudentProfile.appl_id' ))
			
			//and reservation details			
			->joinLeft(array('RoomType' => 'tbl_hostel_room_type'), 'RoomType.IdHostelRoomType = HostelReservation.IdHostelRoomType', array('RoomType.RoomType','RoomType.RoomCode as RoomTypeCode'))
			
			->joinLeft(array('TenantType' => 'tbl_definationms'), 'HostelReservation.tenant_type = TenantType.idDefinition', array('TenantType.DefinitionCode AS tenant_type_name'))
			->joinLeft(array('c' => 'tbl_hostel_room'), 'HostelReservation.IdHostelRoom = c.IdHostelRoom', array('c.RoomName', 'c.RoomCode'))
			->joinLeft(array('AppointedRoomType' => 'tbl_hostel_room_type'), 'AppointedRoomType.IdHostelRoomType = c.RoomType', array('AppointedRoomType.IdHostelRoomType as AppointedRoomTypeId',  'AppointedRoomType.RoomType as AppointedRoomType','AppointedRoomType.RoomCode as AppointedRoomTypeCode'))
			->joinLeft(array('d' => 'tbl_definationms'), 'HostelReservation.Status = d.idDefinition', array('d.DefinitionCode AS status'))
			;
		
		if (isset($post['field3']) && !empty($post['field3'])) {
			//$lstrSelect->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdApplication = StudentProfile.appl_id', array())
			$lstrSelect->where('StudentRegistration.registrationId like "%'.$this->db->quote($post['field3']).'%" OR ApplicantTransaction.at_pes_id like "%'. $this->db->quote($post['field3']).'%"');
		}



		if (isset($post['field5']) && !empty($post['field5'])) {
			$lstrSelect = $lstrSelect->where($this->db->quoteInto("c.Block =?",$post['field5']));
		}

		if (isset($post['field8']) && !empty($post['field8'])) {
			$lstrSelect = $lstrSelect->where($this->db->quoteInto("c.Level =?",$post['field8']));
		}

		if (isset($post['field6']) && !empty($post['field6'])) {
			$lstrSelect = $lstrSelect->where($this->db->quoteInto('c.RoomCode like ?', "%".$post['field6']."%"));
		}

		if (isset($post['field20']) && !empty($post['field20'])) {
			$lstrSelect = $lstrSelect->where($this->db->quoteInto("c.RoomType =?",$post['field20']));
		}

		if(!empty($post['SemesterStartDate'])) {
			$lstrSelect = $lstrSelect->where($this->db->quoteInto('HostelReservation.SemesterStartDate >= ?', $post['SemesterStartDate']));
		}

		if(!empty($post['SemesterEndDate'])) {
			$lstrSelect = $lstrSelect->where($this->db->quoteInto('HostelReservation.SemesterStartDate <= ?', $post['SemesterEndDate']));
		}
                
		$lstrSelect->order('HostelReservation.applied_date DESC');
		$results = $this->db->fetchAll($lstrSelect);
                
		if(!empty($results)) {
			foreach($results as $key => $result) {
				if($result['cust_type'] == 1) {
					//query student registration
					$student_q = $this->db->select()
									->from(array('StudentRegistration' => 'tbl_studentregistration'), array("StudentRegistration.registrationId"))
									->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.appl_id =  StudentRegistration.IdApplication' ,array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as student_name", 'StudentProfile.appl_id' ))
									->where($this->db->quoteInto("StudentRegistration.registrationId = ?", $result['IdStudentRegistration']))
								;

					if (isset($post['field2']) && !empty($post['field2'])) {
						$wh_condition = "
								(StudentProfile.appl_fname like '%".$this->db->quote($post['field2'])."%' OR  StudentProfile.appl_mname like '%".$this->db->quote($post['field2'])."%' OR  StudentProfile.appl_lname like '%".$this->db->quote($post['field2'])."%')
							";
						$student_q = $student_q->where($wh_condition);
					}

					if (isset($post['field4']) && !empty($post['field4'])) {
						$student_q = $student_q->where('StudentRegistration.ExtraIdField1 like "%" ? "%"', $post['field4']);
					}

					if (isset($post['field1']) && !empty($post['field1'])) {
						$student_q = $student_q->where("StudentRegistration.IdProgram =?",$post['field1']);
					}


					$cur_student = $this->db->fetchRow($student_q);
					if(empty($cur_student)) {
						//unset($results[$key]);
						continue;
					}
					$results[$key] = array_merge($result, $cur_student);

				} else if($result['cust_type'] == 2) {
					$student_q = $this->db->select()
						->from(array('ApplicantTransaction' => 'applicant_transaction'), array('ApplicantTransaction.at_trans_id', 'ApplicantTransaction.at_pes_id'))
						->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id',
							array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name", 'ApplicantProfile.appl_id' ))
						->where($this->db->quoteInto("ApplicantTransaction.at_trans_id = ?", $result['IdStudentRegistration']))
					;

					if (isset($post['field2']) && !empty($post['field2'])) {
						$wh_condition = "
							(
								(StudentProfile.appl_fname like '%".$this->db->quote($post['field2'])."%' OR  StudentProfile.appl_mname like '%".$this->db->quote($post['field2'])."%' OR  StudentProfile.appl_lname like '%".$this->db->quote($post['field2'])."%')
								OR
								(ApplicantProfile.appl_fname like '%".$this->db->quote($post['field2'])."%' OR  ApplicantProfile.appl_mname like '%".$this->db->quote($post['field2'])."%' OR  ApplicantProfile.appl_lname like '%".$this->db->quote($post['field2'])."%')
							)
							";
						$lstrSelect = $lstrSelect->where($wh_condition);
					}

					$cur_student = $this->db->fetchRow($student_q);


					$results[$key] = array_merge($result, $cur_student);

					if(!is_array($cur_student)) {
						echo $student_q;
						echo "<br>";
					}
				} else if($result['cust_type'] == 3) {
					$cur_student = array( "at_trans_id" => '',
						'at_pes_id' => '',
						'applicant_name' => 'Agent',
						'appl_id' => '');
					$results[$key] = array_merge($result, $cur_student);
				}
			}
		}
                
		return $results;
	}

	//this is used at room registration. therefore we don't have to worry about applicant
	public function fngetStudentRoomReservationDetail($idStudent) {

		$lstrSelect = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array("a.*"))
		->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = a.IdStudentRegistration', array("StudentRegistration.registrationId"))
		->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.appl_id =  StudentRegistration.IdApplication' ,array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as student_name", 'StudentProfile.appl_id' ))
		->joinLeft(array('b' => 'tbl_hostel_room'), "a.IdHostelRoom = b.IdHostelRoom", array('b.RoomCode as Room', 'b.RoomName as RoomName'))
		->joinLeft(array('c' => 'tbl_hostel_room_type'), "b.RoomType = c.IdHostelRoomType", array('c.RoomType as RoomType'))
		->joinLeft(array('d' => 'tbl_hostel_block'), "d.IdHostelBlock = b.Block", array('d.Name as BlockName'))
		->joinLeft(array('e' => 'tbl_hostel_level'), "e.IdHostelLevel = b.Level", array('e.Name as LevelName'))
		->where("a.IdStudentRegistration =?", $idStudent);
		$result = $this->db->fetchAll($lstrSelect);

		return $result;
	}

	public function fnaddReservation($data) {
		$this->insert($data);
	}

	public function fnagetReservationDetail($IdStudentRegistration, $cust_type = 1) {
		$lstrSelect = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array("a.IdHostelRoom AS RoomId", "a.SeatNo","a.Status","a.IdSemester","a.SemesterStartDate","a.SemesterEndDate", "a.*"))
		->joinLeft(array('TenantType' => 'tbl_definationms'), 'a.tenant_type = TenantType.idDefinition', array('TenantType.DefinitionCode AS tenant_type_name'))
		->joinLeft(array('RoomType' => 'tbl_hostel_room_type'), 'a.IdHostelRoomType = RoomType.IdHostelRoomType', array('RoomType.RoomType AS RoomType'))
		//->joinLeft(array('b' => 'tbl_hostel_room_type'), "a.room_type_id = b.IdHostelRoom", array("b.RoomCode"))
		->where("a.IdStudentRegistration = ?", $IdStudentRegistration)
		->where("a.Active =?", 1);
		//echo $lstrSelect;exit;
		//student
		if($cust_type == 1) {
			//for student
			$lstrSelect = $lstrSelect->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = a.IdStudentRegistration', array("StudentRegistration.registrationId"))
			->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.appl_id =  StudentRegistration.IdApplication' ,array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as student_name", 'StudentProfile.appl_id' ))
			;
		} else {
			//for applicant
			$lstrSelect = $lstrSelect->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_trans_id = a.IdStudentRegistration', array( 'ApplicantTransaction.at_pes_id'))
			->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id', 
					array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name", 'ApplicantProfile.appl_id' ))
			;

		}

		$result = $this->db->fetchRow($lstrSelect);
		return $result;
	}

	public function fnagetReservationDetailById($IdHostelReservation) {
		$lstrSelect = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array("a.IdHostelRoom AS RoomId", "a.SeatNo","a.Status","a.IdSemester","a.SemesterStartDate","a.SemesterEndDate", "a.*"))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_trans_id = a.IdStudentRegistration', array())
		->joinLeft(array('StudentProfile' => 'student_profile'),"StudentProfile.appl_id = ApplicantTransaction.at_appl_id",  array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name" ))
		->joinLeft(array('CustomerType' => 'tbl_definationms'), 'a.cust_type = CustomerType.idDefinition', array('CustomerType.DefinitionCode AS customer_type_name'))
		->joinLeft(array('b' => 'tbl_hostel_room'), "a.IdHostelRoom = b.IdHostelRoom", array("b.RoomCode", "b.Block", "b.Level"))
		->where("a.IdHostelReservation =?", $IdHostelReservation)
		->where("a.Active =?", 1);
		$result = $this->db->fetchRow($lstrSelect);
		return $result;
	}

	public function fngetSeatsOccupied() {
		$lstrSelect = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array("CONCAT_WS('_',IFNULL(a.IdHostelRoom,''),IFNULL(a.SeatNo,'')) as seatname"))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_trans_id = a.IdStudentRegistration', array())
		->joinLeft(array('StudentProfile' => 'student_profile'),"StudentProfile.appl_id = ApplicantTransaction.at_appl_id",  array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as studentname" ))		
		->where("a.Active =?", 1);
		$result = $this->db->fetchAll($lstrSelect);
		return $result;
	}
	
	public function getAllReservation($post='')
	{
		$lstrSelect = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array("a.*"))
		->joinLeft(array('b' => 'tbl_hostel_room'), "a.IdHostelRoom = b.IdHostelRoom", array('b.RoomCode as Room', 'b.RoomName as RoomName', ))
		->joinLeft(array('c' => 'tbl_hostel_room_type'), "b.RoomType = c.IdHostelRoomType", array('c.RoomType as RoomType'))
		->joinLeft(array('d' => 'tbl_hostel_block'), "d.IdHostelBlock = b.Block", array('d.Name as BlockName','d.IdHostelBlock as BlockId'))
		->joinLeft(array('e' => 'tbl_hostel_level'), "e.IdHostelLevel = b.Level", array('e.Name as LevelName'))
		->join(array('f' => 'tbl_studentregistration'),'a.IdStudentRegistration = f.IdStudentRegistration',array("f.registrationId","f.CellPhone", "f.Gender","CONCAT_WS('',IFNULL(f.FName,' '),IFNULL(f.MName,' '),IFNULL(f.LName,' ')) as studentname"));
		
		if ( $post['Block'] != '' )
		{
			$lstrSelect->where('b.Block = ?',$post['Block']);
		}
		
		if ( $post['Level'] != '' )
		{
			$lstrSelect->where('b.Level = ?', $post['Level']);
		}
		
		if ( $post['Room'] != '' )
		{
			$lstrSelect->where('b.IdHostelRoom = ?',$post['Room']);
		}
		
		if ( $post['Gender'] != '' )
		{
			$lstrSelect->where('f.Gender = ?',$post['Gender']);
		}
		
		$lstrSelect->order('LevelName ASC');
		
		$result = $this->db->fetchAll($lstrSelect);
		return $result;
	}

	public function fndeleteReservation($IdStudent, $cust_type = 1) {

        if(empty($IdStudent)) {
            return true;
        }

		$table = 'tbl_hostel_reservation';
		$where = 'IdStudentRegistration =' . $IdStudent . ' AND cust_type = ' . $cust_type;
        
		$this->db->delete($table, $where);
	}

	public function markAsProcessed( $IdStudentRegistration) {
		
		$cur_reservation = $this->getActiveReservation($IdStudentRegistration);
		$updated['Status'] = 762;
		$where = "IdHostelReservation = " . $cur_reservation['IdHostelReservation'];
		$this->update($updated, $where);

		return(true);
	}

	public function getActiveReservation($IdStudent, $cust_type = 1, $status = null) {
		$select = $this->db->select()
					->from(array('HostelReservation' => 'tbl_hostel_reservation'))
					->where('HostelReservation.IdStudentRegistration = ?', $IdStudent)
					->where('HostelReservation.cust_type = ?', $cust_type)
					->where('HostelReservation.Status IN (?)', array(265,266));
		if($status != null) {
			$select->where('HostelReservation.Status = ?', $status);
		}

		$reservation = $this->db->fetchRow($select);

		return($reservation);
	}


    
	public function fngetStudentRRDetail($idStudent) {
		$lstrSelect = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array("a.*"))
		->where("a.IdStudentRegistration = ?", $idStudent);

		$result = $this->db->fetchAll($lstrSelect);
		return $result;
	}

	public function fnupdatereservationstatus($post,$IdStudent){

		$table = 'tbl_hostel_reservation';
		$where = 'IdStudentRegistration =' . $IdStudent;
		//check if exists
		$total_affected =$this->update($post,$where);

		return($total_affected);
	}

	public function save(array $data){
		
		if( isset($data[$this->_primary]) ) {
			$where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
			$this->update($data, $where);
			$id = $data[$this->_primary];
		} else {
			$id = $this->insert($data);	
		}

		return $id;
	}

	public function getStudentDetail($IdStudentRegistration, $cust_type = 1) {
		/*$lstrSelect = $this->db->select()
		->from(array('StudentApplication' => 'tbl_studentapplication'), array("CONCAT_WS(' ',IFNULL(StudentApplication.FName,''),IFNULL(StudentApplication.MName,''),IFNULL(StudentApplication.LName,'')) as name","StudentApplication.*"))
		->joinLeft(array('d' => 'tbl_definationms'), 'StudentApplication.Status = d.idDefinition', array('d.DefinitionDesc'))
		->where('StudentApplication.IdStudentRegistration = ?', $IdStudentRegistration );*/

		//student
		if($cust_type == 1) {
			//for student
			$select = $this->db->select()
				->from(array('StudentRegistration' => 'tbl_studentregistration'))
				->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.appl_id =  StudentRegistration.IdApplication' ,array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name", 'StudentProfile.*',  'Gender' => 'StudentProfile.appl_gender' ))
				->joinLeft(array('StatusDef' => 'tbl_definationms'), 'StudentRegistration.Status = StatusDef.idDefinition', array('StatusDef.DefinitionDesc'))
				->joinLeft(array('Religion' => 'tbl_definationms'), 'StudentProfile.appl_religion = Religion.idDefinition', array('Religion' => 'Religion.DefinitionDesc'))
				->where('StudentRegistration.IdStudentRegistration = ?', $IdStudentRegistration)
				;
		} else {
			//for applicant
			$select = $this->db->select()->from(array('ApplicantTransaction' => 'applicant_transaction'))
				->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id', 
					array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name", 'ApplicantProfile.*', 'Gender' => 'ApplicantProfile.appl_gender', 'Religion' => 'appl_religion' ))
				->joinLeft(array('StatusDef' => 'tbl_definationms'), 'ApplicantTransaction.at_status = StatusDef.idDefinition', array('StatusDef.DefinitionDesc'))
				->joinLeft(array('Religion' => 'tbl_definationms'), 'ApplicantProfile.appl_religion = Religion.idDefinition', array('Religion' => 'Religion.DefinitionDesc'))
				->where('ApplicantTransaction.at_trans_id = ?', $IdStudentRegistration)
			;

		}

		$student = $this->db->fetchRow($select);
		
		return($student);
	}

	public function getApplicantDetail($IdTransactionId) {
		$where = $this->db->select()
		->from(array('ApplicantTransaction' => 'applicant_transaction'), array('*'))
		->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantTransaction.at_appl_id = ApplicantProfile.appl_id',array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as name","ApplicantProfile.*"))
		->joinLeft(array('d' => 'tbl_definationms'), 'ApplicantProfile.appl_religion = d.idDefinition', array('d.DefinitionDesc as Religion'))
		->where('ApplicantTransaction.at_trans_id = ?', $IdTransactionId )
		
		;
		$student = $this->db->fetchRow($where);
		return($student);
	}

	public function getReservationsByRoom($IdHostelRoom) {
		$select = $this->db->select()
		->from(array('a' => 'tbl_hostel_reservation'), array( "a.IdHostelRoom AS RoomId", "a.SeatNo","a.Status","a.IdSemester","a.SemesterStartDate","a.SemesterEndDate", "a.*"))
		->joinLeft(array('StudentRegistration' => 'tbl_studentregistration'), 'StudentRegistration.IdStudentRegistration = a.IdStudentRegistration', array('StudentRegistration.registrationId'))
		->joinLeft(array('StudentProfile' => 'student_profile'),"StudentProfile.id = StudentRegistration.sp_id",  array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name", 'StudentProfile.appl_idnumber' ))
		->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'a.IdStudentRegistration = ApplicantTransaction.at_trans_id', array('ApplicantTransaction.at_appl_id', 'ApplicantTransaction.at_pes_id'))
		->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantTransaction.at_appl_id = ApplicantProfile.appl_id', array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as applicant_name"))
		->joinLeft(array('TenantType' => 'tbl_definationms'), 'a.tenant_type = TenantType.idDefinition', array('TenantType.DefinitionCode AS tenant_type_name'))
		->joinLeft(array('b' => 'tbl_hostel_room'), "a.IdHostelRoom = b.IdHostelRoom", array("b.RoomCode", "b.Block", "b.Level"))
		->where('a.IdHostelRoom = ?', $IdHostelRoom)
		->where("a.Active = ?", 1)
		->where("a.Status = ?", 266)
		;

		$result = $this->db->fetchAll($select);
		return $result;
	}

    /*
    public function studentHasReserved($IdStudentRegistration, $date_from, $date_to) {
        $sql = "
            (
                ( HostelReservation.SemesterStartDate BETWEEN '$date_from' AND '$date_to')
                AND ( HostelReservation.SemesterEndDate BETWEEN '$date_from' AND '$date_to')
            )
            OR
            (
                ( HostelReservation.SemesterStartDate >= '$date_from' )
                AND (HostelReservation.SemesterEndDate <= '$date_to' )
            )
            OR
            (
                (HostelReservation.SemesterStartDate <= '$date_from')
                AND (HostelReservation.SemesterEndDate >= '$date_to')
            )
        ";

        //267  - rejected
        //268 - cancel
        $select = $this->db->select()
                ->from(array('HostelReservation' => 'tbl_hostel_reservation'), array("HostelReservation.*"))
                ->where('HostelReservation.cust_type = 1')
                ->where('HostelReservation.Status NOT IN (267,268)')
                ->where($sql);

        $reservation = $this->db->fetchRow($select);

        if(!empty($reservation)) {
            return true;
        } else {
            return false;
        }
    }
    */

    public function hasActiveReservation($IdStudentRegistration, $cust_type) {
        $select = $this->db->select()
            ->from(array('HostelReservation' => 'tbl_hostel_reservation'), array("HostelReservation.*"))
            ->where('HostelReservation.IdStudentRegistration = ?', $IdStudentRegistration)
            ->where('HostelReservation.cust_type = ?', $cust_type)
            ->where('HostelReservation.Status NOT IN (265,266)')
            ;

        $reservation = $this->db->fetchRow($select);

        if(!empty($reservation)) {
            return true;
        } else {
            return false;
        }
    }


}
