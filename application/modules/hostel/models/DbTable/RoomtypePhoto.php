<?php
class Hostel_Model_DbTable_RoomtypePhoto extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_hostel_roomtype_photos';

	public function save(array $data){
		
		if(isset($data['id'])) {
			$where  = $this->getAdapter()->quoteInto('id = ?', $data['id']);
			$this->update($data, $where);
			$id = $data['id'];
		} else {
			$id = $this->insert($data);	
		}

		return $id;
	}

	public function delete_photo($id) {
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);
		return(true);
	}

}