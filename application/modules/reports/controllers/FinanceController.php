<?php
/**
 *  @author suliana 
 */
 
highcharts_init();

class Reports_FinanceController extends Base_Base {
	
	public function init(){
        $this->financemodel = new Reports_Model_DbTable_Finance();
    }

	public function billingAction(){
        $this->view->title = $this->view->translate('Invoice');
        
         $session = Zend_Registry::get('sis');
         
         //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        $reportArray = array();
        $barchart = null;
        $total = array();
		$semesterArray = array();
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->program = $formData['program'];
			 $this->view->semester = isset($formData['semester'])?$formData['semester']:0;
			 
			 
			 $m=0;
			 
			 foreach($formData['semester'] as $semester){
			 	
			 	$searchData = array(
			 		'program'=>$formData['program'],
			 		'semester'=>$semester,
			 		'date_from'=>$formData['date_from'],
			 		'date_to'=>$formData['date_to']
			 	);
			 	
			 	$list = $this->financemodel->getTotalByProgram($searchData);
			 	
			 	$semesterDb = new Registration_Model_DbTable_Semester();
			 	$semesterData = $semesterDb->getData($semester);
			 	
			 	$programData = $programDb->getProgram($formData['program']);
			 	$totalAmount = isset($list['totalAmount'])?round($list['totalAmount'],2):0;
				$semesterArray[]=$semesterData['SemesterMainName'];
				$total[] = $totalAmount;	
				$reportArray[$m]['semester']=$semesterData['SemesterMainName'];
				$reportArray[$m]['amount']=$totalAmount;
                $m++;
	            
			 }
	        
	        }
	        
	        //bar part
		        $series1 = new HighRollerSeriesData();
		        $series1->addName('Total Amount (MYR)')->addData($total);
//		        $series2 = new HighRollerSeriesData();
//		        $series2->addName('aa')->addData($total);
		
		        $barchart = new HighRollerBarChart();
		        $barchart->chart->renderTo = 'chart_invoice';
		        $barchart->title->text = '';
		        $barchart->credits['enabled'] = false;
		        $barchart->xAxis['categories'] = $semesterArray;
		        $barchart->yAxis['title']['text'] = '';
		        
		        
		        $barchart->addSeries($series1); 
//		        $barchart->addSeries($series2);
		
		        $this->view->chart = $barchart;
		        $this->view->data = $reportArray;
    }
}
?>