<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

highcharts_init();

class Reports_QuitStudentController extends Zend_Controller_Action{

    public function init(){
        $this->model = new Reports_Model_DbTable_QuitStudent();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Total Number of Student Quits');
        
        $list = $this->getStudentList();
        $alltotal = intval(count($list));
        
        if ($list){
            foreach ($list as $key=>$loop){
                $semesterstatus = $this->model->getLastSemesterStatus($loop['IdStudentRegistration']);
                $list[$key]['semesterstatus'] = $semesterstatus;
            }
        }
        
        $quitList = array();
        $quitDeceaseList = array();
        $quitOthersList = array();
        $a = 0;
        $b = 0;
        $c = 0;
        if ($list){
            foreach ($list as $key=>$loop){
                if ($loop['semesterstatus']['studentsemesterstatus']==835){
                    $quitList[$a]=$loop;
                    $a++;
                }else if($loop['semesterstatus']['studentsemesterstatus']==836){
                    $quitDeceaseList[$a]=$loop;
                    $b++;
                }else{
                    //var_dump($loop);
                    $quitOthersList[$a]=$loop;
                    $c++;
                }
            }
        }
        
        $semstatusarr = array(
            array('id'=>835), //quit
            array('id'=>836), //quit deceased
        );
        
        $sum = 0;
        if ($semstatusarr){
            foreach ($semstatusarr as $key => $semstatusloop){
                $definationInfo = $this->model->getDefination($semstatusloop['id']);
                
                if ($semstatusloop['id']==835){
                    $total = count($quitList);
                }else if($semstatusloop['id']==836){
                    $total = count($quitDeceaseList);
                }
                
                $semstatusarr[$key]['status'] = $definationInfo['DefinitionDesc'];
                $semstatusarr[$key]['total'] = $total;
                
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array($definationInfo['DefinitionDesc'], $percent);

                $sum = $sum+$total;
            }
        }
        
        $this->view->semstatusarr = $semstatusarr;
        $this->view->sum = $sum;

        $series1 = new HighRollerSeriesData();
        $series1->addName('Students')->addData($chartData);

        $piechart = new HighRollerPieChart();
        $piechart->chart->renderTo = 'chart_applicant_quit';
        $piechart->title->text = '';
        $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
        $piechart->credits['enabled'] = false;
        $piechart->addSeries($series1); 

        $this->view->chart = $piechart;
    }
    
    public function reasonAction(){
        $this->view->title = $this->view->translate('Reason');
        
        $semstatusid = $this->_getParam('semstatus', 0);
        $this->view->semstatusid = $semstatusid;
        
        $semStatusInfo = $this->model->getDefination($semstatusid);
        $this->view->semStatusInfo = $semStatusInfo;
        
        if ($semstatusid == 835){ //quit
            $resontypeid = 170;
        }else if($semstatusid == 836){ //quit deceased
            $resontypeid = 171;
        }
        
        $reasonList = $this->model->getDefinationByType($resontypeid);
        
        $reasonarr = array(
            'idDefinition' => '99',
            'idDefType' => $resontypeid,
            'DefinitionCode' => 'Others',
            'DefinitionDesc' => 'Others',
            'Status' => '1',
            'BahasaIndonesia' => 'Others',
            'Description' => 'Others',
            'BhasaIndonesia' => 'Others',
            'defOrder' => null
        );
        array_push($reasonList, $reasonarr);
        
        $list = $this->getStudentList();
        
        if ($list){
            foreach ($list as $key => $loop){
                if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid){
                    unset($list[$key]);
                }
            }
        }
        
        $alltotal = intval(count($list));
        
        $sum = 0;
        if ($reasonList){
            foreach ($reasonList as $key2 => $reasonloop){
                
                $total = 0;
                if ($list){
                    foreach ($list as $key3 => $loop){
                        if ($loop['semesterstatus']['appreson']==$reasonloop['idDefinition']){
                            $total++;
                        }
                    }
                }
                
                $reasonList[$key2]['total'] = $total;

                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array($reasonloop['DefinitionDesc'], $percent);

                $sum = $sum+$total;
            }
        }
        
        $this->view->reasonList = $reasonList;
        $this->view->sum = $sum;

        $series1 = new HighRollerSeriesData();
        $series1->addName('Students')->addData($chartData);

        $piechart = new HighRollerPieChart();
        $piechart->chart->renderTo = 'chart_applicant_reason';
        $piechart->title->text = '';
        $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
        $piechart->credits['enabled'] = false;
        $piechart->addSeries($series1); 

        $this->view->chart = $piechart;
    }
    
    public function programmeAction(){
        $this->view->title = $this->view->translate('Programme');
        
        $semstatusid = $this->_getParam('semstatus', 0);
        $reasonid = $this->_getParam('reason', 0);
        $this->view->semstatusid = $semstatusid;
        $this->view->reasonid = $reasonid;
        
        $semStatusInfo = $this->model->getDefination($semstatusid);
        if ($reasonid == 99){
            $reasonInfo = array(
                'idDefinition' => '99',
                //'idDefType' => $resontypeid,
                'DefinitionCode' => 'Others',
                'DefinitionDesc' => 'Others',
                'Status' => '1',
                'BahasaIndonesia' => 'Others',
                'Description' => 'Others',
                'BhasaIndonesia' => 'Others',
                'defOrder' => null
            );
        }else{
            $reasonInfo = $this->model->getDefination($reasonid);
        }
        
        $this->view->semStatusInfo = $semStatusInfo;
        $this->view->reasonInfo = $reasonInfo;
        
        $list = $this->getStudentList();
        
        if ($list){
            foreach ($list as $key => $loop){
                if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid || $loop['semesterstatus']['appreson']!=$reasonid){
                    unset($list[$key]);
                }
            }
        }
        
        $alltotal = intval(count($list));
        
        $programList = $this->model->getProgramList();
        
        $sum = 0;
        $total = 0;
        if ($programList){
            foreach ($programList as $key2 => $programLoop){
                $filter = array(
                    'IdProgram'=>$programLoop['IdProgram']
                );
             
                $appList = $this->getStudentList($filter);
                
                if ($appList){
                    foreach ($appList as $key3 => $loop){
                        if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid || $loop['semesterstatus']['appreson']!=$reasonid){
                            unset($appList[$key3]);
                        }
                    }
                }
                
                $total = intval(count($appList));
                $programList[$key2]['total'] = $total;
                
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array($programLoop['ProgramCode'], $percent);

                $sum = $sum+$total;
            }
        }
        
        $this->view->programList = $programList;
        $this->view->sum = $sum;

        $series1 = new HighRollerSeriesData();
        $series1->addName('Students')->addData($chartData);

        $piechart = new HighRollerPieChart();
        $piechart->chart->renderTo = 'chart_applicant_program';
        $piechart->title->text = '';
        $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
        $piechart->credits['enabled'] = false;
        $piechart->addSeries($series1); 

        $this->view->chart = $piechart;
    }
    
    public function programmeSchemeAction(){
        $this->view->title = $this->view->translate('Programme Scheme');
        
        $semstatusid = $this->_getParam('semstatus', 0);
        $reasonid = $this->_getParam('reason', 0);
        $programid = $this->_getParam('program', 0);
        $this->view->semstatusid = $semstatusid;
        $this->view->reasonid = $reasonid;
        $this->view->programid = $programid;
        
        $semStatusInfo = $this->model->getDefination($semstatusid);
        if ($reasonid == 99){
            $reasonInfo = array(
                'idDefinition' => '99',
                //'idDefType' => $resontypeid,
                'DefinitionCode' => 'Others',
                'DefinitionDesc' => 'Others',
                'Status' => '1',
                'BahasaIndonesia' => 'Others',
                'Description' => 'Others',
                'BhasaIndonesia' => 'Others',
                'defOrder' => null
            );
        }else{
            $reasonInfo = $this->model->getDefination($reasonid);
        }
        
        $programInfo = $this->model->getProgramById($programid);
        
        $this->view->semStatusInfo = $semStatusInfo;
        $this->view->reasonInfo = $reasonInfo;
        $this->view->programInfo = $programInfo;
        
        $filter = array(
            'IdProgram'=>$programid
        );
        
        $list = $this->getStudentList($filter);
        
        if ($list){
            foreach ($list as $key => $loop){
                if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid || $loop['semesterstatus']['appreson']!=$reasonid){
                    unset($list[$key]);
                }
            }
        }
        
        $alltotal = intval(count($list));
        
        $programSchemeList = $this->model->getProgramSchemeList($programid);
            
        $sum = 0;
        //$total = 0;
        if ($programSchemeList){
            foreach ($programSchemeList as $key2 => $programSchemeLoop){
                $filter = array(
                    'IdProgram'=>$programid,
                    'IdProgramScheme'=>$programSchemeLoop['IdProgramScheme']
                );
             
                $appList = $this->getStudentList($filter);
                
                if ($appList){
                    foreach ($appList as $key3 => $loop){
                        if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid || $loop['semesterstatus']['appreson']!=$reasonid){
                            unset($appList[$key3]);
                        }
                    }
                }
                
                $total = intval(count($appList));
                $programSchemeList[$key2]['total'] = $total;
                
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array($programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt'], $percent);

                $sum = $sum+$total;
            }
        }
        
        $this->view->programSchemeList = $programSchemeList;
        $this->view->sum = $sum;

        $series1 = new HighRollerSeriesData();
        $series1->addName('Students')->addData($chartData);

        $piechart = new HighRollerPieChart();
        $piechart->chart->renderTo = 'chart_applicant_programscheme';
        $piechart->title->text = '';
        $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
        $piechart->credits['enabled'] = false;
        $piechart->addSeries($series1); 

        $this->view->chart = $piechart;
    }
    
    public function intakeAction(){
        $this->view->title = $this->view->translate('Intake');
        
        $semstatusid = $this->_getParam('semstatus', 0);
        $reasonid = $this->_getParam('reason', 0);
        $programid = $this->_getParam('program', 0);
        $programschemeid = $this->_getParam('programscheme', 0);
        $this->view->semstatusid = $semstatusid;
        $this->view->reasonid = $reasonid;
        $this->view->programid = $programid;
        $this->view->programschemeid = $programschemeid;
        
        $semStatusInfo = $this->model->getDefination($semstatusid);
        if ($reasonid == 99){
            $reasonInfo = array(
                'idDefinition' => '99',
                //'idDefType' => $resontypeid,
                'DefinitionCode' => 'Others',
                'DefinitionDesc' => 'Others',
                'Status' => '1',
                'BahasaIndonesia' => 'Others',
                'Description' => 'Others',
                'BhasaIndonesia' => 'Others',
                'defOrder' => null
            );
        }else{
            $reasonInfo = $this->model->getDefination($reasonid);
        }
        
        $programInfo = $this->model->getProgramById($programid);
        $programSchemeInfo = $this->model->getProgramSchemeById($programschemeid);
        
        $this->view->semStatusInfo = $semStatusInfo;
        $this->view->reasonInfo = $reasonInfo;
        $this->view->programInfo = $programInfo;
        $this->view->programSchemeInfo = $programSchemeInfo;
        
        $filter = array(
            'IdProgram'=>$programid,
            'IdProgramScheme'=>$programschemeid
        );
        
        $list = $this->getStudentList($filter);
        
        if ($list){
            foreach ($list as $key => $loop){
                if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid || $loop['semesterstatus']['appreson']!=$reasonid){
                    unset($list[$key]);
                }
            }
        }
        
        $alltotal = intval(count($list));
        
        $intakeList = $this->model->getIntakeList();
        $sum = 0;
        if ($intakeList){
            foreach ($intakeList as $key2 => $intakeLoop){
                $filter = array(
                    'IdProgram'=>$programid,
                    'IdProgramScheme'=>$programschemeid,
                    'IdIntake'=>$intakeLoop['IdIntake']
                );
                
                $appList = $this->getStudentList($filter);
                
                if ($appList){
                    foreach ($appList as $key3 => $loop){
                        if ($loop['semesterstatus']['studentsemesterstatus']!=$semstatusid || $loop['semesterstatus']['appreson']!=$reasonid){
                            unset($appList[$key3]);
                        }
                    }
                }
                
                $intakeList[$key2]['total'] = intval(count($appList));

                $intakeid[] = $intakeLoop['IdIntake'];
                $getintake[] = $intakeLoop['IntakeDesc'];
                $totals[] = intval(count($appList));
                $sum = $sum+intval(count($appList));
            }
        }
        
        $this->view->intakeList = $intakeList;
        $this->view->sum = $sum;

        //graph part
        $chartData = $totals;
        $series1 = new HighRollerSeriesData();
        $series1->addName('Students')->addData($chartData);

        $linechart = new HighRollerAreaChart();
        $linechart->chart->renderTo = 'chart_applicant_intake';
        $linechart->title->text = '';
        $linechart->credits['enabled'] = false;
        $linechart->xAxis['categories'] = $getintake;
        $linechart->yAxis['title']['text'] = '';
        $linechart->addSeries($series1); 

        $this->view->chart = $linechart;
    }
    
    /*
     * array $filter
     * 
     */
    public function getStudentList($filter = false){
        $list = $this->model->getStudentQuitByReason($filter);
        
        if ($list){
            foreach ($list as $key=>$loop){
                $semesterstatus = $this->model->getLastSemesterStatus($loop['IdStudentRegistration']);
                $list[$key]['semesterstatus'] = $semesterstatus;
            }
        }
        
        return $list;
    }
}
?>