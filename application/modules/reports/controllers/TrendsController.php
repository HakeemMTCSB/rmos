<?php
highcharts_init();

class Reports_TrendsController extends Zend_Controller_Action
{

    public function init()
    {
        $this->statsDb = new Reports_Model_DbTable_Stats();
    }

    public function indexAction()
    {

		$this->view->title = 'Trends';
	}

	public function applicantAction()
    {
		$this->view->title = 'Applicant';	

		//get programmes
		$progDb = new GeneralSetup_Model_DbTable_Program();
		$programmes = $progDb->fngetProgramDetails();
		
		$data = $dataProg = array();
		$alltotal = 0;

		foreach ( $programmes as $prog )
		{
			$total = $this->statsDb->getApplicantsByProgramme($prog['IdProgram']);

			if ( $total > 0 )
			{
				$alltotal += $total;
				$dataProg[ $prog['IdProgram'] ] = $total;
			}
		}

		foreach ( $programmes as $prog )
		{
			if ( isset($dataProg[ $prog['IdProgram'] ]) ) 
			{
				$total = $dataProg[ $prog['IdProgram'] ];
				$percent = round( ($total / $alltotal * 100), 2);
				$chartData[] = array($prog['ProgramCode'], $percent);
			}
		}

		$series4 = new HighRollerSeriesData();
		$series4->addName('Students')->addData($chartData);

		$linechart4 = new HighRollerPieChart();
		$linechart4->chart->renderTo = 'chart';
		$linechart4->title->text = '';
		$linechart4->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
		$linechart4->credits['enabled'] = false;
		$linechart4->addSeries($series4); 
		
		//3d
		$linechart4->chart->options3d = array('enabled' => true, 'alpha' => 45, 'beta' => 0 );
		$linechart4->plotOptions['pie']['depth'] = 35;

		$this->view->chart = $linechart4;
		
		
		//data
		$this->view->programmes = $programmes;
		$this->view->dataProg = $dataProg;
    }

	public function studentsAction()
    {
		$this->view->title = 'Students';	

		//get programmes
		$progDb = new GeneralSetup_Model_DbTable_Program();
		$programmes = $progDb->fngetProgramDetails();
		
		$data = $dataProg = array();
		$grandtotal = 0;

		foreach ( $programmes as $prog )
		{
			$total = $this->statsDb->getStudentsByProgramme($prog['IdProgram']);

			if ( $total > 0 )
			{
				$dataProg[ $prog['IdProgram'] ] = $total;

				$grandtotal += $total;
			}
		}
		
		foreach ( $programmes as $prog )
		{
			if ( isset($dataProg[ $prog['IdProgram'] ]) ) 
			{
				$total = $dataProg[ $prog['IdProgram'] ];
				$percent = round( ($total / $grandtotal * 100), 2);
				$chartData[] = array($prog['ProgramCode'], $percent);
			}
		}

		$series4 = new HighRollerSeriesData();
		$series4->addName('Students')->addData($chartData);

		$linechart4 = new HighRollerPieChart();
		$linechart4->chart->renderTo = 'chart';
		$linechart4->title->text = '';
		$linechart4->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
		$linechart4->credits['enabled'] = false;
		$linechart4->addSeries($series4); 
		
		//3d
		$linechart4->chart->options3d = array('enabled' => true, 'alpha' => 45, 'beta' => 0 );
		$linechart4->plotOptions['pie']['depth'] = 35;

		$this->view->chart = $linechart4;
		
		//data
		$this->view->programmes = $programmes;
		$this->view->dataProg = $dataProg;
		$this->view->grandtotal = $grandtotal;
    }

}

