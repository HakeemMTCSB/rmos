<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

highcharts_init();

class Reports_TerminatedStudentController extends Zend_Controller_Action{

    public function init(){
        $this->model = new Reports_Model_DbTable_TerminatedStudent();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Total Number of Terminated Students');
        
        $list = $this->getStudentList();
        $alltotal = intval(count($list));
        
        $category = $this->model->getTerminateCategory();
        
        $sum = 0;
        if ($category){
            foreach ($category as $key => $categoryLoop){
                $appList = $this->getStudentList();
                
                if ($appList){
                    foreach ($appList as $key2 => $appLoop){
                        if ($appLoop['semesterstatus']['studentsemesterstatus']!=$categoryLoop['idDefinition']){
                            unset($appList[$key2]);
                        }
                    }
                }
                
                $total = intval(count($appList));
                
                $category[$key]['total'] = $total;
                
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array($categoryLoop['DefinitionDesc'], $percent);

                $sum = $sum+$total;
            }
        }
        
        $this->view->category = $category;
        $this->view->sum = $sum;

        $series1 = new HighRollerSeriesData();
        $series1->addName('Students')->addData($chartData);

        $piechart = new HighRollerPieChart();
        $piechart->chart->renderTo = 'chart_applicant_terminate';
        $piechart->title->text = '';
        $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
        $piechart->credits['enabled'] = false;
        $piechart->addSeries($series1); 

        $this->view->chart = $piechart;
    }
    
    public function programmeAction(){
        $this->view->title = $this->view->translate('Programme');
        
        $categoryid = $this->_getParam('category', 0);
        $this->view->categoryid = $categoryid;
        
        $categoryInfo = $this->model->getDefination($categoryid);
        $this->view->categoryInfo = $categoryInfo;
        
        $list = $this->getStudentList();
        
        if ($list){
            foreach ($list as $key => $loop){
                if ($loop['semesterstatus']['studentsemesterstatus']!=$categoryid){
                    unset($list[$key]);
                }
            }
        }
        
        $alltotal = intval(count($list));
        
        $programList = $this->model->getProgramList();
        
        $sum = 0;
        if ($programList){
            foreach ($programList as $key2=>$programLoop){
                $filter = array(
                    'IdProgram'=>$programLoop['IdProgram']
                );
                
                $appList = $this->getStudentList($filter);
                
                if ($appList){
                    foreach ($appList as $key3 => $appLoop){
                        if ($appLoop['semesterstatus']['studentsemesterstatus']!=$categoryid){
                            unset($appList[$key3]);
                        }
                    }
                }
                
                $programList[$key2]['total'] = intval(count($appList));
                $total = intval(count($appList));
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array($programLoop['ProgramCode'], $percent);

                $sum = $sum+intval(count($appList));
            }
            
            $this->view->programList = $programList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_program';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }
    }
    
    /*
     * array $filter
     * 
     */
    public function getStudentList($filter = false){
        $list = $this->model->getTerminateStudentList($filter);
        
        if ($list){
            foreach ($list as $key=>$loop){
                $semesterstatus = $this->model->getLastSemesterStatus($loop['IdStudentRegistration']);
                $list[$key]['semesterstatus'] = $semesterstatus;
            }
        }
        
        return $list;
    }
}
?>