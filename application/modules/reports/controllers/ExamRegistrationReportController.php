<?php
class Reports_ExamRegistrationReportController extends Base_Base 
{

	public function overallReportAction(){
		
		 set_time_limit(0);
        ini_set('memory_limit', '-1');
			
        $this->view->title = "Exam Registration Report";
        
        $session = Zend_Registry::get('sis');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Reports_Form_SearchStudentReport();
        $reportExamRegDB = new Reports_Model_DbTable_ExamRegistrationReport();
        $semesterDB = new Registration_Model_DbTable_Semester();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			// print_r($formData);exit;
             if($form->isValid($formData))	{
				 
				$form->populate($formData);
				//$this->view->IdProgramScheme = $formData['IdProgramScheme'];
				
				//get semeester
				$semester = $semesterDB->getData($formData['IdSemester']);
				$this->view->semester = $semester;
				
				//get previous semester
				$prevSemester=$semesterDB->getPreviousSemester($semester);
		
				//get subject
				$subject_list = $reportExamRegDB->getLandscapeSubject($formData['IdProgram'],$formData['IdSemester'],null,$formData);
				$this->view->subject_list = $subject_list;
														
				//get student
			    $student_list = $reportExamRegDB->getStudentExamRegistration($formData,$subject_list,$prevSemester);
			    $this->view->student = $student_list;
			    				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));				
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));				
				$form->populate($formData);
				$this->view->paginator = $paginator;
				
			}
        }
        $this->view->form = $form;
                
    }
    
    
    public function printReportAction(){
    	$this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()){
        	
        	    $reportExamRegDB = new Reports_Model_DbTable_ExamRegistrationReport();
       		    $semesterDB = new Registration_Model_DbTable_Semester();
      
            	$formData = $this->getRequest()->getPost();
                        	
            	//get semeester
				$semester = $semesterDB->getData($formData['IdSemester']);
				$this->view->semester = $semester;
				
				//get previous semester
				$prevSemester=$semesterDB->getPreviousSemester($semester);
		
				//get subject
				$subject_list = $reportExamRegDB->getLandscapeSubject($formData['IdProgram'],$formData['IdSemester'],null,$formData);
				$this->view->subject_list = $subject_list;
				 
				//get student
			    $student_list = $reportExamRegDB->getStudentExamRegistration($formData,$subject_list,$prevSemester);
			    $this->view->paginator = $student_list;
			    
        }
        $this->view->filename = date('Ymd').'_exam_registration.xls';
    }
    
    
   
	
}

?>