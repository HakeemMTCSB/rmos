<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

highcharts_init();

class Reports_TotalEnrollmentController extends Zend_Controller_Action{

    public function init(){
        $this->model = new Reports_Model_DbTable_TotalEnrollment();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Total Enrollment');
        
        $intakeList = $this->model->getIntakeList();
        $sum = 0;
        if ($intakeList){
            foreach ($intakeList as $key => $intakeLoop){
                $list = $this->model->getApplicantByIntake($intakeLoop['IdIntake']);
                $intakeList[$key]['total'] = intval(count($list));
                
                $intakeid[] = $intakeLoop['IdIntake'];
                $getintake[] = $intakeLoop['IntakeDesc'];
		$totals[] = intval(count($list));
                $sum = $sum+intval(count($list));
            }
        }
        
        if (count($intakeid) > 0){
            $list2 = $this->model->getApplicantByIntakeOther($intakeid);
            $getintake[] = 'Others';
            $totals[] = intval(count($list2));
            $sum = $sum+intval(count($list2));
            
            $data = array(
                'IdIntake' => 0,
                'total' => intval(count($list2)),
                'IntakeDesc' => 'Others'
            );
            
            array_push($intakeList, $data);
        }
        
        $this->view->intakeList = $intakeList;
        $this->view->sum = $sum;
        
        //graph part
        $chartData = $totals;
        $series1 = new HighRollerSeriesData();
        $series1->addName('Applicants')->addData($chartData);

        $linechart = new HighRollerAreaChart();
        $linechart->chart->renderTo = 'chart_applicant_intake';
        $linechart->title->text = '';
        $linechart->credits['enabled'] = false;
        $linechart->xAxis['categories'] = $getintake;
        $linechart->yAxis['title']['text'] = '';
        $linechart->addSeries($series1); 

        $this->view->chart = $linechart;
    }
    
    public function programmeAction(){
        $this->view->title = $this->view->translate('Programme');
        
        $intakeid = $this->_getParam('intake', 0);
        $this->view->intakeid = $intakeid;
        
        if ($intakeid != 0){
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $this->view->intakeInfo = $intakeinfo;
            
            $programList = $this->model->getProgramList();
            $list = $this->model->getApplicantByStatusAndIntake($intakeid);
            $alltotal = intval(count($list));
            
            $sum = 0;
            
            if ($programList){
                foreach ($programList as $key=>$programLoop){
                    $appList = $this->model->getAppByIntakeStatusProg($intakeid, $programLoop['IdProgram']);
                
                    $programList[$key]['total'] = intval(count($appList));
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($programLoop['ProgramCode'], $percent);

                    $sum = $sum+intval(count($appList));
                    
                    $programId[] = $programLoop['IdProgram'];
                }
            }
            
            if (count($programId) > 0){
                $total = $alltotal-$sum;
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array('Others', $percent);
                
                $sum = $sum+$total;

                $data = array(
                    'IdProgram' => 0,
                    'total' => $total,
                    'ProgramName' => 'Others'
                );

                array_push($programList, $data);
            }
            
            $this->view->programList = $programList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_program';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }
    }
    
    public function programmeSchemeAction(){
        $this->view->title = $this->view->translate('Programme Scheme');
        
        $intakeid = $this->_getParam('intake', 0);
        $programid = $this->_getParam('program', 0);
        $this->view->intakeid = $intakeid;
        $this->view->programid = $programid;
        
        if ($intakeid != 0 && $programid != 0){
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $programInfo = $this->model->getProgramById($programid);
            $this->view->intakeInfo = $intakeinfo;
            $this->view->programInfo = $programInfo;
            
            $list = $this->model->getAppByIntakeStatusProg($intakeid, $programid);
            $alltotal = intval(count($list));
            
            $programSchemeList = $this->model->getProgramSchemeList($programid);
            
            $sum = 0;
            
            if ($programSchemeList){
                foreach ($programSchemeList as $key=>$programSchemeLoop){
                    $appList = $this->model->getAppByIntakeStatusProgScheme($intakeid, $programid, $programSchemeLoop['IdProgramScheme']);
                
                    $programSchemeList[$key]['total'] = intval(count($appList));
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt'], $percent);

                    $sum = $sum+intval(count($appList));
                }
            }
            
            $this->view->programSchemeList = $programSchemeList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_programscheme';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }
    }
    
    public function nationalityAction(){
        $this->view->title = $this->view->translate('Nationality');
        
        $intakeid = $this->_getParam('intake', 0);
        $programid = $this->_getParam('program', 0);
        $programschemeid = $this->_getParam('programscheme', 0);
        $this->view->intakeid = $intakeid;
        $this->view->programid = $programid;
        $this->view->programschemeid = $programschemeid;
        
        if ($intakeid != 0 && $programid != 0 && $programschemeid != 0){
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $programInfo = $this->model->getProgramById($programid);
            $programSchemeInfo = $this->model->getProgramSchemeById($programschemeid);
            $this->view->intakeInfo = $intakeinfo;
            $this->view->programInfo = $programInfo;
            $this->view->programSchemeInfo = $programSchemeInfo;
            
            $list = $this->model->getAppByIntakeStatusProgScheme($intakeid, $programid, $programschemeid);
            $alltotal = intval(count($list));
            
            $nationalityList = $this->model->getNationalityList();

            $sum = 0;
            if ($nationalityList){
                foreach ($nationalityList as $key => $nationalityLoop){
                    $appList = $this->model->getApplicantByIntakeProgSchemeNation($intakeid, $programid, $programschemeid, $nationalityLoop['idCountry']);
                    $nationalityList[$key]['total'] = intval(count($appList));

                    $nationid[] = $nationalityLoop['idCountry'];
                    $getnation[] = $nationalityLoop['CountryName'];
                    $totals[] = intval(count($appList));
                    $sum = $sum+intval(count($appList));
                }
            }
            
            $this->view->nationalityList = $nationalityList;
            $this->view->sum = $sum;
            
            $chartData = $totals;
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);
            
            $barchart = new HighRollerBarChart();
            $barchart->chart->renderTo = 'chart_applicant_nation';
            $barchart->title->text = '';
            $barchart->credits['enabled'] = false;
            $barchart->xAxis['categories'] = $getnation;
            $barchart->yAxis['title']['text'] = '';
            $barchart->addSeries($series1); 

            $this->view->chart = $barchart;
        }
    }
    
    public function genderAction(){
        $this->view->title = $this->view->translate('Gender');
        
        $intakeid = $this->_getParam('intake', 0);
        $programid = $this->_getParam('program', 0);
        $programschemeid = $this->_getParam('programscheme', 0);
        $nationid = $this->_getParam('nation', 0);
        $this->view->intakeid = $intakeid;
        $this->view->programid = $programid;
        $this->view->programschemeid = $programschemeid;
        $this->view->nationid = $nationid;
        
        if ($intakeid != 0 && $programid != 0 && $programschemeid != 0 && $nationid != 0){
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $programInfo = $this->model->getProgramById($programid);
            $programSchemeInfo = $this->model->getProgramSchemeById($programschemeid);
            $nationalityInfo = $this->model->getNationalityById($nationid);
            $this->view->intakeInfo = $intakeinfo;
            $this->view->programInfo = $programInfo;
            $this->view->programSchemeInfo = $programSchemeInfo;
            $this->view->nationalityInfo = $nationalityInfo;
            
            $list = $this->model->getApplicantByIntakeProgSchemeNation($intakeid, $programid, $programschemeid, $nationid);
            $alltotal = intval(count($list));
            
            $genderList = array(
                array('id'=>'1'), //male
                array('id'=>'2'), //female
            );
            
            $sum = 0;
            if ($genderList){
                foreach ($genderList as $key=>$genderLoop){
                    $appList = $this->model->getApplicantByIntakeProgSchemeNationGender($intakeid, $programid, $programschemeid, $nationid, $genderLoop['id']);
                
                    if ($genderLoop['id'] == 1){
                        $gender = 'Male';
                    }else{
                        $gender = 'Female';
                    }
                    
                    $genderList[$key]['total'] = intval(count($appList));
                    $genderList[$key]['gender'] = $gender;
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($gender, $percent);

                    $sum = $sum+intval(count($appList));
                }
            }
            
            $this->view->genderList = $genderList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_gender';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }
    }
}