<?php

class Reports_ExamReportsController extends Base_Base 
{
	function init()
	{
		$this->tplfolder = defined('TEMPLATE_PATH') ? TEMPLATE_PATH : DOCUMENT_PATH;
	}

	public function examScheduleAction(){
		
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
			
        $this->view->title = "Exam Schedule Report";
        
        $session = Zend_Registry::get('sis');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
    	$form = new Reports_Form_SearchExamSchedule();
        $examScheduleDB = new Reports_Model_DbTable_ExamSchedule();
        $semesterDB = new Registration_Model_DbTable_Semester();
        $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 			
             if($form->isValid($formData))	{
				 
				$form->populate($formData);
									
				//get semeester
				$semester = $semesterDB->getData($formData['IdSemester']);
				$this->view->semester = $semester;
				
				//get info exam center
				$this->view->exam_center = $examCenterDB->getDatabyId($formData['ec_id']);
				
				//get list date exam 
				$date_exam = $examScheduleDB->getExamDate($formData);
				 //var_dump($this->view->date_exam[0]['info']); //exit;
				
				$time_exam = $examScheduleDB->getExamTimeList($formData);
				 //var_dump($this->view->time_exam);

				 if ($time_exam){
					 foreach ($time_exam as $key2 => $time_examLoop){
						 $column[$key2]=false;
					 }
				 }

				 if ($date_exam){
					 foreach ($date_exam as $key1 => $date_examLoop){
						if (isset($date_examLoop['info']) && count($date_examLoop['info']) > 0){

							foreach ($date_examLoop['info'] as $key3 => $date_examLoop2){

								//var_dump($column[$key3]);
								$totalstudent = 0;
								if (count($date_examLoop2['paper']) > 0 && $column[$key3] == false){
									foreach ($date_examLoop2['paper'] as $date_examLoop3){
										$totalstudent = $totalstudent + $date_examLoop3['total_student'];
									}

									if ($totalstudent > 0) {
										$column[$key3] = true;
									}else{
										//unset($date_exam[$key1]['info'][$key3]);
									}
								}else{
									//unset($date_exam[$key1]['info'][$key3]);
								}

								$test[$key3] = $column[$key3];
								//echo $column;
							}
						}
					 }
				 }

				 if ($test){
					 foreach ($test as $testkey => $testLoop){
						 if ($testLoop == false){
							 unset($time_exam[$testkey]);
						 }
					 }
				 }

				 if ($date_exam){
					 foreach ($date_exam as $key1 => $date_examLoop){
						 if (isset($date_examLoop['info']) && count($date_examLoop['info']) > 0){

							 foreach ($date_examLoop['info'] as $key3 => $date_examLoop2){
								if ($test[$key3] == false){
									unset($date_exam[$key1]['info'][$key3]);
								}

							 }
						 }
					 }
				 }

				 $this->view->date_exam = $date_exam;
				 $this->view->time_exam = $time_exam;
				
				//get student list
				$student = $examScheduleDB->getStudentByExamCenter($formData);
				$this->view->student_list = $student;
				
			}
        }
        $this->view->form = $form;        
	}
	
	
	public function printExamScheduleAction(){
		
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
	      
        if ($this->getRequest()->isPost()){
        	
        	 	$formData = $this->getRequest()->getPost();
        	 
        	 
        	 	$examScheduleDB = new Reports_Model_DbTable_ExamSchedule();
        		$semesterDB = new Registration_Model_DbTable_Semester();
        		$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
       
        	 	//get semeester
				$semester = $semesterDB->getData($formData['IdSemester']);
					
				//get info exam center
				$exam_center = $examCenterDB->getDatabyId($formData['ec_id']);
				
				//get list date exam 
				$date_exam_de = $examScheduleDB->getExamDate($formData);
				
				$time_exam_te = $examScheduleDB->getExamTimeList($formData);

			if ($time_exam_te){
				foreach ($time_exam_te as $key2 => $time_examLoop){
					$column[$key2]=false;
				}
			}

			if ($date_exam_de){
				foreach ($date_exam_de as $key1 => $date_examLoop){
					if (isset($date_examLoop['info']) && count($date_examLoop['info']) > 0){

						foreach ($date_examLoop['info'] as $key3 => $date_examLoop2){

							//var_dump($column[$key3]);
							$totalstudent = 0;
							if (count($date_examLoop2['paper']) > 0 && $column[$key3] == false){
								foreach ($date_examLoop2['paper'] as $date_examLoop3){
									$totalstudent = $totalstudent + $date_examLoop3['total_student'];
								}

								if ($totalstudent > 0) {
									$column[$key3] = true;
								}else{
									//unset($date_exam[$key1]['info'][$key3]);
								}
							}else{
								//unset($date_exam[$key1]['info'][$key3]);
							}

							$test[$key3] = $column[$key3];
							//echo $column;
						}
					}
				}
			}

			if ($test){
				foreach ($test as $testkey => $testLoop){
					if ($testLoop == false){
						unset($time_exam_te[$testkey]);
					}
				}
			}

			if ($date_exam_de){
				foreach ($date_exam_de as $key1 => $date_examLoop){
					if (isset($date_examLoop['info']) && count($date_examLoop['info']) > 0){

						foreach ($date_examLoop['info'] as $key3 => $date_examLoop2){
							if ($test[$key3] == false){
								unset($date_exam_de[$key1]['info'][$key3]);
							}

						}
					}
				}
			}
				
				$table_student = '';
				if(isset($formData['display_option']) && $formData['display_option']==1){
					//get student list
					$student_list = $examScheduleDB->getStudentByExamCenter($formData);
					
					$table_student = '<table class="" width="100%" cellpadding="5" cellspacing="0" border=1>
								<tr style="background-color:#acc8cc;">	
									<th>No</th>
									<th>Student Name</th>
									<th>Student ID</th>
									<th>Contact No</th>
									<th>Papers</th>		    	
								</tr>';
								foreach($student_list as $index=>$student){
									$index++;
								
								$table_student .= '<tr  style="vertical-align:middle">
									<td align="center">'.$index.'</td>
									<td align="left">'. $student['appl_fname'] .' '.$student['appl_lname'].'</td>
									<td align="center">'. $student['registrationId'].'</td>
									<td align="center">'. $student['appl_phone_mobile'].'</td>
									<td align="center">';
									
									if(count($student['subject'])>0){
										foreach($student['subject'] as $subject){
											$table_student .=  $subject['SubCode'].'<br>';
										}
										
									}
									
									$table_student .= '</td>
								</tr>';
								}
							$table_student .= '</table>';
				}
				
				
        		$table_schedule = '<table class="" width="100%" cellpadding="5" cellspacing="0" border=1>
									<tr style="background-color:#acc8cc;">
										<th rowspan=2 >DAY/DATE</th>	';	
										foreach($time_exam_te as $etime){
										$table_schedule .= "<th colspan='2'>".date('h:i A',strtotime($etime['es_start_time']))." TO ".date('h:i A',strtotime($etime['es_end_time']))."</th>";
										}
									$table_schedule .= "</tr>
									<tr>
										";
										for($i=0; $i<count($time_exam_te); $i++){
										$table_schedule .= " <th  style='background-color:#acc8cc;'>PAPER CODE</th>
										<th style='background-color:#acc8cc;'>NO. OF CANDIDATES</th>";
										}	
									$table_schedule .= "</tr>";
									foreach($date_exam_de as $edate){
										if($edate['schedule']==true){
									$table_schedule .= "<tr> 
															<th  width='25%' style='background-color:#acc8cc;'>".date("l", strtotime($edate['es_date'])).' /<br>'.date("d F Y", strtotime($edate['es_date']))."</th>";
										
										 					foreach($edate['info'] as $key=>$info){
															$table_schedule .= " <td align='center' style='vertical-align:middle'>";												

															 					if(count($info['paper'])>0){
																					foreach($info['paper'] as $paper){
																						if($paper['total_student']>0){
																							$table_schedule .=  $paper['paper_arr'].'<br>';
																						}
																					}
																				}
																				
															$table_schedule .=  "</td>
																				<td style='vertical-align:middle' align='center'>";		
																				
															 					if(count($info['paper'])>0){
																		 			foreach($info['paper'] as $paper){
																		 				if($paper['total_student']>0){
																		 					$table_schedule .=  $paper['total_student'].'<br>';
																		 				}
																					}
																				}
											
															$table_schedule .= "</td>";
										 					}
									$table_schedule .= "</tr>";
									}	}							
								$table_schedule .= "</table>";
        
														
							
							
        		$fieldValues = array(
		    	 			 '$[SEMESTER_NAME]'=>strtoupper( $semester['SemesterMainName'] ), 
							 '$[EXAM_CENTER]'=> strtoupper( $exam_center['ec_name'] ),
        					 '$[SCHEDULE]'=>$table_schedule,
        		 			 '$[STUDENT]'=>$table_student,
							 '$[LOGO]'=> "images/logo_text_high.png"
	    	  				);
	    	  				
				require_once 'dompdf_config.inc.php';
		
				$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
				$autoloader->pushAutoloader('DOMPDF_autoload');
				
				//template path	 
				$html_template_path = DOCUMENT_PATH."/template/ExamSchedule.html";
				
				$html = file_get_contents($html_template_path);			
		    		
				//replace variable
				foreach ($fieldValues as $key=>$value){
					$html = str_replace($key,$value,$html);	
				}
					
				
				$dompdf = new DOMPDF();
				$dompdf->load_html($html);
				$dompdf->set_paper('a4', 'potrait');
				$dompdf->render();
	
				//output filename 
				$output_filename = 'exam_schedule.pdf';
						
				//$dompdf = $dompdf->output($output_filename);
				$dompdf->stream($output_filename);						
				
			   
        }
       exit;
	}
	
	
	public function attendanceListAction(){
		
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
			
        $this->view->title = "Attendance List";
        
        $session = Zend_Registry::get('sis');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	     	
    	$form = new Reports_Form_SearchExamSchedule();
        $examScheduleDB = new Reports_Model_DbTable_ExamSchedule();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();			 
			
             if($form->isValid($formData))	{
				 
				$form->populate($formData);				
				
				$this->view->IdSemester = $formData['IdSemester'];
				$this->view->ec_id = $formData['ec_id'];
				
				$courses = $examScheduleDB->getCourseSchedule($formData);
				 //var_dump($courses); exit;
				$this->view->courses = $courses;
				//echo '<pre>';
				//print_r($courses);
             }
             
         }
         $this->view->form = $form;
    	
	}
	
	public function previewAttendanceoriAction(){
		
		 $this->view->title = "Preview Attendance List";
        
		 if($this->getRequest()->isPost()){
        	
		 	 	
		 	
			 $formData= $this->getRequest()->getPost();
			 
			 $session = Zend_Registry::get('sis');
			  
			 $examRegReportDB = new Reports_Model_DbTable_ExamRegistrationReport();
			 $examScheduleDB = new Reports_Model_DbTable_ExamSchedule();
        	 $semesterDB = new Registration_Model_DbTable_Semester();
        	 $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
        	 $subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
						 
			 //get semeester
			 $semester = $semesterDB->getData($formData['IdSemester']);
			 $this->view->semester = $semester;
			 $session->attendance_semester = $semester;
			
			 //get info exam center
			 $exam_center = $examCenterDB->getDatabyId($formData['ec_id']);
			 $this->view->exam_center = $exam_center;
			 $session->attendance_exam_center = $exam_center;
			 			
			 $subject_list = array();
			
			 for($i=0; $i<count($formData['subjectid_arr']); $i++){			 	
			  
			 	$subject_name_arr = array();
				$subject_code_arr = array();
				
			 	$subject_arr = explode (',',$formData['subjectid_arr'][$i]);

			 	//get subject info
			 	for($s=0; $s<count($subject_arr); $s++){
			 		$course = $subjectDb->getData($subject_arr[$s]);
			 		array_push($subject_name_arr,$course['SubjectName']);
			 		array_push($subject_code_arr,$course['SubCode']);
			 	}
			 	
			 	//get exam Date
			 	$exam_date = $examScheduleDB->getExamDateTimeByCourse($formData['IdSemester'],$formData['ec_id'],$subject_arr);
			 	
			 	 //get list of student by course
			 	$student = $examRegReportDB->getStudentByExamCenter($formData['IdSemester'],$formData['ec_id'],$subject_arr);
			 	
			 	$subject_list[$i]['exam_datetime'] = $exam_date;
			 	$subject_list[$i]['exam_date'] = (isset($exam_date) && $exam_date['es_date']!='') ? date('l',strtotime($exam_date['es_date'])). ' / ' .date('d F Y',strtotime($exam_date['es_date'])):'';
			 	$subject_list[$i]['exam_time'] = (isset($exam_date) && $exam_date['es_start_time']!='') ? date('h:i A',strtotime($exam_date['es_start_time'])). ' to ' . date('h:i A',strtotime($exam_date['es_end_time'])):'';
			 	$subject_list[$i]['subject'] = $subject_arr;
			 	$subject_list[$i]['subject_name'] = $subject_name_arr;
			 	$subject_list[$i]['subject_code'] = $subject_code_arr;
			 	$subject_list[$i]['student'] = $student;			 	
			 }
			 
			 //echo '<pre>';
			//print_r($subject_list);
			$this->view->subject_list = $subject_list;
			
			$session->attendance_subject_list = $subject_list;
		 }
		 		 
	}
	
	
	public function previewAttendanceAction(){
		
		 $this->view->title = "Preview Attendance List";
        
		 if($this->getRequest()->isPost()){
        	
			 $formData= $this->getRequest()->getPost();
			 
			 $session = Zend_Registry::get('sis');
			  
			 $db = Zend_Db_Table::getDefaultAdapter();
			 
			 $examRegReportDB = new Reports_Model_DbTable_ExamRegistrationReport();
			 $examScheduleDB = new Reports_Model_DbTable_ExamSchedule();
        	 $semesterDB = new Registration_Model_DbTable_Semester();
        	 $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
        	 $subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
						 
			 $cacheObj = new Cms_Cache();
			 $cache = $cacheObj->get();


			 //get semeester
			 $semester = $semesterDB->getData($formData['IdSemester']);
			 $this->view->semester = $semester;
			 //$session->attendance_semester = $semester;

			 $cache->save($semester, 'semester');
			
			 //get info exam center
			 $exam_center = $examCenterDB->getDatabyId($formData['ec_id']);
			 $this->view->exam_center = $exam_center;
			 
			 //$session->attendance_exam_center = $exam_center;
			 $cache->save($exam_center, 'exam_center');
						
			 $subject_list = array();
			 $counter=0;
			 
			 for($i=0; $i<count($formData['subjectid_arr']); $i++){			 	
			  
				 	$subject_name_arr = array();
					$subject_code_arr = array();
					
				 	$subject_arr = explode (',',$formData['subjectid_arr'][$i]);
	
				 	//get subject info
				 	for($s=0; $s<count($subject_arr); $s++){
				 		$course = $subjectDb->getData($subject_arr[$s]);

						if (!in_array($course['SubjectName'], $subject_code_arr)) {
							array_push($subject_name_arr, $course['SubjectName']);
						}

						if (!in_array($course['SubCode'], $subject_code_arr)) {
							array_push($subject_code_arr, $course['SubCode']);
						}
				 	}
				 	
				 	//get exam Date
				 	$exam_date = $examScheduleDB->getExamDateTimeByCourse($formData['IdSemester'],$formData['ec_id'],$subject_arr);

				 		
				 	//ini utk cater (PS) MIFP & CIFP WHERE ec_id = IASKL meka nak seperate kan by scheme
				 	if($formData['ec_id']==26 && $semester['IdScheme']==1){
				 	 	//get list of student by course
				 		//$groups = $examRegReportDB->getStudentByExamCenterProgram($formData['IdSemester'],$formData['ec_id'],$subject_arr);
				 		//$total_group = count($groups);		
				 		$groups = array();
				 		$total_group = 0 ;		 		
				 	}else{
				 		$total_group = 0 ;
				 	}
				 	
				 	if($total_group>0){
						
				 		//mifp & cifp detected tuka2 pening dah..
						
						//foreach group get student by program & mode of program						
						/*foreach($groups as $index=>$group){
							
							$subject_list[$counter]['mop'] = $group['mop'];
							$subject_list[$counter]['exam_datetime'] = $exam_date;
						 	$subject_list[$counter]['exam_date'] = (isset($exam_date) && $exam_date['es_date']!='') ? date('l',strtotime($exam_date['es_date'])). ' / ' .date('d F Y',strtotime($exam_date['es_date'])):'';
						 	$subject_list[$counter]['exam_time'] = (isset($exam_date) && $exam_date['es_start_time']!='') ? date('h:i A',strtotime($exam_date['es_start_time'])). ' to ' . date('h:i A',strtotime($exam_date['es_end_time'])):'';
						 	$subject_list[$counter]['subject'] = $subject_arr;
						 	$subject_list[$counter]['subject_name'] = $subject_name_arr;
						 	$subject_list[$counter]['subject_code'] = $subject_code_arr;
				 
							
							 $registrationId_arr = explode (',',$group['studentid']);
							
							 $sql_student = $db->select()
							                  ->from(array('er' => 'exam_registration')) 
							                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('registrationId','IdProgram'))
							                  ->join(array('sp' => 'student_profile'),'sp.id =sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)','group_saya'=> new Zend_Db_Expr ($index)))
							                  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = sr.IdProgramScheme',array('mode_of_program'))    
							                  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ps.mode_of_program',array('mop'=>'DefinitionCode')) 
							                  ->join(array('d2'=>'tbl_definationms'),'d2.idDefinition=ps.mode_of_study',array('mos'=>'DefinitionCode'))                     
							                  ->where('sr.registrationId IN (?)',$registrationId_arr)
							                  ->where('er_idSemester = ?',$formData['IdSemester'])
							                  ->where('er_ec_id = ?',$formData['ec_id'])
							                  ->where('er_idSubject IN (?)',$subject_arr)
							                  ->where('sr.IdProgram = ?',$group['IdProgram'])
							                  ->where('ps.mode_of_program = ?',$group['mode_of_program'])							                 
							                  ->where('sr.profileStatus = ?',92)
							                  ->where('sr.registrationId != ?',"1409999")
							                  ->group('sr.IdStudentRegistration')
							                  ->order('sp.appl_fname')
							                  ->order('sp.appl_lname')
							                  ->order('sr.registrationId');
			               
							 $result_student = $db->fetchAll($sql_student);		
							 $subject_list[$counter]['student'] = $result_student;	
							 $counter++;												 
						}*/
						
						
					}else{

						 $subject_list[$counter]['exam_datetime'] = $exam_date;
					 	 $subject_list[$counter]['exam_date'] = (isset($exam_date) && $exam_date['es_date']!='') ? date('l',strtotime($exam_date['es_date'])). ' / ' .date('d F Y',strtotime($exam_date['es_date'])):'';
					 	 $subject_list[$counter]['exam_time'] = (isset($exam_date) && $exam_date['es_start_time']!='') ? date('h:i A',strtotime($exam_date['es_start_time'])). ' to ' . date('h:i A',strtotime($exam_date['es_end_time'])):'';
					 	 $subject_list[$counter]['subject'] = $subject_arr;
					 	 $subject_list[$counter]['subject_name'] = $subject_name_arr;
					 	 $subject_list[$counter]['subject_code'] = $subject_code_arr;
						 	
						 //get list of student by course
			 			 $student = $examRegReportDB->getStudentByExamCenter($formData['IdSemester'],$formData['ec_id'],$subject_arr);
			 				
			 			 if(count($student)>0){
			 			 	foreach($student as $key_student=>$s){
					        	if(($s['exam_status']=="U" && $s['audit_program']=='') || $s['exam_status']=="EX"){
					        		unset($student[$key_student]);
					        	}
					        }
				        	array_values($student);//index
			 			 }
			 			 
				        	
			 			 $subject_list[$counter]['student'] = $student;
			 			 $counter++;
					}
				 	
					
				 		
			 }
			 
			 //echo '<pre>';
			//print_r($subject_list);
			 //var_dump($subject_list[0]['subject_code']); //exit;
			$this->view->subject_list = $subject_list;
			
			//$session->attendance_subject_list = $subject_list;
			$cache->save($subject_list, 'attendance_subject_list');
		 }
		 		 
	}
	
	
	public function printAttendanceAction(){
		
		$session = Zend_Registry::get('sis');
		$cacheObj = new Cms_Cache();
		$cache = $cacheObj->get();
		 
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
		        
		global $attendance_subject_list;	
		//$attendance_subject_list = $session->attendance_subject_list;
		$attendance_subject_list = $cache->load('attendance_subject_list');
		
        global $attendance_exam_center;
		//$attendance_exam_center = $session->attendance_exam_center;
		$attendance_exam_center = $cache->load('exam_center');

		global $attendance_semester;
		//$attendance_semester = $session->attendance_semester;
		$attendance_semester = $cache->load('semester');

		global $count_subject;
		$count_subject = count($attendance_subject_list);
		//print_r($attendance_exam_center);
	
	
		/*if(count($subject_list)>0 ){
		foreach($subject_list as $subject) { 
			
		
		$attendance = '
		<table class="" width="100%" cellpadding="5" cellspacing="1">
			<tr>
				<td align="center"><h2>'.$this->view->translate('ATTENDANCE LIST').'</h2></td>
			</tr>
			<tr>
				<td align="center"><h3>'.strtoupper($attendance_semester['SemesterMainName']).' '.$this->view->translate('SEMESTER').'</h3></td>
			</tr>
			<tr>
				<td align="center"><h3>'.strtoupper($attendance_exam_center['ec_name']).'</h3></td>
			</tr>
		</table>
		<br>
		<table class="" width="100%" cellpadding="5" cellspacing="1">
			<tr>
				<td width="20%">'.$this->view->translate("DAY/DATE").'</td>
				<td width="5px">:</td>		
				<td>'.strtoupper($subject["exam_date"]).'</td>
			</tr>
			<tr>
				<td>'.$this->view->translate('TIME').'</td>	
				<td width="5px">:</td>		
				<td>'. strtoupper($subject["exam_time"]).'</td>
			</tr>
			<tr>
				<td>'.$this->view->translate('PAPER CODE').'</td>
				<td width="5px">:</td>			
				<td>'. strtoupper ( implode('/',$subject['subject_code']) ).'</td>
			</tr>
			<tr>
				<td>'.$this->view->translate('PAPER TITLE').'</td>
				<td width="5px">:</td>			
				<td>'.strtoupper ( implode('/',array_unique($subject['subject_name'])) ).'</td>
			</tr>
		</table>
		<br>
		<table class="table" width="100%" cellpadding="5" cellspacing="1">
			<tr>	
				<th width="10%">'.$this->view->translate("No").'</th>
				<th>'. $this->view->translate("Student Name").'</th>
				<th>'. $this->view->translate("Student ID").'</th>		
				<th>'. $this->view->translate("Present (Y/N)").'</th>						    	
			</tr>';
		
			 
			foreach($subject['student'] as $index=>$student){	
			
			$attendance .= '<tr valign="middle">
				<td style="vertical-align:middle" align="center">'.$index+1 .'</td>
				<td style="vertical-align:middle" align="left">'.$student['student_name'].'</td>
				<td style="vertical-align:middle" align="center">'.$student['registrationId'].'</td>
				<td style="vertical-align:middle" align="center">';
				
				if($student['er_attendance_status']!='' || $student['er_attendance_status']!=0){
					if($student['er_attendance_status']==395){
						$attendance .= $this->view->translate("Y");
					}else {
						$attendance .= $this->view->translate("N");
					}
				}else{
					$attendance .= $this->view->translate("No Records");
				}
				
			$attendance .=	'</td>
			</tr>';
			}
		$attendance .= '</table>
		<br></br>';		
			 } 
		} 
		//end loop subject */
		
		
		$fieldValues = array(   			         		
							 //'$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.png"
			'$[LOGO]'=> "images/logo_text_high.jpg"
			      			);
      				
		require_once 'dompdf_config.inc.php';

		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//template path	 
		$html_template_path = DOCUMENT_PATH."/template/Attendance.html";
		
		$html = file_get_contents($html_template_path);			
    		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}

		
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'potrait');
		$dompdf->render();

		//output filename 
		$output_filename = 'attendance_list.pdf';
				
		//$dompdf = $dompdf->output($output_filename);
		$dompdf->stream($output_filename);	
		
		//to rename output file		
		//$output_file_path = DOCUMENT_PATH.'/download/'.$output_filename;				
	 		
		//file_put_contents($output_file_path, $dompdf);
		
		//$this->view->file_path = $output_file_path; */
		exit;
	}
	
	
	public function printAttendanceXlsAction(){
		
		$this->_helper->layout->disableLayout();
       
		$session = Zend_Registry::get('sis');
		 
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $cacheObj = new Cms_Cache();
		$cache = $cacheObj->get();
		        	
		//$this->view->subject_list = $session->attendance_subject_list;
		//$this->view->exam_center = $session->attendance_exam_center;
		//$this->view->semester = $session->attendance_semester;

		$this->view->subject_list = $cache->load('attendance_subject_list');		
		$this->view->exam_center = $cache->load('exam_center');
		$this->view->semester = $cache->load('semester');

	    $this->view->filename = date('Ymd').'_attendance_list.xls';
	}
	
	public function preaeercAction(){
		
			
        $this->view->title = "Pre-aeerc Report";
        
        $auth = Zend_Auth::getInstance();
        
       
        if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin	
        		$this->_redirect($this->view->url(array('module'=>'reports','controller'=>'exam-reports', 'action'=>'mypreaeerc'),'default',true));	
        }
        
        $session = Zend_Registry::get('sis');
        
    	
    	$form = new Reports_Form_SearchPreaeerc();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $semesterDB = new Registration_Model_DbTable_Semester();
			 $landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			 $landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			 $landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			 $sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
			 $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			 			
             if($form->isValid($formData))	{
				 
				 $form->populate($formData);
				
				 $semester = $semesterDB->getData($formData['IdSemester']);
				 
				 $progDB= new GeneralSetup_Model_DbTable_Program();
	             $programs = $progDB->getProgramByScheme($semester['IdScheme']);
		
					//print_r($programs);
					
					$i=0;
					$j=0;
					$allblocklandscape=null;
					$allsemlandscape=null;
				
             		$activeLandscape=$landscapeDB->getAllLandscape($formData["IdProgram"]);
					
					foreach($activeLandscape as $actl)
					{
						if($actl["LandscapeType"]!=44)
						{
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}
						elseif($actl["LandscapeType"]==44)
						{
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
		
					if(is_array($allsemlandscape))
					{
						$subjectsem=$sofferedDB->getMultiLandscapeCourseOffer($allsemlandscape, $formData,$formData['IdSemester']);
					}
		
					if(is_array($allblocklandscape))
					{
						$subjectblock=$sofferedDB->getMultiBlockLandscapeCourseOffer($allblocklandscape, $formData,$formData['IdSemester']);
					}
		
					if(is_array($allsemlandscape) && is_array($allblocklandscape))
					{
						$subjects=array_merge( $subjectsem , $subjectblock );
					}
					else
					{
						if(is_array($allsemlandscape) && !is_array($allblocklandscape))
						{
							$subjects=$subjectsem;
						}
						elseif(!is_array($allsemlandscape) && is_array($allblocklandscape))
						{
							$subjects=$subjectblock;
						}
						else
						{
							$subjects=FALSE;
						}
					}			

					foreach($subjects as $index=>$subject){							
						$group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$formData["IdSemester"]);					
						$subjects[$index]["total_group"] = $group;					
					}
						
					$this->view->IdProgram = $formData["IdProgram"];
					$this->view->list_subject = $subjects;
					
				
             }
         }
         $this->view->form = $form;
	}
	
	public function mypreaeercAction(){
		
			
        $this->view->title = "Pre-aeerc Report";
        
        $session = Zend_Registry::get('sis');
        
        $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	    	
    	$form = new Examination_Form_MyGroupSearch(array('locale'=>$locale));
    	$this->view->form = $form;
    	    
    	$semesterDB = new Registration_Model_DbTable_Semester();
    	$semester = $semesterDB->getAllCurrentSemester();		    	    	
		
    	$cur_sem = array();
    	foreach($semester as $sem){
    		array_push($cur_sem,$sem['IdSemesterMaster']);
    	}
    	
		
    	//get group
    	$reportExamDB = new Reports_Model_DbTable_ExamReport();
				
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();		
		
			$this->view->idSemester = $formData["IdSemester"];
			
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
			 	$subject = $reportExamDB->getCourseByGroupLecturer($formData);
				$this->view->list_subject = $subject;
								
			}//if form valid
			
    	}else{
    		
    		//display current semester
    		$formData['IdSemesterArray'] = $cur_sem;
    		$subject = $reportExamDB->getCourseByGroupLecturer($formData);
			$this->view->list_subject = $subject;
    		
    	}//if post		
        		
	}
	
	public function previewPreaeercAction(){
		
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
			
		$auth = Zend_Auth::getInstance();
        
        if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin	
        		$this->view->backurl = $this->view->url(array('module'=>'reports','controller'=>'exam-reports', 'action'=>'mypreaeerc'),'default',true);	
        }
        
        $this->view->title = "Preview Pre-aeerc Report";
        
        $session = Zend_Registry::get('sis');
		
        require_once ('jpgraph/jpgraph.php');
		require_once ('jpgraph/jpgraph_pie.php');
        $theme_class=new UniversalTheme;
        
		// Display the graph
		 $graphDir = DOCUMENT_PATH.'/graph';
		 if ( !is_dir( $graphDir ) )
		 {
			if ( mkdir_p($graphDir) === false )
			{
				throw new Exception('Cannot create folder ('.$graphDir.')');
			}
		 }
		 
		 
        $idSubject = $this->_getParam('idSubject', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idProgram = $this->_getParam('idProgram', 0);
		
		//get semester scheme
		$semesterDB = new Registration_Model_DbTable_Semester();
		$semester = $semesterDB->getData($idSemester);
		$this->view->semester = $semester;
	
		$gradeDB = new Examination_Model_DbTable_Grade();
		
		//get list group
		$examDb = new Reports_Model_DbTable_ExamReport();
		$groups = $examDb->getGroupList($idSubject,$idSemester,$idProgram);
		//dd($groups);
		foreach($groups as $key=>$group){
			
			$verify_flag = true;
			$datas = array();
			$labels = array();
			$chartData = array();
				
			//get dean HOD
			$dean = $examDb->getDeanByProgram($group['program_id']);
			$groups[$key]['dean'] = $dean['FullName'];
			
			
			//get student
			$student = $examDb->getStudentByGroup($group['IdCourseTaggingGroup'],$group['program_scheme_id'],$group['program_id']);
			$groups[$key]['student'] = $student;
			$total_student = count($student);
			
			//get grade list		
			$gradesetup= $gradeDB->getGradeSetup($group['program_id'],$idSemester,$idSubject);
			
			if($total_student > 0 && count($gradesetup)>0){				
				
				$code_color  = '';
				$student_fail = array();
				$colours = array();
				
				foreach($gradesetup as $index=>$grade){
					
					//gettotalno of student by grade
					$total_bygrade = 0;
					
					//$total_bygrade = $examDb->getTotalStudentByGrade($group['IdCourseTaggingGroup'],$group['program_id'],$group['program_scheme_id'],$grade['GradeName']);
					foreach($student as $s){						
						
						if(trim($s['grade_name'])==trim($grade['GradeName'])){
							
							$total_bygrade = $total_bygrade + 1;
							
							if($s['mark_approval_status']!= 2){
								$verify_flag = false;
							}
						}
						
						//attendance fail analysis
						if(trim($grade['GradeName'])=='F' && trim($s['grade_name'])=='F' ){
							array_push($student_fail,$s['IdStudentRegistration']);							
						}
						
					}//end foreach student
					
					
					
					//get percnetage 
					if($total_bygrade!=0){						
						$percentage = round (( ($total_bygrade/$total_student)*100 ),2) ;						
						array_push($datas,$percentage);
						array_push($labels,"(%.2f %%) ".$grade['GradeName']);
						
						//kot2 ade space
						$grade['GradeName'] = trim($grade['GradeName']);

						if($grade['GradeName']=='A'){
							$code_color = '#00b0f0';
						}elseif($grade['GradeName']=='A-'){
							$code_color = '#92d050';
						}elseif($grade['GradeName']=='B+'){
							$code_color = '#ffff00';
						}elseif($grade['GradeName']=='B'){
							$code_color = '#ffc000';
						}elseif($grade['GradeName']=='B-'){
							$code_color = '#4071ca';
						}elseif($grade['GradeName']=='C+'){
							$code_color = '#767171';
						}elseif($grade['GradeName']=='C'){
							$code_color = '#a5a5a5';
						}elseif($grade['GradeName']=='D'){
							$code_color = '#aa5a35';
						}elseif($grade['GradeName']=='F'){
							$code_color = '#ff0000';
						}
						array_push($colours,$code_color);
					}else{
						$percentage = 0;						
					}
					
					$gradesetup[$index]['numberofstudent']=$total_bygrade;
					$gradesetup[$index]['percentage']= ($percentage==0) ? '0.00':$percentage;
										
													
					//----------------Chart Section-------------------
					/*$series = 'series'.$key;
					$series = new HighRollerSeriesData();
					$series->addName('Grade')->addData($chartData);
			
					$linechart = 'linechart'.$key;
					$linechart = new HighRollerPieChart();
					$linechart->chart->renderTo = 'container'.$key;
					$linechart->title->text = '';									
					$linechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
					$linechart->credits['enabled'] = false;
					$linechart->addSeries($series); 					
			
					//$this->view->group_chart.$key = 'xx';
					$groups[$key]['group_chart']=$linechart;*/
					//-----------------------------------------------------
										
				}//end foreach gradesetup
				
				if($verify_flag==true){
					$groups[$key]['approval_status']='Verified By';
				}else{
					$groups[$key]['approval_status']='Pending verification';
				}
				
				//atendance analysis
				if(count($student_fail)>0){	
								
					//get info attendance for fail student
					$student_attendance = $examDb->getFailStudent($group['IdCourseTaggingGroup'],$group['program_id'],$group['program_scheme_id'],$grade['GradeName'],$student_fail);
										
					foreach($student_attendance as $attendkey=>$attendance){
						//calculate percentage
						$student_attendance[$attendkey]['numberofstudent']= $attendance['numberofstudent'];
						$student_attendance[$attendkey]['percentage']=round((($attendance['numberofstudent']/count($student_fail))*100),2);							
					}
					$groups[$key]['attendance_fail_analysis']=$student_attendance;
					
				}//end if attendance analysis
				
				
					//------------------------ another chart ------------------------
					if(count($datas)>0){
						// Create the Pie Graph.
						$groups[$key]['graph'] = new PieGraph(1650,1200);
						$groups[$key]['graph']->ClearTheme();
						//$groups[$key]['graph']->SetTheme($theme_class);
						$groups[$key]['graph']->SetFrame(false);

						 
						// Set A title for the plot
					    //	$groups[$key]['graph']->title->Set('String labels with values');
					    //	$groups[$key]['graph']->title->SetFont(FF_VERDANA,FS_BOLD,12);
						$groups[$key]['graph']->title->SetColor('black');
						 
						// Create pie plot
						$groups[$key]['p1'] = new PiePlot($datas);
						$groups[$key]['p1']->SetCenter(0.5,0.5);
						$groups[$key]['p1']->SetSize(0.3);
						$groups[$key]['p1']->SetSliceColors($colours);
						 
						// Setup the labels to be displayed
						$groups[$key]['p1']->SetLabels($labels);
						 
						// This method adjust the position of the labels. This is given as fractions
						// of the radius of the Pie. A value < 1 will put the center of the label
						// inside the Pie and a value >= 1 will pout the center of the label outside the
						// Pie. By default the label is positioned at 0.5, in the middle of each slice.
						$groups[$key]['p1']->SetLabelPos(1);
						 
						// Setup the label formats and what value we want to be shown (The absolute)
						// or the percentage.
						$groups[$key]['p1']->SetLabelType(PIE_VALUE_ABS);
						$groups[$key]['p1']->value->Show();
						$groups[$key]['p1']->value->SetFont(FF_DEFAULT,FS_NORMAL,30);
						$groups[$key]['p1']->value->SetColor('darkgray');
						$groups[$key]['p1']->ShowBorder();

						 
						// Add and stroke
						$groups[$key]['graph']->Add($groups[$key]['p1']);
							
								
						
						$graphFile = md5(time()).$key.'.png';
						$groups[$key]['graph']->Stroke($graphDir.'/'.$graphFile);
						
						//$this->view->graph1  = DOCUMENT_URL.'/graph/'.$graphFile;
						$groups[$key]['group_piechartname']=$graphFile;
						$groups[$key]['group_piechart']=DOCUMENT_URL.'/graph/'.$graphFile;
						$groups[$key]['group_piechartdata'] = base64_encode(file_get_contents(DOCUMENT_URL.'/graph/'.$graphFile));
					}		
					//------------------------ end another chart -----------------------
				
			}//end if count	

			$groups[$key]['gradesetup'] = $gradesetup;					
					
		}
				
		//echo '<pre>';
		//print_r($colours);
		$this->view->groups = $groups;
		//dd($groups);
		
		$session->preaeerc = $groups;
		$session->semester_group_info = $semester;
				
	}
	
	
	public function printPreaeercAction(){
		
		$this->_helper->layout->disableLayout();
		
		$session = Zend_Registry::get('sis');
		 
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
		        
		global $preaeerc;	
		$preaeerc = $session->preaeerc;
		
		global $semester_group_info;
		$semester_group_info = $session->semester_group_info;
					
		
		$fieldValues = array(   			         		
				 			 '$[LOGO]'=> "images/logo_text_high.png"
      						 );
      				
		require_once 'dompdf_config.inc.php';

		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//template path	 
		$html_template_path = $this->tplfolder."/template/Preaeerc.html";
		
		$html = file_get_contents($html_template_path);			
    		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}

		
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'potrait');
		$dompdf->render();

		//output filename 
		$output_filename = 'preaeerc_list.pdf';
				
		//$dompdf = $dompdf->output($output_filename);
		$dompdf->stream($output_filename);	
				
		//to rename output file		
		//$output_file_path = DOCUMENT_PATH.'/download/'.$output_filename;				
	 		
		//file_put_contents($output_file_path, $dompdf);
		
		//$this->view->file_path = $output_file_path;
		
		if(count($preaeerc)>0 ){
			foreach($preaeerc as $key=>$group) { 
				if(count($group['student'])>0){
					unlink ( DOCUMENT_PATH.'/graph/'.$group['group_piechartname']);
				}
			}
		}
		exit;
	}
	
	
	public function aeercAction(){
		
		ini_set('max_execution_time',300);
		$this->view->title = "Aeerc Report";
        
        $session = Zend_Registry::get('sis');
            	
    	$form = new Reports_Form_SearchAeerc();
         
         if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
			 
			$semesterDB = new Registration_Model_DbTable_Semester();
			$examDb = new Reports_Model_DbTable_ExamReport();
			$reportExamRegDB = new Reports_Model_DbTable_ExamRegistrationReport();
			$gradeDB = new Examination_Model_DbTable_Grade();
			$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			$visaDB = new Records_Model_DbTable_StudentVisa();
			$icampusDB = new icampus_Function_Application_AcademicProgress();
            $recordDb = new Records_Model_DbTable_Academicprogress(); 
			 
			 			
			if($form->isValid($formData))	{
			 
				$form->populate($formData);
				 
				$this->view->IdProgramScheme = $formData['IdProgramScheme'];

				//get subject
				//$subject_list = $reportExamRegDB->getLandscapeSubject($formData['IdProgram'],$formData['IdSemester'],$formData['IdProgramScheme'],$formData);

				$semesterDB = new Registration_Model_DbTable_Semester();
				$semester = $semesterDB->getData($formData['IdSemester']);
				$this->view->semester = $semester;
				
				//get semester open with same scheme
				$semester_open = $semesterDB->getEqSemesterOpen($formData['IdSemester']);
				
				//push in array()
				$array_sem_eq = array();
				foreach($semester_open as $semopen){
					array_push($array_sem_eq,$semopen['IdSemesterMaster']);
				}	
							
				if($semester['IdScheme']==1){
					//get grade setup
					$gradesetup= $gradeDB->getGradeSetup($formData['IdProgram'],$formData['IdSemester']);
					foreach($gradesetup as $keygrade=>$g){
						$gradesetup[$keygrade]['total']=0;
					}
					
					//var_dump($gradesetup); exit;
					$gradesetup[]=array(
						'Grade' => 0,
						'GradeName' => 'CT',
						'total' => 0
					);

					$gradesetup[]=array(
						'Grade' => 0,
						'GradeName' => 'EX',
						'total' => 0
					);

					$this->view->gradesetup_list = $gradesetup;
					$session->aeerc_gradesetup_list  = $gradesetup;
				}
				//get student list
				$student_list = $examDb->getStudentAeerc($formData,$semester['IdScheme']);
				$total = (count($student_list));
				
				$student_audits = $examDb->getStudentAeercAuditProgram($formData,$semester['IdScheme']);

				foreach ($student_audits as $st => $student_audit) {
					$student_list[$total] = $student_audit;
					$total++;
				}
				if($semester['IdScheme']==11){
					$subject_list_arr = $examDb->getStudentSubjectAeerc($formData,$student_list);
					//$subject_list_arr = $examDb->getStudentSubjectAeercPS($formData,$student_list);
				}
				else{
					$subject_list_arr = $examDb->getStudentSubjectAeercPS($formData,$student_list);	
				}
									
				$subject_arr = explode (',',$subject_list_arr['IdSubject']);	
				$subject_list = $subjectDb->getInSubject($subject_arr);
									
				if(count($student_list)>0){
					
					foreach($student_list as $key=>$student){
						
						
						//---------------- get student visa ---------------- 
						if($student['appl_nationality'] != 547){ //if bukan malaysian
							$visa = $visaDB->getLastExpiryDate($student['sp_id']);							
							if($visa){
								if(strtotime($visa['av_expiry']) < date('Y-m-d') ){
									$student_list[$key]['visa']='INACTIVE';
								}else{
									$student_list[$key]['visa']='ACTIVE';
								}
							}else{
								$student_list[$key]['visa']='N/A';
							}
						}else{
							$student_list[$key]['visa']='N/A';
						}
						//---------------- end student visa ---------------- 
						
						
						
						//---------------- get student subject grade info ---------------- 
						$paper_pass = 0;

						foreach($subject_list as $key_subject=>$subject){

							//get grade each subject
							if($semester['IdScheme']==11){
								$student_subject = $examDb->getStudentRegSubjectInfo($student['IdStudentRegistration'],$formData['IdSemester'],$subject['IdSubject']);
							}
							else {
								$student_subject = $examDb->getStudentRegSubjectInfoPS($student['IdStudentRegistration'],$subject['IdSubject'],$formData);


							}

							if($student_subject['grade_name']!='' && $student_subject['grade_name']!='F'){
								$paper_pass=$paper_pass+1;
							}
							$subject_list[$key_subject]['grade_name']=$student_subject['grade_name'];
							$subject_list[$key_subject]['grade_point']=$student_subject['grade_point'];
							$subject_list[$key_subject]['exam_status']=$student_subject['exam_status'];

							if(($student_subject['IdSemesterMain'] == $formData['IdSemester']) || (in_array($student_subject['IdSemesterMain'],$array_sem_eq)==true)) {
								$attendance = $examDb->getAttendance($student['IdStudentRegistration'],$formData['IdSemester'],$subject['IdSubject']);

								$subject_list[$key_subject]['sem_status'] = 'now';
								if($attendance) {
									$subject_list[$key_subject]['attendance_status'] = $attendance['er_attendance_status'];
								}
								else {
									$subject_list[$key_subject]['attendance_status'] = 'none';
								}
							}
							else {
								$subject_list[$key_subject]['sem_status'] = 'none';
								$subject_list[$key_subject]['attendance_status'] = 'none';
							}
						}
						$student_list[$key]['subject']=$subject_list;
						$student_list[$key]['paper_pass']=$paper_pass;

						if($formData['IdProgram']==5){
							$paper_pass = 0;

							unset($formData['Part']);
							$subject_list_arr2 = $examDb->getStudentSubjectAeercPS($formData,$student_list);
							$subject_arr2 = explode (',',$subject_list_arr2['IdSubject']);
							$subject_list2 = $subjectDb->getInSubject($subject_arr2);

							foreach($subject_list2 as $key_subject=>$subject){

								//get grade each subject
								if($semester['IdScheme']==11){
									$student_subject = $examDb->getStudentRegSubjectInfo($student['IdStudentRegistration'],$formData['IdSemester'],$subject['IdSubject']);
								}
								else {
									$student_subject = $examDb->getStudentRegSubjectInfoPS($student['IdStudentRegistration'],$subject['IdSubject'],$formData);


								}

								if($student_subject['grade_name']!='' && $student_subject['grade_name']!='F'){
									$paper_pass=$paper_pass+1;
								}
							}
						}

						//---------------- get student subject grade info ---------------- 
						

						if($semester['IdScheme']==1){ //PS
														if ($subject){
							//$total_bygrade = $examDb->getTotalGrade($student['IdStudentRegistration'],$formData['IdSemester'],$subject['IdSubject']);
							$total_bygrade = $examDb->getTotalGradePS($student['IdStudentRegistration'],$subject['IdSubject'],$formData);
							$student_list[$key]['total_bygrade']=$total_bygrade;
							$sort[$key] = $total_bygrade;
														}
						}
						
						
						//get remaining paper
						list($remaining_paper_cifp,$remaining_paper_mifp,$requiredcifp) = $icampusDB->getExpectedGraduateStudent($student['IdStudentRegistration'],'rpaper');
						
						if($formData['IdProgram']==5){
															/*echo '----------------------------------------------<br/>';
															echo 'id : '.$student['registrationId'].'<br/>';
															echo 'required : '.$requiredcifp.'<br/>';
															echo 'pass : '.$paper_pass.'<br/>';
															echo '----------------------------------------------<br/>';*/
							//CIFP
															$remcifp = ($requiredcifp-$paper_pass)-1;
							$student_list[$key]['remaining_papers']=($remcifp <= 0 ? 0:$remcifp);
						}else if($formData['IdProgram']==2){
							//MIFP
							$student_list[$key]['remaining_papers']=$remaining_paper_mifp;
						}else{
							$student_list[$key]['remaining_papers']=$remaining_paper_mifp;
						}
						
						
					}//end foreach
					
					if($semester['IdScheme']==1){
						array_multisort($sort, SORT_DESC, $student_list);
					}

				}//end count

				//var_dump($student_list[4]['subject']);
									//var_dump($subject_list); exit;
					$this->view->subject_list = $subject_list;
					$this->view->student_list = $student_list;
				//print_r($formData);
				if(isset($formData['IdProgramScheme'])){
					$this->view->scheme = implode($formData['IdProgramScheme'],',');
				}
					$session->aeerc_formData = $formData;
					$session->aeerc_student_list  = $student_list;					
					$session->aeerc_subject_list  = $subject_list;					 
				}

			}
			
			
	 
		$this->view->form = $form;
        
	}
	
	 public function printAeercAction(){
	 	
    	$this->_helper->layout->disableLayout();
        
    	$session = Zend_Registry::get('sis');
      
    	$formData  = $session->aeerc_formData;
    	//print_r($formData);
    	$semesterDB = new Registration_Model_DbTable_Semester();
		$semester = $semesterDB->getData($formData['IdSemester']);
		$this->view->semester = $semester;
	
		$progDB= new GeneralSetup_Model_DbTable_Program();
	    $this->view->program= $progDB->getProgramSchemeDataList($formData['IdProgramScheme']);
		//echo '<pre>';
		//print_r($this->view->program);exit;
    	$this->view->student_list  = $session->aeerc_student_list;
		$this->view->gradesetup_list = $session->aeerc_gradesetup_list;
		$this->view->subject_list = $session->aeerc_subject_list;
					
					
        $this->view->filename = date('Ymd').'_aeerc.xls';
    }
    
    
    public function absenteesAction() {
    	
    	$this->view->title = "List of Absentees";
        
        $session = Zend_Registry::get('sis');
            	
    	$form = new Reports_Form_SearchAbsentees();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			  if($form->isValid($formData))	{
				 
			  		 $session->absentees_formData = $formData;
					 $form->populate($formData);
			
					 $semesterDB = new Registration_Model_DbTable_Semester();
					 $semester = $semesterDB->getData($formData['IdSemester']);
					 $this->view->semester = $semester;
					
					 $examRegDB = new Registration_Model_DbTable_ExamRegistration();
					 $progDB= new GeneralSetup_Model_DbTable_Program();
					 			 
					 for($p=0; $p<count($formData['IdProgram']); $p++){
					 	
					 	$idProgram = $formData['IdProgram'][$p];
					 	
					 	//get program info
					 	$program = $progDB->fngetProgramData($idProgram);
					 	
					 	$programmes[$p] = $program;
					 	
					 	//get absent student
					 	$student_list = $examRegDB->getAbsenteesByProgram($formData['IdSemester'],$idProgram,$formData['attendance_status']); 
					 	$programmes[$p]['absentees']=$student_list;
					 	
					 }//end for
			  }
			 
			
			$this->view->programmes = $programmes;
			$session->absentees_programmes = $programmes;
			
         }
         
         $this->view->form = $form;
	
    }
    
    public function printAbsenteesAction(){
    	
    	$this->_helper->layout->disableLayout();
        
    	$session = Zend_Registry::get('sis');
      
    	$formData  = $session->absentees_formData;
    	
    	$semesterDB = new Registration_Model_DbTable_Semester();
		$semester = $semesterDB->getData($formData['IdSemester']);
		$this->view->semester = $semester;
			
    	$this->view->programmes  = $session->absentees_programmes;
										
        $this->view->filename = date('Ymd').'_absentees.xls';
    }
    
    
    
    
    public function resultAnalysisAction(){
    	
    	$this->view->title = "Result Analysis";
    	
    	require_once ('jpgraph/jpgraph.php');
		require_once ('jpgraph/jpgraph_bar.php');
        
        $session = Zend_Registry::get('sis');
            	
    	$form = new Reports_Form_SearchResultAnalysis();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();

			  if($form->isValid($formData))	{
				 			  		 
					 $form->populate($formData);

					 $theme_class=new UniversalTheme;
					 
 				     // Display the graph
					 $graphDir = DOCUMENT_PATH.'/graph';
					 if ( !is_dir( $graphDir ) )
					 {
						if ( mkdir_p($graphDir) === false )
						{
							throw new Exception('Cannot create folder ('.$graphDir.')');
						}
					 }
					 
					 $graph_x = array();
					 
					 //get semeester
					 $semesterDB = new Registration_Model_DbTable_Semester();
					 $semester = $semesterDB->getData($formData['IdSemester']);
					 $this->view->semester = $semester;

					 $semester_backwards = $semesterDB->getListSemesterBackwards($semester);

					 $progDB= new GeneralSetup_Model_DbTable_Program();
					 $this->view->program = $progDB->fngetProgramData($formData['IdProgram']);
			
					 $subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
					 $this->view->subject = $subjectDB->getData($formData['IdSubject']);
					 
					 //get grade list	
					 $gradeDB = new Examination_Model_DbTable_Grade();	
					 $grade_list = $gradeDB->getGradeSetup($formData['IdProgram'],$formData['IdSemester'],$formData['IdSubject']);
					 $this->view->grade = $grade_list;
					 $session->analysis_grade = $grade_list;
					 //print_r($semester_backwards);
					 
					 $examReportDB = new Reports_Model_DbTable_ExamReport();
					 
					 
					 if(count($semester_backwards)>0){
					 	
					 	foreach($semester_backwards as $index=>$sem){	
					 						 							 		
					 		$overall_total = 0;
					 		$graph_y = array();
					 		
					 		foreach($grade_list as $key=>$grade){
					 									 			 
				 				//get total number of student by subject
				 				$total_student = $examReportDB->getTotalStudentBySubject($formData['IdProgram'],$sem['IdSemesterMaster'],$formData['IdSubject']);
				 				
				 				//get total number of student by grade
				 				$total_student_grade = $examReportDB->getTotalStudentBySemGrade($formData['IdProgram'],$sem['IdSemesterMaster'],$formData['IdSubject'],$grade['GradeName']);
				 				
				 				$grade_list[$key]['total']=$total_student_grade;
				 				if($total_student==0 || $total_student_grade==0){
				 					$grade_list[$key]['percentage']=0;
				 				}else{
				 					$grade_list[$key]['percentage']= round( (($total_student_grade/$total_student)*100), 2);
				 				}
				 				
					 			$overall_total = $overall_total+$total_student_grade;
					 			
					 			array_push($graph_x,$grade['GradeName']);
					 			array_push($graph_y,$grade_list[$key]['percentage']);
					 								 			
					 		}					 		
					 	
					 		$semester_backwards[$index]['graph_y'] = $graph_y;
					 		$semester_backwards[$index]['plot']  = 'plot'.$index;
					 		$semester_backwards[$index]['grade'] = $grade_list;
					 		$semester_backwards[$index]['overall_total'] = $overall_total;					 		
					 	}
					 	
					 	
						 	//---------------- Chart Section -------------------
												
							// Create the graph. These two calls are always required
							$graph = new Graph(350,200,'auto');
							$graph->SetScale("textlin");			
							
							$graph->SetTheme($theme_class);
				
							$graph->yaxis->SetTickPositions(array(0,20,40,60,80,100), array(10,30,50,70,90));
							$graph->SetBox(false);
				
							$graph->ygrid->SetFill(false);
							$graph->xaxis->SetTickLabels($graph_x);
							$graph->yaxis->HideLine(false);
							$graph->yaxis->HideTicks(false,false);
				
							// Create the bar plots
							$plots = array();
							
							foreach($semester_backwards as $semkey=>$sem){
								
								$sem['plot'] = new BarPlot($sem['graph_y']);
								array_push($plots,$sem['plot']);
								
								$sem['plot']->SetColor("white");
								$sem['plot']->SetFillColor("#cc1111");
								$sem['plot']->SetLegend($sem['SemesterMainName']);							
							}
						
							//Create the grouped bar plot
							$gbplot = new GroupBarPlot($plots);
							
							//add it to the graPH
							$graph->Add($gbplot);
	
							//set title
							$graph->title->Set("Analysis for Last Three Semesters");
							

							$graphFile = md5(time()).$index.'.png';
							$graph->Stroke($graphDir.'/'.$graphFile);
							
							$this->view->graph1  = DOCUMENT_URL.'/graph/'.$graphFile;
														
						 	//---------------- End Chart Section -------------------
					 }
					 $this->view->semester_backwards = $semester_backwards;
					 $session->analysis_semester = $semester_backwards;
         			 $session->analysis_graph = 	$graphFile;
					 $session->analysis_graphdata = base64_encode(file_get_contents(DOCUMENT_URL.'/graph/'.$graphFile));
			  }
			  
         }
         
        
         $this->view->form = $form;       
         
    }
    
     public function printAnalysisAction(){
   
     	$this->_helper->layout->disableLayout();
		
		$session = Zend_Registry::get('sis');
		 
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $idProgram = $this->_getParam('idProgram',null); 
        $idSubject = $this->_getParam('idSubject',null); 

        global $subject;
        global $program;
        global $graph;
        
        $progDB= new GeneralSetup_Model_DbTable_Program();
		$program = $progDB->fngetProgramData($idProgram);
	
		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDB->getData($idSubject);
					
		 
		global $semester_backwards;	
		$semester_backwards = $session->analysis_semester;
		
		global $grade_list;	
		$grade_list = $session->analysis_grade;
		
		global $graph;	
		$graph = $session->analysis_graph;
		
		$fieldValues = array(   			         		
				 			 '$[LOGO]'=> "images/logo_text_high.png",
							 '$[GRAPH]'=> $session->analysis_graphdata,
							 '$[GRAPHMIME]'=>'image/png'
      						 );
      				
		require_once 'dompdf_config.inc.php';

		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//template path	 
		$html_template_path = $this->tplfolder."/template/ResultAnalysis.html";
		
		$html = file_get_contents($html_template_path);			
    		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}


		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', (count($grade_list) < 4 ? 'portrait' : 'landscape') );
		$dompdf->render();

		//output filename 
		$output_filename = 'result_analysis.pdf';
				
		//$dompdf = $dompdf->output($output_filename);
		$dompdf->stream($output_filename);	
				
		//to rename output file		
		//$output_file_path = DOCUMENT_PATH.'/download/'.$output_filename;				
	 		
		//file_put_contents($output_file_path, $dompdf);
		
		//$this->view->file_path = $output_file_path;
		
		//once completed
		
		
		//ok, kita dah ambil gambar, kita delete
		unlink ( DOCUMENT_PATH.'/graph/'.$graph);
		exit;
     }
     
     
public function courseAuditAction(){
     	
     	//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
		
        
     	$this->view->title = "Course Audit Report";
        
        $session = Zend_Registry::get('sis');
            	
    	$form = new Reports_Form_SearchCourseAudit();
         
         if($this->getRequest()->isPost()){
        	
         	
			 $formData = $this->getRequest()->getPost();
			 //var_dump($formData); exit;
			 $semesterDB = new Registration_Model_DbTable_Semester();
			 $landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			 $landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			 $landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			 $sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
			 $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			 $icampusDB = new icampus_Function_Application_AcademicProgress();
			 $subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			 $examDb = new Reports_Model_DbTable_ExamReport();
			 $gradeDB = new Examination_Model_DbTable_Grade();
			 $studentSemesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
			 $sregsubDB = new Registration_Model_DbTable_Studentregsubjects();
			 			
             if($form->isValid($formData))	{
				 
					$form->populate($formData);
					
					$this->view->IdProgram = $formData['IdProgram'];
					
					$i=0;
					$j=0;
					$allblocklandscape=null;
					$allsemlandscape=null;
				
             		$activeLandscape=$landscapeDB->getAllLandscape($formData["IdProgram"]);
					
					foreach($activeLandscape as $actl)
					{
						if($actl["LandscapeType"]!=44)
						{
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}
						elseif($actl["LandscapeType"]==44)
						{
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
		
					if(is_array($allsemlandscape))
					{
						if($formData["IdProgram"]==5){
							$listSubjectType = $examDb->getPartList($allsemlandscape);
							//echo 'In Progress....';
							//exit;
						}else{
							$listSubjectType=$sofferedDB->getLandscapeCourse($allsemlandscape, $formData,$formData['IdSemester']);
						}
						
					}

					
					if(count($listSubjectType)>0){						
						foreach($listSubjectType as $index=>$subtype){	
								
								$pgrade = array();
								$id_subject = explode(',',$subtype['IdSubjects'])	;				
								$listSubjectType[$index]['subject_list'] = $subjectDb->getSubjectListIn($id_subject);
								
								// ========== CIFP special grade display by part x sama utk part 3  ========== 
								if($formData['IdProgram']==5){ //CIFP	
									
									//terpaksa aku harcode memandangkan permintaan yg merepek2
									if($subtype['block']==3){
										
										$pgrade[0]['Grade']='P';
										$pgrade[0]['GradeName']='P';
										$pgrade[0]['GradeFullName']='Pass';
										
										$pgrade[1]['Grade']='F';
										$pgrade[1]['GradeName']='F';
										$pgrade[1]['GradeFullName']='Fail';
										
									}else{
										
										$pgrade[0]['Grade']='PD';
										$pgrade[0]['GradeName']='PD';
										$pgrade[0]['GradeFullName']='PD';
										
										$pgrade[1]['Grade']='P';
										$pgrade[1]['GradeName']='P';
										$pgrade[1]['GradeFullName']='P';
										
										$pgrade[2]['Grade']='A';
										$pgrade[2]['GradeName']='A';
										$pgrade[2]['GradeFullName']='A';
										
										$pgrade[3]['Grade']='B';
										$pgrade[3]['GradeName']='B';
										$pgrade[3]['GradeFullName']='B';
										
										$pgrade[4]['Grade']='EX';
										$pgrade[4]['GradeName']='EX';
										$pgrade[4]['GradeFullName']='EX';
										
										$pgrade[5]['Grade']='F';
										$pgrade[5]['GradeName']='F';
										$pgrade[5]['GradeFullName']='F';
									}									
									// ========== END CIFP special grade display by part x sama utk part 3  ========== 
								}								
								else 
								if($formData['IdProgram']==2){
									    //MIFP
					
										$pgrade[0]['Grade']='A';
										$pgrade[0]['GradeName']='A';
										$pgrade[0]['GradeFullName']='A';
										
										$pgrade[1]['Grade']='A-';
										$pgrade[1]['GradeName']='A-';
										$pgrade[1]['GradeFullName']='A-';
										
										$pgrade[2]['Grade']='B+';
										$pgrade[2]['GradeName']='B+';
										$pgrade[2]['GradeFullName']='B+';
										
										$pgrade[3]['Grade']='B';
										$pgrade[3]['GradeName']='B';
										$pgrade[3]['GradeFullName']='B';
										
										$pgrade[4]['Grade']='CT';
										$pgrade[4]['GradeName']='CT';
										$pgrade[4]['GradeFullName']='CT';

										$pgrade[5]['Grade']='EX';
										$pgrade[5]['GradeName']='EX';
										$pgrade[5]['GradeFullName']='EX';

										$pgrade[6]['Grade']='F';
										$pgrade[6]['GradeName']='F';
										$pgrade[6]['GradeFullName']='F';
								}
								
								$listSubjectType[$index]['gradesetup']=$pgrade;
							
						}//end loop subtype
					}						
					$this->view->subject_type_list = $listSubjectType;
									
	
					//semester
					$semesterDB = new Registration_Model_DbTable_Semester();
					$semester = $semesterDB->getData($formData['IdSemester']);
					$this->view->semester = $semester;
												

					$gradesetup= $gradeDB->getGradeSetup($formData['IdProgram'],$formData['IdSemester']);
					$mgrade['Grade']='CT';
					$mgrade['GradeName']='CT';						
					array_push($gradesetup,$mgrade);

					 $mgrade2['Grade']='EX';
					 $mgrade2['GradeName']='EX';
					 array_push($gradesetup,$mgrade2);
												
					//just assign default value
					foreach($gradesetup as $keygrade=>$g){					
						$gradesetup[$keygrade]['total']=0;
					}				
					$this->view->gradesetup_list = $gradesetup;
					$session->ca_gradesetup_list  = $gradesetup;
									
					
					//publish mark
					$publishDB = new Examination_Model_DbTable_PublishMark();
					$publish = $publishDB->getPublishResult($formData['IdProgram'],$formData['IdSemester'],$category=0);
		
					//tnya isham
					/*if($publish['']){
						
					}*/

					//get student list
					$student_list = $examDb->getStudentCourseAudit($formData,$semester['IdScheme']);
					
					if(count($student_list)>0){
													
						foreach($student_list as $key=>$student){

							$flag_pppart_taken = false;	
							$flag_paper_taken  = false;	
							$overall_total_credit_hour_registered = 0;
							
							//student year of study							
							$student_list[$key]['year_of_study'] = $examDb->countYearOfStudy($student['IdStudentRegistration'], $semester['IdScheme']);
							
							//total semester status
							$semester_register = $examDb->getActiveRegisteredSemester($student['IdStudentRegistration']);
							$student_list[$key]['semester_active']=count($semester_register);
											
							//Summary
							$summary = $icampusDB->getExpectedGraduateStudent($student['IdStudentRegistration'],'caudit');
							//var_dump($summary);
							//get remaining paper
							if($semester['IdScheme']==1){								
								if($formData['IdProgram']==5){
									
									//CIFP
									$total_remaining_all_part = abs($summary['remaining_part1'])+abs($summary['remaining_part2'])+abs($summary['remaining_part3']);
									
									$complete_p1p2 = (abs($summary['remaining_part1'])+abs($summary['remaining_part2']));
									
									$student_list[$key]['complete_all_part']= ($total_remaining_all_part == 0) ? 'YES':'NO';
									$student_list[$key]['complete_part1']   = ($summary['remaining_part1'] == 0) ? 'YES':'NO';
									$student_list[$key]['complete_part2']   = ($summary['remaining_part2'] == 0) ? 'YES':'NO';
									$student_list[$key]['complete_p1p2']    = ($complete_p1p2 == 0)  ? 'YES':'NO';
									$student_list[$key]['complete_part3']   = ($summary['remaining_part3'] == 0) ? 'YES':'NO';
									
									$student_list[$key]['remaining_paper']  = $complete_p1p2;
									$student_list[$key]['remaining_pppart'] = ($summary['remaining_part3'] == 0) ? 'NO':'YES';
                                                                        $remmifp = $summary['this_chr']-1;
									$student_list[$key]['remaining_papers'] = ($remmifp <= 0 ? 0:$remmifp);
                                                                        $student_list[$key]['cifprequired'] = $summary['cifprequired'];
									
									$registered_current_semester = $sregsubDB->getTotalPaperRegistered($student['IdStudentRegistration'],$formData['IdSemester']);
									$student_list[$key]['total_registering'] = count($registered_current_semester);
									
								}else if($formData['IdProgram']==2){
								
									//MIFP
									$registered_current_semester = $sregsubDB->getTotalPaperRegistered($student['IdStudentRegistration'],$formData['IdSemester']);
									//var_dump($registered_current_semester); exit;
									$student_list[$key]['total_registering'] = count($registered_current_semester);
							
									$student_list[$key]['remaining_papers']=$summary['remaining_paper_mifp'];
								}								
							}

							
							$paper_pass = 0;							
							foreach($listSubjectType as $key_subtype=>$sub_type){

								$total_paper = 0;	
								$paper_pass_bytype = 0;
								$my_regsub_arr = array();
								$my_sub_arr = array();
								$total_ch_register_bytype = 0;							
								foreach($sub_type['subject_list'] as $key_subject=>$subject){																
										
										//get grade each subject
										$student_subject = $examDb->getStudentRegSubjectData($student['IdStudentRegistration'],$subject['IdSubject'],$formData['IdProgram'],$student['IdLandscape'],$sub_type['SubjectType']);
										
										if(isset($student_subject)){
											
											if($student_subject['IdStudentRegSubjects']!=''){
												array_push($my_regsub_arr,$student_subject['IdStudentRegSubjects']);
												array_push($my_sub_arr,$student_subject['IdSubject']);
											}
																						
											if($student_subject['exam_status']=='CT' || $student_subject['exam_status']=='EX' || $student_subject['exam_status']=='U' ){
												$sub_type['subject_list'][$key_subject]['grade_name']=$student_subject['exam_status'];
												$sub_type['subject_list'][$key_subject]['grade_point']='';	
												$sub_type['subject_list'][$key_subject]['grade']=$student_subject['exam_status'];
												$sub_type['subject_list'][$key_subject]['status']=2;
												$sub_type['subject_list'][$key_subject]['Active']=$student_subject['Active'];
												
												if($student_subject['exam_status']!='U' && $student_subject['exam_status']!='EX'){
													$total_paper=$total_paper+1;													
													$paper_pass_bytype = $paper_pass_bytype+1;
													
													if(isset($sub_type['block']) && $sub_type['block']==3 && $formData['IdProgram']==5 ){ //MIFP exclude part III
														//exclude part III for total paper passed
													}else{
														$paper_pass=$paper_pass+1;
													}
												}
												
												if($student_subject['IdSemesterMain']==$formData['IdSemester']){
													//to check if student is register at current semester
													$sub_type['subject_list'][$key_subject]['reg_cur_sem']=true;
												}else{
													$sub_type['subject_list'][$key_subject]['reg_cur_sem']=false;
												}
											
											}else{
												$sub_type['subject_list'][$key_subject]['grade_name']=$student_subject['grade_name'];
												$sub_type['subject_list'][$key_subject]['grade_point']=$student_subject['grade_point'];													
												$sub_type['subject_list'][$key_subject]['status']=$student_subject['mark_approval_status'];
												$sub_type['subject_list'][$key_subject]['Active']=$student_subject['Active'];
												
												if($sub_type['SubjectType']==271){													
													$sub_type['subject_list'][$key_subject]['grade']=$student_subject['exam_status'];
													$sub_type['subject_list'][$key_subject]['status']=2;
												}else{
													
													if( $student_subject['IdSubject'] == 84 || $student_subject['IdSubject']==85 ){
														if(isset($student_subject['grade_name']) && $student_subject['grade_name']=='P'){
															$sub_type['subject_list'][$key_subject]['grade']=trim($student_subject['grade_name']).' - Pass';
														}
													}else{
														//geli betoll kejap nak kejap x nak off kan temporary kang nak kang hangin wei
														//$sub_type['subject_list'][$key_subject]['grade']=$student_subject['final_course_mark'].' ['.trim($student_subject['grade_name']).']';
														$sub_type['subject_list'][$key_subject]['grade']=trim($student_subject['grade_name']);
													}
												}
												
												if($student_subject['mark_approval_status']==2){
													$total_paper=$total_paper+1;
													
													if(trim($student_subject['grade_name'])!='' && trim($student_subject['grade_name'])!='F'){
														
														if($formData['IdProgram']==5 && $sub_type['block']==3){ //CIFP exclude part III
															//exclude part III for total paper passed															
														}else{
															$paper_pass=$paper_pass+1;
														}	
																										
														$paper_pass_bytype = $paper_pass_bytype+1;
													}

													if($student_subject['IdSemesterMain']==$formData['IdSemester']){
														//to check if student is register at current semester
														$sub_type['subject_list'][$key_subject]['reg_cur_sem']=true;
													}else{
														$sub_type['subject_list'][$key_subject]['reg_cur_sem']=false;
													}
												}
											}
											
										}else{
											$sub_type['subject_list'][$key_subject]['Active']='';
											$sub_type['subject_list'][$key_subject]['grade_name']='-';
											$sub_type['subject_list'][$key_subject]['grade_point']='-';
											$sub_type['subject_list'][$key_subject]['grade']='-';
											$sub_type['subject_list'][$key_subject]['status']='4'; //x register
										}			
								}//end foreach loop subject under subject type
								
								
								
								
								//get subtype summary for GS -----------------------------
								if($semester['IdScheme']==11){ //GS	

									//get overall total ch registered
									//if($sub_type['SubjectType']==271){	
									//	$total_ch_registered = $examDb->getTotalResearchRegistered($student['IdStudentRegistration'],$subject['IdSubject']);
									//}
										
										
									if(count($my_regsub_arr)>0){
										$total_ch_register_bytype = $sregsubDB->getTotalCreditHourRegisteredByType($student['IdStudentRegistration'],$my_sub_arr,$sub_type['SubjectType']);
										//$total_ch_register_bytype = ($total_ch_register_result['total_ch_registered']!='') ? $total_ch_register_result['total_ch_registered']:0;
									}else{
										$total_ch_register_bytype = 0;
									}
									
									if($sub_type['SubjectType']==394){ //CORE
										$listSubjectType[$key_subtype]['ch_registered']=$total_ch_register_bytype;
										$listSubjectType[$key_subtype]['ch_completed']=$summary['credit_hours_completed_core'];
										$listSubjectType[$key_subtype]['ch_required']=$summary['credit_hours_required_core'];
										$listSubjectType[$key_subtype]['ch_remaining']=$summary['credit_hours_remaining_core'];
									}else if($sub_type['SubjectType']==275){ //elective
										$listSubjectType[$key_subtype]['ch_registered']=$total_ch_register_bytype;
										$listSubjectType[$key_subtype]['ch_completed']=$summary['credit_hours_completed_elective'];
										$listSubjectType[$key_subtype]['ch_required']=$summary['credit_hours_required_elective'];
										$listSubjectType[$key_subtype]['ch_remaining']=$summary['credit_hours_remaining_elective'];
									}else if($sub_type['SubjectType']==271){ //research										
										$listSubjectType[$key_subtype]['ch_registered']=$total_ch_register_bytype;
										$listSubjectType[$key_subtype]['ch_completed']=$summary['credit_hours_completed_research'];
										$listSubjectType[$key_subtype]['ch_required']=$summary['credit_hours_required_research'];
										$listSubjectType[$key_subtype]['ch_remaining']=$summary['credit_hours_remaining_research'];
									}
									
									//overall total
									$overall_total_credit_hour_registered = $overall_total_credit_hour_registered+$total_ch_register_bytype;
								}
								//end get subtype summary for GS -----------------------------
								
								
							
								
								//get total grade per subject type
								if(count($my_regsub_arr)>0){
									$total_bygrade = $examDb->getTotalApprovedGrade($student['IdStudentRegistration'],$formData['IdSemester'],$sub_type['subject_list'],$my_regsub_arr);
								}else{
									$total_bygrade = array();
								}
								$listSubjectType[$key_subtype]['total_bygrade']=$total_bygrade;
								
								
								$listSubjectType[$key_subtype]['subject_list']=$sub_type['subject_list'];								
								$listSubjectType[$key_subtype]['total_paper']=$total_paper;								
								$listSubjectType[$key_subtype]['total_paper_pass']=$paper_pass_bytype;
								
								
							}//end loop sub type
							
							
							
							
							//get student summary for GS -----------------------------
							if($semester['IdScheme']==11){ //GS	
									
								//Elective/Core Credit Hours 
								$student_list[$key]['ec_ch_required']= $summary['credit_hours_required_core']+$summary['credit_hours_required_elective'];
								$student_list[$key]['ec_ch_completed']= $summary['credit_hours_completed_core']+$summary['credit_hours_completed_elective'];
	
								//Total Credit Hours
								$student_list[$key]['total_ch_completed'] =  $summary['credit_hours_completed_core'] + $summary['credit_hours_completed_elective'] + $summary['credit_hours_completed_research'];
								$student_list[$key]['total_ch_required'] = $summary['credit_hours_required_core']+$summary['credit_hours_required_elective']+$summary['credit_hours_required_research'];
								$total_ch_remaining = $summary['credit_hours_remaining_core']+$summary['credit_hours_remaining_elective']+$summary['credit_hours_remaining_research'];
								$student_list[$key]['total_ch_remaining'] = $total_ch_remaining;
								
								//Total CH registered
								//$total_ch_registered = $examDb->caTotalCHRegistered($student['IdStudentRegistration']);
								$student_list[$key]['total_ch_registered'] = $overall_total_credit_hour_registered;
								
								//get student grade
								$student_grade = $examDb->getFinalStudentGrade($student['IdStudentRegistration']);
								$student_list[$key]['cgpa']=$student_grade['sg_cgpa'];
						
								//Status
								/*if($total_ch_remaining==0){
									$student_list[$key]['profile_status']='Complete';
								}else{
									$student_list[$key]['profile_status']='Incomplete';
								}*/
								
							} //end get student summary for GS -----------------------------

								
							$student_list[$key]['subject']=$listSubjectType;
							$student_list[$key]['paper_pass']=$paper_pass;
								
						}//end foreach student
						
						
					
					}//end count
					
					
					//echo '<pre>';
					//print_r($student_list);					
					
					$this->view->student_list = $student_list;
					if(isset($formData['IdProgramScheme'])){
						$this->view->scheme = implode($formData['IdProgramScheme'],',');
					}
					//$session->ca_formData = $formData;
					//$session->ca_student_list  = $student_list;					
					//$session->ca_subject_type_list  = $listSubjectType;	
					
								 
             }//end form
             
         }
         
         $this->view->form = $form;
            
     }
     
     
     
     public function printCourseAuditAction(){
	 	
     		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
		
       
        
    	$this->_helper->layout->disableLayout();
         if($this->getRequest()->isPost()){
        	

			 $formData = $this->getRequest()->getPost();
			 
			 $semesterDB = new Registration_Model_DbTable_Semester();
			 $landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			 $landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			 $landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			 $sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
			 $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			 $icampusDB = new icampus_Function_Application_AcademicProgress();
			 $subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
			 $examDb = new Reports_Model_DbTable_ExamReport();
			 $gradeDB = new Examination_Model_DbTable_Grade();
			 $studentSemesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
			 $sregsubDB = new Registration_Model_DbTable_Studentregsubjects();
			 $semesterDB = new Registration_Model_DbTable_Semester();	
             //if($form->isValid($formData))	{
				 
					//$form->populate($formData);
					
					$this->view->IdProgram = $formData['IdProgram'];
					
					$i=0;
					$j=0;
					$allblocklandscape=null;
					$allsemlandscape=null;
				
             		$activeLandscape=$landscapeDB->getAllLandscape($formData["IdProgram"]);
					
					foreach($activeLandscape as $actl)
					{
						if($actl["LandscapeType"]!=44)
						{
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}
						elseif($actl["LandscapeType"]==44)
						{
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
		
					if(is_array($allsemlandscape))
					{
						if($formData["IdProgram"]==5){
							$listSubjectType = $examDb->getPartList($allsemlandscape);
							//echo 'In Progress....';
							//exit;
						}else{
							$listSubjectType=$sofferedDB->getLandscapeCourse($allsemlandscape, $formData,$formData['IdSemester']);
						}
						
					}

					
					if(count($listSubjectType)>0){						
						foreach($listSubjectType as $index=>$subtype){	
								
								$pgrade = array();
								$id_subject = explode(',',$subtype['IdSubjects'])	;				
								$listSubjectType[$index]['subject_list'] = $subjectDb->getSubjectListIn($id_subject);
								
								// ========== CIFP special grade display by part x sama utk part 3  ========== 
								if($formData['IdProgram']==5){ //CIFP	
									
									//terpaksa aku harcode memandangkan permintaan yg merepek2
									if($subtype['block']==3){
										
										$pgrade[0]['Grade']='P';
										$pgrade[0]['GradeName']='P';
										$pgrade[0]['GradeFullName']='Pass';
										
										$pgrade[1]['Grade']='F';
										$pgrade[1]['GradeName']='F';
										$pgrade[1]['GradeFullName']='Fail';
										
									}else{
										
										$pgrade[0]['Grade']='PD';
										$pgrade[0]['GradeName']='PD';
										$pgrade[0]['GradeFullName']='PD';
										
										$pgrade[1]['Grade']='P';
										$pgrade[1]['GradeName']='P';
										$pgrade[1]['GradeFullName']='P';
										
										$pgrade[2]['Grade']='A';
										$pgrade[2]['GradeName']='A';
										$pgrade[2]['GradeFullName']='A';
										
										$pgrade[3]['Grade']='B';
										$pgrade[3]['GradeName']='B';
										$pgrade[3]['GradeFullName']='B';
										
										$pgrade[4]['Grade']='EX';
										$pgrade[4]['GradeName']='EX';
										$pgrade[4]['GradeFullName']='EX';
										
										$pgrade[5]['Grade']='F';
										$pgrade[5]['GradeName']='F';
										$pgrade[5]['GradeFullName']='F';
									}									
									// ========== END CIFP special grade display by part x sama utk part 3  ========== 
								}								
								else 
								if($formData['IdProgram']==2){
									    //MIFP
					
										$pgrade[0]['Grade']='A';
										$pgrade[0]['GradeName']='A';
										$pgrade[0]['GradeFullName']='A';
										
										$pgrade[1]['Grade']='A-';
										$pgrade[1]['GradeName']='A-';
										$pgrade[1]['GradeFullName']='A-';
										
										$pgrade[2]['Grade']='B+';
										$pgrade[2]['GradeName']='B+';
										$pgrade[2]['GradeFullName']='B+';
										
										$pgrade[3]['Grade']='B';
										$pgrade[3]['GradeName']='B';
										$pgrade[3]['GradeFullName']='B';
										
										$pgrade[4]['Grade']='CT';
										$pgrade[4]['GradeName']='CT';
										$pgrade[4]['GradeFullName']='CT';

										$pgrade[5]['Grade']='EX';
										$pgrade[5]['GradeName']='EX';
										$pgrade[5]['GradeFullName']='EX';
										
										$pgrade[6]['Grade']='F';
										$pgrade[6]['GradeName']='F';
										$pgrade[6]['GradeFullName']='F';
								}
								
								$listSubjectType[$index]['gradesetup']=$pgrade;
							
						}//end loop subtype
					}						
					$this->view->subject_type_list = $listSubjectType;
							
	
					//semester
					$semesterDB = new Registration_Model_DbTable_Semester();
					$semester = $semesterDB->getData($formData['IdSemester']);
					$semester_scheme = $semester['IdScheme'];
					$this->view->semester = $semester;
												
					
					$gradesetup= $gradeDB->getGradeSetup($formData['IdProgram'],$formData['IdSemester']);
					$mgrade['Grade']='CT';
					$mgrade['GradeName']='CT';						
					array_push($gradesetup,$mgrade);

					 $mgrade2['Grade']='EX';
					 $mgrade2['GradeName']='EX';
					 array_push($gradesetup,$mgrade2);
												
					//just assign default value
					foreach($gradesetup as $keygrade=>$g){					
						$gradesetup[$keygrade]['total']=0;
					}				
					$this->view->gradesetup_list = $gradesetup;
					//$session->ca_gradesetup_list  = $gradesetup;
									
					
					
					//get student list
					$student_list = $examDb->getStudentCourseAudit($formData,$semester['IdScheme']);
					
					if(count($student_list)>0){
													
						foreach($student_list as $key=>$student){

							$flag_pppart_taken = false;	
							$flag_paper_taken  = false;	
							$overall_total_credit_hour_registered = 0;
							
							//student year of study							
							$student_list[$key]['year_of_study'] = $examDb->countYearOfStudy($student['IdStudentRegistration'], $semester['IdScheme']);
							
							//total semester status
							$semester_register = $examDb->getActiveRegisteredSemester($student['IdStudentRegistration']);
							$student_list[$key]['semester_active']=count($semester_register);
											
							//Summary
							$summary = $icampusDB->getExpectedGraduateStudent($student['IdStudentRegistration'],'caudit');
							
							//get remaining paper
							if($semester['IdScheme']==1){								
								if($formData['IdProgram']==5){
									
									//CIFP
									$total_remaining_all_part = abs($summary['remaining_part1'])+abs($summary['remaining_part2'])+abs($summary['remaining_part3']);
									
									$complete_p1p2 = (abs($summary['remaining_part1'])+abs($summary['remaining_part2']));
									
									$student_list[$key]['complete_all_part']= ($total_remaining_all_part == 0) ? 'YES':'NO';
									$student_list[$key]['complete_part1']   = ($summary['remaining_part1'] == 0) ? 'YES':'NO';
									$student_list[$key]['complete_part2']   = ($summary['remaining_part2'] == 0) ? 'YES':'NO';
									$student_list[$key]['complete_p1p2']    = ($complete_p1p2 == 0)  ? 'YES':'NO';
									$student_list[$key]['complete_part3']   = ($summary['remaining_part3'] == 0) ? 'YES':'NO';
									
									$student_list[$key]['remaining_paper']  = $complete_p1p2;
									$student_list[$key]['remaining_pppart'] = ($summary['remaining_part3'] == 0) ? 'NO':'YES';
							
									$student_list[$key]['remaining_papers'] = $summary['this_chr'];
									
									$registered_current_semester = $sregsubDB->getTotalPaperRegistered($student['IdStudentRegistration'],$formData['IdSemester']);
									$student_list[$key]['total_registering'] = count($registered_current_semester);
									
								}else /*if($formData['IdProgram']==2)*/{
								
									//MIFP
									$registered_current_semester = $sregsubDB->getTotalPaperRegistered($student['IdStudentRegistration'],$formData['IdSemester']);
									$student_list[$key]['total_registering'] = count($registered_current_semester);
							
									$student_list[$key]['remaining_papers']=$summary['remaining_paper_mifp'];
								}								
							}
							
							
							$paper_pass = 0;							
							foreach($listSubjectType as $key_subtype=>$sub_type){

								$total_paper = 0;	
								$paper_pass_bytype = 0;
								$my_regsub_arr = array();
								$my_sub_arr = array();	
								$total_ch_register_bytype = 0;							
								foreach($sub_type['subject_list'] as $key_subject=>$subject){																
										
										//get grade each subject
										$student_subject = $examDb->getStudentRegSubjectData($student['IdStudentRegistration'],$subject['IdSubject'],$formData['IdProgram'],$student['IdLandscape'],$sub_type['SubjectType']);
										
										if(isset($student_subject)){
											
											if($student_subject['IdStudentRegSubjects']!=''){
												array_push($my_regsub_arr,$student_subject['IdStudentRegSubjects']);
												array_push($my_sub_arr,$student_subject['IdSubject']);
											}
																						
											if($student_subject['exam_status']=='CT' || $student_subject['exam_status']=='EX' || $student_subject['exam_status']=='U' ){
												$sub_type['subject_list'][$key_subject]['grade_name']=$student_subject['exam_status'];
												$sub_type['subject_list'][$key_subject]['grade_point']='';	
												$sub_type['subject_list'][$key_subject]['grade']=$student_subject['exam_status'];
												$sub_type['subject_list'][$key_subject]['status']=2;
												$sub_type['subject_list'][$key_subject]['Active']=$student_subject['Active'];
												
												if($student_subject['exam_status']!='U' && $student_subject['exam_status']!='EX'){
													$total_paper=$total_paper+1;													
													$paper_pass_bytype = $paper_pass_bytype+1;
													
													if(isset($sub_type['block']) && $sub_type['block']==3 && $formData['IdProgram']==5 ){ //MIFP exclude part III
														//exclude part III for total paper passed
													}else{
														$paper_pass=$paper_pass+1;
													}
												}
												
												if($student_subject['IdSemesterMain']==$formData['IdSemester']){
													//to check if student is register at current semester
													$sub_type['subject_list'][$key_subject]['reg_cur_sem']=true;
												}else{
													$sub_type['subject_list'][$key_subject]['reg_cur_sem']=false;
												}
											
											}else{
												$sub_type['subject_list'][$key_subject]['grade_name']=$student_subject['grade_name'];
												$sub_type['subject_list'][$key_subject]['grade_point']=$student_subject['grade_point'];													
												$sub_type['subject_list'][$key_subject]['status']=$student_subject['mark_approval_status'];
												$sub_type['subject_list'][$key_subject]['Active']=$student_subject['Active'];
												
												if($sub_type['SubjectType']==271){													
													$sub_type['subject_list'][$key_subject]['grade']=$student_subject['exam_status'];
													$sub_type['subject_list'][$key_subject]['status']=2;
												}else{
													
													if( $student_subject['IdSubject'] == 84 || $student_subject['IdSubject']==85 ){
														if(isset($student_subject['grade_name']) && $student_subject['grade_name']=='P'){
															$sub_type['subject_list'][$key_subject]['grade']=trim($student_subject['grade_name']).' - Pass';
														}
													}else{
														//geli betoll kejap nak kejap x nak off kan temporary kang nak kang hangin wei
														//$sub_type['subject_list'][$key_subject]['grade']=$student_subject['final_course_mark'].' ['.trim($student_subject['grade_name']).']';
														$sub_type['subject_list'][$key_subject]['grade']=trim($student_subject['grade_name']);
													}
												}
												
												if($student_subject['mark_approval_status']==2){
													$total_paper=$total_paper+1;
													
													if(trim($student_subject['grade_name'])!='' && trim($student_subject['grade_name'])!='F'){
														
														if($formData['IdProgram']==5 && $sub_type['block']==3){ //CIFP exclude part III
															//exclude part III for total paper passed															
														}else{
															$paper_pass=$paper_pass+1;
														}	
																										
														$paper_pass_bytype = $paper_pass_bytype+1;
													}

													if($student_subject['IdSemesterMain']==$formData['IdSemester']){
														//to check if student is register at current semester
														$sub_type['subject_list'][$key_subject]['reg_cur_sem']=true;
													}else{
														$sub_type['subject_list'][$key_subject]['reg_cur_sem']=false;
													}
												}
											}
											
										}else{
											$sub_type['subject_list'][$key_subject]['Active']='';
											$sub_type['subject_list'][$key_subject]['grade_name']='-';
											$sub_type['subject_list'][$key_subject]['grade_point']='-';
											$sub_type['subject_list'][$key_subject]['grade']='-';
											$sub_type['subject_list'][$key_subject]['status']='4'; //x register
										}			
								}//end foreach loop subject under subject type
								
								
								
								
								//get subtype summary for GS -----------------------------
								if($semester['IdScheme']==11){ //GS	

									//get overall total ch registered
									//if($sub_type['SubjectType']==271){	
									//	$total_ch_registered = $examDb->getTotalResearchRegistered($student['IdStudentRegistration'],$subject['IdSubject']);
									//}
										
										
									if(count($my_regsub_arr)>0){
										$total_ch_register_bytype = $sregsubDB->getTotalCreditHourRegisteredByType($student['IdStudentRegistration'],$my_sub_arr,$sub_type['SubjectType']);
										//$total_ch_register_bytype = ($total_ch_register_result['total_ch_registered']!='') ? $total_ch_register_result['total_ch_registered']:0;
									}else{
										$total_ch_register_bytype = 0;
									}
									
									if($sub_type['SubjectType']==394){ //CORE
										$listSubjectType[$key_subtype]['ch_registered']=$total_ch_register_bytype;
										$listSubjectType[$key_subtype]['ch_completed']=$summary['credit_hours_completed_core'];
										$listSubjectType[$key_subtype]['ch_required']=$summary['credit_hours_required_core'];
										$listSubjectType[$key_subtype]['ch_remaining']=$summary['credit_hours_remaining_core'];
									}else if($sub_type['SubjectType']==275){ //elective
										$listSubjectType[$key_subtype]['ch_registered']=$total_ch_register_bytype;
										$listSubjectType[$key_subtype]['ch_completed']=$summary['credit_hours_completed_elective'];
										$listSubjectType[$key_subtype]['ch_required']=$summary['credit_hours_required_elective'];
										$listSubjectType[$key_subtype]['ch_remaining']=$summary['credit_hours_remaining_elective'];
									}else if($sub_type['SubjectType']==271){ //research										
										$listSubjectType[$key_subtype]['ch_registered']=$total_ch_register_bytype;
										$listSubjectType[$key_subtype]['ch_completed']=$summary['credit_hours_completed_research'];
										$listSubjectType[$key_subtype]['ch_required']=$summary['credit_hours_required_research'];
										$listSubjectType[$key_subtype]['ch_remaining']=$summary['credit_hours_remaining_research'];
									}
									
									//overall total
									$overall_total_credit_hour_registered = $overall_total_credit_hour_registered+$total_ch_register_bytype;
								}
								//end get subtype summary for GS -----------------------------
								
								
							
								
								//get total grade per subject type
								if(count($my_regsub_arr)>0){
									$total_bygrade = $examDb->getTotalApprovedGrade($student['IdStudentRegistration'],$formData['IdSemester'],$sub_type['subject_list'],$my_regsub_arr);
								}else{
									$total_bygrade = array();
								}
								$listSubjectType[$key_subtype]['total_bygrade']=$total_bygrade;
								
								
								$listSubjectType[$key_subtype]['subject_list']=$sub_type['subject_list'];								
								$listSubjectType[$key_subtype]['total_paper']=$total_paper;								
								$listSubjectType[$key_subtype]['total_paper_pass']=$paper_pass_bytype;
								
								
							}//end loop sub type
							
							
							
							
							//get student summary for GS -----------------------------
							if($semester['IdScheme']==11){ //GS	
									
								//Elective/Core Credit Hours 
								$student_list[$key]['ec_ch_required']= $summary['credit_hours_required_core']+$summary['credit_hours_required_elective'];
								$student_list[$key]['ec_ch_completed']= $summary['credit_hours_completed_core']+$summary['credit_hours_completed_elective'];
	
								//Total Credit Hours
								$student_list[$key]['total_ch_completed'] =  $summary['credit_hours_completed_core'] + $summary['credit_hours_completed_elective'] + $summary['credit_hours_completed_research'];
								$student_list[$key]['total_ch_required'] = $summary['credit_hours_required_core']+$summary['credit_hours_required_elective']+$summary['credit_hours_required_research'];
								$total_ch_remaining = $summary['credit_hours_remaining_core']+$summary['credit_hours_remaining_elective']+$summary['credit_hours_remaining_research'];
								$student_list[$key]['total_ch_remaining'] = $total_ch_remaining;
								
								//Total CH registered
								//$total_ch_registered = $examDb->caTotalCHRegistered($student['IdStudentRegistration']);
								$student_list[$key]['total_ch_registered'] = $overall_total_credit_hour_registered;
								
								//get student grade
								$student_grade = $examDb->getFinalStudentGrade($student['IdStudentRegistration']);
								$student_list[$key]['cgpa']=$student_grade['sg_cgpa'];
						
								//Status
								/*if($total_ch_remaining==0){
									$student_list[$key]['profile_status']='Complete';
								}else{
									$student_list[$key]['profile_status']='Incomplete';
								}*/
								
							} //end get student summary for GS -----------------------------

								
							$student_list[$key]['subject']=$listSubjectType;
							$student_list[$key]['paper_pass']=$paper_pass;
								
						}//end foreach student
						
						
					
					}//end count
					
					
					//echo '<pre>';
					//print_r($student_list);					
					
					$this->view->student_list = $student_list;
					if(isset($formData['IdProgramScheme'])){
						$this->view->scheme = implode($formData['IdProgramScheme'],',');
					}
					//$session->ca_formData = $formData;
					//$session->ca_student_list  = $student_list;					
					//$session->ca_subject_type_list  = $listSubjectType;	
					
								 
            // }//end form
             
         }
    	// $session = Zend_Registry::get('sis');
		
    	// $formData  = $session->ca_formData;
    	// $this->view->IdProgram = $formData['IdProgram'];
    	
    	// $this->view->student_list  = $session->ca_student_list;
		// $this->view->gradesetup_list = $session->ca_gradesetup_list;
		// $this->view->subject_type_list = $session->ca_subject_type_list;
    	
    	$semesterDB = new Registration_Model_DbTable_Semester();
		$semester = $semesterDB->getData($formData['IdSemester']);
		$this->view->semester = $semester;
		
		/*echo '<pre>';			
		print_r($session->ca_student_list);
		print_r($session->ca_gradesetup_list);
		print_r($session->ca_subject_type_list);*/
		
        $this->view->filename = date('Ymd').'_course_audit.xls';
    }
    
}