<?php
highcharts_init();

class Reports_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->statsDb = new Reports_Model_DbTable_Stats();
    }

    public function indexAction()
    {
		
	
		//title
		$this->view->title = $this->view->translate('Reports');

		//stats
		$total_applicants = $this->statsDb->getTotalApplicants();
		$total_students = $this->statsDb->getTotalStudents();
		$total_active = $this->statsDb->getTotalStudents( array('profileStatus' => 92) );
		$total_quit = $this->statsDb->getTotalStudents( array('profileStatus' => 249) );
		$total_terminated = $this->statsDb->getTotalStudents( array('profileStatus' => 94) );
		$total_completed = $this->statsDb->getTotalStudents( array('profileStatus' => 248) );
		$total_sponsored = $this->statsDb->getTotalSponsored();

		//views
		$this->view->total_applicants = $total_applicants;
		$this->view->total_students = $total_students;
		$this->view->total_active = $total_active;
		$this->view->total_quit = $total_quit;
		$this->view->total_terminated = $total_terminated;
		$this->view->total_completed = $total_completed;
		$this->view->total_sponsored = $total_sponsored;
		

		
		$db = getDb();
		$progDb = new GeneralSetup_Model_DbTable_Program();
		$programmes = $progDb->fngetProgramDetails();
		
		$months = array(
		    1=>'January',
		    2=>'February',
		    3=>'March',
		    4=>'April',
		    5=>'May',
		    6=>'June',
		    7=>'July ',
		    8=>'August',
		    9=>'September',
		    10=>'October',
		    11=>'November',
		    12=>'December'
		);
		
		//APPLICATIONS
		$results = $db->fetchAll('SELECT COUNT( * ) AS total, DAY( at_submit_date ) AS
									DAY , MONTH( at_submit_date ) AS
									MONTH FROM `applicant_transaction`
									WHERE at_submit_date IS NOT NULL AND YEAR( at_submit_date ) = YEAR(NOW())
									GROUP BY MONTH ( at_submit_date )  
									ORDER BY at_submit_date ASC
									LIMIT 30');
		
		$getmonths = $totals = array();

		foreach ( $results as $row )
		{
			$getmonths[] = $months[$row['MONTH']];
			$totals[] = intval( $row['total'] );
		}
		
		$chartData = $totals;
		$series1 = new HighRollerSeriesData();
		$series1->addName('Applicants')->addData($chartData);

		$linechart = new HighRollerAreaChart();
		$linechart->chart->renderTo = 'chart_applicant';
		$linechart->title->text = '';
		$linechart->credits['enabled'] = false;
		$linechart->xAxis['categories'] = $getmonths;
		$linechart->yAxis['title']['text'] = '';
		$linechart->addSeries($series1); 

		$this->view->chart = $linechart;

		


		

		//STUDENTS
		$results2 = $db->fetchAll('SELECT COUNT( * ) AS total, DAY( UpdDate ) AS
									DAY , MONTH( UpdDate ) AS
									MONTH FROM `tbl_studentregistration`
									WHERE UpdDate IS NOT NULL AND YEAR( UpdDate ) = YEAR(NOW())
									GROUP BY MONTH ( UpdDate )  
									ORDER BY UpdDate ASC
									LIMIT 30');

		$getmonths = $totals = array();

		foreach ( $results2 as $row )
		{
			$getmonths[] = $months[$row['MONTH']];
			$totals[] = intval( $row['total'] );
		}
		
		$chartData = $totals;
		$series2 = new HighRollerSeriesData();
		$series2->addName('Students')->addData($chartData);

		$linechart2 = new HighRollerAreaChart();
		$linechart2->chart->renderTo = 'chart_student';
		$linechart2->title->text = '';
		$linechart2->credits['enabled'] = false;
		$linechart2->xAxis['categories'] = $getmonths;
		$linechart2->yAxis['title']['text'] = '';
		$linechart2->addSeries($series2); 

		$this->view->chart2 = $linechart2;

		//APPLICANTS PIE
		$data = $dataProg = $chartData = array();
		$alltotal = 0;

		foreach ( $programmes as $prog )
		{
			$total = $this->statsDb->getApplicantsByProgramme($prog['IdProgram'], array('YEAR(at_submit_date)' => new Zend_Db_Expr('YEAR(NOW())')) );

			if ( $total > 0 )
			{
				$alltotal += $total;
				$dataProg[ $prog['IdProgram'] ] = $total;
			}
		}

		foreach ( $programmes as $prog )
		{
			if ( isset($dataProg[ $prog['IdProgram'] ]) ) 
			{
				$total = $dataProg[ $prog['IdProgram'] ];
				$percent = round( ($total / $alltotal * 100), 2);
				$chartData[] = array($prog['ProgramCode'], $percent);
			}
		}

		$series3 = new HighRollerSeriesData();
		$series3->addName('Students')->addData($chartData);

		$linechart3 = new HighRollerPieChart();
		$linechart3->chart->renderTo = 'chart_pie1';
		$linechart3->title->text = '';
		$linechart3->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
		$linechart3->credits['enabled'] = false;
		$linechart3->addSeries($series3); 

		$this->view->chart3 = $linechart3;

		//STUDENTS PIE
		$data = $dataProg = $chartData = array();
		$alltotal = 0;

		foreach ( $programmes as $prog )
		{
			$total = $this->statsDb->getStudentsByProgramme($prog['IdProgram'], array('YEAR(UpdDate)' => new Zend_Db_Expr('YEAR(NOW())')) );

			if ( $total > 0 )
			{
				$alltotal += $total;
				$dataProg[ $prog['IdProgram'] ] = $total;
			}
		}

		foreach ( $programmes as $prog )
		{
			if ( isset($dataProg[ $prog['IdProgram'] ]) ) 
			{
				$total = $dataProg[ $prog['IdProgram'] ];
				$percent = round( ($total / $alltotal * 100), 2);
				$chartData[] = array($prog['ProgramCode'], $percent);
			}
		}

		$series4 = new HighRollerSeriesData();
		$series4->addName('Students')->addData($chartData);

		$linechart4 = new HighRollerPieChart();
		$linechart4->chart->renderTo = 'chart_pie2';
		$linechart4->title->text = '';
		$linechart4->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
		$linechart4->credits['enabled'] = false;
		$linechart4->addSeries($series4); 

		$this->view->chart4 = $linechart4;
    }

	public function homeAction(){

	}
}

