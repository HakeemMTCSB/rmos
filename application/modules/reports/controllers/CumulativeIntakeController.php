<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

highcharts_init();

class Reports_CumulativeIntakeController extends Zend_Controller_Action{

    public function init(){
        $this->model = new Reports_Model_DbTable_CumulativeIntake();
    }

    public function indexAction(){
        //title
        $this->view->title = $this->view->translate('Cumulative Intake');
        
        $intakeList = $this->model->getIntakeList();
        $sum = 0;
        if ($intakeList){
            foreach ($intakeList as $key => $intakeLoop){
                $list = $this->model->getApplicantByIntake($intakeLoop['IdIntake']);
                $intakeList[$key]['total'] = intval(count($list));
                
                $intakeid[] = $intakeLoop['IdIntake'];
                $getintake[] = $intakeLoop['IntakeDesc'];
		$totals[] = intval(count($list));
                $sum = $sum+intval(count($list));
            }
        }
        
        if (count($intakeid) > 0){
            $list2 = $this->model->getApplicantByIntakeOther($intakeid);
            $getintake[] = 'Others';
            $totals[] = intval(count($list2));
            $sum = $sum+intval(count($list2));
            
            $data = array(
                'IdIntake' => 0,
                'total' => intval(count($list2)),
                'IntakeDesc' => 'Others'
            );
            
            array_push($intakeList, $data);
        }
        
        $this->view->intakeList = $intakeList;
        $this->view->sum = $sum;
        
        //graph part
        $chartData = $totals;
        $series1 = new HighRollerSeriesData();
        $series1->addName('Applicants')->addData($chartData);

        $linechart = new HighRollerAreaChart();
        $linechart->chart->renderTo = 'chart_applicant_intake';
        $linechart->title->text = '';
        $linechart->credits['enabled'] = false;
        $linechart->xAxis['categories'] = $getintake;
        $linechart->yAxis['title']['text'] = '';
        $linechart->addSeries($series1); 

        $this->view->chart = $linechart;
    }
    
    public function statusAction(){
        $this->view->title = $this->view->translate('Application Status');
        $intakeid = $this->_getParam('intake', 0);
        $this->view->intakeid = $intakeid;
        
        if ($intakeid != 0){
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $this->view->intakeInfo = $intakeinfo;
            
            $statusList = $this->model->getDefination(56);
            $list = $this->model->getApplicantByIntake($intakeid);
            $alltotal = intval(count($list));
            
            $sum = 0;
            
            if ($statusList){
                foreach ($statusList as $key => $statusLoop){
                    $appList = $this->model->getApplicantByStatusAndIntake($intakeid, $statusLoop['idDefinition']);
                    $statusList[$key]['total'] = intval(count($appList));
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($statusLoop['DefinitionDesc'], $percent);

                    $sum = $sum+intval(count($appList));
                }
            }
            
            $this->view->statusList = $statusList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_status';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }else{
            /*$intakeList = $this->model->getIntakeList();
            $intakeid = array();
            if ($intakeList){
                foreach ($intakeList as $key => $intakeLoop){
                    array_push($intakeid, $intakeLoop['IdIntake']);
                }
            }
            
            $statusList = $this->model->getDefination(56);
            $list = $this->model->getApplicantByIntakeOther($intakeid);
            $alltotal = intval(count($list));
            
            $sum = 0;
            
            if ($statusList){
                foreach ($statusList as $key => $statusLoop){
                    $appList = $this->model->getApplicantByStatusAndIntakeOthers($intakeid, $statusLoop['idDefinition']);
                    $statusList[$key]['total'] = intval(count($appList));
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($statusLoop['DefinitionDesc'], $percent);

                    $sum = $sum+intval(count($appList));
                }
            }
            
            $this->view->statusList = $statusList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_status';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;*/
        }
    }
    
    public function programmeAction(){
        $this->view->title = $this->view->translate('Programme');
        
        $intakeid = $this->_getParam('intake', 0);
        $statusid = $this->_getParam('status', 0);
        $this->view->intakeid = $intakeid;
        $this->view->statusid = $statusid;
        
        if ($intakeid != 0 && $statusid != 0){
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $statusInfo = $this->model->getDefinationById($statusid);
            $this->view->intakeInfo = $intakeinfo;
            $this->view->statusInfo = $statusInfo;
            
            $programList = $this->model->getProgramList();
            $list = $this->model->getApplicantByStatusAndIntake($intakeid, $statusid);
            $alltotal = intval(count($list));
            
            $sum = 0;
            
            if ($programList){
                foreach ($programList as $key=>$programLoop){
                    $appList = $this->model->getAppByIntakeStatusProg($intakeid, $statusid, $programLoop['IdProgram']);
                
                    $programList[$key]['total'] = intval(count($appList));
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($programLoop['ProgramCode'], $percent);

                    $sum = $sum+intval(count($appList));
                    
                    $programId[] = $programLoop['IdProgram'];
                }
            }
            
            if (count($programId) > 0){
                $total = $alltotal-$sum;
                if ($alltotal > 0){
                    $percent = round(($total / $alltotal * 100), 2);
                }else{
                    $percent = 0;
                }
                $chartData[] = array('Others', $percent);
                
                $sum = $sum+$total;

                $data = array(
                    'IdProgram' => 0,
                    'total' => $total,
                    'ProgramName' => 'Others'
                );

                array_push($programList, $data);
            }
            
            $this->view->programList = $programList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_program';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }
    }
    
    public function programmeSchemeAction(){
        $this->view->title = $this->view->translate('Programme Scheme');
        
        $intakeid = $this->_getParam('intake', 0);
        $statusid = $this->_getParam('status', 0);
        $programid = $this->_getParam('program', 0);
        $this->view->intakeid = $intakeid;
        $this->view->statusid = $statusid;
        $this->view->programid = $programid;
        
        if ($intakeid != 0 && $statusid != 0 && $programid != 0){
            
            $intakeinfo = $this->model->getIntakeById($intakeid);
            $statusInfo = $this->model->getDefinationById($statusid);
            $programInfo = $this->model->getProgramById($programid);
            $this->view->intakeInfo = $intakeinfo;
            $this->view->statusInfo = $statusInfo;
            $this->view->programInfo = $programInfo;
            
            $list = $this->model->getAppByIntakeStatusProg($intakeid, $statusid, $programid);
            $alltotal = intval(count($list));
            
            $programSchemeList = $this->model->getProgramSchemeList($programid);
            
            $sum = 0;
            
            if ($programSchemeList){
                foreach ($programSchemeList as $key=>$programSchemeLoop){
                    $appList = $this->model->getAppByIntakeStatusProgScheme($intakeid, $statusid, $programid, $programSchemeLoop['IdProgramScheme']);
                
                    $programSchemeList[$key]['total'] = intval(count($appList));
                    $total = intval(count($appList));
                    if ($alltotal > 0){
                        $percent = round(($total / $alltotal * 100), 2);
                    }else{
                        $percent = 0;
                    }
                    $chartData[] = array($programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt'], $percent);

                    $sum = $sum+intval(count($appList));
                }
            }
            
            $this->view->programSchemeList = $programSchemeList;
            $this->view->sum = $sum;
            
            $series1 = new HighRollerSeriesData();
            $series1->addName('Students')->addData($chartData);

            $piechart = new HighRollerPieChart();
            $piechart->chart->renderTo = 'chart_applicant_programscheme';
            $piechart->title->text = '';
            $piechart->tooltip['pointFormat'] = '<b>{point.percentage:.1f}%</b>';
            $piechart->credits['enabled'] = false;
            $piechart->addSeries($series1); 

            $this->view->chart = $piechart;
        }
    }
}
?>