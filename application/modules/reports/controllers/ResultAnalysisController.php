<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 16/12/2015
 * Time: 10:16 AM
 */
class Reports_ResultAnalysisController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->tplfolder = defined('TEMPLATE_PATH') ? TEMPLATE_PATH : DOCUMENT_PATH;
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Reports_Model_DbTable_ResultAnalysis();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Result Analysis');

        $examReportDB = new Reports_Model_DbTable_ExamReport();
        $gradeDB = new Examination_Model_DbTable_Grade();
        $db = Zend_Db_Table::getDefaultAdapter();

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $form = new Reports_Form_ResultAnalysis(array('programid'=>$formData['Program']));
            $this->view->form = $form;

            $form->populate($formData);

            $semesterList = $this->model->getSemester($formData['Semester']);

            $select2 = $db->select()
                ->from(array('p'=>'tbl_program'))
                ->where('p.IdProgram = ?',$formData['Program']);
            $program = $db->fetchRow($select2);

            $this->view->program = $program;

            $resultAnalysisData = array();

            if (!isset($formData['Subject'])) {
                $select = $db->select()
                    ->from(array('s' => 'tbl_subjectmaster'))
                    ->where('s.IdFaculty = ?', $program['IdCollege'])
                    ->order('s.SubCode ASC');
                $course = $db->fetchAll($select);
            }else{
                $select = $db->select()
                    ->from(array('s' => 'tbl_subjectmaster'))
                    ->where('s.IdSubject IN (?)', $formData['Subject'])
                    ->order('s.SubCode ASC');
                $course = $db->fetchAll($select);
            }

            if ($course){
                foreach ($course as $coursekey => $courseLoop){
                    $gradeheaderarr = array();
                    $attendanceheaderarr = array();

                    if ($semesterList){
                        foreach ($semesterList as $semesterLoop){

                            //get grade list
                            $grade_list = $gradeDB->getGradeSetup($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject']);

                            $overall_total = 0;
                            $gradearr = array();
                            $attendancearr = array();

                            if ($grade_list){
                                foreach ($grade_list as $gradekey => $grade_loop){

                                    //get total number of student by subject
                                    $total_student = $examReportDB->getTotalStudentBySubject($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject']);

                                    //get total number of student by grade
                                    $total_student_grade = $examReportDB->getTotalStudentBySemGrade($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject'],$grade_loop['GradeName']);

                                    $grade_list[$gradekey]['total']=$total_student_grade;
                                    if($total_student==0 || $total_student_grade==0){
                                        $grade_list[$gradekey]['percentage']=0;
                                        $percentage = round(0.00, 2);
                                    }else{
                                        $grade_list[$gradekey]['percentage']= round( (($total_student_grade/$total_student)*100), 2);
                                        $percentage = round( (($total_student_grade/$total_student)*100), 2);
                                    }

                                    $gradearr[$grade_loop['GradeName']] = array(
                                        'Grade' => $grade_loop['Grade'],
                                        'GradeName' => $grade_loop['GradeName'],
                                        'total' => $total_student_grade,
                                        'percentage' => $percentage
                                    );

                                    $overall_total = $overall_total+$total_student_grade;

                                    if (!in_array($grade_loop['GradeName'], $gradeheaderarr)) {
                                        $gradeheaderarr[] = $grade_loop['GradeName'];
                                    }
                                }
                            }

                            $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['grade'] = $gradearr;
                            $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['total'] = $overall_total;

                            $failedStudent = $this->model->getStudentBySemGrade($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject'],'F');

                            if ($failedStudent){
                                $failedStudentArr = array();
                                foreach ($failedStudent as $failedStudentLoop){
                                    $failedStudentArr[] = $failedStudentLoop['IdStudentRegistration'];
                                }

                                $student_attendance = $this->model->getFailStudent($courseLoop['IdSubject'], $failedStudentArr, $semesterLoop['IdSemesterMaster']);

                                foreach($student_attendance as $attendkey=>$attendance){
                                    if (!in_array($attendance['attendance'], $attendanceheaderarr)) {
                                        $attendanceheaderarr[] = $attendance['attendance'];
                                    }

                                    //calculate percentage
                                    $attendancearr[$attendance['attendance']]['numberofstudent'] = $attendance['numberofstudent'];
                                    $attendancearr[$attendance['attendance']]['percentage']=round((($attendance['numberofstudent']/count($failedStudent))*100),2);
                                }
                                $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['attendance_fail_analysis'] = $attendancearr;
                                $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['attendance_fail_analysis_total'] = count($failedStudent);
                            }

                            $course[$coursekey]['attheader'] = $attendanceheaderarr;
                        }
                    }
                }
            }

            $this->view->gradeheaderarr = $gradeheaderarr;
            $this->view->course = $course;
        }else{
            $form = new Reports_Form_ResultAnalysis();
            $this->view->form = $form;
        }
    }

    public function clearAction(){
        $this->_redirect($this->baseUrl . '/reports/result-analysis/index');
        exit;
    }

    public function printAction(){
        $examReportDB = new Reports_Model_DbTable_ExamReport();
        $gradeDB = new Examination_Model_DbTable_Grade();
        $db = Zend_Db_Table::getDefaultAdapter();

        global $globalgradeheaderarr;
        global $globalcourse;
        global $globalprogram;
        global $globalsvg;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            //echo $formData['svg'][104];
            /*foreach ($formData["svg"] as $r){
                //echo $r;
                echo '<img src="'.$r.'">';
            }
            exit;*/

            $svg = $formData['svg'];
            $globalsvg = $svg;

            $form = new Reports_Form_ResultAnalysis(array('programid'=>$formData['Program']));
            $this->view->form = $form;

            $form->populate($formData);

            $semesterList = $this->model->getSemester($formData['Semester']);

            $select2 = $db->select()
                ->from(array('p'=>'tbl_program'))
                ->where('p.IdProgram = ?',$formData['Program']);
            $program = $db->fetchRow($select2);

            $globalprogram = $program;

            $resultAnalysisData = array();

            if (!isset($formData['Subject'])) {
                $select = $db->select()
                    ->from(array('s' => 'tbl_subjectmaster'))
                    ->where('s.IdFaculty = ?', $program['IdCollege'])
                    ->order('s.SubCode ASC');
                $course = $db->fetchAll($select);
            }else{
                $select = $db->select()
                    ->from(array('s' => 'tbl_subjectmaster'))
                    ->where('s.IdSubject IN (?)', $formData['Subject'])
                    ->order('s.SubCode ASC');
                $course = $db->fetchAll($select);
            }

            if ($course){
                foreach ($course as $coursekey => $courseLoop){
                    $gradeheaderarr = array();
                    $attendanceheaderarr = array();

                    if ($semesterList){
                        foreach ($semesterList as $semesterLoop){
                            //get grade list
                            $grade_list = $gradeDB->getGradeSetup($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject']);

                            $overall_total = 0;
                            $gradearr = array();
                            $attendancearr = array();

                            if ($grade_list){
                                foreach ($grade_list as $gradekey => $grade_loop){

                                    //get total number of student by subject
                                    $total_student = $examReportDB->getTotalStudentBySubject($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject']);

                                    //get total number of student by grade
                                    $total_student_grade = $examReportDB->getTotalStudentBySemGrade($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject'],$grade_loop['GradeName']);

                                    $grade_list[$gradekey]['total']=$total_student_grade;
                                    if($total_student==0 || $total_student_grade==0){
                                        $grade_list[$gradekey]['percentage']=0;
                                        $percentage = round(0.00, 2);
                                    }else{
                                        $grade_list[$gradekey]['percentage']= round( (($total_student_grade/$total_student)*100), 2);
                                        $percentage = round( (($total_student_grade/$total_student)*100), 2);
                                    }

                                    $gradearr[$grade_loop['GradeName']] = array(
                                        'Grade' => $grade_loop['Grade'],
                                        'GradeName' => $grade_loop['GradeName'],
                                        'total' => $total_student_grade,
                                        'percentage' => $percentage
                                    );

                                    $overall_total = $overall_total+$total_student_grade;

                                    if (!in_array($grade_loop['GradeName'], $gradeheaderarr)) {
                                        $gradeheaderarr[] = $grade_loop['GradeName'];
                                    }
                                }
                            }

                            $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['grade'] = $gradearr;
                            $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['total'] = $overall_total;

                            $failedStudent = $this->model->getStudentBySemGrade($formData['Program'],$semesterLoop['IdSemesterMaster'],$courseLoop['IdSubject'],'F');

                            if ($failedStudent){
                                $failedStudentArr = array();
                                foreach ($failedStudent as $failedStudentLoop){
                                    $failedStudentArr[] = $failedStudentLoop['IdStudentRegistration'];
                                }

                                $student_attendance = $this->model->getFailStudent($courseLoop['IdSubject'], $failedStudentArr, $semesterLoop['IdSemesterMaster']);

                                foreach($student_attendance as $attendkey=>$attendance){
                                    if (!in_array($attendance['attendance'], $attendanceheaderarr)) {
                                        $attendanceheaderarr[] = $attendance['attendance'];
                                    }

                                    //calculate percentage
                                    $attendancearr[$attendance['attendance']]['numberofstudent'] = $attendance['numberofstudent'];
                                    $attendancearr[$attendance['attendance']]['percentage']=round((($attendance['numberofstudent']/count($failedStudent))*100),2);
                                }
                                $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['attendance_fail_analysis'] = $attendancearr;
                                $course[$coursekey]['sem'][$semesterLoop['SemesterMainName']]['attendance_fail_analysis_total'] = count($failedStudent);
                            }

                            $course[$coursekey]['attheader'] = $attendanceheaderarr;
                        }
                    }
                }
            }

            $globalgradeheaderarr = $gradeheaderarr;
            $globalcourse = $course;
        }

        $fieldValues = array(
            '$[LOGO]'=> "images/logo_text_high.png",
        );

        require_once 'dompdf_config.inc.php';

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        //template path
        $html_template_path = $this->tplfolder."/template/result_analysis.html";

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);
        }
        //echo $html; exit;
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'landscape');
        $dompdf->render();

        //output filename
        $output_filename = 'result_analysis.pdf';
        $dompdf->stream($output_filename);

        exit;
    }
}