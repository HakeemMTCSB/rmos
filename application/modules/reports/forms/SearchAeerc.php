<?php
class Reports_Form_SearchAeerc extends Zend_Form
{
	
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
		
		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true,
		 	'onchange'=>'getProgramScheme()'
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester['value2']);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
			}
		}
		
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),
                        'required'=>true,
                        'onchange'=>'getProgramScheme()'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		
		 //Program Scheme
		$this->addElement('multiselect','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
		 	'required'=>true,
			'registerInArrayValidator'=>false
		));
		$this->IdProgramScheme->setAttrib( 'size',5);
    	$this->IdProgramScheme->setAttrib(  'style','width: 250px' );
	
		
		
		//Branch
		$this->addElement('select','IdBranch', array(
			'label'=>$this->getView()->translate('Branch'),
		    'required'=>false
		));
		
		$branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
				
		$this->IdBranch->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($branchDb->fnGetAllBranchList() as $branch){
			$this->IdBranch->addMultiOption($branch["key"],$branch["value"]);
		}
		
		
		//Part
		$this->addElement('select','Part', array(
			'label'=>$this->getView()->translate('Part'),
			'disabled'=>'disabled'
		));
				
		$this->Part->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		$this->Part->addMultiOption(1,1);
		$this->Part->addMultiOption(2,2);
		$this->Part->addMultiOption(3,3);		
		
		
       	//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
         
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>

