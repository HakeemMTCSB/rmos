<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 16/12/2015
 * Time: 10:23 AM
 */
class Reports_Form_ResultAnalysis extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $programid = $this->getAttrib('programid');

        //load model
        $model = new Registration_Model_DbTable_Studentregistration();
        $definationDB = new App_Model_General_DbTable_Definationms();
        $model2 = new Records_Model_DbTable_Report();

        //program
        $Program = new Zend_Form_Element_Select('Program');
        $Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->setAttrib('required', '');
        $Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getSubject(this.value); getSemester(this.value);');

        $Program->addMultiOption('', '-- Select --');

        $ProgramList = $model->getProgram();

        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $Program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }

        //program
        $Semester = new Zend_Form_Element_Select('Semester');
        $Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('required', '');
        $Semester->setAttrib('multiple', 'multiple');
        $Semester->removeDecorator("Label");

        //program
        $Subject = new Zend_Form_Element_Select('Subject');
        $Subject->removeDecorator("DtDdWrapper");
        $Subject->setAttrib('class', 'select');
        $Subject->setAttrib('multiple', 'multiple');
        //$Subject->setAttrib('required', '');
        $Subject->removeDecorator("Label");

        //$Subject->addMultiOption('', '-- Select --');

        if ($programid){
            $db = Zend_Db_Table::getDefaultAdapter();

            $select2 = $db->select()
                ->from(array('p'=>'tbl_program'))
                ->where('p.IdProgram = ?',$programid);
            $program = $db->fetchRow($select2);

            $select = $db->select()
                ->from(array('s'=>'tbl_subjectmaster'))
                ->where('s.IdFaculty = ?',$program['IdCollege'])
                ->order('s.SubCode ASC');
            $course = $db->fetchAll($select);

            if ($course){
                foreach ($course as $courseLoop){
                    $Subject->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
                }
            }

            $programInfo = $model->getProgramForSem($programid);
            $semList = $model->getSemBasedOnList($programInfo['IdScheme']);

            if ($semList){
                foreach ($semList as $semLoop){
                    $Semester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName']);
                }
            }
        }

        $this->addElements(array(
            $Program,
            $Semester,
            $Subject
        ));
    }
}