<?php 

class Reports_Form_SearchStudent extends Zend_Form {
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','mySearchform');						       
		
		//semester
		
		$this->addElement('select','IdSemester', array(
				'label'=>$this->getView()->translate('Semester'),
		        'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->getData() as $semester){
			$this->IdSemester->addMultiOption($semester["IdSemesterMaster"],$semester["SemesterMainName"]);
		}
		
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program Name')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData()  as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
		}
				
		
		//Student NIM/NAME
		$this->addElement('text','IdStudent', array(
			'label'=>$this->getView()->translate('Student NIM')
		));
		
	
		
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
		
	
}
?>