<?php
class Reports_Form_SearchStudentReport extends Zend_Form
{
	protected $_facultyid;
	
	public function setFacultyid($facultyid) {
		$this->_facultyid = $facultyid;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');

		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}
		
		
			//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program'),
		    'required'=>true
		));
		
		$programDb = new Registration_Model_DbTable_Program();		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}		
						
		/*//program scheme
		$this->addElement('select','IdProgramScheme', array(
					      'label'=>$this->getView()->translate('Programme Scheme'),
						  'required'=>true,
						  'registerInArrayValidator'=>false
		));						
		$this->IdProgramScheme->addMultiOption(null,"-- Please Select --");*/
		
		//start date
		$this->addElement('text','start_date', array(
			'label'=>$this->getView()->translate('Start Date'),
			'class'=>'datepicker'
		));
		
		//end date
		$this->addElement('text','end_date', array(
			'label'=>$this->getView()->translate('End Date'),
			'class'=>'datepicker'
		));
		
		
	               		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>