<?php
class Reports_Form_SearchPreaeerc extends Zend_Form
{
	
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
		//$this->setAction('/reports/exam-reports/exam-schedule');

		$this->addElement('hidden','display_option');
		
		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}
				

		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program Name'),
			'required'=>true,
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
		
		//Department
		/*$this->addElement('select','IdCollege', array(
                        'label'=>$this->getView()->translate('Department'), 
                        'required'=>true
                ));
		
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();

		$this->IdCollege->addMultiOption(null,"-- Please Select --");		

		foreach($collegeDb->getCollege()  as $college){
                    $this->IdCollege->addMultiOption($college["IdCollege"],$college["CollegeCode"].' - '.$college["CollegeName"]);
		}*/
		
		$this->addElement('text','SubjectName', array(
			'label'=>'Subject Name'
		));
		
		//name
		$this->addElement('text','SubjectCode', array(
			'label'=>'Course Code'
		));
		
	               		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>