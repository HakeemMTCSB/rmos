<?php
class Reports_Form_SearchExamSchedule extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
		//$this->setAction('/reports/exam-reports/exam-schedule');

		$this->addElement('hidden','display_option');
		
		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}
		
		
			//Program
		$this->addElement('select','ec_id', array(
			'label'=>$this->getView()->translate('Exam Center'),
		    'required'=>true
		));
		
		$examcenterdb = new GeneralSetup_Model_DbTable_ExamCenter();	
		$this->ec_id->addMultiOption(null,"-- Please Select --");		
		foreach($examcenterdb->getData() as $center){
			$this->ec_id->addMultiOption($center["ec_id"],$center["ec_name"]);
		}		
						
		
		
	               		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		/*$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));*/
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>