<?php
class Reports_Form_SearchAbsentees extends Zend_Form
{
	
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
		
		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester['value2']);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
			}
		}
		
		//Semester
		$this->addElement('select','attendance_status', array(
			'label'=>$this->getView()->translate('Status')
		));
		
		$this->attendance_status->addMultiOption(null,"-- All --");	
		$this->attendance_status->addMultiOption(398,"Absent");	
		$this->attendance_status->addMultiOption(396,"Absent with valid reason");		
		
		//Program
		$this->addElement('multiselect','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),
                        'required'=>true 
		));
    	$this->IdProgram->setAttrib( 'size',10);
    	$this->IdProgram->setAttrib(  'style','width: 350px' );
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
	
		
		
       	//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
         
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>

