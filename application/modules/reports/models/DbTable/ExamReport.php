<?php 
class Reports_Model_DbTable_ExamReport extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = '';
	protected $_primary = "";

	
	public function getGroupList($idSubject,$idSemester){
			
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		//is Dean?
    	$dean_program = $this->isDean();
    	
		$select = $db ->select()
					  ->from(array('cg'=>'tbl_course_tagging_group'))
					  ->join(array('cgp'=>'course_group_program'),'cgp.group_id = cg.IdCourseTaggingGroup',array('program_scheme_id','program_id'))
					  ->join(array('p'=>'tbl_program'), 'cgp.program_id = p.IdProgram',array('ProgramName'))
					  ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))	
					  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=cgp.program_scheme_id',array())	
					  ->join(array('def'=>'tbl_definationms'),'def.idDefinition=ps.mode_of_study',array('studymode'=>'DefinitionDesc'))			
					  ->where('IdSubject = ?',$idSubject)
					  ->where('IdSemester = ?',$idSemester)
					  ->group('cg.IdCourseTaggingGroup')
					  ->group('cgp.program_id')
					  ->order('cg.IdCourseTaggingGroup');
		
					  
	 	 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin				 	
	 	 	if(count($dean_program)>0){
		 		//this is dean
		 		$select->where('cgp.program_id IN (?)',$dean_program);
		 	}else{
		 		$select->where('cg.IdLecturer = ?',$auth->getIdentity()->IdStaff);
		 	}
		 }
		 
		
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	
	//pre-aeerc
	public function getStudentByGroup($idGroup,$idScheme=null,$idProgram=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//get scheme info for audit credit
		$select = $db->select()->from(array('ps'=>'tbl_program_scheme'),array('mode_of_study'))->where('ps.IdProgramScheme=?',$idScheme);
		$row    = $db->fetchRow($select);
		
		 $sql =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->join(array('ap'=>'student_profile'),'ap.id=sr.sp_id',array('appl_fname','appl_lname'))
	 				 ->join(array('d'=>'tbl_program'), 'sr.IdProgram = d.IdProgram')
		             ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		             ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		             ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
		             ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
	 				 ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
	 				 ->where('srs.Active != ?',3)
	 				 ->where('sr.profileStatus = ?',92)
	 				 ->where('sr.registrationId != ?',"1409999")	 			
	 				 ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")');
	 
	 	
	 	$sql2 =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->join(array('ap'=>'student_profile'),'ap.id=sr.sp_id',array('appl_fname','appl_lname'))
	 				 ->join(array('d'=>'tbl_program'), 'sr.IdProgram = d.IdProgram')
		             ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		             ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		             ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
		             ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
		             ->where('srs.audit_program IS NOT NULL')     
	 				 ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
	 				 ->where('srs.Active != ?',3)
	 				 ->where('sr.profileStatus = ?',92)
	 				 ->where('sr.registrationId != ?',"1409999")
	 				 ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")');

	 				 if(isset($idProgram) && $idProgram!=''){
	 				 	$sql->where('sr.IdProgram = ?',$idProgram);
	 				 	//$sql2->where('sr.IdProgram = ?',$idProgram);
	 				 }
	 				 
	 				  $select = $db->select()
				    		       ->union(array($sql, $sql2))
				    		       ->order('final_course_mark desc')
				                   ->order('appl_fname')
				                   ->order('appl_lname')
				                   ->order('registrationId');
    
				  	 $result = $db->fetchAll($select);
		
				  	
				  	 foreach($result as $index=>$student){				  	 
				  	 		if(($student['exam_status']=='U' && $student['audit_program']===NULL) || $student['exam_status']=='EX' || $student['exam_status']=='CT'){
								unset($result[$index]);
				  	 		}				  	 	
				  	 }
				  	 
	 	return $result;
				 
	}
	
	public function getTotalStudentByGrade($idGroup,$idProgram,$idScheme,$grade){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array())
	 				 ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration',array('numberofstudent'=>'COUNT(IdStudentRegSubjects)'))
	 				 ->where('srs.Active != ?',3)
	 				 ->where('sr.profileStatus = ?',92)
	 				 ->where('sr.registrationId != ?',"1409999")
	 				 ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
	 				 ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
	 				 ->where('sr.IdProgram = ?',$idProgram)
	 				 ->where('sr.IdProgramScheme = ?',$idScheme)
	 				 ->where('srs.grade_name = ?',trim($grade));
	 
	 	$row = $db->fetchRow($select);
	 	return $row;
	}
	
	public function getFailStudent($idGroup,$idProgram,$idScheme,$grade,$student_fail){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array())
	 				 ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration',array('numberofstudent'=>'COUNT(IdStudentRegSubjects)'))
	 				 ->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = sr.IdStudentRegistration AND er.er_idSemester=srs.IdSemesterMain AND er.er_idSubject=srs.IdSubject',array('er_attendance_status'))
	 				 ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=er.er_attendance_status', array('attendance'=>'IFNULL(DefinitionDesc,"-")'))
	 				 ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
	 				 ->where('srs.IdStudentRegistration IN (?)',$student_fail)	 				
	 				 ->where('srs.Active != ?',3)
	 				 ->where('sr.profileStatus = ?',92)
	 				 ->group('er.er_attendance_status');
	 
	 	$row = $db->fetchAll($select);
	 	return $row;
	}
	
	public function getDeanByProgram($idProgram){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db->select()	
		 			   ->from(array('cop'=>'tbl_chiefofprogramlist'))
		 			   ->join(array('s'=>'tbl_staffmaster'),'s.IdStaff=cop.IdStaff',array('FullName'))
					   ->where('IdProgram  = ?',$idProgram)
					   ->order('FromDate DESC');
						   
		 $dean = $db->fetchRow($select);
		 return $dean;
	}
	
	public function getStudentAeerc($formData,$idScheme=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semDB = new Registration_Model_DbTable_ExamRegistration();
		$semester_all = $semDB->getSemester($formData['IdSemester']);
				
	    $sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram','sp_id', 'sr.IdLandscape'))				 
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','audit_program','exam_status'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal','appl_nationality','appl_contact_mobile','address'=>'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)','appl_postcode'))
			      ->join(array("c" => "tbl_countries"),'c.idCountry=sp.appl_nationality', array('CountryName'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
	              ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=sp.appl_religion', array('religion'=>'IFNULL(d.DefinitionDesc,"-")'))
	              ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		          ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		          ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
		          ->join(array('int'=>'tbl_intake'), 'int.IdIntake=sr.IdIntake', array('intake'=>'int.IntakeDesc'))
		          ->join(array('dps'=>'tbl_definationms'), 'dps.idDefinition=sr.profileStatus', array('profile_status'=>'IFNULL(dps.DefinitionDesc,"-")'))
		          ->joinLeft(array('q'=>'tbl_countries'), 'sp.appl_nationality = q.idCountry', array('nationality'=>'q.CountryName'))  
		          ->joinLeft(array('ct'=>'tbl_city'), 'sp.appl_city = ct.idCity', array('city'=>'ct.CityName'))            
				  ->where('sr.IdProgram = ?', $formData['IdProgram'])	
				  ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])				
				  ->where('srs.IdSemesterMain = ?', $formData['IdSemester'])
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  ->where('srs.exam_status NOT IN ("U") or srs.exam_status is null or srs.exam_status = ""')
				  //->where('srs.exam_status NOT IN ("U") or ((srs.audit_program IS NOT NULL and srs.audit_program!="" and srs.audit_program!=0) and srs.exam_status="U")')
				  ->group('sr.IdStudentRegistration');

		$sql2 = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram','sp_id', 'sr.IdLandscape'))				 
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','audit_program','exam_status'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal','appl_nationality','appl_contact_mobile','address'=>'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)','appl_postcode'))
			      ->join(array("c" => "tbl_countries"),'c.idCountry=sp.appl_nationality', array('CountryName'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
	              ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=sp.appl_religion', array('religion'=>'IFNULL(d.DefinitionDesc,"-")'))
	              ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		          ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		          ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))	
		          ->join(array('int'=>'tbl_intake'), 'int.IdIntake=sr.IdIntake', array('intake'=>'int.IntakeDesc'))	
		          ->join(array('dps'=>'tbl_definationms'), 'dps.idDefinition=sr.profileStatus', array('profile_status'=>'IFNULL(dps.DefinitionDesc,"-")'))	
		          ->joinLeft(array('q'=>'tbl_countries'), 'sp.appl_nationality = q.idCountry', array('nationality'=>'q.CountryName'))  
		          ->joinLeft(array('ct'=>'tbl_city'), 'sp.appl_city = ct.idCity', array('city'=>'ct.CityName')) 
		          ->where('srs.audit_program = ?', $formData['IdProgram'])           
				  ->where('srs.audit_programscheme IN (?)', $formData['IdProgramScheme']) 
				  ->where('srs.IdSemesterMain IN (?)', $semester_all)
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  ->group('sr.IdStudentRegistration');
				  		  
		/*$sql2 = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram','sp_id'))				 
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','audit_program'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal','appl_nationality','appl_contact_mobile','address'=>'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)','appl_postcode'))
			      ->join(array("c" => "tbl_countries"),'c.idCountry=sp.appl_nationality', array('CountryName'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
	              ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=sp.appl_religion', array('religion'=>'IFNULL(d.DefinitionDesc,"-")'))
	              ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		          ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		          ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))	
		          ->join(array('int'=>'tbl_intake'), 'int.IdIntake=sr.IdIntake', array('intake'=>'int.IntakeDesc'))	
		          ->join(array('dps'=>'tbl_definationms'), 'dps.idDefinition=sr.profileStatus', array('profile_status'=>'IFNULL(dps.DefinitionDesc,"-")'))	
		          ->joinLeft(array('q'=>'tbl_countries'), 'sp.appl_nationality = q.idCountry', array('nationality'=>'q.CountryName'))  
		          ->joinLeft(array('ct'=>'tbl_city'), 'sp.appl_city = ct.idCity', array('city'=>'ct.CityName'))   
				  ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])				
				  ->where('srs.IdSemesterMain IN (?)', $semester_all)
				  ->where('srs.audit_program = ?', $formData['IdProgram'])
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)				  	
				  ->group('sr.IdStudentRegistration');*/
				  
					
				  //GS get student_grade info
				  if(isset($idScheme) && $idScheme==11){
				  		
				  		$sql->joinLeft(array('sg'=>'tbl_student_grade'),'sg.sg_IdStudentRegistration=sr.IdStudentRegistration');
						//$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape',array());
						$sql->joinLeft(array('sm'  => 'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array());
						
						$sql->where('sm.CourseType NOT IN (?)',array('3',20));	
						$sql->where('sg.sg_semesterId = ?',$formData['IdSemester']);	
						//$sql->where('srs.exam_status != ?','U');	
				//$sql2->joinLeft(array('sg'=>'tbl_student_grade'),'sg.sg_IdStudentRegistration=sr.IdStudentRegistration');
				
						//FOR AUDIT CREDIT
						$sql2->joinLeft(array('sg'=>'tbl_student_grade'),'sg.sg_IdStudentRegistration=sr.IdStudentRegistration');
						//$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape',array());
						$sql2->joinLeft(array('sm'  => 'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array());
						
						$sql2->where('sm.CourseType NOT IN (?)',array('3',20));	
						$sql2->where('sg.sg_semesterId = ?',$formData['IdSemester']);	
				  		
				  }
				  
				  if(isset($formData['IdBranch']) && $formData['IdBranch']!=''){
				  		$sql->where("sr.IdBranch = ?",$formData['IdBranch']);
				  		$sql2->where("sr.IdBranch = ?",$formData['IdBranch']);
				  }	
				  
				  if(isset($formData['Part']) && $formData['Part']!=''){
				  		//$sql->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdSemesterMain=srs.IdSemesterMain AND sss.IdStudentRegistration=sr.IdStudentRegistration',array());
				  		//$sql->where("sss.Part = ?",$formData['Part']);
                        $sql->joinLeft(array('lsc'  => 'tbl_landscapesubject'),'lsc.IdLandscape = sr.IdLandscape  AND lsc.IdSubject = srs.IdSubject',array());
                        $sql->joinLeft(array('eq'=>'tbl_subjectequivalent'), 'eq.idsubject = srs.IdSubject AND lsc.IdLandscape is not null');
				  		$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape AND (ls.IdSubject = srs.IdSubject or eq.idsubjectequivalent = ls.IdSubject)',array());
				  		//$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape',array());
                        $sql->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array());
				  		//$sql->joinLeft(array('lsr' => 'tbl_studentregsubjects'),'lsr.IdSubject = ls.IdSubject',array());
						$sql->joinLeft(array('sm'  => 'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array());
						
						//$sql->where('sm.CourseType NOT IN (?)',array('3',20));	
						$sql->where("lsb.block = ?",$formData['Part']);
						//$sql->where("lsb.block = 3");
						$sql->where('srs.IdSemesterMain = ?',$formData['IdSemester']);
						$sql->where('ls.IdLevel IS NOT NULL');
						//$sql2->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdSemesterMain=srs.IdSemesterMain AND sss.IdStudentRegistration=sr.IdStudentRegistration',array());
				  		//$sql2->where("sss.Part = ?",$formData['Part']);
				  		
						//AUDIT CREDIT
						$sql2->joinLeft(array('lsc'  => 'tbl_landscapesubject'),'lsc.IdLandscape = sr.IdLandscape  AND lsc.IdSubject = srs.IdSubject',array());
                        $sql2->joinLeft(array('eq'=>'tbl_subjectequivalent'), 'eq.idsubject = srs.IdSubject AND lsc.IdLandscape is not null');
				  		$sql2->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape AND (ls.IdSubject = srs.IdSubject or eq.idsubjectequivalent = ls.IdSubject)',array());
				  		$sql2->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array());
				  		$sql2->joinLeft(array('sm'  => 'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array()); 
				  		$sql2->where("lsb.block = ?",$formData['Part']);
				  		$sql2->where('srs.IdSemesterMain = ?',$formData['IdSemester']);
						$sql2->where('ls.IdLevel IS NOT NULL');
				  }	

				 
				   $select = $db->select()->union(array($sql, $sql2))->limit();
    
                   if(isset($idScheme) && $idScheme==11){
                   		$select->order('sg_cgpa DESC');
                   		//$sql->order('sg.sg_cgpa DESC');	
                   }else{
                   		$select->order('appl_fname');
                   		$select->order('appl_lname');
                   		//$sql->order('sp.appl_fname');
                   		//$sql->order('sp.appl_lname');
                   }
                  

				 //$sql->limit(1);
				  $result = $db->fetchAll($select);
				  				
				  return $result;
				  		
	}
	
	
	public function getStudentSubjectAeerc($formData,$studentData = null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
	  $sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array())
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(srs.IdSubject))'))
				  ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array())				
				  ->where('sr.IdProgram = ?', $formData['IdProgram'])	
				  ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])				
				  ->where('srs.IdSemesterMain = ?', $formData['IdSemester'])				
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  // ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")')
				  ->where('srs.exam_status NOT IN ("EX","CT","U")')
                                  //->where('srs.exam_status NOT IN ("EX","CT","U") or ((srs.audit_program IS NOT NULL and srs.audit_program!="" and srs.audit_program!=0) and srs.exam_status="U")')
				  ->group('sr.IdProgram')
				  ->group('srs.IdSemesterMain');
				 
					
				  if(isset($formData['IdBranch']) && $formData['IdBranch']!=''){
				  		$sql->where("sr.IdBranch = ?",$formData['IdBranch']);
				  }	
				  
				  if(isset($formData['Part']) && $formData['Part']!=''){
				  		// $sql->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdSemesterMain=srs.IdSemesterMain AND sss.IdStudentRegistration=sr.IdStudentRegistration',array());
				  		// $sql->where("sss.Part = ?",$formData['Part']);
						
						$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape AND ls.IdSubject = srs.IdSubject',array());
				  		//$sql->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array());
				  		$sql->joinLeft(array('lsr' => 'tbl_studentregsubjects'),'lsr.IdSubject = ls.IdSubject',array());
						//$sql->where("srs.audit_program != 2");
						
						//$sql->where("lsb.block = ?",$formData['Part']);
				  }		


				  $result = $db->fetchRow($sql); 
				  
				  return $result;
				  		
	}
	
	
	public function getStudentRegSubjectInfo($IdStudentRegistration,$IdSemester,$IdSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSemesterMain = ?',$IdSemester)
	 				 ->where('srs.IdSubject = ?',$IdSubject);
	 
	 	$row = $db->fetchRow($select);
	 	return $row;
	}
	
	//course audit
	public function getStudentRegSubjectData($IdStudentRegistration,$IdSubject,$idProgram,$idLandscape,$subjectType){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		//$row = array();
		
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))
	 				 ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = srs.IdSemesterMain',array())
	 				 ->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
	 				 ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array('CreditHours'))
	 				 ->joinLeft(array("ls"=>"tbl_landscapesubject"),'ls.IdLandscape =sr.IdLandscape AND ls.IdSubject = srs.IdSubject',array('SubjectType'))
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$IdSubject)
	 				 ->where('srs.Active != 3')	 		
	 				 ->where('sr.IdLandscape = ?',$idLandscape)		
	 				 ->order('s.SemesterMainStartDate DESC');
	 				 
	 	if($idProgram==5){
	 		//CIFP
	 	}else{
	 		 //$select->where('ls.SubjectType = ?',$subjectType)
	 		 $select->group('IdStudentRegSubjects');
	 	}
	 	
	 	//echo $select.'<br /><br />';
	 	$row = $db->fetchRow($select);
	 	
	 	
	 	if($row){
	 		
	 		if($row['exam_status']=='U' || $row['exam_status']=='CT' || $row['exam_status']=='EX'){
	 			return $row;
	 		}else{
	 			//cek attendance
		 		$attendance = $this->getAttendance($IdStudentRegistration,$row['IdSemesterMain'],$IdSubject);
		 		if($attendance['er_attendance_status']!=396){		 			
		 			return $row;
		 		}
	 		}
	 		
	 	}
	 	
	 	//return $row;
	 	
	 	
	}
	
	public function getTotalGrade($IdStudentRegistration,$IdSemester,$IdSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semester_arr = $this->getEqSemesterOpen($IdSemester);
		array_push($semester_arr,$IdSemester);
		
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_id','grade_name','total_bygrade'=>'COUNT(grade_name)'))
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSemesterMain IN (?)',$semester_arr)
	 				 ->where('srs.Active != ?',3)
	 				 ->where('sr.profileStatus = ?',92)
				     ->where('sr.registrationId != ?',"1409999")				  
	 				 ->group('srs.grade_name');
	 
	 	$rows = $db->fetchAll($select);
	 		 
	 	$total_bygrade = array();
	 	foreach($rows as $key=>$row){
	 		if(trim($row['grade_name'])!=''){
	 			$total_bygrade[ trim($row['grade_name']) ]=$row['total_bygrade'];
	 		}
		}
		return $total_bygrade;
	}
	
	public function getTotalGradePS($IdStudentRegistration,$IdSubject,$formData){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//$semester_arr = $this->getEqSemesterOpen($IdSemester);
		//array_push($semester_arr,$IdSemester);
		
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_id','grade_name','total_bygrade'=>'COUNT(grade_name)','GROUP_CONCAT(DISTINCT(srs.IdSubject))','sr.registrationId'))
	 				 ->joinLeft(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('registrationId'))
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
					 ->where('sr.IdProgram = ?', $formData['IdProgram'])	
					 ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])
	 				 //->where('srs.IdSemesterMain IN (?)',$semester_arr)
					 ->where('srs.exam_status NOT IN ("EX","CT","U")')		
	 				 ->where('srs.Active != ?',3)
	 				 ->where('sr.profileStatus = ?',92)
				     ->where('sr.registrationId != ?',"1409999")				  
	 				 ->group('srs.grade_name')
					 ->group('srs.IdStudentRegistration');
	 				 //->group('srs.IdSubject');
					 
					if(isset($formData['Part']) && ($formData['Part'] != '')) {
						$select->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape  AND ls.IdSubject = srs.IdSubject',array());
				  		$select->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array());
				  		//$select->joinLeft(array('lsr' => 'tbl_studentregsubjects'),'lsr.IdSubject = ls.IdSubject',array());
						//$sql->where("sr.IdStudentRegistration IN (?)",$studentList);
						$select->where("lsb.block = ?",$formData['Part']);
				  		
					}
	 
	 	$rows = $db->fetchAll($select);
		//echo '<pre>';
		//print_r($rows);
		//echo $select;
	 	//echo '<br />';
		$total_bygrade = array();
	 	foreach($rows as $key=>$row){
	 		if(trim($row['grade_name'])!=''){
	 			$total_bygrade[ trim($row['grade_name']) ]=$row['total_bygrade'];
	 		}
		}
		return $total_bygrade;
	}
	public function getTotalApprovedGrade($IdStudentRegistration,$IdSemester,$subject_arr,$my_regsub_arr){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semester_arr = $this->getEqSemesterOpen($IdSemester);
		array_push($semester_arr,$IdSemester);
		
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('grade_id','grade_name','total_bygrade'=>'COUNT(grade_name)'))
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)	 			
	 				 ->where('srs.IdStudentRegSubjects IN (?)',$my_regsub_arr)
	 				 ->where('srs.mark_approval_status = ?',2)
	 				 ->where('srs.Active != ?',3)
	 				// ->where('sr.profileStatus = ?',92)				    		  
	 				 ->group('srs.grade_name');
	 
	 	$rows = $db->fetchAll($select);
	 		 
	 	$total_bygrade = array();
	 	foreach($rows as $key=>$row){
	 		if(trim($row['grade_name'])!=''){
	 			$total_bygrade[ trim($row['grade_name']) ]=$row['total_bygrade'];
	 		}
		}
		return $total_bygrade;
	}
	
		
	public function getEqSemesterOpen($idSemester)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();

       	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
        $semester = $semesterDB->getData($idSemester);       
        
        $sql = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'),array('IdSemesterMaster')) 
                  ->where('sm.IdScheme = ?',12)                
                  ->where('sm.AcademicYear = ?',$semester['AcademicYear'])
                  ->where('sm.sem_seq = ?',$semester['sem_seq']);
		$result = $db->fetchAll($sql); 
        
        return $result;
    	
    }
    
	function getTotalStudentBySubject($IdProgram,$IdSemester,$IdSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects','IdStudentRegistration','IdSubject','grade_id','grade_name'))
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())	 				
	 				 ->where('sr.IdProgram = ?',$IdProgram)
	 				 ->where('srs.IdSemesterMain = ?',$IdSemester)
	 				 ->where('srs.IdSubject = ?',$IdSubject)
	 				 ->where('srs.Active != ?',3)
	 				 //->where('sr.profileStatus = ?',92)
				     ->where('sr.registrationId != ?',"1409999")				     
				     ->where('srs.exam_status NOT IN ("EX","CT","U")')	;//MUNzIR TEST
	 
	 	
	 	
	 	$semDB = new Registration_Model_DbTable_ExamRegistration();
		$semester_all = $semDB->getSemester($IdSemester);
	 
		//audit paper
		$sql2 =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects','IdStudentRegistration','IdSubject','grade_id','grade_name'))
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
	 				 ->where('srs.IdSemesterMain IN (?)',$semester_all)
	 				 ->where('srs.IdSubject = ?',$IdSubject)
	 				 ->where('srs.audit_program = ?',$IdProgram)
	 				 ->where('srs.Active != ?',3)	 				 
				     ->where('sr.registrationId != ?',"1409999")
					 ->where('srs.exam_status NOT IN ("EX","CT")');
	 				 //->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")');
	 				 
	 				  $select = $db->select()->union(array($sql, $sql2));
				  	  $result = $db->fetchAll($select);

	 	return count($result);
	}
	
	function getTotalStudentBySemGrade($IdProgram,$IdSemester,$IdSubject,$grade_name){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects','IdStudentRegistration','IdSubject','grade_id','grade_name'))
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())	 				
	 				 ->where('sr.IdProgram = ?',$IdProgram)
	 				 ->where('srs.IdSemesterMain = ?',$IdSemester)
	 				 ->where('srs.IdSubject = ?',$IdSubject)
	 				 ->where('srs.Active != ?',3)
	 				 //->where('sr.profileStatus = ?',92)
				     ->where('sr.registrationId != ?',"1409999")				     
				     ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")')	
				     ->where('srs.grade_name = ?',trim($grade_name));
	 
	 	
	 	
	 	$semDB = new Registration_Model_DbTable_ExamRegistration();
		$semester_all = $semDB->getSemester($IdSemester);
	 
		//audit paper
		$sql2 =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects','IdStudentRegistration','IdSubject','grade_id','grade_name'))
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
	 				 ->where('srs.IdSemesterMain IN (?)',$semester_all)
	 				 ->where('srs.IdSubject = ?',$IdSubject)
	 				 ->where('srs.audit_program = ?',$IdProgram)
	 				 ->where('srs.Active != ?',3)	 				 
				     ->where('sr.registrationId != ?',"1409999")
	 				 ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
	 				 ->where('srs.grade_name = ?',trim($grade_name));
	 				 
	 				  $select = $db->select()->union(array($sql, $sql2));
				  	  $result = $db->fetchAll($select);
				  	  
	 	return count($result);
	}
	
	public function isDean(){
		
		 $auth = Zend_Auth::getInstance();
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
		  $my_program  = array();
		  
		//daaptkan lecturer ni dean utk program mana?	
    	$select = $db->select()	
	 			   ->from('tbl_chiefofprogramlist',array('IdProgram','FromDate'=>'MAX(FromDate)'))
				   ->where('IdStaff  = ?',$auth->getIdentity()->IdStaff)
				   ->group('IdProgram');
	 	$rows_program = $db->fetchAll($select);

	 			if(count($rows_program)>0){
		 	 
			 		foreach($rows_program as $program){		
			 	
				 	 //get latest dean kalo dia ok kalo bukan remove from list
				 	$select2 = $db->select()	
				 			   ->from('tbl_chiefofprogramlist')
							   ->where('IdProgram  = ?',$program['IdProgram'])
							   ->order('FromDate DESC');
					 $dean = $db->fetchRow($select2);
					 
				 		if($dean['IdStaff']==$auth->getIdentity()->IdStaff){
				 		array_push($my_program,$program['IdProgram']);
					 	}
			 		}
		 		}//end if
		 		
	 	return $my_program;
		 
	}
	
	
	public function getCourseByGroupLecturer($formData=null){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();

		
		//is Dean?
    	$dean_program = $this->isDean();
				
		$select = $db ->select()
					  ->from(array('cg'=>'tbl_course_tagging_group'),array('IdSemester','IdSubject'))
					  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('SubCode','subjectMainDefaultLanguage','SubjectName'))					 
					  ->order('sm.SubCode')
					  ->group('cg.IdSubject');
					  
		 if(isset($formData['IdSemesterArray']) && $formData['IdSemesterArray']!=''){
		 			 $select->where('cg.IdSemester IN (?)',$formData['IdSemesterArray']);
		 } 
		 
		 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
		 			 $select->where('cg.IdSemester = ?',$formData['IdSemester']);
		 } 
		 		 
		 if(isset($formData['subject_code']) && $formData['subject_code']!=''){
		 			 $select->where('sm.SubCode LIKE ?','%'.$formData['subject_code'].'%');
		 }

	 	 if(isset($formData['subject_name']) && $formData['subject_name']!=''){
		 			 $select->where('sm.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
		 }
		 
		 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin		

		 	if(count($dean_program)>0){
		 		//this is dean
		 		$select->where('gp.program_id IN (?)',$dean_program);
		 	}else{
		 		$select->where('cg.IdLecturer = ?',$auth->getIdentity()->IdStaff);
		 	}
		 }
		
		$subjects = $db->fetchAll($select);	
		
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$oferDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
		
	
		foreach($subjects as $index=>$subject){							
			$offer = $oferDB->checkOffered($subject["IdSubject"],$subject["IdSemester"]);
			
			if($offer){
				$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$subject["IdSemester"]);
				$subjects[$index]["total_group"] = $total_group;					
			}else{
				unset($subjects[$index]);
			}
		}
					
		 return $subjects;
	}
	
	public function getStudentCourseAudit($formData,$idScheme=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sqlsem = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'),array('IdSemesterMaster'))
                  ->where('sm.IdScheme = ?',$idScheme);
		$semester_all = $db->fetchAll($sqlsem); 
		
		//get all semester by scheme
				
	    $sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram','sp_id','IdIntake','IdLandscape','IdBranch'))				 
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','audit_program'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal','appl_nationality','appl_contact_mobile','address'=>'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)','appl_postcode'=>'IFNULL(appl_postcode," ")'))
			      ->joinLeft(array("c" => "tbl_countries"),'c.idCountry=sp.appl_nationality', array('CountryName'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
	              ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=sp.appl_religion', array('religion'=>'IFNULL(d.DefinitionDesc,"-")'))
	              ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		          ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		          ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))	
		          ->join(array('int'=>'tbl_intake'), 'int.IdIntake=sr.IdIntake', array('intake'=>'int.IntakeDesc'))	
		          ->join(array('dps'=>'tbl_definationms'), 'dps.idDefinition=sr.profileStatus', array('profile_status'=>'IFNULL(dps.DefinitionDesc,"-")'))	
		          ->joinLeft(array('q'=>'tbl_countries'), 'sp.appl_nationality = q.idCountry', array('nationality'=>'q.CountryName'))  
		          ->joinLeft(array('ct'=>'tbl_city'), 'sp.appl_city = ct.idCity', array('city'=>'IFNULL(ct.CityName,sp.appl_city_others)'))
		          ->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = sr.IdStudentRegistration', array())
			      ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('schorlaship'=>'sca.sch_name'))
			      ->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = sr.IdStudentRegistration', array())
			      ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
			      ->joinLeft(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdStudentRegistration=sr.IdStudentRegistration AND sss.IdSemesterMain = "'.$formData['IdSemester'].'"',array())
			      ->joinLeft(array('dpss'=>'tbl_definationms'), 'dpss.idDefinition=sss.studentsemesterstatus', array('studentsemesterstatus'=>'IFNULL(dpss.DefinitionDesc,"-")'))
			      ->where('sr.IdProgram = ?', $formData['IdProgram'])	
				  ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])				
				  //->where('srs.IdSemesterMain IN (?)', $semester_all)
				  ->where('srs.Active != ?',3)
				  //->where('sr.profileStatus IN (92,96,248)')//temporary aje ni utk testing kene enable balik				 
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  ->where('srs.exam_status != "U"')	
				  //->where('sr.registrationId = ?',"1400215")
				  ->group('sr.IdStudentRegistration')
				  ->order('appl_fname')
                  ->order('appl_lname');
                                
			  
				  
		/*$sql2 = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram','sp_id'))				 
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','audit_program'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal','appl_nationality','appl_contact_mobile','address'=>'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)','appl_postcode'))
			      ->join(array("c" => "tbl_countries"),'c.idCountry=sp.appl_nationality', array('CountryName'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
	              ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=sp.appl_religion', array('religion'=>'IFNULL(d.DefinitionDesc,"-")'))
	              ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		          ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		          ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))	
		          ->join(array('int'=>'tbl_intake'), 'int.IdIntake=sr.IdIntake', array('intake'=>'int.IntakeDesc'))	
		          ->join(array('dps'=>'tbl_definationms'), 'dps.idDefinition=sr.profileStatus', array('profile_status'=>'IFNULL(dps.DefinitionDesc,"-")'))	
		          ->joinLeft(array('q'=>'tbl_countries'), 'sp.appl_nationality = q.idCountry', array('nationality'=>'q.CountryName'))  
		          ->joinLeft(array('ct'=>'tbl_city'), 'sp.appl_city = ct.idCity', array('city'=>'ct.CityName'))   
				  ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])				
				  ->where('srs.IdSemesterMain IN (?)', $semester_all)
				  ->where('srs.audit_program = ?', $formData['IdProgram'])
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)				  	
				  ->group('sr.IdStudentRegistration');*/
				  
				  // $select = $db->select()->union(array($sql, $sql2))->limit(10);
                    
                 
				  $result = $db->fetchAll($sql);
				  
				  return $result;
				  		
	}
	
	public function getActiveRegisteredSemester($IdStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('sss' =>'tbl_studentsemesterstatus')) 
                   ->where('sss.IdStudentRegistration = ?',$IdStudentRegistration) 
                   ->where('sss.studentsemesterstatus = ?',130) ;                  
        $result = $db->fetchAll($sql);
        
        return $result;
	}
    
	public function getStudentSponsor($IdStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('st' =>'tbl_sponsor_tag')) 
                   ->where('st.StudentId = ?',$IdStudentRegistration)
                   ->order('st.UpdDate DESC');              
        $result = $db->fetchRow($sql);
        
        return $result;
		
	}
	
	static function countYearOfStudy($id, $scheme){
        $model = new Records_Model_DbTable_Report();
        
        $intakeId = $model->getIntakeId($id);
        $intakeDetail = $model->getIntakeDetail($intakeId);
        
        if ($intakeId==60){
            $semesterInfo['SemesterMainStartDate']='2006-12-01';
        }else{
            $semesterInfo = $model->getSemesterStart($intakeDetail['sem_year'], $intakeDetail['sem_seq'], $scheme);
        }
        
        $date1 = $semesterInfo['SemesterMainStartDate'];
        $date2 = $model->getCurrentSem($scheme);
        
        $diff = abs(strtotime($date2['SemesterMainStartDate']) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        
        return $years+1;
    }
    
    public function getTotalResearchRegistered($IdStudentRegistration,$idSubject){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('total_ch_registered'=>'IFNULL(SUM(credit_hour_registered),0)'))
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.Active != ?',3);
	 				 $row = $db->fetchRow($select);
	 				 return $row;
	}	
	
	public function getFinalStudentGrade($IdStudentRegistration){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					   ->from(array('sg'=>'tbl_student_grade'))
					   ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sg.sg_semesterId',array())
					   ->where('sg_IdStudentRegistration = ?',$IdStudentRegistration)
					   ->order('sm.SemesterMainStartDate DESC');
		
		return $rowSet = $db->fetchRow($select);
		
	}
	
	public function getPartList($Landscapes){
		
		$db =  Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
      				 ->distinct()
	 				 ->from(array("ls"=>"tbl_landscapesubject"),array())
	 				 ->join(array('lb'=>'tbl_landscapeblock'),'lb.idblock = ls.IdLevel',array('SubjectTypeName'=>'blockname','block','SubjectType'=>'block'))
	 				 ->join(array("s"=>"tbl_subjectmaster"),"s.IdSubject=ls.IdSubject ",array('IdSubjects'=>'GROUP_CONCAT(DISTINCT(s.IdSubject) order by s.SubCode)','SubCode'=>'GROUP_CONCAT(DISTINCT(s.SubCode) order by s.SubCode)'))
	 				 ->where("ls.IdLandscape IN (?)",$Landscapes)
	 				 ->where('s.CourseType != ?',20) //CE
	 				 ->group('lb.block')
	 				 ->order('lb.block');	 				        
		
		return $rowSet = $db->fetchAll($select);
	}
	
	public function caTotalCHRegistered($IdStudentRegistration){
		
			$db = Zend_Db_Table::getDefaultAdapter();
			
		 	$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('credit_hour_registered'))
	 				 ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','CourseType'))
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)	 				
	 				 ->where('srs.Active != ?',3);
	 	   $rows = $db->fetchAll($select);
	 	   
	 	   $total_credit_hour = 0;
	 	   foreach($rows as $row){
	 	   		if($row['CourseType']==3){
	 	   			$taken_ch = $row['credit_hour_registered'];
	 	   		}else{
	 	   			$taken_ch =  $row['CreditHours'];
	 	   		}
	 	   		$total_credit_hour=$total_credit_hour+$taken_ch;
	 	   }
	 	   
	 	   return $total_credit_hour;
	 				
	}
	
	public function getStudentSubjectAeercPS($formData, array $studentData = array()){
            //var_dump($studentData); exit;
		$db = Zend_Db_Table::getDefaultAdapter();
				
		$getSemester = $db->select();
		$getSemester ->from(array('sm' => 'tbl_semestermaster'))
					 ->where('sm.IdSemesterMaster = ?', $formData['IdSemester']);
		$semester = $db->fetchRow($getSemester);
		
		//echo '<pre>';
		//print_r($semester);exit;
		$sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array())
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(srs.IdSubject))'))
				  ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array())				
				  ->join(array("c" => "tbl_semestermaster"),'c.IdSemesterMaster=srs.IdSemesterMain', array())
				  
				  ->where('sr.IdProgram = ?', $formData['IdProgram'])	
				  ->where('sr.IdProgramScheme IN (?)', $formData['IdProgramScheme'])			
				  //->where('srs.IdSemesterMain = ?', $formData['IdSemester'])				
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST	
                                  //->where('srs.exam_status NOT IN ("U")')
			//->where('srs.exam_status NOT IN ("U") or srs.exam_status is null or srs.exam_status = ""')
				  //->where('srs.exam_status NOT IN ("U") or ((srs.audit_program IS NOT NULL and srs.audit_program!="" and srs.audit_program!=0) and srs.exam_status="U")')
				  ->where('c.SemesterMainEndDate <= ?',$semester['SemesterMainEndDate']);
				  //->group('srs.IdSubject');					
				  //->group('sr.IdProgram');
				  //->group('srs.IdSemesterMain');

		if($formData['IdProgram']==5){
			$sql->where('srs.exam_status NOT IN ("U")');
		}else{
			$sql->where('srs.exam_status NOT IN ("U") or srs.exam_status is null or srs.exam_status = ""');
		}
				 
					
				  if(isset($formData['IdBranch']) && $formData['IdBranch']!=''){
				  		$sql->where("sr.IdBranch = ?",$formData['IdBranch']);
				  }	
				  
				  if(isset($formData['Part']) && $formData['Part']!=''){
				  		
						/*if(!empty($studentData)) {
							$studentList = '';
							foreach($studentData as $key => $student) {
								$studentList .= $student['IdStudentRegistration'].',';
							}
							$studentList = rtrim($studentList,',');
							
						}*/
                                                $sql->joinLeft(array('lsc'  => 'tbl_landscapesubject'),'lsc.IdLandscape = sr.IdLandscape  AND lsc.IdSubject = srs.IdSubject',array());
						$sql->joinLeft(array('eq'=>'tbl_subjectequivalent'), 'eq.idsubject = srs.IdSubject AND lsc.IdLandscape is not null');
				  		$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape AND (ls.IdSubject = srs.IdSubject or eq.idsubjectequivalent = ls.IdSubject)',array());
						//$sql->join(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape  AND ls.IdSubject = srs.IdSubject',array());
				  		$sql->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array());
				  		$sql->joinLeft(array('lsr' => 'tbl_studentregsubjects'),'lsr.IdSubject = ls.IdSubject',array());
	
						
						//$sql->where("sr.IdStudentRegistration IN (?)",$studentList);
						$sql->where("lsb.block = ?",$formData['Part']);
				  }		

				  //echo $sql;
				  $result = $db->fetchRow($sql); 
				  
				  return $result;
				  		
	}
	
	public function getStudentRegSubjectInfoPS($IdStudentRegistration,$IdSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects',array('*')))
                                         ->joinLeft(array('a'=>'tbl_subjectmaster'), 'srs.IdSubject = a.IdSubject')
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				// ->where('srs.IdSemesterMain = ?',$IdSemester)
	 				 ->where('srs.IdSubject = ?',$IdSubject)
                     ->where('srs.Active <> ?', 3)
                     ->where("srs.exam_status IN ('C','CT','E','U', 'EX')")
					 ->order('srs.grade_name ASC')
					 ->order('srs.UpdDate DESC');
		//echo $select.'<br />';
	 	$row = $db->fetchRow($select);
	 	//print_r($row);
		return $row;
	}
	
	public function getAttendance($idStudentRegistration,$idSemester,$idSubject) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					->from(array('a' => 'exam_registration'))
					->where('a.er_idSemester =?',$idSemester)
					->where('a.er_idStudentRegistration =?',$idStudentRegistration)
					->where('a.er_idSubject =?',$idSubject);
					
		$row = $db->fetchRow($select);

		return $row;
	}
	
	public function getStudentAeercAuditProgram($formData,$idScheme=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semDB = new Registration_Model_DbTable_ExamRegistration();
		$semester_all = $semDB->getSemester($formData['IdSemester']);
				
	    $sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram','sp_id', 'sr.IdLandscape'))				 
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdStudentRegSubjects','audit_program'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal','appl_nationality','appl_contact_mobile','address'=>'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)','appl_postcode'))
			      ->joinLeft(array("c" => "tbl_countries"),'c.idCountry=sp.appl_nationality', array('CountryName'))
	              ->joinLeft(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
	              ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=sp.appl_religion', array('religion'=>'IFNULL(d.DefinitionDesc,"-")'))
	              ->join(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		          ->join(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		          ->join(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))	
		          ->join(array('int'=>'tbl_intake'), 'int.IdIntake=sr.IdIntake', array('intake'=>'int.IntakeDesc'))	
		          ->join(array('dps'=>'tbl_definationms'), 'dps.idDefinition=sr.profileStatus', array('profile_status'=>'IFNULL(dps.DefinitionDesc,"-")'))	
		          ->joinLeft(array('q'=>'tbl_countries'), 'sp.appl_nationality = q.idCountry', array('nationality'=>'q.CountryName'))  
		          ->joinLeft(array('ct'=>'tbl_city'), 'sp.appl_city = ct.idCity', array('city'=>'ct.CityName'))            
				  //->where('sr.IdProgram = ?', $formData['IdProgram'])	
				  ->where('srs.audit_program IN (?)',  $formData['IdProgram'])				
				  ->where('srs.IdSemesterMain = ?', $formData['IdSemester'])
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  //->where('srs.exam_status IN ("U")')
				  //->where('srs.exam_status NOT IN ("U") or ((srs.audit_program IS NOT NULL and srs.audit_program!="" and srs.audit_program!=0) and srs.exam_status="U")')
				  ->group('sr.IdStudentRegistration');
				  		  
					
				  //GS get student_grade info
				  if(isset($idScheme) && $idScheme==11){
				  		
				  		$sql->joinLeft(array('sg'=>'tbl_student_grade'),'sg.sg_IdStudentRegistration=sr.IdStudentRegistration');
						//$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape',array());
						$sql->joinLeft(array('sm'  => 'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array());
						
						$sql->where('sm.CourseType NOT IN (?)',array('3',20));	
						$sql->where('sg.sg_semesterId = ?',$formData['IdSemester']);	
						//$sql->where('srs.exam_status != ?','U');	
				//$sql2->joinLeft(array('sg'=>'tbl_student_grade'),'sg.sg_IdStudentRegistration=sr.IdStudentRegistration');
				  		
				  }
				  
				  if(isset($formData['IdBranch']) && $formData['IdBranch']!=''){
				  		$sql->where("sr.IdBranch = ?",$formData['IdBranch']);
				  		//$sql2->where("sr.IdBranch = ?",$formData['IdBranch']);
				  }	
				  
				  if(isset($formData['Part']) && $formData['Part']!=''){
				  		//$sql->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdSemesterMain=srs.IdSemesterMain AND sss.IdStudentRegistration=sr.IdStudentRegistration',array());
				  		//$sql->where("sss.Part = ?",$formData['Part']);
                    $sql->joinLeft(array('lsc'  => 'tbl_landscapesubject'),'lsc.IdLandscape = sr.IdLandscape  AND lsc.IdSubject = srs.IdSubject',array());
                    $sql->joinLeft(array('eq'=>'tbl_subjectequivalent'), 'eq.idsubject = srs.IdSubject AND lsc.IdLandscape is not null');
					$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape AND (ls.IdSubject = srs.IdSubject or eq.idsubjectequivalent = ls.IdSubject)',array());
				  		//$sql->joinLeft(array('ls'  => 'tbl_landscapesubject'),'ls.IdLandscape = sr.IdLandscape',array());
                    $sql->joinLeft(array('lsb' => 'tbl_landscapeblock'),'lsb.idBlock = ls.IdLevel',array());
				  		//$sql->joinLeft(array('lsr' => 'tbl_studentregsubjects'),'lsr.IdSubject = ls.IdSubject',array());
					$sql->joinLeft(array('sm'  => 'tbl_subjectmaster'),'sm.IdSubject = srs.IdSubject',array());
						
						//$sql->where('sm.CourseType NOT IN (?)',array('3',20));	
					$sql->where("lsb.block = ?",$formData['Part']);
						//$sql->where("lsb.block = 3");
					//$sql->where('srs.IdSemesterMain = ?',$formData['IdSemester']);
					$sql->where('ls.IdLevel IS NOT NULL');
						//$sql2->join(array('sss'=>'tbl_studentsemesterstatus'),'sss.IdSemesterMain=srs.IdSemesterMain AND sss.IdStudentRegistration=sr.IdStudentRegistration',array());
				  		//$sql2->where("sss.Part = ?",$formData['Part']);
				  }	

				 
				  // $select = $db->select()->union(array($sql, $sql2))->limit(10);
    
                   if(isset($idScheme) && $idScheme==11){
                   		//$select->order('sg_cgpa DESC');
                   		$sql->order('sg.sg_cgpa DESC');	
                   }else{
                   		//$select->order('appl_fname');
                   		//$select->order('appl_lname');
                   		$sql->order('sp.appl_fname');
                   		$sql->order('sp.appl_lname');
                   }
                  
                 //echo $sql;
				 //$sql->limit(1);
				  $result = $db->fetchAll($sql);
				  //echo '<pre>';
				  //print_r($result);
				  return $result;
				  		
	}
}
?>
