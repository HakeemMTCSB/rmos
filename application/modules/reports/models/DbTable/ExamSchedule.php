<?php 

class Reports_Model_DbTable_ExamSchedule extends Zend_Db_Table_Abstract {
	
	/*
	 * 	The default table name 
	 */
	
	protected $_name = '';
	protected $_primary = "";

	public function getStudentByExamCenter($formData=null){
				
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram'))				 
				  ->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= sr.IdStudentRegistration',array('er_ec_id','er_idSemester','er_idProgram','er_idSubject','er_ec_id','er_idCity','er_idCityOthers'))
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = er.er_idStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array('IdStudentRegSubjects'))
				  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array('item_id'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal'))
				  ->where('srs.IdSemesterMain = ?', $formData['IdSemester'])
				  ->where('er.er_ec_id = ?',$formData['ec_id'])
				  ->where('srsd.item_id = ?',879)
				  ->where('srs.Active != ?',3)
				  ->where('sr.profileStatus = ?',92)
				  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
				  ->order('sp.appl_fname')
				  ->order('sp.appl_lname')
				  ->order('sr.registrationId')
				  ->group('er.er_idStudentRegistration');     

				  
        
        $rows = $db->fetchAll($sql);
        
        foreach($rows as $index=>$row){
        	//get subject
        	//get total subject registered each student
			$sql_subject = $db->select()
						  			->from(array('srs'=>'tbl_studentregsubjects'),array(''))
						  			->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array())
						  			->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= srs.IdStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array())
						  			->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubCode'))
						  			->where('srs.Active != ?',3)
						  			->where('srsd.item_id = ?',879)
						  			->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
						  			->where('srs.IdSemesterMain = ?', $row['er_idSemester'])
						  			->where('srs.IdStudentRegistration=?',$row['IdStudentRegistration'])
						  			->where('er.er_ec_id = ?',$formData['ec_id']);
			$mysubject = $db->fetchAll($sql_subject);	
			$rows[$index]['subject']=$mysubject;
			
        }
		return $rows;
		
	}
	
	
	public function getExamDate($formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$search_by = 0;
		
		$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'))
				  ->where('es.es_semester = ?',$formData['IdSemester'])
				  ->where('es.es_exam_center = ?',$formData['ec_id'])
				  ->group('es_date')
				  ->order('es_date');
		
		$rows = $db->fetchAll($sql);
		
		if(count($rows)==0){
			
			$search_by = 1; //default
			
			$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'))
				  ->where('es.es_semester = ?',$formData['IdSemester'])
				  ->where('es.es_exam_center IS NULL OR es.es_exam_center=""')
				  ->group('es_date')
				  ->order('es_date');
		
			$rows = $db->fetchAll($sql);
		}
		
		
		$etime = $this->getExamTime($formData,$search_by);
		
		foreach($rows as $index=>$row){
			
			$schedule = false;
			foreach($etime as $t=>$time){
				
					//each date get subject on selected date
				    $sql_papper = $db->select()
								  ->from(array('es'=>'exam_schedule'),array('es_course'))
								  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=es.es_course',array('subjectid_arr'=>'GROUP_CONCAT(IdSubject)','paper_arr'=>'GROUP_CONCAT( DISTINCT(SubCode) SEPARATOR "/" )'))
								  ->where('es.es_semester = ?',$formData['IdSemester'])								  
								  ->where('es.es_date = ?',$row['es_date'])
								  ->where('es.es_start_time = ?',$time['es_start_time'])
								  ->where('es.es_end_time = ?',$time['es_end_time'])
								  ->group('sm.Group');	

								  if(isset($search_by) && $search_by==1){
								  	$sql_papper->where('es.es_exam_center IS NULL OR es.es_exam_center=""');
								  }else{
								  	$sql_papper->where('es.es_exam_center = ?',$formData['ec_id']);
								  }
								  
					$paper = $db->fetchAll($sql_papper);
					
					
					
				
						if(count($paper)>0){
						
							//$subject_arr = array();
						
							foreach($paper as $key_paper=>$p){		
												
								//array_push($subject_arr,$p['es_course']);
								$arr_subject = explode(",", $p['subjectid_arr']);
								
								//get total student taken this course
								$sql_student = $db->select()
									  			->join(array('srs'=>'tbl_studentregsubjects'),array(''))
									  			->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array())
									  			->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= srs.IdStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array())							  			
									  			->where('srs.Active != ?',3)
									  			->where('srsd.item_id = ?',879)
									  			->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
									  			->where('srs.IdSemesterMain = ?', $formData['IdSemester'])	
									  			->where('srs.IdSubject IN (?)',$arr_subject)
									  			->where('er.er_ec_id = ?',$formData['ec_id'])
									  			->group('srs.IdStudentRegSubjects');
									  			
											   
											     if(isset($search_by) && $search_by==1){
											     	$sql_student->join(array('es'=>'exam_schedule'),'es.es_course=er.er_idSubject AND es.es_semester=er.er_idSemester',array())
											     				->where('es.es_exam_center IS NULL OR es.es_exam_center=""')
											     				->where('es.es_date = ?',$row['es_date'])
											    				->where('es.es_start_time = ?',$time['es_start_time'])
											    				->where('es.es_end_time = ?',$time['es_end_time']);
											    				
											     	
											     }else{
											     	$sql_student->join(array('es'=>'exam_schedule'),'es.es_exam_center = er.er_ec_id AND es.es_course=er.er_idSubject AND es.es_semester=er.er_idSemester',array())
											     				->where('er.er_ec_id = ?',$formData['ec_id'])
											     				->where('es.es_date = ?',$row['es_date'])
											    				->where('es.es_start_time = ?',$time['es_start_time'])
											    				->where('es.es_end_time = ?',$time['es_end_time']);
											     }
								//echo '<hr>'.$sql_student;			     
								$student_list = $db->fetchAll($sql_student);	
								$paper[$key_paper]['total_student']=count($student_list);
								//$etime[$t]['total_student']=count($student_list);
								
								if(count($student_list)>0){
									$schedule = true;
								}//end if
								
						}//end foreach
						
					}else{
						$etime[$t]['total_student']=0;						
					}//end count paper
					
				$etime[$t]['paper']=$paper;
			}
			$rows[$index]['info']=$etime;
			$rows[$index]['schedule']=$schedule;
								
		}
		
		//echo '<pre>';
		//print_r($rows);
		 
		return $rows;
	}
	
			
	public function getExamTime($formData=null,$search_by=0){
		
		/*
			 *  SELECT *
				FROM `exam_schedule`
				WHERE `es_semester` = '6'
				group by es_start_time,es_end_time
				order by es_date,es_start_time,es_end_time
			 */
			
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'),array('es_start_time','es_end_time'))
				  ->where('es.es_semester = ?',$formData['IdSemester'])
				  ->group('es_start_time')
				  ->group('es_end_time')				 
				  ->order('es_start_time')
				  ->order('es_end_time');
				  
				  if(isset($search_by) && $search_by==1){
				  	$sql->where('es.es_exam_center IS NULL OR es.es_exam_center=""');
				  }else{
				  	$sql->where('es.es_exam_center = ?',$formData['ec_id']);
				  }
						  
		$row_time = $db->fetchAll($sql);
		
		return $row_time;
	}
	
	public function getExamTimeList($formData=null){
		
			
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'),array('es_start_time','es_end_time'))
				  ->where('es.es_semester = ?',$formData['IdSemester'])
				  ->where('es.es_exam_center = ?',$formData['ec_id'])
				  ->group('es_start_time')
				  ->group('es_end_time')				 
				  ->order('es_start_time')
				  ->order('es_end_time');
				  						  
		$row_time = $db->fetchAll($sql);
		
		if(count($row_time)==0){
			
			$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'),array('es_start_time','es_end_time'))
				  ->where('es.es_semester = ?',$formData['IdSemester'])
				  ->where('es.es_exam_center IS NULL OR es.es_exam_center=""')
				  ->group('es_start_time')
				  ->group('es_end_time')				 
				  ->order('es_start_time')
				  ->order('es_end_time');
				  						  
			$row_time = $db->fetchAll($sql);
			
		}
		
		return $row_time;
	}
	
	
	public function getCourseSchedule($formData){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$search_by = $this->searchBy($formData);
		
		//each date get subject on selected date
	    $sql_papper = $db->select()
					  ->from(array('es'=>'exam_schedule'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=es.es_course',array('subjectid_arr'=>'GROUP_CONCAT(IdSubject)','paper_arr'=>'GROUP_CONCAT( DISTINCT(SubCode) SEPARATOR "/" )','papername_arr'=>'GROUP_CONCAT( DISTINCT(SubjectName) SEPARATOR "/" )'))
					  ->where('es.es_semester = ?',$formData['IdSemester'])
			          ->order('es.es_start_time')
					  ->group('es.es_date')
					  ->group('sm.Group')
					  ->order('es.es_date')
					  ->order('es.es_start_time')
					  ->order('es.es_end_time')
					  ->order('sm.IdFaculty');	

					  if(isset($search_by) && $search_by==1){
					  	$sql_papper->where('es.es_exam_center IS NULL OR es.es_exam_center=""');
					  }else{
					  	$sql_papper->where('es.es_exam_center = ?',$formData['ec_id']);
					  }
					  
		$paper = $db->fetchAll($sql_papper);		
		
		$paper = $this->getTotalStudent($paper,$formData,$search_by);
		
		return $paper;
	}
	

		
	public function searchBy($formData){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$search_by = 0;
		
		$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'))
				  ->where('es.es_semester = ?',$formData['IdSemester'])
				  ->where('es.es_exam_center = ?',$formData['ec_id'])
				  ->group('es_date')
				  ->order('es_date');
		
		$rows = $db->fetchAll($sql);
		
		if(count($rows)==0){
			
			$search_by = 1; //default
			
		}		
		
		return $search_by;
	}
	
	
	public function getTotalStudent($paper,$formData,$search_by){
		
			$db = Zend_Db_Table::getDefaultAdapter();

			$semDB = new Registration_Model_DbTable_ExamRegistration();
			$semester_all = $semDB->getSemester($formData['IdSemester']);
			
			if(count($paper)>0 ){
				foreach($paper as $key_paper=>$p){		
										
						//array_push($subject_arr,$p['es_course']);
						$arr_subject = explode(",", $p['subjectid_arr']);
						
						//get total student taken this course
						/*$sql_student = $db->select()
							  			->join(array('srs'=>'tbl_studentregsubjects'),array(''))
							  			->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array())
							  			->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= srs.IdStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array())							  			
							  			->where('srs.Active != ?',3)
							  			->where('srsd.item_id = ?',879)
							  			->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
							  			->where('srs.IdSemesterMain = ?', $formData['IdSemester'])	
							  			->where('srs.IdSubject IN (?)',$arr_subject)
							  			->where('er.er_ec_id = ?',$formData['ec_id'])
							  			->group('srs.IdStudentRegSubjects');*/

						$sql1 = $db->select()
				                  ->from(array('er' => 'exam_registration')) 
				                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('registrationId','IdProgramScheme'))
				                  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration AND srs.IdSemesterMain=er.er_idSemester AND er.er_idSubject=srs.IdSubject',array('audit_program','exam_status'))
				                  ->where('srs.IdSemesterMain = ?',$formData['IdSemester'])
				                  ->where('er_ec_id = ?',$formData['ec_id'])
				                  ->where('srs.IdSubject IN (?)',$arr_subject)   
				                  ->where('sr.profileStatus = ?',92)
				                  ->where('srs.Active != ?',3)
								  ->where('sr.registrationId != ?',"1409999")   
								  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")')
				                  ->group('srs.IdStudentRegSubjects');
						                  
						                  
						  //audit credit
						$sql2 = $db->select()
				                  ->from(array('er' => 'exam_registration')) 
				                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('registrationId','IdProgramScheme'))
				                  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration AND srs.IdSemesterMain=er.er_idSemester AND er.er_idSubject=srs.IdSubject',array('audit_program','exam_status'))
				                   ->where('srs.audit_program IS NOT NULL')           
				                  ->where('srs.IdSemesterMain IN (?)',$semester_all)
				                  ->where('er_ec_id = ?',$formData['ec_id'])
				                  ->where('srs.IdSubject IN (?)',$arr_subject)   
				                  ->where('sr.profileStatus = ?',92)
				                  ->where('srs.Active != ?',3)
								  ->where('sr.registrationId != ?',"1409999")   
								  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')            
				                  ->group('srs.IdStudentRegSubjects');
				               
					   
									if(isset($search_by) && $search_by==1){
								     	$sql1->join(array('es'=>'exam_schedule'),'es.es_course=er.er_idSubject AND es.es_semester=er.er_idSemester',array())
								     				->where('es.es_exam_center IS NULL OR es.es_exam_center=""');
								     				
					     				$sql2->join(array('es'=>'exam_schedule'),'es.es_course=er.er_idSubject AND es.es_semester=er.er_idSemester',array())
					     							->where('es.es_exam_center IS NULL OR es.es_exam_center=""');
								     	
								     }else{
								     	$sql1->join(array('es'=>'exam_schedule'),'es.es_exam_center = er.er_ec_id AND es.es_course=er.er_idSubject AND es.es_semester=er.er_idSemester',array())
								     				->where('er.er_ec_id = ?',$formData['ec_id']);
								     				
					     				$sql2->join(array('es'=>'exam_schedule'),'es.es_exam_center = er.er_ec_id AND es.es_course=er.er_idSubject AND es.es_semester=er.er_idSemester',array())
					     							->where('er.er_ec_id = ?',$formData['ec_id']);
								     }
									     
				                  $select = $db->select()->union(array($sql1, $sql2));
								  $result = $db->fetchAll($select);                
						                  
						$paper[$key_paper]['total_student']=count($result);
						
						
				}//end foreach
				
			}else{
				$paper[$key_paper]['total_student'] = 0;
				
			}
			
		return $paper;	
	}
	
	
	public function getExamDateTimeByCourse($IdSemester,$ec_id,$subject_arr){
			
			$db = Zend_Db_Table::getDefaultAdapter();	
			
			$search_by = 0;
			
			$sql = $db->select()
					  ->from(array('es'=>'exam_schedule'))
					  ->where('es.es_semester = ?',$IdSemester)
					  ->where('es.es_exam_center = ?',$ec_id)
					  ->where('es.es_course IN (?)',$subject_arr)
					  ->group('es_date')
					  ->order('es_date');
			
			$rows = $db->fetchRow($sql);
			
			if(!$rows){
				
				$search_by = 1; //default
				
				$sql = $db->select()
					  ->from(array('es'=>'exam_schedule'))
					  ->where('es.es_semester = ?',$IdSemester)
					  ->where('es.es_exam_center IS NULL OR es.es_exam_center=""')
					  ->where('es.es_course IN (?)',$subject_arr)
					  ->group('es_date')
					  ->order('es_date');
			
				$rows = $db->fetchRow($sql);
			}
			return $rows;
	}
	
	
	public function getExamDateByCourse($idSemester,$idSubject,$ec_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$rows = null;
		
		if($ec_id!=''){
			$sql = $db->select()
					  ->from(array('es'=>'exam_schedule'))
					  ->where('es.es_semester = ?',$idSemester)
					  ->where('es.es_exam_center = ?',$ec_id)
					  ->where('es.es_course = ?',$idSubject)
					  ->group('es_date')
					  ->order('es_date');
			
			$rows = $db->fetchRow($sql);
		}
		
		if(!$rows){
			
			$sql = $db->select()
				  ->from(array('es'=>'exam_schedule'))
				  ->where('es.es_semester = ?',$idSemester)
				  ->where('es.es_course = ?',$idSubject)
				  ->where('es.es_exam_center IS NULL OR es.es_exam_center=""')
				  ->group('es_date')
				  ->order('es_date');
		
			$rows = $db->fetchRow($sql);
		}
		
		return $rows;
		
	}
}

?>