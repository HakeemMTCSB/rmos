<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reports_Model_DbTable_TerminatedStudent extends Zend_Db_Table_Abstract {
    
    public function getTerminateStudentList($filter = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->where('a.profileStatus = ?', 94);
        
        if ($filter != false){
            if (isset($filter['IdProgram']) && $filter['IdProgram'] != ''){
                $select->where('a.IdProgram = ?', $filter['IdProgram']);
            }
            if (isset($filter['IdProgramScheme']) && $filter['IdProgramScheme'] != ''){
                $select->where('a.IdProgramScheme = ?', $filter['IdProgramScheme']);
            }
            if (isset($filter['IdIntake']) && $filter['IdIntake'] != ''){
                $select->where('a.IdIntake = ?', $filter['IdIntake']);
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getTerminateCategory(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_profilesemesterstatus_tagging'), array('value'=>'*'))
            ->join(array('b'=>'tbl_definationms'), 'a.pst_semesterstatus = b.idDefinition')
            ->where('a.pst_profilestatus = ?', 94);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getLastSemesterStatus($studId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.studentsemesterstatus=b.IdDefinition', array('status'=>'DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemesterMain=c.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $studId)
            ->where('c.IsCountable = ?', 1)
            ->order('c.SemesterMainStartDate DESC')
            ->group('a.IdStudentRegistration');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateSemesterStatus($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentsemesterstatus', $data, 'idstudentsemsterstatus = '.$id);
        return $update;
    }
    
    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefinition = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>