<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reports_Model_DbTable_QuitStudent extends Zend_Db_Table_Abstract {
    
    public function getStudentQuitByReason($filter = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->where('a.profileStatus = ?', 249);
        
        if ($filter != false){
            if (isset($filter['IdProgram']) && $filter['IdProgram'] != ''){
                $select->where('a.IdProgram = ?', $filter['IdProgram']);
            }
            if (isset($filter['IdProgramScheme']) && $filter['IdProgramScheme'] != ''){
                $select->where('a.IdProgramScheme = ?', $filter['IdProgramScheme']);
            }
            if (isset($filter['IdIntake']) && $filter['IdIntake'] != ''){
                $select->where('a.IdIntake = ?', $filter['IdIntake']);
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getLastSemesterStatus($studId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.studentsemesterstatus=b.IdDefinition', array('status'=>'DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemesterMain=c.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $studId)
            ->order('c.SemesterMainStartDate DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefinition = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getDefinationByType($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramSchemeList($program){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $program);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getIntakeList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = 'SELECT `a`.* FROM `tbl_intake` AS `a` '
                . 'ORDER BY `a`.`sem_year` ASC, '
                . 'CASE a.sem_seq '
                . 'WHEN "JAN" THEN 1 '
                . 'WHEN "FEB" THEN 2 '
                . 'WHEN "MAR" THEN 3 '
                . 'WHEN "APR" THEN 4 '
                . 'WHEN "MAY" THEN 5 '
                . 'WHEN "JUN" THEN 6 '
                . 'WHEN "JUL" THEN 7 '
                . 'WHEN "AUG" THEN 8 '
                . 'WHEN "SEP" THEN 9 '
                . 'WHEN "OCT" THEN 10 '
                . 'WHEN "NOV" THEN 11 '
                . 'WHEN "DEC" THEN 12 '
                . 'ELSE 13 '
                . 'END ASC';
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramSchemeById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgramScheme = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
}
?>