<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reports_Model_DbTable_CumulativeIntake extends Zend_Db_Table_Abstract {
    
    public function getIntakeList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = 'SELECT `a`.* FROM `tbl_intake` AS `a` '
                . 'ORDER BY `a`.`sem_year` ASC, '
                . 'CASE a.sem_seq '
                . 'WHEN "JAN" THEN 1 '
                . 'WHEN "FEB" THEN 2 '
                . 'WHEN "MAR" THEN 3 '
                . 'WHEN "APR" THEN 4 '
                . 'WHEN "MAY" THEN 5 '
                . 'WHEN "JUN" THEN 6 '
                . 'WHEN "JUL" THEN 7 '
                . 'WHEN "AUG" THEN 8 '
                . 'WHEN "SEP" THEN 9 '
                . 'WHEN "OCT" THEN 10 '
                . 'WHEN "NOV" THEN 11 '
                . 'WHEN "DEC" THEN 12 '
                . 'ELSE 13 '
                . 'END ASC';
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApplicantByIntake($intake){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->where('a.at_intake = ?', $intake);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApplicantByIntakeOther($intake){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->where('a.at_intake NOT IN (?)', $intake);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApplicantByStatusAndIntake($intake, $status){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->where('a.at_intake = ?', $intake)
            ->where('a.at_status = ?', $status);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApplicantByStatusAndIntakeOthers($intake, $status){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->where('a.at_intake NOT IN (?)', $intake)
            ->where('a.at_status = ?', $status);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getAppByIntakeStatusProg($intake, $status, $program){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->join(array('b'=>'applicant_program'), 'a.at_trans_id = b.ap_at_trans_id')
            ->where('a.at_intake = ?', $intake)
            ->where('a.at_status = ?', $status)
            ->where('b.ap_prog_id = ?', $program);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getAppByIntakeStatusProgOthers($intake, $status, $program){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->joinLeft(array('b'=>'applicant_program'), 'a.at_trans_id = b.ap_at_trans_id')
            ->where('a.at_intake = ?', $intake)
            ->where('a.at_status = ?', $status)
            ->where('b.ap_prog_id NOT IN (?)', $program);
        //echo $select;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getIntakeById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->where('a.IdIntake = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getDefinationById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefinition = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramSchemeList($program){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $program);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getAppByIntakeStatusProgScheme($intake, $status, $program, $programscheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->join(array('b'=>'applicant_program'), 'a.at_trans_id = b.ap_at_trans_id')
            ->where('a.at_intake = ?', $intake)
            ->where('a.at_status = ?', $status)
            ->where('b.ap_prog_id = ?', $program)
            ->where('b.ap_prog_scheme = ?', $programscheme);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
}
?>