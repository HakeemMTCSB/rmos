<?php
class Reports_Model_DbTable_Stats extends Zend_Db_Table 
{
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	/*
	*	Applicants
	*/
	public function getTotalApplicants()
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>'applicant_transaction'));
		
		$getTotal = $db->query($select)->rowCount();
		
		return $getTotal;
	}

	/*
	*	Students
	*/
	public function getTotalStudents($where=null)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>'tbl_studentregistration'));

		if ( !empty($where) )
		{
			foreach ( $where as $cond => $val )
			{
				$select->where( $cond.' = ?', $val );
			}
		}
		
		$getTotal = $db->query($select)->rowCount();
		
		return $getTotal;
	}

	public function getTotalSponsored()
	{
		$db = $this->db;

		$select2 = $db->select()
					->distinct()
	                ->from(array('a'=>'tbl_sponsor_tag'),array('StudentId'))
					->where('appl_id IS NULL');

		$select = $db->select()
	                ->from(array('a'=>'tbl_studentregistration'))
					->where('IdStudentRegistration IN (?)', $select2 );

		$getTotal = $db->query($select)->rowCount();
		
		return $getTotal;
	}

	public function getApplicantsByProgramme( $idprogram, $where=null)
	{
		$db = $this->db;

		$select = $db->select()
	                ->from(array('a'=>'applicant_transaction'))
					->join(array('b'=>'applicant_program'),'b.ap_at_trans_id=a.at_trans_id', array())
					->where('b.ap_prog_id = ?', $idprogram );

		if ( !empty($where) )
		{
			foreach ( $where as $cond => $val )
			{
				$select->where( $cond.' = ?', $val );
			}
		}
		
		$getTotal = $db->query($select)->rowCount();
		
		return $getTotal;
	}

	public function getStudentsByProgramme( $idprogram, $where=null )
	{
		$db = $this->db;

		$select = $db->select()
	                ->from(array('a'=>'tbl_studentregistration'))
					->join(array('b'=>'student_profile'),'b.id=a.sp_id', array())
					->where('a.IdProgram = ?', $idprogram );

		if ( !empty($where) )
		{
			foreach ( $where as $cond => $val )
			{
				$select->where( $cond.' = ?', $val );
			}
		}
		
		$getTotal = $db->query($select)->rowCount();
		
		return $getTotal;
	}
}
