<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 16/12/2015
 * Time: 10:18 AM
 */
class Reports_Model_DbTable_ResultAnalysis extends Zend_Db_Table_Abstract {

    public function getSemester($semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdSemesterMaster IN (?)', $semId)
            ->where('a.IsCountable = ?', 1);;

        $select = $select.' ORDER BY a.AcademicYear DESC, CASE a.sem_seq '
            . 'WHEN "JAN" THEN 1 '
            . 'WHEN "FEB" THEN 2 '
            . 'WHEN "MAR" THEN 3 '
            . 'WHEN "APR" THEN 4 '
            . 'WHEN "MAY" THEN 5 '
            . 'WHEN "JUN" THEN 6 '
            . 'WHEN "JUL" THEN 7 '
            . 'WHEN "AUG" THEN 8 '
            . 'WHEN "SEP" THEN 9 '
            . 'WHEN "OCT" THEN 10 '
            . 'WHEN "NOV" THEN 11 '
            . 'WHEN "DEC" THEN 12 '
            . 'ELSE 13 '
            . 'END DESC';

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getStudentTakeSubjectFailed($subjectid, $semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->where('a.IdSubject = ?', $subjectid)
            ->where('a.IdSemesterMain IN (?)', $semesterid)
            ->where('a.Active != ?',3)
            ->where('a.grade_name != ?','F')
            //->where('b.profileStatus = ?',92)
            ->where('b.registrationId != ?',"1409999")
            ->where('IFNULL("a.exam_status","Z") NOT IN ("EX","CT","U")');

        $select2 = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->where('a.IdSubject = ?', $subjectid)
            ->where('a.IdSemesterMain IN (?)', $semesterid)
            ->where('a.audit_program IS NOT NULL')
            ->where('a.Active != ?',3)
            ->where('a.grade_name != ?','F')
            //->where('b.profileStatus = ?',92)
            ->where('b.registrationId != ?',"1409999")
            ->where('IFNULL("a.exam_status","Z") NOT IN ("EX","CT")');

        $select = $db->select()->union(array($select1, $select2));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemesterEquivalent($year, $month){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month)
            ->where('a.IsCountable = ?', 1);

        $select = $select.' ORDER BY a.AcademicYear DESC, CASE a.sem_seq '
            . 'WHEN "JAN" THEN 1 '
            . 'WHEN "FEB" THEN 2 '
            . 'WHEN "MAR" THEN 3 '
            . 'WHEN "APR" THEN 4 '
            . 'WHEN "MAY" THEN 5 '
            . 'WHEN "JUN" THEN 6 '
            . 'WHEN "JUL" THEN 7 '
            . 'WHEN "AUG" THEN 8 '
            . 'WHEN "SEP" THEN 9 '
            . 'WHEN "OCT" THEN 10 '
            . 'WHEN "NOV" THEN 11 '
            . 'WHEN "DEC" THEN 12 '
            . 'ELSE 13 '
            . 'END DESC';

        $result = $db->fetchAll($select);
        return $result;
    }

    function getStudentBySemGrade($IdProgram,$IdSemester,$IdSubject,$grade_name){

        $db = Zend_Db_Table::getDefaultAdapter();

        $sql =$db->select()
            ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects','IdStudentRegistration','IdSubject','grade_id','grade_name'))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
            ->where('sr.IdProgram = ?',$IdProgram)
            ->where('srs.IdSemesterMain = ?',$IdSemester)
            ->where('srs.IdSubject = ?',$IdSubject)
            ->where('srs.Active != ?',3)
            //->where('sr.profileStatus = ?',92)
            ->where('sr.registrationId != ?',"1409999")
            ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")')
            ->where('srs.grade_name = ?',trim($grade_name));



        $semDB = new Registration_Model_DbTable_ExamRegistration();
        $semester_all = $semDB->getSemester($IdSemester);

        //audit paper
        $sql2 =$db->select()
            ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects','IdStudentRegistration','IdSubject','grade_id','grade_name'))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array())
            ->where('srs.IdSemesterMain IN (?)',$semester_all)
            ->where('srs.IdSubject = ?',$IdSubject)
            ->where('srs.audit_program = ?',$IdProgram)
            ->where('srs.Active != ?',3)
            ->where('sr.registrationId != ?',"1409999")
            ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
            ->where('srs.grade_name = ?',trim($grade_name));

        $select = $db->select()->union(array($sql, $sql2));
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getFailStudent($IdSubject, $student_fail, $idsemester){
        $semDB = new Registration_Model_DbTable_ExamRegistration();
        $semester_all = $semDB->getSemester($idsemester);

        $db = Zend_Db_Table::getDefaultAdapter();

        $select =$db->select()
            ->from(array('sr'=>'tbl_studentregistration'),array())
            ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration',array('numberofstudent'=>'COUNT(IdStudentRegSubjects)'))
            ->join(array('er'=>'exam_registration'),'er.er_idStudentRegistration = sr.IdStudentRegistration AND er.er_idSemester=srs.IdSemesterMain AND er.er_idSubject=srs.IdSubject',array('er_attendance_status'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=er.er_attendance_status', array('attendance'=>'IFNULL(DefinitionDesc,"-")'))
            ->where('srs.IdSubject = ?', $IdSubject)
            ->where('srs.IdSemesterMain IN (?)',$semester_all)
            ->where('srs.IdStudentRegistration IN (?)', $student_fail)
            ->where('srs.Active != ?', 3)
            //->where('sr.profileStatus = ?', 92)
            ->group('er.er_attendance_status');

        $row = $db->fetchAll($select);
        return $row;
    }
}