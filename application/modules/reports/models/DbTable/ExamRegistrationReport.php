<?php 

class Reports_Model_DbTable_ExamRegistrationReport extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = '';
	protected $_primary = "";

	
	public function getStudentExamRegistration($formData,$subjects,$prevSemester){		
		
		$db = Zend_Db_Table::getDefaultAdapter();
				
	    $sql = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram'))
				  ->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= sr.IdStudentRegistration',array("GROUP_CONCAT(`er`.`er_idCity`)","GROUP_CONCAT(`er`.`er_idCityOthers`)","arr_subjects"=>"GROUP_CONCAT(DISTINCT(er_idSubject))",'er_ec_id','er_idSemester','er_idProgram','er_idSubject','er_ec_id','er_idCity','er_idCityOthers'))
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = er.er_idStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array('IdStudentRegSubjects','audit_program'))
				  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array('item_id'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal'))
				  ->joinLeft(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName'))
	              ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	              ->joinLeft(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_code'))
			      //->join(array('ecs'=>'exam_center_setup'),'ecs.ec_id=ec.ec_id')
	              ->join(array("ps" => "tbl_program_scheme"),'ps.IdProgramScheme=sr.IdProgramScheme', array())
	              //->join(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'ds.DefinitionDesc'))
	              ->join(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'dp.DefinitionDesc'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
			->joinLeft(array('df'=>'tbl_definationms'), 'sr.profileStatus = df.idDefinition', array('statusName'=>'df.DefinitionDesc'))
				  ->where('sr.IdProgram = ?', $formData['IdProgram'])				
				  ->where('srs.IdSemesterMain = ?', $formData['IdSemester'])
				  ->where('srsd.item_id = ?',879)
				  ->where('srs.Active != ?',3)
				  //->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
				  ->where('srs.audit_program IS NULL');
				  //->order('sp.appl_fname')
				  //->order('sp.appl_lname')
				 // ->order('sr.registrationId')
				  //->order('er.er_idCity')
				  //->order('er.er_idCityOthers')
				  //->group('er.er_idStudentRegistration')
				  //->group('er.er_idSemester')
				  //->group('er.er_idSubject')
				  //->group('er.er_idCityOthers')
				  //->group('cityothers')
				  //->group('CASE er.er_idCityOthers WHEN NULL THEN "" ELSE er.er_idCityOthers END')
				  //->group('er.er_idCity')
				  //->group('er.er_idStudentRegistration');
				  
				  //->limit(40,0);
					
				  if(isset($formData['start_date']) && $formData['start_date']!=''){
				  		$sql->where("DATE(srs.UpdDate) > ?",  date('Y-m-d',strtotime($formData['start_date'])) );
				  }	

				  if(isset($formData['end_date']) && $formData['end_date']!=''){
        				$sql->where("DATE(srs.UpdDate) < ?",  date('Y-m-d',strtotime($formData['end_date'])) ); 
				  }


		$sql .= ' GROUP BY CASE `er`.`er_idCityOthers` WHEN "" THEN NULL ELSE `er`.`er_idCityOthers` END, `er`.`er_idCity`, `er`.`er_idStudentRegistration`';
				  		
		
		//$semesterCurrent = $this->getCurrentSemester();
		$semDB = new Registration_Model_DbTable_ExamRegistration();
		$semester_all = $semDB->getSemester($formData['IdSemester']);

		$sql_switch = $db->select()
				  ->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgramScheme','IdProgram'))				 
				  ->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= sr.IdStudentRegistration',array("GROUP_CONCAT(`er`.`er_idCity`)","GROUP_CONCAT(`er`.`er_idCityOthers`)","arr_subjects"=>"GROUP_CONCAT(DISTINCT(er_idSubject))",'er_ec_id','er_idSemester','er_idProgram','er_idSubject','er_ec_id','er_idCity','er_idCityOthers'))
				  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = er.er_idStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array('IdStudentRegSubjects','audit_program'))
				  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array('item_id'))
				  ->join(array('sp' => 'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_email','appl_phone_mobile','appl_email_personal'))
				  ->joinLeft(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName'))
	              ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	              ->joinLeft(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_code'))
				  //->join(array('ecs'=>'exam_center_setup'),'ecs.ec_id=ec.ec_id')
	              ->join(array("ps" => "tbl_program_scheme"),'ps.IdProgramScheme=sr.IdProgramScheme', array())
	              //->join(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'ds.DefinitionDesc'))
	              ->join(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'dp.DefinitionDesc'))
	              ->join(array("b" => "tbl_branchofficevenue"),'b.IdBranch=sr.IdBranch', array('BranchCode'))
			->joinLeft(array('df'=>'tbl_definationms'), 'sr.profileStatus = df.idDefinition', array('statusName'=>'df.DefinitionDesc'))
				  ->where('srs.audit_program = ?', $formData['IdProgram'])				
				  ->where('srs.IdSemesterMain IN (?)', $semester_all )
				  ->where('srsd.item_id = ?',879)
				  ->where('srs.Active != ?',3)
				  //->where('sr.profileStatus = ?',92)
				  ->where('sr.registrationId != ?',"1409999")//MUNzIR TEST
				  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")');
				  //->order('sp.appl_fname')
				  //->order('sp.appl_lname')
				  //->order('sr.registrationId')
				  //->order('er.er_idCity')
				  //->order('er.er_idCityOthers')
				  //->group('er.er_idStudentRegistration')
				  //->group('er.er_idSemester')
				  //->group('er.er_idSubject')
				  //->group('er.er_idCityOthers')
				  //->group('er.er_idCity')
				  //->group('er.er_idStudentRegistration');
				 
					
				  if(isset($formData['start_date']) && $formData['start_date']!=''){
				  		$sql_switch->where("DATE(srs.UpdDate) > ?",  date('Y-m-d',strtotime($formData['start_date'])) );
				  }	

				  if(isset($formData['end_date']) && $formData['end_date']!=''){
        				$sql_switch->where("DATE(srs.UpdDate) < ?",  date('Y-m-d',strtotime($formData['end_date'])) ); 
				  } 
				  		
				  $sql_switch .= ' GROUP BY CASE `er`.`er_idCityOthers` WHEN "" THEN NULL ELSE `er`.`er_idCityOthers` END, `er`.`er_idCity`, `er`.`er_idStudentRegistration`';
				  $select = $db->select()
				    		      ->union(array($sql, $sql_switch))
				    		      ->order('appl_fname')
								  ->order('appl_lname')
								  ->order('registrationId')
								  ->order('er_idCity')
								  ->order('er_idCityOthers');
    
				  $result = $db->fetchAll($select);
		
		
		
				
		$prevId='';
		$prevIdCity = '';
		
		foreach($result as $index=>$row){
			//count row
			if($row['IdStudentRegistration'] == $prevId){
				
				$rowspan = $rowspan+1;				
				for($i=0; $i<$rowspan; $i++){
					//echo '<br>index:'.$index.' $i:'.$i.' $rowspan:'.$rowspan;
					$result[$index-$i]['rowspan']=$rowspan;
				}
				$result[$index]['rowno']=$rowspan;
				
			}else{
				
				$rowspan = 1;
				$result[$index]['rowspan']=$rowspan;
				$result[$index]['rowno']=$rowspan;
					
			}			
			$prevId = $row['IdStudentRegistration'];
		
			
			
			//get total subject registered each student
			$sql_subject = $db->select()
						  			->join(array('srs'=>'tbl_studentregsubjects'),array(''))
						  			->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array('item_id'))
						  			->where('srs.Active != ?',3)
						  			->where('srsd.item_id = ?',879)
						  			->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')						  				
						  			->where('srs.IdSemesterMain = ?', $row['er_idSemester'])
						  			//->where('srs.IdSemesterMain IN (?)', $semester_all)
						  			->where('srs.IdStudentRegistration=?',$row['IdStudentRegistration'])
						  			->group('srs.idSubject')
						  			->group('srsd.item_id');
						  			
						  			if($row['audit_program']!=''){
						  				$sql_subject->where('srs.audit_program = ?',$formData['IdProgram']);
						  			}else{
						  				$sql_subject->where('srs.audit_program IS NULL');
						  			}
						  			
			$mysubject = $db->fetchAll($sql_subject);	
			$result[$index]['total_subject_registered']=count($mysubject);
			
			//get exam center at prev semester
			$sql_ec = $db->select()
			  		 ->from(array('er' => 'exam_registration'),array())
			  		 ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name','ec_code'=>'IFNULL(ec_code,"")'))	
			  		 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = er.er_idSemester',array())		  		
			  		 ->where('er.er_idStudentRegistration = ?',$row['IdStudentRegistration'])			
			  		 ->where('er.er_idSemester NOT IN (?)',$semester_all)
			  		 ->where('er.er_ec_id IS NOT NULL')
			  		 ->order('sm.SemesterMainStartDate DESC');	
			  		 // ->where('er.er_idSemester = ?',$prevSemester['IdSemesterMaster']);
			  		 //->where('er.er_idSubject = ?',$row['er_idSubject']);
			$resultec = $db->fetchRow($sql_ec);			
			$result[$index]['prevExamCenter']=$resultec['ec_code'];
			
		}		
		
		//echo '<pre>';
		//print_r($result);
		return $result;

	}
			
	
	public function getLandscapeSubject($IdProgram,$IdSemester,$IdProgramScheme,$formData){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql2 = $db->select()
				   ->from(array('ls' => 'tbl_landscapesubject'),array('IdSubject'))				   
				   ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=ls.IdSubject',array('SubCode'))
				   ->where('ls.IdProgram = ?',$IdProgram)
				   ->where('sm.CourseType NOT IN (19,2)')
				   ->group('ls.IdSubject');
				   
				   if($IdProgram==5){
				  		$sql2->join(array('so'=>'tbl_subjectsoffered'),'so.IdSubject=ls.IdSubject',array())->where('so.IdSemester = ?',$IdSemester);
				   }
				 
		$subject_list = $db->fetchAll($sql2);
		
		foreach($subject_list as $key=>$subject){
					
			$sql = $db->select()		
				 		  ->from(array('sr' => 'tbl_studentregistration'),array())									 
						  ->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= sr.IdStudentRegistration',array())
						  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = er.er_idStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array('IdStudentRegSubjects'))
						  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array('item_id'))						
						  ->where('er.er_idSubject = ?', $subject['IdSubject'])	
						  ->where('sr.IdProgram = ?', $IdProgram)
						  ->where('er.er_idSemester = ?', $IdSemester)
						  ->where('srsd.item_id = ?',879)
						  ->where('srs.Active != ?',3)
						  //->where('sr.profileStatus = ?',92)
						  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")')
						  ->where('sr.registrationId != ?',"1409999")
						  ->group('srs.IdStudentRegSubjects')
						  ->group('srs.idSubject');
					
				$semDB = new Registration_Model_DbTable_ExamRegistration();
				$semester_all = $semDB->getSemester($IdSemester);
				
				$sql2 = $db->select()		
				 		  ->from(array('sr' => 'tbl_studentregistration'),array())									 
						  ->join(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= sr.IdStudentRegistration',array())
						  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = er.er_idStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array('IdStudentRegSubjects'))
						  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array('item_id'))						
						  ->where('er.er_idSubject = ?', $subject['IdSubject'])	
						  ->where('srs.audit_program = ?', $formData['IdProgram'])				
				 		  ->where('srs.IdSemesterMain IN (?)', $semester_all )
						  //->where('sr.IdProgram = ?', $IdProgram)
						  //->where('er.er_idSemester = ?', $IdSemester)
						  ->where('srsd.item_id = ?',879)
						  ->where('srs.Active != ?',3)
						  //->where('sr.profileStatus = ?',92)
						  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
						  ->where('sr.registrationId != ?',"1409999")
						  ->group('srs.IdStudentRegSubjects')
						  ->group('srs.idSubject');
				
					if(isset($formData['start_date']) && $formData['start_date']!=''){
				  		$sql->where("DATE(srs.UpdDate) > ?",  date('Y-m-d',strtotime($formData['start_date'])) );
				  		$sql2->where("DATE(srs.UpdDate) > ?",  date('Y-m-d',strtotime($formData['start_date'])) );
					}	

				  	if(isset($formData['end_date']) && $formData['end_date']!=''){
	        			$sql->where("DATE(srs.UpdDate) < ?",  date('Y-m-d',strtotime($formData['end_date'])) ); 
	        			$sql2->where("DATE(srs.UpdDate) < ?",  date('Y-m-d',strtotime($formData['end_date'])) ); 
				  	} 
				  
				  
				  
				$reg_subject = $db->fetchAll($sql);
				$reg_subject2 = $db->fetchAll($sql2);
			
				$subject_list[$key]['total']=count($reg_subject)+count($reg_subject2);
			
		}
		return $subject_list;
	}
	
	public function getDataCity($IdProgram,$IdSemester,$IdSubject,$IdStudentRegistration,$city,$formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql = $db->select()		
				 		  //->from(array('sr' => 'tbl_studentregistration'),array('IdStudentRegistration'))									 
						  ->from(array('er' => 'exam_registration'),array('er_idCity','er_idSubject'))
						  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = er.er_idStudentRegistration  AND srs.IdSemesterMain = er.er_idSemester AND er.er_idSubject = srs.IdSubject',array())
						  ->joinLeft(array('cgp'=>'course_group_program'),'cgp.group_id = srs.IdCourseTaggingGroup',array('program_scheme_id'))
						  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id = srs.IdStudentRegSubjects',array())
						  //->where('er.er_idProgram = ?', $IdProgram)				
						  ->where('er.er_idSemester = ?', $IdSemester)
						  ->where('er.er_idStudentRegistration = ?', $IdStudentRegistration)
						  ->where('er.er_idCity = ?',$city)
						  ->where('srsd.item_id = ?',879)
						  ->where('srs.Active != ?',3)						
						  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')
						  ->group('er.er_idStudentRegistration')
						  ->group('er.er_idSemester')
						  ->group('er.er_idSubject');;
						 					 
					
				  if(isset($formData['start_date']) && $formData['start_date']!=''){
				  		$sql->where("DATE(srs.UpdDate) > ?",  date('Y-m-d',strtotime($formData['start_date'])) );
				  }	

				  if(isset($formData['end_date']) && $formData['end_date']!=''){
        				$sql->where("DATE(srs.UpdDate) < ?",  date('Y-m-d',strtotime($formData['end_date'])) ); 
				  } 
		  	
				
				$reg_city = $db->fetchAll($sql);
				
				return $reg_city;
	}
	
		
	public function getCurrentSemester()
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'),array('IdSemesterMaster'))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()');
		$result = $db->fetchAll($sql); 
        
        return $result;
    	
    }
    
    public function getStudentByExamCenter($idSemester,$ec_id,$subjectArr){
    	
     
    	$db = Zend_Db_Table::getDefaultAdapter();	
        
       $sql = $db->select()
                  ->from(array('er' => 'exam_registration')) 
                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('registrationId','IdProgramScheme'))
                  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration AND srs.IdSemesterMain=er.er_idSemester AND er.er_idSubject=srs.IdSubject',array('audit_program','exam_status'))
                  ->join(array('sp' => 'student_profile'),'sp.id =sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)','appl_fname','appl_lname'))  
                  ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName','IdProgram','ProgramCode','IdScheme'))      
                  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = sr.IdProgramScheme',array('mode_of_program'))   
                  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ps.mode_of_program',array('mop'=>'DefinitionDesc')) 
                  ->join(array('d2'=>'tbl_definationms'),'d2.idDefinition=ps.mode_of_study',array('mos'=>'DefinitionDesc'))           
                  ->where('srs.IdSemesterMain = ?',$idSemester)
                  ->where('er_ec_id = ?',$ec_id)
                  ->where('srs.IdSubject IN (?)',$subjectArr)   
                  ->where('sr.profileStatus = ?',92)
                  ->where('srs.Active != ?',3)
				  ->where('sr.registrationId != ?',"1409999")   
				  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")')            
                 // ->order(new Zend_Db_Expr("CASE WHEN sr.IdProgram=3 THEN 0 ELSE 1 END")) 
                 // ->order('sr.IdProgram')                
                 // ->order('sr.IdProgramScheme')
                 // ->order('sp.appl_fname')
                 // ->order('sp.appl_lname')
                 // ->order('sr.registrationId')
                  ->group('srs.IdStudentRegSubjects');
               
		//$result = $db->fetchAll($sql); 
        
		
		$semDB = new Registration_Model_DbTable_ExamRegistration();
		$semester_all = $semDB->getSemester($idSemester);

		//audit credit
		$sql2 = $db->select()
                  ->from(array('er' => 'exam_registration')) 
                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('registrationId','IdProgramScheme'))
                  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=sr.IdStudentRegistration AND srs.IdSemesterMain=er.er_idSemester AND er.er_idSubject=srs.IdSubject',array('audit_program','exam_status'))
                  ->join(array('sp' => 'student_profile'),'sp.id =sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)','appl_fname','appl_lname'))  
                  ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName','IdProgram','ProgramCode','IdScheme'))      
                  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = sr.IdProgramScheme',array('mode_of_program'))   
                  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ps.mode_of_program',array('mop'=>'DefinitionDesc')) 
                  ->join(array('d2'=>'tbl_definationms'),'d2.idDefinition=ps.mode_of_study',array('mos'=>'DefinitionDesc'))
                  ->where('srs.audit_program IS NOT NULL')           
                  ->where('srs.IdSemesterMain IN (?)',$semester_all)
                  ->where('er_ec_id = ?',$ec_id)
                  ->where('srs.IdSubject IN (?)',$subjectArr)   
                  ->where('sr.profileStatus = ?',92)
                  ->where('srs.Active != ?',3)
				  ->where('sr.registrationId != ?',"1409999")   
				  ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT")')            
                  ->group('srs.IdStudentRegSubjects');
               
		//$result2 = $db->fetchAll($sql2); 
        
                  $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
                  $semester_data = $semesterDB->getData($idSemester);
                  
                  if($semester_data['IdScheme']==11){
                  	//GS
                  	$select = $db->select()
				    		      ->union(array($sql, $sql2))
				    		      ->order(new Zend_Db_Expr("CASE WHEN IdProgram=3 THEN 0 ELSE 1 END")) 
				                  ->order('IdProgram')
				                  ->order('appl_fname')
				                  ->order('appl_lname')
				                  ->order('registrationId')
				                  ->order('IdProgramScheme');
                  }else{
                  	$select = $db->select()
				    		      ->union(array($sql, $sql2)) 
				                  ->order('IdProgram')                
				                  ->order('mop')
				                  ->order('appl_fname')
				                  ->order('appl_lname')
				                  ->order('registrationId');
                  }
                   
    
				  $result = $db->fetchAll($select);
			  
        return $result;
        
    }
    
    //nak cater nazira request to seperate by schme for mifp & cifp
 	public function getStudentByExamCenterProgram($idSemester,$ec_id,$subjectArr){
    	
    
    	$db = Zend_Db_Table::getDefaultAdapter();	
        
       $sql = $db->select()
                  ->from(array('er' => 'exam_registration'),array()) 
                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('studentid'=>'GROUP_CONCAT(DISTINCT(registrationId))','IdProgram'))
                  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = sr.IdProgramScheme',array('mode_of_program'))    
                  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ps.mode_of_program',array('mop'=>'DefinitionCode')) 
                  ->join(array('d2'=>'tbl_definationms'),'d2.idDefinition=ps.mode_of_study',array('mos'=>'DefinitionCode'))                              
                  ->where('er_idSemester = ?',$idSemester)
                  ->where('er_ec_id = ?',$ec_id)
                  ->where('er_idSubject IN (?)',$subjectArr)
                  ->where('sr.IdProgram IN (2,5)')
                  ->group('sr.IdProgram')
                  ->group('ps.mode_of_program');
               
		$groups = $db->fetchAll($sql); 

		/*if(count($groups)>0){
			//mifp & cifp detected
			
			//foreach group get student by scheme
			foreach($groups as $index=>$group){
				
				$registrationId_arr = explode (',',$group['studentid']);
				
				 $sql_student = $db->select()
                  ->from(array('er' => 'exam_registration')) 
                  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = er.er_idStudentRegistration',array('registrationId'))
                  ->join(array('sp' => 'student_profile'),'sp.id =sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))                
                  ->where('sr.registrationId IN (?)',$registrationId_arr)
                  ->order('sp.appl_fname')
                  ->order('sp.appl_lname')
                  ->order('sr.registrationId');
               
				$result_student = $db->fetchAll($sql_student);
				$groups[$index]['student'] =  $result_student;
			}
		}*/
        return $groups;
        
    }
	
	
}

?>