<?php

class Reports_Model_DbTable_Finance extends Zend_Db_Table_Abstract {
    
	public function getTotalByProgram($searchData=null){
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$semester = isset($searchData['semester']) ? $searchData['semester'] : 0;
		$status = array('A');
		
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('a'=>'invoice_detail'),array(
            	'totalAmount' => "
            		SUM(IF(b.currency_id = 2,".new Zend_Db_Expr ('bill_amount * cr.cr_exchange_rate').",".new Zend_Db_Expr ('bill_amount')."))",
            	"amount"=>"SUM(bill_amount)")
            	)
            ->join(array('b'=>'invoice_main'), 'a.invoice_main_id = b.id',array())
            ->joinLeft(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = b.exchange_rate',array())
            ->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = b.trans_id',array())
			->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
			->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = b.IdStudentRegistration AND (b.IdStudentRegistration IS NOT NULL OR b.IdStudentRegistration != 0)',array())
			->join(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array())
			->where("b.status IN (?)",$status)
			->where('pa.IdProgram =?', $program)
			->where("DATE(b.invoice_date) >= ?",$dateFrom)
			->where("DATE(b.invoice_date) <= ?",$dateTo);
            
		if($semester){
			$selectData->where('b.semester IN (?)', $semester);
		}
        
        $result = $db->fetchRow($selectData);
        return $result;
    }
}