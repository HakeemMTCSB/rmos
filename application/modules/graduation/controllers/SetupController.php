<?php
class Graduation_SetupController extends Base_Base {

	private $_sis_session;

	public function init(){
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}

	public function convocationAction() {
		//title
		$this->view->title= $this->view->translate("Convocation Setup");
				
		//get data
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$data = $convocationDb->getData();
		
		if(count($data)>0){
		foreach($data as $key=>$convo){
			//get award tagging
			$award = $convocationDb->getTaggingAward($convo['c_id']);
			$data[$key]['award']=$award;
		}
		}
		
		$this->view->data = $data;

	}
	
	public function convocationAddAction(){
		
		//title
		$this->view->title= $this->view->translate("Convocation Setup - Add");
		$auth = Zend_Auth::getInstance();
		$form = new Graduation_Form_Convocation();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$db = Zend_Db_Table::getDefaultAdapter();
				$convocationDb = new Graduation_Model_DbTable_Convocation();
				
				//check duplicate
				$exist = $convocationDb->checkDuplicate($formData['c_year'],$formData['c_session']);
				
				if($exist){
					$this->_helper->flashMessenger->addMessage(array('error' => 'Duplicate entry is not allowed. Convocation year '.$formData['c_year'].' session '.$formData['c_session'].' already exist.'));
					$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'setup', 'action'=>'convocation-add'),'default',true));
				}
				
				$convo['c_year'] = $formData['c_year'];
				$convo['c_session'] = $formData['c_session'];
				$convo['c_capacity'] = $formData['c_capacity'];
				$convo['c_guest'] = $formData['c_guest'];
				$convo['c_weblink'] = $formData['c_weblink'];
				$convo['c_date_from'] = date('Y-m-d', strtotime($formData['c_date_from']));
				$convo['c_date_to'] = date('Y-m-d', strtotime($formData['c_date_to']));
				$convo['c_confirm_attendance_enddate'] = date('Y-m-d', strtotime($formData['c_confirm_attendance_enddate']));
				$convo['c_record_verification_enddate'] = date('Y-m-d', strtotime($formData['c_record_verification_enddate']));
				$convo['c_guest_app_enddate'] = date('Y-m-d', strtotime($formData['c_guest_app_enddate']));
				$convo['c_student_portal_enddate'] = date('Y-m-d', strtotime($formData['c_student_portal_enddate']));
				$convo['last_edit_date']= date('Y-m-d H:i:s');
				$convo['last_edit_by']= $auth->getIdentity()->id;
				
				$id = $convocationDb->insert($convo);
				
				//award
				$tag['convocation_id'] = $id;
				$tag['ca_createddt']= date('Y-m-d H:i:s');
				$tag['ca_createdby']= $auth->getIdentity()->id;
				
				foreach($formData['award'] as $key=>$value){
					$tag['IdAward'] = $value;
					$db->insert('convocation_award',$tag);
				}

				//redirect				
				$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'setup', 'action'=>'convocation'),'default',true));
				
			}else{
				$form->populate($formData);
			}
			
		}
		$this->view->form = $form;
	}
	
	public function convocationEditAction(){
		
		$id = $this->_getParam("id", 0);
		
		//title
		$this->view->title= $this->view->translate("Convocation Setup - Edit");
		
		$auth = Zend_Auth::getInstance();
		$form = new Graduation_Form_Convocation();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
				
			if ($form->isValid($formData)) {
		
				$db = Zend_Db_Table::getDefaultAdapter();
				$convocationDb = new Graduation_Model_DbTable_Convocation();
				
				$convo['c_year'] = $formData['c_year'];
				$convo['c_session'] = $formData['c_session'];
				$convo['c_capacity'] = $formData['c_capacity'];
				$convo['c_guest'] = $formData['c_guest'];
				$convo['c_weblink'] = $formData['c_weblink'];
				$convo['c_date_from'] = date('Y-m-d', strtotime($formData['c_date_from']));
				$convo['c_date_to'] = date('Y-m-d', strtotime($formData['c_date_to']));
				$convo['c_confirm_attendance_enddate'] = date('Y-m-d', strtotime($formData['c_confirm_attendance_enddate']));
				$convo['c_record_verification_enddate'] = date('Y-m-d', strtotime($formData['c_record_verification_enddate']));
				$convo['c_guest_app_enddate'] = date('Y-m-d', strtotime($formData['c_guest_app_enddate']));
				$convo['c_student_portal_enddate'] = date('Y-m-d', strtotime($formData['c_student_portal_enddate']));				
				$convo['last_edit_date']= date('Y-m-d H:i:s');
				$convo['last_edit_by']= $auth->getIdentity()->id;
				
				$convocationDb->update($convo,'c_id = '.$formData['c_id']);
		
				//award
				//1)delete previous award tagging
				$convocationDb->deleteAwardTag($formData['c_id']);
				
				
				//2)add new award tagging
				$tag['convocation_id'] = $formData['c_id'];
				$tag['ca_createddt']= date('Y-m-d H:i:s');
				$tag['ca_createdby']= $auth->getIdentity()->id;
				
				foreach($formData['award'] as $key=>$value){
					$tag['IdAward'] = $value;
					$db->insert('convocation_award',$tag);
				}
				
				//redirect
				$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'setup', 'action'=>'convocation'),'default',true));
		
			}else{
				$form->populate($formData);
			}
				
		}else{
			
			$convocationDb = new Graduation_Model_DbTable_Convocation();
			$data = $convocationDb->getData($id);
			
			$award = $convocationDb->getTaggingAward($id);
			$award_arr = array();
			foreach($award as $key=>$value){
				array_push($award_arr,$value['id']);
			}
			$data['award']=$award_arr;
			
			$form->populate($data);
				
		}
		
		$this->view->form = $form;
		
	}
	
	public function convocationDeleteAction(){
		
		$id = $this->_getParam("id", 0);
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$checklistDB = new Graduation_Model_DbTable_ConvocationChecklist();
		
		if($id !=0){
			$convocationDb->deleteConvocation($id);
			
			//delete award
			$convocationDb->deleteAwardTag($id);
			
			//delete checklist tagging
			$where = 'c_id = '.(int)$id;
			$checklistDB->deleteChecklistByConvo($where);
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been deleted'));
		}
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'setup', 'action'=>'convocation'),'default',true));
	}
	
	
	
}
?>