<?php
class Graduation_DocumentsController extends Base_Base 
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->documentModel = new Graduation_Model_DbTable_GraduationDocuments();
		$this->documentDetailModel = new Graduation_Model_DbTable_GeneralDocuments();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();

		$this->uploadDir = $this->uploadDir = DOCUMENT_PATH.'/graduation/';
		
	}

	public function indexAction()
	{
		//title
    	$this->view->title = "Graduation - Downloads";

		$p_data = $this->documentModel->getData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}
	
	public function addAction()
	{
		$this->view->title = "Graduation - Add Document";
		
		$form = new Graduation_Form_Document();
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();

			//if($form->isValid($formData)){
				//var_dump($formData); exit;
				//data
				$data = array(
								'gd_title'					=> $formData['title'],
								'gd_type'					=> $formData['type'],
								'gd_description'			=> $formData['description'],
								'created_by'				=> $this->auth->getIdentity()->iduser,
								'created_date'				=> new Zend_Db_Expr('NOW()'),
								'created_role'				=> 'admin'
							);
	
							
				$download_id = $this->documentModel->addData($data);
	
				//upload file
				try 
				{
					$uploadDir = $this->uploadDir;
	
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create graduation document folder ('.$uploadDir.')');
						}
					}
	
					$uploadDir = $this->uploadDir.date('Y');
					$filepath = '/graduation/'.date('Y');
					
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create graduation document folder ('.$uploadDir.')');
						}
					}
					
					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
	
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $uploadDir );
					//$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 20971520 , 'bytestring' => true));
	
					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);
	
							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;
	
							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'gd_table_name'		=> 'tbl_graduation_documents',
											'gd_table_id'		=> $download_id,
											'gd_filename'		=> $fileName,
											'gd_ori_filename'	=> $fileinfo['name'],
											'gd_filepath'		=> $filepath.'/'.$fileName,
											'gd_type'			=> $formData['type'],
											'gd_filetype'		=> 0,
											'gd_createddt'		=> new Zend_Db_Expr('NOW()'),
											'gd_createdby'		=> $this->auth->getIdentity()->iduser
										);
								
							$this->documentDetailModel->addData($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}
	
				$this->_helper->flashMessenger->addMessage(array('success' => "Documents has been uploaded."));
			
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'documents', 'action'=>'edit', 'id' => $download_id ),'default',true));
				
			//}
			
		}

		//views
		$this->view->form = $form;
	}

	public function editAction()
	{
		$this->view->title = "Graduation - Edit Download";
		
		$form = new Graduation_Form_Document();

			
		$id = $this->_getParam('id');

		$info = $this->documentModel->getDocumentById($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Download ID');
		}

		
		$this->view->info = $info;

		$info['type']=$info['gd_type'];
		$info['title']=$info['gd_title'];
		$info['description']=$info['gd_description'];
		$form->populate($info);

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->documentDetailModel->getmyDocuments('tbl_graduation_documents',$info['gd_id']);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();

			if($form->isValid($formData)){
				
				//data
				$data = array(
								//'gd_type'					=> $formData['type'],
								'gd_title'					=> $formData['title'],
								'gd_description'			=> $formData['description'],
								'updated_by'				=> $this->auth->getIdentity()->iduser,
								'updated_date'				=> new Zend_Db_Expr('NOW()'),
								'updated_role'				=> 'admin'
							);
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
	
				$status_id = $this->documentModel->updateData($data, $id );
	
				//upload file
				try 
				{
					$uploadDir = $this->uploadDir;
	
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create thesis document folder ('.$uploadDir.')');
						}
					}
	
					$uploadDir = $this->uploadDir.date('Y');
					$filepath = '/graduation/'.date('Y');
					
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create graduation document folder ('.$uploadDir.')');
						}
					}
					
					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
	
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $uploadDir );
					//$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
	
					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);
	
							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;
	
							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'gd_table_name'		=> 'tbl_graduation_documents',
											'gd_table_id'		=> $id,
											'gd_filename'		=> $fileName,
											'gd_ori_filename'	=> $fileinfo['name'],
											'gd_filepath'		=> $filepath.'/'.$fileName,
											'gd_type'			=> $formData['type'],
											'gd_filetype'		=> 0,
											'gd_createddt'		=> new Zend_Db_Expr('NOW()'),
											'gd_createdby'		=> $this->auth->getIdentity()->iduser
										);
								
							$this->documentDetailModel->addData($data);
							
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}
			 
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'documents', 'action'=>'edit', 'id' => $id ),'default',true));
				
			}
			
			
		}

		
		//views
		$this->view->form = $form;
	}

	public function deleteAction()
	{
	
		$id = $this->_getParam('id');

		$info = $this->documentModel->getDocumentById($id);
		
		if ( empty($info) )
		{
			throw new Exception('Invalid Document ID');
		}
	
		//delete all files
		$uploadedfiles = $this->documentDetailModel->getmyDocuments('tbl_graduation_documents',$info['gd_id']);
		foreach ( $uploadedfiles as $file )
		{
			$this->documentDetailModel->deleteData($file['gd_id']);

			//unlink
			@unlink(DOCUMENT_PATH.$file['gd_filepath']);
		}

		//delete document entry
		$this->documentModel->deleteData($id);
		
	
		$this->_helper->flashMessenger->addMessage(array('success' => "Document successfully deleted"));

		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'documents', 'action'=>'index'),'default',true));

	}

	public function deleteFileAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$db = getDb();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$file = $this->documentDetailModel->getDocumentById($id);

			if ( empty($file) )
			{
				$data = array('msg' => 'Invalid ID');

				echo Zend_Json::encode($data);
				exit;
			}

			$this->documentDetailModel->deleteData($id);

			//unlink
			@unlink(DOCUMENT_PATH.$file['gd_filepath']);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
		}
	}
}