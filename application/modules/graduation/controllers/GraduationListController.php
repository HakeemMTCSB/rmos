<?php
use Zend\Ldap\Converter\Converter;
class Graduation_GraduationListController extends Base_Base {

	private $_sis_session;

	public function init(){
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}
	
	
	public function indexAction() {
		//title
		$this->view->title= $this->view->translate("Graduation List");
	
		//faculty list
		$facultyDb = new GeneralSetup_Model_DbTable_Collegemaster();
		$faculty_list = $facultyDb->getCollege();
	
		//program list
		$programDb = new GeneralSetup_Model_DbTable_Program();
	
		foreach ($faculty_list as &$faculty){
			$faculty['program'] = $programDb->fngetFacultyProgramData($faculty['IdCollege']);
		}
	
		$this->view->program = $faculty_list;
	
	
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();
	
	
		$graduationDb = new Graduation_Model_DbTable_Graduation();
	
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();

			$formData['search_date_add_from'] = $this->view->escape(strip_tags($formData['search_date_add_from']));
			$formData['search_date_add_to'] = $this->view->escape(strip_tags($formData['search_date_add_to']));
			$formData['search_nim'] = $this->view->escape(strip_tags($formData['search_nim']));

			if($formData['search_nim']!=""){
				$filter['registrationId'] =  $formData['search_nim'];
				$this->view->search_nim = $formData['search_nim'];
			}
	
			if($formData['search_program_id']!=0){
				$filter['pr.IdProgram'] =  $formData['search_program_id'];
				$this->view->search_program_id = $formData['search_program_id'];
			}
	
			if($formData['search_intake_id']!=0){
				$filter['itk.IdIntake'] =  $formData['search_intake_id'];
				$this->view->search_intake_id = $formData['search_intake_id'];
			}
				
			if($formData['search_dean_approve']!=0){
				$filter['dean_approval'] = $formData['search_dean_approve'];
				$this->view->search_dean_approval = $formData['search_dean_approve'];
			}
				
			if($formData['search_rector_approve']!=0){
				$filter['rector_approval'] = $formData['search_rector_approve'];
				$this->view->search_rector_approval = $formData['search_rector_approve'];
			}
			
			if($formData['search_date_add_from']!=""){
				$filter['date_add_from'] = $formData['search_date_add_from'];
				$this->view->search_date_add_from = $formData['search_date_add_from'];
			}
			
			if($formData['search_date_add_to']!=""){
				$filter['date_add_to'] = $formData['search_date_add_to'];
				$this->view->search_date_add_to = $formData['search_date_add_to'];
			}
	
	
			$this->view->list = $graduationDb->getFilteredData($filter);
		}else{
			$this->view->list = $graduationDb->getData();
		}
	
	}
	
	public function generateListAction(){
		
		$ses_generate_graduation_list = new Zend_Session_Namespace("grad_ses");
		$this->view->ses_data = $ses_generate_graduation_list;
		
		//title
		$this->view->title= $this->view->translate("Graduation List : Selection process");
		
		$step = $this->_getParam("step", null);
		$this->view->step = $step;
			
		if($step==null){
			$step = 1;
			Zend_Session::namespaceUnset('grad_ses');
				
			//redirect
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'generate-list', 'step'=>$step),'default',true));
		}
		
		if($step==1){
						
			if($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
		
				if( isset($formData['student']) && sizeof($formData['student'])>0 ){
					$ses_generate_graduation_list->student_list = $formData['student'];
				}
					
				//redirect
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'generate-list', 'step'=>2),'default',true));
					
			}else{
					
				//intake
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$this->view->intake_list = $intakeDb->getIntakeWithStudent(92);
		
				if(isset($ses_generate_graduation_list->intake_id)){
					$this->view->intake_id = $ses_generate_graduation_list->intake_id;
				}
				
				//faculty list
				$facultyDb = new GeneralSetup_Model_DbTable_Collegemaster();
				$faculty_list = $facultyDb->getCollege();
				
				//program list
				$programDb = new GeneralSetup_Model_DbTable_Program();
				
				foreach ($faculty_list as &$faculty){
					$faculty['program'] = $programDb->fngetFacultyProgramData($faculty['IdCollege']);
				}
				
				$this->view->program = $faculty_list;
					
			}
			
		}else 
		if($step==2){
			if(!isset($ses_generate_graduation_list->student_list)){
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'generate-list', 'step'=>1),'default',true));
			}
			
			
			if($this->getRequest()->isPost()) {
					
				//save to grad table
				$graduationDb = new Graduation_Model_DbTable_Graduation();
			
			
				$student = $ses_generate_graduation_list->student_list;
			
				foreach ($student as $idStudentRegistration){
					$data = array(
							'idStudentRegistration' => $idStudentRegistration,
					);
			
					$graduationDb->insert($data);
				}
			
				//redirect
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'index'),'default',true));
					
			}else{
					
				//loop all student list
				$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
				$studentProfileDb = new Records_Model_DbTable_Studentprofile();
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$studentGradeDb = new Examination_Model_DbTable_StudentGrade();
				$programDb = new GeneralSetup_Model_DbTable_Program();
				$db = Zend_Db_Table::getDefaultAdapter();
			
				$pregrad_list = array();
				foreach ($ses_generate_graduation_list->student_list as $i => $student){
					$arr_data = array();
			
					//get registration detail
					$arr_data['registration_info'] = $studentRegistrationDb->fetchRow(array('idStudentRegistration=?'=>$student))->toArray();
			
					//profile
					$arr_data['profile'] = $studentProfileDb->fetchRow(array('appl_id=?'=>$arr_data['registration_info']['IdApplication']))->toArray();
			
					//intake
					$arr_data['intake'] = $intakeDb->getData($arr_data['registration_info']['IdIntake']);
			
					//current sem/level/block
					$select = $db->select()
					->from(array('ss'=>'tbl_studentsemesterstatus'),array('idStudentRegistration', 'max_level'=>'MAX(Level)'))
					->where('ss.idStudentRegistration = ?', $student)
					->group('ss.IdStudentRegistration');
			
					$arr_data['current_level'] = $db->fetchRow($select);
			
					//program
					$arr_data['program'] = $programDb->fetchRow(array('IdProgram = ?'=>$arr_data['registration_info']['IdProgram']))->toArray();
			
					//cgpa TODO:uncomment
					/*$cgpa = $studentGradeDb->getStudentGradeInfo($arr_data['registration_info']['IdStudentRegistration'])->sg_cgpa;
					if(isset($cgpa)){
						$arr_data['cgpa'] = $cgpa;
					}else{
						$arr_data['cgpa'] = 0.00;
					}*/
					$arr_data['cgpa'] = 0.00;
			
					$pregrad_list[] = $arr_data;
				}
			
				$this->view->list = $pregrad_list;
			
			}
			
		}
			
	}
	
	public function searchStudentAction(){
	
		$ses_generate_graduation_list = new Zend_Session_Namespace("grad_ses");
	
		$data_landscape = array();
		$data_requirement = array();
	
		$this->_helper->layout()->disableLayout();
	
		$program = $this->_getParam("program", 0);
		$intake = $this->_getParam("intake", 0);
		$payment = $this->_getParam("payment", 0);
		$cgpa = $this->_getParam("cgpa", null);
		
		if($program==0 || $intake==0 || $cgpa==null){
			throw new Exception('Parameter not complete');
			exit;
		}
	
	
		//find student who is active
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select2 = $db->select()
		->from(array('ss'=>'tbl_studentsemesterstatus'),array('idStudentRegistration', 'max_level'=>'MAX(Level)'))
		->group('ss.IdStudentRegistration');
	
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intake_data = $intakeDb->getData($intake);
		$list['intake'] = $intake_data;
	
		$select = $db->select()
		->from(array('sr'=>'tbl_studentregistration'), array('IdStudentRegistration', 'registrationId','IdLandscape', 'IdProgram','IdProgramMajoring','IdIntake'))
		->join(array('sp'=>'student_profile'),'sp.appl_id = sr.IdApplication', array("fullname"=>"concat_ws(' ',appl_fname,appl_mname,appl_lname)"))
		->join(array('sss'=>$select2), 'sss.idStudentRegistration = sr.IdStudentRegistration')
		->where('NOT EXISTS (SELECT NULL from pregraduate_list a WHERE a.idStudentRegistration = sr.IdStudentRegistration)')
		->where('NOT EXISTS (SELECT NULL from graduate b WHERE b.idStudentRegistration = sr.IdStudentRegistration)')
		->where('sr.IdProgram = ?',$program)
		->where('sr.IdLandscape != 0')
		->where('sr.IdIntake = ?', $intake)
		//->where('sr.IdProgramMajoring != 0')
		->where('sr.profileStatus = 92')
		->order('sss.max_level DESC');
	
		$list['student'] = $db->fetchAll($select);
	
	
	
		//loop all student list
		$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
		$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
		$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
	
		$blockSemDB = new GeneralSetup_Model_DbTable_LandscapeBlockSemester();
		$blockCourseDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		$academicProgressDb = new Records_Model_DbTable_Academicprogress();
	
		$studentGradeDb = new Examination_Model_DbTable_StudentGrade();
		$paymentInfo = new icampus_Function_Studentfinance_PaymentInfo();
	
		foreach ($list['student'] as $i => &$student){
				
				
			/*
			 * get program requirement info and put in reuseable array
			*/
			if( !isset($data_requirement[$student['IdProgram']][$student['IdLandscape']]) ){
				$data_requirement[$student['IdProgram']][$student['IdLandscape']] = $this->getProgramRequirement($student['IdProgram'], $student['IdLandscape']);
			}
				
			$student['ch_requirement'] = $data_requirement[$student['IdProgram']][$student['IdLandscape']];
				
				
				
			/*
			 * get landscape data and put in reuseable array
			*/
			if( !isset($data_landscape[$student['IdProgram']][$student['IdLandscape']][$student['IdProgramMajoring']]) ){
				$data_landscape[$student['IdProgram']][$student['IdLandscape']][$student['IdProgramMajoring']] = $this->getLanscapeDataByMajoring($student['IdLandscape'],$student['IdProgramMajoring'],$student['IdProgram']);
			}
			//HEAVY PROCESSING
			//do not store into array student (timeout will occur)
			$courses = $data_landscape[$student['IdProgram']][$student['IdLandscape']][$student['IdProgramMajoring']];
	
				
				
			/*
			 * 	HEAVY PROCESSING
			*	check for subject completeness
			*/
			foreach ($courses['all'] as $index => &$subject){
	
				if(isset($subject['coursetypeid'])){
					$subject['SubjectType'] = $subject['coursetypeid'];
				}
	
				$grademark = $academicProgressDb->checkcompleted($student['IdStudentRegistration'],$subject["IdSubject"]);
	
				//requirement - subject count
				$student['ch_requirement'][$subject['SubjectType']]['total_subject_count'] += 1;
	
	
				if($grademark){
					//$list[$i]['courses']['all'][$index]['completed'] = 1;
						
					//requirement - subject ch completed
					$student['ch_requirement'][$subject['SubjectType']]['ch_completed'] = $student['ch_requirement'][$subject['SubjectType']]['ch_completed'] + $subject['CreditHours'];
						
					//requirement - subject count completed
					$student['ch_requirement'][$subject['SubjectType']]['total_subject_completed_count'] += 1;
						
				}else{
					//$list[$i]['courses']['all'][$index]['completed'] = 0;
				}
	
			}
				
				
			//get cgpa TODO:uncomment
			/*if(isset($studentGradeDb->getStudentGradeInfo($student['IdStudentRegistration'])->sg_cgpa)){
				$student['cgpa'] = $studentGradeDb->getStudentGradeInfo($student['IdStudentRegistration'])->sg_cgpa;
			}else{
				$student['cgpa'] = 0.00;
			}*/
			$student['cgpa'] = 0.00;	
				
			//outstanding payment
			//$student['finance'] = $paymentInfo->getStudentPaymentInfo($student['IdStudentRegistration']);
			
			
				
				
				
		}// end loop student list
	
	
		/*
		 * Run filter TODO:uncommetn unset
		*/
		if(is_array($list['student'])){
			foreach ($list['student'] as $index=> $student){
	
				//filter by subject bill
				$subject_count = 42;
				
				if($subject_count){
						
					$sub_remaining = 0;
					foreach ($student['ch_requirement'] as $sub_req){
						$sub_remaining += $sub_req['total_subject_count'] - $sub_req['total_subject_completed_count'];
					}
						
					if($sub_remaining > $subject_count){
						/*unset($list['student'][$index]);
						continue;*/
					}
						
				}
	
				//filter by outstanding payment
				$student['finance']['total_invoice_balance'] = 0;
				/*if($payment=='1'){
						
					if( $student['finance']['total_invoice_balance'] > 0 ){
						/*unset($list['student'][$index]);
						continue;*/
					/*}
						
				}*/
	
				//filter by cgpa
				/*if($cgpa!=null && $cgpa!=""){
					if(floatval($cgpa) > $student['cgpa']){
						/*unset($list['student'][$index]);
						continue;*/
					/*}
				}*/
			}
				
			$list['student'] = array_values($list['student']);
		}
	
	
		/*echo "<pre>";
			print_r($list['student']);
		echo "<pre>";
		exit;*/
	
	
		//pack to json
		$json_data = Zend_Json_Encoder::encode($list);
		echo $json_data;
		exit;
	
	}
	
	private function getProgramRequirement($program_id, $landscape_id){
	
		$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
	
		$programRequirement = $progReqDB->getlandscapecoursetype($program_id,$landscape_id);
			
		//requirement detail
		$arr_req_detail = array();
		foreach ($programRequirement as $req){
			$arr_req_detail[$req['SubjectType']] = array(
					'ch_required'=>$req['CreditHours'],
					'ch_completed'	=> 0, //init ch completed
					'total_subject_count' => 0, //init sub count
					'total_subject_completed_count' => 0 //init sub completed count
			);
		}
	
		return $arr_req_detail;
	}
	
	private function getLanscapeDataByMajoring($landscape_id, $majoring_id,$program_id){
	
		$courses = array();
	
		$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
		$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
		$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
	
		$blockSemDB = new GeneralSetup_Model_DbTable_LandscapeBlockSemester();
		$blockCourseDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
	
		//get landscape info
		$landscape = $landscapeDB->getLandscapeDetails($landscape_id);
			
	
			
		//get 2 type of subjects including major subject
		if($landscape["LandscapeType"]==42 || $landscape["LandscapeType"]==43) {//Semester & level Based
	
			//get Common Course
			$common_course = $landscapeSubjectDb->getCommonCourse($program_id,$landscape_id);
			$courses['all'] = $common_course;
	
			//majoring course
			if($majoring_id!=null && $majoring_id!=0){
				$majoring_course = $landscapeSubjectDb->getMajoringCourse($program_id, $landscape_id, $majoring_id);
				$courses['all'] = array_merge($courses['all'], $majoring_course);
			}
	
	
		}else
		if($landscape["LandscapeType"]==44){
				
			$courses['all'] = array();
	
			for($ii=1; $ii<=$landscape["Blockcount"]; $ii++){
					
				$blocks = $blockSemDB->getlandscapeblockBySem($landscape_id,$ii);
	
				foreach($blocks as $block){
	
					//get course
					$common_course = $blockCourseDb->getBlockCourse($landscape_id,$block["idblock"]);
					$courses['all'] = array_merge($courses['all'],$common_course);
				}
			}
	
		}
	
		return $courses;
	}
	
	
	public function deanApprovalAction(){
	
		$this->view->title = "Graduation - Dean Approval";
	
		//faculty list
		$facultyDb = new GeneralSetup_Model_DbTable_Collegemaster();
		$faculty_list = $facultyDb->getCollege();
	
		//program list
		$programDb = new GeneralSetup_Model_DbTable_Program();
	
		foreach ($faculty_list as &$faculty){
			$faculty['program'] = $programDb->fngetFacultyProgramData($faculty['IdCollege']);
		}
	
		$this->view->program = $faculty_list;
	
	
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();
	
		//skr
		$skrDb = new Graduation_Model_DbTable_Skr();
		$this->view->skr_list = $skrDb->getActiveData('graduation');
	
	
		$graduationDb = new Graduation_Model_DbTable_Graduation();
	
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
				
			if($formData['search_nim']!=""){
				$filter['registrationId'] =  $formData['search_nim'];
				$this->view->search_nim = $formData['search_nim'];
			}
				
			if($formData['search_program_id']!=0){
				$filter['pr.IdProgram'] =  $formData['search_program_id'];
				$this->view->search_program_id = $formData['search_program_id'];
			}
				
			if($formData['search_intake_id']!=0){
				$filter['itk.IdIntake'] =  $formData['search_intake_id'];
				$this->view->search_intake_id = $formData['search_intake_id'];
			}
				
				
			$this->view->data = $graduationDb->getDataToApprove(1,$filter);
		}else{
			$this->view->data = $graduationDb->getDataToApprove(1);
		}
	
	
	}
	
	public function deanApproveAction(){
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
				
			$graduationDb = new Graduation_Model_DbTable_Graduation();
				
			foreach ($formData['student'] as $student){
				$data = array(
						'dean_approval_Date'=>date('Y-m-d H:i:s'),
						'dean_approval_skr' => $formData['skr']
				);
	
				$graduationDb->update($data, 'id = '.$student);
			}
				
		}
	
	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'dean-approval'),'default',true));
	
	
	}
	
	public function deanRejectAction(){
	
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			$graduationDb = new Graduation_Model_DbTable_Graduation();
	
			foreach ($formData['student'] as $student){
				$graduationDb->delete('id = '.$student);
			}
	
		}
	
	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'dean-approval'),'default',true));
	
	}
	
	public function rectorApprovalAction(){
	
		$this->view->title = "Graduation - Rector Approval";
	
		//faculty list
		$facultyDb = new GeneralSetup_Model_DbTable_Collegemaster();
		$faculty_list = $facultyDb->getCollege();
	
		//program list
		$programDb = new GeneralSetup_Model_DbTable_Program();
	
		foreach ($faculty_list as &$faculty){
			$faculty['program'] = $programDb->fngetFacultyProgramData($faculty['IdCollege']);
		}
	
		$this->view->program = $faculty_list;
	
	
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();
	
		//skr
		$skrDb = new Graduation_Model_DbTable_Skr();
		$this->view->skr_list = $skrDb->getActiveData('graduation');
	
	
		$graduationDb = new Graduation_Model_DbTable_Graduation();
	
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			if($formData['search_nim']!=""){
				$filter['registrationId'] =  $formData['search_nim'];
				$this->view->search_nim = $formData['search_nim'];
			}
	
			if($formData['search_program_id']!=0){
				$filter['pr.IdProgram'] =  $formData['search_program_id'];
				$this->view->search_program_id = $formData['search_program_id'];
			}
	
			if($formData['search_intake_id']!=0){
				$filter['itk.IdIntake'] =  $formData['search_intake_id'];
				$this->view->search_intake_id = $formData['search_intake_id'];
			}
	
	
			$this->view->data = $graduationDb->getDataToApprove(2,$filter);
		}else{
			$this->view->data = $graduationDb->getDataToApprove(2);
		}
	
	}
	
	public function rectorApproveAction(){
		
		
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			$graduationDb = new Graduation_Model_DbTable_Graduation();
			$registrationDb = new App_Model_Record_DbTable_StudentRegistration();
	
			foreach ($formData['student'] as $student){
				$data = array(
						'rector_approval_Date'=>date('Y-m-d H:i:s'),
						'rector_approval_skr' => $formData['skr']
				);
	
				$graduationDb->update($data, 'id = '.$student);
				
				//get registration data
				$graduation_data = $graduationDb->getData($student);
				
				//update student profile status to graduated defid = 96
				$regData = array(
						'profileStatus' => 96
				);
				
				
				$registrationDb->update($regData, 'IdStudentRegistration = ' . $graduation_data['idStudentRegistration']);
				
			}
	
		}
	
	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'rector-approval'),'default',true));
	
	
	}
	
	public function rectorRejectAction(){
	
		if($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			$graduationDb = new Graduation_Model_DbTable_Graduation();
	
			foreach ($formData['student'] as $student){
				$graduationDb->delete('id = '.$student);
			}
	
		}
	
	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'rector-approval'),'default',true));
	
	}
}
?>