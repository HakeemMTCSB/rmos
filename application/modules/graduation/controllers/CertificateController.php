<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 18/3/2016
 * Time: 4:07 PM
 */
class Graduation_CertificateController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Graduation_Model_DbTable_Certificate();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Certificate');

        $form = new Graduation_Form_ReportsSearchForm();
        $this->view->form = $form;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->studentList($formData);
            $this->view->list = $list;
            $form->populate($formData);

            $template = $this->model->getTemplate($formData['program']);
            $this->view->template = $template;
        }
    }

    public function templateAction(){
        $this->view->title = $this->view->translate('Certificate Template Setup');

        $programList = $this->model->getProgram();
        $this->view->programList = $programList;
    }

    public function templateProgramAction(){
        $programid = $this->_getParam('id',null);

        $template = $this->model->getTemplate($programid);

        $programInfo = $this->model->getProgramById($programid);

        $this->view->programid = $programid;
        $this->view->template = $template;

        $this->view->title = $this->view->translate('Certificate Template Setup ('.$programInfo['ProgramName'].')');
    }

    public function templateSetupAction(){
        $id = $this->_getParam('id',null);

        $tplModel = new Communication_Model_DbTable_Template();

        $template_tags = $tplModel->getTemplateTags('graduation', $id);
        $this->view->tags = $template_tags;

        $template = $this->model->getTemplateById($id);

        $this->view->template = $template;

        $this->view->title = $this->view->translate('Certificate Template Setup ('.$template['ProgramName'].')');

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'crt_name'=>$formData['title'],
                'crt_footer'=>$formData['footer_content'],
                'crt_content'=>$formData['content'],
                'crt_updby'=>$this->auth->getIdentity()->iduser,
                'crt_upddate'=>date('Y-m-d H:i:s')
            );

            $this->model->updateTemplate($data, $template['crt_id']);

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/graduation/certificate/template-setup/id/'.$id);
        }
    }

    public function templateAddAction(){
        $id = $this->_getParam('id',null);

        $programInfo = $this->model->getProgramById($id);

        $this->view->id = $id;
        $this->view->title = $this->view->translate('Certificate Template Setup ('.$programInfo['ProgramName'].')');

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'crt_program_id'=>$id,
                'crt_name'=>$formData['title'],
                'crt_footer'=>$formData['footer_content'],
                'crt_content'=>$formData['content'],
                'crt_updby'=>$this->auth->getIdentity()->iduser,
                'crt_upddate'=>date('Y-m-d H:i:s')
            );

            $crtid = $this->model->insertTemplate($data);

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/graduation/certificate/template-setup/id/'.$crtid);
        }
    }

    public function printAction(){

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $contentAll = '';
            //$refno = array();
            if (isset($formData['check']) && count($formData['check']) > 0){
                $refnoCount = 2;
                foreach ($formData['check'] as $id => $value){
                    //$studentInfo = $this->model->studentInfo($id);

                    $template = $this->model->getTemplateById($formData['template']);

                    if (!$template){
                        $template = $this->model->getTemplate(0);
                    }

                    if (!$template){
                        $template['crt_content'] = '';
                        $template['crt_footer'] = '';
                    }

                    $content = $template['crt_content'];
                    $contentAll .= $content;
                    $contentAll .= '<div style="page-break-before: always;"></div>';
                    $contentAll .= '<br /><br /><br /><br /><br /><br /><br /><br /><br />
                                    <br /><br /><br /><br /><br /><br /><br /><br /><br />
                                    <br /><br /><br /><br /><br /><br /><br /><br /><br />
                                    <br /><br /><br /><br /><span style="font-size: smaller; font-family: helvetica, georgia, serif;">[Ref No]</span>';
                    $contentAll .= '<div style="page-break-after: always;"></div>';
                    $contentAll = $this->parseTag($id, $contentAll);
                }
            }

            $this->generatePDF($contentAll, 'cert', '', $template['crt_footer']);
        }
        exit;
    }

    public function parseTag($id, $content){
        $studentInfo = $this->model->studentInfo($id);

        $content = str_replace('[Student Name]', ucwords(strtolower($studentInfo['appl_fname'].' '.$studentInfo['appl_lname'])), $content);
        $content = str_replace('[Programme]', $studentInfo['ProgramName'], $content);

        //ref no part
        if ($studentInfo['cert_ref_no'] != null){
            $content = str_replace('[Ref No]', $studentInfo['cert_ref_no'], $content);
        }else{
            $seqno = $this->model->getSequence($studentInfo['IdProgram']);

            $refno = strtoupper((($seqno['csn_code']==null || $seqno['csn_code']=='') ? $seqno['code']:$seqno['csn_code']).sprintf("%06d", $seqno['csn_seq_no']));

            $seqdata = array(
                'csn_seq_no'=>$seqno['csn_seq_no']+1
            );
            $this->model->updateSequence($seqdata, $seqno['csn_id']);

            $studData = array(
                'cert_ref_no'=>$refno
            );
            $this->model->updateCertNo($studData, $studentInfo['studentid']);

            $content = str_replace('[Ref No]', $refno, $content);
        }

        return $content;
    }

    function generateRefNo($id){
        $studentInfo = $this->model->studentInfo($id);

        if ($studentInfo['cert_ref_no'] != null){
            $content = $studentInfo['cert_ref_no'];
        }else{
            $seqno = $this->model->getSequence($studentInfo['IdProgram']);

            $refno = strtoupper((($seqno['csn_code']==null || $seqno['csn_code']=='') ? $seqno['code']:$seqno['csn_code']).sprintf("%06d", $seqno['csn_seq_no']));

            $seqdata = array(
                'csn_seq_no'=>$seqno['csn_seq_no']+1
            );
            $this->model->updateSequence($seqdata, $seqno['csn_id']);

            $studData = array(
                'cert_ref_no'=>$refno
            );
            $this->model->updateCertNo($studData, $studentInfo['studentid']);

            $content = $refno;
        }

        return $content;
    }

    function generatePDF($html,$name,$url,$footer){

        include 'Hijri_GregorianConvert.class';

        $DateConv=new Hijri_GregorianConvert;

        $format="YYYY/MM/DD";
        $date= date('Y/m/d');

        $hijriDate = $DateConv->GregorianToHijri($date,$format);

        $hijriDay = date('dS', strtotime('2016-01-'.$hijriDate['day']));

        switch($hijriDate['month']){
            case '01' :
                $hijriMonth = "Muharram";
                break;
            case '02' :
                $hijriMonth = "Safar";
                break;
            case '03' :
                $hijriMonth = "Rabi'ulawwal";
                break;
            case '04' :
                $hijriMonth = "Rabi'ulakhir";
                break;
            case '05' :
                $hijriMonth = "Jamadilawwal";
                break;
            case '06' :
                $hijriMonth = "Jamadilakhir";
                break;
            case '07' :
                $hijriMonth = "Rejab";
                break;
            case '08' :
                $hijriMonth = "Sha'ban";
                break;
            case '09' :
                $hijriMonth = "Ramadhan";
                break;
            case '10' :
                $hijriMonth = "Shawwal";
                break;
            case '11' :
                $hijriMonth = "Dzulkaedah";
                break;
            case '12' :
                $hijriMonth = "Dzulhijjah";
                break;
        }

        $hijriYear = $hijriDate['year'];

        $hijriFormatDate = $hijriDay.' Day of '.$hijriMonth.' '.$hijriYear;
        $gregorianDate = date('dS').' Day of '.date('F').' '.date('Y');

        //exit;
        $options = array(
            'content' => $html,
            'file_name' => $name,
            'file_extension' => 'pdf',
            'save_path' => $url,
            'save' => false,
            'css' => '@page { margin: 150px 50px 10px 50px}
                body {}
                table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
                table.tftable tr {background-color:#ffffff;}
                table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
            'header' => '<script type="text/php">
                if ( isset($pdf) ) {

                }
                </script>',
            'footer' => '<script type="text/php"></script>',
            'type'=>'landscape',
            'alternate'=>'
                if (($PAGE_NUM % 2) == 1) {
                    $font = Font_Metrics::get_font("helvetica", "normal");
                    $size = 10;
                    //$pageText = "This '.$hijriFormatDate.' / '.$gregorianDate.'";
                    $pageText = "'.strip_tags($footer).'";
                    $y = $pdf->get_height() - 24;
                    $x = ($pdf->get_width() /2) - ( Font_Metrics::get_text_width($pageText, $font, $size) / 2);
                    $pdf->text($x, $y, $pageText, $font, $size);
                }
            '
        );

        //generate pdf
        generatePdf($options);
    }

    public function templateDeleteAction(){
        $id = $this->_getParam('id',null);
        $programid = $this->_getParam('programid',null);

        $this->model->deleteTemplate($id);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Template Deleted!'));
        $this->_redirect($this->baseUrl . '/graduation/certificate/template-program/id/'.$programid);
    }

    public function runningNumberAction(){
        $this->view->title = $this->view->translate('Certificate Number Setup');

        $programList = $this->model->getProgramSeqNumber();
        $this->view->programList = $programList;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
                //dd($formData); exit;

            if (isset($formData['csn_code']) && count($formData['csn_code']) > 0) {
                foreach ($formData['csn_code'] as $key => $value) {
                    $checkSeq = $this->model->checkSeq($key);

                    if ($checkSeq['csn_id'] != null){
                        $data = array(
                            'csn_code' => (($value == '' || $value == null) ? $checkSeq['ProgramCode']:$value),
                            'csn_seq_no' => (($formData['csn_seq_no'][$key] == '' || $formData['csn_code'][$key] == null || $formData['csn_seq_no'][$key] <= 0) ? 1:$formData['csn_seq_no'][$key]),
                        );

                        $this->model->updateSequence($data, $checkSeq['csn_id']);
                    }else{
                        $data = array(
                            'csn_code' => (($value == '' || $value == null) ? $checkSeq['ProgramCode']:$value),
                            'csn_program_id' => $key,
                            'csn_seq_no' => (($formData['csn_seq_no'][$key] == '' || $formData['csn_seq_no'][$key] == null || $formData['csn_code'][$key] <= 0) ? 1:$formData['csn_seq_no'][$key]),
                        );

                        $this->model->insertSequence($data);
                    }
                }
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Certificate sequence number saved.'));
            $this->_redirect($this->baseUrl . '/graduation/certificate/running-number/');
        }
    }

    public function downloadListAction(){
        $this->_helper->layout->disableLayout();

        //exit;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->studentList($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_certificate_list.xls';
    }
}