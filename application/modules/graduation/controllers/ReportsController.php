<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 21/9/2015
 * Time: 9:09 AM
 */
class Graduation_ReportsController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Graduation_Model_DbTable_Reports();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Attendance');
        $form = new Graduation_Form_ReportsSearchForm();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);
            $this->view->list = $list;
            $form->populate($formData);
            //var_dump($list);
        }
    }

    public function attendanceDownloadAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_graduation_attendance_list.xls';
    }

    public function guestReportAction(){
        $this->view->title = $this->view->translate('Additional Guest & Refreshment');
        $form = new Graduation_Form_ReportsSearchForm();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);

            if ($list){
                foreach ($list as $key => $value){
                    $guestinfo = $this->model->guestrefreshmentReport($value['studentid']);

                    if ($guestinfo){
                        $list[$key]['guestinfo']=$guestinfo;
                    }

                    $refreshmentinfo = $this->model->guestrefreshmentReport($value['studentid'], 2);

                    if ($refreshmentinfo){
                        $list[$key]['refreshmentinfo']=$refreshmentinfo;
                    }
                }
            }
            $this->view->list = $list;
            $form->populate($formData);
        }
    }

    public function guestDownloadAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);

            if ($list){
                foreach ($list as $key => $value){
                    $guestinfo = $this->model->guestrefreshmentReport($value['studentid']);

                    if ($guestinfo){
                        $list[$key]['guestinfo']=$guestinfo;
                    }

                    $refreshmentinfo = $this->model->guestrefreshmentReport($value['studentid'], 2);

                    if ($refreshmentinfo){
                        $list[$key]['refreshmentinfo']=$refreshmentinfo;
                    }
                }
            }
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_additional_guest_&_refreshment.xls';
    }

    public function collectionReportAction(){
        $this->view->title = $this->view->translate('Scroll, transcript and Convocation Photo Collection');
        $form = new Graduation_Form_ReportsSearchForm();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);
            $this->view->list = $list;
            $form->populate($formData);
        }
    }

    public function collectionDownloadAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_graduation_collection_report.xls';
    }

    public function summaryAction(){
        $this->view->title = $this->view->translate('Consolidated Report');
        $form = new Graduation_Form_ReportsSearchForm();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['convosession'] = $this->view->escape(strip_tags($formData['convosession']));
            $formData['program'] = $this->view->escape(strip_tags($formData['program']));
            $formData['studentid'] = $this->view->escape(strip_tags($formData['studentid']));
            $formData['studentname'] = $this->view->escape(strip_tags($formData['studentname']));

            $list = $this->model->attendanceReport($formData);

            if ($list){
                foreach ($list as $key => $value){
                    $guestinfo = $this->model->guestrefreshmentReport($value['studentid']);

                    if ($guestinfo){
                        $list[$key]['guestinfo']=$guestinfo;
                    }

                    $refreshmentinfo = $this->model->guestrefreshmentReport($value['studentid'], 2);

                    if ($refreshmentinfo){
                        $list[$key]['refreshmentinfo']=$refreshmentinfo;
                    }
                }
            }
            $this->view->list = $list;
            $form->populate($formData);
        }
    }

    public function summaryDownloadAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->attendanceReport($formData);

            if ($list){
                foreach ($list as $key => $value){
                    $guestinfo = $this->model->guestrefreshmentReport($value['studentid']);

                    if ($guestinfo){
                        $list[$key]['guestinfo']=$guestinfo;
                    }

                    $refreshmentinfo = $this->model->guestrefreshmentReport($value['studentid'], 2);

                    if ($refreshmentinfo){
                        $list[$key]['refreshmentinfo']=$refreshmentinfo;
                    }
                }
            }
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_Consolidated_Report.xls';
    }

    public function clearenceAction(){
        //$session = Zend_Registry::get('sis');
        $this->view->title = $this->view->translate('Summary of the clearance');
        $form = new Graduation_Form_SearchSummary();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);

            $pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
            $students = $pregraduationListDb->getClearanceGraduate($formData);

            $clearanceDB = new Graduation_Model_DbTable_Clearance();
            foreach($students as $index=>$student){

                //get kmc
                $students[$index]['kmc'] = $clearanceDB->getKmcData($student['idStudentRegistration']);

                //get finance
                $students[$index]['fin'] = $clearanceDB->getFinData($student['idStudentRegistration']);

                //get ict
                $students[$index]['ict'] = $clearanceDB->getIctData($student['idStudentRegistration']);

                //get logistic
                $students[$index]['log'] = $clearanceDB->getLogisticData($student['idStudentRegistration']);
            }
            $this->view->student = $students;
        }
    }

    public function clearenceDownloadAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
            $students = $pregraduationListDb->getClearanceGraduate($formData);

            $clearanceDB = new Graduation_Model_DbTable_Clearance();
            foreach($students as $index=>$student){

                //get kmc
                $students[$index]['kmc'] = $clearanceDB->getKmcData($student['idStudentRegistration']);

                //get finance
                $students[$index]['fin'] = $clearanceDB->getFinData($student['idStudentRegistration']);

                //get ict
                $students[$index]['ict'] = $clearanceDB->getIctData($student['idStudentRegistration']);

                //get logistic
                $students[$index]['log'] = $clearanceDB->getLogisticData($student['idStudentRegistration']);
            }
            $this->view->student = $students;
        }

        $this->view->filename = date('Ymd').'_summary_of_the_clearance.xls';
    }

    public function awardReportAction(){
        $this->view->title = $this->view->translate('Award Report');

        $model = new Graduation_Model_DbTable_AwardReport();
        $form = new Graduation_Form_ReportsSearchForm();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $model->getAwardListing($formData);

            $this->view->list = $list;
            $form->populate($formData);
        }
    }

    public function awardReportDownloadAction(){
        $this->_helper->layout->disableLayout();

        $model = new Graduation_Model_DbTable_AwardReport();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $model->getAwardListing($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_award_report.xls';
    }
}