<?php
class Graduation_AttestationController extends Base_Base {

	public function indexAction() {
		
		$this->view->title= $this->view->translate("Attestation Application List");
							
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();					
		
		$form = new Graduation_Form_SearchAttestationApplication();
				
		if($this->getRequest()->isPost() && $this->_request->getPost ('search')) {

			$formData = $this->getRequest()->getPost();
			
			if($form->isValid($formData)){
				
				$form->populate($formData);
				
				$this->view->status = $formData['status'];
				
				$AttestationDB = new Graduation_Model_DbTable_Attestation();
				$student_list = $AttestationDB->getApplication($formData);
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage(100);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
						
				$this->view->student = $paginator;	
								
			}			
			
		}
		
		$this->view->form = $form;
		
		
		if($this->getRequest()->isPost() && $this->_request->getPost ('submit')) {
			
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
									
			$AttestationDB = new Graduation_Model_DbTable_Attestation();
			
			for($x=0; $x < count($formData['id']); $x++){
				
				$id=$formData['id'][$x];
				
				if($formData['submit']=='Approve'){
					$status = 'APPROVED';
				}
				
				if($formData['submit']=='Reject'){
					$status = 'APPROVED';
				}				
					
				$data['status'] = $status;
				$data['approval_date'] = date('Y-m-d H:i:s');
				$data['approval_by'] = $auth->getIdentity()->id;
				
				$AttestationDB->updateData($data,'id='.(int)$id);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been update'));
			//$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'guest', 'action'=>'index'),'default',true));
		}
				
	}
	
}