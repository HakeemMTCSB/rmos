<?php
class Graduation_AwardController extends Base_Base {

	

	public function init(){
		$this->Session = new Zend_Session_Namespace('sis');
	}
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate('Award Setup');
		
		$Award = new Graduation_Model_DbTable_Award();
		$select = $Award->select()->order('status')->order('name');
		$this->view->awards = $Award->fetchAll($select);
		
	}

	public function addAction() {
		
		$this->_helper->layout->disableLayout();
		
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();
			
		
		if($this->getRequest()->isPost()) {
			
			$Award = new Graduation_Model_DbTable_Award();
			
			$formData = $this->getRequest()->getPost();
			
			
			//check if duplicate code
			$exist = $Award->checkDuplicate($formData['code']);
			
			if($exist){
				$this->_helper->flashMessenger->addMessage(array('error' => 'Award Code already exist'));
			}else{
				
				$post_data['name']=$formData['name'];
				$post_data['code']=$formData['code'];
				$post_data['description']=$formData['description'];
				$post_data['status']=1;
				$post_data['createddt']= date('Y-m-d H:i:s');
				$post_data['createdby']= $auth->getIdentity()->id;
				$post_data['modifydt']= date('Y-m-d H:i:s');
				$post_data['modifyby']= $auth->getIdentity()->id;
				
				$idAward = $db->insert('tbl_graduation_award',$post_data);
				
				for($x=0; $x<count($formData['IdProgram']); $x++){
					
					$tag_data['gap_award_id']= $idAward;
					$tag_data['gap_program_id']= $formData['IdProgram'][$x];
					$tag_data['gap_createddt']= date('Y-m-d H:i:s');
					$tag_data['gap_createdby']= $auth->getIdentity()->id;
				
					$db->insert('tbl_graduation_award_program',$tag_data);
				}
				
				$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			}			
			
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'award', 'action'=>'index'),'default',true));
		}
	}
	
	public function editAction() {
		
		$this->_helper->layout->disableLayout();
		
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$id = $this->_getParam('id',null);
		
		$Award = new Graduation_Model_DbTable_Award();
		$this->view->award = $Award->fetchRow('id = ' . $id);
		
		$award_program = $Award->getAwardProgram($id);		
		$award_program_tagging = array();
		if(count($award_program)>0){
			foreach($award_program as $program){
				array_push($award_program_tagging,$program['gap_program_id']);
			}
		}
		$this->view->award_program=$award_program_tagging;
		
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->program_list = $programDb->getData();
		
		
		if($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
						
			$post_data['name']=$formData['name'];
			$post_data['code']=$formData['code'];
			$post_data['description']=$formData['description'];
			$post_data['status']=1;
			$post_data['modifydt']= date('Y-m-d H:i:s');
			$post_data['modifyby']= $auth->getIdentity()->id;
			
			$where = 'id='.(int)$formData['id'];
			$idAward = $db->update('tbl_graduation_award',$post_data,$where);
			
			//delete award tagging program
			$where_del = 'gap_award_id ='.(int)$formData['id'];
			$db->delete('tbl_graduation_award_program',$where_del);
			
			for($x=0; $x<count($formData['IdProgram']); $x++){
				
				$tag_data['gap_award_id']= $formData['id'];
				$tag_data['gap_program_id']= $formData['IdProgram'][$x];
				$tag_data['gap_createddt']= date('Y-m-d H:i:s');
				$tag_data['gap_createdby']= $auth->getIdentity()->id;
			
				$db->insert('tbl_graduation_award_program',$tag_data);
			}

			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'award', 'action'=>'index'),'default',true));
		}

		
	}

	


	public function deleteAction() {
		$Award = new Graduation_Model_DbTable_Award();
		$id = $this->_getParam('id',null);

		$Award->remove($id);

		$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been deleted'));
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'award', 'action'=>'index'),'default',true));

	}
	
	public function changeStatusAction() {
		$Award = new Graduation_Model_DbTable_Award();
		$id = $this->_getParam('id',null);
		$Award->updateData(array('status'=>0),$id);

		$this->_helper->flashMessenger->addMessage(array('success' => 'Status has been changed'));
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'award', 'action'=>'index'),'default',true));

	}

}