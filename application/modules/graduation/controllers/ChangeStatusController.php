<?php
class Graduation_ChangeStatusController extends Base_Base {

	
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate('Change Status - Graduated');
		
		$form = new Graduation_Form_SearchChangeStatus();
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			$form->populate($formData);
			
			$formData['profileStatusArray'] = array(248,96); //to get student with profile status completed saja
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			$this->view->student = $pregraduationListDb->getApprovedGraduate($formData);
			
			
		}
		$this->view->form = $form;
	}
	
	public function approveAction() {
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$auth = Zend_Auth::getInstance();
			
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			$semesterDB = new Registration_Model_DbTable_Semester();
			$systemErrorDB = new App_Model_General_DbTable_SystemError();
			
			
			
			for($i=0; $i<count($formData['idStudentRegistration']); $i++){
				
				$IdStudentRegistration = $formData['idStudentRegistration'][$i];	
				
				//get student pregraduate info
				$graduate_info = $pregraduationListDb->getInfo($IdStudentRegistration);				
				
				$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$graduate_info['IdScheme'],'branch_id'=>$graduate_info['IdBranch']));

				
				try {
					
					$db->beginTransaction();
					
					//update status = 1 & convo session
					$pregraduationListDb->updateStatus(array('status'=>2,'graduate_date'=>date('Y-m-d H:i:s'),'graduate_by'=>$auth->getIdentity()->id),$IdStudentRegistration);
										
					//update profile status
					$db->update('tbl_studentregistration',array('profileStatus'=>96),'IdStudentRegistration = "'.$IdStudentRegistration.'"'); //Completed Deftype=20

				
					//  ---------------- update studentsemesterstatus table ----------------	
					$this->updateStudentSemesterStatus($IdStudentRegistration,$semester['IdSemesterMaster'],848); //Graduated
					//  ---------------- end update studentsemesterstatus table ----------------	
					
					$db->commit();
					
				}catch (Exception $e) {
																						
						//echo $e->getMessage();
						$db->rollBack();	
						
						$stack_student['status'] = 'Fail';
						$stack_student['function'] = 'Save Data';
						$stack_student['error_message'] = $e->getMessage();
						
						$error['se_txn_id']=0;
						$error['se_IdStudentRegistration']=$IdStudentRegistration;
						$error['se_IdStudentRegSubjects']=0;
						$error['se_title']='Student Graduation : Change Status From Completed to Graduated';
						$error['se_message']=$e->getMessage();
						$error['se_createddt']=date("Y-m-d H:i:s");
						$error['se_createdby']=$auth->getIdentity()->id;
						$systemErrorDB->addData($error);
				}								
						
			}
	
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been updated'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'change-status', 'action'=>'index'),'default',true));
		}
	}
	
	public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
    	
    	//check current status
    	$semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
    	$semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);
    	
    	if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
    		//nothing to update
    	}else{
    		//add new status & keep old status into history table
    		$cms = new Cms_Status();
    		$auth = Zend_Auth::getInstance();
    			      			        	
			$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
				            'idSemester' => $IdSemesterMain,
				            'IdSemesterMain' => $IdSemesterMain,								
				            'studentsemesterstatus' => $newstatus, 	
	                        'Level'=>0,
				            'UpdDate' => date ( 'Y-m-d H:i:s'),
				            'UpdUser' => $auth->getIdentity()->iduser
	        );				
					
    		$message = 'Graduation upon approval';
    		$cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,null);
    	}
    	
    }
	
}

?>
