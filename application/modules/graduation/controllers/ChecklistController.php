<?php
class Graduation_ChecklistController extends Base_Base {

	

	public function init(){
		$this->Session = new Zend_Session_Namespace('sis');
	}
	
	public function indexAction() {
		
		$this->view->title= $this->view->translate("Record Verification Setup - Program List");
		
		$c_id = $this->_getParam('c_id',null);
		$this->view->c_id = $c_id;
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$this->view->convo = $convocationDb->getData($c_id);
		
		$convoChecklistDb = new Graduation_Model_DbTable_ConvocationChecklist();
		$program = $convoChecklistDb->getProgramByConvo($c_id);
		
		//get checklist
		foreach($program as $key=>$prog){
			$checklist = $convoChecklistDb->getChecklistByConvoProgram($prog['cc_id']);
			$program[$key]['checklist']=$checklist;
		}
		
		$this->view->program = $program;
		
		
	}

	public function addAction() {
		
		$this->view->title= $this->view->translate("Record Verification Setup");
		
		$c_id = $this->_getParam('c_id',null);
		$this->view->c_id = $c_id;
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$this->view->convo = $convocationDb->getData($c_id);
		
		$Program = new GeneralSetup_Model_DbTable_Program();
		$this->view->program_list = $Program->ProgramChecklist($c_id);
		
		//$definationDb = new App_Model_General_DbTable_Definationms();
		$checklistDB = new Graduation_Model_DbTable_Checklist();
		$this->view->checklist = $checklistDB->getActiveChecklist();
		
		
		if($this->getRequest()->isPost()) {
			
			
			$convoChecklistDb = new Graduation_Model_DbTable_ConvocationChecklist();
			
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
			
			unset($formData['submit']);
						
			foreach($formData['idProgram'] as $program){
				
				$data['c_id'] = $formData['c_id'];
				$data['idProgram'] = $program;
				$data['cc_createddt'] = date('Y-m-d H:i:s');
				$data['cc_createdby'] = $auth->getIdentity()->id;
				
				$cc_id = $convoChecklistDb->addData($data);
				
				foreach($formData['idChecklist'] as $checklist){
					$check['idChecklist']=$checklist;
					$check['cc_id'] = $cc_id;
					$check['c_id'] = $formData['c_id'];
					
					$convoChecklistDb->addChecklistData($check);
				}
				
				
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'checklist', 'action'=>'index','c_id'=>$formData['c_id']),'default',true));
		}

		
		
	}
	
	public function editAction() {
		
		$this->view->title= $this->view->translate("Record Verification Setup - Manage Checklist");
		
		$c_id = $this->_getParam('c_id',null);
		$this->view->c_id = $c_id;
		
		$cc_id = $this->_getParam('cc_id',null);
		$this->view->cc_id = $cc_id;
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$this->view->convo = $convocationDb->getData($c_id);		

		$convoChecklistDb = new Graduation_Model_DbTable_ConvocationChecklist();
		$program = $convoChecklistDb->getDataChecklist($cc_id);
		$this->view->program = $program;
		
		$array_checklist = array();
		foreach($program as $checklist){
			array_push($array_checklist,$checklist['idChecklist']);
		}
		$this->view->array_checklist = $array_checklist;
		
		
		//$definationDb = new App_Model_General_DbTable_Definationms();
		//$this->view->checklist = $definationDb->getDataByType(176);
		$checklistDB = new Graduation_Model_DbTable_Checklist();
		$this->view->checklist = $checklistDB->getActiveChecklist();

		if($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//delete dulu
			$where = 'cc_id = '.(int)$formData['cc_id'];
			$convoChecklistDb->deleteChecklist($where);
			
			//add bariu
			foreach($formData['idChecklist'] as $checklist){
				
					$check['idChecklist']=$checklist;
					$check['cc_id'] = $formData['cc_id'];
					$check['c_id'] = $formData['c_id'];
					
					$convoChecklistDb->addChecklistData($check);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'checklist', 'action'=>'index','c_id'=>$formData['c_id'],'cc_id'=>$formData['cc_id']),'default',true));
		}

	}

	


	public function deleteAction() {
		
		$Checklist = new Graduation_Model_DbTable_Checklist();
		
		$id = $this->_getParam('id',null);

		$Checklist->remove($id);

		//get document
		$documentDB = new Graduation_Model_DbTable_GeneralDocuments();
		$documents = $documentDB->getmyDocuments('tbl_graduation_checklist',$id);
			
		if(count($documents)>0){
			foreach($documents as $doc){
			
				if(file_exists(DOCUMENT_PATH.$doc['gd_filepath'])){
								
				//delete document info dtbase  	
	  			$documentDB->deleteData('gd_id='.(int)$doc['gd_id']);	
	  			
			  	unlink(DOCUMENT_PATH.$doc['gd_filepath']);
				}
			}
		}
	  	 
		
		$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been deleted'));
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'checklist', 'action'=>'setup-index'),'default',true));


	}
	
	public function setupIndexAction(){
		
		$this->view->title = $this->view->translate('Record Verification Setup');
		
		$Checklist = new Graduation_Model_DbTable_Checklist();
		$select = $Checklist->select()->order('name');
		$this->view->checklist = $Checklist->fetchAll($select);
	}
	
	
	public function addSetupAction() {
		
		//$this->_helper->layout->disableLayout();
		$this->view->title= $this->view->translate("Record Verification Setup - Add");
		
		$auth = Zend_Auth::getInstance();
		
		if($this->getRequest()->isPost()) {
			
			$ChecklistDB = new Graduation_Model_DbTable_Checklist();
			
			$post_data = $this->getRequest()->getPost();
						
			if(count($_FILES['attachment'])>0){
				$post_data['document']=1;
			}
			
			if(isset($post_data['upload'])){
				$post_data['upload']=1;
			}else{
				$post_data['upload']=0;
			}
			
			$post_data['status']=1;
			$post_data['createddt']= date('Y-m-d H:i:s');
			$post_data['createdby']= $auth->getIdentity()->id;
			
			$id = $ChecklistDB->save($post_data);
			
			$this->multipleUpload($_FILES,'tbl_graduation_checklist',$id);
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));	
			
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'checklist', 'action'=>'setup-index'),'default',true));
		}
	}
	
	public function editSetupAction() {
		
		//$this->_helper->layout->disableLayout();
		$this->view->title= $this->view->translate("Record Verification Setup - Edit");
		
		$auth = Zend_Auth::getInstance();
				
		$id = $this->_getParam('id',null);
		$this->view->id = $id;

		$ChecklistDB = new Graduation_Model_DbTable_Checklist();
		
		if($this->getRequest()->isPost()) {
			
			$post_data = $this->getRequest()->getPost();

			if(count($_FILES['attachment'])>0){
				$data['document']=1;
			}
			
			if(isset($post_data['upload'])){
				$data['upload']=1;
			}else{
				$data['upload']=0;
			}
			
			$data['status']=$post_data['status'];
			$data['name']=$post_data['name'];
			$data['description']=$post_data['description'];
			
			$ChecklistDB->update($data,'id='.(int)$post_data['id']);
			
			$this->multipleUpload($_FILES,'tbl_graduation_checklist',$post_data['id']);

			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'checklist', 'action'=>'edit-setup','id'=>$post_data['id']),'default',true));
		}else{
			
			$this->view->checklist = $ChecklistDB->fetchRow('id = ' . $id );
					
			//get document
			$documentDB = new Graduation_Model_DbTable_GeneralDocuments();
			$this->view->documents = $documentDB->getmyDocuments('tbl_graduation_checklist',$id);
		
		}

		 
	}
	
	
	function multipleUpload($files_upl,$table_name,$table_id){
				
		   $auth = Zend_Auth::getInstance(); 
			
		   $uploadfileDB = new Graduation_Model_DbTable_GeneralDocuments();
		   		   
		   $this->createDir();
		   
           //foreach file name
		   foreach ($files_upl as $name => $attributes) {
		   	
 				for ($i=0; $i<count($files_upl[$name]['name']); $i++){
				 		 									 		
				 		if(isset($name) && $name!=''){
				 			
				 			if (is_uploaded_file($files_upl[$name]['tmp_name'][$i])){
		        						       			
					        					
			        				$filetype = $files_upl[$name]["type"][$i];
									$ori_filename = strtolower($files_upl[$name]["name"][$i]);
									$rename_filename = date('Ymdhs')."_".$ori_filename;							
									$path_file = '/download/'.date('Ym').'/'.$rename_filename;						
									move_uploaded_file($files_upl[$name]['tmp_name'][$i], DOCUMENT_PATH.$path_file);
									
									$upload_file = array(		
										'gd_table_name' => $table_name,
										'gd_table_id' => $table_id,								
										'gd_filename' => $rename_filename, 
                                        'gd_ori_filename' => $ori_filename, 
										'gd_type' => 939, //Graduation file
									    'gd_filepath' => $path_file, 
										'gd_createddt' => date("Y-m-d h:i:s"),
										'gd_createdby' => $auth->getIdentity()->iduser,
										'gd_filetype' => $filetype, 
									);
									
									$uploadfileDB->addData($upload_file);	
										
			        			
							
		    	        	}//end if is_uploaded
				 		}
				  } 							
			}
				      
	}
	
	function createDir(){
        	
		$general_path = DOCUMENT_PATH.'/download/'.date('Ym');
		
	    //create directory to locate fisle			
		if (!is_dir($general_path)) {
	    	mkdir($general_path, 0775);
		}    	

  }
  
	
	
	function delDocAction($doc){
         
		$id = $this->_getParam('id',null);
		$gdid = $this->_getParam('gdid',null);
				
		if(file_exists(DOCUMENT_PATH.$doc['gd_filepath'])){
		  	unlink(DOCUMENT_PATH.$doc['gd_filepath']);
		}
		
  		//delete document info dtbase
  		$uploadfileDB = new Graduation_Model_DbTable_GeneralDocuments();
  		$uploadfileDB->deleteData('gd_id='.(int)$gdid);	 
  		
  		$mydocs = $uploadfileDB->getmyDocuments('tbl_graduation_checklist',$id);
  		if(count($mydocs)>0){
  			$document = 1;
  		}else{
  			$document = 0;
  		}
  		$ChecklistDB = new Graduation_Model_DbTable_Checklist();
  		$data['document']=$document;
  		$ChecklistDB->update($data,'id='.(int)$id);
  		
  		$this->_helper->flashMessenger->addMessage(array('success' => 'Document has been deleted'));
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'checklist', 'action'=>'edit-setup','id'=>$id),'default',true));
	}	

}