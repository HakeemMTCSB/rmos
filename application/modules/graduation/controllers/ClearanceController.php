<?php
class Graduation_ClearanceController extends Base_Base {

	private $_sis_session;

	public function init(){
		$this->_sis_session = new Zend_Session_Namespace('sis');
		$this->auth = Zend_Auth::getInstance();
		$this->registry = Zend_Registry::getInstance();
		$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
	}
	
	public function summaryAction() {
		
		if($this->auth->getIdentity()->IdRole == 943){ //KMC
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'kmc'),'default',true));
		}else
		if($this->auth->getIdentity()->IdRole == 876){ //Finance
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'fin'),'default',true));
		}else
		if($this->auth->getIdentity()->IdRole == 944){ //ICT
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'ict'),'default',true));
		}else
		if($this->auth->getIdentity()->idRole == 945){ //LOGISTIC
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'logistic'),'default',true));
		}else
		if($this->auth->getIdentity()->IdRole == 455){ //ASAD
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'index'),'default',true));
		}else
		if($this->auth->getIdentity()->IdRole == 1){ //ADMIN
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'index'),'default',true));
		}else{
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'index', 'action'=>'index'),'default',true));
		}
		exit;
	}
	
	public function indexAction() {
		
		if($this->auth->getIdentity()->IdRole != 1  && $this->auth->getIdentity()->IdRole != 455){ //ASAD
		
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'summary'),'default',true));
		}
		
		//title
		$this->view->title= $this->view->translate("Clearance");
		
		$form = new Graduation_Form_SearchSummary();
		
		$session = Zend_Registry::get('sis');
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result_kmc_sum);
    	}
    	
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$session->result_kmc_sum = $formData;
			
			$form->populate($formData);
			
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			$students = $pregraduationListDb->getClearanceGraduate($formData);	
			
			$clearanceDB = new Graduation_Model_DbTable_Clearance();
			foreach($students as $index=>$student){
				
				//get kmc				
				$students[$index]['kmc'] = $clearanceDB->getKmcData($student['idStudentRegistration']);
				
				//get finance
				$students[$index]['fin'] = $clearanceDB->getFinData($student['idStudentRegistration']);
				
				//get ict
				$students[$index]['ict'] = $clearanceDB->getIctData($student['idStudentRegistration']);
				
				//get logistic
				$students[$index]['log'] = $clearanceDB->getLogisticData($student['idStudentRegistration']);
			}		
			$this->view->student = $students;	
			
		}else{
			
			//populate by session 
	    	if (isset($session->result_kmc_sum)) {    	
	    		$form->populate($session->result_kmc_sum);
	    		$formData = $session->result_kmc_sum;			
	    		
	    		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
				$students = $pregraduationListDb->getClearanceGraduate($formData);	
				
				$clearanceDB = new Graduation_Model_DbTable_Clearance();
				foreach($students as $index=>$student){
					
					//get kmc				
					$students[$index]['kmc'] = $clearanceDB->getKmcData($student['idStudentRegistration']);
					
					//get finance
					$students[$index]['fin'] = $clearanceDB->getFinData($student['idStudentRegistration']);
					
					//get ict
					$students[$index]['ict'] = $clearanceDB->getIctData($student['idStudentRegistration']);
					
					//get logistic
					$students[$index]['log'] = $clearanceDB->getLogisticData($student['idStudentRegistration']);
				}		
				$this->view->student = $students;
	    	}
	    	
		}
		$this->view->form = $form;
	}
	
	public function saveSummaryAction(){
		
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			for($i=0; $i<count($formData['IdStudentRegistration']); $i++){
				
				$IdStudentRegistration = $formData['IdStudentRegistration'][$i];
				
				$data['dcs_status']=1;
				$data['approval_remarks']=$formData['approval_remarks'];
				$data['IdStudentRegistration'] = $IdStudentRegistration;
				$data['dcs_createddt'] = date('Y-m-d H:i:s');
				$data['dcs_createdby'] = $auth->getIdentity()->id;
				
				$db->insert('department_clearance_summary',$data);
			}
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'index'),'default',true));
		}
	}
	
	public function kmcAction()
	{
		$this->view->auth = $this->auth; 
		if($this->auth->getIdentity()->IdRole != 1  && $this->auth->getIdentity()->IdRole != 455 && $this->auth->getIdentity()->IdRole != 943){ //ASAD
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'summary'),'default',true));
		}
		
		$this->view->title= $this->view->translate("Clearance - KMC");
		
		$session = Zend_Registry::get('sis');
		
		$form = new Graduation_Form_SearchApprovedGraduate();
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result_kmc);
    	}
    	
    	$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
    	
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$session->result_kmc = $formData;
			
			$form->populate($formData);			
			
			$this->view->student = $pregraduationListDb->getClearanceGraduate($formData,'kmc');			
			
		}else{
			
			//populate by session 
	    	if (isset($session->result_kmc)) {    	
	    		$form->populate($session->result_kmc);
	    		$formData = $session->result_kmc;			
	    		$this->view->student = $pregraduationListDb->getClearanceGraduate($formData,'kmc');	
	    	}
			
		}
		$this->view->form = $form;
	}

	public function kmcEditAction()
	{
		$this->view->title= $this->view->translate("Clearance - KMC Review");
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$idStudentRegistration = $this->_getParam("id", null);
		
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		$this->view->student = $pregraduationListDb->getGraduateInfo($idStudentRegistration);
		
		$clearanceDB = new Graduation_Model_DbTable_Clearance();
		$kmc = $clearanceDB->getKmcData($idStudentRegistration);
		$this->view->kmc = $kmc;
			
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();

			$data['IdStudentRegistration']=$formData['idStudentRegistration'];
			$data['kmc_return']=$formData['pending_return'];
			$data['kmc_penalty']=$formData['penalty_fee'];
			$data['kmc_paiddt']= (isset($formData['paid_on']) && $formData['paid_on']!='') ? date('Y-m-d',strtotime($formData['paid_on'])):'';
			$data['kmc_status']=$formData['status'];
			$data['kmc_remarks']=$formData['kmc_remarks'];
						
			$clearanceDB =  new Graduation_Model_DbTable_Clearance();
			
			if(isset($formData['kmc_id']) && $formData['kmc_id']!=''){
				$db->update('department_clearance_kmc',$data,$where="kmc_id = ".(int)$formData['kmc_id']);
			}else{
				$data['kmc_createddt']= date('Y-m-d H:i:s');
				$data['kmc_createdby']= $auth->getIdentity()->id;
				$db->insert('department_clearance_kmc',$data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'kmc-edit','id'=>$formData['idStudentRegistration']),'default',true));
		}
	}

	public function finAction()
	{
		$this->view->auth = $this->auth; 
		
		
		if($this->auth->getIdentity()->IdRole != 1  && $this->auth->getIdentity()->IdRole != 455 && $this->auth->getIdentity()->IdRole != 876){ //Finance
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'fin'),'default',true));
		}
		
		$this->view->title= $this->view->translate("Clearance - Finance");
		
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		
		$form = new Graduation_Form_SearchApprovedGraduate();
		
		$session = Zend_Registry::get('sis');
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result_fin);
    	}
    	
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$session->result_fin = $formData;
			
			$form->populate($formData);			
			
			$student_list = $pregraduationListDb->getClearanceGraduate($formData,'fin');			
		}else{
			
			//populate by session 
	    	if (isset($session->result_fin)) {    	
	    		$form->populate($session->result_fin);
	    		$formData = $session->result_fin;			
	    		$student_list = $pregraduationListDb->getClearanceGraduate($formData,'fin');	
	    	}
	    	
		}
		
		if(isset($student_list) && count($student_list)>0){
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
			foreach($student_list as $index=>$student){				
				//$student_list[$index]['os'] = $invoiceClass->checkOutstandingSoa($student['idStudentRegistration'],2); //IdStudentRegistration, 1=applicant, 2=student
				$balance = $invoiceClass->balance($student['idStudentRegistration'], 2, '', ''); //IdStudentRegistration, 1=applicant, 2=student
				$student_list[$index]['os'] = isset($balance['balance']) && $balance['balance'] != '' ? number_format(floatval($balance['balance']), 2, '.', ''):number_format(floatval(0.00), 2, '.', '');
			}
			$this->view->student = $student_list;
		}
		
		$this->view->form = $form;
	}

	public function finEditAction()
	{
		$this->view->title= $this->view->translate("Clearance - Finance Review");
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$idStudentRegistration = $this->_getParam("id", null);
	
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		$this->view->student = $pregraduationListDb->getGraduateInfo($idStudentRegistration);
	
		$clearanceDB = new Graduation_Model_DbTable_Clearance();
		$this->view->fin = $clearanceDB->getFinData($idStudentRegistration);
		$this->view->kmc = $clearanceDB->getKmcData($idStudentRegistration);

		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();

			$data['IdStudentRegistration']=$formData['idStudentRegistration'];
			$data['dcf_security_deposit']=$formData['dcf_security_deposit'];
			$data['dcf_bond']=$formData['dcf_bond'];
			$data['dcf_fee']=$formData['dcf_fee'];
			$data['dcf_paiddt']= (isset($formData['dcf_paiddt']) && $formData['dcf_paiddt']!='') ? date('Y-m-d',strtotime($formData['dcf_paiddt'])):'';
			$data['dcf_status']=$formData['dcf_status'];
			$data['dcf_remarks']=$formData['dcf_remarks'];
						
			$clearanceDB =  new Graduation_Model_DbTable_Clearance();
			
			if(isset($formData['dcf_id']) && $formData['dcf_id']!=''){
				$db->update('department_clearance_finance',$data,$where="dcf_id = ".(int)$formData['dcf_id']);
			}else{
				$data['dcf_createddt']= date('Y-m-d H:i:s');
				$data['dcf_createdby']= $auth->getIdentity()->id;
				$db->insert('department_clearance_finance',$data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'fin-edit','id'=>$formData['idStudentRegistration']),'default',true));
		}
	}

	public function ictAction()
	{
		$this->view->auth = $this->auth; 
		
		if($this->auth->getIdentity()->IdRole != 1  && $this->auth->getIdentity()->IdRole != 455 && $this->auth->getIdentity()->IdRole != 944){ //ICT
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'ict'),'default',true));
		}
		
		$this->view->title= $this->view->translate("Clearance - ICT");
		
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		
		$form = new Graduation_Form_SearchApprovedGraduate();
		
		$session = Zend_Registry::get('sis');
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result_ict);
    	}
    	
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$session->result_ict = $formData;
			
			$form->populate($formData);
			
			$this->view->student = $pregraduationListDb->getClearanceGraduate($formData,'ict');			
		}else{
			
			//populate by session 
	    	if (isset($session->result_ict)) {    	
	    		$form->populate($session->result_ict);
	    		$formData = $session->result_ict;			
	    		$this->view->student = $pregraduationListDb->getClearanceGraduate($formData,'ict');	
	    	}
	    	
		}
		$this->view->form = $form;
	}

	public function ictEditAction()
	{
		$this->view->title= $this->view->translate("Clearance - ICT Review");
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$idStudentRegistration = $this->_getParam("id", null);
	
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		$this->view->student = $pregraduationListDb->getGraduateInfo($idStudentRegistration);
	
		$clearanceDB = new Graduation_Model_DbTable_Clearance();
		$this->view->ict = $clearanceDB->getIctData($idStudentRegistration);
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();

			
			if(isset($formData['ict_removeemail']) && $formData['ict_removeemail']!=''){
				$data['ict_removeemail']=1;	
			}else{
				$data['ict_removeemail']=0;	
			}	
				
			$data['IdStudentRegistration']=$formData['idStudentRegistration'];
			$data['ict_status']=$formData['ict_status'];
			$data['ict_remarks']=$formData['ict_remarks'];
						
			$clearanceDB =  new Graduation_Model_DbTable_Clearance();
			
			if(isset($formData['ict_id']) && $formData['ict_id']!=''){
				$db->update('department_clearance_ict',$data,$where="ict_id = ".(int)$formData['ict_id']);
			}else{
				$data['ict_createddt']= date('Y-m-d H:i:s');
				$data['ict_createdby']= $auth->getIdentity()->id;
				$db->insert('department_clearance_ict',$data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'ict-edit','id'=>$formData['idStudentRegistration']),'default',true));
		}
	}
	
	public function logisticAction()
	{
		$this->view->auth = $this->auth; 
		
		if($this->auth->getIdentity()->IdRole != 1  && $this->auth->getIdentity()->IdRole != 455  &&  $this->auth->getIdentity()->IdRole != 945){ //LOGISTIC
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'logistic'),'default',true));
		}
		
		$this->view->title= $this->view->translate("Clearance - Logistic");
		
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		
		$form = new Graduation_Form_SearchApprovedGraduate();
		
		$session = Zend_Registry::get('sis');
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result_logistic);
    	}
    	
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$session->result_logistic = $formData;
			
			$form->populate($formData);
			
			$this->view->student = $pregraduationListDb->getClearanceGraduate($formData,'logistic');			
		}else{
			
			//populate by session 
	    	if (isset($session->result_logistic)) {    	
	    		$form->populate($session->result_logistic);
	    		$formData = $session->result_logistic;			
	    		$this->view->student = $pregraduationListDb->getClearanceGraduate($formData,'logistic');	
	    	}
	    	
		}
		$this->view->form = $form;
	}
	
	
	public function logisticEditAction()
	{
		$this->view->title= $this->view->translate("Clearance - Logistic Review");
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$idStudentRegistration = $this->_getParam("id", null);
	
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		$this->view->student = $pregraduationListDb->getGraduateInfo($idStudentRegistration);
	
		$clearanceDB = new Graduation_Model_DbTable_Clearance();
		$this->view->log = $clearanceDB->getLogisticData($idStudentRegistration);
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();

			
			if(isset($formData['log_blocked_building_access']) && $formData['log_blocked_building_access']!=''){
				$data['log_blocked_building_access']=1;	
			}else{
				$data['log_blocked_building_access']=0;	
			}	
				
			$data['IdStudentRegistration']=$formData['idStudentRegistration'];
			$data['log_status']=$formData['log_status'];
			$data['log_remarks']=$formData['log_remarks'];
						
			$clearanceDB =  new Graduation_Model_DbTable_Clearance();
			
			if(isset($formData['log_id']) && $formData['log_id']!=''){
				$db->update('department_clearance_logistic',$data,$where="log_id = ".(int)$formData['log_id']);
			}else{
				$data['log_createddt']= date('Y-m-d H:i:s');
				$data['log_createdby']= $auth->getIdentity()->id;
				$db->insert('department_clearance_logistic',$data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'clearance', 'action'=>'logistic-edit','id'=>$formData['idStudentRegistration']),'default',true));
		}
	}

	public function printAction(){
		//set unlimited
		set_time_limit(0);
		ini_set('memory_limit', '-1');

		$model = new Records_Model_DbTable_TerminateStudent();

		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if (isset($formData['IdStudentRegistration']) && count($formData['IdStudentRegistration']) > 0){

				$printcontent = '';

				$fieldValues = array(
					'$[LOGO]'=> "images/logo_text_high.jpg"
				);

				$html_template_path = DOCUMENT_PATH."/template/clearance_graduation.html";
				$output_filename = date('YmdHis')."-graduation-clearance.pdf";

				require_once 'dompdf_config.inc.php';
				error_reporting(0);

				$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
				$autoloader->pushAutoloader('DOMPDF_autoload');

				$count = 0;

				foreach ($formData['IdStudentRegistration'] as $id){
					$count++;

					global $appdetail;
					global $kmc;
					global $celFin;
					global $ict;
					global $logistic;

					$appdetail = $model->studentList2(null, $id);
					//var_dump($appdetail); exit;
					$kmc = $model->getKmc($id);
					$celFin = $model->getClearanceFinance($id);
					$ict = $model->getIct($id);
					$logistic = $model->getLogistic($id);

					$html2 = '
						<!-- Header -->
						<table class="" width="100%">
							<tr>
								<td width="" align="center">
									<img src="$[LOGO]" width="210" />
								</td>
							</tr>
						</table>
						<table class="" width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="center" colspan="3"><h2>STUDENT CLEARANCE FORM</h2></td>
							</tr>
							<tr>
								<td colspan="3">All students leaving INCEIF must obtain clearance and signature from the respective departments before transcripts, scroll or verification of official withdrawal can be issued.</td>
							</tr>
							<tr>
								<td colspan="3"><hr></td>
							</tr>
							<tr id="tr-title">
								<td  colspan="3"><h3>A. STUDENT DETAILS</h3></td>
							</tr>
							<tr>
								<td width="20%">Name</td>
								<td width="3%"> : </td>
								<td>'.$appdetail["appl_fname"].' '.$appdetail["appl_lname"].'</td>
							</tr>
							<tr>
								<td>Student ID </td>
								<td> : </td>
								<td>'.(($appdetail['registrationId']) ? $appdetail['registrationId'] : '').'</td>
							</tr>
							<tr>
								<td>Programme </td>
								<td> : </td>
								<td>'.(($appdetail['ProgramName']) ? $appdetail['ProgramName'] : '').'</td>
							</tr>
							<tr>
								<td>Reason For Clearance</td>
								<td> : </td>
								<td>Graduated</td>
							</tr>
						</table>
						<table class="" width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="2"><hr></td>
							</tr>
							<tr id="tr-title">
								<td colspan="2"><h3>B.	DIVISIONS CLEARANCE</h3></td>
							</tr>
							<tr id="tr-sub-title" >
								<td><strong>Knowledge Management Centre</strong></td>
								<td align="right">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<ul>
										<li>Pending to return : '.$kmc['kmc_return'].'</li>
										<li>Penalty RM : '.$kmc['kmc_penalty'].'</li>
										<li>Paid On : '.(isset($kmc['kmc_paiddt']) && $kmc['kmc_paiddt']!='1970-01-01' && $kmc['kmc_paiddt']!='0000-00-00' && $kmc['kmc_paiddt']!='' && $kmc['kmc_paiddt']!=null ? date('d-m-Y', strtotime($kmc['kmc_paiddt'])):'').'</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>Remarks : '.$kmc['kmc_remarks'].'</td>
								<td></td>
							</tr>
							<tr>
								<td>Status : '.($kmc['kmc_status']!=null ? ($kmc['kmc_status']==1 ? 'Approved':'Rejected'):'').'</td>
								<td></td>
							</tr>
							<tr>
								<td width="10%">Approved By : '.$kmc['name'].'</td>
								<td>Approved Date : '.($kmc['kmc_createddt']!='' && $kmc['kmc_createddt']!=null ? date('d-m-Y', strtotime($kmc['kmc_createddt'])):'').'</td>
							</tr>
							<tr>
								<td colspan="2"><hr></td>
							</tr>
							<tr id="tr-sub-title" >
								<td><strong>Finance Division</strong></td>
								<td align="right" width="">

								</td>
							</tr>
							<tr>
								<td colspan="2">
									Payment to be collected/refunded
									<ul>
										<li>Security Deposit : '.$celFin['dcf_security_deposit'].'</li>
										<li>Personal Bond : '.$celFin['dcf_bond'].'</li>
										<li>Tuition Fee/Accommodation : '.$celFin['dcf_fee'].'</li>
										<li>Total : '.($celFin['dcf_security_deposit']+$celFin['dcf_bond']+$celFin['dcf_fee']).'</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>Remarks : '.$celFin['dcf_remarks'].'</td>
								<td></td>
							</tr>
							<tr>
								<td>Status : '.($celFin['dcf_status']!=null ? ($celFin['dcf_status']==1 ? 'Approved':'Rejected'):'').'</td>
								<td></td>
							</tr>
							<tr>
								<td>Approved By : '.$celFin['name'].'</td>
								<td>Approved Date : '.($celFin['dcf_createddt']!='' && $celFin['dcf_createddt']!=null ? date('d-m-Y', strtotime($celFin['dcf_createddt'])):'').'</td>
							</tr>
							<tr>
								<td colspan="2"><hr></td>
							</tr>
							<tr id="tr-sub-title" >
								<td><strong>ICT Division</strong></td>
								<td align="right" width="10%">

								</td>
							</tr>
							<tr>
								<td colspan="2">
									<ul>
										<li>Removed email address from the group</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>Remarks : '.$ict['ict_remarks'].'</td>
								<td></td>
							</tr>
							<tr>
								<td>Status : '.($ict['ict_status']!=null ? ($ict['ict_status']==1 ? 'Approved':'Rejected'):'').'</td>
								<td></td>
							</tr>
							<tr>
								<td>Approved By : '.$ict['name'].'</td>
								<td>Approved Date : '.($ict['ict_createddt']!='' && $ict['ict_createddt']!=null ? date('d-m-Y', strtotime($ict['ict_createddt'])):'').'</td>
							</tr>
							<tr>
								<td colspan="2"><hr></hr></td>
							</tr>
							<tr id="tr-sub-title" >
								<td><strong>Facilities Management And Procurement Division </strong></td>
								<td align="right" width="10%">

								</td>
							</tr>
							<tr>
								<td colspan="2">
									<ul>
										<li>Blocked building access </li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>Remarks : '.$logistic['log_remarks'].'</td>
								<td></td>
							</tr>
							<tr>
								<td>Status : '.($logistic['log_status']!=null ? ($logistic['log_status']==1 ? 'Approved':'Rejected'):'').'</td>
								<td></td>
							</tr>
							<tr>
								<td>Approved By : '.$logistic['name'].'</td>
								<td>Approved Date : '.($logistic['log_createddt']!='' && $logistic['log_createddt']!=null ? date('d-m-Y', strtotime($logistic['log_createddt'])):'').'</td>
							</tr>
						</table>
						<table class="" width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="2"><hr></td>
							</tr>
							<tr id="tr-title">
								<td colspan="2"><h3>C.	FOR OFFICE USE (Admission and Student Affairs Division)</h3></td>
							</tr>
							<tr>
								<td colspan="2">Remarks : '.$appdetail['Remarks'].'</td>
							</tr>
							<tr>
								<td width="50%">Applied By : '.(($appdetail['loginName']) ? $appdetail['loginName'] : '').'</td>
								<td>Applied Date : '.(($appdetail['ApplicationDate']) ? date('d-m-Y',strtotime($appdetail['ApplicationDate'])) : '').'</td>
							</tr>
							<tr>
								<td>Approved By : '.(($appdetail['ApprovedBy']) ? $appdetail['ApprovedBy'] : '').'</td>
								<td>Approved Date : '.(($appdetail['DateApproved']) ? date('d-m-Y',strtotime($appdetail['DateApproved'])) : '').'</td>
							</tr>

						</table>
					';

					if (count($formData['IdStudentRegistration']) > $count) {
						$html2 .= '<div style="page-break-after: always"></div>';
					}

					//replace variable
					foreach ($fieldValues as $key=>$value){
						$html2 = str_replace($key,$value,$html2);
					}

					$printcontent = $printcontent.$html2;
				}

				$fieldValues2['$[content]'] = $printcontent;

				$html = file_get_contents($html_template_path);

				//replace variable
				foreach ($fieldValues2 as $key=>$value){
					$html = str_replace($key,$value,$html);
				}

				//echo $html;
				$dompdf = new DOMPDF();
				$dompdf->load_html($html);
				$dompdf->set_paper('a4', 'potrait');
				$dompdf->render();

				$dompdf->stream($output_filename);
			}
		}

		exit;
	}
}