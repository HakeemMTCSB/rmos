<?php
class Graduation_PregraduationListController extends Base_Base {

	private $_sis_session;

	public function init(){
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}

	public function indexAction(){
		
		//title
		$this->view->title= $this->view->translate("Pre-Graduation List");
		
		$form = new Graduation_Form_SearchShortlistedPregraduate();
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($this->_sis_session->search_pregraduate);
    	}
    	
		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				
			if ($form->isValid($formData)) {
				
				$this->_sis_session->search_pregraduate = $formData;
				
				$this->view->status = $formData['Status'];
				$this->view->idProgram = $formData['IdProgram'];
				
				$form->populate($formData);
				
				$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
				$this->view->student = $pregraduationListDb->getData($formData);
			}
			
			
			
		}else{
			
			//populate by session 
	    	if (isset($this->_sis_session->search_pregraduate)) {    	
	    		$form->populate($this->_sis_session->search_pregraduate);
	    		$formData = $this->_sis_session->search_pregraduate;	

	    		$this->view->status = $formData['Status'];
				$this->view->idProgram = $formData['IdProgram'];	    		
				
				$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
				$this->view->student = $pregraduationListDb->getData($formData);
	    	}
	    	
		}
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$this->view->convo = $convocationDb->getListConvoByYear();
			
		$this->view->form = $form;
	}
	
	public function publishAction(){
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			
			$data['publish']=1;
			$data['publish_createdt']=date('Y-m-d H:i:s');
			$data['publish_createby']=$auth->getIdentity()->id;
			
				for($i=0; $i<count($formData['idStudentRegistration']); $i++){
								
					$IdStudentRegistration = $formData['idStudentRegistration'][$i];
					$pregraduationListDb->updateStatus($data,$IdStudentRegistration);	
				
				}
				
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'index'),'default',true));
			
		}
		
	}
	
	public function approveAction(){
		
		set_time_limit(600); //10 minutes
		ini_set('post_max_size', '2M');
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$auth = Zend_Auth::getInstance();
			
			$studentDB = new Graduation_Model_DbTable_StudentRegistration();
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			$graduationDB = new Graduation_Model_DbTable_ConvocationChecklist();
			$graduatestudentchecklistDB = new Graduation_Model_DbTable_PregraduateStudentChecklist();
			$systemErrorDB = new App_Model_General_DbTable_SystemError();
			$semesterDB = new Registration_Model_DbTable_Semester();		
			 
			$id_convocation = $formData['id_convocation'];
			$idProgram = $formData['idProgram'];
			
			//get convo checklist
			$checklist = $graduationDB->getChecklistByProgram($id_convocation,$idProgram);

			for($i=0; $i<count($formData['idStudentRegistration']); $i++){
								
				$IdStudentRegistration = $formData['idStudentRegistration'][$i];				
				
				//get student pregraduate info
				$graduate_info = $pregraduationListDb->getInfo($IdStudentRegistration);				
				
				$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$graduate_info['IdScheme'],'branch_id'=>$graduate_info['IdBranch']));

				$db->beginTransaction();
				
				try {	
					
						//update status = 1 & convo session
						$pregraduationListDb->updateStatus(array('status'=>1,'c_id'=>$id_convocation,'approve_date'=>date('Y-m-d H:i:s'),'approve_by'=>$auth->getIdentity()->id),$IdStudentRegistration);
						
						//add student convo checklist
						if ($checklist) {
							foreach ($checklist as $check) {

								$checkData['pregraduate_list_id'] = $graduate_info['id'];
								$checkData['IdStudentRegistration'] = $IdStudentRegistration;
								$checkData['idChecklist'] = $check['idChecklist'];
								$checkData['psc_createddt'] = date('Y-m-d H:i:s');
								$checkData['psc_createdby'] = $auth->getIdentity()->id;

								$db->insert('pregraduate_student_checklist', $checkData);
							}
						}
						
						
						//update profile status
						$db->update('tbl_studentregistration',array('profileStatus'=>248),'IdStudentRegistration = "'.$IdStudentRegistration.'"'); //Completed Deftype=20
						
						//update semester status 229 = completed						
						//  ---------------- update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($IdStudentRegistration,$semester['IdSemesterMaster'],229);
						//  ---------------- end update studentsemesterstatus table ----------------	
													
													
						/*send email*/
						$student = $studentDB->getProfile($IdStudentRegistration);
		          		//$this->sendMail($student,$tpl_id);
		          		
						$db->commit();
					
				}catch (Exception $e) {
																						
						//echo $e->getMessage();
						$db->rollBack();
						
						$stack_student['status'] = 'Fail';
						$stack_student['function'] = 'Save Data';
						$stack_student['error_message'] = $e->getMessage();
						
						$error['se_txn_id']=0;
						$error['se_IdStudentRegistration']=$IdStudentRegistration;
						$error['se_IdStudentRegSubjects']=0;
						$error['se_title']='Approve Student Graduation';
						$error['se_message']=$e->getMessage();
						$error['se_createddt']=date("Y-m-d H:i:s");
						$error['se_createdby']=$auth->getIdentity()->id;
						$systemErrorDB->addData($error);
						
				}//end try catch
					          		
			}
						
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'index'),'default',true));
			
		}
		
	}
	
	public function sendMail($student,$tpl_id){
    	
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
        $templateData = $pregraduationListDb->getTemplate($tpl_id); 

        if($templateData){
            $templateMail = $templateData['tpl_content'];

            $templateMail = $this->parseTag($student,$tpl_id,$templateMail);

            $email_receipient = $student["appl_email"];
            $email_personal_receipient = $student["appl_email_personal"];
            $name_receipient  = $student["appl_fname"].' '.$student["appl_lname"];    		

            //email personal
            $data_inceif = array(
                'recepient_email' => $email_personal_receipient,
                'subject' => $templateData["tpl_name"],
                'content' => $templateMail	,
                'attachment_path'=>null,
                'attachment_filename'=>null	,
                'date_que' => date("Y-m-d H:i:s")				
            );	
          
            $this->addData($data_inceif);
            
        }//end if email template exist	
        
    }
    
    
	public function parseTag($student, $tpl_id, $content){
		
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
        $templateTags = $pregraduationListDb->getTemplateTag($tpl_id);

        foreach ($templateTags as $tag){
        	
            $tag_name = $tag['tag_name'];

            if($tag_name=='Salutation') {
                $content = str_replace("[".$tag['tag_name']."]", $student['appl_salutation'], $content); 
            } 
            else if($tag_name=='Student Name') {
                $content = str_replace("[".$tag['tag_name']."]", $student['appl_fname'].' '.$student['appl_lname'], $content);
            } 
            else if($tag_name=='Student ID') {
                $content = str_replace("[".$tag['tag_name']."]", $student['registrationId'], $content); 
            }
        }

        return $content;
    }
    
 	public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
    	
    	//check current status
    	$semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
    	$semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);
    	
    	if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
    		//nothing to update
    	}else{
    		//add new status & keep old status into history table
    		$cms = new Cms_Status();
    		$auth = Zend_Auth::getInstance();
    			      			        	
			$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
				            'idSemester' => $IdSemesterMain,
				            'IdSemesterMain' => $IdSemesterMain,								
				            'studentsemesterstatus' => $newstatus, 	
	                        'Level'=>0,
				            'UpdDate' => date ( 'Y-m-d H:i:s'),
				            'UpdUser' => $auth->getIdentity()->iduser
	        );				
					
    		$message = 'Graduation upon approval';
    		$cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,null);
    	}
    	
    }
	
	public function studentListAction(){
		
		//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
		$this->view->title= $this->view->translate("Pre-Graduation : Student List");
				
		$auth = Zend_Auth::getInstance();
		
		$studentDB = new Graduation_Model_DbTable_StudentRegistration();
		$icampusDB = new icampus_Function_Application_AcademicProgress();
					
		$form = new  Graduation_Form_SearchPregraduate();
		$this->view->form = $form;
		
		if($this->getRequest()->isPost() && $this->_request->getPost ('search')) {
					
				$formData = $this->getRequest()->getPost();
				
				if ($form->isValid($formData)) {
					
					$student_list = $studentDB->getStudent($formData);
					
					if(count($student_list)>0){
						foreach($student_list as $key=>$student){
							//echo '<hr>';
							$result = $icampusDB->getExpectedGraduateStudent($student['IdStudentRegistration']);
							//var_dump();
							//echo $student['IdStudentRegistration'].'========>$result:'.$result.'<br>';
							if($result>0){
								unset($student_list[$key]);
								//echo $student['IdStudentRegistration'].'======>unset';

							}
						}
						array_values($student_list);
						
						$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
						$paginator->setItemCountPerPage(100);
						$paginator->setCurrentPageNumber($this->_getParam('page',1));
									
						$this->view->paginator = $paginator;
					}else{
					
						$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
						$paginator->setItemCountPerPage(100);
						$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
						$this->view->paginator = $paginator;
						
					}
					
					//echo 'Total:'.count($this->view->paginator);
				}
				
		}
		
		
		if($this->getRequest()->isPost() && $this->_request->getPost ('submit')) {
			
			$formData = $this->getRequest()->getPost();
			
			$pregraduateDB = new Graduation_Model_DbTable_Pregraduation();
			
			for($i=0; $i<count($formData['IdStudentRegistration']); $i++){
				
				$data['idStudentRegistration'] = $formData['IdStudentRegistration'][$i];
				$data['shortlisted_date']=date("Y-m-d H:i:s");
				$data['shortlisted_by']=$auth->getIdentity()->id;
						
				$pregraduateDB->insert($data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'student-list'),'default',true));
		}
				
	}
	
	public function manualAddAction(){
		
		$this->view->title= $this->view->translate("Pre-Graduation : Add Student - Manual");
				
		$auth = Zend_Auth::getInstance();
		
		$studentDB = new Graduation_Model_DbTable_StudentRegistration();
					
		$form = new  Graduation_Form_SearchManual();
		$this->view->form = $form;
		
		if($this->getRequest()->isPost() && $this->_request->getPost ('search')) {
					
				$formData = $this->getRequest()->getPost();
				
				if ($form->isValid($formData)) {
					
					$student = $studentDB->getStudentManualAdd($formData);
					$this->view->student = $student;
					
				}
				
		}
		
		
		if($this->getRequest()->isPost() && $this->_request->getPost ('submit')) {
			
			$formData = $this->getRequest()->getPost();
			
			$pregraduateDB = new Graduation_Model_DbTable_Pregraduation();
			
			for($i=0; $i<count($formData['IdStudentRegistration']); $i++){
				
				$data['idStudentRegistration'] = $formData['IdStudentRegistration'][$i];
				$data['shortlisted_date']=date("Y-m-d H:i:s");
				$data['shortlisted_by']=$auth->getIdentity()->id;
						
				$pregraduateDB->insert($data);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'manual-add'),'default',true));
		}
				
	}
	
	
	public function generateListAction(){
		
		$ses_generate_pregraduation_list = new Zend_Session_Namespace("pregrad_ses");
		$this->view->ses_data = $ses_generate_pregraduation_list;
		
		//title
		$this->view->title= $this->view->translate("Pre-Graduation List : Generate");
		
		$step = $this->_getParam("step", null);
		$this->view->step = $step;
		 
		if($step==null){
			$step = 1;
			Zend_Session::namespaceUnset('pregrad_ses');
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>$step),'default',true));
		}
		
		
		if($step==1){
			
			if($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
				
				$ses_generate_pregraduation_list->faculty_id = $formData['faculty_id'];
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>2),'default',true));
				
			}else{
				
				//faculty list
				$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
				$college_list = $collegeDb->getCollege();
				$this->view->faculty_list = $college_list;

				if(isset($ses_generate_pregraduation_list->faculty_id)){
					$this->view->faculty_id = $ses_generate_pregraduation_list->faculty_id;
				}
			}
			
		}else
		if($step==2){
			
			if(!isset($ses_generate_pregraduation_list->faculty_id)){
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>1),'default',true));
			}
			
			if($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
			
				$ses_generate_pregraduation_list->program_id = $formData['program_id'];
			
				//redirect
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>3),'default',true));
			
			}else{
			
				//program list
				$programDb = new GeneralSetup_Model_DbTable_Program();
				
				if(isset($ses_generate_pregraduation_list->faculty_id)){
					$program_list = $programDb->fngetFacultyProgramData($ses_generate_pregraduation_list->faculty_id);
				}else{
					$program_list = null;
				}
				
				$this->view->program_list = $program_list;
			
				if(isset($ses_generate_pregraduation_list->program_id)){
					$this->view->program_id = $ses_generate_pregraduation_list->program_id;
				}

			}
			
		}else
		if($step==3){
			if(!isset($ses_generate_pregraduation_list->program_id)){
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>2),'default',true));
			}
			
			if($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
					
				
				if( isset($formData['student']) && sizeof($formData['student'])>0 ){
					$ses_generate_pregraduation_list->student_list = $formData['student'];
				}
					
				//redirect
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>4),'default',true));
					
			}else{
					
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$this->view->intake_list = $intakeDb->getIntakeWithStudent(92,$ses_generate_pregraduation_list->program_id);
				
				if(isset($ses_generate_pregraduation_list->intake_id)){
					$this->view->intake_id = $ses_generate_pregraduation_list->intake_id;
				}
			
			}
		}else
		if($step==4){
			if(!isset($ses_generate_pregraduation_list->student_list)){
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'generate-list', 'step'=>3),'default',true));
			}
			
						
			if($this->getRequest()->isPost()) {
					
				//save to pregrad table
				$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
				
				
				$student = $ses_generate_pregraduation_list->student_list;
				
				foreach ($student as $idStudentRegistration){
					$data = array(
						'idStudentRegistration' => $idStudentRegistration,
					);
					
					$pregraduationListDb->insert($data);
				}
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'index'),'default',true));
					
			}else{
					
				//loop all student list	
				$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
				$studentProfileDb = new Records_Model_DbTable_Studentprofile();
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$studentGradeDb = new Examination_Model_DbTable_StudentGrade();
				$programDb = new GeneralSetup_Model_DbTable_Program();
				$db = Zend_Db_Table::getDefaultAdapter();
				
				$pregrad_list = array();
				foreach ($ses_generate_pregraduation_list->student_list as $i => $student){
					$arr_data = array();
					
					//get registration detail
					$arr_data['registration_info'] = $studentRegistrationDb->fetchRow(array('idStudentRegistration=?'=>$student))->toArray();
					
					//profile
					$arr_data['profile'] = $studentProfileDb->fetchRow(array('appl_id=?'=>$arr_data['registration_info']['IdApplication']))->toArray();
					
					//intake
					$arr_data['intake'] = $intakeDb->getData($arr_data['registration_info']['IdIntake']);
					
					//current sem/level/block
					$select = $db->select()
								->from(array('ss'=>'tbl_studentsemesterstatus'),array('idStudentRegistration', 'max_level'=>'MAX(Level)'))
								->where('ss.idStudentRegistration = ?', $student)
								->group('ss.IdStudentRegistration');
					
					$arr_data['current_level'] = $db->fetchRow($select);
						
					//program
					$arr_data['program'] = $programDb->fetchRow(array('IdProgram = ?'=>$arr_data['registration_info']['IdProgram']))->toArray();
					
					//cgpa
					//TODO:uncomment
					/*$cgpa = $studentGradeDb->getStudentGradeInfo($arr_data['registration_info']['IdStudentRegistration'])->sg_cgpa;
					if(isset($cgpa)){
						$arr_data['cgpa'] = $cgpa;
					}else{
						$arr_data['cgpa'] = 0.00;
					}*/
					$arr_data['cgpa'] = 0.00;
					
					
					
					
					
					
					
					$pregrad_list[] = $arr_data;
				}
				
				$this->view->list = $pregrad_list;
				
			}
		}

		
		
	}
	
	public function searchStudentAction(){
		
		$ses_generate_pregraduation_list = new Zend_Session_Namespace("pregrad_ses");
		
		$data_landscape = array();
		$data_requirement = array();
		
		$this->_helper->layout()->disableLayout();
		
		
		$faculty = $this->_getParam("faculty", $ses_generate_pregraduation_list->faculty_id);
		$program = $this->_getParam("program", $ses_generate_pregraduation_list->program_id);
		$intake = $this->_getParam("intake", 0);
		$subject_count = $this->_getParam("subject_bil", null);
		$payment = $this->_getParam("payment", 0);
		$cgpa = $this->_getParam("cgpa", null);
		
		
		//find student who is active
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select2 = $db->select()
		->from(array('ss'=>'tbl_studentsemesterstatus'),array('idStudentRegistration', 'max_level'=>'MAX(Level)'))
		->group('ss.IdStudentRegistration');
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intake_data = $intakeDb->getData($intake); 
		$list['intake'] = $intake_data;
		
		$select = $db->select()
					 ->from(array('sr'=>'tbl_studentregistration'), array('IdStudentRegistration', 'registrationId','IdLandscape', 'IdProgram','IdProgramMajoring','IdIntake'))
					 ->join(array('sp'=>'student_profile'),'sp.appl_id = sr.IdApplication', array("fullname"=>"concat_ws(' ',appl_fname,appl_mname,appl_lname)"))
					 ->join(array('sss'=>$select2), 'sss.idStudentRegistration = sr.IdStudentRegistration')
					 ->where('NOT EXISTS (SELECT NULL from pregraduate_list a WHERE a.idStudentRegistration = sr.IdStudentRegistration)')
					 ->where('NOT EXISTS (SELECT NULL from graduate b WHERE b.idStudentRegistration = sr.IdStudentRegistration)')
					->where('sr.IdProgram = ?',$program)
					->where('sr.IdLandscape != 0')
					->where('sr.IdIntake = ?', $intake)
					//->where('sr.IdProgramMajoring != 0')
					->where('sr.profileStatus = 92')
					->order('sr.registrationid ASC');
		//echo $select;
		//exit;
		$list['student'] = $db->fetchAll($select);
		//$json_data = Zend_Json_Encoder::encode($list);
		//echo $json_data;
		//exit;

		//loop all student list
		$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
		$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
		$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
		
		$blockSemDB = new GeneralSetup_Model_DbTable_LandscapeBlockSemester();
		$blockCourseDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		$academicProgressDb = new Records_Model_DbTable_Academicprogress();
		
		$studentGradeDb = new Examination_Model_DbTable_StudentGrade();
		$paymentInfo = new icampus_Function_Studentfinance_PaymentInfo();

		foreach ($list['student'] as $i => &$student){
			
			
			/*
			 * get program requirement info and put in reuseable array
			 */
			if( !isset($data_requirement[$student['IdProgram']][$student['IdLandscape']]) ){
				$data_requirement[$student['IdProgram']][$student['IdLandscape']] = $this->getProgramRequirement($student['IdProgram'], $student['IdLandscape']);
			}
			
			$student['ch_requirement'] = $data_requirement[$student['IdProgram']][$student['IdLandscape']];
			
			
			
			/*
			 * get landscape data and put in reuseable array
			 */
			if( !isset($data_landscape[$student['IdProgram']][$student['IdLandscape']][$student['IdProgramMajoring']]) ){
				$data_landscape[$student['IdProgram']][$student['IdLandscape']][$student['IdProgramMajoring']] = $this->getLanscapeDataByMajoring($student['IdLandscape'],$student['IdProgramMajoring'],$student['IdProgram']);
			}
			//HEAVY PROCESSING
			//do not store into array student (timeout will occur) 
			$courses = $data_landscape[$student['IdProgram']][$student['IdLandscape']][$student['IdProgramMajoring']];

			
			
			/*
			 * 	HEAVY PROCESSING
			 *	check for subject completeness 
			 */
			foreach ($courses['all'] as $index => &$subject){
				
				if(isset($subject['coursetypeid'])){
					$subject['SubjectType'] = $subject['coursetypeid']; 
				}
				
				$grademark = $academicProgressDb->checkcompleted($student['IdStudentRegistration'],$subject["IdSubject"]);
				
				//requirement - subject count
				$student['ch_requirement'][$subject['SubjectType']]['total_subject_count'] += 1;
				
				
				if($grademark){
					//$list[$i]['courses']['all'][$index]['completed'] = 1;
					
					//requirement - subject ch completed
					$student['ch_requirement'][$subject['SubjectType']]['ch_completed'] = $student['ch_requirement'][$subject['SubjectType']]['ch_completed'] + $subject['CreditHours']; 
					
					//requirement - subject count completed
					$student['ch_requirement'][$subject['SubjectType']]['total_subject_completed_count'] += 1;
					
				}else{
					//$list[$i]['courses']['all'][$index]['completed'] = 0;
				}
				
			}
			
			
			//get cgpa
			/*if(isset($studentGradeDb->getStudentGradeInfo($student['IdStudentRegistration'])->sg_cgpa)){
				$student['cgpa'] = $studentGradeDb->getStudentGradeInfo($student['IdStudentRegistration'])->sg_cgpa;
			}else{
				$student['cgpa'] = 0.00;
			}*/
			//TODO:unremove
			$student['cgpa'] = 0.00;
			
			
			//outstanding payment
			$student['finance'] = $paymentInfo->getStudentPaymentInfo($student['IdStudentRegistration']);
			
			
			
		}// end loop student list
		
		
		/*
		 * Run filter
		 * TODO: uncomment filters
		 */
		if(is_array($list['student'])){
			foreach ($list['student'] as $index=> $stud){
				
				//filter by subject bill
				if($subject_count){
					
					$sub_remaining = 0;
					foreach ($stud['ch_requirement'] as $sub_req){
						$sub_remaining += $sub_req['total_subject_count'] - $sub_req['total_subject_completed_count'];
					}
					
					if($sub_remaining > $subject_count){
						//unset($list['student'][$index]);
						//continue;
					}
			
				}
				
				//filter by outstanding payment
				if($payment=='1'){
					
					if( $stud['finance']['total_invoice_balance'] > 0 ){
						//unset($list['student'][$index]);
						//continue;
					}
					
				}
				
				//filter by cgpa
				if($cgpa!=null && $cgpa!=""){
					if(floatval($cgpa) > $stud['cgpa']){
						//unset($list['student'][$index]);
						//continue;
					}
				}
			}
			
			$list['student'] = array_values($list['student']);
		}
		
		
		/*echo "<pre>";
		print_r($list['student']);
		echo "<pre>";
		exit;*/
		
		//pack to json
		$json_data = Zend_Json_Encoder::encode($list);
		echo $json_data;
		exit;
		
	}
	
	private function getProgramRequirement($program_id, $landscape_id){
		
		$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
		
		$programRequirement = $progReqDB->getlandscapecoursetype($program_id,$landscape_id);
			
		//requirement detail
		$arr_req_detail = array();
		foreach ($programRequirement as $req){
			$arr_req_detail[$req['SubjectType']] = array(
					'ch_required'=>$req['CreditHours'],
					'ch_completed'	=> 0, //init ch completed
					'total_subject_count' => 0, //init sub count
					'total_subject_completed_count' => 0 //init sub completed count
			);
		}
		
		return $arr_req_detail;
	}
	
	private function getLanscapeDataByMajoring($landscape_id, $majoring_id,$program_id){
		
		$courses = array();
		
		$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
		$progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
		$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
		
		$blockSemDB = new GeneralSetup_Model_DbTable_LandscapeBlockSemester();
		$blockCourseDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		
		//get landscape info
		$landscape = $landscapeDB->getLandscapeDetails($landscape_id);
			
		
			
		//get 2 type of subjects including major subject
		if($landscape["LandscapeType"]==42 || $landscape["LandscapeType"]==43) {//Semester & level Based
		
			//get Common Course
			$common_course = $landscapeSubjectDb->getCommonCourse($program_id,$landscape_id);
			$courses['all'] = $common_course;
		
			//majoring course
			if($majoring_id!=null && $majoring_id!=0){
				$majoring_course = $landscapeSubjectDb->getMajoringCourse($program_id, $landscape_id, $majoring_id);
				$courses['all'] = array_merge($courses['all'], $majoring_course);
			}
		
		
		}else
		if($landscape["LandscapeType"]==44){
			
			$courses['all'] = array();
				
			for($ii=1; $ii<=$landscape["Blockcount"]; $ii++){
					
				$blocks = $blockSemDB->getlandscapeblockBySem($landscape_id,$ii);
				
				foreach($blocks as $block){
						
					//get course
					$common_course = $blockCourseDb->getBlockCourse($landscape_id,$block["idblock"]);
					$courses['all'] = array_merge($courses['all'],$common_course);
				}
			}
		
		}
		
		return $courses;
	}
	
	
	
	
	public function migrateAction(){
		
		$this->view->title = "Pre-graduation - migration to graduation list";
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		
			$arr_data = array();
			foreach ($formData['student'] as $student){
				$prereg_data = $pregraduationListDb->getData($student);
				
				//make sure data is approved and not yet migrated
				if(isset($prereg_data['dean_approval_skr']) && isset($prereg_data['rector_approval_skr']) && $prereg_data['migrated']!=1 ){
					$arr_data[] = $prereg_data;
				} 
			}
			
			$this->view->list = $arr_data;
		
		
		}else{
			
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'pregraduation-list', 'action'=>'index'),'default',true));
			
		}
		
	}
	
	public function migrateConfirmAction(){
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			$graduationDb = new Graduation_Model_DbTable_Graduation();
			
			foreach ($formData['student'] as $student){
				
				//pregrad list
				$pregrad_data = $pregraduationListDb->getData($student);
				
				//update migrated
				$pregraduationListDb->update(array('migrated'=>1), 'id = '.$student);
				
				//grad data
				$data = array(
						'idStudentRegistration' => $pregrad_data['idStudentRegistration'],
				);
					
				$graduationDb->insert($data);
				
			}
			
		}
		
		
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'graduation-list', 'action'=>'index'),'default',true));
		
	}
	
	
	

}
?>