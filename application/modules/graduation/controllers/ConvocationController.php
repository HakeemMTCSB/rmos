<?php
class Graduation_ConvocationController extends Base_Base {

	private $_sis_session;

	public function init(){
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}
	
	public function indexAction() {
			exit;
	}
	
	public function convocationListAction(){
		//title
		$this->view->title= $this->view->translate("Convocation List");
		
		
		$academic_year = $this->_getParam("academic_year", null);
			
		//get data
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			$data = $convocationDb->getData(0,$formData);
						
		} else{
			$data = $convocationDb->getData();
		}
				
		
		if(count($data)>0){
		foreach($data as $key=>$convo){
			//get award tagging
			$award = $convocationDb->getTaggingAward($convo['c_id']);
			$data[$key]['award']=$award;
		}
		}
		
		$this->view->data = $data;
		
	}
	
	public function graduateListAction(){
		
		$id = $this->getParam('id', null);
		$this->view->id = $id;
		
		if(!$id){
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'convocation-list'),'default',true));
		}
		
		//title
		$this->view->title= $this->view->translate("Convocation - Graduate List");
		
		$form = new Graduation_Form_SearchGraduand();
		$this->view->form = $form;
			
		//convocation info
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$convo_data = $convocationDb->getData($id);
		$this->view->convo = $convo_data;

		//get program list
		$programList = $convocationDb->getProgramList();
		$this->view->programList = $programList;
		
		//pre-graduate list by status=1 approved and convo id
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();

		if ($this->getRequest()->isPost()){
			$formData = $this->getRequest()->getPost();
			$formData['c_id']=$id;
			//$formData['Status']=2;

			if (isset($formData['search'])){
				$this->view->student = $pregraduationListDb->getData($formData);
				$this->view->programId = $formData['IdProgram'];
			}else{
				$this->view->student = $pregraduationListDb->getData(1);
				$this->view->programId = false;
			}
		}else{
			$this->view->student = $pregraduationListDb->getData();
			$this->view->programId = false;
		}
	}
	

	public function graduateConvoDataAction() {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$cid = $this->getParam('id', null);
		$this->view->cid = $cid;
		
		$id = $this->getParam('idStudent', null);
		$this->view->studentid = $id;
		
		if(!$id){
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'convocation-list'),'default',true));
		}
		
		//title
		$this->view->title= $this->view->translate("Convocation - Graduate Info");
		
		$auth = Zend_Auth::getInstance();
		
		$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
		$grad_data = $pregraduationListDb->getGraduateInfo($id);
		$this->view->grad_data = $grad_data;

		//refreshment
		$Guest = new Graduation_Model_DbTable_Guest();
		$refreshment = $Guest->getrefreshment($id);
		$this->view->refreshment = $refreshment;
		
		//get robe info
		$robeDB = new Graduation_Model_DbTable_ConvocationGraduate();
		$this->view->robe = $robeDB->getDataById($id);
				
		//guest
		$Guest = new Graduation_Model_DbTable_Guest();
		$guests = $Guest->getguests($id);
		$this->view->guests = $guests;
		
		//award
	    $ConvoAwardDB = new Graduation_Model_DbTable_ConvocationAward();
	    $this->view->awards = $ConvoAwardDB->getDataByConvo($grad_data['c_id']);
	    
	    $StudentAward = new Graduation_Model_DbTable_AwardStudent();
	    $this->view->awarded = $StudentAward->get_awarded($id);

	    //checklist
	    $graduatestudentchecklistDB = new Graduation_Model_DbTable_PregraduateStudentChecklist();
		$this->view->checklists = $graduatestudentchecklistDB->getStudentChecklist($id);
	    
		$countryDB = new App_Model_General_DbTable_Country();
		$this->view->country_data = $countryDB->getData();	
    	
		//collection data
		$collect = $robeDB->getCollectionInfo($id) ;
		$this->view->collect = $collect;
		
		if($this->getRequest()->isPost() && $this->getRequest()->getPost('update')) {
				
				
				$post_data = $this->getRequest()->getPost();
				//print_r($post_data);
			
				$invoiceClass = new icampus_Function_Studentfinance_Invoice();
				
				//robe section	
				$data['robe_collect'] = (isset($post_data['robe_collect'])) ? $post_data['robe_collect']:null;
				$data['robe_return'] = (isset($post_data['robe_return'])) ? $post_data['robe_return']:null;
							
				//$data['robe_serial_number'] = $post_data['robe_serial_number'];
				$data['robe_size'] = (isset($post_data['robe_size'])) ? $post_data['robe_size']:null;
				
				$data['robe_date_collected'] = (isset($post_data['robe_date_collected']) && $post_data['robe_date_collected']!='') ? date('Y-m-d',strtotime($post_data['robe_date_collected'])):null;
				$data['robe_date_returned'] =  (isset($post_data['robe_date_returned']) && $post_data['robe_date_returned']!='') ? date('Y-m-d',strtotime($post_data['robe_date_returned'])):null;
			
				$data['hood_collect'] = (isset($post_data['hood_collect'])) ? $post_data['hood_collect']:null;
				$data['hood_return'] = (isset($post_data['hood_return'])) ? $post_data['hood_return']:null;
				
				$data['hood_date_collected'] = (isset($post_data['hood_date_collected'])) ?  date('Y-m-d',strtotime($post_data['hood_date_collected'])):null;
				$data['hood_date_returned'] = (isset($post_data['hood_date_returned'])) ? date('Y-m-d',strtotime($post_data['hood_date_returned'])):null;
				
				$data['mortar_collect'] = (isset($post_data['mortar_collect'])) ? $post_data['mortar_collect']:null;
				$data['mortar_return'] = (isset($post_data['mortar_return'])) ? $post_data['mortar_return']:null;
				
				$data['mortar_date_collected'] = (isset($post_data['mortar_date_collected'])) ? date('Y-m-d',strtotime($post_data['mortar_date_collected'])):null;
				$data['mortar_date_returned'] = (isset($post_data['mortar_date_returned'])) ? date('Y-m-d',strtotime($post_data['mortar_date_returned'])):null;
				
				$data['date_collected'] = (isset($post_data['date_collected']) && $post_data['date_collected']!='') ? format_date_to_view($post_data['date_collected'], 'Y-m-d'):null;
				$data['date_returned'] = (isset($post_data['date_returned']) && $post_data['date_returned']!='') ? format_date_to_view($post_data['date_returned'], 'Y-m-d'):null;
								
				
				if(isset($post_data['cg_id']) && $post_data['cg_id']!=''){
					$robeDB->update($data, "id = " . $post_data['cg_id']);
				}else{
					
					$data['idStudentRegistration'] = $post_data['idStudentRegistration'];
					$data['convocation_id'] = $post_data['c_id'];
					$data['add_by'] = date('Y-m-d H:i:s');
					$data['add_date'] = $auth->getIdentity()->id;
				
					$robeDB->insert($data);
				}
				
				
				//award section
				if(!empty($post_data['award'])) {
					//award section
					$StudentAward->clear_tagging($post_data['idStudentRegistration']);
					
					foreach($post_data['award'] as $award_id => $awarded) {
						$StudentAward->tag_student_to_award($post_data['idStudentRegistration'], $award_id);
					}
				}
				
				
				
				//Scroll,Transcript & Photo Collection
				
				//$collection['scroll_self_collect'] = $post_data['scroll_self_collect'];
				//$collection['scroll_courier'] = $post_data['scroll_courier'];
				
				
				$collection['scroll_collection'] = $post_data['scroll_collection'];
				$collection['photo_collection'] = $post_data['photo_collection'];
				
				if($post_data['photo_collection']==2){
					$collection['photo_number'] = $post_data['photo_number'];
				}else{
					$collection['photo_number'] = null;
				}
				
				if($post_data['photo_collection']==2 || $post_data['scroll_collection']==2){
					$collection['scroll_address1'] = $post_data['scroll_address1'];
					$collection['scroll_address2'] = $post_data['scroll_address2'];
					$collection['scroll_postcode'] = $post_data['scroll_postcode'];
					$collection['scroll_country'] = $post_data['scroll_country'];
					$collection['scroll_state'] = $post_data['scroll_state'];
					$collection['scroll_state_others'] = (isset($post_data['scroll_state_others'])) ? $post_data['scroll_state_others']:'';
					$collection['scroll_city'] = $post_data['scroll_city'];	
					$collection['scroll_city_others'] = (isset($post_data['scroll_city_others'])) ? $post_data['scroll_city_others']:'';	
				}else{
					$collection['scroll_address1'] = null;
					$collection['scroll_address2'] = null;
					$collection['scroll_postcode'] = null;
					$collection['scroll_country'] = null;
					$collection['scroll_state'] = null;
					$collection['scroll_state_others'] = null;
					$collection['scroll_city'] = null;
					$collection['scroll_city_others'] = null;
				}
				
				//$collection['photo_self_collect'] = $post_data['photo_self_collect'];
				//$collection['photo_courier'] = $post_data['photo_courier'];	
				
				/*$collection['photo_address1'] = $post_data['photo_address1'];
				$collection['photo_address2'] = $post_data['photo_address2'];
				$collection['photo_postcode'] = $post_data['photo_postcode'];
				$collection['photo_country'] = $post_data['photo_country'];
				$collection['photo_state'] = $post_data['photo_state'];
				$collection['photo_state_others'] = (isset($post_data['photo_state_others'])) ? $post_data['photo_state_others']:'';
				$collection['photo_city'] = $post_data['photo_city'];	
				$collection['photo_city_others'] = (isset($post_data['photo_city_others'])) ? $post_data['photo_city_others']:'';
				*/
					
				if(isset($post_data['gc_id']) && $post_data['gc_id']!=''){
					
				
					if($post_data['photo_collection']==2 || $post_data['scroll_collection']==2){
						
						if($collect['gc_invoice_id']==''){
							//echo 'masuk generate';	
							$invoice_id = $invoiceClass->generateInvoicePostage($id,123,$post_data['scroll_country'],$post_data['scroll_state']);
							$collection['gc_invoice_id'] = $invoice_id;
						}
						
					}else					
					if($post_data['photo_collection']!=2 && $post_data['scroll_collection']!=2){
						
						if($collect['gc_invoice_id']!=''){		
							//echo 'masuk cancel';									
							$invoiceClass->generateCreditNoteEntry($id,$collect['gc_invoice_id'],100,0,1);
							$collection['gc_invoice_id'] = null;
						}
					}							
					
					$db->update('tbl_graduation_collection',$collection, "gc_id = " . $post_data['gc_id']);
					
				}else{
					
					//generate invoice for photo charges
					if($post_data['photo_collection']==2 || $post_data['scroll_collection']==2){
						//echo 'masuk generate';
						$invoice_id = $invoiceClass->generateInvoicePostage($id,123,$post_data['scroll_country'],$post_data['scroll_state']);
						$collection['gc_invoice_id'] = $invoice_id;
					}
					
					$collection['idStudentRegistration'] = $post_data['idStudentRegistration'];
					$collection['gc_createddt'] = date('Y-m-d H:i:s');
					$collection['gc_createdby'] = $auth->getIdentity()->id;								
					$db->insert('tbl_graduation_collection',$collection);
				}
				//exit;
				$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'graduate-convo-data','id'=>$post_data['c_id'],'idStudent'=>$post_data['idStudentRegistration']),'default',true));
		}
		
		/*if($this->getRequest()->isPost() && $this->getRequest()->getPost('update_award')) {
			
				$post_data = $this->getRequest()->getPost();
			
				$StudentAward->clear_tagging($post_data['idStudentRegistration']);

				if(!empty($post_data['award'])) {
					foreach($post_data['award'] as $award_id => $awarded) {
						$StudentAward->tag_student_to_award($post_data['idStudentRegistration'], $award_id);
					}
				}
				$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
				$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'graduate-convo-data','id'=>$post_data['idStudentRegistration']),'default',true));
		}*/
	    
		
		
	}
	
	
	public function attendanceAction(){
		
		if($this->getRequest()->isPost()) {					
		
			$formData = $this->getRequest()->getPost();
		
			$auth = Zend_Auth::getInstance();
			
			
			$pregraduationListDb = new Graduation_Model_DbTable_Pregraduation();
			$graduand = $pregraduationListDb->getInfo($formData['IdStudentRegistration']);
				
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
				
			if($formData['confirm_attendance'] == 1){ // admin change tu atttend:yes					
				if(isset($graduand) && $graduand['invoice_id']==''){	
					//check if invoice already generrated			
					$invoice_id = $invoiceClass->generateInvoiceConvocation($formData['IdStudentRegistration'],$formData['c_id'],67);
					$data['invoice_id'] = $invoice_id;
				}
			}
			
			if($formData['confirm_attendance'] == 0 ){ // admin change tu atttend:no					
				if(isset($graduand) && $graduand['invoice_id']!=''){
					//cancel the invoice if exist
					$invoiceClass->generateCreditNoteEntry($formData['IdStudentRegistration'],$graduand['invoice_id'],100,0,1);
					$data['invoice_id'] = null;
				}
			}
						
			$data['confirm_attend']=$formData['confirm_attendance'];
			$data['confirm_by']=$auth->getIdentity()->iduser;
			$data['confirm_date']=date("Y-m-d H:i:s");
			$data['confirm_role']='admin';
			
			$pregraduationListDb->updateStatus($data,$formData['IdStudentRegistration']);
						

			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'graduate-convo-data','id'=>$formData['c_id'],'idStudent'=>$formData['IdStudentRegistration']),'default',true));
		}
	}
	
	public function applicationAction() {
		
		//title
		$this->view->title= $this->view->translate("Convocation - Application List");
		
		$convocationApplicationDb = new Graduation_Model_DbTable_ConvocationApplication();
		$convocationGraduateDb = new Graduation_Model_DbTable_ConvocationGraduate();
		
		if($this->getRequest()->isPost()) {
					
		
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
				
			foreach ($formData['convocation_application_id'] as $app_id){
				
				//update application
				$data = array(
					'status' => $formData['type']==1?1:0,
					'status_date' => date('Y-m-d H:i:s'),
					'status_by' => $auth->getIdentity()->iduser		
				);
			
				$convocationApplicationDb->update($data, 'id = '.$app_id);
				
				if($formData['type']==1){
					//insert convocation graduate table
					
					$app_data = $convocationApplicationDb->getData($app_id);
					
					$data_cg = array(
						'idStudentRegistration' => $app_data['IdStudentRegistration'],
						'convocation_id' => $app_data['convocation_id']
					);
					
					$convocationGraduateDb->insert($data_cg);
				}
			}
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'application'),'default',true));
			
		}
		
		$this->view->application_list = $convocationApplicationDb->getApplication();
		$this->view->application_list_history = $convocationApplicationDb->getApplicationHistory();
				
	}
	
	public function addApplicationAction(){
		//title
		$this->view->title= $this->view->translate("Convocation - Add Application");
		
		//faculty list
		$facultyDb = new GeneralSetup_Model_DbTable_Collegemaster();
		$faculty_list = $facultyDb->getCollege();
		
		//program list
		$programDb = new GeneralSetup_Model_DbTable_Program();
		
		foreach ($faculty_list as &$faculty){
			$faculty['program'] = $programDb->fngetFacultyProgramData($faculty['IdCollege']);
		}
		
		$this->view->program = $faculty_list;
		
		
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$this->view->intake_list = $intakeDb->getData();
		
		//convocation list
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$this->view->convo_list = $convocationDb->getData();
		
		$graduationDb = new Graduation_Model_DbTable_Graduation();
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$nim = isset($formData['search_nim'])?$formData['search_nim']:null;
			$this->view->search_nim = $nim;
			
			$program = isset($formData['search_program_id'])?$formData['search_program_id']:null;
			$this->view->search_program_id = $program;
			
			$intake = isset($formData['search_intake_id'])?$formData['search_intake_id']:null;
			$this->view->search_intake_id = $intake;
			
			$from = isset($formData['search_date_add_from'])?$formData['search_date_add_from']:null;
			$this->view->search_date_add_from = $from;
			
			$to = isset($formData['search_date_add_to'])?$formData['search_date_add_to']:null;
			$this->view->search_date_add_to = $to;
			
			
			$list = $graduationDb->getConvocationCandidate($nim,$program,$intake,$from,$to);
		}else{
			$list = $graduationDb->getConvocationCandidate();
		}
		
		$this->view->list = $list;
		
	}
	
	public function saveManualApplicationAction(){
		
		if($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			$convocationApplicationDb = new Graduation_Model_DbTable_ConvocationApplication();
			$graduationDb = new Graduation_Model_DbTable_Graduation();
			
			foreach ($formData['graduate_id'] as $gid){
				
				$graduate = $graduationDb->fetchRow(array('id=?'=>$gid));
				
				$data = array(
					'IdStudentRegistration' => $graduate['idStudentRegistration'],
					'convocation_id' => $formData['convocation_id']	
				);
				
				$convocationApplicationDb->insert($data);
				
			}
			
		}
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'convocation', 'action'=>'application'),'default',true));
	}
}
?>