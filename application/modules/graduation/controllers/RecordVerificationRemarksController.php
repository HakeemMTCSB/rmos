<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/9/2015
 * Time: 2:55 PM
 */
class Graduation_RecordVerificationRemarksController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Graduation_Model_DbTable_RecordVerificationRemarks();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Record Verification Remarks');

        $form = new Graduation_Form_RecordVerificationRemarksSearch();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])){
                $studentList = $this->model->getStudent($formData);

                if ($studentList){
                    foreach ($studentList as $key => $studentLoop){
                        $checklist = $this->model->getGraduationChecklist($studentLoop['IdStudentRegistration']);

                        if ($checklist){
                            $studentList[$key]['checklist']=$checklist;
                        }else{
                            unset($studentList[$key]);
                        }
                    }
                }
                $this->view->studentList = $studentList;
                $form->populate($formData);
            }else{

            }
        }else{

        }
    }

    public function saveAction(){
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['viewchecklist']) && count($formData['viewchecklist']) > 0){
                foreach($formData['viewchecklist'] as $key=>$value){
                    $data = array(
                        'psc_view'=>$value
                    );
                    $this->model->updateChecklist($data, $key);
                }
            }
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
        $this->_redirect($this->baseUrl . '/graduation/record-verification-remarks/index/');
    }
}