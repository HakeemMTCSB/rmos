<?php
class Graduation_GuestController extends Base_Base {

	public function indexAction() {
		
		$this->view->title= $this->view->translate("Guest/Refreshment Application List");
							
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();
					
		$form = new Graduation_Form_SearchGuestApplication();
				
		if($this->getRequest()->isPost() && $this->_request->getPost ('search')) {

			$formData = $this->getRequest()->getPost();
			
			if($form->isValid($formData)){
				
				$form->populate($formData);
				
				$this->view->status = $formData['status'];
				$this->view->type = $formData['type'];
				
				$guestDB = new Graduation_Model_DbTable_Guest();
				$student_list = $guestDB->getApplication($formData,1);
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage(100);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
						
				$this->view->student = $paginator;	
								
			}			
			
		}
		
		$this->view->form = $form;
		
		
		if($this->getRequest()->isPost() && ($this->_request->getPost ('submit') || $this->_request->getPost ('reject'))) {
			
			$formData = $this->getRequest()->getPost();

			$auth = Zend_Auth::getInstance();
									
			$guestDB = new Graduation_Model_DbTable_Guest();
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
			$guestDB = new Graduation_Model_DbTable_Guest();
			
			for($x=0; $x < count($formData['id']); $x++){
				
				$id=$formData['id'][$x];
				$guest_approve=$formData['guests'][$id];

				//get student info
				$info = $guestDB->studentInfo($id,1);
				
				if($formData['submit']=='Approve'){
					
					$status = 'APPROVED';
					$data['guest_approve'] = $guest_approve;
					
					//invoice for guest
					if($formData['type']==1){
						$invoice_id = $invoiceClass->generateInvoiceConvocation($info['IdStudentRegistration'],$info['c_id'],164,$guest_approve);
					
						$data['invoice_id'] = $invoice_id;
					}
					
					//invoice for refreshment
					$info = $guestDB->studentInfo($id,2);
					if($formData['type']==2){
						$invoice_id = $invoiceClass->generateInvoiceConvocation($info['IdStudentRegistration'],$info['c_id'],162,$guest_approve);
						$data['invoice_id'] = $invoice_id;
					}
				}
				
				if($formData['submit']=='Reject'){
					$status = 'REJECTED';
				}

				if($formData['submit']=='Revert'){
					$status = 'APPLIED';
					$data['guest_approve'] = null;
					$data['invoice_id'] = null;

					$appInfo = $guestDB->getGuestApplication($id);

					if (isset($appInfo['invoice_id']) && $appInfo['invoice_id']!=null && $appInfo['invoice_id']!=0) {
						$invoiceClass->generateCreditNoteByInvoice($info['IdStudentRegistration'], $appInfo['invoice_id']);
					}
				}

				if(isset($formData['reject']) && $formData['reject']=='Reject'){
					$status = 'REJECTED';
					$data['guest_approve'] = null;
					$data['invoice_id'] = null;

					$appInfo = $guestDB->getGuestApplication($id);

					if (isset($appInfo['invoice_id']) && $appInfo['invoice_id']!=null && $appInfo['invoice_id']!=0) {
						$invoiceClass->generateCreditNoteByInvoice($info['IdStudentRegistration'], $appInfo['invoice_id']);
					}
				}
					
				$data['status'] = $status;				
				$data['approval_date'] = date('Y-m-d H:i:s');
				$data['approval_by'] = $auth->getIdentity()->id;
				
				$guestDB->updateData($data,'id='.(int)$id);
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been update'));
			$this->_redirect($this->view->url(array('module'=>'graduation','controller'=>'guest', 'action'=>'index'),'default',true));
		}
				
	}
	
}