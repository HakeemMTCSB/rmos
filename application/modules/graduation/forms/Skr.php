<?php

class Graduation_Form_Skr extends Zend_Dojo_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_skr');
		
		//No SKR
		$this->addElement('text','skr',
				array(
						
						'required'=>'true'
				)
		);
		$dtSkr = new Zend_Dojo_Form_Element_DateTextBox('date_of_skr');
        $dtSkr->setAttrib('dojoType',"dijit.form.DateTextBox");
       	//$dtSkr->setAttrib('constraints', "$dateofbirth");
		//$DOB->setAttrib('required',"true");
        $dtSkr->removeDecorator("DtDdWrapper");
        $dtSkr->setAttrib('title',"dd-mm-yyyy");
        //$dtSkr->setAttrib('Label',"Date of SKR");
        $dtSkr->removeDecorator("Label");
        $dtSkr->removeDecorator('HtmlTag'); 
        
        $this->addElement($dtSkr);
		
		//type
        $type = new Zend_Dojo_Form_Element_FilteringSelect('type');
        $type->addMultiOptions(array('pregraduation'=>'Pregraduation','graduation'=>'Graduation'));
        // $gender->setAttrib('required',"true");
        $type->removeDecorator("DtDdWrapper");
        $type->removeDecorator("Label");
        $type->removeDecorator('HtmlTag');
        $type->setRegisterInArrayValidator(false);
        $type->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
		$this->addElement($type);
		
		$period = new Zend_Dojo_Form_Element_FilteringSelect('period');
		$period->addMultiOptions(array('1'=>'1','2'=>'2','3'=>'3'));
		// $gender->setAttrib('required',"true");
		$period->removeDecorator("DtDdWrapper");
		$period->removeDecorator("Label");
		$period->removeDecorator('HtmlTag');
		$period->setRegisterInArrayValidator(false);
		$period->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$this->addElement($period);
		
	//	$this->type->addMultiOption('pregraduation','Pregraduation');
	//	$this->type->addMultiOption('graduation','Graduation');
		$sem = new Zend_Dojo_Form_Element_FilteringSelect('IdSemesterMain');
		
		// $gender->setAttrib('required',"true");
		$sem->removeDecorator("DtDdWrapper");
		$sem->removeDecorator("Label");
		$sem->removeDecorator('HtmlTag');
		$sem->setRegisterInArrayValidator(false);
		$sem->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$this->addElement($sem);
		
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));
		
		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'graduation', 'controller'=>'setup','action'=>'skr'),'default',true) . "'; return false;"
		));
		
		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>