<?php

class Graduation_Form_SearchShortlistedPregraduate extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','search_form');
				
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake')
		));
		
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDb->getData() as $intake){
			if($this->_locale=='ms_MY'){
				$this->IdIntake->addMultiOption($intake["IdIntake"],$intake["IntakeDefaultLanguage"]);
			}else{
				$this->IdIntake->addMultiOption($intake["IdIntake"],$intake["IntakeDesc"]);
			}
		}	

		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme'),
			'required'=>true
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}	
	
		
		//Student ID
		$this->addElement('text','studentId', array(
			'label'=>$this->getView()->translate('Student ID')
		));	
		
		//Status
		$this->addElement('select','Status', array(
			'label'=>$this->getView()->translate('Status')
		));
		
		$this->Status->addMultiOption(0,'Shortlisted');
		$this->Status->addMultiOption(1,'Approved');
		$this->Status->addMultiOption(2,'Graduated');
		
		//button
		$this->addElement('submit', 'search', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
		
		
		$this->addDisplayGroup(array('search'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>