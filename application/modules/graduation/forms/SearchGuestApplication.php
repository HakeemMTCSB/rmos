<?php

class Graduation_Form_SearchGuestApplication extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','search_form');

		$model = new Graduation_Model_DbTable_Guest();
				
		//Type
		$this->addElement('select','type', array(
			'label'=>$this->getView()->translate('Type'),
			'required'=>true
		));
		
		$this->type->addMultiOption(null,"-- Please Select --");
		$this->type->addMultiOption(1,"Guest");
		$this->type->addMultiOption(2,"Refreshment");

		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}

		//convo session
		$this->addElement('select','c_id', array(
			'label'=>$this->getView()->translate('Convo Session')
		));

		$convoList = $model->getListConvoBy();

		$this->c_id->addMultiOption(null,"-- All --");
		if ($convoList){
			foreach ($convoList as $convoLoop){
				$this->c_id->addMultiOption($convoLoop["c_id"],'Year: '.$convoLoop['c_year'].' Session: '.$convoLoop['c_session'].' Date: '.date("d-m-Y",strtotime($convoLoop['c_date_from'])).' to '.date("d-m-Y",strtotime($convoLoop['c_date_to'])));
			}
		}
					
		//Statys
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Status'),
			'required'=>true
		));
		
		$this->status->addMultiOption(null,"-- All --");
		$this->status->addMultiOption('APPLIED',"APPLIED");
		$this->status->addMultiOption('APPROVED',"APPROVED");
		$this->status->addMultiOption('REJECTED',"REJECTED");
		
		//Student ID
		$this->addElement('text','studentId', array(
			'label'=>$this->getView()->translate('Student ID')
		));
		
		
		//Student ID
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));	
		
		//button
		$this->addElement('submit', 'search', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
		
		
		$this->addDisplayGroup(array('search'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>