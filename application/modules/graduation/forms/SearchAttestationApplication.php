<?php

class Graduation_Form_SearchAttestationApplication extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','search_form');
				
			
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}	
					
		//Statys
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Status')
		));
		
		$this->status->addMultiOption(null,"-- All --");
		$this->status->addMultiOption('APPLIED',"APPLIED");
		$this->status->addMultiOption('APPROVED',"APPROVED");
		$this->status->addMultiOption('REJECTED',"REJECTED");
		
		//Student ID
		$this->addElement('text','studentId', array(
			'label'=>$this->getView()->translate('Student ID')
		));	
		
		
		//Student ID
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));	
		
		//button
		$this->addElement('submit', 'search', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
		
		
		$this->addDisplayGroup(array('search'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>