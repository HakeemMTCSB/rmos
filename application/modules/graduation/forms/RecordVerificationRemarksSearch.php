<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/9/2015
 * Time: 3:08 PM
 */
class Graduation_Form_RecordVerificationRemarksSearch extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $model = new Graduation_Model_DbTable_RecordVerificationRemarks();

        //convo session
        $convosession = new Zend_Form_Element_Select('convosession');
        $convosession->removeDecorator("DtDdWrapper");
        $convosession->setAttrib('class', 'select');
        $convosession->setAttrib('required', true);
        $convosession->removeDecorator("Label");

        $convosession->addMultiOption('', '-- Select --');

        $convoSessionList = $model->getConvoSession();

        if ($convoSessionList){
            foreach ($convoSessionList as $convoSessionLoop){
                $convosession->addMultiOption($convoSessionLoop['c_id'], 'Year: '.$convoSessionLoop['c_year'].' Session: '.$convoSessionLoop['c_session']);
            }
        }

        //program
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programList = $model->getProgramList();

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }

        //student id
        $studentid = new Zend_Form_Element_Text('studentid');
        $studentid->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //student name
        $studentname = new Zend_Form_Element_Text('studentname');
        $studentname->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $convosession,
            $program,
            $studentid,
            $studentname
        ));
    }
}