<?php

class Graduation_Form_Convocation extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_convocation');
				
		//Academic Year
		/*$this->addElement('select','IdSemesterMaster', array(
				'label'=>'Semester',
				'required'=>'true'
		));
		
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$semester_list = $semesterDb->fnGetSemesterList();
		
		$this->IdSemesterMaster->addMultiOption(null,'Please select');
		foreach ($semester_list as $semester){
			$this->IdSemesterMaster->addMultiOption($semester['key'],$semester['value']);
		}*/
		
		
		$this->addElement('hidden','c_id');
		
		
		$this->addElement('text','c_year', array(
				'label'=>'Convocation Year',
				'class'=>'monthcal'
		));
		
		
		$this->addElement('text','c_session',
				array(
						'label'=>'Convocation Session'
				)
		);
		
		$this->addElement('text','c_capacity',
				array(
						'label'=>'Capacity per session'
				)
		);
		
		$this->c_capacity->addValidator('regex', false, array(
				'pattern' => '/^[0-9]*$/i',
				'messages' => array(
						'regexInvalid'   => "Invalid type given, value should be integer",
						'regexNotMatch' => "Please insert only round number",
						'regexErrorous'  => "There was an internal error while using the pattern '%pattern%'"
				)
		));
		
		$this->addElement('text','c_guest',
				array(
						'label'=>'Number of Guest'
				)
		);
		
		$this->c_guest->addValidator('regex', false, array(
				'pattern' => '/^[0-9]*$/i',
				'messages' => array(
						'regexInvalid'   => "Invalid type given, value should be integer",
						'regexNotMatch' => "Please insert only round number",
						'regexErrorous'  => "There was an internal error while using the pattern '%pattern%'"
				)
		));
		
		$this->addElement('text','c_weblink', array(
				'label'=>'Weblink'
		));
		
		$this->addElement('text','c_date_from', 
			array(
				'label'=>'Convocation Start Date',
				'placeholder'=>'dd-mm-yyyy',
				'required'=>'true'
			)
		);
		
		$this->addElement('text','c_date_to',
				array(
						'label'=>'Convocation End Date',
						'placeholder'=>'dd-mm-yyyy',
						'required'=>'true'
				)
		);
		
		
		$this->addElement('text','c_confirm_attendance_enddate',
				array(
						'label'=>'Confirm Attendance End Date',
						'placeholder'=>'dd-mm-yyyy',
						'required'=>'true'
				)
		);
		
		$this->addElement('text','c_record_verification_enddate',
				array(
						'label'=>'Record Verification End Date',
						'placeholder'=>'dd-mm-yyyy',
						'required'=>'true'
				)
		);
		
		$this->addElement('text','c_guest_app_enddate',
				array(
						'label'=>'Graduation Guests Application End Date',
						'placeholder'=>'dd-mm-yyyy',
						'required'=>'true'
				)
		);
		
		$this->addElement('text','c_student_portal_enddate',
				array(
						'label'=>'Student Portal End Date',
						'placeholder'=>'dd-mm-yyyy',
						'required'=>'true'
				)
		);
		
		$awardDb = new Graduation_Model_DbTable_Award();
		$award=$awardDb->getData();
		
		$this->addElement('multicheckbox', 'award', array(
		    'label' => 'Award',
		    'name' => 'award'
		));		
		
		foreach($award as $data){
			$this->award->addMultiOption($data['id'],' '.$data['name']);
		}
		
		
		
		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Save',
				'decorators'=>array('ViewHelper')
		));
		
		
		$this->addDisplayGroup(array('save'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}


?>

<style>
label {
    width:880px;
}

input, label {
    
}
</style>