<?php
class Graduation_Form_Document extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//type
        $doctype = new Zend_Form_Element_Select('type');
		$doctype->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$doctype->addMultiOption('', '-- Select --');

		$defDB = new Graduation_Model_DbTable_GeneralDocuments();
		
		foreach($defDB->getDocumentType() as $type){
			$doctype->addMultiOption($type['idDefinition'], $type['DefinitionDesc']);
		}
		
		//title
        $title = new Zend_Form_Element_Text('title');
		$title->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		 //description
        $description = new Zend_Form_Element_Textarea('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
		
		
				

		//form elements
        $this->addElements(array(
			$description,
			$title,
			$doctype

		));
		
	}
}
?>