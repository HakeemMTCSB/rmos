<?php

class Graduation_Form_SearchManual extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','search_form');
				
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}		
	
		//Student
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));
		
		//Student
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Studentt ID')
		));
		
		//button
		$this->addElement('submit', 'search', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
		
		
		$this->addDisplayGroup(array('search'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>