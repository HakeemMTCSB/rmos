<?php

class Graduation_Form_SearchApprovedGraduate extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','search_form');

		$model = new Graduation_Model_DbTable_RecordVerificationRemarks();

		//convo session
		$this->addElement('select','convosession', array(
			'label'=>$this->getView()->translate('Convo Session')
		));

		$this->convosession->addMultiOption('', '-- All --');

		$convoSessionList = $model->getConvoSession();

		if ($convoSessionList){
			foreach ($convoSessionList as $convoSessionLoop){
				$this->convosession->addMultiOption($convoSessionLoop['c_id'], 'Year: '.$convoSessionLoop['c_year'].' Session: '.$convoSessionLoop['c_session']);
			}
		}
				
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake')
		));
		
		//intake
		$intakeDb = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDb->getData() as $intake){
			if($this->_locale=='ms_MY'){
				$this->IdIntake->addMultiOption($intake["IdIntake"],$intake["IntakeDefaultLanguage"]);
			}else{
				$this->IdIntake->addMultiOption($intake["IdIntake"],$intake["IntakeDesc"]);
			}
		}	

		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}	
		
		//Student ID
		$this->addElement('text','studentId', array(
			'label'=>$this->getView()->translate('Student ID')
		));	
		
		//button
		$this->addElement('submit', 'search', array(
				'label'=>'Search',
				'decorators'=>array('ViewHelper')
		));
		
		
		
		$this->addDisplayGroup(array('search'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

		
	}
}
?>