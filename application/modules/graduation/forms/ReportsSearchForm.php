<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 21/9/2015
 * Time: 9:23 AM
 */
class Graduation_Form_ReportsSearchForm extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $model = new Graduation_Model_DbTable_RecordVerificationRemarks();

        //convo session
        $convosession = new Zend_Form_Element_Select('convosession');
        $convosession->removeDecorator("DtDdWrapper");
        $convosession->setAttrib('class', 'select');
        $convosession->setAttrib('required', true);
        $convosession->removeDecorator("Label");

        $convosession->addMultiOption('', '-- Select --');

        $convoSessionList = $model->getConvoSession();

        if ($convoSessionList){
            foreach ($convoSessionList as $convoSessionLoop){
                $convosession->addMultiOption($convoSessionLoop['c_id'], 'Year: '.$convoSessionLoop['c_year'].' Session: '.$convoSessionLoop['c_session']);
            }
        }

        //program
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('required', true);
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programList = $model->getProgramList();

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }

        //award
        $award = new Zend_Form_Element_Select('award');
        $award->removeDecorator("DtDdWrapper");
        $award->setAttrib('class', 'select');
        $award->removeDecorator("Label");

        $award->addMultiOption('', '-- Select --');

        $awardList = $model->awardList();

        if ($awardList){
            foreach ($awardList as $awardLoop){
                $award->addMultiOption($awardLoop['id'], $awardLoop['name']);
            }
        }

        //student id
        $studentid = new Zend_Form_Element_Text('studentid');
        $studentid->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //student name
        $studentname = new Zend_Form_Element_Text('studentname');
        $studentname->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $convosession,
            $program,
            $award,
            $studentid,
            $studentname
        ));
    }
}