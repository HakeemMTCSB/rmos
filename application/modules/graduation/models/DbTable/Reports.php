<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 21/9/2015
 * Time: 9:14 AM
 */
class Graduation_Model_DbTable_Reports extends Zend_Db_Table_Abstract {

    public function attendanceReport($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_list'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.idStudentRegistration=b.IdStudentRegistration', array('*', 'IdStudentRegistration as studentid'))
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->join(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_graduation_collection'), 'a.idStudentRegistration=e.IdStudentRegistration')
            ->group('a.id');

        if ($search != false){
            if (isset($search['convosession']) && $search['convosession']!=''){
                $select->where('a.c_id = ?', $search['convosession']);
            }
            if (isset($search['program']) && $search['program']!=''){
                $select->where('b.IdProgram = ?', $search['program']);
            }
            if (isset($search['studentid']) && $search['studentid']!=''){
                $select->where($db->quoteInto('b.registrationId LIKE ?', '%'.$search['studentid'].'%'));
            }
            if (isset($search['studentname']) && $search['studentname']!=''){
                $select->where($db->quoteInto('CONCAT_WS(" ", c.appl_fname, c.appl_lname) LIKE ?', '%'.$search['studentname'].'%'));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function guestrefreshmentReport($studentid, $type = 1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_graduation_guests'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->where('a.type = ?', $type);

        $result = $db->fetchRow($select);
        return $result;
    }

    static function getChecklist($studentid, $type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_student_checklist'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_graduation_checklist'), 'a.idChecklist=b.id')
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->where('b.type = ?', $type)
            ->where('a.psc_status = ?', 1);

        $result = $db->fetchRow($select);
        return $result;
    }

    static function getCity($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_city'), array('a.CityName'))
            ->where('a.idCity = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    static function getState($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_state'), array('a.StateName'))
            ->where('a.idState = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    static function getCountry($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_countries'), array('a.CountryName'))
            ->where('a.idCountry = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    static function getPaymentStatus($studentid, $feeid){
        $paystatus = false;

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array('value'=>'*', 'invoiceid'=>'a.id', 'invoicedtlid'=>'b.id', 'invoicedtlamount'=>'b.amount'))
            ->join(array('b'=>'invoice_detail'), 'a.id = b.invoice_main_id')
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->where('b.fi_id = ?', $feeid)
            ->where('a.status = ?', 'A');

        $invoice = $db->fetchRow($select);

        if ($invoice){
            $select2 = $db->select()
                ->from(array('a'=>'receipt_invoice'), array('value'=>'*'))
                ->join(array('b'=>'receipt'), 'a.rcp_inv_rcp_id = b.rcp_id')
                ->where('a.rcp_inv_invoice_dtl_id = ?', $invoice['invoicedtlid'])
                ->where('b.rcp_status = ?', 'APPROVE');

            $receipt = $db->fetchRow($select2);

            if ($receipt){
                if ($receipt['rcp_inv_amount'] >= $invoice['invoicedtlamount']){
                    $paystatus = true;
                }else{
                    $paystatus = false;
                }
            }else{
                $paystatus = false;
            }

        }else{
            $paystatus = false;
        }

        return $paystatus;
    }
}