<?php 
class Graduation_Model_DbTable_StudentRegistration extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregistration';
	protected $_primary = "id";

	function getStudent($formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$pregraduate_list = $db->select()
							   ->from(array('pl' => 'pregraduate_list'),array('idStudentRegistration'));
		$row2 = $db->fetchAll($pregraduate_list);
		
		$selectData = $db->select()
						 ->from(array('sr' => $this->_name))
						 ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))						
						 ->join(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDefaultLanguage','IntakeId'))
						 ->where('sr.profileStatus = ?',92)
						 ->order('sr.IdStudentRegistration')
						 ->order('sp.appl_fname')
						 ->order('sp.appl_lname');
						 //->where('IdStudentRegistration = ?', 32);
						 //->limit(1);
						 
						 if(count($row2)>0){						 	
						 	$selectData->where('sr.IdStudentRegistration NOT IN (?)',$row2);
						 }
						 
						 if(isset($formData) && $formData!=null){						 	
						 	if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
						 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
						 	}
						 	
							 if(isset($formData['student_id']) && $formData['student_id']!=''){
						 		$selectData->where('sr.registrationId = ?',$formData['student_id']);
						 	}
						 	
						 	if(isset($formData['student_name']) && $formData['student_name']!=''){
						 		$selectData->where('( sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
						 		$selectData->orwhere('sp.appl_lname LIKE ? )','%'.$formData['student_name'].'%');
						 	}
						 }
				
		//echo $selectData;		 
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	function getProfile($IdStudentRegistration){
		
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$sql = $db->select()
						->from(array('a' => 'tbl_studentregistration'))
						->joinLeft(array('StudentProfile' => 'student_profile'), 'StudentProfile.id = a.sp_id', array('StudentProfile.*'))
						->joinLeft(array('b' => 'tbl_program'), "a.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme"))
						->joinLeft(array('c' => 'tbl_intake'), "a.IdIntake = c.IdIntake", array("c.IntakeDesc"))
						->joinLeft(array('d' => 'tbl_definationms'), "a.profileStatus = d.idDefinition", array("d.DefinitionDesc"))
						->where("a.IdStudentRegistration =?", $IdStudentRegistration);
			$row = $db->fetchAll($sql);
			return $row;
	}
	
	function getStudentManualAdd($formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$pregraduate_list = $db->select()
							   ->from(array('pl' => 'pregraduate_list'),array('idStudentRegistration'));
		$row2 = $db->fetchAll($pregraduate_list);
		
		$selectData = $db->select()
						 ->from(array('sr' => $this->_name))
						 ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))						
						 ->join(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDefaultLanguage','IntakeId'))
						 ->where('sr.profileStatus = ?',92)						
						 ->order('sr.registrationId')
						 ->limit(300);
						 
						 if(count($row2)>0){						 	
						 	$selectData->where('sr.IdStudentRegistration NOT IN (?)',$row2);
						 }
						 
						 if(isset($formData) && $formData!=null){						 	
						 	if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
						 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
						 	}
						 	
							 if(isset($formData['student_id']) && $formData['student_id']!=''){
						 		$selectData->where('sr.registrationId = ?',$formData['student_id']);
						 	}
						 	
						 	if(isset($formData['student_name']) && $formData['student_name']!=''){
						 		$selectData->where('( sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
						 		$selectData->orwhere('sp.appl_lname LIKE ? )','%'.$formData['student_name'].'%');
						 	}
						 }
				
				 
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
}

?>