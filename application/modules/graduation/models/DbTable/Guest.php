<?php

class Graduation_Model_DbTable_Guest extends Zend_Db_Table
{
    protected $_name = "tbl_graduation_guests";

    function clear($student_id) {
    	$select = 'IdStudentRegistration = '. $student_id;
    				;
    	$this->delete($select);
    	return(true);
    }

    function getguests($student_id) {
    	$select = $this->select()
    				->where('IdStudentRegistration = ?', $student_id)
    				->where('type = ?',1)
    				;
    	$checklists = $this->fetchRow($select);

    	return($checklists);
    }

	function getrefreshment($student_id) {
		$select = $this->select()
			->where('IdStudentRegistration = ?', $student_id)
			->where('type = ?',2)
		;
		$checklists = $this->fetchRow($select);

		return($checklists);
	}

    function save($data) {
    	$this->clear($data['IdStudentRegistration']);

    	$this->insert($data);
    	return(true);
    }

    public function getApplication($formData=null){
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$selectData = $db->select()
    					->from(array('g'=>$this->_name))
    					->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.IdStudentRegistration')
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
						->join(array('itk' =>'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDesc'))	
						->joinLeft(array('im'=>'invoice_main'),'im.id=g.invoice_id',array('bill_amount'))
						->joinLeft(array('cur'=>'tbl_currency'),'im.currency_id=cur.cur_id',array('cur_symbol_prefix'))
						->joinLeft(array('pgl'=>'pregraduate_list'), 'g.IdStudentRegistration = pgl.idStudentRegistration', 'pgl.c_id')
						->joinLeft(array('c'=>'convocation'), 'pgl.c_id=c.c_id', array('c.c_year', 'c.c_date_from', 'c.c_date_to', 'c.c_session'))
						->order('g.applied_date');
    	
						if(isset($formData) && $formData!=null){	
												 	
						 	if(isset($formData['type']) && $formData['type']!=''){
						 		$selectData->where('g.type = ?',$formData['type']);
						 	}
						 	
							if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
						 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
						 	}
						 	
							if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
						 		$selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
						 	}
						 	
							if(isset($formData['studentId']) && $formData['studentId']!=''){
						 		$selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
						 	}

							if(isset($formData['c_id']) && $formData['c_id']!=''){
								$selectData->where('pgl.c_id = ?', $formData['c_id']);
							}
						 	
							if(isset($formData['student_name']) && $formData['student_name']!=''){
						 		$selectData->where('( sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
						 		$selectData->orwhere('sp.appl_lname LIKE ? )','%'.$formData['student_name'].'%');
						 	}
						 	
							if(isset($formData['status']) && $formData['status']!=''){
						 		$selectData->where('g.status = ?',$formData['status']);
						 	}else{
						 		$selectData->where('g.status IN ("APPLIED","APPROVED","REJECTED")');
						 	}
						 						 	
						 }	 
				 
		$row = $db->fetchAll($selectData);
		return $row;			
    }
    
    
	public function updateData($data,$where){
		 $this->update($data, $where);
	}

	
	public function studentInfo($student_id,$type) {
		$db = Zend_Db_Table::getDefaultAdapter();
    	$select =  $db->select()
	    				->from(array('g'=>$this->_name))
	    				->join(array('pl'=>'pregraduate_list'),'pl.idStudentRegistration=g.IdStudentRegistration',array('c_id'))
	    				->where('g.id = ?', $student_id)	    				
	    				->where('g.type = ?',$type);
    				
    	$row = $db->fetchRow($select);

    	return($row);
    }

	public function getListConvoBy(){

		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
			->from(array('c' => 'convocation'));
			//->where('c.c_year >= ?',date('Y'));

		$row = $db->fetchAll($selectData);
		return $row;
	}

	public function getGuestApplication($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>$this->_name))
			->where('a.id = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}
}
