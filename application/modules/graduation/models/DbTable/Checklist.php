<?php 
class Graduation_Model_DbTable_Checklist extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_graduation_checklist';
	protected $_primary = "id";

	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	function save($data) {
		$data = friendly_columns($this->_name, $data);
		//updates

		if(!empty($data[$this->_primary])) {
			$where =  $this->_primary ." = " . $data[$this->_primary];
			$this->update($data, $where);
			$this->id = $data[$this->_primary];

		} else {
			$this->id = $this->insert($data);
		}

		return($this->id);

	}

	function remove($id) {
		$where =  $this->_primary ." = " . $id;
		$this->delete($where);
		return(true);
	}


    
	function getActiveChecklist() {
		$db = Zend_Db_Table::getDefaultAdapter();
		
    	$select = $db->select()
    				->from(array('gc'=>'tbl_graduation_checklist'))
    				//->join(array('gd'=>'general_documents'),'gd.gd_table_id=gc.id')
    				//->where('gd.gd_table_name = ?','tbl_graduation_checklist')
    				->where('status = 1');
    				
    	$checklists = $db->fetchAll($select);
    	
    	foreach($checklists as $index=>$val){
    		$select2 = $db->select()
    					   ->from(array('gd'=>'general_documents'))
    					   ->where('gd.gd_table_name = ?','tbl_graduation_checklist')
    					   ->where('gd.gd_table_id = ?',$val['id']);
    					   $docs = $db->fetchAll($select2);
    					  $checklists[$index]['docs']=$docs;
    					
    	}
    	return($checklists);

    }

}