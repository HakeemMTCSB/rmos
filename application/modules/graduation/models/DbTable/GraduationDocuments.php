<?php 

class Graduation_Model_DbTable_GraduationDocuments extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_graduation_documents';
    protected $_primary = 'gd_id';
   
	public function getDataById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('gd' => $this->_name));
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
	public function getDocumentById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('gd' => $this->_name))
                ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=gd.gd_type',array('type_name'=>'DefinitionDesc'))
                ->where('gd_id = ?',$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    

	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
		return $id =  $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){
		 $this->delete( $this->_primary .' = '. $id );
	}
	
	public function getData()
	{
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
					->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as staff_name')
					->joinLeft(array('f'=>'general_documents'),"f.gd_table_id=a.gd_id AND f.gd_table_name='tbl_graduation_documents'", array('COUNT(f.gd_table_id) as total_files'))
					->group('a.gd_id')
					;
		

		
		return $select;
	}
}
?>