<?php 
class Graduation_Model_DbTable_AwardStudent extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_graduation_award_student';
	protected $_primary = "id";

	/**
	* IdStudentRegistration could be an array of ID or just an int
	* 
	*/
	function tag_student_to_award($IdStudentRegistration, $tbl_graduation_award_id) {

		if(is_array($IdStudentRegistration)) {
			foreach($IdStudentRegistration as $student_id) {
				$data['IdStudentRegistration'] =  $student_id;
				$data['tbl_graduation_award_id'] =  $tbl_graduation_award_id;
				$this->insert($data);
			}
		} else {
			
			$data['IdStudentRegistration'] =  $IdStudentRegistration;
			$data['tbl_graduation_award_id'] =  $tbl_graduation_award_id;
			$this->insert($data);
		}

		return(true);

	}


	/**
	* since this is a simple tagging, lets just clear everything about the student and plunk in again
	*
	*/
	function clear_tagging($IdStudentRegistration) {
		$where = "IdStudentRegistration = " . (int) $IdStudentRegistration;
		$this->delete($where);
		return(true);
	}

	function get_awarded($IdStudentRegistration) {
		$where = $this->select()
			->where('IdStudentRegistration =?', $IdStudentRegistration);
		$awards = $this->fetchAll($where);
		$award_list = array();
		foreach($awards as $award) {
			$award_list[] = $award['tbl_graduation_award_id'] ;
		}

		return($award_list);
	}


}