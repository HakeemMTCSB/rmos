<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/3/2016
 * Time: 11:21 AM
 */
class Graduation_Model_DbTable_AwardReport extends Zend_Db_Table_Abstract {

    public function getAwardListing($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_list'), array('value'=>'*'))
            ->join(array('b'=>'tbl_graduation_award_student'), 'a.idStudentRegistration = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'tbl_graduation_award'), 'b.tbl_graduation_award_id = c.id', array('awardname'=>'c.name'))
            ->joinLeft(array('d'=>'convocation'), 'a.c_id = d.c_id')
            ->join(array('e'=>'tbl_studentregistration'), 'a.idStudentRegistration = e.IdStudentRegistration')
            ->join(array('l'=>'student_profile'), 'e.sp_id = l.id')
            ->joinLeft(array('f'=>'tbl_program'), 'e.IdProgram = f.IdProgram')
            ->joinLeft(array('g'=>'tbl_program_scheme'), 'e.IdProgramScheme = g.IdProgramScheme')
            ->joinLeft(array('h'=>'tbl_definationms'), 'g.mode_of_program = h.IdDefinition', array('mop'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'g.mode_of_study = i.IdDefinition', array('mos'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'g.program_type = j.IdDefinition', array('pt'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'e.profileStatus = k.IdDefinition', array('profileStatusName'=>'k.DefinitionDesc'))
            ->where('a.status = ?', 2);

        if ($search != false){
            if (isset($search['convosession']) && $search['convosession']!=''){
                $select->where('a.c_id = ?', $search['convosession']);
            }
            if (isset($search['program']) && $search['program']!=''){
                $select->where('e.IdProgram = ?', $search['program']);
            }
            if (isset($search['award']) && $search['award']!=''){
                $select->where('c.id = ?', $search['award']);
            }
            if (isset($search['studentid']) && $search['studentid']!=''){
                $select->where('e.registrationid = "%'.$search['studentid'].'%"');
            }
            if (isset($search['studentname']) && $search['studentname']!=''){
                $select->where('CONCAT_WS(" ", l.appl_fname, l.appl_lname) = "%'.$search['studentname'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }
}