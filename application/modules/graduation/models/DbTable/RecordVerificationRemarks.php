<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/9/2015
 * Time: 2:57 PM
 */
class Graduation_Model_DbTable_RecordVerificationRemarks extends Zend_Db_Table_Abstract {

    public function getStudent($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_list'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.idStudentRegistration = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->where('a.status IN (1, 2)');

        if ($search != false){
            if (isset($search['convosession']) && $search['convosession']!=''){
                $select->where('a.c_id = ?', $search['convosession']);
            }
            if (isset($search['program']) && $search['program']!=''){
                $select->where('b.IdProgram = ?', $search['program']);
            }
            if (isset($search['studentid']) && $search['studentid']!=''){
                $select->where('b.registrationid = "%'.$search['studentid'].'%"');
            }
            if (isset($search['studentname']) && $search['studentname']!=''){
                $select->where('concat(c.appl_fname, " ", c.appl_lname) = "%'.$search['studentname'].'%"');
            }
        }
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getGraduationChecklist($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_student_checklist'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_graduation_checklist'), 'a.idChecklist=b.id')
            ->where('a.remarks_message is not null')
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getConvoSession(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'convocation'), array('value'=>'*'))
            ->order('a.c_year DESC')
            ->order('a.c_session DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateChecklist($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('pregraduate_student_checklist', $bind, 'psc_id = '.$id);
        return $update;
    }

    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function awardList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_graduation_award'), array('value'=>'*'));

        $result = $db->fetchAll($select);
        return $result;
    }
}