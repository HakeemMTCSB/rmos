<?php 
class Graduation_Model_DbTable_ConvocationAward extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'convocation_award';
	protected $_primary = "ca_id";

	function getDataByConvo($c_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						 ->from(array('ca' => $this->_name))
						 ->join(array('a'=>'tbl_graduation_award'),'a.id=ca.IdAward')
						 ->where('ca.convocation_id = ?',$c_id);
		$row = $db->fetchAll($selectData);
		return $row;
	}
}

?>