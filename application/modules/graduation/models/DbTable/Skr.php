<?php
class Graduation_Model_DbTable_Skr extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'graduation_skr';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('c'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = c.add_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"));
		
		if($id!=0){
			$selectData->where("c.id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getActiveData($type=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('c'=>$this->_name))
		->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = c.add_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
		->joinLeft(array('sm'=>'tbl_semestermaster'),'c.IdSemesterMain=sm.IdSemesterMaster',array('SemesterMainName'))
		->where('c.status = 1');
		
		if($type){
			$selectData->where('c.type = ?',$type);
		}
			
		$row = $db->fetchAll($selectData);
		
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['add_by'])){
			$data['add_by'] = $auth->getIdentity()->iduser;
		}
		
		$data['add_date'] = date('Y-m-d H:i:s');
			
        return parent::insert($data);
	}		
		

	public function update(array $data,$where){
		
		$auth = Zend_Auth::getInstance();
		$data['add_date'] = $auth->getIdentity()->iduser;
		$data['add_date'] = date('Y-m-d H:i:s');
		
		return parent::update($data, $where);
	}
	
	public function deleteData($id=null){
		if($id!=null){
			$data = array(
				'status' => 0				
			);
				
			$this->update($data, "c_id = '".$id."'");
		}
	}	
}

?>