<?php
class Graduation_Model_DbTable_Clearance extends Zend_Db_Table_Abstract {
	
	public function getKmcData($idStudentRegistration) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('kmc'=>'department_clearance_kmc'))
						->where('kmc.IdStudentRegistration = ?', $idStudentRegistration);
		
		$row = $db->fetchRow($selectData);
		
		return $row;
	}
	
	public function getFinData($idStudentRegistration) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('fin'=>'department_clearance_finance'))
						->where('fin.IdStudentRegistration = ?', $idStudentRegistration);
		
		$row = $db->fetchRow($selectData);
		
		return $row;
	}
	
	public function getIctData($idStudentRegistration) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('ict'=>'department_clearance_ict'))
						->where('ict.IdStudentRegistration = ?', $idStudentRegistration);
		
		$row = $db->fetchRow($selectData);
		
		return $row;
	}
	
	public function getLogisticData($idStudentRegistration) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('log'=>'department_clearance_logistic'))
						->where('log.IdStudentRegistration = ?', $idStudentRegistration);
		
		$row = $db->fetchRow($selectData);
		
		return $row;
	}
		
}

?>