<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 18/3/2016
 * Time: 4:10 PM
 */
class Graduation_Model_DbTable_Certificate extends Zend_Db_Table_Abstract {

    public function getTemplate($programid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'certificate_template'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.crt_program_id = b.IdProgram')
            ->where('a.crt_program_id = ?', $programid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTemplateById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'certificate_template'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.crt_program_id = b.IdProgram')
            ->where('a.crt_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertTemplate($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('certificate_template', $data);
        $id = $db->lastInsertId('certificate_template', 'crt_id');
        return $id;
    }

    public function updateTemplate($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update('certificate_template', $data, 'crt_id = '.$id);
    }

    public function studentList($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_list'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.idStudentRegistration=b.IdStudentRegistration', array('*', 'IdStudentRegistration as studentid'))
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->join(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.profileStatus=e.idDefinition', array('statusname'=>'e.DefinitionDesc'));

        if ($search != false){
            if (isset($search['convosession']) && $search['convosession']!=''){
                $select->where('a.c_id = ?', $search['convosession']);
            }
            if (isset($search['program']) && $search['program']!=''){
                $select->where('b.IdProgram = ?', $search['program']);
            }
            if (isset($search['studentid']) && $search['studentid']!=''){
                $select->where('b.registrationId = "%'.$search['studentid'].'%"');
            }
            if (isset($search['studentname']) && $search['studentname']!=''){
                $select->where('concat(b.appl_fname, " ", b.appl_lname) = "%'.$search['studentname'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function studentInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('b'=>'tbl_studentregistration'), array('*', 'IdStudentRegistration as studentid'))
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->join(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.profileStatus=e.idDefinition', array('statusname'=>'e.DefinitionDesc'))
            ->where('b.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSequence($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'certificate_seq_no'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.csn_program_id = b.IdProgram', array('code'=>'b.ProgramCode'))
            ->where('a.csn_program_id = ?', $programId);

        $result = $db->fetchRow($select);

        if (!$result){
            $data = array(
                'csn_program_id'=>$programId,
                'csn_seq_no'=>1
            );
            $db->insert('certificate_seq_no', $data);

            $result = $this->getSequence($programId);
        }

        return $result;
    }

    public function updateSequence($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('certificate_seq_no', $data, 'csn_id = '.$id);
        return $update;
    }

    public function updateCertNo($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $data, 'IdStudentRegistration = '.$id);
        return $update;
    }

    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function deleteTemplate($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('certificate_template', 'crt_id = '.$id);
        return $delete;
    }

    public function getProgramSeqNumber(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->joinLeft(array('b'=>'certificate_seq_no'), 'a.IdProgram = b.csn_program_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkSeq($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->joinLeft(array('b'=>'certificate_seq_no'), 'a.IdProgram = b.csn_program_id')
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertSequence($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('certificate_seq_no', $data);
        $id = $db->lastInsertId('certificate_seq_no', 'csn_id');
        return $id;
    }
}