<?php

class Graduation_Model_DbTable_Attestation extends Zend_Db_Table
{
    protected $_name = "tbl_graduation_attestation";
    
	 public function getApplication($formData=null){
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$selectData = $db->select()
    					->from(array('g'=>$this->_name))
    					->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.student_id')
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDesc'))
						->order('g.applied_date');
    	
						if(isset($formData) && $formData!=null){	
												 	
						 	if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
						 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
						 	}
						 	
							if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
						 		$selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
						 	}
						 	
							if(isset($formData['studentId']) && $formData['studentId']!=''){
						 		$selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
						 	}
						 	
						 	
							if(isset($formData['student_name']) && $formData['student_name']!=''){
						 		$selectData->where('( sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
						 		$selectData->orwhere('sp.appl_lname LIKE ? )','%'.$formData['student_name'].'%');
						 	}
						 	
							if(isset($formData['status']) && $formData['status']!=''){
						 		$selectData->where('g.status = ?',$formData['status']);
						 	}else{
						 		$selectData->where('g.status IN ("APPLIED","APPROVED","REJECTED")');
						 	}
						 						 	
						 }	 
				 
		$row = $db->fetchAll($selectData);
		return $row;			
    }
    
    
	public function updateData($data,$where){
		 $this->update($data, $where);
	}
}