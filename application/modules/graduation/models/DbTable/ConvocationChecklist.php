<?php
class Graduation_Model_DbTable_ConvocationChecklist extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'convocation_checklist';
	protected $_primary = "cc_id";
		
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function addChecklistData($data){	
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $db->insert('convocation_program_checklist',$data);
	}
	
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cc'=>$this->_name))
					->join(array('p'=>'tbl_program'),'p.IdProgram=cc.idProgram',array('ProgramCode','ProgramName'));
					
		
		if($id!=0){
			$selectData->where("cc_id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	
	public function getProgramByConvo($c_id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cc'=>$this->_name))
					->join(array('p'=>'tbl_program'),'p.IdProgram=cc.idProgram',array('ProgramCode','ProgramName'))
					->where('cc.c_id = ?',$c_id);
		return $row = $db->fetchAll($selectData);
		
	}
	
	public function getChecklistByConvoProgram($cc_id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $selectData = $db->select()
					->from(array('cpc'=>'convocation_program_checklist'))
					->join(array('d'=>'tbl_graduation_checklist'),'d.id=cpc.idChecklist')
					->where('cpc.cc_id = ?',$cc_id);
		return $row = $db->fetchAll($selectData);
		
	}
	
	public function getDataChecklist($cc_id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cc'=>$this->_name))
					->join(array('p'=>'tbl_program'),'p.IdProgram=cc.idProgram',array('ProgramCode','ProgramName'))
					->joinLeft(array('cpc'=>'convocation_program_checklist'),'cpc.cc_id=cc.cc_id')
					->joinLeft(array('d'=>'tbl_graduation_checklist'),'d.id=cpc.idChecklist')
					->where('cc.cc_id = ?',$cc_id);
		return $row = $db->fetchAll($selectData);
		
	}
	
	public function deleteChecklist($where){
		$db = Zend_Db_Table::getDefaultAdapter();		
	    $db->delete('convocation_program_checklist',$where);
	}
	
	
	public function getChecklistByProgram($c_id,$idProgram){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cc'=>$this->_name))
					->join(array('p'=>'tbl_program'),'p.IdProgram=cc.idProgram',array('ProgramCode','ProgramName'))
					->join(array('cpc'=>'convocation_program_checklist'),'cpc.cc_id=cc.cc_id')
					->join(array('d'=>'tbl_definationms'),'d.idDefinition=cpc.idChecklist')
					->where('cc.c_id = ?',$c_id)
					->where('cc.idProgram = ?',$idProgram);
		return $row = $db->fetchAll($selectData);
		
	}
	
	public function deleteChecklistByConvo($where){
		$db = Zend_Db_Table::getDefaultAdapter();		
	    $db->delete('convocation_checklist',$where);
	}
	
}

?>