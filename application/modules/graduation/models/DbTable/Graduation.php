<?php
class Graduation_Model_DbTable_Graduation extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'graduate';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('g'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = g.add_by', array('shortlisted_by'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
					->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname'))
					->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ArabicName'))
					->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage'));
		
		if($id!=0){
			$selectData->where("g.id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getCountofGraduates($program=null,$prog=null,$dean=null,$idCollege=null,$idyear=null) {
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('pgl'=>$this->_name),array('counts'=>'count(distinct sr.idStudentRegistration)'))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration',array())
		->join(array('pr'=>'tbl_program'),'sr.IdProgram=pr.IdProgram',array('IdProgram','ProgramName','ProgramCode'))
		->join(array('cl'=>'tbl_collegemaster'),'pr.IdCollege=cl.IdCollege',array('CollegeName'))
		->joinLeft(array('dean'=>'graduation_skr'),'pgl.dean_approval_skr=dean.id',array('date_of_skr'=>'dean.date_of_skr','dean.skr'))
		->joinLeft(array('skr'=>'graduation_skr'),'pgl.rector_approval_skr=skr.id',array('date_of_skr'=>'skr.date_of_skr','skr.skr'))
		->joinLeft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=skr.IdSemesterMain')
		->order('MID(ProgramCode,2,1)','ProgramCode');
		//filtering
		if ($program!=null) {
			$selectData->where('sr.IdProgram = ?',$program);
			$selectData->group('IdProgram','ProgramCode','ProgramName','CollegeName');
		}
		if ($idCollege!=null) {
			$selectData->where('cl.IdCollege = ?',$idCollege);
			$selectData->group('cl.IdCollege','cl.CollegeName');
		}
		//if ($migrated!=null) $selectData->where('pgl.migrated = ?',$migrated);
		if ($prog!=null) $selectData->where('pgl.dean_approval_date is not null');
		if ($dean!=null) $selectData->where('pgl.rector_approval_date is not null');
		if ($idyear!=null) $selectData->where('sm.idacadyear=?',$idyear);
		
		if ($program!=null) {$row = $db->fetchAll($selectData); }
		else {$row = $db->fetchRow($selectData);}
		//echo var_dump($row);
		//exit;
		return $row;
	
	}
	public function getCountofGraduatesGroupByIntake($program=null,$idCollege=null,$acadyear) {
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('pgl'=>$this->_name),array('counts'=>'count(distinct sr.idStudentRegistration)'))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration',array())
		->join(array('pr'=>'tbl_program'),'sr.IdProgram=pr.IdProgram',array('IdProgram','ProgramName','ProgramCode'))
		->join(array('cl'=>'tbl_collegemaster'),'pr.IdCollege=cl.IdCollege',array('CollegeName'))
		->join(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('IntakeId'=>'left(IntakeId,9)'))
		->join(array('skr'=>'graduation_skr'),'pgl.rector_approval_skr=skr.id',array())
		->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=skr.IdSemesterMain')
		->join(array('ac'=>'tbl_academic_year'),'sm.idacadyear=ac.ay_id')
		->where('pgl.rector_approval_skr is not null')
		->group('left(i.IntakeId,9)');
		//filtering
		if ($program!=null) {
			$selectData->where('sr.IdProgram = ?',$program);
			//$selectData->group('IdProgram','ProgramCode','ProgramName','CollegeName');
		} else
		if ($idCollege!=null) {
			$selectData->where('cl.IdCollege = ?',$idCollege);
			//$selectData->group('cl.IdCollege','cl.CollegeName');
		}
		//if ($migrated!=null) $selectData->where('pgl.migrated = ?',$migrated);
		if ($acadyear!=null) $selectData->where('ac.ay_code <= ?',$acadyear);
		$row = $db->fetchAll($selectData);
		//echo $selectData;
		//exit;
		return $row;
	
	}
	public function getFilteredData(array $filter=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('g'=>$this->_name))
		->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = g.add_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
		->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname'))
		->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ArabicName'))
		->join(array('def'=>'tbl_definationms'),'def.idDefinition=pr.programSalutation',array('gelar'=>'def.BahasaIndonesia'))
		->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage'))
		->joinLeft(array('dean'=>'graduation_skr'),'g.dean_approval_skr=dean.id',array('date_of_Yudisium'=>'dean.date_of_skr','dean_sk'=>'dean.skr'))
		->joinLeft(array('skr'=>'graduation_skr'),'g.rector_approval_skr=skr.id',array('date_of_skr'=>'skr.date_of_skr','skr.skr'))
		->order('g.NoSeri');
		
		if($filter){
			foreach ($filter as $key=>$value){
		
				if($key == 'registrationId'){
					$selectData->where($db->quoteInto($key." like ?", "%".$value."%"));
				}else
				if($key == 'dean_approval'){
					if($value == 1){
						$selectData->where('g.dean_approval_date is not null');
					}else{
						$selectData->where('g.dean_approval_date is null');
					}
				}else
				if($key == 'rector_approval'){
					if($value == 1){
						$selectData->where('g.rector_approval_date is not null');
					}else{
						$selectData->where('g.rector_approval_date is null');
					}
				}else
				if($key == 'date_add_from'){
						$selectData->where($db->quoteInto('g.add_date >= ?', date('Y-m-d', strtotime($value))));
				}else
				if($key == 'date_add_to'){
						$selectData->where($db->quoteInto('g.add_date <= ?', date('Y-m-d', strtotime($value))));
				}else{
					$selectData->where($key." = ".$value);
				}
		
			}
		}
				
			$row = $db->fetchAll($selectData);
		
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function calculateNoWis($program,$semester){
	
		
		$graduates=$this->getGraduatesNoWis($program, $semester);
		$DbProgram = new GeneralSetup_Model_DbTable_Program();
		$prgDetail = $DbProgram->fngetProgramData($program);
		$count=1;
		$DbGrade = new Examination_Model_DbTable_StudentGrade();
		
		foreach ($graduates as $graduate) {
			//echo var_dump($graduate);
			//exit;
			$id =$graduate['idStudentRegistration'];
			$intake = substr($graduate['IntakeId'],2,2);
			$urut = 1000 + $count;
			$strurut= substr($urut,1,3);
			$nowis= $strurut.'.'.$prgDetail['ShortName'].'.'.$intake;
			//sks
			$grade=$DbGrade->getStudentGradeInfo($id);
			//echo var_dump($grade);
			//exit;
			$this->updateGraduate(array('NoSeri'=>$nowis,'CGPA'=>$grade['sg_cgpa'],'TotalCreditHours'=>$grade['sg_cum_credithour'],'LengthOfStudy'=>$grade['Level'],'Predicate'=>$grade['sg_cgpa_status']),$graduate['id']);
			$count++;
		}
		
	
	}
	
	public function getGraduatesNoWis($idprogram=null,$idsemester=null,$idstd=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('g'=>$this->_name))
		->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = g.add_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake','sr.IdProgram','transaction_id','IdLandscape'))
		->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname','appl_address1','appl_address_rt','appl_address_rw','appl_city','appl_kelurahan','appl_postcode','appl_birth_place','appl_dob','appl_phone_hp'))
		->joinLeft(array('mj'=>'tbl_programmajoring'),'mj.IDProgramMajoring=sr.IdProgramMajoring',array('majoring'=>'mj.BahasaDescription','majoring_english'=>'mj.EnglishDescription'))
		->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('programEng'=>'pr.ProgramName','program_name'=>'ArabicName','ProgramCode','Departement','Dept_Bahasa','strata'))
		->joinLeft(array('def'=>'tbl_definationms'),'def.idDefinition=pr.Award',array('program_pendidikan'=>'BahasaIndonesia','program_eng'=>'description'))
		->join(array('cl'=>'tbl_collegemaster'),'cl.IdCollege=pr.IdCollege',array('idCol'=>'pr.IdCollege','CollegeName','CollegeBahasa'=>'ArabicName'))
		->join(array('def1'=>'tbl_definationms'),'def1.idDefinition=pr.programSalutation',array('gelar'=>'BahasaIndonesia','gelarP'=>'DefinitionDesc','gelarEng'=>'Description'))
		->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage','IntakeId'))
		->joinLeft(array('dean'=>'graduation_skr'),'g.dean_approval_skr=dean.id',array('date_of_Yudisium'=>'dean.date_of_skr','dean_sk'=>'dean.skr'))
		->joinLeft(array('skr'=>'graduation_skr'),'g.rector_approval_skr=skr.id',array('date_of_skr'=>'skr.date_of_skr','skr.skr','IdSemesterMain'))
		->order('left(IntakeId,4) DESC')
		->order('itk.period DESC')
		->order('sr.registrationid ASC');
		
		if ($idstd != null)
			$selectData->where('sr.idStudentRegistration=?',$idstd);
		if ($idprogram != null)
			$selectData->where('sr.IdProgram=?',$idprogram);
		if ($idsemester != null)
			$selectData->where('skr.IdSemesterMain=?',$idsemester);
		if ($idstd != null)
			$row = $db->fetchRow($selectData);
		else 
			$row = $db->fetchAll($selectData);
		return $row;
	}
	public function generateNIRL($program,$semester){
	
	
		$graduates=$this->getGraduatesNoWis($program, $semester);
		$DbProgram = new GeneralSetup_Model_DbTable_Program();
		$prgDetail = $DbProgram->fngetProgramData($program);
		$count=$prgDetail['n_ofLast_graduation']+1;
		$DbGrade = new Examination_Model_DbTable_StudentGrade();
		$DbCommon = new App_Model_Common();
		$db = new GeneralSetup_Model_DbTable_Semestermaster();
		foreach ($graduates as $graduate) {
				
			$sems = $db->getData($graduate['IdSemesterMain']);
			$semester=$sems['SemesterCountType'];
			$id =$graduate['idStudentRegistration'];
			$dt =$graduate['date_of_skr'];
			
			$month=$DbCommon->fnCovertMonthToRomawi($dt);
			$timestamp = strtotime($dt);
			$year=date('Y',$timestamp);
			$urut = 10000 + $count;
			$strurut= substr($urut,1,4);
			if (substr($graduate['registrationId'],1)=='0') {
				$nowis= $year.'.'.$prgDetail['ProgramCode'].'.'.$strurut;
				
				if (isset($prgDetail['ShortName']))
					$noseriijasah = $strurut.'/'.$prgDetail['collegeShortcode'].'/'.$prgDetail['strata'].'/'.$prgDetail['ShortName'].'/'.$month.'/'.$year;
				else
					$noseriijasah = $strurut.'/'.$prgDetail['collegeShortcode'].'/'.$prgDetail['strata'].'/'.$month.'/'.$year;
			}
			else 
			{
				$nowis= $year.'.0'.$semester.'.'.$prgDetail['ProgramCode'].'.'.$strurut;
				
				if (isset($prgDetail['ShortName']))
					$noseriijasah = $strurut.'/'.$prgDetail['ShortName'].'/'.$prgDetail['strata'].'/'.$month.'/'.$year;
				else
					$noseriijasah = $strurut.'/'.$prgDetail['strata'].'/'.$month.'/'.$year;
			}
			$this->updateGraduate(array('NoSeriIjazah'=>$noseriijasah,'NIRL'=>$nowis),$graduate['id']);
			$count++;
		}
	
	
	}
	public function getDataToApprove($type=1, array $filter=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('g'=>$this->_name))
		->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = g.add_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
		->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname'))
		->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_id'=>'IdProgram','program_name'=>'ArabicName'))
		->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_id'=>'IdIntake','intake_name'=>'IntakeDefaultLanguage'));
		
		if($type==1){
			$selectData->where('g.dean_approval_date is null');
		}else
		if($type==2){
			$selectData->joinLeft(array('gskr'=>'graduation_skr'), 'gskr.id = g.dean_approval_skr', array('skr'));
			$selectData->where('g.rector_approval_date is null');
		}
		
		if($filter){
			foreach ($filter as $key=>$value){
				
				if($key == 'registrationId'){
					$selectData->where($key." like '%".$value."%'");
				}else{
					$selectData->where($key." = ".$value);
				}
				
			}
		}
		
		/*echo $selectData;
		exit;*/
			
		$row = $db->fetchAll($selectData);
		
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
		
	}
	
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['add_by'])){
			$data['add_by'] = $auth->getIdentity()->iduser;
		}
		
		$data['add_date'] = date('Y-m-d H:i:s');
			
        return parent::insert($data);
	}		
		

	
	
	public function deleteData($id=null){
		if($id!=null){
			$data = array(
				'status' => 0				
			);
				
			$this->update($data, "c_id = '".$id."'");
		}
	}
	
	public function updateData($data,$id=null){
		if($id!=null){
			$data = array(
					'status' => 0
			);
	
			$this->update($data, "c_id = '".$id."'");
		}
	}
	public function updateGraduate($data,$id=null){
		
		if($id!=null){
			$row=$this->update($data, "id = ".$id);
			//echo $row;
			//exit;
		}
		
		return $row;
	}
	public function getConvocationCandidate($nim=null, $program=null, $intake=null, $from=null, $to=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$selectData = $db->select()
						->from(array('g'=>$this->_name))
						->where('NOT EXISTS (SELECT idStudentRegistration FROM convocation_graduate as cg WHERE cg.idStudentRegistration = g.idStudentRegistration)')
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_id'=>'IdProgram','program_name'=>'ArabicName'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_id'=>'IdIntake','intake_name'=>'IntakeDefaultLanguage'))
						->joinLeft(array('gskr'=>'graduation_skr'), 'gskr.id = g.dean_approval_skr', array('skr_dean'=>'skr'))
						->joinLeft(array('gskr2'=>'graduation_skr'), 'gskr2.id = g.rector_approval_skr', array('skr_rector'=>'skr'))
						//->where('g.dean_approval_date IS NOT NULL')
						//->where('g.rector_approval_date IS NOT NULL')
						;
		
		if($nim){
			$selectData->where("sr.registrationId like '%".$nim."%'");
		}
		
		if($program){
			$selectData->where("pr.IdProgram = ?", $program);
		}
					
		if($intake){
			$selectData->where("itk.IdIntake = ?", $intake);
		}
		
		
		if($from){
			$selectData->where('g.add_date >= ?', date('Y-m-d', strtotime($from)));
		}
		
		if($to){
			$selectData->where('g.add_date <= ?', date('Y-m-d', strtotime($to)));
		}
		
		$row = $db->fetchAll($selectData);
		
		return $row;
		
	}
	public function getCountofGraduatesAll($periode=null) {
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('pgl'=>$this->_name),array('counts'=>'count(distinct sr.idStudentRegistration)'))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration',array())
		->join(array('pr'=>'tbl_program'),'sr.IdProgram=pr.IdProgram',array('IdProgram','ProgramName','ProgramCode'))
		->join(array('cl'=>'tbl_collegemaster'),'pr.IdCollege=cl.IdCollege',array('CollegeName'))
		->group('IdProgram','ProgramCode','ProgramName','CollegeName')
		->order('MID(ProgramCode,2,1)','ProgramCode');
		$row = $db->fetchAll($selectData);
		//echo var_dump($row);
		//exit;
		return $row;
	
	}
	public function getCountofGraduatesbyProgram($program,$periode=null) {
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('pgl'=>$this->_name),array('counts'=>'count(distinct sr.idStudentRegistration)'))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration',array())
		->join(array('pr'=>'tbl_program'),'sr.IdProgram=pr.IdProgram',array('IdProgram','ProgramName','ProgramCode'))
		->join(array('cl'=>'tbl_collegemaster'),'pr.IdCollege=cl.IdCollege',array('CollegeName'))
		->where('IdProgram=?',$program)
		->group('IdProgram','ProgramCode','ProgramName','CollegeName')
		->order('MID(ProgramCode,2,1)','ProgramCode');
		$row = $db->fetchRow($selectData);
		//echo var_dump($row);
		//exit;
		return $row;
	
	}
    
    public function getConvocationCandidateList($program=null, $idsemester=null, $from=null, $to=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
            ->from(array('g'=>$this->_name))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
            ->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('id','appl_id','appl_fname', 'appl_mname', 'appl_lname','appl_birth_place','appl_dob', 'appl_address_rw', 'appl_address_rt', 'appl_address1', 'appl_address2', 'appl_kelurahan', 'appl_kecamatan', 'appl_city', 'appl_postcode', 'appl_state'))
            ->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ArabicName','ProgramCode','Departement','Dept_Bahasa'))
            ->joinLeft(array('def'=>'tbl_definationms'),'def.idDefinition=pr.Award',array('program_pendidikan'=>'BahasaIndonesia','program_eng'=>'description'))
            ->join(array('cl'=>'tbl_collegemaster'),'cl.IdCollege=pr.IdCollege',array('CollegeName','CollegeBahasa'=>'ArabicName'))
            ->join(array('def1'=>'tbl_definationms'),'def1.idDefinition=pr.programSalutation',array('gelar'=>'DefinitionCode'))
            ->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage','IntakeId'))
            ->joinLeft(array('dean'=>'graduation_skr'),'g.dean_approval_skr=dean.id',array('date_of_Yudisium'=>'dean.date_of_skr','dean_sk'=>'dean.skr'))
            ->join(array('skr'=>'graduation_skr'),'g.rector_approval_skr=skr.id',array('date_of_skr'=>'skr.date_of_skr','skr.skr','IdSemesterMain'))
           
            ->order('left(IntakeId,4) DESC')
            ->order('sr.registrationid ASC');
        /*
		$selectData = $db->select()
						->from(array('g'=>$this->_name))
						->where('NOT EXISTS (SELECT idStudentRegistration FROM convocation_graduate as cg WHERE cg.idStudentRegistration = g.idStudentRegistration)')
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname','appl_birth_place','appl_dob', 'appl_address_rw', 'appl_address_rt', 'appl_address1', 'appl_address2', 'appl_kelurahan', 'appl_kecamatan', 'appl_city', 'appl_postcode', 'appl_state' ))
						//->join(array('st'=>'tbl_state'), 'st.idState = sp.appl_state')
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_id'=>'IdProgram','program_name'=>'ArabicName'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_id'=>'IdIntake','intake_name'=>'IntakeDefaultLanguage'))
						->join(array('gskr'=>'graduation_skr'), 'gskr.id = g.dean_approval_skr', array('skr_dean'=>'skr'))
						->join(array('gskr2'=>'graduation_skr'), 'gskr2.id = g.rector_approval_skr', array('skr_rector'=>'skr'))
						->where('g.dean_approval_date IS NOT NULL')
						->where('g.rector_approval_date IS NOT NULL');
		
		*/
		if($program){
			$selectData->where("pr.IdProgram = ?", $program);
		}
					
		if($idsemester){
			$selectData->where('skr.IdSemesterMain=?',$idsemester);
            //$selectData->where("itk.IdIntake = ?", $intake);
		}
		
		
		if($from){
			$selectData->where('g.add_date >= ?', date('Y-m-d', strtotime($from)));
		}
		
		if($to){
			$selectData->where('g.add_date <= ?', date('Y-m-d', strtotime($to)));
		}
		
		$row = $db->fetchAll($selectData);
		
		return $row;
		
	}
    
    public function getConvocationCandidateTopList($program=null, $idsemester=null, $cgpa=null, $from=null, $to=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
            ->from(array('g'=>$this->_name))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
            ->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('id',  'appl_id','appl_fname', 'appl_mname', 'appl_lname','appl_birth_place','appl_dob', 'appl_address_rw', 'appl_address_rt', 'appl_address1', 'appl_address2', 'appl_kelurahan', 'appl_kecamatan', 'appl_city', 'appl_postcode', 'appl_state'))
            ->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ArabicName','ProgramCode','Departement','Dept_Bahasa'))
            ->joinLeft(array('def'=>'tbl_definationms'),'def.idDefinition=pr.Award',array('program_pendidikan'=>'BahasaIndonesia','program_eng'=>'description'))
            ->join(array('cl'=>'tbl_collegemaster'),'cl.IdCollege=pr.IdCollege',array('CollegeName','CollegeBahasa'=>'ArabicName'))
            ->join(array('def1'=>'tbl_definationms'),'def1.idDefinition=pr.programSalutation',array('gelar'=>'DefinitionCode'))
            ->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage','IntakeId'))
            ->joinLeft(array('dean'=>'graduation_skr'),'g.dean_approval_skr=dean.id',array('date_of_Yudisium'=>'dean.date_of_skr','dean_sk'=>'dean.skr'))
            ->join(array('skr'=>'graduation_skr'),'g.rector_approval_skr=skr.id',array('date_of_skr'=>'skr.date_of_skr','skr.skr','IdSemesterMain'))
           
            ->order('left(IntakeId,4) DESC')
            ->order('sr.registrationid ASC');
        
        /*
		$selectData = $db->select()
						->from(array('g'=>$this->_name))
						->where('NOT EXISTS (SELECT idStudentRegistration FROM convocation_graduate as cg WHERE cg.idStudentRegistration = g.idStudentRegistration)')
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = g.idStudentRegistration', array('registrationId', 'idIntake'))
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_id','appl_fname', 'appl_mname', 'appl_lname','appl_birth_place','appl_dob', 'appl_address_rw', 'appl_address_rt', 'appl_address1', 'appl_address2', 'appl_kelurahan', 'appl_kecamatan', 'appl_city', 'appl_postcode', 'appl_state' ))
						//->join(array('st'=>'tbl_state'), 'st.idState = sp.appl_state')
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_id'=>'IdProgram','program_name'=>'ArabicName'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_id'=>'IdIntake','intake_name'=>'IntakeDefaultLanguage'))
						->join(array('gskr'=>'graduation_skr'), 'gskr.id = g.dean_approval_skr', array('skr_dean'=>'skr'))
						->join(array('gskr2'=>'graduation_skr'), 'gskr2.id = g.rector_approval_skr', array('skr_rector'=>'skr'))
						->where('g.dean_approval_date IS NOT NULL')
						->where('g.rector_approval_date IS NOT NULL');
		
		*/
		if($program){
			$selectData->where("pr.IdProgram = ?", $program);
		}
					
		if($idsemester){
			$selectData->where('skr.IdSemesterMain=?',$idsemester);
            //$selectData->where("itk.IdIntake = ?", $intake);
		}
		
		if($cgpa) {
            $selectData->where("g.CGPA >= ?", $cgpa);
        }
        
        
		if($from){
			$selectData->where('g.add_date >= ?', date('Y-m-d', strtotime($from)));
		}
		
		if($to){
			$selectData->where('g.add_date <= ?', date('Y-m-d', strtotime($to)));
		}
		
		$row = $db->fetchAll($selectData);
		
		return $row;
		
	}
}

?>