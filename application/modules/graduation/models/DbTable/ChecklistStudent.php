<?php

class Graduation_Model_DbTable_ChecklistStudent extends Zend_Db_Table
{

	protected $_name = "tbl_graduation_checklist_student";

    function answer_checklist($checklist_id, $student_id) {
    	//delete existing

    	
    	$data['IdStudentRegistration'] = $student_id;
    	$data['tbl_graduation_checklist_id'] = $checklist_id;
    	$a = $this->insert($data);

    	return(true);
    }

    function exists($checklist_id, $student_id) {
    	$select = $this->select()
    				->where('tbl_graduation_checklist_id = ?', $checklist_id)
    				->where('IdStudentRegistration = ?', $student_id)
    				;
    	$checklists = $this->fetchAll($select);
    	if(count($checklists) > 0) {
    		return(true);
    	} else {
    		return(false);
    	}

    }

    function clear($student_id) {
    	$select = 'IdStudentRegistration = '. $student_id;
    				;
    	$this->delete($select);
    	return(true);
    }

    function getanswers($student_id) {
    	$select = $this->select()
    				->where('IdStudentRegistration = ?', $student_id)
    				;
    	$checklists = $this->fetchAll($select);

    	return($checklists);
    }

}