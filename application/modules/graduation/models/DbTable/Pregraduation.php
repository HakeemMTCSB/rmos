<?php
class Graduation_Model_DbTable_Pregraduation extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'pregraduate_list';
	protected $_primary = "id";
		
	public function getInfo($idStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
						->from(array('pgl'=>$this->_name))
						->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=pgl.idStudentRegistration')
						->join(array('p' => 'tbl_program'), "p.IdProgram = sr.IdProgram", array("IdScheme"))
						->where('pgl.idStudentRegistration = ?',$idStudentRegistration);
						
		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	
	public function getGraduateInfo($idStudentRegistration) {
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('pgl'=>$this->_name))
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.IdStudentRegistration', array('registrationId', 'IdIntake', 'IdProgram'))
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
						->where('pgl.idStudentRegistration = ?', $idStudentRegistration);
		
		$row = $db->fetchRow($selectData);
		
		return $row;
	}
	
	public function getData($formData=0, $search=false){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('pgl'=>$this->_name))
						->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pgl.shortlisted_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
						->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = pgl.approve_by', array('add_by_name2'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
						->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = pgl.graduate_by', array('add_by_name3'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
						->joinLeft(array('c'=>'convocation'), 'pgl.c_id=c.c_id', array('c.c_year', 'c.c_date_from', 'c.c_date_to', 'c.c_session'))
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration')
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage'))
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
		
					if(isset($formData) && $formData!=null){	
											 	
					 	if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
					 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
					 	}
					 	
						if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
					 		$selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
					 	}
					 	
						if(isset($formData['studentId']) && $formData['studentId']!=''){
					 		$selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
					 	}
					 	
						if(isset($formData['student_name']) && $formData['student_name']!=''){
					 		$selectData->where('(sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
					 		$selectData->orwhere('sp.appl_lname LIKE ?)','%'.$formData['student_name'].'%');
					 	}
					 	
						if(isset($formData['c_id']) && $formData['c_id']!=''){
					 		$selectData->where('pgl.c_id = ?',$formData['c_id']);
					 	}

						if (isset($formData['Status']) && $formData['Status']!='') {
							$selectData->where('pgl.status = ?', $formData['Status']);
						}
					 	
					 }else{
					 	$selectData->where('pgl.status = 0'); //Default
					 }

		if ($search != false){
			if (isset($search['program']) && $search['program']!=''){
				$selectData->where('sr.IdProgram = ?', $search['program']);
			}
		}

		//echo $selectData;
		$row = $db->fetchAll($selectData);
		return $row;			
		
	}
	
	
	public function getApprovedGraduate($formData=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('pgl'=>$this->_name))
						->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pgl.shortlisted_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
						->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = pgl.approve_by', array('add_by_name2'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
						->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = pgl.graduate_by', array('add_by_name3'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration')
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDesc'))
						//->where('pgl.status = ?',1)
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
		
						if(isset($formData) && $formData!=null){	
												 	
						 	if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
						 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
						 	}
						 	
							if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
						 		$selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
						 	}
						 	
							if(isset($formData['studentId']) && $formData['studentId']!=''){
						 		$selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
						 	}
						 	
							if(isset($formData['profileStatus']) && $formData['profileStatus']!=''){
						 		$selectData->where('sr.profileStatus= ?',$formData['profileStatus']);
						 	}
						 	
							if(isset($formData['profileStatusArray']) && $formData['profileStatusArray']!=''){
						 		$selectData->where('sr.profileStatus IN ( ? )',$formData['profileStatusArray']);
						 	}
						 	
							if(isset($formData['student_name']) && $formData['student_name']!=''){
						 		$selectData->where('( sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
						 		$selectData->orwhere('sp.appl_lname LIKE ? )','%'.$formData['student_name'].'%');
						 	}
						 	
							if(isset($formData['status']) && $formData['status']!=''){
						 		$selectData->where('pgl.status = ?',$formData['status']);
						 	}else{
						 		$selectData->where('pgl.status IN (1,2)');
						 	}
						 						 	
						 }	 
		//echo $selectData;			 
		$row = $db->fetchAll($selectData);
		return $row;			
		
	}
	
	
	public function getClearanceGraduate($formData=0,$type=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('pgl'=>$this->_name))
						->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pgl.shortlisted_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
						->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = pgl.approve_by', array('add_by_name2'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
						->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = pgl.graduate_by', array('add_by_name3'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration')
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
						->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage'))						
						//->where('pgl.status = ?',1)
						->order('sp.appl_fname')
						->order('sp.appl_lname')
						->order('sr.registrationId');
						
						if(isset($type) && $type!=''){
							if($type=='kmc'){
								$selectData->joinLeft(array('dc'=>'department_clearance_kmc'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'kmc_status'));
							}else 
							if($type=='fin'){
								$selectData->joinLeft(array('dc'=>'department_clearance_finance'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'dcf_status','dcf_createddt'))
								 		   ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dc.dcf_createdby',array('fin_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
							}else
							if($type=='ict'){
								$selectData->joinLeft(array('dc'=>'department_clearance_ict'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'ict_status','ict_createddt'))
										   ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dc.ict_createdby',array('ict_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
							}
							if($type=='logistic'){
								$selectData->joinLeft(array('dc'=>'department_clearance_logistic'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'log_status','log_createddt'))
										   ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dc.log_createdby',array('log_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
							}
						}else{
							$selectData->joinLeft(array('dcs'=>'department_clearance_summary'),'dcs.IdStudentRegistration=pgl.idStudentRegistration')
									   ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dcs.dcs_createdby',array('summary_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
							
							if(isset($formData['status']) && $formData['status']!='All'){
								if($formData['status']==1){
						 			$selectData->where('dcs.dcs_status = ?',1);
								}
								if($formData['status']==0){
						 			$selectData->where('dcs.dcs_status IS NULL');
								}
						 	}
						}
		
						if(isset($formData) && $formData!=null){	
												 	
						 	if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
						 		$selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
						 	}
						 	
							if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
						 		$selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
						 	}
						 							
						 	
							if(isset($formData['studentId']) && $formData['studentId']!=''){
						 		$selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
						 	}

							if(isset($formData['convosession']) && $formData['convosession']!=''){
								$selectData->where('pgl.c_id = ?', $formData['convosession']);
							}
						}
		//echo $selectData;
		$row = $db->fetchAll($selectData);
		return $row;			
		
	}
	
 	public function getTemplate($tpl_id,$locale='en_US'){ 
		$db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ct'=>'comm_template'))
            ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id=ct.tpl_id')
            ->where('ct.tpl_id  = ?',$tpl_id)
            ->where('ctc.locale = ?',$locale);

        $row = $db->fetchRow($select);	
        return $row;           
    }
	
	public function getTemplateTag($tplId){
		$db = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $db->select()
            ->from(array("a"=>"comm_template_tags"),array("value"=>"a.*"))
            ->where('a.tpl_id = '.$tplId.' or a.tpl_id = 0')
            ->order("a.tpl_id");
        $larrResult = $db->fetchAll($lstrSelect);
        return $larrResult;
    }
    
    
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateStatus($data,$idStudentRegistration){
		 $this->update($data, 'idStudentRegistration = '. (int)$idStudentRegistration);
	}
	
	
	public function getConvoInfo($idStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
						->from(array('pgl'=>$this->_name))
						->join(array('c' => 'convocation'),'c.c_id=pgl.c_id')
						->where('pgl.idStudentRegistration = ?',$idStudentRegistration);
						
		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	
}

?>