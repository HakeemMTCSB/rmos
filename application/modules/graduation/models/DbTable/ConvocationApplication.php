<?php
/**
 * @author Muhamad Alif
 * @date Jun 11, 2014
 */

class Graduation_Model_DbTable_ConvocationApplication extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'convocation_application';
	protected $_primary = "id";

	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('ca'=>$this->_name));

		if($id!=0){
			$selectData->where("ca.id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
		}else{
				
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;
		}

	}
	
	public function getApplication($status=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						->from(array('ca'=>$this->_name))
						->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = ca.IdStudentRegistration', array('registrationId', 'idIntake'))
						->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname'))
						->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName'))
						->join(array('c'=>'convocation'), 'c.c_id = ca.convocation_id')
						->join(array('ay'=>'tbl_semestermaster'), 'ay.IdSemesterMaster = c.IdSemesterMaster', array('SemesterMainName'));
		
		if($status){
			$selectData->where('ca.status = ?', $status);
		}else{
			$selectData->where('ca.status is null');
		}
		
		$row = $db->fetchAll($selectData);
		
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getApplicationHistory(){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('ca'=>$this->_name))
		->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = ca.IdStudentRegistration', array('registrationId', 'idIntake'))
		->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id', array('appl_fname', 'appl_mname', 'appl_lname'))
		->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName'))
		->join(array('c'=>'convocation'), 'c.c_id = ca.convocation_id', array('IdSemesterMaster','c_semester_count_type','c_date_from', 'c_date_to', 'c_session', 'c_capacity'))
		->join(array('ay'=>'tbl_semestermaster'), 'ay.IdSemesterMaster = c.IdSemesterMaster', array('SemesterMainName'))
		->where('ca.status is not null');
	
	
		$row = $db->fetchAll($selectData);
	
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['apply_date'])){
			$data['apply_date'] = date('Y-m-d H:i:s');
		}
		
		return parent::insert($data);
	}
}
?>