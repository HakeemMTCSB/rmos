<?php
class Graduation_Model_DbTable_Convocation extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'convocation';
	protected $_primary = "c_id";
		
	public function getData($id=0,$formData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('c'=>$this->_name));
					//->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = c.last_edit_by', array('last_edit_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					//->join(array('ay'=>'tbl_semestermaster'), 'ay.IdSemesterMaster = c.IdSemesterMaster', array('SemesterMainName'))
					
		
		if($id!=0){
			$selectData->where("c.c_id = '".$id."'");			
			$row = $db->fetchRow($selectData);
		}else{
			
			if(isset($formData) && $formData!=null){
				if(isset($formData['c_year']) && $formData['c_year']!=''){
					$selectData->where('c.c_year = ?',$formData['c_year']);
				}
			}
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getConvocationData() {
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
			->from(array('c' => $this->_name))
			//->join(array('ay' => 'tbl_semestermaster'), 'ay.IdSemesterMaster = c.IdSemesterMaster', array('SemesterMainName'))
			->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = c.last_edit_by', array('last_edit_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
			->where('c.status = 1');	
			
		$row = $db->fetchAll($selectData);
		
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['c_last_edit_by'])){
			$data['last_edit_by'] = $auth->getIdentity()->iduser;
		}
		
		$data['last_edit_date'] = date('Y-m-d H:i:s');
			
        return parent::insert($data);
	}		
		

	public function update(array $data,$where){
		
		$auth = Zend_Auth::getInstance();
		$data['last_edit_by'] = $auth->getIdentity()->iduser;
		$data['last_edit_date'] = date('Y-m-d H:i:s');
		
		return parent::update($data, $where);
	}
	
	public function deleteData($id=null){
		if($id!=null){
			$data = array(
				'status' => 0				
			);
				
			$this->update($data, "c_id = '".$id."'");
		}
	}	
	
	
	
	public function getTaggingAward($c_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						 ->from(array('ca' => 'convocation_award'),array())
						 ->join(array('ga'=>'tbl_graduation_award'),'ga.id=ca.IdAward')
						 ->where('ca.convocation_id = ?',$c_id);
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	public function deleteAwardTag($c_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$where = "convocation_id = ".(int)$c_id;
		$db->delete('convocation_award',$where);
	}
	
	
	public function deleteConvocation($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getListConvoByYear(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						 ->from(array('c' => $this->_name))
						 ->where('c.c_year >= ?',date('Y'));
						 
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	
	public function checkDuplicate($c_year,$c_session=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						 ->from(array('c' => $this->_name))
						 ->where('c.c_year >= ?',$c_year)
						 ->where('c.c_session >= ?',$c_session);
						 
		$row = $db->fetchRow($selectData);
		return $row;
	}

	public function getProgramList(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_program'), array('value'=>'*'));

		$result = $db->fetchAll($select);
		return $result;
	}
	
	public function getConvocationList(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						 ->from(array('c' => $this->_name))
						 ->order('c_date_from desc');
						 
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	
}

?>