<?php 

class Graduation_Model_DbTable_GeneralDocuments extends Zend_Db_Table_Abstract {

	protected $_name = 'general_documents';
    protected $_primary = 'gd_id';
   
	public function getDataById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sd' => $this->_name));
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
	public function getDocumentById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sd' => $this->_name))
                ->where('gd_id = ?',$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    

	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
		return $id =  $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){
		 $this->delete( $this->_primary .' = '. $id );
	}
	
	public function getmyDocuments($tbl_name,$tbl_id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('gd' => $this->_name))
                ->where('gd.gd_table_name = ?',$tbl_name)
                ->where('gd.gd_table_id = ?',$tbl_id);
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
	public function getDocumentType() {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('d' => 'tbl_definationms'))
                ->where('d.idDefType = ?',184);
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
	
}
?>