<?php
class Graduation_Model_DbTable_PregraduateStudentChecklist extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'pregraduate_student_checklist';
	protected $_primary = "psc_id";
		
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getStudentChecklist($idStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
						->from(array('psc'=>$this->_name))
						->join(array('gc'=>'tbl_graduation_checklist'),'gc.id=psc.idChecklist',array('checklist_name'=>'name'))
						->where('psc.IdStudentRegistration = ?',$idStudentRegistration);
						
		$row = $db->fetchAll($selectData);
		return $row;
	}
}

?>