<?php 
class Graduation_Model_DbTable_Award extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_graduation_award';
	protected $_primary = "id";

	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	
	function getData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						 ->from(array('ga' => $this->_name))
						 ->where('status = 1');
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	function save($data) {
		$data = friendly_columns($this->_name, $data);
		//updates

		if(!empty($data[$this->_primary])) {
			$where =  $this->_primary ." = " . $data[$this->_primary];
			$this->update($data, $where);
			$this->id = $data[$this->_primary];

		} else {
			$this->id = $this->insert($data);
		}

		return($this->id);

	}

	function remove($id) {
		$where =  $this->_primary ." = " . $id;
		$this->delete($where);
		return(true);
	}
	
	
	function checkDuplicate($code){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('a' => $this->_name))               
                ->where('a.code = ?',$code);
             
        $result = $db->fetchRow($sql);        
        return $result;
	}
	
	
	function getAwardProgram($award_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('gap' => 'tbl_graduation_award_program')) 
                ->where('gap_award_id = ?',$award_id);
             
        $result = $db->fetchAll($sql);  
        return $result;
        	
	}
	
	function checkDuplicateTagging($program_id,$award_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('gap' => 'tbl_graduation_award_program'))               
                ->where('gap_program_id = ?',$program_id)
                ->where('gap_award_id = ?',$award_id);
             
        $result = $db->fetchRow($sql);  
        if($result)      
        	return $result;
        else
        	return null;
        	
	}

}