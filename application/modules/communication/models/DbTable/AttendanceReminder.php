<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 7/3/2016
 * Time: 4:12 PM
 */
class Communication_Model_DbTable_AttendanceReminder extends Zend_Db_Table {

    public function getTemplate($id, $locale = 'en_US'){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'comm_template'), array('value'=>'*'))
            ->join(array('b'=>'comm_template_content'), 'a.tpl_id = b.tpl_id')
            ->where('a.tpl_id = ?', $id)
            ->where('b.locale = ?', $locale);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->join(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->join(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram')
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getGroupInfo($subject, $semester, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_course_tagging_group'), 'a.IdCourseTaggingGroup = b.IdCourseTaggingGroup')
            ->join(array('c'=>'tbl_subjectmaster'), 'a.IdSubject = c.IdSubject')
            ->join(array('d'=>'tbl_semestermaster'), 'a.IdSemesterMain = d.IdSemesterMaster')
            ->where('a.IdSubject = ?', $subject)
            ->where('a.IdSemesterMain = ?', $semester)
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getRefSeq($year, $type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'attendance_reminder_seq'), array('value'=>'*'))
            ->where('ars_year = ?', $year)
            ->where('ars_type = ?', $type);

        $result = $db->fetchRow($select);

        if (!$result){
            $data = array(
                'ars_type'=>$type,
                'ars_year'=>$year,
                'ars_seq'=>1
            );

            $db->insert('attendance_reminder_seq', $data);

            $result = $this->getRefSeq($year, $type);
        }else{
            $data = array(
                'ars_seq'=>$result['ars_seq']+1
            );
            $db->update('attendance_reminder_seq', $data, 'ars_id = '.$result['ars_id']);
        }

        return $result;
    }

    public function insertAttRem($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('attendance_reminder', $data);
        $id = $db->lastInsertId('attendance_reminder', 'atr_id');
        return $id;
    }

    public function insertAttAttch($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('attendance_reminder_attahcment', $data);
        $id = $db->lastInsertId('attendance_reminder_attahcment', 'ara_id');
        return $id;
    }

    public function getExamDate($semester, $type=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_activity_calender'), array('value'=>'*'))
            ->where('a.IdSemesterMain = ?', $semester)
            ->where('a.IdActivity = ?', 42);

        if ($type==0) {
            $select->order('a.StartDate ASC');
        }else{
            $select->order('a.EndDate DESC');
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getHistory($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'attendance_reminder'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.atr_student_id = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->joinLeft(array('e'=>'tbl_course_tagging_group'), 'a.atr_group_id = e.IdCourseTaggingGroup')
            ->joinLeft(array('f'=>'tbl_subjectmaster'), 'a.atr_subject_id = f.IdSubject')
            ->joinLeft(array('g'=>'tbl_semestermaster'), 'a.atr_semester_id = g.IdSemesterMaster')
            ->joinLeft(array('h'=>'tbl_user'), 'a.atr_send_by = h.iduser', array('h.loginName'));

        if ($search != false){
            if (isset($search['IdSemester']) && $search['IdSemester'] != ''){
                $select->where('a.atr_semester_id = ?', $search['IdSemester']);
            }

            if (isset($search['IdProgram']) && $search['IdProgram'] != ''){
                $select->where('b.IdProgram = ?', $search['IdProgram']);
            }

            if (isset($search['IdSubject']) && $search['IdSubject'] != ''){
                $select->where('a.atr_subject_id = ?', $search['IdSubject']);
            }

            if (isset($search['att_type']) && $search['att_type'] != ''){
                $select->where('a.atr_type = ?', $search['att_type']);
            }

            if (isset($search['IdGroup']) && $search['IdGroup'] != ''){
                $select->where('a.atr_group_id = ?', $search['IdGroup']);
            }
        }
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function viewHistory($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'attendance_reminder'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.atr_student_id = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->joinLeft(array('e'=>'tbl_course_tagging_group'), 'a.atr_group_id = e.IdCourseTaggingGroup')
            ->joinLeft(array('f'=>'tbl_subjectmaster'), 'a.atr_subject_id = f.IdSubject')
            ->joinLeft(array('g'=>'tbl_semestermaster'), 'a.atr_semester_id = g.IdSemesterMaster')
            ->joinLeft(array('h'=>'tbl_user'), 'a.atr_send_by = h.iduser', array('h.loginName'))
            ->where('a.atr_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function viewAttachment($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'attendance_reminder_attahcment'), array('value'=>'*'))
            ->where('a.ara_atr_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getMonth($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
            ->from(array('a'=>'tbl_activity_calender'))
            ->where('a.IdSemesterMain = ?', $id)
            ->where('a.IdActivity = ?', 39);

        $semesterBegin = $db->fetchRow($select1);

        $select2 = $db->select()
            ->from(array('a'=>'tbl_activity_calender'))
            ->where('a.IdSemesterMain = ?', $id)
            ->where('a.IdActivity = ?', 41);

        $classEnd = $db->fetchRow($select2);

        if ($select1 && $select2) {

            $start = (new DateTime($semesterBegin['StartDate']))->modify('first day of this month');
            $end = (new DateTime($classEnd['EndDate']))->modify('last day of this month');
            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($start, $interval, $end);

            $monthList = array();
            $i = 0;
            foreach ($period as $dt) {
                $monthList[$i]['id'] = $dt->format("Y-m-d");
                $monthList[$i]['value'] = $dt->format("F");
                $i++;
            }
        }else{
            $monthList = false;
        }

        return $monthList;
    }

    public function barExam($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregsubjects', $data, 'IdStudentRegSubjects = '.$id);
        return $update;
    }

    public function getExamBarList($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_course_tagging_group'), 'a.IdCourseTaggingGroup = b.IdCourseTaggingGroup')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.IdSubject = c.IdSubject')
            ->joinLeft(array('d'=>'tbl_semestermaster'), 'a.IdSemesterMain = d.IdSemesterMaster')
            ->joinLeft(array('e'=>'tbl_studentregistration'), 'a.IdStudentRegistration = e.IdStudentRegistration')
            ->joinLeft(array('f'=>'student_profile'), 'e.sp_id = f.id')
            ->joinLeft(array('g'=>'tbl_program'), 'e.IdProgram = g.IdProgram')
            ->where('a.exam_bar = ?', 0);

        if ($search != false){
            if (isset($search['IdSemester']) && $search['IdSemester']!=''){
                $select->where('a.IdSemesterMain = ?', $search['IdSemester']);
            }
            if (isset($search['IdProgram']) && $search['IdProgram']!=''){
                $select->where('e.IdProgram = ?', $search['IdProgram']);
            }
            if (isset($search['IdSubject']) && $search['IdSubject']!=''){
                $select->where('a.IdSubject = ?', $search['IdSubject']);
            }
            if (isset($search['IdGroup']) && $search['IdGroup']!=''){
                $select->where('a.IdCourseTaggingGroup = ?', $search['IdGroup']);
            }
            if (isset($search['studentid']) && $search['studentid']!=''){
                $select->where('e.registrationId like "%'.$search['studentid'].'%"');
            }
            if (isset($search['studentname']) && $search['studentname']!=''){
                $select->where('CONCAT_WS(" ", f.appl_fname, f.appl_lname) like "%'.$search['studentname'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }
}