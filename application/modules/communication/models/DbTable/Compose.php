<?php
class Communication_Model_DbTable_Compose extends Zend_Db_Table {
	protected $_name = 'comm_compose';
	protected $_rec = 'comm_compose_recipients';
	protected $_attach = 'comm_compose_attachment';
	
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function addAttachment($data)
	{
		$db = $this->db;
		
		$db->insert($this->_attach, $data );
		
		$attach_id = $db->lastInsertId();

		return $attach_id;
	} 

	public function addCompose($data)
	{
		$db = $this->db;
		
		$db->insert($this->_name, $data );
		
		$tpl_id = $db->lastInsertId();

		return $tpl_id;
	} 

	public function addComposeRecipient($data)
	{
		$db = $this->db;
		
		$db->insert($this->_rec, $data );

		return $db->lastInsertId();
	} 

	public function getHistoryData($module = null)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
	                ->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('created_by_name'=>'Fullname'))
					->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=a.comp_type', array('comp_type_name' => 'd.DefinitionDesc'))
					->order('a.comp_id DESC');

		if ( $module != '' )
		{
			$select->where('comp_module = ?',$module);
		}
		
		return $select;
	}

	public function getHistory($id)
	{
		$db = $this->db;
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=a.comp_type', array('comp_type_name' => 'd.DefinitionDesc'))
					->where('a.comp_id = ?', $id);
		
		$result = $db->fetchRow($select);
		return $result;
	}

	public function getHistoryRecipients($id,$type='applicants')
	{
		$db = $this->db;
		
		if ( $type == 'applicants' )
		{
			$select = $db->select()
						->from(array('a'=>$this->_rec),array('a.cr_status','a.cr_id'))
						->joinLeft(array('b' => 'applicant_transaction'),'b.at_trans_id=a.cr_rec_id',array('b.at_trans_id', 'b.at_pes_id' ))
						->joinleft(array('ap'=>'applicant_profile'),'ap.appl_id=b.at_appl_id',array("CONCAT_WS(' ',appl_fname,appl_mname,appl_lname) as app_name","ap.appl_email","ap.appl_phone_mobile"))
						->where('a.cr_comp_id = ?', $id);
		}
		else
		{
			$select = $db->select()
						->from(array('a'=>$this->_rec),array('a.cr_status','a.cr_id'))
						->joinLeft(array('b' => 'tbl_studentregistration'),'a.cr_rec_id=b.IdStudentRegistration',array('b.IdStudentRegistration as at_trans_id', 'b.registrationId as at_pes_id' ))
						->joinleft(array('ap'=>'student_profile'),'ap.id=b.sp_id',array("CONCAT_WS(' ',appl_fname,appl_mname,appl_lname) as app_name","ap.appl_email","ap.appl_phone_mobile"))
						->where('a.cr_comp_id = ?', $id);
		}
	
		$results = $db->fetchAll($select);
		return $results;
	}

	public function getHistoryRecipient($id)
	{
		$db = $this->db;
		$select = $db->select()
	                ->from(array('a'=>$this->_rec),array('a.cr_status','a.cr_id','a.cr_content'))
					->joinLeft(array('b' => 'applicant_transaction'),'b.at_trans_id=a.cr_rec_id',array('b.at_trans_id', 'b.at_pes_id' ))
					->joinleft(array('ap'=>'applicant_profile'),'ap.appl_id=b.at_appl_id',array("CONCAT_WS(' ',appl_fname,appl_mname,appl_lname) as app_name","ap.appl_email","ap.appl_phone_mobile"))
					->joinLeft(array('c' => $this->_name), 'c.comp_id=cr_comp_id', array('c.comp_subject'))
					->where('a.cr_id = ?', $id);
	
		$results = $db->fetchRow($select);
		return $results;
	}
        
        public function getAttachmentInfoForSchemeChange($appId){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $lstrSelect = $lobjDbAdpt->select()
                ->from(array("a"=>"applicant_changescheme_history"),array("value"=>"a.*"))
                ->where('a.ach_trans_id = ?', $appId)
                ->order("a.ach_Id");
            $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
            return $larrResult;
        }
        
        public function resendEmail($data, $crId){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $update = $lobjDbAdpt->update($this->_rec, $data, 'cr_id = '.$crId);
            return $update;
        }
        
        public function viewEmail($crId){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $lstrSelect = $lobjDbAdpt->select()
                ->from(array("a"=>$this->_rec),array("value"=>"a.*"))
                ->join(array('b' => 'comm_compose'),'b.comp_id=a.cr_comp_id')
                ->where('a.cr_id = ?', $crId);
            $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
            return $larrResult;
        }
}