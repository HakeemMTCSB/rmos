<?php
class Communication_Model_DbTable_Groups extends Zend_Db_Table {
	protected $_name = 'comm_groups';
	protected $_rec = 'comm_groups_recipients';
	
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function getPaginateData($module=null)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('ebt'=>$this->_name))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ebt.created_by', array())
	                ->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('created_by_name'=>'Fullname'));
		
		if ( $module != '' )
		{
			$select->where('module = ?',$module);
		}
		
		
		return $select;
	}
	
	public function addGroup($data)
	{
		$db = $this->db;
		
		$db->insert($this->_name, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}

	public function getGroup($id)
	{
		$db = $this->db;

		$select = $db->select()->from($this->_name)->where('group_id = ?', $id);
		
		$results = $db->fetchRow($select);

		return $results;
	}

	public function updateGroup($data, $where)
	{
		$db = $this->db;
		
		$db->update($this->_name, $data, $where );
	}


	public function fetchRecipientsIds($id)
	{
		$results = $this->fetchRecipients($id);
		
		$ids = array();
		foreach ( $results as $row )
		{
			$ids[] = $row['recipient_id'];
		}

		return $ids;
	}
	
	//applicants
	public function fetchRecipientsByIds($ids, $type='applicants')
	{
		$db = $this->db;
	
		if ( $type == 'applicants' )
		{
			$select = $db->select()->from(array('a' => 'applicant_transaction'),array('a.at_trans_id', 'a.at_pes_id' ))
									->joinleft(array('ap'=>'applicant_profile'),'ap.appl_id=a.at_appl_id',array("CONCAT_WS(' ',appl_fname,appl_mname,appl_lname) as app_name","ap.appl_email","ap.appl_phone_mobile"))
									->where('a.at_trans_id IN ('.implode(',',$ids).')');
		}
		else
		{
			$select = $db->select()->from(array('a' => 'tbl_studentregistration'),array('a.IdStudentRegistration as at_trans_id', 'a.registrationId as at_pes_id'))
									->joinleft(array('ap'=>'student_profile'),'ap.id=a.sp_id',array("CONCAT_WS(' ',appl_fname,appl_mname,appl_lname) as app_name","ap.appl_email","ap.appl_phone_mobile", "appl_email_personal"))
									->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram')
									->where('a.IdStudentRegistration IN ('.implode(',',$ids).')');
		}

	
		$results = $db->fetchAll($select);

		return $results;
	}



	public function fetchRecipients($id, $join=0, $type='applicants')
	{
		$db = $this->db;

		$select = $db->select()->from(array('a' => $this->_rec))->where('a.group_id = ?', $id);
		if ( $join )
		{	
			if ( $type == 'applicants' )
			{
				$select->joinLeft(array('b' => 'applicant_transaction'), 'b.at_trans_id=a.recipient_id', array('b.at_pes_id'));
			}
			else
			{
				$select->joinLeft(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration=a.recipient_id', array('b.registrationId as at_pes_id'));
			}
		}

		$results = $db->fetchAll($select);

		return $results;
	}

	public function addRecipient($data)
	{
		$db = $this->db;
		
		$db->insert($this->_rec, $data );
	}

	public function recountRecipients($id, $user_id)
	{
		$db = $this->db;

		$select = $db->select()->from($this->_rec)->where('group_id = ?', $id);
		
		$count = $db->query($select)->rowCount();

		$db->update($this->_name, array('total_recipients' => $count, 'updated_date' => new Zend_Db_Expr('NOW()'), 'updated_by' => $user_id), 'group_id = '.$id );
	}

	public function deleteRecipient($data)
	{
		$db = $this->db;
		$db->delete($this->_rec, $data);
	}
        
	/*
	 * get reciepent for auto group
	 * 
	 * @author izham 
	 * @on 18/7/2014
	 */
	public function getAutoReciepant($status)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>"applicant_transaction"),array("value"=>"a.*"))
			->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
			->where('a.at_status = ?', $status)
			->order("a.at_trans_id");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	/*
	 * get incomplete checklist
	 * 
	 * @author izham 
	 * @on 18/7/2014
	 */
	public function getIncompleteChecklist($txnId, $status){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>"applicant_document_status"),array("value"=>"a.*"))
			->join(array('b'=>'tbl_documentchecklist_dcl'), 'a.ads_dcl_id = b.dcl_Id')
			->join(array('c'=>'tbl_application_section'), 'a.ads_section_id = c.id')
			->where('a.ads_status = ?', $status)
			->where('a.ads_txn_id = ?', $txnId)
			->order("a.ads_id");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	static function getChecklistDesc($table, $column, $where = null, $tableJoin, $columnJoin1, $columnJoin2){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>$table),array("value"=>"a.".$column));

			if ($tableJoin!='0' && $columnJoin1!='0' && $columnJoin2!='0'){
				$lstrSelect->joinLeft(array("b"=>$tableJoin), "a.".$columnJoin1." = b.".$columnJoin2);
			}

			if ($where!=null){
				$lstrSelect->where($where);
			}

		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function getStudentProgram($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_studentregistration'))
			->join(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram')
			->where('a.IdStudentRegistration = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function getRegisteredCourse($id, $sem){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_studentregsubjects'))
			->join(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
			->where('a.IdStudentRegistration = ?', $id)
			->where('a.IdSemesterMain = ?', $sem);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function getRegisteredClass($id, $sem){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_studentregsubjects'))
			->join(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
			->join(array('c'=>'tbl_course_tagging_group'), 'a.IdCourseTaggingGroup = c.IdCourseTaggingGroup')
			->where('a.IdStudentRegistration = ?', $id)
			->where('a.IdSemesterMain = ?', $sem);

		$result = $db->fetchAll($select);
		return $result;
	}
}