<?php
class Communication_Model_DbTable_General extends Zend_Db_Table {
	protected $_name = 'comm_compose';
	protected $_rec = 'comm_compose_recipients';
	protected $_attach = 'comm_compose_attachment';
	
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
		
	public function getAllCommunication($email)
	{
		/*$db = $this->db;
		
		$sql_que = $db->select()->from(array('a'=>'email_que'),array('id','subject','date_que'))->where('a.recepient_email = ?', $email)->order('id DESC');
		
		$results = $db->fetchAll($sql_que);

		

		return $results;*/

		$db = $this->db;
		
		//SELECT id, subject, date_que, 'que' as type FROM `email_que` WHERE 
		//$sql_que = $db->select()->from(array('a'=>'email_que'),array('id','subject','date_que','type' => 'que')->where('a.recepient_email = ?', $email);
		
		$stmt = $db->quoteInto("SELECT id, subject, date_que, 'que' as type, 'Email' as method FROM `email_que` WHERE recepient_email = ?",$email);
		$sql_que = $db->query($stmt);
		
		//$sql_comm = $db->select()->from(array('a'=>$this->_rec),array('id'=>'a.cr_id','subject'=>'a.cr_subject','type' => 'comm')
		//	->joinLeft(array('b'=>$this->_name),'a.cr_comp_id=b.comp_id',array('date_que'=>'b.created_date')->where('a.cr_email = ?', //$email);
		$stmt2 = $db->quoteInto("SELECT b.comp_id as id, cr_subject as subject,b.created_date as date_que, 'comm' as type, d.DefinitionDesc as method FROM `comm_compose_recipients` as a LEFT JOIN `comm_compose` as b ON (a.cr_comp_id=b.comp_id) LEFT JOIN `tbl_definationms` as d ON (d.idDefinition=b.comp_type) WHERE d.DefinitionDesc != 'Email' AND cr_email = ?",$email);
		$sql_comm = $db->query($stmt2);
		
		$select = $db->select()->union(array($stmt, $stmt2))->order('date_que DESC');
				
		return $db->fetchAll($select);
	}

	public function communicationTypeId($type)
	{
		$defModel = new App_Model_General_DbTable_Definationms();

		$get = $defModel->getByCode('Communication Types');
		
		foreach ( $get as $row )
		{
			if ( $row['DefinitionCode'] == $type )
			{
				return $row['idDefinition'];
			}
		}
	}


	
}