<?php
class Communication_Form_TemplateTags extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_tpltag');
		$this->setAttrib('onsubmit','return checkform()');
		
		
		$this->addElement('text','tag_name', 
			array(
				'label'=>'Tag Name',
				'required'=>'true',
				'class'	=> 'input-txt'
			)
		);
		
		$this->addElement('text','tag_value', 
			array(
				'label'=>'Description',
				'required'=>'false',
				'class'	=> 'input-txt'
			)
		);
		
		$this->addElement('select','tpl_id', 
			array(
				'label'=>'Template',
				'required'=>'true',
				'multiple'=>'multiple',
				'style'=>'height:80px'
			)
		);

		$this->tpl_id->addMultiOption(0,"All");
	
		//button
		$this->addElement('submit', 'addtag', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper'),
		  'class'	=> 'btn-submit'
        ));
        
       
        
        $this->addDisplayGroup(array('addtag'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>