<?php
class Communication_Form_GroupsAdd extends Zend_Form
{
	protected $rectype;

	public function setRectype($rectype){
		$this->rectype = $rectype; 
	}

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_groupsadd');
		$this->setAttrib('onsubmit','return checkform()');
		
		
		$this->addElement('text','group_name', 
			array(
				'label'=>'Group Name',
				'required'=>'true',
				'class' => 'input-txt'
			)
		);
		
		$this->addElement('text','group_description', 
			array(
				'label'=>'Description',
				'required'=>'false',
				'class' => 'input-txt'
			)
		);
              
		
		
		$this->addElement('select','group_rectype', array(
            'label'=>'Recipient Type',
            'required'=>'true'
		));
				
		//$this->group_rectype->addMultiOption(null,"-- Select --");
		if ( $this->rectype['applicant'] ) {
			$this->group_rectype->addMultiOption(0,"Applicant");
		}

		if ( $this->rectype['student'] ) {
			$this->group_rectype->addMultiOption(1,"Student");
		}

        $this->addElement('select','group_type', array(
            'label'=>'Group Type',
            'required'=>'true',
            'id'=>'groupType',
            'onchange'=>'showGroupStatus(this)'
		));
				
		$this->group_type->addMultiOption(null,"-- Select --");
		$this->group_type->addMultiOption(0,"Normal");
		$this->group_type->addMultiOption(1,"Auto");
			
		$this->addElement('select','group_status', array(
			'label'=>'Group Status',
			'id'=>'groupStatus'
		));
					
		$this->group_status->addMultiOption(null,"-- Select --");
			
		$definationDB = new App_Model_General_DbTable_Definationms();
		$statusList = $definationDB->getDataByType(56);
		
		if (count($statusList)>0){
			foreach ($statusList as $statusLoop){
				$this->group_status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
			}
		}
			
		//button
		$this->addElement('submit', 'addgroup', array(
		  'label'=>'Create Group',
		  'decorators'=>array('ViewHelper'),
		  'class' => 'btn-submit'
		));
			
		   
			
		$this->addDisplayGroup(array('addgroup'),'buttons', array(
		  'decorators'=>array(
			'FormElements',
			array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
			'DtDdWrapper'
		  )
		));
		
	}
}
?>