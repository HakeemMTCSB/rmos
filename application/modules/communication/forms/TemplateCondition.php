<?php
class Communication_Form_TemplateCondition extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_tplcondition');
		$this->setAttrib('onsubmit','return checkform()');
		
		$this->addElement('select','tpl_id', 
			array(
				'label'=>'Template',
				'required'=>'true',
			)
		);
		
		//semester
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$semesterList = $semesterDb->fnGetSemestermasterList();
		
		$this->addElement('select','semester_id', 
			array(
				'label'=>'Semester',
				'required'=>'true',
				'onchange'=>'getActivity(this);'
			)
		);
		$this->semester_id->addMultiOption(0,'Please Select');
		foreach($semesterList as $sem){
			$this->semester_id->addMultiOption($sem['key'],$sem['value']);
		}
		
		$this->addElement('select','activity_id', 
			array(
				'label'=>'Semester Activity',
				'required'=>'true',
			)
		);
		
		$this->addElement('select','publish', 
			array(
				'label'=>'Publish',
				'required'=>'true',
			)
		);

		$this->publish->addMultiOption(1,"Yes");
		$this->publish->addMultiOption(0,"No");
		
		
		$this->addElement('text','last_date', 
			array(
				'label'=>'Last Payment Date',
				'class'	=> 'input-txt datepicker'
			)
		);
		
		/*$this->addElement('text','balance', 
			array(
				'label'=>'Balance (RM)',
				'class'	=> 'input-txt'
			)
		);*/
	
		//button
		$this->addElement('submit', 'addtag', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper'),
		  'class'	=> 'btn-submit'
        ));
        
       
        
        $this->addDisplayGroup(array('addtag'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>