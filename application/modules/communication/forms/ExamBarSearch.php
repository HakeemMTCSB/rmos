<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 22/4/2016
 * Time: 3:26 PM
 */
class Communication_Form_ExamBarSearch extends Zend_Form {

    public function init(){
        $this->setMethod('post');
        $this->setAttrib('id','myform');

        //Semester
        $this->addElement('select','IdSemester', array(
            'label'=>$this->getView()->translate('Semester'),
            'onchange'=>'getCourse(); getMonth(this.value)'
        ));

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();

        $this->IdSemester->addMultiOption(null,"-- Select --");
        foreach($semesterDB->fnGetSemesterList() as $semester){
            if($this->_locale=='ms_MY'){
                $this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester['value2']);
            }else{
                $this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
            }
        }

        //Program
        $this->addElement('select','IdProgram', array(
            'label'=>$this->getView()->translate('Programme Name'),
            'onchange'=>'getCourse();'
        ));

        $programDb = new Registration_Model_DbTable_Program();

        $this->IdProgram->addMultiOption(null,"-- Select --");
        foreach($programDb->getData() as $program){
            if($this->_locale=='ms_MY'){
                $this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
            }else{
                $this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
            }
        }

        //Course
        $this->addElement('select','IdSubject', array(
            'label'=>$this->getView()->translate('Course Name'),
            'onchange'=>'getCourseGroup();',
            'registerInArrayValidator'=>false
        ));
        $this->IdSubject->addMultiOption(null,"-- Select --");

        //Group
        $this->addElement('select','IdGroup', array(
            'label'=>$this->getView()->translate('Section Name'),
            'registerInArrayValidator'=>false
        ));
        $this->IdGroup->addMultiOption(null,"-- Select --");

        //student id
        $this->addElement('text','studentid',
            array(
                'label'=>'Student ID',
                'class' => 'input-txt'
            )
        );

        //student name
        $this->addElement('text','studentname',
            array(
                'label'=>'Student Name',
                'class' => 'input-txt'
            )
        );

        //button
        $this->addElement('submit', 'Search', array(
            'label'=>$this->getView()->translate('Search'),
            'decorators'=>array('ViewHelper')
        ));

        $this->addDisplayGroup(array('Search'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));
    }
}
?>
<script type="text/javascript">
	function getCourse(){

        var semester_id = $('#IdSemester').val();
        var program_id = $('#IdProgram').val();

        if(semester_id!=='' && program_id!=''){
            $.ajax({
				url: "/default/ajax-utility/search-program-course",
			    type: "post",
			    async : false,
			    data: {'semester_id':semester_id,'program_id':program_id},
			    dataType: 'json',
			    success: function(data){

                $('#IdSubject').empty();
                $('#IdSubject').append('<option value="">-- Select --</option>');

                $.each(data, function(index) {
                    $('#IdSubject').append('<option value="'+data[index].IdSubject+'">'+data[index].SubCode+' - '+data[index].SubjectName+'</option>');
				    	});

            },
				error:function(){
                alert("failure");
            },
				beforeSend: function() {
                showLoading('IdSubject');

            },
				complete: function() {
                hideLoading('IdSubject');
            }
			});
		}
    }


	function getCourseGroup(){

        var semester_id = $('#IdSemester').val();
        var program_id = $('#IdProgram').val();
        var subject_id = $('#IdSubject').val();

        if(semester_id!=='' && program_id!='' && subject_id!=''){
            $.ajax({
				url: "/default/ajax-utility/search-course-group",
			    type: "post",
			    async : false,
			    data: {'subject_id':subject_id,'semester_id':semester_id},
			    dataType: 'json',
			    success: function(data){

                $('#IdGroup').empty();
                $('#IdGroup').append('<option value="">-- Select --</option>');

                $.each(data, function(index) {
                    $('#IdGroup').append('<option value="'+data[index].IdCourseTaggingGroup+'">'+data[index].GroupName+'</option>');
				    	});

            },
				error:function(){
                alert("failure");
            },
				beforeSend: function() {
                showLoading('IdGroup');

            },
				complete: function() {
                hideLoading('IdGroup');
            }
			});
		}
    }

	function showLoading(target){
        $('#'+target).hide();
        $('#'+target).after("<div id='loading'><img src='/images/spinner.gif' width='30' heigth='30' /></div>");
    }

	function hideLoading(target){
        $('#loading').remove();
        $('#'+target).show();
    }
</script>
