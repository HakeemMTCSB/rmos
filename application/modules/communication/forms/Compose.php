<?php
class Communication_Form_Compose extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_compose');
		$this->setAttrib('onsubmit','return checkform()');
		
		
		$this->addElement('text','comp_subject', 
			array(
				'label'=>'Title/Subject',
				'required'=>'true',
				'class' => ''
			)
		);
		
		$this->addElement('select','comp_tpl_id', 
			array(
				'label'		=> 'Template',
				'required'	=> 'true',
				'onchange'	=> 'changetpl(this); getTplTags(this);'
			)
		);

		$this->addElement('select','comp_type', 
			array(
				'label'		=> 'Type',
				'required'	=> 'true',
				'onchange'	=> 'checktype()'
			)
		);
		
		$this->addElement('radio', 'comp_lang', array(
			'label'=>'Language',
			'separator' => '' 
		));

		$this->addElement('file','comp_file_attach',
			array(
					'label'		=> 'Attachment',
                                        'id' => 'fileAttach'
				)
		);

		/*$this->addElement('select','comp_type', 
			array(
				'label'			=> 'Type',
				'required'		=> 'true',
				'multiOptions'	=> array(
											'email'	=> 'Email',
											'sms'	=> 'SMS',
											'pdf'	=> 'PDF'
										)
			)
		);*/
		
	}
}
?>