<?php
class Communication_AjaxController extends Base_Base 
{
	public function init() 
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$this->tplModel = new Communication_Model_DbTable_Template();
		$this->groupsModel = new Communication_Model_DbTable_Groups();
	}

	public function getTplAction()
	{
		if ($this->_request->isPost ())
		{
			$formData = $this->getRequest()->getPost();
			
			$template_content = $this->tplModel->getTemplateContent($formData['tpl_id'], $formData['locale']);
			
			$output = array();

			if ( empty($template_content) )
			{
				$output = array(); //error maybe?	
			}
			else
			{
				$output = array('content' => $template_content['tpl_content'], 'tpl_type' => $template_content['tpl_type']);
			}

			echo Zend_Json::encode($output);
		}
	}

	public function getTagsAction()
	{
		if ($this->_request->isPost ())
		{
			$formData = $this->getRequest()->getPost();
			
			$template_tags = $this->tplModel->getTemplateTags($formData['module'], $formData['tpl_id']);
			
			echo Zend_Json::encode($template_tags);

		}
	}

}