<?php
class Communication_StudentfinanceController extends Base_Base 
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		//for templatin usage
		$this->view->module = $this->module = 'studentfinance';

		//Set Sidebar
		$dbAdapter = getDb();
		$getPage = $dbAdapter->fetchRow("SELECT nav_menu_id FROM `tbl_nav_sidebar` WHERE module = '".$this->view->module."' LIMIT 1");
		Zend_Layout::getMvcInstance()->assign('sidebar', $getPage['nav_menu_id']);
			
		$this->tplModel = new Communication_Model_DbTable_Template();
		$this->groupsModel = new Communication_Model_DbTable_Groups();
		$this->composeModel = new Communication_Model_DbTable_Compose();
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
															/*
															'id_ID'	=> 'Indonesian'
															*/
													  );
		
		//attachment
		$this->attachment_path = DOCUMENT_PATH.'/attachments';

		/* there is no spoon */
		$this->rectype = array(
								'applicant' =>	false,
								'outstanding' =>	true,
								'student'	=>	true
						);

		$this->view->rectype = $this->rectype;

		/*
			#APPMODULE (untuk changes specificly untuk application module je)
		*/
	}
	
	public function indexAction()
	{
		//title
    	$this->view->title = "Communication";

		//just redirect, we dont need no index
		$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'compose'),'default',true)); 
	}
	
	/*
		HISTORY
	*/
	public function historyAction()
	{
		//title
    	$this->view->title = "History - Compose";

		$p_data = $this->composeModel->getHistoryData($this->module);
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}
        
	public function viewEmailAjaxAction()
	{
		$cr_id = $this->_getParam('id',null);

		$this->_helper->layout->disableLayout();

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();

		$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();

		$getemailinfo = $this->composeModel->viewEmail($cr_id);
		$json = Zend_Json::encode($getemailinfo);

		echo $json;
		exit();
	}

	public function historyDetailAction()
	{
		//title
    	$this->view->title = "Communication - History Details";

		$id = $this->_getParam('id', 0);
		$this->view->id = $id;
                
		if ($this->getRequest()->isPost() && $this->_request->getPost('resend'))
		{
			$formData = $this->getRequest()->getPost();
			$data = array(
							'cr_status' => $formData['status']
			);

			$this->composeModel->resendEmail($data, $formData['cr_id']);
		}
		
		//comm types
		$commTypes = $this->view->commTypes = $this->communicationTypes();
		$commTypesByName = $this->view->commTypesByName = array_flip($commTypes);

		//history
		$comp = $this->composeModel->getHistory($id);
		$getrec = explode(',',$comp['comp_rec']);
		$this->view->comp = $comp;

		if ( empty($comp) )
		{
			throw new Exception('Invalid Composed History');
		}

		$historydata = $this->composeModel->getHistoryRecipients($id, $comp['comp_rectype']);
		
		$this->view->results = $historydata;

		//render
		if ( $commTypesByName['Documents'] == $comp['comp_type'] )
		{
			$this->_helper->viewRenderer('history-detail-documents');
		}
	}

	public function getpdfAction()
	{
		$id = $this->_getParam('id', 0);
		$type = $this->_getParam('type', 0);
		$this->view->id = $id;

		$this->getpdfdata($id, $type);
		exit;
	}

	public function getpdfdata($id, $type=0)
	{
		$comp = $this->composeModel->getHistoryRecipient($id);
		if ( empty($comp) )
		{
			throw new Exception('Invalid ID');
		}
		
		$filename = $comp['comp_subject'].'-'.$comp['app_name'].'.pdf';

		//PDF yo
		require_once realpath(APPLICATION_PATH . '/../library/dompdf/') . "/dompdf_config.inc.php";
 
		$dompdf = new DOMPDF();

		$dompdf->load_html($comp['cr_content']);
		$dompdf->render();
		
		if ( $type == 0 )
		{
			$dompdf->stream($filename);
		}
		else
		{
			return $dompdf->output();
		}
	}
	
	public function getprintAction()
	{
		$id = $this->_getParam('id', 0);
		$this->view->id = $id;

		$comp = $this->composeModel->getHistoryRecipient($id);
		if ( empty($comp) )
		{
			throw new Exception('Invalid ID');
		}

		$this->view->comp = $comp;
		$this->_helper->layout()->setLayout('/print-empty');
	}


	public function downloadAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

		if ( $this->_request->isPost () )
		{
			$formData = $this->getRequest()->getPost();

			$zip = new ZipArchive();
			$zip_name = "Documents.zip"; // Zip name
			$file = tempnam("tmp", "zip");
			$zip->open($file,  ZipArchive::CREATE);
			foreach ($formData['rec'] as $rec_id) 
			{
				$comp = $this->composeModel->getHistoryRecipient($rec_id);

				$zip->addFromString( $comp['comp_subject'].'-'.$comp['app_name'].'.pdf', $this->getpdfdata($rec_id,1));
			}
			$zip->close();

			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zip_name);
			header('Content-Length: ' . filesize($file));
			readfile($file);
			unlink($file);
		}
	}

	/*
		COMPOSE
	*/
	public function composeAction()
	{
		//title
    	$this->view->title = "Communication - Compose";
		
		$type = $this->view->type = $this->_getParam('type');
		

		if ( $type == 'applicants' )
		{
			//applicants
			$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
			$form = new Application_Form_SearchApplicant();
			$txnList = $applicantTxnDB->getPaginateData();
		}
		elseif($type == 'os_students')
		{
			//outstanding 
			$form = new Registration_Form_SearchOutstandingStudent();
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
		    $txnList = $invoiceClass->getListStudentDataOutstanding();
		}
		else
		{
			$form = new Registration_Form_SearchStudent();
			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $txnList = $studentRegDB->getListStudentData();
		}
		
		//Post
		if ($this->getRequest()->isPost() && ( $this->_request->getPost('Search') || $this->_request->getPost('save') ) ) 
		{
			$formData = $this->getRequest()->getPost();

			if ( $type == 'os_students' )
			{
				$txnList = $invoiceClass->getListStudentOutstanding($formData);
			}
			else if ( $type == 'applicants' )
			{
				$txnList = $applicantTxnDB->getSearchPaginate($formData);
			}
			else
			{
				$txnList = $studentRegDB->getListStudentData($formData);
			}

			$db = getDb();
			$getTotal = $db->query($txnList)->rowCount();
			
			$this->view->totalfound = $getTotal;

			$app_paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
			$app_paginator->setItemCountPerPage(10000000);
			$app_paginator->setCurrentPageNumber($this->_getParam('page',1));

			$form->populate($formData);
		}
		else
		{
			$db = getDb();
			$getTotal = $db->query($txnList)->rowCount();

			$this->view->totalfound = $getTotal;

			$app_paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
			$app_paginator->setItemCountPerPage(20);
			$app_paginator->setCurrentPageNumber($this->_getParam('page',1));
		}



		//groups
		$groups_results = $this->groupsModel->getPaginateData();
    	
    	$groups_paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($groups_results));
    	$groups_paginator->setItemCountPerPage($this->gintPageCount);
    	$groups_paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
		//Search Form
		$this->view->search_form = $form;
		
		//Results
		$this->view->app_paginator = $app_paginator;
    	$this->view->groups_paginator = $groups_paginator;
	}

	public function composeReviewAction()
	{
		//title
    	$this->view->title = "Communication - Review";
        $auth = Zend_Auth::getInstance(); 
		$this->view->recipients = array();
		
		$recipients = array();
		
		
		//app
		if ( $this->_request->isPost () && $this->_request->getPost ( 'byapp' ))
		{
			$formData = $this->getRequest()->getPost();
			$this->view->formdata = $formData;
                        
			$apps = count($formData['txn_id']);
			if ( $apps > 0 )
			{
				$rec_id = array();
				foreach ( $formData['txn_id'] as $app_id => $app_name )
				{
					if ( !in_array($app_id, $rec_id) )
					{
						$rec_id[] = $app_id;
					}
				}

				if ( count($rec_id) > 0 )
				{
					$getRec = $this->groupsModel->fetchRecipientsByIds($rec_id, $formData['rectype']);
					foreach ( $getRec as $rec )
					{
						$recipients[$rec['at_trans_id']] = $rec;
					}
				}
			}
		}

		//group
		if ( $this->_request->isPost () && $this->_request->getPost ( 'bygroup' ))
		{
			$formData = $this->getRequest()->getPost();

			$groups = count($formData['group_id']);

			if ( $groups > 0 )
			{
				$recipients = array();

				foreach ( $formData['group_id'] as $group )
				{
                                    
					$groupInfo = $this->groupsModel->getGroup($group);
					
					$formData['rectype'] = $groupInfo['group_rectype'] == 1 ? 'students' : 'applicants';
					
					$this->view->formdata = $formData;
					
				
					if ($groupInfo['group_type']==1)
					{
						//$getStatus = $this->defModel->getIdByDefType('Status', 'Incomplete');
						$getRec = $this->groupsModel->getAutoReciepant($groupInfo['group_status']);
						foreach ( $getRec as $rec )
						{
							if ( !array_key_exists($rec['at_trans_id'], $recipients ) )
							{
								$recipients[$rec['at_trans_id']] = array(
									'at_trans_id' => $rec['at_trans_id'],
									'app_name' => $rec['appl_fname']." ".$rec['appl_mname']." ".$rec['appl_lname'],
									'at_pes_id' => $rec['at_pes_id']
								);
							}
						}
					}
					else
					{
						$getRec = $this->groupsModel->fetchRecipients($group, 1, ($group['group_rectype'] == 1 ? 'students' :'applicants'));
						

						foreach ( $getRec as $rec )
						{
							if ( !array_key_exists($rec['recipient_id'], $recipients ) )
							{
								$recipients[$rec['recipient_id']] = array(
																			'at_trans_id'	=> $rec['recipient_id'],									'app_name'		=> $rec['recipient_name'],
																			'at_pes_id'		=> $rec['at_pes_id']
																		);
							}
						}
					}
				}
			} // group

		}
		
		$this->view->total = count($recipients);
		$this->view->recipients = $recipients;
		
	}

	public function composeMsgAction()
	{
		//title
    	$this->view->title = "Communication - Compose";
		$auth = Zend_Auth::getInstance(); 
		$form = new Communication_Form_Compose();
		$this->view->trigger = 0;
                
		$cancompose = 0;
		if ($this->_request->isPost () && $this->_request->getPost ( 'compose' ))
		{
			$formData = $this->getRequest()->getPost();
			$total = $this->view->total = count($formData['rec']);
			
			$this->view->comp_rectype = $formData['rectype'];
			
			if ( $total > 0 )
			{
				$cancompose = 1;
				$rec_ids = array();

				foreach ( $formData['rec'] as $rec_id => $rec )
				{
					$rec_ids[] = $rec_id;
				}
				$this->view->rec = implode(',', $rec_ids);
			}
                        
			if (isset($formData['fromwhere']))
			{
				$this->view->fromwhere = $formData['fromwhere'];
			}
			else
			{
				$this->view->fromwhere = 0;
			}
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'composefromprev' ))
		{
			$formData = $this->getRequest()->getPost();
			$this->view->formData = $formData;

			$reclist = explode(',',$formData['rec']);

			$total = $this->view->total = count($reclist);
			
			if ( $total > 0 )
			{
				$cancompose = 1;
				
				$this->view->rec = implode(',', $reclist);
			}
			
			$this->view->trigger = 1;
			$form->populate($formData);
		}

		if ( $cancompose == 0 )
		{
			throw new Exception('You have to select recipients first');
		}
		
		//comm types
		$commTypes = $this->view->commTypes = $this->communicationTypes();
		$commTypesByName = $this->view->commTypesByName = array_flip($commTypes);

		//templates
		if ( isset($formData['category']) )
		{
			$tplData = $this->tplModel->getTemplatesByCategory($this->module, 'quicksend');
		}
		else
		{
			$tplData = $this->tplModel->getTemplates($this->module);
		}

		$form->comp_tpl_id->addMultiOption('', 'Select');
		$form->comp_type->addMultiOption('', 'Select');
		$form->comp_type->setValue($commTypesByName['Email']);

		$tpls[] = array();

		foreach ($tplData as $tpl)
		{
			$tpls[$tpl['tpl_type']][$tpl['tpl_id']] = $tpl['tpl_name'];
			//$form->comp_tpl_id->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
		}
	
		foreach ( $commTypes  as $type_id => $type_name )
		{
			$form->comp_type->addMultiOption($type_id, $type_name);

			if ( !empty($tpls[$type_id]) ) 
			{
				if ( count($tpls[$type_id]) > 0 )
				{
					$form->comp_tpl_id->addMultiOption($type_name, $tpls[$type_id]);
				}
			}
		}
		
		//radio lang
		$form->comp_lang->addMultiOptions($this->locales);
		if ( isset($formData['comp_lang']) && $formData['comp_lang'] != '' )
		{
			$form->comp_lang->setValue($formData['comp_lang']);
		}
		else
		{
			$form->comp_lang->setValue($this->currLocale);
		}

		//
		$this->view->form = $form;
	}
	
	public function composeRecipientsAction()
	{
		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
		}

		if ($this->_request->isPost() )
		{
			$this->view->recipients = array();

			$formData = $this->getRequest()->getPost();
			
			$reclist = explode(',',$formData['rec']);
			$apps = count($reclist);

			if ( $apps > 0 )
			{
				$getRec = $this->groupsModel->fetchRecipientsByIds($reclist,$formData['rectype']);
				foreach ( $getRec as $rec )
				{
					$recipients[$rec['at_trans_id']] = $rec;
				}
				
				$this->view->recipients = $recipients;
			}
		}
	}
	
	public function composeSendAction()
	{
		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
		}

		//title
		$this->view->title = "Communication - Send";
		
		$this->view->sent = 0;

		//comm types
		$commTypes = $this->view->commTypes = $this->communicationTypes();
		$commTypesByName = $this->view->commTypesByName = array_flip($commTypes);
                $cmsClass = new Cms_TemplateTags();

		$error = 1;
		if ($this->_request->isPost () && $this->_request->getPost ( 'preview' ))
		{
			$formData = $this->getRequest()->getPost();
                        
			$reclist = explode(',',$formData['rec']);

			if ( $formData['comp_type'] == $commTypesByName['SMS'] )
			{
				$formData['content'] = $formData['smscontent'];
			}
                        
			$getApp = $this->groupsModel->fetchRecipientsByIds($reclist);
			$this->applicant = $this->sortbyId($getApp,'at_trans_id');
			$this->template_tags = $this->tplModel->getTemplateTags($this->module);
			//var_dump($this->template_tags); exit;
			//$cmsClass = new Cms_TemplateTags();
			if ( isset($formData['comp_tpl_id']) && $formData['comp_tpl_id'] != '' )
			{
				$studentRegDB = new Registration_Model_DbTable_Studentregistration();
				$profile = $studentRegDB->getTheStudentRegistrationDetail($reclist[0]);

				$semesterModel = new App_Model_General_DbTable_Semestermaster();
				$semester = $semesterModel->getCurrentSemesterScheme($profile['IdScheme']);

				$activityDb = new App_Model_General_DbTable_Activity();
				$paymentReminderActivity = $activityDb->getCurrencyActivityPaymentReminder($semester['IdSemesterMaster']);

				$formData['content'] = $cmsClass->parseContentFinance($reclist[0], $formData['comp_tpl_id'], $formData['content'],'studentfinance', $paymentReminderActivity['cond_id']);
			}
			//$formData['content'] = $this->parseContent($reclist[0], $formData['content']);

			$error = 0;
			$this->view->type = 'preview'; 
			$this->view->formData = $formData;
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'send' ))
		{
			$formData = $this->getRequest()->getPost();
                        
			$error = 0;
			$this->view->type = 'send'; 
			$this->view->fromwhere = $formData['fromwhere'];
			$auth = Zend_Auth::getInstance();
                        
			$reclist = explode(',',$formData['rec']);
                        
			if ($formData['fromwhere'] != 0)
			{
				$model = new Application_Model_DbTable_ChecklistVerification();
				$info = $model->infoBox($formData['rec']);
				$this->view->trxid = $formData['rec'];
				$this->view->schemeid = $info['ap_prog_scheme'];
				$this->view->stdCtgy = $info['appl_category'];
			}

			$total = $this->view->total = count($reclist);
			
			if ( $total > 0 )
			{
				//process attachment first
				$attachments = array();

				try 
				{
					if ( !is_dir( $this->attachment_path ) )
					{
						if ( $this->mkdir_p($this->attachment_path) === false )
						{
							throw new Exception('Cannot create attachment folder ('.$this->attachment_path.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					//$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 2)); //maybe soon we will allow more?
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
					$adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip' , 'case' => false));
					$adapter->addValidator('NotExists', false, $this->attachment_path );
					$adapter->setDestination( $this->attachment_path );

					$files = $adapter->getFileInfo();
					foreach ($files as $no => $fileinfo) 
					{
						if ($adapter->isUploaded($no)) 
						{

							$ext = $this->getext($fileinfo['name']);
							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;

							$fileUrl = $this->attachment_path.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true), $no);
							$adapter->setOptions(array('useByteString' => false));

							$size = $adapter->getFileSize($no);

							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
											$errmessage[] = $errmsg;
									}

									throw new Exception(implode('<br />',$errmessage));
								}
							}

							//save into db
							$attachments[] = array(
													'fileName'		=> $fileinfo['name'],
													'fileUrl'		=> $fileName,
													'fileSize'		=> $size
												);
						} //isuploaded

					} //foreach

				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}
			
				
				//rectype
				$formData['rectype'] = isset($formData['rectype']) ? (empty($formData['rectype']) ? 'applicants' : $formData['rectype']) : 'applicants';

				//cache applicants
				$getApp = $this->groupsModel->fetchRecipientsByIds($reclist, $formData['rectype']);
				$this->applicant = $this->sortbyId($getApp,'at_trans_id');
				$this->template_tags = $this->tplModel->getTemplateTags($this->module, $formData['comp_tpl_id']);
		
				//sms
				if ( $formData['comp_type'] == $commTypesByName['SMS'] )
				{
					$formData['content'] = $formData['smscontent'];
				}
			
				//add comp_compose
				$data = array(
								 'comp_subject'				=>	$formData['comp_subject'],
								 'comp_module'				=>	$this->module,
								 'comp_tpl_id'				=>	$formData['comp_tpl_id'],
								 'comp_lang'				=>	$formData['comp_lang'],
								 'comp_type'				=>	$formData['comp_type'],
								 'comp_totalrecipients'		=>	$total,
								 'comp_rec'					=>	$formData['rec'],
								 'comp_rectype'				=>	$formData['rectype'],
								 'comp_content'				=>	$formData['content'],
								 'created_by'				=>	$auth->getIdentity()->iduser,
								 'created_date'				=>	new Zend_Db_Expr('NOW()')
							);

				$comp_id = $this->composeModel->addCompose($data);

				//attachments
				$attach_id = $attach_filename = $attach_name = '';
				if (!empty($attachments)){
					foreach ($attachments as $attachment) {
						$data3 = array(
							'ca_comp_id' => $comp_id,
							'ca_filename' => $attachment['fileName'],
							'ca_fileurl' => $attachment['fileUrl'],
							'ca_filesize' => $attachment['fileSize']
						);

						$attach_id = $this->composeModel->addAttachment($data3);
						$attach_filename = $attachment['fileUrl'];
						$attach_name = $attachment['fileName'];
					}
				}
                                
				if ($formData['comp_type']==$commTypesByName['Changed Scheme Email'])
				{
					$attachInfo = $this->composeModel->getAttachmentInfoForSchemeChange($formData['rec']);
					$attach_id = $attachInfo['ach_id'];
				}

				//recipients
				foreach ( $reclist as $app_id )
				{
					$studentRegDB = new Registration_Model_DbTable_Studentregistration();
					$profile = $studentRegDB->getTheStudentRegistrationDetail($app_id);

					$semesterModel = new App_Model_General_DbTable_Semestermaster();
					$semester = $semesterModel->getCurrentSemesterScheme($profile['IdScheme']);

					$activityDb = new App_Model_General_DbTable_Activity();
					$paymentReminderActivity = $activityDb->getCurrencyActivityPaymentReminder($semester['IdSemesterMaster']);

					//add into db
					$data2 = array(	
									'cr_comp_id'			=> $comp_id,
									'cr_subject'			=> $formData['comp_subject'],
									'cr_content'			=> $cmsClass->parseContentFinance($app_id, $formData['comp_tpl_id'], $formData['content'],'studentfinance', $paymentReminderActivity['cond_id']),
									'cr_rec_id'				=> $app_id,
									'cr_email'				=> $this->applicant[$app_id]['appl_email'],
									'cr_status'				=> 0,
									'cr_attachment_id'		=> $attach_id,
									'cr_attachment_filename'=> $attach_filename,
									'cr_attachment_name'	=> $attach_name
								);

					$rec_id = $this->composeModel->addComposeRecipient($data2);

					//if email
					if ( in_array($formData['comp_type'],array($commTypesByName['Changed Scheme Email'], $commTypesByName['Email'])) )
					{
						 //add to email que
                        $dataEmail = array(
											'recepient_email'	=> $this->applicant[$app_id]['appl_email'],
											'subject'			=> $formData['comp_subject'],
											'content'			=> $cmsClass->parseContentFinance($app_id, $formData['comp_tpl_id'], $formData['content'],'studentfinance', $paymentReminderActivity['cond_id']),
											'date_que'			=> date('Y-m-d H:i:s'),
											'comm_rec_id'		=> $rec_id
										);

                        $emailQueId = $this->emailModel->add($dataEmail);

						//add email attachment
						if (!empty($attachments)){
							foreach ($attachments as $attachment) {
								$dataEmailAttachment = array(
									'eqa_emailque_id' => $emailQueId,
									'eqa_filename' => $attachment['fileName'],
									'eqa_path' => $this->attachment_path . '/' . $attachment['fileUrl'],
									'eqa_updDate' => date('Y-m-d H:i:s'),
									'eqa_updUser' => $auth->getIdentity()->iduser
								);

								$this->emailModel->addAttachment($dataEmailAttachment);
							}
						}
					}
				}


				
				//if document, redirect to history detail page
				if ( $formData['comp_type'] == $commTypesByName['Documents'] )
				{
					$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'history-detail','id' => $comp_id),'default',true));
				}
				
				$this->view->sent = 1;

				$this->_helper->flashMessenger->addMessage(array('success' => 'Email succesfully sent'));

				$this->_redirect($this->view->url(array('module'=>'communication','controller'=>'studentfinance', 'action'=>'compose'),'default',true));
			}

		}

		if ( $error == 1)
		{
			throw new Exception('Error sending message');
		}
	}
	
	
	protected function parseContent($app_id, $content)
	{
		foreach ( $this->template_tags as $tag )
		{
			switch( $tag['tag_name'] )
			{
				case "Date":
					$content = str_replace("[".$tag['tag_name']."]", date("j-m-Y, g:i a"), $content); 
				break;

				case "Applicant ID":
					$this->applicant[$app_id]['at_pes_id'] = isset($this->applicant[$app_id]['at_pes_id']) ? $this->applicant[$app_id]['at_pes_id'] : '';
					$content = str_replace("[".$tag['tag_name']."]", $this->applicant[$app_id]['at_pes_id'], $content);
				break;

				case "Applicant Name":
					$this->applicant[$app_id]['app_name'] = isset($this->applicant[$app_id]['app_name']) ? $this->applicant[$app_id]['app_name']: '';
					$content = str_replace("[".$tag['tag_name']."]", $this->applicant[$app_id]['app_name'], $content);
				break;
                            
				case "Incomplete Checklist":
					$content = $this->parseContentIncompleteChecklist($app_id, $content, $tag['tag_name']);
				break;
				case 'Programme':
					$programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id='.$this->applicant[$app_id]['at_trans_id']);
					$programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme='.$programSchemeId);
					$program = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram='.$programId);
					$content = str_replace("[".$tag['tag_name']."]", $program, $content);
				break;
				case 'Intake':
					$intakeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_intake', 'at_trans_id = '.$this->applicant[$app_id]['at_trans_id']);
					$intake = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_intake', 'IntakeDesc', 'IdIntake = '.$intakeId);
					$content = str_replace("[".$tag['tag_name']."]", $intake, $content);
				break;
				case "Salutation":
					$applId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = '.$this->applicant[$app_id]['at_trans_id']);
					$salutationCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_salutation', 'appl_id = '.$applId);
					$salutation = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = '.$salutationCode);
					$content = str_replace("[".$tag['tag_name']."]", $salutation, $content); 
				break;
			}
		}
		return $content;
	}
        
	protected function parseContentIncompleteChecklist($app_id, $content, $tagName)
	{
		$incompleteChecklist = $this->groupsModel->getIncompleteChecklist($app_id, 3);
		
		$class_th = ' font:normal 11px Arial; background-color:#f1f1f1; color:#666; padding: 4px 4px; border-bottom:1px solid #ccc; font-weight:normal; text-align:left ';
		$class_td = ' font:normal 11px Arial; background-color:#fafafa; border-bottom:1px solid #e1e1e1; padding: 4px; font-weight:normal; vertical-align:top';

		$listChecklist = '';
		if (count($incompleteChecklist) > 0)
		{
			$i=1;
			$listChecklist .= '<div style="background:#ccc;padding:1px 1px 0 1px;"><table class="table" bgcolor="#fcfcfc" width="100%" border="0" cellspacing="0" cellpadding="5" style="font:normal 11px \"Lucida Grande\", Arial; background-color: #fcfcfc; border-spacing: 0; border-collapse: collapse;">';
			$listChecklist .= '<tr>';
			$listChecklist .= '<th width="5%" style="'.$class_th.'">No.</th>';
			$listChecklist .= '<th width="50%" style="'.$class_th.'">Checklist Description</th>';
			$listChecklist .= '<th width="35%" style="'.$class_th.'">Comment</th>';
			$listChecklist .= '<th style="'.$class_th.'">Status</th>';
			$listChecklist .= '</tr>';
			foreach ($incompleteChecklist as $incompleteChecklistLoop){
				
				$checklistDesc = Communication_Model_DbTable_Groups::getChecklistDesc($incompleteChecklistLoop['table_name'], '*', $incompleteChecklistLoop['table_primary_id'].' = '.$incompleteChecklistLoop['ads_table_id'], $incompleteChecklistLoop['table_join'], $incompleteChecklistLoop['join_column1'], $incompleteChecklistLoop['join_column2']);
				//var_dump($checklistDesc); 
				
				$status = '';
				switch ($incompleteChecklistLoop['ads_status']){
					case 1:
						$status = 'New';
						break;
					case 2:
						$status = 'Viewed';
						break;
					case 3:
						$status = 'Incomplete';
						break;
					case 3:
						$status = 'Complete';
						break;
				}
				
				$listChecklist .= '<tr>';
				$listChecklist .= '<td valign="top" style="'.$class_td.'">'.$i.'</td>';
				$listChecklist .= '<td valign="top" style="'.$class_td.'">'.$incompleteChecklistLoop['dcl_desc'].'</td>';
				$listChecklist .= '<td valign="top" style="'.$class_td.'">'.$incompleteChecklistLoop['ads_comment'].'</td>';
				$listChecklist .= '<td style="'.$class_td.'">'.$status.'</td>';
				$listChecklist .= '</tr>';
				$i++;
			}
			$listChecklist .= '</table></div>';
		}
		
		$content = str_replace("[".$tagName."]", $listChecklist, $content);
		
		return $content;
	}
	
	protected function sortbyId($recs,$id='')
	{
		$data = array();
		foreach ( $recs as $rec )
		{
			$data[$rec[$id]] = $rec;
		}

		return $data;
	}


	/*
		GROUPS
	*/
	public function groupsAction()
	{
		//title
    	$this->view->title = "Communication - Groups";
    	
		
		$results = $this->groupsModel->getPaginateData($this->module);
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($results));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function groupsAddAction()
	{
		$this->view->title = "Communication - Add New Group";
		
		$form = new Communication_Form_GroupsAdd(array('rectype'=>$this->rectype));

		$auth = Zend_Auth::getInstance(); 
		if ($this->_request->isPost () && $this->_request->getPost ( 'addgroup' ))
		{
			$formData = $this->getRequest()->getPost();
			
			if ( $form->isValid($formData) ) 
			{
				$data = array(
									'module'			=> $this->module,
									'group_name'		=> $formData['group_name'],
									'group_description'	=> $formData['group_description'],
									'group_rectype'		=> $formData['group_rectype'],
									'group_type'		=> $formData['group_type'],
									'group_status'		=> $formData['group_status'],
									'total_recipients'	=> 0,
									'created_by'		=> $auth->getIdentity()->iduser,
									'created_date'		=> new Zend_Db_Expr('NOW()')
								);
				
				$id = $this->groupsModel->addGroup($data);

				$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'groups-detail','id' => $id),'default',true));
			}
		}

		$this->view->form = $form;
	}

	public function groupsDetailAction()
	{
		$auth = Zend_Auth::getInstance(); 

		//title
		$this->view->title = "Communication - Groups";

		$id = $this->_getParam('id', 0);
		$this->view->id = $id;

		$group = $this->groupsModel->getGroup($id);
		$recipientIds = $this->groupsModel->fetchRecipientsIds($id);

		$this->view->group = $group;

		if ( empty($group) )
		{
			throw new Exception('Invalid Group');
		}
		
		//update
		if ($this->_request->isPost () && $this->_request->getPost ( 'update' ))
		{
			$formData = $this->getRequest()->getPost();

			$data = array(		
							'group_name'		=> $formData['group_name'],
							'group_description'	=> $formData['group_description'],
							'updated_by'		=> $auth->getIdentity()->iduser,
							'updated_date'		=> new Zend_Db_Expr('NOW()')
						);
				
			$id = $this->groupsModel->updateGroup($data, 'group_id = '.$id);

			$this->_helper->flashMessenger->addMessage(array('success' => 'Information Updated'));

			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'groups-detail','id' => $formData['group_id']),'default',true));
		}
		
		//Add Into Group
		if ($this->_request->isPost () && $this->_request->getPost ( 'addgroup' ))
		{
			$formData = $this->getRequest()->getPost();
			$totalchecked = count($formData['txn_id']);
			
			if ( $totalchecked == 0 )
			{
				$this->view->noticeError = $this->view->translate('Please atleast select one applicant');
			}
			else
			{
				$added = 0 ;

				foreach ( $formData['txn_id'] as $appid => $appname )
				{
					if ( !in_array($appid, $recipientIds) )
					{
						$data = array(
										'rec_type'			=>	$this->module,
										'group_id'			=>	$formData['group_id'],
										'recipient_name'	=>	$appname,
										'recipient_id'		=>	$appid
									);

						$this->groupsModel->addRecipient($data);

						$added = 1;
					}
				}

				if ( $added )
				{
					$this->groupsModel->recountRecipients($formData['group_id'], $auth->getIdentity()->iduser );
					
					$this->_helper->flashMessenger->addMessage(array('success' => 'Recipients Updated'));

					$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'groups-detail','id' => $id ),'default',true));
				}
			}

		}
		
		if ( $group['group_rectype'] == 0 )
		{
			//Applications
			$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
			$form = new Application_Form_SearchApplicant();
			$txnList = $applicantTxnDB->getPaginateData();
		}
		else
		{
			//Students
			$form = new Registration_Form_SearchStudent();
			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $txnList = $studentRegDB->getListStudentData();
		}
		
		//Post
		if ($this->getRequest()->isPost() && ( $this->_request->getPost('Search') || $this->_request->getPost('save') ) ) 
		{
			$formData = $this->getRequest()->getPost();
			
			if ( $group['group_rectype'] == 0 )
			{
				$txnList = $applicantTxnDB->getSearchPaginate($formData);
			}
			else
			{
				$txnList = $studentRegDB->getListStudent($formData);
			}

			$this->view->totalfound = count($txnList);


			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($txnList));
			$paginator->setItemCountPerPage(10000000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));

			$form->populate($formData);
		}
		else
		{
			$db = getDb();
			$getTotal = $db->query($txnList)->rowCount();

			$this->view->totalfound = $getTotal;

			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
	
		
		//Search Form
		$this->view->search_form = $form;
		
		//Results
		$this->view->paginator = $paginator;
	}

	public function groupsRecipientsAction()
	{
		$auth = Zend_Auth::getInstance(); 

		//title
		$this->view->title = "Communication - Groups Recipients";

		$id = $this->_getParam('id', 0);
		$this->view->id = $id;

		$group = $this->groupsModel->getGroup($id);
		$recipients = $this->groupsModel->fetchRecipients($id);
		
		//Zap zap zap
		if ($this->_request->isPost () && $this->_request->getPost ( 'delete' ))
		{
			$formData = $this->getRequest()->getPost();
			$totalchecked = count($formData['rec_id']);
			
			if ( $totalchecked == 0 )
			{
				$this->view->noticeError = $this->view->translate('Please atleast select one applicant');
			}

			foreach ( $formData['rec_id'] as $rec_id )
			{
				$data = array(
								'rec_id	= ?'	=>	$rec_id
							);

				$this->groupsModel->deleteRecipient($data);
			}

			$this->groupsModel->recountRecipients($formData['group_id'], $auth->getIdentity()->iduser );

			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'groups-recipients','id' => $id ),'default',true));
		}

		//rest
		if ( $group['total_recipients'] == 0 )
		{
			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'groups-detail','id' => $id ),'default',true));
		}

		$this->view->results = $recipients;
		$this->view->group = $group;
	}

	/*
		TEMPLATES
	*/
	public function templateAction()
	{
		//title
    	$this->view->title = "Communication - Template Setup";
    	
		$p_data = $this->tplModel->getPaginateData($this->module);
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
    		
	}
	
	public function templateAddAction()
	{
		$this->view->title = "Communication - Template Add";
		
		$template_tags = $this->tplModel->getTemplateTags($this->module);
		$this->view->tags = $template_tags;
		
		//comm types
		$commTypes = $this->view->commTypes = $this->communicationTypes();
		$commTypesByName = $this->view->commTypesByName = array_flip($commTypes);
                
		$auth = Zend_Auth::getInstance(); 
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
                        
			$tpl_data = array(
                            'tpl_module'		=> $this->module,
                            'tpl_name'			=> $formData['tpl_name'],
                            'tpl_type'			=> $formData['tpl_type'],
                            'tpl_pname'			=> clean_string($formData['tpl_name']),
                            'created_by'		=> $auth->getIdentity()->iduser,
                            'created_date'		=> new Zend_Db_Expr('NOW()'),
                            'email_from_name'	=> $formData['email_from_name'],
                            'email_from'		=> $formData['email_from']
                        );
			
			$tpl_id = $this->tplModel->addTemplate($tpl_data);
                        
                        $tplType = $this->defModel->getIdByDefType('Communication Types', 'Standard Document');
			
                        if ($formData['tpl_type'] == $tplType['key']){
                            if ( !is_dir(APP_DOC_PATH."/download/") )
                            {
                                if ( $this->mkdir_p(APP_DOC_PATH."/download/") === false )
                                {
                                    throw new Exception('Cannot create attachment folder ('.APP_DOC_PATH.'/download/)');
                                }
                            }

                            $adapter = new Zend_File_Transfer_Adapter_Http();
                            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 2)); //maybe soon we will allow more?
                            $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                            $adapter->addValidator('NotExists', false, APP_DOC_PATH."/download/" );
                            $adapter->setDestination( APP_DOC_PATH."/download/" );
                            $adapter->isUploaded();
                            $adapter->receive();

                            $files = $adapter->getFileInfo();
                            foreach ( $this->locales as $locale_code => $locale ){

                                $content = $files['file'.$locale_code]['name'];

                                $cdata = array(
                                    'tpl_id' =>	$tpl_id,
                                    'locale' =>	$locale_code,
                                    'tpl_content' => "/download/".$content
                                );

                                $this->tplModel->addTemplateContent($cdata);
                            }
                        }
						else
						{
                            foreach ( $this->locales as $locale_code => $locale )
                            {
								$cdata = array(
												'tpl_id'		=>	$tpl_id,
												'locale'		=>	$locale_code,
												'tpl_content'	=>	$formData['content_'.strtolower($locale_code)]
										);

								$this->tplModel->addTemplateContent($cdata);
                            }
                        }

			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'template'),'default',true));
		}
	}
	
	public function templateCategoriesAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
		
		$tags = array();
		$db = getDb();
		$select = $db->select()->from('comm_template',array('tpl_categories'));

		$results = $db->fetchAll($select);

		foreach ( $results as $row )
		{
			if ( !empty($row['tpl_categories']) )
			{
				$gettags = explode(',',$row['tpl_categories']);
				$tags = array_merge($tags, $gettags);
			}
		}
		
		$tags = array_unique($tags);

		$term = $this->_getParam('term', null);
		
		$data = array();
		foreach( $tags as $tag )
		{
			if ( $term != null )
			{
				if ( preg_match("/".$term."/", $tag) )
				{
					$data[] = array('id' => $tag, 'label' => $tag );
				}
			}
			else
			{
				$data[] = array('id' => $tag, 'label' => $tag );
			}
		}

		echo json_encode($data);
		exit;


	}
	public function templateDetailAction()
	{
		$db = getDb();
		$auth = Zend_Auth::getInstance(); 

		//title
		$this->view->title = "Communication - Template Detail";
		
		$id = $this->_getParam('id', 0);
		
		$template = $this->tplModel->getTemplate($id);
		$get_template_content = $this->tplModel->getTemplateContent($id);
		$template_tags = $this->tplModel->getTemplateTags($this->module, $id);
		
		//comm types
		$commTypes = $this->view->commTypes = $this->communicationTypes();
		$commTypesByName = $this->view->commTypesByName = array_flip($commTypes);


		//format content
		$template_content = array();
		foreach ( $get_template_content as $content )
		{
			$template_content[$content['locale']] = $content['tpl_content'];
		}

		//
		$this->view->template = $template;
		$this->view->content = $template_content;
		$this->view->tags = $template_tags;
		
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$tpl_data = array(
								'tpl_name'			=> $formData['tpl_name'],
								'tpl_type'			=> $formData['tpl_type'],
								'tpl_pname'			=> clean_string($formData['tpl_name']),
								'updated_by'		=> $auth->getIdentity()->iduser,
								'updated_date'		=> new Zend_Db_Expr('NOW()'),
								'email_from_name'	=> $formData['email_from_name'],
								'email_from'		=> $formData['email_from'],
								'tpl_categories'	=> $formData['tpl_categories']
							);
		

			$tpl_id = $this->tplModel->updateTemplate($tpl_data, $db->quoteInto('tpl_id = ?', $formData['tpl_id']));
                        
			$tplType = $this->defModel->getIdByDefType('Communication Types', 'Standard Document');

			if ($formData['tpl_type'] == $tplType['key'])
			{
				if ( !is_dir(APP_DOC_PATH."/download/") )
				{
					if ( $this->mkdir_p(APP_DOC_PATH."/download/") === false )
					{
						throw new Exception('Cannot create attachment folder ('.APP_DOC_PATH.'/download/)');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 2)); //maybe soon we will allow more?
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
				$adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
				$adapter->addValidator('NotExists', false, APP_DOC_PATH."/download/");
				$adapter->setDestination(APP_DOC_PATH."/download/");
				//$adapter->isUploaded();
				//$adapter->receive();

				$files = $adapter->getFileInfo();
                                
                                if ($files){
                                    foreach($files as $key=>$filesLoop){
                                        $adapter->isUploaded($key);
                                        $adapter->receive($key);
                                    }
                                }
                                
				foreach ( $this->locales as $locale_code => $locale ){
                                    
                                        $content = $files['file'.$locale_code]['name'];

					$where = array();
					$cdata = array(
						'tpl_content' => '/download/'.$content
					);

					$where[] = $db->quoteInto('tpl_id = ?',$formData['tpl_id']);
					$where[] = $db->quoteInto('locale = ?', $locale_code);



					$this->tplModel->updateTemplateContent($cdata, $where);
					
				}
			}
			else
			{
				foreach ( $this->locales as $locale_code => $locale )
				{
					$where = array();
					$cdata = array(
										'tpl_content'	=>	$formData['content_'.strtolower($locale_code)]
								);

					$where[] = $db->quoteInto('tpl_id = ?',$formData['tpl_id']);
					$where[] = $db->quoteInto('locale = ?', $locale_code);

					$this->tplModel->updateTemplateContent($cdata, $where);

						
					// #APPMODULE
					if ( $this->module == 'application' )
					{
						if ( $locale_code == 'en_US' )
						{
							$db->update('tbl_statustemplate_stp', array('contentEng' => $formData['content_en_us']), $db->quoteInto('stp_tplId = ?', $formData['tpl_id']));
						}
						else
						{
							$db->update('tbl_statustemplate_stp', array('contentMy' => $formData['content_ms_my']), $db->quoteInto('stp_tplId = ?', $formData['tpl_id']));
						}
					}

				}
			}

			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'template-detail','id'=>$id),'default',true));
		}
	}
	
	public function templateTagsAction()
	{
		//title
		$this->view->title = "Communication - Template Tags";

		$form = new Communication_Form_TemplateTags();
		
		//template list
		$tplData = $this->tplModel->getTemplates($this->module);

		//tags
		$results = $this->tplModel->getTemplateTagsSelect($this->module,0,1,'tag_name ASC');

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($results));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
		
		$tplbyid = array();
		foreach ( $tplData as $tpl )
		{
			$tplbyid[$tpl['tpl_id']] = $tpl;
			$form->tpl_id->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
		}

		$this->view->templates = $tplbyid;

		//set "All" as default
		$form->tpl_id->setValue(0);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'addtag' ))
		{
			$formData = $this->getRequest()->getPost();

			$cdata = array(
								'tpl_id'		=>	implode(',',$formData['tpl_id']),
								'tag_name'		=>	$formData['tag_name'],
								'tag_value'		=>	$formData['tag_value'],
								'module'		=>	$this->module
							);

			$this->tplModel->addTemplateTag($cdata);	
		}

		$this->view->form = $form;
	}

	public function templateEdittagAction()
	{
		//title
		$this->view->title = "Communication - Edit Tag";

		$form = new Communication_Form_TemplateTags();
		
		$id = $this->_getParam('id', 0);

		//tags
		$gettag = $this->tplModel->getTemplateTag($id);

		//template list
		$tplData = $this->tplModel->getTemplates($this->module);

		if ( empty($gettag) ) 
		{
			throw new Zend_Exception('Invalid Tag');
		}

		//template list
		foreach ( $tplData as $tpl )
		{
			$form->tpl_id->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
		}


		
		if ($this->_request->isPost () && $this->_request->getPost ( 'addtag' ))
		{
			$formData = $this->getRequest()->getPost();
			
			
			$cdata = array(
								'tpl_id'		=>	implode(',',$formData['tpl_id']),
								'tag_name'		=>	$formData['tag_name'],
								'tag_value'		=>	$formData['tag_value'],
								'module'		=>	$this->module
							);
		
			$this->tplModel->updateTemplateTag($cdata, $id );

			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'template-tags'),'default',true));
		}

		$form->populate($gettag);
		$form->tpl_id->setValue(explode(',',$gettag['tpl_id']));


		$this->view->taginfo = $gettag;
		$this->view->form = $form;

	}

	/*
	* credits to WP
	*/
	public function mkdir_p( $target )
	{
		// safe mode fails with a trailing slash under certain PHP versions.
		$target = rtrim($target, '/'); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
		if ( empty($target) )
			$target = '/';

		if ( file_exists( $target ) )
			return @is_dir( $target );

		// We need to find the permissions of the parent folder that exists and inherit that.
		$target_parent = dirname( $target );
		while ( '.' != $target_parent && ! is_dir( $target_parent ) ) {
			$target_parent = dirname( $target_parent );
		}

		// Get the permission bits.
		$dir_perms = false;
		if ( $stat = @stat( $target_parent ) ) 
		{
			$dir_perms = $stat['mode'] & 0007777;
		}
		else 
		{
			$dir_perms = 0777;
		}

		if ( @mkdir( $target, $dir_perms, true ) ) 
		{

			// If a umask is set that modifies $dir_perms, we'll have to re-set the $dir_perms correctly with chmod()
			if ( $dir_perms != ( $dir_perms & ~umask() ) ) 
			{
				$folder_parts = explode( '/', substr( $target, strlen( $target_parent ) + 1 ) );
				for ( $i = 1; $i <= count( $folder_parts ); $i++ ) 
				{
					@chmod( $target_parent . '/' . implode( '/', array_slice( $folder_parts, 0, $i ) ), $dir_perms );
				}
			}

			return true;
		}

		return false;
	}

	protected function getext($filename)
	{
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		return $ext;
	}

	public function communicationTypes()
	{
		$get = $this->defModel->getByCode('Communication Types');
		$data = array();
		
		foreach ( $get as $row )
		{
			$data[$row['idDefinition']] = ($this->currLocale == 'en_US' ? $row['DefinitionDesc'] : $row['BahasaIndonesia']);
		}

		return $data;
	}
	
	public function templateConditionAction()
	{
		//title
		$this->view->title = "Communication - Template Condition";
		
		

		$form = new Communication_Form_TemplateCondition();
		
		//template list
		$tplData = $this->tplModel->getTemplates($this->module);

		//tags
		$results = $this->tplModel->getTemplateConditionSelect($this->module,0,1,'SemesterName ASC');

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($results));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
		
		$tplbyid = array();
		foreach ( $tplData as $tpl )
		{
			$tplbyid[$tpl['tpl_id']] = $tpl;
			$form->tpl_id->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
		}

		$this->view->templates = $tplbyid;

		//set "All" as default
		$form->tpl_id->setValue(0);
//		$form->balance->setValue('0.00');
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'addtag' ))
		{
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();

			$cdata = array(
								'tpl_id'		=>	$formData['tpl_id'],
								'semester_id'	=>	$formData['semester_id'],
								'activity_id'	=>	$formData['activity_id'],
								'publish'		=>	$formData['publish'],
								'last_date'		=>	$formData['last_date'],
//								'balance'		=>	$formData['balance'],
								'UpdUser'		=>	$auth->getIdentity()->iduser,
								'UpdDate'		=>	date('Y-m-d H:i:s'),
								'module'		=>	$this->module
							);

			$this->tplModel->addTemplateCondition($cdata);	
		}

		$this->view->form = $form;
	}
	
	public function getSemesterActivityAction()
	{	
    	$semID = $this->_getParam('id', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
					->from(array('a'=>'tbl_activity_calender'))
					->join(array('b' => 'tbl_activity'), 'b.idActivity = a.IdActivity')
                    ->where('a.IdSemesterMain = ?',$semID)
					->order('b.ActivityName asc');
					
       	$result = $db->fetchAll($select);
         
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
	public function templateEditconditionAction()
	{
		//title
		$this->view->title = "Communication - Edit Tag";

		$form = new Communication_Form_TemplateCondition();
		
		$id = $this->_getParam('id', 0);

		//tags
		$gettag = $this->tplModel->getTemplateCondition($id);

		//template list
		$tplData = $this->tplModel->getTemplates($this->module);

		if ( empty($gettag) ) 
		{
			throw new Zend_Exception('Invalid Tag');
		}

		//template list
		foreach ( $tplData as $tpl )
		{
			$form->tpl_id->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
		}

		$form->semester_id->disabled = true;
		$form->activity_id->disabled = true;
		$form->tpl_id->disabled = true;

		if ($this->_request->isPost () && $this->_request->getPost ( 'addtag' ))
		{
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
			
			$cdata = array(
								'publish'		=>	$formData['publish'],
								'last_date'		=>	$formData['last_date'],
//								'balance'		=>	$formData['balance'],
								'UpdUser'		=>	$auth->getIdentity()->iduser,
								'UpdDate'		=>	date('Y-m-d H:i:s'),
								'module'		=>	$this->module
							);
		
			$this->tplModel->updateTemplateCondition($cdata, $id );

			$this->_redirect($this->view->url(array('module'=>'communication','controller'=>$this->module, 'action'=>'template-condition'),'default',true));
		}

		$form->populate($gettag);
		$form->tpl_id->setValue($gettag['tpl_id']);


		$this->view->taginfo = $gettag;
		$this->view->form = $form;

	}
    
}