<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 7/3/2016
 * Time: 4:04 PM
 */
class Communication_AttendanceReminderController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Communication_Model_DbTable_AttendanceReminder();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Attendance Reminder');

        $form = new Registration_Form_SearchStudentAttendanceReminder();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if ($form->isValid($formData)) {
                $attendanceDB = new GeneralSetup_Model_DbTable_Attendance();
                $student_list = $attendanceDB->getStudentWithAttendance($formData);

                $this->view->formData = $formData;
                $this->view->student_list = $student_list;
            }

            $form->populate($formData);
        }
    }

    public function printAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $attendanceDB = new GeneralSetup_Model_DbTable_Attendance();
            $student_list = $attendanceDB->getStudentWithAttendance($formData);

            if ($formData['att_type']==1){
                $this->view->filename = date('Ymd').'_attendance_reminder_report.xls';
            }else{
                $this->view->filename = date('Ymd').'_exam_bar_report.xls';
            }

            $this->view->formData = $formData;
            $this->view->student_list = $student_list;
        }
    }

    public function sendReminderAction(){

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if ($formData['type']==1){ //attendance warning
                $emailTemplate = $this->model->getTemplate(210);
                $docTemplate = $this->model->getTemplate(211);

                if (isset($formData['check']) && count($formData['check']) > 0){
                    foreach ($formData['check'] as $id => $value){
                        $studentInfo = $this->model->getStudentInfo($id);

                        $groupInfo = $this->model->getGroupInfo($formData['subject'], $formData['semester'], $id);

                        $emailContent = $this->parseTag($id, $emailTemplate['tpl_content'], $groupInfo, $formData['item'], $formData['type'], $formData);

                        $seqInfo = $this->model->getRefSeq(date('Y'), $formData['type']);

                        $seqNo = sprintf("%04d", $seqInfo['ars_seq']);

                        $refNo = 'WL/'.$seqInfo['ars_year'].'/'.$seqNo;
                        $refNoDoc = 'WL_'.$seqInfo['ars_year'].'_'.$seqNo;

                        $emailContent = str_replace('[attendance_warning_ref_no]', $refNo, $emailContent);

                        $docContent = $this->parseTag($id, $docTemplate['tpl_content'], $groupInfo, $formData['item'], $formData['type'], $formData);

                        $docContent = str_replace('[attendance_warning_ref_no]', $refNo, $docContent);

                        //attachment name
                        $name = $refNoDoc.'_'.date('YmdHis');

                        //attachment url
                        $url = DOCUMENT_PATH.'/attendance_reminder';

                        //generate pdf
                        $this->generatePDF($docContent, $name, $url);

                        $auth = Zend_Auth::getInstance();
                        $userId = $auth->getIdentity()->iduser;

                        //insert data
                        $atrData = array(
                            'atr_student_id'=>$id,
                            'atr_subject_id'=>$formData['subject'],
                            'atr_semester_id'=>$formData['semester'],
                            'atr_group_id'=>$groupInfo['IdCourseTaggingGroup'],
                            'atr_type'=>$formData['type'],
                            'atr_item'=>$formData['item'],
                            'atr_status'=>0,
                            'atr_email_content'=>$emailContent,
                            'atr_send_by'=>$userId,
                            'atr_send_date'=>date('Y-m-d H:i:s')
                        );
                        $atr_id = $this->model->insertAttRem($atrData);
                        
                        $atrAttcData = array(
                            'ara_atr_id'=>$atr_id,
                            'ara_filename'=>$name,
                            'ara_path'=>'/attendance_reminder/'.$name.'.pdf',
                            'ara_updDate'=>date('Y-m-d'),
                            'ara_updUser'=>$userId
                        );
                        $this->model->insertAttAttch($atrAttcData);

                        //email part
                        $emailDb = new App_Model_System_DbTable_Email();
                        $vmodel = new Records_Model_DbTable_Visitingstudent();

                        //email personal
                        $data_inceif = array(
                            'recepient_email' => $studentInfo['appl_email'],
                            'subject' => 'Attendance Reminder',
                            'content' => $emailContent,
                            'attachment_path'=>null,
                            'attachment_filename'=>null
                        );
                        //add email to email que
                        $email_id2 = $emailDb->addData($data_inceif);
                        
                        //email attachment
                        $email_attach = array(
                            'eqa_emailque_id'=>$email_id2,
                            'eqa_filename'=>$name.'.pdf',
                            'eqa_path'=>$url.'/'.$name.'.pdf',
                            'eqa_updDate'=>date('Y-m-d H:i:s'),
                            'eqa_updUser'=>$userId
                        );
                        $emailDb->addEmailQueAtttachment($email_attach);

                        //save to history
                        $data_compose = array(
                            'comp_subject'=> 'Attendance Reminder',
                            'comp_module' => 'records',
                            'comp_type' => 585,
                            'comp_tpl_id' => $emailTemplate["tpl_id"],
                            'comp_rectype' => 'students',
                            'comp_lang' => 'en_US',
                            'comp_totalrecipients' => 1,
                            'comp_rec' => $studentInfo['IdStudentRegistration'],
                            'comp_content' => $emailContent,
                            'created_by' => $userId,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $comId = $vmodel->insertCommpose($data_compose);

                        $data_compose_rec = array(
                            'cr_comp_id' => $comId,
                            'cr_subject' => 'Attendance Reminder',
                            'cr_content' => $emailContent,
                            'cr_rec_id' => $studentInfo['IdStudentRegistration'],
                            'cr_email' => $studentInfo['appl_email'],
                            'cr_status' => 1,
                            'cr_datesent' => date('Y-m-d H:i:s')
                        );
                        $vmodel->insertComposeRec($data_compose_rec);
                        
                        $data_compose_attach = array(
                            'ca_comp_id'=>$comId,
                            'ca_filename'=>$name.'.pdf',
                            'ca_fileurl'=>$name.'.pdf',
                            'ca_filesize'=>0
                        );
                        $vmodel->insertComposeAttach($data_compose_attach);
                    }
                }
            }else{ //exam bar
                $emailTemplate = $this->model->getTemplate(212);
                $docTemplate = $this->model->getTemplate(213);

                if (isset($formData['check']) && count($formData['check']) > 0){
                    foreach ($formData['check'] as $id => $value){
                        $studentInfo = $this->model->getStudentInfo($id);

                        $groupInfo = $this->model->getGroupInfo($formData['subject'], $formData['semester'], $id);

                        $emailContent = $this->parseTag($id, $emailTemplate['tpl_content'], $groupInfo, $formData['item'], $formData['type'], $formData);

                        $seqInfo = $this->model->getRefSeq(date('Y'), $formData['type']);

                        $seqNo = sprintf("%04d", $seqInfo['ars_seq']);

                        $refNo = 'BL/'.$seqInfo['ars_year'].'/'.$seqNo;
                        $refNoDoc = 'BL_'.$seqInfo['ars_year'].'_'.$seqNo;

                        $emailContent = str_replace('[exam_bar_ref_no]', $refNo, $emailContent);

                        $docContent = $this->parseTag($id, $docTemplate['tpl_content'], $groupInfo, $formData['item'], $formData['type'], $formData);

                        $docContent = str_replace('[exam_bar_ref_no]', $refNo, $docContent);

                        //attachment name
                        $name = $refNoDoc.'_'.date('YmdHis');

                        //attachment url
                        $url = DOCUMENT_PATH.'/exam_bar';

                        //generate pdf
                        $this->generatePDF($docContent, $name, $url);

                        $auth = Zend_Auth::getInstance();
                        $userId = $auth->getIdentity()->iduser;

                        //insert data
                        $atrData = array(
                            'atr_student_id'=>$id,
                            'atr_subject_id'=>$formData['subject'],
                            'atr_semester_id'=>$formData['semester'],
                            'atr_group_id'=>$groupInfo['IdCourseTaggingGroup'],
                            'atr_type'=>$formData['type'],
                            'atr_item'=>$formData['item'],
                            'atr_status'=>0,
                            'atr_email_content'=>$emailContent,
                            'atr_send_by'=>$userId,
                            'atr_send_date'=>date('Y-m-d H:i:s')
                        );
                        $atr_id = $this->model->insertAttRem($atrData);

                        $atrAttcData = array(
                            'ara_atr_id'=>$atr_id,
                            'ara_filename'=>$name,
                            'ara_path'=>'/exam_bar/'.$name.'.pdf',
                            'ara_updDate'=>date('Y-m-d'),
                            'ara_updUser'=>$userId
                        );
                        $this->model->insertAttAttch($atrAttcData);

                        //email part
                        $emailDb = new App_Model_System_DbTable_Email();
                        $vmodel = new Records_Model_DbTable_Visitingstudent();

                        //email personal
                        $data_inceif = array(
                            'recepient_email' => $studentInfo['appl_email'],
                            'subject' => 'Notice of Examination Barring',
                            'content' => $emailContent,
                            'attachment_path'=>null,
                            'attachment_filename'=>null
                        );
                        //add email to email que
                        $email_id2 = $emailDb->addData($data_inceif);

                        //email attachment
                        $email_attach = array(
                            'eqa_emailque_id'=>$email_id2,
                            'eqa_filename'=>$name.'.pdf',
                            'eqa_path'=>$url.'/'.$name.'.pdf',
                            'eqa_updDate'=>date('Y-m-d H:i:s'),
                            'eqa_updUser'=>$userId
                        );
                        $emailDb->addEmailQueAtttachment($email_attach);

                        //save to history
                        $data_compose = array(
                            'comp_subject'=> 'Notice of Examination Barring',
                            'comp_module' => 'records',
                            'comp_type' => 585,
                            'comp_tpl_id' => $emailTemplate["tpl_id"],
                            'comp_rectype' => 'students',
                            'comp_lang' => 'en_US',
                            'comp_totalrecipients' => 1,
                            'comp_rec' => $studentInfo['IdStudentRegistration'],
                            'comp_content' => $emailContent,
                            'created_by' => $userId,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $comId = $vmodel->insertCommpose($data_compose);

                        $data_compose_rec = array(
                            'cr_comp_id' => $comId,
                            'cr_subject' => 'Notice of Examination Barring',
                            'cr_content' => $emailContent,
                            'cr_rec_id' => $studentInfo['IdStudentRegistration'],
                            'cr_email' => $studentInfo['appl_email'],
                            'cr_status' => 1,
                            'cr_datesent' => date('Y-m-d H:i:s')
                        );
                        $vmodel->insertComposeRec($data_compose_rec);

                        $data_compose_attach = array(
                            'ca_comp_id'=>$comId,
                            'ca_filename'=>$name.'.pdf',
                            'ca_fileurl'=>$name.'.pdf',
                            'ca_filesize'=>0
                        );
                        $vmodel->insertComposeAttach($data_compose_attach);

                        //bar from exam
                        $exam_bar_data = array(
                            'exam_bar'=>0
                        );
                        $this->model->barExam($exam_bar_data, $groupInfo['IdStudentRegSubjects']);
                    }
                }
            }
        }

        $this->_helper->flashMessenger->addMessage(array('success' => 'Attendance Reminder Send'));
        $this->_redirect($this->baseUrl . '/communication/attendance-reminder/index/');
    }

    public function historyAction(){
        $this->view->title = $this->view->translate('Attendance Reminder History');

        $form = new Registration_Form_SearchStudentAttendanceReminder(array('type'=>1));
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if ($form->isValid($formData)) {
                $list = $this->model->getHistory($formData);
                $this->view->list = $list;
                $this->view->formData = $formData;
            }

            $form->populate($formData);
        }
    }

    public function printHistoryAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $list = $this->model->getHistory($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_attendance_reminder_history.xls';
    }

    public function viewHistoryAction(){
        $this->view->title = $this->view->translate('Attendance Reminder History');

        $id = $this->_getParam('id', 0);

        if ($id != 0){
            $history = $this->model->viewHistory($id);

            if ($history){
                $attachment = $this->model->viewAttachment($id);

                $this->view->history = $history;
                $this->view->attachment = $attachment;
            }else{
                $this->_redirect($this->baseUrl . '/communication/attendance-reminder/index/');
            }
        }else{
            $this->_redirect($this->baseUrl . '/communication/attendance-reminder/index/');
        }
    }

    public function parseTag($id, $content, $groupInfo, $item, $type, $formData){
        $attendanceDB = new GeneralSetup_Model_DbTable_Attendance();
        $attendanceSetupDB = new Registration_Model_DbTable_AttendanceSetup();

        $studentInfo = $this->model->getStudentInfo($id);
        $calculate = $attendanceDB->calculateAttendance($groupInfo['IdCourseTaggingGroup'], $id, $item);
        $attendance_setup = $attendanceSetupDB->getLatestPercentage($studentInfo['IdProgram'], $studentInfo['IdProgramScheme'], $item, $type);
        //var_dump($attendance_setup);

        //date
        $date = date('d F Y');
        $content = str_replace('[Date]', $date, $content);

        //student name
        $studentName = $studentInfo['appl_fname'].' '.$studentInfo['appl_lname'];
        $content = str_replace('[Student Name]', $studentName, $content);

        //student Id
        $studentId = $studentInfo['registrationId'];
        $content = str_replace('[Student ID]', $studentId, $content);

        //group name
        $groupName = $groupInfo['SubCode'].' '.$groupInfo['SubjectName'];
        $content = str_replace('[group_name]', $groupName, $content);

        //semester name
        $semesterName = $groupInfo['SemesterMainName'];
        $content = str_replace('[Semester]', $semesterName, $content);

        //attendance percentage
        $attPercentage = $calculate['percentage'];
        $content = str_replace('[attendance_percentage]', $attPercentage, $content);

        //semester month
        if (isset($formData['month']) && count($formData['month']) > 0){
            $semmonth = '';
            $i = 0;
            if (count($formData['month']) > 1) {
                foreach ($formData['month'] as $month) {
                    $i++;

                    if ($i != count($formData['month'])) {
                        $semmonth .= date('F', strtotime($month)) . ', ';
                    } else {
                        $semmonth .= 'and ' . date('F', strtotime($month)) . ' ' . date('Y', strtotime($month));
                    }
                }
            }else{
                foreach ($formData['month'] as $month) {
                    $i++;

                    $semmonth .= date('F', strtotime($month)) . ' ' . date('Y', strtotime($month));
                }
            }

            $content = str_replace('[semester month]', $semmonth, $content);
        }

        //programme code
        $progCode = $studentInfo['ProgramCode'];
        $content = str_replace('[programme_code]', $progCode, $content);

        //percentage required
        $percentageReq = $attendance_setup['att_percentage'];
        $content = str_replace('[percentage_required]', $percentageReq, $content);
        $content = str_replace('[required_percentage]', $percentageReq, $content);

        //exam date
        $examStartDate = $this->model->getExamDate($groupInfo['IdSemesterMaster'], 0);
        $examEndDate = $this->model->getExamDate($groupInfo['IdSemesterMaster'], 1);
        $examDate = date('d F', strtotime($examStartDate['StartDate'])).' until '.date('d F Y', strtotime($examEndDate['EndDate']));
        $content = str_replace('[exam_date]', $examDate, $content);

        //course name
        $courseName = '1) '.$groupInfo['SubCode'].' '.$groupInfo['SubjectName'];
        $content = str_replace('[course_name]', $courseName, $content);

        return $content;
    }

    function generatePDF($html,$name,$url){

        $options = array(
            'content' => $html,
            'file_name' => $name,
            'file_extension' => 'pdf',
            'save_path' => $url,
            'save' => true,
            'css' => '@page { margin: 110px 50px 80px 50px}
                body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
                table.tftable tr {background-color:#ffffff;}
                table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
            'header' => '<script type="text/php">
                if ( isset($pdf) ) {
                $header = $pdf->open_object();

                $w = $pdf->get_width();
                $h = $pdf->get_height();
                $color = array(0,0,0);

                $img_w = 180;
                $img_h = 59;
                $pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

                // Draw a line along the bottom
                $font = Font_Metrics::get_font("Helvetica");
                $size = 6;
                $text_height = Font_Metrics::get_font_height($font, $size)+2;
                $y = $h - (3.5 * $text_height)-10;
                $pdf->line(10, $y, $w - 10, $y, $color, 1);

                // Draw a second line along the bottom
                $y = $h - (3.5 * $text_height)+10;
                $pdf->line(10, $y, $w - 10, $y, $color, 1);

                $pdf->close_object();

                $pdf->add_object($header, "all");
                }
                </script>',
            'footer' => '<script type="text/php">
                if ( isset($pdf) ) {
                $footer = $pdf->open_object();

                $font = Font_Metrics::get_font("Helvetica");
                $size = 6;
                $color = array(0,0,0);
                $text_height = Font_Metrics::get_font_height($font, $size)+2;

                $w = $pdf->get_width();
                $h = $pdf->get_height();


                // Draw a line along the bottom
                $y = $h - (3.5 * $text_height)-10;
                //$pdf->line(10, $y, $w - 10, $y, $color, 1);

                //1st row footer
                $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
                $width = Font_Metrics::get_text_width($text, $font, $size);
                $y = $h - (2 * $text_height)-20;
                $x = ($w - $width) / 2.0;

                $pdf->page_text($x, $y, $text, $font, $size, $color);

                //2nd row footer
                $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
                $width = Font_Metrics::get_text_width($text, $font, $size);
                $y = $h - (1 * $text_height)-20;
                $x = ($w - $width) / 2.0;

                $pdf->page_text($x, $y, $text, $font, $size, $color);



                $pdf->close_object();

                $pdf->add_object($footer, "all");
                }
                </script>'
        );

        //generate pdf
        generatePdf($options);
    }

    public function getMonthAction(){
        $id = $this->_getParam('id',null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $monthList = $this->model->getMonth($id);

        $json = Zend_Json::encode($monthList);

        echo $json;
        exit();
    }

    public function examBarAction(){
        $this->view->title = $this->view->translate('Exam Bar List');

        $form = new Communication_Form_ExamBarSearch();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->formData = $formData;

            $form->populate($formData);

            $list = $this->model->getExamBarList($formData);
            $this->view->list = $list;
        }
    }

    public function unbarAction(){
        $id = $this->_getParam('id',null);

        if ($id != null){
            //bar from exam
            $exam_bar_data = array(
                'exam_bar'=>1
            );
            $this->model->barExam($exam_bar_data, $id);

            $this->_helper->flashMessenger->addMessage(array('success' => 'Unbar Success'));
            $this->_redirect($this->baseUrl . '/communication/attendance-reminder/exam-bar/');
        }else{
            $this->_redirect($this->baseUrl . '/communication/attendance-reminder/index/');
        }
    }
}