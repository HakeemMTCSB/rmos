<?php
class Thesis_SetupController extends Base_Base 
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );
		$this->auth = Zend_Auth::getInstance();
		
	}
	
	/*
		Setup Index - Research Topics
	*/
	public function indexAction()
	{
		//title
    	$this->view->title = "Research Topic";

		$p_data = $this->thesisModel->getTopicData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function topicAddAction()
	{
		$this->view->title = "Research - Add Topic";
		
		$form = new Thesis_Form_Topic();
		
		$supervisor_form = new Thesis_Form_AddSupervisorSetup();		
		$this->view->supervisor_form = $supervisor_form;
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'r_topicname'				=> $formData['r_topicname'],
                            'r_category'				=> $formData['r_category'],
                            'r_maxcandidate'			=> $formData['r_maxcandidate'],
							'r_assignmentrequirement'	=> $formData['r_assignmentrequirement'],
                            'r_createdby'				=> $this->auth->getIdentity()->iduser,
                            'r_createddate'				=> new Zend_Db_Expr('NOW()'),
							'active'					=> $formData['active']
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New research topic succesfully added"));

			$topic_id = $this->thesisModel->addTopic($data);

			//add supervisor
			for($i=0;$i<count($formData['supervisor_userid']);)
			{
				//save file into db
				$data = array(
								'ps_pid'			=> $topic_id,
								'ps_type'			=> 'topics',
								'ps_supervisor_id'	=> $formData['supervisor_userid'][$i],
								'ps_addedby'		=> $this->auth->getIdentity()->iduser,
								'ps_addeddate'		=> new Zend_Db_Expr('NOW()')
							);
					
				$this->thesisModel->addSupervisorSetup($data);

				$i++;
			}
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'index'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function topicEditAction()
	{
		$this->view->title = "Research - Edit Topic";
		
		$form = new Thesis_Form_Topic();
	
		$id = $this->view->escape(strip_tags($this->_getParam('id')));

		$topic = $this->thesisModel->getTopicSingle($id);

		if ( empty($topic) )
		{
			throw new Exception('Invalid Topic ID');
		}
		
		$supervisor_form = new Thesis_Form_AddSupervisorSetup();		
		$this->view->supervisor_form = $supervisor_form;

		//get supervisors
		$supervisors = $this->view->supervisors = $this->thesisModel->getSupervisors($id, 'topics');
		$supervisor_ids = array();
		foreach ( $supervisors as $sup )
		{
			$supervisor_ids[] = $sup['ps_id'];
		}

		$this->view->supervisor_ids = implode(',',$supervisor_ids);

		$form->populate($topic);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'r_topicname'				=> $formData['r_topicname'],
                            'r_category'				=> $formData['r_category'],
                            'r_maxcandidate'			=> $formData['r_maxcandidate'],
							'r_assignmentrequirement'	=> $formData['r_assignmentrequirement'],
                            'r_updatedby'				=> $this->auth->getIdentity()->iduser,
                            'r_updateddate'				=> new Zend_Db_Expr('NOW()'),
							'active'					=> $formData['active']
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));


			$this->thesisModel->updateTopic($data,'r_id='.$id);

						
			//add supervisor
			for($i=0;$i<count($formData['supervisor_userid']);)
			{
				//save file into db
				$data = array(
								'ps_pid'			=> $id,
								'ps_type'			=> 'topics',
								'ps_supervisor_id'	=> $formData['supervisor_userid'][$i],
								'ps_addedby'		=> $this->auth->getIdentity()->iduser,
								'ps_addeddate'		=> new Zend_Db_Expr('NOW()'),
								'active'			=> $formData['active']
							);
					
				$this->thesisModel->addSupervisorSetup($data);

				$i++;
			}
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'topic-edit','id'=>$id),'default',true));
		}

		//views
		$this->view->form = $form;
	}
	
	/*
		Setup Index - Categories
	*/
	public function categoriesAction()
	{
		//title
    	$this->view->title = "Research Category";

		$p_data = $this->thesisModel->getCategoriesData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}
	
	public function categoriesAddAction()
	{
		$this->view->title = "Research - Add Categories";
		
		$form = new Thesis_Form_Categories();
		
		$supervisor_form = new Thesis_Form_AddSupervisorSetup();		
		$this->view->supervisor_form = $supervisor_form;
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();

			$data = array(
                            'rc_name'					=> $formData['rc_name'],
                            'rc_program'				=> $formData['rc_program'],
							'rc_maxcandidate'			=> $formData['rc_maxcandidate'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()'),
							'active'					=> $formData['active']
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New category succesfully added"));

			$status_id = $this->thesisModel->addCategories($data);

			//add supervisor
			for($i=0;$i<count($formData['supervisor_userid']);)
			{
				//save file into db
				$data = array(
								'ps_pid'			=> $status_id,
								'ps_type'			=> 'categories',
								'ps_supervisor_id'	=> $formData['supervisor_userid'][$i],
								'ps_addedby'		=> $this->auth->getIdentity()->iduser,
								'ps_addeddate'		=> new Zend_Db_Expr('NOW()')
							);
					
				$this->thesisModel->addSupervisorSetup($data);

				$i++;
			}
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'categories'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function categoriesEditAction()
	{
		$this->view->title = "Research - Edit Category";
		
		$form = new Thesis_Form_Categories();
	
		$id = $this->_getParam('id');

		$category = $this->thesisModel->getCategory($id);

		if ( empty($category) )
		{
			throw new Exception('Invalid Research Category ID');
		}
		
		$supervisor_form = new Thesis_Form_AddSupervisorSetup();		
		$this->view->supervisor_form = $supervisor_form;

		//get supervisors
		$supervisors = $this->view->supervisors = $this->thesisModel->getSupervisors($id, 'categories');
		$supervisor_ids = array();
		foreach ( $supervisors as $sup )
		{
			$supervisor_ids[] = $sup['ps_id'];
		}

		$this->view->supervisor_ids = implode(',',$supervisor_ids);

		$form->populate($category);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'rc_name'					=> $formData['rc_name'],
                            'rc_program'				=> $formData['rc_program'],
							'rc_maxcandidate'			=> $formData['rc_maxcandidate'],
                            'updated_by'				=> $this->auth->getIdentity()->iduser,
                            'updated_date'				=> new Zend_Db_Expr('NOW()'),
							'active'					=> $formData['active']
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->thesisModel->updateCategories($data,'rc_id='.$id);

			//add supervisor
			for($i=0;$i<count($formData['supervisor_userid']);)
			{
				//save file into db
				$data = array(
								'ps_pid'			=> $id,
								'ps_type'			=> 'categories',
								'ps_supervisor_id'	=> $formData['supervisor_userid'][$i],
								'ps_addedby'		=> $this->auth->getIdentity()->iduser,
								'ps_addeddate'		=> new Zend_Db_Expr('NOW()')
							);
					
				$this->thesisModel->addSupervisorSetup($data);

				$i++;
			}
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'categories-edit','id'=>$id),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	/*
		Setup Index - Research Status
	*/
	public function statusAction()
	{
		//title
    	$this->view->title = "Research Status";

		$p_data = $this->thesisModel->getStatusData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function statusAddAction()
	{
		$this->view->title = "Research - Add Status";
		
		$form = new Thesis_Form_Status();

		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'status_code'				=> $formData['code'],
                            'status_description'		=> $formData['description'],
                            'status_course'				=> $formData['course'],
                            'status_program'			=> $formData['program'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New status succesfully added"));

			$status_id = $this->thesisModel->addStatus($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'status'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function statusEditAction()
	{
		$this->view->title = "Research - Edit Status";
		
		$form = new Thesis_Form_Status();
	
		$id = $this->_getParam('id');

		$status = $this->thesisModel->getStatus($id);

		if ( empty($status) )
		{
			throw new Exception('Invalid Status ID');
		}
		
		$formstatus = array(
								'code'			=> $status['status_code'],
								'description'	=> $status['status_description'],
								'course'		=> $status['status_course'],
								'program'		=> $status['status_program']
							);

		$form->populate($formstatus);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'status_code'				=> $formData['code'],
                            'status_description'		=> $formData['description'],
                            'status_course'				=> $formData['course'],
                            'status_program'			=> $formData['program'],
                            'updated_by'				=> $this->auth->getIdentity()->iduser,
                            'updated_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->thesisModel->updateStatus($data,'status_id='.$id);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'status-edit','id'=>$id),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	/*
		Supervisor Setup
	*/
	public function supervisorAction()
	{
		//title
    	$this->view->title = "Research - Supervisor Roles";

		$p_data = $this->thesisModel->getSupervisorPaginate();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function supervisorAddAction()
	{
		$this->view->title = "Research - Add Supervisor Role";
		
		$form = new Thesis_Form_Supervisor();

		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'code'						=> $formData['code'],
                            'description'				=> $formData['description'],
                            'maxcandidate'				=> $formData['maxcandidate'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New supervisor role succesfully added"));

			$status_id = $this->thesisModel->addSupervisor($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'supervisor'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function supervisorEditAction()
	{
		$this->view->title = "Research - Edit Supervisor Role";
		
	
		$form = new Thesis_Form_Supervisor();
		
		$id = $this->_getParam('id');

		$supervisor = $this->thesisModel->getSupervisor($id);
		
		if ( empty($supervisor) )
		{
			throw new Exception('Invalid Supervisor ID');
		}
		
		
		$form->populate($supervisor);
		//$form->supervisor_userid->setAttrib('disabled','disabled');
		
		//repopulate
		/*$userDB = new GeneralSetup_Model_DbTable_User();
		$userList = $userDB->fngetUserDetails();
		$form->supervisor_userid->clearMultiOptions ();
		$form->supervisor_userid->addMultiOption('', '-- Select --');
		foreach ($userList as $user)
		{
			$form->supervisor_userid->addMultiOption($user['iduser'], $user['FullName']);
		}*/

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'code'						=> $formData['code'],
                            'description'				=> $formData['description'],
                            'maxcandidate'				=> $formData['maxcandidate'],
                            'updated_by'				=> $this->auth->getIdentity()->iduser,
                            'updated_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

			$status_id = $this->thesisModel->updateSupervisor($data, 'supervisor_id='.$id);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'supervisor'),'default',true));
		}

		//views
		$this->view->form = $form;
	}
	
	/*
		Examiner Setup
	*/
	public function examinerAction()
	{
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster ();

		//title
    	$this->view->title = "Research - Examiner Setup";
		
		$p_data = $this->thesisModel->getExaminerSetupData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}
	
	public function examinerAddAction()
	{
		$this->view->title = "Research - New Examiner Setup";
		
		$form = new Thesis_Form_ExaminerSetup();

		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'es_min'					=> $formData['es_min'],
                            'es_max'					=> $formData['es_max'],
                            'es_course'					=> $formData['es_course'],
                            'es_program'				=> $formData['es_program'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New examiner setup succesfully added"));

			$status_id = $this->thesisModel->addExaminerSetup($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'examiner'),'default',true));
		}

		//views
		$this->view->form = $form;
	}
	
	public function examinerEditAction()
	{
		$this->view->title = "Research - Edit Examiner Setup";
		
		$form = new Thesis_Form_ExaminerSetup();
		
		$id = $this->_getParam('id');

		$info = $this->thesisModel->getExaminerSetup($id);
		
		if ( empty($info) )
		{
			throw new Exception('Invalid Examiner Setup ID');
		}

		$form->populate($info);
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'es_min'					=> $formData['es_min'],
                            'es_max'					=> $formData['es_max'],
                            'es_course'					=> $formData['es_course'],
                            'es_program'				=> $formData['es_program'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$status_id = $this->thesisModel->updateExaminerSetup($data,'es_id='.(int) $id );
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'examiner-edit', 'id' => $id ),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	

	/*
		Setup Index - Research Interest
	*/
	public function interestAction()
	{
		//title
    	$this->view->title = "Research - Interest";

		$p_data = $this->thesisModel->getInterestData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function interestAddAction()
	{
		$this->view->title = "Research - Add Interest";
		
		$form = new Thesis_Form_Interest();

		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'i_title'					=> $formData['i_title'],
                            'i_description'				=> $formData['i_description'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New research interest succesfully added"));

			$status_id = $this->thesisModel->addInterest($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'interest'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function interestEditAction()
	{
		$this->view->title = "Research - Edit Interest";
		
	
		$form = new Thesis_Form_Interest();
		
		$id = $this->_getParam('id');

		$interest = $this->thesisModel->getInterest($id);
		
		if ( empty($interest) )
		{
			throw new Exception('Invalid Interest ID');
		}
		
		
		$form->populate($interest);
	

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'i_title'					=> $formData['i_title'],
                            'i_description'				=> $formData['i_description']
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

			$status_id = $this->thesisModel->updateInterest($data, 'i_id='.$id);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'interest'),'default',true));
		}

		//views
		$this->view->form = $form;
	}
	
	public function interestDeleteAction()
	{
		$form = new Thesis_Form_Interest();
		
		$id = $this->_getParam('id');

		$interest = $this->thesisModel->getInterest($id);
		
		if ( empty($interest) )
		{
			throw new Exception('Invalid Interest ID');
		}

		$this->thesisModel->deleteInterest($id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Interest successfully deleted"));

		$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'interest'),'default',true));

	}


	/*
		Setup Index - Reason
	*/
	public function reasonAction()
	{
		//title
    	$this->view->title = "Research - Reason for Applying";

		$p_data = $this->thesisModel->getReasonData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function reasonAddAction()
	{
		$this->view->title = "Research - Add New Reason";
		
		$form = new Thesis_Form_Interest();

		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'i_title'					=> $formData['i_title'],
                            'i_description'				=> $formData['i_description'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New reason succesfully added"));

			$status_id = $this->thesisModel->addReason($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'reason'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function reasonEditAction()
	{
		$this->view->title = "Research - Edit Reason";
		
	
		$form = new Thesis_Form_Interest();
		
		$id = $this->_getParam('id');

		$supervisor = $this->thesisModel->getReason($id);
		
		if ( empty($supervisor) )
		{
			throw new Exception('Invalid Reason ID');
		}
		
		
		$form->populate($supervisor);
	

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'i_title'					=> $formData['i_title'],
                            'i_description'				=> $formData['i_description']
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

			$status_id = $this->thesisModel->updateReason($data, 'i_id='.$id);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'reason'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	/*
		Setup Index - Activity
	*/
	public function activitiesAction()
	{
		//title
    	$this->view->title = "Research - Activities Setup";

		$p_data = $this->thesisModel->getActivitySetupData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function activitiesListAction()
	{
		//title
    	$this->view->title = "Research - Activities Setup";

		$type = $this->view->type = $this->_getParam('type');
		
		$p_data = $this->thesisModel->getActivitySetupData($type);
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function activitiesAddAction()
	{
		$this->view->title = "Research - Add New Activity";
		
		$form = new Thesis_Form_Activity();

		$type = $this->view->type = $this->_getParam('type');

		if ( $type == '' )
		{
			throw new Exception('Invalid Research Type');
		}

		$form->research_type->setValue($type);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'title'						=> $formData['title'],
                            'description'				=> $formData['description'],
							'research_type'				=> $formData['research_type'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New activity succesfully added"));

			$status_id = $this->thesisModel->addActivitySetup($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'activities-list','type'=>$formData['research_type']),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function activitiesEditAction()
	{
		$this->view->title = "Research - Edit Activity";
		
		$form = new Thesis_Form_Activity();

		$id = $this->_getParam('id');
		
		$activity = $this->view->activity = $this->thesisModel->getActivitySetup($id);

		if ( empty($activity) )
		{
			throw new Exception('Invalid Activity Setup ID');
		}
		
		$form->populate($activity);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'title'						=> $formData['title'],
                            'description'				=> $formData['description'],
							'research_type'				=> $formData['research_type'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$status_id = $this->thesisModel->updateActivitySetup($data, array('id = ?' => $id ));
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'activities-list', 'type' => $activity['research_type']),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function activitiesDeleteAction()
	{
		$id = $this->_getParam('id');
		
		$activity = $this->view->activity = $this->thesisModel->getActivitySetup($id);

		if ( empty($activity) )
		{
			throw new Exception('Invalid Activity Setup ID');
		}

		$this->thesisModel->deleteActivitySetup($id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Activity successfully deleted"));

		$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'activities-list','type' => $activity['research_type']),'default',true));

	}


	/*
		Setup Index - Activity
	*/

	public function colloquiumAction()
	{
		//title
		$this->view->title = "Research - Colloquium Setup";

		$p_data = $this->thesisModel->getColloquiumSetupData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function colloquiumAddAction()
	{
		$this->view->title = "Research - Add New Colloquium";
		
		$form = new Thesis_Form_Colloquium();

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'name'						=> $formData['name'],
                            'description'				=> $formData['description'],
							'date_from'					=> $formData['date_from'],
							'date_to'					=> $formData['date_to'],
							'url'						=> $formData['url'],
                            'created_by'				=> $this->auth->getIdentity()->iduser,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New colloquium succesfully added"));

			$status_id = $this->thesisModel->addColloquiumSetup($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'colloquium'),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function colloquiumEditAction()
	{
		$this->view->title = "Research - Edit Colloquium";
		
		$form = new Thesis_Form_Colloquium();

		$id = $this->_getParam('id');
		
		$colloquium = $this->view->activity = $this->thesisModel->getColloquiumSetup($id);

		if ( empty($colloquium) )
		{
			throw new Exception('Invalid Colloquium ID');
		}
		
		$form->populate($colloquium);

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'name'						=> $formData['name'],
                            'description'				=> $formData['description'],
							'date_from'					=> $formData['date_from'],
							'date_to'					=> $formData['date_to'],
							'url'						=> $formData['url'],
                            'updated_by'				=> $this->auth->getIdentity()->iduser,
                            'updated_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$status_id = $this->thesisModel->updateColloquiumSetup($data, array('id = ?' => $id ));
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'setup', 'action'=>'colloquium-edit', 'id' => $colloquium['id']),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	/* 
		not used anymore
	*/
	public static function researchType($type)
	{
		$return = '';

		switch ( $type )
		{
			case 'articleship': $return = 'Articleship';	break;
			case 'exemption':	$return = 'PPP';			break;
			case 'proposal':	$return = 'Proposal';		break;
		}

		return $return;
	}
}