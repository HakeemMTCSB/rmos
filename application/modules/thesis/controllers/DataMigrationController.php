<?php

class Thesis_DataMigrationController extends Base_Base
{
    public $eventModel;
    public $researchModel;

    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->researchModel = new Thesis_Model_DbTable_ResearchActivity();

        $this->lobjCommon = new App_Model_Common();
        $this->gintPageCount = empty($larrInitialSettings['noofrowsingrid']) ? "5" : $larrInitialSettings['noofrowsingrid'];

        $this->auth = Zend_Auth::getInstance();

        $this->db = Zend_Db_Table::getDefaultAdapter();

    }

    public function intakeMigrationAction()
    {
        $session = new Zend_Session_Namespace('intakeMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $intakes_all = $DB->intakeData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $intakes_all = $DB->intakeData(NULL, $formData['intake_code']);
                $session->Query = $intakes_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/intake-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $intake_icem = $DB->intakeIcemData();
                foreach ($intake_icem as $icem) {

                    $intakes = $DB->intakeData($icem['INTAKE_ID']);

                    if ($intakes['num'] == 0) {

                        $i++;

                        $data['IntakeId'] = $icem['INTAKE_ID'];
                        $data['IntakeDesc'] = $icem['MALAY_NAME'];

                        if (!empty($icem['INTAKE_NAME'])) {
                            $data['IntakeDefaultLanguage'] = $icem['INTAKE_NAME'];
                        } else {
                            $data['IntakeDefaultLanguage'] = $icem['MALAY_NAME'];
                        }


                        $startdate = date("Y-m-d", strtotime($icem['START_DATE']));
                        if (!empty($icem['START_DATE'])) {
                            $data['ApplicationStartDate'] = $startdate;
                        }

                        $enddate = date("Y-m-d", strtotime($icem['END_DATE']));
                        if (!empty($icem['END_DATE'])) {
                            $data['ApplicationEndDate'] = $enddate;
                        }

                        $data['sem_seq'] = '';
                        $data['MigratedCode'] = '';
                        $data['UpdDate'] = new Zend_Db_Expr('NOW()');
                        $data['UpdUser'] = $this->auth->getIdentity()->iduser;
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');

                        $db->insert('tbl_intake', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' intake was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'intake-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $intakes_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($intakes_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }

    public function branchMigrationAction()
    {
        $session = new Zend_Session_Namespace('branchMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $branch_all = $DB->branchData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $branch_all = $DB->branchData(NULL, $formData['branch_code']);
                $session->Query = $branch_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/branch-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $branch_icem = $DB->branchIcemData();
                foreach ($branch_icem as $icem) {

                    $branches = $DB->branchData($icem['BRANCH_ID']);

                    if ($branches['num'] == 0) {

                        $i++;

                        if (empty($icem['BRANCH_TEL'])) {
                            $icem['BRANCH_TEL'] = '';
                        }

                        $data['BranchName'] = $icem['BRANCH_DESC'];
                        $data['ShortName'] = $icem['BRANCH_SHORT'];
                        $data['BranchCode'] = $icem['BRANCH_ID'];
                        $data['IdType'] = 2;
                        $data['Active'] = 1;
                        $data['Phone2'] = $icem['BRANCH_TEL'];
                        $data['Currency'] = 1;
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');

                        $db->insert('tbl_branchofficevenue', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' branch was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'branch-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $branch_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($branch_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }

    public function semesterMigrationAction()
    {
        $session = new Zend_Session_Namespace('semesterMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $semester_all = $DB->semesterData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $semester_all = $DB->semesterData(NULL, $formData['semester_code']);
                $session->Query = $semester_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/semester-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $semester_icem = $DB->semesterIcemData();
                foreach ($semester_icem as $icem) {

                    $semesters = $DB->semesterData($icem['SEMESTERMAINCODE']);

                    if ($semesters['num'] == 0) {

                        $i++;

                        $data['SemesterMainName'] = $icem['SEMESTERMAINNAME'];
                        $data['SemesterMainDefaultLanguage'] = $icem['SEMESTERMAINDEFAULTLANGUAGE'];
                        $data['SemesterMainCode'] = $icem['SEMESTERMAINCODE'];
                        $data['description'] = $icem['DESCRIPTION'];
                        $data['AcademicYear'] = $icem['ACADEMICYEAR'];
                        $data['SemesterType'] = $icem['SEMESTERTYPE'];
                        $data['SemesterMainStartDate'] = $icem['SEMESTERMAINSTARTDATE'];
                        $data['SemesterMainEndDate'] = $icem['SEMESTERMAINENDDATE'];
                        $data['sequence'] = $icem['SEQUENCE'];
                        $data['IdScheme'] = $icem['IDSCHEME'];
                        $data['UpdDate'] = new Zend_Db_Expr('NOW()');
                        $data['UpdUser'] = $this->auth->getIdentity()->iduser;
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');

                        $db->insert('tbl_semestermaster', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' semester was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'semester-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $semester_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($semester_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }

    public function courseMigrationAction()
    {
        $session = new Zend_Session_Namespace('courseMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $course_all = $DB->courseData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $course_all = $DB->courseData(NULL, $formData['course_code']);
                $session->Query = $course_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/course-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $course_icem = $DB->courseIcemData();
                foreach ($course_icem as $icem) {

                    $courses = $DB->courseData($icem['SUBCODE']);

                    if ($courses['num'] == 0) {
                        if ($icem['SUBJECTNAME'] == '') {
                            $icem['SUBJECTNAME'] = 'No name registered in ICEM';
                            $icem['SUBJECTNAME'] = 'No name registered in ICEM';
                        }
                        $i++;

                        $faculty = $DB->getFacultyId($icem['IDFACULTY']);

                        $data['IdFaculty'] = $faculty['IdCollege'];
                        $data['SubjectName'] = $icem['SUBJECTNAME'];
                        $data['ShortName'] = $icem['SUBJECTNAME'];
                        $data['subjectMainDefaultLanguage'] = $icem['SUBJECTNAME'];
                        $data['SubCode'] = $icem['SUBCODE'];
                        $data['CourseType'] = 3;
                        $data['Active'] = 1;
                        if ($icem['CREDITHOURS'] == NULL) {
                            $icem['CREDITHOURS'] = 0;
                        }
                        $data['CreditHours'] = $icem['CREDITHOURS'];
                        $data['UpdDate'] = new Zend_Db_Expr('NOW()');
                        $data['UpdUser'] = $this->auth->getIdentity()->iduser;
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');

                        $db->insert('tbl_subjectmaster', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' course was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'course-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $course_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($course_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }

    public function programMigrationAction()
    {
        $session = new Zend_Session_Namespace('programMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $program_all = $DB->programData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $program_all = $DB->programData(NULL, $formData['program_code']);
                $session->Query = $program_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/program-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $program_icem = $DB->programIcemData();
                foreach ($program_icem as $icem) {

                    $programs = $DB->programData($icem['PROGRAMCODE']);

                    if ($programs['num'] == 0) {

                        $i++;

                        $data['ProgramName'] = $icem['PROGRAMNAME'];
                        $data['ProgramCode'] = $icem['PROGRAMCODE'];
                        $data['ShortName'] = $icem['PROGRAMCODE'];
                        if ($icem['TOTALCREDITHOURS'] == NULL) {
                            $icem['TOTALCREDITHOURS'] = 0;
                        }
                        $data['TotalCreditHours'] = $icem['TOTALCREDITHOURS'];
                        $data['ArabicName'] = $icem['ARABICNAME'];
                        if ($icem['IDSCHEME'] == 5) {
                            $award = 628;
                        } elseif ($icem['IDSCHEME'] == 6) {
                            $award = 629;
                        } else {
                            $award = 0;
                        }
                        $data['Award'] = $award;
                        $data['AwardName'] = $icem['PROGRAMNAME'];
                        $data['Active'] = 1;
                        $data['IdScheme'] = $icem['IDSCHEME'];
                        $data['UpdDate'] = new Zend_Db_Expr('NOW()');
                        $data['UpdUser'] = $this->auth->getIdentity()->iduser;
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');

                        $db->insert('tbl_program', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' program was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'program-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $program_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($program_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }

    public function studentMigrationAction()
    {
        $session = new Zend_Session_Namespace('studentMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $student_all = $DB->studentData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $student_all = $DB->studentData(NULL, $formData['student_id']);
                $session->Query = $student_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/student-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $student_icem = $DB->studentIcemData();
                foreach ($student_icem as $icem) {

                    $students = $DB->studentData($icem['APPL_USERNAME']);

                    if ($students['num'] == 0) {

                        $i++;

                        $data['appl_fname'] = $icem['APPL_FNAME'];
                        $data['appl_idnumber'] = $icem['APPL_IDNUMBER'];
                        $data['appl_address1'] = $icem['APPL_ADDRESS1'];
                        $data['appl_postcode'] = $icem['APPL_POSTCODE'];
                        $data['appl_email'] = $icem['APPL_EMAIL'];
                        $data['appl_username'] = $icem['APPL_USERNAME'];
                        $data['appl_password'] = md5(123456);
                        $data['appl_dob'] = $icem['APPL_DOB'];
                        $data['appl_gender'] = $icem['APPL_GENDER'];
                        $data['appl_phone_home'] = $icem['PHONE_HOME'];
                        $data['appl_phone_mobile'] = $icem['PHONE_MOBILE'];
                        $data['appl_phone_office'] = $icem['PHONE_OFFICE'];
                        $data['appl_prefer_lang'] = 2;
                        $data['appl_nationality'] = 121;
                        $data['appl_type_nationality'] = 547;
                        $data['appl_category'] = 579;
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');
                        $db->insert('student_profile', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' student profile was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'student-migration'), 'default', true));
            }
        }
        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $student_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($student_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }

    public function studentRegistrationMigrationAction()
    {
        $session = new Zend_Session_Namespace('studentRegistrationMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $studentreg_all = $DB->studentRegistrationData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $studentreg_all = $DB->studentRegistrationData(NULL, $formData['student_id']);
                $session->Query = $studentreg_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/student-registration-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $studentreg_icem = $DB->studentRegistrationIcemData();
                foreach ($studentreg_icem as $icem) {

                    $studentregs = $DB->studentRegistrationData($icem['STUDENT_ID']);

                    if ($studentregs['num'] == 0) {

                        $program = $DB->getProgramID($icem['PROGRAM']);
                        $sp = $DB->getStudentProfileIDOnly($icem['STUDENT_ID']);
                        $intake = $DB->getIntakeID($icem['INTAKE']);
                        $branch = $DB->getBranchID($icem['BRANCH']);

                        if($sp) {
                            $i++;

                            $data['sp_id'] = $sp['id'];
                            $data['registrationId'] = $icem['STUDENT_ID'];
                            $data['IdLandscape'] = 1;
                            $data['IdProgram'] = $program['IdProgram'];
                            $data['IdProgramScheme'] = $program['IdProgram'];
                            $data['IdBranch'] = $branch['IdBranch'];
                            $data['IdIntake'] = $intake['IdIntake'];
                            $data['profileStatus'] = 92;
                            $data['student_type'] = 740;
                            $data['icem_migration'] = 'Migrated';
                            $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');
                            $db->insert('tbl_studentregistration', $data);
                        }
                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' student registration was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'student-registration-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $studentreg_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($studentreg_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;

    }

    public function researchMigrationAction()
    {
        $session = new Zend_Session_Namespace('researchMigration');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

        $research_all = $DB->researchData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $research_all = $DB->researchData(NULL, $formData['student_id']);
                $session->Query = $research_all;
                $session->Params = $formData;
                $this->_redirect($this->baseUrl . '/thesis/data-migration/research-migration/search/1');
            }

            if (isset($formData['upload'])) {
                $researchIcemData = $DB->researchIcemData($formData['sem'],$formData['level']);
                foreach ($researchIcemData as $icem) {

                    $sp = $DB->getStudentProfileID($icem['COURSEREGIS_STUD_ID']);
                    $sem = $DB->getSemesterID($icem['SEMESTER']);
                    $course = $DB->getCourseID($icem['COURSEREGIS_COURSE_ID']);

                    $param = array(
                        'student_id'        => $sp['IdStudentRegistration'],
                        'semester_start'    => $sem['IdSemesterMaster'],
                        'course_id'         => $course['IdSubject'],
                    );

                    if($param['student_id'] != '' && $param['semester_start'] != '' && $param['course_id'] != ''){
                        $researches = $DB->researchData($param);
                    }else{
                        $researches['num'] = 1;
                    }

                    if ($researches['num'] == 0) {

                        $i++;

                        $data['student_id'] = $sp['IdStudentRegistration'];
                        $data['p_status'] = 2;
                        $data['activity_id'] = 1057;
                        $data['semester_start'] = $sem['IdSemesterMaster'];
                        $data['applied_date'] = $sem['SemesterMainStartDate'];
                        $data['course_id'] = $course['IdSubject'];
                        $data['pd_status'] = 6;
                        $data['created_date'] = date('Y-m-d H:i:s');
                        $data['icem_migration'] = 'Migrated';
                        $data['icem_migration_date'] = new Zend_Db_Expr('NOW()');
                        $db->insert('thesis_proposal', $data);

                    } else {

//                nothing

                    }

                }
                notify('success', $i . ' research registration was succefully imported.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'research-migration'), 'default', true));
            }
        }

        if ($this->_getParam('search', 0)) {
            if (isset($session->Query) && isset($session->Query)) {
                $research_all = $session->Query;
                $formData = $session->Params;
                $this->view->search = $formData;
            }
        } else {
            unset($session->Query);
            unset($session->Params);
        }

        $paginator = Zend_Paginator::factory($research_all);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->count = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;

        $listSem = $DB->getSemester();
        $this->view->listSem = $listSem;
    }

    public function researchDetailMigrationAction()
    {
        $session = new Zend_Session_Namespace('researchdetailMigration');
        $session->setExpirationSeconds(60 * 10);

        $DB = new Thesis_Model_DbTable_DataMigration();

        $research_all = $DB->getResearchDetail();
        
        foreach ($research_all as $key => $research){

            foreach ($research['each_research'] as $res){

                if($res['p_title'] == NULL) {
                    $data = array(
                        'p_title' => $research['p_title']
                    );

                    $DB->updateProposal($data, 'p_id = ' . (int)$res['p_id']);

                    foreach ($research['each_supervisor'] as $sup) {

                        $data2['ps_pid'] = $res['p_id'];
                        $data2['ps_type'] = $sup['ps_type'];
                        $data2['ps_supervisor_id'] = $sup['ps_supervisor_id'];
                        $data2['ps_supervisor_role'] = $sup['ps_supervisor_role'];
                        $data2['ps_supervisor_type'] = $sup['ps_supervisor_type'];
                        $data2['ps_active'] = $sup['ps_active'];
                        $data2['ps_modified_by'] = $sup['ps_modified_by'];
                        $data2['ps_modified_date'] = $sup['ps_modified_date'];
                        $data2['ps_addedby'] = $sup['ps_addedby'];
                        $data2['ps_addeddate'] = $sup['ps_addeddate'];
                        $data2['ps_notified'] = $sup['ps_notified'];
                        $data2['ps_notified_date'] = $sup['ps_notified_date'];
                        $data2['ps_pdf'] = $sup['ps_pdf'];

                        $supervisor_id = $DB->addSupervisor($data2);

                        $data3['pid'] = $res['p_id'];
                        $data3['ps_id'] = $supervisor_id;
                        $data3['action'] = $sup['action'];
                        $data3['type'] = $sup['type'];
                        $data3['reason'] = $sup['reason'];
                        $data3['user_id'] = $sup['user_id'];
                        $data3['created_date'] = $sup['createdate_history'];
                        $data3['created_by'] = $sup['createdby_history'];

                        $DB->addSupervisorHistory($data3);
                    }
                }
            }
        }

        $allsupervisor = $DB->getSupervisorAll();

        foreach ($allsupervisor as $as){

            $history = $DB->getSupervisorHistory($as['ps_pid']);

            if($history['num'] == 0){
                $data4['pid'] = $as['ps_pid'];
                $data4['ps_id'] = $as['ps_id'];
                $data4['action'] = 'ADD';
                $data4['type'] = $as['ps_type'];
                $data4['reason'] = '';
                $data4['user_id'] = $as['ps_supervisor_id'];
                $data4['created_date'] = $as['ps_addeddate'];
                $data4['created_by'] = $as['ps_addedby'];

                $DB->addSupervisorHistory($data4);
            }
        }

        notify('success', 'Research details succefully updated.');
        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal'), 'default', true));
    }

    public function studentRegistrationUpdateAction()
    {
        $session = new Zend_Session_Namespace('studentRegistrationUpdate');
        $session->setExpirationSeconds(60 * 10);

        $db = $this->db;

        $DB = new Thesis_Model_DbTable_DataMigration();

//        $studentreg_all = $DB->studentRegistrationData();

        $i = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

//            if (isset($formData['search'])) {
//                $studentreg_all = $DB->studentRegistrationData(NULL, $formData['student_id']);
//                $session->Query = $studentreg_all;
//                $session->Params = $formData;
//                $this->_redirect($this->baseUrl . '/thesis/data-migration/student-registration-update/search/1');
//            }

            if (isset($formData['upload'])) {
                $studentreg_icem = $DB->studentRegistrationIcemDataUpdate();
                foreach ($studentreg_icem as $icem) {

                    $studentregs = $DB->studentRegistrationData($icem['STUDENT_ID']);

                    if ($studentregs['num'] == 0) {

                    } else {
                        $pstatus = ucfirst($icem['STATUS']);
//                        print_r($pstatus);exit;
                        $def = $DB->getProfileStatusId($pstatus);
                        if($def) {
                            $data = array(
                                'profileStatus' => $def['idDefinition']
                            );
                            $DB->updateRegistration($data, 'IdStudentRegistration = ' . (int)$studentregs['IdStudentRegistration']);
                        $i++;
                        }
                    }

                }
                notify('success', $i . ' student registration was succefully updated.');
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'data-migration', 'action' => 'student-registration-update'), 'default', true));
            }
        }

//        if ($this->_getParam('search', 0)) {
//            if (isset($session->Query) && isset($session->Query)) {
//                $studentreg_all = $session->Query;
//                $formData = $session->Params;
//                $this->view->search = $formData;
//            }
//        } else {
//            unset($session->Query);
//            unset($session->Params);
//        }
//
//        $paginator = Zend_Paginator::factory($studentreg_all);
//        $paginator->setItemCountPerPage(25);
//        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
//        $this->view->count = $paginator->getTotalItemCount();
//        $this->view->paginator = $paginator;

    }

}