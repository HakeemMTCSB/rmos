<?php

class Thesis_RegistrationController extends Base_Base
{
    public $regModel;
    public $thesisModel;
    public $defModel;
    public $session;
    public $emailModel;

    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();
        $this->lobjCommon = new App_Model_Common();
        $this->gintPageCount = empty($larrInitialSettings['noofrowsingrid']) ? "5" : $larrInitialSettings['noofrowsingrid'];

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

    }

    public function indexAction()
    {
    }

    /*
        Registration - Proposal
    */
    public function proposalAction()
    {
        //title
        $this->view->title = "Proposal Registration";

        $this->view->form = new Thesis_Form_SearchProposal();

        $pageCount = 50;
        $query = $this->regModel->getProposalData(null, 'sql');

        $session = new Zend_Session_Namespace('ThesisRegistrationIndex');
        $session->setExpirationSeconds(60 * 10);

        $formData = '';
        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $this->view->form->populate($formData);
            $query = $this->regModel->getProposalData($formData, 'sql');

            $session->corporateQuery = $query;
            $session->corporateParams = $formData;
            $this->_redirect($this->baseUrl.'/thesis/registration/proposal/search/1');
        }

        if($this->_getParam('search', 0)) {
            if(isset($session->corporateQuery) && isset($session->corporateQuery)) {
                $query    = $session->corporateQuery;
                $formData = $session->corporateParams;
                $this->view->form->populate($formData);
            }
        } else {
            unset($session->corporateQuery);
            unset($session->corporateParams);
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->formData = $formData;
        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
    }

    public function proposalActivityAction()
    {
        $this->view->title = "Registration - Activity";

//        $form = new Thesis_Form_EditProposal(array('full' => true));
//
//        $supervisor_form = new Thesis_Form_AddSupervisor();
//        $this->view->supervisor_form = $supervisor_form;
//
//        $examiner_form = new Thesis_Form_AddExaminer();
//        $this->view->examiner_form = $examiner_form;

        $id = $this->_getParam('id');
//        $this->view->tab = $this->_getParam('tab');

        //get proposal info
        $proposal = $this->regModel->getProposal($id);
        $activities = $this->regModel->getActivities($id, $proposal['course_id']);

        $proposal_latest_update = $this->regModel->getProposalLatest($proposal['student_id']);

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
        $supervisors_latest = $this->view->supervisors_latest = $this->regModel->getSupervisors($proposal_latest_update['p_id'], 'proposal');

        $this->view->id = $id;
        $this->view->student_id = $proposal['student_id'];
        $this->view->activities = $activities;
        $this->view->proposal = $proposal;
        $this->view->proposal_latest = $proposal_latest_update;
    }

    public function proposalActivityAddAction()
    {
        $this->view->title = "Registration - Add Activity";

        $id = $this->_getParam('id');
        $activity_id = $this->_getParam('activity_id');
        $student_id = $this->_getParam('student_id');

        //get proposal info
        $proposal = $this->regModel->getProposal($id);
        $regdate = date("Y-m-d");
        $regsem = $this->regModel->getRegisterSem($regdate, $student_id);

        //add proposal
        $data = array(
            'pid'             => $proposal['p_id'],
            'student_id'      => $proposal['student_id'],
            'p_title'         => $proposal['p_title'],
            'p_category'      => $proposal['p_category'] ?? null,
            'p_topic'         => $proposal['p_topic'] ?? null,
            'p_description'   => $proposal['p_description'] ?? null,
            'meeting_date'    => $proposal['meeting_date'] ?? null,
            'p_status'        => $proposal['p_status'],
            'activity_id'     => $activity_id,
            'created_by'      => $this->auth->getIdentity()->iduser,
            'created_date'    => new Zend_Db_Expr('NOW()'),
            'created_role'    => 'admin',
            'completion_date' => $proposal['completion_date'] ?? null,
            'semester_start'  => $regsem['IdSemesterMaster'] ?? null,
            'semester_end'    => $proposal['semester_end'] ?? null,
            'course_id'       => $proposal['course_id'],
            'applied_date'    => new Zend_Db_Expr('NOW()'),
            'pd_status'       => $this->thesisModel->getStatusByCode('applied', 1)
        );

        $proposal_id = $this->regModel->addProposal($data);

        //add supervisor
        $supervisors = $this->regModel->getSupervisors($id, 'proposal');

        if(count($supervisors) > 0) {

            foreach ($supervisors as $sps) {

                $data = array(
                    'ps_pid'             => $proposal_id,
                    'ps_type'            => 'proposal',
                    'ps_supervisor_id'   => $sps['ps_supervisor_id'],
                    'ps_supervisor_type' => $sps['ps_supervisor_type'],
                    'ps_supervisor_role' => $sps['ps_supervisor_role'],
                    'ps_active'          => $sps['ps_active'],
                    'ps_addedby'         => $this->auth->getIdentity()->iduser,
                    'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                );

                $supervisor_id = $this->regModel->addSupervisor($data);

                $this->regModel->addSupervisorHistory([
                    'pid'          => $proposal_id,
                    'ps_id'          => $supervisor_id,
                    'action'       => 'ADD',
                    'type'         => 'proposal',
                    'reason'      => $sps['reason'],
                    'user_id'      => $sps['ps_supervisor_id'],
                    'created_date' => new Zend_Db_Expr('NOW()'),
                    'created_by'   => $this->auth->getIdentity()->iduser
                ]);
            }
        }

        //send email
        $submit = $this->regModel->getProposal($proposal_id);
        $getactivity = $this->regModel->getActivity($activity_id);

//        print_r($submit);exit;

//        //get supervisors
//        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($submit['p_id'], 'proposal');

        foreach ($supervisors as $sup) {

            $emailDb = new App_Model_Email();
            $commDb = new Communication_Model_DbTable_Template();

            if($submit['DefinitionCode'] == 'viva') {
                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-application-viva', 0, 1);
            }else {
                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-application', 0, 1);
            }
            $template = $gettemplate[0];

            $submit['ActivityName'] = $getactivity['ActivityName'];

            $submit['research_type'] = 'proposal';

            if (!isset($submit)) {
                throw new Exception('Invalid Research');
            }

            if (empty($template)) {
                throw new Exception('Invalid Template. Did you change the template name recently?');
            }

            $submit['supervisor_info'] = $sup['supervisor_name'];

            //email
            $dataEmail = array(
                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $sup['supervisor_email'],
                'subject' => $template['tpl_name'],
                'content' => Thesis_Model_DbTable_Registration::parseContent($submit, $template['tpl_content']),
                'date_que' => date('Y-m-d H:i:s')
            );

            $emailDb->add($dataEmail);

            if($sup['supervisor_email2'] != NULL){
                $dataEmail2 = array(
                    'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $sup['supervisor_email2'],
                    'subject' => $template['tpl_name'],
                    'content' => Thesis_Model_DbTable_Registration::parseContent($submit, $template['tpl_content']),
                    'date_que' => date('Y-m-d H:i:s')
                );

                $emailDb->add($dataEmail2);
            }
        }
        //send email

        $this->_helper->flashMessenger->addMessage(array('success' => "New activity succesfully added"));
        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-activity', 'id' => $proposal['p_id']), 'default', true));

    }

    public function proposalExportAction()
    {

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $p_data = $this->regModel->getProposalData($formData, 'array');

            if (isset($formData['lecturer']) && $formData['lecturer'] != '') {
                if ($p_data) {
                    //dd($p_data); exit;
                    foreach ($p_data as $p_key => $proposal) {
                        $lecturers = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

                        //dd($lecturers); exit;

                        $lecArr = array(0);
                        if ($lecturers) {
                            foreach ($lecturers as $lecturer) {
                                array_push($lecArr, $lecturer['IdStaff']);
                            }
                        }

                        if (!in_array($formData['lecturer'], $lecArr)) {
                            unset($p_data[$p_key]);
                        } else {

                        }
                    }
                }
            }
        }

        if ($p_data) {
            foreach ($p_data as $p_key => $proposal) {
                $lecturers = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
                $examiners = $this->regModel->getExaminers($proposal['p_id'], 'proposal');

                $p_data[$p_key]['lecturer'] = $lecturers;
                $p_data[$p_key]['examiner'] = $examiners;
            }
        }

        ///dd($p_data); exit;

        $this->view->paginator = $p_data;

        $this->view->filename = date('Ymd') . '_thesis_proposal.xls';
    }

    public function proposalAddAction()
    {
        $this->view->title = "Registration - Nomination of Supervisor";

        $form = new Thesis_Form_Proposal();

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        $registration = $this->regModel->getRegistration('registration', 'Research Type');

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                
                $check = $this->regModel->checkPid($formData['student_id'], $formData['course_id'], $formData['semester_start']);

//                if ($check['p_id'] == '') {
//                    $pid = 0;
//                } else {
//                    $pid = $check['p_id'];
//                }

                if ($check['num'] > 0) {

                    $this->_helper->flashMessenger->addMessage(array('success' => "This student already registered the course for selected semester"));

                    $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-add'), 'default', true));
                }

                //add proposal
                $data = array(
                    'pid'             => 0,
                    'student_id'      => $formData['student_id'],
                    'p_title'         => $formData['p_title'],
                    'p_category'      => $formData['p_category'] ?? null,
                    'p_topic'         => $formData['p_topic'] ?? null,
                    'p_description'   => $formData['p_description'] ?? null,
                    'meeting_date'    => $formData['meeting_date'] ?? null,
                    'p_status'        => $formData['p_status'],
                    'activity_id'     => $registration['idDefinition'],
                    'created_by'      => $this->auth->getIdentity()->iduser,
                    'created_date'    => new Zend_Db_Expr('NOW()'),
                    'created_role'    => 'admin',
                    'completion_date' => $formData['completion_date'] ?? null,
                    'semester_start'  => $formData['semester_start'] ?? null,
                    'semester_end'    => $formData['semester_end'] ?? null,
                    'course_id'       => $formData['course_id'],
                    'applied_date'    => $formData['applied_date'],
                    'pd_date'         => new Zend_Db_Expr('NOW()'),
                    'pd_status'       => $this->thesisModel->getStatusByCode('applied', 1)
                );

                $proposal_id = $this->regModel->addProposal($data);

                $p_status = $this->thesisModel->getStatus($formData['p_status']);
                $this->regModel->saveSubmission($proposal_id, 'registration', $p_status['status_code']);

                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    $adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $proposal_id,
                                'af_type'     => 'proposal',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db


                    $data = array(
                        'ps_pid'             => $proposal_id,
                        'ps_type'            => 'proposal',
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_type' => $formData['supervisor_type'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_active'          => $formData['supervisor_active'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $supervisor_id = $this->regModel->addSupervisor($data);

                    $this->regModel->addSupervisorHistory([
                        'pid'          => $proposal_id,
                        'action'       => 'ADD',
                        'type'         => 'proposal',
                        'user_id'      => $formData['supervisor_userid'][$i],
                        'created_date' => new Zend_Db_Expr('NOW()'),
                        'created_by'   => $this->auth->getIdentity()->iduser
                    ]);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {
                    //save file into db
                    $data = array(
                        'ex_pid'          => $proposal_id,
                        'ex_type'         => 'proposal',
                        'ex_examiner_id'  => $formData['examiner_userid'][$i],
                        'ex_active'       => $formData['examiner_active'][$i],
                        'ex_exminer_role' => $formData['examiner_role'][$i],
                        'ex_addedby'      => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'    => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }

                //send registered email
                //get supervisors
                $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal_id, 'proposal');

                $supervisor_name = array();
                foreach ($supervisors as $sup_name) {

                    $supervisor_name[] = $sup_name['supervisor_name'];

                }

                $sname = "<p>" . implode('<br>', $supervisor_name) . "</p>";

                if ($formData['p_status'] == 2) {

                    $emailDb = new App_Model_Email();
                    $commDb = new Communication_Model_DbTable_Template();

                    $research = $this->regModel->getProposal($proposal_id);
                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'registration', 0, 1);
                    $template = $gettemplate[0];

                    $research['research_type'] = 'proposal';

                    if (!isset($research)) {
                        throw new Exception('Invalid Research');
                    }

                    if (empty($template)) {
                        throw new Exception('Invalid Template. Did you change the template name recently?');
                    }

                    $research['supervisor_info'] = $sname;

                    //email
                    $dataEmail = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                        'subject'         => $template['tpl_name'],
                        'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                        'date_que'        => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail);
                }
                //send registered email

                $this->_helper->flashMessenger->addMessage(array('success' => "New proposal registration succesfully added"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-activity', 'id' => $proposal_id), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }

    /*
        Edit
    */
    public function proposalEditAction()
    {
        $this->view->title = "Registration - Nomination of Supervisor";

        $form = new Thesis_Form_EditProposal(array('full' => true));

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        $id = $this->_getParam('id');
        $activity_id = $this->_getParam('activity_id');
        $this->view->tab = $this->_getParam('tab');

        //get proposal info
        $proposal = $this->view->proposal = $this->regModel->getProposalActivity($id, $activity_id);
//        print_r($proposal);exit;
        $this->view->id = $id;
//        $form->p_title->setAttrib('disabled', 'disabled');

        if (empty($proposal)) {
            throw new Exception('Invalid Proposal ID');
        }

        //get files
        $uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($proposal['p_id'], 'proposal');

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

        $supervisor_ids = array();
        foreach ($supervisors as $sup) {
            $supervisor_ids[] = $sup['ps_id'];
        }

        $this->view->supervisor_ids = implode(',', $supervisor_ids);

        //check main supervisor
        $supervisor_roles = array();
        $mainrole = '';

        foreach ($supervisors as $role) {

            $supervisor_roles[] = $role['ps_supervisor_role'];

            if ($role['ps_supervisor_role'] == 1) {
                $mainrole = 'selected';
            }

        }

        $this->view->supervisor_roles = implode(',', $supervisor_roles);

        $this->view->mainrole = $mainrole;


        //get examiners
        $examiners = $this->view->examiners = $this->regModel->getExaminers($proposal['p_id'], 'proposal');
        $examiners_ids = array();
        foreach ($examiners as $exp) {
            $exminers_ids[] = $exp['ex_id'];
        }

        $this->view->examiners_ids = implode(',', $examiners_ids);

        //repopulate form
        $form->populate($proposal);

        $this->view->activity_id = $proposal['activity_id'];

        $this->view->proposal = $proposal;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                //update proposal
                $data = array(
                    'p_title'         => $formData['p_title'],
                    'student_id'      => $formData['student_id'],
                    'p_category'      => $formData['p_category'],
                    'p_topic'         => $formData['p_topic'],
                    'p_description'   => $formData['p_description'],
                    'p_status'        => $formData['p_status'],
                    'activity_id'     => $formData['activity_id'],
                    'meeting_date'    => $formData['meeting_date'],
                    'updated_by'      => $this->auth->getIdentity()->iduser,
                    'updated_date'    => new Zend_Db_Expr('NOW()'),
                    'archive'         => $formData['archive'] ?? null,
                    'completion_date' => $formData['completion_date'],
                    'semester_start'  => $formData['semester_start'],
                    'semester_end'    => $formData['semester_end'],
                    'course_id'       => $formData['course_id'],
                    'applied_date'    => $formData['applied_date'],
                    'pd_status'       => isset($formData['submit_proposal']) ? $this->thesisModel->getStatusByCode('applied', 1) : $proposal['pd_status'],
                    'pd_by'           => isset($formData['submit_proposal']) ? $this->auth->getIdentity()->iduser : $proposal['pd_by'],
                    'pd_date'         => isset($formData['submit_proposal']) ? new Zend_Db_Expr('NOW()') : $proposal['pd_date'],
                    'pd_role'         => isset($formData['submit_proposal']) ? 'admin' : $proposal['pd_role'],
                );

                $this->regModel->updateProposal($data, 'p_id = ' . (int)$proposal['p_id']);


                //insert/update thesis_submission  //applied
                if (isset($formData['submit_proposal'])) {
                    $this->regModel->saveSubmission($proposal['p_id'], 'proposal', 'applied');
                } else {
                    $p_status = $this->thesisModel->getStatus($formData['p_status']);
                    if ($proposal['p_status'] != $formData['p_status']) {
                        $this->regModel->saveSubmission($proposal['p_id'], 'registration', $p_status['status_code']);
                    }
                }

                $proposal_id = $proposal['p_id'];

                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    //$adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $proposal_id,
                                'af_type'     => 'proposal',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db

                    $data = array(
                        'ps_pid'             => $proposal_id,
                        'ps_type'            => 'proposal',
                        'ps_supervisor_type' => $formData['supervisor_type'][$i],
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $supervisor_id = $this->regModel->addSupervisor($data);

                    $this->regModel->addSupervisorHistory([
                        'pid'          => $proposal_id,
                        'ps_id'          => $supervisor_id,
                        'action'       => 'ADD',
                        'type'         => 'proposal',
                        'reason'         => $formData['reason'][$i],
                        'user_id'      => $formData['supervisor_userid'][$i],
                        'created_date' => new Zend_Db_Expr('NOW()'),
                        'created_by'   => $this->auth->getIdentity()->iduser
                    ]);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {
                    //save file into db
                    $data = array(
                        'ex_pid'           => $proposal_id,
                        'ex_type'          => 'proposal',
                        'ex_examiner_id'   => $formData['examiner_userid'][$i],
                        'ex_examiner_role' => $formData['examiner_role'][$i],
                        'ex_active'        => $formData['examiner_active'][$i],
                        'ex_addedby'       => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'     => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }

                $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

                $supervisor_name = array();
                foreach ($supervisors as $sup_name) {

                    $supervisor_name[] = $sup_name['supervisor_name'];

                }

                $sname = "<p>" . implode('<br>', $supervisor_name) . "</p>";

                //send registered email
                if ($formData['p_status'] == 2) {

                    $emailDb = new App_Model_Email();
                    $commDb = new Communication_Model_DbTable_Template();

                    $research = $this->regModel->getProposal($proposal_id);
//                    print_r($research);exit;
                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'registration', 0, 1);
                    $template = $gettemplate[0];

                    $research['research_type'] = 'proposal';

                    if (!isset($research)) {
                        throw new Exception('Invalid Research');
                    }

                    if (empty($template)) {
                        throw new Exception('Invalid Template. Did you change the template name recently?');
                    }

                    $research['supervisor_info'] = $sname;

                    //email
                    $dataEmail = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                        'subject'         => $template['tpl_name'],
                        'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                        'date_que'        => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail);
                }
                //send registered email

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-edit', 'activity_id' => $proposal['activity_id'], 'id' => $id, 'student_id' => $proposal['student_id']), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }


    /*
        ARTICLESHIP
    */
    public
    function articleshipAction()
    {
        //title
        $this->view->title = "Research - Articleship";

        $p_data = $this->regModel->getArticleshipData(null, 'array');

        $this->view->form = new Thesis_Form_SearchProposal();

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->form->populate($formData);

            $p_data = $this->regModel->getArticleshipData($formData, 'array');

            if (isset($formData['lecturer']) && $formData['lecturer'] != '') {
                if ($p_data) {
                    //dd($p_data); exit;
                    foreach ($p_data as $p_key => $proposal) {
                        $lecturers = $this->regModel->getSupervisors($proposal['a_id'], 'articleship');

                        //dd($lecturers); exit;

                        $lecArr = array(0);
                        if ($lecturers) {
                            foreach ($lecturers as $lecturer) {
                                array_push($lecArr, $lecturer['IdStaff']);
                            }
                        }

                        if (!in_array($formData['lecturer'], $lecArr)) {
                            unset($p_data[$p_key]);
                        }
                    }
                }
            }
        }

        //$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator = $this->lobjCommon->fnPagination($p_data, $lintpage, $lintpagecount);
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    public
    function articleshipExportAction()
    {
        $p_data = $this->regModel->getArticleshipData(null, 'array');

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $p_data = $this->regModel->getArticleshipData($formData, 'array');

            if (isset($formData['lecturer']) && $formData['lecturer'] != '') {
                if ($p_data) {
                    //dd($p_data); exit;
                    foreach ($p_data as $p_key => $proposal) {
                        $lecturers = $this->regModel->getSupervisors($proposal['a_id'], 'articleship');

                        $lecArr = array(0);
                        if ($lecturers) {
                            foreach ($lecturers as $lecturer) {
                                array_push($lecArr, $lecturer['IdStaff']);
                            }
                        }

                        if (!in_array($formData['lecturer'], $lecArr)) {
                            unset($p_data[$p_key]);
                        }
                    }
                }
            }
        }

        if ($p_data) {
            foreach ($p_data as $p_key => $proposal) {
                $lecturers = $this->regModel->getSupervisors($proposal['a_id'], 'articleship');
                $examiners = $this->regModel->getExaminers($proposal['a_id'], 'articleship');

                $p_data[$p_key]['lecturer'] = $lecturers;
                $p_data[$p_key]['examiner'] = $examiners;
            }
        }

        //dd($p_data); exit;

        $this->view->paginator = $p_data;

        $this->view->filename = date('Ymd') . '_thesis_articleship.xls';
    }

    public
    function articleshipAddAction()
    {
        $this->view->title = "Research - New Articleship";

        $semesterDb = new Registration_Model_DbTable_Semester();
        $studentSemesterStatusDB = new CourseRegistration_Model_DbTable_Studentsemesterstatus();
        $subjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
        $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
        $studentDb = new Registration_Model_DbTable_Studentregistration();

        $auth = Zend_Auth::getInstance();
        $db = getDb();
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();

        $form = new Thesis_Form_Articleship();

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        //
        $interest = $this->thesisModel->getInterest();
        $this->view->interests = $interest;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $student = $studentDb->getData($formData['student_id']);

            $semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $student['IdScheme']));

            //get subject articleship
            $landscape = $landscapeDb->getData($student["IdLandscape"]);

            if ($landscape["LandscapeType"] == 42) { //for level landscape
                $policy = $subjectDb->levelPolicy($student['IdStudentRegistration'], $student['IdLandscape']);
            } else {
                $policy = array();

                throw new Exception('This student is not allowed to register for Articleship. Check Programme.');
            }

            $subjects = $subjectDb->getCourseOffer($student['IdLandscape'], $semester['IdSemesterMaster'], $landscape["LandscapeType"], $policy, $student['IdStudentRegistration'], 2);//2=coursetype articleship

            if (empty($subjects)) {
                throw new Exception('Cannot find any course linked to student\'s information related to research type');
            }

            if ($form->isValid($formData)) {
                $data = array(
                    'student_id'             => $formData['student_id'],
                    'semester_id'            => $formData['semester_id'],
                    'fieldofinterest'        => json_encode($formData['interest']),
                    'fieldofinterest_others' => $formData['fieldofinterest_others'],
                    'company'                => $formData['company'],
                    'designation'            => $formData['designation'],
                    'contactperson'          => $formData['contactperson'],
                    'address'                => $formData['address'],
                    'city'                   => $formData['city'],
                    'city_others'            => $formData['city_others'],
                    'state'                  => $formData['state'],
                    'state_others'           => $formData['state_others'],
                    'country'                => $formData['country'],
                    'postcode'               => $formData['postcode'],
                    'phoneno'                => $formData['phoneno'],
                    'faxno'                  => $formData['faxno'],
                    'email'                  => $formData['email'],
                    'isemployee'             => $formData['isemployee'],
                    'yearsofservice'         => $formData['yearsofservice'],
                    'status'                 => $this->thesisModel->getStatusByCode('APPLIED'),
                    'created_by'             => $this->auth->getIdentity()->iduser,
                    'created_date'           => new Zend_Db_Expr('NOW()'),
                    'created_role'           => 'admin',
                    'start_date'             => $formData['start_date'],
                    'end_date'               => $formData['end_date']
                );

                $articleship_id = $this->regModel->addArticleship($data);

                //student semester status
                //checking semester status exist or not
                $semesterStatus = $studentSemesterStatusDB->getSemesterStatusBySemester($student['IdStudentRegistration'], $semester['IdSemesterMaster']);

                if (empty($semesterStatus)) {
                    $dataSemStatus = array(
                        'IdStudentRegistration' => $student['IdStudentRegistration'],
                        'idSemester'            => $semester['IdSemesterMaster'],
                        'IdSemesterMain'        => $semester['IdSemesterMaster'],
                        'studentsemesterstatus' => 130,//registered
                        'UpdUser'               => $auth->getIdentity()->iduser,
                        'UpdDate'               => date("Y-m-d H:i:s"),
                        'UpdRole'               => 'admin',
                        'Level'                 => 1,
                    );

                    $db->insert('tbl_studentsemesterstatus', $dataSemStatus);
                    $semstatus_id = $db->lastInsertId();

                }

                if ($this->regModel->isRegistered($subjects[0]['IdSubject'], $semester['IdSemesterMaster'], $student['IdStudentRegistration']) == false) {

                    //regsubjects
                    $data = array(
                        'IdStudentRegistration' => $student['IdStudentRegistration'],
                        'IdSubject'             => $subjects[0]['IdSubject'],
                        'IdSemesterMain'        => $semester['IdSemesterMaster'],
                        'UpdUser'               => $auth->getIdentity()->iduser,
                        'UpdDate'               => date("Y-m-d H:i:s"),
                        'UpdRole'               => 'admin',
                        'Active'                => 0,
                    );

                    $db->insert('tbl_studentregsubjects', $data);
                    $regsub_id = $db->lastInsertId();

                    //add into history
                    $data = array(
                        'IdStudentRegistration' => $student['IdStudentRegistration'],
                        'IdSubject'             => $subjects[0]['IdSubject'],
                        'IdSemesterMain'        => $semester['IdSemesterMaster'],
                        'UpdUser'               => $auth->getIdentity()->iduser,
                        'UpdDate'               => date("Y-m-d H:i:s"),
                        'UpdRole'               => 'admin',
                        'Active'                => 0
                    );

                    $db->insert('tbl_studentregsubjects_history', $data);

                    //regsubjects_detail
                    $data = array(
                        'student_id'   => $student['IdStudentRegistration'],
                        'regsub_id'    => $regsub_id,
                        'semester_id'  => $semester['IdSemesterMaster'],
                        'subject_id'   => $subjects[0]['IdSubject'],
                        'item_id'      => 890,//item paper only
                        'section_id'   => 0,
                        'ec_country'   => 0,
                        'ec_city'      => 0,
                        'created_date' => new Zend_Db_Expr('NOW()'),
                        'created_by'   => $auth->getIdentity()->iduser,
                        'created_role' => 'admin',
                        'ses_id'       => 0
                    );

                    $db->insert('tbl_studentregsubjects_detail', $data);

                    //update back regsub_id
                    $this->regModel->updateArticleship(array('regsub_id' => $regsub_id), array('a_id = ?' => $articleship_id));


                    //generate invoice
                    $invoiceClass->generateInvoiceStudent($student['IdStudentRegistration'], $semester['IdSemesterMaster'], $subjects[0]['IdSubject']);

                } else {
                    $isRegisteredInfo = $this->regModel->isRegisteredInfo($formData['subject_id'], $semester['IdSemesterMaster'], $this->studentInfo['IdStudentRegistration']);

                    //update back regsub_id
                    $this->regModel->updateArticleship(array('regsub_id' => $isRegisteredInfo['IdStudentRegSubjects']), array('a_id = ?' => $articleship_id));
                }

                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    $adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $articleship_id,
                                'af_type'     => 'articleship',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db
                    $data = array(
                        'ps_pid'             => $articleship_id,
                        'ps_type'            => 'articleship',
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addSupervisor($data);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {
                    //save file into db
                    $data = array(
                        'ex_pid'           => $articleship_id,
                        'ex_type'          => 'articleship',
                        'ex_examiner_id'   => $formData['examiner_userid'][$i],
                        'ex_examiner_role' => $formData['examiner_role'][$i],
                        'ex_active'        => $formData['examiner_active'][$i],
                        'ex_addedby'       => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'     => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }

                $this->_helper->flashMessenger->addMessage(array('success' => "New articleship succesfully added"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'articleship'), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }

    public
    function articleshipEditAction()
    {
        $this->view->title = "Research - Edit Articleship";

        $form = new Thesis_Form_Articleship();

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        //
        $interest = $this->thesisModel->getInterest();
        $this->view->interests = $interest;

        $id = $this->_getParam('id');
        $this->view->tab = $this->_getParam('tab');


        //get articleship info
        $articleship = $this->regModel->getArticleship($id);

        if (empty($articleship)) {
            throw new Exception('Invalid Articleship ID');
        }

        //get files
        $uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($articleship['a_id'], 'articleship');

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($articleship['a_id'], 'articleship');
        $supervisor_ids = array();
        foreach ($supervisors as $sup) {
            $supervisor_ids[] = $sup['ps_id'];
        }

        $this->view->supervisor_ids = implode(',', $supervisor_ids);

        //get examiners
        $examiners = $this->view->examiners = $this->regModel->getExaminers($articleship['a_id'], 'articleship');
        $examiners_ids = array();
        foreach ($examiners as $exp) {
            $exminers_ids[] = $exp['ex_id'];
        }

        $this->view->examiners_ids = implode(',', $examiners_ids);

        //interest
        $_interest = array();
        $this->view->interest = json_decode($articleship['fieldofinterest'], true);


        //repopulate form
        $form->populate($articleship);

        $this->view->articleship = $articleship;

        //disable semester_id
        $form->semester_id->setAttrib('disabled', 'disabled')
            ->setRequired(false);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $data = array(
                    //'semester_id'				=> $formData['semester_id'],
                    'fieldofinterest'        => json_encode($formData['interest']),
                    'fieldofinterest_others' => $formData['fieldofinterest_others'],
                    'company'                => $formData['company'],
                    'designation'            => $formData['designation'],
                    'contactperson'          => $formData['contactperson'],
                    'address'                => $formData['address'],
                    'city'                   => $formData['city'],
                    'city_others'            => $formData['city_others'],
                    'state'                  => $formData['state'],
                    'state_others'           => $formData['state_others'],
                    'country'                => $formData['country'],
                    'postcode'               => $formData['postcode'],
                    'phoneno'                => $formData['phoneno'],
                    'faxno'                  => $formData['faxno'],
                    'email'                  => $formData['email'],
                    'isemployee'             => $formData['isemployee'],
                    'yearsofservice'         => $formData['yearsofservice'],
                    //'status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
                    'updated_by'             => $this->auth->getIdentity()->iduser,
                    'updated_date'           => new Zend_Db_Expr('NOW()'),

                    'start_date' => $formData['start_date'],
                    'end_date'   => $formData['end_date'],
                    'archive'    => $formData['archive']
                );

                $articleship_id = $id;


                $this->regModel->updateArticleship($data, 'a_id = ' . (int)$id);

                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    $adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $articleship_id,
                                'af_type'     => 'articleship',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db
                    $data = array(
                        'ps_pid'             => $articleship_id,
                        'ps_type'            => 'articleship',
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addSupervisor($data);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {
                    //save file into db
                    $data = array(
                        'ex_pid'           => $articleship_id,
                        'ex_type'          => 'articleship',
                        'ex_examiner_id'   => $formData['examiner_userid'][$i],
                        'ex_examiner_role' => $formData['examiner_role'][$i],
                        'ex_active'        => $formData['examiner_active'][$i],
                        'ex_addedby'       => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'     => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'articleship-edit', 'id' => $id), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }

    /*
        EXEMPTION
    */
    public
    function exemptionAction()
    {
        //title
        $this->view->title = "Registration - Professional Practice Paper";

        $p_data = $this->regModel->getExemptionData(null, 'array');

        $this->view->form = new Thesis_Form_SearchExemption();

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->form->populate($formData);

            $p_data = $this->regModel->getExemptionData($formData, 'array');

            if (isset($formData['lecturer']) && $formData['lecturer'] != '') {
                if ($p_data) {
                    //dd($p_data); exit;
                    foreach ($p_data as $p_key => $proposal) {
                        $lecturers = $this->regModel->getSupervisors($proposal['e_id'], 'exemption');

                        //dd($lecturers); exit;

                        $lecArr = array(0);
                        if ($lecturers) {
                            foreach ($lecturers as $lecturer) {
                                array_push($lecArr, $lecturer['IdStaff']);
                            }
                        }

                        if (!in_array($formData['lecturer'], $lecArr)) {
                            unset($p_data[$p_key]);
                        }
                    }
                }
            }
        }

        //$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator = $this->lobjCommon->fnPagination($p_data, $lintpage, $lintpagecount);
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    public
    function exemptionExportAction()
    {
        $p_data = $this->regModel->getExemptionData(null, 'array');

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $p_data = $this->regModel->getExemptionData($formData, 'array');

            if (isset($formData['lecturer']) && $formData['lecturer'] != '') {
                if ($p_data) {
                    //dd($p_data); exit;
                    foreach ($p_data as $p_key => $proposal) {
                        $lecturers = $this->regModel->getSupervisors($proposal['e_id'], 'exemption');

                        //dd($lecturers); exit;

                        $lecArr = array(0);
                        if ($lecturers) {
                            foreach ($lecturers as $lecturer) {
                                array_push($lecArr, $lecturer['IdStaff']);
                            }
                        }

                        if (!in_array($formData['lecturer'], $lecArr)) {
                            unset($p_data[$p_key]);
                        }
                    }
                }
            }
        }

        if ($p_data) {
            foreach ($p_data as $p_key => $proposal) {
                $lecturers = $this->regModel->getSupervisors($proposal['e_id'], 'exemption');
                $examiners = $this->regModel->getExaminers($proposal['e_id'], 'exemption');

                $p_data[$p_key]['lecturer'] = $lecturers;
                $p_data[$p_key]['examiner'] = $examiners;
            }
        }

        //dd($p_data); exit;

        $this->view->paginator = $p_data;

        $this->view->filename = date('Ymd') . '_thesis_exemption';
    }

    public
    function exemptionAddAction()
    {
        $this->view->title = "Registration - New Professional Practice Paper";


        $semesterDb = new Registration_Model_DbTable_Semester();
        $studentSemesterStatusDB = new CourseRegistration_Model_DbTable_Studentsemesterstatus();
        $subjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
        $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
        $studentDb = new Registration_Model_DbTable_Studentregistration();

        $auth = Zend_Auth::getInstance();
        $db = getDb();
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();

        $form = new Thesis_Form_Exemption();

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        //reason list
        $reasons = $this->thesisModel->getReason();
        $this->view->reasons = $reasons;


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $student = $studentDb->getData($formData['student_id']);

            $semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $student['IdScheme']));

            //get subject articleship
            $landscape = $landscapeDb->getData($student["IdLandscape"]);

            if ($landscape["LandscapeType"] == 42) { //for level landscape
                $policy = $subjectDb->levelPolicy($student['IdStudentRegistration'], $student['IdLandscape']);
            } else {
                $policy = array();

                throw new Exception('This student is not allowed to register for Articleship. Check Programme.');
            }

            $subjects = $subjectDb->getCourseOffer($student['IdLandscape'], $semester['IdSemesterMaster'], $landscape["LandscapeType"], $policy, $student['IdStudentRegistration'], 19);//2=coursetype ppp

            if (empty($subjects)) {
                throw new Exception('Cannot find any course linked to student\'s information related to research type');
            }


            if ($form->isValid($formData)) {
                $data = array(
                    'student_id'      => $formData['student_id'],
                    'reason'          => $formData['reason'],
                    'reason_selected' => json_encode($formData['reason_selected']),
                    'proposedarea'    => $formData['proposedarea'],
                    'status'          => $this->thesisModel->getStatusByCode('APPLIED'),
                    'created_by'      => $this->auth->getIdentity()->iduser,
                    'created_date'    => new Zend_Db_Expr('NOW()'),
                    'created_role'    => 'admin',
                    'start_date'      => $formData['start_date'],
                    'end_date'        => $formData['end_date']
                );

                $exemption_id = $this->regModel->addExemption($data);

                //student semester status
                //checking semester status exist or not
                $semesterStatus = $studentSemesterStatusDB->getSemesterStatusBySemester($student['IdStudentRegistration'], $semester['IdSemesterMaster']);

                if (empty($semesterStatus)) {
                    $dataSemStatus = array(
                        'IdStudentRegistration' => $student['IdStudentRegistration'],
                        'idSemester'            => $semester['IdSemesterMaster'],
                        'IdSemesterMain'        => $semester['IdSemesterMaster'],
                        'studentsemesterstatus' => 130,//registered
                        'UpdUser'               => $auth->getIdentity()->iduser,
                        'UpdDate'               => date("Y-m-d H:i:s"),
                        'UpdRole'               => 'admin',
                        'Level'                 => 1,
                    );

                    $db->insert('tbl_studentsemesterstatus', $dataSemStatus);
                    $semstatus_id = $db->lastInsertId();

                }

                if ($this->regModel->isRegistered($subjects[0]['IdSubject'], $semester['IdSemesterMaster'], $student['IdStudentRegistration']) == false) {

                    //regsubjects
                    $data = array(
                        'IdStudentRegistration' => $student['IdStudentRegistration'],
                        'IdSubject'             => $subjects[0]['IdSubject'],
                        'IdSemesterMain'        => $semester['IdSemesterMaster'],
                        'UpdUser'               => $auth->getIdentity()->iduser,
                        'UpdDate'               => date("Y-m-d H:i:s"),
                        'UpdRole'               => 'admin',
                        'Active'                => 0,
                    );

                    $db->insert('tbl_studentregsubjects', $data);
                    $regsub_id = $db->lastInsertId();

                    //add into history
                    $data = array(
                        'IdStudentRegistration' => $student['IdStudentRegistration'],
                        'IdSubject'             => $subjects[0]['IdSubject'],
                        'IdSemesterMain'        => $semester['IdSemesterMaster'],
                        'UpdUser'               => $auth->getIdentity()->iduser,
                        'UpdDate'               => date("Y-m-d H:i:s"),
                        'UpdRole'               => 'admin',
                        'Active'                => 0
                    );

                    $db->insert('tbl_studentregsubjects_history', $data);

                    //regsubjects_detail
                    $data = array(
                        'student_id'   => $student['IdStudentRegistration'],
                        'regsub_id'    => $regsub_id,
                        'semester_id'  => $semester['IdSemesterMaster'],
                        'subject_id'   => $subjects[0]['IdSubject'],
                        'item_id'      => 890,//item paper only
                        'section_id'   => 0,
                        'ec_country'   => 0,
                        'ec_city'      => 0,
                        'created_date' => new Zend_Db_Expr('NOW()'),
                        'created_by'   => $auth->getIdentity()->iduser,
                        'created_role' => 'admin',
                        'ses_id'       => 0
                    );

                    $db->insert('tbl_studentregsubjects_detail', $data);

                    //update back regsub_id
                    $this->regModel->updateExemption(array('regsub_id' => $regsub_id), array('e_id = ?' => $exemption_id));


                    //generate invoice
                    $invoiceClass->generateInvoiceStudent($student['IdStudentRegistration'], $semester['IdSemesterMaster'], $subjects[0]['IdSubject']);

                } else {
                    $isRegisteredInfo = $this->regModel->isRegisteredInfo($subjects[0]['IdSubject'], $semester['IdSemesterMaster'], $formData['student_id']);

                    //update back regsub_id
                    $this->regModel->updateExemption(array('regsub_id' => $isRegisteredInfo['IdStudentRegSubjects']), array('e_id = ?' => $exemption_id));
                }

                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    $adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $exemption_id,
                                'af_type'     => 'exemption',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add employment detail
                for ($i = 0; $i < count($formData['employment_compname']);) {
                    //save file into db
                    $data = array(
                        'ae_pid'         => $exemption_id,
                        'ae_compname'    => $formData['employment_compname'][$i],
                        'ae_compaddress' => $formData['employment_compaddress'][$i],
                        'ae_position'    => $formData['employment_position'][$i],
                        'ae_jobfunction' => $formData['employment_jobfunction'][$i],
                        'ae_year_from'   => $formData['employment_year_from'][$i],
                        'ae_year_to'     => $formData['employment_year_to'][$i],
                        'ae_refname'     => $formData['employment_refname'][$i],
                        'ae_refaddress'  => $formData['employment_refaddress'][$i],
                        'ae_addedby'     => $this->auth->getIdentity()->iduser,
                        'ae_addeddate'   => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addEmployment($data);

                    $i++;
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db
                    $data = array(
                        'ps_pid'             => $exemption_id,
                        'ps_type'            => 'exemption',
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addSupervisor($data);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {
                    //save file into db
                    $data = array(
                        'ex_pid'           => $exemption_id,
                        'ex_type'          => 'exemption',
                        'ex_examiner_id'   => $formData['examiner_userid'][$i],
                        'ex_examiner_role' => $formData['examiner_role'][$i],
                        'ex_active'        => $formData['examiner_active'][$i],
                        'ex_addedby'       => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'     => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }


                $this->_helper->flashMessenger->addMessage(array('success' => "New articleship exemption succesfully added"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'exemption'), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }

    public
    function exemptionEditAction()
    {
        $this->view->title = "Registration - Edit Professional Practice Paper";

        $form = new Thesis_Form_Exemption();

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        $id = $this->_getParam('id');
        $this->view->tab = $this->_getParam('tab');


        //get exemption info
        $exemption = $this->regModel->getExemption($id);

        if (empty($exemption)) {
            throw new Exception('Invalid PPP ID');
        }

        //get files
        $uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($exemption['e_id'], 'exemption');

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($exemption['e_id'], 'exemption');
        $supervisor_ids = array();
        foreach ($supervisors as $sup) {
            $supervisor_ids[] = $sup['ps_id'];
        }

        $this->view->supervisor_ids = implode(',', $supervisor_ids);

        //get examiners
        $examiners = $this->view->examiners = $this->regModel->getExaminers($exemption['e_id'], 'exemption');
        $examiners_ids = array();
        foreach ($examiners as $exp) {
            $exminers_ids[] = $exp['ex_id'];
        }

        $this->view->examiners_ids = implode(',', $examiners_ids);

        //employment list
        $employment = $this->regModel->getEmployment($id);
        $this->view->employment = $employment;

        //reason list
        $reasons = $this->thesisModel->getReason();
        $this->view->reasons = $reasons;


        //repopulate form
        $form->populate($exemption);

        $this->view->exemption = $exemption;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $data = array(
                    'reason'          => $formData['reason'],
                    'reason_selected' => json_encode($formData['reason_selected']),
                    'proposedarea'    => $formData['proposedarea'],
                    'updated_by'      => $this->auth->getIdentity()->iduser,
                    'updated_date'    => new Zend_Db_Expr('NOW()'),
                    'start_date'      => $formData['start_date'],
                    'end_date'        => $formData['end_date'],
                    'archive'         => $formData['archive']
                );

                $this->regModel->updateExemption($data, 'e_id=' . (int)$id);
                $exemption_id = $id;


                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    $adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $exemption_id,
                                'af_type'     => 'exemption',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add employment detail
                for ($i = 0; $i < count($formData['employment_compname']);) {
                    //save file into db
                    $data = array(
                        'ae_pid'         => $exemption_id,
                        'ae_compname'    => $formData['employment_compname'][$i],
                        'ae_compaddress' => $formData['employment_compaddress'][$i],
                        'ae_position'    => $formData['employment_position'][$i],
                        'ae_jobfunction' => $formData['employment_jobfunction'][$i],
                        'ae_year_from'   => $formData['employment_year_from'][$i],
                        'ae_year_to'     => $formData['employment_year_to'][$i],
                        'ae_refname'     => $formData['employment_refname'][$i],
                        'ae_refaddress'  => $formData['employment_refaddress'][$i],
                        'ae_addedby'     => $this->auth->getIdentity()->iduser,
                        'ae_addeddate'   => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addEmployment($data);

                    $i++;
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db
                    $data = array(
                        'ps_pid'             => $exemption_id,
                        'ps_type'            => 'exemption',
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addSupervisor($data);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {

                    //save file into db
                    $data = array(
                        'ex_pid'           => $exemption_id,
                        'ex_type'          => 'exemption',
                        'ex_examiner_id'   => $formData['examiner_userid'][$i],
                        'ex_examiner_role' => $formData['examiner_role'][$i],
                        'ex_active'        => $formData['examiner_active'][$i],
                        'ex_addedby'       => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'     => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }


                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'exemption-edit', 'id' => $id), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }

    public
    function exemptionViewempAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $id = $this->_getParam('id');

        $employment = $this->regModel->getEmploymentSingle($id);

        $this->view->emp = $employment;
    }

    public
    function notifyAction()
    {
        $emailDb = new App_Model_Email();
        $commDb = new Communication_Model_DbTable_Template();

        $id = $this->_getParam('id');
        $research_id = $this->_getParam('research_id');

        //ok get supervisor info
        $supervisor = $this->regModel->getSupervisor($id);

        if (!isset($supervisor)) {
            throw new Exception('Invalid Supervisor');
        }

        $research = $this->regModel->getProposal($research_id);
        $gettemplate = $commDb->getTemplatesByCategory('thesis', 'appointment-supervisor-approved', 0, 1);
        $template = $gettemplate[0];

        $research['research_type'] = 'proposal';

        if (!isset($research)) {
            throw new Exception('Invalid Research');
        }

        if (empty($template)) {
            throw new Exception('Invalid Template. Did you change the template name recently?');
        }

        $research['supervisor_info'] = $supervisor['supervisor_name'];

        //email
        $dataEmail = array(
            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $supervisor['supervisor_email'],
            'subject'         => $template['tpl_name'],
            'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
            'date_que'        => date('Y-m-d H:i:s')
        );

        $emailDb->add($dataEmail);

        if($supervisor['supervisor_email2'] != NULL){
            $dataEmail2 = array(
                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $supervisor['supervisor_email2'],
                'subject'         => $template['tpl_name'],
                'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                'date_que'        => date('Y-m-d H:i:s')
            );

            $emailDb->add($dataEmail2);
        }

        //update notified
        $data = array(
            'ps_notified'      => 1,
            'ps_notified_date' => new Zend_Db_Expr('NOW()'),
            'ps_pdf'           => NULL
        );

        $this->regModel->updateSupervisor($data, $supervisor['ps_id']);

        $this->_helper->flashMessenger->addMessage(array('success' => "Supervisor has been notified"));
        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-edit', 'id' => $research['p_id']), 'default', true));
    }

    /**
     * Proposal Defence Application
     *
     * TODO
     */

    protected
    function notifyProposalApplication($research_id, $type = '')
    {
        $emailDb = new App_Model_Email();
        $commDb = new Communication_Model_DbTable_Template();

        //ok get supervisor info
        $supervisor = $this->regModel->getSupervisor($id);

        if (!isset($supervisor)) {
            throw new Exception('Invalid Supervisor');
        }

        $research = $this->regModel->getProposal($research_id);
        $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-application', 0, 1);
        $template = $gettemplate[0];

        $research['research_type'] = 'proposal';

        if (!isset($research)) {
            throw new Exception('Invalid Research');
        }

        if (empty($template)) {
            throw new Exception('Invalid Template. Did you change the template name recently?');
        }

        $research['supervisor_info'] = $supervisor;

        //email
        $dataEmail = array(
            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $supervisor['supervisor_email'],
            'subject'         => $template['tpl_name'],
            'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
            'date_que'        => date('Y-m-d H:i:s')
        );

        $emailDb->add($dataEmail);

        if($supervisor['supervisor_email2'] != NULL){
            $dataEmail2 = array(
                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $supervisor['supervisor_email2'],
                'subject'         => $template['tpl_name'],
                'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                'date_que'        => date('Y-m-d H:i:s')
            );

            $emailDb->add($dataEmail2);
        }

        //update notified
        $data = array(
            'ps_notified'      => 1,
            'ps_notified_date' => new Zend_Db_Expr('NOW()'),
            'ps_pdf'           => NULL
        );

        $this->regModel->updateSupervisor($data, $supervisor['ps_id']);

        $this->_helper->flashMessenger->addMessage(array('success' => "Supervisor has been notified"));
        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-edit', 'id' => $research['p_id']), 'default', true));
    }

    public
    function courseRegAction()
    {
        $semesterDb = new Registration_Model_DbTable_Semester();
        $studentSemesterStatusDB = new CourseRegistration_Model_DbTable_Studentsemesterstatus();
        $subjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
        $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
        $studentDb = new Registration_Model_DbTable_Studentregistration();

        $auth = Zend_Auth::getInstance();
        $db = getDb();
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();

        $this->view->title = 'Research Course Registration';

        $type = $this->_getParam('type', 'articleship');

        $this->view->type = $type;

        if ($this->_getParam('regen') == 1) {
            $student_id = $this->_getParam('student_id');
            $research_id = $this->_getParam('research_id');

            $student = $studentDb->getData($student_id);

            $semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $student['IdScheme']));

            //get subject articleship
            $landscape = $landscapeDb->getData($student["IdLandscape"]);

            if ($landscape["LandscapeType"] == 42) { //for level landscape
                $policy = $subjectDb->levelPolicy($student['IdStudentRegistration'], $student['IdLandscape']);
            } else {
                $policy = array();

                throw new Exception('This student is not allowed to register for Articleship. Check Programme.');
            }

            $subjects = $subjectDb->getCourseOffer($student['IdLandscape'], $semester['IdSemesterMaster'], $landscape["LandscapeType"], $policy, $student['IdStudentRegistration'], ($type == 'articleship' ? 2 : 19));

            if (empty($subjects)) {
                throw new Exception('Cannot find any course linked to student\'s information related to research type');
            }

            //student semester status
            //checking semester status exist or not
            $semesterStatus = $studentSemesterStatusDB->getSemesterStatusBySemester($student['IdStudentRegistration'], $semester['IdSemesterMaster']);

            if (empty($semesterStatus)) {
                $dataSemStatus = array(
                    'IdStudentRegistration' => $student['IdStudentRegistration'],
                    'idSemester'            => $semester['IdSemesterMaster'],
                    'IdSemesterMain'        => $semester['IdSemesterMaster'],
                    'studentsemesterstatus' => 130,//registered
                    'UpdUser'               => $auth->getIdentity()->iduser,
                    'UpdDate'               => date("Y-m-d H:i:s"),
                    'UpdRole'               => 'admin',
                    'Level'                 => 1,
                );

                $db->insert('tbl_studentsemesterstatus', $dataSemStatus);
                $semstatus_id = $db->lastInsertId();

            }

            if ($this->regModel->isRegistered($subjects[0]['IdSubject'], $semester['IdSemesterMaster'], $student['IdStudentRegistration']) == false) {

                //regsubjects
                $data = array(
                    'IdStudentRegistration' => $student['IdStudentRegistration'],
                    'IdSubject'             => $subjects[0]['IdSubject'],
                    'IdSemesterMain'        => $semester['IdSemesterMaster'],
                    'UpdUser'               => $auth->getIdentity()->iduser,
                    'UpdDate'               => date("Y-m-d H:i:s"),
                    'UpdRole'               => 'admin',
                    'Active'                => 0,
                );

                $db->insert('tbl_studentregsubjects', $data);
                $regsub_id = $db->lastInsertId();

                //add into history
                $data = array(
                    'IdStudentRegistration' => $student['IdStudentRegistration'],
                    'IdSubject'             => $subjects[0]['IdSubject'],
                    'IdSemesterMain'        => $semester['IdSemesterMaster'],
                    'UpdUser'               => $auth->getIdentity()->iduser,
                    'UpdDate'               => date("Y-m-d H:i:s"),
                    'UpdRole'               => 'admin',
                    'Active'                => 0
                );

                $db->insert('tbl_studentregsubjects_history', $data);

                //regsubjects_detail
                $data = array(
                    'student_id'   => $student['IdStudentRegistration'],
                    'regsub_id'    => $regsub_id,
                    'semester_id'  => $semester['IdSemesterMaster'],
                    'subject_id'   => $subjects[0]['IdSubject'],
                    'item_id'      => 890,//item paper only
                    'section_id'   => 0,
                    'ec_country'   => 0,
                    'ec_city'      => 0,
                    'created_date' => new Zend_Db_Expr('NOW()'),
                    'created_by'   => $auth->getIdentity()->iduser,
                    'created_role' => 'admin',
                    'ses_id'       => 0
                );

                $db->insert('tbl_studentregsubjects_detail', $data);

                //update back regsub_id
                if ($type == 'articleship') {
                    $this->regModel->updateArticleship(array('regsub_id' => $regsub_id), array('a_id = ?' => $research_id));
                } else {
                    $this->regModel->updateExemption(array('regsub_id' => $regsub_id), array('e_id = ?' => $research_id));
                }

                //generate invoice
                $invoiceClass->generateInvoiceStudent($student['IdStudentRegistration'], $semester['IdSemesterMaster'], $subjects[0]['IdSubject']);

                $this->_helper->flashMessenger->addMessage(array('success' => "Done"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'course-reg', 'type' => $type), 'default', true));
            }
        }
        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            foreach ($formData['research'] as $research_id => $regsub_id) {

                if ($type == 'articleship') {
                    $this->regModel->updateArticleship(array('regsub_id' => $regsub_id), array('a_id = ?' => $research_id));

                } elseif ($type == 'exemption') {
                    $this->regModel->updateExemption(array('regsub_id' => $regsub_id), array('e_id = ?' => $research_id));


                }

            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Done"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'course-reg', 'type' => $type), 'default', true));

        }


        $search = array();
        switch ($type) {
            case 'proposal':
                $p_data = $this->regModel->getProposalData($search);
                break;

            case 'articleship':
                $p_data = $this->regModel->getArticleshipData($search);
                break;

            case 'exemption':
                $p_data = $this->regModel->getExemptionData($search);
                break;
        }

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage(999);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        if ($type == 'proposal') {
            $newresults = Zend_Paginator::factory(array());
        }

        //get
        if (!empty($paginator) && $type != 'proposal') {
            foreach ($paginator as $row) {
                $check = $this->regModel->checkResearchCourseReg($row['student_id'], $type);
                $row['has_coursereg'] = empty($check) ? false : true;
                $row['IdStudentRegSubjects'] = $check['IdStudentRegSubjects'];

                $newresults[] = $row;
            }

            $newresults = Zend_Paginator::factory($newresults);
            $newresults->setItemCountPerPage(999);

        }

        $this->view->paginator = $newresults;
        $this->view->type = $type;
    }

    /* registration successful */

    protected
    function notifyRegistration($research, $type = '')
    {

        //if p_status = registered (Registration Status)

        /*$supervisorDb = new Thesis_Model_DbTable_Supervisor();
        $supervisor = $supervisorDb->getSupervisor($id, 'proposal', $main = 1);

        $research['student_email'] = $proposal['student_email'];
        $research['student_name'] = $proposal['student_name'];
        $research['p_title'] = $proposal['p_title'];
        $research['semester_start'] = $proposal['semester_start'];
        $research['course'] = $proposal['course'];
        $research['supervisor_info'] = $supervisor;

        $this->notifyRegistration($research, 'proposal');*/

        $emailDb = new App_Model_Email();
        $commDb = new Communication_Model_DbTable_Template();

        $gettemplate = $commDb->getTemplatesByCategory('thesis', 'registration', 0, 1);
        $template = $gettemplate[0];

        if (empty($template)) {
            throw new Exception('Invalid Template');
        }

        //email
        $dataEmail = array(
            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
            'subject'         => $template['tpl_name'],
            'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
            'date_que'        => date('Y-m-d H:i:s')
        );

        $emailDb->add($dataEmail);

        /*//email history
        $data_compose = array(
            'comp_subject'=> $templateData["tpl_name"],
            'comp_module' => 'records',
            'comp_tpl_id' => $templateData["tpl_id"],
            'comp_type' => 585,
            'comp_rectype' => 'students',
            'comp_lang' => 'en_US',
            'comp_totalrecipients' => 1,
            'comp_rec' => $student['IdStudentRegistration'],
            'comp_content' => $templateMail,
            'created_by' => 1,
            'created_date' => date('Y-m-d H:i:s')
        );
        $comId = $this->insertCommpose($data_compose);

        $data_compose_rec = array(
            'cr_comp_id' => $comId,
            'cr_subject' => $templateData["tpl_name"],
            'cr_content' => $templateMail,
            'cr_rec_id' => $student['IdStudentRegistration'],
            'cr_email' => 'norhisham@inceif.org',
            //'cr_email' => $student['appl_email_personal'],
            'cr_status' => 1,
            'cr_datesent' => date('Y-m-d H:i:s')
        );
        $this->insertComposeRec($data_compose_rec);*/

    }

    public
    function generatePdf($data = '')
    {
        if (empty($data)) {
            throw new Exception('Empty Data for PDF Generation');
        }

        $url = $this->uploadDir . '/notify';

        //option pdf
        $option = array(
            'content'        => $data['html'],
            'save'           => true,
            'file_extension' => 'pdf',
            'save_path'      => $url,
            'file_name'      => $data['filename'],
            'css'            => '@page { margin: 110px 50px 50px 50px}
			body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
			table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
			table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
			table.tftable tr {background-color:#ffffff;}
			table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
            'header'         => '<script type="text/php">
			if ( isset($pdf) ) {
					$header = $pdf->open_object();

					$w = $pdf->get_width();
					$h = $pdf->get_height();
					$color = array(0,0,0);

					$img_w = 180; 
					$img_h = 59;
					$pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

					// Draw a line along the bottom
					$font = Font_Metrics::get_font("Helvetica");
					$size = 6;
					$text_height = Font_Metrics::get_font_height($font, $size)+2;
					$y = $h - (3.5 * $text_height)-10;
					$pdf->line(10, $y, $w - 10, $y, $color, 1);

					// Draw a second line along the bottom
					$y = $h - (3.5 * $text_height)+10;
					$pdf->line(10, $y, $w - 10, $y, $color, 1);

					$pdf->close_object();

					$pdf->add_object($header, "all");
			}
			</script>',
            'footer'         => '<script type="text/php">
			if ( isset($pdf) ) {
				$footer = $pdf->open_object();

				$font = Font_Metrics::get_font("Helvetica");
				$size = 6;
				$color = array(0,0,0);
				$text_height = Font_Metrics::get_font_height($font, $size)+2;

				$w = $pdf->get_width();
				$h = $pdf->get_height();


				// Draw a line along the bottom
				$y = $h - (3.5 * $text_height)-10;
				//$pdf->line(10, $y, $w - 10, $y, $color, 1);

				//1st row footer
				$text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
				$width = Font_Metrics::get_text_width($text, $font, $size);	
				$y = $h - (2 * $text_height)-20;
				$x = ($w - $width) / 2.0;

				$pdf->page_text($x, $y, $text, $font, $size, $color);

				//2nd row footer
				$text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
				$width = Font_Metrics::get_text_width($text, $font, $size);	
				$y = $h - (1 * $text_height)-20;
				$x = ($w - $width) / 2.0;

				$pdf->page_text($x, $y, $text, $font, $size, $color);



				$pdf->close_object();

				$pdf->add_object($footer, "all");
		}
		</script>'
        );

        //genarate attachment
        $pdf = generatePdf($option);
    }

    public
    function getCourseIdAction()
    {
        $id = $this->_getParam('course_id', 0);

//        echo $id;exit;

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $courseDb = new Thesis_Model_DbTable_ResearchActivity();
        $result = $courseDb->getActivityByCourse($id);

//        print_r($result);exit;

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public
    function getSemesterAction()
    {
        $date = $this->_getParam('date', 0);
        $IdScheme = $this->_getParam('IdScheme', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $courseDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $result = $courseDb->getSemesterBasedOnDate($date, $IdScheme);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function deleteActivityAction()
    {
        $activity_id = $this->_getParam('activity_id');
        $p_id = $this->_getParam('p_id');
        $student_id = $this->_getParam('student_id');
        $pid = $this->_getParam('pid');

        $this->regModel->deleteActivity($p_id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Activity successfully deleted"));

        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-activity', 'id' => $pid), 'default', true));

    }

}

