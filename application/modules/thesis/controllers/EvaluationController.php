<?php

class Thesis_EvaluationController extends Base_Base
{
    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->evalModel = new Thesis_Model_DbTable_Evaluation();
        $this->regModel = new Thesis_Model_DbTable_Registration();

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();

    }

    public function setAction()
    {
        //title
        $this->view->title = "Research - Evaluation Set";

        $p_data = $this->thesisModel->getEvaluationSetData();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    public function setAddAction()
    {
        $this->view->title = "Research - Add Evaluation Set";

        $form = new Thesis_Form_EvaluationSet();


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //data
            $data = array(
                'name' => $formData['name'],
                'description' => $formData['description'],
                'evaluation_type' => $formData['evaluation_type'],
                'rating_min' => $formData['rating_min'],
                'rating_max' => $formData['rating_max'],
                'rating_min_label' => $formData['rating_min_label'],
                'rating_max_label' => $formData['rating_max_label'],
                'created_by' => $this->auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()'),
                'subject_id' => $formData['subject_id']
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "New evaluation set successfully added"));

            $status_id = $this->thesisModel->addEvaluationSet($data);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'set-details', 'id' => $status_id), 'default', true));
        }

        //views
        $this->view->form = $form;
    }

    public function setEditAction()
    {
        $this->view->title = "Research - Edit Evaluation Set";

        $form = new Thesis_Form_EvaluationSet();

        //$form->evaluation_type->setAttrib('disabled', 'disabled');

        $id = $this->_getParam('id');

        $info = $this->thesisModel->getEvaluationSetSingle($id);

        if (empty($info)) {
            throw new Exception('Invalid Set ID');
        }

        $this->view->info = $info;

        $form->populate($info);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //data
            $data = array(
                'name' => $formData['name'],
                'description' => $formData['description'],
                'evaluation_type' => $formData['evaluation_type'],
                'rating_min' => $formData['rating_min'],
                'rating_max' => $formData['rating_max'],
                'rating_min_label' => $formData['rating_min_label'],
                'rating_max_label' => $formData['rating_max_label'],
                'updated_by' => $this->auth->getIdentity()->iduser,
                'updated_date' => new Zend_Db_Expr('NOW()'),
                'subject_id' => $formData['subject_id']
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $status_id = $this->thesisModel->updateEvaluationSet($data, 'id=' . (int)$id);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'set-edit', 'id' => $id), 'default', true));
        }

        //views
        $this->view->form = $form;
    }

    public function setDetailsAction()
    {
        $this->view->title = "Research - Evaluation Set Details";

        $id = $this->_getParam('id');

        $info = $this->thesisModel->getEvaluationSetSingle($id);

        if (empty($info)) {
            throw new Exception('Invalid Set ID');
        }

        //data
        $items = $this->thesisModel->getEvaluationItemsBySet($id);
        $groups = $this->thesisModel->getEvaluationGroupsBySet($id);

        $itemsbygroup = array();
        foreach ($items as $item) {
            $itemsbygroup[$item['group_id']][] = $item;
        }

        $this->view->items = $itemsbygroup;
        $this->view->groups = $groups;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (!empty($formData['cur_item'])) {
                foreach ($formData['cur_item'] as $item_id => $item) {
                    $item_data = array(
                        'name' => $item['name'],
                        'description' => $item['description'],
                        'marks' => $item['marks']
                    );

                    $this->thesisModel->updateEvaluationItem($item_data, 'cid=' . (int)$item_id);
                }
            }

            //curgroup
            foreach ($formData['curgroup'] as $group_index => $curgroup_id) {
                if (!empty($formData['item_name'][$group_index])) {
                    $x = 0;
                    foreach ($formData['item_name'][$group_index] as $item) {
                        if ($item != '') {
                            $data2 = array(
                                'name' => $item,
                                'marks' => isset($formData['item_marks'][$group_index][$x]) ? $formData['item_marks'][$group_index][$x] : '',
                                'description' => isset($formData['item_desc'][$group_index][$x]) ? $formData['item_desc'][$group_index][$x] : '',
                                'set_id' => $id,
                                'group_id' => $curgroup_id
                            );

                            $this->thesisModel->addEvaluationItem($data2);
                        }

                        $x++;
                    } //foreach
                }
            }

            //newgroup
            $group_count = count($formData['group_name']);
            if ($group_count > 0) {
                $i = 0;
                foreach ($formData['group_name'] as $group_name) {
                    $i++;

                    $data = array(
                        'group_name' => $group_name,
                        'group_description' => '',
                        'set_id' => $id
                    );

                    $group_id = $this->thesisModel->addEvaluationGroup($data);

                    if (!empty($formData['item_name'][$i])) {
                        $x = 0;
                        foreach ($formData['item_name'][$i] as $item) {
                            if ($item != '') {
                                $data2 = array(
                                    'name' => $item,
                                    'marks' => isset($formData['item_marks'][$i][$x]) ? $formData['item_marks'][$i][$x] : '',
                                    'description' => isset($formData['item_desc'][$i][$x]) ? $formData['item_desc'][$i][$x] : '',
                                    'set_id' => $id,
                                    'group_id' => $group_id
                                );

                                $this->thesisModel->addEvaluationItem($data2);
                            }

                            $x++;
                        } //foreach
                    }
                    //item loop

                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));


            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'set-details', 'id' => $id), 'default', true));
        }

        $this->view->info = $info;
    }

    public function itemAction()
    {
        //title
        $this->view->title = "Research - Evaluation Items";

        $p_data = $this->thesisModel->getEvaluationItemData();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }


    public function itemAddAction()
    {
        $this->view->title = "Research - Add Evaluation Item";

        $form = new Thesis_Form_EvaluationItem();

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //data
            $data = array(
                'name' => $formData['name'],
                'description' => $formData['description']
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "New evaluation item successfully added"));

            $status_id = $this->thesisModel->addEvaluationItem($data);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'item'), 'default', true));
        }

        //views
        $this->view->form = $form;
    }

    public function itemEditAction()
    {
        $this->view->title = "Research - Edit Evaluation Item";

        $form = new Thesis_Form_EvaluationItem();

        $id = $this->_getParam('id');

        $info = $this->thesisModel->getEvaluationItemSingle($id);

        if (empty($info)) {
            throw new Exception('Invalid Evaluation Item ID');
        }

        $this->view->info = $info;

        $form->populate($info);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //data
            $data = array(
                'name' => $formData['name'],
                'description' => $formData['description']
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $status_id = $this->thesisModel->updateEvaluationItem($data, 'cid=' . (int)$id);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'item'), 'default', true));
        }

        //views
        $this->view->form = $form;
    }


    public function sheetAction()
    {
        //title
        $this->view->title = "Research - Evaluation Sheet";

        $type = $this->_getParam('type', 'proposal');

        $p_data = $this->evalModel->getEvaluationSheetData($type, 1);

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
        $this->view->type = $type;
    }

    public function sheetListAction()
    {
        //title
        $this->view->title = "Research - Evaluation Sheet Details";

        $set_id = $this->_getParam('set_id');
        $student_id = $this->_getParam('student_id');

        $results = $this->evalModel->getEvaluationSheet($set_id, $student_id);

        $this->view->set_id = $set_id;
        $this->view->student_id = $student_id;
        $this->view->results = $results;
    }

    protected function getResearchInfo($id, $type)
    {
        switch ($type) {
            case 'proposal':
                $info = $this->regModel->getProposal($id);
                break;

            case 'articleship':
                $info = $this->regModel->getArticleship($id);
                break;

            case 'ppp':
                $info = $this->regModel->getExemption($id);
                break;
        }

        return $info;
    }


    public function sheetViewAction()
    {
        $this->view->title = 'Research - Evaluation Sheet Review';

        $id = $this->_getParam('id');

        $info = $this->evalModel->getEvaluationById($id);

        $set_id = $this->_getParam('set_id');
        $student_id = $this->_getParam('student_id');

        $this->view->set_id = $set_id;
        $this->view->student_id = $student_id;

        $this->view->info = $info;
        $this->view->researchInfo = $this->getResearchInfo($info['research_id'], $info['research_type']);

        $data = $this->view->data = json_decode($info['sheet_data'], true);

        //data
        $items = $this->evalModel->getEvaluationItemsBySet($info['set_id']);
        $groups = $this->evalModel->getEvaluationGroupsBySet($info['set_id']);

        $itemsbygroup = array();
        foreach ($items as $item) {
            $itemsbygroup[$item['group_id']][] = $item;
        }


        $this->view->items = $itemsbygroup;
        $this->view->groups = $groups;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($info['locked'] == 1) {
                throw new Exception('Evaluation Sheet confirmed submission');
            }

            if ($info['evaluation_type'] == 0) {

                $sheet_data = json_encode($formData['cur_item']);

                $data = array(
                    'sheet_data' => $sheet_data,
                    'updated_by' => $this->auth->getIdentity()->iduser,
                    'updated_date' => new Zend_Db_Expr('NOW()'),
                    'updated_role' => 'admin'
                );


            } else {
                $sheet_data = json_encode(array(
                    'item' => !isset($formData['item']) ? array() : $formData['item'],
                    'comments' => !isset($formData['comments']) ? array() : $formData['comments'],
                    'section' => !isset($formData['section']) ? array() : $formData['section'],
                    'recommendation' => !isset($formData['recommendation']) ? array() : $formData['recommendation']
                ));
                $data = array(
                    'sheet_data' => $sheet_data,
                    'updated_by' => $this->auth->getIdentity()->iduser,
                    'updated_date' => new Zend_Db_Expr('NOW()'),
                    'updated_role' => 'admin'
                );
            }

            $eval_id = $this->evalModel->updateEvaluation($data, array('id=?' => $id));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'sheet-view', 'id' => $id, 'set_id' => $set_id, 'student_id' => $student_id), 'default', true));

        }

    }


    /*
        Grading
    */
    public function gradingAction()
    {
        //title
        $this->view->title = "Research - Grading";

        $type = $this->_getParam('type', 'proposal');

        $status = $this->thesisModel->getStatusByCode('APPROVED');

        $this->view->form = new Thesis_Form_SearchGrading();

        $search = array('p_status' => $status);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->form->populate($formData);

            $search = $formData + $search;
        }

        switch ($type) {
            case 'proposal':
                $p_data = $this->regModel->getProposalData($search);
                break;

            case 'articleship':
                $p_data = $this->regModel->getArticleshipData($search);
                break;

            case 'exemption':
                $p_data = $this->regModel->getExemptionData($search);
                break;
        }

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
        $this->view->type = $type;
    }

    public function gradingListAction()
    {
        //title
        $this->view->title = "Research - Grading Details";

        $set_id = $this->_getParam('set_id');
        $student_id = $this->_getParam('student_id');
        $errMsg = '';

        $results = $this->evalModel->getEvaluationSheet(0, $student_id);

        if (empty($results)) {
            $errMsg = 'No Evaluation Found For Selected Student.';
        }

        //get set
        $set = '';
        if (!empty($results)) {
            $set = $this->evalModel->getEvaluationSetById($results[0]['set_id']);

            switch ($results[0]['research_type']) {
                case 'proposal':
                    $research = $this->regModel->getProposal($results[0]['research_id']);
                    $researchThesis = $this->regModel->getProposalSubject($results[0]['student_id']);
                    $research['regsub_id'] = $researchThesis['IdStudentRegSubjects'];
                    break;

                case 'articleship':
                    $research = $this->regModel->getArticleship($results[0]['research_id']);
                    $coursereg = $this->regModel->checkResearchCourseReg($research['student_id'], 'articleship');

                    if (empty($coursereg)) {
                        throw new Exception('This student\'s research course registration information is not synced');
                    }

                    break;

                case 'exemption':
                    $research = $this->regModel->getExemption($results[0]['research_id']);

                    $coursereg = $this->regModel->checkResearchCourseReg($research['student_id'], 'exemption');

                    if (empty($coursereg)) {
                        throw new Exception('This student\'s research course registration information is not synced');
                    }

                    break;
            }

            //updated
            $updated = array();
            if ($research['eval_by'] != '') {
                $updated = $this->regModel->getUserByRole($research['eval_by'], 'admin');
            }

            $this->view->updated = $updated;

            if (empty($research)) {
                $errMsg = 'Invalid Research';
            }

            $this->view->research = $research;
        }


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (empty($results)) {
                throw new Exception($errMsg);
            }

            $data = array(
                'eval_grade' => $formData['eval'],
                'eval_marks' => $formData['marks'],
                'eval_role' => 'admin',
                'eval_by' => $this->auth->getIdentity()->iduser,
                'eval_date' => new Zend_Db_Expr('NOW()')
            );

            switch ($results[0]['research_type']) {
                case 'proposal':
                    $this->regModel->updateProposal($data, array('p_id = ?' => $results[0]['research_id']));
                    break;
                case 'articleship':
                    $this->regModel->updateArticleship($data, array('a_id = ?' => $results[0]['research_id']));
                    break;
                case 'exemption':
                    $this->regModel->updateExemption($data, array('e_id = ?' => $results[0]['research_id']));
                    break;
            }

            //tblstudentregsubjects
            $db = getDb();

            //3
            if ($results[0]['research_type'] == 'proposal') {
                if (trim($researchThesis['exam_status']) == 'IP') {
                    $data = false;
                } else {
                    $data = array(
                        'grade_status' => $formData['eval'] == 1 ? 'Pass' : 'Fail',
                        'exam_status' => 'C',
                        'grade_name' => $formData['eval'] == 1 ? 'P' : 'IP',
                        'final_course_mark' => $formData['marks']
                    );
                }
            } else {
                $data = array(
                    'grade_status' => $formData['eval'] == 1 ? 'Pass' : 'Fail',
                    'exam_status' => 'C',
                    'grade_name' => $formData['eval'] == 1 ? 'P' : 'F',
                    'final_course_mark' => $formData['marks']
                );
            }

            if ($data != false) {
                $data['mark_approval_status'] = 1;
                $db->update('tbl_studentregsubjects', $data, array('IdStudentRegSubjects = ?' => $research['regsub_id']));
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Grading successfully updated"));


            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'grading-list', 'student_id' => $results[0]['student_id']), 'default', true));

        }


        $this->view->set_id = $set_id;
        $this->view->student_id = $student_id;
        $this->view->results = $results;
        $this->view->set = $set;
        $this->view->errMsg = $errMsg;
    }


    public function sheetAddAction()
    {
        $this->view->title = 'Research - New Evaluation Sheet';

        //$id = $this->_getParam('id');

        $student_id = $this->_getParam('student_id');
        $research_id = $this->_getParam('research_id');
        $type = $this->_getParam('type');

        $this->view->type = $type;
        $this->view->student_id = $student_id;
        $this->view->research_id = $research_id;

        $errMsg = '';

        switch ($type) {
            case 'proposal':
                $research = $this->regModel->getProposal($research_id);
                break;

            case 'articleship':
                $research = $this->regModel->getArticleship($research_id);
                break;

            case 'exemption':
                $research = $this->regModel->getExemption($research_id);
                break;
        }

        if (empty($research)) {
            throw new Exception('Invalid Research');
        }

        $this->view->research = $research;

        //evaluation
        $sheet = $this->evalModel->getEvaluationSheet(0, $student_id);

        //form
        $form = new Thesis_Form_Evaluation();

        //set options
        $reg_sql = $this->thesisModel->subjectRegistered($student_id);
        $sets = $this->evalModel->getEvaluationSetData($reg_sql);

        if (empty($sets)) {
            throw new Exception('No course registered to allow for Evaluation');
        }

        foreach ($sets as $set) {
            $form->set_id->addMultiOption($set['id'], $set['name']);
        }

        $curexaminer = array();
        foreach ($sheet as $srow) {
            $curexaminer[] = $srow['examiner_id'];
        }

        //examiners
        $examiners = $this->regModel->getExaminers($research['research_id'], $type);

        if (empty($examiners)) {
            throw new Exception('No examiners assigned to this research yet');
        }

        $i = 0;
        foreach ($examiners as $exm) {
            if (!in_array($exm['ex_examiner_id'], $curexaminer)) {
                $i++;

                $form->examiner_id->addMultiOption($exm['ex_examiner_id'], $exm['internal_fullname'] != '' ? $exm['internal_fullname'] : $exm['supervisor_name']);
            }
        }

        if ($i == 0) {
            $errMsg = 'No more examiners available for new evaluation';
        }

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'set_id' => $formData['set_id'],
                'examiner_id' => $formData['examiner_id'],
                'student_id' => $formData['student_id'],
                'research_id' => $formData['research_id'],
                'research_type' => $formData['type'],
                'sheet_data' => '',
                'created_by' => $this->auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()'),
                'created_role' => 'admin'
            );

            if ($set['evaluation_type'] == 0) {
                $sheet_data = json_encode($formData['cur_item']);

                $data['sheet_data'] = $sheet_data;
            } else {
                $sheet_data = json_encode(array(
                    'item' => !isset($formData['item']) ? array() : $formData['item'],
                    'comments' => !isset($formData['comments']) ? array() : $formData['comments'],
                    'section' => !isset($formData['section']) ? array() : $formData['section'],
                    'recommendation' => !isset($formData['recommendation']) ? array() : $formData['recommendation']
                ));

                $data['sheet_data'] = $sheet_data;
            }

            $eval_id = $this->evalModel->addEvaluation($data);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'sheet-view', 'id' => $eval_id, 'sheet_id' => $formData['set_id'], 'student_id' => $formData['student_id']), 'default', true));

        }


        //views
        $this->view->errMsg = $errMsg;
        $this->view->form = $form;
        $this->view->sheet = $sheet;
    }

    /*
        Grading IP
    */
    public function gradingIpAction()
    {
        $db = getDb();

        //title
        $this->view->title = "Grading";

        $type = $this->_getParam('type', 'proposal');

        $status = $this->thesisModel->getStatusByCode('approved', 1);

        $gradingList = $this->defModel->getDataByTypeCode('Research Grading');

        $form = new Thesis_Form_SearchGradingIP();

        $this->view->data = array();


        if ($this->_request->isPost() && $this->_request->getPost('gradesubmit')) {
            $formData = $this->getRequest()->getPost();

//            dd($formData); exit;

            foreach ($formData['student'] as $i => $proposal_id) {
                //$idregsubject = $formData['student'][$i];
                $grade = $formData['grade'][$proposal_id];

                $db->update('thesis_proposal', array(
                    'grade_remarks' => $formData['remarks'][$proposal_id],
                    'total_marks' => $formData['total_marks'][$proposal_id],
                    'grade_status' => $grade,
                    'grade_by' => $this->auth->getIdentity()->iduser,
                    'grade_date' => new Zend_Db_Expr('NOW()')
                ), array('p_id = ?' => $proposal_id));

            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Grading successfully updated"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'evaluation', 'action' => 'grading-ip', 'semester_id' => $formData['semester_id'], 'course_id' => $formData['course_id']), 'default', true));
        }

        if ($this->getRequest()->isPost() || ($this->_getParam('semester_id') != '' && $this->_getParam('course_id') != '')) {
            $formData = $this->getRequest()->getPost();

            /*if ( $this->_getParam('semester_id') != '' && $this->_getParam('course_id') )
            {
                $formData['course_id']  = $this->_getParam('course_id');
                $formData['semester_id'] = $this->_getParam('semester_id');
            }else{
                $formData['semester_id'] = $this->view->escape(strip_tags($formData['semester_id']));
                $formData['course_id'] = $this->view->escape(strip_tags($formData['course_id']));
            }*/
            $form->populate($formData);

            $searchdata = array(
                'p_status' => $status,
                'semester_id' => $formData['semester_id'],
                'course_id' => $formData['course_id'],
                'student_id' => $formData['student_id'],
                'student_name' => $formData['student_name']
            );

            $p_data = $this->evalModel->getGradingIP($searchdata);

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
            $paginator->setItemCountPerPage($this->gintPageCount);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

            $this->view->data = $formData;
        } else {
            $paginator = Zend_Paginator::factory(array());
        }


        $this->view->paginator = $paginator;
        $this->view->type = $type;
        $this->view->form = $form;
        $this->view->gradinglist = $gradingList;
    }

    public function downloadAction()
    {
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();
        //title
        $this->view->title = "Grading";

        $semester_id = $this->_getParam('semester_id');
        $course_id = $this->_getParam('course_id');
        $student_id = $this->_getParam('student_id');
        $student_name = $this->_getParam('student_name');

        $status = $this->thesisModel->getStatusByCode('approved', 1);
        $gradingListGrade = $this->defModel->getDataByTypeCode('Research Grading');
        $this->view->gradingListGrade = $gradingListGrade;

        $this->view->data = array();

        $searchdata = array(
            'p_status' => $status,
            'semester_id' => $semester_id,
            'course_id' => $course_id,
            'student_id' => $student_id,
            'student_name' => $student_name
        );
        $p_data = $this->evalModel->getGradingIP($searchdata, 1);

        $this->view->gradinglist = $p_data;

        $this->view->filename = date('Ymd') . '_Grading_Student.xls';
    }
}