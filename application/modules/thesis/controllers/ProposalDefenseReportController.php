<?php
class Thesis_ProposalDefenseReportController extends Base_Base
{
    public $regModel;
    public $repModel;
    public $session;

    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->regModel = new Thesis_Model_DbTable_Registration();
        $this->repModel = new Thesis_Model_DbTable_Report();
        $this->lobjCommon = new App_Model_Common();
        $this->gintPageCount = empty($larrInitialSettings['noofrowsingrid']) ? "5":$larrInitialSettings['noofrowsingrid'];

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US'	=> 'English',
            'ms_MY'	=> 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH.'/thesis';

    }

	public function proposalDefenseReportAction(){

        //title
        $this->view->title = "Proposal Defense";

        $this->view->form = new Thesis_Form_SearchProposalDefense();

        $p_data = $this->repModel->getProposalDefenseData(null, 'array');

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        $lintpage = $this->_getParam('page',1); // Paginator instance

        //post
        if ( $this->getRequest()->isPost() )
        {
            $formData = $this->getRequest()->getPost();

            $this->view->form->populate($formData);

            $p_data = $this->repModel->getProposalDefenseData( $formData, 'array' );
        }

        if ($p_data) {
            foreach ($p_data as $p_key => $proposal) {
//                $lecturers = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
                $examiners = $this->regModel->getExaminers($proposal['p_id'], 'proposal');

//                $p_data[$p_key]['lecturer'] = $lecturers;
                $p_data[$p_key]['examiner'] = $examiners;
            }
        }

        $paginator = $this->lobjCommon->fnPagination($p_data, $lintpage, $lintpagecount);
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page',1));

        $this->view->paginator = $paginator;
                
    }

    public function proposalDefenseExportAction()
    {

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $p_data = $this->repModel->getProposalDefenseData($formData, 'array');
        }

        if ($p_data) {
            foreach ($p_data as $p_key => $proposal) {
//                $lecturers = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
                $examiners = $this->regModel->getExaminers($proposal['p_id'], 'proposal');

//                $p_data[$p_key]['lecturer'] = $lecturers;
                $p_data[$p_key]['examiner'] = $examiners;
            }
        }

        $this->view->paginator = $p_data;

        $this->view->filename = date('Ymd') . '_thesis_proposal_defense.xls';
    }
	
}

?>