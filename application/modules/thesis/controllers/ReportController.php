<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 14/9/2015
 * Time: 12:02 PM
 */
class Thesis_ReportController extends Base_Base{
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Thesis_Model_DbTable_Report();
    }

    public function indexAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->view->title = $this->view->translate('Comprehensive Exam');
        $icampusDB = new icampus_Function_Application_AcademicProgress();

        $list = $this->model->comprehensiveExamList();

        if ($list){
            foreach ($list as $key => $loop){
                $credithoursinfo = $icampusDB->getExpectedGraduateStudent($loop['IdStudentRegistration'], 'caudit');
                //var_dump($credithoursinfo);

                if ($credithoursinfo){
                    $credithourscomplete = $credithoursinfo['credit_hours_completed_core']+$credithoursinfo['credit_hours_completed_elective']+$credithoursinfo['credit_hours_completed_research'];
                    $list[$key]['completech']=$credithourscomplete;
                }

                $sublist = $this->model->subList($loop['IdStudentRegistration']);
                if ($sublist){
                    $list[$key]['sublist']=$sublist;
                }
            }
        }
        //exit;
        $this->view->list = $list;
    }

    public function downloadAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $this->view->title = $this->view->translate('Comprehensive Exam');
        $icampusDB = new icampus_Function_Application_AcademicProgress();

        $list = $this->model->comprehensiveExamList();

        if ($list){
            foreach ($list as $key => $loop){
                $credithoursinfo = $icampusDB->getExpectedGraduateStudent($loop['IdStudentRegistration'], 'caudit');
                //var_dump($credithoursinfo);

                if ($credithoursinfo){
                    $credithourscomplete = $credithoursinfo['credit_hours_completed_core']+$credithoursinfo['credit_hours_completed_elective']+$credithoursinfo['credit_hours_completed_research'];
                    $list[$key]['completech']=$credithourscomplete;
                }

                $sublist = $this->model->subList($loop['IdStudentRegistration']);
                if ($sublist){
                    $list[$key]['sublist']=$sublist;
                }
            }
        }
        //exit;
        $this->view->list = $list;
        $this->view->filename = date('Ymd').'_CE_student.xls';
    }

    public function pdReportAction()
    {
        //title
        $this->view->title = "Supervisor List";

        $this->view->form = new Thesis_Form_SearchSupervisorPd();

        $gobjsessionsis = $this->view->gobjsessionsis = Zend_Registry::get('sis');
//        echo $this->gobjsessionsis->rolename;
        $auth = Zend_Auth::getInstance();
        $userid = $auth->getIdentity()->iduser;

        $regModel = new Thesis_Model_DbTable_Registration();
        $program = $regModel->getProgramByPd($userid);

        $prog = array();

        foreach($program as $p){
            $prog[] = $p['IdProgram'];
        }

        $progId = implode(',', $prog);

//        $status = $this->thesisModel->getStatusByCode('approved', 1);
        $repModel = new Thesis_Model_DbTable_Report();
        $query = $repModel->getSupervisor2(array('id_program' => $progId));

//        $this->view->data = $query;
        $this->view->program = $progId;

        $pageCount = 50;

        $session = new Zend_Session_Namespace('PDReportIndex');
        $session->setExpirationSeconds(60 * 10);

        $formData = '';
        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $this->view->form->populate($formData);
            $query = $repModel->getSupervisor2(array('id_program' => $progId),$formData);

            $session->corporateQuery = $query;
            $session->corporateParams = $formData;
            $this->_redirect($this->baseUrl.'/thesis/report/pd-report/search/1');
        }

        if($this->_getParam('search', 0)) {
            if(isset($session->corporateQuery) && isset($session->corporateQuery)) {
                $query    = $session->corporateQuery;
                $formData = $session->corporateParams;
                $this->view->form->populate($formData);
            }
        } else {
            unset($session->corporateQuery);
            unset($session->corporateParams);
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->formData = $formData;
        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
    }

    public function pdReportExportAction()
    {

        $this->_helper->layout->disableLayout();

        $auth = Zend_Auth::getInstance();
        $userid = $auth->getIdentity()->iduser;

        $regModel = new Thesis_Model_DbTable_Registration();
        $program = $regModel->getProgramByPd($userid);

        $prog = array();

        foreach($program as $p){
            $prog[] = $p['IdProgram'];
        }

        $progId = implode(',', $prog);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $repModel = new Thesis_Model_DbTable_Report();
            $query = $repModel->getSupervisor2(array('id_program' => $progId),$formData);
        }

        $this->view->paginator = $query;
        $this->view->formData = $formData;

        $this->view->filename = date('Ymd') . '_SupervisorList_Report.xls';
    }

    public function studentListAction()
    {
        $id = $this->_getParam('supervisor_id');

        //title
        $this->view->title = "Student List";

        $gobjsessionsis = $this->view->gobjsessionsis = Zend_Registry::get('sis');
//        echo $this->gobjsessionsis->rolename;
        $auth = Zend_Auth::getInstance();
        $userid = $auth->getIdentity()->iduser;

        $regModel = new Thesis_Model_DbTable_Registration();
        $program = $regModel->getProgramByPd($userid);

        $prog = array();

        foreach($program as $p){
            $prog[] = $p['IdProgram'];
        }

        $progId = implode(',', $prog);

        $repModel = new Thesis_Model_DbTable_Report();
        $data = $repModel->getStudentNumbers(array('id_program' => $progId,'supervisor_id' => $id));
//        print_r($data);exit;
        $this->view->data = $data;
//        print_r($data);exit;
    }

    public function studentDetailAction()
    {
        $this->view->title = "Student Detail";

        $id = $this->_getParam('id');
        $supervisor_id = $this->_getParam('supervisor_id');

        $regModel = new Thesis_Model_DbTable_Registration();

        //get proposal info
        $proposal = $regModel->getProposal($id);

        //get supervisors
        $supervisors = $this->view->supervisors = $regModel->getSupervisors($proposal['p_id'], 'proposal');

        $this->view->id = $id;
        $this->view->student_id = $proposal['student_id'];
        $this->view->proposal = $proposal;

        $repModel = new Thesis_Model_DbTable_Report();
        $activities = $repModel->getActivities($id, $proposal['course_id']);

        $this->view->activities = $activities;
        $this->view->supervisor_id = $supervisor_id;
    }

    public function supervisorReportAction()
    {
        //title
        $this->view->title = "Supervisor Report";

        $this->view->form = new Thesis_Form_SearchSupervisor();

        $pageCount = 50;
        $repModel = new Thesis_Model_DbTable_Report();
        $query = $repModel->getSupervisorReport();

        $session = new Zend_Session_Namespace('SupervisorReportIndex');
        $session->setExpirationSeconds(60 * 10);

        $formData = '';
        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $this->view->form->populate($formData);
            $query = $repModel->getSupervisorReport($formData);

            $session->corporateQuery = $query;
            $session->corporateParams = $formData;
            $this->_redirect($this->baseUrl.'/thesis/report/supervisor-report/search/1');
        }

        if($this->_getParam('search', 0)) {
            if(isset($session->corporateQuery) && isset($session->corporateQuery)) {
                $query    = $session->corporateQuery;
                $formData = $session->corporateParams;
                $this->view->form->populate($formData);
            }
        } else {
            unset($session->corporateQuery);
            unset($session->corporateParams);
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->formData = $formData;
        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
    }

    public function supervisorReportExportAction()
    {

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $repModel = new Thesis_Model_DbTable_Report();
            $query = $repModel->getSupervisorReport($formData);
        }

        $this->view->paginator = $query;

        $this->view->filename = date('Ymd') . '_Supervisor_Report.xls';
    }

    public function progressReportAction()
    {
        //title
        $this->view->title = "Progress Report";

        $this->view->form = new Thesis_Form_SearchProgress();

        $pageCount = 50;
        $repModel = new Thesis_Model_DbTable_Report();
//        $query = $repModel->getProgressReport();

        $session = new Zend_Session_Namespace('ProgressReportIndex');
        $session->setExpirationSeconds(60 * 10);

        $formData = array();
        $query = array();
        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $this->view->form->populate($formData);
            $query = $repModel->getProgressReport($formData);

            $session->query = $query;
            $session->formData = $formData;
            $this->_redirect($this->baseUrl.'/thesis/report/progress-report/search/1');
        }

        if($this->_getParam('search', 0)) {
            if(isset($session->query) && isset($session->query)) {
                $query    = $session->query;
                $formData = $session->formData;
                $this->view->form->populate($formData);
            }
        } else {
            unset($session->query);
            unset($session->progressParams);
        }
        if($query) {
            $paginator = Zend_Paginator::factory($query);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));
            $paginator->setItemCountPerPage($pageCount);
            $totalRecord = $paginator->getTotalItemCount();
            $this->view->paginator = $paginator;
            $this->view->totalRecord = $totalRecord;
        }
        $this->view->formData = $formData;
    }

    public function progressReportExportAction()
    {

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $repModel = new Thesis_Model_DbTable_Report();
            $query = $repModel->getProgressReport($formData);
        }

        $this->view->paginator = $query;
        $this->view->formData = $formData;

        $this->view->filename = date('Ymd') . '_Progress_Report.xls';
    }
}