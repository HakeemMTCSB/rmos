<?php
/*
	Munzir Rosdi
	9/18/2014
*/
class Thesis_AjaxController extends Base_Base 
{
	public function init() 
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$this->db = getDB();
	}

	public function studentAction()
	{
		$query = $this->_getParam('term');
		$type = $this->_getParam('type','name');

		$db = $this->db;
		
		

		if ( $query != '' )
		{
			if ( $type == 'id')
			{
				$select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.IdStudentRegistration as value'))        		
							  ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT_WS(" ",sa.registrationId, appl_fname,appl_mname,appl_lname) as label'));
				$select->where('sa.registrationId LIKE ?','%'.$query.'%'); 
			}
			else
			{
				$select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.IdStudentRegistration as value'))        		
							  ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as label'));
				$select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?','%'.$query.'%'); 
			}
		}


		$results = $db->fetchAll($select);

		echo Zend_Json::encode($results);
	}

    public function supervisorAction()
    {
        $query = $this->_getParam('term');

        $db = $this->db;

        if ( $query != '' )
        {
                $select = $db->select()->from(array('user' => 'tbl_user'), array('user.iduser as value'))
                    ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff',array())
                    ->joinLeft(array('college' => 'tbl_collegemaster'), 'college.IdCollege  = staff.IdCollege')
                    ->joinLeft(array('role' => 'tbl_definationms'), 'role.idDefinition  = user.IdRole')
                    ->joinLeft(array('salutation' => 'tbl_definationms'), 'salutation.idDefinition  = staff.FrontSalutation',array("CONCAT(salutation.DefinitionDesc,' ',staff.FullName) as label"))
                    ->where('user.UserStatus = 1')
                    ->where('staff.StaffAcademic = 0')
                ;
                $select->where('staff.FullName LIKE ?','%'.$query.'%');
        }


        $results = $db->fetchAll($select);

        echo Zend_Json::encode($results);
    }

	public function deleteSupervisorAction()
	{
		$db = $this->db;

		$id = $this->getParam('id');
        $type = $this->getParam('type');
        $reason = $this->getParam('reason');

		$this->regModel = new Thesis_Model_DbTable_Registration();

		$supervisor = $this->regModel->getSupervisor($id);

		if ( $id != '' )
		{
            $this->regModel->addSupervisorHistory([
                'pid'           => $supervisor['ps_pid'],
                'action'        => 'DELETE',
                'type'          => $type,
                'reason'        => $reason,
                'user_id'       => $supervisor['ps_supervisor_id'],
                'created_date'  => new Zend_Db_Expr('NOW()'),
                'created_by'    => Zend_Auth::getInstance()->getIdentity()->iduser
            ]);

			$db->delete('thesis_article_supervisor', 'ps_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteSupervisorsetupAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('thesis_setup_supervisor', 'ps_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteExaminerAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('thesis_article_examiner', 'ex_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteEvalitemAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('thesis_evaluation_item', 'cid = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteFileAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$regDB = new Thesis_Model_DbTable_Registration();
			$file = $regDB->getFile($id);

			if ( empty($file) )
			{
				$data = array('msg' => 'Invalid ID');

				echo Zend_Json::encode($data);
			}

			$db->delete('thesis_article_files', 'af_id = '.(int) $id);

			//unlink
			@unlink(DOCUMENT_PATH.$file['af_fileurl']);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
		}
	}

	public function deleteEmploymentAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('thesis_article_employment', 'ae_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
		}
	}

	public function getsheetAction()
	{
		$this->evalModel = new Thesis_Model_DbTable_Evaluation();
		$this->thesisModel = new Thesis_Model_DbTable_General();

		$this->_helper->viewRenderer->setNoRender(false);
		
		$formData = $this->getRequest()->getPost();
		
		$data = array('error' => 0, 'msg' => '', 'html' => '');
		
		$reg_sql = $this->thesisModel->subjectRegistered($formData['student_id']);

		$set = $this->evalModel->getEvaluationSetData($reg_sql);
		
		if ( empty($set) )
		{
			$data['error'] = 1;
			$data['msg'] = 'Invalid Set';
		}

		$set = $set[0];

		//data
		$items = $this->evalModel->getEvaluationItemsBySet($set['id']);
		$groups = $this->evalModel->getEvaluationGroupsBySet($set['id']);

		$itemsbygroup=array();
		foreach ( $items as $item )
		{
			$itemsbygroup[$item['group_id']][] = $item;
		}

		$this->view->formdata = $formData;
		$this->view->items = $itemsbygroup;
		$this->view->groups = $groups;
		
		//set
		$this->view->info = $set;

		$html = $this->view->render('evaluation/ajax-getsheet.phtml');
		
		$data['html'] = $html;

		echo Zend_Json::encode($data);
		exit;
	}
}