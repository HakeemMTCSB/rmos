<?php

class Thesis_ManageController extends Base_Base
{
    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->regModel = new Thesis_Model_DbTable_Registration();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->trackModel = new Thesis_Model_DbTable_Tracking();

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();

        $this->uploadDir = $this->uploadDir = DOCUMENT_PATH . '/thesis/docs';

    }

    public function trackingAction()
    {
        $this->view->ajax = 0;
        $errMsg = '';

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }


        $this->view->title = "Research - Tracking";

        //activities
        $getactivities = $this->trackModel->getActivitiesByResearch($id, $student_id);

        if (empty($getactivities)) {
            $errMsg = 'No activities found.';
        }

        //levels by sub
        $this->activities = $this->acttree = array();

        $this->_levellist = $getactivities;

        foreach ($this->_levellist as $activity) {
            $this->activities[$activity['pid']][] = $activity;
        }

        $this->getChildren(0);


        //views
        $this->view->errMsg = $errMsg;
        $this->view->activities = $this->acttree;
        $this->view->student_id = $student_id;
    }

    protected function getChildren($pid = 0, $depth = 1)
    {
        $sub = $this->activities;

        if (!isset($sub[$pid])) return;

        while (list($parent, $category) = each($sub[$pid])) {

            $this->acttree[] = array('info' => $category, 'depth' => $depth);

            $this->getChildren($category['id'], $depth + 1);
        }
    }

    public function activityAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');

        $activity = $this->trackModel->getActivity($id);

        if (empty($activity)) {
            throw new Exception('Invalid Activity ID');
        }

        if ($activity['created_by'] != $student_id) {
            throw new Exception('This Activity doesn\'t belong to you');
        }

        $this->view->title = "View Activity";

        //comments
        $this->view->comments = $this->trackModel->getComments($id, 'tracking');
        $this->view->activity = $activity;
    }

    public function progressAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');

        $reports = $this->trackModel->getReports($id, $type);
        $reports2 = $this->trackModel->getReports2($student_id);

        if ($reports) {
            foreach ($reports as $index => $rp) {
                $reports[$index]['comments'] = $this->trackModel->getComments($rp['id'], 'report');
            }
        }

//        $this->view->comments = $this->trackModel->getComments($id, 'report');

        $this->view->title = "View Reports";
//        echo '<pre>';
//print_r($reports[0]['research_id']);exit;
        //comments
        $this->view->listing = $reports;
        $this->view->listing2 = $reports2;

        $this->view->id = $reports;
    }

    public function evaluationAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');

        //get examiners
        $examiners = $this->view->examiners = $this->regModel->getExaminers($id, $type, 1);

        $research = $this->view->research = $this->thesisModel->getResearchById($id, $type);

        $gradingList = $this->view->grade = $this->defModel->getDataByTypeCode('Research Grading');

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->student_id = $student_id;
        $this->view->title = "Evaluation";

    }

    public function vivaAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');

        //get examiners
        $examiners = $this->view->examiners = $this->regModel->getExaminers($id, $type, 1);

        $research = $this->view->research = $this->thesisModel->getResearchById($id, $type);

        $gradingList = $this->view->grade = $this->defModel->getDataByTypeCode('Research Grading');

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->student_id = $student_id;
        $this->view->title = "Evaluation";

    }

    public function markAction()
    {
        $this->view->title = "Details";

        $db = getDb();
        $this->auth = Zend_Auth::getInstance();

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');

        $form = new Thesis_Form_AddEvaluationFile();
//        $form->setAction($this->view->url(array('module'=>'thesis','controller'=>'manage', 'action'=>'mark', 'id' => $id, 'student_id' => $student_id, 'type' => $type ),'default',true));

        $this->view->addfile_form = $form;

        //get examiners
        $this->view->assessors = $this->regModel->getExaminers($id, $type, 1);

        $proposaldata = $this->view->research = $this->thesisModel->getResearchById($id, $type);

        $this->view->gradinglist = $this->defModel->getDataByTypeCode('Research Grading');

//        $form->populate($gg);

        if ($this->_request->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($proposaldata);

            if (isset($formData['gradesubmit'])) {
                $db->update('thesis_proposal', array(
                    'grade_status' => $formData['grade'],
                    'total_marks' => $formData['mark'],
                    'grade_remarks' => $formData['remark'],
                    'grade_by' => $this->auth->getIdentity()->iduser,
                    'grade_date' => new Zend_Db_Expr('NOW()')
                ), array('p_id = ?' => $id));
            } else {

                //upload file
                try {


                    $uploadDir = DOCUMENT_PATH . '/thesis/' . $student_id;

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();

                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            $datetime = new Zend_Db_Expr('NOW()');
                            $user = $this->auth->getIdentity()->iduser;

                            //save file into db
                            $data = array(
                                'ef_pid' => $id,
                                'ef_description' => $formData['description'],
                                'ef_type' => $formData['type_id'],
                                'ef_student_id' => $student_id,
                                'ef_created_by' => $user,
                                'ef_created_at' => $datetime,
                                'ef_fileurl' => '/thesis/' . $student_id . '/' . $fileName,
                                'ef_filename' => $fileinfo['name'],
                                'ef_filesize' => $size
                            );

                            $lastid = $this->regModel->addEvaluationFile($data);

                            foreach ($formData['visibility'] as $v) {

                                $data = array(
                                    'v_ef_id' => $lastid,
                                    'v_ef_pid' => $id,
                                    'v_visible_id' => $v,
                                    'v_active' => 1,
                                    'v_created_at' => $datetime,
                                    'v_created_by' => $user
                                );

                                $this->regModel->addVisibility($data);
                            }


                        } //isuploaded
                    } //foreach
                } catch
                (Exception $e) {
                    throw new Exception($e->getMessage());
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Grading successfully updated"));

//            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-edit', 'id' => $id ), 'default', true));
            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'manage', 'action' => 'mark', 'id' => $id, 'type' => $type, 'student_id' => $student_id  ), 'default', true));

        }

        $this->view->type = $type;
    }

    public function gradeAction()
    {
        $this->view->title = "Details";

        $db = getDb();
        $this->auth = Zend_Auth::getInstance();

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');

        $form = new Thesis_Form_Grade();
        $this->view->form = $form;

        $form->institution->setAttrib('readonly', '');

        $proposaldata = $this->view->research = $this->thesisModel->getResearchById($id, $type);

        $this->view->gradinglist = $this->defModel->getDataByTypeCode('Research Grading');

        $form->populate($proposaldata);

        if ($this->_request->isPost()) {
            $formData = $this->getRequest()->getPost();

                $db->update('thesis_proposal', array(
                    'grade_status' => $formData['grade'],
                    'grade_viva' => $formData['grade_viva'],
                    'correction_viva' => $formData['correction_viva'],
                    'accept_viva' => $formData['accept_viva'],
                    'examiner_viva' => $formData['examiner_viva'],
                    'grade_viva_date' => new Zend_Db_Expr('NOW()')
                ), array('p_id = ?' => $id));

            //upload file
            try {

                $uploadDir = DOCUMENT_PATH . '/thesis/' . $proposaldata['student_id'];

                if (!is_dir($uploadDir)) {
                    if (mkdir_p($uploadDir) === false) {
                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                    }
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $files = $adapter->getFileInfo();

                $adapter->addValidator('NotExists', false, $uploadDir);
                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                foreach ($files as $no => $fileinfo) {

                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            $datetime = new Zend_Db_Expr('NOW()');
                            $user = $this->auth->getIdentity()->iduser;

                            //save file into db
                            $data2 = array(
                                'ef_pid' => $proposaldata['p_id'],
                                'type' => 'viva',
                                'ef_student_id' => $proposaldata['student_id'],
                                'ef_created_by' => $user,
                                'ef_created_at' => $datetime,
                                'ef_fileurl' => '/thesis/' . $proposaldata['student_id'] . '/' . $fileName,
                                'ef_filename' => $fileinfo['name'],
                                'ef_filesize' => $size
                            );

                            $lastid = $this->regModel->addEvaluationFile($data2);

                        } //isuploaded
                } //foreach
            } catch
            (Exception $e) {
                throw new Exception($e->getMessage());
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Grading successfully updated"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-edit', 'id' => $id ), 'default', true));
        }

        $this->view->type = $type;
    }

    public function getInstitutionAction()
    {
        $id = $this->_getParam('examiner_id', 0);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $examinerDb = new Thesis_Model_DbTable_General();
        $result = $examinerDb->getExaminerSingle($id);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public
    function filesAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');

        $files = $this->thesisModel->getResearchFiles($id, $type);

        $form = new Thesis_Form_AddResearchFile();
        $form->setAction($this->view->url(array('module' => 'thesis', 'controller' => 'manage', 'action' => 'files', 'id' => $id, 'student_id' => $student_id, 'type' => $type), 'default', true));

        $this->view->addfile_form = $form;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                //upload file
                try {
                    $uploadDir = DOCUMENT_PATH . '/thesis/' . $student_id;

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
//					$adapter->setDestination( $this->uploadDir );
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'type_id' => $formData['type_id'],
                                'research_id' => $id,
                                'description' => $formData['description'],
                                'student_id' => $student_id,
                                'research_type' => $type,
                                'created_by' => $this->auth->getIdentity()->iduser,
                                'created_date' => new Zend_Db_Expr('NOW()'),
                                'file_url' => '/thesis/' . $student_id . '/' . $fileName,
                                'file_name' => $fileinfo['name'],
                                'file_size' => $size
                            );

                            $this->thesisModel->addResearchFile($data);


                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                $this->_helper->flashMessenger->addMessage(array('success' => "New research file succesfully added"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => $type . '-edit', 'id' => $id, 'tab' => ($type == 'proposal' ? 6 : 5)), 'default', true));

            } // valid


        }

        //views
        $this->view->files = $files;
        $this->view->type = $type;
        $this->view->title = "Files";
    }

    public
    function historyAction()
    {

        $this->view->title = "View Submission History";

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $id = $this->_getParam('id');

        $reports = $this->regModel->getSubmissionHistoryByProposal($id);

        //comments
        $this->view->listing = $reports;
    }

    public function downloadProgressReportAction(){

        $this->_helper->layout->disableLayout();

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');

        $reports = $this->trackModel->getReports($id, $type);

        //comments
        $this->view->listing = $reports;

        $this->view->filename = $student_id.'_'.date('Ymd').'_progress_report.xls';
    }
}