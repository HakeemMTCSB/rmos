<?php
class Thesis_IndexController extends Base_Base 
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		
	}
	
	/*
		Index
	*/
	public function indexAction()
	{
		//title
    	$this->view->title = "Research";
	}

	public function portalAction()
	{
		$userDb = new GeneralSetup_Model_DbTable_User();
		$user = $this->auth->getIdentity();
		
		$userinfo = $userDb->getById($user->iduser);
		
		if ( !defined('THESIS_URL') )
		{
			die('THESIS_URL not defined');
		}

		$url = THESIS_URL;
			
		$data = array( 'username' => $userinfo['loginName'], 'password' => $userinfo['passwd'] );
		
		$token  = urlencode( encrypt( json_encode($data) ) );
		$this->_redirect($url.'/authentication/validate/type/1/?token='.$token, array('prependBase' => false));
	}
}
?>