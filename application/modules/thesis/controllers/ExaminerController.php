<?php

class Thesis_ExaminerController extends Base_Base
{
    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();

    }

    /*
        Examiner - Index
    */
    public function indexAction()
    {
        //title
        $this->view->title = "Research - Examiner";

        $p_data = $this->thesisModel->getExaminerData();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    /*
    Add
    */
    public function addAction()
    {
        $this->view->title = "Research - Add Examiner";

        $form = new Thesis_Form_Examiner();

        $this->view->error = '';

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form->populate($formData);

//            print_r($formData);exit;

            //$check_examiner_sem = $this->thesisModel->getExaminerSem($formData['semester_id']);
            //$get_total_examiner_sem = $this->thesisModel->getExaminerBySem($formData['semester_id']);

            //$semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
            //$semdet = $semDB->fngetSemestermainDetails($formData['semester_id']);

            //if ( $get_total_examiner_sem > $check_examiner_sem['es_max'] )
            //{
            //	$this->view->error = $this->view->translate('Maximum ('.$check_examiner_sem['es_max'].') examiner already assigned for selected semester ('.$semdet['SemesterMainCode'].')');
            //}
            //else
            //{
            //normal examiner
            //kena cachekan username and password, else hard to detect
            //will revisit issue later on


            //password read directly

            if ($formData['examiner_type'] == 0) {
                $userinfo = $this->thesisModel->getUser($formData['staff_id']);

                $formData['email'] = $userinfo['email'];
                //$formData['password'] = $userinfo['passwd'];
                $formData['password'] = '';
            } else {
                $formData['password'] = md5($formData['password']);
            }


            //data
            $data = array(
                'examiner_type' => $formData['examiner_type'],
                'staff_id' => $formData['staff_id'],
                'semester_id' => 0,
                'fullname' => $formData['fullname'],
                'email' => $formData['email'],
                'password' => $formData['password'],
                'contact_no' => $formData['contact_no'],
                'institution' => $formData['institution'],
                'created_by' => $this->auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()'),
                'start_date' => $formData['start_date'],
                'end_date' => $formData['end_date']
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "New examiner successfully added"));

            $status_id = $this->thesisModel->addExaminer($data);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'examiner', 'action' => 'index'), 'default', true));
            //}
        }

        //views
        $this->view->form = $form;
    }

    /*
    Examiner
    */
    public function editAction()
    {
        $this->view->title = "Research - Edit Examiner";

        $form = new Thesis_Form_Examiner();

        $this->view->error = '';

        $id = $this->getParam('id');

        $info = $this->thesisModel->getExaminerSingle($id);

        if (empty($info)) {
            throw new Exception('Invalid Examiner ID');
        } else {
            $form->populate($info);
            $this->view->info = $info;
//            $form->examiner_type->setAttrib('disabled', 'disabled');
            //$form->semester_id->setAttrib('disabled','disabled');

        }

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form->populate($formData);


            $data = array(
                'examiner_type' => $formData['examiner_type'],
                'fullname' => $formData['fullname'],
                'email' => $formData['email'],
                'contact_no' => $formData['contact_no'],
                'institution' => $formData['institution'],
                'start_date' => $formData['start_date'],
                'end_date' => $formData['end_date']
            );


            if ($formData['newpassword'] != '' && $info['examiner_type'] == 1) {
                $data['password'] = md5($formData['newpassword']);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $status_id = $this->thesisModel->updateExaminer($data, 'id=' . (int)$id);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'examiner', 'action' => 'edit', 'id' => $id), 'default', true));
        }

        //views
        $this->view->form = $form;
    }

    /*
        Examiner - Role
    */
    public function roleAction()
    {
        //title
        $this->view->title = "Research - Examiner Roles";

        $p_data = $this->thesisModel->getExaminerRoleData();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    public function roleAddAction()
    {
        $this->view->title = "Research - Add Examiner Role";

        $form = new Thesis_Form_ExaminerRole();


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'code' => $formData['code'],
                'description' => $formData['description'],
                'created_by' => $this->auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()')
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "New examiner role succesfully added"));

            $status_id = $this->thesisModel->addExaminerRole($data);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'examiner', 'action' => 'role'), 'default', true));
        }

        //views
        $this->view->form = $form;
    }

    public function roleEditAction()
    {
        $this->view->title = "Research - Edit Examiner Role";


        $form = new Thesis_Form_ExaminerRole();

        $id = $this->_getParam('id');

        $examinerrole = $this->thesisModel->getExaminerRole($id);

        if (empty($examinerrole)) {
            throw new Exception('Invalid Supervisor ID');
        }


        $form->populate($examinerrole);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'code' => $formData['code'],
                'description' => $formData['description'],
                'updated_by' => $this->auth->getIdentity()->iduser,
                'updated_date' => new Zend_Db_Expr('NOW()')
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

            $status_id = $this->thesisModel->updateExaminerRole($data, 'exr_id=' . (int)$id);

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'examiner', 'action' => 'role'), 'default', true));
        }

        //views
        $this->view->form = $form;
    }
}