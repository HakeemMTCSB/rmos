<?php

class Thesis_ApprovalController extends Base_Base
{
    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

    }

    public function indexAction()
    {

    }

    public function rejectAction()
    {

        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $id = $this->_getParam('id');
        $type = $this->_getParam('type');

        $status = $this->thesisModel->getStatusByCode('REJECT');

        if (empty($status)) {
            throw new Exception('Invalid Status ID');
        }

        if ($type == 'proposal') {

            //email
            $research = $this->regModel->getProposal($id, 1);
            $this->notifyReject($research, 'proposal');

            //update proposal
            $data = array(
                'rejected_date' => new Zend_Db_Expr('NOW()'),
                'rejected_by'   => $this->auth->getIdentity()->iduser,
                'updated_by'    => $this->auth->getIdentity()->iduser,
                'updated_date'  => new Zend_Db_Expr('NOW()'),
                'p_status'      => $status
            );

            $this->regModel->updateProposal($data, 'p_id = ' . (int)$id);

            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal successfully rejected"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal'), 'default', true));

        } else if ($type == 'exemption') {
            //email
            $research = $this->regModel->getExemption($id, 1);
            $this->notifyReject($research, 'exemption');

            $data = array(
                'rejected_date' => new Zend_Db_Expr('NOW()'),
                'rejected_by'   => $this->auth->getIdentity()->iduser,
                'updated_by'    => $this->auth->getIdentity()->iduser,
                'updated_date'  => new Zend_Db_Expr('NOW()'),
                'status'        => $status
            );

            $this->regModel->updateExemption($data, 'e_id = ' . (int)$id);

            $subRegDtl = $this->regModel->getStudentRegSubjectDtl($research['regsub_id']);

            if (isset($subRegDtl['invoice_id']) && $subRegDtl['invoice_id'] != 0) {
                $invoiceClass->generateCreditNote($subRegDtl['student_id'], $subRegDtl['subject_id'], $subRegDtl['invoice_id'], $subRegDtl['item_id'], 100);
            }

            if ($research['regsub_id'] != null) {
                $this->regModel->deleteSubject($research['regsub_id']);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "PPP successfully rejected"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal'), 'default', true));

        } else if ($type == 'articleship') {
            //email
            $research = $this->regModel->getArticleship($id, 1);
            $this->notifyReject($research, 'articleship');

            //update proposal
            $data = array(
                'rejected_date' => new Zend_Db_Expr('NOW()'),
                'rejected_by'   => $this->auth->getIdentity()->iduser,
                'updated_by'    => $this->auth->getIdentity()->iduser,
                'updated_date'  => new Zend_Db_Expr('NOW()'),
                'status'        => $status
            );

            $this->regModel->updateArticleship($data, 'a_id = ' . (int)$id);

            $subRegDtl = $this->regModel->getStudentRegSubjectDtl($research['regsub_id']);

            if (isset($subRegDtl['invoice_id']) && $subRegDtl['invoice_id'] != 0) {
                $invoiceClass->generateCreditNote($subRegDtl['student_id'], $subRegDtl['subject_id'], $subRegDtl['invoice_id'], $subRegDtl['item_id'], 100);
            }

            if ($research['regsub_id'] != null) {
                $this->regModel->deleteSubject($research['regsub_id']);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Articleship successfully rejected"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal'), 'default', true));
        }
    }

    /*
        Approval - Proposal
    */
    public function proposalAction()
    {
        //title
        $this->view->title = "Research - Proposal Approval";

        $this->view->form = new Thesis_Form_SearchResearchProposal();

        $gradform = new Thesis_Form_AddGradingFile();
        $this->view->addfile_form = $gradform;

        $gobjsessionsis = $this->view->gobjsessionsis = Zend_Registry::get('sis');

//        $status = $this->thesisModel->getStatusByCode('approved', 1);



            $p_data = $this->regModel->getProposalApproval();

//        print_r($p_data);exit;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if(isset($formData['save'])) {

                $this->view->form->populate($formData);
                $formData['pd_status'] = $status;
                $p_data = $this->regModel->getProposalApproval($formData);

            }else{

                $count = count($formData['chk']);

                if ($count > 0) {
                    foreach ($formData['chk'] as $app_id) {

                        if ($this->_request->getPost('approve')) {
                            $pd_status = 'approved';
                            $status = $this->thesisModel->getStatusByCode($pd_status,1);

                            if (empty($status)) {
                                throw new Exception('Invalid Status ID');
                            }

                            //email
                            $research = $this->regModel->getProposal($app_id, 1);
                            $pdf_url = $this->notifyApprove($research, 'proposal');

                            //update proposal
                            $data = array(
                                'approved_date' => new Zend_Db_Expr('NOW()'),
                                'approved_by'   => $this->auth->getIdentity()->iduser,
                                'pd_update_by'    => $this->auth->getIdentity()->iduser,
                                'pd_updated_date'  => new Zend_Db_Expr('NOW()'),
                                'pd_status'      => $status,
                                'pdf_file'      => $pdf_url
                            );

                            //upload file
                            try {

                                $uploadDir = DOCUMENT_PATH . '/thesis/' . $research['student_id'];

                                if (!is_dir($uploadDir)) {
                                    if (mkdir_p($uploadDir) === false) {
                                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                                    }
                                }

                                $adapter = new Zend_File_Transfer_Adapter_Http();

                                $files = $adapter->getFileInfo();

                                $adapter->addValidator('NotExists', false, $uploadDir);
                                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                                foreach ($files as $no => $fileinfo) {

                                    $pid = (explode("_", $no));

                                    if ($pid[1] == $app_id ) {

                                        if($fileinfo['name'] == '') {
                                            $this->_helper->flashMessenger->addMessage(array('error' => "Please attach file for this student"." | Name : ".$research['student_name']." | Activity : ".$research['DefinitionDesc']));
                                            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'proposal'), 'default', true));
                                        }

                                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                                        if ($adapter->isUploaded($no)) {
                                            $ext = getext($fileinfo['name']);

                                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                                            $fileUrl = $uploadDir . '/' . $fileName;

                                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                                            $adapter->setOptions(array('useByteString' => false));

                                            $size = $adapter->getFileSize($no);

                                            if (!$adapter->receive($no)) {
                                                $errmessage = array();
                                                if (is_array($adapter->getMessages())) {
                                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                                        $errmessage[] = $errmsg;
                                                    }

                                                    throw new Exception(implode('<br />', $errmessage));
                                                }
                                            }

                                            $datetime = new Zend_Db_Expr('NOW()');
                                            $user = $this->auth->getIdentity()->iduser;

                                            //save file into db
                                            $data2 = array(
                                                'ef_pid' => $research['p_id'],
                                                'type' => 'approval',
                                                'ef_student_id' => $research['student_id'],
                                                'ef_created_by' => $user,
                                                'ef_created_at' => $datetime,
                                                'ef_fileurl' => '/thesis/' . $research['student_id'] . '/' . $fileName,
                                                'ef_filename' => $fileinfo['name'],
                                                'ef_filesize' => $size
                                            );

                                            $lastid = $this->regModel->addEvaluationFile($data2);

                                        } //isuploaded
                                    }
                                } //foreach
                            } catch
                            (Exception $e) {
                                throw new Exception($e->getMessage());
                            }

                            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal(s) successfully approved"));
                        } elseif ($this->_request->getPost('reject')) {
                            $pd_status = 'rejected';
                            $status = $this->thesisModel->getStatusByCode($pd_status,1);

                            if (empty($status)) {
                                throw new Exception('Invalid Status ID');
                            }

                            //email
                            $research = $this->regModel->getProposal($app_id, 1);
                            $this->notifyReject($research, 'proposal');

                            //update proposal
                            $data = array(
                                'pd_updated_date' => new Zend_Db_Expr('NOW()'),
                                'pd_update_by'   => $this->auth->getIdentity()->iduser,
                                'pd_status'      => $status
                            );


                            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal(s) successfully rejected"));
                        }

                        $this->regModel->updateProposal($data, 'p_id = ' . (int)$app_id);
                        $this->regModel->saveSubmission($app_id, 'proposal', $pd_status);
                    }

                    $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'proposal'), 'default', true));
                }
            }
        }

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    //pd approval
    public function pdApprovalAction()
    {
        //title
        $this->view->title = "Programme Director Approval";

        $this->view->form = new Thesis_Form_SearchResearchProposal();

        $gradform = new Thesis_Form_AddGradingFile();
        $this->view->addfile_form = $gradform;

        $gobjsessionsis = $this->view->gobjsessionsis = Zend_Registry::get('sis');
//        echo $this->gobjsessionsis->rolename;
        $auth = Zend_Auth::getInstance();
        $this->view->userid = $userid = $auth->getIdentity()->iduser;

        $program = $this->regModel->getProgramByPd($userid);

        $prog = array();

        foreach($program as $p){
            $prog[] = $p['IdProgram'];
        }

        $progId = implode(',', $prog);

        $status = $this->thesisModel->getStatusByCode('approved', 1);

        $p_data = $this->regModel->getPdApproval(array('pd_status' => $status,'id_program' => $progId));

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if(isset($formData['save'])) {

                $this->view->form->populate($formData);
                $formData['pd_status'] = $status;
                $p_data = $this->regModel->getPdApproval($formData);

            }else{

                $count = count($formData['chk']);

                if ($count > 0) {
                    foreach ($formData['chk'] as $k => $app_id) {

                        if ($this->_request->getPost('approve')) {
                            $pd_status = 'approved';
                            $status = $this->thesisModel->getStatusByCode($pd_status,1);

                            if (empty($status)) {
                                throw new Exception('Invalid Status ID');
                            }

//                            //email
//                            $research = $this->regModel->getProposal($app_id, 1);
//                            $pdf_url = $this->notifyApprove($research, 'proposal');

                            //update proposal
                            $data = array(
//                                'approved_date' => new Zend_Db_Expr('NOW()'),
//                                'approved_by'   => $this->auth->getIdentity()->iduser,
//                                'pd_update_by'    => $this->auth->getIdentity()->iduser,
//                                'pd_updated_date'  => new Zend_Db_Expr('NOW()'),
                                'pd_approval'      => $status,
                                'approval_remarks'      => $formData['remarks'][$k]
//                                'pdf_file'      => $pdf_url
                            );

                            //upload file
//                            try {
//
//                                $uploadDir = DOCUMENT_PATH . '/thesis/' . $research['student_id'];
//
//                                if (!is_dir($uploadDir)) {
//                                    if (mkdir_p($uploadDir) === false) {
//                                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
//                                    }
//                                }
//
//                                $adapter = new Zend_File_Transfer_Adapter_Http();
//
//                                $files = $adapter->getFileInfo();
//
//                                $adapter->addValidator('NotExists', false, $uploadDir);
//                                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));
//
//                                foreach ($files as $no => $fileinfo) {
//
//                                    $pid = (explode("_", $no));
//
//                                    if ($pid[1] == $app_id ) {
//
//                                        if($fileinfo['name'] == '') {
//                                            $this->_helper->flashMessenger->addMessage(array('error' => "Please attach file for this student"." | Name : ".$research['student_name']." | Activity : ".$research['DefinitionDesc']));
//                                            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'proposal'), 'default', true));
//                                        }
//
//                                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
//
//                                        if ($adapter->isUploaded($no)) {
//                                            $ext = getext($fileinfo['name']);
//
//                                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
//                                            $fileUrl = $uploadDir . '/' . $fileName;
//
//                                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
//                                            $adapter->setOptions(array('useByteString' => false));
//
//                                            $size = $adapter->getFileSize($no);
//
//                                            if (!$adapter->receive($no)) {
//                                                $errmessage = array();
//                                                if (is_array($adapter->getMessages())) {
//                                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
//                                                        $errmessage[] = $errmsg;
//                                                    }
//
//                                                    throw new Exception(implode('<br />', $errmessage));
//                                                }
//                                            }
//
//                                            $datetime = new Zend_Db_Expr('NOW()');
//                                            $user = $this->auth->getIdentity()->iduser;
//
//                                            //save file into db
//                                            $data2 = array(
//                                                'ef_pid' => $research['p_id'],
//                                                'type' => 'approval',
//                                                'ef_student_id' => $research['student_id'],
//                                                'ef_created_by' => $user,
//                                                'ef_created_at' => $datetime,
//                                                'ef_fileurl' => '/thesis/' . $research['student_id'] . '/' . $fileName,
//                                                'ef_filename' => $fileinfo['name'],
//                                                'ef_filesize' => $size
//                                            );
//
//                                            $lastid = $this->regModel->addEvaluationFile($data2);
//
//                                        } //isuploaded
//                                    }
//                                } //foreach
//                            } catch
//                            (Exception $e) {
//                                throw new Exception($e->getMessage());
//                            }

                            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal(s) successfully approved"));
                        } elseif ($this->_request->getPost('reject')) {
                            $pd_status = 'rejected';
                            $status = $this->thesisModel->getStatusByCode($pd_status,1);

                            if (empty($status)) {
                                throw new Exception('Invalid Status ID');
                            }

//                            //email
//                            $research = $this->regModel->getProposal($app_id, 1);
//                            $this->notifyReject($research, 'proposal');

                            //update proposal
                            $data = array(
//                                'pd_updated_date' => new Zend_Db_Expr('NOW()'),
//                                'pd_update_by'   => $this->auth->getIdentity()->iduser,
                                'pd_approval'      => $status,
                                'approval_remarks'      => $formData['remarks'][$k]
                            );


                            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal(s) successfully rejected"));
                        }

                        $this->regModel->updateProposal($data, 'p_id = ' . (int)$app_id);
                        $this->regModel->saveSubmission($app_id, 'proposal', $pd_status);
                    }

                    $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'pd-approval'), 'default', true));
                }
            }
        }

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    /*
        Approval - Articleship
    */
    public function articleshipAction()
    {
        //title
        $this->view->title = "Research - Articleship Approval";

        $status = $this->thesisModel->getStatusByCode('APPLIED');

        $p_data = $this->regModel->getArticleshipData(array('status' => $status));

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $count = count($formData['chk']);

            if ($count > 0) {
                foreach ($formData['chk'] as $app_id) {
                    if ($this->_request->getPost('approve')) {
                        $status = $this->thesisModel->getStatusByCode('APPROVED');

                        if (empty($status)) {
                            throw new Exception('Invalid Status ID');
                        }

                        //email
                        $research = $this->regModel->getArticleship($app_id, 1);
                        $pdf_url = $this->notifyApprove($research, 'articleship');

                        //update proposal
                        $data = array(
                            'approved_date' => new Zend_Db_Expr('NOW()'),
                            'approved_by'   => $this->auth->getIdentity()->iduser,
                            'updated_by'    => $this->auth->getIdentity()->iduser,
                            'updated_date'  => new Zend_Db_Expr('NOW()'),
                            'status'        => $status,
                            'pdf_file'      => $pdf_url
                        );

                        $this->_helper->flashMessenger->addMessage(array('success' => "Articleship(s) successfully approved"));
                    } elseif ($this->_request->getPost('reject')) {
                        $status = $this->thesisModel->getStatusByCode('REJECT');

                        if (empty($status)) {
                            throw new Exception('Invalid Status ID');
                        }

                        //update proposal
                        $data = array(
                            'rejected_date' => new Zend_Db_Expr('NOW()'),
                            'rejected_by'   => $this->auth->getIdentity()->iduser,
                            'updated_by'    => $this->auth->getIdentity()->iduser,
                            'updated_date'  => new Zend_Db_Expr('NOW()'),
                            'status'        => $status
                        );

                        $this->_helper->flashMessenger->addMessage(array('success' => "Articleship(s) successfully rejected"));
                    }

                    $this->regModel->updateArticleship($data, 'a_id = ' . (int)$app_id);

                }

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'articleship'), 'default', true));
            }
        }
    }


    /*
        Approval - Exemption
    */
    public function exemptionAction()
    {
        //title
        $this->view->title = "Research - Exemption Approval";

        $status = $this->thesisModel->getStatusByCode('APPLIED');

        $p_data = $this->regModel->getExemptionData(array('status' => $status));

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $count = count($formData['chk']);

            if ($count > 0) {
                foreach ($formData['chk'] as $app_id) {
                    if ($this->_request->getPost('approve')) {
                        $status = $this->thesisModel->getStatusByCode('APPROVED');

                        if (empty($status)) {
                            throw new Exception('Invalid Status ID');
                        }

                        //email
                        $research = $this->regModel->getExemption($app_id, 1);
                        $pdf_url = $this->notifyApprove($research, 'ppp');

                        //update proposal
                        $data = array(
                            'approved_date' => new Zend_Db_Expr('NOW()'),
                            'approved_by'   => $this->auth->getIdentity()->iduser,
                            'updated_by'    => $this->auth->getIdentity()->iduser,
                            'updated_date'  => new Zend_Db_Expr('NOW()'),
                            'status'        => $status,
                            'pdf_file'      => $pdf_url
                        );

                        $this->_helper->flashMessenger->addMessage(array('success' => "PPP successfully approved"));
                    } elseif ($this->_request->getPost('reject')) {
                        $status = $this->thesisModel->getStatusByCode('REJECT');

                        if (empty($status)) {
                            throw new Exception('Invalid Status ID');
                        }

                        //update proposal
                        $data = array(
                            'rejected_date' => new Zend_Db_Expr('NOW()'),
                            'rejected_by'   => $this->auth->getIdentity()->iduser,
                            'updated_by'    => $this->auth->getIdentity()->iduser,
                            'updated_date'  => new Zend_Db_Expr('NOW()'),
                            'status'        => $status
                        );

                        $this->_helper->flashMessenger->addMessage(array('success' => "PPP successfully rejected"));
                    }

                    $this->regModel->updateExemption($data, 'e_id = ' . (int)$app_id);


                }


                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'exemption'), 'default', true));
            }
        }
    }

    /* Notify Approve */

    protected function notifyApprove($research, $type = '')
    {
        $emailDb = new App_Model_Email();
        $commDb = new Communication_Model_DbTable_Template();

        $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-approved', 0, 1);
        $template = $gettemplate[0];

        if (empty($template)) {
            throw new Exception('Invalid Template');
        }

        //email
        $dataEmail = array(
            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
            'subject'         => $template['tpl_name'],
            'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
            'date_que'        => date('Y-m-d H:i:s')
        );

        $emailDb->add($dataEmail);

    }

    /* Notify Reject */

    protected function notifyReject($research, $type = '')
    {
        $emailDb = new App_Model_Email();
        $commDb = new Communication_Model_DbTable_Template();

        $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-rejected', 0, 1);
        $template = $gettemplate[0];

        if (empty($template)) {
            throw new Exception('Invalid Template');
        } else {

            $dataEmail = array(
                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                'subject'         => $template['tpl_name'],
                'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                'date_que'        => date('Y-m-d H:i:s')
            );

            $emailDb->add($dataEmail);
        }
    }

    public function generatePdf($data = '')
    {
        if (empty($data)) {
            throw new Exception('Empty Data for PDF Generation');
        }

        $url = $this->uploadDir . '/notify';

        //option pdf
        $option = array(
            'content'        => $data['html'],
            'save'           => true,
            'file_extension' => 'pdf',
            'save_path'      => $url,
            'file_name'      => $data['filename'],
            'css'            => '@page { margin: 110px 50px 50px 50px}
			body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
			table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
			table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
			table.tftable tr {background-color:#ffffff;}
			table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
            'header'         => '<script type="text/php">
			if ( isset($pdf) ) {
					$header = $pdf->open_object();

					$w = $pdf->get_width();
					$h = $pdf->get_height();
					$color = array(0,0,0);

					$img_w = 180; 
					$img_h = 59;
					$pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

					// Draw a line along the bottom
					$font = Font_Metrics::get_font("Helvetica");
					$size = 6;
					$text_height = Font_Metrics::get_font_height($font, $size)+2;
					$y = $h - (3.5 * $text_height)-10;
					$pdf->line(10, $y, $w - 10, $y, $color, 1);

					// Draw a second line along the bottom
					$y = $h - (3.5 * $text_height)+10;
					$pdf->line(10, $y, $w - 10, $y, $color, 1);

					$pdf->close_object();

					$pdf->add_object($header, "all");
			}
			</script>',
            'footer'         => '<script type="text/php">
			if ( isset($pdf) ) {
				$footer = $pdf->open_object();

				$font = Font_Metrics::get_font("Helvetica");
				$size = 6;
				$color = array(0,0,0);
				$text_height = Font_Metrics::get_font_height($font, $size)+2;

				$w = $pdf->get_width();
				$h = $pdf->get_height();


				// Draw a line along the bottom
				$y = $h - (3.5 * $text_height)-10;
				//$pdf->line(10, $y, $w - 10, $y, $color, 1);

				//1st row footer
				$text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
				$width = Font_Metrics::get_text_width($text, $font, $size);	
				$y = $h - (2 * $text_height)-20;
				$x = ($w - $width) / 2.0;

				$pdf->page_text($x, $y, $text, $font, $size, $color);

				//2nd row footer
				$text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
				$width = Font_Metrics::get_text_width($text, $font, $size);	
				$y = $h - (1 * $text_height)-20;
				$x = ($w - $width) / 2.0;

				$pdf->page_text($x, $y, $text, $font, $size, $color);



				$pdf->close_object();

				$pdf->add_object($footer, "all");
		}
		</script>'
        );

        //genarate attachment
        $pdf = generatePdf($option);

    }

    public function proposalViewAction()
    {
        $this->view->title = "Student Details";

        $form = new Thesis_Form_EditProposal(array('full' => true));

        $supervisor_form = new Thesis_Form_AddSupervisor();
        $this->view->supervisor_form = $supervisor_form;

        $examiner_form = new Thesis_Form_AddExaminer();
        $this->view->examiner_form = $examiner_form;

        $id = $this->_getParam('id');
        $activity_id = $this->_getParam('activity_id');
        $this->view->tab = $this->_getParam('tab');

        //get proposal info
        $proposal = $this->view->proposal = $this->regModel->getProposalActivity($id, $activity_id);
//        print_r($proposal);exit;
        $this->view->id = $id;
//        $form->p_title->setAttrib('disabled', 'disabled');

        if (empty($proposal)) {
            throw new Exception('Invalid Proposal ID');
        }

        //get files
        $uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($proposal['p_id'], 'proposal');

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

        $supervisor_ids = array();
        foreach ($supervisors as $sup) {
            $supervisor_ids[] = $sup['ps_id'];
        }

        $this->view->supervisor_ids = implode(',', $supervisor_ids);

        //check main supervisor
        $supervisor_roles = array();
        $mainrole = '';

        foreach ($supervisors as $role) {

            $supervisor_roles[] = $role['ps_supervisor_role'];

            if ($role['ps_supervisor_role'] == 1) {
                $mainrole = 'selected';
            }

        }

        $this->view->supervisor_roles = implode(',', $supervisor_roles);

        $this->view->mainrole = $mainrole;


        //get examiners
        $examiners = $this->view->examiners = $this->regModel->getExaminers($proposal['p_id'], 'proposal');
        $examiners_ids = array();
        foreach ($examiners as $exp) {
            $exminers_ids[] = $exp['ex_id'];
        }

        $this->view->examiners_ids = implode(',', $examiners_ids);

        //repopulate form
        $form->populate($proposal);

        $this->view->activity_id = $proposal['activity_id'];

        $this->view->proposal = $proposal;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                //update proposal
                $data = array(
                    'p_title'         => $formData['p_title'],
                    'student_id'      => $formData['student_id'],
                    'p_category'      => $formData['p_category'],
                    'p_topic'         => $formData['p_topic'],
                    'p_description'   => $formData['p_description'],
                    'p_status'        => $formData['p_status'],
                    'activity_id'     => $formData['activity_id'],
                    'meeting_date'    => $formData['meeting_date'],
                    'updated_by'      => $this->auth->getIdentity()->iduser,
                    'updated_date'    => new Zend_Db_Expr('NOW()'),
                    'archive'         => $formData['archive'] ?? null,
                    'completion_date' => $formData['completion_date'],
                    'semester_start'  => $formData['semester_start'],
                    'semester_end'    => $formData['semester_end'],
                    'course_id'       => $formData['course_id'],
                    'applied_date'    => $formData['applied_date'],
                    'pd_status'       => isset($formData['submit_proposal']) ? $this->thesisModel->getStatusByCode('applied', 1) : $proposal['pd_status'],
                    'pd_by'           => isset($formData['submit_proposal']) ? $this->auth->getIdentity()->iduser : $proposal['pd_by'],
                    'pd_date'         => isset($formData['submit_proposal']) ? new Zend_Db_Expr('NOW()') : $proposal['pd_date'],
                    'pd_role'         => isset($formData['submit_proposal']) ? 'admin' : $proposal['pd_role'],
                );

                $this->regModel->updateProposal($data, 'p_id = ' . (int)$proposal['p_id']);


                //insert/update thesis_submission  //applied
                if (isset($formData['submit_proposal'])) {
                    $this->regModel->saveSubmission($proposal['p_id'], 'proposal', 'applied');
                } else {
                    $p_status = $this->thesisModel->getStatus($formData['p_status']);
                    if ($proposal['p_status'] != $formData['p_status']) {
                        $this->regModel->saveSubmission($proposal['p_id'], 'registration', $p_status['status_code']);
                    }
                }

                $proposal_id = $proposal['p_id'];

                //upload file
                try {
                    $uploadDir = $this->uploadDir . '/' . $formData['student_id'];

                    if (!is_dir($uploadDir)) {
                        if (mkdir_p($uploadDir) === false) {
                            throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                        }
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();


                    $files = $adapter->getFileInfo();
                    $adapter->addValidator('NotExists', false, $uploadDir);
                    //$adapter->setDestination($this->uploadDir);
                    //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                    $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                    foreach ($files as $no => $fileinfo) {
                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                        if ($adapter->isUploaded($no)) {
                            $ext = getext($fileinfo['name']);

                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                            $fileUrl = $uploadDir . '/' . $fileName;

                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                            $adapter->setOptions(array('useByteString' => false));

                            $size = $adapter->getFileSize($no);

                            if (!$adapter->receive($no)) {
                                $errmessage = array();
                                if (is_array($adapter->getMessages())) {
                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                        $errmessage[] = $errmsg;
                                    }

                                    throw new Exception(implode('<br />', $errmessage));
                                }
                            }

                            //save file into db
                            $data = array(
                                'af_pid'      => $proposal_id,
                                'af_type'     => 'proposal',
                                'af_filename' => $fileinfo['name'],
                                'af_fileurl'  => '/thesis/' . $formData['student_id'] . '/' . $fileName,
                                'af_filesize' => $size
                            );

                            $this->regModel->addFile($data);

                        } //isuploaded

                    } //foreach
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

                //add supervisor
                for ($i = 0; $i < count($formData['supervisor_userid']);) {
                    //save file into db

                    $data = array(
                        'ps_pid'             => $proposal_id,
                        'ps_type'            => 'proposal',
                        'ps_supervisor_type' => $formData['supervisor_type'][$i],
                        'ps_supervisor_id'   => $formData['supervisor_userid'][$i],
                        'ps_supervisor_role' => $formData['supervisor_role'][$i],
                        'ps_addedby'         => $this->auth->getIdentity()->iduser,
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $supervisor_id = $this->regModel->addSupervisor($data);

                    $this->regModel->addSupervisorHistory([
                        'pid'          => $proposal_id,
                        'action'       => 'ADD',
                        'type'         => 'proposal',
                        'user_id'      => $formData['supervisor_userid'][$i],
                        'created_date' => new Zend_Db_Expr('NOW()'),
                        'created_by'   => $this->auth->getIdentity()->iduser
                    ]);

                    $i++;
                }

                //add examiners
                for ($i = 0; $i < count($formData['examiner_userid']);) {
                    //save file into db
                    $data = array(
                        'ex_pid'           => $proposal_id,
                        'ex_type'          => 'proposal',
                        'ex_examiner_id'   => $formData['examiner_userid'][$i],
                        'ex_examiner_role' => $formData['examiner_role'][$i],
                        'ex_active'        => $formData['examiner_active'][$i],
                        'ex_addedby'       => $this->auth->getIdentity()->iduser,
                        'ex_addeddate'     => new Zend_Db_Expr('NOW()')
                    );

                    $this->regModel->addExaminer($data);

                    $i++;
                }

                $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

                $supervisor_name = array();
                foreach ($supervisors as $sup_name) {

                    $supervisor_name[] = $sup_name['supervisor_name'];

                }

                $sname = "<p>" . implode('<br>', $supervisor_name) . "</p>";

                //send registered email
                if ($formData['p_status'] == 2) {

                    $emailDb = new App_Model_Email();
                    $commDb = new Communication_Model_DbTable_Template();

                    $research = $this->regModel->getProposal($proposal_id);
//                    print_r($research);exit;
                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'registration', 0, 1);
                    $template = $gettemplate[0];

                    $research['research_type'] = 'proposal';

                    if (!isset($research)) {
                        throw new Exception('Invalid Research');
                    }

                    if (empty($template)) {
                        throw new Exception('Invalid Template. Did you change the template name recently?');
                    }

                    $research['supervisor_info'] = $sname;

                    //email
                    $dataEmail = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                        'subject'         => $template['tpl_name'],
                        'content'         => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                        'date_que'        => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail);
                }
                //send registered email

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'registration', 'action' => 'proposal-edit', 'activity_id' => $proposal['activity_id'], 'id' => $id, 'student_id' => $proposal['student_id']), 'default', true));

            } // valid


        }

        //views
        $this->view->form = $form;
    }

    public function pdApprovalReportAction()
    {
        //title
//        echo 'test';exit;
        $this->view->title = "Programme Director Approval Report";

        $this->view->form = new Thesis_Form_SearchResearchProposal();

        $gradform = new Thesis_Form_AddGradingFile();
        $this->view->addfile_form = $gradform;

        $gobjsessionsis = $this->view->gobjsessionsis = Zend_Registry::get('sis');
//        echo $this->gobjsessionsis->rolename;
        $auth = Zend_Auth::getInstance();
        $userid = $auth->getIdentity()->iduser;

        $program = $this->regModel->getProgramByPd($userid);

        $prog = array();

        foreach($program as $p){
            $prog[] = $p['IdProgram'];
        }

        $progId = implode(',', $prog);

        $status = $this->thesisModel->getStatusByCode('approved', 1);

        $p_data = $this->regModel->getPdApprovalReport(array('id_program' => $progId));

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if(isset($formData['save'])) {

                $this->view->form->populate($formData);
                $formData['pd_status'] = $status;
                $p_data = $this->regModel->getPdApproval($formData);

            }else{

                $count = count($formData['chk']);

                if ($count > 0) {
                    foreach ($formData['chk'] as $k => $app_id) {

                        if ($this->_request->getPost('approve')) {
                            $pd_status = 'approved';
                            $status = $this->thesisModel->getStatusByCode($pd_status,1);

                            if (empty($status)) {
                                throw new Exception('Invalid Status ID');
                            }

//                            //email
//                            $research = $this->regModel->getProposal($app_id, 1);
//                            $pdf_url = $this->notifyApprove($research, 'proposal');

                            //update proposal
                            $data = array(
//                                'approved_date' => new Zend_Db_Expr('NOW()'),
//                                'approved_by'   => $this->auth->getIdentity()->iduser,
//                                'pd_update_by'    => $this->auth->getIdentity()->iduser,
//                                'pd_updated_date'  => new Zend_Db_Expr('NOW()'),
                                'pd_approval'      => $status,
                                'approval_remarks'      => $formData['remarks'][$k]
//                                'pdf_file'      => $pdf_url
                            );

                            //upload file
//                            try {
//
//                                $uploadDir = DOCUMENT_PATH . '/thesis/' . $research['student_id'];
//
//                                if (!is_dir($uploadDir)) {
//                                    if (mkdir_p($uploadDir) === false) {
//                                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
//                                    }
//                                }
//
//                                $adapter = new Zend_File_Transfer_Adapter_Http();
//
//                                $files = $adapter->getFileInfo();
//
//                                $adapter->addValidator('NotExists', false, $uploadDir);
//                                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));
//
//                                foreach ($files as $no => $fileinfo) {
//
//                                    $pid = (explode("_", $no));
//
//                                    if ($pid[1] == $app_id ) {
//
//                                        if($fileinfo['name'] == '') {
//                                            $this->_helper->flashMessenger->addMessage(array('error' => "Please attach file for this student"." | Name : ".$research['student_name']." | Activity : ".$research['DefinitionDesc']));
//                                            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'proposal'), 'default', true));
//                                        }
//
//                                        $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
//
//                                        if ($adapter->isUploaded($no)) {
//                                            $ext = getext($fileinfo['name']);
//
//                                            $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
//                                            $fileUrl = $uploadDir . '/' . $fileName;
//
//                                            $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
//                                            $adapter->setOptions(array('useByteString' => false));
//
//                                            $size = $adapter->getFileSize($no);
//
//                                            if (!$adapter->receive($no)) {
//                                                $errmessage = array();
//                                                if (is_array($adapter->getMessages())) {
//                                                    foreach ($adapter->getMessages() as $errtype => $errmsg) {
//                                                        $errmessage[] = $errmsg;
//                                                    }
//
//                                                    throw new Exception(implode('<br />', $errmessage));
//                                                }
//                                            }
//
//                                            $datetime = new Zend_Db_Expr('NOW()');
//                                            $user = $this->auth->getIdentity()->iduser;
//
//                                            //save file into db
//                                            $data2 = array(
//                                                'ef_pid' => $research['p_id'],
//                                                'type' => 'approval',
//                                                'ef_student_id' => $research['student_id'],
//                                                'ef_created_by' => $user,
//                                                'ef_created_at' => $datetime,
//                                                'ef_fileurl' => '/thesis/' . $research['student_id'] . '/' . $fileName,
//                                                'ef_filename' => $fileinfo['name'],
//                                                'ef_filesize' => $size
//                                            );
//
//                                            $lastid = $this->regModel->addEvaluationFile($data2);
//
//                                        } //isuploaded
//                                    }
//                                } //foreach
//                            } catch
//                            (Exception $e) {
//                                throw new Exception($e->getMessage());
//                            }

                            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal(s) successfully approved"));
                        } elseif ($this->_request->getPost('reject')) {
                            $pd_status = 'rejected';
                            $status = $this->thesisModel->getStatusByCode($pd_status,1);

                            if (empty($status)) {
                                throw new Exception('Invalid Status ID');
                            }

//                            //email
//                            $research = $this->regModel->getProposal($app_id, 1);
//                            $this->notifyReject($research, 'proposal');

                            //update proposal
                            $data = array(
//                                'pd_updated_date' => new Zend_Db_Expr('NOW()'),
//                                'pd_update_by'   => $this->auth->getIdentity()->iduser,
                                'pd_approval'      => $status,
                                'approval_remarks'      => $formData['remarks'][$k]
                            );


                            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal(s) successfully rejected"));
                        }

                        $this->regModel->updateProposal($data, 'p_id = ' . (int)$app_id);
                        $this->regModel->saveSubmission($app_id, 'proposal', $pd_status);
                    }

                    $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'pd-approval'), 'default', true));
                }
            }
        }

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

}