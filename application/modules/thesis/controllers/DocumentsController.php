<?php
class Thesis_DocumentsController extends Base_Base 
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->thesisModel = new Thesis_Model_DbTable_General();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();

		$this->uploadDir = $this->uploadDir = DOCUMENT_PATH.'/thesis/docs';
		
	}

	public function indexAction()
	{
		//title
    	$this->view->title = "Research - Downloads";

		$p_data = $this->thesisModel->getDownloadsData();
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}
	
	public function addAction()
	{
		$this->view->title = "Research - Add Document";
		
		$form = new Thesis_Form_Document();
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();

			//data
			$data = array(
							'title'						=> $formData['title'],
							'research_type'				=> $formData['research_type'],
							'description'				=> $formData['description'],
							'created_by'				=> $this->auth->getIdentity()->iduser,
							'created_date'				=> new Zend_Db_Expr('NOW()'),
							'created_role'				=> 'admin',
							'subject_id'				=> $formData['subject_id']
						);

			$download_id = $this->thesisModel->addDownload($data);

			//upload file
			try 
			{
				$uploadDir = $this->uploadDir;

				if ( !is_dir( $uploadDir ) )
				{
					if ( mkdir_p($uploadDir) === false )
					{
						throw new Exception('Cannot create thesis document folder ('.$uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();

				$adapter->addValidator('NotExists', false, $uploadDir );
				$adapter->setDestination( $uploadDir );
				//$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);

						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						$fileUrl = $uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
					
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}
						
						//save file into db
						$data = array(
										'af_pid'			=> $download_id,
										'af_type'			=> 'download',
										'af_filename'		=> $fileinfo['name'],
										'af_fileurl'		=> '/thesis/docs/'.$fileName,
										'af_filesize'		=> $size
									);
							
						$this->regModel->addFile($data);
						
					} //isuploaded
					
				} //foreach
			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			$this->_helper->flashMessenger->addMessage(array('success' => "All done"));
		 
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'documents', 'action'=>'edit', 'id' => $download_id ),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function editAction()
	{
		$this->view->title = "Research - Edit Download";
		
		$form = new Thesis_Form_Document();

			
		$id = $this->_getParam('id');

		$info = $this->thesisModel->getDownloadSingle($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Download ID');
		}

		
		$this->view->info = $info;

		$form->populate($info);

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($info['id'], 'download');

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();

			//data
			$data = array(
							'title'						=> $formData['title'],
							'research_type'				=> $formData['research_type'],
							'description'				=> $formData['description'],
							'updated_by'				=> $this->auth->getIdentity()->iduser,
							'updated_date'				=> new Zend_Db_Expr('NOW()'),
							'subject_id'				=> $formData['subject_id'],
							'updated_role'				=> 'admin'
						);
			

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$status_id = $this->thesisModel->updateDownload($data, array('id=?'=>$id) );

			//upload file
			try 
			{
				$uploadDir = $this->uploadDir;

				if ( !is_dir( $uploadDir ) )
				{
					if ( mkdir_p($uploadDir) === false )
					{
						throw new Exception('Cannot create thesis document folder ('.$uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();

				$adapter->addValidator('NotExists', false, $uploadDir );
				$adapter->setDestination( $uploadDir );
				//$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);

						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						$fileUrl = $uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
					
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}
						
						//save file into db
						$data = array(
										'af_pid'			=> $id,
										'af_type'			=> 'download',
										'af_filename'		=> $fileinfo['name'],
										'af_fileurl'		=> '/thesis/docs/'.$fileName,
										'af_filesize'		=> $size
									);
							
						$this->regModel->addFile($data);
						
					} //isuploaded
					
				} //foreach
			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}
		 
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'documents', 'action'=>'edit', 'id' => $id ),'default',true));
		}

		//views
		$this->view->form = $form;
	}

	public function deleteAction()
	{
	
		$id = $this->_getParam('id');

		$info = $this->thesisModel->getDownloadSingle($id);
		
		if ( empty($info) )
		{
			throw new Exception('Invalid Document ID');
		}

		$this->thesisModel->deleteDownload($id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Document successfully deleted"));

		$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'documents', 'action'=>'index'),'default',true));

	}
}