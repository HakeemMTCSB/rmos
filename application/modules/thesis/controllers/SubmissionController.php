<?php
class Thesis_SubmissionController extends Base_Base 
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->thesisModel = new Thesis_Model_DbTable_General();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();

		$this->uploadDir = $this->uploadDir = DOCUMENT_PATH.'/thesis/docs';
		
	}
	
	public function preAction()
	{
		$this->view->ajax = 0;
		$errMsg = '';

		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
			$this->view->ajax = 1;
		}
		
		$student_id = $this->_getParam('student_id');
		$research_id = $this->_getParam('id');

		//check submission
		$submit = $this->thesisModel->getSubmission($student_id, 'pre');

		if ( empty($submit) )
		{
			$errMsg = 'No pre-submission data found.';
		}

		$this->view->title = "Research - Pre-Submission";
		
		$form = new Thesis_Form_Document();
		
		//views
		$this->view->form = $form;
		$this->view->errMsg = $errMsg;
	}

	public function finalAction()
	{
		$this->view->ajax = 0;
		$errMsg = '';

		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
			$this->view->ajax = 1;
		}
		
		$student_id = $this->_getParam('student_id');
		$research_id = $this->_getParam('id');

		//check submission
		$submit = $this->thesisModel->getSubmission($student_id, 'final');

		if ( empty($submit) )
		{
			$errMsg = 'No final submission data found.';
		}

		$this->view->title = "Research - Final Submission";
		
		$form = new Thesis_Form_Document();
		
		//views
		$this->view->submit = $submit;
		$this->view->form = $form;
		$this->view->errMsg = $errMsg;
	}
}