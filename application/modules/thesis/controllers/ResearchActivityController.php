<?php

class Thesis_ResearchActivityController extends Base_Base
{
    public $eventModel;
    public $researchModel;

    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->researchModel = new Thesis_Model_DbTable_ResearchActivity();

        $this->lobjCommon = new App_Model_Common();
        $this->gintPageCount = empty($larrInitialSettings['noofrowsingrid']) ? "5" : $larrInitialSettings['noofrowsingrid'];

        $this->auth = Zend_Auth::getInstance();

        $this->db = Zend_Db_Table::getDefaultAdapter();

    }

    public function activitySetupAction()
    {
        //title
        $this->view->title = "Research - Activity Setup";

        $this->view->form = new Thesis_Form_SearchActivity();

        $p_data = $this->researchModel->getResearchData();

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $this->view->form->populate($formData);

            $p_data = $this->researchModel->getCourseByActivity($formData['course'],'search');
        }

        $paginator = $this->lobjCommon->fnPagination($p_data, $lintpage, $lintpagecount);
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    public function activityAddAction()
    {
        $this->view->title = "Add Activity";

        $form = new Thesis_Form_AddActivity();

        $datetime = new Zend_Db_Expr('NOW()');
        $user = $this->auth->getIdentity()->iduser;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $checkduplicate = $this->researchModel->getCourseByActivity($formData['ra_course']);

            if(!$checkduplicate) {
                //save file into db
                $data = array(
                    'ra_course' => $formData['ra_course'],
                    'created_date' => $datetime,
                    'created_by' => $user
                );

                $lastid = $this->researchModel->addActivity($data);

                foreach ($formData['rat_type'] as $v) {
                    $data = array(
                        'rat_ra_id' => $lastid,
                        'rat_course' => $formData['ra_course'],
                        'rat_type' => $v,
                        'rat_active' => 1,
                        'rat_created_date' => $datetime,
                        'rat_created_by' => $user
                    );

                    $this->researchModel->addActivityTagged($data);
                }

                $this->_helper->flashMessenger->addMessage(array('success' => "Activity succesfully tagged"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'research-activity', 'action' => 'activity-setup'), 'default', true));
            }else{
                $this->_helper->flashMessenger->addMessage(array('error' => "Activity for that course already exist. Please choose another course to add or edit the existing course."));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'research-activity', 'action' => 'activity-add'), 'default', true));
            }

        }

        //views
        $this->view->form = $form;
    }

    public function activityEditAction()
    {
        $this->view->title = "Research - Edit Examiner Setup";

        $form = new Thesis_Form_AddActivity();

        $id = $this->_getParam('id');

        $datetime = new Zend_Db_Expr('NOW()');
        $user = $this->auth->getIdentity()->iduser;

        $info = $this->view->research = $this->researchModel->getResearchData($id);

        $activityList = $this->researchModel->getActivityData($info['ra_id']);

        foreach($activityList as $key => $t )
        {

            $info['rat_type'][$key] = $t['rat_type'];
        }

        if (empty($info)) {
            throw new Exception('Invalid Examiner Setup ID');
        }

        $form->populate($info);

        $form->ra_course->setAttrib('disabled','disabled');

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
//            print_r($formData);exit;
            $this->researchModel->deleteActivity($info['ra_id']);

            foreach ($formData['rat_type'] as $v) {
                $data = array(
                    'rat_ra_id' => $info['ra_id'],
                    'rat_course' => $info['ra_course'],
                    'rat_type' => $v,
                    'rat_active' => 1,
                    'rat_created_date' => $datetime,
                    'rat_created_by' => $user
                );

                $this->researchModel->addActivityTagged($data);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'research-activity', 'action' => 'activity-edit', 'id' => $id), 'default', true));
        }

        //views
        $this->view->form = $form;
    }

    public function activityDeleteAction()
    {
        $id = $this->_getParam('id');

        $interest = $this->researchModel->getResearchData($id);

        if (empty($interest)) {
            throw new Exception('Invalid ID');
        }

        $this->researchModel->deleteResearch($id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Research successfully deleted"));

        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'research-activity', 'action' => 'activity-setup'), 'default', true));

    }


//    public function researchDeleteAction()
//    {
//        $id = $this->_getParam('id');
//
//        $interest = $this->researchModel->getResearchData($id);
//
//        if (empty($interest)) {
//            throw new Exception('Invalid ID');
//        }
//
//        $this->researchModel->deleteResearch($id);
//
//        $this->_helper->flashMessenger->addMessage(array('success' => "Research successfully deleted"));
//
//        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'research-activity', 'action' => 'activity-setup'), 'default', true));
//
//    }

}