<?php

class Thesis_EventController extends Base_Base
{
    public $eventModel;

//    public $regModel;
//    public $thesisModel;
//    public $defModel;
//    public $session;
//    public $emailModel;

    public function init()
    {
        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->eventModel = new Thesis_Model_DbTable_Event();

//        $this->lobjStudenttaggingForm = new Thesis_Form_Studenttagging();
        $this->lobjEvent = new Thesis_Model_DbTable_Event();

//        $this->defModel = new App_Model_General_DbTable_Definationms();
//        $this->emailModel = new App_Model_Email();
//        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();
        $this->lobjCommon = new App_Model_Common();
        $this->gintPageCount = empty($larrInitialSettings['noofrowsingrid']) ? "5" : $larrInitialSettings['noofrowsingrid'];

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

        $this->db = Zend_Db_Table::getDefaultAdapter();

    }

    public function eventSetupAction()
    {
        //title
        $this->view->title = "Event List";

        $this->view->form = new Thesis_Form_SearchProposal();

        $p_data = $this->eventModel->getEventData(null, 'array');

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $this->view->form->populate($formData);

            $p_data = $this->eventModel->getEventData($formData, 'array');

        }

        $paginator = $this->lobjCommon->fnPagination($p_data, $lintpage, $lintpagecount);
        $paginator->setItemCountPerPage($this->gintPageCount);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    public function eventAddAction()
    {
        $this->view->title = "Add Event";

        $form = new Thesis_Form_AddEvent();

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

                $data = array(
                    'event_name'      => $formData['event_name'],
                    'event_type'      => $formData['event_type'],
                    'event_date'        => $formData['event_date'],
                    'event_venue'        => $formData['event_venue'],
                    'event_remark'        => $formData['event_remark'],
                    'event_display'        => $formData['event_display'],
                    'created_date'        => new Zend_Db_Expr('NOW()'),
                    'created_by'      => $this->auth->getIdentity()->iduser
                );

                $this->eventModel->addEvent($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "New Event succesfully added"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'event', 'action' => 'event-setup'), 'default', true));



        }

        //views
        $this->view->form = $form;
    }

    public function eventEditAction()
    {
        $this->view->title = "Research - Add Event";

        $form = new Thesis_Form_AddEvent();

        $id = $this->_getParam('id');

        $info = $this->eventModel->getEventData($id);

        if ( empty($info) )
        {
            throw new Exception('Invalid Examiner Setup ID');
        }

        $form->populate($info);

        //post
        if ( $this->getRequest()->isPost() )
        {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'event_name'      => $formData['event_name'],
                'event_type'      => $formData['event_type'],
                'event_date'        => $formData['event_date'],
                'event_venue'        => $formData['event_venue'],
                'event_remark'        => $formData['event_remark'],
                'event_display'        => $formData['event_display'],
                'created_date'        => new Zend_Db_Expr('NOW()'),
                'created_by'      => $this->auth->getIdentity()->iduser
            );


            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $status_id = $this->eventModel->updateEventSetup($data,'event_id='.(int) $id );

            $this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'event', 'action'=>'event-setup'),'default',true));
        }

        //views
        $this->view->form = $form;
    }

    public function eventTagStudentAction()
    {
        $this->view->title = "Student Tagging";

        $form = new Thesis_Form_TagStudent();

        $event_id = $this->_getParam('event_id');

        $activity = $this->view->activity = $this->eventModel->getEventData($event_id);

        $tagged = $this->view->tagged = $this->eventModel->getTaggedStudent($event_id);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

                $data = array(
                    'student_id'      => $formData['student_id'],
                    'research_id'      => $formData['research_id'],
                    'event_id'      => $event_id,
                    'start_time'      => $formData['starttime'],
                    'end_time'      => $formData['endtime'],
                    'created_date'        => new Zend_Db_Expr('NOW()'),
                    'created_by'      => $this->auth->getIdentity()->iduser

                );

                $this->eventModel->addEventStudent($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "New student succesfully tagged"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'event', 'action' => 'event-tag-student','event_id' => $event_id), 'default', true));



        }

        //views
        $this->view->form = $form;
    }

    public function studentDeleteAction()
    {
        $event_id = $this->_getParam('event_id');
        $id = $this->_getParam('id');

        $interest = $this->eventModel->getEventStudent($id);

        if ( empty($interest) )
        {
            throw new Exception('Invalid ID');
        }

        $this->eventModel->deleteEventStudent($id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Student successfully deleted"));

        $this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'event', 'action'=>'event-tag-student', 'event_id' => $event_id),'default',true));

    }

    public function eventTagAssessorAction()
    {
        $this->view->title = "Assessor Tagging";

        $form = new Thesis_Form_TagAssessor();

        $event_id = $this->_getParam('event_id');

        $tagged = $this->view->tagged = $this->eventModel->getTaggedAssessor($event_id);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'examiner_id'      => $formData['staff_id'],
                'event_id'      => $event_id,
                'created_date'        => new Zend_Db_Expr('NOW()'),
                'created_by'      => $this->auth->getIdentity()->iduser

            );

            $this->eventModel->addEventAssessor($data);

            $this->_helper->flashMessenger->addMessage(array('success' => "New Assessor succesfully tagged"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'event', 'action' => 'event-tag-assessor','event_id' => $event_id), 'default', true));



        }

        //views
        $this->view->form = $form;
    }

    public function studentAction()
    {
        $query = $this->_getParam('term');
        $type = $this->_getParam('type','name');
        $activity = $this->_getParam('activity');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $db = $this->db;

        if ( $query != '' )
        {
            if ( $type == 'id')
            {
                $select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.IdStudentRegistration as value'))
                    ->join(array('a' => 'thesis_proposal'),'a.student_id=sa.IdStudentRegistration', array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
                    ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT_WS(" ",sa.registrationId, appl_fname,appl_mname,appl_lname) as label'))
//                    ->join(array('ts'=>'thesis_status'),'ts.status_id=a.pd_status',array())
                    ->join(array('ts'=>'thesis_status'),'ts.status_id=a.pd_approval',array())
                    ->where('a.activity_id = ?', $activity)
                    ->where('ts.status_code LIKE ?','approved')
                ;
                $select->where('sa.registrationId LIKE ?','%'.$query.'%');
//                echo $select;exit;
            }
            else
            {
                $select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.IdStudentRegistration as value'))
                    ->join(array('a' => 'thesis_proposal'),'a.student_id=sa.IdStudentRegistration', array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
                    ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as label'))
                    ->join(array('ts'=>'thesis_status'),'ts.status_id=a.pd_approval',array())
                    ->where('ts.status_code LIKE ?','approved')
                    ->where('a.activity_id = ?', $activity)
                ;
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?','%'.$query.'%');
            }
        }


        $results = $db->fetchAll($select);

        echo Zend_Json::encode($results);
    }

    public function assessorAction()
    {
        $query = $this->_getParam('assessorid');
        $type = $this->_getParam('type','name');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $db = $this->db;

        if ( $query != '' )
        {
            if ( $type == 'name')
            {
                $select = $db->select()
                    ->from(array('user' => 'thesis_examiner_user'), array('user.fullname as label' , 'user.staff_id as value'))
//                    ->join(array("b" => 'thesis_examiner_role'), 'a.ex_examiner_role=b.exr_id', array('b.code as role_name'))
                    ->joinLeft(array('u' => 'tbl_user'), 'user.staff_id = u.iduser and user.staff_id > 0', array())
//                    ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as internal_fullname')
                ;
                
                $select->where('user.fullname LIKE ?','%'.$query.'%');
            }
        }

        $results = $db->fetchAll($select);

        echo Zend_Json::encode($results);
    }

    public function getAssessorAction()
    {
        $this->_helper->layout->disableLayout();

        $id = $this->_getParam('id');

        $result = $this->eventModel->getAssessor($id);

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function notifyAction()
    {
        $emailDb = new App_Model_Email();
        $commDb = new Communication_Model_DbTable_Template();

        $event_id = $this->_getParam('event_id');
//        $research_id = $this->_getParam('research_id');

//        //ok get supervisor info
//        $supervisor = $this->regModel->getSupervisor($id);
        $student = $this->eventModel->getEmail($event_id);
//        print_r($student);exit;
        foreach($student as $stud){

            //email

            if($stud['role'] == 'student') {
                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-student-invitation', 0, 1);
            }
            elseif($stud['role'] == 'supervisor')
            {
                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-supervisor-invitation', 0, 1);
            }
            elseif($stud['role'] == 'director')
            {
                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-director-invitation', 0, 1);
            }

            $template = $gettemplate[0];

            $research['research_type'] = 'proposal';

//        $research['supervisor_info'] = $supervisor['supervisor_name'];

            //email
            $dataEmail = array(
                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $stud['email'],
                'subject' => $template['tpl_name'],
                'content' => Thesis_Model_DbTable_Registration::parseContent($stud, $template['tpl_content']),
                'date_que' => date('Y-m-d H:i:s')
            );

            $emailDb->add($dataEmail);

            if($stud['email2'] != NULL){
                $dataEmail2 = array(
                    'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $stud['email2'],
                    'subject' => $template['tpl_name'],
                    'content' => Thesis_Model_DbTable_Registration::parseContent($stud, $template['tpl_content']),
                    'date_que' => date('Y-m-d H:i:s')
                );

                $emailDb->add($dataEmail2);
            }

//            //update notified
//            $data = array(
//                'ps_event_notify' => 1
//            );
//
//            $this->regModel->updateSupervisor($data, $stud['sup_id']);

            //email

        }


        $this->_helper->flashMessenger->addMessage(array('success' => "Student has been notified"));
        $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'event', 'action' => 'event-setup', 'id' => $research['p_id']), 'default', true));
    }

}