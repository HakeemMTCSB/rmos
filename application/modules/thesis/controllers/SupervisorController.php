<?php
class Thesis_SupervisorController extends Base_Base
{
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;

        $this->regModel = new Thesis_Model_DbTable_Registration();
		$this->thesisModel = new Thesis_Model_DbTable_General();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );
		$this->auth = Zend_Auth::getInstance();
		
	}

	public function indexAction()
	{

	}

	public function historyAction()
    {
        $this->view->title = "Research - Supervisor History";

        $pid = $this->_getParam('pid');
        $type = $this->_getParam('type');

        $histories = $this->regModel->getSupervisorHistories($pid, $type);

        $this->view->pid = $pid;
        $this->view->type = $type;
        $this->view->histories = $histories;
    }
}