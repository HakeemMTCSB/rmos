<?php

class Thesis_Model_DbTable_Registration extends Zend_Db_Table
{

    protected $_proposal = 'thesis_proposal';
    protected $_file = 'thesis_article_files';
    protected $_supervisor = 'thesis_article_supervisor';
    protected $_article = 'thesis_articleship';
    protected $_exemption = 'thesis_exemption';
    protected $_employment = 'thesis_article_employment';
    protected $_submission = 'thesis_submission';
    protected $_submission_history = 'thesis_submission_history';

    protected $_topic = 'thesis_researchtopics';
    protected $_categories = 'thesis_categories';
    protected $_status = 'thesis_status';
    protected $_superrole = 'thesis_supervisor';
    protected $_examinersem = 'thesis_examinersem'; //deprecated
    protected $_examiner_setup = 'thesis_examiner_setup';
    protected $_examiner = 'thesis_article_examiner';
    protected $_examiner_role = 'thesis_examiner_role';

    protected $_supervisor_history = 'thesis_supervisor_history';

    protected $_evaluation_file = 'thesis_evaluation_files';
    protected $_visibility = 'thesis_evaluation_visibility';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

    }

    public function checkResearchCourseReg($student_id, $type)
    {
        $db = $this->db;

        if ($type == 'proposal') {
            //3
        } else if ($type == 'articleship') {
            $coursetype = 2;
        } else if ($type == 'exemption') {
            $coursetype = 19;
        }

        if (isset($coursetype)) {
            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects'), array('a.*'))
                ->join(array("sm" => "tbl_subjectmaster"), 'sm.IdSubject=a.IdSubject')
                ->join(array("ct" => "tbl_coursetype"), 'ct.IdCourseType =sm.CourseType', array("ct.CourseType"))
                ->where('ct.IdCourseType = ?', $coursetype)
                ->where('a.IdStudentRegistration = ?', $student_id);


            //echo $select . '<br />';

            return $db->fetchRow($select);
        }

        return '';
    }

    /*
    *	getProposal
    */
    public function getProposalApproval($where = array(), $returnType = 'sql')
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ProgramName', 'ProgramCode'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc as intake_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->joinLeft(array('col' => 'tbl_collegemaster'), 'col.IdCollege=prg.IdCollege', array('IdCollege','CollegeName'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc'))
            ->joinLeft(array('pda' => 'thesis_status'), 'pda.status_id=a.pd_approval', array('pda.status_code as pd_app_name', 'pda.status_description as pd_approval_desc'))
            ->where('td.DefinitionDesc != ?', 'registration')
//            ->where('a.pd_approval is not null')
            ->where('pst.status_code = ?', 'approved')
            ->order('a.created_date DESC');

        if (!empty($where)) {
            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['pd_status']) && $where['pd_status'] != '') {
                $select->where('a.pd_status = ?', $where['pd_status']);
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.p_status = ?', $where['status']);
            }

            if (isset($where['approval_date']) && $where['approval_date'] != '') {
                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where('sr.IdProgram = ?', $where['id_program']);
            }

            if (isset($where['course_id']) && $where['course_id'] != '') {
                $select->where('a.course_id = ?', $where['course_id']);
            }

            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
                $select->where('col.IdCollege = ?', $where['IdCollege']);
            }

            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
            }

            if (isset($where['pd_approval']) && $where['pd_approval'] != '') {
                $select->where('a.pd_approval = ?', $where['pd_approval']);
            }
        }
//        echo $select;exit;

        if ($returnType == 'sql') {
            return $select;
        } else {
            $result = $db->fetchAll($select);
            return $result;
        }
    }

    public function getPdApproval($where = array(), $returnType = 'sql')
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('IdProgram','ProgramName', 'ProgramCode'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc as intake_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->joinLeft(array('col' => 'tbl_collegemaster'), 'col.IdCollege=prg.IdCollege', array('IdCollege','CollegeName'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc'))
            ->joinLeft(array('tcp' => 'tbl_chiefofprogramlist'), 'tcp.IdProgram = prg.IdProgram and tcp.Active = 1', array('IdStaff'))
            ->joinLeft(array('u4' => 'tbl_user'), 'u4.IdStaff = tcp.IdStaff', array('u4.iduser'))
//            ->joinLeft(array('staff4' => 'tbl_staffmaster'), 'staff4.IdStaff  = tcp.IdStaff', 'FullName as program_director_name')
            ->joinLeft(array('tas' => 'thesis_article_supervisor'), 'tas.ps_pid = a.p_id and tas.ps_supervisor_id = u4.iduser', array('tas.ps_id as own'))
            ->where('td.DefinitionDesc != ?', 'registration')
            ->where('a.pd_approval is null')
            ->order('a.created_date DESC');

        if (!empty($where)) {
            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['pd_status']) && $where['pd_status'] != '') {
                $select->where('a.pd_status = ?', $where['pd_status']);
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.p_status = ?', $where['status']);
            }

            if (isset($where['approval_date']) && $where['approval_date'] != '') {
                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where("sr.IdProgram IN ($where[id_program])");
            }

            if (isset($where['course_id']) && $where['course_id'] != '') {
                $select->where('a.course_id = ?', $where['course_id']);
            }

            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
                $select->where('col.IdCollege = ?', $where['IdCollege']);
            }

            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
            }

            if (isset($where['admin_approval']) && $where['admin_approval'] != '') {
                if($where['admin_approval'] == 1){
                    $select->where('tas.ps_id != ?', '');
                }else{
                    $select->where('tas.ps_id is null');
                }
            }
        }

        if ($returnType == 'sql') {
            return $select;
        } else {
            $result = $db->fetchAll($select);
            return $result;
        }
    }

    public function getPdApprovalReport($where = array(), $returnType = 'sql')
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ProgramName', 'ProgramCode'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc as intake_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->joinLeft(array('col' => 'tbl_collegemaster'), 'col.IdCollege=prg.IdCollege', array('IdCollege','CollegeName'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc'))
            ->joinLeft(array('dir' => 'thesis_status'), 'dir.status_id=a.pd_approval', array('dir.status_code as dir_status_name', 'dir.status_description as dir_status_description'))
            ->where('td.DefinitionDesc != ?', 'registration')
            ->where('a.pd_approval is not null')
            ->order('a.created_date DESC');

        if (!empty($where)) {
            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['pd_status']) && $where['pd_status'] != '') {
                $select->where('a.pd_status = ?', $where['pd_status']);
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.p_status = ?', $where['status']);
            }

            if (isset($where['approval_date']) && $where['approval_date'] != '') {
                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where("sr.IdProgram IN ($where[id_program])");
            }

            if (isset($where['course_id']) && $where['course_id'] != '') {
                $select->where('a.course_id = ?', $where['course_id']);
            }

            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
                $select->where('col.IdCollege = ?', $where['IdCollege']);
            }

            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
            }
        }
//        echo $select;exit;

        if ($returnType == 'sql') {
            return $select;
        } else {
            $result = $db->fetchAll($select);
            return $result;
        }
    }

    public function getProgramByPd($id)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('tu' => 'tbl_user'), array('tu.iduser'))
            ->join(array('tc' => 'tbl_chiefofprogramlist'), 'tu.IdStaff=tc.IdStaff', array('IdProgram'))
            ->where('tu.iduser = ?', $id)
        ;

//        echo $select;exit;
            $result = $db->fetchAll($select);
            return $result;
    }

    public function getProposalData($where = array(), $returnType = 'sql')
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ProgramName', 'ProgramCode'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc as intake_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->joinLeft(array('col' => 'tbl_collegemaster'), 'col.IdCollege=prg.IdCollege', array('IdCollege','CollegeName'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc'))
            ->where('a.pid = ?', 0)
            ->order('sem.SemesterMainCode DESC');

        if (!empty($where)) {
            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['pd_status']) && $where['pd_status'] != '') {
                $select->where('a.pd_status = ?', $where['pd_status']);
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.p_status = ?', $where['status']);
            }

            if (isset($where['approval_date']) && $where['approval_date'] != '') {
                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where('sr.IdProgram = ?', $where['id_program']);
            }

            if (isset($where['course_id']) && $where['course_id'] != '') {
                $select->where('a.course_id = ?', $where['course_id']);
            }

            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
                $select->where('col.IdCollege = ?', $where['IdCollege']);
            }

            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
            }
        }
//        echo $select;exit;

        if ($returnType == 'sql') {
            return $select;
        } else {
            $result = $db->fetchAll($select);
            return $result;
        }
    }

    public function getProposal($id, $full = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('address' => 'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)', 'appl_postcode', 'appl_phone_home', 'appl_phone_mobile','appl_username','appl_email','appl_idnumber','CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc', 'IntakeDesc as IntakeDefaultLanguage'))
            ->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = sr.IdBranch', array('br.BranchName'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ProgramName', 'ProgramCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionCode'))
            ->where('a.p_id = ?', $id);

        if ($full) {
            $select->joinLeft(array('city' => 'tbl_city'), 'city.idCity=p.appl_city', array('CityName'))
                ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=p.appl_state', array('StateName'))
                ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=p.appl_country', array('CountryName'));
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProposalLatest($id, $full = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('address' => 'CONCAT_WS(",",appl_address1,appl_address2,appl_address3)', 'appl_postcode', 'appl_phone_home', 'appl_phone_mobile','appl_username','appl_email','appl_idnumber','CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc', 'IntakeDesc as IntakeDefaultLanguage'))
            ->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = sr.IdBranch', array('br.BranchName'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ProgramName', 'ProgramCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionCode'))
            ->where('a.student_id = ?', $id)
            ->order('a.updated_date desc')
        ;

        if ($full) {
            $select->joinLeft(array('city' => 'tbl_city'), 'city.idCity=p.appl_city', array('CityName'))
                ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=p.appl_state', array('StateName'))
                ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=p.appl_country', array('CountryName'));
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getActivities($id,$course)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'thesis_research_activity_tagged'), array('a.*'))
            ->joinLeft(array('tp' => 'thesis_proposal'), "tp.course_id = a.rat_course AND tp.activity_id = a.rat_type AND (tp.pid = $id OR tp.p_id = $id)", array('tp.pid','tp.p_id','created_date','pd_status','pd_approval'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.rat_type', array('DefinitionDesc','DefinitionCode'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=tp.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=tp.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('dir' => 'thesis_status'), 'dir.status_id=tp.pd_approval', array('dir.status_code as dir_status_name', 'dir.status_description as dir_status_description'))
//            ->where('td.DefinitionCode != ?', 'registration')
            ->where('a.rat_course = ?', $course)
            ->order('td.defOrder')
        ;
//echo $select;exit;
        $result = $db->fetchAll($select);
        return $result;

    }

    public function getRegisterSem($date,$studid)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('ts' => 'tbl_studentregistration'), array('ts.IdStudentRegistration'))
            ->join(array('tp' => 'tbl_program'), 'ts.IdProgram = ts.IdProgram', array('tp.IdProgram'))
            ->join(array('sem' => 'tbl_semestermaster'), 'sem.IdScheme = tp.IdScheme', array('sem.IdSemesterMaster'))
            ->where('ts.IdStudentRegistration = ? ', $studid)
            ->where('sem.SemesterMainStartDate <= ?', $date)
            ->where('sem.SemesterMainEndDate >= ?', $date)
        ;
//echo $select;exit;
        $result = $db->fetchRow($select);
        return $result;

    }

    public function getProposalActivity($id, $activity)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionCode'))
            ->where('a.p_id = ?', $id)
//            ->where('a.activity_id = ?', $activity)
        ;

//        if($activity == 1065){
//            $select->where('a.p_id = ?', $id);
//        }else{
//            $select->where('a.pid = ?', $id);
//        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateProposal($data, $where)
    {
        $db = $this->db;

        $db->update($this->_proposal, $data, $where);
    }

    public function addProposal($data)
    {
        $db = $this->db;

        $db->insert($this->_proposal, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
        Get User By Role
    */
    public function getUserByRole($id, $role)
    {
        $db = $this->db;

        switch ($role) {
            case 'admin':
            case 'supervisor':

                $select = $db->select()
                    ->from(array('u' => 'tbl_user'), array())
                    ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
                    ->where('u.iduser = ?', $id);

                break;
        }

        $results = $db->fetchRow($select);
        return $results;
    }

    /*
    *	Files
    */
    public function addFile($data)
    {
        $db = $this->db;

        $db->insert($this->_file, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addEvaluationFile($data)
    {
        $db = $this->db;

        $db->insert($this->_evaluation_file, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addVisibility($data)
    {
        $db = $this->db;

        $db->insert($this->_visibility, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getFile($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_file))
            ->where('a.af_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getFiles($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_file))
            ->where('a.af_pid = ?', $pid)
            ->where('a.af_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEvaluationFile($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_evaluation_file))
            ->where('a.ef_pid = ?', $id)
//            ->where('a.ef_id = ?', $id2)
        ;

        $result = $db->fetchRow($select);
        return $result;
    }

//    public function getEvaluationFiles($pid, $type)
//    {
//        $db = $this->db;
//        $select = $db->select()
//            ->from(array("a" => $this->_evaluation_file))
//            ->where('a.ef_pid = ?', $pid)
//            ->where('a.ef_type = ?', $type);
//
//        $result = $db->fetchAll($select);
//        return $result;
//    }

    public function getEvaluationFiles($pid,$type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_evaluation_file))
            ->where('a.ef_pid = ?', $pid)
            ->where('a.type = ?', $type)
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    /* Supervisor */
    public function addSupervisor($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisor, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisors($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisor))
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array('user.IdStaff'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.Email as supervisor_email','staff.Email2 as supervisor_email2'))
            ->joinLeft(array('salutation' => 'tbl_definationms'), 'salutation.idDefinition  = staff.FrontSalutation',array("CONCAT(salutation.DefinitionDesc,' ',staff.FullName) as supervisor_name"))
            ->joinLeft(array('history' => 'thesis_supervisor_history'), 'history.ps_id=a.ps_id', array('history.reason'))
            ->where('a.ps_pid = ?', $pid)
            ->where('a.ps_type = ?', $type);
//echo $select;exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSupervisor($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisor))
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array('user.IdStaff'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.Email as supervisor_email','staff.Email2 as supervisor_email2'))
            ->joinLeft(array('salutation' => 'tbl_definationms'), 'salutation.idDefinition  = staff.FrontSalutation',array("CONCAT(salutation.DefinitionDesc,' ',staff.FullName) as supervisor_name"))
            ->where('a.ps_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSupervisorByProposal($pid = null, $userid = null)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisor), array("num" => "COUNT(*)", "*"))
//            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array('user.IdStaff'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.Email as supervisor_email'))
            ->joinLeft(array('salutation' => 'tbl_definationms'), 'salutation.idDefinition  = staff.FrontSalutation',array("CONCAT(salutation.DefinitionDesc,' ',staff.FullName) as supervisor_name"))
            ->where('a.ps_pid = ?', $pid)
            ->where('a.ps_supervisor_id = ?', $userid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSupervisorByUserid($id)
    {
        $db = $this->db;

        $select = $db->select()->from(array('user' => 'tbl_user'), array('user.iduser as value'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff',array('staff.FullName as label'))
            ->joinLeft(array('college' => 'tbl_collegemaster'), 'college.IdCollege  = staff.IdCollege')
            ->joinLeft(array('role' => 'tbl_definationms'), 'role.idDefinition  = user.IdRole')
            ->where('user.UserStatus = 1')
            ->where('staff.StaffAcademic = 0')
            ->where('user.iduser = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateSupervisor($data, $id)
    {
        $db = $this->db;

        $db->update($this->_supervisor, $data, array('ps_id = ?' => $id));
    }

    /* Supervisor History */
    public function addSupervisorHistory($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisor_history, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisorHistories($pid, $type, $order = 'desc')
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisor_history))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.user_id', array('user.IdStaff as supervisor_idstaff'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.pid = ?', $pid)
            ->where('a.type = ?', $type)
            ->order('a.id ' . $order);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function updateSupervisorHistory($data, $id)
    {
        $db = $this->db;

        $db->update($this->_supervisor_history, $data, array('id = ?' => $id));
    }

    /* Examiners */
    public function addExaminer($data)
    {
        $db = $this->db;

        $db->insert($this->_examiner, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public static function getExaminers($pid, $type, $eval = 0)
    {
        $db = getDb();
        $select = $db->select()
            ->from(array("a" => 'thesis_article_examiner'))
            ->join(array('user' => 'thesis_examiner_user'), 'user.id=a.ex_examiner_id', array('user.fullname as supervisor_name'))
            ->join(array("b" => 'thesis_examiner_role'), 'a.ex_examiner_role=b.exr_id', array('b.code as role_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'user.staff_id = u.iduser and user.staff_id > 0', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as internal_fullname')
            ->where('a.ex_pid = ?', $pid)
            ->where('a.ex_type = ?', $type);

        //if has eval sheet
        if ($eval) {
            $select->joinLeft(array('e' => 'thesis_evaluation_sheet'), 'a.ex_examiner_id=e.examiner_id AND research_type=\'' . $type . '\' AND research_id=\'' . $pid . '\'', array('e.id as sheet_id'));
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    /* Employment */
    public function addEmployment($data)
    {
        $db = $this->db;

        $db->insert($this->_employment, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getEmployment($pid)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_employment))
            ->where('a.ae_pid = ?', $pid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEmploymentSingle($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_employment))
            ->where('a.ae_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    /*
        Articleship
    */
    public function getArticleshipData($where = array(), $returnType = 'sql')
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article), array('a.*', 'a.a_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->order('a.status ASC')
            ->order('a.created_date DESC');

        $select->joinLeft(array('city' => 'tbl_city'), 'city.idCity=a.city', array('CityName'))
            ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=a.state', array('StateName'))
            ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=a.country', array('CountryName'));

        if (!empty($where)) {
            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.status = ?', $where['status']);
            }

            if (isset($where['approval_date']) && $where['approval_date'] != '') {
                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
            }
        }

        $result = $db->fetchAll($select);

        if ($returnType == 'sql') {
            return $select;
        } else {
            return $result;
        }
    }

    public function getArticleship($id, $full = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article), array('a.*', 'a.a_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.a_id = ?', $id);

        if ($full) {
            $select->joinLeft(array('city' => 'tbl_city'), 'city.idCity=p.appl_city', array('CityName'))
                ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=p.appl_state', array('StateName'))
                ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=p.appl_country', array('CountryName'));
        }

        $result = $db->fetchRow($select);

        return $result;
    }

    public function updateArticleship($data, $where)
    {
        $db = $this->db;

        $db->update($this->_article, $data, $where);
    }

    public function addArticleship($data)
    {
        $db = $this->db;

        $db->insert($this->_article, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
        Exemption
    */
    public function getExemptionData($where = array(), $returnType = 'sql')
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption), array('a.*', 'a.e_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->order('a.status ASC')
            ->order('a.created_date DESC');

        if (!empty($where)) {
            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.status = ?', $where['status']);
            }

            if (isset($where['approval_date']) && $where['approval_date'] != '') {
                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
            }
        }

        $result = $db->fetchAll($select);

        if ($returnType == 'sql') {
            return $select;
        } else {
            return $result;
        }
    }

    public function getExemption($id, $full = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption), array('a.*', 'a.e_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.e_id = ?', $id);

        if ($full) {
            $select->joinLeft(array('city' => 'tbl_city'), 'city.idCity=p.appl_city', array('CityName'))
                ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=p.appl_state', array('StateName'))
                ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=p.appl_country', array('CountryName'));
        }

        $result = $db->fetchRow($select);

        return $result;
    }

    public function updateExemption($data, $where)
    {
        $db = $this->db;

        $db->update($this->_exemption, $data, $where);
    }

    public function addExemption($data)
    {
        $db = $this->db;

        $db->insert($this->_exemption, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
        Parse Content
    */

    public static function parseContent($research, $template)
    {
        $commDb = new Communication_Model_DbTable_Template();

        $tags = $commDb->getTemplateTags('thesis');

        foreach ($tags as $tag) {
            if (preg_match('/\[' . $tag['tag_name'] . '\]/', $template)) {
                switch ($tag['tag_name']) {
                    case "Date":
                        $template = str_replace('[' . $tag['tag_name'] . ']', format_date(date('d-m-Y'), 'd F Y'), $template);
                        break;

                    case "Supervisor":
//                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['supervisor_info']['supervisor_name'], $template);
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['supervisor_info'], $template);
                        break;

                    case "Data":

                        if ($research['research_type'] == 'articleship') {
                            $html = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"><tr><th style="padding:5px; border:1px solid #ccc">STUDENT NAME</th><th style="padding:5px; border:1px solid #ccc">COMPANY</th><th style="padding:5px; border:1px solid #ccc">DATE OF ARTICLESHIP</th></tr>';
                            $html .= '<tr><td style="padding:5px; border:1px solid #ccc">' . $research['student_name'] . '</td><td style="padding:5px; border:1px solid #ccc">' . $research['company'] . '</td><td style="padding:5px; border:1px solid #ccc">' . format_date($research['start_date']) . ' - ' . format_date($research['end_date']) . '</td></tr></table>';

                            $template = str_replace('[' . $tag['tag_name'] . ']', $html, $template);
                        } else if ($research['research_type'] == 'exemption') {
                            $html = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"><tr><th style="padding:5px; border:1px solid #ccc">STUDENT NAME</th><th style="padding:5px; border:1px solid #ccc">PROPOSED TOPIC</th><th style="padding:5px; border:1px solid #ccc">DATE OF ARTICLESHIP</th></tr>';
                            $html .= '<tr><td style="padding:5px; border:1px solid #ccc">' . $research['student_name'] . '</td><td style="padding:5px; border:1px solid #ccc">' . $research['proposedarea'] . '</td><td style="padding:5px; border:1px solid #ccc">' . format_date($research['start_date']) . ' - ' . format_date($research['end_date']) . '</td></tr></table>';

                            $template = str_replace('[' . $tag['tag_name'] . ']', $html, $template);
                        }
                        break;

                    case "Title":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['p_title'], $template);
                        break;

                    /* approval only */

                    case "Name - Student":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['student_name'], $template);
                        break;

                    case "Address - Student":
                        //student info
                        $address = trim($research["appl_address1"]);

                        if ($research["appl_address2"]) {
                            $address .= '<br/>' . $research["appl_address2"] . ' ';
                        }

                        if ($research["appl_postcode"]) {
                            $address .= '<br/>' . $research["appl_postcode"] . ' ';
                        }

                        if ($research["appl_city"] != 99) {
                            $address .= '<br />' . $research["CityName"] . ' ';
                        } else {
                            $address .= '<br/ >' . $research["appl_city_others"] . ' ';
                        }

                        if ($research["appl_state"] != 99) {
                            $address .= $research["StateName"] . ' ';
                        } else {
                            $address .= $research["appl_state_others"];
                        }

                        $address .= '<br/>' . $research["CountryName"];

                        $address = trim(str_replace('_x000D_', '', $address));

                        $template = str_replace("[" . $tag['tag_name'] . "]", strtoupper($address), $template);
                        break;

                    case "ID - Student":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['registrationId'], $template);
                        break;

                    case "Semester Start":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['SemesterMainName'], $template);
                        break;

                    case "Course":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['SubCode'].' - '.$research['course_name'], $template);
                        break;

                    case "Semester End Date":
                        $template = str_replace('[' . $tag['tag_name'] . ']', format_date_to_view($research['semester_end_date']), $template);
                        break;

                    case "Venue":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['event_venue'], $template);
                        break;

                    case "Event Date":
                        $template = str_replace('[' . $tag['tag_name'] . ']', format_date_to_view($research['event_date']), $template);
                        break;

                    case "Event Time":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['start_time'], $template);
                        break;

                    case "Event End":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['end_time'], $template);
                        break;

                    case "DynamicName":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['d_name'], $template);
                        break;

                    case "Activity":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['ActivityName'], $template);
                        break;

                    case "Program":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['ProgramCode'], $template);
                        break;

                    case "User":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['fName'], $template);
                        break;

                    case "Keylink":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['keylink'], $template);
                        break;

                }
            }
        }


        return $template;
    }

    /*
        MISC
    */
    public function isRegistered($subject_id, $semester_id, $student_id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects'))
            ->where('a.IdSubject = ?', $subject_id)
            //->where('a.IdSemesterMain = ?', $semester_id)
            ->where('a.IdStudentRegistration = ?', $student_id);

        $row = $db->fetchRow($select);

        return empty($row) ? false : true;
    }

    public function isRegisteredInfo($subject_id, $semester_id, $student_id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects'))
            ->where('a.IdSubject = ?', $subject_id)
            //->where('a.IdSemesterMain = ?', $semester_id)
            ->where('a.IdStudentRegistration = ?', $student_id);

        $row = $db->fetchRow($select);

        return $row;
    }

    public function getStudentRegSubject($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects'))
            ->where('a.IdStudentRegSubjects = ?', $id);

        $row = $db->fetchRow($select);

        return $row;
    }

    public function getStudentRegSubjectDtl($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects_detail'))
            ->where('a.regsub_id = ?', $id)
            ->where('a.invoice_id IS NOT NULL');

        $row = $db->fetchRow($select);

        return $row;
    }

    public function deleteSubject($id)
    {
        $db = $this->db;
        $db->delete('tbl_studentregsubjects', 'IdStudentRegSubjects = ' . $id);
        $db->delete('tbl_studentregsubjects_detail', 'regsub_id = ' . $id);
    }

    public function getProposalSubject($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects'))
            ->join(array('b' => 'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->join(array('c' => 'tbl_semestermaster'), 'a.IdSemesterMain = c.IdSemesterMaster')
            ->where('b.CourseType = ?', 3)
            ->where('a.IdStudentRegistration = ?', $id)
            ->order('c.SemesterMainStartDate DESC');

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStatus($type = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'thesis_status'));

        if ($type) {
            $select->where('a.status_type = ?', $type);
        } else {
            $select->where('a.status_type = ?', 0);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getLecturer()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_staffmaster'), array('value' => '*'))
            ->where('a.StaffAcademic = ?', 0)
            ->order('a.FullName');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function saveSubmission($id, $type, $status)
    {
        $db = $this->db;
        $defModel = new App_Model_General_DbTable_Definationms();
        $thesisModel = new Thesis_Model_DbTable_General();
        $regModel = new Thesis_Model_DbTable_Registration();

        $getResearchType = $defModel->getIdByDefType('Research Type', $type); // research type
        $getSubmission = $thesisModel->getSubmissionByResearchId($id, $getResearchType['key']);
        $getProposal = $regModel->getProposal($id);

        $typeId = ($type == 'proposal') ? 1 : 0;
        $agree = 1;
        $approved = 1;

        if ($getSubmission && $getProposal) {

            $submissionId = $getSubmission['id'];

            $dataUpd = array(
                'status'       => $thesisModel->getStatusByCode($status, $typeId),
                'agree'        => $agree,
                'approved'     => null,
                'created_role' => 'admin',
                'created_by'   => $this->auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()')
            );

            $thesisModel->updateSubmission($dataUpd, 'id = ' . (int)$submissionId);

        } else {

            $data = array(
                'student_id'    => $getProposal['student_id'],
                'research_id'   => $id,
                'research_type' => $getResearchType['key'],
                'submit_type'   => 'pre',
                'submit_date'   => null,
                'agree'         => $agree,
                'approved'      => $approved,
                'status'        => $thesisModel->getStatusByCode($status, $typeId),
                'created_role'  => 'admin',
                'created_by'    => $this->auth->getIdentity()->iduser,
                'created_date'  => new Zend_Db_Expr('NOW()')
            );

            $submissionId = $thesisModel->addSubmission($data);
        }

        $dataHistory = array(
            'submission_id' => $submissionId,
            'student_id'    => $getProposal['student_id'],
            'research_id'   => $id,
            'research_type' => $getResearchType['key'],
            'submit_type'   => 'pre',
            'submit_date'   => null,
            'agree'         => $agree,
            'approved'      => $approved,
            'status'        => $thesisModel->getStatusByCode($status, $typeId),
            'created_role'  => 'admin',
            'created_by'    => $this->auth->getIdentity()->iduser,
            'created_date'  => new Zend_Db_Expr('NOW()')
        );

        $db->insert($this->_submission_history, $dataHistory);

    }

    public function getSubmissionHistory($studentId)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submission_history), array('a.*'))
//            ->join(array('b' => $this->_submission_history), 'a.id = b.submission_id', array(''))
            ->join(array('b' => 'tbl_definationms'), " a.research_type = b.idDefinition ", array('type' => 'b.DefinitionDesc'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as created_user')
            ->where('a.student_id = ?', $studentId)
            ->order('a.created_date asc');

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getSubmissionHistoryByProposal($id)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submission_history), array('a.*'))
//            ->join(array('b' => $this->_submission_history), 'a.id = b.submission_id', array(''))
            ->join(array('b' => 'tbl_definationms'), " a.research_type = b.idDefinition ", array('type' => 'b.DefinitionDesc'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as created_user')
            ->joinLeft(array('p' => 'thesis_proposal'), 'p.p_id  = a.research_id ', array('p.p_id'))
            ->joinLeft(array('e' => 'tbl_definationms'), " p.activity_id = e.idDefinition ", array('typed' => 'e.DefinitionDesc'))
            ->where('a.research_id = ?', $id)
            ->order('a.created_date asc');

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getClusterList()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array("a" => "tbl_collegemaster"), array("key" => "a.IdCollege", "value" => "a.CollegeName" ))
            ->where('a.CollegeType = ?', 1)
        ;

        $results = $db->fetchAll($select);

        return $results;
    }

    public function getRegistration($td, $tdt)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("td" => 'tbl_definationms'))
            ->join(array('tdt' => 'tbl_definationtypems'), " tdt.idDefType = td.idDefType ", array('type' => 'idDefType'))
            ->where('td.DefinitionCode = ?', $td)
            ->where('tdt.defTypeDesc = ?', $tdt)
        ;

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkPid($studid, $courseid, $sem)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_proposal), array("num" => "COUNT(*)", "*"))
            ->where('a.student_id = ?', $studid)
            ->where('a.course_id = ?', $courseid)
            ->where('a.semester_start = ?', $sem)
            ->where('a.pid = ?', 0)
        ;

        $result = $db->fetchRow($select);
        return $result;
    }

    public function deleteActivity($id)
    {
        $db = $this->db;

        $db->delete('thesis_proposal', 'p_id = ' . (int)$id);
    }

    public function getActivity($id)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_definationms'), array('DefinitionDesc as ActivityName'))
            ->where('a.idDefinition = ?', $id)
        ;

        $result = $db->fetchRow($select);
        return $result;

    }

}