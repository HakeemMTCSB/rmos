<?php

class Thesis_Model_DbTable_ResearchActivity extends Zend_Db_Table
{

    protected $_proposal = 'thesis_proposal';
    protected $_event = 'thesis_event_setup';
    protected $_event_student = 'thesis_event_student';
    protected $_event_examiner = 'thesis_event_examiner';

    protected $_research_activity = 'thesis_research_activity_setup';
    protected $_research_activity_tagged = 'thesis_research_activity_tagged';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

    }

    public function getResearchData($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('ra' => $this->_research_activity), array('ra.*',))
            ->joinLeft(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject = ra.ra_course', array('SubjectName', 'SubCode'));

        if ($id) {
            $select->where('ra.ra_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;

    }

    public function getActivityData($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('rat' => $this->_research_activity_tagged), array('rat.*',))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = rat.rat_type', array('DefinitionDesc'))
            ->where('rat.rat_ra_id = ?', $id)
            ->order('td.defOrder');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getActivityByCourse($id=null)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("rat" => "thesis_research_activity_tagged"), array("rat.*"))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = rat.rat_type', array('DefinitionDesc'))
            ->where('rat.rat_course = ? ', $id)
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addActivity($data)
    {
        $db = $this->db;

        $db->insert($this->_research_activity, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addActivityTagged($data)
    {
        $db = $this->db;

        $db->insert($this->_research_activity_tagged, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function deleteActivity($id)
    {
        $db = $this->db;

        $db->delete($this->_research_activity_tagged, 'rat_ra_id = ' . (int)$id);
    }

    public function deleteResearch($id)
    {
        $db = $this->db;

        $db->delete($this->_research_activity, 'ra_id = ' . (int)$id);
    }

    public function getCourseByActivity($id=null, $type=null)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("ra" => "thesis_research_activity_setup"))
            ->joinLeft(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject = ra.ra_course', array('SubjectName', 'SubCode'))
            ->where('ra.ra_course = ? ', $id)
        ;

        if($type == 'search'){
            $result = $db->fetchAll($select);
        }else{
            $result = $db->fetchRow($select);
        }

        return $result;
    }

}