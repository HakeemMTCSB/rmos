<?php

class Thesis_Model_DbTable_General extends Zend_Db_Table
{

    protected $_topic = 'thesis_researchtopics';
    protected $_categories = 'thesis_categories';
    protected $_status = 'thesis_status';
    protected $_supervisor = 'thesis_supervisor';
    protected $_examinersem = 'thesis_examinersem'; //deprecated
    protected $_interest = 'thesis_fieldofinterest';
    protected $_examiner = 'thesis_examiner_user';
    protected $_evalset = 'thesis_evaluation_set';
    protected $_evalitem = 'thesis_evaluation_item';
    protected $_evalgroups = 'thesis_evaluation_groups';
    protected $_reason = 'thesis_reasonapplying';
    protected $_supervisorsetup = 'thesis_setup_supervisor';
    protected $_examiner_setup = 'thesis_examiner_setup';
    protected $_examiner_role = 'thesis_examiner_role';
    protected $_activity_setup = 'thesis_activity_setup';
    protected $_downloads = 'thesis_download';
    protected $_file = 'thesis_article_files';
    protected $_colloquium = 'thesis_colloquium';
    protected $_submit = 'thesis_submission';
    protected $_files = 'thesis_files';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }


    /*
    *	Research Categories
    */
    public function getCategoriesData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_categories))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.rc_program=p.IdProgram', array('p.ProgramName'));


        return $select;
    }


    public function updateCategories($data, $where)
    {
        $db = $this->db;

        $db->update($this->_categories, $data, $where);
    }

    public function addCategories($data)
    {
        $db = $this->db;

        $db->insert($this->_categories, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getCategory($id = null, $all = false)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_categories))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.rc_program=p.IdProgram', array('p.ProgramName'));

        if ($all == false) {
            $select->where('a.approved = 1');
        }

        if ($id) {
            $select->where('a.rc_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
    *	Research Topic
    */
    public function getTopicData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'));


        return $select;
    }

    public function getTopics($all = false)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'));

        if ($all == false) {
            $select->where('a.r_approved = 1');
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTopicSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.r_createdby', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'))
            ->where('a.r_id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateTopic($data, $where)
    {
        $db = $this->db;

        $db->update($this->_topic, $data, $where);
    }

    public function addTopic($data)
    {
        $db = $this->db;

        $db->insert($this->_topic, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
    *	Research Status
    */
    public function getStatusData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_status))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.status_program=p.IdProgram', array('p.ProgramName as status_program_name'))
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.status_course=c.IdSubject', array('CONCAT_WS(" - ", SubCode, SubjectName) as status_course_name'));


        return $select;
    }

    public function updateStatus($data, $where)
    {
        $db = $this->db;

        $db->update($this->_status, $data, $where);
    }

    public function addStatus($data)
    {
        $db = $this->db;

        $db->insert($this->_status, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getStatusByCode($name = '',$type=0)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_status), 'status_id')
            ->where('a.status_code = ?', $name)
            ->where('a.status_type = ?', $type);

        $result = $db->fetchRow($select);

        return $result['status_id'];

    }

    public function getStatus($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_status))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.status_program=p.IdProgram', array('p.ProgramName as status_program_name'))
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.status_course=c.IdSubject', array('SubjectName as status_course_name'));

        if ($id) {
            $select->where('a.status_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }


    /*
    *	Supervisors
    */

    public function getSupervisorPaginate()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_supervisor));
        //->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.supervisor_userid', array())
        //->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as supervisor_name');


        return $select;
    }

    public function updateSupervisor($data, $where)
    {
        $db = $this->db;

        $db->update($this->_supervisor, $data, $where);
    }

    public function addSupervisor($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisor, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisor($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_supervisor));
        //->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.supervisor_userid', array())
        //->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as supervisor_name');

        if ($id) {
            $select->where('a.supervisor_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
    *	Examiner
    */
    public function getExaminerSem($sem_id = '')
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_examinersem));

        if ($sem_id) {
            $select->where('a.es_semesterid=?', $sem_id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function getExaminerBySem($sem_id = '')
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_examiner));

        $select->where('a.semester_id=?', $sem_id);

        $result = $db->query($select)->rowCount();

        return $result;
    }

    public function updateExaminerSem($data, $where)
    {
        $db = $this->db;

        $db->update($this->_examinersem, $data, $where);
    }

    public function addExaminerSem($data)
    {
        $db = $this->db;

        $db->insert($this->_examinersem, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    /* Other Functions */
    public function getCourses($type = null)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("sm" => "tbl_subjectmaster"), array("sm.*"))
            ->join(array("ct" => "tbl_coursetype"), 'ct.IdCourseType =sm.CourseType', array("ct.CourseType"));

        if (is_array($type) && !empty($type)) {
            $select->where('ct.IdCourseType IN (?)', $type);
        } else {
            $select->where('ct.IdCourseType IN (?)', array(2, 3, 19));
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAllCourses()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("sm" => "tbl_subjectmaster"), array("sm.*"))
            ->join(array("ct" => "tbl_coursetype"), 'ct.IdCourseType =sm.CourseType', array("ct.CourseType"))
            ->order('sm.SubCode ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProposalCourses()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("sm" => "tbl_subjectmaster"), array("sm.*"))
            ->join(array("ct" => "tbl_coursetype"), 'ct.IdCourseType =sm.CourseType', array("ct.CourseType"))
            ->join(array("ras" => "thesis_research_activity_setup"), 'ras.ra_course =sm.IdSubject', array("ras.*"))
            ->order('sm.SubCode ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAvailableSupervisors()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        /*$select_not = $lobjDbAdpt->select()
                    ->distinct()
                    ->from(array('a'=>$this->_supervisor),'a.supervisor_userid');*/

        $select = $lobjDbAdpt->select()
            ->from(array('user' => 'tbl_user'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff')
            ->joinLeft(array('college' => 'tbl_collegemaster'), 'college.IdCollege  = staff.IdCollege')
            ->joinLeft(array('role' => 'tbl_definationms'), 'role.idDefinition  = user.IdRole')
            ->where('user.UserStatus = 1')
            ->where('staff.StaffAcademic = 0')
            //->where('user.iduser NOT IN (?)', $select_not)
            ->order('staff.FullName ASC');


        $result = $lobjDbAdpt->fetchAll($select);
        return $result;
    }

    /*
        field of interest
    */


    public function getInterestData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_interest))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');


        return $select;
    }

    public function deleteInterest($id)
    {
        $db = $this->db;

        $db->delete($this->_interest, 'i_id = ' . (int)$id);
    }

    public function updateInterest($data, $where)
    {
        $db = $this->db;

        $db->update($this->_interest, $data, $where);
    }

    public function addInterest($data)
    {
        $db = $this->db;

        $db->insert($this->_interest, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    public function getInterest($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_interest))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        if ($id) {
            $select->where('a.i_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
        reason for applying
    */


    public function getReasonData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_reason))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');


        return $select;
    }

    public function updateReason($data, $where)
    {
        $db = $this->db;

        $db->update($this->_reason, $data, $where);
    }

    public function addReason($data)
    {
        $db = $this->db;

        $db->insert($this->_reason, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    public function getReason($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_reason))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        if ($id) {
            $select->where('a.i_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
    *	Examiner
    */
    public function getExaminerData($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner))
            ->joinLeft(array('s' => 'tbl_semestermaster'), 's.IdSemesterMaster=a.semester_id', array('s.SemesterMainCode', 's.SemesterMainName'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.staff_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->order('fullname ASC');

        if (!empty($where)) {
            if (isset($where['examiner_type']) && $where['examiner_type'] != '') {
                $select->where('a.examiner_type = ?', $where['examiner_type']);
            }
        }


        return $select;
    }

    public function getExaminer()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner))
            ->joinLeft(array('u' => 'tbl_user'), 'a.staff_id = u.iduser and a.staff_id > 0', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as internal_fullname');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExaminerSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->where('a.id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getExaminerViva($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->where('a.id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function selectRegistry($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('rv' => 'registry_values'))
            ->join(array('rt' => 'registry_types'), 'rt.id = rv.type_id', array())
            ->where('rt.code = ?', $id)
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getUser($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_user'))
            ->where('a.iduser=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }


    public function updateExaminer($data, $where)
    {
        $db = $this->db;

        $db->update($this->_examiner, $data, $where);
    }

    public function addExaminer($data)
    {
        $db = $this->db;

        $db->insert($this->_examiner, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
    *	Examiner Roles
    */
    public function getExaminerRoleData($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner_role))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->order('fullname ASC');

        return $select;
    }

    public function getExaminerRole($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner_role));

        if ($id) {
            $select->where('a.exr_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function updateExaminerRole($data, $where)
    {
        $db = $this->db;

        $db->update($this->_examiner_role, $data, $where);
    }

    public function addExaminerRole($data)
    {
        $db = $this->db;

        $db->insert($this->_examiner_role, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    /* evaluation */

    public function getEvaluationSetData($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalset))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.subject_id=c.IdSubject', array('CONCAT_WS(" - ", SubCode, SubjectName) as course_name'))
            ->order('a.id DESC');

        if (!empty($where)) {

        }


        return $select;
    }

    public function getEvaluationSetSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalset))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->where('a.id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateEvaluationSet($data, $where)
    {
        $db = $this->db;

        $db->update($this->_evalset, $data, $where);
    }

    public function addEvaluationSet($data)
    {
        $db = $this->db;

        $db->insert($this->_evalset, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    //item
    public function getEvaluationItemData($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalitem));

        if (!empty($where)) {

        }


        return $select;
    }

    public function getEvaluationItemSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalitem))
            ->where('a.cid=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateEvaluationItem($data, $where)
    {
        $db = $this->db;

        $db->update($this->_evalitem, $data, $where);
    }

    public function addEvaluationItem($data)
    {
        $db = $this->db;

        $db->insert($this->_evalitem, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addEvaluationGroup($data)
    {
        $db = $this->db;

        $db->insert($this->_evalgroups, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getEvaluationItemsBySet($set_id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalitem))
            ->where('a.set_id=?', $set_id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEvaluationGroupsBySet($set_id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalgroups))
            ->where('a.set_id=?', $set_id);

        $result = $db->fetchAll($select);
        return $result;
    }

    /* Supervisor */
    public function addSupervisorSetup($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisorsetup, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisors($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisorsetup))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name'))
            ->where('a.ps_pid = ?', $pid)
            ->where('a.ps_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

    /*
    *	Examiner Setup
    */
    public function getExaminerSetupData()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_examiner_setup))
            ->joinLeft(array('p' => 'tbl_program'), 'a.es_program=p.IdProgram', array('p.ProgramName as es_program_name'))
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.es_course=c.IdSubject', array('CONCAT_WS(" - ", SubCode, SubjectName) as es_course_name'));

        return $select;
    }

    public function updateExaminerSetup($data, $where)
    {
        $db = $this->db;

        $db->update($this->_examiner_setup, $data, $where);
    }

    public function addExaminerSetup($data)
    {
        $db = $this->db;

        $db->insert($this->_examiner_setup, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getExaminerSetup($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_examiner_setup))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.es_program=p.IdProgram', array('p.ProgramName as es_program_name'))
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.es_course=c.IdSubject', array('SubjectName as es_course_name'));

        if ($id) {
            $select->where('a.es_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
        activities setup
    */


    public function getActivitySetupData($type = '')
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_activity_setup))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
            ->order('a.id ASC');

        if ($type != '') {
            $select->where('a.research_type = ?', $type);
        }

        return $select;
    }

    public function updateActivitySetup($data, $where)
    {
        $db = $this->db;

        $db->update($this->_activity_setup, $data, $where);
    }

    public function addActivitySetup($data)
    {
        $db = $this->db;

        $db->insert($this->_activity_setup, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function deleteActivitySetup($id)
    {
        $db = $this->db;

        $db->delete($this->_activity_setup, array('id = ?' => $id));
    }

    public function getActivitySetup($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_activity_setup))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        if ($id) {
            $select->where('a.id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /* downloads */

    public function getDownloadsData($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_downloads))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->joinLeft(array('f' => $this->_file), "f.af_type='download' AND f.af_pid=a.id", array('COUNT(f.af_id) as total_files'))
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.subject_id=c.IdSubject', array('CONCAT_WS(" - ", SubCode, SubjectName) as course_name'))
            ->order('a.id DESC')
            ->group('a.id');

        if (!empty($where)) {

        }


        return $select;
    }

    public function addDownload($data)
    {
        $db = $this->db;

        $db->insert($this->_downloads, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function updateDownload($data, $where)
    {
        $db = $this->db;

        $db->update($this->_downloads, $data, $where);
    }

    public function deleteDownload($id)
    {
        $db = $this->db;

        $db->delete($this->_downloads, array('id = ?' => $id));
    }

    public function getDownloadSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_downloads))
            ->where('a.id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public static function researchType($type)
    {
        $return = '';

        switch ($type) {
            case 'articleship':
                $return = 'Articleship';
                break;
            case 'exemption':
                $return = 'PPP';
                break;
            case 'proposal':
                $return = 'Proposal';
                break;
        }

        return $return;
    }

    /* submission */

    public function getSubmissionByResearchId($id, $type)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submit))
            ->where('a.research_id=?', $id)
            ->where('a.research_type=?', $type);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getSubmission($student_id, $type)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submit))
            ->where('a.student_id=?', $student_id)
            ->where('a.submit_type=?', $type)
            ->where('a.approved IN (?)', array(0, 1))
            ->order('a.id DESC');

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateSubmission($data, $where)
    {
        $db = $this->db;

        $db->update($this->_submit, $data, $where);
    }

    public function addSubmission($data)
    {
        $db = $this->db;

        $db->insert($this->_submit, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
        Colloquium setup
    */


    public function getColloquiumSetupData($type = '')
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_colloquium))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
            ->order('a.id DESC');

        if ($type != '') {
            $select->where('a.research_type = ?', $type);
        }

        return $select;
    }

    public function updateColloquiumSetup($data, $where)
    {
        $db = $this->db;

        $db->update($this->_colloquium, $data, $where);
    }

    public function addColloquiumSetup($data)
    {
        $db = $this->db;

        $db->insert($this->_colloquium, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function deleteColloquiumSetup($id)
    {
        $db = $this->db;

        $db->delete($this->_colloquium, array('id = ?' => $id));
    }

    public function getColloquiumSetup($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_colloquium))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        if ($id) {
            $select->where('a.id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function subjectRegistered($idstudreg)
    {
        $db = $this->db;

        $sql = $db->select()
            ->from(array("a" => 'tbl_studentregsubjects'), array('a.IdSubject'))
            ->join(array("s" => 'tbl_subjectmaster'), 's.IdSubject=a.IdSubject', array())
            ->where('s.CourseType IN (?)', array(2, 3, 19))
            ->where("a.IdStudentRegistration = ?", $idstudreg);


        return $sql;
    }

    public function getResearchById($id, $type)
    {
        $regModel = new Thesis_Model_DbTable_Registration();

        switch ($type) {
            case 'proposal':
                $proposal = $regModel->getProposal($id);
                break;
            case 'articleship':
                $articleship = $regModel->getArticleship($id);
                break;
            case 'exemption':
                $exemption = $regModel->getExemption($id);
                break;
        }

        if (!empty($proposal)) return $proposal;
        if (!empty($articleship)) return $articleship;
        if (!empty($exemption)) return $exemption;

        return '';
    }

    /*
    *	Research Files
    */
    public function addResearchFile($data)
    {
        $db = $this->db;

        $db->insert($this->_files, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getResearchFile($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_files))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getResearchFiles($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_files))
            ->where('a.research_id = ?', $pid)
            ->where('a.research_type = ?', $type)
            ->joinLeft(array('d' => 'tbl_definationms'), "a.type_id = d.idDefinition", array("d.DefinitionDesc as type_name"))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        $result = $db->fetchAll($select);
        return $result;
    }
}