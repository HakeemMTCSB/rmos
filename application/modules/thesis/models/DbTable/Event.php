<?php

class Thesis_Model_DbTable_Event extends Zend_Db_Table
{

    protected $_proposal = 'thesis_proposal';
    protected $_event = 'thesis_event_setup';
    protected $_event_student = 'thesis_event_student';
    protected $_event_examiner = 'thesis_event_examiner';

//    protected $_file = 'thesis_article_files';
//    protected $_supervisor = 'thesis_article_supervisor';
//    protected $_article = 'thesis_articleship';
//    protected $_exemption = 'thesis_exemption';
//    protected $_employment = 'thesis_article_employment';
//    protected $_submission = 'thesis_submission';
//    protected $_submission_history = 'thesis_submission_history';
//
//    protected $_topic = 'thesis_researchtopics';
//    protected $_categories = 'thesis_categories';
//    protected $_status = 'thesis_status';
//    protected $_superrole = 'thesis_supervisor';
//    protected $_examinersem = 'thesis_examinersem'; //deprecated
//    protected $_examiner_setup = 'thesis_examiner_setup';
//    protected $_examiner = 'thesis_article_examiner';
//    protected $_examiner_role = 'thesis_examiner_role';
//
//    protected $_supervisor_history = 'thesis_supervisor_history';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

    }

    public function getEventData($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_event), array('a.*',));

        if ($id) {
            $select->where('a.event_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;

    }

    public function updateEventSetup($data, $where)
    {
        $db = $this->db;

        $db->update($this->_event, $data, $where);
    }

    public function getCount($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_event_student), array('a.*',))
            ->where('event_id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function addEvent($data)
    {
        $db = $this->db;

        $db->insert($this->_event, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addEventStudent($data)
    {
        $db = $this->db;

        $db->insert($this->_event_student, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getEventStudent($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_event_student));

        if ($id) {
            $select->where('a.event_student_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function deleteEventStudent($id)
    {
        $db = $this->db;

        $db->delete($this->_event_student, 'event_student_id = ' . (int)$id);
    }

    public function getProposalApprove($id, $full = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
//            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
//            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
//            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
//            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
//            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
//            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->where('a.p_id = ?', $id);

        if ($full) {
            $select->joinLeft(array('city' => 'tbl_city'), 'city.idCity=p.appl_city', array('CityName'))
                ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=p.appl_state', array('StateName'))
                ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=p.appl_country', array('CountryName'));
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getTaggedStudent($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('td' => 'thesis_event_student'), array('td.*'))
            ->join(array('sr' => 'tbl_studentregistration'), 'td.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->where('td.event_id = ?', $id)//            ->order('sm.SubCode ASC')
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addEventAssessor($data)
    {
        $db = $this->db;

        $db->insert($this->_event_examiner, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getType($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('td' => 'tbl_definationms'), array('td.*'))
            ->where('td.idDefinition = ?', $id)//            ->where('td.idDefType = ?', 187)
        ;

        $result = $db->fetchRow($select);
        return $result;
    }

    public function selectType()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('td' => 'tbl_definationms'), array('td.*'))
//            ->where('td.idDefinition = ?', $id)
            ->where('td.idDefType = ?', 187)
            ->order('td.defOrder');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTaggedAssessor($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('e' => 'thesis_event_examiner'), array())
//            ->join(array("b" => 'thesis_examiner_role'), 'a.ex_examiner_role=b.exr_id', array('b.code as role_name'))
            ->join(array("u" => 'tbl_user'), 'u.iduser=e.examiner_id', array('IdStaff'))
            ->join(array("user" => 'thesis_examiner_user'), 'e.examiner_id=user.staff_id AND user.staff_id > 0', array('user.fullname as assessor_name', 'staff_id'))
            ->where('e.event_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAssessor($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('e' => 'thesis_examiner_user'), array('e.fullname as assessor_name', 'staff_id'))
//            ->from(array('e' => 'thesis_event_examiner'), array())
//            ->join(array("b" => 'thesis_examiner_role'), 'a.ex_examiner_role=b.exr_id', array('b.code as role_name'))
            ->join(array("u" => 'tbl_user'), 'u.iduser=e.staff_id', array('IdStaff'))
//            ->join(array("user" => 'thesis_examiner_user'), 'e.examiner_id=user.staff_id AND user.staff_id > 0', array('user.fullname as assessor_name','staff_id'))
            ->where('u.iduser = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudent($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('tes' => 'thesis_event_student'))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = tes.student_id', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email', 'appl_address2', 'appl_address1', 'appl_postcode', 'appl_city', 'appl_state', 'appl_state_others', 'appl_city_others'))
            ->joinLeft(array('te' => 'thesis_event_setup'), 'te.event_id = tes.event_id', array('te.*'))
//            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
//            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
//            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
//            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=a.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
//            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
//            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
//            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode'))
//            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster =a.semester_start', array('SemesterMainName','SemesterMainCode'))
//            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc'))
            ->where('tes.event_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getResearch($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('tp' => 'thesis_proposal'))->where('tp.student_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSupervisors($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => 'thesis_article_supervisor'))
            ->join(array("b" => 'thesis_supervisor'), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array('user.IdStaff'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name'))
            ->where('a.ps_pid = ?', $pid)
            ->where('a.ps_type = ?', $type);
//echo $select;exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEmail($id)
    {
        $db = $this->db;

        $select = "
                       select a.student_id as d_id,CONCAT_WS('',appl_fname,appl_mname,appl_lname) as d_name, d.appl_email as email,NULL as email2, 'student' as role,b.p_id,e.event_venue,e.event_name,e.event_type,e.event_date,a.start_time,a.end_time,d.appl_fname as student_name,ac.DefinitionDesc as ActivityName,pr.ProgramCode from thesis_event_student a 
                    join thesis_proposal b on b.student_id = a.student_id 
                    join tbl_studentregistration c on c.IdStudentRegistration = a.student_id 
                    join student_profile d on d.id = c.sp_id 
                    join thesis_event_setup e on e.event_id = a.event_id
                    join tbl_definationms ac on ac.idDefinition = e.event_type
                    join tbl_program pr on pr.IdProgram= c.IdProgram
                    where a.event_id = '$id'
                    group by d.appl_email 
                    
                    union 
                    
                    select a.ps_supervisor_id as d_id,CONCAT(sal.DefinitionDesc,' ',s.FullName) as d_name, s.Email as email,s.Email2 as email2, 'supervisor' as role,b.p_id,e.event_venue,e.event_name,e.event_type,e.event_date,c.start_time,c.end_time,sp.appl_fname as student_name,ac.DefinitionDesc as ActivityName,pr.ProgramCode from thesis_article_supervisor a 
                    join tbl_user u on u.IdUser = a.ps_supervisor_id 
                    join tbl_staffmaster s on s.IdStaff = u.IdStaff 
                    join thesis_proposal b on b.p_id = a.ps_pid 
                    join thesis_event_student c on c.student_id = b.student_id 
                    join thesis_event_setup e on e.event_id = c.event_id
                    join student_profile sp on sp.id= c.student_id 
                    join tbl_definationms ac on ac.idDefinition = e.event_type
                    join tbl_definationms sal on sal.idDefinition = s.FrontSalutation
                    join tbl_studentregistration st on st.sp_id= c.student_id 
                    join tbl_program pr on pr.IdProgram= st.IdProgram
                    where a.ps_type = 'proposal' and c.event_id = '$id'
                    group by s.Email

                    union 
                    
                    select s.IdStaff as d_id,CONCAT(sal.DefinitionDesc,' ',s.FullName) as d_name, s.Email as email,s.Email2 as email2, 'director' as role,b.p_id,e.event_venue,e.event_name,e.event_type,e.event_date,c.start_time,c.end_time,sp.appl_fname as student_name,ac.DefinitionDesc as ActivityName,pr.ProgramCode from tbl_studentregistration a   
                    join (SELECT IdProgram,IdStaff, MAX(UpdDate) maxmodify FROM tbl_chiefofprogramlist GROUP BY IdProgram) u ON u.IdProgram = a.IdProgram    
                    join tbl_staffmaster s on s.IdStaff = u.IdStaff
                    join thesis_event_student c on c.student_id = a.sp_id
                    join thesis_proposal b on b.student_id = c.student_id 
                    join thesis_event_setup e on e.event_id = c.event_id
                    join student_profile sp on sp.id= a.sp_id
                    join tbl_definationms ac on ac.idDefinition = e.event_type
                    join tbl_definationms sal on sal.idDefinition = s.FrontSalutation
                    join tbl_program pr on pr.IdProgram= a.IdProgram
                    where c.event_id = '$id'
                    group by s.Email
                    ";
//echo $select;exit;
        $result = $db->fetchAll($select);
        return $result;
    }

}