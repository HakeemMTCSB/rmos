<?php

class Thesis_Model_DbTable_Supervisor extends Zend_Db_Table
{

    protected $_supervisor = 'thesis_article_supervisor';
    protected $_superrole = 'thesis_supervisor';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public function getSupervisor($id, $type, $main = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array("a" => $this->_supervisor))
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.description as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name'))
            ->where('a.ps_pid = ?', $id)
            ->where('a.ps_type = ?', $type);

        if ($main == 1) {
            $select->where('b.code = ?', 'main_supervisor');
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function getSupervisorByUserId($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array("a" => $this->_supervisor))
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.description as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name'))
            ->where('user.iduser = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }



}