<?php

/**
 * Created by Izham.
 * User: Izham
 * Date: 14/9/2015
 * Time: 12:03 PM
 */
class Thesis_Model_DbTable_Report extends Zend_Db_Table
{

    public function comprehensiveExamList()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('Name' => 'concat(b.appl_fname, " ", b.appl_lname)', 'Student_ID' => 'a.registrationId', 'Programme_Name' => 'c.ProgramName', 'Programme_Scheme' => 'concat(e.DefinitionDesc, " ", f.DefinitionDesc, " ", g.DefinitionDesc)'))
            ->joinLeft(array('b' => 'student_profile'), 'a.sp_id=b.id')
            ->joinLeft(array('c' => 'tbl_program'), 'a.IdProgram=c.IdProgram')
            ->joinLeft(array('d' => 'tbl_program_scheme'), 'a.IdProgramScheme=d.IdProgramScheme')
            ->joinLeft(array('e' => 'tbl_definationms'), 'd.mode_of_program=e.idDefinition')
            ->joinLeft(array('f' => 'tbl_definationms'), 'd.mode_of_study=f.idDefinition')
            ->joinLeft(array('g' => 'tbl_definationms'), 'd.program_type=g.idDefinition')
            ->join(array('h' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=h.IdStudentRegistration AND h.IdSubject=16 AND (h.grade_status="Pass" OR h.exam_status="CT" OR h.exam_status="EX")')
            ->join(array('i' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=i.IdStudentRegistration AND i.IdSubject=18 AND (i.grade_status="Pass" OR i.exam_status="CT" OR i.exam_status="EX")')
            ->join(array('j' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=j.IdStudentRegistration AND (j.IdSubject=26 OR j.IdSubject=2) AND (j.grade_status="Pass" OR j.exam_status="CT" OR j.exam_status="EX")')
            ->join(array('k' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=k.IdStudentRegistration AND k.IdSubject=21 AND (k.grade_status="Pass" OR k.exam_status="CT" OR k.exam_status="EX")')
            ->join(array('l' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=l.IdStudentRegistration AND l.IdSubject=22 AND (l.grade_status="Pass" OR l.exam_status="CT" OR l.exam_status="EX")')
            ->join(array('m' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=m.IdStudentRegistration AND m.IdSubject=23 AND (m.grade_status="Pass" OR m.exam_status="CT" OR m.exam_status="EX")')
            ->joinLeft(array('n' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration=n.IdStudentRegistration AND n.IdSubject=126', array())
            ->where('IFNULL(n.grade_status, "X") != "Pass"')
            ->where('a.IdProgram = ?', 3)
            ->group('a.IdStudentRegistration');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function subList($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregsubjects'), array('value' => '*'))
            ->joinLeft(array('b' => 'tbl_subjectmaster'), 'a.IdSubject=b.IdSubject')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.IdSubject = 16 OR a.IdSubject = 18 OR a.IdSubject = 26 OR a.IdSubject = 2 OR a.IdSubject = 21 OR a.IdSubject = 22 OR a.IdSubject = 23');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProposalDefenseData($where = array(), $returnType = 'sql')
    {
        $db = Zend_Db_Table::getDefaultAdapter();

//        $select = $db->select()
//            ->from(array('a' => 'thesis_proposal'), array('a.*', 'a.p_id as research_id'))
//            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
//            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
//            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode','IdFaculty'))
//            ->order('a.created_date DESC');

        $select = $db->select()
            ->from(array('tes' => 'thesis_event_student'), array('tes.*'))
            ->join(array('sa' => 'tbl_studentregistration'), 'tes.student_id=sa.IdStudentRegistration', array('registrationId'))
            ->join(array('a' => 'thesis_proposal'), 'a.student_id=sa.IdStudentRegistration', array('a.*', 'a.p_id as research_id', 'a.p_title as p_title'))
            ->join(array('p' => 'student_profile'), 'p.id=sa.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'subj.IdSubject=a.course_id', array('SubjectName as course_name', 'SubCode', 'IdFaculty'))
            ->join(array('ts' => 'thesis_status'), 'ts.status_id=a.pd_status', array())
            ->join(array('es' => 'thesis_event_setup'), 'tes.event_id=es.event_id', array('es.*'))
            ->where('ts.status_code LIKE ?', 'approved');

        if (!empty($where)) {

            if (isset($where['faculty_id']) && $where['faculty_id'] != '') {
                $select->where('subj.IdFaculty = ?', $where['faculty_id']);
            }

            if (isset($where['event_type']) && $where['event_type'] != '') {
                $select->where('es.event_type = ?', $where['event_type']);
            }

            if (isset($where['start_date']) && $where['end_date']) {
                $select->where('es.event_date between "' . $where['start_date'] . '" and "' . $where['end_date'] . '"');
            }
        }

        $result = $db->fetchAll($select);

        if ($returnType == 'sql') {
            return $select;
        } else {
            return $result;
        }
    }

    public function getSupervisor($where = array())
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('sr.IdStudentRegistration'))
            ->join(array('a' => 'thesis_proposal'), 'a.student_id=sr.IdStudentRegistration', array('a.p_id'))
            ->join(array('b' => 'thesis_article_supervisor'), 'b.ps_pid=a.p_id', array('b.ps_supervisor_id'))
            ->join(array('c' => 'tbl_user'), 'c.iduser=b.ps_supervisor_id', array('c.fName', 'c.UserStatus'))
//            ->join(array('d' => 'tbl_staffmaster'), 'd.IdStaff=c.IdStaff', array('d.FullName'))
//            ->where('a.p_id = ? ',525)
            ->where('a.pid = ? ', 0)
            ->group('b.ps_supervisor_id')//            ->limit(5)
            ->order('c.fName asc')
        ;

        if (!empty($where)) {
//            if (isset($where['student_name']) && $where['student_name'] != '') {
//                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
//            }
//
//            if (isset($where['student_id']) && $where['student_id'] != '') {
//                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
//            }
//
//            if (isset($where['pd_status']) && $where['pd_status'] != '') {
//                $select->where('a.pd_status = ?', $where['pd_status']);
//            }
//
//            if (isset($where['status']) && $where['status'] != '') {
//                $select->where('a.p_status = ?', $where['status']);
//            }
//
//            if (isset($where['approval_date']) && $where['approval_date'] != '') {
//                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
//            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where("sr.IdProgram IN ($where[id_program])");
            }

//            if (isset($where['course_id']) && $where['course_id'] != '') {
//                $select->where('a.course_id = ?', $where['course_id']);
//            }
//
//            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
//                $select->where('col.IdCollege = ?', $where['IdCollege']);
//            }
//
//            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
//                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
//            }
        }
//        echo $select;exit;

//        if ($returnType == 'sql') {
//            return $select;
//        } else {
        $result = $db->fetchAll($select);
        return $result;
//        }
    }

    public function getSupervisor2($where = array(),$where2 = array())
    {
//print_r($where2);exit;
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('sr.IdStudentRegistration'))
            ->join(array('a' => 'thesis_proposal'), 'a.student_id=sr.IdStudentRegistration', array('a.p_id'))
            ->join(array('b' => 'thesis_article_supervisor'), 'b.ps_pid=a.p_id', array('b.ps_supervisor_id'))
            ->join(array('c' => 'tbl_user'), 'c.iduser=b.ps_supervisor_id', array('c.fName', 'c.UserStatus'))
//            ->join(array('d' => 'tbl_staffmaster'), 'd.IdStaff=c.IdStaff', array('d.FullName'))
//            ->where('a.p_id = ? ',525)
            ->where('a.pid = ? ', 0)
            ->group('b.ps_supervisor_id')
            //            ->limit(5)
            ->order('c.fName asc')
        ;

//        if (!empty($where) || !empty($where2)) {
            if (isset($where2['supervisor_name']) && $where2['supervisor_name'] != '') {
                $select->where('c.fName LIKE ?', '%' . $where2['supervisor_name'] . '%');
            }
//
//            if (isset($where['student_id']) && $where['student_id'] != '') {
//                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
//            }
//
//            if (isset($where['pd_status']) && $where['pd_status'] != '') {
//                $select->where('a.pd_status = ?', $where['pd_status']);
//            }
//
//            if (isset($where['status']) && $where['status'] != '') {
//                $select->where('a.p_status = ?', $where['status']);
//            }
//
//            if (isset($where['approval_date']) && $where['approval_date'] != '') {
//                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
//            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where("sr.IdProgram IN ($where[id_program])");
            }

//            if (isset($where['course_id']) && $where['course_id'] != '') {
//                $select->where('a.course_id = ?', $where['course_id']);
//            }
//
//            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
//                $select->where('col.IdCollege = ?', $where['IdCollege']);
//            }
//
//            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
//                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
//            }
//        }
//        echo $select;exit;

//        if ($returnType == 'sql') {
//            return $select;
//        } else {
        $result = $db->fetchAll($select);
        return $result;
//        }
    }

    public function getStudent($where = array())
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('sr.IdStudentRegistration'))
            ->join(array('sp' => 'student_profile'), 'sp.id=sr.sp_id', array('sp.appl_fname', 'sp.id as student_id'))
            ->join(array('a' => 'thesis_proposal'), 'a.student_id=sr.IdStudentRegistration', array('a.p_id'))
            ->join(array('b' => 'thesis_article_supervisor'), 'b.ps_pid=a.p_id', array('b.ps_supervisor_id'))
            ->join(array('c' => 'tbl_user'), 'c.iduser=b.ps_supervisor_id', array('c.fName', 'c.UserStatus'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
//            ->join(array('d' => 'tbl_staffmaster'), 'd.IdStaff=c.IdStaff', array('d.FullName'))
//            ->where('a.p_id = ? ',525)
            ->where('a.pid = ? ', 0)
//            ->group('b.ps_supervisor_id')
//            ->limit(5)
        ;

        if (!empty($where)) {

            if (isset($where['supervisor_id']) && $where['supervisor_id'] != '') {
                $select->where('b.ps_supervisor_id = ?', $where['supervisor_id']);
            }
//            if (isset($where['student_name']) && $where['student_name'] != '') {
//                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
//            }
//
//            if (isset($where['student_id']) && $where['student_id'] != '') {
//                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
//            }
//
//            if (isset($where['pd_status']) && $where['pd_status'] != '') {
//                $select->where('a.pd_status = ?', $where['pd_status']);
//            }
//
//            if (isset($where['status']) && $where['status'] != '') {
//                $select->where('a.p_status = ?', $where['status']);
//            }
//
//            if (isset($where['approval_date']) && $where['approval_date'] != '') {
//                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
//            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where("sr.IdProgram IN ($where[id_program])");
            }

//            if (isset($where['course_id']) && $where['course_id'] != '') {
//                $select->where('a.course_id = ?', $where['course_id']);
//            }
//
//            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
//                $select->where('col.IdCollege = ?', $where['IdCollege']);
//            }
//
//            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
//                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
//            }
        }
//        echo $select;exit;

//        if ($returnType == 'sql') {
//            return $select;
//        } else {
        $result = $db->fetchAll($select);
        return $result;
//        }
    }

    public function getStudentNumbers($where = array())
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('sr.IdStudentRegistration'))
            ->join(array('sp' => 'student_profile'), 'sp.id=sr.sp_id', array('sp.appl_fname', 'sp.id as student_id'))
            ->join(array('a' => 'thesis_proposal'), 'a.student_id=sr.IdStudentRegistration', array('a.p_id'))
            ->join(array('b' => 'thesis_article_supervisor'), 'b.ps_pid=a.p_id', array('b.ps_supervisor_id'))
            ->join(array('c' => 'tbl_user'), 'c.iduser=b.ps_supervisor_id', array('c.fName', 'c.UserStatus'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition=sr.profileStatus', array('d.DefinitionDesc as profile_status'))
//            ->join(array('d' => 'tbl_staffmaster'), 'd.IdStaff=c.IdStaff', array('d.FullName'))
//            ->where('a.p_id = ? ',525)
            ->where('a.pid = ? ', 0)
            ->group('sp.id')
//            ->limit(5)
        ;

        if (!empty($where)) {

            if (isset($where['supervisor_id']) && $where['supervisor_id'] != '') {
                $select->where('b.ps_supervisor_id = ?', $where['supervisor_id']);
            }
//            if (isset($where['student_name']) && $where['student_name'] != '') {
//                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
//            }
//
//            if (isset($where['student_id']) && $where['student_id'] != '') {
//                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
//            }
//
//            if (isset($where['pd_status']) && $where['pd_status'] != '') {
//                $select->where('a.pd_status = ?', $where['pd_status']);
//            }
//
//            if (isset($where['status']) && $where['status'] != '') {
//                $select->where('a.p_status = ?', $where['status']);
//            }
//
//            if (isset($where['approval_date']) && $where['approval_date'] != '') {
//                $select->where('DATE_FORMAT(a.approved_date,"%Y-%m-%d") = ?', date('Y-m-d', $where['approval_date']));
//            }

            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where("sr.IdProgram IN ($where[id_program])");
            }

//            if (isset($where['course_id']) && $where['course_id'] != '') {
//                $select->where('a.course_id = ?', $where['course_id']);
//            }
//
//            if (isset($where['IdCollege']) && $where['IdCollege'] != '') {
//                $select->where('col.IdCollege = ?', $where['IdCollege']);
//            }
//
//            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
//                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
//            }
        }
//        echo $select;exit;

//        if ($returnType == 'sql') {
//            return $select;
//        } else {
        $result = $db->fetchAll($select);
        return $result;
//        }
    }

    public function getActivities($id, $course)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'thesis_research_activity_tagged'), array('a.*'))
            ->joinLeft(array('tp' => 'thesis_proposal'), "tp.course_id = a.rat_course AND tp.activity_id = a.rat_type AND (tp.pid = $id OR tp.p_id = $id)", array('tp.pid', 'tp.p_id', 'created_date', 'pd_status', 'pd_approval'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.rat_type', array('DefinitionDesc', 'DefinitionCode'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=tp.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=tp.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('dir' => 'thesis_status'), 'dir.status_id=tp.pd_approval', array('dir.status_code as dir_status_name', 'dir.status_description as dir_status_description'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster = tp.semester_start', array('sem.SemesterMainDefaultLanguage'))
//            ->where('td.DefinitionCode != ?', 'registration')
            ->where('a.rat_course = ?', $course)
            ->order('td.defOrder');

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getSupervisorReport($where = array())
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select .= "SELECT `tp`.*, `tu`.`fName`, `sp`.`appl_fname`,`sm`.`SubCode`,`sem`.`SemesterMainDefaultLanguage`,`sr`.`registrationId`,`sem`.`AcademicYear`,`prog`.`ProgramCode` FROM `thesis_article_supervisor` AS `tas` 
                   LEFT JOIN `thesis_proposal` AS `tp` ON tp.p_id=tas.ps_pid
                   INNER JOIN (SELECT tp2.student_id, MAX(tp2.updated_date) AS maxdateid FROM thesis_proposal tp2 GROUP BY tp2.student_id) tp3 ON tp.updated_date = tp3.maxdateid
                   LEFT JOIN `tbl_user` AS `tu` ON tu.iduser=tas.ps_supervisor_id                     
                   LEFT JOIN `tbl_studentregistration` AS `sr` ON sr.IdStudentRegistration=tp.student_id 
                   LEFT JOIN `student_profile` AS `sp` ON sp.id=sr.sp_id
                   LEFT JOIN `tbl_subjectmaster` AS `sm` ON sm.IdSubject=tp.course_id
                   LEFT JOIN `tbl_semestermaster` AS `sem` ON sem.IdSemesterMaster=tp.semester_start
                   LEFT JOIN `tbl_program` AS `prog` ON prog.IdProgram=sr.IdProgram
                   ";
//        WHERE (tas.ps_supervisor_id = 970)
        $t = 0;
        if (!empty($where)) {

            if (isset($where['id_program']) && $where['id_program'] != '') {
                if($t == 0){
                    $select.="where sr.IdProgram =".$where['id_program'];
                    $t = 1;
                }else{
                    $select.=" and sr.IdProgram =".$where['id_program'];
                }
            }

            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
                if($t == 0){
                    $select.="where sem.IdSemesterMaster =".$where['IdSemester'];
                    $t = 1;
                }else{
                    $select.=" and sem.IdSemesterMaster =".$where['IdSemester'];
                }
            }

            if (isset($where['Year']) && $where['Year'] != '') {
                if($t == 0){
                    $select.="where sem.AcademicYear =".$where['Year'];
                    $t = 1;
                }else{
                    $select.=" and sem.AcademicYear =".$where['Year'];
                }
            }

            if (isset($where['supervisor_name']) && $where['supervisor_name'] != '') {
//                $select->where('fName LIKE ?', '%' . $where['supervisor_name'] . '%');
//                $select.="where fName LIKE '%min%'";
                if($t == 0){
                    $select.="where fName LIKE '%".$where['supervisor_name']."%'";
                    $t = 1;
                }else{
                    $select.=" and fName LIKE '%".$where['supervisor_name']."%'";
                }
            }

            if (isset($where['course_id']) && $where['course_id'] != '') {
                if($t == 0){
                    $select.="where course_id =".$where['course_id'];
                    $t = 1;
                }else{
                    $select.=" and course_id =".$where['course_id'];
                }
            }

            if ($where['supervisor_type'] != 2) {
                if($t == 0){
                    $select.="where tas.ps_supervisor_type =".$where['supervisor_type'];
                    $t = 1;
                }else{
                    $select.=" and tas.ps_supervisor_type =".$where['supervisor_type'];
                }
            }
        }

        $select .= ' GROUP BY tp.student_id,tu.iduser';
        $select .= ' ORDER BY tu.fName';

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getProgressReport($where = array())
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("tas" => 'thesis_article_supervisor'), array())
            ->join(array('tp' => 'thesis_proposal'), 'tp.p_id=tas.ps_pid', array('tp.*'))
//            ->joinLeft(array('tpr' => 'thesis_progressreport'), 'tpr.research_id=tp.p_id', array('tpr.student_id as std_id'))
//            ->join(array('tpr' => 'thesis_progressreport'), 'tpr.student_id=tp.student_id', array('tpr.student_id as std_id'))
//            ->joinLeft(array('tpr' => 'thesis_progressreport'), 'tpr.student_id=tp.student_id', array('tpr.student_id as std_id'))
            ->joinLeft(array('tu' => 'tbl_user'), 'tu.iduser=tas.ps_supervisor_id', array('tu.fName'))
            ->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=tp.student_id', array('sr.registrationId'))
            ->joinLeft(array('sp' => 'student_profile'), 'sp.id=sr.sp_id', array('sp.appl_fname'))
            ->joinLeft(array('prog' => 'tbl_program'), 'prog.IdProgram=sr.IdProgram', array('prog.ProgramCode'))
//            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=tpr.semester_id', array())
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=tp.semester_start', array())
//            ->where('tp.student_id = ?', 10911)
            ->group('tp.student_id')
            ->group('tu.iduser')
        ;

        if (!empty($where)) {
            if (isset($where['id_program']) && $where['id_program'] != '') {
                $select->where('sr.IdProgram = ?', $where['id_program']);
            }

            if (isset($where['IdSemester']) && $where['IdSemester'] != '') {
                $select->where('sem.IdSemesterMaster = ?', $where['IdSemester']);
            }

            if (isset($where['Year']) && $where['Year'] != '') {
                $select->where('sem.AcademicYear = ?', $where['Year']);
            }

            if (isset($where['OrderBy']) && $where['OrderBy'] == 1) {
                $select->order('prog.ProgramCode ASC');
                $select->order('tu.fName ASC');
            }elseif(isset($where['OrderBy']) && $where['OrderBy'] == 2){
                $select->order('tu.fName ASC');
                $select->order('prog.ProgramCode ASC');
            }else{
                $select->order('prog.ProgramCode ASC');
                $select->order('tu.fName ASC');
            }
        }

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getProgressPerSemesterYear($id= null,$sem = null,$year = null,$seq = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('tp' => 'thesis_progressreport'), array("num" => "COUNT(*)"))
            ->join(array('ts' => 'tbl_semestermaster'), 'ts.IdSemesterMaster=tp.semester_id', array())
            ->where('tp.student_id = ?', $id)
            ->where('ts.sequence = ?', $seq);

        if (isset($sem) && $sem != null) {
            $select->where('ts.IdSemesterMaster = ?', $sem);
        }
        if (isset($year) && $year != null) {
            $select->where('ts.AcademicYear = ?', $year);
        }


        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSeq($id= null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ts' => 'tbl_semestermaster'), array('ts.sequence'))
            ->where('ts.IdSemesterMaster = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getLatestPdByStudent($id= null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('tp' => 'thesis_proposal'), array('tp.p_id'))
            ->where('tp.student_id = ?', $id)
            ->order('tp.updated_date desc')
        ;

        $result = $db->fetchRow($select);
        return $result;
    }
}