<?php

class Thesis_Model_DbTable_DataMigration extends Zend_Db_Table
{
    protected $_event = 'thesis_event_setup';
    protected $_event_student = 'thesis_event_student';
    protected $_event_examiner = 'thesis_event_examiner';

    protected $_research_activity = 'thesis_research_activity_setup';
    protected $_research_activity_tagged = 'thesis_research_activity_tagged';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

    }

    public function intakeData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'tbl_intake'), array("num" => "COUNT(*)", "*"))
                ->where('a.IntakeId = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'tbl_intake'))
                ->where("a.IntakeId LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'tbl_intake'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function intakeIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select i.sim_id intake_id,i.sim_desc_eng intake_name, i.sim_desc malay_name, i.sim_app_start_date start_date, i.sim_app_end_date end_date
                    from stud_intake_main i
                    where length(sim_id) = 3
                    and sim_id <> 'BPG'
                    order by 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function branchData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'tbl_branchofficevenue'), array("num" => "COUNT(*)", "*"))
                ->where('a.BranchCode = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'tbl_branchofficevenue'))
                ->where("a.BranchCode LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'tbl_branchofficevenue'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function branchIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select branch_desc,branch_short,branch_id,branch_tel from branch_main
                    where branch_status = 'ACTIVE'
                    order by 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function semesterData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'tbl_semestermaster'), array("num" => "COUNT(*)", "*"))
                ->where('a.SemesterMainCode = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where("a.SemesterMainCode LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function semesterIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select s.SEMESTER_STATUS_DESC_SHORT SemesterMainName,s.semester_status_desc_eng SemesterMainDefaultLanguage,s.semester_status_id SemesterMainCode,
                    s.SEMESTER_STATUS_SESSION description, s.semester_status_year AcademicYear,4 SemesterType, 
                    TO_CHAR(s.semester_status_datefrm, 'YYYY-MM-DD') SemesterMainStartDate, TO_CHAR(s.semester_status_dateto, 'YYYY-MM-DD') SemesterMainEndDate,
                    s.SEMESTER_STATUS_SEMESTER sequence,semester_status_program IdScheme
                    from semester_status s
                    where semester_status_program in (5,6)
                    order by semester_status_id desc";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function courseData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'tbl_subjectmaster'), array("num" => "COUNT(*)", "*"))
                ->where('a.SubCode = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'tbl_subjectmaster'))
                ->where("a.SubCode LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'tbl_subjectmaster'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function courseIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select m.course_faculty IdFaculty, m.course_desc_long SubjectName, m.course_id SubCode, m.course_credit_hours CreditHours
                    from course_main m
                    where course_nature in ('PROJECT', 'PROJECT I', 'PROJECT II')
                    order by 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function programData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'tbl_program'), array("num" => "COUNT(*)", "*"))
                ->where('a.ProgramCode = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'tbl_program'))
                ->where("a.ProgramCode LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'tbl_program'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function getProgramID($id = null)
    {
        $db = $this->db;

            $select = $db->select()
                ->from(array('a' => 'tbl_program'))
                ->where('a.ProgramCode = ?', $id);

            $result = $db->fetchRow($select);

            return $result;

    }

    public function getStudentProfileIDOnly($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'student_profile'))
//            ->join(array('b' => 'tbl_studentregistration'), 'b.sp_id = a.id', array('IdStudentRegistration'))
            ->where('a.appl_username = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getStudentProfileID($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'student_profile'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.sp_id = a.id', array('IdStudentRegistration'))
            ->where('a.appl_username = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function programIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select degree_desc_short ProgramName, degree_id ProgramCode, degree_tot_credit_hours TotalCreditHours, degree_desc_long ArabicName, 
                    decode(degree_level,4,5,degree_level) IdScheme
                    from degree_main d
                    where degree_level in (4,6)
                    and degree_id <> '1'
                    order by 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function getIntakeID($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_intake'))
            ->where('a.IntakeId = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getBranchID($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_branchofficevenue'))
            ->where('a.BranchCode = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getSemesterID($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_semestermaster'))
            ->where('a.SemesterMainCode = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getCourseID($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_subjectmaster'))
            ->where('a.SubCode = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function studentData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'student_profile'), array("num" => "COUNT(*)", "*"))
                ->where('a.appl_username = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'student_profile'))
                ->where("a.appl_username LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'student_profile'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function studentIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select stud_name appl_fname, stud_ic_cur appl_idnumber, p.stud_addr_cur ||' '|| stud_addr_cur2 appl_address1, stud_pcode_cur appl_postcode, 
                    stud_email appl_email,stud_id appl_username, TO_CHAR(stud_dob, 'YYYY-MM-DD') appl_dob,
                    decode(stud_sex,'MALE',1,'FEMALE',2,'OTHERS',3,stud_sex) appl_gender, stud_tel_home phone_home, stud_tel_work phone_office, stud_hp_no phone_mobile
                    from student_profile p where stud_program in (6)
                    and stud_status = 'ACTIVE'
                    order by stud_program, 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function studentRegistrationData($id = null, $formdata = null)
    {
        $db = $this->db;

        if ($id) {

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'), array("num" => "COUNT(*)", "*"))
                ->where('a.registrationId = ?', $id);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->joinLeft(array('b' => 'tbl_intake'), 'b.IdIntake = a.IdIntake', array('IntakeId','IntakeDefaultLanguage'))
                ->joinLeft(array('c' => 'tbl_branchofficevenue'), 'c.IdBranch = a.IdBranch', array('BranchCode','BranchName'))
                ->where("a.registrationId LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->joinLeft(array('b' => 'tbl_intake'), 'b.IdIntake = a.IdIntake', array('IntakeId','IntakeDefaultLanguage'))
                ->joinLeft(array('c' => 'tbl_branchofficevenue'), 'c.IdBranch = a.IdBranch', array('BranchCode','BranchName'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function studentRegistrationIcemData()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select stud_id student_ID, stud_degree program, stud_intake intake, 
                    branch_id branch
                    from student_profile p where stud_program in (6)
                    and stud_status = 'ACTIVE'
                    order by stud_program, 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function researchData($id = array(), $formdata = null)
    {
        $db = $this->db;

        if ($id) {
            $select = $db->select()
                ->from(array('a' => 'thesis_proposal'), array("num" => "COUNT(*)", "*"))
                ->where('a.student_id = ?', $id['student_id'])
                ->where('a.semester_start = ?', $id['semester_start'])
                ->where('a.course_id = ?', $id['course_id']);

            $result = $db->fetchRow($select);

        } elseif ($formdata) {
            $select = $db->select()
                ->from(array('a' => 'thesis_proposal'))
                ->joinLeft(array('e' => 'tbl_studentregistration'), 'e.IdStudentRegistration = a.student_id', array())
                ->joinLeft(array('b' => 'student_profile'), 'b.id = e.sp_id', array('appl_fname','appl_username'))
                ->joinLeft(array('c' => 'tbl_semestermaster'), 'c.IdSemesterMaster = a.semester_start', array('SemesterMainDefaultLanguage'))
                ->joinLeft(array('d' => 'tbl_subjectmaster'), 'd.IdSubject = a.course_id', array('SubjectName'))
                ->where("b.appl_username LIKE '%" . $formdata . "%'");

            $result = $db->fetchAll($select);
        } else {

            $select = $db->select()
                ->from(array('a' => 'thesis_proposal'))
                ->joinLeft(array('e' => 'tbl_studentregistration'), 'e.IdStudentRegistration = a.student_id', array())
                ->joinLeft(array('b' => 'student_profile'), 'b.id = e.sp_id', array('appl_fname','appl_username'))
                ->joinLeft(array('c' => 'tbl_semestermaster'), 'c.IdSemesterMaster = a.semester_start', array('SemesterMainDefaultLanguage'))
                ->joinLeft(array('d' => 'tbl_subjectmaster'), 'd.IdSubject = a.course_id', array('SubjectName'))
                ->where('a.icem_migration = ?', 'Migrated')
                ->order('a.icem_migration_date DESC');

            $result = $db->fetchAll($select);

        }

        return $result;

    }

    public function researchIcemData($sem = null,$level = null)
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select s.* from
                    (select c.courseregis_stud_id , courseregis_semester_id semester, c.courseregis_course_id 
                    from course_registration c
                    where substr(c.courseregis_semester_id,-3) = '$sem'
                    and stud_check.subject_project(c.courseregis_course_id) = 'Y'
                    and c.courseregis_de_confirm = 'Y'
                    and get_stud_info.stud_program(courseregis_stud_id) in ($level)
                    order by 1)s";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function getFacultyId($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_collegemaster'), array('a.IdCollege'))
            ->where('a.CollegeCode = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getSemester()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_semestermaster'), array('substr(a.SemesterMainCode,-3) as sem_id','a.IdSemesterMaster','a.SemesterMainDefaultLanguage'))
            ->where('a.IdScheme = ?', 6)
            ->order('a.SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getActivityData($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('rat' => $this->_research_activity_tagged), array('rat.*',))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = rat.rat_type', array('DefinitionDesc'))
            ->where('rat.rat_ra_id = ?', $id)
            ->order('td.defOrder');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getActivityByCourse($id = null)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("rat" => "thesis_research_activity_tagged"), array("rat.*"))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = rat.rat_type', array('DefinitionDesc'))
            ->where('rat.rat_course = ? ', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addActivity($data)
    {
        $db = $this->db;

        $db->insert($this->_research_activity, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addActivityTagged($data)
    {
        $db = $this->db;

        $db->insert($this->_research_activity_tagged, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function deleteActivity($id)
    {
        $db = $this->db;

        $db->delete($this->_research_activity_tagged, 'rat_ra_id = ' . (int)$id);
    }

    public function deleteResearch($id)
    {
        $db = $this->db;

        $db->delete($this->_research_activity, 'ra_id = ' . (int)$id);
    }

    public function getResearchDetail($id = null)
    {
        $db = $this->db;
        $details = array();
        $select1 = $db->select()
//            ->from(array('tp' => 'thesis_proposal'), array('tp.*','MAX(updated_date)'))
            ->from(array('tp' => 'thesis_proposal'), array('tp.*'))
            ->where('tp.p_title IS NOT NULL')
//            ->where('tp.student_id = ?', 8573)
            ->where('tp.updated_date = (SELECT MAX(tp2.updated_date)
                 FROM thesis_proposal tp2
                 WHERE tp2.student_id = tp.student_id)')
            ->group('tp.student_id')
        ;
        
        $details = $db->fetchAll($select1);

        foreach ($details as $key => $detail) {
            $select2 = $db->select()
                ->from(array('tp2' => 'thesis_proposal'), array('tp2.*'))
                ->where('tp2.student_id = ?', $detail['student_id'])
                ->where('tp2.p_id > ?', $detail['p_id'])
            ;
            $each_research = $db->fetchAll($select2);

            $details[$key]['each_research'] = $each_research;

            $select3 = $db->select()
                ->from(array('tas' => 'thesis_article_supervisor'), array('tas.*'))
                ->joinLeft(array('tsh' => 'thesis_supervisor_history'), 'tsh.ps_id=tas.ps_id', array('tsh.*','createdate_history' => 'tsh.created_date','createdby_history' => 'tsh.created_by'))
                ->where('tas.ps_pid = ?', $detail['p_id'])
            ;
            $each_supervisor = $db->fetchAll($select3);

            $details[$key]['each_supervisor'] = $each_supervisor;
        }

        return $details;
    }

    public function updateProposal($data, $where)
    {
        $db = $this->db;

        $db->update('thesis_proposal', $data, $where);
    }

    public function addSupervisor($data)
    {
        $db = $this->db;

        $db->insert('thesis_article_supervisor', $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addSupervisorHistory($data)
    {
        $db = $this->db;

        $db->insert('thesis_supervisor_history', $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisorAll()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("tas" => "thesis_article_supervisor"))
//            ->where('tas.ps_pid = ? ', 5790)
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSupervisorHistory($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("tsh" => "thesis_supervisor_history"), array("num" => "COUNT(*)"))
            ->where('tsh.pid = ? ', $id)
            ;

        $result = $db->fetchRow($select);
        return $result;
    }

    public function studentRegistrationIcemDataUpdate()
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select stud_id student_ID, stud_degree program, stud_intake intake, 
                    branch_id branch, stud_status status
                    from student_profile p where stud_program in (6)
                    and stud_status != 'ACTIVE'
                    --and stud_id = 'CGS01340331'
                    order by stud_program, 1";

        $result = $this->_db->fetchAll($select);

        return $result;

    }

    public function updateRegistration($data, $where)
    {
        $db = $this->db;

        $db->update('tbl_studentregistration', $data, $where);
    }

    public function getProfileStatusId($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("d" => "tbl_definationms"))
            ->where('d.idDefType = ? ', 20)
            ->where('d.DefinitionCode = ? ', $id)
        ;

        $result = $db->fetchRow($select);
        return $result;
    }

}