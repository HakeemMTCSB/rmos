<?php
class Thesis_Form_AddExaminer extends Zend_Form
{
	
	public function init()
	{
		$gstrtranslate = Zend_Registry::get('Zend_Translate');

		//User
		$examiner_userid = new Zend_Form_Element_Select('examiner_userid');
		$examiner_userid->setAttrib('class', 'select reqfield')
					 ->setRequired(true)
					 ->setLabel('Examiner');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$userList = $thesisDB->getExaminer();
		$examiner_userid->addMultiOption('', '-- Select --');
		foreach ($userList as $user)
		{
			$examiner_userid->addMultiOption($user['id'], $user['staff_id'] > 0 ? $user['internal_fullname'] : $user['fullname']);
		}
		
		

		 $examiner_active = new Zend_Form_Element_Checkbox('examiner_active');
		 $examiner_active->setValue(1)->setLabel('Active');

		//Role
		$examiner_role = new Zend_Form_Element_Select('examiner_role');
		$examiner_role->setAttrib('class', 'select reqfield')
					 ->setRequired(true)
					 ->setLabel('Role');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$roles = $thesisDB->getExaminerRole();
		$examiner_role->addMultiOption('', '-- Select --');
		foreach ($roles as $role)
		{
			$examiner_role->addMultiOption($role['exr_id'], $role['code']);
		}
		
		//form elements
        $this->addElements(array(
            $examiner_userid,
			$examiner_role,
			$examiner_active
		));
		
	}
}
?>