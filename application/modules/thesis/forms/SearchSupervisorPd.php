<?php
class Thesis_Form_SearchSupervisorPd extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form_supervisorpd');

        //name
        $this->addElement('text','supervisor_name', array(
            'label'=>'Supervisor Name',
            'class'=>'input-txt'
        ));
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
//		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
          'onclick'=> "window.location=$this->redirect('/thesis/report/pd-report')"
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}