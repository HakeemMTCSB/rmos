<?php
class Thesis_Form_Categories extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//name
        $rc_name = new Zend_Form_Element_Text('rc_name');
		$rc_name->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//program
        $rc_program = new Zend_Form_Element_Select('rc_program');
		$rc_program->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
			
		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		$rc_program->addMultiOption('', '-- Select --');
		foreach ($progList as $progLoop){
			$rc_program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
		}
		
		//name
        $rc_maxcandidate = new Zend_Form_Element_Text('rc_maxcandidate');
		$rc_maxcandidate->setAttrib('class', 'input-txt small')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		$active = new Zend_Form_Element_Checkbox('active');
		$active ->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
	
		//form elements
        $this->addElements(array(
            $rc_name,
			$rc_program,
			$rc_maxcandidate,
			$active
		));
		
	}
}
?>