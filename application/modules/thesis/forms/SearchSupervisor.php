<?php
class Thesis_Form_SearchSupervisor extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form_supervisor');

		$model = new Thesis_Model_DbTable_Registration();
        $generalModel = new Thesis_Model_DbTable_General();

        //Program
        $this->addElement('select','id_program', array(
            'label'=>$this->getView()->translate('Programme'),
            'required'=>false
        ));

        $programDb = new Registration_Model_DbTable_Program();

        $this->id_program->addMultiOption(null,"ALL");
        foreach($programDb->getData()  as $program){
            if($this->_locale=='ms_MY'){
                $this->id_program->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
            }else{
                $this->id_program->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
            }
        }

        //Semester
        $this->addElement('select','IdSemester', array(
            'label'=>$this->getView()->translate('Semester'),
            'required'=>false
        ));

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();

        $this->IdSemester->addMultiOption(null,"ALL");
        foreach($semesterDB->fnGetSemesterList(6) as $semester){
            if($this->_locale=='ms_MY'){
                $this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
            }else{
                $this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
            }
        }

        //Year
        $this->addElement('select','Year', array(
            'label'=>$this->getView()->translate('Year'),
            'required'=>false
        ));

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();

        $this->Year->addMultiOption(null,"ALL");
        foreach($semesterDB->fnGetSemesterYear(6) as $Year){
            if($this->_locale=='ms_MY'){
                $this->Year->addMultiOption($Year["key"],$Year["value"]);
            }else{
                $this->Year->addMultiOption($Year["key"],$Year["value"]);
            }
        }

        //Course
        $this->addElement('select', 'course_id', array(
            'label' => $this->getView()->translate('Course'),
            'required' => false
        ));

        $this->course_id->addMultiOption(null,'ALL');
        $coursesList = $generalModel->getAllCourses();
        foreach ($coursesList as $c)
        {
            $this->course_id->addMultiOption($c['IdSubject'], $c['SubCode'].' - '.$c['SubjectName']);
        }

        //type
        $supervisor_type = new Zend_Form_Element_Radio('supervisor_type');
        $supervisor_type
            ->setAttrib('dojoType',"dijit.form.RadioButton")
            ->setAttrib('required',"true")
            ->removeDecorator("DtDdWrapper")
//            ->setAttrib('id', 'supervisor_type')
            ->setLabel('Type')
            ->addMultiOptions(array('2' => 'All','0' => 'Internal','1' => 'External'))
            ->setValue('2')
            ->setSeparator(' ')
        ;

        $this->addElements(array(
            $supervisor_type

        ));

//        //name
//        $this->addElement('text','supervisor_name', array(
//            'label'=>'Supervisor Name',
//            'class'=>'input-txt'
//        ));
		
//		//personal ID
//		$this->addElement('text','student_id', array(
//			'label'=>'Student ID',
//			'class'=>'input-txt'
//		));
//
//		//registration Status
//		$this->addElement('select','status', array(
//			'label'=>'Registration Status',
//			'class'=>'select'
//		));
//
//		$this->status->addMultiOption(null,"ALL");
//
//		$statusList = $model->getStatus();
//
//		if ($statusList){
//			foreach ($statusList as $status){
//				$this->status->addMultiOption($status['status_id'], $status['status_code']);
//			}
//		}
//
//        //proposal defence Status
//        $this->addElement('select','pd_status', array(
//            'label'=>'Proposal Defence Status',
//            'class'=>'select'
//        ));
//
//        $this->pd_status->addMultiOption(null,"ALL");
//
//        $pdstatusList = $model->getStatus(1);
//
//        if ($pdstatusList){
//            foreach ($pdstatusList as $status){
//                $this->pd_status->addMultiOption($status['status_id'], $status['status_description']);
//            }
//        }

//        //Cluster
//        $this->addElement('select','IdCollege', array(
//            'label'=>$this->getView()->translate('Cluster'),
//            'required'=>false
//        ));
//
//        $clusterDB = new Thesis_Model_DbTable_Registration();
//
//        $this->IdCollege->addMultiOption(null,"ALL");
//        foreach($clusterDB->getClusterList() as $cluster){
//                $this->IdCollege->addMultiOption($cluster["key"],$cluster["value"]);
//        }

		//approval date
		/*$this->addElement('text','approval_date', array(
			'label'=>'Approval Date',
			'class'=>'input-txt'
		));*/

		//Lecturer's
		/*$this->addElement('select','lecturer', array(
			'label'=>'Lecturer',
			'class'=>'select'
		));

		$this->lecturer->addMultiOption(null,"-- Select --");

		$lecturerList = $model->getLecturer();

		if ($lecturerList){
			foreach ($lecturerList as $lecturer){
				$this->lecturer->addMultiOption($lecturer['IdStaff'], $lecturer['FullName']);
			}
		}*/
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
//		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
          'onclick'=> "window.location=$this->redirect('/thesis/report/supervisor-report')"
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}