<?php
class Thesis_Form_Articleship extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate = Zend_Registry::get('Zend_Translate');
		
		
		//semesterid
		$semester_id = new Zend_Form_Element_Hidden('semester_id');
		$semester_id->setAttrib('class', 'hidden')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag')
				 ->setValue($this->_semesterid);
				 
		//subjectid
		$subject_id = new Zend_Form_Element_Hidden('subject_id');
		$subject_id->setAttrib('class', 'hidden')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag')
				 ->setValue($this->_subjectid);

		 //code
        $student_id = new Zend_Form_Element_Hidden('student_id');
		$student_id->setAttrib('class', 'reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');


		//semesterid
		$semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
		$semList = $semDB->fngetSemestermainDetails();

		$semester_id = new Zend_Form_Element_Select('semester_id');
		$semester_id->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
			
		$semester_id->addMultiOption('', '-- Select --');
		foreach ( $semList as $sem )
		{
			$semester_id->addMultiOption($sem['IdSemesterMaster'], $sem['SemesterMainName'] );
		}
		
		//assistance
		$assistance = new Zend_Form_Element_Radio('assistance');
		$assistance->addMultiOptions(array(
			0 => 'No',
			1 => 'Yes'
		  ))->setSeparator(' ')
		  ->removeDecorator("DtDdWrapper")
		  ->removeDecorator("Label")
		  ->removeDecorator('HtmlTag')
		  ->setValue(0);
		

		//others
		$fieldofinterest_others = new Zend_Form_Element_Text('fieldofinterest_others');
		$fieldofinterest_others->setAttrib('class', 'input-txt')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		//company
		$company = new Zend_Form_Element_Text('company');
		$company->setAttrib('class', 'input-txt')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//designation
		$designation = new Zend_Form_Element_Text('designation');
		$designation->setAttrib('class', 'input-txt')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//file
		$uploadfile = new Zend_Form_Element_File('uploadfile');
		$uploadfile->setRequired(false)
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		//contactperson
        $contactperson = new Zend_Form_Element_Text('contactperson');
		$contactperson->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//Email
        $email = new Zend_Form_Element_Text('email');
		$email->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		//address
        $address = new Zend_Form_Element_Text('address');
		$address->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//address
        $address = new Zend_Form_Element_Text('address');
		$address->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//phoneno
        $phoneno = new Zend_Form_Element_Text('phoneno');
		$phoneno->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//faxno
        $faxno = new Zend_Form_Element_Text('faxno');
		$faxno->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//postcode
        $postcode = new Zend_Form_Element_Text('postcode');
		$postcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		 //city
		$city = new Zend_Form_Element_Select('city');
		$city->setRegisterInArrayValidator(false);
		$city->removeDecorator("DtDdWrapper");
		$city->setAttrib('class', 'select');
		$city->removeDecorator("Label");
        
        $cityModel = new GeneralSetup_Model_DbTable_City();
        $cityList = $cityModel->fetchAll();
        
        $city->addMultiOption('', '-- Select --');
        
        if (count($cityList) > 0){
            foreach ($cityList as $cityLoop){
                $city->addMultiOption($cityLoop['idCity'], $cityLoop['CityName']);
            }
        }
        
		//country
		$country = new Zend_Form_Element_Select('country');
		$country->removeDecorator("DtDdWrapper");
		$country->setAttrib('class', 'select');
		$country->removeDecorator("Label");
		$country->setAttrib('onchange', 'getState(this.value, "state");');

		$country->addMultiOption('', '-- Select --');
		$countryModel = new GeneralSetup_Model_DbTable_Countrymaster();
        $countryList = $countryModel->fetchAll();
        
		if (count($countryList) > 0){
			foreach ($countryList as $countryLoop){
				$country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
			}
		}

		//state
		$state = new Zend_Form_Element_Select('state');
		$state->removeDecorator("DtDdWrapper");
		$state->setAttrib('class', 'select');
		$state->removeDecorator("Label");
		$state->setAttrib('onchange', 'getCity(this.value, "city");');

		$stateModel = new GeneralSetup_Model_DbTable_State();
		$stateList = $stateModel->fetchAll();

		$state->addMultiOption('', '-- Select --');

		if (count($stateList) > 0){
			foreach ($stateList as $stateLoop){
				$state->addMultiOption($stateLoop['idState'], $stateLoop['StateName']);
			}
		}

		//isemployee
		$isemployee = new Zend_Form_Element_Radio('isemployee');
		$isemployee->addMultiOptions(array(
			0 => 'No',
			1 => 'Yes'
		  ))->setSeparator(' ')
		  ->removeDecorator("DtDdWrapper")
		  ->removeDecorator("Label")
		  ->removeDecorator('HtmlTag')
		  ->setValue(0);

		//yearsofservice
		$yearsofservice = new Zend_Form_Element_Radio('yearsofservice');
		$yearsofservice->addMultiOptions(array(
			1 => 'Below 1',
			2 => '1-4',
			3 => '5-10',
			4 => 'Above 10'

		  ))->setSeparator(' ')
		  ->removeDecorator("DtDdWrapper")
		  ->removeDecorator("Label")
		  ->removeDecorator('HtmlTag');

		 //start date
        $start_date = new Zend_Form_Element_Text('start_date');
		$start_date->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		 //end date
        $end_date = new Zend_Form_Element_Text('end_date');
		$end_date->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		//remarks
	    $remarks = new Zend_Form_Element_Textarea('remarks');
		$remarks->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		//archive
	    $archive = new Zend_Form_Element_Checkbox('archive');
		$archive->setAttrib('class', 'chk')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


		//form elements
        $this->addElements(array(
            $student_id,
			$semester_id,
			$fieldofinterest_others,

			$designation,
			$contactperson,
			$company,
			$address,
			$email,
			$country,
			$state,
			$city,
			$phoneno,
			$faxno,
			$postcode,
			$isemployee,
			$yearsofservice,
			
			$assistance,
			$uploadfile,

			$start_date,
			$end_date,
			$remarks,

			$archive
			
			
		));
		
	}
}
?>