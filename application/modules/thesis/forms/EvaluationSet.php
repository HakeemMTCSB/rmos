<?php
class Thesis_Form_EvaluationSet extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//name
        $name = new Zend_Form_Element_Text('name');
		$name->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		 //description
        $description = new Zend_Form_Element_Textarea('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
		
		//evaluation_type
		$evaluation_type = new Zend_Form_Element_Radio('evaluation_type');
		$evaluation_type->addMultiOptions(array(
			0 => 'Marks',
			1 => 'Rating'
		  ))->setSeparator(' ')
		  ->removeDecorator("DtDdWrapper")
		  ->removeDecorator("Label")
		  ->removeDecorator('HtmlTag')
		  ->setValue(0)
		  ->setRequired(true);
		
		//rating_min
        $rating_min = new Zend_Form_Element_Text('rating_min');
		$rating_min->setAttrib('class', 'input-txt small reqfield2')
				->setAttrib('maxlength',2)
				->setValue(1)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//rating_min label
        $rating_min_label = new Zend_Form_Element_Text('rating_min_label');
		$rating_min_label->setAttrib('class', 'input-txt reqfield2')
				//->setAttrib('maxlength',2)
				->setValue('Not appropriate at all')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//rating_max
        $rating_max = new Zend_Form_Element_Text('rating_max');
		$rating_max->setAttrib('class', 'input-txt small reqfield2')
				->setAttrib('maxlength',2)
				->setValue(7)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//rating_max label
        $rating_max_label = new Zend_Form_Element_Text('rating_max_label');
		$rating_max_label->setAttrib('class', 'input-txt reqfield2')
				//->setAttrib('maxlength',2)
				->setValue('Most appropriate')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//courses
        $subject_id = new Zend_Form_Element_Select('subject_id');
		$subject_id->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$coursesList = $thesisDB->getCourses();
		foreach ($coursesList as $c)
		{
			$subject_id->addMultiOption($c['IdSubject'], $c['SubCode'].' - '.$c['SubjectName']);
		}
		

		//form elements
        $this->addElements(array(
			$description,
			$name,
			$evaluation_type,
			$rating_min_label,
			$rating_min,
			$rating_max,
			$rating_max_label,
			$subject_id

		));
		
	}
}
?>