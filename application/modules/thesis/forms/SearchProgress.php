<?php
class Thesis_Form_SearchProgress extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form_progress');

        //Program
        $this->addElement('select','id_program', array(
            'label'=>$this->getView()->translate('Programme'),
            'required'=>false
        ));

        $programDb = new Registration_Model_DbTable_Program();

        $this->id_program->addMultiOption(null,"ALL");
        foreach($programDb->getData()  as $program){
            if($this->_locale=='ms_MY'){
                $this->id_program->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
            }else{
                $this->id_program->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
            }
        }

        //Order
        $this->addElement('select','OrderBy', array(
            'label'=>$this->getView()->translate('Order By'),
            'required'=>false
        ));

        $this->OrderBy->addMultiOption(0,"ALL");
        $this->OrderBy->addMultiOption(1,"BY PROGRAMME");
        $this->OrderBy->addMultiOption(2,"BY SUPERVISOR");

        //Semester
        $this->addElement('select','IdSemester', array(
            'label'=>$this->getView()->translate('Semester'),
            'required'=>false
        ));

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();

        $this->IdSemester->addMultiOption(null,"ALL");
        foreach($semesterDB->fnGetSemesterList(6) as $semester){
            if($this->_locale=='ms_MY'){
                $this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
            }else{
                $this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
            }
        }

        //Year
        $this->addElement('select','Year', array(
            'label'=>$this->getView()->translate('Year'),
            'required'=>false
        ));

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();

        $this->Year->addMultiOption(null,"ALL");
        foreach($semesterDB->fnGetSemesterYear(6) as $Year){
            if($this->_locale=='ms_MY'){
                $this->Year->addMultiOption($Year["key"],$Year["value"]);
            }else{
                $this->Year->addMultiOption($Year["key"],$Year["value"]);
            }
        }
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
//		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
          'onclick'=> "window.location=$this->redirect('/thesis/report/progress-report')"
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}