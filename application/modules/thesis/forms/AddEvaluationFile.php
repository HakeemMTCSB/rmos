<?php
class Thesis_Form_AddEvaluationFile extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_addfile');
//		$this->setAttrib('onsubmit','return checkform_addfile()');
		
		$this->addElement('text','description',
			array(
				'label'=>'Description',
				'required'=> true,
				'class' => 'input-txt reqfield'
			)
		);

		$this->addElement('select','type_id', array(
			'label'=>'Type',
			'id'=>'type_id',
			'required'=> true,
			'class' => 'select reqfield'
		));

		$this->type_id->addMultiOption(null,"-- Select --");

		$definationDB = new App_Model_General_DbTable_Definationms();
		$statusList = $definationDB->getByCode('Thesis Files Type');

		if (count($statusList)>0){
			foreach ($statusList as $statusLoop){
				$this->type_id->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
			}
		}


		$this->addElement('file','uploadfile',
			array(
				'label'=>'File',
				'required'=> true,
				'class' => 'reqfield'
			)
		);

        $this->addElement (
            'multiCheckbox', 'visibility',
            array (
                'label'=>'Visibility',
//                'value' => array('11.com', '3.com'), // select these 2 values
                'multiOptions' => array(
                    '1' => 'Student',
                    '2' => 'Supervisor',
                    '3' => 'Program Director'
                )
            )
        );
		
	}
}
?>