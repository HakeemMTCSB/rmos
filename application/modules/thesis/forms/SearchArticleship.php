<?php
class Thesis_Form_SearchArticleship extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');

		//name
		$this->addElement('text','student_name', array(
			'label'=>'Student Name',
			'class'=>'input-txt'
		));
		
		//personal ID
		$this->addElement('text','student_id', array(
			'label'=>'Student ID',
			'class'=>'input-txt'
		));
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>