<?php
class Thesis_Form_SearchProposalDefense extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form_lala');

        //faculty
        $facultyDb = new App_Model_General_DbTable_Collegemaster();
        $this->addElement('select','faculty_id', array(
            'label'=>'Faculty',
            'required'=>false
        ));

        $this->faculty_id->addMultiOption(null,"-- Any --");
        foreach($facultyDb->getFaculty()  as $faculty){
                $this->faculty_id->addMultiOption($faculty["IdCollege"],$faculty["CollegeName"]);
        }

        //type
        $eventDB = new Thesis_Model_DbTable_Event();
        $this->addElement('select','event_type', array(
            'label'=>'Type',
            'required'=>false
        ));

        $this->event_type->addMultiOption(null,"-- Any --");
        foreach($eventDB->selectType()  as $type){
            $this->event_type->addMultiOption($type["idDefinition"],$type["DefinitionDesc"]);
        }

//        //event_type
//        $event_type = new Zend_Form_Element_Select('event_type');
//        $event_type->setAttrib('class', 'select reqfield')
//            ->setRequired(true)
//            ->removeDecorator("DtDdWrapper")
//            ->removeDecorator("Label")
//            ->removeDecorator('HtmlTag');
//
//        $eventDB = new Thesis_Model_DbTable_Event();
//        $typeList = $eventDB->selectType();
//        $event_type->addMultiOption('', '-- Select --');
//        foreach ($typeList as $c) {
//            $event_type->addMultiOption($c['idDefinition'], $c['DefinitionDesc']);
//        }

        //start date
        $this->addElement('text','start_date', array(
            'label'=>'Start Date',
            'class'=>'input-txt'
        ));

        //end date
        $this->addElement('text','end_date', array(
            'label'=>'End Date',
            'class'=>'input-txt'
        ));
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}