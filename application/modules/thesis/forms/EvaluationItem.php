<?php
class Thesis_Form_EvaluationItem extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//name
        $name = new Zend_Form_Element_Text('name');
		$name->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//description
        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		
		//form elements
        $this->addElements(array(
			$description,
			$name
		));
		
	}
}
?>