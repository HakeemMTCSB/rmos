<?php
class Thesis_Form_ExaminerSetup extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//es_min
        $es_min = new Zend_Form_Element_Text('es_min');
		$es_min->setAttrib('class', 'input-txt small reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//es_max
        $es_max = new Zend_Form_Element_Text('es_max');
		$es_max->setAttrib('class', 'input-txt small reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//program
        $es_program = new Zend_Form_Element_Select('es_program');
		$es_program->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
			
		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		$es_program->addMultiOption('0', 'All');
		foreach ($progList as $progLoop){
			$es_program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
		}
		
		//courses
        $es_course = new Zend_Form_Element_Select('es_course');
		$es_course->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$coursesList = $thesisDB->getCourses();
		$es_course->addMultiOption('0', 'All');
		foreach ($coursesList as $course)
		{
			$es_course->addMultiOption($course['IdSubject'], $course['SubCode'].' - '.$course['SubjectName']);
		}
		
		//form elements
        $this->addElements(array(
            $es_min,
			$es_max,
			$es_program,
			$es_course
		));
		
	}
}
?>