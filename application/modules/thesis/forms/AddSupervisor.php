<?php
class Thesis_Form_AddSupervisor extends Zend_Form
{
	
	public function init()
	{
		$gstrtranslate = Zend_Registry::get('Zend_Translate');
        $this->setName('Supervisor');

//		//User
//		$supervisor_userid = new Zend_Form_Element_Select('supervisor_userid');
//		$supervisor_userid->setAttrib('class', 'select reqfield')
//					 ->setRequired(true)
//					 ->setLabel('Name');
//
//		$thesisDB = new Thesis_Model_DbTable_General();
//		$userList = $thesisDB->getAvailableSupervisors();
//		$supervisor_userid->addMultiOption('', '-- Select --');
//		foreach ($userList as $user)
//		{
//			$supervisor_userid->addMultiOption($user['iduser'], $user['FullName']);
//		}

        //supervisor_name
        $supervisor_name = new Zend_Form_Element_Text('supervisor_name');
        $supervisor_name->setAttrib('onchange','searchSupervisor()')
            ->setAttrib('class', 'input-txt reqfield')
            ->setRequired(true)
            ->setLabel('Name');

        //supervisor_userid
        $supervisor_userid = new Zend_Form_Element_Hidden('supervisor_userid');
        $supervisor_userid->setAttrib('class', 'reqfield')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');
		
		//Role
        $supervisor_role = new Zend_Form_Element_Select('supervisor_role');
        $supervisor_role->setAttrib('class', 'select reqfield')
            ->setRequired(true)
            ->setLabel('Role');

        $thesisDB = new Thesis_Model_DbTable_General();
        $roles = $thesisDB->getSupervisor();
        $supervisor_role->addMultiOption('', '-- Select --');
        foreach ($roles as $role)
        {
            $supervisor_role->addMultiOption($role['supervisor_id'], $role['code']);
        }

        //type
        $supervisor_type = new Zend_Form_Element_Radio('supervisor_type');
        $supervisor_type
            ->setAttrib('dojoType',"dijit.form.RadioButton")
            ->setAttrib('required',"true")
            ->removeDecorator("DtDdWrapper")
//            ->setAttrib('id', 'supervisor_type')
            ->setLabel('Type')
            ->addMultiOptions(array('0' => 'Internal','1' => 'External'))
//            ->setMultiOptions(array('0' => 'Internal','1' => 'External'))
            ->setValue('0')
            ->setSeparator('')
            ;

        //active
		 $supervisor_active = new Zend_Form_Element_Checkbox('supervisor_active');
		 $supervisor_active->setValue(1)->setLabel('Active');

//        //reason
//        $reason = new Zend_Form_Element_Text('reason');
//        $reason->setAttrib('class', 'input-txt')
//            ->setRequired(false)
//            ->setLabel('Reason');

        //reason
        $reason = new Zend_Form_Element_Text('reason');
        $reason->setAttrib('class', 'input-txt reqfield')
            ->setRequired(true)
            ->setLabel('Reason');
		
		//form elements
        $this->addElements(array(
            $supervisor_name,
            $supervisor_userid,
			$supervisor_role,
            $reason,
            $supervisor_active,
            $supervisor_type

		));
		
	}
}
?>