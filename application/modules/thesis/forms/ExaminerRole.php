<?php
class Thesis_Form_ExaminerRole extends Zend_Form
{
	
	public function init()
	{
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//description
        $code = new Zend_Form_Element_Text('code');
		$code->setAttrib('class', 'input-txt initcap')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//description
        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');


		
		//form elements
        $this->addElements(array(
            $code,
			$description,
		));
		
	}
}
?>