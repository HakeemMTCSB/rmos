<?php
class Thesis_Form_Interest extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		 //code
        $i_title = new Zend_Form_Element_Text('i_title');
		$i_title->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		 //description
        $i_description = new Zend_Form_Element_Text('i_description');
		$i_description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
		
		//form elements
        $this->addElements(array(
            $i_title,
			$i_description
		));
		
	}
}
?>