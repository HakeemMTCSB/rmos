<?php
class Thesis_Form_Grade extends Zend_Form
{
    public function init()
    {
        //grade
        $grade = new Zend_Form_Element_Radio('grade_viva');
        $grade
            ->setAttrib('class', 'chk1')
            ->setRequired(true)
            ->removeDecorator("Label")
        ;

        $registryDB = new Thesis_Model_DbTable_General();
        $gradeList = $registryDB->selectRegistry('grade-awarded');

        foreach ($gradeList as $a) {
            $grade->addMultiOption($a['id'], $a['name']);
        }

        //correction
        $correction = new Zend_Form_Element_Radio('correction_viva');
        $correction
            ->setAttrib('class', 'chk2')
            ->setRequired(true)
            ->removeDecorator("Label")
        ;

        $registryDB = new Thesis_Model_DbTable_General();
        $correctionList = $registryDB->selectRegistry('submit-correction');

        foreach ($correctionList as $a) {
            $correction->addMultiOption($a['id'], $a['name']);
        }

        //accept
        $accept = new Zend_Form_Element_Radio('accept_viva');
        $accept
            ->setAttrib('class', 'chk3')
            ->setRequired(true)
            ->removeDecorator("Label")
        ;

        $registryDB = new Thesis_Model_DbTable_General();
        $acceptList = $registryDB->selectRegistry('thesis-acception');

        foreach ($acceptList as $a) {
            $accept->addMultiOption($a['id'], $a['name']);
        }
        //User
        $examiner_userid = new Zend_Form_Element_Select('examiner_viva');
        $examiner_userid
            ->setAttrib('class', 'select reqfield')
            ->setAttrib('onChange','getExaminerId(this)')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $thesisDB = new Thesis_Model_DbTable_General();
        $userList = $thesisDB->getExaminer();
        $examiner_userid->addMultiOption('', '-- Select --');
        foreach ($userList as $user)
        {
            $examiner_userid->addMultiOption($user['id'], $user['staff_id'] > 0 ? $user['internal_fullname'] : $user['fullname']);
        }

        //institution
        $institution = new Zend_Form_Element_Text('institution');
        $institution->setAttrib('class', 'input-txt')
            ->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array(

            $grade,
            $correction,
            $accept,
            $examiner_userid,
            $institution

        ));

    }
}
?>