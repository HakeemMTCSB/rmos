<?php
class Thesis_Form_Status extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		 //code
        $code = new Zend_Form_Element_Text('code');
		$code->setAttrib('class', 'input-txt reqfield initcap')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		 //description
        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//program
        $program = new Zend_Form_Element_Select('program');
		$program->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
			
		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		$program->addMultiOption('0', 'All');
		foreach ($progList as $progLoop){
			$program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
		}
		
		//courses
        $course = new Zend_Form_Element_Select('course');
		$course->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$coursesList = $thesisDB->getCourses();
		$course->addMultiOption('0', 'All');
		foreach ($coursesList as $c)
		{
			$course->addMultiOption($c['IdSubject'], $c['SubCode'].' - '.$c['SubjectName']);
		}
		
		//form elements
        $this->addElements(array(
            $code,
			$description,
			$program,
			$course
		));
		
	}
}
?>