<?php
class Application_Form_SearchApplicant extends Zend_Form
{
	protected $_facultyid;
	
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');

		
		//semester_id
		$this->addElement('select','semester_id', array(
			'label'=>'Semester'
		));
		
		//name
		$this->addElement('text','name', array(
			'label'=>'Name',
			'class'=>'input-txt'
		));
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>