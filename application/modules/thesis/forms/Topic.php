<?php
class Thesis_Form_Topic extends Zend_Form
{
	
	public function init()
	{
		/*
		r_id
		r_topicname
		r_program
		r_supervisor
		r_maxcandidate
		r_assignmentrequirement
		r_createddate
		r_createdby
		r_updatedby
		r_updateddate*/
		
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		 //topic name
        $r_topicname = new Zend_Form_Element_Text('r_topicname');
		$r_topicname->setAttrib('class', 'input-txt reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//program
        /*$r_program = new Zend_Form_Element_Select('r_program');
		$r_program->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
			
		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		$r_program->addMultiOption('', '-- Select --');
		foreach ($progList as $progLoop){
			$r_program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
		}*/
		$progModel = new Application_Model_DbTable_ProgramScheme();
		$progList = $progModel->fnGetProgramList();

		//category
		$r_category = new Zend_Form_Element_Select('r_category');
		$r_category->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
			
		$thesisModel = new Thesis_Model_DbTable_General();
        $catList = $thesisModel->getCategory();
		$catListByProg = array();
		foreach ($catList as $cat)
		{
			$catListByProg[$cat['rc_program']][$cat['rc_id']] = $cat['rc_name']; 
		}

		$r_category->addMultiOption('', '-- Select --');
		foreach ( $progList as $prog )
		{
			if ( isset($catListByProg[$prog['IdProgram']]) && !empty($catListByProg[$prog['IdProgram']]) )
			{
				$r_category->addMultiOption($prog['ProgramName'], $catListByProg[$prog['IdProgram']] );
			}
		}
		
		//supervisor
        $r_supervisor = new Zend_Form_Element_Select('r_supervisor');
		$r_supervisor->setAttrib('class', 'select reqfield')
					 ->setRequired(true)
					 ->removeDecorator("DtDdWrapper")
				     ->removeDecorator("Label")
				     ->removeDecorator('HtmlTag');
		
		$userDB = new GeneralSetup_Model_DbTable_User();
		$userList = $userDB->fngetUserDetails();
		$r_supervisor->addMultiOption('', '-- Select --');
		foreach ($userList as $user)
		{
			$r_supervisor->addMultiOption($user['iduser'], $user['FullName']);
		}

		//maxcandidate
        $r_maxcandidate = new Zend_Form_Element_Text('r_maxcandidate');
		$r_maxcandidate->setAttrib('class', 'input-txt small')
					->setAttrib('maxlength','3')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//assignmentrequirement
        $r_assignmentrequirement = new Zend_Form_Element_Textarea('r_assignmentrequirement');
		$r_assignmentrequirement->setAttrib('class', '')
								->removeDecorator("DtDdWrapper")
								->removeDecorator("Label")
								->removeDecorator('HtmlTag');
		
		$active = new Zend_Form_Element_Checkbox('active');
		$active ->setRequired(true)
				->setValue(1)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//form elements
        $this->addElements(array(
            $r_topicname,
			$r_category,
			$r_maxcandidate,
			$r_assignmentrequirement,
			$active
		));
		
	}
}
?>