<?php
class Thesis_Form_Activity extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//name
        $title = new Zend_Form_Element_Text('title');
		$title->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//description
        $description = new Zend_Form_Element_Textarea('description');
		$description->setAttrib('class', 'textarea')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//research_type
        $research_type = new Zend_Form_Element_Hidden('research_type');
		$research_type->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//form elements
        $this->addElements(array(
            $title,
			$description,
			$research_type
		));
		
	}
}
?>