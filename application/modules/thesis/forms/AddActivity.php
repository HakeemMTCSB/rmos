<?php

class Thesis_Form_AddActivity extends Zend_Form
{
    protected $full = false;

    public function setFull($full)
    {
        $this->full = $full;
    }

    public function init()
    {

        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $course = new Zend_Form_Element_Select('ra_course');
        $course->setAttrib('class', 'select reqfield')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
        $courseList = $subjectDB->searchSubject();
        $course->addMultiOption('', 'Please Select');
        foreach($courseList as $subject){
            $course->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
        }

        $model = new Thesis_Model_DbTable_Registration();

        //type
        $type = new Zend_Form_Element_Select('type');
        $type->setAttrib('class', 'select reqfield')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $eventDB = new Thesis_Model_DbTable_Event();
        $typeList = $eventDB->selectType();
        $type->addMultiOption('', 'Please Select');
        foreach ($typeList as $c) {
            $type->addMultiOption($c['idDefinition'], $c['DefinitionDesc']);
        }

        $activity = new Zend_Form_Element_MultiCheckbox('rat_type');
        $activity
            ->setAttrib('class', 'chk')
            ->setRequired(true)
            ->removeDecorator("Label")
        ;

        $eventDB = new Thesis_Model_DbTable_Event();
        $activityList = $eventDB->selectType();

        foreach ($activityList as $a) {
            $activity->addMultiOption($a['idDefinition'], $a['DefinitionDesc']);
        }

        //form elements
        $this->addElements(array(

            $course,
            $type,
            $activity

        ));

    }
}