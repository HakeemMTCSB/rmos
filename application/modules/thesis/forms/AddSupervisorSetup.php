<?php
class Thesis_Form_AddSupervisorSetup extends Zend_Form
{
	
	public function init()
	{
		$gstrtranslate = Zend_Registry::get('Zend_Translate');

		//User
		$supervisor_userid = new Zend_Form_Element_Select('supervisor_userid');
		$supervisor_userid->setAttrib('class', 'select reqfield')
					 ->setRequired(true)
					 ->setLabel('Supervisor');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$userList = $thesisDB->getAvailableSupervisors();
		$supervisor_userid->addMultiOption('', '-- Select --');
		foreach ($userList as $user)
		{
			$supervisor_userid->addMultiOption($user['iduser'], $user['FullName']);
		}
		
		
		//form elements
        $this->addElements(array(
            $supervisor_userid
		));
		
	}
}
?>