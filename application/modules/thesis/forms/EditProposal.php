<?php

class Thesis_Form_EditProposal extends Zend_Form
{
    protected $full = false;

    public function setFull($full)
    {
        $this->full = $full;
    }

    public function init()
    {

        $gstrtranslate = Zend_Registry::get('Zend_Translate');
        $model = new Thesis_Model_DbTable_Registration();

        $p_title = new Zend_Form_Element_Text('p_title');
        $p_title->setAttrib('class', 'input-txt reqfield')
            //->setAttrib('style','width:500px')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //code
        $student_id = new Zend_Form_Element_Hidden('student_id');
        $student_id->setAttrib('class', 'reqfield')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //p_category

        $progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();

        //category
        $p_category = new Zend_Form_Element_Select('p_category');
        $p_category->setAttrib('class', 'select')
            ->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $thesisModel = new Thesis_Model_DbTable_General();
        $catList = $thesisModel->getCategory(null, $this->full);
        $catListByProg = array();
        foreach ($catList as $cat) {
            $catListByProg[$cat['rc_program']][$cat['rc_id']] = $cat['rc_name'];
        }

        $p_category->addMultiOption('', '-- Select --');
        foreach ($progList as $prog) {
            if (isset($catListByProg[$prog['IdProgram']]) && !empty($catListByProg[$prog['IdProgram']])) {
                $p_category->addMultiOption($prog['ProgramName'], $catListByProg[$prog['IdProgram']]);
            }
        }

        //p_topic
        $p_topic = new Zend_Form_Element_Select('p_topic');
        $p_topic->setAttrib('class', 'select')
            ->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $topicList = $thesisModel->getTopics($this->full);

        $p_topic->addMultiOption('', '-- Select --');
        foreach ($topicList as $topic) {
            $p_topic->addMultiOption($topic['r_id'], $topic['r_topicname']);
        }

        //p_description
        $p_description = new Zend_Form_Element_Textarea('p_description');
        $p_description->setAttrib('class', 'textarea')
            ->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //file
        $uploadfile = new Zend_Form_Element_File('uploadfile');
        $uploadfile->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //archive
        $archive = new Zend_Form_Element_Checkbox('archive');
        $archive->setAttrib('class', 'chk')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //meeting date
        $meeting_date = new Zend_Form_Element_Text('meeting_date');
        $meeting_date->setAttrib('class', 'input-txt half datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //completion_date
        $completion_date = new Zend_Form_Element_Text('completion_date');
        $completion_date->setAttrib('class', 'input-txt half datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //semester_start
        $semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
        $semList = $semDB->fngetSemestermainDetails('', '');

        $semester_start = new Zend_Form_Element_Select('semester_start');
        $semester_start->setAttrib('class', 'select')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $semester_start->addMultiOption('', '-- Select --');
        foreach ($semList as $sem) {
            $semester_start->addMultiOption($sem['IdSemesterMaster'], $sem['SemesterMainCode'].' - '.$sem['SemesterMainName'].' ('.$sem['SchemeName'].')');
        }

        $semester_end = new Zend_Form_Element_Select('semester_end');
        $semester_end->setAttrib('class', 'select')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $semester_end->addMultiOption('', '-- Select --');
        foreach ($semList as $sem) {
            $semester_end->addMultiOption($sem['IdSemesterMaster'], $sem['SemesterMainCode']);
        }

        //rmos
        $course_id = new Zend_Form_Element_Select('course_id');
        $course_id
            ->setAttrib('class', 'select reqfield')
            ->setAttrib('onChange','getCourseId(this)')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $thesisDB = new Thesis_Model_DbTable_General();
        $coursesList = $thesisDB->getProposalCourses();
        $course_id->addMultiOption('', '-- Select --');
        foreach ($coursesList as $c) {
            $course_id->addMultiOption($c['IdSubject'], $c['SubCode'] . ' - ' . $c['SubjectName']);
        }

        //rmos
        $activity_id = new Zend_Form_Element_Select('activity_id');
        $activity_id->setAttrib('class', 'select reqfield')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $activity_id->setRegisterInArrayValidator(false);

        $activity_id->addMultiOption('', '-- Select --');

        //registration Status

        $reg_status = new Zend_Form_Element_Select('p_status');
        $reg_status->setAttrib('class', 'select reqfield')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $statusList = $model->getStatus();
        foreach ($statusList as $status) {
            $reg_status->addMultiOption($status['status_id'], $status['status_description']);
        }

        //proposal defence Status

        $pd_status = new Zend_Form_Element_Select('pd_status');
        $pd_status->setAttrib('class', 'select')
            ->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $pdstatusList = $model->getStatus(1);
        $pd_status->addMultiOption('', '-- Select --');
        foreach ($pdstatusList as $status) {
            $pd_status->addMultiOption($status['status_id'], $status['status_description']);
        }

        //applied date
        $applied_date = new Zend_Form_Element_Text('applied_date');
        $applied_date->setAttrib('class', 'input-txt half datepicker')
            ->setAttrib('onChange','getSemester(this)')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //form elements
        $this->addElements(array(
            $p_title,
            $student_id,
            $uploadfile,
            $p_category,
            $p_description,
            $p_topic,
            $archive,
            $meeting_date,
            $completion_date,
            $semester_start,
            $semester_end,
            $course_id,
            $applied_date,
            $reg_status,
            $activity_id,
            $pd_status

        ));

    }
}