<?php
class Thesis_Form_Colloquium extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//name
        $name = new Zend_Form_Element_Text('name');
		$name->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//description
        $description = new Zend_Form_Element_Textarea('description');
		$description->setAttrib('class', 'textarea')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
			
		//url
		$url = new Zend_Form_Element_Text('url');
		$url->setAttrib('class', 'input-txt')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//date_from
		$date_from = new Zend_Form_Element_Text('date_from');
		$date_from->setAttrib('class', 'input-txt datepicker half reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//date_from
		$date_to = new Zend_Form_Element_Text('date_to');
		$date_to->setAttrib('class', 'input-txt datepicker half reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//form elements
        $this->addElements(array(
            $name,
			$description,
			$url,
			$date_from,
			$date_to
		));
		
	}
}
?>