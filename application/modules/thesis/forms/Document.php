<?php
class Thesis_Form_Document extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		//title
        $title = new Zend_Form_Element_Text('title');
		$title->setAttrib('class', 'input-txt reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		 //description
        $description = new Zend_Form_Element_Textarea('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
		
		//courses
        $subject_id = new Zend_Form_Element_Select('subject_id');
		$subject_id->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$coursesList = $thesisDB->getCourses();
		foreach ($coursesList as $c)
		{
			$subject_id->addMultiOption($c['IdSubject'], $c['SubCode'].' - '.$c['SubjectName']);
		}

		//research_type
        $research_type = new Zend_Form_Element_Select('research_type');
		$research_type->setAttrib('class', 'select reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$research_type->addMultiOption('', '-- Select --');
		$research_type->addMultiOption('supervisor', 'Supervisor');
		$research_type->addMultiOption('student', 'Student');

		
		

		//form elements
        $this->addElements(array(
			$description,
			$title,
			$subject_id,
			$research_type

		));
		
	}
}
?>