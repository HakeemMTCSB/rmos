<?php
class Thesis_Form_SearchActivity extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');

        $course = new Zend_Form_Element_Select('course');
        $course
            ->setLabel('Course')
            ->setAttrib('class', 'select')
        ;

        $subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
        $courseList = $subjectDB->searchSubject();
        $course->addMultiOption('', 'Please Select');
        foreach($courseList as $subject){
            $course->addMultiOption($subject["IdSubject"],$subject['SubCode'].' - '.$subject["SubjectName"]);
        }

        $this->addElements(array($course));
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>