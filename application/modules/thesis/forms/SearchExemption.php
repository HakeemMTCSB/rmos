<?php
class Thesis_Form_SearchExemption extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form_lala');

		$model = new Thesis_Model_DbTable_Registration();

		//name
		$this->addElement('text','student_name', array(
			'label'=>'Student Name',
			'class'=>'input-txt'
		));
		
		//personal ID
		$this->addElement('text','student_id', array(
			'label'=>'Student ID',
			'class'=>'input-txt'
		));

		//Status
		$this->addElement('select','status', array(
			'label'=>'Status',
			'class'=>'select'
		));

		$this->status->addMultiOption(null,"-- Select --");

		$statusList = $model->getStatus();

		if ($statusList){
			foreach ($statusList as $status){
				$this->status->addMultiOption($status['status_id'], $status['status_code']);
			}
		}

		//approval date
		$this->addElement('text','approval_date', array(
			'label'=>'Approval Date',
			'class'=>'input-txt'
		));

		//Lecturer's
		$this->addElement('select','lecturer', array(
			'label'=>'Lecturer',
			'class'=>'select'
		));

		$this->lecturer->addMultiOption(null,"-- Select --");

		$lecturerList = $model->getLecturer();

		if ($lecturerList){
			foreach ($lecturerList as $lecturer){
				$this->lecturer->addMultiOption($lecturer['IdStaff'], $lecturer['FullName']);
			}
		}
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>