<?php
class Thesis_Form_Exemption extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate = Zend_Registry::get('Zend_Translate');

		 //code
        $student_id = new Zend_Form_Element_Hidden('student_id');
		$student_id->setAttrib('class', 'reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');


		
		
		//reason
		$reason = new Zend_Form_Element_Textarea('reason');
		$reason->setAttrib('class', 'textarea reqfield large')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//proposedarea
		$proposedarea = new Zend_Form_Element_Textarea('proposedarea');
		$proposedarea->setAttrib('class', 'textarea reqfield large')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//file
		$uploadfile = new Zend_Form_Element_File('uploadfile');
		$uploadfile->setRequired(false)
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		 //start date
        $start_date = new Zend_Form_Element_Text('start_date');
		$start_date->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		 //end date
        $end_date = new Zend_Form_Element_Text('end_date');
		$end_date->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		//remarks
	    $remarks = new Zend_Form_Element_Textarea('remarks');
		$remarks->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		//archive
	    $archive = new Zend_Form_Element_Checkbox('archive');
		$archive->setAttrib('class', 'chk')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		//form elements
        $this->addElements(array(
            $student_id,
			$reason,
			$proposedarea,
			$uploadfile,
			$start_date,
			$end_date,
			$remarks,
			$archive
			
		));
		
	}
}
?>