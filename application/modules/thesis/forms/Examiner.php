<?php
class Thesis_Form_Examiner extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		
		
		
		/*
		//semester_id
		$semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
		$semList = $semDB->fngetSemestermainDetails();

		$semester_id = new Zend_Form_Element_Select('semester_id');
		$semester_id->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
			
		$semester_id->addMultiOption('', '-- Select --');
		foreach ( $semList as $sem )
		{
			$semester_id->addMultiOption($sem['IdSemesterMaster'], $sem['SemesterMainName'] );
		}*/
		
		//examiner_type
		$examiner_type = new Zend_Form_Element_Radio('examiner_type');
		$examiner_type->addMultiOptions(array(
			0 => 'Internal',
			1 => 'External'
		  ))->setSeparator(' ')
		  ->removeDecorator("DtDdWrapper")
		  ->removeDecorator("Label")
		  ->removeDecorator('HtmlTag')
		  ->setValue(0)
		  ->setRequired(true);

		
		 //contact_no
        $contact_no = new Zend_Form_Element_Text('contact_no');
		$contact_no->setAttrib('class', 'input-txt')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

        //institution
        $institution = new Zend_Form_Element_Text('institution');
        $institution->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

		 //contact_no
        $email = new Zend_Form_Element_Text('email');
		$email->setAttrib('class', 'input-txt')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		//name
		$fullname = new Zend_Form_Element_Text('fullname');
		$fullname->setAttrib('class', 'input-txt')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		 //password
        $password = new Zend_Form_Element_Password('password');
		$password->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		 //password
        $newpassword = new Zend_Form_Element_Password('newpassword');
		$newpassword->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');


		//staff
        $staff_id = new Zend_Form_Element_Select('staff_id');
		$staff_id->setAttrib('class', 'select reqfield')
					 ->removeDecorator("DtDdWrapper")
				     ->removeDecorator("Label")
				     ->removeDecorator('HtmlTag');
		
		$userDB = new GeneralSetup_Model_DbTable_User();
		$userList = $userDB->fngetUserDetails();
		$staff_id->addMultiOption('', '-- Select --');
		foreach ($userList as $user)
		{
			$staff_id->addMultiOption($user['iduser'], $user['FullName']);
		}
		
		 //start date
        $start_date = new Zend_Form_Element_Text('start_date');
		$start_date->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		 //end date
        $end_date = new Zend_Form_Element_Text('end_date');
		$end_date->setAttrib('class', 'input-txt datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		
		//form elements
        $this->addElements(array(
            $fullname,
			$password,
			$examiner_type,
			$contact_no,
			$email,
			$staff_id,
			$start_date,
			$end_date,
			$newpassword,
            $institution

		));
		
	}
}
?>