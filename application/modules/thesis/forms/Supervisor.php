<?php
class Thesis_Form_Supervisor extends Zend_Form
{
	
	public function init()
	{
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		/*$supervisor_userid = new Zend_Form_Element_Select('supervisor_userid');
		$supervisor_userid->setAttrib('class', 'select reqfield')
					 ->setRequired(true)
					 ->removeDecorator("DtDdWrapper")
				     ->removeDecorator("Label")
				     ->removeDecorator('HtmlTag');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$userList = $thesisDB->getAvailableSupervisors();
		$supervisor_userid->addMultiOption('', '-- Select --');
		foreach ($userList as $user)
		{
			$supervisor_userid->addMultiOption($user['iduser'], $user['FullName']);
		}*/
		
		//description
        $code = new Zend_Form_Element_Text('code');
		$code->setAttrib('class', 'input-txt')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//description
        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('class', 'input-txt')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');


		//maxcandidate
        $maxcandidate = new Zend_Form_Element_Text('maxcandidate');
		$maxcandidate->setAttrib('class', 'input-txt small reqfield')
					->setAttrib('maxlength','3')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

	
		
		//form elements
        $this->addElements(array(
            $code,
			$description,
			$maxcandidate
		));
		
	}
}
?>