<?php
class Thesis_Form_SearchResearchProposal extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','test');

//		$model = new Thesis_Model_DbTable_Registration();
        $generalModel = new Thesis_Model_DbTable_General();

//        //Program
//        $this->addElement('select','id_program', array(
//            'label'=>$this->getView()->translate('Programme'),
//            'required'=>false
//        ));
//
//        $programDb = new Registration_Model_DbTable_Program();
//
//        $this->id_program->addMultiOption(null,"ALL");
//        foreach($programDb->getData()  as $program){
//            if($this->_locale=='ms_MY'){
//                $this->id_program->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
//            }else{
//                $this->id_program->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
//            }
//        }

        //Course
        $this->addElement('select', 'course_id', array(
            'label' => $this->getView()->translate('Course'),
            'required' => false
        ));

        $this->course_id->addMultiOption(null,'ALL');
        $coursesList = $generalModel->getAllCourses();
        foreach ($coursesList as $c)
        {
            $this->course_id->addMultiOption($c['IdSubject'], $c['SubCode'].' - '.$c['SubjectName']);
        }

//		//name
//		$this->addElement('text','student_name', array(
//			'label'=>'Student Name',
//			'class'=>'input-txt'
//		));
//
//		//personal ID
//		$this->addElement('text','student_id', array(
//			'label'=>'Student ID',
//			'class'=>'input-txt'
//		));
//
//		//registration Status
//		$this->addElement('select','status', array(
//			'label'=>'Registration Status',
//			'class'=>'select'
//		));
//
//		$this->status->addMultiOption(null,"ALL");
//
//		$statusList = $model->getStatus();
//
//		if ($statusList){
//			foreach ($statusList as $status){
//				$this->status->addMultiOption($status['status_id'], $status['status_code']);
//			}
//		}
//
//        //proposal defence Status
//        $this->addElement('select','pd_status', array(
//            'label'=>'Proposal Defence Status',
//            'class'=>'select'
//        ));
//
//        $this->pd_status->addMultiOption(null,"ALL");
//
//        $pdstatusList = $model->getStatus(1);
//
//        if ($pdstatusList){
//            foreach ($pdstatusList as $status){
//                $this->pd_status->addMultiOption($status['status_id'], $status['status_description']);
//            }
//        }

        //Semester
        $this->addElement('select','IdSemester', array(
            'label'=>$this->getView()->translate('Semester'),
            'required'=>false
        ));

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();

        $this->IdSemester->addMultiOption(null,"ALL");
        foreach($semesterDB->fnGetSemesterList(6) as $semester){
            if($this->_locale=='ms_MY'){
                $this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
            }else{
                $this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
            }
        }

//        //Cluster
//        $this->addElement('select','IdCollege', array(
//            'label'=>$this->getView()->translate('Cluster'),
//            'required'=>false
//        ));
//
//        $clusterDB = new Thesis_Model_DbTable_Registration();
//
//        $this->IdCollege->addMultiOption(null,"ALL");
//        foreach($clusterDB->getClusterList() as $cluster){
//                $this->IdCollege->addMultiOption($cluster["key"],$cluster["value"]);
//        }

		//approval date
		/*$this->addElement('text','approval_date', array(
			'label'=>'Approval Date',
			'class'=>'input-txt'
		));*/

		//Lecturer's
		/*$this->addElement('select','lecturer', array(
			'label'=>'Lecturer',
			'class'=>'select'
		));

		$this->lecturer->addMultiOption(null,"-- Select --");

		$lecturerList = $model->getLecturer();

		if ($lecturerList){
			foreach ($lecturerList as $lecturer){
				$this->lecturer->addMultiOption($lecturer['IdStaff'], $lecturer['FullName']);
			}
		}*/

        //admin approval
        $this->addElement('select','admin_approval', array(
            'label'=>'Admin Approval',
            'class'=>'select'
        ));

        $this->admin_approval->addMultiOption('',"ALL");
        $this->admin_approval->addMultiOption(1,"YES");
        $this->admin_approval->addMultiOption(0,"NO");
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}