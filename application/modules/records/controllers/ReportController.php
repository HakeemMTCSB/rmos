<?php
class Records_ReportController extends Base_Base { //Controller for the User Module
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_Report();
    }
    
    public function indexAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->role = $this->auth->getIdentity()->IdRole;
        $this->view->title = $this->view->translate('Student Records');
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form = new Records_Form_Report(array('programid'=>$formData['program']));
            $this->view->form = $form;
            $form->populate($formData);
            
            if (isset($formData['search'])){
                $studentList = $this->model->getStudentRecords($formData);
                
                if ($studentList){
                    foreach ($studentList as $key=>$studentLoop){
                        $semesterstatus = self::defineSemesterStatusId($studentLoop['IdStudentRegistration'], $studentLoop['profileStatus'], $studentLoop['idscheme']);
                        
                        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                        $taggingFinancialAid=$studentRegistrationDb->getFinancialAidTagging($studentLoop['IdStudentRegistration']);
                        $studentList[$key] = array_merge($studentList[$key],$taggingFinancialAid);
                        
                        if (isset($formData['semesterstatus']) && $formData['semesterstatus']!=''){
                            if ($formData['semesterstatus']!=$semesterstatus){
                                unset($studentList[$key]);
                            }
                        }
                    }
                }
                
                $this->view->studentList = $studentList;
            }else{
                $form = new Records_Form_Report();
                $this->view->form = $form;
            }
        }else{
            $form = new Records_Form_Report();
            $this->view->form = $form;
        }
    }
    
    public function printAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        $this->view->role = $this->auth->getIdentity()->IdRole;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $studentList = $this->model->getStudentRecords($formData);
            
            if ($studentList){
                foreach ($studentList as $key=>$studentLoop){
                    $semesterstatus = self::defineSemesterStatusId($studentLoop['IdStudentRegistration'], $studentLoop['profileStatus'], $studentLoop['idscheme']);

                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                    $taggingFinancialAid=$studentRegistrationDb->getFinancialAidTagging($studentLoop['IdStudentRegistration']);
                    $studentList[$key] = array_merge($studentList[$key],$taggingFinancialAid);

                    if (isset($formData['semesterstatus']) && $formData['semesterstatus']!=''){
                        if ($formData['semesterstatus']!=$semesterstatus){
                            unset($studentList[$key]);
                        }
                    }
                }
            }
            
            $this->view->studentList = $studentList;
        }
        
        $this->view->filename = date('Ymd').'_list_of_student.xls';
    }
    
    static function defineSemesterStatus($spId, $status, $scheme){
        
        $model = new Records_Model_DbTable_Report();
        
        $lastSem = $model->getLastSemesterStatus($spId);
        
        if ($lastSem){
            $currentSemStatus= $lastSem['status'];
        }else{
            $currentSemStatus = 'Not Registered';
        }
        //echo $scheme; exit;
        /*switch ($status){
            case 92:
                if ($scheme != null){
                    $curSem = $model->getCurrentSem($scheme);
                }else{
                    $curSem = false;
                }
                
                if ($curSem){
                    $semStatus = $model->getSemesterStatus($spId,$curSem["IdSemesterMaster"]);

                    if ($semStatus && isset($semStatus['studentsemesterstatus']) && $semStatus['studentsemesterstatus']!=131){
                        $currentSemStatus = $semStatus['status'];
                    }else{
                        $lastSem = $model->getLastSemesterStatus($spId);
                        
                        if ($lastSem){
                            if ($scheme==1){ //ps
                                $getsem = $model->countSemesterStatus($scheme, $lastSem['SemesterMainStartDate'], $curSem['SemesterMainStartDate']);
                            
                                $semcount = count($getsem)-2;
                                
                                if ($semcount >= 3){
                                    $currentSemStatus = 'Dormant';
                                }else{
                                    $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($spId, $curSem["IdSemesterMaster"]);
                                    
                                    if ($checkSemStatusBasedOnSubjectReg){
                                        $currentSemStatus = 'Registered';
                                    }else{
                                        $currentSemStatus = 'Not Registered';
                                    }
                                }
                            }else if ($scheme==11){ //gs
                                $getsem = $model->countSemesterStatus($scheme, $lastSem['SemesterMainStartDate'], $curSem['SemesterMainStartDate'], 172);
                            
                                $semcount = count($getsem)-2;
                                
                                if ($semcount >= 2){
                                    $currentSemStatus = 'Dormant';
                                }else{
                                    $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($spId, $curSem["IdSemesterMaster"]);
                                    
                                    if ($checkSemStatusBasedOnSubjectReg){
                                        $currentSemStatus = 'Registered';
                                    }else{
                                        $currentSemStatus = 'Not Registered';
                                    }
                                }
                            }else{ //open
                                $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($curSem["IdSemesterMaster"]);
                                    
                                if ($checkSemStatusBasedOnSubjectReg){
                                    $currentSemStatus = 'Registered';
                                }else{
                                    $currentSemStatus = 'Not Registered';
                                }
                            }
                        }else{
                            $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($spId, $curSem["IdSemesterMaster"]);
                                    
                            if ($checkSemStatusBasedOnSubjectReg){
                                $currentSemStatus = 'Registered';
                            }else{
                                $currentSemStatus = 'Not Registered';
                            }
                        }
                    }
                }else{
                    $currentSemStatus = 'Not Registered';
                }
                
                break;
            case 94:
                if ($scheme != null){
                    $curSem = $model->getCurrentSem($scheme);
                }else{
                    $curSem = false;
                }
                
                if ($curSem){
                    $semStatus = $model->getSemesterStatus($spId,$curSem["IdSemesterMaster"]);
                    if ($semStatus){
                        $currentSemStatus = $semStatus['status'];
                    }else{
                        $currentSemStatus = 'Terminated';
                    }
                }else{
                    $currentSemStatus = 'Terminated';
                }
                break;
            case 96:
                $currentSemStatus = 'Graduated';
                break;
            case 248:
                $currentSemStatus = 'Completed';
                break;
            case 249:
                if ($scheme != null){
                    $curSem = $model->getCurrentSem($scheme);
                }else{
                    $curSem = false;
                }
                
                if ($curSem){
                    $semStatus = $model->getSemesterStatus($spId,$curSem["IdSemesterMaster"]);
                    if ($semStatus){
                        $currentSemStatus = $semStatus['status'];
                    }else{
                        $currentSemStatus = 'Quit';
                    }
                }else{
                    $currentSemStatus = 'Quit';
                }
                break;
            default:
                $currentSemStatus = 'Not Registered';
        }*/
        
        return $currentSemStatus;
    }
    
    static function defineSemesterStatusId($spId, $status, $scheme){
        
        $model = new Records_Model_DbTable_Report();
        
        $lastSem = $model->getLastSemesterStatus($spId);
        
        if ($lastSem){
            $currentSemStatus= $lastSem['studentsemesterstatus'];
        }else{
            $currentSemStatus = 131;
        }
        
        return $currentSemStatus;
    }
    
    static function countYearOfStudy($id, $scheme){
        $model = new Records_Model_DbTable_Report();
        
        $intakeId = $model->getIntakeId($id);
        $intakeDetail = $model->getIntakeDetail($intakeId);
        
        if ($intakeId==60){
            $semesterInfo['SemesterMainStartDate']='2006-12-01';
        }else{
            $semesterInfo = $model->getSemesterStart($intakeDetail['sem_year'], $intakeDetail['sem_seq'], $scheme);
        }
        
        $date1 = $semesterInfo['SemesterMainStartDate'];
        $date2 = $model->getCurrentSem($scheme);
        
        $diff = abs(strtotime($date2['SemesterMainStartDate']) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        
        return $years+1;
    }
    
    static function countSemesterActive($id, $scheme, $year){
        $model = new Records_Model_DbTable_Report();
        
        $intakeId = $model->getIntakeId($id);
        $intakeDetail = $model->getIntakeDetail($intakeId);
        
        $year = $year-1;
        $yearStudy = $intakeDetail['sem_year']+$year;
        
        if ($intakeId==60){
            $semStart['SemesterMainStartDate']='2006-12-01';
        }else{
            $semStart = $model->getSemesterStart($yearStudy, $intakeDetail['sem_seq'], $scheme);
        }
        
        if ($semStart){
            $semList = $model->getOneYearSemList($semStart['SemesterMainStartDate'], $scheme);

            $arrSemId = array();
            if ($semList){
                foreach ($semList as $semLoop){
                    array_push($arrSemId, $semLoop['IdSemesterMaster']);
                }
            }

            $activeSem = $model->getCurrentYearSemester($id, $scheme, $arrSemId);

            return count($activeSem);
        }else{
            return 0;
        }
    }

    static function countSemesterRegistered($id){
        $model = new Records_Model_DbTable_Report();
        $semreg = $model->getSemesterRegistered($id);

        return count($semreg);
    }
    
    public function visaReportAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = $this->view->translate('Visa Report');
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['search'])){
                $visaReportList = $this->model->getVisaReport($formData);
                
                if ($visaReportList){
                    foreach ($visaReportList as $key => $visaReportLoop){
                        $studentPassport = $this->model->getStudentPassport($visaReportLoop['sp_id']);
                        
                        if ($studentPassport){
                            $visaReportList[$key]['studentpassport'] = $studentPassport;
                        }
                        
                        $studentPass = $this->model->getStudentPass($visaReportLoop['sp_id']);
                        
                        if ($studentPass){
                            $visaReportList[$key]['studentpass'] = $studentPass;
                        }
                        
                        $dependentPass = $this->model->getDependentPass($visaReportLoop['sp_id']);
                        
                        if ($dependentPass){
                            $visaReportList[$key]['dependentpass'] = $dependentPass;
                        }
                    }
                }
                
                $this->view->visaReportList = $visaReportList;
                $form = new Records_Form_VisaReport(array('programid'=>$formData['program']));
                $this->view->form = $form;
                $form->populate($formData);
            }else{
                $form = new Records_Form_VisaReport();
                $this->view->form = $form;
            }
        }else{
            $form = new Records_Form_VisaReport();
            $this->view->form = $form;
        }
    }
    
    public function printVisaReportAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        $this->view->role = $this->auth->getIdentity()->IdRole;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $visaReportList = $this->model->getVisaReport($formData);
            
            if ($visaReportList){
                foreach ($visaReportList as $key => $visaReportLoop){
                    $studentPassport = $this->model->getStudentPassport($visaReportLoop['sp_id']);

                    if ($studentPassport){
                        $visaReportList[$key]['studentpassport'] = $studentPassport;
                    }

                    $studentPass = $this->model->getStudentPass($visaReportLoop['sp_id']);

                    if ($studentPass){
                        $visaReportList[$key]['studentpass'] = $studentPass;
                    }
                    
                    $dependentPass = $this->model->getDependentPass($visaReportLoop['sp_id']);
                        
                    if ($dependentPass){
                        $visaReportList[$key]['dependentpass'] = $dependentPass;
                    }
                }
            }
            
            $this->view->visaReportList = $visaReportList;
        }
        
        $this->view->filename = date('Ymd').'_visa_report.xls';
    }
    
    public function updateSemesterStatusAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        echo 'function disabled';
        exit;
        
        $data = $this->model->getCorrectionData();
        //var_dump(count($data));
        //var_dump($data); exit;
        if ($data){
            $i=0;
            $j=0;
            foreach ($data as $dataLoop){
                
                //define status besar
                /*if ($dataLoop['status']=='Quit'){
                    $statusBesar = 249;
                }else{
                    $statusBesar = 94;
                }*/
                $statusBesar = 248;
                
                //update status besar
                $dataStatusBesar = array(
                    'profileStatus'=>$statusBesar
                );
                $this->model->updateStatusBesar($dataStatusBesar, $dataLoop['IdStudentRegistration']);
                
                //define status kecil
                /*if ($dataLoop['semesterstatus']=='Deceased'){
                    $statusKecil = 836;
                }else if($dataLoop['semesterstatus']=='Failed Exam'){
                    $statusKecil = 132;
                }else{
                    $statusKecil = 844;
                }*/
                $statusKecil = 229;
                
                //define semester
                if ($dataLoop['IdScheme']==1){
                    $semesterId = 6;
                }else{
                    $semesterId = 11;
                }
                
                //update semester kecil
                $check = $this->model->checkStatusKecil($dataLoop['IdStudentRegistration'], $semesterId);
                
                if ($check){ //update
                    $dataStatusKecil = array(
                        'studentsemesterstatus'=>$statusKecil,
                        'UpdDate'=>date('Y-m-d H:i:s'), 
                        'UpdUser'=>1, 
                        'UpdRole'=>'admin'
                    );
                    $this->model->updateStatusKecil($dataStatusKecil, $check['idstudentsemsterstatus']);
                    
                    $i++;
                }else{ //insert
                    $dataStatusKecil = array(
                        'IdStudentRegistration'=>$dataLoop['IdStudentRegistration'], 
                        'idSemester'=>$semesterId, 
                        'IdSemesterMain'=>$semesterId, 
                        'IdBlock'=>null, 
                        'studentsemesterstatus'=>$statusKecil, 
                        'Level'=>1, 
                        'Reason'=>'', 
                        'UpdDate'=>date('Y-m-d H:i:s'), 
                        'UpdUser'=>1, 
                        'UpdRole'=>'admin'
                    );
                    $this->model->insertStatusKecil($dataStatusKecil);
                    
                    $j++;
                }
            }
        }
        echo $j.' Data Insert And '.$i.' Data Update';
        exit;
    }
}

?>