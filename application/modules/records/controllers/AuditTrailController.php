<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 7/11/2016
 * Time: 2:34 PM
 */
class Records_AuditTrailController extends Base_Base {

    private $_gobjlog;
    private $locale;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Audit Trail');
    }

    public function exportAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();
        $this->view->filename = date('Ymd').'_audit_trail.xls';

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $model = new Records_Model_DbTable_AuditTrail();

            $data = array();
            $i = 0;

            $invList = $model->getAuditTrailInvoice();

            if ($formData['date_from'] != ''){
                $formData['date_from'] = date('Y-m-d', strtotime($formData['date_from']));
            }else{
                $formData['date_from'] = '1970-01-01';
            }

            if ($formData['date_to'] != ''){
                $formData['date_to'] = date('Y-m-d', strtotime($formData['date_to']));
            }else{
                $formData['date_to'] = date("Y-m-d", strtotime(date("Y-m-d") . " + 10 year"));
            }

            if ($invList) {
                foreach ($invList as $inv) {
                    if ($inv['createdby'] != null) {
                        if ($formData['date_from'] <= $inv['date_create'] && $formData['date_to'] >= $inv['date_create']) {
                            $data[$i]['audit_trail_id'] = $inv['bill_number'];
                            $data[$i]['date_time'] = $inv['date_create'];
                            $data[$i]['username'] = $inv['createdby'];
                            $data[$i]['transaction_type'] = 'Invoice';
                            $data[$i]['action'] = 'Add';

                            $i++;
                        }
                    }

                    if ($inv['approvedby'] != null) {
                        if ($formData['date_from'] <= $inv['approve_date'] && $formData['date_to'] >= $inv['approve_date']) {
                            $data[$i]['audit_trail_id'] = $inv['bill_number'];
                            $data[$i]['date_time'] = $inv['approve_date'];
                            $data[$i]['username'] = $inv['approvedby'];
                            $data[$i]['transaction_type'] = 'Invoice';
                            $data[$i]['action'] = 'Approve';

                            $i++;
                        }
                    }

                    if ($inv['canceledby'] != null) {
                        if ($formData['date_from'] <= $inv['cancel_date'] && $formData['date_to'] >= $inv['cancel_date']) {
                            $data[$i]['audit_trail_id'] = $inv['bill_number'];
                            $data[$i]['date_time'] = $inv['cancel_date'];
                            $data[$i]['username'] = $inv['canceledby'];
                            $data[$i]['transaction_type'] = 'Invoice';
                            $data[$i]['action'] = 'Cancel';

                            $i++;
                        }
                    }
                }
            }

            $disList = $model->getAuditTrailDiscount();

            if ($disList) {
                foreach ($disList as $dis) {
                    if ($dis['createdby'] != null) {
                        if ($formData['date_from'] <= $dis['dcnt_create_date'] && $formData['date_to'] >= $dis['dcnt_create_date']) {
                            $data[$i]['audit_trail_id'] = $dis['dcnt_fomulir_id'];
                            $data[$i]['date_time'] = $dis['dcnt_create_date'];
                            $data[$i]['username'] = $dis['createdby'];
                            $data[$i]['transaction_type'] = 'Discount';
                            $data[$i]['action'] = 'Add';

                            $i++;
                        }
                    }

                    if ($dis['approvedby'] != null) {
                        if ($formData['date_from'] <= $dis['dcnt_approve_date'] && $formData['date_to'] >= $dis['dcnt_approve_date']) {
                            $data[$i]['audit_trail_id'] = $dis['dcnt_fomulir_id'];
                            $data[$i]['date_time'] = $dis['dcnt_approve_date'];
                            $data[$i]['username'] = $dis['approvedby'];
                            $data[$i]['transaction_type'] = 'Discount';
                            $data[$i]['action'] = 'Approve';

                            $i++;
                        }
                    }

                    if ($dis['canceledby'] != null) {
                        if ($formData['date_from'] <= $dis['dcnt_cancel_date'] && $formData['date_to'] >= $dis['dcnt_cancel_date']) {
                            $data[$i]['audit_trail_id'] = $dis['dcnt_fomulir_id'];
                            $data[$i]['date_time'] = $dis['dcnt_cancel_date'];
                            $data[$i]['username'] = $dis['canceledby'];
                            $data[$i]['transaction_type'] = 'Discount';
                            $data[$i]['action'] = 'Cancel';

                            $i++;
                        }
                    }
                }
            }

            $cnList = $model->getAuditTrailCreditNote();

            if ($cnList) {
                foreach ($cnList as $cn) {
                    if ($cn['createdby'] != null) {
                        if ($formData['date_from'] <= $cn['cn_create_date'] && $formData['date_to'] >= $cn['cn_create_date']) {
                            $data[$i]['audit_trail_id'] = $cn['cn_billing_no'];
                            $data[$i]['date_time'] = $cn['cn_create_date'];
                            $data[$i]['username'] = $cn['createdby'];
                            $data[$i]['transaction_type'] = 'Credit Note';
                            $data[$i]['action'] = 'Add';

                            $i++;
                        }
                    }

                    if ($cn['approvedby'] != null) {
                        if ($formData['date_from'] <= $cn['cn_approve_date'] && $formData['date_to'] >= $cn['cn_approve_date']) {
                            $data[$i]['audit_trail_id'] = $cn['cn_billing_no'];
                            $data[$i]['date_time'] = $cn['cn_approve_date'];
                            $data[$i]['username'] = $cn['approvedby'];
                            $data[$i]['transaction_type'] = 'Credit Note';
                            $data[$i]['action'] = 'Approve';

                            $i++;
                        }
                    }

                    if ($cn['canceledby'] != null) {
                        if ($formData['date_from'] <= $cn['cn_cancel_date'] && $formData['date_to'] >= $cn['cn_cancel_date']) {
                            $data[$i]['audit_trail_id'] = $cn['cn_billing_no'];
                            $data[$i]['date_time'] = $cn['cn_cancel_date'];
                            $data[$i]['username'] = $cn['canceledby'];
                            $data[$i]['transaction_type'] = 'Credit Note';
                            $data[$i]['action'] = 'Cancel';

                            $i++;
                        }
                    }
                }
            }

            $rcpList = $model->getAuditTrailReceipt();

            if ($rcpList) {
                foreach ($rcpList as $rcp) {
                    if ($rcp['createdby'] != null) {
                        if ($formData['date_from'] <= $rcp['rcp_date'] && $formData['date_to'] >= $rcp['rcp_date']) {
                            $data[$i]['audit_trail_id'] = $rcp['rcp_no'];
                            $data[$i]['date_time'] = $rcp['rcp_date'];
                            $data[$i]['username'] = $rcp['createdby'];
                            $data[$i]['transaction_type'] = 'Payment';
                            $data[$i]['action'] = 'Add';

                            $i++;
                        }
                    }

                    if ($rcp['canceledby'] != null) {
                        if ($formData['date_from'] <= $rcp['cancel_date'] && $formData['date_to'] >= $rcp['cancel_date']) {
                            $data[$i]['audit_trail_id'] = $rcp['rcp_no'];
                            $data[$i]['date_time'] = $rcp['cancel_date'];
                            $data[$i]['username'] = $rcp['canceledby'];
                            $data[$i]['transaction_type'] = 'Payment';
                            $data[$i]['action'] = 'Cancel';

                            $i++;
                        }
                    }
                }
            }

            $regList = $model->getAuditTrailCourseRegistration();

            if ($regList) {
                foreach ($regList as $reg) {
                    if ($formData['date_from'] <= $reg['createddt'] && $formData['date_to'] >= $reg['createddt']) {
                        $data[$i]['audit_trail_id'] = $reg['id'];
                        $data[$i]['date_time'] = $reg['createddt'];
                        $data[$i]['username'] = $reg['createdby'] == 'student' ? 'Student' : $reg['createdby'];
                        $data[$i]['transaction_type'] = 'Course Registration';
                        $data[$i]['action'] = $reg['message'];

                        $i++;
                    }
                }
            }

            $ctList = $model->getAuditTrailCreditTransfer();

            if ($ctList) {
                foreach ($ctList as $ct) {
                    if ($ct['createdby'] != null) {
                        if ($formData['date_from'] <= $ct['DateApplied'] && $formData['date_to'] >= $ct['DateApplied']) {
                            $data[$i]['audit_trail_id'] = $ct['IdCTApplication'];
                            $data[$i]['date_time'] = $ct['DateApplied'];
                            $data[$i]['username'] = $ct['createdby'];
                            $data[$i]['transaction_type'] = $ct['applicationType'] == 780 ? 'Credit Transfer' : 'Exemption';
                            $data[$i]['action'] = 'Add';

                            $i++;
                        }
                    }

                    if ($formData['date_from'] <= $ct['DateApproved'] && $formData['date_to'] >= $ct['DateApproved']) {
                        if ($ct['ApplicationStatus'] == 785) {
                            $data[$i]['audit_trail_id'] = $ct['IdCTApplication'];
                            $data[$i]['date_time'] = $ct['DateApproved'];
                            $data[$i]['username'] = $ct['approvedby'];
                            $data[$i]['transaction_type'] = $ct['applicationType'] == 780 ? 'Credit Transfer' : 'Exemption';
                            $data[$i]['action'] = 'Cancel';

                            $i++;
                        }

                        if ($ct['ApplicationStatus'] == 786) {
                            $data[$i]['audit_trail_id'] = $ct['IdCTApplication'];
                            $data[$i]['date_time'] = $ct['DateApproved'];
                            $data[$i]['username'] = $ct['approvedby'];
                            $data[$i]['transaction_type'] = $ct['applicationType'] == 780 ? 'Credit Transfer' : 'Exemption';
                            $data[$i]['action'] = 'Approve';

                            $i++;
                        }

                        if ($ct['ApplicationStatus'] == 787) {
                            $data[$i]['audit_trail_id'] = $ct['IdCTApplication'];
                            $data[$i]['date_time'] = $ct['DateApproved'];
                            $data[$i]['username'] = $ct['approvedby'];
                            $data[$i]['transaction_type'] = $ct['applicationType'] == 780 ? 'Credit Transfer' : 'Exemption';
                            $data[$i]['action'] = 'Rejected';

                            $i++;
                        }
                    }
                }
            }

            $this->aasort($data, 'date_time');
            $this->view->data = $data;
        }
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }
}