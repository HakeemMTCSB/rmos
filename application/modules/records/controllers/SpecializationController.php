<?php
class Records_SpecializationController extends Base_Base {

    private $_gobjlog;
    private $locale;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
    }

    public function chooseAction(){
        $this->view->title = $this->view->translate('Specialization');

        $studentId = $this->_getParam('id');

        $studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
        $studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($studentId);
        $this->view->studentdetails = $studentdetails;

        $programDb = new App_Model_General_DbTable_Program();
        $majors = $programDb->fnviewProgramMajoring($studentdetails['IdProgram'])->toArray();
        $this->view->majors = $majors;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'IdProgramMajoring'=>$formData['IDProgramMajoring'],
                'Specialization' => 1
            );
            $studentRegistrationDB->updateStudentProfile($studentId, $data);

            $this->_helper->flashMessenger->addMessage(array('success' => "Specialization updated."));
            $this->_redirect( $this->baseUrl . '/records/specialization/choose/id/'.$studentId);
        }
    }
}