<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_SponsorMigrateController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_SponsorMigrate();
    }
    
    public function indexAction(){
        echo 'function block';
        exit;
        $list = $this->model->getMigrateData();
        //var_dump($list); exit;
        if ($list){
            foreach ($list as $loop){
                
                /*if ($loop['IdScheme'] == null){
                    var_dump($loop);
                }*/
                //$semester = $this->model->getCurSem($loop['IdScheme']);
                if ($loop['IdScheme']==1){
                    $semester = 55;
                }else{
                    $semester = 54;
                }
                
                $data = array(
                    'sa_cust_id' => $loop['IdStudentRegistration'],
                    'sa_cust_type' => 1,
                    'sa_status' => 1,
                    'sa_semester_id' => $semester,
                    'sa_createddt' => date('Y-m-d H:i:s'),
                    'sa_createdby' => 1
                );
                $app_id = $this->model->insertSponsor($data);

                //insert details
                $details = array(	
                    'application_id' => $app_id,
                    'student_id' => $loop['IdStudentRegistration'],
                    'totaldependent' => 0,
                    'empstatus' => 2,
                    'cgpa' => 0.00,
                    'industry' => 0,
                    'income' => 0,
                    'currency' => 0,
                    'compaddress' => '',
                    'compemail' => '',
                    'created_date' => date('Y-m-d H:i:s')
                );
                $this->model->insertSponsorDetails($details);
            }
        }
        
        exit;
    }
}
?>