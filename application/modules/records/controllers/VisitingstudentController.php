<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_VisitingstudentController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_Visitingstudent();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Add Student');
        
        $personalDetailForm = new Records_Form_PersonalDetailsVSForm();
        $this->view->personalDetailForm = $personalDetailForm;
        
        $programDetailForm = new Records_Form_ProgramDetailsVSForm();
        $this->view->programDetailForm = $programDetailForm;
        
        //get student type list
        $studentTypeList = $this->model->getDefination(142);
        $this->view->studentTypeList = $studentTypeList;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            /*if ($formData['student_type']==741){
                $feeItem = $this->model->getFeeId($formData['feeplan']);

                if (!$feeItem){
                    //redirect
                    $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot add student (Fee Structure Not Setup Properly)'));
                    $this->_redirect($this->baseUrl . '/records/visitingstudent/');
                }
            }*/
            
            //todo generate visiting student id (refer yati function)
            $VSID = $this->generateRegistrationID($formData['student_type'], $formData['IdIntake']);
            
            if($formData["appl_type_nationality"]==547){
                $formData['appl_category']=579; //local 
            }else if($formData["appl_type_nationality"]==548){
                $formData['appl_category']=581; //pr
            }else if($formData["appl_type_nationality"]==549){
                $formData['appl_category']=580; //int
            }
            
            //generate password
            //$password = $this->fnCreateRandPassword(6);
            $password = '123456#';
            
            $studentProfileData = array(
                'appl_salutation' => $formData['appl_salutation'],
                'appl_fname' => $formData['appl_fname'],
                'appl_lname' => $formData['appl_lname'],
                'appl_idnumber' => $formData['appl_idnumber'],
                'appl_passport_expiry' => $formData['appl_passport_expiry'],
                'appl_gender' => $formData['appl_gender'],
                'appl_dob' => $formData['appl_dob'],
                'appl_marital_status' => $formData['appl_marital_status'],
                'appl_religion' => $formData['appl_religion'],
                'appl_religion_others' => $formData['appl_religion_others'],
                'appl_nationality' => $formData['appl_nationality'],
                'appl_type_nationality' => $formData['appl_type_nationality'],
                'appl_category' => $formData['appl_category'],
                'appl_race' => $formData['appl_race'],
                'appl_bumiputera' => $formData['appl_bumiputera'],
                'appl_email' => $VSID.'@student.inceif.org',
                'appl_email_personal' => $formData['appl_email_personal'],
                'appl_username' => $VSID,
                'appl_password' => md5($password),
                'appl_address1' => $formData['appl_address1'],
                'appl_address2' => $formData['appl_address2'],
                'appl_address3' => $formData['appl_address3'],
                'appl_postcode' => $formData['appl_postcode'],
                'appl_country' => $formData['appl_country'],
                'appl_state' => $formData['appl_state'],
                'appl_state_others' => $formData['appl_state_others'],
                'appl_city' => $formData['appl_city'],
                'appl_city_others' => $formData['appl_city_others'],
                'appl_caddress1' => $formData['appl_caddress1'],
                'appl_caddress2' => $formData['appl_caddress2'],
                'appl_caddress3' => $formData['appl_caddress3'],
                'appl_cpostcode' => $formData['appl_cpostcode'],
                'appl_ccountry' => $formData['appl_ccountry'],
                'appl_cstate' => $formData['appl_cstate'],
                'appl_cstate_others' => $formData['appl_cstate_others'],
                'appl_ccity' => $formData['appl_ccity'],
                'appl_ccity_others' => $formData['appl_ccity_others'],
                'appl_phone_home' => $formData['appl_phone_home'],
                'appl_phone_mobile' => $formData['appl_phone_mobile'],
                'appl_phone_office' => $formData['appl_phone_office'],
                'appl_fax' => $formData['appl_fax'],
                'appl_cphone_home' => $formData['appl_cphone_home'],
                'appl_cphone_mobile' => $formData['appl_cphone_mobile'],
                'appl_cphone_office' => $formData['appl_cphone_office']
            );
            $spId = $this->model->insertStudentProfile($studentProfileData);
            
            // --------- create repository folder (student)	---------   
            $student_path = DOCUMENT_PATH.'/student/'.date('Ym');

            //create directory to locate fisle			
            if (!is_dir($student_path)) {
                mkdir($student_path, 0775);
            }    			

            $txn_path = $student_path."/".$spId;

            //create directory to locate file			
            if (!is_dir($txn_path)) {
                mkdir($txn_path, 0775);
            }							

            //update repository info	
            $rep['sp_repository']='/student/'.date('Ym').'/'.$spId;								
            $this->model->updateStudentProfile($rep,$spId);
            
            //get program scheme, semester, and fee structure
            if ($formData['student_type']==740){
                $programScheme = $this->model->getProgramScheme($formData['IdProgram'], $formData['mode_of_program'], $formData['mode_of_study'], $formData['program_type']);
                $programId = $formData['IdProgram'];
                $semester = $this->getSemester($formData['IdIntake'], $formData['IdProgram']);
            }else{
                $programScheme['IdProgramScheme']=41;
                $programId = 21;
                $semester = $this->getSemester($formData['IdIntake'], $programId);
            }
            
            /*if (isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $semester = $this->getSemester($formData['IdIntake'], $formData['IdProgram']);
            }else{
                $semester['IdSemesterMaster'] = 0;
            }*/
            if (!$semester){
                $semester['IdSemesterMaster'] = 0;
            }
            //exit;
            //get semester
            $sem = $this->model->getCurrentSemester();
            
            $studentRegData = array(
                'IdApplication' =>0, 
                'sp_id' => $spId, 
                'transaction_id' => 0, 
                'registrationId' => $VSID, 
                'IdSemestersyllabus' => $semester['IdSemesterMaster'], 
                'IdSemester' => $semester['IdSemesterMaster'], 
                'Semesterstatus' => null, 
                'SemesterType' => null, 
                'IdSemesterMain' => $semester['IdSemesterMaster'], 
                'IdSemesterDetails' => $semester['IdSemesterMaster'],
                //'IdSemesterMain' => $sem['IdSemesterMaster'],
                //'IdLandscape'=> 192,
                'IdProgram' => $programId, 
                'IdProgramScheme' => $programScheme['IdProgramScheme'], 
                'IdProgramMajoring' => 0, 
                'IdFeePlan' => null, 
                'IdDiscountPlan' => null, 
                'IdDiscountPlanDate' => null, 
                'IdDiscountPlanBy' => null, 
                'IdFeePlanDate' => null, 
                'IdFeePlanBy' => null, 
                'IdBranch' => $formData['IdBranch']=='' ? 0:$formData['IdBranch'], 
                'IdIntake' => $formData['IdIntake']=='' ? 0:$formData['IdIntake'], 
                'profileStatus' => 92, 
                'UpdUser' => $this->auth->getIdentity()->iduser, 
                'UpdDate' => date('y-m-d h:i:s'), 
                'student_type' =>$formData['student_type'], 
                'AcademicAdvisor' => 0, 
                'senior_student' => 0, 
                'sr_remarks' => null, 
                'ref_id' => null, 
                'MigrateCode' => null, 
                'fs_id'=>$formData['feeplan']
            );
            $idStudentReg = $this->model->insertStudentRegistration($studentRegData);
            
            if ($formData['student_type']==740){
                $landscapeId = $this->getStudentLandscapeId($idStudentReg);
            }else{
                $landscapeId = 192;
            }
            
            $landscapeData = array(
                'IdLandscape'=>$landscapeId
            );

            //update landscape id
            $this->model->updateStudent($landscapeData, $idStudentReg);
            
            //generate invoice
            /*if ($formData['student_type']==741){
                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $invoiceClass->generateInvoice($idStudentReg, $feeItem, $sem['IdSemesterMaster']);
            }*/
            
            //push to moodle
            if($idStudentReg) {
                $data = array(
                    "Users"=>array(
                        array(
                            'role'=> 5,
                            'username'=> $VSID,
                            'password'=>$password,
                            'firstname'=>$formData['appl_fname'],
                            'lastname'=>$formData['appl_lname'],
                            'email'=>$VSID .'@student.inceif.org'
                        )
                    )
                );

                $data1 = array(
                    'username'=> $VSID
                );

                if ( defined('MOODLE_SYNC') ){
                    $response = push_to_moodle('CheckUser',$data1);

                    $Xml = new SimpleXmlElement($response);

                    if($Xml){
                        if($Xml->Message == 'User does not exist!'){
                            push_to_moodle('CreateUser',$data);
                        }
                    }
                } //MOODLE_SYNC check
            } 
            
            //send email
            $this->sendEmail($spId, $password);
            //exit;
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/records/studentprofile/student-profile/id/'.$idStudentReg);
        }
    }
    
    public function generateRegistrationID($student_type, $intake){
        $generateLib = new icampus_Function_General_Format();
        $myRegistrationId = $generateLib->generateStudentID($student_type, $intake);

        //check unique ID
        $txnDB = new Registration_Model_DbTable_ApplicantTransaction();						
        $exist = $txnDB->fncheckUniqueRegID($myRegistrationId);

        if($exist){			 	
            $this->generateRegistrationID($student_type, $intake);
        }else{			 	
            return $myRegistrationId;
        }
    }
    
    public function getStudentLandscapeId($txnId){
		
        $db = Zend_Db_Table::getDefaultAdapter();

        //get landscape from txn 
        $txnDb = $this->model;
        $txnData = $txnDb->getTheStudentRegistrationDetail($txnId);

        $program_id = $txnData['IdProgram'];
        if($program_id){
            $program_id = $program_id;
        }else{
            //throw new Exception('No student program data');
            $program_id = 0;
        }

        //assign landscape if null
        $landscapeDb = new App_Model_General_DbTable_Landscape();
        if($txnData['IdLandscape'] == null || $txnData['IdLandscape'] == 0){

            //get landscape based on program, scheme and active
            $where = array(
                'IdProgram = ?' => $program_id,
                'IdStartSemester = ?' => $txnData['IdIntake']==null ? 0:$txnData['IdIntake'],
                'IdProgramScheme = ?' => $txnData['IdProgramScheme']==null ? 0:$txnData['IdProgramScheme']
            );
            //var_dump($where); exit;

            $row_landscape = $landscapeDb->fetchRow($where);

            if($row_landscape == null){
                //throw new Exception('No Active landscape');
                $landscape_id = 0;
            }else{
                $landscape_id = $row_landscape['IdLandscape'];
            }

        }else{
            $landscape_id = $txnData['IdLandscape'];
        }

        return $landscape_id;
    }
    
    public function getSemester($intakeId, $programId){
        $getProgram = $this->model->getProgram($programId);
        $getIntake = $this->model->getIntake($intakeId);
        $getSemester = $this->model->getSemester($getIntake['sem_year'], $getIntake['sem_seq'], $getProgram['IdScheme']);
        
        return $getSemester;
    }
    
    public function getMopAction(){
        $programId = $this->_getParam('programId', 0);
        $mos = $this->_getParam('mos', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $mopList = $this->model->getMop($programId, $mos);
        
        $json = Zend_Json::encode($mopList);
		
	echo $json;
	exit();
    }
    
    public function getMosAction(){
        $programId = $this->_getParam('programId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $mosList = $this->model->getMos($programId);
        
        $json = Zend_Json::encode($mosList);
		
	echo $json;
	exit();
    }
    
    public function getPtAction(){
        $programId = $this->_getParam('programId', 0);
        $mos = $this->_getParam('mos', 0);
        $mop = $this->_getParam('mop', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $ptList = $this->model->getPt($programId, $mos, $mop);
        
        $json = Zend_Json::encode($ptList);
		
	echo $json;
	exit();
    }
    
    public function fnCreateRandPassword($length){
        $chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $i = 0;
        $password = "";
        
        while ($i <= $length){
            @$password .= $chars{mt_rand(0,strlen($chars))};
            $i++;
        }
        
        return $password;
    }
    
    private function sendEmail($sp_id,$pwds){
			
        $cmsTags = new Cms_TemplateTags();

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');

        $studentDB = new Records_Model_DbTable_Studentprofile();
        $student = $studentDB->getProfile($sp_id);

        $templateDB = new App_Model_General_DbTable_EmailTemplate();
        $templateData = $templateDB->getEmailTemplate(601, 5, 579);

        if($templateData){

            if($locale=='ms_MY'){
                $templateMail = $templateData['contentMy'];
            }else{
                $templateMail = $templateData['contentEng'];
            }

            $templateMail = $cmsTags->parseTag($sp_id,$templateData['stp_tplId'],$templateMail);
            $templateMail = str_replace("[Student Password]", $pwds, $templateMail); 


            $email_receipient = $student["appl_email"];
            $email_personal_receipient = $student["appl_email_personal"];
            $name_receipient  = $student["appl_fname"].' '.$student["appl_lname"];    		

            $emailDb = new App_Model_System_DbTable_Email();

            //email personal
            $data_inceif = array(
                'recepient_email' => $email_personal_receipient,
                'subject' => $templateData["stp_title"],
                'content' => $templateMail,
                'attachment_path'=>null,
                'attachment_filename'=>null					
            );	

            //add email to email que
            $email_id2 = $emailDb->addData($data_inceif);
            
            //save to history
            $data_compose = array(
                'comp_subject'=> $templateData["stp_title"],
                'comp_module' => 'records', 
                'comp_type' => 585, 
                'comp_tpl_id' => $templateData["stp_tplId"],
                'comp_rectype' => 'students', 
                'comp_lang' => 'en_US', 
                'comp_totalrecipients' => 1, 
                'comp_rec' => $student['IdStudentRegistration'], 
                'comp_content' => $templateMail, 
                'created_by' => $this->auth->getIdentity()->iduser, 
                'created_date' => date('Y-m-d H:i:s')
            );
            $comId = $this->model->insertCommpose($data_compose);
            
            $data_compose_rec = array(
                'cr_comp_id' => $comId, 
                'cr_subject' => $templateData["stp_title"], 
                'cr_content' => $templateMail, 
                'cr_rec_id' => $student['IdStudentRegistration'], 
                'cr_email' => $email_personal_receipient, 
                'cr_status' => 1, 
                'cr_datesent' => date('Y-m-d H:i:s')
            );
            $this->model->insertComposeRec($data_compose_rec);
            
            return true;
        }else{
            return false;
        }		
    }
}
?>