<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_AuditpaperController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_Auditpaper();
    }
    
    public function indexAction(){
        //echo md5('123456#');
        $this->view->title = $this->view->translate('Audit Paper Application');
        $form = new Records_Form_SearchAuditPaper();

        $cacheObj = new Cms_Cache();
        $cache = $cacheObj->get();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['Program'] = $this->view->escape(strip_tags($formData['Program']));
            $formData['ProgramScheme'] = $this->view->escape(strip_tags($formData['ProgramScheme']));
            $formData['AppStatus'] = $this->view->escape(strip_tags($formData['AppStatus']));
            $formData['StudentName'] = $this->view->escape(strip_tags($formData['StudentName']));
            $formData['StudentCategory'] = $this->view->escape(strip_tags($formData['StudentCategory']));
            $formData['AppType'] = $this->view->escape(strip_tags($formData['AppType']));
            $formData['course'] = $this->view->escape(strip_tags($formData['course']));
            $formData['StudentId'] = $this->view->escape(strip_tags($formData['StudentId']));
            $formData['ApplicationID'] = $this->view->escape(strip_tags($formData['ApplicationID']));
            $formData['DateFrom'] = $this->view->escape(strip_tags($formData['DateFrom']));
            $formData['DateTo'] = $this->view->escape(strip_tags($formData['DateTo']));
            $formData['Semester'] = $this->view->escape(strip_tags($formData['Semester']));

            if (isset($formData['search'])){
                $auditPaperAppList = $this->model->auditPaperApplicationList($formData);
                //$this->gobjsessionsis->auditpapercontroller = $auditPaperAppList;
                $cache->save($auditPaperAppList, 'auditpapercontroller');
            }else{
                $auditPaperAppList = $this->model->auditPaperApplicationList();
            }
        }else{
            $auditPaperAppList = $this->model->auditPaperApplicationList();
        }
        
        if(!$this->_getParam('search')){
            //unset($this->gobjsessionsis->auditpapercontroller);
            $cache->remove('auditpapercontroller');
        }
        
        $lintpagecount = $this->gintPageCount;// Definitiontype model
        
        $lintpage = $this->_getParam('page',1); // Paginator instance

        $cached_results = $cache->load('auditpapercontroller');

        if( $cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($auditPaperAppList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }
    
    public function auditpaperapplicationAction(){
        $this->view->title = $this->view->translate('Audit Paper Application');
        $form = new Records_Form_AddAuditPaper();
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            
            $appData = array(
                'apa_studregid'=>$formData['studRegId'], 
                'apa_appid'=>$this->generateApplicationID(), 
                'apa_programId'=>$formData['programId'], 
                'apa_semesterid'=>$formData['Semester'], 
                'apa_status'=>823, 
                'apa_approveddate'=>null, 
                'apa_applieddate'=>date('Y-m-d'),
                'apa_appliedby'=>$userId,
                'apa_remarks'=>'', 
                'apa_source'=>'Admin', 
                'apa_type'=>$formData['Type'],
                'apa_upddate'=>date('Y-m-d'), 
                'apa_upduser'=>$userId
            );
            $appId = $this->model->insertApApp($appData);
            
            $courseData = array(
                'apc_apaid'=>$appId, 
                'apc_courseid'=>$formData['Course'], 
                'apc_credithours'=>$formData['credithours'], 
                'apc_gred'=>$formData['grade'], 
                'apc_remarks'=>$formData['Remarks'],
                'apc_program'=>$formData['Program'],
                'apc_programscheme'=>$formData['ProgramScheme'],
                'apc_coursesection'=>$formData['CourseSection'],
                'apc_country'=>$formData['ExamCenterCountry'],
                'apc_city'=>$formData['ExamCenterCity'],
                'apc_cityothers'=>$formData['ExamCenterCityOthers'],
                'apc_itemreg'=>implode('|',$formData['itemreg']),
                'apc_upddate'=>date('Y-m-d'), 
                'apc_upduser'=>$userId
            );
            $idCourse = $this->model->insertApAppCourse($courseData);
            
            if (isset($formData['itemreg']) && count($formData['itemreg']) > 0){
                foreach ($formData['itemreg'] as $itemregLoop){
                    $itemregData = array(
                        'api_apa_id'=>$appId, 
                        'api_apc_id'=>$idCourse, 
                        'api_item'=>$itemregLoop, 
                        'api_upddate'=>date('Y-m-d H:i:s'), 
                        'api_upduser'=>$userId
                    );
                    $apiId = $this->model->insertCourseItem($itemregData);
                    
                    //get student info
                    $studentInfo = $this->model->getStudentInfo($formData['studRegId']);
                    
                    //get current sem
                    $curSem = $this->model->getCurSem($studentInfo['IdScheme']);
                    
                    //get program info choose
                    $programChoose = $this->model->getProgramForSem($formData['Program']);
                    
                    //get current intake
                    $curIntake = $this->model->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);
                    
                    //get audit paper fee structure
                    $auditPaperFsId = $this->model->getApplicantFeeStructure($formData['Program'], $formData['ProgramScheme'], $curIntake['IdIntake'], $studentInfo['appl_category'], 0);

                    if ($studentInfo['student_type']){
                        if ($studentInfo['IdScheme'] != $programChoose['IdScheme']){
                            $fsid = $auditPaperFsId['fs_id'];
                        }else{
                            $fsid = 0;
                        }
                    }else{
                        $fsid = 0;
                    }
                    
                    //generate invoice
                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceid = $invoiceClass->generateOtherInvoiceStudent($formData['studRegId'],$formData['Semester'],'AU',$formData['Course'],0,$fsid,$itemregLoop);

                    $invData = array(
                        'api_invid_0'=>$invoiceid
                    );
                    $this->model->updateCourseItem($invData, $apiId);
                }
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/records/auditpaper/editauditpaperapplication/apaId/'.$appId);
        }
    }
    
    public function editauditpaperapplicationAction(){
        $apaId = $this->_getParam('apaId', 0);
        $this->view->title = $this->view->translate("Edit Audit Paper Application");
        $appApList = $this->model->getAuditPaperApp($apaId);
        $this->view->appApList = $appApList;
        $getCourse = $this->model->getAuditAppCourse($apaId);
        $this->view->course = $getCourse;
        
        //get exam registration info
        $examRegDb= new Registration_Model_DbTable_ExamRegistration();
        $this->view->exam_reg = $examRegDb->getDataExamRegistrationBySubject($appApList['apa_studregid'],$appApList['apa_semesterid'],$getCourse['apc_courseid']);
        
        $form = new Records_Form_EditAuditPaper(array(
            'programId'=>$getCourse['apc_program'], 
            'programschemeId'=>$getCourse['apc_programscheme'], 
            'intakeId'=>$appApList['IdIntake'], 
            'semesterId'=>$appApList['apa_semesterid'],
            'subjectId'=>$getCourse['apc_courseid'],
            'countryId'=>$getCourse['apc_country'],
        ));
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        //address
        $address = '';
        $address .= (($appApList['appl_address1']!='' || $appApList['appl_address1']!=null) ? $appApList['appl_address1']:'');
        $address .= (($appApList['appl_address2']!='' || $appApList['appl_address2']!=null) ? ',<br/>'.$appApList['appl_address2']:'');
        $address .= (($appApList['appl_address3']!='' || $appApList['appl_address3']!=null) ? ',<br/>'.$appApList['appl_address3']:'');
        $address .= (($appApList['appl_postcode']!='' || $appApList['appl_postcode']!=null) ? ',<br/>'.$appApList['appl_postcode']:'');
        $address .= (($appApList['CityName']!='' || $appApList['CityName']!=null) ? ',<br/>'.$appApList['CityName']:'');
        $address .= (($appApList['StateName']!='' || $appApList['StateName']!=null) ? ',<br/>'.$appApList['StateName']:'');
        $address .= (($appApList['CountryName']!='' || $appApList['CountryName']!=null) ? ',<br/>'.$appApList['CountryName']:'');
        
        $this->view->address = $address;
        
        $populate['Course'] = $getCourse['apc_courseid'];
        $populate['Remarks'] = $getCourse['apc_remarks'];
        $populate['Program'] = $getCourse['apc_program'];
        $populate['ProgramScheme'] = $getCourse['apc_programscheme'];
        $populate['ExamCenterCountry'] = $getCourse['apc_country'];
        $populate['ExamCenterCity'] = $getCourse['apc_city'];
        $populate['ExamCenterCityOthers'] = $getCourse['apc_cityothers'];
        $populate['CourseSection'] = $getCourse['apc_coursesection'];
        
        $form->populate($populate);
        
        //item reg
        $itemreg = $this->model->getItemRegistration($getCourse['apc_program'], $getCourse['apc_programscheme']);
        $this->view->itemreg = $itemreg;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $courseData = array(
                'apc_courseid'=>$formData['Course'], 
                'apc_credithours'=>$formData['credithours'], 
                'apc_gred'=>$formData['grade'], 
                'apc_remarks'=>$formData['Remarks'],
                'apc_program'=>$formData['Program'],
                'apc_programscheme'=>$formData['ProgramScheme'],
                'apc_coursesection'=>$formData['CourseSection'],
                'apc_country'=>$formData['ExamCenterCountry'],
                'apc_city'=>$formData['ExamCenterCity'],
                'apc_cityothers'=>isset($formData['ExamCenterCityOthers']) ? $formData['ExamCenterCityOthers']:'',
                'apc_itemreg'=>implode('|',$formData['itemreg']),
                'apc_upddate'=>date('Y-m-d'), 
                'apc_upduser'=>$userId
            );
            $this->model->updateApAppCourse($courseData, $getCourse['apc_id']);
            
            //if any changes happen
            if ($getCourse['apc_program'] != $formData['Program'] || $getCourse['apc_programscheme'] != $formData['ProgramScheme']){
                
                $getItem = $this->model->getCourseItem($apaId);
            
                if ($getItem){
                    foreach ($getItem as $itemLoop){
                        if ($itemLoop['api_invid_0'] != 0){
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            //$invoiceClass->generateCreditNote($appApList['apa_studregid'], 0, $itemLoop['api_invid_0'], 0);
                            $invoiceClass->generateCreditNoteEntry($appApList['apa_studregid'],$itemLoop['api_invid_0']);
                        }
                        $this->model->deleteCourseItem($itemLoop['api_id']);
                    }
                }
                
                if (isset($formData['itemreg']) && count($formData['itemreg']) > 0){
                    foreach ($formData['itemreg'] as $itemregLoop){
                        $itemregData = array(
                            'api_apa_id'=>$apaId, 
                            'api_apc_id'=>$formData['Course'], 
                            'api_item'=>$itemregLoop, 
                            'api_upddate'=>date('Y-m-d H:i:s'), 
                            'api_upduser'=>$userId
                        );
                        $apiId = $this->model->insertCourseItem($itemregData);

                        //get student info
                        $studentInfo = $this->model->getStudentInfo($appApList['apa_studregid']);
                        
                        //get current sem
                        $curSem = $this->model->getCurSem($studentInfo['IdScheme']);

                        //get current intake
                        $curIntake = $this->model->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);

                        //get audit paper fee structure
                        $auditPaperFsId = $this->model->getApplicantFeeStructure($formData['Program'], $formData['ProgramScheme'], $curIntake['IdIntake'], $studentInfo['appl_category'], 0);
                        
                        //get program info choose
                        $programChoose = $this->model->getProgramForSem($formData['Program']);
                        
                        if ($studentInfo['student_type']){
                            if ($studentInfo['IdScheme'] != $programChoose['IdScheme']){
                                $fsid = $auditPaperFsId['fs_id'];
                            }else{
                                $fsid = 0;
                            }
                        }else{
                            $fsid = 0;
                        }
                        
                        //generate invoice
                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        $invoiceid = $invoiceClass->generateOtherInvoiceStudent($appApList['apa_studregid'],$appApList['apa_semesterid'],'AU',$formData['Course'],0,$fsid,$itemregLoop);

                        $invData = array(
                            'api_invid_0'=>$invoiceid
                        );
                        $this->model->updateCourseItem($invData, $apiId);
                    }
                }
            }else{
                if (isset($formData['itemreg'])){
                    $arrItem = $formData['itemreg'];
                }else{
                    $arrItem = array(0);
                }
                
                $getUntickItem = $this->model->getUntickCourseItem($apaId, $arrItem);
                
                if ($getUntickItem){
                    foreach ($getUntickItem as $getUntickItemLoop){
                        if ($getUntickItemLoop['api_invid_0'] != 0){
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceClass->generateCreditNoteEntry($appApList['apa_studregid'],$getUntickItemLoop['api_invid_0']);
                            //$invoiceClass->generateCreditNote($appApList['apa_studregid'], 0, $getUntickItemLoop['api_invid_0'], 0);
                        }
                        $this->model->deleteCourseItem($getUntickItemLoop['api_id']);
                    }
                }
                
                if (isset($formData['itemreg']) && count($formData['itemreg']) > 0){
                    foreach ($formData['itemreg'] as $itemregLoop2){
                        $checkItem = $this->model->getTickCourseItemById($apaId, $itemregLoop2);
                        
                        if (!$checkItem){
                            $itemregData = array(
                                'api_apa_id'=>$apaId, 
                                'api_apc_id'=>$formData['Course'], 
                                'api_item'=>$itemregLoop2, 
                                'api_upddate'=>date('Y-m-d H:i:s'), 
                                'api_upduser'=>$userId
                            );
                            $apiId = $this->model->insertCourseItem($itemregData);

                            //get student info
                            $studentInfo = $this->model->getStudentInfo($appApList['apa_studregid']);
                            
                            //get current sem
                            $curSem = $this->model->getCurSem($studentInfo['IdScheme']);

                            //get current intake
                            $curIntake = $this->model->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);

                            //get audit paper fee structure
                            $auditPaperFsId = $this->model->getApplicantFeeStructure($formData['Program'], $formData['ProgramScheme'], $curIntake['IdIntake'], $studentInfo['appl_category'], 0);
                            
                            //get program info choose
                            $programChoose = $this->model->getProgramForSem($formData['Program']);

                            if ($studentInfo['student_type']){
                                if ($studentInfo['IdScheme'] != $programChoose['IdScheme']){
                                    $fsid = $auditPaperFsId['fs_id'];
                                }else{
                                    $fsid = 0;
                                }
                            }else{
                                $fsid = 0;
                            }

                            //generate invoice
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceid = $invoiceClass->generateOtherInvoiceStudent($appApList['apa_studregid'],$appApList['apa_semesterid'],'AU',$formData['Course'],0,$fsid,$itemregLoop2);

                            $invData = array(
                                'api_invid_0'=>$invoiceid
                            );
                            $this->model->updateCourseItem($invData, $apiId);
                        }
                    }
                }
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/records/auditpaper/editauditpaperapplication/apaId/'.$apaId);
        }
    }
    
    public function findStudentAction(){
        $searchElement = $this->_getParam('term', '');
        $searchType = $this->_getParam('searchType', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $studentList = $this->model->findStudent($searchElement, $searchType);
        //var_dump($studentList); exit;
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $userInfo = $this->model->getUser($userId);
        
        if ($studentList){
            foreach($studentList as $student) {
                
                $address = (($student['appl_address1']!='' || $student['appl_address1']!=null) ? $student['appl_address1']:'');
                $address .= (($student['appl_address2']!='' || $student['appl_address2']!=null) ? ', '.$student['appl_address2']:'');
                $address .= (($student['appl_address3']!='' || $student['appl_address3']!=null) ? ', '.$student['appl_address3']:'');
                $address .= (($student['appl_postcode']!='' || $student['appl_postcode']!=null) ? ', '.$student['appl_postcode']:'');
                $address .= (($student['CityName']!='' || $student['CityName']!=null) ? ', '.$student['CityName']:'');
                $address .= (($student['StateName']!='' || $student['StateName']!=null) ? ', '.$student['StateName']:'');
                $address .= (($student['CountryName']!='' || $student['CountryName']!=null) ? ', '.$student['CountryName']:'');

                $status = $this->model->getDefination2(155, 'Applied');
                
                //current sem
                if ($student['schemeid']!=null){
                    $curSem = $this->model->getCurSem($student['schemeid']);
                }else{
                    $curSem['IdSemesterMaster'] = null;
                }
                
                $student_array[] = array(
                    'id' => $student['IdStudentRegistration'],
                    'ApplicationID'=>'Will generate after save',
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeId'=>$student['IdProgramScheme'],
                    'LandscapeId'=>$student['IdLandscape'],
                    'StatusName'=>$student['StatusName'],
                    'IntakeName'=>$student['IntakeName'],
                    'IntakeId'=>$student['IdIntake'],
                    'BranchName'=>$student['BranchName'],
                    'ProgramName'=>$student['ProgramName'],
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeName'=>$student['mop'].' '.$student['mos'].' '.$student['pt'],
                    'Address'=>$address,
                    'PhoneHome'=>$student['appl_phone_home'],
                    'PhoneMobile'=>$student['appl_phone_mobile'],
                    'PhoneOffice'=>$student['appl_phone_office'],
                    'Email'=>$student['appl_email'],
                    'IdType'=>$student['IdTypeName'],
                    'IdNo'=>$student['appl_idnumber'],
                    'AppliedDate'=>date('d-m-Y'),
                    'AppliedBy'=>$userInfo['loginName'],
                    'AppliedStatus'=>$status['DefinitionDesc'],
                    'CurrentSem'=>$curSem['IdSemesterMaster'],
                    'label' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname'],
                    'value' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
                );
            }
        }else{
            $student_array[] = array();
        }
        
        $json = Zend_Json::encode($student_array);
		
	echo $json;
	exit();
    }
    
    public function getCourseInfoAction(){
        $courseId = $this->_getParam('courseId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseInfo = $this->model->getCourseInfo($courseId);
        
        $json = Zend_Json::encode($courseInfo);
		
	echo $json;
	exit();
    }
    
    public function auditpaperapprovalAction(){
        $this->view->title = $this->view->translate('Audit Paper Approval');
        $form = new Records_Form_SearchAuditPaper();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['search'])){
                $auditPaperAppList = $this->model->auditPaperApplicationList($formData);
                $this->gobjsessionsis->auditpapercontroller = $auditPaperAppList;
            }else{
                $auditPaperAppList = $this->model->auditPaperApplicationList();
            }
            
        }else{
            $auditPaperAppList = $this->model->auditPaperApplicationList();
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->auditpapercontroller);
        }
        
        $lintpagecount = $this->gintPageCount;// Definitiontype model
        
        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->auditpapercontroller)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->auditpapercontroller,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($auditPaperAppList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }
    
    public function approveauditpaperapplicationAction(){
        $apaId = $this->_getParam('apaId', 0);
        $this->view->title = $this->view->translate("Audit Paper Approval");
        $form = new Records_Form_ApproveCreditTransfer();
        $this->view->form = $form;
        $appApList = $this->model->getAuditPaperApp($apaId);
        $this->view->appApList = $appApList;
        //var_dump($appApList); exit;
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        //address
        $address = '';
        $address .= (($appApList['appl_address1']!='' || $appApList['appl_address1']!=null) ? $appApList['appl_address1']:'');
        $address .= (($appApList['appl_address2']!='' || $appApList['appl_address2']!=null) ? ',<br/>'.$appApList['appl_address2']:'');
        $address .= (($appApList['appl_address3']!='' || $appApList['appl_address3']!=null) ? ',<br/>'.$appApList['appl_address3']:'');
        $address .= (($appApList['appl_postcode']!='' || $appApList['appl_postcode']!=null) ? ',<br/>'.$appApList['appl_postcode']:'');
        $address .= (($appApList['CityName']!='' || $appApList['CityName']!=null) ? ',<br/>'.$appApList['CityName']:'');
        $address .= (($appApList['StateName']!='' || $appApList['StateName']!=null) ? ',<br/>'.$appApList['StateName']:'');
        $address .= (($appApList['CountryName']!='' || $appApList['CountryName']!=null) ? ',<br/>'.$appApList['CountryName']:'');
        
        $this->view->address = $address;
        
        $getCourse = $this->model->getAuditAppCourse($apaId);
        $this->view->course = $getCourse;
        
        $userInfo = $this->model->getUser($userId);
        $this->view->userInfo = $userInfo;
        
        //item reg
        $itemreg = $this->model->getItemRegistration($getCourse['apc_program'], $getCourse['apc_programscheme']);
        $this->view->itemreg = $itemreg;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['approve_app'])){
                $url = $this->baseUrl . '/records/auditpaper/approveauditpaperapplication/apaId/'.$apaId;
                $this->approve($apaId, $url);

                //update status
                $statusData = array(
                    'apa_status'=>825,
                    'apa_remarks'=>$formData['Remarks'],
                    'apa_approvedby'=>$userId,
                    'apa_approveddate'=>date('Y-m-d'),
                    'apa_upddate'=>date('Y-m-d'), 
                    'apa_upduser'=>$userId
                );
                $this->model->updateAppStatus($statusData, $apaId);
                
                $getItem = $this->model->getCourseItem($apaId);
                
                if ($getItem){
                    foreach ($getItem as $itemLoop){
                        //get student info
                        $studentInfo = $this->model->getStudentInfo($appApList['apa_studregid']);

                        //get current sem
                        $curSem = $this->model->getCurSem($studentInfo['IdScheme']);

                        //get current intake
                        $curIntake = $this->model->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);
                        
                        //get audit paper fee structure
                        $auditPaperFsId = $this->model->getApplicantFeeStructure($getCourse['apc_program'], $getCourse['apc_programscheme'], $curIntake['IdIntake'], $appApList['appl_category'], 0);

                        //get program info choose
                        $programChoose = $this->model->getProgramForSem($getCourse['apc_program']);

                        if ($studentInfo['student_type']){
                            if ($studentInfo['IdScheme'] != $programChoose['IdScheme']){
                                $fsid = $auditPaperFsId['fs_id'];
                            }else{
                                $fsid = 0;
                            }
                        }else{
                            $fsid = 0;
                        }
                        
                        //generate invoice
                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        $invoiceid = $invoiceClass->generateOtherInvoiceStudent($appApList['apa_studregid'],$appApList['apa_semesterid'],'AU',$getCourse['apc_courseid'],1,$fsid,$itemLoop['api_item']);

                        $invData = array(
                            'api_invid_1'=>$invoiceid
                        );
                        $this->model->updateCourseItem($invData, $itemLoop['api_id']);
                    }
                }
                
                $message = "Application has been approved";
            }else if(isset($formData['reject_app'])){
                //update status
                $statusData = array(
                    'apa_status'=>826,
                    'apa_remarks'=>$formData['Remarks'],
                    'apa_approvedby'=>$userId,
                    'apa_approveddate'=>date('Y-m-d'),
                    'apa_upddate'=>date('Y-m-d'), 
                    'apa_upduser'=>$userId
                );
                $this->model->updateAppStatus($statusData, $apaId);
                
                $message = "Application has been rejected";
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => $message));
            $this->_redirect($this->baseUrl . '/records/auditpaper/approveauditpaperapplication/apaId/'.$apaId);
        }
    }
    
    public function cancelauditpaperapplicationAction(){
        $apaId = $this->_getParam('apaId', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($apaId!=0){
            $appApList = $this->model->getAuditPaperApp($apaId);
            //var_dump($appApList);
            
            $statusData = array(
                'apa_status'=>824,
                'apa_remarks'=>'Application cancel',
                'apa_approvedby'=>$userId,
                'apa_approveddate'=>date('Y-m-d'),
                'apa_upddate'=>date('Y-m-d'), 
                'apa_upduser'=>$userId
            );
            $this->model->updateAppStatus($statusData, $apaId);
            
            $getItem = $this->model->getCourseItem($apaId);
            
            if ($getItem){
                foreach ($getItem as $itemLoop){
                    if ($itemLoop['api_invid_0'] != 0){
                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        //$invoiceClass->generateCreditNote($appApList['apa_studregid'], 0, $itemLoop['api_invid_0'], 0);
                        $invoiceClass->generateCreditNoteEntry($appApList['apa_studregid'], $itemLoop['api_invid_0']);
                    }
                }
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Application has been cancel <br/><br/>Info :<br/>Name : '.$appApList['appl_fname'].' '.$appApList['appl_lname'].'<br/>Student ID : '.$appApList['registrationId']));
            $this->_redirect($this->baseUrl . '/records/auditpaper/index');
        }
        exit;
    }
    
    function generateApplicationID(){
        
        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        //bermulanya sebuah penciptaan id
        
        //check sequence
        $checkSequence = $this->model->getSequence(3, date('Y'));

        if (!$checkSequence){
            $dataSeq = array(
                'tsi_type'=>3, 
                'tsi_seq_no'=>1, 
                'tsi_seq_year'=>date('Y'), 
                'tsi_upd_date'=>date('Y-m-d'), 
                'tsi_upd_user'=>$userId
            );
            $this->model->insertSequence($dataSeq);
        }

        //generate applicant ID
        $applicationid_format = array();

        //get sequence
        $sequence = $this->model->getSequence(3, date('Y'));

        $config = $this->model->getConfig(1);

        //format
        $format_config = explode('|',$config['auditPaperIdFormat']);
        //var_dump($format_config);
        for($i=0; $i<count($format_config); $i++){
            $format = $format_config[$i];
            if($format=='AU'){ //prefix
                $result = $config['auditPaperPrefix'];
            }elseif($format=='YYYY'){
                $result = date('Y');
            }elseif($format=='YY'){
                $result = date('y');
            }elseif($format=='seqno'){				
                $result = sprintf("%05d", $sequence['tsi_seq_no']);
            }
            array_push($applicationid_format,$result);
        }

        $applicationID = implode("",$applicationid_format);

        //update sequence		
        $this->model->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        
        return $applicationID;
    }
    
    public function revertApplicationAction(){
        $apaId = $this->_getParam('apaId', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($apaId != 0){
            $appApList = $this->model->getAuditPaperApp($apaId);
            $getCourse = $this->model->getAuditAppCourse($apaId);
            
            $statusData = array(
                'apa_status'=>823,
                'apa_remarks'=>'',
                'apa_upddate'=>date('Y-m-d'), 
                'apa_upduser'=>$userId,
                'apa_subjectreg_id'=>''
            );
            $this->model->updateAppStatus($statusData, $apaId);
            
            $idRegSub = explode('|', $appApList['apa_subjectreg_id']);
            //var_dump($idRegSub); exit;
            if ($idRegSub[0] != ''){
                foreach ($idRegSub as $idRegSubLoop){
                    $this->model->deleteRegAuditPaper($idRegSubLoop);
                    $this->model->deleteRegAuditPaperHistory($idRegSubLoop);
                    $this->model->deleteItemReg($idRegSubLoop);
                    $this->model->deleteItemRegHis($idRegSubLoop);
                }
            }
            
            $ecarr = explode ('|', $appApList['apa_ec_id']);
            
            if ($ecarr[0] != ''){
                foreach ($ecarr as $ecLoop){
                    $this->model->deleteExamCenter($ecLoop);
                }
            }
            
            $getItem = $this->model->getCourseItem($apaId);
            
            if ($getItem){
                foreach ($getItem as $itemLoop){
                    if ($itemLoop['api_invid_1'] != 0){
                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        //$invoiceClass->generateCreditNote($appApList['apa_studregid'], 0, $itemLoop['api_invid_1'], 0);
                        $invoiceClass->generateCreditNoteEntry($appApList['apa_studregid'], $itemLoop['api_invid_1']);
                    }
                }
            }
        }
        
        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Revert Successs'));
        $this->_redirect($this->baseUrl . '/records/auditpaper/approveauditpaperapplication/apaId/'.$apaId);
    }
    
    public function getSemesterAction(){
        $programId = $this->_getParam('programId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $programInfo = $this->model->getProgramForSem($programId);
        
        $semList = $this->model->getSemBasedOnList($programInfo['IdScheme']);
        
        $json = Zend_Json::encode($semList);
		
	echo $json;
	exit();
    }
    
    public function getCourseAction(){
        $programId = $this->_getParam('programid', 0);
        $schemeId = $this->_getParam('schemeid', 0);
        $intakeId = $this->_getParam('intakeId', 0);
        $semesterId = $this->_getParam('semesterId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $landscapeInfo = $this->model->getlandscape($programId, $schemeId);
        //var_dump($landscapeInfo);
        if ($landscapeInfo){
            $subarr = array();
            foreach ($landscapeInfo as $landscapeLoop){
                if ($landscapeLoop['LandscapeType']==44){
                    $courseList = $this->model->getBlockSubject($landscapeLoop['IdLandscape']);
                    //var_dump($courseList);
                    if ($courseList){
                        foreach ($courseList as $courseLoop){
                            array_push($subarr, $courseLoop['subjectid']);
                        }
                    }
                }else{
                    $courseList = $this->model->getSubject($landscapeLoop['IdLandscape']);
                    //var_dump($courseList);
                    if ($courseList){
                        foreach ($courseList as $courseLoop){
                            array_push($subarr, $courseLoop['IdSubject']);
                        }
                    }
                }
            }
            
            if (count($subarr)>0){
                $subList = $this->model->getSubjectList($subarr);
            }else{
                $subList = array();
            }
        }else{
            $subList = array();
        }

        $json = Zend_Json::encode($subList);
		
	echo $json;
	exit();
    }
    
    public function getCourseSectionAction(){
        $courseid = $this->_getParam('courseid', 0);
        $semesterid = $this->_getParam('semesterid', 0);
        $programid = $this->_getParam('programid', 0);
        $programschemeid = $this->_getParam('programschemeid', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseSecList = $this->model->getGroupQuota($courseid, $programid, $programschemeid);
        
        $json = Zend_Json::encode($courseSecList);
		
	echo $json;
	exit();
    }
    
    public function getExamCenterCityAction(){
        $countryid = $this->_getParam('countryid', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $city = $this->model->city($countryid);
        
        $json = Zend_Json::encode($city);
		
	echo $json;
	exit();
    }
    
    public function getItemRegistrationAction(){
        $programid = $this->_getParam('programid', 0);
        $schemeid = $this->_getParam('schemeid', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $itemRegList = $this->model->getItemRegistration($programid, $schemeid);
        
        $json = Zend_Json::encode($itemRegList);
		
	echo $json;
	exit();
    }
    
    public function bulkapproveAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $url = $this->baseUrl.'/records/auditpaper/auditpaperapproval';
        
        $i=0;
        $j=0;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['apa_id']) && count($formData['apa_id']) > 0){
                if (isset($formData['approve_app'])){
                    foreach ($formData['apa_id'] as $apaLoop){
                        $result = $this->approve($apaLoop);
                        $appApList = $this->model->getAuditPaperApp($apaLoop);
                        $getCourse = $this->model->getAuditAppCourse($apaLoop);

                        if ($result == true){
                            $statusData = array(
                                'apa_status'=>825,
                                'apa_remarks'=>'Bulk Approval',
                                'apa_approvedby'=>$userId,
                                'apa_approveddate'=>date('Y-m-d'),
                                'apa_upddate'=>date('Y-m-d'), 
                                'apa_upduser'=>$userId
                            );
                            $this->model->updateAppStatus($statusData, $apaLoop);
                            
                            $getItem = $this->model->getCourseItem($apaLoop);
                
                            if ($getItem){
                                foreach ($getItem as $itemLoop){
                                    //get student info
                                    $studentInfo = $this->model->getStudentInfo($appApList['apa_studregid']);

                                    //get current sem
                                    $curSem = $this->model->getCurSem($studentInfo['IdScheme']);

                                    //get current intake
                                    $curIntake = $this->model->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);

                                    //get audit paper fee structure
                                    $auditPaperFsId = $this->model->getApplicantFeeStructure($getCourse['apc_program'], $getCourse['apc_programscheme'], $curIntake['IdIntake'], $appApList['appl_category'], 0);

                                    //get program info choose
                                    $programChoose = $this->model->getProgramForSem($getCourse['apc_program']);

                                    if ($studentInfo['student_type']){
                                        if ($studentInfo['IdScheme'] != $programChoose['IdScheme']){
                                            $fsid = $auditPaperFsId['fs_id'];
                                        }else{
                                            $fsid = 0;
                                        }
                                    }else{
                                        $fsid = 0;
                                    }
                                    
                                    //generate invoice
                                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                    $invoiceid = $invoiceClass->generateOtherInvoiceStudent($appApList['apa_studregid'],$appApList['apa_semesterid'],'AU',$getCourse['apc_courseid'],1,$fsid,$itemLoop['api_item']);

                                    $invData = array(
                                        'api_invid_1'=>$invoiceid
                                    );
                                    $this->model->updateCourseItem($invData, $itemLoop['api_id']);
                                }
                            }

                            $i++;
                        }else{
                            $j++;
                        }
                    }
                    $this->_helper->flashMessenger->addMessage(array('success' => $i.' application has been approve & '.$j.' application failed to approve'));
                    $this->_redirect($url);
                }else{
                    foreach ($formData['apa_id'] as $apaLoop){
                        $statusData = array(
                            'apa_status'=>826,
                            'apa_remarks'=>'Bulk Rejected',
                            'apa_approvedby'=>$userId,
                            'apa_approveddate'=>date('Y-m-d'),
                            'apa_upddate'=>date('Y-m-d'), 
                            'apa_upduser'=>$userId
                        );
                        $this->model->updateAppStatus($statusData, $apaLoop);
                        $i++;
                    }
                    $this->_helper->flashMessenger->addMessage(array('success' => $i.' application has been rejected'));
                    $this->_redirect($url);
                }
            }else{
                $this->_helper->flashMessenger->addMessage(array('error' => 'Please select checkbox at least one for approve/reject application'));
                $this->_redirect($url);
            }
        }
        $this->_redirect($url);
        exit;
    }
    
    public function approve($apaId, $url=false){
    	
    	$cms =  new Cms_Status();
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $appApList = $this->model->getAuditPaperApp($apaId);
        $getCourse = $this->model->getAuditAppCourse($apaId);
        
        $checkSubReg = $this->model->checkSubReg($appApList['IdStudentRegistration'], $getCourse['apc_courseid'], $appApList['apa_semesterid']);

        if ($checkSubReg){
            //redirect here
            if ($url!=false){
                $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot approve this application because subject: '.$getCourse['CourseName'].' has been register'));
                $this->_redirect($url);
            }
            
            $return = false;
        }else{
            $this->updateStudentSemesterStatus($appApList['apa_studregid'],$appApList['apa_semesterid'],130);
        	
            $getCourse2 = $this->model->getAuditAppCourse2($apaId);
            //var_dump($getCourse2); exit;
            if ($getCourse2){
                $subregId = array();
                $ecArr = array();
                foreach ($getCourse2 as $getCourseLoop){
                    $auditSub = array(
                        'IdStudentRegistration'=>$appApList['apa_studregid'], 
                        'IdSubject'=>$getCourseLoop['apc_courseid'], 
                        'IdSemesterMain'=>$appApList['apa_semesterid'], 
                        'SemesterLevel'=>count($this->model->getSemStatusBasedRegId($appApList['apa_studregid']))+1, 
                        'exam_status'=>$getCourseLoop['apc_gred'],
                        //'credit_hour_registered'=>$getCourseLoop['apc_credithours'],
                        'grade_name'=>$appApList['apa_type']==858 ? null:$getCourseLoop['apc_gred'],
                        'audit_program'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_program']:null,
                        'audit_programscheme'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_programscheme']:null,
                        'mark_approval_status'=>$appApList['apa_type']==858 ? 0:2,
                        'UpdUser'=>$userId, 
                        'UpdDate'=>date('Y-m-d'), 
                        'Active'=>0,
                        'IdCourseTaggingGroup'=>$getCourseLoop['apc_coursesection']
                    );
                    $studRegSubId = $this->model->registerAuditPaper($auditSub);

                    $auditSubHis = array(
                        'IdStudentRegSubjects'=>$studRegSubId,
                        'IdStudentRegistration'=>$appApList['apa_studregid'], 
                        'IdSubject'=>$getCourseLoop['apc_courseid'], 
                        'IdSemesterMain'=>$appApList['apa_semesterid'], 
                        'SemesterLevel'=>count($this->model->getSemStatusBasedRegId($appApList['apa_studregid']))+1, 
                        'exam_status'=>$getCourseLoop['apc_gred'],
                        //'credit_hour_registered'=>$getCourseLoop['apc_credithours'],
                        //'grade_name'=>$getCourseLoop['apc_gred'],
                        'grade_name'=>$appApList['apa_type']==858 ? null:$getCourseLoop['apc_gred'],
                        'audit_program'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_program']:null,
                        'audit_programscheme'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_programscheme']:null,
                        'mark_approval_status'=>$appApList['apa_type']==858 ? 0:2,
                        'UpdUser'=>$userId, 
                        'UpdDate'=>date('Y-m-d'), 
                        'Active'=>0,
                        'IdCourseTaggingGroup'=>$getCourseLoop['apc_coursesection']
                    );
                    $this->model->historyRegisterAuditPaper($auditSubHis);

                    array_push($subregId, $studRegSubId);
                    
                    //item registration part
                    $itemreg = explode('|', $getCourseLoop['apc_itemreg']);
                    if ($itemreg[0] != ''){
                        foreach ($itemreg as $itemregLoop){
                            $itemRegData = array(
                                'regsub_id'=>$studRegSubId, 
                                'student_id'=>$appApList['apa_studregid'], 
                                'semester_id'=>$appApList['apa_semesterid'], 
                                'subject_id'=>$getCourseLoop['apc_courseid'], 
                                'item_id'=>$itemregLoop, 
                                'section_id'=>$getCourseLoop['apc_coursesection'], 
                                'ec_country'=>$getCourseLoop['apc_country'], 
                                'ec_city'=>($getCourseLoop['apc_city']==99 || $getCourseLoop['apc_city']==0) ? $getCourseLoop['apc_cityothers']:$getCourseLoop['City'], 
                                'invoice_id'=>NULL, 
                                'created_date'=>date('Y-m-d h:i:s'), 
                                'created_by'=>$userId, 
                                'created_role'=>'admin', 
                                'ses_id'=>0, 
                                'status'=>0
                            );
                            $regItemId = $this->model->insertItemReg($itemRegData);
                            
                            $itemRegHisData = array(
                                'id'=>$regItemId, 
                                'regsub_id'=>$studRegSubId, 
                                'student_id'=>$appApList['apa_studregid'], 
                                'semester_id'=>$appApList['apa_semesterid'], 
                                'subject_id'=>$getCourseLoop['apc_courseid'], 
                                'item_id'=>$itemregLoop, 
                                'section_id'=>$getCourseLoop['apc_coursesection'], 
                                'ec_country'=>$getCourseLoop['apc_country'], 
                                'ec_city'=>($getCourseLoop['apc_city']==99 || $getCourseLoop['apc_city']==0) ? $getCourseLoop['apc_cityothers']:$getCourseLoop['City'], 
                                'invoice_id'=>NULL, 
                                'created_date'=>date('Y-m-d h:i:s'), 
                                'created_by'=>$userId, 
                                'created_role'=>'admin', 
                                'ses_id'=>0, 
                                'status'=>0
                            );
                            $this->model->insertItemRegHis($itemRegHisData);
                            
                            if ($itemregLoop == 879){
                                if($getCourseLoop['apc_country']==0 || $getCourseLoop['apc_city']==0){
                                    //auto set
                                    $getCourseLoop['apc_country'] = 121; //Malaysia
                                    $getCourseLoop['apc_city'] = 1348; //Kuala Lumpur
                                }
                                
                                //assign exam center to student
                                if($getCourseLoop['apc_country']!='' && $getCourseLoop['apc_city']!=''){	
                                    
                                    //get exam center that has been assigned
                                    $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
                                    $ec_info = $examCenterDB->getExamCenterByCountryCity($appApList['apa_semesterid'], $getCourseLoop['apc_country'], $getCourseLoop['apc_city'], $getCourseLoop['apc_cityothers']);
                                    //var_dump($ec_info); exit;
                                    if(count($ec_info) > 0){													
                                        $er_ec_id = $ec_info[0]['ec_id'];							
                                    }else{
                                        $er_ec_id = null;
                                    }

                                }else{
                                    $er_ec_id = null;
                                }
                                
                                //insert exam center
                                $examCenterData = array(
                                    'er_idCountry'=>$getCourseLoop['apc_country'], 
                                    'er_idCity'=>$getCourseLoop['apc_city'], 
                                    'er_idCityOthers'=>$getCourseLoop['apc_cityothers'], 
                                    'er_idSemester'=>$appApList['apa_semesterid'], 
                                    'er_idProgram'=>$getCourseLoop['apc_program'], 
                                    'er_idStudentRegistration'=>$appApList['apa_studregid'], 
                                    'er_idSubject'=>$getCourseLoop['apc_courseid'], 
                                    'er_ec_id'=>$er_ec_id,
                                    'er_status'=>764, 
                                    'er_createdby'=>$userId, 
                                    'er_createddt'=>date('Y-m-d h:i:s')
                                );
                                $ecId = $this->model->insertExamCenter($examCenterData);

                                array_push($ecArr, $ecId);
                            }
                        }
                    }
                }

                $implodeSubRegId = implode('|',$subregId);
                $implodeEcId = implode('|',$ecArr);

                $imlplodeData = array(
                    'apa_subjectreg_id'=>$implodeSubRegId,
                    'apa_ec_id'=>$implodeEcId
                );
                $this->model->updateAppStatus($imlplodeData, $apaId);
            }
            
            $return = true;
        }
        
        return $return;
    }
    
    
    public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){

        //check current status
        $semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
        $semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);

        if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
            //nothing to update
        }else{
            //add new status & keep old status into history table
            $cms = new Cms_Status();
            $auth = Zend_Auth::getInstance();

            $data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
            'idSemester' => $IdSemesterMain,
            'IdSemesterMain' => $IdSemesterMain,								
            'studentsemesterstatus' => $newstatus, 	//Register idDefType = 32 (student semester status)
            'Level'=>0,
            'UpdDate' => date ( 'Y-m-d H:i:s'),
            'UpdUser' => $auth->getIdentity()->iduser
            );				

            $message = 'Audit Paper : Approval';
            $cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,3);
        }
    }
    
    public function updateItemAction(){
        echo 'function block only developer can use it';
        exit;
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        echo 'process start<br/>';
        $auditPaperAppList = $this->model->auditPaperApplicationList();
        
        if ($auditPaperAppList){
            $i = 0;
            foreach ($auditPaperAppList as $auditPaperAppLoop){
                $checkItemExist = $this->model->getCourseItem($auditPaperAppLoop['apa_id']);
                
                if (empty($checkItemExist)){
                    $course = $this->model->getAuditAppCourse($auditPaperAppLoop['apa_id']);
                    
                    $item = array();
                    if ($course['apc_itemreg']==''){
                        if (isset($course['IdScheme']) == 1){
                            array_push($item, 890);
                        }else if (isset($course['IdScheme']) == 11){
                            array_push($item, 889);
                        }else{
                            array_push($item, 889);
                        }
                        
                        if ($auditPaperAppLoop['apa_type']==858){
                            array_push($item, 879);
                        }
                        
                        $implodeData = array(
                            'apc_itemreg'=>implode('|',$item)
                        );
                        $this->model->updateApAppCourse($implodeData, $course['apc_id']);
                        
                        if (!empty($item)){
                            foreach ($item as $itemLoop){
                                $itemregData = array(
                                    'api_apa_id'=>$auditPaperAppLoop['apa_id'], 
                                    'api_apc_id'=>$course['apc_id'], 
                                    'api_item'=>$itemLoop, 
                                    'api_upddate'=>date('Y-m-d H:i:s'), 
                                    'api_upduser'=>1
                                );
                                $this->model->insertCourseItem($itemregData);
                            }
                        }
                    }else{
                        $item = explode('|', $course['apc_itemreg']);
                        
                        foreach ($item as $itemLoop){
                            $itemregData = array(
                                'api_apa_id'=>$auditPaperAppLoop['apa_id'], 
                                'api_apc_id'=>$course['apc_id'], 
                                'api_item'=>$itemLoop, 
                                'api_upddate'=>date('Y-m-d H:i:s'), 
                                'api_upduser'=>1
                            );
                            $this->model->insertCourseItem($itemregData);
                        }
                    }
                    
                    if ($auditPaperAppLoop['apa_status']==825){
                        if ($auditPaperAppLoop['apa_subjectreg_id']!=''){
                            $itemreg = $item;
                            if ($itemreg[0] != ''){
                                foreach ($itemreg as $itemregLoop){
                                    $itemRegData = array(
                                        'regsub_id'=>$auditPaperAppLoop['apa_subjectreg_id'], 
                                        'student_id'=>$auditPaperAppLoop['apa_studregid'], 
                                        'semester_id'=>$auditPaperAppLoop['apa_semesterid'], 
                                        'subject_id'=>$course['apc_courseid'], 
                                        'item_id'=>$itemregLoop, 
                                        'section_id'=>$course['apc_coursesection'], 
                                        'ec_country'=>$course['apc_country'], 
                                        'ec_city'=>($course['apc_city']==99 || $course['apc_city']==0) ? $course['apc_cityothers']:$course['City'], 
                                        'invoice_id'=>NULL, 
                                        'created_date'=>date('Y-m-d h:i:s'), 
                                        'created_by'=>1, 
                                        'created_role'=>'admin', 
                                        'ses_id'=>0, 
                                        'status'=>0
                                    );
                                    $regItemId = $this->model->insertItemReg($itemRegData);

                                    $itemRegHisData = array(
                                        'id'=>$regItemId, 
                                        'regsub_id'=>$auditPaperAppLoop['apa_subjectreg_id'], 
                                        'student_id'=>$auditPaperAppLoop['apa_studregid'], 
                                        'semester_id'=>$auditPaperAppLoop['apa_semesterid'], 
                                        'subject_id'=>$course['apc_courseid'], 
                                        'item_id'=>$itemregLoop, 
                                        'section_id'=>$course['apc_coursesection'], 
                                        'ec_country'=>$course['apc_country'], 
                                        'ec_city'=>($course['apc_city']==99 || $course['apc_city']==0) ? $course['apc_cityothers']:$course['City'], 
                                        'invoice_id'=>NULL, 
                                        'created_date'=>date('Y-m-d h:i:s'), 
                                        'created_by'=>1, 
                                        'created_role'=>'admin', 
                                        'ses_id'=>0, 
                                        'status'=>0
                                    );
                                    $this->model->insertItemRegHis($itemRegHisData);
                                }
                            }
                        }
                    }
                }
            }
        }
        echo 'done';
        exit;
    }
    
    public function saveExamRegistration($formData,$idSubject,$IdStudentRegSubjects=null){
    	
        $auth = Zend_Auth::getInstance();

        $examRegDB = new Registration_Model_DbTable_ExamRegistration();
        $subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();

        $subject_info = $subjectDB->getData($idSubject);

        //if 2,3,9 should not have exam registration info
        if(in_array($subject_info['CourseType'],array(2,3,19)) == false){

            if($formData['country']==0 || $formData['city']==0){
                //auto set
                $formData['country'] = 121; //Malaysia
                $formData['city'] = 1348; //Kuala Lumpur
            }

            if(isset($formData['country']) && $formData['country']!=''){

                if ($formData['city']!=99){
                    $city = $formData['city'];
                    $city_others = '';
                }else{
                    $city = 99;
                    $city_others = $formData['cityothers']; 
                }

                $dataec['er_idCountry']=$formData['country'];
                $dataec['er_idCity']=$city;
                $dataec['er_idCityOthers'] = $city_others;	

                //assign exam center to student
                if($dataec['er_idCountry']!='' && $dataec['er_idCity']!=''){	

                    //get exam center that has been assigned
                    $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
                    $ec_info = $examCenterDB->getExamCenterByCountryCity($formData['IdSemester'],$dataec['er_idCountry'],$dataec['er_idCity'],$dataec['er_idCityOthers']);

                    if(count($ec_info) == 1){													
                        $dataec['er_ec_id'] = $ec_info[0]['ec_id'];							
                    }else{
                        $dataec['er_ec_id'] = null;
                    }

                }else{
                    $dataec['er_ec_id'] = null;
                }

                $dataec['er_idStudentRegistration']=$formData['IdStudentRegistration'];
                $dataec['er_idSemester']=$formData['IdSemester'];
                $dataec['er_idProgram']=$formData['IdProgram'];
                $dataec['er_idSubject']=$idSubject;
                $dataec['er_status']=764; //764:Regiseterd 
                $dataec['er_createdby']=$auth->getIdentity()->iduser;
                $dataec['er_createddt']= date ( 'Y-m-d H:i:s');	

                $examRegDB->addData($dataec);
            }//end isset
        }//end in array	
    }
    
    
    public function saveExamRegAction(){
    	$auth = Zend_Auth::getInstance();

    	if ($this->getRequest()->isPost()) {
            
    		$formData = $this->getRequest()->getPost();
             
            $dataapc['apc_country']=$formData['ExamCenterCountry'];
            $dataapc['apc_city']=$formData['ExamCenterCity'];
           
            $auditdb = new Records_Model_DbTable_Auditpaper();
            $auditdb->updateData($dataapc,$formData['apc_id']);
            
            $data['er_idCountry']=$formData['ExamCenterCountry'];
            $data['er_idCity']=$formData['ExamCenterCity'];
         
            $examRegDB = new Registration_Model_DbTable_ExamRegistration();
    		$examRegDB->updateData($data,$formData['er_id']);
    		
    		//redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Exam registration info has been updated'));
            $this->_redirect($this->baseUrl . '/records/auditpaper/editauditpaperapplication/apaId/'.$formData['apa_id']);
    	}
        
    }
}
?>