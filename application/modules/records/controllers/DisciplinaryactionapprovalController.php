<?php
class Records_DisciplinaryactionapprovalController extends Base_Base { //Controller for the Create Appraisal Module 
	
	private $_gobjlog;
	public function init(){
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();			 
	}
	public function fnsetObj(){
		
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$idUniversity = $this->gstrsessionSIS->idUniversity;
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();		
		$this->lobjdisciplinaryactionmodel = new Application_Model_DbTable_Disciplinaryaction();		
		$this->lobjdisciplinaryactionapprovalmodel = new Records_Model_DbTable_Disciplinaryactionapproval();		
		$this->lobjDisciplinaryactionapprovalForm = new Records_Form_Disciplinaryactionapproval();
		
	}

	
	// action for search and view
	public function indexAction() { 
		
		$lobjSearchForm = 	$this->view->lobjSearchForm = $this->lobjform; 			
		$this->view->lobjDisciplinaryactionapprovalForm = $this->lobjDisciplinaryactionapprovalForm;			
		$lobjSearchForm->field17->setAttrib("OnChange","fnGetDetails");			
		//Initialize Hostel Master Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;		
		$larrDisciplinaryactionList = $lobjDisciplinaryactionModel->fnGetDisciplinaryActionTypeList();
		$this->view->lobjSearchForm->field1->addMultiOptions($larrDisciplinaryactionList);		
			$this->lobjSubjectsMarksEntrynModelbulk = new Examination_Model_DbTable_Subjectsmarksentry();
			$larSemesterNameCombo = $lobjDisciplinaryactionModel->fngetSemesterNameCombo();	
			$this->view->lobjSearchForm->field10->addMultiOptions($larSemesterNameCombo);			
		//Get Disciplinary Action Details
		$lobjDisciplinaryActionDetails = $lobjDisciplinaryactionModel->fnGetStudentDisciplinaryActionDetails();		
				$ldtsystemDate = date ( 'Y-m-d H:i:s' );
				$this->view->lobjDisciplinaryactionapprovalForm->UpdDate->setValue( $ldtsystemDate );
				$auth = Zend_Auth::getInstance();
				$this->view->lobjDisciplinaryactionapprovalForm->UpdUser->setValue( $auth->getIdentity()->iduser);	
		//Disciplinary Action Details - Pagination
		$lintpagecount = $this->gintPageCount;  // Records per page		
		$lobjPaginator = new App_Model_Definitiontype(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsams->disciplinaryactionpaginatorresult)) {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($this->gobjsessionsams->disciplinaryactionpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($lobjDisciplinaryActionDetails,$lintpage,$lintpagecount);
		}		
		//Disciplinary Action Search
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjSearchForm->isValid ( $larrformData )) {								
				$larrSearchResult = $lobjDisciplinaryactionModel->fnSearchDsiciplinaryActionDetails( $larrformData );				
				//Disciplinary Action Details Search - Pagination
				$this->view->lobjPaginator = $lobjPaginator->fnPagination($larrSearchResult,$lintpage,$lintpagecount);
		    	$this->gobjsessionsams->disciplinaryactionpaginatorresult = $larrSearchResult;	    		
			}
		}		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {			
			$larrformData = $this->_request->getPost (); 					
					$this->lobjdisciplinaryactionapprovalmodel->fnupdateDisplinaryaction($larrformData);

					$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Disciplinary Action Approval Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
					$this->_redirect( $this->baseUrl . '/records/disciplinaryactionapproval/index');
			}
		//Disciplinary Action Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/records/disciplinaryactionapproval/index');			
		}
	}	
}