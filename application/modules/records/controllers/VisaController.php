<?php

class Records_VisaController extends Base_Base { //Controller for the User Module

	public function indexAction() {
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		//title
		$this->view->title= $this->view->translate("Visa Details");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$form = new Records_Form_VisaSearchStudent();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			$this->view->IdProgramScheme = $formData['IdProgramScheme'];
						
			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			$this->view->paginator = $paginator;
		}else{			 
		   
		    $studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent(array('appl_category'=>580));
		    
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			$this->view->paginator = $paginator;
		   
		}
	}
	
	public function passportAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Student Passport");		 
	
		$auth = Zend_Auth::getInstance();
                $this->view->role = $auth->getIdentity()->IdRole;
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		//$registrationId = $this->_getParam('id');
		//$this->view->registrationId = $registrationId;
		
		$spid = $this->_getParam('spid');
		$this->view->spid = $spid;
		
		//get student info
		$studentProfileDB = new Records_Model_DbTable_Studentprofile();
		$studentInfo = $studentProfileDB->getStudentInfoBySpid($spid);      
        $this->view->studentInfo = $studentInfo;
        
       
        //get graduation date
        $this->view->graduation_date = $this->getGraduationDate($studentInfo);
        
       //echo '<pre>';
       // print_r($studentInfo);
       
      	$this->view->app_photo = $this->getPhoto($spid);
        
        //check if passport not exist?
        $passportDB = new Records_Model_DbTable_StudentPassport();
        $passport = $passportDB->getTotalStudentPassport($studentInfo['id']);
        
        if(count($passport)>0){
        	//trace dari student_passport
        }else{
        	//trace dari student_profile insert dalam student_passport display balik
        	$data['sp_id'] = $studentInfo['id'];
        	//$data['IdStudentRegistration'] = $studentInfo['IdStudentRegistration'];
        	$data['p_passport_no'] = $studentInfo['appl_idnumber'];
        	$data['p_expiry_date'] = $studentInfo['appl_passport_expiry'];
        	$data['p_active']=1; //auto active for
        	$data['p_createddt']=date('Y-m-d H:i:s');
			$data['p_createdby']=$auth->getIdentity()->id;
        	$p_id = $passportDB->addData($data);
        }        
        $passport_list = $passportDB->getData($studentInfo['id']);
        
		$uploadfileDB = new Records_Model_DbTable_StudentDocuments();
		
		foreach($passport_list as $index=>$p){
			//get documents
			$passport_list[$index]['attachment'] = $uploadfileDB->getDocuments($studentInfo['id'],65,$p['p_id'],'student_passport');			
		}
        $this->view->passport_list = $passport_list;
        
        //get active passport only
        $active = $passportDB->getActivePassport($studentInfo['id']);
        if(isset($active['ad_id']) && $active['ad_id']!=''){
        	$active['attachment'] = $uploadfileDB->getDocumentById($active['ad_id']);		
        }
        $this->view->active = $active;
        
	}
	
	
	
	
	public function addAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->_helper->layout->disableLayout();
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Add Passport");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$auth = Zend_Auth::getInstance();
    		
		$registrationId = $this->_getParam('id');
		$spid = $this->_getParam('spid');
		
		$form = new Records_Form_PassportDetailForm(array('locale'=>$locale));	
		$form->populate(array('sp_id'=>$spid));
		$this->view->form = $form;
			
		if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();
                        //var_dump($formData); exit;
			$passportDB = new Records_Model_DbTable_StudentPassport();
			
			//check duplicate student pass
			$exist = $passportDB->checkDuplicate($formData['p_passport_no']);
	
			if(!$exist){
			
				if($formData['p_date_issue']!=''){
			    	$formData['p_date_issue']  = date('Y-m-d',strtotime( $formData['p_date_issue'] ));
			    }
			    
			    if($formData['p_expiry_date']!=''){
			    	$formData['p_expiry_date'] = date('Y-m-d',strtotime( $formData['p_expiry_date'] ));
			    }
			    
			  
				$formData['p_createddt']=date('Y-m-d H:i:s');
				$formData['p_createdby']=$auth->getIdentity()->id;
				
				unset($formData['MAX_FILE_SIZE']);
				
				
				$p_id = $passportDB->addData($formData);
				
				$this->multipleUpload($_FILES,$formData['sp_id'],$p_id,'student_passport');
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			}else{
				$this->_helper->flashMessenger->addMessage(array('error' => "Visa reference no already exist"));
			}
			$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'passport','spid'=>$formData['sp_id']),'default',true));
		}
		
	}
	
	public function editAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->_helper->layout->disableLayout();
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Edit");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$auth = Zend_Auth::getInstance();
    		
		$idPassport = $this->_getParam('id');
		$spid = $this->_getParam('spid');
		
		$passportDB = new Records_Model_DbTable_StudentPassport();
                $docDB = new Records_Model_DbTable_StudentDocuments();
		
		if ($this->getRequest()->isPost()) {			
			$formData = $this->getRequest()->getPost();
			
			 if($formData['p_date_issue']!=''){
		    	$formData['p_date_issue']  = date('Y-m-d',strtotime( $formData['p_date_issue'] ));
		    }
		    
		    if($formData['p_expiry_date']!=''){
		    	$formData['p_expiry_date'] = date('Y-m-d',strtotime( $formData['p_expiry_date'] ));
		    }
		    
			$formData['p_createddt']=date('Y-m-d H:i:s');
			$formData['p_createdby']=$auth->getIdentity()->id;
			
			unset($formData['MAX_FILE_SIZE']);
			$passportDB->updateData($formData,$formData['p_id']);
			
			$this->multipleUpload($_FILES,$formData['sp_id'],$formData['p_id'],'student_passport');
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'passport','spid'=>$formData['sp_id']),'default',true));
			
		}else{
			
			
		    $row = $passportDB->getDataById($idPassport);
                    $docList = $docDB->getDocuments($spid, 65, $idPassport, 'student_passport');
		    //var_dump($docList);
		    $form = new Records_Form_PassportDetailForm(array('idPassport'=>$idPassport));
		    
		    if($row['p_date_issue']!=''){
		    	$row['p_date_issue']  = date('d-m-Y',strtotime( $row['p_date_issue'] ));
		    }
		    
		    if($row['p_expiry_date']!=''){
		    	$row['p_expiry_date'] = date('d-m-Y',strtotime( $row['p_expiry_date'] ));
		    }
		    
		    $form->populate($row);
                    $this->view->form = $form;
                    $this->view->docList = $docList;
		
		}
	}
	
	public function studentPassAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Student Pass");		 
	
		$auth = Zend_Auth::getInstance();
                $this->view->role = $auth->getIdentity()->IdRole;
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$spid = $this->_getParam('spid');
		$this->view->spid = $spid;
		
		$pid = $this->_getParam('pid'); //passport id
		$this->view->pid = $pid;
		
		//get student info
		$studentProfileDB = new Records_Model_DbTable_Studentprofile();
		$studentInfo = $studentProfileDB->getStudentInfoBySpid($spid);      
        $this->view->studentInfo = $studentInfo;
		
         //get graduation date
        $this->view->graduation_date = $this->getGraduationDate($studentInfo);
        
        //get photo
       	$this->view->app_photo = $this->getPhoto($spid);
        	
		$studentVisaDB = new Records_Model_DbTable_StudentVisa();
		$student_pass = $studentVisaDB->getListDataBySpId($spid);
		
		if(count($student_pass)==1){
			//update $pid
			if($student_pass[0]['p_id']==''){
				$studentVisaDB->updateData(array('p_id'=>$pid),$student_pass[0]['av_id']);
			}
		}
		
		//get passport info
		$passportDB = new Records_Model_DbTable_StudentPassport();
		$this->view->passport = $passportDB->getDataById($pid);
		
		//get student_pass under pid
		$visa_list = $studentVisaDB->getStudentPassByPassportId($pid);
		
		$uploadfileDB = new Records_Model_DbTable_StudentDocuments();
		foreach($visa_list as $index=>$visa){
			//get documents
			$visa_list[$index]['visa'] = $uploadfileDB->getDocuments($spid,843,$visa['av_id'],'student_visa');
			$visa_list[$index]['bond'] = $uploadfileDB->getDocuments($spid,828,$visa['av_id'],'student_visa');
			$visa_list[$index]['val'] = $uploadfileDB->getDocuments($spid,829,$visa['av_id'],'student_visa');
			
		}
		$this->view->visa_list = $visa_list;
		
		//get active student pass
        $active = $studentVisaDB->getActiveVisa($spid);
        if($active && $active['ad_id']!=''){
	        //get active documents
			$active['visa'] = $uploadfileDB->getDocumentById($active['ad_id']);
        }
		$this->view->active = $active;	
	}
	
	public function addPassAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->_helper->layout->disableLayout();
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Add Student Pass");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$auth = Zend_Auth::getInstance();
    		
		$spid = $this->_getParam('spid');
		$pid = $this->_getParam('pid');
                //var_dump($spid);
                //var_dump($pid);
		//exit;
		$form = new Records_Form_VisaDetailForm(array('locale'=>$locale));	
		$form->populate(array('sp_id'=>$spid,'p_id'=>$pid));
		$this->view->form = $form;
			
		if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();
			/*var_dump($formData); 
                        var_dump($_FILES);
                        exit;*/
			$studentVisaDB = new Records_Model_DbTable_StudentVisa();
			
			//check duplicate student pass
			$last_visa = $studentVisaDB->getLastExpiryDate($formData['sp_id']);
			$new_issue_dt = date('Y-m-d',strtotime($formData['av_issue_date'])); //new DateTime($formData['av_issue_date']);
			$old_expire_dt = date('Y-m-d',strtotime($last_visa['av_expiry'])); //new DateTime($last_visa['av_expiry']);
                        //var_dump($new_issue_dt);
                        //var_dump($old_expire_dt);
			//exit;
                        //comment for allow overlapping
			//if( (!$last_visa) || (isset($last_visa) && ($new_issue_dt >= $old_expire_dt)) ){
				
				if($formData['av_issue_date']!=''){
		    		$formData['av_issue_date']  = date('Y-m-d',strtotime( $formData['av_issue_date'] ));
			    }
			    
			    if($formData['av_expiry']!=''){
			    	$formData['av_expiry'] = date('Y-m-d',strtotime( $formData['av_expiry'] ));
			    }
			    
				$formData['upd_date']=date('Y-m-d H:i:s');
				$formData['upd_by']=$auth->getIdentity()->id;
				
				unset($formData['MAX_FILE_SIZE']);				
				
				$av_id = $studentVisaDB->addData($formData);
				
				$this->multipleUpload($_FILES,$formData['sp_id'],$av_id,'student_visa');
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			/*}else{
				$this->_helper->flashMessenger->addMessage(array('error' => "Overlapping student pass date"));
			}*/
			
			$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'student-pass','spid'=>$formData['sp_id'],'pid'=>$formData['p_id']),'default',true));
		}
		
	}
	
	public function editPassAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->_helper->layout->disableLayout();
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Edit Student Pass");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$auth = Zend_Auth::getInstance();
    		
		$spid = $this->_getParam('spid');
		$pid = $this->_getParam('pid'); //passport id
		$avid = $this->_getParam('avid');
		
		$studentVisaDB = new Records_Model_DbTable_StudentVisa();
                $docDB = new Records_Model_DbTable_StudentDocuments();
		
		if ($this->getRequest()->isPost()) {			
			$formData = $this->getRequest()->getPost();
			
			if($formData['av_issue_date']!=''){
		    	$formData['av_issue_date']  = date('Y-m-d',strtotime( $formData['av_issue_date'] ));
		    }
		    
		    if($formData['av_expiry']!=''){
		    	$formData['av_expiry'] = date('Y-m-d',strtotime( $formData['av_expiry'] ));
		    }
		    
			$formData['upd_date']=date('Y-m-d H:i:s');
			$formData['upd_by']=$auth->getIdentity()->id;
			
			unset($formData['MAX_FILE_SIZE']);
			$studentVisaDB->updateData($formData,$formData['av_id']);
			
			$this->multipleUpload($_FILES,$formData['sp_id'],$formData['av_id'],'student_visa');
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'student-pass','spid'=>$formData['sp_id'],'pid'=>$formData['p_id']),'default',true));
			
		}else{
			
			
			$row = $studentVisaDB->getDataById($avid);
                        //$docList = $docDB->getDocuments($spid, 65, $idPassport, 'student_passport');
                        $visa = $docDB->getDocuments($spid,843,$avid,'student_visa');
			$bond = $docDB->getDocuments($spid,828,$avid,'student_visa');
			$val = $docDB->getDocuments($spid,829,$avid,'student_visa');
                        
                        $this->view->visa = $visa;
                        $this->view->bond = $bond;
                        $this->view->val = $val;
			
		 	if($row['av_issue_date']!=''){
		    	$row['av_issue_date']  = date('d-m-Y',strtotime( $row['av_issue_date'] ));
		    }
		    
		    if($row['av_expiry']!=''){
		    	$row['av_expiry'] = date('d-m-Y',strtotime( $row['av_expiry'] ));
		    }
			    
			$form = new Records_Form_VisaDetailForm(array('locale'=>$locale,'avid'=>$avid));
			$form->populate($row);
			$this->view->form = $form;
		}
	}
	
	
	public function dependentPassAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Dependent Pass");		 
	
		$auth = Zend_Auth::getInstance();
                $this->view->role = $auth->getIdentity()->IdRole;
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$spid = $this->_getParam('spid');		
		$this->view->spid = $spid;
		
		$avid = $this->_getParam('avid'); //visa@student pass id
		$this->view->avid = $avid;
		
		$pid = $this->_getParam('pid'); //passport id
		$this->view->pid = $pid;
		
		//get student info
		$studentProfileDB = new Records_Model_DbTable_Studentprofile();
		$studentInfo = $studentProfileDB->getStudentInfoBySpid($spid);      
        $this->view->studentInfo = $studentInfo;
		
         //get graduation date
        $this->view->graduation_date = $this->getGraduationDate($studentInfo);
        
        //get photo
       	$this->view->app_photo = $this->getPhoto($spid);
       	
       	//get passport info
		$passportDB = new Records_Model_DbTable_StudentPassport();
		$this->view->passport = $passportDB->getDataById($pid);
		
		//get visa
		$studentVisaDB = new Records_Model_DbTable_StudentVisa();
		$this->view->visa = $studentVisaDB->getDataById($avid);
       	
		$dependentPassDB = new Records_Model_DbTable_StudentDependentPass();
		$dependent_pass = $dependentPassDB->getListDataByAvId($avid);
		
		$uploadfileDB = new Records_Model_DbTable_StudentDocuments();
		foreach($dependent_pass as $index=>$pass){
			//get documents
			$dependent_pass[$index]['passport'] = $uploadfileDB->getDocuments($spid,65,$pass['sdp_id'],'student_dependent_pass');
			$dependent_pass[$index]['pass'] = $uploadfileDB->getDocuments($spid,830,$pass['sdp_id'],'student_dependent_pass');
			
		}
		$this->view->dependent_list = $dependent_pass;
		
		
	}
	
	
	public function addDependentAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->_helper->layout->disableLayout();
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Add Dependent Pass");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$auth = Zend_Auth::getInstance();
    		
		$spid = $this->_getParam('spid');
		$avid = $this->_getParam('avid');
		$pid = $this->_getParam('pid');
		
		$form = new Records_Form_DependentPassForm(array('locale'=>$locale));	
		$form->populate(array('sp_id'=>$spid,'av_id'=>$avid,'p_id'=>$pid));
		$this->view->form = $form;
			
		if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();
	
			//check duplicate dependent pass
			$dependentPassDB = new Records_Model_DbTable_StudentDependentPass();
			$exist = $dependentPassDB->checkDuplicate($formData['sdp_passport_no']);
	
			if(!$exist){
				
				if($formData['sdp_date_issue']!=''){
		    	$formData['sdp_date_issue']  = date('Y-m-d',strtotime( $formData['sdp_date_issue'] ));
			    }
			    
			    if($formData['sdp_expiry_date']!=''){
			    	$formData['sdp_expiry_date'] = date('Y-m-d',strtotime( $formData['sdp_expiry_date'] ));
			    }
			    
				if($formData['sdp_dob']!=''){
			    	$formData['sdp_dob'] = date('Y-m-d',strtotime( $formData['sdp_dob'] ));
			    }
			    
				$formData['sdp_createddt']=date('Y-m-d H:i:s');
				$formData['sdp_createdby']=$auth->getIdentity()->id;
				
				unset($formData['MAX_FILE_SIZE']);				
				
				$sdp_id = $dependentPassDB->addData($formData);
				
				$this->multipleUpload($_FILES,$formData['sp_id'],$sdp_id,'student_dependent_pass');
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			}else{
				$this->_helper->flashMessenger->addMessage(array('error' => "Dependent Passport No already exist"));
			}
			
			$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'dependent-pass','spid'=>$formData['sp_id'],'avid'=>$formData['av_id'],'pid'=>$formData['p_id']),'default',true));
		}
		
	}
	
	
	public function editDependentAction() {
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->_helper->layout->disableLayout();
		
		//title
		$this->view->title= $this->view->translate("Visa Details : Add Dependent Pass");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$auth = Zend_Auth::getInstance();
    		
		$spid = $this->_getParam('spid');
		$sdpid = $this->_getParam('sdpid'); //passport id
		$avid = $this->_getParam('avid');
		$pid = $this->_getParam('pid');
		
		$dependentPassDB = new Records_Model_DbTable_StudentDependentPass();
                $docDB = new Records_Model_DbTable_StudentDocuments();
		
		if ($this->getRequest()->isPost()) {			
			$formData = $this->getRequest()->getPost();
			
			if($formData['sdp_date_issue']!=''){
		    	$formData['sdp_date_issue']  = date('Y-m-d',strtotime( $formData['sdp_date_issue'] ));
		    }
		    
		    if($formData['sdp_expiry_date']!=''){
		    	$formData['sdp_expiry_date'] = date('Y-m-d',strtotime( $formData['sdp_expiry_date'] ));
		    }
		    
			if($formData['sdp_dob']!=''){
		    	$formData['sdp_dob'] = date('Y-m-d',strtotime( $formData['sdp_dob'] ));
		    }
			
			unset($formData['MAX_FILE_SIZE']);
			$dependentPassDB->updateData($formData,$formData['sdp_id']);
			
			$this->multipleUpload($_FILES,$formData['sp_id'],$formData['sdp_id'],'student_dependent_pass');
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'dependent-pass','spid'=>$formData['sp_id'],'avid'=>$formData['av_id'],'pid'=>$formData['p_id']),'default',true));
			
		}else{
			
			
			$row = $dependentPassDB->getDataById($sdpid);
                        
                        $passport = $docDB->getDocuments($spid,65,$sdpid,'student_dependent_pass');
			$pass = $docDB->getDocuments($spid,830,$sdpid,'student_dependent_pass');
                        $this->view->passport = $passport;
                        $this->view->pass = $pass;
			
			if($row['sdp_date_issue']!=''){
		    	$row['sdp_date_issue']  = date('d-m-Y',strtotime( $row['sdp_date_issue'] ));
		    }
		    
		    if($row['sdp_expiry_date']!=''){
		    	$row['sdp_expiry_date'] = date('d-m-Y',strtotime( $row['sdp_expiry_date'] ));
		    }
		    
			if($row['sdp_dob']!=''){
		    	$row['sdp_dob'] = date('d-m-Y',strtotime( $row['sdp_dob'] ));
		    }
			    
			$form = new Records_Form_DependentPassForm(array('locale'=>$locale,'sdpid'=>$row['sdp_id']));	
			$form->populate($row);
			$this->view->form = $form;
		
		}
	}
	
   function getPhoto($spid){
       
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
       
		$studentProfileDB = new Records_Model_DbTable_Studentprofile();
		$photo = $studentProfileDB->getStudentPhoto($spid);
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo))
        {
                $this->view->app_photo_full = DOCUMENT_URL.$photo['ad_filepath'];
                $app_photo = $this->view->baseUrl().'/thumb.php?s=1&w=100&f='.APP_DOC_PATH.$photo['ad_filepath'];
        }
      
        return $app_photo;
	}
	
	function multipleUpload($files_upl,$sp_id,$primary_id,$tbl_name){
            
            //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
				
			//Passport => 65
			//Bond =>828
			//VAL => 829
		
			$auth = Zend_Auth::getInstance(); 
			
		    //get transaction repository path
		    $studentProfileDB =  new Records_Model_DbTable_Studentprofile();
            $profile_data = $studentProfileDB->getData($sp_id);    	        
    		 
           $uploadfileDB = new Records_Model_DbTable_StudentDocuments();
         	 			
           //foreach file name
		   foreach ($files_upl as $name => $attributes) {	 				
 				
 				for ($i=0; $i<count($files_upl[$name]['name']); $i++){
				 		 									 		
				 		if(isset($name) && $name!=''){
				 			
				 			list($var,$type_id)= explode("_",$name);
				 		
				 			if (is_uploaded_file($files_upl[$name]['tmp_name'][$i])){
		        				
		        				$validImage = $this->isValidateFileType($files_upl[$name]["type"][$i]);
		        				
			        			if($validImage){
			        				
			        				if(!file_exists($profile_data['sp_repository'])){		        					
			        					$this->createDir($profile_data['sp_repository']);
			        				}
					        					
			        				$filetype = $files_upl[$name]["type"][$i];
									$ori_filename = strtolower($files_upl[$name]["name"][$i]);
									$rename_filename = date('Ymdhs')."_".$ori_filename;							
									$path_file = $profile_data['sp_repository'].'/'.$rename_filename;						
									move_uploaded_file($files_upl[$name]['tmp_name'][$i], DOCUMENT_PATH.$path_file);
									
									$upload_file = array(
										'sp_id' => $sp_id,
										'ad_dcl_id' => 0,
										'ad_section_id' => 0,
										'ad_type' =>$type_id,
										'ad_table_name' => $tbl_name,
                                                                                'ad_table_id' => $primary_id,
										'ad_filename' => $rename_filename, 
                                                                                'ad_ori_filename' => $ori_filename, 
										'ad_filepath' => $path_file, 
										'ad_createddt' => date("Y-m-d h:i:s"),
										'ad_createdby' => $auth->getIdentity()->iduser,
										'ad_filetype' => $filetype, 
									);
									$uploadfileDB->addData($upload_file);	
										
			        			}else{
			        				//throw new Exception('Image file type only');
			        			}
							
		    	        	}//end if is_uploaded
				 		}
				  } 							
			}
				      
	}
	
	
	function upload($files,$sp_id,$primary_id,$tbl_name){
		
            
            //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		//Passport => 65
		//Bond =>828
		//VAL => 829
		
			$auth = Zend_Auth::getInstance(); 
			
		    //get transaction repository path
		    $studentProfileDB =  new Records_Model_DbTable_Studentprofile();
            $profile_data = $studentProfileDB->getData($sp_id);    	        
    		
           // $files = (is_array($file)) ? $file : array();
	    	 
           $uploadfileDB = new Records_Model_DbTable_StudentDocuments();
            
	       foreach ($files as $name => $attributes) {	  
					
	       		list($var,$type_id)= explode("_",$name);	  
	       		      	
	        	if(isset($files[$name]['name'])){
	        			
	        			if (is_uploaded_file($files[$name]['tmp_name'])){
	        				
	        				$validImage = $this->isValidateFileType($files[$name]["type"]);
	        				
		        			if($validImage){
		        				
		        				if(!file_exists($profile_data['sp_repository'])){		        					
		        					$this->createDir($profile_data['sp_repository']);
		        				}
	
		        				//delete previous data
		        				$document = $uploadfileDB->getDocument($sp_id,$type_id,$primary_id,$tbl_name);
		        				if($document){
		        					$this->deleteDocuments($document);
		        				}
		        				
								$ori_filename = strtolower($files[$name]["name"]);
								$rename_filename = date('Ymdhs')."_".$ori_filename;							
								$path_file = $profile_data['sp_repository'].'/'.$rename_filename;						
								move_uploaded_file($files[$name]['tmp_name'], DOCUMENT_PATH.$path_file);
								
								$upload_file = array(
									'sp_id' => $sp_id,
									'ad_dcl_id' => 0,
									'ad_section_id' => 0,
									'ad_type' =>$type_id,
									'ad_table_name' => $tbl_name,
								    'ad_table_id' => $primary_id,
									'ad_filename' => $rename_filename, 
								    'ad_ori_filename' => $ori_filename, 
									'ad_filepath' => $path_file, 
									'ad_createddt' => date("Y-m-d h:i:s"),
									'ad_createdby' => $auth->getIdentity()->iduser
								);
								
								
								$uploadfileDB->addData($upload_file);	
									
		        			}else{
		        				//throw new Exception('Image file type only');
		        			}
						
	    	        	}//end if is_uploaded
            	
          			}//end if
	       
	    	}//end foreach 
	      
	}
	
	function deleteDocuments($doc){
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
                
			 $uploadfileDB = new Records_Model_DbTable_StudentDocuments();
			
			if(file_exists(DOCUMENT_PATH.$doc['ad_filepath'])){
			  	unlink(DOCUMENT_PATH.$doc['ad_filepath']);
			}
  			//delete document info dtbase
  			$uploadfileDB->deleteData('ad_id='.$doc['ad_id']);	 
	}	
	
	public function getPassportAction(){
            
            //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
	
		$id = $this->_getParam('id',null);
		 
		$this->_helper->layout->disableLayout();
	
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		$db = Zend_Db_Table::getDefaultAdapter();
       
	    $passportDB = new Records_Model_DbTable_StudentPassport();
	    $row = $passportDB->getDataById($id);
	
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($row);
	
		echo $json;
		exit();
	}
	
	public function changeStatusAction(){
	
            //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();
	
			$type = $formData['type']; //1:passport 2:student pass 3: dependent pass
									
			$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
			
			if($type==3){
				
				$dependentPassDB = new Records_Model_DbTable_StudentDependentPass();
				
				$sdp_id = $formData['id'];
				
				//get dependent pass info
				$info = $dependentPassDB->getDataById($sdp_id);
				
				//name&relationship is unique
				//each person can only have one active dependent pass
				$where="sdp_name = '".$info['sdp_name']."' AND sdp_dob='".$info['sdp_dob']."' AND sdp_id!='".$sdp_id."'";
				$dependentPassDB->updateStatus(array('sdp_active'=>0),$where);		
				$dependentPassDB->updateData(array('sdp_active'=>1),$formData['id']);
				
				$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'dependent-pass','spid'=>$formData['spid'],'pid'=>$formData['pid'],'avid'=>$formData['avid']),'default',true));
			}else if($type==2){
				
				$studentVisaDB = new Records_Model_DbTable_StudentVisa();
				$studentVisaDB->updateData(array('av_active'=>0),$formData['aid']);		
				$studentVisaDB->updateData(array('av_active'=>1),$formData['id']);
			
				$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'student-pass','spid'=>$formData['spid'],'pid'=>$formData['pid']),'default',true));
			}else{
				
				$passportDB = new Records_Model_DbTable_StudentPassport();
				$passportDB->updateData(array('p_active'=>0),$formData['aid']);		
				$passportDB->updateData(array('p_active'=>1),$formData['id']);
			
				$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'passport','spid'=>$formData['spid']),'default',true));
			}
		
		}
				
	}
	
  function isValidateFileType($file){
   
        //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
      
		$fileType = strtolower($file);
     	$allowedImageTypes = array( "image/pjpeg","image/jpeg","image/jpg","image/png","image/x-png","image/gif","application/pdf");
 	 	
     	if (!in_array($fileType, $allowedImageTypes)) 
	    {
	        return false;
	    }
	  
	    return true;
  }

  function createDir($repository){
      
      //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
  	
	  	$dir_array = explode('/',$repository);
	  	
		$student_path = DOCUMENT_PATH.'/student/'.$dir_array[2];
		
	    //create directory to locate fisle			
		if (!is_dir($student_path)) {
	    	mkdir($student_path, 0775);
		}    			
				
		$txn_path = $student_path."/".$dir_array[3];
		
	    //create directory to locate file			
		if (!is_dir($txn_path)) {
	    	mkdir($txn_path, 0775);
		}

  }
  
  function deleteAttachmentAction(){
      
      //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
  	
  		 $ad_id = $this->_getParam('ad_id');
  		
  		if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();
			
			$type = $formData['type']; //1:passport 2:student pass 3: dependent pass
			
			$uploadfileDB = new Records_Model_DbTable_StudentDocuments();
  			$document = $uploadfileDB->getDocumentById($ad_id);
  			
  			$this->deleteDocuments($document);
	
  			$this->_helper->flashMessenger->addMessage(array('success' => "Document has been deleted"));
  			
			if($type==1){
				$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'passport','spid'=>$formData['spid']),'default',true));
			}else if($type==2){
				$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'student-pass','spid'=>$formData['spid'],'pid'=>$formData['pid']),'default',true));
			}else if($type==3){  		
				$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'dependent-pass','spid'=>$formData['spid'],'pid'=>$formData['pid'],'avid'=>$formData['avid']),'default',true));
			}
			
  		} 		
  		
  }
  
		  function getGraduationDate($studentInfo){
                      
                      //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		  	      
		                $duration = '+'.$studentInfo['MinYear'].' year +'.$studentInfo['MinMonth'].' month';
		                
		                if($studentInfo['IdSemesterMain']!=0){
		                	   $CurrentSemester  = new GeneralSetup_Model_DbTable_Semestermaster();
		                	   $current_semester = $CurrentSemester->fnGetSemestermaster($studentInfo['IdSemesterMain']);
		              
		                	   $date = date("Y-m-d",strtotime($current_semester['SemesterMainStartDate']));
		                }else{
		                	   $date = date("Y-m-d");// current date
		                }
		             
		                //$finish_month = date('F Y',strtotime("+$duration month",$RegistrationDate));
						$finish_month = date('F Y',strtotime(date("Y-m-d", strtotime($date)) . $duration));
						
						return $finish_month;
		
		  }
  
		function activeAttachmentAction(){
                    
                    //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
	  	
	  		 $ad_id = $this->_getParam('ad_id');
	  		
	  		if ($this->getRequest()->isPost()) {			
				
				$formData = $this->getRequest()->getPost();
				
				
				$type = $formData['type']; //1:passport 2:student pass 3: dependent pass
								
	  			$this->_helper->flashMessenger->addMessage(array('success' => "Document has been deleted"));
	  			
				if($type==1){
					
					$passportDB = new Records_Model_DbTable_StudentPassport();
					$passportDB->updateData(array('ad_id'=>$ad_id),$formData['aid']);
					 
					$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'passport','spid'=>$formData['spid']),'default',true));
					
				}else if($type==2){
					
					$studentVisaDB = new Records_Model_DbTable_StudentVisa();
					$studentVisaDB->updateData(array('ad_id'=>$ad_id),$formData['aid']);
					
					$this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'student-pass','spid'=>$formData['spid'],'pid'=>$formData['pid']),'default',true));
					
				}
				
	  		} 		
	  		
	  }
	
          public function deleteStudentPassportAction(){
              $id = $this->_getParam('id', 0);
              $spid = $this->_getParam('spid', 0);
              //var_dump($id); exit;
              $studentVisaDB = new Records_Model_DbTable_StudentVisa();
              
              if ($id != 0){
                  //delete student passport
                  $studentVisaDB->deleteStudentPassport($id);
              }
              
              $this->_helper->flashMessenger->addMessage(array('success' => "Data has been deleted"));
              $this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'passport','spid'=>$spid),'default',true));
              
              exit;
          }
          
          public function deleteStudentPassAction(){
              $id = $this->_getParam('id', 0);
              $spid = $this->_getParam('spid', 0);
              $pid = $this->_getParam('pid', 0);
              //var_dump($id); exit;
              $studentVisaDB = new Records_Model_DbTable_StudentVisa();
              
              if ($id != 0){
                  //delete student passport
                  $studentVisaDB->deleteStudentPass($id);
              }
              
              $this->_helper->flashMessenger->addMessage(array('success' => "Data has been deleted"));
              $this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'student-pass','spid'=>$spid, 'pid'=>$pid),'default',true));
              
              exit;
          }
          
          public function deleteStudentDependentPassAction(){
              $id = $this->_getParam('id', 0);
              $spid = $this->_getParam('spid', 0);
              $pid = $this->_getParam('pid', 0);
              $avid = $this->_getParam('avid', 0);
              //var_dump($id); exit;
              $studentVisaDB = new Records_Model_DbTable_StudentVisa();
              
              if ($id != 0){
                  //delete student passport
                  $studentVisaDB->deleteStudentDependentPass($id);
              }
              
              $this->_helper->flashMessenger->addMessage(array('success' => "Data has been deleted"));
              $this->_redirect($this->view->url(array('module'=>'records','controller'=>'visa', 'action'=>'dependent-pass', 'avid'=>$avid, 'spid'=>$spid, 'pid'=>$pid),'default',true));
              
              exit;
          }
}

?>