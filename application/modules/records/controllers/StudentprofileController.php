<?php

class Records_StudentprofileController extends Base_Base
{ //Controller for the User Module

    private $lobjStudentprofileForm;
    private $lobjStudentprofile;
    private $lobjUser;
    private $_gobjlog;
    private $otherDocModel;

    public function init()
    { //initialization function
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->fnsetObj();
    }

    public function fnsetObj()
    {

        $this->lobjStudentprofileForm = new Records_Form_Studentprofile();
        $this->lobjStudentprofile = new Records_Model_DbTable_Studentprofile();
        $this->lobjUser = new GeneralSetup_Model_DbTable_User();
        $this->lobjLandscape = new Records_Model_DbTable_Studentprofile();
        $this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
        $this->studentprofileForm = new Registration_Form_Studentprofile();
        $this->lobjdeftype = new App_Model_Definitiontype();
        $this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
        $this->lsemesterModel = new GeneralSetup_Model_DbTable_Semester();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->otherDocModel = new Records_Model_DbTable_OtherDocument();
    }

    public function indexAction()
    {

        $this->view->title = "Student Profile";

        $studentRegDB = new Registration_Model_DbTable_Studentregistration();
        $objdeftypeDB = new App_Model_Definitiontype();
        $form = new Registration_Form_SearchStudent();
        $db = getDb();

        $query = $studentRegDB->getListStudent();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $query = $studentRegDB->getListStudent($formData, NULL, 0);

            $this->gobjsessionsis->studentprofilefilter = $formData;
            $this->gobjsessionsis->studentprofilequery = $query;
            $form->populate($formData);
            $this->_redirect($this->baseUrl . '/records/studentprofile/index/search/1');
        }

        if ($this->_getparam('search', 0) && isset($this->gobjsessionsis->studentprofilequery)) {
            $query = $this->gobjsessionsis->studentprofilequery;
        } else {
            unset($this->gobjsessionsis->studentprofilequery);
            unset($this->gobjsessionsis->studentprofilefilter);
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage(50);
        $totalRecord = $paginator->getTotalItemCount();

        //status
        $status = $objdeftypeDB->fnGetDefinations('Profile Status');
        $this->view->statusCount = [];
        foreach ($status as $sts) {
            $getTotal = $db->query($db->quoteInto('SELECT IdStudentRegistration FROM tbl_studentregistration WHERE profileStatus=?', $sts['idDefinition']))->rowCount();
            $this->view->statusCount[$sts['DefinitionDesc']] = $getTotal;

        }

        //intake
        $this->view->intakeCount = [];
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $listData = $intakeDb->fngetIntakeList();
        foreach ($listData as $list) {
            $getTotal = $db->query($db->quoteInto('SELECT IdStudentRegistration FROM tbl_studentregistration WHERE IdIntake=?', $list['IdIntake']))->rowCount();
            $this->view->intakeCount[$list['IntakeDesc']] = $getTotal;
        }


        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
        $this->view->form = $form;

    }

    public function studentprofilelistAction()
    { //Action for the updation and view of the  details

        $this->view->title = $this->view->translate("Student Record Detail");

        $registrationId = $this->_getParam('id');
        $this->view->registrationId = $registrationId;
        $semMasterObj = new GeneralSetup_Model_DbTable_Semestermaster();
        if ($this->getRequest()->isPost()) {
            $auth = Zend_Auth::getInstance();
            $dballowreg = new Records_Model_DbTable_Allowsubjectregis();
            $formData = $this->getRequest()->getPost();
            $data["IdStudentRegistration"] = $registrationId;
            $data["allowedBy"] = $auth->getIdentity()->id;
            $dballowreg->cleanallowsubject($registrationId);
            foreach ($formData["allowreg"] as $asubject) {
                $data["IdSubject"] = $asubject;
                $dballowreg->allowsubject($data);
            }
            $this->_redirect($this->baseUrl . '/records/studentprofile/studentprofilelist/id/' . $registrationId . '#Tab5');
        }



        //get student info
        $studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
        $studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($registrationId);

        $this->view->studentdetails = $studentdetails;

        $auth = Zend_Auth::getInstance();
        $this->view->IdRole = $auth->getIdentity()->IdRole;
        $staffInfo = $studentRegistrationDB->staffinfo($auth->getIdentity()->IdStaff);
        $this->view->staffInfo = $staffInfo;

        $photo = $this->lobjStudentprofile->getStudentPhoto($studentdetails['sp_id']);
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . urlencode($photo['ad_filepath']);
        }

        $this->view->app_photo = $app_photo;

        // display name for sysmester syllabus chosen
        $IdSemesterMain = $studentdetails['IdSemesterMain'];
        $IdSemesterDetails = $studentdetails['IdSemesterDetails'];

        if ($IdSemesterMain != '' && $IdSemesterMain != 0) {

            $semResultData = $semMasterObj->fngetSemestermainDetails($IdSemesterMain);
            $this->view->semesterNameData = $semResultData[0]['SemesterMainName'];
        }

        if ($IdSemesterDetails != '' && $IdSemesterDetails != 0) {
            $semDetailObj = new GeneralSetup_Model_DbTable_Semester();
            $semResultData = $semDetailObj->fngetsemdetailByid($IdSemesterDetails);
            $this->view->semesterNameData = $semResultData[0]['SemesterCode'];
        }

        //cek landscape
        $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
        $landscape = $landscapeDb->getData($studentdetails["IdLandscape"]);
        //var_dump($landscape);
        $this->view->landscape = $landscape;

        if ($landscape["LandscapeType"] == 43 || $landscape["LandscapeType"] == 44 || $landscape["LandscapeType"] == 42) {//Semester Based

            //get total registered semester for course registration
            $studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
            $semester = $studentSemesterDB->getRegisteredSemester($registrationId);
            $this->view->semester = $semester;

            //GET CURRENT Semester
            $ProgramScheme = new GeneralSetup_Model_DbTable_Programscheme();
            $program_scheme = $ProgramScheme->getSchemeById($landscape['IdProgramScheme']);

            $curr_semester = $semMasterObj->getCurrentSemesterScheme($program_scheme['IdScheme']);
            //dd($curr_semester); exit;
            if (!isset($curr_semester['SemName']) || $curr_semester['SemName'] == '') {
                $this->_redirect($this->baseUrl . '/records/studentprofile/error/');
            }

            $this->view->currSemester = $curr_semester;

            //Academic Progress
            $landscapeId = $studentdetails["IdLandscape"];
            $this->view->landscapeId = $landscapeId;
            $this->view->landscapeType = $landscape["LandscapeType"];
            $programId = $studentdetails["IdProgram"];
            $this->view->programId = $programId;

            //get landscape info
            $landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
            $landscape = $landscapeDB->getLandscapeDetails($landscapeId);
            $this->view->landscape = $landscape;

            //get program requirement info
            $progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
            $programRequirement = $progReqDB->getlandscapecoursetype($programId, $landscapeId);
            $this->view->programrequirement = $programRequirement;
            //var_dump($programRequirement);
            //get majoring for this program
            $progMajDb = new GeneralSetup_Model_DbTable_ProgramMajoring();
            $majoring = $progMajDb->getData($programId);
            $this->view->majoring = $majoring;


            //get Compulsory Common Course
            $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
            $compulsory_course = $landscapeSubjectDb->getCommonCourse($programId, $landscapeId, 1);
            $this->view->compulsory_course = $compulsory_course;

            //get Not Compulsory Common Course
            $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
            $elective_course = $landscapeSubjectDb->getCommonCourse($programId, $landscapeId, 2);
            $this->view->elective_course = $elective_course;

            //$semesterAsc = $studentSemesterDB->getRegisteredSemester($registrationId,'Asc');

            //echo "<pre>";print_r($landscape);echo "</pre>";
            /**
             ** GET SUBJECT FOR ACADEMIC PROGRESS
             **/
            if ($this->view->landscapeType == 43 || $this->view->landscapeType == 42) {
                // GET ALL SUBJECT TAKEN SORT BY DATE DESC
                //$courseRegisterDb = new Registration_Model_DbTable_Studentregistration();
                //$courses = $courseRegisterDb->getCourseRegisteredByAllSemester($registrationId);
                $recordDb = new Records_Model_DbTable_Academicprogress();
                $courses = $recordDb->getAllregistered($registrationId);

                if ($this->view->landscapeType == 43) {
                    $compulsory['completed'] = [];
                    $compulsory['registered'] = [];
                    $compulsory['open'] = [];

                    $elective['completed'] = [];
                    $elective['registered'] = [];
                    $elective['open'] = [];

                    $majoring['completed'] = [];
                    $majoring['registered'] = [];
                    $majoring['open'] = [];

                    $research = [];

                } else {
                    $compulsory = [];
                    $elective = [];
                    $research = [];
                }
                $subject_ids = [];

                $z = 0;
                $p = 0;
                $q = 0;
                $r = 0;
                $countCtCredit = 0;
                $countExCredit = 0;
                $countCompletePaperCore = 0;
                $countCompletePaperElective = 0;
                $countCompletePaperMajoring = 0;
                $countCompleteCreditHoursCore = 0;
                $countCompleteCreditHoursElective = 0;
                $countCompleteCreditHoursMajoring = 0;
                $countCreditHoursCore = 0;
                $countCreditHoursElective = 0;
                $countCreditHoursMajoring = 0;
                $countCreditHoursCore2 = 0;
                $countCreditHoursElective2 = 0;
                $cum_elective_ch = 0;
                $cum_majoring_ch = 0;
                $researchGred = '-';
                $researchCreditHours = 0;
                $researchRemarks = '';
                $countCreditHoursResearch = 0;

                $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
                //echo "<pre>";print_r($courses);echo "</pre>";
                //var_dump($courses); exit;
                foreach ($courses as $a => $course) {
                    $subsem = $recordDb->getSemesterById($course['IdSemesterMain']);

                    $publishresult = false;

                    if ($subsem) {
                        $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                        if ($publishInfo) {
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                            $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                $publishresult = true;
                            }
                        }
                    }

                    $equivid = false;
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $course['IdSubject']);
                    if (!$course_status) {
                        $equivid = $recordDb->checkEquivStatus($landscapeId, $course["IdSubject"]);

                        if ($equivid) {
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $equivid);
                        }
                    }

                    $equivsubid = $recordDb->checkEquivStatus($landscapeId, $course["IdSubject"]);

                    if ($equivsubid) {
                        $subject_ids[$q] = $equivsubid;
                        $q++;
                    }

                    //$getRegisteredCompleteCourses;
                    if ($this->view->landscapeType == 43) {
                        //var_dump($course);
                        //var_dump($course_status);
                        if ($course_status['DefinitionDesc'] == "Core") {
                            //var_dump($course);
                            //$completed=$recordDb->checkcompleted($studentdetails['IdStudentRegistration'],$course["IdSubject"]);
                            //$others=$recordDb->checkregistered($studentdetails['IdStudentRegistration'],$course["IdSubject"]);
                            if (($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U"))//Complete
                            {
                                //echo "<pre>";print_r($course);echo "</pre><hr>";
                                $compulsory['completed'][$z] = $course;
                                $compulsory['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $compulsory['completed'][$z]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $compulsory['completed'][$z]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    if ($equivid) {
                                        $subject_ids[$q] = $equivid;
                                    }

                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 394) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursCore = $ch + $countCreditHoursCore;
                                            } else {
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursCore = $countCompleteCreditHoursCore + $ch2;
                                        $countCompletePaperCore++;
                                    }
                                }

                                $z++;
                            } else if ($course["exam_status"] == "C") {
                                $compulsory['completed'][$z] = $course;
                                $compulsory['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $compulsory['completed'][$z]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $compulsory['completed'][$z]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    if ($equivid) {
                                        $subject_ids[$q] = $equivid;
                                    }

                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 394) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursCore = $ch + $countCreditHoursCore;
                                            } else {
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursCore = $countCompleteCreditHoursCore + $ch2;
                                        $countCompletePaperCore++;
                                    }
                                }

                                $z++;
                            } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                                $compulsory['registered'][$z] = $course;
                                $compulsory['registered'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"] == "U") {
                                    $compulsory['registered'][$z]['CreditHours'] = 0;
                                    $compulsory['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 394) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursCore = $ch + $countCreditHoursCore;
                                            } else {
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
                            }
                        } else if ($course_status['DefinitionDesc'] == "Elective" && $course['CourseType'] != 3) {
                            //var_dump($course);
                            if (($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == 'U'))//Complete
                            {
                                //echo "<pre>";print_r($course);echo "</pre><hr>";
                                $elective['completed'][$p] = $course;
                                $elective['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $elective['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $elective['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_elective_ch = $cum_elective_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 275) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursElective = $ch + $countCreditHoursElective;
                                            } else {
                                                $countCreditHoursElective++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursElective = $countCompleteCreditHoursElective + $ch2;
                                        $countCompletePaperElective++;
                                    }
                                }

                                $p++;
                            } else if ($course["exam_status"] == "C") {
                                $elective['completed'][$p] = $course;
                                $elective['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $elective['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $elective['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_elective_ch = $cum_elective_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 275) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursElective = $ch + $countCreditHoursElective;
                                            } else {
                                                $countCreditHoursElective++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursElective = $countCompleteCreditHoursElective + $ch2;
                                        $countCompletePaperElective++;
                                    }
                                }

                                $p++;
                            } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                                //var_dump($course);
                                $elective['registered'][$z] = $course;
                                $elective['registered'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"] == "U") {
                                    $elective['registered'][$z]['CreditHours'] = 0;
                                    $elective['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 275) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursElective = $ch + $countCreditHoursElective;
                                            } else {
                                                $countCreditHoursElective++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
                            }
                        } else if ($course_status['DefinitionDesc'] == "Majoring" && $course['CourseType'] != 3) {
                            //var_dump($course);
                            if (($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == 'U'))//Complete
                            {
                                //echo "<pre>";print_r($course);echo "</pre><hr>";
                                $majoring['completed'][$p] = $course;
                                $majoring['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $majoring['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $majoring['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_majoring_ch = $cum_majoring_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 1054) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursMajoring = $ch + $countCreditHoursMajoring;
                                            } else {
                                                $countCreditHoursMajoring++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursMajoring = $countCompleteCreditHoursMajoring + $ch2;
                                        $countCompletePaperMajoring++;
                                    }
                                }

                                $p++;
                            } else if ($course["exam_status"] == "C") {
                                $majoring['completed'][$p] = $course;
                                $majoring['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $majoring['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $majoring['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_majoring_ch = $cum_majoring_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 1054) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursMajoring = $ch + $countCreditHoursMajoring;
                                            } else {
                                                $countCreditHoursMajoring++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursMajoring = $countCompleteCreditHoursMajoring + $ch2;
                                        $countCompletePaperMajoring++;
                                    }
                                }

                                $p++;
                            } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                                //var_dump($course);
                                $majoring['registered'][$z] = $course;
                                $majoring['registered'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"] == "U") {
                                    $majoring['registered'][$z]['CreditHours'] = 0;
                                    $majoring['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 1054) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursMajoring = $ch + $countCreditHoursMajoring;
                                            } else {
                                                $countCreditHoursMajoring++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
                            }
                        } else if ($course_status['DefinitionDesc'] == "Research" || $course['CourseType'] == 3) {
                            if ($researchGred == '-') {
                                if ($course['exam_status'] != 'IP' && $course['exam_status'] != '') {
                                    if ($course['mark_approval_status'] == 2) {
                                        $researchGred = $course['grade_name'];
                                        $researchRemarks = '';
                                    } else {
                                        $researchRemarks = 'In Progress';
                                    }
                                } else {
                                    $researchRemarks = 'In Progress';
                                }
                            }

                            //count credit hours
                            if ($course["exam_status"] != "U") {
                                $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : 0;
                                $researchCreditHours = $researchCreditHours + $ch2;
                                if ($course["exam_status"] != null && $course['mark_approval_status'] == 2) {
                                    if ($course_status['DefinitionDesc'] == "Elective") {
                                        $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : 0;
                                        $countCompleteCreditHoursElective = $countCompleteCreditHoursElective + $ch;
                                    } else {
                                        $countCreditHoursResearch = $countCreditHoursResearch + $ch;
                                    }
                                }
                            }

                            //$researchCreditHours = $course['credit_hour_registered'] + $researchCreditHours;

                            $research['name'] = $course['SubCode'] . ' ' . $course['SubName'];
                            $research['credithours'] = $researchCreditHours;
                            $research['grademark'] = $researchGred;
                            $research['remarks'] = $researchRemarks;
                            $research['typesub'] = $course_status['DefinitionDesc'] == "Research" ? 'Research' : 'Project Paper';
                            //var_dump($researchCreditHours);
                            $r++;
                        }
                    } elseif ($this->view->landscapeType == 42) {
                        //echo "<pre>";print_r($course_status);echo "</pre><hr>";
                        if ($course["exam_status"] == "C" || $course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U")//Complete
                        {
                            //echo "<pre>";print_r($course);echo "</pre><hr>";
                            $compulsory[$course_status['block']]['completed'][$z] = $course;
                            $compulsory[$course_status['block']]['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                            if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                $compulsory[$course_status['block']]['completed'][$z]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                            }

                            if ($course["grade_name"] == "U") {
                                $compulsory[$course_status['block']]['completed'][$z]['CreditHours'] = 0;
                            }

                            if ($course['exam_status'] == 'CT') {
                                $countCtCredit = $course['CreditHours'] + $countCtCredit;
                            } else if ($course['exam_status'] == 'EX') {
                                $countExCredit = $course['CreditHours'] + $countExCredit;
                            }

                            if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                $subject_ids[$q] = $course['IdSubject'];
                                if ($equivid) {
                                    $subject_ids[$q] = $equivid;
                                }

                                $q++;

                            } else {
                                if ($equivid) {
                                    $pos = array_search($equivid, $subject_ids);
                                    unset($subject_ids[$pos]);
                                }
                            }

                            //count paper/credit hourse
                            if ($course["exam_status"] != "U") {
                                $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                    if ($programRequirement[0]['RequirementType'] == 1) {
                                        $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                        $countCreditHoursCore = $ch + $countCreditHoursCore;
                                    } else {
                                        $countCreditHoursCore++;
                                    }

                                    $countCompleteCreditHoursCore = $countCompleteCreditHoursCore + $ch2;
                                    $countCompletePaperCore++;
                                }
                            }

                            $z++;
                        } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                            //echo "masuk";
                            $compulsory[$course_status['block']]['completed'][$z] = $course;
                            $compulsory[$course_status['block']]['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                            $subject_ids[$q] = $course['IdSubject'];
                            $q++;


                            if ($course["grade_name"] == "U") {
                                $compulsory[$course_status['block']]['completed'][$z]['CreditHours'] = 0;
                            }

                            if ($course['exam_status'] == 'CT') {
                                $countCtCredit = $course['CreditHours'] + $countCtCredit;
                            } else if ($course['exam_status'] == 'EX') {
                                $countExCredit = $course['CreditHours'] + $countExCredit;
                            }

                            //count paper/credit hourse
                            if ($course["exam_status"] != "U") {
                                if ($programRequirement[0]['RequirementType'] == 1) {
                                    $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                    $countCreditHoursCore = $ch + $countCreditHoursCore;
                                } else {
                                    $countCreditHoursCore++;
                                }
                            }

                            $z++;
                        }
                    }
                }
                //echo "<pre>";print_r($compulsory);echo"<pre>";exit;
                //get remaining Course
                $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
                $remaining_courses = $landscapeSubjectDb->getRemainingCourses($programId, $landscapeId, $subject_ids);
                $remaining_courses_majoring = $landscapeSubjectDb->getMajoringCourseRemaining($programId, $landscapeId, $subject_ids, $studentdetails['IdProgramMajoring']);

                $remaining_courses_check = [0];
                if ($remaining_courses) {
                    foreach ($remaining_courses as $rc) {
                        array_push($remaining_courses_check, $rc['IdSubject']);
                    }
                }

                if ($remaining_courses_majoring) {
                    foreach ($remaining_courses_majoring as $rcm) {
                        if (!in_array($rcm['IdSubject'], $remaining_courses_check))
                            array_push($remaining_courses, $rcm);
                    }
                }

                $elecreq = $recordDb->getElectiveReq($landscapeId);
                $majreq = $recordDb->getMajoringReq($landscapeId);
                //echo "<pre>";print_r($remaining_courses);echo"<pre>";
                // echo "xxxx = $elecreq === $cum_elective_ch";
                //var_dump($remaining_courses);
                $researchRem = [];

                foreach ($remaining_courses as $b => $remaining) {
                    if ($this->view->landscapeType == 43) {
                        if ($remaining['DefinitionDesc'] == "Core") {
                            $compulsory['open'][$z] = $remaining;
                            $compulsory['open'][$z]['grademark'] = '-';
                            $z++;
                        } elseif ($remaining['DefinitionDesc'] == 'Elective') {
                            if ($cum_elective_ch < $elecreq) {
                                $elective['open'][$p] = $remaining;
                                $elective['open'][$p]['grademark'] = '-';
                                $p++;
                            }
                        } elseif ($remaining['DefinitionDesc'] == 'Majoring') {
                            if ($cum_majoring_ch < $majreq) {
                                $majoring['open'][$p] = $remaining;
                                $majoring['open'][$p]['grademark'] = '-';
                                $p++;
                            }
                        } else if ($remaining['DefinitionDesc'] == 'Research') {
                            $researchRem['open'][$p] = $remaining;
                            $researchRem['open'][$p]['grademark'] = '-';
                            $p++;
                        }
                    } else {
                        $compulsory[$remaining['block']]['open'][$z] = $remaining;
                        $compulsory[$remaining['block']]['open'][$z]['grademark'] = '-';
                        $z++;
                    }
                }

                if ($this->view->landscapeType == 43) {
                    if (array_key_exists("registered", $compulsory)) {
                        $row_span = count($compulsory['completed']) + count($compulsory['registered']);
                    } else {
                        $row_span = count($compulsory['completed']);
                    }
                    $this->view->rowspan = $row_span;

                    if (array_key_exists("registered", $elective)) {
                        $row_span_elective = count($elective['completed']) + count($elective['registered']);
                    } else {
                        $row_span_elective = count($elective['completed']);
                    }
                    $this->view->rowspan_elective = $row_span_elective;
                    $this->view->elective = $elective;

                    if (array_key_exists("registered", $majoring)) {
                        $row_span_majoring = count($majoring['completed']) + count($majoring['registered']);
                    } else {
                        $row_span_majoring = count($majoring['completed']);
                    }
                    $this->view->rowspan_majoring = $row_span_majoring;

                    //var_dump($elective);
                    $row_span_open_compulsary = count($compulsory['open']);
                    $row_span_open_elective = count($elective['open']);
                    $row_span_open_majoring = count($majoring['open']);

                    $this->view->rowspan_open_compulsary = $row_span_open_compulsary;
                    $this->view->rowspan_open_elective = $row_span_open_elective;
                    $this->view->rowspan_open_majoring = $row_span_open_majoring;

                }
                //var_dump($compulsory);
                //var_dump($elective);
                //var_dump($research);
                $this->view->research = $research;
                $this->view->researchRem = $researchRem;

                //count complete subject
                $viewRemainingSubjectCore = true;
                $viewRemainingSubjectElective = true;
                $viewRemainingSubjectMajoring = true;
                $viewRemainingSubjectResearch = true;

                $this->view->countCreditHoursCore = $countCreditHoursCore + $countCreditHoursElective + $countCreditHoursMajoring + $researchCreditHours;
                //var_dump($countCreditHoursCore);
                /*var_dump($countCreditHoursCore);
                var_dump($countCreditHoursElective);
                var_dump($researchCreditHours);
                var_dump($this->view->countCreditHoursCore);*/
                $chr1 = 0;
                $chr2 = 0;
                $chr3 = 0;
                $chr4 = 0;
                $cch = 0;
                $remPaperCore = 0;
                $remPaperElective = 0;

                if ($this->view->landscapeType == 43) {
                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 394) {
                            $remPaperCore = ($programRequirementLoop['CreditHours'] / 3) - ($countCompleteCreditHoursCore / 3);

                            if ($remPaperCore < 0) {
                                $remPaperCore = 0;
                            }

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $countCreditHoursCore2 = $countCompleteCreditHoursCore;
                            } else {
                                $countCreditHoursCore2 = $countCompletePaperCore;
                            }

                            if ($countCreditHoursCore2 >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectCore = false;
                                //$chr1 = 0;
                            }/*else{
                                $chr1 = $programRequirementLoop['CreditHours']-$countCreditHoursCore;
                            }*/

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $chr1 = $programRequirementLoop['CreditHours'] - $countCompleteCreditHoursCore;
                            } else {
                                $chr1 = $programRequirementLoop['CreditHours'] - $countCompletePaperCore;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 275) {
                            $remPaperElective = ($programRequirementLoop['CreditHours'] / 3) - ($countCompletePaperElective);

                            if ($remPaperElective < 0) {
                                $remPaperElective = 0;
                            }

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $countCreditHoursElective2 = $countCompleteCreditHoursElective;
                            } else {
                                $countCreditHoursElective2 = $countCompletePaperElective;
                            }

                            if ($countCreditHoursElective2 >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectElective = false;
                                //$chr2 = 0;
                            }/*else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursElective;
                            }*/

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $chr2 = $programRequirementLoop['CreditHours'] - $countCompleteCreditHoursElective;
                            } else {
                                $chr2 = $programRequirementLoop['CreditHours'] - $countCompletePaperElective;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 1054) {
                            $remPaperMajoring = ($programRequirementLoop['CreditHours'] / 3) - ($countCompletePaperMajoring);

                            if ($remPaperMajoring < 0) {
                                $remPaperMajoring = 0;
                            }

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $countCreditHoursMajoring2 = $countCompleteCreditHoursMajoring;
                            } else {
                                $countCreditHoursMajoring2 = $countCompletePaperMajoring;
                            }

                            if ($countCreditHoursMajoring2 >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectMajoring = false;
                                //$chr2 = 0;
                            }/*else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursElective;
                            }*/

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $chr4 = $programRequirementLoop['CreditHours'] - $countCompleteCreditHoursMajoring;
                            } else {
                                $chr4 = $programRequirementLoop['CreditHours'] - $countCompletePaperMajoring;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 271) {
                            if ($countCreditHoursResearch >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectResearch = false;
                                $chr3 = 0;
                            } else {
                                $chr3 = $programRequirementLoop['CreditHours'] - $countCreditHoursResearch;
                            }
                        }
                    }

                    if ($chr1 == null) {
                        $chr1 = 0;
                    }
                    if ($chr2 == null) {
                        $chr2 = 0;
                    }
                    if ($chr3 == null) {
                        $chr3 = 0;
                    }
                    if ($chr4 == null) {
                        $chr4 = 0;
                    }

                    $cch = $countCompleteCreditHoursCore + $countCompleteCreditHoursElective + $countCompleteCreditHoursMajoring + $countCreditHoursResearch;
                    $this->view->cch = $cch;
                    $this->view->chr = $chr1 + $chr2 + $chr3 + $chr4;

                } else if ($this->view->landscapeType == 42) {
                    //count part summary
                    $completePart1 = 0;
                    $completePart2 = 0;
                    $completePart3 = 0;
                    $remainingPart1 = 0;
                    $remainingPart2 = 0;
                    $remainingPart3 = '';

                    $chr = 0;

                    if ($countCreditHoursCore >= $programRequirement[0]['CreditHours']) {
                        $viewRemainingSubjectCore = false;
                        $chr = 0;
                    } else {
                        $chr = $programRequirement[0]['CreditHours'] - $countCreditHoursCore;
                    }
                    $this->view->chr = $chr;

                    if ($compulsory) {
                        foreach ($compulsory as $key => $compulsoryLoop) {

                            if ($key == 1) {
                                if (isset($compulsoryLoop['completed'])) {
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub) {

                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);

                                        $publishresult = false;

                                        if ($subsem) {
                                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                                            if ($publishInfo) {
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                                    $publishresult = true;
                                                }
                                            }
                                        }

                                        if (($compulsoryLoopSub["exam_status"] == "C" || $compulsoryLoopSub["exam_status"] == "EX" || $compulsoryLoopSub["exam_status"] == "CT" || $compulsoryLoopSub["exam_status"] == "U") && $compulsoryLoopSub['mark_approval_status'] == 2 && $publishresult == true) {
                                            if ($compulsoryLoopSub["exam_status"] != "U") {
                                                if ($compulsoryLoopSub["grade_name"] != "F") {
                                                    $completePart1++;
                                                }
                                            }
                                        } else {
                                            $remainingPart1++;
                                        }
                                    }
                                }

                                if (isset($compulsoryLoop['open'])) {
                                    foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                        $remainingPart1++;
                                    }
                                }
                            } else if ($key == 2) {
                                if (isset($compulsoryLoop['completed'])) {
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub) {

                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);

                                        $publishresult = false;

                                        if ($subsem) {
                                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                                            if ($publishInfo) {
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                                    $publishresult = true;
                                                }
                                            }
                                        }

                                        if (($compulsoryLoopSub["exam_status"] == "C" || $compulsoryLoopSub["exam_status"] == "EX" || $compulsoryLoopSub["exam_status"] == "CT" || $compulsoryLoopSub["exam_status"] == "U") && $compulsoryLoopSub['mark_approval_status'] == 2 && $publishresult == true) {
                                            if ($compulsoryLoopSub["exam_status"] != "U") {
                                                if ($compulsoryLoopSub["grade_name"] != "F") {
                                                    $completePart2++;
                                                }
                                            }
                                        } else {
                                            $remainingPart2++;
                                        }
                                    }
                                }

                                if (isset($compulsoryLoop['open'])) {
                                    foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                        $remainingPart2++;
                                    }
                                }
                            } else if ($key == 3) {
                                if (isset($compulsoryLoop['completed'])) {
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub) {

                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);

                                        $publishresult = false;

                                        if ($subsem) {
                                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                                            if ($publishInfo) {
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                                    $publishresult = true;
                                                }
                                            }
                                        }

                                        if (($compulsoryLoopSub["exam_status"] == "C" || $compulsoryLoopSub["exam_status"] == "EX" || $compulsoryLoopSub["exam_status"] == "CT" || $compulsoryLoopSub["exam_status"] == "U") && $compulsoryLoopSub['mark_approval_status'] == 2 && $publishresult == true) {
                                            if ($compulsoryLoopSub["exam_status"] != "U") {
                                                $completePart3++;
                                            }
                                        } else {
                                            $remainingPart3++;
                                        }
                                    }
                                }

                                if (isset($compulsoryLoop['open'])) {
                                    if (count($compulsoryLoop['open']) > 1) {
                                        $o = 0;
                                        foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                            $remainingPart3 .= $compulsoryLoopSub['SubCode'];
                                            $o++;

                                            if ($o < count($compulsoryLoop['open'])) {
                                                $remainingPart3 .= '/';
                                            }
                                        }
                                    } else {
                                        if ($completePart3 > 0) {
                                            $remainingPart3 = 0;
                                        } else {
                                            foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                                $remainingPart3 = $compulsoryLoopSub['SubCode'] == 'PPP' ? 'Articleship' : 'PPP';
                                            }
                                        }
                                        unset($compulsory[$key]['open']);
                                    }
                                }
                            }
                        }
                    }

                    if ($viewRemainingSubjectCore == false) {
                        $remainingPart1 = 0;
                        $remainingPart2 = 0;
                    }

                    $this->view->completePart1 = $completePart1;
                    $this->view->completePart2 = $completePart2;
                    $this->view->completePart3 = $completePart3;
                    $this->view->remainingPart1 = $remainingPart1;
                    $this->view->remainingPart2 = $remainingPart2;
                    $this->view->remainingPart3 = $remainingPart3;

                    $sumRemainingPapers = $remainingPart1 + $remainingPart2;
                }

                $this->view->compulsory = $compulsory;
                $this->view->majoring = $majoring;
                //dd($majoring); exit;
                $this->view->countCompleteCreditHoursCore = $countCompleteCreditHoursCore;
                $this->view->countCompleteCreditHoursElective = $countCompleteCreditHoursElective;
                $this->view->countCtCredit = $countCtCredit;
                $this->view->countExCredit = $countExCredit;
                $this->view->remPaperCore = $remPaperCore;
                $this->view->remPaperElective = $remPaperElective;
                $this->view->countCompletePaperCore = $countCompletePaperCore;
                $this->view->countCompletePaperElective = $countCompletePaperElective;

                $Asol = 0;
                $Atol = 0;
                $Btam = 0;
                $Bsol = 0;
                $Btol = 0;
                $Ctam = 0;
                $Csol = 0;
                $Dsol = 0;
                $Fsol = 0;
                $NP = 0;
                $P = 0;
                $PD = 0;
                $CT = 0;
                $EX = 0;
                $U = 0;

                if ($courses) {
                    foreach ($courses as $coursesLoop) {

                        $equivid = false;
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $coursesLoop['IdSubject']);
                        if (!$course_status) {
                            $equivid = $recordDb->checkEquivStatus($landscapeId, $coursesLoop["IdSubject"]);

                            if ($equivid) {
                                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $equivid);
                            }
                        }

                        $subsem = $recordDb->getSemesterById($coursesLoop['IdSemesterMain']);

                        $publishresult = false;

                        if ($subsem) {
                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                            if ($publishInfo) {
                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                    $publishresult = true;
                                }
                            }
                        }

                        //var_dump($course_status);
                        //echo "<pre>";print_r($course_status);echo "</pre><hr>";
                        if ($coursesLoop["mark_approval_status"] == 2 && $publishresult == true && $course_status) {
                            if (trim($coursesLoop['grade_name']) == 'A') {
                                $Asol++;
                            } else if (trim($coursesLoop['grade_name']) == 'A-') {
                                $Atol++;
                            } else if (trim($coursesLoop['grade_name']) == 'B+') {
                                $Btam++;
                            } else if (trim($coursesLoop['grade_name']) == 'B') {
                                $Bsol++;
                            } else if (trim($coursesLoop['grade_name']) == 'B-') {
                                $Btol++;
                            } else if (trim($coursesLoop['grade_name']) == 'C+') {
                                $Ctam++;
                            } else if (trim($coursesLoop['grade_name']) == 'C') {
                                $Csol++;
                            } else if (trim($coursesLoop['grade_name']) == 'D') {
                                $Dsol++;
                            } else if (trim($coursesLoop['grade_name']) == 'F') {
                                $Fsol++;
                            } else if (trim($coursesLoop['grade_name']) == 'NP') {
                                $NP++;
                            } else if (trim($coursesLoop['grade_name']) == 'P') {
                                $P++;
                            } else if (trim($coursesLoop['grade_name']) == 'PD') {
                                $PD++;
                            } else if (trim($coursesLoop['grade_name']) == 'CT') {
                                $CT++;
                            } else if (trim($coursesLoop['grade_name']) == 'EX') {
                                $EX++;
                            } else if (trim($coursesLoop['grade_name']) == 'U') {
                                $U++;
                            }
                        }
                    }
                }

                $CAP = [
                    'A'  => $Asol,
                    'A-' => $Atol,
                    'B+' => $Btam,
                    'B'  => $Bsol,
                    'B-' => $Btol,
                    'C+' => $Ctam,
                    'C'  => $Csol,
                    'D'  => $Dsol,
                    'F'  => $Fsol,
                    'NP' => $NP,
                    'P'  => $P,
                    'PD' => $PD,
                    'CT' => $CT,
                    'EX' => $EX,
                    'U'  => $U
                ];
                $this->view->CAP = $CAP;

                //calculate cgpa
                //$total = $this->calculateCGPA($courses, $studentdetails['IdProgram']); //this is old method
                $semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
                $regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
                $studentGradeDB = new Examination_Model_DbTable_StudentGrade();

                $registered_semester = $semesterStatusDB->getListSemesterRegistered($registrationId);
                $student_grade = false;
                $ttt = 0;
                foreach ($registered_semester as $index => $semester) {

                    $subsem = $recordDb->getSemesterById($semester['IdSemesterMain']);

                    $publishresult = false;

                    if ($subsem) {
                        $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                        if ($publishInfo) {
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                            $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                $publishresult = true;
                            }
                        }
                    }

                    //get subject registered in each semester
                    if ($semester["IsCountable"] == 1) {
                        $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($registrationId, $semester['IdSemesterMain']);
                    } else {
                        $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($registrationId, $semester['IdSemesterMain']);
                    }
                    //var_dump($subject_list);
                    $registered_semester[$index]['subjects'] = $subject_list;

                    $ttt++;

                    if ($publishresult == true) {
                        //get grade info
                        $student_grade = $studentGradeDB->getStudentGrade($registrationId, $semester['IdSemesterMain']);
                    }
                }

                if (!$student_grade) {
                    $student_grade['sg_cum_credithour'] = 0;
                    $student_grade['sg_cgpa'] = 0;
                }

                $this->view->student_grade = $student_grade;

                //$this->view->total = $total;
                $this->view->viewRemainingSubjectCore = $viewRemainingSubjectCore;
                $this->view->viewRemainingSubjectElective = $viewRemainingSubjectElective;
                $this->view->viewRemainingSubjectMajoring = $viewRemainingSubjectMajoring;
                $this->view->viewRemainingSubjectResearch = $viewRemainingSubjectResearch;
            }

            /**
             ** END
             **/
            //get subjects offered
            $SubjectOffered = new GeneralSetup_Model_DbTable_Subjectsoffered();
            $subject_offered = $SubjectOffered->getMultiLandscapeCourseOffer(["$landscapeId"], "", $curr_semester['IdSemesterMaster']);

            $offered = [];

            $y = 0;
            foreach ($subject_offered as $sub => $courseOffered) {
                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $courseOffered['IdSubject']);
                if ($this->view->landscapeType == 43) {
                    if ($course_status['DefinitionDesc'] == "Core") {
                        $offered['compulsory'][$y] = $courseOffered;
                    } elseif ($course_status['DefinitionDesc'] == "Elective") {
                        $offered['elective'][$y] = $courseOffered;
                    } elseif ($course_status['DefinitionDesc'] == "Majoring") {
                        $offered['majoring'][$y] = $courseOffered;
                    } elseif ($course_status['DefinitionDesc'] == "Research") {
                        $offered['research'][$y] = $courseOffered;
                    }
                    $y++;
                } else {//level landscape
                    $offered[$courseOffered['block']]['compulsory'][$y] = $courseOffered;
                    $y++;
                }
            }
            //echo "<pre>";print_r($offered);echo"<pre>";
            if ($landscape["LandscapeType"] == 43) {
                $this->view->offered_compulsory = isset($offered['compulsory']) ? count($offered['compulsory']) : 0;
                $this->view->offered_elective = isset($offered['elective']) ? count($offered['elective']) : 0;
                $this->view->offered_majoring = isset($offered['majoring']) ? count($offered['majoring']) : 0;
            }
            $this->view->offered = $offered;

        } elseif ($landscape["LandscapeType"] == 45) {

            //get registered blocks
            $studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
            $blocks = $studentSemesterDB->getRegisteredBlock($registrationId);
            $this->view->blocks = $blocks;

            $landscapeId = $studentdetails["IdLandscape"];
            $this->view->landscapeId = $landscapeId;
            $this->view->landscapeType = 44;
            $programId = $studentdetails["IdProgram"];
            $this->view->programId = $programId;


            //get landscape info
            $landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
            $landscape = $landscapeDB->getLandscapeDetails($landscapeId);
            $this->view->landscape = $landscape;

            //get program requirement info
            $progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
            $programRequirement = $progReqDB->getlandscapecoursetype($programId, $landscapeId);
            $this->view->programrequirement = $programRequirement;
        }


        // GET PROFILE STATUS HISTORY
        $this->view->studentProfileHistory = $studentRegistrationDB->fetchStudentProfileHistory($registrationId);

        // GET SEMESTER HISTORY
        $this->view->studentSemesterHistory = $studentRegistrationDB->fetchStudentSemesterHistory($registrationId);
        $this->view->studentSemesterHistory2 = $studentRegistrationDB->fetchStudentSemesterHistory2($registrationId);

        // GET NEXT SEMESTER FOR SENIOR CITIZEN
        $this->view->studentSemesterSenior = $studentRegistrationDB->fetchStudentSemesterSenior($registrationId);

        // GET ACADEMIC ADVISOR Info
        $this->view->currentAdvisor = $studentRegistrationDB->getCurrentAcademicAdvisor($studentdetails['IdStudentRegistration']);

        //GET ACADEMIC ADVISOR HISTORY
        $StudentAdvisor = new Registration_Model_DbTable_StudentAcademicAdvisorHistory();
        $this->view->academicAdvisor = $StudentAdvisor->getData($studentdetails['IdStudentRegistration']);

        //student info part
        $studentId = $this->_getparam("id");
        $sectionList = $this->lobjStudentprofile->getSection();
        $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);
        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($studentId);
        $studentInfo = array_merge($studentInfo, $taggingFinancialAid);
        $auth = Zend_Auth::getInstance();

        //count age
        $dob = date('Y-m-d', strtotime($studentInfo['appl_dob']));
        $today = date('Y-m-d');
        $age = $today - $dob;
        $studentInfo['appl_age'] = $age;

        //student info
        $this->view->studentId = $studentId;
        $this->view->studentInfo = $studentInfo;
        $this->view->sectionList = $sectionList;
        //var_dump($studentInfo); exit;

        //educatio detail
        $educationDetailInfo = $this->lobjStudentprofile->getEducationDetails($studentInfo['sp_id']);
        $this->view->educationDetailInfo = $educationDetailInfo;
        //var_dump($educationDetailInfo); exit;

        //english proficiency
        $englishProficiencyInfo = $this->lobjStudentprofile->getEnglishProficiencyDetails($studentInfo['sp_id']);
        $this->view->englishProficiencyInfo = $englishProficiencyInfo;
        //var_dump($englishProficiencyInfo);exit();

        //quantitative proficiency
        $quantitativeProficiencyInfo = $this->lobjStudentprofile->getQuantitativeProficiencyDetails($studentInfo['sp_id']);
        $this->view->quantitativeProficiencyInfo = $quantitativeProficiencyInfo;
        //var_dump($quantitativeProficiencyInfo); exit;

        //research and publication
        $researchAndPublicationInfo = $this->lobjStudentprofile->getResearchAndPublicationDetails($studentInfo['sp_id']);
        $this->view->researchAndPublicationInfo = $researchAndPublicationInfo;
        //var_dump($researchAndPublicationInfo); exit;

        //employment detail
        $employmentDetailsInfo = $this->lobjStudentprofile->getEmploymentDetails($studentInfo['sp_id']);
        $this->view->employmentDetailsInfo = $employmentDetailsInfo;
        //var_dump($employmentDetailsInfo); exit;

        //industry working experience
        $defModel = new App_Model_General_DbTable_Definationms();
        $employmentIndustryList = $defModel->getDataByType(114);
        $industryYearList = $defModel->getDataByType(111);
        $this->view->employmentIndustryList = $employmentIndustryList;
        $this->view->industryYearList = $industryYearList;

        //health condition
        $healthConditionList = $defModel->getDataByType(109);
        $this->view->healthConditionList = $healthConditionList;

        //financial particular
        $financialParticularInfo = $this->lobjStudentprofile->getFinancialParticular($studentInfo['sp_id']);
        $this->view->financialParticularInfo = $financialParticularInfo;
        //var_dump($financialParticularInfo); exit;

        //visa detail
        $visaDetailsInfo = $this->lobjStudentprofile->getVisaDetails($studentInfo['sp_id']);
        $this->view->visaDetailsInfo = $visaDetailsInfo;
        //var_dump($visaDetailsInfo); //exit;

        //referees
        $refreesDatilsInfo = $this->lobjStudentprofile->getRefereesDetails($studentInfo['sp_id']);
        $this->view->refreesDatilsInfo = $refreesDatilsInfo;
        //var_dump($refreesDatilsInfo); exit;

        //relationship informarion
        $relationshipInformationInfo = $this->lobjStudentprofile->getRelationshipInformation($studentInfo['sp_id']);
        $this->view->relationshipInformationInfo = $relationshipInformationInfo;
        //var_dump($relationshipInformationInfo); exit;

        //emergency contact
        $emergencyContectInfo = $this->lobjStudentprofile->getEmergencyContactInfo($studentInfo['sp_id']);
        $this->view->emergencyContactInfo = $emergencyContectInfo;

        //curricular activities
        $curricularAcitvitiesInfo = $this->lobjStudentprofile->getCurricularActivities($studentInfo['sp_id']);
        $this->view->curricularActivitiesInfo = $curricularAcitvitiesInfo;

        //student attribute
        $attributeInfo = $this->lobjStudentprofile->getAttributeInfo($studentInfo['sp_id']);
        $this->view->attributeInfo = $attributeInfo;

        //comment
        $commentInfoPublic = $this->lobjStudentprofile->getCommentInfo($studentInfo['sp_id']);
        $commentInfoPrivate = $this->lobjStudentprofile->getCommentInfoPrivate($studentInfo['sp_id'], $auth->getIdentity()->id);
        $commentInfo = array_merge($commentInfoPublic, $commentInfoPrivate);
        $this->view->commentInfo = $commentInfo;

        //audit log
        $auditDB = new App_Model_Audit();
        $audit_history = $auditDB->getDataByUser($registrationId);
        $this->view->audit_history = $audit_history;

        /*
             * visa details part
             */
        $studentPassportInfo = $this->lobjStudentprofile->studentPassportInfo($studentInfo['sp_id']);
        $this->view->studentPassportInfo = $studentPassportInfo;

        $studentPassInfo = $this->lobjStudentprofile->studentPassInfo($studentInfo['sp_id']);
        $this->view->studentPassInfo = $studentPassInfo;

        $dependentPassportInfo = $this->lobjStudentprofile->dependantPassportInfo($studentInfo['sp_id']);
        $this->view->dependentPassportInfo = $dependentPassportInfo;
        //var_dump($studentInfo['sp_id']);

        //email history part
        $vsmodel = new Records_Model_DbTable_Visitingstudent();
        $emailHis = $vsmodel->getEmailHistory($registrationId);
        $this->view->emailHis = $emailHis;
    }

    public function auditDetailsAction()
    {
        $auditDB = new App_Model_Audit();
        $this->view->title = "Student Profile History";

        $id = $this->_getParam('id', 0);
        $student_id = $this->_getParam('stud_id', 0);
        $sid = $this->_getParam('section_id', 0);

        $this->view->student_id = $student_id;

        $varinfo = $this->getVariables($sid);

        //audit log data
        $audit_info = $auditDB->getDataById($id);
        $log_data = json_decode($audit_info['log_data'], true);

        //populate values
        $this->view->values = [];

        foreach ($log_data as $row) {
            $this->view->values[$row['variable']]['value'] = $this->remapValues($row['variable'], $row['value']);
            $this->view->values[$row['variable']]['prev_value'] = $this->remapValues($row['variable'], $row['prev_value']);
        }

        //views
        $this->view->varinfo = $varinfo;
        $this->view->audit_info = $audit_info;
        $this->view->data = $log_data;
    }

    protected function getVariables($section_id)
    {
        switch ($section_id) {
            //personal details
            case 2:
                $list = [
                    'senior_student'        => 'Previous Student',
                    'appl_salutation'       => 'Salutation',
                    'appl_fname'            => 'First Name',
                    'appl_lname'            => 'Last Name',
                    'appl_idnumber_type'    => 'ID Type',
                    'appl_idnumber'         => 'ID Number',
                    'appl_passport_expiry'  => 'Passport Expiry Date',
                    'appl_gender'           => 'Gender',
                    'appl_dob'              => 'Date of Birth',
                    'appl_age'              => 'Age',
                    'appl_marital_status'   => 'Marital Status',
                    'appl_religion'         => 'Religion',
                    'appl_nationality'      => 'Nationality',
                    'appl_type_nationality' => 'Type of Nationality',
                    'appl_race'             => 'Race',
                    'appl_bumiputera'       => 'Bumiputera Eligibility',
                    'appl_email'            => 'Email ID',
                    'appl_email_personal'   => 'Personal Email ID',
                    'appl_address1'         => 'Permanent Address',
                    'appl_address2'         => 'Permanent Address',
                    'appl_address3'         => 'Permanent Address',
                    'appl_postcode'         => 'Permanent Postcode/Zip Code',
                    'appl_city'             => 'Permanent City',
                    'appl_country'          => 'Permanent Country',
                    'appl_state'            => 'Permanent State',
                    'appl_caddress1'        => 'Correspondence Address',
                    'appl_caddress2'        => 'Correspondence Address',
                    'appl_caddress3'        => 'Correspondence Address',
                    'appl_cpostcode'        => 'Correspondence Postcode/Zip Code',
                    'appl_ccity'            => 'Correspondence City',
                    'appl_ccountry'         => 'Correspondence Country',
                    'appl_cstate'           => 'Correspondence State',
                    'appl_phone_home'       => 'Telephone Number (Home)(P)',
                    'appl_phone_mobile'     => 'Telephone Number (Mobile)(P)',
                    'appl_phone_office'     => 'Telephone Number (Office)(P)',
                    'appl_fax'              => 'Fax (P)',
                    'appl_cphone_home'      => 'Telephone Number (Home)(C)',
                    'appl_cphone_mobile'    => 'Telephone Number (Mobile)(C)',
                    'appl_cphone_office'    => 'Telephone Number (Office)(C)',
                    'appl_cfax'             => 'Fax (C)'
                ];
                break;

            case 3:
                $list = [
                    'ae_qualification'      => 'Qualification Level',
                    'ae_degree_awarded'     => 'Degree Awarded',
                    'ae_class_degree'       => 'Class Degree',
                    'ae_result'             => 'Result/CGPA',
                    'ae_year_graduate'      => 'Year of Graduation',
                    'ae_medium_instruction' => 'Medium of Instruction',
                    'ae_institution'        => 'Name of University/College'
                ];
                break;

            case 4:
                $list = [
                    'ep_test'       => 'Test',
                    'ep_date_taken' => 'Date Taken',
                    'ep_score'      => 'Score'
                ];

                break;

            case 5:
                $list = [
                    'qp_level'       => 'Level',
                    'qp_grade_mark'  => 'Grade Marks',
                    'qp_institution' => 'Institution'
                ];
                break;

            case 6:
                $list = [
                    'rpd_title'     => 'Title',
                    'rpd_date'      => 'Date',
                    'rpd_publisher' => 'Publisher'
                ];

                break;

            case 7:
                $list = [
                    'ae_status'          => 'Employment Status',
                    'ae_comp_name'       => 'Company Name',
                    'ae_comp_address'    => 'Company Address',
                    'ae_comp_phone'      => 'Company Phone',
                    'ae_comp_fax'        => 'Company Fax',
                    'ae_designation'     => 'Designation',
                    'ae_position'        => 'Position Level',
                    'emply_year_service' => 'Service Duration',
                    'ae_industry'        => 'Industry',
                    'ae_job_desc'        => 'Job Description'
                ];

                break;

            case 8:
                $list = [
                    'aw_industry_id' => 'Industry',
                    'aw_years'       => 'Years'
                ];
                break;

            case 9:
                $list = [
                    'health_condition' => 'Health Condition'
                ];
                break;

            case 10:
                $list = [
                    'af_sponsor_name'        => 'Name of Sponsor',
                    'af_type_scholarship'    => 'Select Scholarship',
                    'af_scholarship_secured' => 'Secured Scholarship',
                    'af_scholarship_apply'   => 'Applying for Scholarship'
                ];
                break;

            case 11:
                $list = [
                    'av_malaysian_visa' => 'Do you hold a Malaysian Visa',
                    'av_expiry'         => 'Visa Expiry Date',
                    'av_status'         => 'Visa Status'
                ];
                break;

            case 16:
                $list = [
                    'r_ref1_name'     => 'Referee1 Name',
                    'r_ref1_position' => 'Referee1 Position',
                    'r_ref1_add1'     => 'Referee1 Address 1',
                    'r_ref1_add2'     => 'Referee1 Address 2',
                    'r_ref1_add3'     => 'Referee1 Address 3',
                    'r_ref1_postcode' => 'Referee1 Postcode',
                    'r_ref1_country'  => 'Referee1 Country',
                    'r_ref1_state'    => 'Referee1 State',
                    'r_ref1_city'     => 'Referee1 City',
                    'r_ref1_phone'    => 'Referee1 Phone',
                    'r_ref1_fax'      => 'Referee1 Fax',
                    'r_ref1_email'    => 'Referee1 Email',
                    'r_ref2_name'     => 'Referee2 Name',
                    'r_ref2_position' => 'Referee2 Position',
                    'r_ref2_add2'     => 'Referee2 Address 2',
                    'r_ref2_add2'     => 'Referee2 Address 2',
                    'r_ref2_add3'     => 'Referee2 Address 3',
                    'r_ref2_postcode' => 'Referee2 Postcode',
                    'r_ref2_country'  => 'Referee2 Country',
                    'r_ref2_state'    => 'Referee2 State',
                    'r_ref2_city'     => 'Referee2 City',
                    'r_ref2_phone'    => 'Referee2 Phone',
                    'r_ref2_fax'      => 'Referee2 Fax',
                    'r_ref2_email'    => 'Referee2 Email'

                ];
                break;

            case 18:
                $list = [
                    'ri_staff_id' => 'Staff'
                ];
                break;

            case 19:
                $list = [
                    'ec_type'          => 'Relationship Type',
                    'ec_name'          => 'Name',
                    'ec_add1'          => 'Address',
                    'ec_add2'          => 'Address 2',
                    'ec_country'       => 'Country',
                    'ec_state'         => 'State',
                    'ec_city'          => 'City',
                    'ec_postcode'      => 'Postcode',
                    'ec_phone_mobile'  => 'Tel (Mobile)',
                    'ec_phone_home'    => 'Tel (Home)',
                    'ec_phone_office'  => 'Tel (Office)',
                    'ec_permanent_add' => 'Same as permanent address'
                ];
                break;
            case 20:
                $list = [
                    'ca_date' => 'Date',
                    'ca_desc' => 'Description'
                ];
                break;

            case 22:

                $list = [
                    'scm_type' => 'Comment Type',
                    'scm_desc' => 'Description'
                ];
                break;
            default;
                $list = [
                ];

        }

        return $list;
    }

    //convert variable to readable-value
    protected function remapValues($variable, $value)
    {
        $objdeftypeDB = new App_Model_Definitiontype();
        $countryDB = new App_Model_General_DbTable_Country();
        $branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
        $qualificationDB = new App_Model_General_DbTable_Qualificationmaster();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $institutionDB = new App_Model_Application_DbTable_Institution();

        switch ($variable) {
            case "mode_study":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Mode of Study'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;
            case "program_mode":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Mode of Program'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;
            case "program_type":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Program Type'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            /*2 */
            case 'appl_salutation':
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Salutation'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "appl_nationality":
            case "appl_country":
            case "appl_ccountry":
            case "ae_institution_country":
                return $this->getValueById($value, $countryDB->getData(), ['k' => 'idCountry', 'v' => 'CountryName']);
                break;

            case "appl_type_nationality":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Type of Nationality'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "appl_idnumber_type":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('ID Type'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "branch_id":
                return $this->getValueById($value, $branchDB->fnGetBranchList(), ['k' => 'id', 'v' => 'value']);
                break;

            case "appl_gender":
                return $value == '1' ? $this->view->translate('Male') : $this->view->translate('Female');
                break;

            case "appl_religion":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Religion'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "appl_race":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Race'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "appl_marital_status":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Marital Status'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "appl_state":
            case "appl_cstate":
                return $this->getStateById($value);
                break;

            /*
			3
			*/
            case "ae_qualification":
                return $this->getValueById($value, $entryReqDB->getListGeneralReq(), ['k' => 'IdQualification', 'v' => 'QualificationLevel']);
                break;

            case "ae_class_degree":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Class Degree'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc'], ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "ae_institution":
                return $this->getValueById($value, $institutionDB->getData(), ['k' => 'idInstitution', 'v' => 'InstitutionName']);
                break;

            /*
			4
			*/
            case "ep_test":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsBylocale('English Proficiency Test List'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;
            case "ep_test_detail":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsByLocale('Format  TOEFL'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            /*
			7
			*/
            case "ae_status":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsBylocale('Employment Status'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "ae_position":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsBylocale('Position Level'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "ae_industry":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsBylocale('Industry Working Experience'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            /*
			8
			*/
            case "aw_industry_id":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsBylocale('Industry Working Experience'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;

            case "aw_years":
                return $this->getValueById($value, $objdeftypeDB->fnGetDefinationsBylocale('Years of Industry'), ['k' => 'idDefinition', 'v' => 'DefinitionDesc']);
                break;
            /*
			9
			*/
            case "health_condition":
                $result = $objdeftypeDB->fnGetDefinationsByLocale('Health Condition');
                $defbyid = [];
                foreach ($result as $def) {
                    $defbyid[$def['idDefinition']] = $def['DefinitionDesc'];
                }

                $out = [];
                $values = explode(',', $value);
                foreach ($values as $val) {
                    $out[] = $defbyid[$val];
                }

                return implode(', ', $out);
                break;
            /*
			11
			*/
            case "av_malaysian_visa":
                return $value == 0 ? $this->view->translate('No') : $this->view->translate('Yes');
                break;

            /*
			18
			*/
            case "ri_staff_id":

                $staffModel = new GeneralSetup_Model_DbTable_Staffmaster();

                $staffList = $staffModel->fetchAll();
                return $this->getValueById($value, $staffList, ['k' => 'IdStaff', 'v' => 'FullName']);
                break;

        }

        return $value;
    }

    public function getValueById($id, $data, $map)
    {
        if (!empty($data)) {
            foreach ($data as $row) {
                if ($row[$map['k']] == $id) {
                    return $row[$map['v']];
                }
            }
        }
    }

    public function getcollegedetailsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        //Initialize Disciplinary Action Model
        //$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;
        $lobjStudentprofile = $this->lobjStudentprofile;
        //Get Disciplinary Action Type Id
        $idstudent = $this->_getParam('field12');


        $lstrsubjectdetails = $this->lobjCommon->fnResetArrayFromValuesToNames($lobjStudentprofile->fnGetCollegeDetails($idstudent));
        echo Zend_Json_Encoder::encode($lstrsubjectdetails);
    }

    public function uploadPhotoAction()
    {

        $auth = Zend_Auth::getInstance();

        $formData = $formData = $this->getRequest()->getPost();

        ///upload_file
        $apath = DOCUMENT_PATH . "/applicant";
        //$apath = "/Users/alif/git/triapp/documents/applicant";

        //create directory to locate file
        if (!is_dir($apath)) {
            mkdir($apath, 0775);
        }

        ///upload_file
        $applicant_path = DOCUMENT_PATH . "/applicant/" . date("mY");
        //$applicant_path = "/Users/alif/git/triapp/documents/applicant/".date("mY");

        //create directory to locate file
        if (!is_dir($applicant_path)) {
            mkdir($applicant_path, 0775);
        }


        $major_path = $applicant_path . "/" . $formData["transaction_id"];

        //create directory to locate file
        if (!is_dir($major_path)) {
            mkdir($major_path, 0775);
        }

        if (is_uploaded_file($_FILES["file"]['tmp_name'])) {
            $ext_photo = $this->getFileExtension($_FILES["file"]["name"]);

            if ($ext_photo == ".jpg" || $ext_photo == ".JPG" || $ext_photo == ".jpeg" || $ext_photo == ".JPEG" || $ext_photo == ".gif" || $ext_photo == ".GIF" || $ext_photo == ".png" || $ext_photo == ".PNG") {
                $flnamenric = date('Ymdhs') . "_TRISAKTI_PHOTO" . $ext_photo;
                $path_photo = $major_path . "/" . $flnamenric;
                move_uploaded_file($_FILES["file"]['tmp_name'], $path_photo);

                $upd_photo = [
                    'auf_appl_id'     => $formData["transaction_id"],
                    'auf_file_name'   => $flnamenric,
                    'auf_file_type'   => 51,
                    'auf_upload_date' => date("Y-m-d h:i:s"),
                    'auf_upload_by'   => $auth->getIdentity()->id,
                    'pathupload'      => $path_photo
                ];

                $uploadfileDB = new App_Model_Application_DbTable_UploadFile();

                $previous_record = $uploadfileDB->getFile($formData["transaction_id"], 51);

                if ($previous_record) {
                    $uploadfileDB->updateData($upd_photo, $previous_record['auf_id']);
                } else {
                    $uploadfileDB->addData($upd_photo);
                }
            }
        }


        if (isset($formData['redirect_path'])) {
            $this->_redirect($this->baseUrl . $formData['redirect_path']);
        } else {
            $this->_redirect($this->baseUrl . '/records/studentprofile/index');
        }

        exit;
    }

    function getFileExtension($filename)
    {
        return substr($filename, strrpos($filename, '.'));
    }

    function getCourseDetailAction()
    {


        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $studId = $this->_getParam('studId', null);
        $subId = $this->_getParam('subId', null);
        $semId = $this->_getParam('semId', null);

        //student info
        $studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
        $studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($studId);

        //class group
        $courseGroupStudentDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
        $classGroup = $courseGroupStudentDb->checkStudentCourseGroup($studId, $semId, $subId);

        //class attendance
        $courseGroupStudentAttendanceDb = new Examination_Model_DbTable_CourseGroupStudentAttendanceDetail();

        $classGroup['class_session'] = $courseGroupStudentAttendanceDb->getAttendanceSessionCount($classGroup['IdCourseTaggingGroup'], $studId);
        $classGroup['class_att_attended'] = $courseGroupStudentAttendanceDb->getAttendanceStatusCount($classGroup['IdCourseTaggingGroup'], $studId, 395);
        $classGroup['class_att_permission'] = $courseGroupStudentAttendanceDb->getAttendanceStatusCount($classGroup['IdCourseTaggingGroup'], $studId, 396);
        $classGroup['class_att_sick'] = $courseGroupStudentAttendanceDb->getAttendanceStatusCount($classGroup['IdCourseTaggingGroup'], $studId, 397);
        $classGroup['class_att_absent'] = $courseGroupStudentAttendanceDb->getAttendanceStatusCount($classGroup['IdCourseTaggingGroup'], $studId, 398);
        $classGroup['class_attendance_percentage'] = ($classGroup['class_att_attended'] / $classGroup['class_session']) * 100;

        $this->view->class_group = $classGroup;


        /*
		 * coursework mark
		 *
		 */

        //get subject component
        $markDistributionDB = new Examination_Model_DbTable_Marksdistributionmaster();
        $component = $markDistributionDB->getListMainComponent($semId, $studentdetails['IdProgram'], $subId);

        //get student reg subject info details(subject registration info)
        $studentRegistrationSubjectDb = new Examination_Model_DbTable_StudentRegistrationSubject();
        $subjectRegInfo = $studentRegistrationSubjectDb->getSemesterSubjectStatus($semId, $studId, $subId);

        //get mark
        $studentMarkEntryDb = new Examination_Model_DbTable_StudentMarkEntry();
        foreach ($component as $index => $com) {

            $component[$index]['student_mark'] = $studentMarkEntryDb->getMark($studId, $com['IdMarksDistributionMaster'], $subjectRegInfo['IdStudentRegSubjects'], $semId);
        }

        $this->view->coursework = $component;

    }

    function getNextSemesterListAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $registrationId = $this->_getParam('registrationId', null);

        $this->_helper->layout->disableLayout();
        $current_date = date('Y-m-d');
        $getNextSemesterList = new Records_Model_DbTable_SemesterMaster();
        $row = $getNextSemesterList->getNextSemesterList($current_date, $registrationId);

        $this->view->row = $row;
        $this->view->registrationId = $registrationId;
    }

    function addNewSemesterAction()
    {
        //definitionCode = PreRegistration

        $auth = Zend_Auth::getInstance();

        $getNextSemesterList = new Records_Model_DbTable_SemesterMaster();

        $maxLevel = $getNextSemesterList->getLevel($this->_getParam('registrationId'));
        $statusId = $getNextSemesterList->getStatusId('PreRegistration');

        $data = [
            'IdStudentRegistration' => $this->_getParam('registrationId'),
            'IdSemesterMain'        => $this->_getParam('semesterId'),
            'studentsemesterstatus' => $statusId,
            'level'                 => $maxLevel,
            'Reason'                => '',
            'UpdDate'               => date("Y-m-d H:m:s"),
            'UpdUser'               => $auth->getIdentity()->id
        ];

        //print_r($data);
        $this->_helper->viewRenderer->setNoRender(true);


        $db = new Zend_Db_Table('tbl_studentsemesterstatus');
        $db->insert($data);

        $this->_redirect($this->baseUrl . '/records/studentprofile/studentprofilelist/id/' . $this->_getParam('registrationId'));
    }

    function changeSubjectStatusAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $listSubjectIdString = rtrim($this->_getParam('listApproveId'));

        $listSubjectIdArray = explode(',', $listSubjectIdString);

        $auth = Zend_Auth::getInstance();


        $db = new Registration_Model_DbTable_Studentregsubjects();

        foreach ($listSubjectIdArray as $key => $value) {
            $data = [
                'Active'  => '1',
                'UpdDate' => date("Y-m-d H:m:s"),
                'UpdUser' => $auth->getIdentity()->id

            ];

            $db->updateData($data, $value);

            //print_r($db->assemble());
        }

        $this->_redirect($this->baseUrl . '/records/studentprofile/studentprofilelist/id/' . $this->_getParam('registrationId'));
    }

    public function dropsubjectAction()
    {
        $idregsub = $this->_getparam("idregsub");
        $idreg = $this->_getparam("idreg");
        $db = new Registration_Model_DbTable_Studentregsubjects();
        $db->dropRegisterSubjects($idregsub);
        $this->_redirect($this->baseUrl . '/records/studentprofile/studentprofilelist/id/' . $idreg . '/#Tab2');
    }

    public function studentProfileAction()
    {
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        //$test = ini_set('post_max_size', '10M');
        //var_dump($test);
        //php_value post_max_size 10M;
        //ini_set('upload_max_file_size', '-1');
        //var_dump(ini_get('post_max_size'));
        //var_dump(ini_get('memory_limit'));

        $studentId = $this->_getparam("id");
        $sId = $this->_getparam("sectionId", 1);
        $sectionList = $this->lobjStudentprofile->getSection();
        $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);
        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($studentId);
        $studentInfo = array_merge($studentInfo, $taggingFinancialAid);
        $this->view->studentId = $studentId;
        $this->view->studentInfo = $studentInfo;
        $this->view->section = $sectionList;
        $this->view->sectionId = $sId;
        $auth = Zend_Auth::getInstance();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            /*$size = (int) $_SERVER['CONTENT_LENGTH'];
            var_dump($size);
            var_dump($_FILES);*/
            //var_dump($formData); exit;
            $this->view->sectionId = $formData['sectionId'];

            //program details
            if (isset($formData['save_program_detail']) && $formData['save_program_detail'] == 'Save') {
                $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);

                if ($studentInfo['IdBranch'] != $formData['IdBranch']) {
                    $branchData = [
                        'IdBranch' => $formData['IdBranch']
                    ];

                    $this->lobjStudentprofile->updateStudent($branchData, $studentId);

                    $feeStructureData = $this->lobjStudentprofile->getApplicantFeeStructure($studentInfo['IdProgram'], $studentInfo['IdProgramScheme'], $studentInfo['IdIntake'], $studentInfo['appl_category'], 0);
                    if ($feeStructureData != false) {
                        $datafee = [
                            'fs_id' => $feeStructureData['fs_id']
                        ];
                        $this->lobjStudentprofile->updateStudent($datafee, $studentId);
                    }
                }
            }

            //personal detail
            if (isset($formData['save_personal_detail']) && $formData['save_personal_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_personal_detail']);

                if ($formData["appl_type_nationality"] == 547) {
                    $formData['appl_category'] = 579; //local
                } else if ($formData["appl_type_nationality"] == 548) {
                    $formData['appl_category'] = 581; //pr
                } else if ($formData["appl_type_nationality"] == 549) {
                    $formData['appl_category'] = 580; //int
                }

                if ($formData["appl_type_nationality"] != $studentInfo['appl_type_nationality']) {
                    //change fs id
                    $feeStructureData = $this->lobjStudentprofile->getApplicantFeeStructure($studentInfo['IdProgram'], $studentInfo['IdProgramScheme'], $studentInfo['IdIntake'], $formData['appl_category'], 0);

                    $bind = [
                        'fs_id' => $feeStructureData['fs_id']
                    ];
                    $this->lobjStudentprofile->updateStudent($bind, $studentInfo['IdStudentRegistration']);
                }

                $formData['appl_passport_expiry'] = ($formData['appl_passport_expiry'] == '' ? NULL : date('Y-m-d', strtotime($formData['appl_passport_expiry'])));
                $formData['appl_dob'] = ($formData['appl_dob'] == '' ? NULL : date('Y-m-d', strtotime($formData['appl_dob'])));

                /* AUDIT ENTRY	*/
                $auditDB = new App_Model_Audit();
                $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);

                $log_audit = $auditDB->compareChanges($formData, $studentInfo);

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Personal Details', 2, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }
                /* END AUDIT	*/
                $plainpass = $formData['appl_password'];
                if ($formData['appl_password'] != '') {

                    $newpassword = md5($formData['appl_password']);
                    $plainPassword = $formData['appl_password'];
                    $formData['appl_password'] = $newpassword;
                    $formData['password_changed_by'] = $auth->getIdentity()->id;
                    $formData['password_changed_date'] = new Zend_Db_Expr('NOW()');

                    $KMC = new Registration_Model_DbTable_Kmc();
                    $kmc_data = [
                        'username' => $studentInfo['registrationId'],
                        'password' => $plainPassword
                    ];

                    $KMC->changePasswordKmc($kmc_data);

                    $LDAP = new Registration_Model_DbTable_Ldap();

                    $ldap_data = [
                        'username'     => $studentInfo['registrationId'],
                        'unicodePwd'   => $plainPassword,
                        'account_type' => 'STUDENT'
                    ];

                    $LDAP->changePasswordLdap($ldap_data);
                }

                //update
                $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_profile', 'id');

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();
                //var_dump($files); exit;
                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 23) == 'personalDetail_idNumber') {
                                $definationCode = 'PID';
                            } else if (substr($file, 0, 20) == 'personalDetail_photo') {
                                $definationCode = 'PH';
                            } else if (substr($file, 0, 21) == 'personalDetail_resume') {
                                $definationCode = 'FIN4';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];


                                }
                            }
                        }

                        //AUDIT
                        if (!empty($log_audit2)) {
                            $auditDB->recordLog('Personal Details', 2, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                        }

                    }
                } else {
                    throw new Exception("Upload Error (check zend file transfer)");
                }

                //kmc part
                $kmcModel = new Registration_Model_DbTable_Kmc();

                $kmcData = [
                    'username'                => $studentInfo['registrationId'],
                    'password'                => $plainpass,
                    'email'                   => $studentInfo['appl_email'],
                    'name'                    => $formData['appl_fname'] . ' ' . $formData['appl_lname'],
                    'religion'                => $formData['appl_religion'] != 99 ? $this->lobjStudentprofile->getDefinationDesc($formData['appl_religion']) : $formData['appl_religion_others'],
                    'nationality'             => $this->lobjStudentprofile->getCountriesById($formData['appl_nationality']),
                    'identity_type'           => $this->lobjStudentprofile->getDefinationDesc($formData['appl_idnumber_type']),
                    'identity_number'         => $formData['appl_idnumber'],
                    'date_of_birth'           => date('Y-m-d', strtotime($formData['appl_dob'])),
                    'permanent_address'       => $formData['appl_address1'] . ' ' . $formData['appl_address2'] . ' ' . $formData['appl_address3'],
                    'permanent_city'          => $formData['appl_city'] != 99 ? $this->lobjStudentprofile->getCountriesById($formData['appl_city']) : $formData['appl_city_others'],
                    'permanent_postcode'      => $formData['appl_postcode'],
                    'permanent_state'         => $formData['appl_state'] != 99 ? $this->lobjStudentprofile->getStateById($formData['appl_state']) : $formData['appl_state_others'],
                    'permanent_country'       => $this->lobjStudentprofile->getCountriesById($formData['appl_country']),
                    'correspondence_address'  => $formData['appl_caddress1'] . ' ' . $formData['appl_caddress2'] . ' ' . $formData['appl_caddress3'],
                    'correspondence_city'     => $formData['appl_ccity'] != 99 ? $this->lobjStudentprofile->getCountriesById($formData['appl_ccity']) : $formData['appl_ccity_others'],
                    'correspondence_postcode' => $formData['appl_cpostcode'],
                    'correspondence_state'    => $formData['appl_cstate'] != 99 ? $this->lobjStudentprofile->getStateById($formData['appl_cstate']) : $formData['appl_cstate_others'],
                    'correspondence_country'  => $this->lobjStudentprofile->getCountriesById($formData['appl_ccountry']),
                    'contact_home'            => $formData['appl_phone_home'],
                    'contact_office'          => $formData['appl_phone_office'],
                    'contact_mobile'          => $formData['appl_phone_mobile']
                ];
                //var_dump($kmcData); exit;
                $kmcModel->updateKmc($kmcData);

                $studentInfo2 = $this->lobjStudentprofile->getStudentInfo($studentId);
                $this->view->studentInfo = $studentInfo2;
            }

            //education detail
            if (isset($formData['save_education_detail']) && $formData['save_education_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_education_detail']);
                //$formData['ae_institution'] = $formData['ae_university'];
                //unset($formData['ae_university']);

                $formData['createBy'] = $auth->getIdentity()->id;
                $formData['ae_role'] = 'admin';

                $educationDetailInfo = $this->lobjStudentprofile->getEducationDetails($studentInfo['sp_id']);

                //update
                /*if ($educationDetailInfo){
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_qualification', 'sp_id');

					//AUDIT ENTRY
					$auditDB = new App_Model_Audit();

					$log_audit = $auditDB->compareChanges($formData, $educationDetailInfo, array('ae_id','sp_id','ae_appl_id','ae_transaction_id','createDate'));

					if ( !empty($log_audit) )
					{
						$auditDB->recordLog('Education Details', 3, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records' );
					}
					//END AUDIT

                }else{*/
                $formData['sp_id'] = $studentInfo['sp_id'];
                $aeid = $this->lobjStudentprofile->insertStudentProfile('student_qualification', $formData);
                //}

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 27) == 'educationDetail_certificate') {
                                $definationCode = 'CEE';
                            } else if (substr($file, 0, 26) == 'educationDetail_transcript') {
                                $definationCode = 'TRE';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;
                            //var_dump($filepath);
                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_table_id'     => $aeid,
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }//exit;

                    //AUDIT
                    /*if ( !empty($log_audit2) )
					{
						$auditDB->recordLog('Education Details', 2, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records' );
					}*/
                }
            }

            //english proficiency
            if (isset($formData['save_english_proficiency']) && $formData['save_english_proficiency'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_english_proficiency']);

                $englishProficiencyInfo = $this->lobjStudentprofile->getEnglishProficiencyDetails($studentInfo['sp_id']);

                $formData['ep_date_taken'] = ($formData['ep_date_taken'] == '' ? NULL : date('Y-m-d', strtotime($formData['ep_date_taken'])));

                //update
                if ($englishProficiencyInfo) {
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_english_proficiency', 'sp_id');

                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $englishProficiencyInfo, ['ep_id', 'sp_id', 'ep_transaction_id', 'ep_updatedt', 'ep_updatedby']);

                    if (!empty($log_audit)) {
                        //Section Name, Section ID, StudentId, logData, UserId, Module
                        $auditDB->recordLog('English Proficiency Details', 4, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT	*/

                } else {
                    $formData['sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_english_proficiency', $formData);
                }

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 23) == 'englishProficiency_file') {
                                $definationCode = 'ENG1';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }

                    //AUDIT
                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('English Proficiency Details', 4, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //quantitative proficiency
            if (isset($formData['save_quantitative_proficiency']) && $formData['save_quantitative_proficiency'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_quantitative_proficiency']);

                $quantitatvieProficiencyInfo = $this->lobjStudentprofile->getQuantitativeProficiencyDetails($studentInfo['sp_id']);

                //update
                if ($quantitatvieProficiencyInfo) {
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_quantitative_proficiency', 'sp_id');

                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $quantitatvieProficiencyInfo, ['qp_id', 'sp_id', 'qp_trans_id', 'qp_createddt', 'qp_createdby']);

                    if (!empty($log_audit)) {
                        //Section Name, Section ID, StudentId, logData, UserId, Module
                        $auditDB->recordLog('Quantitative Proficiency Details', 5, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT	*/

                } else {
                    $formData['sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_quantitative_proficiency', $formData);
                }

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 32) == 'quantitativeProficiency_document') {
                                $definationCode = 'QUA';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }

                    //AUDIT
                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('Quantitative Proficiency Details', 5, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //research and publication
            if (isset($formData['save_research_publication']) && $formData['save_research_publication'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_research_publication']);

                $researchAndPublicationInfo = $this->lobjStudentprofile->getResearchAndPublicationDetails($studentInfo['sp_id']);

                $formData['rpd_date'] = $formData['rpd_date'] == '' ? NULL : date('Y-m-d', strtotime($formData['rpd_date']));


                //update
                if ($researchAndPublicationInfo) {
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_research_publication', 'sp_id');

                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $researchAndPublicationInfo, ['rpd_id', 'sp_id', 'rpd_trans_id', 'rpd_createddt', 'rpd_createdby']);

                    if (!empty($log_audit)) {
                        $auditDB->recordLog('Research/Publication Details', 6, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT	*/

                } else {
                    $formData['sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_research_publication', $formData);
                }

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 28) == 'researchPublication_document') {
                                $definationCode = 'RSC';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }

                    //AUDIT
                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('Research/Publication Details', 6, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //employment detail
            if (isset($formData['save_employment_detail']) && $formData['save_employment_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_employment_detail']);

                $formData['upd_by'] = $auth->getIdentity()->id;
                $formData['ae_role'] = 'admin';
                $formData['upd_date'] = date("Y-m-d H:i:s");

                //insert
                $formData['sp_id'] = $studentInfo['sp_id'];
                $this->lobjStudentprofile->insertStudentProfile('student_employment', $formData);

                //no need for audit
                unset($formData['upd_by'], $formData['ae_role'], $formData['upd_date']);

                /* AUDIT ENTRY	*/
                $auditDB = new App_Model_Audit();

                $datafields = [
                    'ae_status'          => '',
                    'ae_comp_name'       => '',
                    'ae_comp_address'    => '',
                    'ae_comp_phone'      => '',
                    'ae_comp_fax'        => '',
                    'ae_designation'     => '',
                    'ae_position'        => '',
                    'emply_year_service' => '',
                    'ae_industry'        => '',
                    'ae_job_desc'        => '',
                ];

                $log_audit = $auditDB->compareChanges($formData, $datafields, ['sp_id']);

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Employment Details', 7, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }

                /* END AUDIT	*/

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 23) == 'employmentDetail_letter') {
                                $definationCode = 'CONF';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];

                                    $this->lobjStudentprofile->insertUploadData($data);
                                }
                            }
                        }
                    }

                    //AUDIT
                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('Employment Details', 7, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //industry working experience
            if (isset($formData['save_industry_experience']) && $formData['save_industry_experience'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_industry_experience']);

                //getall
                $workexps = $this->lobjStudentprofile->getWorkExperience($studentInfo['sp_id']);
                $workxpList = [];
                foreach ($workexps as $xp) {
                    $workxpList[$xp['aw_industry_id']] = ['aw_industry_id' => $xp['aw_industry_id'], 'aw_years' => $xp['aw_years']];
                }


                //delete
                $this->lobjStudentprofile->deleteStudentProfile('student_working_experience', 'sp_id = ' . $studentInfo['sp_id']);

                //update
                if (isset($formData['iwe']) && isset($formData['yof'])) {
                    foreach ($formData['iwe'] as $iwe) {
                        $data = [
                            'sp_id'          => $studentInfo['sp_id'],
                            'aw_appl_id'     => $studentInfo['appl_id'],
                            'aw_trans_id'    => $studentInfo['transaction_id'],
                            'aw_industry_id' => $iwe,
                            'aw_years'       => $formData['yof'][$iwe]
                        ];
                        $this->lobjStudentprofile->insertStudentProfile('student_working_experience', $data);

                        /* AUDIT ENTRY	*/
                        $auditDB = new App_Model_Audit();

                        if (isset($workxpList[$iwe])) {
                            $datafields = $workxpList[$iwe];
                        } else {
                            $datafields = [
                                'aw_industry_id' => '',
                                'aw_years'       => '',
                            ];
                        }

                        $log_audit = $auditDB->compareChanges(['aw_industry_id' => $iwe, 'aw_years' => $formData['yof'][$iwe]], $datafields, ['sp_id', 'aw_appl_id', 'aw_trans_id']);


                        if (!empty($log_audit)) {
                            //Section Name, Section ID, StudentId, logData, UserId, Module
                            $auditDB->recordLog('Industry Working Experience', 8, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                        }

                        /* END AUDIT	*/
                    }
                }
            }

            //health condition
            if (isset($formData['save_health_condition']) && $formData['save_health_condition'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_health_condition']);

                $objdeftypeDB = new App_Model_Definitiontype();
                $result = $objdeftypeDB->fnGetDefinationsByLocale('Health Condition');

                $prevhealth = [];
                foreach ($result as $index => $r) {
                    //get data
                    $appHealth = $this->lobjStudentprofile->getHealthData($studentInfo['sp_id'], $r['idDefinition']);

                    if (isset($appHealth) && $appHealth['ah_status'] != '') {
                        $prevhealth[] = $r['idDefinition'];
                    }
                }

                //var_dump($formData); exit;
                //delete
                $this->lobjStudentprofile->deleteStudentProfile('student_health_condition', 'sp_id = ' . $studentInfo['sp_id']);

                //update
                $curhealth = [];
                if (isset($formData['iwe'])) {
                    foreach ($formData['iwe'] as $iwe) {
                        $data = [
                            'sp_id'       => $studentInfo['sp_id'],
                            'ah_appl_id'  => $studentInfo['appl_id'],
                            'ah_trans_id' => $studentInfo['transaction_id'],
                            'ah_status'   => $iwe
                        ];
                        $this->lobjStudentprofile->insertStudentProfile('student_health_condition', $data);
                        $curhealth[] = $iwe;
                    }
                }

                /* AUDIT */
                $auditDB = new App_Model_Audit();
                $log_audit = [[
                    'type'       => 'field',
                    'variable'   => 'health_condition',
                    'value'      => implode(',', $prevhealth),
                    'prev_value' => implode(',', $curhealth)
                ]];

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Health Condition', 9, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }
            }

            //financial particulars
            if (isset($formData['save_financial_particular']) && $formData['save_financial_particular'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_financial_particular']);

                $financialParticularInfo = $this->lobjStudentprofile->getFinancialParticular($studentInfo['sp_id']);

                //update
                if ($financialParticularInfo) {
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_financial', 'sp_id');

                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $financialParticularInfo);

                    if (!empty($log_audit)) {
                        $auditDB->recordLog('Financial Particulars', 10, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT	*/

                } else {
                    $formData['sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_financial', $formData);
                }

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 38) == 'financialParticular_affidavitStatement') {
                                $definationCode = 'FIN1';
                            } else if (substr($file, 0, 37) == 'financialParticular_sponsorshipLetter') {
                                $definationCode = 'FIN2';
                            } else if (substr($file, 0, 35) == 'financialParticular_guaranteeLetter') {
                                $definationCode = 'FIN3';
                            } else if (substr($file, 0, 26) == 'financialParticular_resume') {
                                $definationCode = 'FIN4';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }

                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('Financial Particulars', 10, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //accomodation detail
            if (isset($formData['save_accomodation_detail']) && $formData['save_accomodation_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_accomodation_detail']);

                $accomodationDetails = $this->lobjStudentprofile->getAccomodationDetails($studentInfo['sp_id']);

                if ($formData['acd_assistance'] == 0) {
                    $formData['acd_type'] = 0;
                } else {
                    if (isset($formData['tick'])) {
                        $formData['acd_type'] = $formData['tick'];
                    } else {
                        $formData['acd_type'] = 0;
                    }
                }

                unset($formData['tick']);

                $formData['sp_id'] = $studentInfo['sp_id'];
                $formData['acd_updateby'] = $auth->getIdentity()->id;
                $formData['acd_updatedt'] = date('Y-m-d');


                if ($accomodationDetails) {
                    $this->lobjStudentprofile->updateAccomodationDetails($formData, $studentInfo['sp_id']);
                } else {
                    $this->lobjStudentprofile->insertAccomodationDetails($formData);
                }
            }

            //visa details
            if (isset($formData['save_visa_detail']) && $formData['save_visa_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_visa_detail']);

                $visaDetailsInfo = $this->lobjStudentprofile->getVisaDetails($studentInfo['sp_id']);

                $formData['av_expiry'] = $formData['av_expiry'] == '' ? NULL : date('Y-m-d', strtotime($formData['av_expiry']));

                //update
                if ($visaDetailsInfo) {

                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $visaDetailsInfo);

                    if (!empty($log_audit)) {
                        $auditDB->recordLog('Visa Details', 11, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT	*/

                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_visa', 'sp_id');
                } else {
                    $formData['sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_visa', $formData);
                }
            }

            //essay
            if (isset($formData['save_essay_detail']) && $formData['save_essay_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_essay_detail']);

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 10) == 'essay_file') {
                                $definationCode = 'ESS';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $auditDB = new App_Model_Audit();
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }

                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('Essay', 14, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //research statement
            if (isset($formData['save_statement_research']) && $formData['save_statement_research'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_statement_research']);

                $dir = APP_DOC_PATH . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                //insert upload file in db
                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {
                            //get dcl id
                            $definationCode = '';
                            if (substr($file, 0, 18) == 'statement_research') {
                                $definationCode = 'RSC';
                            }

                            $definationId = $this->defModel->getIdByDefType('Document Type', $definationCode);
                            //$dclInfo = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $this->view->sectionId, $definationId['key']);

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sp_id'           => $studentInfo['sp_id'],
                                        //'ad_dcl_id' => $dclInfo['dcl_Id'],
                                        //'ad_table_id' => $formData['table_id'],
                                        'ad_type'         => $definationId['key'],
                                        'ad_section_id'   => $this->view->sectionId,
                                        'ad_table_name'   => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = ' . $this->view->sectionId),
                                        'ad_filepath'     => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'ad_filename'     => $fileRename,
                                        'ad_ori_filename' => $fileOriName,
                                        'ad_createddt'    => date('Y-m-d'),
                                        'ad_createdby'    => $auth->getIdentity()->id,
                                        'ad_role'         => 'admin'
                                    ];

                                    $this->lobjStudentprofile->insertUploadData($data);

                                    //AUDIT LOG
                                    $auditDB = new App_Model_Audit();
                                    $log_audit2 = [[
                                        'type'       => 'upload',
                                        'variable'   => $definationCode,
                                        'value'      => $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'prev_value' => ''
                                    ]];
                                }
                            }
                        }
                    }

                    if (!empty($log_audit2)) {
                        $auditDB->recordLog('Statement of Research Interest', 15, $studentId, $log_audit2, $auth->getIdentity()->iduser, 'records');
                    }
                }
            }

            //referees
            if (isset($formData['save_referees_detail']) && $formData['save_referees_detail'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_referees_detail']);

                $refreesDatilsInfo = $this->lobjStudentprofile->getRefereesDetails($studentInfo['sp_id']);

                //update
                if ($refreesDatilsInfo) {
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_referees', 'sp_id');

                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $refreesDatilsInfo);
                    if (!empty($log_audit)) {
                        $auditDB->recordLog('Referees', 16, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT */

                } else {
                    $formData['sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_referees', $formData);
                }
            }

            //relationship information
            if (isset($formData['save_relationship_information']) && $formData['save_relationship_information'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_relationship_information']);

                $relationshipInformationInfo = $this->lobjStudentprofile->getRelationshipInformation($studentInfo['sp_id']);

                //update
                if ($relationshipInformationInfo) {
                    $this->lobjStudentprofile->updateStudentProfile($formData, $studentInfo['sp_id'], 'student_relationship_information', 'ri_sp_id');


                    /* AUDIT ENTRY	*/
                    $auditDB = new App_Model_Audit();

                    $log_audit = $auditDB->compareChanges($formData, $relationshipInformationInfo);

                    if (!empty($log_audit)) {
                        $auditDB->recordLog('Relationship Information', 18, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                    }
                    /* END AUDIT */
                } else {
                    $formData['ri_sp_id'] = $studentInfo['sp_id'];
                    $this->lobjStudentprofile->insertStudentProfile('student_relationship_information', $formData);
                    //exit;
                }
            }

            //emergency contact
            if (isset($formData['save_emergency_contact']) && $formData['save_emergency_contact'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_emergency_contact']);
                unset($formData['ec_permanent_add']);

                $formData['upd_date'] = date("Y-m-d H:i:s");
                $formData['upd_by'] = $auth->getIdentity()->id;
                $formData['ec_role'] = 'admin';

                //insert
                $formData['ec_sp_id'] = $studentInfo['sp_id'];
                $this->lobjStudentprofile->insertStudentProfile('student_emergency_contact', $formData);

                unset($formData['upd_date']);
                unset($formData['upd_by']);
                unset($formData['ec_role']);

                /* AUDIT ENTRY	*/
                $auditDB = new App_Model_Audit();

                $datafields = [
                    'ec_type'         => '',
                    'ec_name'         => '',
                    'ec_add1'         => '',
                    'ec_add2'         => '',
                    'ec_add3'         => '',
                    'ec_country'      => '',
                    'ec_state'        => '',
                    'ec_city'         => '',
                    'ec_state_others' => '',
                    'ec_city_others'  => '',
                    'ec_postcode'     => '',
                    'ec_phone_mobile' => '',
                    'ec_phone_home'   => '',
                    'ec_phone_office' => '',
                    'ec_email'        => ''
                ];

                $log_audit = $auditDB->compareChanges($formData, $datafields, ['ec_sp_id']);

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Emergency Contact Information', 19, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }

                /* END AUDIT	*/
            }

            //curricular activities
            if (isset($formData['save_curricular_activities']) && $formData['save_curricular_activities'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_curricular_activities']);

                $formData['ca_date'] = $formData['ca_date'] == '' ? NULL : date('Y-m-d', strtotime($formData['ca_date']));

                //insert
                $formData['ca_sp_id'] = $studentInfo['sp_id'];

                $this->lobjStudentprofile->insertStudentProfile('student_curricular_activities', $formData);

                /* AUDIT ENTRY	*/
                $auditDB = new App_Model_Audit();

                $datafields = [
                    'ca_date' => '',
                    'ca_desc' => ''
                ];

                $log_audit = $auditDB->compareChanges($formData, $datafields, ['ca_seq', 'ca_sp_id']);

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Curricular Activities', 20, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }

                /* END AUDIT	*/
            }

            //student attribute
            if (isset($formData['save_student_attribute']) && $formData['save_student_attribute'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_student_attribute']);

                $formData['sa_rating'] = $formData['score'];

                unset($formData['score']);

                //insert
                $formData['sa_sp_id'] = $studentInfo['sp_id'];
                $this->lobjStudentprofile->insertStudentProfile('student_attribute', $formData);

                /* AUDIT ENTRY	*/
                $auditDB = new App_Model_Audit();

                $datafields = [
                    'sa_type'     => '',
                    'sa_category' => '',
                    'sa_rating'   => ''
                ];

                $log_audit = $auditDB->compareChanges($formData, $datafields, ['sa_sp_id']);

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Attributes', 21, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }
                /* END AUDIT	*/
            }

            //student comment
            if (isset($formData['save_student_comment']) && $formData['save_student_comment'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_student_comment']);

                $formData['scm_user'] = $auth->getIdentity()->id;

                //insert
                $formData['scm_sp_id'] = $studentInfo['sp_id'];
                $this->lobjStudentprofile->insertStudentProfile('student_comment', $formData);

                /* AUDIT ENTRY	*/
                $auditDB = new App_Model_Audit();

                $datafields = [
                    'scm_type' => '',
                    'scm_desc' => ''
                ];

                $log_audit = $auditDB->compareChanges($formData, $datafields, ['scm_sp_id', 'scm_user']);

                if (!empty($log_audit)) {
                    //Section Name, Section ID, StudentId, logData, UserId, Module
                    $auditDB->recordLog('Comments', 22, $studentId, $log_audit, $auth->getIdentity()->iduser, 'records');
                }
                /* END AUDIT	*/
            }

            //other document
            if (isset($formData['save_other_document']) && $formData['save_other_document'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_other_document']);

                $formData['sod_spid'] = $studentInfo['sp_id'];
                $formData['sod_createby'] = $auth->getIdentity()->id;
                $formData['sod_createdate'] = date('Y-m-d');
                $formData['sod_updby'] = $auth->getIdentity()->id;
                $formData['sod_upddate'] = date('Y-m-d');

                $sod_id = $this->otherDocModel->insertOtherDocument($formData);

                $dir = APP_DOC_PATH . '/student_other_document' . $studentInfo['sp_repository'];

                if (!is_dir($dir)) {
                    if (mkdir_p($dir) === false) {
                        throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, ['min' => 1, 'max' => 10]); //maybe soon we will allow more?
                //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, ['extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false]);
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                if ($files) {
                    foreach ($files as $file => $info) {
                        if ($adapter->isValid($file)) {

                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'] . '/' . $fileRename;

                            if ($adapter->isUploaded($file)) {
                                $adapter->addFilter('Rename', ['target' => $filepath, 'overwrite' => true], $file);
                                if ($adapter->receive($file)) {
                                    $data = [
                                        'sou_sodid'        => $sod_id,
                                        'sou_filename'     => $fileOriName,
                                        'sou_filerename'   => $fileRename,
                                        'sou_filelocation' => '/student_other_document' . $studentInfo['sp_repository'] . '/' . $fileRename,
                                        'sou_filesize'     => $info['size'],
                                        'sou_mimetype'     => $info['type'],
                                        'sou_updby'        => $auth->getIdentity()->id,
                                        'sou_upddate'      => date('Y-m-d')
                                    ];
                                    $this->otherDocModel->insertOtherDocumentUpload($data);
                                }
                            }
                        }
                    }
                }
            }

            if (isset($formData['save_fee_plan']) && $formData['save_fee_plan'] == 'Save') {
                unset($formData['sectionId']);
                unset($formData['save_fee_plan']);

                $feeData = [
                    'fs_id'                => $formData['feeplan'],
                    'resource_fee_exclude' => isset($formData['resource_fee_exclude']) ? $formData['resource_fee_exclude'] : 0
                ];
                $this->lobjStudentprofile->updateStudent($feeData, $studentId);
            }
        }

        $photo = $this->lobjStudentprofile->getStudentPhoto($studentInfo['sp_id']);
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . urlencode($photo['ad_filepath']);
        }

        $this->view->app_photo = $app_photo;
        //var_dump($this->view->app_photo_full);
        //var_dump($app_photo); exit;
    }

    public function loadContentAction()
    {
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();
        $studentId = $this->_getparam("id");
        $sectionId = $this->_getparam("sectionId");

        $sectionInfo = $this->lobjStudentprofile->getSectionById($sectionId);
        $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);
        //var_dump($studentInfo); exit;
        $auth = Zend_Auth::getInstance();

        switch ($sectionId) {
            case 1: //program details
                $programDetailForm = new Records_Form_ProgramDetailsForm([
                    'programId'     => $studentInfo['IdProgram'],
                    'modeOfStudy'   => $studentInfo['mode_of_study'],
                    'modeOfProgram' => $studentInfo['mode_of_program']
                ]);
                $programDetailForm->populate($studentInfo);
                $this->view->programDetailForm = $programDetailForm;
                break;
            case 2: //personal details
                $personalDetailForm = new Records_Form_PersonalDetailsForm(['country' => $studentInfo['appl_country'], 'ccountry' => $studentInfo['appl_ccountry'], 'state' => $studentInfo['appl_state'], 'cstate' => $studentInfo['appl_cstate']]);

                $studentInfo['appl_passport_expiry'] = ($studentInfo['appl_passport_expiry'] == NULL ? '' : date('d-m-Y', strtotime($studentInfo['appl_passport_expiry'])));
                $studentInfo['appl_dob'] = ($studentInfo['appl_dob'] == NULL ? '' : date('d-m-Y', strtotime($studentInfo['appl_dob'])));

                $dob = date('Y-m-d', strtotime($studentInfo['appl_dob']));
                $today = date('Y-m-d');
                $age = $today - $dob;
                $studentInfo['appl_age'] = $age;

                $studentInfo['appl_password'] = '';

                $personalDetailForm->populate($studentInfo);
                $this->view->personalDetailForm = $personalDetailForm;

                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'PID');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadIdList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadIdList = $uploadIdList;

                $definationId2 = $this->defModel->getIdByDefType('Document Type', 'PH');
                //$dclInfo2 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId2['key']);
                $uploadPhotoList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId2['key'], $sectionId);
                $this->view->uploadPhotoList = $uploadPhotoList;

                $definationId3 = $this->defModel->getIdByDefType('Document Type', 'FIN4');
                //$dclInfo3 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId3['key']);
                $uploadCVList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId3['key'], $sectionId);
                $this->view->uploadCVList = $uploadCVList;

                if ($studentInfo['password_changed_by'] != '') {
                    $changedby = $this->lobjCommon->getUseR($studentInfo['password_changed_by']);
                    $studentInfo['password_changed_by_name'] = $changedby['FullName'];
                }

                break;
            case 3: //education details
                $educationDetailForm = new Records_Form_EducationDetailsForm();
                $educationDetailInfo = $this->lobjStudentprofile->getEducationDetails($studentInfo['sp_id']);
                /*if ($educationDetailInfo){
                    $educationDetailForm->populate($educationDetailInfo);
                }*/

                if ($educationDetailInfo) {
                    foreach ($educationDetailInfo as $key => $educationDetailInfoLoop) {
                        $educationDetailInfo[$key]['documents'] = [];

                        //upload side
                        $definationId1 = $this->defModel->getIdByDefType('Document Type', 'CEE');
                        $uploadCertList = $this->lobjStudentprofile->getDocumentList2($studentInfo['sp_id'], $definationId1['key'], $sectionId, $educationDetailInfoLoop['ae_id']);
                        //$educationDetailInfo[$key]['documents'] = $uploadCertList;

                        if ($uploadCertList) {
                            foreach ($uploadCertList as $uploadCertLoop) {
                                array_push($educationDetailInfo[$key]['documents'], $uploadCertLoop);
                            }
                        }

                        $definationId2 = $this->defModel->getIdByDefType('Document Type', 'TRE');
                        $uploadTransList = $this->lobjStudentprofile->getDocumentList2($studentInfo['sp_id'], $definationId2['key'], $sectionId, $educationDetailInfoLoop['ae_id']);
                        //$educationDetailInfo[$key]['documents'] = $uploadTransList;

                        if ($uploadTransList) {
                            foreach ($uploadTransList as $uploadTransLoop) {
                                array_push($educationDetailInfo[$key]['documents'], $uploadTransLoop);
                            }
                        }
                    }
                }

                $this->view->educationDetailForm = $educationDetailForm;
                $this->view->educationDetailInfo = $educationDetailInfo;

                break;
            case 4: //english proficiency
                $englishProficiencyForm = new Records_Form_EnglishProficiencyForm();
                $englishProficiencyInfo = $this->lobjStudentprofile->getEnglishProficiencyDetails($studentInfo['sp_id']);
                $this->view->englishProficiencyInfo = $englishProficiencyInfo;
                $englishProficiencyInfo['ep_date_taken'] = ($englishProficiencyInfo['ep_date_taken'] == NULL || $englishProficiencyInfo['ep_date_taken'] == '1970-01-01' ? '' : date('d-m-Y', strtotime($englishProficiencyInfo['ep_date_taken'])));

                if ($englishProficiencyInfo) {
                    $englishProficiencyForm->populate($englishProficiencyInfo);
                }
                $this->view->englishProficiencyForm = $englishProficiencyForm;

                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'ENG1');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadEngProList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadEngProList = $uploadEngProList;

                break;
            case 5: //quantitative proficiency
                $quantitativeProficiencyForm = new Records_Form_QuantitativeProficiencyForm();
                $quantitatvieProficiencyInfo = $this->lobjStudentprofile->getQuantitativeProficiencyDetails($studentInfo['sp_id']);
                if ($quantitatvieProficiencyInfo) {
                    $quantitativeProficiencyForm->populate($quantitatvieProficiencyInfo);
                }
                $this->view->quantitativeProficiencyForm = $quantitativeProficiencyForm;

                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'QUA');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadQuanProList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadQuanProList = $uploadQuanProList;

                break;
            case 6: //research & publication
                $researchAndPublicationForm = new Records_Form_ResearchAndPublicationForm();
                $researchAndPublicationInfo = $this->lobjStudentprofile->getResearchAndPublicationDetails($studentInfo['sp_id']);

                $researchAndPublicationInfo['rpd_date'] = $researchAndPublicationInfo['rpd_date'] == NULL ? '' : date('d-m-Y', strtotime($researchAndPublicationInfo['rpd_date']));

                if ($researchAndPublicationInfo) {
                    $researchAndPublicationForm->populate($researchAndPublicationInfo);
                }
                $this->view->researchAndPublicationForm = $researchAndPublicationForm;

                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'RSC');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadResearchPubList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadResearchPubList = $uploadResearchPubList;

                break;
            case 7: //employment details
                $employmentDetailsForm = new Records_Form_EmploymentDetailsForm();
                $employmentDetailsInfo = $this->lobjStudentprofile->getEmploymentDetails($studentInfo['sp_id']);
                $this->view->employmentDetailsInfo = $employmentDetailsInfo;
                $this->view->employmentDetailsForm = $employmentDetailsForm;

                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'VL');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadEmploymentList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadEmploymentList = $uploadEmploymentList;

                break;
            case 8: //industry working experience
                $defModel = new App_Model_General_DbTable_Definationms();
                $employmentIndustryList = $defModel->getDataByType(114);
                $industryYearList = $defModel->getDataByType(111);
                $this->view->employmentIndustryList = $employmentIndustryList;
                $this->view->industryYearList = $industryYearList;
                break;
            case 9: //health condition
                $defModel = new App_Model_General_DbTable_Definationms();
                $healthConditionList = $defModel->getDataByType(109);
                $this->view->healthConditionList = $healthConditionList;
                break;
            case 10: //financial particular details
                $financialParticularForm = new Records_Form_FinancialParticularForm();
                $financialParticularInfo = $this->lobjStudentprofile->getFinancialParticular($studentInfo['sp_id']);

                $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($studentId);

                if ($financialParticularInfo) {
                    $financialParticularForm->populate($financialParticularInfo);
                }
                $this->view->financialParticularForm = $financialParticularForm;
                $this->view->taggingFinancialAid = $taggingFinancialAid;

                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'FIN1');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadFinStatementList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadFinStatementList = $uploadFinStatementList;

                $definationId2 = $this->defModel->getIdByDefType('Document Type', 'FIN2');
                //$dclInfo2 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId2['key']);
                $uploadSponLetterList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId2['key'], $sectionId);
                $this->view->uploadSponLetterList = $uploadSponLetterList;

                $definationId3 = $this->defModel->getIdByDefType('Document Type', 'FIN3');
                //$dclInfo3 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId3['key']);
                $uploadGLetterList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId3['key'], $sectionId);
                $this->view->uploadGLetterList = $uploadGLetterList;

                $definationId4 = $this->defModel->getIdByDefType('Document Type', 'FIN4');
                //$dclInfo4 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId4['key']);
                $uploadFinResumeList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId4['key'], $sectionId);
                $this->view->uploadFinResumeList = $uploadFinResumeList;

                break;
            case 11: //visa details
                $visaDetailsForm = new Records_Form_VisaDetailsForm();
                $visaDetailsInfo = $this->lobjStudentprofile->getVisaDetails($studentInfo['sp_id']);

                $visaDetailsInfo['av_expiry'] = $visaDetailsInfo['av_expiry'] == NULL ? '' : date('d-m-Y', strtotime($visaDetailsInfo['av_expiry']));

                if ($visaDetailsInfo) {
                    $visaDetailsForm->populate($visaDetailsInfo);
                }
                $this->view->visaDetailsForm = $visaDetailsForm;
                break;
            case 12: //accomodation details
                $accomodationDetailsForm = new Records_Form_AccomodationDetailsForm();
                $accomodationDetails = $this->lobjStudentprofile->getAccomodationDetails($studentInfo['sp_id']);
                $accomodationInfo = $this->lobjStudentprofile->getRoomList();
                $this->view->accomodationDetailsForm = $accomodationDetailsForm;
                $this->view->accomodationInfo = $accomodationInfo;
                $this->view->accomodationDetails = $accomodationDetails;
                if ($accomodationDetails) {
                    $accomodationDetailsForm->populate($accomodationDetails);
                }
                //var_dump($accomodationInfo); exit;
                break;
            case 14: //essay
                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'ESS');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadEssayList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadEssayList = $uploadEssayList;
                break;
            case 15: //research statement
                //upload side
                $definationId1 = $this->defModel->getIdByDefType('Document Type', 'RSC');
                //$dclInfo1 = $this->lobjStudentprofile->getDclInfo($studentInfo['appl_category'], $studentInfo['IdProgramScheme'], $sectionId, $definationId1['key']);
                $uploadResearchStatementList = $this->lobjStudentprofile->getDocumentList($studentInfo['sp_id'], $definationId1['key'], $sectionId);
                $this->view->uploadResearchStatementList = $uploadResearchStatementList;
                break;
            case 16: //referees details
                $refereesDetailsForm = new Records_Form_refereesDetailsForm();
                $refreesDatilsInfo = $this->lobjStudentprofile->getRefereesDetails($studentInfo['sp_id']);
                if ($refreesDatilsInfo) {
                    $refereesDetailsForm->populate($refreesDatilsInfo);
                }
                $this->view->refereesDetailsForm = $refereesDetailsForm;
                break;
            case 18: //relationship information
                $relationshipInformationForm = new Records_Form_RelationshipInformationForm();
                $relationshipInformationInfo = $this->lobjStudentprofile->getRelationshipInformation($studentInfo['sp_id']);
                if ($relationshipInformationInfo) {
                    $relationshipInformationForm->populate($relationshipInformationInfo);
                }
                $this->view->relationshipInformationForm = $relationshipInformationForm;
                break;
            case 19: //emergency contact information
                $emergencyContactForm = new Records_Form_EmergencyContactForm();
                $emergencyContectInfo = $this->lobjStudentprofile->getEmergencyContactInfo($studentInfo['sp_id']);
                $this->view->emergencyContactForm = $emergencyContactForm;
                $this->view->emergencyContactInfo = $emergencyContectInfo;
                break;
            case 20: //curricular activities
                $curricularActivitiesForm = new Records_Form_CurricularActivitiesForm();
                $curricularAcitvitiesInfo = $this->lobjStudentprofile->getCurricularActivities($studentInfo['sp_id']);
                $this->view->curricularActivitiesForm = $curricularActivitiesForm;
                $this->view->curricularActivitiesInfo = $curricularAcitvitiesInfo;
                break;
            case 21: //student attribute
                $attributeForm = new Records_Form_AttributesForm();
                $attributeInfo = $this->lobjStudentprofile->getAttributeInfo($studentInfo['sp_id']);
                $this->view->attributeForm = $attributeForm;
                $this->view->attributeInfo = $attributeInfo;
                break;
            case 22: //comment
                $commentForm = new Records_Form_CommentForm();
                $commentInfoPublic = $this->lobjStudentprofile->getCommentInfo($studentInfo['sp_id']);
                $commentInfoPrivate = $this->lobjStudentprofile->getCommentInfoPrivate($studentInfo['sp_id'], $auth->getIdentity()->id);
                $commentInfo = array_merge($commentInfoPublic, $commentInfoPrivate);
                $this->view->commentForm = $commentForm;
                $this->view->commentInfo = $commentInfo;
                break;
            case 24: //other document
                $otherDocForm = new Records_Form_OtherDocumentForm();
                $otherDocList = $this->otherDocModel->getOtherDocument($studentInfo['sp_id']);
                $this->view->otherDocForm = $otherDocForm;
                $this->view->otherDocList = $otherDocList;
                break;
            case 25: //fee plan
                $feePlanForm = new Records_Form_ProgramDetailsVSForm();
                $this->view->feePlanForm = $feePlanForm;
                $populateFeePlan['feeplan'] = $studentInfo['fs_id'];
                $feePlanForm->populate($populateFeePlan);
                break;
            default:
                break;
        }

        $this->view->studentInfo = $studentInfo;
        $this->view->sectionInfo = $sectionInfo;
        $this->view->studentId = $studentId;
        $this->view->sectionId = $sectionId;
        //exit;
    }

    public function deleteStudentProfileInfoAction()
    {
        $sectionId = $this->getParam('sectionId', null);
        $id = $this->getParam('id', null);
        $studentId = $this->getParam('studentId', null);

        switch ($sectionId) {
            case 7: //employment details
                $this->lobjStudentprofile->deleteStudentProfile('student_employment', 'ae_id = ' . $id);
                break;
            case 19: //emergency contact
                $this->lobjStudentprofile->deleteStudentProfile('student_emergency_contact', 'ec_id = ' . $id);
                break;
            case 20:
                $this->lobjStudentprofile->deleteStudentProfile('student_curricular_activities', 'ca_id = ' . $id);
                break;
            case 21:
                $this->lobjStudentprofile->deleteStudentProfile('student_attribute', 'sa_id = ' . $id);
                break;
            case 22:
                $this->lobjStudentprofile->deleteStudentProfile('student_comment', 'scm_id = ' . $id);
                break;
            case 24:
                $this->lobjStudentprofile->deleteStudentProfile('student_otherdocument_upload', 'sou_sodid = ' . $id);
                $this->lobjStudentprofile->deleteStudentProfile('student_otherdocument', 'sod_id = ' . $id);
                break;
            default:
                break;
        }

        $this->view->backURL = $this->view->url(['module' => 'records', 'controller' => 'studentprofile', 'action' => 'student-profile', 'id' => $studentId, 'sectionId' => $sectionId], 'default', true);

        //redirect
        $this->_redirect($this->view->backURL);

        exit();
    }

    public function loadStateAction()
    {
        $countryId = $this->_getParam('id', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $stateList = $this->lobjStudentprofile->getState($countryId);

        $json = Zend_Json::encode($stateList);

        echo $json;
        exit();
    }

    public function loadCityAction()
    {
        $stateId = $this->_getParam('id', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $cityList = $this->lobjStudentprofile->getCity($stateId);

        $json = Zend_Json::encode($cityList);

        echo $json;
        exit();
    }

    public function loadPositionAction()
    {
        $staffId = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        if ($staffId == '') {
            $staffId = 0;
        }

        $staffPosition = $this->lobjStudentprofile->getStaffPosition($staffId);

        if ($staffPosition) {
            $data = $staffPosition;
        } else {
            $data = ['staff_position' => ''];
        }

        $json = Zend_Json::encode($data);

        echo $json;
        exit();
    }

    public function studentSummaryAction()
    {
        $studentId = $this->_getparam("id");
        $sectionList = $this->lobjStudentprofile->getSection();
        $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);
        $auth = Zend_Auth::getInstance();

        //count age
        $dob = date('Y-m-d', strtotime($studentInfo['appl_dob']));
        $today = date('Y-m-d');
        $age = $today - $dob;
        $studentInfo['appl_age'] = $age;

        //student info
        $this->view->studentId = $studentId;
        $this->view->studentInfo = $studentInfo;
        $this->view->sectionList = $sectionList;
        //var_dump($studentInfo); exit;

        //educatio detail
        $educationDetailInfo = $this->lobjStudentprofile->getEducationDetails($studentInfo['sp_id']);
        $this->view->educationDetailInfo = $educationDetailInfo;
        //var_dump($educationDetailInfo); exit;

        //english proficiency
        $englishProficiencyInfo = $this->lobjStudentprofile->getEnglishProficiencyDetails($studentInfo['sp_id']);
        $this->view->englishProficiencyInfo = $englishProficiencyInfo;
        //var_dump($englishProficiencyInfo);exit();

        //quantitative proficiency
        $quantitativeProficiencyInfo = $this->lobjStudentprofile->getQuantitativeProficiencyDetails($studentInfo['sp_id']);
        $this->view->quantitativeProficiencyInfo = $quantitativeProficiencyInfo;
        //var_dump($quantitativeProficiencyInfo); exit;

        //research and publication
        $researchAndPublicationInfo = $this->lobjStudentprofile->getResearchAndPublicationDetails($studentInfo['sp_id']);
        $this->view->researchAndPublicationInfo = $researchAndPublicationInfo;
        //var_dump($researchAndPublicationInfo); exit;

        //employment detail
        $employmentDetailsInfo = $this->lobjStudentprofile->getEmploymentDetails($studentInfo['sp_id']);
        $this->view->employmentDetailsInfo = $employmentDetailsInfo;
        //var_dump($employmentDetailsInfo); exit;

        //industry working experience
        $defModel = new App_Model_General_DbTable_Definationms();
        $employmentIndustryList = $defModel->getDataByType(114);
        $industryYearList = $defModel->getDataByType(111);
        $this->view->employmentIndustryList = $employmentIndustryList;
        $this->view->industryYearList = $industryYearList;

        //health condition
        $healthConditionList = $defModel->getDataByType(109);
        $this->view->healthConditionList = $healthConditionList;

        //financial particular
        $financialParticularInfo = $this->lobjStudentprofile->getFinancialParticular($studentInfo['sp_id']);
        $this->view->financialParticularInfo = $financialParticularInfo;
        //var_dump($financialParticularInfo); exit;

        //visa detail
        $visaDetailsInfo = $this->lobjStudentprofile->getVisaDetails($studentInfo['sp_id']);
        $this->view->visaDetailsInfo = $visaDetailsInfo;
        //var_dump($visaDetailsInfo); exit;

        //referees
        $refreesDatilsInfo = $this->lobjStudentprofile->getRefereesDetails($studentInfo['sp_id']);
        $this->view->refreesDatilsInfo = $refreesDatilsInfo;
        //var_dump($refreesDatilsInfo); exit;

        //relationship informarion
        $relationshipInformationInfo = $this->lobjStudentprofile->getRelationshipInformation($studentInfo['sp_id']);
        $this->view->relationshipInformationInfo = $relationshipInformationInfo;
        //var_dump($relationshipInformationInfo); exit;

        //emergency contact
        $emergencyContectInfo = $this->lobjStudentprofile->getEmergencyContactInfo($studentInfo['sp_id']);
        $this->view->emergencyContactInfo = $emergencyContectInfo;

        //curricular activities
        $curricularAcitvitiesInfo = $this->lobjStudentprofile->getCurricularActivities($studentInfo['sp_id']);
        $this->view->curricularActivitiesInfo = $curricularAcitvitiesInfo;

        //student attribute
        $attributeInfo = $this->lobjStudentprofile->getAttributeInfo($studentInfo['sp_id']);
        $this->view->attributeInfo = $attributeInfo;

        //comment
        $commentInfoPublic = $this->lobjStudentprofile->getCommentInfo($studentInfo['sp_id']);
        $commentInfoPrivate = $this->lobjStudentprofile->getCommentInfoPrivate($studentInfo['sp_id'], $auth->getIdentity()->id);
        $commentInfo = array_merge($commentInfoPublic, $commentInfoPrivate);
        $this->view->commentInfo = $commentInfo;
    }

    public function printStudentSummaryAction()
    {

        $this->_helper->layout()->setLayout('/printstudentsummary');

        $studentId = $this->_getparam("id");
        $sectionList = $this->lobjStudentprofile->getSection();
        $studentInfo = $this->lobjStudentprofile->getStudentInfo($studentId);
        $auth = Zend_Auth::getInstance();

        //count age
        $dob = date('Y-m-d', strtotime($studentInfo['appl_dob']));
        $today = date('Y-m-d');
        $age = $today - $dob;
        $studentInfo['appl_age'] = $age;

        //student info
        $this->view->studentId = $studentId;
        $this->view->studentInfo = $studentInfo;
        $this->view->sectionList = $sectionList;
        //var_dump($studentInfo); exit;

        //educatio detail
        $educationDetailInfo = $this->lobjStudentprofile->getEducationDetails($studentInfo['sp_id']);
        $this->view->educationDetailInfo = $educationDetailInfo;
        //var_dump($educationDetailInfo); exit;

        //english proficiency
        $englishProficiencyInfo = $this->lobjStudentprofile->getEnglishProficiencyDetails($studentInfo['sp_id']);
        $this->view->englishProficiencyInfo = $englishProficiencyInfo;
        //var_dump($englishProficiencyInfo);exit();

        //quantitative proficiency
        $quantitativeProficiencyInfo = $this->lobjStudentprofile->getQuantitativeProficiencyDetails($studentInfo['sp_id']);
        $this->view->quantitativeProficiencyInfo = $quantitativeProficiencyInfo;
        //var_dump($quantitativeProficiencyInfo); exit;

        //research and publication
        $researchAndPublicationInfo = $this->lobjStudentprofile->getResearchAndPublicationDetails($studentInfo['sp_id']);
        $this->view->researchAndPublicationInfo = $researchAndPublicationInfo;
        //var_dump($researchAndPublicationInfo); exit;

        //employment detail
        $employmentDetailsInfo = $this->lobjStudentprofile->getEmploymentDetails($studentInfo['sp_id']);
        $this->view->employmentDetailsInfo = $employmentDetailsInfo;
        //var_dump($employmentDetailsInfo); exit;

        //industry working experience
        $defModel = new App_Model_General_DbTable_Definationms();
        $employmentIndustryList = $defModel->getDataByType(114);
        $industryYearList = $defModel->getDataByType(111);
        $this->view->employmentIndustryList = $employmentIndustryList;
        $this->view->industryYearList = $industryYearList;

        //health condition
        $healthConditionList = $defModel->getDataByType(109);
        $this->view->healthConditionList = $healthConditionList;

        //financial particular
        $financialParticularInfo = $this->lobjStudentprofile->getFinancialParticular($studentInfo['sp_id']);
        $this->view->financialParticularInfo = $financialParticularInfo;
        //var_dump($financialParticularInfo); exit;

        //visa detail
        $visaDetailsInfo = $this->lobjStudentprofile->getVisaDetails($studentInfo['sp_id']);
        $this->view->visaDetailsInfo = $visaDetailsInfo;
        //var_dump($visaDetailsInfo); exit;

        //referees
        $refreesDatilsInfo = $this->lobjStudentprofile->getRefereesDetails($studentInfo['sp_id']);
        $this->view->refreesDatilsInfo = $refreesDatilsInfo;
        //var_dump($refreesDatilsInfo); exit;

        //relationship informarion
        $relationshipInformationInfo = $this->lobjStudentprofile->getRelationshipInformation($studentInfo['sp_id']);
        $this->view->relationshipInformationInfo = $relationshipInformationInfo;
        //var_dump($relationshipInformationInfo); exit;

        //emergency contact
        $emergencyContectInfo = $this->lobjStudentprofile->getEmergencyContactInfo($studentInfo['sp_id']);
        $this->view->emergencyContactInfo = $emergencyContectInfo;

        //curricular activities
        $curricularAcitvitiesInfo = $this->lobjStudentprofile->getCurricularActivities($studentInfo['sp_id']);
        $this->view->curricularActivitiesInfo = $curricularAcitvitiesInfo;

        //student attribute
        $attributeInfo = $this->lobjStudentprofile->getAttributeInfo($studentInfo['sp_id']);
        $this->view->attributeInfo = $attributeInfo;

        //comment
        $commentInfoPublic = $this->lobjStudentprofile->getCommentInfo($studentInfo['sp_id']);
        $commentInfoPrivate = $this->lobjStudentprofile->getCommentInfoPrivate($studentInfo['sp_id'], $auth->getIdentity()->id);
        $commentInfo = array_merge($commentInfoPublic, $commentInfoPrivate);
        $this->view->commentInfo = $commentInfo;

        $photo = $this->lobjStudentprofile->getStudentPhoto($studentInfo['sp_id']);
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = '/documents' . $photo['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . APP_DOC_PATH . urlencode($photo['ad_filepath']);
        }

        $this->view->app_photo = $app_photo;
        //var_dump($photo); exit;
        //exit;
    }

    public function deleteUploadFileAction()
    {
        $sectionId = $this->getParam('sectionId', 0);
        $id = $this->getParam('id', 0);
        $studentId = $this->getParam('studentId', 0);

        //delete file
        $this->lobjStudentprofile->deleteStudentProfile('student_documents', 'ad_id = ' . $id);

        $this->view->backURL = $this->view->url(['module' => 'records', 'controller' => 'studentprofile', 'action' => 'student-profile', 'id' => $studentId, 'sectionId' => $sectionId], 'default', true);

        //redirect
        $this->_redirect($this->view->backURL);

        exit();
    }

    public function loadInstitutionAction()
    {
        $countryid = $this->_getParam('countryid', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $institution = $this->lobjStudentprofile->getInstitution($countryid);
        $json = Zend_Json::encode($institution);

        echo $json;
        exit();
    }

    private function encrypt_decrypt($data, $encrypt)
    {
        if ($encrypt == true) {
            $output = base64_encode(convert_uuencode($data));
        } else {
            $output = convert_uudecode(base64_decode($data));
        }
        return $output;
    }

    static function getRemark($data, $landscape)
    {

        $model = new Records_Model_DbTable_Academicprogressremarks();

        $remarks = '';

        if ($data['replace_subject'] != null && $data['replace_subject'] != 0) {

            $subjectRepeat = $model->getSubjectReplace($data['replace_subject']);

            if ($subjectRepeat) {
                $subject = $model->getSubjectById($subjectRepeat['IdSubject']);

                $remarks = 'Replace/Retake with ' . $subject['SubCode'];
            }
        } else if (($data['Active'] == 4 || $data['Active'] == 9) && $data['exam_status'] != 'U' && $data['exam_status'] != 'EX' && $data['exam_status'] != 'CT') {
            $remarks = 'Replace/Retake';
        } else if ($data['exam_status'] == 'CT') {
            $ch = $data['credit_hour_registered'] != null ? $data['credit_hour_registered'] : $data['CreditHours'];
            if ($data['IdSubject'] == 111) {
                $remarks = 'Credit hours taken is ' . $ch;
            } else if ($landscape['AssessmentMethod'] == 'point') {
                $remarks = 'Credit transfer';
            }
        }


        return $remarks;
    }

    public function calculateCGPA(array $data, $program_id)
    {
        //var_dump($data);
        $total_point = 0;
        $total_credit = 0;
        $subject_array = [];
        $subject_repeat = [];

        if ($data) {
            foreach ($data as $val) {
                if (in_array($val['IdSubject'], $subject_array)) {
                    array_push($subject_repeat, $val['IdSubject']);
                } else if ($val['replace_subject'] != null) {
                    array_push($subject_repeat, $val['IdSubject']);
                }

                array_push($subject_array, $val['IdSubject']);
            }
        }

        //var_dump($subject_repeat);
        //exit;

        if ($data) {
            foreach ($data as $val) {
                if ($val["exam_status"] != "EX" && $val["exam_status"] != "CT" && $val["exam_status"] != "U" && $val["exam_status"] != 'IP' && $val["exam_status"] != "" && $val["exam_status"] != null) {

                    $grade = $this->getGrade($program_id, $val['IdSubject'], $val['grade_name']);

                    if (!in_array($val['IdSubject'], $subject_repeat)) {
                        if ($grade && $grade != null) {

                            $current_point = $grade['GradePoint'] * $val['CreditHours'];

                            $total_point = $total_point + $current_point;
                            $total_credit = $total_credit + $val['CreditHours'];
                        }
                    } else if ($val['Active'] == 4) {
                        if ($grade && $grade != null) {

                            $current_point = $grade['GradePoint'] * $val['CreditHours'];

                            $total_point = $total_point + $current_point;
                            $total_credit = $total_credit + $val['CreditHours'];
                        }
                    }
                }
            }
        }

        if ($total_credit == 0) {
            $cgpa = 0;
        } else {
            $cgpa = $total_point / $total_credit;
        }
        $credit_earn = $total_credit;

        $total = ['cgpa' => $cgpa, 'credit_earn' => $credit_earn];

        return $total;
    }

    private function getGrade($program_id, $subject_id, $subject_grade)
    {

        $db = Zend_Db_Table::getDefaultAdapter();


        //1st : check setup yang base on effective semester, program & subject
        $select_grade = $db->select()
            ->from(['gsm' => 'tbl_gradesetup_main'])
            ->join(['g' => 'tbl_gradesetup'], 'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain', ['GradeDesc', 'GradePoint', 'Pass'])
            ->join(['d' => 'tbl_definationms'], 'd.idDefinition=g.Grade', ['GradeName' => 'DefinitionDesc'])
            ->join(['sm' => 'tbl_semestermaster'], 'gsm.IdSemester = sm.IdSemesterMaster', ['*'])
            ->where('sm.SemesterMainStartDate <= NOW()')
            ->where('gsm.BasedOn = 1 ')//program	& subject
            ->where('gsm.IdProgram = ?', $program_id)
            ->where('gsm.IdSubject = ?', $subject_id)
            ->where('d.DefinitionCode = ?', $subject_grade);

        $grade = $db->fetchRow($select_grade);


        if (!$grade) {

            //check setup yang base on effective semester, program
            $select_grade2 = $db->select()
                ->from(['gsm' => 'tbl_gradesetup_main'])
                ->join(['g' => 'tbl_gradesetup'], 'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain', ['GradeDesc', 'GradePoint', 'Pass'])
                ->join(['d' => 'tbl_definationms'], 'd.idDefinition=g.Grade', ['GradeName' => 'DefinitionDesc'])
                ->join(['sm' => 'tbl_semestermaster'], 'gsm.IdSemester = sm.IdSemesterMaster', ['*'])
                ->where('sm.SemesterMainStartDate <= NOW()')
                ->where('gsm.BasedOn = 2 ')//program
                ->where('gsm.IdProgram = ?', $program_id)
                ->where('d.DefinitionCode = ?', $subject_grade);

            $grade2 = $db->fetchRow($select_grade2);

            if (!$grade2) {


                //get progrm award
                $programDB = new GeneralSetup_Model_DbTable_Program();
                $program = $programDB->fngetProgramData($program_id);

                //check setup yang base on effective semester, award
                $select_grade3 = $db->select()
                    ->from(['gsm' => 'tbl_gradesetup_main'])
                    ->join(['g' => 'tbl_gradesetup'], 'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain', ['GradeDesc', 'GradePoint', 'Pass'])
                    ->join(['d' => 'tbl_definationms'], 'd.idDefinition=g.Grade', ['GradeName' => 'DefinitionDesc'])
                    ->join(['sm' => 'tbl_semestermaster'], 'gsm.IdSemester = sm.IdSemesterMaster', ['*'])
                    ->where('sm.SemesterMainStartDate <= NOW()')
                    ->where('gsm.BasedOn = 0 ')//award
                    ->where('gsm.IdAward = ?', $program['Award'])
                    ->where('d.DefinitionCode = ?', $subject_grade);


                $grade3 = $db->fetchRow($select_grade3);
                $grade = $grade3;


            } else {
                $grade = $grade2;
            }

        }

        //throw exception if grade never been setup
        if (!$grade) {
            //throw new exception('There is no grade setup for subject id: '.$subject_id);

            $grade = null;
        }
        return $grade;
    }

    public function printAcademicProgressAction()
    {
        $registrationId = $this->_getParam('id', null);

        $semMasterObj = new GeneralSetup_Model_DbTable_Semestermaster();

        //get student info
        $studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
        $studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($registrationId);
        $this->view->studentdetails = $studentdetails;

        // display name for sysmester syllabus chosen
        $IdSemesterMain = $studentdetails['IdSemesterMain'];
        $IdSemesterDetails = $studentdetails['IdSemesterDetails'];

        if ($IdSemesterMain != '' && $IdSemesterMain != 0) {
            $semResultData = $semMasterObj->fngetSemestermainDetails($IdSemesterMain);
            $this->view->semesterNameData = $semResultData[0]['SemesterMainName'];
        }

        if ($IdSemesterDetails != '' && $IdSemesterDetails != 0) {
            $semDetailObj = new GeneralSetup_Model_DbTable_Semester();
            $semResultData = $semDetailObj->fngetsemdetailByid($IdSemesterDetails);
            $this->view->semesterNameData = $semResultData[0]['SemesterCode'];
        }

        //cek landscape
        $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
        $landscape = $landscapeDb->getData($studentdetails["IdLandscape"]);
        $this->view->landscape = $landscape;

        if ($landscape["LandscapeType"] == 43 || $landscape["LandscapeType"] == 44 || $landscape["LandscapeType"] == 42) {//Semester Based

            //get total registered semester for course registration
            $studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
            $semester = $studentSemesterDB->getRegisteredSemester($registrationId);
            $this->view->semester = $semester;

            //GET CURRENT Semester
            $ProgramScheme = new GeneralSetup_Model_DbTable_Programscheme();
            $program_scheme = $ProgramScheme->getSchemeById($landscape['IdProgramScheme']);

            $curr_semester = $semMasterObj->getCurrentSemesterScheme($program_scheme['IdScheme']);
            global $globalcurrSemester;
            $globalcurrSemester = $curr_semester;

            //Academic Progress
            $landscapeId = $studentdetails["IdLandscape"];
            $this->view->landscapeId = $landscapeId;
            $this->view->landscapeType = $landscape["LandscapeType"];
            $programId = $studentdetails["IdProgram"];
            $this->view->programId = $programId;

            //get landscape info
            $landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
            $landscape = $landscapeDB->getLandscapeDetails($landscapeId);

            global $globallandscape;
            $globallandscape = $landscape;
            //$this->view->landscape = $landscape;

            //get program requirement info
            $progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
            $programRequirement = $progReqDB->getlandscapecoursetype($programId, $landscapeId);
            $this->view->programrequirement = $programRequirement;
            $progMajDb = new GeneralSetup_Model_DbTable_ProgramMajoring();
            $majoring = $progMajDb->getData($programId);
            $this->view->majoring = $majoring;

            //get Compulsory Common Course
            $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
            $compulsory_course = $landscapeSubjectDb->getCommonCourse($programId, $landscapeId, 1);
            $this->view->compulsory_course = $compulsory_course;

            //get Not Compulsory Common Course
            $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
            $elective_course = $landscapeSubjectDb->getCommonCourse($programId, $landscapeId, 2);
            $this->view->elective_course = $elective_course;

            /**
             ** GET SUBJECT FOR ACADEMIC PROGRESS
             **/
            if ($this->view->landscapeType == 43 || $this->view->landscapeType == 42) {
                // GET ALL SUBJECT TAKEN SORT BY DATE DESC
                $recordDb = new Records_Model_DbTable_Academicprogress();
                $courses = $recordDb->getAllregistered($registrationId);

                if ($this->view->landscapeType == 43) {
                    $compulsory['completed'] = [];
                    $compulsory['registered'] = [];
                    $compulsory['open'] = [];

                    $elective['completed'] = [];
                    $elective['registered'] = [];
                    $elective['open'] = [];

                    $majoringsub['completed'] = [];
                    $majoringsub['registered'] = [];
                    $majoringsub['open'] = [];

                    $research = [];
                } else {
                    $compulsory = [];
                    $elective = [];
                    $research = [];
                }

                $subject_ids = [];
                $z = 0;
                $p = 0;
                $q = 0;
                $r = 0;
                $countCtCredit = 0;
                $countExCredit = 0;
                $countCompletePaperCore = 0;
                $countCompletePaperElective = 0;
                $countCompletePaperMajoring = 0;
                $countCompleteCreditHoursCore = 0;
                $countCompleteCreditHoursElective = 0;
                $countCompleteCreditHoursMajoring = 0;
                $countCreditHoursCore = 0;
                $countCreditHoursElective = 0;
                $countCreditHoursMajoring = 0;
                $countCreditHoursCore2 = 0;
                $countCreditHoursElective2 = 0;
                $cum_elective_ch = 0;
                $cum_majoring_ch = 0;
                $researchGred = '-';
                $researchCreditHours = 0;
                $researchRemarks = '';
                $countCreditHoursResearch = 0;

                $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();

                foreach ($courses as $a => $course) {
                    $subsem = $recordDb->getSemesterById($course['IdSemesterMain']);

                    $publishresult = false;

                    if ($subsem) {
                        $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                        if ($publishInfo) {
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                            $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                $publishresult = true;
                            }
                        }
                    }

                    $equivid = false;
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $course['IdSubject']);
                    if (!$course_status) {
                        $equivid = $recordDb->checkEquivStatus($landscapeId, $course["IdSubject"]);

                        if ($equivid) {
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $equivid);
                        }
                    }

                    $equivsubid = $recordDb->checkEquivStatus($landscapeId, $course["IdSubject"]);

                    if ($equivsubid) {
                        $subject_ids[$q] = $equivsubid;
                        $q++;
                    }
                    //$getRegisteredCompleteCourses;
                    if ($this->view->landscapeType == 43) {
                        //var_dump($course);
                        //var_dump($course_status);
                        if ($course_status['DefinitionDesc'] == "Core") {
                            //var_dump($course);
                            //$completed=$recordDb->checkcompleted($studentdetails['IdStudentRegistration'],$course["IdSubject"]);
                            //$others=$recordDb->checkregistered($studentdetails['IdStudentRegistration'],$course["IdSubject"]);
                            if (($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U"))//Complete
                            {
                                //echo "<pre>";print_r($course);echo "</pre><hr>";
                                $compulsory['completed'][$z] = $course;
                                $compulsory['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $compulsory['completed'][$z]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $compulsory['completed'][$z]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    if ($equivid) {
                                        $subject_ids[$q] = $equivid;
                                    }

                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 394) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursCore = $ch + $countCreditHoursCore;
                                            } else {
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursCore = $countCompleteCreditHoursCore + $ch2;
                                        $countCompletePaperCore++;
                                    }
                                }

                                $z++;
                            } else if ($course["exam_status"] == "C") {
                                $compulsory['completed'][$z] = $course;
                                $compulsory['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $compulsory['completed'][$z]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $compulsory['completed'][$z]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    if ($equivid) {
                                        $subject_ids[$q] = $equivid;
                                    }

                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 394) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursCore = $ch + $countCreditHoursCore;
                                            } else {
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursCore = $countCompleteCreditHoursCore + $ch2;
                                        $countCompletePaperCore++;
                                    }
                                }

                                $z++;
                            } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                                $compulsory['registered'][$z] = $course;
                                $compulsory['registered'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course['grade_name'] : '';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"] == "U") {
                                    $compulsory['registered'][$z]['CreditHours'] = 0;
                                    $compulsory['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 394) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursCore = $ch + $countCreditHoursCore;
                                            } else {
                                                $countCreditHoursCore++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
                            }
                        } elseif ($course_status['DefinitionDesc'] == "Elective" && $course['CourseType'] != 3) {
                            //var_dump($course);
                            if (($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U"))//Complete
                            {
                                //echo "<pre>";print_r($course);echo "</pre><hr>";
                                $elective['completed'][$p] = $course;
                                $elective['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $elective['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $elective['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_elective_ch = $cum_elective_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 275) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursElective = $ch + $countCreditHoursElective;
                                            } else {
                                                $countCreditHoursElective++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursElective = $countCompleteCreditHoursElective + $ch2;
                                        $countCompletePaperElective++;
                                    }
                                }

                                $p++;
                            } else if ($course["exam_status"] == "C") {
                                $elective['completed'][$p] = $course;
                                $elective['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $elective['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $elective['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_elective_ch = $cum_elective_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 275) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursElective = $ch + $countCreditHoursElective;
                                            } else {
                                                $countCreditHoursElective++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursElective = $countCompleteCreditHoursElective + $ch2;
                                        $countCompletePaperElective++;
                                    }
                                }

                                $p++;
                            } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                                $elective['registered'][$z] = $course;
                                $elective['registered'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course['grade_name'] : '';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"] == "U") {
                                    $elective['registered'][$z]['CreditHours'] = 0;
                                    $elective['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 275) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursElective = $ch + $countCreditHoursElective;
                                            } else {
                                                $countCreditHoursElective++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
                            }
                        } else if ($course_status['DefinitionDesc'] == "Majoring" && $course['CourseType'] != 3) {
                            //var_dump($course);
                            if (($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U"))//Complete
                            {
                                //echo "<pre>";print_r($course);echo "</pre><hr>";
                                $majoringsub['completed'][$p] = $course;
                                $majoringsub['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $majoringsub['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $majoringsub['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_majoring_ch = $cum_majoring_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 1054) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursMajoring = $ch + $countCreditHoursMajoring;
                                            } else {
                                                $countCreditHoursMajoring++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursMajoring = $countCompleteCreditHoursMajoring + $ch2;
                                        $countCompletePaperMajoring++;
                                    }
                                }

                                $p++;
                            } else if ($course["exam_status"] == "C") {
                                $majoringsub['completed'][$p] = $course;
                                $majoringsub['completed'][$p]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                                if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                    $majoringsub['completed'][$p]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                                }

                                if ($course["grade_name"] == "U") {
                                    $majoringsub['completed'][$p]['CreditHours'] = 0;
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                    $subject_ids[$q] = $course['IdSubject'];
                                    $cum_majoring_ch = $cum_majoring_ch + $course["CreditHours"];
                                    $q++;

                                } else {
                                    if ($equivid) {
                                        $pos = array_search($equivid, $subject_ids);
                                        unset($subject_ids[$pos]);
                                    }
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 1054) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursMajoring = $ch + $countCreditHoursMajoring;
                                            } else {
                                                $countCreditHoursMajoring++;
                                            }
                                        }
                                    }

                                    $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                    //if ($course["grade_name"]!="F" && $course['mark_approval_status']==2 && $publishresult==true && ($course['Active']!=4 || $course['cgpa_calculation']==0) && ($course['Active']!=9 || $course['cgpa_calculation']==0) && ($course['Active']!=10 || $course['cgpa_calculation']==0) && ($course['Active']!=6 || $course['cgpa_calculation']==0)){
                                    if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                        $countCompleteCreditHoursMajoring = $countCompleteCreditHoursMajoring + $ch2;
                                        $countCompletePaperMajoring++;
                                    }
                                }

                                $p++;
                            } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                                $majoringsub['registered'][$z] = $course;
                                $majoringsub['registered'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course['grade_name'] : '';
                                $subject_ids[$q] = $course['IdSubject'];

                                if ($course["grade_name"] == "U") {
                                    $majoringsub['registered'][$z]['CreditHours'] = 0;
                                    $majoringsub['registered'][$z]['grademark'] = $course["grade_name"];
                                }

                                if ($course['exam_status'] == 'CT') {
                                    $countCtCredit = $course['CreditHours'] + $countCtCredit;
                                } else if ($course['exam_status'] == 'EX') {
                                    $countExCredit = $course['CreditHours'] + $countExCredit;
                                }

                                //count paper/credit hourse
                                if ($course["exam_status"] != "U") {
                                    foreach ($programRequirement as $programRequirementLoop) {
                                        if ($programRequirementLoop['SubjectType'] == 1054) {
                                            if ($programRequirementLoop['RequirementType'] == 1) {
                                                $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                                $countCreditHoursMajoring = $ch + $countCreditHoursMajoring;
                                            } else {
                                                $countCreditHoursMajoring++;
                                            }
                                        }
                                    }
                                }

                                $q++;
                                $z++;
                            }
                        } else if ($course_status['DefinitionDesc'] == "Research" || $course['CourseType'] == 3) {
                            if ($researchGred == '-') {
                                if ($course['exam_status'] != 'IP' && $course['exam_status'] != '') {
                                    if ($course['mark_approval_status'] == 2) {
                                        $researchGred = $course['grade_name'];
                                        $researchRemarks = '';
                                    } else {
                                        $researchRemarks = 'In Progress';
                                    }
                                } else {
                                    $researchRemarks = 'In Progress';
                                }
                            }

                            //count credit hours
                            if ($course["exam_status"] != "U") {
                                $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : 0;
                                $researchCreditHours = $researchCreditHours + $ch2;
                                if ($course["exam_status"] != null && $course['mark_approval_status'] == 2) {
                                    if ($course_status['DefinitionDesc'] == "Elective") {
                                        $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : 0;
                                        $countCompleteCreditHoursElective = $countCompleteCreditHoursElective + $ch;
                                    } else {
                                        $countCreditHoursResearch = $countCreditHoursResearch + $ch;
                                    }
                                }
                            }

                            //$researchCreditHours = $course['credit_hour_registered'] + $researchCreditHours;

                            $research['name'] = $course['SubCode'] . ' ' . $course['SubName'];
                            $research['credithours'] = $researchCreditHours;
                            $research['grademark'] = $researchGred;
                            $research['remarks'] = $researchRemarks;
                            $research['typesub'] = $course_status['DefinitionDesc'] == "Research" ? 'Research' : 'Project Paper';
                            //var_dump($researchCreditHours);
                            $r++;
                        }
                    } elseif ($this->view->landscapeType == 42) {
                        if ($course["exam_status"] == "C" || $course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U")//Complete
                        {
                            //echo "<pre>";print_r($course);echo "</pre><hr>";
                            $compulsory[$course_status['block']]['completed'][$z] = $course;
                            $compulsory[$course_status['block']]['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course["grade_name"] : '';
                            if ($course["exam_status"] == "EX" || $course["exam_status"] == "CT" || $course["exam_status"] == "U") {
                                $compulsory[$course_status['block']]['completed'][$z]['grademark'] = $landscape['AssessmentMethod'] != 'point' ? $course["exam_status"] : $course["grade_name"];
                            }

                            if ($course["grade_name"] == "U") {
                                $compulsory[$course_status['block']]['completed'][$z]['CreditHours'] = 0;
                            }

                            if ($course['exam_status'] == 'CT') {
                                $countCtCredit = $course['CreditHours'] + $countCtCredit;
                            } else if ($course['exam_status'] == 'EX') {
                                $countExCredit = $course['CreditHours'] + $countExCredit;
                            }

                            if ($course["grade_name"] != "F") { //Fail Masuk dlm list tapi not completed
                                $subject_ids[$q] = $course['IdSubject'];
                                if ($equivid) {
                                    $subject_ids[$q] = $equivid;
                                }

                                $q++;

                            } else {
                                if ($equivid) {
                                    $pos = array_search($equivid, $subject_ids);
                                    unset($subject_ids[$pos]);
                                }
                            }

                            if ($course["exam_status"] != "U") {
                                $ch2 = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];

                                if ($course["grade_name"] != "F" && $course['mark_approval_status'] == 2 && $publishresult == true) {
                                    if ($programRequirement[0]['RequirementType'] == 1) {
                                        $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                        $countCreditHoursCore = $ch + $countCreditHoursCore;
                                    } else {
                                        $countCreditHoursCore++;
                                    }

                                    $countCompleteCreditHoursCore = $countCompleteCreditHoursCore + $ch2;
                                    $countCompletePaperCore++;
                                }
                            }

                            $z++;
                        } else /*if($course["exam_status"]=="" && $course["grade_status"]=="")*/ {
                            //echo "masuk";
                            $compulsory[$course_status['block']]['completed'][$z] = $course;
                            $compulsory[$course_status['block']]['completed'][$z]['grademark'] = ($course['mark_approval_status'] == 2 && $publishresult == true) ? $course['grade_name'] : '';
                            $subject_ids[$q] = $course['IdSubject'];
                            $q++;

                            if ($course["grade_name"] == "U") {
                                $compulsory[$course_status['block']]['completed'][$z]['CreditHours'] = 0;
                            }

                            if ($course['exam_status'] == 'CT') {
                                $countCtCredit = $course['CreditHours'] + $countCtCredit;
                            } else if ($course['exam_status'] == 'EX') {
                                $countExCredit = $course['CreditHours'] + $countExCredit;
                            }

                            //count paper/credit hourse
                            if ($course["exam_status"] != "U") {
                                if ($programRequirement[0]['RequirementType'] == 1) {
                                    $ch = $course['credit_hour_registered'] != null && $course['credit_hour_registered'] != '' ? $course['credit_hour_registered'] : $course['CreditHours'];
                                    $countCreditHoursCore = $ch + $countCreditHoursCore;
                                } else {
                                    $countCreditHoursCore++;
                                }
                            }

                            $z++;
                        }
                    }
                }

                //get remaining Course
                $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
                $remaining_courses = $landscapeSubjectDb->getRemainingCourses($programId, $landscapeId, $subject_ids);
                $remaining_courses_majoring = $landscapeSubjectDb->getMajoringCourseRemaining($programId, $landscapeId, $subject_ids, $studentdetails['IdProgramMajoring']);
                $elecreq = $recordDb->getElectiveReq($landscapeId);
                $majreq = $recordDb->getMajoringReq($landscapeId);

                $remaining_courses_check = [0];
                if ($remaining_courses) {
                    foreach ($remaining_courses as $rc) {
                        array_push($remaining_courses_check, $rc['IdSubject']);
                    }
                }

                if ($remaining_courses_majoring) {
                    foreach ($remaining_courses_majoring as $rcm) {
                        if (!in_array($rcm['IdSubject'], $remaining_courses_check))
                            array_push($remaining_courses, $rcm);
                    }
                }

                $researchRem = [];

                foreach ($remaining_courses as $b => $remaining) {
                    if ($this->view->landscapeType == 43) {
                        if ($remaining['DefinitionDesc'] == "Core") {
                            $compulsory['open'][$z] = $remaining;
                            $compulsory['open'][$z]['grademark'] = '-';
                            $z++;
                        } elseif ($remaining['DefinitionDesc'] == 'Elective') {
                            if ($cum_elective_ch < $elecreq) {
                                $elective['open'][$p] = $remaining;
                                $elective['open'][$p]['grademark'] = '-';
                                $p++;
                            }
                        } elseif ($remaining['DefinitionDesc'] == 'Majoring') {
                            if ($cum_majoring_ch < $majreq) {
                                $majoringsub['open'][$p] = $remaining;
                                $majoringsub['open'][$p]['grademark'] = '-';
                                $p++;
                            }
                        } else if ($remaining['DefinitionDesc'] == 'Research') {
                            $researchRem['open'][$p] = $remaining;
                            $researchRem['open'][$p]['grademark'] = '-';
                            $p++;
                        }
                    } else {
                        $compulsory[$remaining['block']]['open'][$z] = $remaining;
                        $compulsory[$remaining['block']]['open'][$z]['grademark'] = '-';
                        $z++;
                    }
                }

                if ($this->view->landscapeType == 43) {
                    if (array_key_exists("registered", $compulsory)) {
                        $row_span = count($compulsory['completed']) + count($compulsory['registered']);
                    } else {
                        $row_span = count($compulsory['completed']);
                    }

                    global $globalrowspan;
                    $globalrowspan = $row_span;

                    if (array_key_exists("registered", $elective)) {
                        $row_span_elective = count($elective['completed']) + count($elective['registered']);
                    } else {
                        $row_span_elective = count($elective['completed']);
                    }

                    global $globalrowspanelective;
                    $globalrowspanelective = $row_span_elective;
                    //$this->view->rowspan_elective = $row_span_elective;
                    global $globalelective;
                    $globalelective = $elective;
                    //$this->view->elective = $elective;

                    if (array_key_exists("registered", $majoringsub)) {
                        $row_span_majoring = count($majoringsub['completed']) + count($majoringsub['registered']);
                    } else {
                        $row_span_majoring = count($majoringsub['completed']);
                    }

                    global $globalrowspanmajoring;
                    $globalrowspanmajoring = $row_span_majoring;

                    $row_span_open_compulsary = count($compulsory['open']);
                    $row_span_open_elective = count($elective['open']);
                    $row_span_open_majoring = count($majoringsub['open']);

                    global $globalrow_span_open_compulsary;
                    global $globalrow_span_open_elective;
                    global $globalrow_span_open_majoring;
                    $globalrow_span_open_compulsary = $row_span_open_compulsary;
                    $globalrow_span_open_elective = $row_span_open_elective;
                    $globalrow_span_open_majoring = $row_span_open_majoring;
                } else {
                    global $globalresearch;
                    global $globalelective;
                    global $globalrowspanelective;
                    global $globalrowspan;
                    global $globalrow_span_open_compulsary;
                    global $globalrow_span_open_elective;
                }
                global $globalresearch;
                $globalresearch = $research;
                global $globalresearchrem;
                $globalresearchrem = $researchRem;

                //var_dump($globalcompulsary);
                //exit;

                //count complete subject
                $viewRemainingSubjectCore = true;
                $viewRemainingSubjectElective = true;
                $viewRemainingSubjectMajoring = true;
                $viewRemainingSubjectResearch = true;

                global $globalcountCreditHoursCore;
                $globalcountCreditHoursCore = $countCreditHoursCore + $countCreditHoursElective + $countCreditHoursMajoring + $researchCreditHours;

                $chr1 = 0;
                $chr2 = 0;
                $chr3 = 0;
                $chr4 = 0;
                $cch = 0;
                $remPaperCore = 0;
                $remPaperElective = 0;

                if ($this->view->landscapeType == 43) {
                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 394) {

                            $remPaperCore = ($programRequirementLoop['CreditHours'] / 3) - ($countCompleteCreditHoursCore / 3);

                            if ($remPaperCore < 0) {
                                $remPaperCore = 0;
                            }

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $countCreditHoursCore2 = $countCompleteCreditHoursCore;
                            } else {
                                $countCreditHoursCore2 = $countCompletePaperCore;
                            }

                            if ($countCreditHoursCore2 >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectCore = false;
                                //$chr1 = 0;
                            }/*else{
                                $chr1 = $programRequirementLoop['CreditHours']-$countCreditHoursCore;
                            }*/

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $chr1 = $programRequirementLoop['CreditHours'] - $countCompleteCreditHoursCore;
                            } else {
                                $chr1 = $programRequirementLoop['CreditHours'] - $countCompletePaperCore;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 275) {

                            $remPaperElective = ($programRequirementLoop['CreditHours'] / 3) - ($countCompletePaperElective);

                            if ($remPaperElective < 0) {
                                $remPaperElective = 0;
                            }

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $countCreditHoursElective2 = $countCompleteCreditHoursElective;
                            } else {
                                $countCreditHoursElective2 = $countCompletePaperElective;
                            }

                            if ($countCreditHoursElective2 >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectElective = false;
                                //$chr2 = 0;
                            }/*else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursElective;
                            }*/

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $chr2 = $programRequirementLoop['CreditHours'] - $countCompleteCreditHoursElective;
                            } else {
                                $chr2 = $programRequirementLoop['CreditHours'] - $countCompletePaperElective;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 1054) {

                            $remPaperMajoring = ($programRequirementLoop['CreditHours'] / 3) - ($countCompletePaperMajoring);

                            if ($remPaperMajoring < 0) {
                                $remPaperMajoring = 0;
                            }

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $countCreditHoursMajoring2 = $countCompleteCreditHoursMajoring;
                            } else {
                                $countCreditHoursMajoring2 = $countCompletePaperMajoring;
                            }

                            if ($countCreditHoursElective2 >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectMajoring = false;
                                //$chr2 = 0;
                            }/*else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursElective;
                            }*/

                            if ($programRequirementLoop['RequirementType'] == 1) {
                                $chr4 = $programRequirementLoop['CreditHours'] - $countCompleteCreditHoursMajoring;
                            } else {
                                $chr4 = $programRequirementLoop['CreditHours'] - $countCompletePaperMajoring;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop) {
                        if ($programRequirementLoop['SubjectType'] == 271) {
                            if ($countCreditHoursResearch >= $programRequirementLoop['CreditHours']) {
                                $viewRemainingSubjectResearch = false;
                                $chr3 = 0;
                            } else {
                                $chr3 = $programRequirementLoop['CreditHours'] - $countCreditHoursResearch;
                            }
                        }
                    }

                    if ($chr1 == null) {
                        $chr1 = 0;
                    }
                    if ($chr2 == null) {
                        $chr2 = 0;
                    }
                    if ($chr3 == null) {
                        $chr3 = 0;
                    }
                    if ($chr4 == null) {
                        $chr4 = 0;
                    }

                    global $globalchr;
                    global $globalcch;
                    $cch = $countCompleteCreditHoursCore + $countCompleteCreditHoursElective + $countCompleteCreditHoursMajoring + $countCreditHoursResearch;
                    $globalcch = $cch;
                    $globalchr = $chr1 + $chr2 + $chr3 + $chr4;

                } else if ($this->view->landscapeType == 42) {
                    if ($countCreditHoursCore >= $programRequirement[0]['CreditHours']) {
                        $viewRemainingSubjectCore = false;
                        $chr = 0;
                    } else {
                        $chr = $programRequirement[0]['CreditHours'] - $countCreditHoursCore;
                    }
                    global $globalchr;
                    $globalchr = $chr;

                    //count part summary
                    $completePart1 = 0;
                    $completePart2 = 0;
                    $completePart3 = 0;
                    $remainingPart1 = 0;
                    $remainingPart2 = 0;
                    $remainingPart3 = '';

                    if ($compulsory) {
                        foreach ($compulsory as $key => $compulsoryLoop) {
                            if ($key == 1) {
                                if (isset($compulsoryLoop['completed'])) {
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub) {

                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);

                                        $publishresult = false;

                                        if ($subsem) {
                                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                                            if ($publishInfo) {
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                                    $publishresult = true;
                                                }
                                            }
                                        }

                                        if (($compulsoryLoopSub["exam_status"] == "C" || $compulsoryLoopSub["exam_status"] == "EX" || $compulsoryLoopSub["exam_status"] == "CT" || $compulsoryLoopSub["exam_status"] == "U") && $compulsoryLoopSub['mark_approval_status'] == 2 && $publishresult == true) {
                                            if ($compulsoryLoopSub["exam_status"] != "U") {
                                                if ($compulsoryLoopSub["grade_name"] != "F") {
                                                    $completePart1++;
                                                }
                                            }
                                        } else {
                                            $remainingPart1++;
                                        }
                                    }
                                }

                                if (isset($compulsoryLoop['open'])) {
                                    foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                        $remainingPart1++;
                                    }
                                }
                            } else if ($key == 2) {
                                if (isset($compulsoryLoop['completed'])) {
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub) {

                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);

                                        $publishresult = false;

                                        if ($subsem) {
                                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                                            if ($publishInfo) {
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                                    $publishresult = true;
                                                }
                                            }
                                        }

                                        if (($compulsoryLoopSub["exam_status"] == "C" || $compulsoryLoopSub["exam_status"] == "EX" || $compulsoryLoopSub["exam_status"] == "CT" || $compulsoryLoopSub["exam_status"] == "U") && $compulsoryLoopSub['mark_approval_status'] == 2 && $publishresult == true) {
                                            if ($compulsoryLoopSub["exam_status"] != "U") {
                                                if ($compulsoryLoopSub["grade_name"] != "F") {
                                                    $completePart2++;
                                                }
                                            }
                                        } else {
                                            $remainingPart2++;
                                        }
                                    }
                                }

                                if (isset($compulsoryLoop['open'])) {
                                    foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                        $remainingPart2++;
                                    }
                                }
                            } else if ($key == 3) {
                                if (isset($compulsoryLoop['completed'])) {
                                    foreach ($compulsoryLoop['completed'] as $compulsoryLoopSub) {

                                        $subsem = $recordDb->getSemesterById($compulsoryLoopSub['IdSemesterMain']);

                                        $publishresult = false;

                                        if ($subsem) {
                                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                                            if ($publishInfo) {
                                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                                    $publishresult = true;
                                                }
                                            }
                                        }

                                        if (($compulsoryLoopSub["exam_status"] == "C" || $compulsoryLoopSub["exam_status"] == "EX" || $compulsoryLoopSub["exam_status"] == "CT" || $compulsoryLoopSub["exam_status"] == "U") && $compulsoryLoopSub['mark_approval_status'] == 2 && $publishresult == true) {
                                            if ($compulsoryLoopSub["exam_status"] != "U") {
                                                $completePart3++;
                                            }
                                        } else {
                                            $remainingPart3++;
                                        }
                                    }
                                }

                                if (isset($compulsoryLoop['open'])) {
                                    if (count($compulsoryLoop['open']) > 1) {
                                        $o = 0;
                                        foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                            $remainingPart3 .= $compulsoryLoopSub['SubCode'];
                                            $o++;

                                            if ($o < count($compulsoryLoop['open'])) {
                                                $remainingPart3 .= '/';
                                            }
                                        }
                                    } else {
                                        if ($completePart3 > 0) {
                                            $remainingPart3 = 0;
                                        } else {
                                            foreach ($compulsoryLoop['open'] as $compulsoryLoopSub) {
                                                $remainingPart3 = $compulsoryLoopSub['SubCode'] == 'PPP' ? 'Articleship' : 'PPP';
                                            }
                                        }
                                        unset($compulsory[$key]['open']);
                                    }
                                }
                            }
                        }
                    }

                    global $globalcompletePart1;
                    global $globalcompletePart2;
                    global $globalcompletePart3;
                    global $globalremainingPart1;
                    global $globalremainingPart2;
                    global $globalremainingPart3;

                    if ($viewRemainingSubjectCore == false) {
                        $remainingPart1 = 0;
                        $remainingPart2 = 0;
                    }

                    $globalcompletePart1 = $completePart1;
                    $globalcompletePart2 = $completePart2;
                    $globalcompletePart3 = $completePart3;
                    $globalremainingPart1 = $remainingPart1;
                    $globalremainingPart2 = $remainingPart2;
                    $globalremainingPart3 = $remainingPart3;
                }

                global $globalcompulsary;
                $globalcompulsary = $compulsory;
                global $globalmajoring;
                $globalmajoring = $majoringsub;

                //dd($globalcompulsary);
                //dd($globalmajoring); exit;

                global $globalremPaperCore;
                global $globalremPaperElective;
                global $globalcountCompletePaperCore;
                global $globalcountCompletePaperElective;
                global $globalcountCtCredit;
                global $globalcountExCredit;
                global $globalcountCompleteCreditHoursCore;
                global $globalcountCompleteCreditHoursElective;

                $globalcountCompleteCreditHoursCore = $countCompleteCreditHoursCore;
                $globalcountCompleteCreditHoursElective = $countCompleteCreditHoursElective;
                $globalcountCtCredit = $countCtCredit;
                $globalcountExCredit = $countExCredit;
                $globalremPaperCore = $remPaperCore;
                $globalremPaperElective = $remPaperElective;
                $globalcountCompletePaperCore = $countCompletePaperCore;
                $globalcountCompletePaperElective = $countCompletePaperElective;

                $Asol = 0;
                $Atol = 0;
                $Btam = 0;
                $Bsol = 0;
                $Btol = 0;
                $Ctam = 0;
                $Csol = 0;
                $Dsol = 0;
                $Fsol = 0;
                $NP = 0;
                $P = 0;
                $PD = 0;
                $CT = 0;
                $EX = 0;
                $U = 0;

                if ($courses) {
                    foreach ($courses as $coursesLoop) {
                        $equivid = false;
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $coursesLoop['IdSubject']);
                        if (!$course_status) {
                            $equivid = $recordDb->checkEquivStatus($landscapeId, $coursesLoop["IdSubject"]);

                            if ($equivid) {
                                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $equivid);
                            }
                        }

                        $subsem = $recordDb->getSemesterById($coursesLoop['IdSemesterMain']);

                        $publishresult = false;

                        if ($subsem) {
                            $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                            if ($publishInfo) {
                                $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                                $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                                if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                    $publishresult = true;
                                }
                            }
                        }

                        if ($coursesLoop['mark_approval_status'] == 2 && $publishresult == true && $course_status) {
                            if (trim($coursesLoop['grade_name'] == 'A')) {
                                $Asol++;
                            } else if (trim($coursesLoop['grade_name']) == 'A-') {
                                $Atol++;
                            } else if (trim($coursesLoop['grade_name']) == 'B+') {
                                $Btam++;
                            } else if (trim($coursesLoop['grade_name']) == 'B') {
                                $Bsol++;
                            } else if (trim($coursesLoop['grade_name']) == 'B-') {
                                $Btol++;
                            } else if (trim($coursesLoop['grade_name']) == 'C+') {
                                $Ctam++;
                            } else if (trim($coursesLoop['grade_name']) == 'C') {
                                $Csol++;
                            } else if (trim($coursesLoop['grade_name']) == 'D') {
                                $Dsol++;
                            } else if (trim($coursesLoop['grade_name']) == 'F') {
                                $Fsol++;
                            } else if (trim($coursesLoop['grade_name']) == 'NP') {
                                $NP++;
                            } else if (trim($coursesLoop['grade_name']) == 'P') {
                                $P++;
                            } else if (trim($coursesLoop['grade_name']) == 'PD') {
                                $PD++;
                            } else if (trim($coursesLoop['grade_name']) == 'CT') {
                                $CT++;
                            } else if (trim($coursesLoop['grade_name']) == 'EX') {
                                $EX++;
                            } else if (trim($coursesLoop['grade_name']) == 'U') {
                                $U++;
                            }
                        }
                    }
                }

                $CAP = [
                    'A'  => $Asol,
                    'A-' => $Atol,
                    'B+' => $Btam,
                    'B'  => $Bsol,
                    'B-' => $Btol,
                    'C+' => $Ctam,
                    'C'  => $Csol,
                    'D'  => $Dsol,
                    'F'  => $Fsol,
                    'NP' => $NP,
                    'P'  => $P,
                    'PD' => $PD,
                    'CT' => $CT,
                    'EX' => $EX,
                    'U'  => $U
                ];
                global $globalCAP;
                $globalCAP = $CAP;

                //calculate cgpa
                //$total = $this->calculateCGPA($courses, $studentdetails['IdProgram']);
                $semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
                $regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();
                $studentGradeDB = new Examination_Model_DbTable_StudentGrade();

                $registered_semester = $semesterStatusDB->getListSemesterRegistered($registrationId);

                $student_grade = false;
                $ttt = 0;
                foreach ($registered_semester as $index => $semester) {
                    $subsem = $recordDb->getSemesterById($semester['IdSemesterMain']);

                    $publishresult = false;

                    if ($subsem) {
                        $publishInfo = $recordDb->getPublishResult($programId, $subsem['IdSemesterMaster']);

                        if ($publishInfo) {
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'] . ' ' . $publishInfo['pm_time']);
                            $publishInfo['pm_cdate'] = strtotime(date('Y-m-d H:i:s'));

                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']) {
                                $publishresult = true;
                            }
                        }
                    }

                    //get subject registered in each semester
                    if ($semester["IsCountable"] == 1) {
                        $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($registrationId, $semester['IdSemesterMain']);
                    } else {
                        $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($registrationId, $semester['IdSemesterMain']);
                    }
                    //var_dump($subject_list);
                    $registered_semester[$index]['subjects'] = $subject_list;

                    $ttt++;

                    if ($publishresult == true) {
                        //get grade info
                        $student_grade = $studentGradeDB->getStudentGrade($registrationId, $semester['IdSemesterMain']);
                    }
                }

                if (!$student_grade) {
                    $student_grade['sg_cum_credithour'] = 0;
                    $student_grade['sg_cgpa'] = 0;
                }

                global $globalstudent_grade;
                $globalstudent_grade = $student_grade;

                //global $globaltotal;
                //$globaltotal = $total;
                global $globalviewRemainingSubjectCore;
                global $globalviewRemainingSubjectElective;
                global $globalviewRemainingSubjectMajoring;
                global $globalviewRemainingSubjectResearch;
                $globalviewRemainingSubjectCore = $viewRemainingSubjectCore;
                $globalviewRemainingSubjectElective = $viewRemainingSubjectElective;
                $globalviewRemainingSubjectMajoring = $viewRemainingSubjectMajoring;
                $globalviewRemainingSubjectResearch = $viewRemainingSubjectResearch;
            }

            /**
             ** END
             **/
            //get subjects offered
            $SubjectOffered = new GeneralSetup_Model_DbTable_Subjectsoffered();
            $subject_offered = $SubjectOffered->getMultiLandscapeCourseOffer(["$landscapeId"], "", $curr_semester['IdSemesterMaster']);

            $offered = [];

            $y = 0;
            foreach ($subject_offered as $sub => $courseOffered) {
                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($programId, $landscapeId, $courseOffered['IdSubject']);
                if ($this->view->landscapeType == 43) {
                    if ($course_status['DefinitionDesc'] == "Core") {
                        $offered['compulsory'][$y] = $courseOffered;
                    } elseif ($course_status['DefinitionDesc'] == "Elective") {
                        $offered['elective'][$y] = $courseOffered;
                    } elseif ($course_status['DefinitionDesc'] == "Majoring") {
                        $offered['majoring'][$y] = $courseOffered;
                    } elseif ($course_status['DefinitionDesc'] == "Research") {
                        $offered['research'][$y] = $courseOffered;
                    }
                    $y++;
                } else {//level landscape
                    $offered[$courseOffered['block']]['compulsory'][$y] = $courseOffered;
                    $y++;
                }
            }

            if ($landscape["LandscapeType"] == 43) {
                global $globaloffered_compulsory;
                global $globaloffered_elective;
                global $globaloffered_majoring;
                $globaloffered_compulsory = isset($offered['compulsory']) ? count($offered['compulsory']) : 0;
                $globaloffered_elective = isset($offered['elective']) ? count($offered['elective']) : 0;
                $globaloffered_majoring = isset($offered['majoring']) ? count($offered['majoring']) : 0;
            }
            global $globaloffered;
            $globaloffered = $offered;

        } elseif ($landscape["LandscapeType"] == 45) {

            //get registered blocks
            $studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
            $blocks = $studentSemesterDB->getRegisteredBlock($registrationId);
            $this->view->blocks = $blocks;

            $landscapeId = $studentdetails["IdLandscape"];
            $this->view->landscapeId = $landscapeId;
            $this->view->landscapeType = 44;
            $programId = $studentdetails["IdProgram"];
            $this->view->programId = $programId;


            //get landscape info
            $landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
            $landscape = $landscapeDB->getLandscapeDetails($landscapeId);
            $this->view->landscape = $landscape;

            //get program requirement info
            $progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
            $programRequirement = $progReqDB->getlandscapecoursetype($programId, $landscapeId);
            $this->view->programrequirement = $programRequirement;
        }

        $studentRegDB = new Examination_Model_DbTable_StudentRegistration();
        $student = $studentRegDB->getStudentInfo($registrationId);

        $city = ($student['appl_city'] != 99) ? $student['CityName'] : $student['appl_city_others'];
        $state = ($student['appl_state'] != 99) ? $student['StateName'] : $student['appl_state_others'];

        $address = (!empty($student['appl_address1'])) ? $student['appl_address1'] : '';
        $address .= (!empty($student['appl_address2'])) ? '<br>' . $student['appl_address2'] : '';
        $address .= (!empty($student['appl_address3'])) ? '<br>' . $student['appl_address3'] : '';
        $address .= (!empty($student['appl_postcode'])) ? '<br>' . $student['appl_postcode'] . ' ' . $city : '';
        $address .= (!empty($student['CountryName'])) ? '<br>' . $state . ' ' . $student['CountryName'] : '<br>' . $state;


        $fieldValues = [
            '$[STUDENT_NAME]' => strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"]),
            '$[STUDENT_ID]'   => strtoupper($student["registrationId"]),
            '$[ADDRESS]'      => strtoupper($address),
            '$[INTAKE]'       => strtoupper($student["IntakeDesc"]),
            '$[PROGRAMME]'    => strtoupper($student["ProgramName"]),
            '$[LOGO]'         => APPLICATION_URL . "/images/logo_text.png"
            //'$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
        ];


        require_once 'dompdf_config.inc.php';
        error_reporting(0);

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        //template path
        $html_template_path = DOCUMENT_PATH . "/template/printacademicprogress.html";

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

        //echo $html; exit;

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();

        //output filename
        $output_filename = $student["registrationId"] . "-academic-progress.pdf";

        //$dompdf = $dompdf->output();
        $dompdf->stream($output_filename);

        //to rename output file
        $output_file_path = DOCUMENT_PATH . $student['sp_repository'] . '/' . $output_filename;

        file_put_contents($output_file_path, $dompdf);

        $this->view->file_path = $output_file_path;
        exit;
    }

    static function defineSemesterStatus($spId, $status, $scheme)
    {

        $model = new Records_Model_DbTable_Report();

        $lastSem = $model->getLastSemesterStatus($spId);

        if ($lastSem) {
            $currentSemStatus = $lastSem['status'];
        } else {
            $currentSemStatus = 'Not Registered';
        }
        //echo $scheme; exit;
        /*switch ($status){
            case 92:
                if ($scheme != null){
                    $curSem = $model->getCurrentSem($scheme);
                }else{
                    $curSem = false;
                }

                if ($curSem){
                    $semStatus = $model->getSemesterStatus($spId,$curSem["IdSemesterMaster"]);

                    if ($semStatus && isset($semStatus['studentsemesterstatus']) && $semStatus['studentsemesterstatus']!=131){
                        $currentSemStatus = $semStatus['status'];
                    }else{
                        $lastSem = $model->getLastSemesterStatus($spId);

                        if ($lastSem){
                            if ($scheme==1){ //ps
                                $getsem = $model->countSemesterStatus($scheme, $lastSem['SemesterMainStartDate'], $curSem['SemesterMainStartDate']);

                                $semcount = count($getsem)-2;

                                if ($semcount >= 3){
                                    $currentSemStatus = 'Dormant';
                                }else{
                                    $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($spId, $curSem["IdSemesterMaster"]);

                                    if ($checkSemStatusBasedOnSubjectReg){
                                        $currentSemStatus = 'Registered';
                                    }else{
                                        $currentSemStatus = 'Not Registered';
                                    }
                                }
                            }else if ($scheme==11){ //gs
                                $getsem = $model->countSemesterStatus($scheme, $lastSem['SemesterMainStartDate'], $curSem['SemesterMainStartDate'], 172);

                                $semcount = count($getsem)-2;

                                if ($semcount >= 2){
                                    $currentSemStatus = 'Dormant';
                                }else{
                                    $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($spId, $curSem["IdSemesterMaster"]);

                                    if ($checkSemStatusBasedOnSubjectReg){
                                        $currentSemStatus = 'Registered';
                                    }else{
                                        $currentSemStatus = 'Not Registered';
                                    }
                                }
                            }else{ //open
                                $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($curSem["IdSemesterMaster"]);

                                if ($checkSemStatusBasedOnSubjectReg){
                                    $currentSemStatus = 'Registered';
                                }else{
                                    $currentSemStatus = 'Not Registered';
                                }
                            }
                        }else{
                            $checkSemStatusBasedOnSubjectReg = $model->checkSemStatusBasedOnSubjectReg($spId, $curSem["IdSemesterMaster"]);

                            if ($checkSemStatusBasedOnSubjectReg){
                                $currentSemStatus = 'Registered';
                            }else{
                                $currentSemStatus = 'Not Registered';
                            }
                        }
                    }
                }else{
                    $currentSemStatus = 'Not Registered';
                }

                break;
            case 94:
                if ($scheme != null){
                    $curSem = $model->getCurrentSem($scheme);
                }else{
                    $curSem = false;
                }

                if ($curSem){
                    $semStatus = $model->getSemesterStatus($spId,$curSem["IdSemesterMaster"]);
                    if ($semStatus){
                        $currentSemStatus = $semStatus['status'];
                    }else{
                        $currentSemStatus = 'Terminated';
                    }
                }else{
                    $currentSemStatus = 'Terminated';
                }
                break;
            case 96:
                $currentSemStatus = 'Graduated';
                break;
            case 248:
                $currentSemStatus = 'Completed';
                break;
            case 249:
                if ($scheme != null){
                    $curSem = $model->getCurrentSem($scheme);
                }else{
                    $curSem = false;
                }

                if ($curSem){
                    $semStatus = $model->getSemesterStatus($spId,$curSem["IdSemesterMaster"]);
                    if ($semStatus){
                        $currentSemStatus = $semStatus['status'];
                    }else{
                        $currentSemStatus = 'Quit';
                    }
                }else{
                    $currentSemStatus = 'Quit';
                }
                break;
            default:
                $currentSemStatus = 'Not Registered';
        }*/

        return $currentSemStatus;
    }

    static function defineSemesterStatusId($spId, $status, $scheme)
    {

        $model = new Records_Model_DbTable_Report();

        $lastSem = $model->getLastSemesterStatus($spId);

        if ($lastSem) {
            $currentSemStatus = $lastSem['studentsemesterstatus'];
        } else {
            $currentSemStatus = 131;
        }

        return $currentSemStatus;
    }

    public function loadEmailContentAction()
    {
        $id = $this->_getParam('id', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $vsmodel = new Records_Model_DbTable_Visitingstudent();
        $content = $vsmodel->getEmailContent($id);

        unset($content['cr_subject']);

        $json = Zend_Json::encode($content);

        echo $json;
        exit();
    }

    public function gridAction()
    {
        $db = getDb();

        $sql = $db->select()
            ->from(['p' => 'student_profile'])
            ->where('photo IS NOT NULL OR photo != ?', '');

        $results = $db->fetchAll($sql);

        $this->view->results = $results;
    }

    public function deleteEducationDetailsAction()
    {
        $id = $this->_getParam('id', 0);
        $txnid = $this->_getParam('txnid', 0);
        $this->lobjStudentprofile->deleteEducationDetails($id);
        //var_dump($id); exit;
        $this->_helper->flashMessenger->addMessage(['success' => "Data Has Been Deleted."]);
        $this->_redirect($this->baseUrl . '/records/studentprofile/student-profile/id/' . $txnid . '/sectionId/3');
    }

    public function updateSemesterStatusAction()
    {
        $id = $this->_getParam('id', 0);

        $model = new Records_Model_DbTable_SemesterStatus();
        $studentInfo = $model->getStudentInfo($id);
        $intakeInfo = $model->getIntake($studentInfo['IdIntake']);
        $semInfo = $model->getSemester($studentInfo['IdScheme'], $intakeInfo['sem_year'], $intakeInfo['sem_seq']);
        $currentSem = $model->getCurrentSemester($studentInfo['IdScheme']);
        $semList = $model->getSemesterList($studentInfo['IdScheme'], $semInfo['SemesterMainStartDate'], $currentSem['SemesterMainStartDate']);

        if ($semList) {
            $i = 0;
            foreach ($semList as $key2 => $semLoop) {
                $checkSemStatus = $model->checkSemesterStatus($studentInfo['IdStudentRegistration'], $semLoop['IdSemesterMaster']);
                $checkSemSubject = $model->checkSubjectRegister($studentInfo['IdStudentRegistration'], $semLoop['IdSemesterMaster']);

                $i++;

                if ($i == count($semList)) {
                    switch ($studentInfo['profileStatus']) {
                        case 92: //Active
                            if ($checkSemStatus) {
                                //if ($checkSemStatus['studentsemesterstatus']==130 || $checkSemStatus['studentsemesterstatus']==131){
                                if ($checkSemSubject) {
                                    $semStatus = 130;
                                } else {
                                    $semStatus = 131;
                                }

                                if ($semStatus == 130) {
                                    $data = [
                                        'IdBlock'               => null,
                                        'studentsemesterstatus' => $semStatus,
                                        'UpdDate'               => date('Y-m-d h:i:s'),
                                        'UpdUser'               => 1,
                                        'UpdRole'               => 'admin'
                                    ];
                                    $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                } else {
                                    //check dormant or not
                                    $getLastSemReg = $model->getLastSemesterRegister($studentInfo['IdStudentRegistration']);

                                    if ($getLastSemReg) {
                                        $date1 = $getLastSemReg['SemesterMainStartDate'];
                                        $substractvalue = 2;
                                    } else {
                                        $date1 = $semInfo['SemesterMainStartDate'];
                                        $substractvalue = 1;
                                    }

                                    if ($studentInfo['IdScheme'] == 1) { //ps
                                        $semType = 0;
                                        $consecutivesem = 3;
                                    } else if ($studentInfo['IdScheme'] == 11) { //gs
                                        $semType = 172;
                                        $consecutivesem = 2;
                                    } else { //open
                                        $semType = 172;
                                        $consecutivesem = 2;
                                    }

                                    $listUnregSem = $model->listUnregisteredSemester($studentInfo['IdScheme'], $date1, $semLoop['SemesterMainStartDate'], $semType);

                                    $semcount = count($listUnregSem) - $substractvalue;
                                    //var_dump($listUnregSem); exit;
                                    if ($semcount >= $consecutivesem) { //dormant
                                        $data = [
                                            'IdBlock'               => null,
                                            'studentsemesterstatus' => 844,
                                            'UpdDate'               => date('Y-m-d h:i:s'),
                                            'UpdUser'               => 1,
                                            'UpdRole'               => 'admin'
                                        ];
                                        $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                    } else { //unregistered
                                        $data = [
                                            'IdBlock'               => null,
                                            'studentsemesterstatus' => $semStatus,
                                            'UpdDate'               => date('Y-m-d h:i:s'),
                                            'UpdUser'               => 1,
                                            'UpdRole'               => 'admin'
                                        ];
                                        $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                    }
                                }
                                //}
                            } else {
                                if ($checkSemSubject) {
                                    $semStatus = 130;
                                } else {
                                    $semStatus = 131;
                                }

                                if ($semStatus == 130) {
                                    $data = [
                                        'IdStudentRegistration' => $studentInfo['IdStudentRegistration'],
                                        'idSemester'            => $semLoop['IdSemesterMaster'],
                                        'IdSemesterMain'        => $semLoop['IdSemesterMaster'],
                                        'IdBlock'               => null,
                                        'studentsemesterstatus' => $semStatus,
                                        'Level'                 => 1,
                                        'UpdDate'               => date('Y-m-d h:i:s'),
                                        'UpdUser'               => 1,
                                        'UpdRole'               => 'admin'
                                    ];
                                    $idsemstatus = $model->insertSemesterStatus($data);

                                    $data2 = [
                                        'idstudentsemsterstatus' => $idsemstatus,
                                        'IdStudentRegistration'  => $studentInfo['IdStudentRegistration'],
                                        'idSemester'             => $semLoop['IdSemesterMaster'],
                                        'IdSemesterMain'         => $semLoop['IdSemesterMaster'],
                                        'IdBlock'                => null,
                                        'studentsemesterstatus'  => $semStatus,
                                        'Level'                  => 1,
                                        'UpdDate'                => date('Y-m-d h:i:s'),
                                        'UpdUser'                => 1,
                                        'UpdRole'                => 'admin',
                                        'Type'                   => null,
                                        'message'                => 0,
                                        'createddt'              => date('Y-m-d h:i:s'),
                                        'createdby'              => 1
                                    ];
                                    $model->insertSemesterStatusHistory($data2);
                                } else {
                                    //check dormant or not
                                    $getLastSemReg = $model->getLastSemesterRegister($studentInfo['IdStudentRegistration']);

                                    if ($getLastSemReg) {
                                        $date1 = $getLastSemReg['SemesterMainStartDate'];
                                        $substractvalue = 2;
                                    } else {
                                        $date1 = $semInfo['SemesterMainStartDate'];
                                        $substractvalue = 1;
                                    }

                                    if ($studentInfo['IdScheme'] == 1) { //ps
                                        $semType = 0;
                                        $consecutivesem = 3;
                                    } else if ($studentInfo['IdScheme'] == 11) { //gs
                                        $semType = 172;
                                        $consecutivesem = 2;
                                    } else { //open
                                        $semType = 172;
                                        $consecutivesem = 2;
                                    }

                                    $listUnregSem = $model->listUnregisteredSemester($studentInfo['IdScheme'], $date1, $semLoop['SemesterMainStartDate'], $semType);

                                    $semcount = count($listUnregSem) - $substractvalue;

                                    if ($semcount >= $consecutivesem) { //dormant
                                        $data = [
                                            'IdStudentRegistration' => $studentInfo['IdStudentRegistration'],
                                            'idSemester'            => $semLoop['IdSemesterMaster'],
                                            'IdSemesterMain'        => $semLoop['IdSemesterMaster'],
                                            'IdBlock'               => null,
                                            'studentsemesterstatus' => 844,
                                            'Level'                 => 1,
                                            'UpdDate'               => date('Y-m-d h:i:s'),
                                            'UpdUser'               => 1,
                                            'UpdRole'               => 'admin'
                                        ];
                                        $idsemstatus = $model->insertSemesterStatus($data);

                                        $data2 = [
                                            'idstudentsemsterstatus' => $idsemstatus,
                                            'IdStudentRegistration'  => $studentInfo['IdStudentRegistration'],
                                            'idSemester'             => $semLoop['IdSemesterMaster'],
                                            'IdSemesterMain'         => $semLoop['IdSemesterMaster'],
                                            'IdBlock'                => null,
                                            'studentsemesterstatus'  => 844,
                                            'Level'                  => 1,
                                            'UpdDate'                => date('Y-m-d h:i:s'),
                                            'UpdUser'                => 1,
                                            'UpdRole'                => 'admin',
                                            'Type'                   => null,
                                            'message'                => 0,
                                            'createddt'              => date('Y-m-d h:i:s'),
                                            'createdby'              => 1
                                        ];
                                        $model->insertSemesterStatusHistory($data2);
                                    } else { //unregistered
                                        $data = [
                                            'IdStudentRegistration' => $studentInfo['IdStudentRegistration'],
                                            'idSemester'            => $semLoop['IdSemesterMaster'],
                                            'IdSemesterMain'        => $semLoop['IdSemesterMaster'],
                                            'IdBlock'               => null,
                                            'studentsemesterstatus' => $semStatus,
                                            'Level'                 => 1,
                                            'UpdDate'               => date('Y-m-d h:i:s'),
                                            'UpdUser'               => 1,
                                            'UpdRole'               => 'admin'
                                        ];
                                        $idsemstatus = $model->insertSemesterStatus($data);

                                        $data2 = [
                                            'idstudentsemsterstatus' => $idsemstatus,
                                            'IdStudentRegistration'  => $studentInfo['IdStudentRegistration'],
                                            'idSemester'             => $semLoop['IdSemesterMaster'],
                                            'IdSemesterMain'         => $semLoop['IdSemesterMaster'],
                                            'IdBlock'                => null,
                                            'studentsemesterstatus'  => $semStatus,
                                            'Level'                  => 1,
                                            'UpdDate'                => date('Y-m-d h:i:s'),
                                            'UpdUser'                => 1,
                                            'UpdRole'                => 'admin',
                                            'Type'                   => null,
                                            'message'                => 0,
                                            'createddt'              => date('Y-m-d h:i:s'),
                                            'createdby'              => 1
                                        ];
                                        $model->insertSemesterStatusHistory($data2);
                                    }
                                }
                            }
                            break;
                        /*case 94: //Terminated
                            if ($checkSemStatus){
                                $data = array(
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>132,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                            }else{
                                $data = array(
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>132,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $idsemstatus = $model->insertSemesterStatus($data);

                                $data2 = array(
                                    'idstudentsemsterstatus'=>$idsemstatus,
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>132,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin',
                                    'Type'=>null,
                                    'message'=>0,
                                    'createddt'=>date('Y-m-d h:i:s'),
                                    'createdby'=>1
                                );
                                $model->insertSemesterStatusHistory($data2);
                            }
                            break;
                        case 96: //Graduated
                            if ($checkSemStatus){
                                $data = array(
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>848,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                            }else{
                                $data = array(
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>848,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $idsemstatus = $model->insertSemesterStatus($data);

                                $data2 = array(
                                    'idstudentsemsterstatus'=>$idsemstatus,
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>848,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin',
                                    'Type'=>null,
                                    'message'=>0,
                                    'createddt'=>date('Y-m-d h:i:s'),
                                    'createdby'=>1
                                );
                                $model->insertSemesterStatusHistory($data2);
                            }
                            break;
                        case 248: //Completed
                            if ($checkSemStatus){
                                $data = array(
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>229,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                            }else{
                                $data = array(
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>229,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $idsemstatus = $model->insertSemesterStatus($data);

                                $data2 = array(
                                    'idstudentsemsterstatus'=>$idsemstatus,
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>229,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin',
                                    'Type'=>null,
                                    'message'=>0,
                                    'createddt'=>date('Y-m-d h:i:s'),
                                    'createdby'=>1
                                );
                                $model->insertSemesterStatusHistory($data2);
                            }
                            break;
                        case 249: //Quit
                            if ($checkSemStatus){
                                if ($checkSemStatus['studentsemesterstatus']==130 || $checkSemStatus['studentsemesterstatus']==131){
                                    $data = array(
                                        'IdBlock'=>null,
                                        'studentsemesterstatus'=>835,
                                        'UpdDate'=>date('Y-m-d h:i:s'),
                                        'UpdUser'=>1,
                                        'UpdRole'=>'admin'
                                    );
                                    $model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                                }
                            }else{
                                $data = array(
                                    'IdStudentRegistration'=>$studentInfo['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>835,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin'
                                );
                                $idsemstatus = $this->insertSemesterStatus($data);

                                $data2 = array(
                                    'idstudentsemsterstatus'=>$idsemstatus,
                                    'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                                    'idSemester'=>$semLoop['IdSemesterMaster'],
                                    'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                                    'IdBlock'=>null,
                                    'studentsemesterstatus'=>835,
                                    'Level'=>1,
                                    'UpdDate'=>date('Y-m-d h:i:s'),
                                    'UpdUser'=>1,
                                    'UpdRole'=>'admin',
                                    'Type'=>null,
                                    'message'=>0,
                                    'createddt'=>date('Y-m-d h:i:s'),
                                    'createdby'=>1
                                );
                                $this->insertSemesterStatusHistory($data2);
                            }
                            break;*/
                    }
                } else {
                    /*if (!$checkSemStatus){
                        if ($checkSemSubject){
                            $semStatus = 130;
                        }else{
                            $semStatus = 131;
                        }

                        $data = array(
                            'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                            'idSemester'=>$semLoop['IdSemesterMaster'],
                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                            'IdBlock'=>null,
                            'studentsemesterstatus'=>$semStatus,
                            'Level'=>1,
                            'UpdDate'=>date('Y-m-d h:i:s'),
                            'UpdUser'=>1,
                            'UpdRole'=>'admin'
                        );
                        $idsemstatus = $this->insertSemesterStatus($data);

                        $data2 = array(
                            'idstudentsemsterstatus'=>$idsemstatus,
                            'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                            'idSemester'=>$semLoop['IdSemesterMaster'],
                            'IdSemesterMain'=>$semLoop['IdSemesterMaster'],
                            'IdBlock'=>null,
                            'studentsemesterstatus'=>$semStatus,
                            'Level'=>1,
                            'UpdDate'=>date('Y-m-d h:i:s'),
                            'UpdUser'=>1,
                            'UpdRole'=>'admin',
                            'Type'=>null,
                            'message'=>0,
                            'createddt'=>date('Y-m-d h:i:s'),
                            'createdby'=>1
                        );
                        $this->insertSemesterStatusHistory($data2);
                    }else{
                        if ($checkSemSubject){
                            $semStatus = 130;
                        }else{
                            $semStatus = 131;
                        }

                        $data = array(
                            'IdBlock'=>null,
                            'studentsemesterstatus'=>$semStatus,
                            'UpdDate'=>date('Y-m-d h:i:s'),
                            'UpdUser'=>1,
                            'UpdRole'=>'admin'
                        );
                        $this->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                    }*/
                }
            }
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(['success' => 'Information Saved!']);
        $this->_redirect($this->baseUrl . '/records/studentprofile/studentprofilelist/id/' . $id . '#Tab4');
    }

    public function errorAction()
    {
        $this->view->title = $this->view->translate('Error Message');
    }
}