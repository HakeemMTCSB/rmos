<?php
class Records_AdvisementController extends Base_Base { //Controller for the User Module

    private $lobjStudentprofileForm;
	private $lobjStudentprofile;
	private $lobjUser;
	private $_gobjlog;
	
    public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();	
	}
	
    public function fnsetObj(){
		
		$this->lobjStudentprofileForm = new Records_Form_Studentprofile();
		$this->lobjStudentprofile = new Records_Model_DbTable_Studentprofile();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjLandscape = new Records_Model_DbTable_Studentprofile();
		$this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
		$this->studentprofileForm = new Registration_Form_Studentprofile();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
        $this->lsemesterModel  = new GeneralSetup_Model_DbTable_Semester();
        $this->defModel = new App_Model_General_DbTable_Definationms();
	}
	
    public function indexAction(){
		
		$this->view->title = $this->view->translate("Advisement - Student List");
		$auth = Zend_Auth::getInstance();
		$form = new Registration_Form_SearchStudent();
		$this->view->form = $form;

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
                        if (isset($formData['Search'])){
			$this->view->IdProgramScheme = $formData['IdProgramScheme'];
			
			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent($formData, NULL, 1);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(1000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			$this->view->paginator = $paginator;
                        }else{
                            $studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent(NULL, NULL, 1);
		    
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(1000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			$this->view->paginator = $paginator;
                        }
		}else{			 
		   
		    $studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent(NULL, NULL, 1);
		    
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(1000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			$this->view->paginator = $paginator;
		   
		}
    }
    
    public function detailAction() {
        $this->view->title = $this->view->translate("Advisement - What if analysis");
        
        $this->view->step = $step = $this->_getParam('step',1);
        $this->view->id = $id = $this->_getParam('id',0);
         
        $Studentprofile = new Records_Model_DbTable_Studentprofile();
        $studentInfo = $Studentprofile->getStudentInfo($id);
        $this->view->studentInfo = $studentInfo;
        $this->view->IdScheme = $studentInfo['IdScheme'];
        
        $advs = new Records_Model_DbTable_AdvisementVerTwo();
        
        if ($step == 1){
            $Advisement = new Records_Model_DbTable_Advisement();
            $Advisement->deleteAll($id);
            
            $Scheme  = new GeneralSetup_Model_DbTable_Programscheme();
            $schemes = $Scheme->getProgSchemeByProgram($studentInfo['IdProgram']);
           
            $program_scheme = array();
            
            $i = 0;
            foreach ($schemes as $key => $value){
                if(!isset($program_scheme['ProgramMode'][$value['ProgramModeId']]))
                    $program_scheme['ProgramMode'][$value['ProgramModeId']] = $value['ProgramMode'];
                                                                             
                if(!isset($program_scheme['ProgramMode'][$value['StudyModeId']]))
                    $program_scheme['StudyMode'][$value['StudyModeId']] = $value['StudyMode'];
                                                
                if(!isset($program_scheme['ProgramType'][$value['ProgramTypeId']]))
                    $program_scheme['ProgramType'][$value['ProgramTypeId']] =$value['ProgramType'];
            }
            
            $this->view->program_scheme = $program_scheme;
        }elseif($step == 2){
            if ($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                
                $ProgramScheme  = new GeneralSetup_Model_DbTable_Programscheme();
                $program_scheme = $ProgramScheme->fnGetProgramSchemeIdWithType($studentInfo['IdProgram'],$formData['studymode'],$formData['programmode'],$studentInfo['IdProgramType']);
                
                if (!$program_scheme){
                    //redirect here
                    $this->_helper->flashMessenger->addMessage(array('error' => 'No Programme Scheme'));
                    $this->_redirect($this->baseUrl . '/records/advisement/detail/id/'.$id);
                }
                
                $scheme = $ProgramScheme->getSchemeById($program_scheme['IdProgramScheme']);
                $this->view->scheme = $scheme;
                
                $select_duration  = (int)$program_scheme['Duration']+1;
                $select_duration2  = (int)$program_scheme['OptimalDuration']+1;
                $current_duration = (int)$studentInfo['Duration'];
                
                $CurrentSemester  = new GeneralSetup_Model_DbTable_Semestermaster();
                $intakeInfo = $CurrentSemester->getIntakeInfo($studentInfo['IdIntake']);
                $current_semester = $CurrentSemester->getSemesterBasedOnIntake($intakeInfo['sem_year'], $intakeInfo['sem_seq']);
                //$current_semester = $CurrentSemester->fnGetSemestermaster($studentInfo['IdSemesterMain']);
                $today = new DateTime();
                $RegistrationDate = new DateTime($current_semester['SemesterMainStartDate']);
               
                $diff = $RegistrationDate->diff($today);
                $current_month = $diff->m;
                
                //defer semester
                $deferSemList = $advs->getDeferSemester($id);
                
                $deferMonth = 0;
                
                if ($deferSemList){
                    foreach ($deferSemList as $deferSemLoop){
                        $deferDate1 = new DateTime($deferSemLoop['SemesterMainStartDate']);
                        $deferDate2 = new DateTime($deferSemLoop['SemesterMainEndDate']);
                        
                        $deferDiff = $deferDate1->diff($deferDate2);
                        
                        $deferMonth = $deferDiff->m + $deferMonth;
                    }
                }
                
                //GET BALANCE MONTH LEFT
                $balance_month = $select_duration - $current_month + $deferMonth;
                $balance_month2 = $select_duration2 - $current_month + $deferMonth;
               
                $finish_month = date('F Y',strtotime("+$balance_month month"));
                $finish_month2 = date('F Y',strtotime("+$balance_month2 month"));
                $this->view->finish_month = $finish_month;
                $this->view->finish_month2 = $finish_month2;
                
                //get landscape
                $landscapeInfo = $advs->getLandscape($studentInfo['IdProgram'], $program_scheme['IdProgramScheme'], $studentInfo['IdIntake']);
                $semesterTotal = isset($landscapeInfo['SemsterCount']) ? $landscapeInfo['SemsterCount']:0;
                
                $courseList = $advs->getCourse($studentInfo['IdStudentRegistration']);
                
                $takenCourse = array();
                $subject_ids = array();
                $a = 0;
                $q = 0;
                
                if ($courseList){
                    $recordDb = new Records_Model_DbTable_Academicprogress();
                    
                    foreach ($courseList as $courseLoop){
                        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $courseLoop['IdSubject']);
                        
                        $equivid = false;
                        if(!$course_status){
                            $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $courseLoop["IdSubject"]);

                            if($equivid){
                                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                            }
                        }
                        
                        if ($landscapeInfo['LandscapeType'] == 43){
                            if($course_status['DefinitionDesc'] == "Core"){
                                if($courseLoop["exam_status"]=="C" ||$courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                    if ($studentInfo['IdScheme'] == 1){
                                        $takenCourse['compulsory'][$a] = $courseLoop;
                                        $takenCourse['compulsory'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse['compulsory'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        $a++;
                                    }else{
                                        $takenCourse[$courseLoop['IdSemesterMain']]['compulsory'][$a] = $courseLoop;
                                        $takenCourse[$courseLoop['IdSemesterMain']]['compulsory'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse[$courseLoop['IdSemesterMain']]['compulsory'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        $a++;
                                    }
                                }
                            }else if($course_status['DefinitionDesc'] == "Elective"){
                                if($courseLoop["exam_status"]=="C" ||$courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                    if ($studentInfo['IdScheme'] == 1){
                                        $takenCourse['elective'][$a] = $courseLoop;
                                        $takenCourse['elective'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse['elective'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        $a++;
                                    }else{
                                        $takenCourse[$courseLoop['IdSemesterMain']]['elective'][$a] = $courseLoop;
                                        $takenCourse[$courseLoop['IdSemesterMain']]['elective'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse[$courseLoop['IdSemesterMain']]['elective'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        $a++;
                                    }
                                }
                            }else if($course_status['DefinitionDesc'] == "Research"){
                                if ($studentInfo['IdScheme'] != 1){
                                    if ($courseLoop["grade_name"]!="" && $courseLoop["grade_name"]!=null){
                                        $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a] = $courseLoop;
                                        $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['grademark'] = $courseLoop['grade_name'];
                                        $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['CreditHours'] = $courseLoop['credit_hour_registered'];
                                        
                                        if ($courseLoop["grade_name"]=="IP"){
                                            $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['remarks'] = 'In Progress';
                                        }else{
                                            $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['remarks'] = '';
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F" || $courseLoop["grade_name"]!="IP"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }

                                        $a++;
                                    }
                                }
                            }
                        }else if($landscapeInfo['LandscapeType'] == 42){
                            if($courseLoop["exam_status"]=="C" ||$courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                $takenCourse[$course_status['block']]['compulsory'][$a] = $courseLoop;
                                $takenCourse[$course_status['block']]['compulsory'][$a]['grademark'] = $courseLoop['grade_name'];

                                if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                    $takenCourse[$course_status['block']]['compulsory'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                }

                                if($courseLoop["grade_name"]!="F"){
                                    $subject_ids[$q] = $courseLoop['IdSubject'];
                                    $q++;
                                }

                                $a++;
                            }
                        }
                    }
                }
                
                //kosongkan array untuk scheme GS
                if ($studentInfo['IdScheme']==11){
                    $subject_ids = array(0);
                }

                $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                $remaining_courses = $landscapeSubjectDb->getRemainingCourses($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $subject_ids);
                
                $compulsory['open'] = array();
                $elective['open'] = array();
                $research['open'] = array();
                $p = 0;
                
                foreach($remaining_courses as $b => $remaining){
                    $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                    $recordDb = new Records_Model_DbTable_Academicprogress();
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'],$studentInfo["IdLandscape"],$remaining['IdSubject']);

                    $equivid = false;
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $remaining["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                        }
                    }
                    
                    if($landscapeInfo['LandscapeType'] == 43){
                        if($course_status['DefinitionDesc'] == 'Core'){
                            $compulsory['open'][$p] = $remaining;
                            $compulsory['open'][$p]['grademark'] = '-';
                        }else if($course_status['DefinitionDesc'] == 'Elective'){
                            $elective['open'][$p] = $remaining;
                            $elective['open'][$p]['grademark'] = '-';
                        }else if($course_status['DefinitionDesc'] == 'Research'){
                            $research['open'][$p] = $remaining;
                            $research['open'][$p]['grademark'] = '-';
                        }
                    }else{
                        $compulsory['open'][$course_status['block']][$p] = $remaining;
                        $compulsory['open'][$course_status['block']][$p]['grademark'] = '-';
                    }
                    $p++;
                }
                
                $row_span_open_compulsary = count($compulsory['open']);
                $row_span_open_elective   = count($elective['open']);
                $row_span_open_research   = count($research['open']);
                
                $totalTaken = count($takenCourse);
                $this->view->semTaken = $totalTaken;
                $this->view->allSemester = $semesterTotal;

                $this->view->takenCourse = $takenCourse;
                $this->view->rowspan_open_compulsary = $row_span_open_compulsary;
                $this->view->rowspan_open_elective   = $row_span_open_elective;
                $this->view->rowspan_open_research   = $row_span_open_research;
                $this->view->compulsory = $compulsory;
                $this->view->elective = $elective;
                $this->view->research = $research;
                $this->view->landscape_id = $landscapeInfo["IdLandscape"];
                $this->view->landscapeInfo = $landscapeInfo;
            }else{
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot skip'));
                $this->_redirect($this->baseUrl . '/records/advisement/detail/id/'.$id);
            }
        }elseif($step == 3){
            $this->view->studentInfo = $studentInfo;
            if ($this->getRequest()->isPost()) {
                
                $formData = $this->getRequest()->getPost();
                $Advisement = new Records_Model_DbTable_Advisement();
                
                foreach ($formData as $key => $data){
                    if(($key != 'sub') && ($key != 'landscape_id')){   
                        $arr = explode('_',$key);
                        
                        if ($arr[0]=='subject'){
                            $advisement_id = $arr[1];
                            $update = array('grade' => $data);
                            
                            $Advisement->updateData($update,$advisement_id);
                        }else if ($arr[0]=='repeat'){
                            $advisement_id = $arr[1];
                            $update2 = array('IdSubjectRepeat' => $data);
                            
                            $Advisement->updateData($update2,$advisement_id);
                        }else if ($arr[0]=='type'){
                            $advisement_id = $arr[1];
                            $update3 = array('type' => $data);
                            
                            $Advisement->updateData($update3,$advisement_id);
                        }else if ($arr[0]=='credithours'){
                            $advisement_id = $arr[1];
                            $update3 = array('credit_hours' => $data);
                            
                            $Advisement->updateData($update3,$advisement_id);
                        }
                    }
                }
                
                //ladscape
                $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
                $landscapeInfo = $landscapeDb->getLandscapeDetails($formData['landscape_id']);
                $this->view->wLandscape = $landscapeInfo;
                
                //get program requirement info
                $progReqDB = new GeneralSetup_Model_DbTable_Programrequirement();
                $programRequirement = $progReqDB->getlandscapecoursetype($studentInfo['IdProgram'],$formData['landscape_id']);
                $this->view->programrequirement = $programRequirement;
                
                $ProgramScheme  = new GeneralSetup_Model_DbTable_Programscheme();
                $scheme = $ProgramScheme->getSchemeById($landscapeInfo['IdProgramScheme']);
                $this->view->scheme = $scheme;
                
                $select_duration = (int)$scheme['Duration']+1;
                $select_duration2  = (int)$scheme['OptimalDuration']+1;
                $current_duration = (int)$studentInfo['Duration'];
                
                $CurrentSemester  = new GeneralSetup_Model_DbTable_Semestermaster();
                $intakeInfo = $CurrentSemester->getIntakeInfo($studentInfo['IdIntake']);
                $current_semester = $CurrentSemester->getSemesterBasedOnIntake($intakeInfo['sem_year'], $intakeInfo['sem_seq']);
                //$current_semester = $CurrentSemester->fnGetSemestermaster($studentInfo['IdSemesterMain']);
                
                //ramalan graduasi
                $today = new DateTime();
                $RegistrationDate = new DateTime($current_semester['SemesterMainStartDate']);
               
                $diff = $RegistrationDate->diff($today);
                $current_month = $diff->m;
                
                //defer semester
                $deferSemList = $advs->getDeferSemester($id);
                
                $deferMonth = 0;
                
                if ($deferSemList){
                    foreach ($deferSemList as $deferSemLoop){
                        $deferDate1 = new DateTime($deferSemLoop['SemesterMainStartDate']);
                        $deferDate2 = new DateTime($deferSemLoop['SemesterMainEndDate']);
                        
                        $deferDiff = $deferDate1->diff($deferDate2);
                        
                        $deferMonth = $deferDiff->m + $deferMonth;
                    }
                }
                
                //GET BALANCE MONTH LEFT
                $balance_month = $select_duration - $current_month + $deferMonth;
                $balance_month2 = $select_duration2 - $current_month + $deferMonth;
               
                $finish_month = date('F Y',strtotime("+$balance_month month"));
                $finish_month2 = date('F Y',strtotime("+$balance_month2 month"));
                $this->view->finish_month = $finish_month;
                $this->view->finish_month2 = $finish_month2;   
                
                $courseList = $advs->getCourse($studentInfo['IdStudentRegistration']);
                
                $takenCourse = array();
                $subject_ids = array();
                $subject_ids_remarks = array();
                $countCreditHoursCore = 0;
                $countCreditHoursElective = 0;
                $countCreditHoursResearch = 0;
                $countPaperTakenCore = 0;
                $countPaperTakenElective = 0;
                $countPaperCompleteCore = 0;
                $countPaperCompleteElective = 0;
                $countCtCredit = 0;
                $countExCredit = 0;
                $a = 0;
                $q = 0;
                
                if ($courseList){
                    $recordDb = new Records_Model_DbTable_Academicprogress();
                    
                    foreach ($courseList as $courseLoop){
                        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $courseLoop['IdSubject']);
                        
                        $equivid = false;
                        if(!$course_status){
                            $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $courseLoop["IdSubject"]);

                            if($equivid){
                                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                            }
                        }
                        
                        if ($landscapeInfo['LandscapeType'] == 43){
                            if($course_status['DefinitionDesc'] == "Core"){
                                if($courseLoop["exam_status"]=="C" ||$courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                    if ($studentInfo['IdScheme'] == 1){
                                        $takenCourse['compulsory'][$a] = $courseLoop;
                                        $takenCourse['compulsory'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse['compulsory'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        //count credit hours
                                        if ($courseLoop["exam_status"]!="U"){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCreditHoursCore = $countCreditHoursCore+$ch;
                                            
                                            //count paper taken
                                            $countPaperTakenCore++;
                                            
                                            //count paper complete
                                            if($courseLoop["grade_name"]!="F"){
                                                $countPaperCompleteCore++;
                                            }
                                        }
                                        
                                        if ($courseLoop['exam_status']=='CT'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCtCredit = $ch+$countCtCredit;
                                        }else if ($courseLoop['exam_status']=='EX'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countExCredit = $ch+$countExCredit;
                                        }
                                        
                                        $a++;
                                    }else{
                                        $takenCourse[$courseLoop['IdSemesterMain']]['compulsory'][$a] = $courseLoop;
                                        $takenCourse[$courseLoop['IdSemesterMain']]['compulsory'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse[$courseLoop['IdSemesterMain']]['compulsory'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        //count credit hours
                                        if ($courseLoop["exam_status"]!="U"){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCreditHoursCore = $countCreditHoursCore+$ch;
                                        }
                                        
                                        if ($courseLoop['exam_status']=='CT'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCtCredit = $ch+$countCtCredit;
                                        }else if ($courseLoop['exam_status']=='EX'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countExCredit = $ch+$countExCredit;
                                        }
                                        
                                        $a++;
                                    }
                                    array_push($subject_ids_remarks, $courseLoop['IdSubject']);
                                }
                            }else if($course_status['DefinitionDesc'] == "Elective"){
                                if($courseLoop["exam_status"]=="C" ||$courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                    if ($studentInfo['IdScheme'] == 1){
                                        $takenCourse['elective'][$a] = $courseLoop;
                                        $takenCourse['elective'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse['elective'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        //count credit hours
                                        if ($courseLoop["exam_status"]!="U"){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCreditHoursElective = $countCreditHoursElective+$ch;
                                            
                                            //count paper taken
                                            $countPaperTakenElective++;
                                            
                                            //count paper complete
                                            if($courseLoop["grade_name"]!="F"){
                                                $countPaperCompleteElective++;
                                            }
                                        }
                                        
                                        if ($courseLoop['exam_status']=='CT'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCtCredit = $ch+$countCtCredit;
                                        }else if ($courseLoop['exam_status']=='EX'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countExCredit = $ch+$countExCredit;
                                        }
                                        
                                        $a++;
                                    }else{
                                        $takenCourse[$courseLoop['IdSemesterMain']]['elective'][$a] = $courseLoop;
                                        $takenCourse[$courseLoop['IdSemesterMain']]['elective'][$a]['grademark'] = $courseLoop['grade_name'];
                                        
                                        if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                            $takenCourse[$courseLoop['IdSemesterMain']]['elective'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        //count credit hours
                                        if ($courseLoop["exam_status"]!="U"){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCreditHoursElective = $countCreditHoursElective+$ch;
                                        }
                                        
                                        if ($courseLoop['exam_status']=='CT'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCtCredit = $ch+$countCtCredit;
                                        }else if ($courseLoop['exam_status']=='EX'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countExCredit = $ch+$countExCredit;
                                        }
                                        
                                        $a++;
                                    }
                                    array_push($subject_ids_remarks, $courseLoop['IdSubject']);
                                }
                            }else if($course_status['DefinitionDesc'] == "Research"){
                                if ($studentInfo['IdScheme'] != 1){
                                    if ($courseLoop["grade_name"]!="" && $courseLoop["grade_name"]!=null){
                                        $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a] = $courseLoop;
                                        $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['grademark'] = $courseLoop['grade_name'];
                                        $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['CreditHours'] = $courseLoop['credit_hour_registered'];
                                        
                                        if ($courseLoop["grade_name"]=="IP"){
                                            $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['remarks'] = 'In Progress';
                                        }else{
                                            $takenCourse[$courseLoop['IdSemesterMain']]['research'][$a]['remarks'] = '';
                                        }
                                        
                                        if($courseLoop["grade_name"]!="F" || $courseLoop["grade_name"]!="IP"){
                                            $subject_ids[$q] = $courseLoop['IdSubject'];
                                            $q++;
                                        }
                                        
                                        //count credit hours
                                        if ($courseLoop["exam_status"]!="U"){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCreditHoursResearch = $countCreditHoursResearch+$ch;
                                        }
                                        
                                        if ($courseLoop['exam_status']=='CT'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countCtCredit = $ch+$countCtCredit;
                                        }else if ($courseLoop['exam_status']=='EX'){
                                            $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                            $countExCredit = $ch+$countExCredit;
                                        }

                                        $a++;
                                    }
                                }
                            }
                        }else if($landscapeInfo['LandscapeType'] == 42){
                            if($courseLoop["exam_status"]=="C" ||$courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                $takenCourse[$course_status['block']]['compulsory'][$a] = $courseLoop;
                                $takenCourse[$course_status['block']]['compulsory'][$a]['grademark'] = $courseLoop['grade_name'];

                                if($courseLoop["exam_status"]=="EX" || $courseLoop["exam_status"]=="CT" || $courseLoop["exam_status"]=="U"){
                                    $takenCourse[$course_status['block']]['compulsory'][$a]['grademark'] = $landscapeInfo['AssessmentMethod']!='point' ? $courseLoop["exam_status"]:$courseLoop['grade_name'];
                                }

                                if($courseLoop["grade_name"]!="F"){
                                    $subject_ids[$q] = $courseLoop['IdSubject'];
                                    $q++;
                                }
                                
                                //count credit hours
                                if ($courseLoop["exam_status"]!="U"){
                                    $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                    $countCreditHoursCore = $countCreditHoursCore+$ch;
                                    
                                    //count paper taken
                                    $countPaperTakenCore++;

                                    //count paper complete
                                    if($courseLoop["grade_name"]!="F"){
                                        $countPaperCompleteCore++;
                                    }
                                }
                                
                                if ($courseLoop['exam_status']=='CT'){
                                    $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                    $countCtCredit = $ch+$countCtCredit;
                                }else if ($courseLoop['exam_status']=='EX'){
                                    $ch = $courseLoop['credit_hour_registered']!=null && $courseLoop['credit_hour_registered']!='' ? $courseLoop['credit_hour_registered']:$courseLoop['CreditHours'];
                                    $countExCredit = $ch+$countExCredit;
                                }

                                $a++;
                                
                                array_push($subject_ids_remarks, $courseLoop['IdSubject']);
                            }
                        }
                    }
                }
                
                //calculate complete course gpa
                if ($landscapeInfo['LandscapeType'] == 43 && $studentInfo['IdScheme'] != 1){
                    if ($takenCourse){
                        foreach ($takenCourse as $key => $loop){
                            $gpaComplete = $this->calculateGPA1($loop, $studentInfo['IdProgram']);
                            $takenCourse[$key]['gpa'] = number_format($gpaComplete, 2, '.', '');
                        }
                    }
                }
                
                //taken course yang baru ditambah pada step 2
                $current_courses = $Advisement->getList($id);
                
                $takenCurrentCourse = array();
                $b = 0;
                
                if ($current_courses){
                    foreach ($current_courses as $current_courseLoop){
                        $recordDb = new Records_Model_DbTable_Academicprogress();
                        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $current_courseLoop['IdSubject']);
                        
                        $equivid = false;
                        if(!$course_status){
                            $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $current_courseLoop["IdSubject"]);

                            if($equivid){
                                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                            }
                        }
                        
                        if ($landscapeInfo['LandscapeType'] == 43){
                            if($course_status['DefinitionDesc'] == "Core"){
                                $takenCurrentCourse[$current_courseLoop['semester']]['compulsory'][$b] = $current_courseLoop;
                                $takenCurrentCourse[$current_courseLoop['semester']]['compulsory'][$b]['grademark'] = $current_courseLoop['grade'];

                                if($current_courseLoop["grade"]!="F"){
                                    $subject_ids[$q] = $current_courseLoop['IdSubject'];
                                    $q++;
                                }
                                
                                //count credit hours
                                if ($current_courseLoop["type"]!=2){
                                    $ch = $current_courseLoop['CreditHours'];
                                    $countCreditHoursCore = $countCreditHoursCore+$ch;
                                    
                                    //count paper taken
                                    $countPaperTakenCore++;

                                    //count paper complete
                                    if($current_courseLoop["grade"]!="F"){
                                        $countPaperCompleteCore++;
                                    }
                                }
                                
                                if ($current_courseLoop['type']==4){
                                    $countCtCredit = $current_courseLoop['CreditHours']+$countCtCredit;
                                }else if ($current_courseLoop['type']==3){
                                    $countExCredit = $current_courseLoop['CreditHours']+$countExCredit;
                                }

                                $b++;
                            }else if ($course_status['DefinitionDesc'] == "Elective"){
                                $takenCurrentCourse[$current_courseLoop['semester']]['elective'][$b] = $current_courseLoop;
                                $takenCurrentCourse[$current_courseLoop['semester']]['elective'][$b]['grademark'] = $current_courseLoop['grade'];

                                if($current_courseLoop["grade"]!="F"){
                                    $subject_ids[$q] = $current_courseLoop['IdSubject'];
                                    $q++;
                                }
                                
                                //count credit hours
                                if ($current_courseLoop["type"]!=2){
                                    $ch = $current_courseLoop['CreditHours'];
                                    $countCreditHoursElective = $countCreditHoursElective+$ch;
                                    
                                    //count paper taken
                                    $countPaperTakenElective++;

                                    //count paper complete
                                    if($current_courseLoop["grade"]!="F"){
                                        $countPaperCompleteElective++;
                                    }
                                }
                                
                                if ($current_courseLoop['type']==4){
                                    $countCtCredit = $current_courseLoop['CreditHours']+$countCtCredit;
                                }else if ($current_courseLoop['type']==3){
                                    $countExCredit = $current_courseLoop['CreditHours']+$countExCredit;
                                }

                                $b++;
                            }else if ($course_status['DefinitionDesc'] == "Research"){
                                if ($studentInfo['IdScheme'] != 1){
                                    $takenCurrentCourse[$current_courseLoop['semester']]['research'][$b] = $current_courseLoop;
                                    $takenCurrentCourse[$current_courseLoop['semester']]['research'][$b]['grademark'] = $current_courseLoop['grade'];

                                    if ($current_courseLoop["grade"]=="IP"){
                                        $takenCurrentCourse[$current_courseLoop['semester']]['research'][$b]['remarks'] = 'In Progress';
                                    }else{
                                        $takenCurrentCourse[$current_courseLoop['semester']]['research'][$b]['remarks'] = '';
                                    }

                                    if($current_courseLoop["grade"]!="F" || $current_courseLoop["grade"]!="IP"){
                                        $subject_ids[$q] = $current_courseLoop['IdSubject'];
                                        $q++;
                                    }
                                    
                                    //count credit hours
                                    if ($current_courseLoop["type"]!=2){
                                        $ch = $current_courseLoop['credit_hours'];
                                        $countCreditHoursResearch = $countCreditHoursResearch+$ch;
                                    }
                                    
                                    if ($current_courseLoop['type']==4){
                                        $countCtCredit = $current_courseLoop['credit_hours']+$countCtCredit;
                                    }else if ($current_courseLoop['type']==3){
                                        $countExCredit = $current_courseLoop['credit_hours']+$countExCredit;
                                    }

                                    $b++;
                                }
                            }
                        }else if ($landscapeInfo['LandscapeType'] == 42){
                            $takenCurrentCourse[$current_courseLoop['semester']][$course_status['block']]['compulsory'][$b] = $current_courseLoop;
                            $takenCurrentCourse[$current_courseLoop['semester']][$course_status['block']]['compulsory'][$b]['grademark'] = $current_courseLoop['grade'];

                            if($current_courseLoop["grade"]!="F"){
                                $subject_ids[$q] = $current_courseLoop['IdSubject'];
                                $q++;
                            }
                            
                            //count credit hours
                            if ($current_courseLoop["type"]!=2){
                                $ch = $current_courseLoop['CreditHours'];
                                $countCreditHoursCore = $countCreditHoursCore+$ch;
                                
                                //count paper taken
                                $countPaperTakenCore++;

                                //count paper complete
                                if($current_courseLoop["grade"]!="F"){
                                    $countPaperCompleteCore++;
                                }
                            }
                            
                            if ($current_courseLoop['type']==4){
                                $countCtCredit = $current_courseLoop['CreditHours']+$countCtCredit;
                            }else if ($current_courseLoop['type']==3){
                                $countExCredit = $current_courseLoop['CreditHours']+$countExCredit;
                            }

                            $b++;
                        }
                    }
                }
                
                //calculate current course gpa
                if ($landscapeInfo['LandscapeType'] == 43 && $studentInfo['IdScheme'] != 1){
                    if ($takenCurrentCourse){
                        foreach ($takenCurrentCourse as $key => $takenCurrentCourseLoop){
                            $gpaCurrent = $this->calculateGPA2($takenCurrentCourseLoop, $studentInfo['IdProgram']);
                            $takenCurrentCourse[$key]['gpa'] = number_format($gpaCurrent, 2, '.', '');
                        }
                    }
                }
                
                //calculate cgpa
                if ($landscapeInfo['LandscapeType'] == 43 && $studentInfo['IdScheme'] != 1){
                    $cgpa = $this->calculateCGPA($takenCourse, $takenCurrentCourse, $studentInfo['IdProgram']);
                    $this->view->total = $cgpa;
                }
                
                //remaining course 
                $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                $remaining_courses = $landscapeSubjectDb->getRemainingCourses($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $subject_ids);
                
                $compulsory['open'] = array();
                $elective['open'] = array();
                $research['open'] = array();
                $p = 0;
                
                foreach($remaining_courses as $b => $remaining){
                    $recordDb = new Records_Model_DbTable_Academicprogress();
                    $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'],$studentInfo["IdLandscape"],$remaining['IdSubject']);

                    $equivid = false;
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $remaining["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                        }
                    }
                    
                    if($landscapeInfo['LandscapeType'] == 43){
                        if($course_status['DefinitionDesc'] == 'Core'){
                            $compulsory['open'][$p] = $remaining;
                            $compulsory['open'][$p]['grademark'] = '-';
                        }else if($course_status['DefinitionDesc'] == 'Elective'){
                            $elective['open'][$p] = $remaining;
                            $elective['open'][$p]['grademark'] = '-';
                        }else if($course_status['DefinitionDesc'] == 'Research'){
                            $research['open'][$p] = $remaining;
                            $research['open'][$p]['grademark'] = '-';
                        }
                    }else{
                        $compulsory['open'][$remaining['block']][$p] = $remaining;
                        $compulsory['open'][$remaining['block']][$p]['grademark'] = '-';
                    }
                    $p++;
                }
                
                //total credit
                $this->view->countCreditHours = $countCreditHoursCore+$countCreditHoursElective+$countCreditHoursResearch;
                $this->view->countCreditHoursCore = $countCreditHoursCore;
                $this->view->countCreditHoursElective = $countCreditHoursElective;
                $this->view->countPaperTaken = $countPaperTakenCore+$countPaperTakenElective;
                $this->view->countPaperCompleteCore = $countPaperCompleteCore;
                $this->view->countPaperCompleteElective = $countPaperCompleteElective;
                $this->view->countCtCredit = $countCtCredit;
                $this->view->countExCredit = $countExCredit;
                
                //count remaining credit
                $viewRemainingSubjectCore = true;
                $viewRemainingSubjectElective = true;
                $viewRemainingSubjectResearch = true;
                $chr1 = 0;
                $chr2 = 0;

                if($landscapeInfo['LandscapeType']==43){
                    foreach ($programRequirement as $programRequirementLoop){
                        if ($programRequirementLoop['SubjectType'] == 394){
                            if ($countCreditHoursCore >= $programRequirementLoop['CreditHours']){
                                $viewRemainingSubjectCore = false;
                                $chr1 = 0;
                            }else{
                                $chr1 = $programRequirementLoop['CreditHours']-$countCreditHoursCore;
                            }
                        }
                    }

                    foreach ($programRequirement as $programRequirementLoop){
                        if ($programRequirementLoop['SubjectType'] == 275){
                            if ($countCreditHoursElective >= $programRequirementLoop['CreditHours']){
                                $viewRemainingSubjectElective = false;
                                $chr2 = 0;
                            }else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursElective;
                            }
                        }
                    }
                    
                    foreach ($programRequirement as $programRequirementLoop){
                        if ($programRequirementLoop['SubjectType'] == 271){
                            if ($countCreditHoursResearch >= $programRequirementLoop['CreditHours']){
                                $viewRemainingSubjectResearch = false;
                                $chr2 = 0;
                            }else{
                                $chr2 = $programRequirementLoop['CreditHours']-$countCreditHoursResearch;
                            }
                        }
                    }

                    if ($chr1 == null){
                        $chr1 = 0;
                    }
                    if ($chr2 == null){
                        $chr2 = 0;
                    }

                    $this->view->chr1 = $chr1;
                    $this->view->chr2 = $chr2;
                    $this->view->chr = $chr1+$chr2;

                }else if($landscapeInfo['LandscapeType']==42){
                    $chr = 0;
                    if ($countPaperCompleteCore >= $programRequirement[0]['CreditHours']){
                        $viewRemainingSubjectCore = false;
                        $chr = 0;
                    }else{
                        $chr = $programRequirement[0]['CreditHours']-$countPaperCompleteCore;
                    }
                    $this->view->chr = $chr;
                    
                    //count complete subject by part
                    $takenPart1 = 0;
                    $takenPart2 = 0;
                    $takenPart3 = 0;
                    $completePart1 = 0;
                    $completePart2 = 0;
                    $completePart3 = 0;
                    
                    if ($takenCourse){
                        foreach ($takenCourse as $key => $loop){
                            if (isset($loop['compulsory']) && count($loop['compulsory']) > 0){
                                foreach ($loop['compulsory'] as $loop2){
                                    if ($key == 1){
                                        if ($loop2['exam_status']!='U'){
                                            $takenPart1++;
                                            
                                            if ($loop2['grademark']!='F'){
                                                $completePart1++;
                                            }
                                        }
                                    }else if ($key == 2){
                                        if ($loop2['exam_status']!='U'){
                                            $takenPart2++;
                                            
                                            if ($loop2['grademark']!='F'){
                                                $completePart2++;
                                            }
                                        }
                                    }else if ($key == 3){
                                        if ($loop2['exam_status']!='U'){
                                            $takenPart3++;
                                            
                                            if ($loop2['grademark']!='F'){
                                                $completePart3++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if ($takenCurrentCourse){
                        foreach ($takenCurrentCourse as $loop3){
                            foreach ($loop3 as $key => $loop){
                                if (isset($loop['compulsory']) && count($loop['compulsory']) > 0){
                                    foreach ($loop['compulsory'] as $loop2){
                                        if ($key == 1){
                                            if ($loop2['type']!=2){
                                                $takenPart1++;

                                                if ($loop2['grademark']!='F'){
                                                    $completePart1++;
                                                }
                                            }
                                        }else if ($key == 2){
                                            if ($loop2['type']!=2){
                                                $takenPart2++;

                                                if ($loop2['grademark']!='F'){
                                                    $completePart2++;
                                                }
                                            }
                                        }else if ($key == 3){
                                            if ($loop2['type']!=2){
                                                $takenPart3++;

                                                if ($loop2['grademark']!='F'){
                                                    $completePart3++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    $remainingPart1 = 0;
                    $remainingPart2 = 0;
                    $remainingPart3 = '';
                    
                    if (isset($compulsory['open']) && count($compulsory['open']) > 0){
                        foreach ($compulsory['open'] as $key => $loop){
                            if ($key == 1){
                                foreach ($loop as $loop2){
                                    $remainingPart1++;
                                }
                            }else if($key == 2){
                                foreach ($loop as $loop2){
                                    $remainingPart2++;
                                }
                            }else if($key == 3){
                                $i = 0;
                                if (count($loop) > 1){
                                    foreach ($loop as $loop2){
                                        $remainingPart3 .= $loop2['SubCode'];
                                        $i++;

                                        if ($i < count($loop)){
                                            $remainingPart3 .= '/';
                                        }
                                    }
                                }else{
                                    $remainingPart3 .= 0;
                                }
                            }
                        }
                    }
                    
                    $this->view->takenPart1 = $takenPart1;
                    $this->view->takenPart2 = $takenPart2;
                    $this->view->takenPart3 = $takenPart3;
                    $this->view->completePart1 = $completePart1;
                    $this->view->completePart2 = $completePart2;
                    $this->view->completePart3 = $completePart3;
                    $this->view->remainingPart1 = $remainingPart1;
                    $this->view->remainingPart2 = $remainingPart2;
                    $this->view->remainingPart3 = $remainingPart3;
                }
                
                $Asol = 0;
                $Atol = 0;
                $Btam = 0;
                $Bsol = 0;
                $Btol = 0;
                $Ctam = 0;
                $Csol = 0;
                $Dsol = 0;
                $Fsol = 0;
                $NP = 0;
                $P = 0;
                $PD = 0;
                $CT = 0;
                $EX = 0;
                $U = 0;

                if ($courseList){
                    foreach ($courseList as $coursesLoop){
                        if ($coursesLoop['grade_name']=='A'){
                            $Asol++;
                        }else if($coursesLoop['grade_name']=='A-'){
                            $Atol++;
                        }else if($coursesLoop['grade_name']=='B+'){
                            $Btam++;
                        }else if($coursesLoop['grade_name']=='B'){
                            $Bsol++;
                        }else if($coursesLoop['grade_name']=='B-'){
                            $Btol++;
                        }else if($coursesLoop['grade_name']=='C+'){
                            $Ctam++;
                        }else if($coursesLoop['grade_name']=='C'){
                            $Csol++;
                        }else if($coursesLoop['grade_name']=='D'){
                            $Dsol++;
                        }else if($coursesLoop['grade_name']=='F'){
                            $Fsol++;
                        }else if($coursesLoop['grade_name']=='NP'){
                            $NP++;
                        }else if($coursesLoop['grade_name']=='P'){
                            $P++;
                        }else if($coursesLoop['grade_name']=='PD'){
                            $PD++;
                        }else if($coursesLoop['grade_name']=='CT'){
                            $CT++;
                        }else if($coursesLoop['grade_name']=='EX'){
                            $EX++;
                        }else if($coursesLoop['grade_name']=='U'){
                            $U++;
                        }
                    }
                }
                
                if ($current_courses){
                    foreach ($current_courses as $coursesLoop2){
                        if ($coursesLoop2['grade']=='A'){
                            $Asol++;
                        }else if($coursesLoop2['grade']=='A-'){
                            $Atol++;
                        }else if($coursesLoop2['grade']=='B+'){
                            $Btam++;
                        }else if($coursesLoop2['grade']=='B'){
                            $Bsol++;
                        }else if($coursesLoop2['grade']=='B-'){
                            $Btol++;
                        }else if($coursesLoop2['grade']=='C+'){
                            $Ctam++;
                        }else if($coursesLoop2['grade']=='C'){
                            $Csol++;
                        }else if($coursesLoop2['grade']=='D'){
                            $Dsol++;
                        }else if($coursesLoop2['grade']=='F'){
                            $Fsol++;
                        }else if($coursesLoop2['grade']=='NP'){
                            $NP++;
                        }else if($coursesLoop2['grade']=='P'){
                            $P++;
                        }else if($coursesLoop2['grade']=='PD'){
                            $PD++;
                        }else if($coursesLoop2['grade']=='CT'){
                            $CT++;
                        }else if($coursesLoop2['grade']=='EX'){
                            $EX++;
                        }else if($coursesLoop2['grade']=='U'){
                            $U++;
                        }
                    }
                }

                $CAP = array(
                    'A' => $Asol,
                    'A-' => $Atol,
                    'B+' => $Btam,
                    'B' => $Bsol,
                    'B-' => $Btol,
                    'C+' => $Ctam,
                    'C' => $Csol,
                    'D' => $Dsol,
                    'F' => $Fsol,
                    'NP' => $NP,
                    'P' => $P,
                    'PD' => $PD,
                    'CT' => $CT,
                    'EX' => $EX,
                    'U' => $U
                );
                $this->view->CAP = $CAP;
                
                //calculate cgpa
                $this->view->viewRemainingSubjectCore = $viewRemainingSubjectCore;
                $this->view->viewRemainingSubjectElective = $viewRemainingSubjectElective;
                $this->view->viewRemainingSubjectResearch = $viewRemainingSubjectResearch;
                
                //define remarks current course
                if ($landscapeInfo['LandscapeType'] == 43){
                    if ($takenCurrentCourse){
                        foreach ($takenCurrentCourse as $key => $takenCurrentCourseLoop){
                            
                            $repSubject = $subject_ids_remarks;
                            
                            foreach ($takenCurrentCourse as $key2 => $takenCurrentCourseLoop2){
                                if ($key == $key2){
                                    break;
                                }
                                
                                if (isset($takenCurrentCourseLoop2['compulsory']) && count($takenCurrentCourseLoop2['compulsory']) > 0){
                                    foreach ($takenCurrentCourseLoop2['compulsory'] as $key3 => $takenCurrentCourseLoop3){
                                        array_push($repSubject, $takenCurrentCourseLoop3['IdSubject']);
                                    }
                                }
                                
                                if (isset($takenCurrentCourseLoop2['elective']) && count($takenCurrentCourseLoop2['elective']) > 0){
                                    foreach ($takenCurrentCourseLoop2['elective'] as $key4 => $takenCurrentCourseLoop4){
                                        array_push($repSubject, $takenCurrentCourseLoop4['IdSubject']);
                                    }
                                }
                            }
                            
                            if (isset($takenCurrentCourseLoop['compulsory']) && count($takenCurrentCourseLoop['compulsory']) > 0){
                                foreach ($takenCurrentCourseLoop['compulsory'] as $key5 => $takenCurrentCourseLoop5){
                                    if (in_array($takenCurrentCourseLoop5['IdSubject'], $repSubject) && $takenCurrentCourseLoop5['type']!=2 && $takenCurrentCourseLoop5['type']!=3 && $takenCurrentCourseLoop5['type']!=4){
                                        $remarks = 'Replace/Retake';
                                    }else{
                                        $remarks = '';
                                    }

                                    if (isset($takenCurrentCourseLoop5['type']) && $takenCurrentCourseLoop5['type']==5){
                                        $subrep = $Advisement->getRepeatSub($takenCurrentCourseLoop5['IdSubjectRepeat']);
                                        $remarks = 'Replace/Retake with '.$subrep['SubCode'];
                                    }else if (isset($takenCurrentCourseLoop5['type']) && $takenCurrentCourseLoop5['type']==4){
                                        if ($takenCurrentCourseLoop5['grade']!='CT'){
                                            $remarks = 'Credit Transfer';
                                        }
                                    }
                                    $takenCurrentCourse[$key]['compulsory'][$key5]['remarks'] = $remarks;
                                }
                            }

                            if (isset($takenCurrentCourseLoop['elective']) && count($takenCurrentCourseLoop['elective']) > 0){
                                foreach ($takenCurrentCourseLoop['elective'] as $key6 => $takenCurrentCourseLoop6){
                                    if (in_array($takenCurrentCourseLoop6['IdSubject'], $repSubject) && $takenCurrentCourseLoop6['type']!=2 && $takenCurrentCourseLoop6['type']!=3 && $takenCurrentCourseLoop6['type']!=4){
                                        $remarks = 'Replace/Retake';
                                    }else{
                                        $remarks = '';
                                    }

                                    if (isset($takenCurrentCourseLoop6['type']) && $takenCurrentCourseLoop6['type']==5){
                                        $subrep = $Advisement->getRepeatSub($takenCurrentCourseLoop6['IdSubjectRepeat']);
                                        $remarks = 'Replace/Retake with '.$subrep['SubCode'];
                                    }else if (isset($takenCurrentCourseLoop6['type']) && $takenCurrentCourseLoop6['type']==4){
                                        if ($takenCurrentCourseLoop6['grade']!='CT'){
                                            $remarks = 'Credit Transfer';
                                        }
                                    }
                                    $takenCurrentCourse[$key]['elective'][$key6]['remarks'] = $remarks;
                                }
                            }
                        }
                    }
                }else if ($landscapeInfo['LandscapeType'] == 42){
                    if ($takenCurrentCourse){
                        foreach ($takenCurrentCourse as $key => $takenCurrentCourseLoop){
                            $repSubject = $subject_ids_remarks;
                            
                            foreach ($takenCurrentCourse as $key2 => $takenCurrentCourseLoop2){
                                if ($key == $key2){
                                    break;
                                }
                                
                                foreach ($takenCurrentCourseLoop2 as $key3 => $takenCurrentCourseLoop3){
                                    if (isset($takenCurrentCourseLoop3['compulsory']) && count($takenCurrentCourseLoop3['compulsory']) > 0){
                                        foreach ($takenCurrentCourseLoop3['compulsory'] as $key4 => $takenCurrentCourseLoop4){
                                            array_push($repSubject, $takenCurrentCourseLoop4['IdSubject']);
                                        }
                                    }
                                }
                            }
                            
                            foreach ($takenCurrentCourseLoop2 as $key5 => $takenCurrentCourseLoop5){
                                if (isset($takenCurrentCourseLoop5['compulsory']) && count($takenCurrentCourseLoop5['compulsory']) > 0){
                                    foreach ($takenCurrentCourseLoop5['compulsory'] as $key6 => $takenCurrentCourseLoop6){
                                        if (in_array($takenCurrentCourseLoop6['IdSubject'], $repSubject) && $takenCurrentCourseLoop6['type']!=2 && $takenCurrentCourseLoop6['type']!=3 && $takenCurrentCourseLoop6['type']!=4){
                                            $remarks = 'Replace/Retake';
                                        }else{
                                            $remarks = '';
                                        }

                                        if (isset($takenCurrentCourseLoop6['type']) && $takenCurrentCourseLoop6['type']==5){
                                            $subrep = $Advisement->getRepeatSub($takenCurrentCourseLoop6['IdSubjectRepeat']);
                                            $remarks = 'Replace/Retake with '.$subrep['SubCode'];
                                        }else if (isset($takenCurrentCourseLoop6['type']) && $takenCurrentCourseLoop6['type']==4){
                                            if ($takenCurrentCourseLoop6['grade']!='CT'){
                                                $remarks = 'Credit Transfer';
                                            }
                                        }
                                        $takenCurrentCourse[$key][$key5]['compulsory'][$key6]['remarks'] = $remarks;
                                    }
                                }
                            }
                        }
                    }
                }
                
                $row_span_open_compulsary = count($compulsory['open']);
                $row_span_open_elective   = count($elective['open']);
                $row_span_open_research   = count($research['open']);
                
                $totalTaken = count($takenCourse);
                $this->view->semTaken = $totalTaken;

                $this->view->takenCourse = $takenCourse;
                $this->view->takenCurrentCourse = $takenCurrentCourse;
                $this->view->rowspan_open_compulsary = $row_span_open_compulsary;
                $this->view->rowspan_open_elective   = $row_span_open_elective;
                $this->view->rowspan_open_research   = $row_span_open_research;
                $this->view->compulsory = $compulsory;
                $this->view->elective = $elective;
                $this->view->research = $research;
                $this->view->landscape_id = $landscapeInfo["IdLandscape"];
                $this->view->landscapeInfo = $landscapeInfo;
            }else{
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot skip'));
                $this->_redirect($this->baseUrl . '/records/advisement/detail/id/'.$id);
            }
        }
    }
    
    public function addCourseAction() {
        $student_id = $this->_getParam('id',0);
        
        $message = array();
        
        if($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
            
           $Advisement = new Records_Model_DbTable_Advisement();
            
            foreach ($formData['IdSubject'] as $key => $data){
                
                $check = $Advisement->checkCourseDuplicate($student_id, $formData['semesterLevel'], $data);
                
                if (!$check){
                    $insertSubject = array(
                        'semester' => $formData['semesterLevel'],
                        'registration_id' => $student_id,
                        'IdSubject' => $data
                    );
                    $Advisement->insertData($insertSubject);
                }else{
                    array_push($message, $check['SubCode'].' '.$check['SubjectName']);
                }
            }
        }
        
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        if (count($message) > 0){
            $json = Zend_Json::encode($message);
        }else{
            $json = Zend_Json::encode(0);
        }
		
	echo $json;
        
        exit;
    }
    
    public function listCoursesAction() {
    
        if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
        
        $student_id = $this->_getParam('id',0);
        $landscape_id = $this->_getParam('landscape_id',0);
        $this->view->id = $student_id;
        
        $Studentprofile = new Records_Model_DbTable_Studentprofile();
        $studentInfo    = $Studentprofile->getStudentInfo($student_id);
        $this->view->programid = $studentInfo['IdProgram'];
        $this->view->IdScheme = $studentInfo['IdScheme'];
        
        $courseRegisterDb = new Registration_Model_DbTable_Studentregistration();
        $courses = $courseRegisterDb->getCourseRegisteredByAllSemester($student_id,'Asc');
        
        $repeatSubArr = array();
        $i=0;
        if ($courses){
            foreach ($courses as $key => $coursesLoop){
                $recordDb = new Records_Model_DbTable_Academicprogress();
                $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'],$studentInfo["IdLandscape"],$coursesLoop['IdSubject']);
                
                $equivid = false;
                if(!$course_status){
                    $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $coursesLoop["IdSubject"]);

                    if($equivid){
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                    }
                }
                
                if ($course_status['DefinitionDesc'] == 'Elective'){
                    $repeatSubArr[$i]['IdSubject']=$coursesLoop['IdSubject'];
                    $repeatSubArr[$i]['SubjectName']=$coursesLoop['SubjectName'];
                    $repeatSubArr[$i]['SubCode']=$coursesLoop['SubCode'];
                    $i++;
                }
            }
        }

        /**
         **  GET SELECTION LANDSCAPE
         **/
        $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
        $wLandscape = $landscapeDb->getData($landscape_id);
        $this->view->landscape = $wLandscape;
        $lanscapeDetail = new GeneralSetup_Model_DbTable_LandscapeDetail();
        $wLanscapeDetail = $lanscapeDetail->getDatabyLandscapeId($landscape_id);
        foreach ($wLanscapeDetail as $key => $detail) {
        
            if($detail['SemesterType'] == 'Long Semester') {
                $minCourse = $detail['MinRegCourse'];
                $maxCourse = $detail['MaxRegCourse'];
                
                $type = $detail['Type'];
                $this->view->type = $type;
            }
        }
        
        $this->lobjGradesetup = new Examination_Model_DbTable_Gradesetup();         
        $lobjGradeValue   = $this->lobjGradesetup->fnGetGrade();
        $this->view->grade_list = $lobjGradeValue;
                        
        
        $Advisement      = new Records_Model_DbTable_Advisement();
        $current_courses = $Advisement->getList($student_id);
        $course_count    = $Advisement->countCourseSemester($student_id);
        $semester = '';
        $save = 'yes';
        
        if ($current_courses){
            foreach ($current_courses as $key => $current_coursesLoop){
                $recordDb = new Records_Model_DbTable_Academicprogress();
                $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'],$studentInfo["IdLandscape"],$current_coursesLoop['IdSubject']);
                
                $equivid = false;
                if(!$course_status){
                    $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $current_coursesLoop["IdSubject"]);

                    if($equivid){
                        $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                    }
                }
                
                if ($course_status['DefinitionDesc'] == 'Elective'){
                    $repeatSubArr[$i]['IdSubject']=$current_coursesLoop['IdSubject'];
                    $repeatSubArr[$i]['SubjectName']=$current_coursesLoop['SubjectName'];
                    $repeatSubArr[$i]['SubCode']=$current_coursesLoop['SubCode'];
                    $i++;
                }
                
                $current_courses[$key]['subtype']=$course_status['DefinitionDesc'];
            }
        }
        
        
        foreach ($course_count as $index => $data){
            $subListSem = $Advisement->getListBySem($student_id, $data['semester']);
            
            $total = 0;
            if ($subListSem){
                foreach ($subListSem as $current_coursesLoop){
                    $recordDb = new Records_Model_DbTable_Academicprogress();
                    $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'],$studentInfo["IdLandscape"],$current_coursesLoop['IdSubject']);
                    
                    $equivid = false;
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentInfo["IdLandscape"], $current_coursesLoop["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentInfo['IdProgram'], $studentInfo["IdLandscape"], $equivid);
                        }
                    }
                    
                    if ($type == 1){
                        $total++;
                    }else{
                        if ($course_status['DefinitionDesc'] == 'Research'){
                            
                            if ($current_coursesLoop['credit_hours'] != 0){
                                $ch = $current_coursesLoop['credit_hours'];
                            }else{
                                $ch = 3;
                            }
                            
                            $total=$total+$ch;
                        }else{
                            $total=$total+$current_coursesLoop['CreditHours'];
                        }
                    }
                }
            }
            
            if(($total < $minCourse) || ($total > $maxCourse)){
                $semester.= $data['semester'].', ';
                $save = 'no';
            }
        }
        
        $this->view->repeatSub = $repeatSubArr;
       
        $this->view->current_courses = $current_courses; 
        $this->view->id = $student_id; 
        
        $this->view->landscape_id = $landscape_id; 
        
        $this->view->semester = rtrim($semester,', '); 
        $this->view->save = $save; 
        $this->view->min = $minCourse;
        $this->view->max = $maxCourse;
        
    }
    
    public function deleteCourseAction() {
        
        if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
        
        $advisement_id = $this->_getParam('advisement_id',0);
        
        if($advisement_id != 0) {
            
            $Advisement = new Records_Model_DbTable_Advisement();
            $Advisement->deleteData($advisement_id);
        
        }
        exit;
    }
    
    public function updateCourseAction() {
        
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
        $advisement_id = $this->_getParam('id',0);
        $credithours = $this->_getParam('credithours',0);
        
        if($advisement_id != 0) {
            
            $Advisement = new Records_Model_DbTable_Advisement();
            
            $data = array(
                'credit_hours'=>$credithours
            );
            $Advisement->updateData($data, $advisement_id);
        
        }
        exit;
    }
    
    static function getRemark($data, $landscape){
        //var_dump($data);
        $model = new Records_Model_DbTable_Academicprogressremarks();
        
        $remarks = '';
        
        if ($data['replace_subject'] != null){
            $subjectRepeat = $model->getSubjectReplace($data['replace_subject']);
            $subject = $model->getSubjectById($subjectRepeat['IdSubject']);
            
            $remarks = 'Replace/Retake with '.$subject['SubCode'];
        }else if (($data['Active'] == 4 || $data['Active'] == 9) && $data['exam_status']!='U' && $data['exam_status']!='EX' && $data['exam_status']!='CT'){
            $remarks = 'Replace/Retake';
        }else if ($data['exam_status']=='CT'){
            $ch = $data['credit_hour_registered'] != null ? $data['credit_hour_registered']:$data['CreditHours'];
            if ($data['IdSubject'] == 111){
                $remarks = 'Credit hours taken is '.$ch;
            }else if ($landscape['AssessmentMethod']=='point'){
                $remarks = 'Credit transfer';
            }
        }
        return $remarks;
    }
    
    public function calculateGPA1($arrCourse, $programId){
        $model = new Records_Model_DbTable_Advisement();
        
        $total_point = 0;
        $total_credit = 0;
        
        if ($arrCourse){
            foreach ($arrCourse as $arrCourseLoop){
                foreach ($arrCourseLoop as $arrCourseLoop2){
                    if ($arrCourseLoop2["exam_status"]!="EX" && $arrCourseLoop2["exam_status"]!="CT" && $arrCourseLoop2["exam_status"]!="U" && $arrCourseLoop2["exam_status"]!="IP" && $arrCourseLoop2["exam_status"]!="" && $arrCourseLoop2["exam_status"]!=null){
                
                        $grade = $model->getGrade($programId, $arrCourseLoop2['IdSubject'], $arrCourseLoop2['grade_name']);

                        $current_point = $grade['GradePoint'] * $arrCourseLoop2['CreditHours'];

                        $total_point = $total_point + $current_point;
                        $total_credit = $total_credit + $arrCourseLoop2['CreditHours'];
                    }
                }
            }
        }
        
        if ($total_credit == 0){
            return $gpa = 0;
        }else{
            return $gpa = $total_point / $total_credit;
        }
    }
    
    public function calculateGPA2($arrCourse, $programId){
        $model = new Records_Model_DbTable_Advisement();
        
        $total_point = 0;
        $total_credit = 0;
        
        if ($arrCourse){
            foreach ($arrCourse as $arrCourseLoop){
                foreach ($arrCourseLoop as $arrCourseLoop2){
                    if ($arrCourseLoop2["grade"]!="EX" && $arrCourseLoop2["grade"]!="CT" && $arrCourseLoop2["grade"]!="U" && $arrCourseLoop2["grade"]!="IP" && $arrCourseLoop2["grade"]!="" && $arrCourseLoop2["grade"]!=null && $arrCourseLoop2["type"]!=2 && $arrCourseLoop2["type"]!=3 && $arrCourseLoop2["type"]!=4){
                        $grade = $model->getGrade($programId, $arrCourseLoop2['IdSubject'], $arrCourseLoop2['grade']);

                        $current_point = $grade['GradePoint'] * $arrCourseLoop2['CreditHours'];

                        $total_point = $total_point + $current_point;
                        $total_credit = $total_credit + $arrCourseLoop2['CreditHours'];
                    }
                }
            }
        }
        
        if ($total_credit == 0){
            return $gpa = 0;
        }else{
            return $gpa = $total_point / $total_credit;
        }
    }
    
    public function calculateCGPA($arrCourseComplete, $arrCourseCurrent, $programId){
        $model = new Records_Model_DbTable_Advisement();
        
        $total_point = 0;
        $total_credit = 0;
        
        if ($arrCourseComplete){
            foreach ($arrCourseComplete as $arrCourseLoop){
                foreach ($arrCourseLoop as $key => $arrCourseLoop2){
                    if ($key != 'gpa'){
                        foreach ($arrCourseLoop2 as $arrCourseLoop3){
                            if ($arrCourseLoop3["exam_status"]!="EX" && $arrCourseLoop3["exam_status"]!="CT" && $arrCourseLoop3["exam_status"]!="U" && $arrCourseLoop3["exam_status"]!="IP" && $arrCourseLoop3["exam_status"]!="" && $arrCourseLoop3["exam_status"]!=null){

                                $grade = $model->getGrade($programId, $arrCourseLoop3['IdSubject'], $arrCourseLoop3['grade_name']);

                                $current_point = $grade['GradePoint'] * $arrCourseLoop3['CreditHours'];

                                $total_point = $total_point + $current_point;
                                $total_credit = $total_credit + $arrCourseLoop3['CreditHours'];
                            }
                        }
                    }
                }
            }
        }
        
        if ($arrCourseCurrent){
            foreach ($arrCourseCurrent as $arrCourseLoop){
                foreach ($arrCourseLoop as $key => $arrCourseLoop2){
                    if ($key != 'gpa'){
                        foreach ($arrCourseLoop2 as $arrCourseLoop3){
                            if ($arrCourseLoop3["grade"]!="EX" && $arrCourseLoop3["grade"]!="CT" && $arrCourseLoop3["grade"]!="U" && $arrCourseLoop3["grade"]!="IP" && $arrCourseLoop3["grade"]!="" && $arrCourseLoop3["grade"]!=null && $arrCourseLoop3["type"]!=2 && $arrCourseLoop3["type"]!=3 && $arrCourseLoop3["type"]!=4){
                                $grade = $model->getGrade($programId, $arrCourseLoop3['IdSubject'], $arrCourseLoop3['grade']);

                                $current_point = $grade['GradePoint'] * $arrCourseLoop3['CreditHours'];

                                $total_point = $total_point + $current_point;
                                $total_credit = $total_credit + $arrCourseLoop3['CreditHours'];
                            }
                        }
                    }
                }
            }
        }
        
        if ($total_credit == 0){
            $cgpa = 0;
        }else{
            $cgpa = $total_point / $total_credit;
        }
        
        $total = array('cgpa' => number_format($cgpa, 2, '.', ''), 'credit_earn' => $total_credit);
        return $total;
    }
    
    public function viewTranscriptAction(){
		
        $this->view->title = "Transcript";

        $IdStudentRegistration = $this->_getParam('id',null);   	
        $this->view->id = $IdStudentRegistration;
		      
        $studentRegDB = new Examination_Model_DbTable_StudentRegistration();
        $semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
        $regSubjectDB = new Examination_Model_DbTable_StudentRegistrationSubject();   
        $studentGradeDB = new Examination_Model_DbTable_StudentGrade();	
        $cms = new Cms_ExamCalculation();	
		
	//To get Student Academic Info  
        $student = $studentRegDB->getStudentInfo($IdStudentRegistration);

        $this->view->student = $student;
        
        $scheme = '';
        if($student['IdScheme']==1){
            $scheme = '( '.$student['mop'].' '.$student['mos'].' '.$student['pt'].' )';    
        }    
        if($student['IdScheme']==11){ //GS
            if($student['pt']!='')
            $scheme = '( '.$student['pt'].' )';    
        }
        $this->view->program_scheme = $scheme;
          
        //get registered semester   		
        $registered_semester = $semesterStatusDB->getListSemesterRegistered($IdStudentRegistration);

        foreach($registered_semester as $index=>$semester){
            //get subject registered in each semester
            if($semester["IsCountable"]==1){				
                $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithAttendanceStatus($IdStudentRegistration,$semester['IdSemesterMain']);
            }else{			
                $subject_list = $regSubjectDB->getListCourseRegisteredBySemesterWithoutAttendance($IdStudentRegistration,$semester['IdSemesterMain']);
            }
            $registered_semester[$index]['subjects']=$subject_list;

            //get grade info
            $student_grade = $studentGradeDB->getStudentGrade($IdStudentRegistration,$semester['IdSemesterMain']);
            $registered_semester[$index]['grade'] = $student_grade;
        }

        if ($registered_semester){
            $i = 0;
            foreach ($registered_semester as $registered_semesterLoop){
                if (count($registered_semesterLoop['subjects']) > 0){
                    $k = 0;
                    foreach ($registered_semesterLoop['subjects'] as $subjectLoop){
                        $subArr = array();
                        $j = 0;
                        foreach ($registered_semester as $registered_semesterLoop2){
                            if ($j > $i){
                                foreach ($registered_semesterLoop2['subjects'] as $subjectLoop2){
                                    array_push($subArr, $subjectLoop2['IdSubject']);
                                }
                            }
                            $j++;
                        }
                        
                        if(in_array($subjectLoop['IdSubject'], $subArr)){
                            $pemalar = '-';
                            $pemalar2 = '0';
                           // $registered_semester[$i]['subjects'][$k]['credit_hour_registered'] = $pemalar2; /buang sebab thesis bukan category repeat
                            $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
                            $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
                        }

                        $checkRepeatRetake = $regSubjectDB->getRetakeSubject($subjectLoop['IdStudentRegSubjects'], $IdStudentRegistration);

                        if ($checkRepeatRetake){
                            $pemalar = '-';
                            $pemalar2 = '0';
                            
                            $registered_semester[$i]['subjects'][$k]['CreditHours'] = $pemalar2;
                            $registered_semester[$i]['subjects'][$k]['grade_point'] = $pemalar;
                        }
                        $k++;
                    }
                }
                $i++;
            }
        }

        $this->view->registered_semester = $registered_semester; 
    }
}
?>