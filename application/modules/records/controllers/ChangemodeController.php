<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_ChangemodeController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_ChangeMode();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Change Mode of Study/Programme Application');
        $form = new Records_Form_SearchChangeMode();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['search'])){
                $appList = $this->model->getChangeModeAppList($formData);
                $this->gobjsessionsis->changemodecontroller = $appList;
            }else{
                $appList = $this->model->getChangeModeAppList();
            }
        }else{
            $appList = $this->model->getChangeModeAppList();
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->changemodecontroller);
        }
        
        $lintpagecount = $this->gintPageCount;// Definitiontype model
        
        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->changemodecontroller)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->changemodecontroller,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($appList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }
    
    public function changemodeapplicationAction(){
        $this->view->title = $this->view->translate('Change Mode of Study/Programme Application');
        $form = new Records_Form_AddChangeMode();
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            
            if ($formData['NewMop']==$formData['currentmop'] && $formData['NewMos']==$formData['currentmos']){
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot apply the same mode!'));
                $this->_redirect($this->baseUrl . '/records/changemode/index');
            }else{
                $appData = array(
                    'cma_studregid'=>$formData['studRegId'],
                    'cma_appid'=>$this->generateApplicationID(),
                    'cma_applieddate'=>date('Y-m-d'),
                    'cma_appliedby'=>$userId,
                    'cma_status'=>839, 
                    'cma_reason'=>$formData['Reason'],
                    'cma_reasonothers'=>$formData['Reason'] == 99 ? $formData['others']:'',
                    'cma_remarks'=>$formData['Remarks'], 
                    'cma_semesterid'=>$formData['Semester'], 
                    'cma_mop'=>$formData['NewMop'], 
                    'cma_mos'=>$formData['NewMos'],
                    'cma_currentmop'=>$formData['currentmop'],
                    'cma_currentmos'=>$formData['currentmos'],
                    'cma_programid'=>$formData['programId'], 
                    'cma_pt'=>$formData['programType'], 
                    'cma_upddate'=>date('Y-m-d'), 
                    'cma_upduser'=>$userId,
                    'cma_source'=>'Admin'
                );
                $appId = $this->model->insertChangeModeApplication($appData);

                //upload file
                $folder = '/changemode/'.$appId;
                $dir = DOCUMENT_PATH.$folder;

                if ( !is_dir($dir) ){
                    if ( mkdir_p($dir) === false )
                    {
                        throw new Exception('Cannot create attachment folder ('.$dir.')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
                $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                if ($files){
                    foreach($files as $key=>$filesLoop){

                        $fileOriName = $filesLoop['name'];
                        $fileRename = date('YmdHis').'_'.$fileOriName;
                        $filepath = $filesLoop['destination'].'/'.$fileRename;

                        $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                        $adapter->isUploaded($key);
                        $adapter->receive($key);

                        if ($filesLoop['size']!=null){
                            $uploadData = array(
                                'cmu_cmaid'=>$appId, 
                                'cmu_filename'=>$fileOriName, 
                                'cmu_filerename'=>$fileRename, 
                                'cmu_filelocation'=>$folder.'/'.$fileRename, 
                                'cmu_filesize'=>$filesLoop['size'], 
                                'cmu_mimetype'=>$filesLoop['type'], 
                                'cmu_upddate'=>date('Y-m-d'), 
                                'cmu_upduser'=>$userId
                            );
                            $this->model->insertChangeModeUpload($uploadData);
                        }
                    }
                }
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/records/changemode/changemodeapplicationedit/cmaId/'.$appId);
        }
    }
    
    public function changemodeapplicationeditAction(){
        $cmaId = $this->_getParam('cmaId', 0);
        $this->view->title = $this->view->translate('Edit Change Mode of Study/Programme Application');
        $appInfo = $this->model->getChangeModeApplication($cmaId);
        $this->view->appInfo = $appInfo;
        $form = new Records_Form_EditChangeMode(array('programId'=>$appInfo['IdProgram'], 'programType'=>$appInfo['program_type'], 'mop'=>$appInfo['cma_mop']));
        $this->view->form = $form;
        
        $populateData = array();
        $populateData['NewMop']=$appInfo['cma_mop'];
        $populateData['NewMos']=$appInfo['cma_mos'];
        $populateData['Reason']=$appInfo['cma_reason'];
        $populateData['Remarks']=$appInfo['cma_remarks'];
        $populateData['Semester']=$appInfo['cma_semesterid'];

        $form->populate($populateData);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        //address
        $address = '';
        $address .= (($appInfo['appl_address1']!='' || $appInfo['appl_address1']!=null) ? $appInfo['appl_address1']:'');
        $address .= (($appInfo['appl_address2']!='' || $appInfo['appl_address2']!=null) ? ',<br/>'.$appInfo['appl_address2']:'');
        $address .= (($appInfo['appl_address3']!='' || $appInfo['appl_address3']!=null) ? ',<br/>'.$appInfo['appl_address3']:'');
        $address .= (($appInfo['appl_postcode']!='' || $appInfo['appl_postcode']!=null) ? ',<br/>'.$appInfo['appl_postcode']:'');
        $address .= (($appInfo['CityName']!='' || $appInfo['CityName']!=null) ? ',<br/>'.$appInfo['CityName']:'');
        $address .= (($appInfo['StateName']!='' || $appInfo['StateName']!=null) ? ',<br/>'.$appInfo['StateName']:'');
        $address .= (($appInfo['CountryName']!='' || $appInfo['CountryName']!=null) ? ',<br/>'.$appInfo['CountryName']:'');
        
        $this->view->address = $address;
        
        $fileUpl = $this->model->getFileUpload($cmaId);
        $this->view->fileUpl = $fileUpl;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $appData = array(
                'cma_mop'=>$formData['NewMop'],
                'cma_mos'=>$formData['NewMos'],
                'cma_reason'=>$formData['Reason'],
                'cma_reasonothers'=>$formData['Reason'] == 99 ? $formData['others']:'',
                'cma_semesterid'=>$formData['Semester'],
                'cma_remarks'=>$formData['Remarks'],
                'cma_upddate'=>date('Y-m-d'),
                'cma_upduser'=>$userId
            );
            $this->model->updateChangeModeApplication($appData, $cmaId);
            
            //delete file upload
            if (isset($formData['deleteuplid']) || count($formData['deleteuplid']) > 0){
                foreach ($formData['deleteuplid'] as $delUplLoop){
                    $getFileuplInfo = $this->model->getDelUplFileInfo($delUplLoop);
                    
                    unlink(DOCUMENT_PATH.$getFileuplInfo['cmu_filelocation']);
                    
                    $this->model->deleteUploadFile($delUplLoop);
                }
            }
            
            //upload file
            $folder = '/changemode/'.$cmaId;
            $dir = DOCUMENT_PATH.$folder;

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false )
                {
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
            $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();
            
            if ($files){
                foreach($files as $key=>$filesLoop){
                    $fileOriName = $filesLoop['name'];
                    $fileRename = date('YmdHis').'_'.$fileOriName;
                    $filepath = $filesLoop['destination'].'/'.$fileRename;

                    $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                    $adapter->isUploaded($key);
                    $adapter->receive($key);
                    
                    if ($filesLoop['size']!=null){
                        $uploadData = array(
                            'cmu_cmaid'=>$cmaId, 
                            'cmu_filename'=>$fileOriName, 
                            'cmu_filerename'=>$fileRename, 
                            'cmu_filelocation'=>$folder.'/'.$fileRename, 
                            'cmu_filesize'=>$filesLoop['size'], 
                            'cmu_mimetype'=>$filesLoop['type'], 
                            'cmu_upddate'=>date('Y-m-d'), 
                            'cmu_upduser'=>$userId
                        );
                        $this->model->insertChangeModeUpload($uploadData);
                    }
                }
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/records/changemode/changemodeapplicationedit/cmaId/'.$cmaId);
        }
    }
    
    public function changemodeapprovallistAction(){
        $this->view->title = $this->view->translate('Change Mode of Study/Programme Approval');
        $form = new Records_Form_SearchChangeMode();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['search'])){
                $appList = $this->model->getChangeModeAppList($formData);
                $this->gobjsessionsis->changemodecontroller = $appList;
            }else{
                $appList = $this->model->getChangeModeAppList();
            }
        }else{
            $appList = $this->model->getChangeModeAppList();
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->changemodecontroller);
        }
        
        $lintpagecount = $this->gintPageCount;// Definitiontype model
        
        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->changemodecontroller)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->changemodecontroller,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($appList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }
    
    public function changemodeapprovalAction(){
        $cmaId = $this->_getParam('cmaId', 0);
        $this->view->title = $this->view->translate('Edit Change Mode of Study/Programme Approval');
        $appInfo = $this->model->getChangeModeApplication($cmaId);
        $this->view->appInfo = $appInfo;
        $form = new Records_Form_EditChangeMode(array('programId'=>$appInfo['IdProgram'], 'programType'=>$appInfo['program_type'], 'mop'=>$appInfo['cma_mop']));
        $this->view->form = $form;
        
        $populateData = array();
        $populateData['NewMop']=$appInfo['cma_mop'];
        $populateData['NewMos']=$appInfo['cma_mos'];
        $populateData['Reason']=$appInfo['cma_reason'];
        $populateData['Remarks']=$appInfo['cma_remarks'];
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $userInfo = $this->model->getUser($userId);
        $this->view->userInfo = $userInfo;
        
        $form->populate($populateData);
        
        //address
        $address = '';
        $address .= (($appInfo['appl_address1']!='' || $appInfo['appl_address1']!=null) ? $appInfo['appl_address1']:'');
        $address .= (($appInfo['appl_address2']!='' || $appInfo['appl_address2']!=null) ? ',<br/>'.$appInfo['appl_address2']:'');
        $address .= (($appInfo['appl_address3']!='' || $appInfo['appl_address3']!=null) ? ',<br/>'.$appInfo['appl_address3']:'');
        $address .= (($appInfo['appl_postcode']!='' || $appInfo['appl_postcode']!=null) ? ',<br/>'.$appInfo['appl_postcode']:'');
        $address .= (($appInfo['CityName']!='' || $appInfo['CityName']!=null) ? ',<br/>'.$appInfo['CityName']:'');
        $address .= (($appInfo['StateName']!='' || $appInfo['StateName']!=null) ? ',<br/>'.$appInfo['StateName']:'');
        $address .= (($appInfo['CountryName']!='' || $appInfo['CountryName']!=null) ? ',<br/>'.$appInfo['CountryName']:'');
        
        $this->view->address = $address;
        
        $fileUpl = $this->model->getFileUpload($cmaId);
        $this->view->fileUpl = $fileUpl;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['approve_app'])){
                //get new program scheme
                $newProgScheme = $this->model->getNewProgramScheme($appInfo['IdProgram'], $appInfo['cma_mop'], $appInfo['cma_mos'], $appInfo['program_type']);
                
                //get new landscape
                $newLandscape = $this->model->getNewLandscape($appInfo['IdProgram'], $newProgScheme['IdProgramScheme'], $appInfo['IdIntake']);
                
                $feeStructureData = $this->model->getApplicantFeeStructure($appInfo['IdProgram'], $newProgScheme['IdProgramScheme'], $appInfo['IdIntake'], $appInfo['appl_category'], 0);
                if ($feeStructureData != false){
                    $data = array (
                        'fs_id'=>$feeStructureData['fs_id']
                    );
                    $this->model->updateStudentProgramScheme($data, $appInfo['cma_studregid']);
                }else{
                    //redirect here
                    $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot Approve, No Fee Structure.'));
                    $this->_redirect($this->baseUrl . '/records/changemode/changemodeapproval/cmaId/'.$cmaId);
                }
                
                //update new landcape and program scheme
                $updateData = array(
                    'IdLandscape'=>$newLandscape['IdLandscape'],
                    'IdProgramScheme'=>$newProgScheme['IdProgramScheme']
                );
                $this->model->updateStudentProgramScheme($updateData, $appInfo['cma_studregid']);
                
                //update status
                $statusData = array(
                    'cma_status'=>841,
                    'cma_comment'=>$formData['RemarksApprove'],
                    'cma_approveddate'=>date('Y-m-d'),
                    'cma_approvedby'=>$userId,
                    'cma_upddate'=>date('Y-m-d'),
                    'cma_upduser'=>$userId
                );
                $this->model->updateChangeModeApplication($statusData, $cmaId);
                
                $message = "Application has been approved";
            }else if(isset($formData['reject_app'])){
                //update status
                $statusData = array(
                    'cma_status'=>842,
                    'cma_comment'=>$formData['RemarksApprove'],
                    'cma_approveddate'=>date('Y-m-d'),
                    'cma_approvedby'=>$userId,
                    'cma_upddate'=>date('Y-m-d'),
                    'cma_upduser'=>$userId
                );
                $this->model->updateChangeModeApplication($statusData, $cmaId);
                
                $message = "Application has been rejected";
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => $message));
            $this->_redirect($this->baseUrl . '/records/changemode/changemodeapproval/cmaId/'.$cmaId);
        }
    }
    
    public function findStudentAction(){
        $searchElement = $this->_getParam('term', '');
        $searchType = $this->_getParam('searchType', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $studentList = $this->model->findStudent($searchElement, $searchType);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $userInfo = $this->model->getUser($userId);
        
        if ($studentList){
            foreach($studentList as $student) {
                
                $address = (($student['appl_address1']!='' || $student['appl_address1']!=null) ? $student['appl_address1']:'');
                $address .= (($student['appl_address2']!='' || $student['appl_address2']!=null) ? ', '.$student['appl_address2']:'');
                $address .= (($student['appl_address3']!='' || $student['appl_address3']!=null) ? ', '.$student['appl_address3']:'');
                $address .= (($student['appl_postcode']!='' || $student['appl_postcode']!=null) ? ', '.$student['appl_postcode']:'');
                $address .= (($student['CityName']!='' || $student['CityName']!=null) ? ', '.$student['CityName']:'');
                $address .= (($student['StateName']!='' || $student['StateName']!=null) ? ', '.$student['StateName']:'');
                $address .= (($student['CountryName']!='' || $student['CountryName']!=null) ? ', '.$student['CountryName']:'');

                $status = $this->model->getDefination2(155, 'Applied');
                
                //current sem
                $curSem = $this->model->getCurSem($student['schemeid']);
                
                $student_array[] = array(
                    'id' => $student['IdStudentRegistration'],
                    'ApplicationID'=>'Will generate after save',
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeId'=>$student['IdProgramScheme'],
                    'LandscapeId'=>$student['IdLandscape'],
                    'StatusName'=>$student['StatusName'],
                    'IntakeName'=>$student['IntakeName'],
                    'BranchName'=>$student['BranchName'],
                    'ProgramName'=>$student['ProgramName'],
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeName'=>$student['mop'].' '.$student['mos'].' '.$student['pt'],
                    'mop'=>$student['mop'],
                    'mos'=>$student['mos'],
                    'currentmop'=>$student['mode_of_program'],
                    'currentmos'=>$student['mode_of_study'],
                    'pt'=>$student['program_type'],
                    'Address'=>$address,
                    'PhoneHome'=>$student['appl_phone_home'],
                    'PhoneMobile'=>$student['appl_phone_mobile'],
                    'PhoneOffice'=>$student['appl_phone_office'],
                    'Email'=>$student['appl_email'],
                    'IdType'=>$student['IdTypeName'],
                    'IdNo'=>$student['appl_idnumber'],
                    'AppliedDate'=>date('d-m-Y'),
                    'AppliedBy'=>$userInfo['loginName'],
                    'AppliedStatus'=>$status['DefinitionDesc'],
                    'CurrentSem'=>$curSem['IdSemesterMaster'],
                    'label' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname'],
                    'value' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
                );
            }
        }else{
            $student_array[] = array();
        }
        
        $json = Zend_Json::encode($student_array);
		
	echo $json;
	exit();
    }
    
    public function getNewMopAction(){
        $programId = $this->_getParam('programId', 0);
        $programType = $this->_getParam('pt', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $mopList = $this->model->getNewMop($programId, $programType);
        
        $json = Zend_Json::encode($mopList);
		
	echo $json;
	exit();
    }
    
    public function getNewMosAction(){
        $programId = $this->_getParam('programId', 0);
        $programType = $this->_getParam('pt', 0);
        $mop = $this->_getParam('mop', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $mosList = $this->model->getNewMos($programId, $programType, $mop);
        
        $json = Zend_Json::encode($mosList);
		
	echo $json;
	exit();
    }
    
    public function getprogramschemeAction(){
        $programId = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $programSchemeList = $this->model->getProgramSchemeBasedOnProgram($programId);
        
        $json = Zend_Json::encode($programSchemeList);
		
	echo $json;
	exit();
    }
    
    public function cancelchangemodeapplicationAction(){
        $cmaId = $this->_getParam('cmaId', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($cmaId!=0){
            $appApList = $this->model->getChangeModeApplication($cmaId);
            //var_dump($appApList);
            
            $statusData = array(
                'cma_status'=>840,
                'cma_comment'=>'Application Cancel',
                'cma_approveddate'=>date('Y-m-d'),
                'cma_approvedby'=>$userId,
                'cma_upddate'=>date('Y-m-d'),
                'cma_upduser'=>$userId
            );
            $this->model->updateChangeModeApplication($statusData, $cmaId);
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Application has been cancel <br/><br/>Info :<br/>Name : '.$appApList['appl_fname'].' '.$appApList['appl_lname'].'<br/>Student ID : '.$appApList['registrationId']));
            $this->_redirect($this->baseUrl . '/records/changemode/index');
        }
        exit;
    }
    
    function generateApplicationID(){
        
        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        //bermulanya sebuah penciptaan id
        
        //check sequence
        $checkSequence = $this->model->getSequence(4, date('Y'));

        if (!$checkSequence){
            $dataSeq = array(
                'tsi_type'=>4, 
                'tsi_seq_no'=>1, 
                'tsi_seq_year'=>date('Y'), 
                'tsi_upd_date'=>date('Y-m-d'), 
                'tsi_upd_user'=>$userId
            );
            $this->model->insertSequence($dataSeq);
        }

        //generate applicant ID
        $applicationid_format = array();

        //get sequence
        $sequence = $this->model->getSequence(4, date('Y'));

        $config = $this->model->getConfig(1);

        //format
        $format_config = explode('|',$config['changeModeIdFormat']);
        //var_dump($format_config);
        for($i=0; $i<count($format_config); $i++){
            $format = $format_config[$i];
            if($format=='CM'){ //prefix
                $result = $config['changeModePrefix'];
            }elseif($format=='YYYY'){
                $result = date('Y');
            }elseif($format=='YY'){
                $result = date('y');
            }elseif($format=='seqno'){				
                $result = sprintf("%05d", $sequence['tsi_seq_no']);
            }
            array_push($applicationid_format,$result);
        }

        $applicationID = implode("",$applicationid_format);

        //update sequence		
        $this->model->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        
        return $applicationID;
    }
    
    public function revertApplicationAction(){
        $cmaId = $this->_getParam('cmaId', 0);
        
        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($cmaId != 0){
            $appInfo = $this->model->getChangeModeApplication($cmaId);
            
            //get new program scheme
            $newProgScheme = $this->model->getNewProgramScheme($appInfo['IdProgram'], $appInfo['cma_currentmop'], $appInfo['cma_currentmos'], $appInfo['program_type']);

            //get new landscape
            $newLandscape = $this->model->getNewLandscape($appInfo['IdProgram'], $newProgScheme['IdProgramScheme'], $appInfo['IdIntake']);

            $feeStructureData = $this->model->getApplicantFeeStructure($appInfo['IdProgram'], $newProgScheme['IdProgramScheme'], $appInfo['IdIntake'], $appInfo['appl_category'], 0);
            if ($feeStructureData != false){
                $data = array (
                    'fs_id'=>$feeStructureData['fs_id']
                );
                $this->model->updateStudentProgramScheme($data, $appInfo['cma_studregid']);
            }else{
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot Revert, No Fee Structure.'));
                $this->_redirect($this->baseUrl . '/records/changemode/changemodeapproval/cmaId/'.$cmaId);
            }
            
            //update new landcape and program scheme
            $updateData = array(
                'IdLandscape'=>$newLandscape['IdLandscape'],
                'IdProgramScheme'=>$newProgScheme['IdProgramScheme']
            );

            $this->model->updateStudentProgramScheme($updateData, $appInfo['cma_studregid']);
            
            //update status
            $statusData = array(
                'cma_status'=>839,
                'cma_comment'=>'',
                'cma_upddate'=>date('Y-m-d'),
                'cma_upduser'=>$userId
            );
            $this->model->updateChangeModeApplication($statusData, $cmaId);
            
            $updateProgramData = array(
                'mode_study' => $appInfo['cma_currentmop'], 
                'program_mode' => $appInfo['cma_currentmop'], 
                'ap_prog_scheme' => $newProgScheme['IdProgramScheme']
            );
            $this->model->updateProgramData($updateProgramData, $appInfo['transaction_id']);

            $message = "Revert success";
            $this->_helper->flashMessenger->addMessage(array('success' => $message));
        }else{
            $message = "Revert failed";
            $this->_helper->flashMessenger->addMessage(array('error' => $message));
        }
        
        //redirect here
        $this->_redirect($this->baseUrl . '/records/changemode/changemodeapproval/cmaId/'.$cmaId);

        exit;
    }
    
    public function getSemesterAction(){
        $programId = $this->_getParam('programId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $programInfo = $this->model->getProgramForSem($programId);
        
        $semList = $this->model->getSemBasedOnList($programInfo['IdScheme']);
        
        $json = Zend_Json::encode($semList);
		
	echo $json;
	exit();
    }
    
    public function generateSlipAction(){
        $cmaId = $this->_getParam('id', 0);
        $appInfo = $this->model->getChangeModeApplication($cmaId);
        
        $template = $this->model->getSlipTemplate();
        //var_dump($appInfo);
        //echo $template['tpl_content']; exit;
        $content = $this->replaceTag($template['tpl_content'], $cmaId);
        //echo $content; exit;
        $filename = $appInfo['cma_appid'].'_'.date('dmYHis');
        
        $content_option = array(
            'content'=>$content,
            'file_name' => $filename,
            'file_extension' => 'pdf',
        );
        
        generatePdf($content_option);
        
        exit;
    }
    
    public function replaceTag($content, $cmaId){
        $appInfo = $this->model->getChangeModeApplication($cmaId);
        $tags = $this->model->getTags();
        
        if ($tags){
            foreach ($tags as $tagsLoop){
                switch ($tagsLoop['tag_name']){
                    case 'Student Name':
                        $content = str_replace('[Student Name]', $appInfo['appl_fname'].' '.$appInfo['appl_lname'], $content);
                        break;
                    case 'Student ID':
                        $content = str_replace('[Student ID]', $appInfo['registrationId'], $content);
                        break;
                    case 'ID Number':
                        $content = str_replace('[ID Number]', $appInfo['appl_idnumber'], $content);
                        break;
                    case 'Programme':
                        $content = str_replace('[Programme]', $appInfo['ProgramName'], $content);
                        break;
                    case 'Mode of Programme':
                        $content = str_replace('[Mode of Programme]', $appInfo['mop'], $content);
                        break;
                    case 'Mode of Study':
                        $content = str_replace('[Mode of Study]', $appInfo['mos'], $content);
                        break;
                    case 'Intake':
                        $content = str_replace('[Intake]', $appInfo['IntakeName'], $content);
                        break;
                    case 'Old Mode of Programme':
                        $content = str_replace('[Old Mode of Programme]', $appInfo['currentMopName'], $content);
                        break;
                    case 'Old Mode of Study':
                        $content = str_replace('[Old Mode of Study]', $appInfo['currentMosName'], $content);
                        break;
                    case 'New Mode of Programme':
                        $content = str_replace('[New Mode of Programme]', $appInfo['changeMopName'], $content);
                        break;
                    case 'New Mode of Study':
                        $content = str_replace('[New Mode of Study]', $appInfo['changeMosName'], $content);
                        break;
                    case 'Semester':
                        $content = str_replace('[Semester]', $appInfo['SemesterName'], $content);
                        break;
                    case 'Date':
                        $content = str_replace('[Date]', date('d F Y'), $content);
                        break;
                }
            }
        }
        
        return $content;
    }
}
?>