	<?php

class Records_StudentprogramchangeController extends Base_Base { //Controller for the User Module

	private $lobjUser;
	private $_gobjlog;
	private $lobjplacementtestModel;
	private $lobjdeftype;
	private $lobjchangeprogramappModel;
	private $lobjstudentgeneratedmodel;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjcredittransferForm = new Records_Form_Credittransferapp();
		$this->lobjchangeprogramForm = new Records_Form_Changeprogram();
		$this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
		$this->lobjstudentprogramchange = new Records_Model_DbTable_Studentprogramchange();
		$this->lobjplacementtestModel = new Application_Model_DbTable_Placementtest();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjchangeprogramappModel = new Records_Model_DbTable_Changeprogramapplication();
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjstudentHistoryModel = new Registration_Model_DbTable_Studenthistory();
		$this->lobjstudentgeneratedmodel = new App_Model_StudentGeneratedId();
	}

	public function changeprogramapplicationAction(){
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform;
		$ProgramList = $this->lobjplacementtestModel->fnGetProgramMaterList();
		$lobjform->field1->addMultiOptions($ProgramList);

		$larrdefmsstatus= $this->lobjdeftype->fnGetProfilestatus('Change Program');
		$lobjform->field8->addMultiOptions($larrdefmsstatus);

		$larrdefmsstatusprofile= $this->lobjdeftype->fnGetProfilestatus('Student Status');
		$lobjform->field5->addMultiOptions($larrdefmsstatusprofile);

		$larrresult = $this->lobjchangeprogramappModel->getAllchangeprogramApplication();

		if (!$this->_getParam('search'))
		unset($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult);
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page', 1); // Paginator instance
		if (isset($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
		}

		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			//if ($this->lobjform->isValid($larrformData)) {
			//  die('here');
			$larrresult = $this->lobjchangeprogramappModel->fngetSearchchangeprogramApplication($larrformData); //searching the values for the user
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
			$this->gobjsessionsis->progseniorstudentregistrationpaginatorresult = $larrresult;
			//}
			//die;
		}

		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/records/studentprogramchange/changeprogramapplication');
		}


	}

	public function addchangeprogramapplicationAction(){
		$newcredit = "";
		$this->view->lobjcredittransferForm = $this->lobjcredittransferForm;
		$this->view->lobjchangeprogramForm = $this->lobjchangeprogramForm;
		$changeprogramconfig = $this->lobjstudentprogramchange->fngetchangeprogramconfig();

		$date = date("Y-m-d");
		$this->view->lobjchangeprogramForm->Date->setValue($date);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjchangeprogramForm->AppliedBy->setValue($auth->getIdentity()->iduser);
		$authUserName = $this->lobjsubjectmasterModel->getUserName($auth->getIdentity()->iduser);
		$this->view->lobjchangeprogramForm->AppliedByName->setValue($authUserName['loginName']);
		$this->view->lobjchangeprogramForm->ApplicationStatus->setValue(236);
		$this->view->lobjchangeprogramForm->ApplicationStatusByName->setValue('Entry');
		$this->view->ctType = $this->lobjdeftype->fnGetProfilestatus('CT Status');
		$listCPapplication = $this->lobjstudentprogramchange->fngetListCPApplication();

		$count = 0;
		if(!empty($listCPapplication)){
			$count = count($listCPapplication);
		}
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$dateTime = date('Y-m-d H:i:s');
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$data['IdCPApplication'] = 'CPA'.($count+1);
			$data['IdStudent'] = $formData['IdStudentRegistration'];
			$data['Reason'] = $formData['Reason'];
			$data['ApplicationDate'] = $dateTime;
			$data['AppliedBy'] = $formData['AppliedBy'];
			$data['ApplicationStatus'] = $formData['ApplicationStatus'];
			$data['CurrentProgram'] = $formData['CurrentProgramId'];
			$data['NewProgram'] = $formData['NewProgram'];
			$data['EffectiveSemester'] = $formData['EffectiveSemester'];
			$data['Remarks'] = "";
			$data['ApprovedBy'] = "";
			$data['ApprovedDate'] = "";
			$data['UpdUser'] = $userId;
			$data['UpdDate'] = $dateTime;
			$insertId = $this->lobjstudentprogramchange->fnSaveChangeprogram($data);
			if(isset($formData['IdCourseGrid'])) {
				for($i=0;$i<count($formData['IdCourseGrid']);$i++){

					if($formData['setGrid'][$i]==1){
						$subjectdataNew['IdChangeProgram'] = $insertId;
						$subjectdataNew['IdSemesterMain'] = $formData['EffectiveSemester'];
						$subjectdataNew['IdSemesterDetail'] = "";
						$subjectdataNew['IdCourse'] = $formData['IdCourseGrid'][$i];
						$subjectdataNew['Grade'] = $formData['GradeGrid'][$i];
						$subjectdataNew['Type'] = $formData['ctTypeGrid'][$i];
						$subjectdataNew['UpdUser'] = $userId;
						$subjectdataNew['UpdDate'] = $dateTime;
						$this->lobjstudentprogramchange->fnSaveChangeprogramSubjects($subjectdataNew);
					}
				}
			}
				
			if(isset($changeprogramconfig[0]['ApprovalChangeProgram']) && $changeprogramconfig[0]['ApprovalChangeProgram'] == "0"){
				$appDetail = $this->lobjstudentprogramchange->getDetailApplication($insertId);
				$studentinfo = $this->lobjstudentprogramchange->fngetstudentinfo($appDetail[0]['IdStudent']);
				$credittransferapp = $this->lobjstudentprogramchange->fngetstudentcredittransferapp($studentinfo[0]['IdStudentRegistration']);
				if(count($credittransferapp) == 0){
					$studentdetails = $this->lobjStudentregistrationModel->fetchStudentDetails($studentinfo[0]['IdApplication']);
					if(isset($studentinfo[0]['IdIntake'])) {
						$studentdetails['intake'] = $studentinfo[0]['IdIntake'];
					}
					if(isset($appDetail[0]['NewProgram'])) {
						$studentdetails['ProgramOfferred'] = $appDetail[0]['NewProgram'];
					}
					$this->view->currentsem = $currentsem = $this->lobjstudentprogramchange->fngetstudentcurrentsem($appDetail[0]['IdStudent']);
					unset($studentinfo[0]['IdApplication']);
					// check for Registration ID format. is it manual(0) or auto(1) from tbl_config
					$this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
					$idUniversity = $this->gobjsessionsis->idUniversity;
					$confdetail = $this->lobjconfig->fnGetInitialConfigDetails($idUniversity);
					//asd($confdetail,false);
					$registrationCodeType = $confdetail['RegistrationCodeType'];
					$registrationFormat = $confdetail['RegistrationIdFormat'];
					if($registrationCodeType=='0') {
						$this->view->makeRegFieldHide = '0';
					} else {
						$this->view->makeRegFieldHide = '1';
						$codeGenrateObj = new Cms_CodeGeneration();
						$codeGenrateResult = $codeGenrateObj->RegistrationIdGenration($idUniversity, $confdetail,$studentdetails);
					}


					$this->view->lobjchangeprogramForm->EffectiveSemester->setValue($appDetail[0]['EffectiveSemester']);

					$this->view->lobjchangeprogramForm->NewProgram->setValue($appDetail[0]['NewProgram']);
					$this->view->lobjchangeprogramForm->NewProgram->setAttrib('readonly','true');
					$studentlandscapedetails = $this->lobjstudentprogramchange->fngetstudentprogramcourseedit($appDetail[0]['CurrentProgram'],$appDetail[0]['IdStudent']);
					$index = 0;
					$beforecredit = 0;
					$aftercredit = 0;
					for($i=0;$i<count($appDetail);$i++){
						for($j=0;$j<count($studentlandscapedetails);$j++){
							if(isset($studentlandscapedetails[$j]['IdSubject'])){
								if($appDetail[$i]['IdCourse']==$studentlandscapedetails[$j]['IdSubject']){
									$registeredDetails[$index] = $studentlandscapedetails[$j];
									$aftercredit = $aftercredit+$studentlandscapedetails[$j]['CreditHours'];
									unset($studentlandscapedetails[$j]);
									$index++;
								}
							}
						}
					}
					$studentnewlandscapedetails = $this->lobjstudentprogramchange->fngetstudentnewprogramcourse($appDetail[0]['NewProgram']);
					if(!isset($studentnewlandscapedetails[0]['IdLandscape'])) {
						echo "
							<script type='text/javascript'>
							var x=window.confirm('This application cannot be processed as no landscape is active for ".$appDetail[0]['NewProgramName']."');
		
							if(x){
							parent.location ='".$this->view->baseUrl()."/records/studentprogramchange/changeprogramapplication';
							}
							else{
							parent.location ='".$this->view->baseUrl()."/records/studentprogramchange/changeprogramapplication';
							}
							</script>";
						exit;
					}
						
					$studentid = $studentinfo[0]['IdStudentRegistration'];
					unset($studentinfo[0]['IdStudentRegistration']);
					$studentinfo[0]['OldIdStudentRegistration'] = $formData['IdStudentRegistration'];
					if($this->view->makeRegFieldHide == "1"){
						$studentinfo[0]['registrationId'] = $codeGenrateResult['IdStudentRegistrationFormatted'];
						$studentinfo[0]['IdStudentRegistrationFormatted'] = $codeGenrateResult['IdStudentRegistrationFormatted'];
						$studentinfo[0]['IdFormat'] = $codeGenrateResult['IdFormat'];
						$codeGenrateResult['larrsepvalues']['UpdDate'] = date("Y-m-d");
						$codeGenrateResult['larrsepvalues']['UpdUser'] = $auth->getIdentity()->iduser;
					}
					else if($this->view->makeRegFieldHide == "0"){
						$studentinfo[0]['registrationId'] = $formData['registrationId'];
					}
					$studentinfo[0]['profileStatus'] = 92;
					$studentinfo[0]['IdLandscape'] = $studentnewlandscapedetails[0]['IdLandscape'];
					$studentinfo[0]['IdProgram'] = $appDetail[0]['NewProgram'];
					$studentinfo[0]['UpdDate'] = date("Y-m-d");
					$studentinfo[0]['UpdUser'] = $auth->getIdentity()->iduser;
					$studentinfo[0]['ApplicationDate'] = date('Y-m-d H:i:s');
					$newstudentId = $this->lobjstudentprogramchange->fncreatenewstudent($studentinfo[0]);
					if($this->view->makeRegFieldHide) {
						$codeGenrateResult['larrsepvalues']['IdStudentRegistration'] = $newstudentId;
						$this->lobjstudentgeneratedmodel->fninsert($codeGenrateResult['larrsepvalues']);
					}
					$statusArray1['profileStatus'] = 92;
					$statusArray1['IdStudentRegistration'] = $newstudentId;
					$statusArray1['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
					$statusArray1['UpdUser'] = $auth->getIdentity()->iduser;
					$statusArray1['UpdDate'] = date("Y-m-d");
					$this->lobjstudentHistoryModel->addStudentProfileHistory($statusArray1);
					$upddata['profileStatus'] = 251;
					$this->lobjStudentregistrationModel->updateStudentProfile($studentid,$upddata);
					$statusArray['profileStatus'] = 251;
					$statusArray['IdStudentRegistration'] = $studentid;
					$statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
					$statusArray['UpdUser'] = $auth->getIdentity()->iduser;
					$statusArray['UpdDate'] = date("Y-m-d");
					$this->lobjstudentHistoryModel->addStudentProfileHistory($statusArray);
					$oldsem['studentsemesterstatus'] = 252;
					$this->lobjStudentregistrationModel->updateStudentoldsem($currentsem[0]['idstudentsemsterstatus'],$oldsem);
					$effectivesem = $this->lobjsubjectmasterModel->getSemesterid($appDetail[0]['EffectiveSemester']);
					$semdata['IdStudentRegistration'] = $newstudentId;
					if(isset($effectivesem['IdSemesterMaster'])){
						$semdata['IdSemesterMain'] = $effectivesem['IdSemesterMaster'];
					}
					else if(isset($effectivesem['IdSemester'])){
						$semdata['idSemester'] = $effectivesem['IdSemester'];
					}
					$semdata['studentsemesterstatus'] = 130;
					$semdata['UpdUser'] = $auth->getIdentity()->iduser;
					$semdata['UpdDate'] = date("Y-m-d");

					$this->lobjstudentprogramchange->fnregistersemester($newstudentId,$semdata);
					$credittransfercourse = $this->lobjstudentprogramchange->fngetcredittransfercourse($appDetail[0]['IdChangeProgramApplication'],$studentid);
					$idsem = $this->lobjsubjectmasterModel->getSemesterid("Dummy".date('Y'));
					if($idsem == ''){
						$dummysemData['SemesterMainName'] = "Dummy".date('Y');
						$dummysemData['SemesterMainDefaultLanguage'] = "Dummy".date('Y');
						$dummysemData['SemesterMainCode'] = "Dummy".date('Y');
						$dummysemData['IsCountable'] = 1;
						$dummysemData['SemesterMainStatus'] = 130;
						$dummysemData['SemesterMainStartDate'] = date('Y-m-d');
						$dummysemData['SemesterMainEndDate'] = date('Y-m-d');
						$dummysemData['Scheme'] = 1;
						$dummysemData['DummyStatus'] = 1;
						$dummysemData['UpdUser'] = $auth->getIdentity()->iduser;
						$dummysemData['UpdDate'] = date("Y-m-d");
						$idsem = $this->lobjstudentprogramchange->fncreatesemester($dummysemData);
					}
					for($i=0;$i<count($credittransfercourse);$i++){
						$subjectdata['IdStudentRegistration'] = $newstudentId;
						$subjectdata['IdSubject'] = $credittransfercourse[$i]['IdCourse'];
						$subjectdata['IdGrade'] = "CT";
						$subjectdata['IdSemesterMain'] = $idsem['IdSemesterMaster'];
						$subjectdata['UpdUser'] = $auth->getIdentity()->iduser;
						$subjectdata['UpdDate'] = date("Y-m-d");
						$this->lobjstudentprogramchange->fnregistersubjects($subjectdata);
					}
					for($i=0;$i<count($studentnewlandscapedetails);$i++){
						if($studentnewlandscapedetails[$i]['IdSemester'] == "1"){
							for($j=0;$j<count($credittransfercourse);$j++){
								if($studentnewlandscapedetails[$i]['IdSubject'] != $credittransfercourse[$j]['IdCourse']){
									$newsubjectdata['IdStudentRegistration'] = $newstudentId;
									$newsubjectdata['IdSubject'] = $studentnewlandscapedetails[$i]['IdSubject'];
									if(isset($effectivesem['IdSemesterMaster'])){
										$newsubjectdata['IdSemesterMain'] = $effectivesem['IdSemesterMaster'];
									}
									else if(isset($effectivesem['IdSemester'])){
										$newsubjectdata['IdSemesterDetails'] = $effectivesem['IdSemester'];
									}
									$newsubjectdata['UpdUser'] = $auth->getIdentity()->iduser;
									$newsubjectdata['UpdDate'] = date("Y-m-d");
									$this->lobjstudentprogramchange->fnregistersubjects($newsubjectdata);
								}
							}
						}
					}

					$approvedata['Remarks'] = "Approve";
					$approvedata['ApprovedBy'] = $auth->getIdentity()->iduser;
					$approvedata['ApprovedDate'] = date("Y-m-d");
					$approvedata['ApplicationStatus'] = 238;
					$this->lobjstudentprogramchange->fnupdateapplication($approvedata,$insertId);

				}
			}
			$this->_redirect($this->baseUrl . '/records/studentprogramchange/changeprogramapplication');
		}
			
	}

	public function editchangeprogramapplicationAction(){
		$newcredit = "";
		$this->view->lobjcredittransferForm = $this->lobjcredittransferForm;
		$this->view->lobjchangeprogramForm = $this->lobjchangeprogramForm;
		$IdchangeProgramApp = $this->_getParam('Id');
		$this->view->appDetail=$appDetail = $this->lobjstudentprogramchange->getDetailApplication($IdchangeProgramApp);
		$this->view->lobjchangeprogramForm->Reason->setValue($appDetail[0]['Reason']);
		$this->view->lobjchangeprogramForm->Reason->setAttrib('readonly','true');
		$this->view->currentsem = $currentsem = $this->lobjstudentprogramchange->fngetstudentcurrentsem($appDetail[0]['IdStudent']);
		$studentSemestersMain = $this->lobjstudentprogramchange->fngetstudentsemestersmain($currentsem[0]);
		$studentSemestersDetail = $this->lobjstudentprogramchange->fngetstudentsemestersDetail($currentsem[0]);
		if(count($studentSemestersMain)!=0){
			for($i=0;$i<count($studentSemestersMain);$i++){
				$totalsem[$i]['key'] = $studentSemestersMain[$i]['value'];
				$totalsem[$i]['value']= $studentSemestersMain[$i]['value'];
			}
		}
		if(count($studentSemestersDetail)!=0){
			$j=0;
			for($i=count($studentSemestersMain);$i<(count($studentSemestersDetail)+count($studentSemestersMain));$i++){
				$totalsem[$i]['key'] = $studentSemestersDetail[$j]['value'];
				$totalsem[$i]['value']= $studentSemestersDetail[$j]['value'];
				$j++;
			}
		}
		if(isset($totalsem)){
			$this->view->lobjchangeprogramForm->EffectiveSemester->addmultioptions($totalsem);
		}
		$this->view->lobjchangeprogramForm->EffectiveSemester->setValue($appDetail[0]['EffectiveSemester']);
		$this->view->lobjchangeprogramForm->EffectiveSemester->setAttrib('readonly','true');
		$this->view->lobjchangeprogramForm->Date->setValue($appDetail[0]['ApplicationDate']);
		$this->view->lobjchangeprogramForm->AppliedByName->setValue($appDetail[0]['username']);
		$this->view->lobjchangeprogramForm->ApplicationStatusByName->setValue($appDetail[0]['DefinitionDesc']);
		$this->view->lobjchangeprogramForm->CurrentProgram->setValue($appDetail[0]['ProgramName']);
		$this->view->lobjchangeprogramForm->CurrentProgram->setAttrib('readonly','true');
		$currentprogram = $this->lobjstudentprogramchange->fngetstudentcurrentsem($appDetail[0]['IdStudent']);
		$allprogram = $this->lobjstudentprogramchange->fngetallprogram($currentprogram[0]['IdProgram'],$currentprogram[0]['IdScheme'],$currentprogram[0]['Award']);
		if(!empty($allprogram)){
			for($i=0;$i<count($allprogram);$i++){
				$temp[$i]['key'] = $allprogram[$i]['key'];
				$temp[$i]['value']= $allprogram[$i]['name'];
			}
		}
		if(isset($temp)){
			$this->view->lobjchangeprogramForm->NewProgram->addmultioptions($temp);
		}
		$this->view->lobjchangeprogramForm->NewProgram->setValue($appDetail[0]['NewProgram']);
		$this->view->lobjchangeprogramForm->NewProgram->setAttrib('readonly','true');
		$studentlandscapedetails = $this->lobjstudentprogramchange->fngetstudentprogramcourseedit($appDetail[0]['CurrentProgram'],$appDetail[0]['IdStudent']);
		$index = 0;
		$beforecredit = 0;
		$aftercredit = 0;
		for($i=0;$i<count($appDetail);$i++){
			for($j=0;$j<count($studentlandscapedetails);$j++){
				if(isset($studentlandscapedetails[$j]['IdSubject'])){
					if($appDetail[$i]['IdCourse']==$studentlandscapedetails[$j]['IdSubject']){
						$registeredDetails[$index] = $studentlandscapedetails[$j];
						$aftercredit = $aftercredit+$studentlandscapedetails[$j]['CreditHours'];
						unset($studentlandscapedetails[$j]);
						$index++;
					}
				}
			}
		}
		$this->view->studentlandscapedetails = $studentlandscapedetails;
		foreach($this->view->studentlandscapedetails as $studentlandscapedetails){
			$beforecredit = $beforecredit+$studentlandscapedetails['CreditHours'];
		}
		if(isset($registeredDetails)){
			$this->view->registeredDetails = $registeredDetails;
		}
		$this->view->lobjchangeprogramForm->BeforeTotalCreditHours->setValue($beforecredit);
		$this->view->lobjchangeprogramForm->BeforeTotalCreditHours->setAttrib('readonly','true');
		$this->view->lobjchangeprogramForm->AfterTotalCreditHours->setValue($aftercredit);
		$this->view->lobjchangeprogramForm->AfterTotalCreditHours->setAttrib('readonly','true');
		$this->view->ctType = $this->lobjdeftype->fnGetProfilestatus('CT Status');
		$this->view->studentnewlandscapedetails=$studentnewlandscapedetails = $this->lobjstudentprogramchange->fngetstudentnewprogramcourse($appDetail[0]['NewProgram']);
		if(!empty($studentnewlandscapedetails)){
			$newcredit = 0;
			for($i=0;$i<count($studentnewlandscapedetails);$i++){
				$newcredit = $newcredit+$studentnewlandscapedetails[$i]['CreditHours'];
			}
		}
		$this->view->lobjchangeprogramForm->TotalCreditHours->setValue($newcredit);
		$this->view->lobjchangeprogramForm->TotalCreditHours->setAttrib('readonly','true');
	}

	public function changeprogramapplicationapprovalAction(){
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform;
		$ProgramList = $this->lobjplacementtestModel->fnGetProgramMaterList();
		$lobjform->field1->addMultiOptions($ProgramList);

		$larrdefmsstatus= $this->lobjdeftype->fnGetProfilestatus('Change Program');
		$lobjform->field8->addMultiOptions($larrdefmsstatus);

		$larrdefmsstatusprofile= $this->lobjdeftype->fnGetProfilestatus('Student Status');
		$lobjform->field5->addMultiOptions($larrdefmsstatusprofile);

		$larrresult = $this->lobjchangeprogramappModel->getAllActivechangeprogramApplication();
		if (!$this->_getParam('search'))
		unset($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult);
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page', 1); // Paginator instance
		if (isset($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
		}

		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			//if ($this->lobjform->isValid($larrformData)) {
			//  die('here');
			$larrresult = $this->lobjchangeprogramappModel->fngetSearchActivechangeprogramApplication($larrformData); //searching the values for the user
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
			$this->gobjsessionsis->progseniorstudentregistrationpaginatorresult = $larrresult;
			//}
			//die;
		}

		if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
			$this->_redirect($this->baseUrl . '/records/studentprogramchange/changeprogramapplicationapproval');
		}
	}

	public function approvechangeprogramapplicationAction(){
		$newcredit = 0;
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcredittransferForm = $this->lobjcredittransferForm;
		$this->view->lobjchangeprogramForm = $this->lobjchangeprogramForm;
		$IdchangeProgramApp = $this->_getParam('Id');
		$this->view->appDetail=$appDetail = $this->lobjstudentprogramchange->getDetailApplication($IdchangeProgramApp);
		$this->view->appstatus = $appDetail[0]['ApplicationStatus'];

		$studentinfo = $this->lobjstudentprogramchange->fngetstudentinfo($appDetail[0]['IdStudent']);
		$credittransferapp = $this->lobjstudentprogramchange->fngetstudentcredittransferapp($studentinfo[0]['IdStudentRegistration']);
		if(isset($credittransferapp[0]['IdCreditTransfer'])){
			$this->view->credittransferapp = $credittransferapp[0]['IdCreditTransfer'];
		}

		$studentdetails = $this->lobjStudentregistrationModel->fetchStudentDetails($studentinfo[0]['IdApplication']);
		if(isset($studentinfo[0]['IdIntake'])) {
			$studentdetails['intake'] = $studentinfo[0]['IdIntake'];
		}
		if(isset($appDetail[0]['NewProgram'])) {
			$studentdetails['ProgramOfferred'] = $appDetail[0]['NewProgram'];
		}
		unset($studentinfo[0]['IdApplication']);
		// check for Registration ID format. is it manual(0) or auto(1) from tbl_config
		$this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$idUniversity = $this->gobjsessionsis->idUniversity;
		$confdetail = $this->lobjconfig->fnGetInitialConfigDetails($idUniversity);
		//asd($confdetail,false);
		$registrationCodeType = $confdetail['RegistrationCodeType'];
		$registrationFormat = $confdetail['RegistrationIdFormat'];
		if($registrationCodeType=='0') {
			$this->view->makeRegFieldHide = '0';
			$this->view->lobjchangeprogramForm->registrationId->setValue('');
		} else {
			$this->view->makeRegFieldHide = '1';
			$codeGenrateObj = new Cms_CodeGeneration();

			$codeGenrateResult = $codeGenrateObj->RegistrationIdGenration($idUniversity, $confdetail,$studentdetails);
			$codeGenrateResult['larrsepvalues']['UpdDate'] = date("Y-m-d");
			$codeGenrateResult['larrsepvalues']['UpdUser'] = $auth->getIdentity()->iduser;
			$this->view->lobjchangeprogramForm->registrationId->setValue($codeGenrateResult)->setAttribs(array('readonly' => 'true'));
		}
			
		$this->view->lobjchangeprogramForm->Reason->setValue($appDetail[0]['Reason']);
		$this->view->lobjchangeprogramForm->IdStudentreg->setValue($appDetail[0]['IdStudent']);
		$this->view->lobjchangeprogramForm->Reason->setAttrib('readonly','true');
		$this->view->currentsem = $currentsem = $this->lobjstudentprogramchange->fngetstudentcurrentsem($appDetail[0]['IdStudent']);
		$studentSemestersMain = $this->lobjstudentprogramchange->fngetstudentsemestersmain($currentsem[0]);
		$studentSemestersDetail = $this->lobjstudentprogramchange->fngetstudentsemestersDetail($currentsem[0]);
		if(count($studentSemestersMain)!=0){
			for($i=0;$i<count($studentSemestersMain);$i++){
				$totalsem[$i]['key'] = $studentSemestersMain[$i]['value'];
				$totalsem[$i]['value']= $studentSemestersMain[$i]['value'];
			}
		}
		if(count($studentSemestersDetail)!=0){
			$j=0;
			for($i=count($studentSemestersMain);$i<(count($studentSemestersDetail)+count($studentSemestersMain));$i++){
				$totalsem[$i]['key'] = $studentSemestersDetail[$j]['value'];
				$totalsem[$i]['value']= $studentSemestersDetail[$j]['value'];
				$j++;
			}
		}
		if(isset($totalsem)){
			$this->view->lobjchangeprogramForm->EffectiveSemester->addmultioptions($totalsem);
		}
		$this->view->lobjchangeprogramForm->EffectiveSemester->setValue($appDetail[0]['EffectiveSemester']);
		$this->view->lobjchangeprogramForm->EffectiveSemester->setAttrib('readonly','true');
		$this->view->lobjchangeprogramForm->Date->setValue($appDetail[0]['ApplicationDate']);
		$this->view->lobjchangeprogramForm->AppliedByName->setValue($appDetail[0]['username']);
		$this->view->lobjchangeprogramForm->ApplicationStatusByName->setValue($appDetail[0]['DefinitionDesc']);
		$this->view->lobjchangeprogramForm->CurrentProgram->setValue($appDetail[0]['ProgramName']);
		$this->view->lobjchangeprogramForm->CurrentProgram->setAttrib('readonly','true');
		$currentprogram = $this->lobjstudentprogramchange->fngetstudentcurrentsem($appDetail[0]['IdStudent']);
		$allprogram = $this->lobjstudentprogramchange->fngetallprogram($currentprogram[0]['IdProgram'],$currentprogram[0]['IdScheme'],$currentprogram[0]['Award']);
		if(!empty($allprogram)){
			for($i=0;$i<count($allprogram);$i++){
				$temp[$i]['key'] = $allprogram[$i]['key'];
				$temp[$i]['value']= $allprogram[$i]['name'];
			}
		}
		if(isset($temp)){
			$this->view->lobjchangeprogramForm->NewProgram->addmultioptions($temp);
		}
		$this->view->lobjchangeprogramForm->NewProgram->setValue($appDetail[0]['NewProgram']);
		$this->view->lobjchangeprogramForm->NewProgram->setAttrib('readonly','true');
		$studentlandscapedetails = $this->lobjstudentprogramchange->fngetstudentprogramcourseedit($appDetail[0]['CurrentProgram'],$appDetail[0]['IdStudent']);
		$index = 0;
		$beforecredit = 0;
		$aftercredit = 0;
		for($i=0;$i<count($appDetail);$i++){
			for($j=0;$j<count($studentlandscapedetails);$j++){
				if(isset($studentlandscapedetails[$j]['IdSubject'])){
					if($appDetail[$i]['IdCourse']==$studentlandscapedetails[$j]['IdSubject']){
						$registeredDetails[$index] = $studentlandscapedetails[$j];
						$aftercredit = $aftercredit+$studentlandscapedetails[$j]['CreditHours'];
						unset($studentlandscapedetails[$j]);
						$index++;
					}
				}
			}
		}
		$this->view->studentlandscapedetails = $studentlandscapedetails;
		foreach($this->view->studentlandscapedetails as $studentlandscapedetails){
			$beforecredit = $beforecredit+$studentlandscapedetails['CreditHours'];
		}
		if(isset($registeredDetails)){
			$this->view->registeredDetails = $registeredDetails;
		}
		$this->view->lobjchangeprogramForm->BeforeTotalCreditHours->setValue($beforecredit);
		$this->view->lobjchangeprogramForm->BeforeTotalCreditHours->setAttrib('readonly','true');
		$this->view->lobjchangeprogramForm->AfterTotalCreditHours->setValue($aftercredit);
		$this->view->lobjchangeprogramForm->AfterTotalCreditHours->setAttrib('readonly','true');
		$this->view->ctType = $this->lobjdeftype->fnGetProfilestatus('CT Status');
		$this->view->studentnewlandscapedetails=$studentnewlandscapedetails = $this->lobjstudentprogramchange->fngetstudentnewprogramcourse($appDetail[0]['NewProgram']);

		if(!isset($studentnewlandscapedetails[0]['IdLandscape'])) {
			echo "
							<script type='text/javascript'>
							var x=window.confirm('This application cannot be processed as no landscape is active for ".$appDetail[0]['NewProgramName']."');
		
							if(x){
							parent.location ='".$this->view->baseUrl()."/records/studentprogramchange/changeprogramapplicationapproval';
							}
							else{
							parent.location ='".$this->view->baseUrl()."/records/studentprogramchange/changeprogramapplicationapproval';
							}
							</script>";
			exit;
		}
			
		if(!empty($studentnewlandscapedetails)){
			$newcredit = 0;
			for($i=0;$i<count($studentnewlandscapedetails);$i++){
				$newcredit = $newcredit+$studentnewlandscapedetails[$i]['CreditHours'];
			}
		}
		$this->view->lobjchangeprogramForm->TotalCreditHours->setValue($newcredit);
		$this->view->lobjchangeprogramForm->TotalCreditHours->setAttrib('readonly','true');
		$date = date("Y-m-d");
		$this->view->lobjchangeprogramForm->ApproveDate->setValue($date);

		$this->view->lobjchangeprogramForm->ApproveBy->setValue($auth->getIdentity()->iduser);
		$authUserName = $this->lobjsubjectmasterModel->getUserName($auth->getIdentity()->iduser);
		$this->view->lobjchangeprogramForm->ApproveByName->setValue($authUserName['loginName']);

		if ($this->getRequest()->isPost() && $this->_request->getPost ( 'Approve' )) {
			$formData = $this->getRequest()->getPost();
			$codeGenrateResult = $codeGenrateObj->RegistrationIdGenration($idUniversity, $confdetail,$studentdetails);
			$codeGenrateResult['larrsepvalues']['UpdDate'] = date("Y-m-d");
			$codeGenrateResult['larrsepvalues']['UpdUser'] = $auth->getIdentity()->iduser;

			$studentid = $studentinfo[0]['IdStudentRegistration'];

			unset($studentinfo[0]['IdStudentRegistration']);
			$studentinfo[0]['OldIdStudentRegistration'] = $formData['IdStudentreg'];
			if($this->view->makeRegFieldHide == "1"){
				$studentinfo[0]['registrationId'] = $codeGenrateResult['IdStudentRegistrationFormatted'];
				$studentinfo[0]['IdStudentRegistrationFormatted'] = $codeGenrateResult['IdStudentRegistrationFormatted'];
				$studentinfo[0]['IdFormat'] = $codeGenrateResult['IdFormat'];
			}
			else if($this->view->makeRegFieldHide == "0"){
				$studentinfo[0]['registrationId'] = $formData['registrationId'];
			}
			$studentinfo[0]['profileStatus'] = 92;
			$studentinfo[0]['IdLandscape'] = $studentnewlandscapedetails[0]['IdLandscape'];
			$studentinfo[0]['IdProgram'] = $appDetail[0]['NewProgram'];
			$studentinfo[0]['UpdDate'] = date("Y-m-d");
			$studentinfo[0]['UpdUser'] = $auth->getIdentity()->iduser;
			$studentinfo[0]['ApplicationDate'] = date('Y-m-d H:i:s');

			$newstudentId = $this->lobjstudentprogramchange->fncreatenewstudent($studentinfo[0]);
			//Insert data into student generated id table
			$codeGenrateResult['larrsepvalues']['IdStudentRegistration'] = $newstudentId;
			$this->lobjstudentgeneratedmodel->fninsert($codeGenrateResult['larrsepvalues']);

			$data['profileStatus'] = 251;
			$oldsem['studentsemesterstatus'] = 252;
			$this->lobjStudentregistrationModel->updateStudentProfile($studentid,$data);
			$statusArray1['profileStatus'] = 92;
			$statusArray1['IdStudentRegistration'] = $newstudentId;
			$statusArray1['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
			$statusArray1['UpdUser'] = $auth->getIdentity()->iduser;
			$statusArray1['UpdDate'] = date("Y-m-d");
			$this->lobjstudentHistoryModel->addStudentProfileHistory($statusArray1);
			$upddata['profileStatus'] = 251;
			$this->lobjStudentregistrationModel->updateStudentProfile($studentid,$upddata);
			$statusArray['profileStatus'] = 251;
			$statusArray['IdStudentRegistration'] = $studentid;
			$statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
			$statusArray['UpdUser'] = $auth->getIdentity()->iduser;
			$statusArray['UpdDate'] = date("Y-m-d");
			$this->lobjstudentHistoryModel->addStudentProfileHistory($statusArray);
			$this->lobjStudentregistrationModel->updateStudentoldsem($currentsem[0]['idstudentsemsterstatus'],$oldsem);
			$effectivesem = $this->lobjsubjectmasterModel->getSemesterid($appDetail[0]['EffectiveSemester']);
			$semdata['IdStudentRegistration'] = $newstudentId;
			if(isset($effectivesem['IdSemesterMaster'])){
				$semdata['IdSemesterMain'] = $effectivesem['IdSemesterMaster'];
			}
			else if(isset($effectivesem['IdSemester'])){
				$semdata['idSemester'] = $effectivesem['IdSemester'];
			}
			$semdata['studentsemesterstatus'] = 130;
			$semdata['UpdUser'] = $auth->getIdentity()->iduser;
			$semdata['UpdDate'] = date("Y-m-d");

			$this->lobjstudentprogramchange->fnregistersemester($newstudentId,$semdata);
			$credittransfercourse = $this->lobjstudentprogramchange->fngetcredittransfercourse($appDetail[0]['IdChangeProgramApplication'],$studentid);
			$idsem = $this->lobjsubjectmasterModel->getSemesterid("Dummy".date('Y'));
			if($idsem == ''){
				$dummysemData['SemesterMainName'] = "Dummy".date('Y');
				$dummysemData['SemesterMainDefaultLanguage'] = "Dummy".date('Y');
				$dummysemData['SemesterMainCode'] = "Dummy".date('Y');
				$dummysemData['IsCountable'] = 1;
				$dummysemData['SemesterMainStatus'] = 130;
				$dummysemData['SemesterMainStartDate'] = date('Y-m-d');
				$dummysemData['SemesterMainEndDate'] = date('Y-m-d');
				$dummysemData['Scheme'] = 1;
				$dummysemData['DummyStatus'] = 1;
				$dummysemData['UpdUser'] = $auth->getIdentity()->iduser;
				$dummysemData['UpdDate'] = date("Y-m-d");
				$idsem = $this->lobjstudentprogramchange->fncreatesemester($dummysemData);
			}
			for($i=0;$i<count($credittransfercourse);$i++){
				$subjectdata['IdStudentRegistration'] = $newstudentId;
				$subjectdata['IdSubject'] = $credittransfercourse[$i]['IdCourse'];
				$subjectdata['IdGrade'] = "CT";
				$subjectdata['IdSemesterMain'] = $idsem['IdSemesterMaster'];
				$subjectdata['UpdUser'] = $auth->getIdentity()->iduser;
				$subjectdata['UpdDate'] = date("Y-m-d");
				$this->lobjstudentprogramchange->fnregistersubjects($subjectdata);
			}
			for($i=0;$i<count($studentnewlandscapedetails);$i++){
				if($studentnewlandscapedetails[$i]['IdSemester'] == "1"){
					for($j=0;$j<count($credittransfercourse);$j++){
						if($studentnewlandscapedetails[$i]['IdSubject'] != $credittransfercourse[$j]['IdCourse']){
							$newsubjectdata['IdStudentRegistration'] = $newstudentId;
							$newsubjectdata['IdSubject'] = $studentnewlandscapedetails[$i]['IdSubject'];
							if(isset($effectivesem['IdSemesterMaster'])){
								$newsubjectdata['IdSemesterMain'] = $effectivesem['IdSemesterMaster'];
							}
							else if(isset($effectivesem['IdSemester'])){
								$newsubjectdata['IdSemesterDetails'] = $effectivesem['IdSemester'];
							}
							$newsubjectdata['UpdUser'] = $auth->getIdentity()->iduser;
							$newsubjectdata['UpdDate'] = date("Y-m-d");
							$this->lobjstudentprogramchange->fnregistersubjects($newsubjectdata);
						}
					}
				}
			}

			$approvedata['Remarks'] = $formData['Remarks'];
			$approvedata['ApprovedBy'] = $formData['ApproveBy'];
			$approvedata['ApprovedDate'] = $formData['ApproveDate'];
			$approvedata['ApplicationStatus'] = 238;
			$this->lobjstudentprogramchange->fnupdateapplication($approvedata,$IdchangeProgramApp);
			$this->_redirect($this->baseUrl . '/records/studentprogramchange/changeprogramapplicationapproval');
		}
		else if($this->getRequest()->isPost() && $this->_request->getPost ( 'Disapprove' )){
			$formData = $this->getRequest()->getPost();
			$approvedata['Remarks'] = $formData['Remarks'];
			$approvedata['ApprovedBy'] = $formData['ApproveBy'];
			$approvedata['ApprovedDate'] = $formData['ApproveDate'];
			$approvedata['ApplicationStatus'] = 239;
			$this->lobjstudentprogramchange->fnupdateapplication($approvedata,$IdchangeProgramApp);
			$this->_redirect($this->baseUrl . '/records/studentprogramchange/changeprogramapplicationapproval');

			//die('Disapprove');
		}
	}

	public function studentprogramAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('IdStudent');
		$program = $this->lobjstudentprogramchange->fngetprogram($id);
		echo Zend_Json_Encoder::encode($program);
	}

	public function studentsemesterAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('IdStudent');
		$currentsem = $this->lobjstudentprogramchange->fngetstudentcurrentsem($id);
		$studentSemestersMain = $this->lobjstudentprogramchange->fngetstudentsemestersmain($currentsem[0]);
		$studentSemestersDetail = $this->lobjstudentprogramchange->fngetstudentsemestersDetail($currentsem[0]);
		if(count($studentSemestersMain)!=0){
			for($i=0;$i<count($studentSemestersMain);$i++){
				$totalsem[$i]['key'] = $studentSemestersMain[$i]['value'];
				$totalsem[$i]['name']= $studentSemestersMain[$i]['value'];
			}
		}
		if(count($studentSemestersDetail)!=0){
			$j=0;
			for($i=count($studentSemestersMain);$i<(count($studentSemestersDetail)+count($studentSemestersMain));$i++){
				$totalsem[$i]['key'] = $studentSemestersDetail[$j]['value'];
				$totalsem[$i]['name']= $studentSemestersDetail[$j]['value'];
				$j++;
			}
		}
		if(!(isset($totalsem))){
			$totalsem[0]['key'] = "";
			$totalsem[0]['name']= "";
		}
		echo Zend_Json_Encoder::encode($totalsem);

	}

	public function getallprogramAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('IdStudent');
		$currentprogram = $this->lobjstudentprogramchange->fngetstudentcurrentsem($id);
		$allprogram = $this->lobjstudentprogramchange->fngetallprogram($currentprogram[0]['IdProgram'],$currentprogram[0]['IdScheme'],$currentprogram[0]['Award']);
		echo Zend_Json_Encoder::encode($allprogram);
	}

	public function getallcoursesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$currentProgram = $this->_getParam('currentProgram');
		$StudentId = $this->_getParam('StudentId');
		$studentlandscapedetails = $this->lobjstudentprogramchange->fngetstudentprogramcourse($currentProgram,$StudentId);
		echo Zend_Json_Encoder::encode($studentlandscapedetails);
	}

	public function getnewprogramcoursesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$NewProgram = $this->_getParam('NewProgram');
		$studentlandscapedetails = $this->lobjstudentprogramchange->fngetstudentnewprogramcourse($NewProgram);
		echo Zend_Json_Encoder::encode($studentlandscapedetails);
			
	}

}

?>
