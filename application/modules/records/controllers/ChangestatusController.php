<?php
class Records_ChangestatusController extends Base_Base { //Controller for the User Module

	private $lobjChangestatusForm;
	private $lobjChangestatus;
	private $lobjUser;
	private $_gobjlog;
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();	
	}
	public function fnsetObj(){
		
		$this->lobjChangestatusForm = new Records_Form_Changestatus();
		$this->lobjChangestatus = new Records_Model_DbTable_Changestatus();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		
		$lobjApplicantNameList = $this->lobjChangestatus->fnGetApplicantNameList();
		$lobjform->field5->addMultiOptions($lobjApplicantNameList);
		
		$lobjFacultyNameList = $this->lobjChangestatus->fnGetFacultyNameList();
		$lobjform->field1->addMultiOptions($lobjFacultyNameList);
		
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); 
		$ProgramList=$lobjplacementtest->fnGetProgramMaterList();
		$lobjform->field8->addMultiOptions($ProgramList);
		
		 if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->changestatuspaginatorresult);
		
		$larrresult = $this->lobjChangestatus->fngetStudentprofileDtls(); //get user details
		//echo "<pre>";
		//print_r($larrresult);
		//die();
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->changestatuspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->changestatuspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjChangestatus ->fnSearchStudentApplication( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->changestatuspaginatorresult = $larrresult;
				
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/records/changestatus/index');
		}


	}

	public function changestatuslistAction() { //Action for the updation and view of the  details
		
		$this->view->lobjChangestatusForm = $this->lobjChangestatusForm;
		 
		$lintIdApplication = ( int ) $this->_getParam ( 'id' );
		$this->view->IdApplication = $lintIdApplication;
		
		$this->view->lobjChangestatusForm->IdApplication->setValue ( $lintIdApplication );
		
		$larrresult = $this->lobjChangestatus->fnViewStudentAppnDetails($lintIdApplication); 
		$this->view->Matrix= $larrresult ['registrationId'];
		//echo "<pre>";
		//print_r($larrresult);die();
		
		$this->view->StudentName = $larrresult['FName'].' '.$larrresult['MName'].' '.$larrresult['LName'];
		$this->view->ICNumber = $larrresult['ICNumber'];
		$this->view->ProgramName = $larrresult['ProgramName'];
		$this->view->CollegeName = $larrresult['CollegeName'];
		$this->view->ApplicationDate = date('d-m-Y H:i',strtotime($larrresult['ApplicationDate']));
		
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjChangestatusForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjChangestatusForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		
		$larrChangestatus = $this->lobjChangestatus->fnViewChangestatus( $lintIdApplication );
		if($larrChangestatus)
		$this->view->lobjChangestatusForm->populate($larrChangestatus);	
		
		$lobjStudentStatusList = $this->lobjChangestatus->fnGetStudentStatusList();
		$this->view->lobjChangestatusForm->Status->addMultiOptions($lobjStudentStatusList);
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjChangestatusForm->isValid ( $larrformData )) {
					$this->lobjChangestatus->fnupdateStudentAppn($larrformData,$lintIdApplication);
					
					if ($larrformData ['IdApplication'] == $larrChangestatus['IdApplication']) {
						$this->lobjChangestatus->fnupdateChangeStatus( $larrformData );
					} else {
						$this->lobjChangestatus->fnaddChangeStatus($larrformData );
					}
					
					
					// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Change student Status Add Id=' . $lintIdApplication,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				
					$this->_redirect( $this->baseUrl . '/records/Changestatus/index');
				}
			}
		}
		$this->view->lobjChangestatusForm = $this->lobjChangestatusForm;
	}
}