<?php

class Records_SemesterchangestatusController extends Base_Base { //Controller for the User Module

    private $lobjChangestatusForm;
    private $lobjChangeStatus;
    private $lobjUser;
    private $lobjChangeSemesterStatus;
    private $_gobjlog;
    private $lobjdeftype;
    private $lobjcredittransferForm;
    private $lobjchangestatusForm;
    private $lobjsubjectmasterModel;
    private $lobjdeferreasonModel;
    private $lobjquitreasonModel;
    private $lobjstudentprogramchange;
    private $lobjchangestatusModel;
    private $changestatusapplicationForm;
    private $lobjsemesterstatusModel;
    private $lobjstudentregModel;
    private $lobjrecordconfigModel;
    private $lobjstudentRegistrationModel;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->fnsetObj();
    }

    public function fnsetObj() {
        $this->lobjdeftype = new App_Model_Definitiontype();
        $this->lobjChangestatusForm = new Records_Form_Changestatus();
        $this->lobjChangestatus = new Records_Model_DbTable_Changestatus();
        $this->lobjUser = new GeneralSetup_Model_DbTable_User();
        $this->lobjChangeSemesterStatus = new Records_Model_DbTable_SemesterChangestatus();
        $this->lobjcredittransferForm = new Records_Form_Credittransferapp();
        $this->lobjchangestatusForm = new Records_Form_Changestatus();
        $this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
        $this->lobjdeferreasonModel = new Records_Model_DbTable_Recorddeferreason();
        $this->lobjquitreasonModel = new Records_Model_DbTable_Recordquitreason();
        $this->lobjstudentprogramchange = new Records_Model_DbTable_Studentprogramchange();
        $this->lobjchangestatusModel = new Records_Model_DbTable_Changestatusapplication();
        $this->changestatusapplicationForm = new Records_Form_Changestatusapprove();
        $this->lobjsemesterstatusModel = new Registration_Model_DbTable_Semesterstatus();
        $this->lobjstudentregModel = new Registration_Model_DbTable_Studentregistration();
        $this->lobjrecordconfigModel = new Records_Model_DbTable_Recordconfig();
        $this->lobjstudentRegistrationModel = new Registration_Model_DbTable_Studenthistory();
    }

    public function indexAction() { // action for search and view
    	
    	$this->view->title = $this->view->translate('Change Status');
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
    	
        $lobjform = new Records_Form_ChangeStatusSearchForm (); //intialize search lobjuserForm
      
       
            if (!$this->_getParam('search')) { 
            	unset($this->gobjsessionsis->changestatuspaginatorresult); 
            }
            
            $larrresult = $this->lobjchangestatusModel->fnSearchCSApplication();
                       
            $lintpagecount = $this->gintPageCount;
            $lobjPaginator = new App_Model_Common(); // Definitiontype model
            $lintpage = $this->_getParam('page', 1); // Paginator instance

            if (isset($this->gobjsessionsis->changestatuspaginatorresult)) {
                $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->changestatuspaginatorresult, $lintpage, $lintpagecount);
            } else {
                $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
            }
            
            if ($this->_request->isPost() && $this->_request->getPost('Search')) {                
                	$larrformData = $this->_request->getPost();
                	$lobjform->populate($larrformData);
                    $larrresult = $this->lobjchangestatusModel->fnSearchCSApplication($larrformData);
                    $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
                    $this->gobjsessionsis->changestatuspaginatorresult = $larrresult;               
            }
            
            if ($this->_request->isPost() && $this->_request->getPost('Clear')) {                
                $this->_redirect($this->baseUrl . '/records/semesterchangestatus/index');
            }
            
            $this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
            
    }

    public function addchangestatusapplicationAction() {
    	
    	$this->view->title = $this->view->translate('Apply Change Status');
    	
        $this->view->form = new Records_Form_SearchStudentChangeStatus;
                
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$changeStatusDB = new Records_Model_DbTable_Changestatusapplication();
		    $student_list = $changeStatusDB->getStudent($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(50);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));			
			$this->view->paginator = $paginator;
		}else{			 
		   
		    $changeStatusDB = new Records_Model_DbTable_Changestatusapplication();
		    $student_list = $changeStatusDB->getStudent();
		    
		    
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(50);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			$this->view->paginator = $paginator;
		   
		}
		
        /*if ($this->_request->isPost()) { 
        	
        	$formData = $this->_request->getPost();
        	
        	echo '<pre>';
        	print_r($formData);
        	
	        
	        $student = $changeStatusDB->getStudent($formData);
	        $this->view->student = $student;
	        
	       
	        
	        $IdUniversity = $this->gobjsessionsis->idUniversity;
        	$univconfigList = $this->lobjrecordconfigModel->fnfetchConfiguration($IdUniversity);
       
	        $this->view->lobjchangestatusForm->ApplicationDate->setValue(date('Y-m-d'));
	        $ldtsystemDate = date('Y-m-d H:i:s');
	        $this->view->lobjchangestatusForm->UpdDate->setValue($ldtsystemDate);
	        $auth = Zend_Auth::getInstance();
	        $this->view->lobjchangestatusForm->UpdUser->setValue($auth->getIdentity()->iduser);
	        
	        $authUserName = $this->lobjsubjectmasterModel->getUserName($auth->getIdentity()->iduser);
	        $this->view->lobjchangestatusForm->AppliedByName->setValue($authUserName['loginName']);
	        $this->view->lobjchangestatusForm->AppliedBy->setValue($auth->getIdentity()->iduser);
	        $this->view->lobjchangestatusForm->ApplicationStatusByName->setValue('ENTRY');
	        $this->view->lobjchangestatusForm->ApplicationStatus->setValue(240);
	        
        }*/

        /*if ($this->_request->isPost()) {           
            $formData = $this->_request->getPost();
            $listCTapplication = $this->lobjchangestatusModel->getListCSApplication();
            $count = 0;
            if (!empty($listCTapplication)) {
                $count = count($listCTapplication);
            }
            $formData['CSApplicationCode'] = 'CSA' . ($count + 1);
            $formData['AppliedBy'] = $auth->getIdentity()->iduser;
            $formData['Remarks'] = '';
            $formData['ApprovedBy'] = '';
            $formData['DateApproved'] = '';
            if($univconfigList['ApprovalChangeStatus'] == 0){
                $formData['ApplicationStatus'] = 241;
                $formData['Remarks'] = 'This Application is Automatic Approved';
            }
            unset($formData['Save']);
            unset($formData['IdStudentScheme']);
            unset($formData['AppliedByName']);
            unset($formData['ApplicationStatusByName']);
            unset($formData['IdStudentProgram']);
            
            $this->lobjchangestatusModel->saveCSApplication($formData);
            
           

            if($univconfigList['ApprovalChangeStatus'] == 0){
                $data = array();
                $data['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                $effectivesem = $this->lobjsubjectmasterModel->getSemesterid($formData['IdEffectiveSemester']);
                if(isset($effectivesem['IdSemesterMaster'])){
                    $data['IdSemesterMain'] = $effectivesem['IdSemesterMaster'];
                }else{
                    $data['idSemester'] = $effectivesem['IdSemester'];
                }
                if($formData['IdApplyingFor'] == 248){
                    $data['studentsemesterstatus'] =  250;
                }else{
                     $data['studentsemesterstatus'] =  249;
                }
                $this->lobjsemesterstatusModel->addSemesterStatus($data);
                //if($formData['IdApplyingFor'] == 249){
                    $statusArray['profileStatus'] = 249;
                    $this->lobjstudentregModel->updateStudentProfile($formData['IdStudentRegistration'],$statusArray);
                    $statusArray['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                    $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
                    $statusArray['UpdUser'] = $formData['UpdUser'];
                    $statusArray['UpdDate'] = $formData['UpdDate'];
                    $this->lobjstudentRegistrationModel->addStudentProfileHistory($statusArray);
                //}
            } 
            $this->_redirect($this->baseUrl . '/records/semesterchangestatus/index');
        }*/
    }

    function addAction(){
    	
    	//$this->_helper->layout->disableLayout();
    	
    	$this->view->title = $this->view->translate('Apply Change Status');
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$this->view->form = new Records_Form_SearchStudentChangeStatus;
        
    	$IdStudentRegistration = $this->_getParam('id',1);
    	$this->view->IdStudentRegistration = $IdStudentRegistration;
    	     	 
        
        if ($this->_request->isPost()) { 
        	          
            $formData = $this->_request->getPost();

            $CSApplicationCode = $this->generateChangeStatusID();

            $data['IdStudentRegistration'] = $formData['IdStudentRegistration'];
            $data['CSApplicationCode']     = $CSApplicationCode;
            $data['Reason'] = $formData['Reason'];
            $data['ValidReason'] = $formData['ValidReason'];
            $data['EffectiveSemesterFrom'] = $formData['EffectiveSemesterFromName'];;
            $data['IdApplyingFor'] = $formData['IdApplyingFor'];
            $data['Others'] = $formData['others'];
            $data['ApplicationStatus'] = 240; //entry
            $data['ApplicationDate'] = date('Y-m-d');
            $data['AppliedBy'] = $auth->getIdentity()->iduser;
            $data['AppliedByRole'] = $auth->getIdentity()->IdRole;
            $data['UpdUser']   = $auth->getIdentity()->iduser;
            $data['UpdDate']   = date('Y-m-d H:i:s');

            $id = $this->lobjchangestatusModel->saveCSApplication($data);

            //upload file
            $folder = '/changestatus/'.$id;
            $dir = DOCUMENT_PATH.$folder;

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false )
                {
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();

            if ($files){
                foreach ($files as $file=>$info){
                    if ($adapter->isValid($file)){
                        //var_dump($info); exit;
                        $fileOriName = $info['name'];
                        $fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
                        $filepath = $info['destination'].'/'.$fileRename;

                        if ($adapter->isUploaded($file)){
                            $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
                            if ($adapter->receive($file)){
                                $uploadData = array(
                                    'upl_changestatus_id'=>$id,
                                    'upl_filename'=>$fileOriName,
                                    'upl_filerename'=>$fileRename,
                                    'upl_filelocation'=>$folder.'/'.$fileRename,
                                    'upl_filesize'=>$info['size'],
                                    'upl_mimetype'=>$info['type'],
                                    'upl_upddate'=>date('Y-m-d H:i:s'),
                                    'upl_upduser'=>$auth->getIdentity()->iduser
                                );
                                //var_dump($uploadData); exit;
                                $this->lobjchangestatusModel->uploadfile($uploadData);
                            }
                        }
                    }
                }
            }

            $this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
            $this->_redirect($this->baseUrl . '/records/semesterchangestatus/edit/id/'.$id);
	            
        }else{        	
            $this->view->lobjchangestatusForm = $this->lobjchangestatusForm;
        }
       
    }
    
    function editAction(){
    	
    	//$this->_helper->layout->disableLayout();
    	$this->view->title = $this->view->translate('Edit Change Status');
    	
    	$auth = Zend_Auth::getInstance();
    	
    	
    	if ($this->_request->isPost()) { 
        	          
	            $formData = $this->_request->getPost();
	            	    	            
             	if(isset($formData['ValidReason'])){ 
             		 $data['ValidReason'] = $formData['ValidReason'];	              	
             	}else{
             		 $data['ValidReason'] = 0;
             	}
             	             	
	            if(isset($formData['EffectiveSemesterTo'])){          
	            	$data['EffectiveSemesterTo'] = $formData['EffectiveSemesterTo'];
	            }
	            
	            $data['Reason'] = $formData['Reason'];	
	            $data['Others'] = $formData['others'];
	            $data['IdApplyingFor'] = $formData['IdApplyingFor'];
	            $data['UpdUser']   = $auth->getIdentity()->iduser; 
	            $data['UpdDate']   = date('Y-m-d H:i:s');	              
	           
	            $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$data);

                //upload file
                $folder = '/changestatus/'.$formData['IdChangeStatusApplication'];
                $dir = DOCUMENT_PATH.$folder;

                if ( !is_dir($dir) ){
                    if ( mkdir_p($dir) === false )
                    {
                        throw new Exception('Cannot create attachment folder ('.$dir.')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
                $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();

                if ($files){
                    foreach ($files as $file=>$info){
                        if ($adapter->isValid($file)){
                            //var_dump($info); exit;
                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'].'/'.$fileRename;

                            if ($adapter->isUploaded($file)){
                                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
                                if ($adapter->receive($file)){
                                    $uploadData = array(
                                        'upl_changestatus_id'=>$formData['IdChangeStatusApplication'],
                                        'upl_filename'=>$fileOriName,
                                        'upl_filerename'=>$fileRename,
                                        'upl_filelocation'=>$folder.'/'.$fileRename,
                                        'upl_filesize'=>$info['size'],
                                        'upl_mimetype'=>$info['type'],
                                        'upl_upddate'=>date('Y-m-d H:i:s'),
                                        'upl_upduser'=>$auth->getIdentity()->iduser
                                    );
                                    //var_dump($uploadData); exit;
                                    $this->lobjchangestatusModel->uploadfile($uploadData);
                                }
                            }
                        }
                    }
                }
	          
	            $this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
	            $this->_redirect($this->baseUrl . '/records/semesterchangestatus/edit/id/'.$formData['IdChangeStatusApplication']);
	            
        }else{  
        	
		    	$IdChangeStatusApplication = $this->_getParam('id');
		    	$this->view->IdChangeStatusApplication = $IdChangeStatusApplication;
		    	    	
		    	$changeStatusDB = new Records_Model_DbTable_Changestatusapplication();
		    	$student_info = $changeStatusDB->fnGetApplicationDetail($IdChangeStatusApplication);    	     
		        $this->view->student = $student_info;	
		        
		        //get student current semester status
		        $changeStatusDB = new Records_Model_DbTable_Studentprogramchange();
		        $this->view->sem_status = $changeStatusDB->getStudentSemesterStatus($student_info['IdStudentRegistration'],$student_info['EffectiveSemesterFrom']);
		        
		        //get semester       
		        $semester_from = $this->lobjstudentprogramchange->getProgramSemester($student_info['EffectiveSemesterFrom']);
		        $student_info['EffectiveSemesterFromName'] = $semester_from['SemesterMainName'];

                $upl = $this->lobjchangestatusModel->viewUpl($IdChangeStatusApplication);

                $this->view->upl = $upl;
                $this->view->lobjchangestatusForm = new Records_Form_Changestatus(array('edit'=>true));
		        $this->view->lobjchangestatusForm->populate($student_info);
        }
        
       
    }

    public function deleteuplAction(){
        $id = $this->_getParam('id');
        $csid = $this->_getParam('csid');

        $this->lobjchangestatusModel->deleteUpl($id);

        //redirect
        $this->gobjsessionsis->flash = array('message' => 'File has been deleted', 'type' => 'success');
        $this->_redirect($this->baseUrl . '/records/semesterchangestatus/edit/id/'.$csid);
    }
    
    function cancelAction(){
    
    	$auth = Zend_Auth::getInstance();
    	
    	$IdChangeStatusApplication = $this->_getParam('id');
		    	
    	$data = array(  'ApplicationStatus'=>833,
    					'cancelBy'=>$auth->getIdentity()->iduser,
    					'cancelDate'=>date('Y-m-d H:i:s'),
    					'cancelByRole'=>$auth->getIdentity()->IdRole );
    	
    	$changeStatusDB = new Records_Model_DbTable_Changestatusapplication();
    	$changeStatusDB->fnUpdateApplicationstatus($IdChangeStatusApplication,$data);
    
    	$this->gobjsessionsis->flash = array('message' => 'Data has been cancel', 'type' => 'success');
    	$this->_redirect($this->baseUrl . '/records/semesterchangestatus/index');
	            
    }
     
    
    function viewAction(){
    
    	$this->_helper->layout->disableLayout();
    	
    	$IdChangeStatusApplication = $this->_getParam('id');
		    	
    	//$IdChangeStatusApplication = $this->_getParam('id');
    	$this->view->IdChangeStatusApplication = $IdChangeStatusApplication;
    	    	
    	$changeStatusDB = new Records_Model_DbTable_Changestatusapplication();
    	$student_info = $changeStatusDB->fnGetApplicationDetail($IdChangeStatusApplication);    	     
            //var_dump($student_info); exit;
       //get student current semester status
        $changeStatusDB = new Records_Model_DbTable_Studentprogramchange();
        $this->view->sem_status = $changeStatusDB->getStudentSemesterStatus($student_info['IdStudentRegistration'],$student_info['EffectiveSemesterFrom']);
        
        //get semester       
        $semester_from = $this->lobjstudentprogramchange->getProgramSemester($student_info['EffectiveSemesterFrom']);
        $student_info['EffectiveSemesterFromName'] = $semester_from['SemesterMainName']; 
		       
        /*$reason = '';
        if($student_info['IdApplyingFor'] == 250){
           $resonsList = $this->lobjdeferreasonModel->getDeferDescription($student_info['Reason']);
           $reason = $resonsList['ReasonDefer'];
        }else{
           $resonsList = $this->lobjquitreasonModel->getQuitDescription($student_info['Reason']);
           $reason = $resonsList['Reason'];
        }*/
        
        $resonsList = $this->lobjquitreasonModel->getDefination($student_info['Reason']);
        
        $this->view->student = $student_info;	
        $this->view->reasons = $resonsList['name'];
	            
    }
     
    
    function fngetreasonsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $Idapplyingfor = $this->_getParam('Idapplyingfor');
        //$resonsList = '';
        $IdUniversity = $this->gobjsessionsis->idUniversity;
        
        //250 defer 835:quit 836:quit:deceased
        if ($Idapplyingfor == 250){
            $typeId = 169;
        }else if ($Idapplyingfor == 835){
            $typeId = 170;
        }else{
            $typeId = 171;
        }
        
        $resonsList = $this->lobjdeferreasonModel->getReason($typeId);
        
        /*if ($Idapplyingfor == 250) {
            $resonsList = $this->lobjdeferreasonModel->getDeferReasons($IdUniversity);
        } else {
            $resonsList = $this->lobjquitreasonModel->getQuitReasons($IdUniversity);
        }*/
        echo Zend_Json_Encoder::encode($resonsList);
    }

    public function fngeteffectivesemesterAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $IdStudentRegistration = $this->_getParam('IdStudentRegistration');
        $currentsem = $this->lobjstudentprogramchange->fngetstudentcurrentsem($IdStudentRegistration);
       
        $studentSemestersMain = $this->lobjstudentprogramchange->fngetstudentsemestersmain($currentsem[0]);
        $studentSemestersDetail = $this->lobjstudentprogramchange->fngetstudentsemestersDetail($currentsem[0]);
        $totalsem = array();
        if (count($studentSemestersMain) != 0) {
            for ($i = 0; $i < count($studentSemestersMain); $i++) {
                $totalsem[$i]['key'] = $studentSemestersMain[$i]['value'];
                $totalsem[$i]['name'] = $studentSemestersMain[$i]['value'];
            }
        }
        if (count($studentSemestersDetail) != 0) {
            $j = 0;
            for ($i = count($studentSemestersMain); $i < (count($studentSemestersDetail) + count($studentSemestersMain)); $i++) {
                $totalsem[$i]['key'] = $studentSemestersDetail[$j]['value'];
                $totalsem[$i]['name'] = $studentSemestersDetail[$j]['value'];
                $j++;
            }
        }
        $newsemList = array();
        
//        foreach($totalsem as $sem){
//                $ret = $this->lobjchangestatusModel->checkeffectivesemester($IdStudentRegistration,$sem['name']);
//                if(empty($ret)){
//                    $newsemList[] = $sem;
//                }
//        }
        echo Zend_Json_Encoder::encode($totalsem);
    }


    public function changestatusapplicationapprovalAction(){
    	
    	$this->view->title = $this->view->translate('Change Status Approval');
    	
    	$lobjform = new Records_Form_ChangeStatusSearchForm (); //intialize search lobjuserForm

        	if (!$this->_getParam('search')){
        		unset($this->gobjsessionsis->changestatuspaginatorresult);
        	}
        	
            $larrresult = $this->lobjchangestatusModel->fnSearchCSApplication();
           
            $lintpagecount = $this->gintPageCount;
            $lobjPaginator = new App_Model_Common(); // Definitiontype model
            $lintpage = $this->_getParam('page', 1); // Paginator instance

            if (isset($this->gobjsessionsis->changestatuspaginatorresult)) {
                $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->changestatuspaginatorresult, $lintpage, $lintpagecount);
            } else {
                $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
            }
            
            if ($this->_request->isPost() && $this->_request->getPost('Search')) {
                	$larrformData = $this->_request->getPost();
					$lobjform->populate($larrformData);
                    $larrresult = $this->lobjchangestatusModel->fnSearchCSApplication($larrformData);
                    $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
                    $this->gobjsessionsis->changestatuspaginatorresult = $larrresult;
               
            }
            
            if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
                  $this->_redirect($this->baseUrl . '/records/semesterchangestatus/changestatusapplicationapproval');
            }
            
             $this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
    }

 public function approvalAction(){        
    
    	$this->view->title = $this->view->translate('Change Status Approval');
    	
    	$auth = Zend_Auth::getInstance();

    	$this->view->role = $auth->getIdentity()->IdRole;

        if ($this->_request->isPost()) {
        	
            $cmsStatus = new Cms_Status();
            $formData = $this->_request->getPost(); 
            $lintIdApplication = ( int ) $this->_getParam ('id');
            $larrresult = $this->lobjchangestatusModel->fnGetApplicationDetail($lintIdApplication);

            if($formData['IdApplyingFor'] == 835 || $formData['IdApplyingFor'] == 836) {
                $modelForSemester = new Records_Model_DbTable_ChangeStatusSemester();

                $programInfo = $modelForSemester->getProgramInfo($larrresult['IdProgram']);

                $currentSemester = $modelForSemester->getCurrentSemester($programInfo['IdScheme']);
                $effectiveSemester = $modelForSemester->getSemById($formData['EffectiveSemesterFrom']);

                if ($currentSemester['IdSemesterMaster'] != $effectiveSemester['IdSemesterMaster']){
                    $semesterBetween = $modelForSemester->getSemesterBetween($effectiveSemester['IdSemesterMaster'], $programInfo['IdScheme'], $currentSemester['SemesterMainStartDate'], $effectiveSemester['SemesterMainStartDate']);

                    if ($semesterBetween){
                        foreach ($semesterBetween as $semesterBetweenLoop){
                            $modelForSemester->deleteSemesterStatus($formData['IdStudentRegistration'], $semesterBetweenLoop['IdSemesterMaster']);
                        }

                    }
                }
            }

            //audit trail change status
            $this->lobjchangestatusModel->copyAuditTrail($formData['IdChangeStatusApplication'],$formData['status']);
                       
            
            //240 entry 241 approve 242 reject 833 cncel
            
            if($formData['status']=='Approve'){//approve            	
            	
	            	//1)update change status app
	            	
	                $statusdata['Remarks'] = $formData['remarks'];
	                $statusdata['ApplicationStatus'] = 241; 
	                $statusdata['DateApproved'] = date('Y-m-d H:i:s');
	                $statusdata['ApprovedBy'] = $auth->getIdentity()->iduser;
	                
	                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata);
	                
	                
	                //==UPDATE PROFILE STATUS==	                
	                //2)add history and update profile status
	                
	                if($formData['IdApplyingFor'] == 835){ //Quit :=>def type Semester Status = 32    
	                	                
	                	$profilestatus['profileStatus'] = 249; //Quit :=>def type Profile Status = 20   
	                	                 
	                }else if($formData['IdApplyingFor'] == 250){ //DEFER  
	                	              	
	                    $profilestatus['profileStatus'] = 92; //active                                    
	                  
	                }else if($formData['IdApplyingFor'] == 836){ //decease
	                 	
	                	$profilestatus['profileStatus'] = 249; //Quit :=>def type = 32	                                      
	                }	                
	                $cmsStatus->profileStatus($formData['IdStudentRegistration'],$profilestatus,1);
                  	
                    
	                //==UPDATE SEMESTER STATUS==
	                //3)update semester status
	                
	                //1st : get current semester info
	                $reg_sem = $this->lobjChangeSemesterStatus->getStudentRegisterSemester($formData['IdStudentRegistration']);
					
	                if(count($reg_sem)>0){
	                	$level= count($reg_sem);
	                }else{
	                	$level=1;
	                }
	               	                
	                $data['IdStudentRegistration'] = $formData['IdStudentRegistration'];
	                $data['idSemester'] = $formData['EffectiveSemesterFrom'];
	                $data['IdSemesterMain'] = $formData['EffectiveSemesterFrom'];
	                $data['Level'] = $level;
	                $data['UpdDate'] = date('Y-m-d H:i:s');
	                $data['UpdUser'] = $auth->getIdentity()->iduser;
                        $data['validReason'] = $larrresult['ValidReason'];
                        $data['appreson'] = $larrresult['Reason'];
                        if (isset($larrresult['Others'])){
                            $data['appreasonother'] = $larrresult['Others'];
                        }
	               
                         if($formData['IdApplyingFor'] == 250){       //defer def type =32
                             $data['studentsemesterstatus'] =  250;   //def type = 32
                         }else if($formData['IdApplyingFor'] == 835){ //Quit
                             $data['studentsemesterstatus'] =  835;
                         }else if($formData['IdApplyingFor'] == 836){ //decease
                                 $data['studentsemesterstatus'] =  836;
                         }
                         
                         //todo drop course proses here
                         
                        //get subject register
                        $subList = $this->lobjChangeSemesterStatus->getRegisterSubjec($formData['IdStudentRegistration'], $formData['EffectiveSemesterFrom']);

                        //store subject into subject history
                        if ($subList){
                            $arrIdRegSub = array();
                            $arrIdRegSubDtl = array();
                            
                            foreach ($subList as $subLoop){
                                $arrSub = array(
                                    'IdStudentRegSubjects'=>$subLoop['IdStudentRegSubjects'], 
                                    'IdStudentRegistration'=>$subLoop['IdStudentRegistration'], 
                                    'initial'=>$subLoop['initial'], 
                                    'IdSubject'=>$subLoop['IdSubject'], 
                                    'credit_hour_registered'=>$subLoop['credit_hour_registered'], 
                                    'SubjectsApproved'=>$subLoop['SubjectsApproved'], 
                                    'SubjectsApprovedComments'=>$subLoop['SubjectsApprovedComments'], 
                                    'IdSemesterMain'=>$subLoop['IdSemesterMain'], 
                                    'IdSemesterDetails'=>$subLoop['IdSemesterDetails'], 
                                    'SemesterLevel'=>$subLoop['SemesterLevel'], 
                                    'IdLandscapeSub'=>$subLoop['IdLandscapeSub'], 
                                    'IdBlock'=>$subLoop['IdBlock'], 
                                    'BlockLevel'=>$subLoop['BlockLevel'], 
                                    'subjectlandscapetype'=>$subLoop['subjectlandscapetype'], 
                                    'IdGrade'=>$subLoop['IdGrade'], 
                                    'UpdUser'=>$subLoop['UpdUser'], 
                                    'UpdDate'=>$subLoop['UpdDate'], 
                                    'UpdRole'=>$subLoop['UpdRole'], 
                                    'Active'=>2, 
                                    'exam_status'=>$subLoop['exam_status'], 
                                    'exam_status_updateby'=>$subLoop['exam_status_updateby'], 
                                    'exam_status_updatedt'=>$subLoop['exam_status_updatedt'], 
                                    'final_course_mark'=>$subLoop['final_course_mark'], 
                                    'grade_id'=>$subLoop['grade_id'], 
                                    'grade_point'=>$subLoop['grade_point'], 
                                    'grade_name'=>$subLoop['grade_name'], 
                                    'grade_desc'=>$subLoop['grade_desc'], 
                                    'grade_status'=>$subLoop['grade_status'], 
                                    'mark_approval_status'=>$subLoop['mark_approval_status'], 
                                    'mark_approveby'=>$subLoop['mark_approveby'], 
                                    'mark_approvedt'=>$subLoop['mark_approvedt'], 
                                    'IdCourseTaggingGroup'=>$subLoop['IdCourseTaggingGroup'], 
                                    'cgpa_calculation'=>$subLoop['cgpa_calculation'], 
                                    'replace_subject'=>$subLoop['replace_subject'], 
                                    'appeal_mark'=>$subLoop['appeal_mark'], 
                                    'exam_scaling_mark'=>$subLoop['exam_scaling_mark'], 
                                    'migrate_date'=>$subLoop['migrate_date'], 
                                    'migrate_by'=>$subLoop['migrate_by'], 
                                    'message'=>$subLoop['message'], 
                                    'createddt'=>$subLoop['createddt'], 
                                    'createdby'=>$subLoop['createdby'],
                                    'pre_active'=>$subLoop['Active']
                                );    
                                $subId = $this->lobjChangeSemesterStatus->insertRegisterSubjectHistory($arrSub);
                                
                                array_push($arrIdRegSub, $subId);
                                
                                //subject detail
                                $subDtlList = $this->lobjChangeSemesterStatus->getSubjectDetail($subLoop['IdStudentRegSubjects'], $formData['IdStudentRegistration'], $formData['EffectiveSemesterFrom']);
                                
                                if ($subDtlList){
                                    foreach ($subDtlList as $subDtlLoop){
                                        $arrSubDtl = array(
                                            'id'=>$subDtlLoop['id'], 
                                            'regsub_id'=>$subDtlLoop['regsub_id'], 
                                            'student_id'=>$subDtlLoop['student_id'], 
                                            'semester_id'=>$subDtlLoop['semester_id'], 
                                            'subject_id'=>$subDtlLoop['subject_id'], 
                                            'item_id'=>$subDtlLoop['item_id'], 
                                            'section_id'=>$subDtlLoop['section_id'], 
                                            'ec_country'=>$subDtlLoop['ec_country'], 
                                            'ec_city'=>$subDtlLoop['ec_city'], 
                                            'invoice_id'=>$subDtlLoop['invoice_id'], 
                                            'created_date'=>$subDtlLoop['created_date'], 
                                            'created_by'=>$subDtlLoop['created_by'], 
                                            'created_role'=>$subDtlLoop['created_role'], 
                                            'ses_id'=>$subDtlLoop['ses_id'], 
                                            'status'=>$subDtlLoop['status'], 
                                            'modifyby'=>$subDtlLoop['modifyby'], 
                                            'modifydt'=>$subDtlLoop['modifydt'], 
                                            'message'=>$subDtlLoop['message'], 
                                            'createddt'=>$subDtlLoop['createddt'], 
                                            'createdby'=>$subDtlLoop['createdby']
                                        );
                                        $subDtlId = $this->lobjChangeSemesterStatus->insertSubjectDetailHistory($arrSubDtl);
                                        
                                        array_push($arrIdRegSubDtl, $subDtlId);
                                        
                                        //drop subjet detail
                                        $this->lobjChangeSemesterStatus->dropSubjectDetail($subDtlLoop['id']);
                                    }
                                }
                                
                                //drop subject
                                $this->lobjChangeSemesterStatus->dropSubject($subLoop['IdStudentRegSubjects']);
                            }
                            
                            //insert id subject history and subject detail history (use for revert)
                            $dataSubDrop = array(
                                'idregsub'=>implode(',', $arrIdRegSub),
                                'idregsubdtl'=>implode(',', $arrIdRegSubDtl)
                            );
                            $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'], $dataSubDrop);
                        }
	         
	                //2nd : save into history and delete old semester status  add new semester status
	                $message = 'Change semester status upon change status approval.';
	                $cmsStatus->copyDeleteAddSemesterStatus($formData['IdStudentRegistration'],$formData['EffectiveSemesterFrom'],$data,$message,1);	                  
            }
            
            
            
            if($formData['status']=='Reject'){ //reject
            	
                $statusdata['Remarks'] = $formData['remarks'];
                $statusdata['DateApproved'] = date('Y-m-d H:i:s');
                $statusdata['ApprovedBy'] = $auth->getIdentity()->iduser;              
                $statusdata['ApplicationStatus'] = 242;
                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata);
            }

            
            
            
            if($formData['status']=='Revert'){ //revert
            	
            	//==UPDATE CHANGE STATUS==
                $statusdata['Remarks'] = $formData['remarks'];
                $statusdata['ApplicationStatus'] = 240; 
                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata);                
                
                
                //==UPDATE PROFILE STATUS==
                //get last profile status transaction
                $prevStatus = $this->lobjstudentRegistrationModel->getChangeStatusLatestProfileStatus($formData['IdStudentRegistration']);
                //$statusArray['profileStatus'] = $prevStatus['profileStatus'];
                $statusArray['profileStatus'] = 92;

                //add history and update profile status
                $cmsStatus->profileStatus($formData['IdStudentRegistration'],$statusArray,1);
                //==END PROFILE STATUS== 
                
                
                //==UPDATE SEMESTER STATUS==
                //get lastest semester status transaction
                $prevSemStatus = $this->lobjChangeSemesterStatus->getChangeStatusLatestSemesterStatus($formData['IdStudentRegistration']);

                if($prevSemStatus){
                        unset($prevSemStatus['id']);
	                unset($prevSemStatus['idstudentsemsterstatus']);
	                unset($prevSemStatus['Type']);
	                unset($prevSemStatus['message']);
	                unset($prevSemStatus['createddt']);
	                unset($prevSemStatus['createdby']);
	                
	                $prevSemStatus['UpdDate'] = date('Y-m-d H:i:s');
	                $prevSemStatus['UpdUser'] = $auth->getIdentity()->iduser;
                }
                
                //revert drop subject
                $larrresult = $this->lobjchangestatusModel->fnGetApplicationDetail($formData['IdChangeStatusApplication']);
                
                $arrIdRegSub = explode(',', $larrresult['idregsub']);
                $arrIdRegSubDtl = explode(',', $larrresult['idregsubdtl']);
                
                $dropSubList = $this->lobjChangeSemesterStatus->getDropSubject($arrIdRegSub);
                $dropSubDtlList = $this->lobjChangeSemesterStatus->getDropSubjectDetail($arrIdRegSubDtl);
                
                if ($dropSubList){
                    foreach ($dropSubList as $dropSubLoop){
                        $dataDropSub = array(
                            'IdStudentRegSubjects'=>$dropSubLoop['IdStudentRegSubjects'], 
                            'IdStudentRegistration'=>$dropSubLoop['IdStudentRegistration'], 
                            'initial'=>$dropSubLoop['initial'], 
                            'IdSubject'=>$dropSubLoop['IdSubject'], 
                            'credit_hour_registered'=>$dropSubLoop['credit_hour_registered'], 
                            'SubjectsApproved'=>$dropSubLoop['SubjectsApproved'], 
                            'SubjectsApprovedComments'=>$dropSubLoop['SubjectsApprovedComments'], 
                            'IdSemesterMain'=>$dropSubLoop['IdSemesterMain'], 
                            'IdSemesterDetails'=>$dropSubLoop['IdSemesterDetails'], 
                            'SemesterLevel'=>$dropSubLoop['SemesterLevel'], 
                            'IdLandscapeSub'=>$dropSubLoop['IdLandscapeSub'], 
                            'IdBlock'=>$dropSubLoop['IdBlock'], 
                            'BlockLevel'=>$dropSubLoop['BlockLevel'], 
                            'subjectlandscapetype'=>$dropSubLoop['subjectlandscapetype'], 
                            'IdGrade'=>$dropSubLoop['IdGrade'], 
                            'UpdUser'=>$dropSubLoop['UpdUser'], 
                            'UpdDate'=>$dropSubLoop['UpdDate'], 
                            'UpdRole'=>$dropSubLoop['UpdRole'], 
                            'Active'=>$dropSubLoop['pre_active'], 
                            'exam_status'=>$dropSubLoop['exam_status'], 
                            'exam_status_updateby'=>$dropSubLoop['exam_status_updateby'], 
                            'exam_status_updatedt'=>$dropSubLoop['exam_status_updatedt'], 
                            'final_course_mark'=>$dropSubLoop['final_course_mark'], 
                            'grade_id'=>$dropSubLoop['grade_id'], 
                            'grade_point'=>$dropSubLoop['grade_point'], 
                            'grade_name'=>$dropSubLoop['grade_name'], 
                            'grade_desc'=>$dropSubLoop['grade_desc'], 
                            'grade_status'=>$dropSubLoop['grade_status'], 
                            'mark_approval_status'=>$dropSubLoop['mark_approval_status'], 
                            'mark_approveby'=>$dropSubLoop['mark_approveby'], 
                            'mark_approvedt'=>$dropSubLoop['mark_approvedt'], 
                            'IdCourseTaggingGroup'=>$dropSubLoop['IdCourseTaggingGroup'], 
                            'cgpa_calculation'=>$dropSubLoop['cgpa_calculation'], 
                            'replace_subject'=>$dropSubLoop['replace_subject'], 
                            'appeal_mark'=>$dropSubLoop['appeal_mark'], 
                            'exam_scaling_mark'=>$dropSubLoop['exam_scaling_mark'], 
                            'migrate_date'=>$dropSubLoop['migrate_date'], 
                            'migrate_by'=>$dropSubLoop['migrate_by'], 
                            //'message'=>$dropSubLoop['message'], 
                            //'createddt'=>$dropSubLoop['createddt'], 
                            //'createdby'=>$dropSubLoop['createdby']
                        );
                        $this->lobjChangeSemesterStatus->revertDropSubject($dataDropSub);
                        
                        //delete drop subject history (not in used)
                        //$this->lobjChangeSemesterStatus->deleteDropSubjectHistory($dropSubLoop['id']);
                    }
                }
                
                if ($dropSubDtlList){
                    foreach ($dropSubDtlList as $dropSubDtlLoop){
                        $dataDropSubDtl = array(
                            'id'=>$dropSubDtlLoop['id'], 
                            'regsub_id'=>$dropSubDtlLoop['regsub_id'], 
                            'student_id'=>$dropSubDtlLoop['student_id'], 
                            'semester_id'=>$dropSubDtlLoop['semester_id'], 
                            'subject_id'=>$dropSubDtlLoop['subject_id'], 
                            'item_id'=>$dropSubDtlLoop['item_id'], 
                            'section_id'=>$dropSubDtlLoop['section_id'], 
                            'ec_country'=>$dropSubDtlLoop['ec_country'], 
                            'ec_city'=>$dropSubDtlLoop['ec_city'], 
                            'invoice_id'=>$dropSubDtlLoop['invoice_id'], 
                            'created_date'=>$dropSubDtlLoop['created_date'], 
                            'created_by'=>$dropSubDtlLoop['created_by'], 
                            'created_role'=>$dropSubDtlLoop['created_role'], 
                            'ses_id'=>$dropSubDtlLoop['ses_id'], 
                            'status'=>$dropSubDtlLoop['status'], 
                            'modifyby'=>$dropSubDtlLoop['modifyby'], 
                            'modifydt'=>$dropSubDtlLoop['modifydt'], 
                            //'message'=>$dropSubDtlLoop['message'], 
                            //'createddt'=>$dropSubDtlLoop['createddt'], 
                            //'createdby'=>$dropSubDtlLoop['createdby']
                        );
                        $this->lobjChangeSemesterStatus->revertDropSubjectDtl($dataDropSubDtl);
                        
                        //delete subject detail history (not in used)
                        //$this->lobjChangeSemesterStatus->deleteDropSubjectDtlHistory($dropSubDtlLoop['srsdh_id']);
                    }
                }
                
                $statusdata2['idregsub'] = 0;
                $statusdata2['idregsubdtl'] = 0;
                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata2);
	                
                $cmsStatus->copyDeleteAddSemesterStatus($formData['IdStudentRegistration'],$formData['EffectiveSemesterFrom'],$prevSemStatus,'Change semester status upon revert change status',1);               
                //==END SEMESTER STATUS==
            }
           
            $this->gobjsessionsis->flash = array('message' => 'Data has been update', 'type' => 'success');
            $this->_redirect($this->baseUrl . '/records/semesterchangestatus/approval/id/'.$formData['IdChangeStatusApplication']);
            
        }else{
        	
        	$lintIdApplication = ( int ) $this->_getParam ('id');
	        $this->view->IdApplication = $lintIdApplication;
	        
	        $larrresult = $this->lobjchangestatusModel->fnGetApplicationDetail($lintIdApplication);
	                
	        //get current semester
		    $semester = $this->lobjstudentprogramchange->getProgramCurrentSemester($larrresult['IdScheme'],$larrresult['IdBranch']);

            $upl = $this->lobjchangestatusModel->viewUpl($lintIdApplication);

            $this->view->upl = $upl;
		       	       
		    if($semester['IdSemesterMaster']==$larrresult['EffectiveSemesterFrom']){		    	
		    	$this->view->allow =true;
		    }else{
		    	$this->view->allow=false;
		    }
		    
	        //echo '<pre>';
	        // print_r($larrresult);
	        
	        $IdUniversity = $this->gobjsessionsis->idUniversity;
	        $this->view->confdetail = $this->lobjrecordconfigModel->fnfetchConfiguration($IdUniversity);
	
	         //get semester       
	        $semester_from = $this->lobjstudentprogramchange->getProgramSemester($larrresult['EffectiveSemesterFrom']);
	        $larrresult['EffectiveSemesterFromName'] = $semester_from['SemesterMainName']; 
	        	
	        $reason = '';        
	        if($larrresult['IdApplyingFor'] == 250){
	           //$resonsList = $this->lobjdeferreasonModel->getDeferDescription($larrresult['Reason']);
                   $resonsList = $this->lobjdeferreasonModel->getDefination($larrresult['Reason']);
	           $reason = $resonsList['name'];
	        }else{
	           //$resonsList = $this->lobjquitreasonModel->getQuitDescription($larrresult['Reason']);   
                   $resonsList = $this->lobjdeferreasonModel->getDefination($larrresult['Reason']);
	           $reason = $resonsList['name'];
	        }
	        //var_dump($resonsList);
	        $this->view->reasons = $reason;       
	        $this->view->appdetail = $larrresult;
                //var_dump($larrresult);
                if ($larrresult['IdApplyingFor'] == 835){
                    //get kmc data
                    $kmc = $this->lobjchangestatusModel->getKmc($lintIdApplication);
                    $celFin = $this->lobjchangestatusModel->getClearanceFinance($lintIdApplication);
                    $ict = $this->lobjchangestatusModel->getIct($lintIdApplication);
                    $logistic = $this->lobjchangestatusModel->getLogistic($lintIdApplication);
                    
                    $this->view->kmc = $kmc;
                    $this->view->celFin = $celFin;
                    $this->view->ict = $ict;
                    $this->view->logistic = $logistic;
                    
                    /*var_dump($kmc);
                    var_dump($celFin);
                    var_dump($ict);*/
                }
	        
	        if(!empty($larrresult)){
	        	
	            $this->view->changestatusapplicationForm = $this->changestatusapplicationForm;
	            $this->view->changestatusapplicationForm->populate($larrresult);
		        
		        $this->view->changestatusapplicationForm->DateApproved->setValue(date('Y-m-d'));
		        $ldtsystemDate = date('Y-m-d H:i:s');
		        $this->view->changestatusapplicationForm->UpdDate->setValue($ldtsystemDate);                
		        $this->view->changestatusapplicationForm->UpdUser->setValue($auth->getIdentity()->iduser);
		        $this->view->changestatusapplicationForm->ApprovedBy->setValue($auth->getIdentity()->iduser);
		
		        $authUserName = $this->lobjsubjectmasterModel->getUserName($auth->getIdentity()->iduser);
		        $this->view->changestatusapplicationForm->ApprovedByName->setValue($authUserName['loginName']);
		        $this->view->changestatusapplicationForm->IdChangeStatusApplication->setValue($lintIdApplication);        
		        $this->view->changestatusapplicationForm->IdStudentRegistration->setValue($larrresult['IdStudentRegistration']);
		        $this->view->changestatusapplicationForm->IdApplyingFor->setValue($larrresult['IdApplyingFor']);
	        
	        }
	        
	        
        }
     
    	
    }
    
    public function changestatuslistAction(){        
        $lintIdApplication = ( int ) $this->_getParam ('id');
        $this->view->IdApplication = $lintIdApplication;
        $larrresult = $this->lobjchangestatusModel->fnGetApplicationDetail($lintIdApplication);        
        $currsemList = $this->lobjstudentprogramchange->fngetstudentcurrentsem($larrresult['IdStudentRegistration']);
        $currSem = '';
        $currSemstatus = '';

        if(!empty($currsemList)){
            if($currsemList[0]['SemesterCode'] != ''){
               $currSem = $currsemList[0]['SemesterCode'];
               $currSemstatus = $currsemList[0]['DefinitionDesc'];
            }
            if($currsemList[0]['SemesterMainCode'] != ''){
               $currSem = $currsemList[0]['SemesterMainCode'];
               $currSemstatus = $currsemList[0]['DefinitionDesc'];
            }
        }

        $this->view->currsemstatus = $currSemstatus;
        $this->view->currsem = $currSem;
        $IdUniversity = $this->gobjsessionsis->idUniversity;
        $this->view->confdetail = $this->lobjrecordconfigModel->fnfetchConfiguration($IdUniversity);


        //250 defer 835:quit 836:quit:deceased
        $reason = '';        
        if($larrresult['IdApplyingFor'] == 250){
           $resonsList = $this->lobjdeferreasonModel->getDeferDescription($larrresult['Reason']);
           $reason = $resonsList['ReasonDefer'];
        }else{
           $resonsList = $this->lobjquitreasonModel->getQuitDescription($larrresult['Reason']);           
           $reason = $resonsList['Reason'];
        }
       
        $this->view->reasons = $reason;       
        $this->view->appdetail = $larrresult;

        if(!empty($larrresult)){
            $this->view->changestatusapplicationForm = $this->changestatusapplicationForm;
        }
        $this->view->changestatusapplicationForm->populate($larrresult);
        $this->view->changestatusapplicationForm->DateApproved->setValue(date('Y-m-d'));
        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->changestatusapplicationForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
        
        $this->view->changestatusapplicationForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->changestatusapplicationForm->ApprovedBy->setValue($auth->getIdentity()->iduser);

        $authUserName = $this->lobjsubjectmasterModel->getUserName($auth->getIdentity()->iduser);
        $this->view->changestatusapplicationForm->ApprovedByName->setValue($authUserName['loginName']);
        $this->view->changestatusapplicationForm->IdChangeStatusApplication->setValue($lintIdApplication);
        $this->view->changestatusapplicationForm->IdEffectiveSemester->setValue($larrresult['IdEffectiveSemester']);
        $this->view->changestatusapplicationForm->IdStudentRegistration->setValue($larrresult['IdStudentRegistration']);
        $this->view->changestatusapplicationForm->IdApplyingFor->setValue($larrresult['IdApplyingFor']);
        

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            
            $effectivesem = $this->lobjsubjectmasterModel->getSemesterid($formData['IdEffectiveSemester']);
           
            if(isset($formData['Approve'])){
                $statusdata['Remarks'] = $formData['Remarks'];
                $statusdata['ApplicationStatus'] = 241;
                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata);
                $data = array();
                $data['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                if(isset($effectivesem['IdSemesterMaster'])){
                    $data['IdSemesterMain'] = $effectivesem['IdSemesterMaster'];
                }else{
                    $data['idSemester'] = $effectivesem['IdSemester'];
                }
                $data['UpdDate'] = $formData['UpdDate'];
                $data['UpdUser'] = $formData['UpdUser'];
               
                 if($formData['IdApplyingFor'] == 248){
                    $data['studentsemesterstatus'] =  250;
                 }else{
                     $data['studentsemesterstatus'] =  249;
                 }
         
                $this->lobjsemesterstatusModel->addSemesterStatus($data);
                if($formData['IdApplyingFor'] == 249){
                    $statusArray['profileStatus'] = 249;
                    $this->lobjstudentregModel->updateStudentProfile($formData['IdStudentRegistration'],$statusArray);
                    $statusArray['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                    $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR'); 
                    $statusArray['UpdUser'] = $formData['UpdUser'];
                    $statusArray['UpdDate'] = $formData['UpdDate'];
                    $this->lobjstudentRegistrationModel->addStudentProfileHistory($statusArray);
                }else{
                    $statusArray['profileStatus'] = 92;
                    $this->lobjstudentregModel->updateStudentProfile($formData['IdStudentRegistration'],$statusArray);
                    $statusArray['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                    $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR'); 
                    $statusArray['UpdUser'] = $formData['UpdUser'];
                    $statusArray['UpdDate'] = $formData['UpdDate'];
                    $this->lobjstudentRegistrationModel->addStudentProfileHistory($statusArray);

                }
                
            }
            
            if(isset($formData['DisApprove'])){
                $statusdata['Remarks'] = $formData['Remarks'];
                $statusdata['ApplicationStatus'] = 242;
                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata);
            }

            if(isset($formData['Revert'])){
                $statusdata['Remarks'] = $formData['Remarks'];
                $statusdata['ApplicationStatus'] = 240;
                $statusdata['Remarks'] = '';
                $this->lobjchangestatusModel->fnUpdateApplicationstatus($formData['IdChangeStatusApplication'],$statusdata);

                $historyList = $this->lobjstudentRegistrationModel->fetchStudentHistory($formData['IdStudentRegistration']);
                $count = count($historyList);
                $prevStatus = '';                
                $prevStatus = $historyList[$count-2]['profileStatus'];                

                $statusArray['profileStatus'] = $prevStatus;

                $this->lobjstudentregModel->updateStudentProfile($formData['IdStudentRegistration'],$statusArray);

                $statusArray['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
                $statusArray['UpdUser'] = $formData['UpdUser'];
                $statusArray['UpdDate'] = $formData['UpdDate'];
                $this->lobjstudentRegistrationModel->addStudentProfileHistory($statusArray);

                if(isset($effectivesem['IdSemesterMaster'])){
                    $this->lobjchangestatusModel->deleteMainSemester($formData['IdStudentRegistration'],$effectivesem['IdSemesterMaster']);
                }else{
                    $this->lobjchangestatusModel->deleteSemester($formData['IdStudentRegistration'],$effectivesem['IdSemester']);
                }
            }
            $this->_redirect($this->baseUrl . '/records/semesterchangestatus/changestatusapplicationapproval');
        }
    }

    

    public function editchangestatuslistAction(){
        $lintIdApplication = ( int ) $this->_getParam ('id');
        $this->view->IdApplication = $lintIdApplication;
        $larrresult = $this->lobjchangestatusModel->fnGetApplicationDetail($lintIdApplication);
        $currsemList = $this->lobjstudentprogramchange->fngetstudentcurrentsem($larrresult['IdStudentRegistration']);
        $currSem = '';
        $currSemstatus = '';
        
        if($currsemList[0]['SemesterCode'] != ''){
           $currSem = $currsemList[0]['SemesterCode'];
           $currSemstatus = $currsemList[0]['DefinitionDesc'];
        }
        if($currsemList[0]['SemesterMainCode'] != ''){
           $currSem = $currsemList[0]['SemesterMainCode'];
           $currSemstatus = $currsemList[0]['DefinitionDesc'];
        }
       
        $this->view->currsemstatus = $currSemstatus;
        $this->view->currsem = $currSem;

       

        $reason = '';
        if($larrresult['IdApplyingFor'] == 248){
           $resonsList = $this->lobjdeferreasonModel->getDeferDescription($larrresult['Reason']);
           $reason = $resonsList['ReasonDefer'];
        }else{
           $resonsList = $this->lobjquitreasonModel->getQuitDescription($larrresult['Reason']);
           $reason = $resonsList['Reason'];
        }
        $this->view->reasons = $reason;
        $this->view->appdetail = $larrresult;
    }

    public function approveapplicationAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $Idstudent = $this->_getParam('IdStudent');
        $ret = $this->lobjChangeSemesterStatus->checkValidchangeStatusApplication($Idstudent);        
        if(empty($ret)){
            echo "1";
        }else{
            echo "0";
        }
        die;
    }
    
    
	function searchSemesterAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$semester_id = $this->_getParam('semester_id', null);
		$scheme_id = $this->_getParam('scheme_id', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
	
		//get semester to 
		$result = $this->lobjstudentprogramchange->getProgramNextSemester($semester_id,$scheme_id);
			
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	
		$json = Zend_Json::encode($result);
	
		echo $json;
		exit();
		
	}
	
	
	 public function findStudentAction(){
	 	
        $searchElement = $this->_getParam('term', '');
        $searchType = $this->_getParam('searchType', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
		$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $studentChangeDB = new Records_Model_DbTable_Studentprogramchange();
            
        $changeStatusDB = new Records_Model_DbTable_Changestatusapplication();
		$student_list = $changeStatusDB->getStudent(array('searchElement'=>$searchElement,'searchType'=>$searchType));
		    
		//echo '<pre>';
		//print_r($student_list);
		
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
         
        if ($student_list){
            foreach($student_list as $student) { 
            	            	
		        //get semester 
		        $semester = $studentChangeDB->getProgramCurrentSemester($student['IdScheme'],$student['IdBranch']);
		        
		        //get student semester status
		        if($semester){
		        	$sem_status = $studentChangeDB->getStudentSemesterStatus($student['IdStudentRegistration'],$semester['IdSemesterMaster']);
		        	if($sem_status){
		        		$semesterStatus = $sem_status['semester_status'];
		        	}else{
		        		$semesterStatus = 'Not Registered';
		        	}
		        }else{
		        	$semesterStatus = 'Unidentified Current Semester';
		        }
		         
                $student_array[] = array(
                    'IdStudentRegistration' => $student['IdStudentRegistration'],
                    'StudentID' => $student['registrationId'],
                    'StudentName'=>$student['appl_fname'] . ' ' . $student['appl_lname'],
                    'IDNo' => $student['appl_idnumber'],					                   
                    'IntakeName'=>$student['IntakeDesc'],
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramName'=>$student['ProgramName'],
                    'ProgramSchemeName'=>$student['mop'].' '.$student['mos'].' '.$student['pt'],
                    'ProfileStatus'=>$student['ProfileStatus'],
                    'label' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_lname']. ' ('.$student['ProfileStatus'].')',
                    'value' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_lname']. ' ('.$student['ProfileStatus'].')',
                    'IdSemester'=>$semester['IdSemesterMaster'],
                    'SemesterName'=>$semester['SemesterMainName'],
                    'SemesterStatus'=>$semesterStatus,
                    'StudentCategory' => $student['StudentCategory'],
                );
            }
        }else{
            $student_array[] = array();
        }
        
        $json = Zend_Json::encode($student_array);
		
	echo $json;
	exit();
    }
    
    public function generateChangeStatusID(){
    	
    	$formatLib = new icampus_Function_General_Format();
        $CSApplicationCode = $formatLib->generateApplicationID(5,'changeStatusIdFormat','changeStatusPrefix');
            
        //check duplicate code
        $isDuplicate = $this->lobjchangestatusModel->isDuplicateCode($CSApplicationCode);
        
        if($isDuplicate){
        	$this->generateChangeStatusID();
        }else{
        	return $CSApplicationCode;
        }
    }
    
    
    public function clearanceAction(){
    	
        $id = $this->_getParam('id', 0);
    	$auth = Zend_Auth::getInstance();
    	
    	 if ($this->_request->isPost()) {
    	 	
            $formData = $this->_request->getPost();

            if($formData['type']=='kmc'){

                $formData['kmc_paiddt'] = date('Y-m-d', strtotime($formData['kmc_paiddt']));
            	$formData['kmc_createddt'] = date('Y-m-d H:i:s');
	            $formData['kmc_createdby'] = $auth->getIdentity()->iduser;
	            
	            $table = "department_clearance_kmc";
                    
                    $check = $this->lobjchangestatusModel->getKmc($id);
            }
            
    	  	if($formData['type']=='finance'){          
            	  	
            	$formData['dcf_createddt'] = date('Y-m-d H:i:s');
	            $formData['dcf_createdby'] = $auth->getIdentity()->iduser;
	            
	            $table = "department_clearance_finance";
                    
                    $check = $this->lobjchangestatusModel->getClearanceFinance($id);
            }
            
    	 	if($formData['type']=='ict'){          
            	  	
            	$formData['ict_createddt'] = date('Y-m-d H:i:s');
	            $formData['ict_createdby'] = $auth->getIdentity()->iduser;
	            
	            $table = "department_clearance_ict";
                    
                    $check = $this->lobjchangestatusModel->getIct($id);
            }
            
            if($formData['type']=='logistic'){          
            	  	
            	$formData['log_createddt'] = date('Y-m-d H:i:s');
	            $formData['log_createdby'] = $auth->getIdentity()->iduser;
	            $formData['log_status'] = $formData['logistic_status'];
	            $formData['log_remarks'] = $formData['logistic_remarks'];

                unset($formData['logistic_status']);
                unset($formData['logistic_remarks']);

	            $table = "department_clearance_logistic";
                    
                    $check = $this->lobjchangestatusModel->getLogistic($id);
            }
            
            $clearanceDb = new Records_Model_DbTable_Clearance();

            if (is_array($check)){
                $clearanceDb->updateData($table, $formData, $id);
            }else{
                $clearanceDb->addData($table,$formData);
            }
           
            
    	 }
         
         $this->_redirect($this->baseUrl . '/records/semesterchangestatus/approval/id/'.$id.'#tabs-2');
    	 exit;
    }
    
    public function printDeferSlipAction(){
        $id = $this->_getParam('id', 0);
        
        $appInfo = $this->lobjchangestatusModel->fnGetApplicationDetail($id);
        $template = $this->lobjchangestatusModel->getDeferSlipTemplate();
        
        $templaterep = $this->replaceTag($template['tpl_content'], $id);
        
        $filename = $appInfo['CSApplicationCode'].'_'.date('dmYHis');
        
        $content_option = array(
            'content'=>$templaterep,
            'file_name' => $filename,
            'file_extension' => 'pdf',
        );
        //exit;
        generatePdf($content_option);
        
        exit;
    }
    
    public function replaceTag($content, $lintIdApplication){
        $appInfo = $this->lobjchangestatusModel->fnGetApplicationDetail($lintIdApplication);
        $tags = $this->lobjchangestatusModel->getTags();
        
        if ($tags){
            foreach ($tags as $tagsLoop){
                switch ($tagsLoop['tag_name']){
                    case 'Student Name':
                        $content = str_replace('[Student Name]', $appInfo['appl_fname'].' '.$appInfo['appl_lname'], $content);
                        break;
                    case 'Student ID':
                        $content = str_replace('[Student ID]', $appInfo['registrationId'], $content);
                        break;
                    case 'ID Number':
                        $content = str_replace('[ID Number]', $appInfo['appl_idnumber'], $content);
                        break;
                    case 'Programme':
                        $content = str_replace('[Programme]', $appInfo['ProgramName'], $content);
                        break;
                    case 'Mode of Programme':
                        $content = str_replace('[Mode of Programme]', $appInfo['mop'], $content);
                        break;
                    case 'Mode of Study':
                        $content = str_replace('[Mode of Study]', $appInfo['mos'], $content);
                        break;
                    case 'Intake':
                        $content = str_replace('[Intake]', $appInfo['IntakeName'], $content);
                        break;
                    /*case 'Old Mode of Programme':
                        $content = str_replace('[Old Mode of Programme]', $appInfo['currentMopName'], $content);
                        break;
                    case 'Old Mode of Study':
                        $content = str_replace('[Old Mode of Study]', $appInfo['currentMosName'], $content);
                        break;
                    case 'New Mode of Programme':
                        $content = str_replace('[New Mode of Programme]', $appInfo['changeMopName'], $content);
                        break;
                    case 'New Mode of Study':
                        $content = str_replace('[New Mode of Study]', $appInfo['changeMosName'], $content);
                        break;*/
                    case 'Date':
                        $content = str_replace('[Date]', date('d F Y'), $content);
                        break;
                    case 'Semester':
                        $sem = $this->lobjchangestatusModel->getSemesterById($appInfo['EffectiveSemesterFrom']);
                        $content = str_replace('[Semester]', $sem['SemesterMainName'], $content);
                        break;
                    case 'Next Semester':
                        $sem = $this->lobjchangestatusModel->getSemesterById($appInfo['EffectiveSemesterFrom']);
                        
                        if ($sem){
                            if ($sem['sem_seq']=='SEP'){
                                $year = ($sem['AcademicYear']+1);
                                $month = 'JAN';
                            }else if ($sem['sem_seq']=='JUN'){
                                $year = $sem['AcademicYear'];
                                $month = 'SEP';
                            }else if ($sem['sem_seq']=='JAN'){
                                $year = $sem['AcademicYear'];
                                $month = 'JUN';
                            }
                            
                            $getnextsem = $this->lobjchangestatusModel->getSemesterByYearAndMonth($sem['IdScheme'], $year, $month);
                            
                            if ($getnextsem){
                                $nextsem = $getnextsem['SemesterMainName'];
                            }else{
                                $nextsem = 'No Next Semester In Setup';
                            }
                        }else{
                            $nextsem = 'No Next Semester In Setup';
                        }
                        
                        $content = str_replace('[Next Semester]', $nextsem, $content);
                        break;
                }
            }
        }
        
        return $content;
    }

    public function printClearanceAction(){
        $id = $this->_getParam('id', 0);

        global $appdetail;
        global $kmc;
        global $celFin;
        global $ict;
        global $logistic;

        $appdetail = $this->lobjchangestatusModel->fnGetApplicationDetail($id);
        $kmc = $this->lobjchangestatusModel->getKmc($id);
        $celFin = $this->lobjchangestatusModel->getClearanceFinance($id);
        $ict = $this->lobjchangestatusModel->getIct($id);
        $logistic = $this->lobjchangestatusModel->getLogistic($id);

        global $reason;
        $reason = '';
        if($appdetail['IdApplyingFor'] == 250){
            //$resonsList = $this->lobjdeferreasonModel->getDeferDescription($larrresult['Reason']);
            $resonsList = $this->lobjdeferreasonModel->getDefination($appdetail['Reason']);
            $reason = $resonsList['name'];
        }else{
            //$resonsList = $this->lobjquitreasonModel->getQuitDescription($larrresult['Reason']);
            $resonsList = $this->lobjdeferreasonModel->getDefination($appdetail['Reason']);
            $reason = $resonsList['name'];
        }

        $fieldValues = array(
            '$[LOGO]'=> "images/logo_text_high.jpg"
        );

        $html_template_path = DOCUMENT_PATH."/template/clearance.html";
        $output_filename = $appdetail["registrationId"]."-clearance.pdf";

        require_once 'dompdf_config.inc.php';
        error_reporting(0);

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);
        }

        //echo $html;
        //exit;

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();

        $dompdf->stream($output_filename);

        exit;
    }
}