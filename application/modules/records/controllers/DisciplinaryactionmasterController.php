<?php
class Records_DisciplinaryactionmasterController extends Base_Base { //Controller for the Create Appraisal Module 
	
	private $_gobjlog;
	public function init()	{
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();			 
	}
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjplacementtestmodel = new Application_Model_DbTable_Placementtest();
		$this->lobjdisciplinaryactionmastermodel = new Application_Model_DbTable_Disciplinaryactionmaster();	
		$this->lobjdisciplinaryactionmasterform = new Application_Form_Disciplinaryactionmaster();	
	}	
	// action for search and view
	public function indexAction() { 	
		$lobjSearchForm = $this->view->lobjSearchForm = $this->lobjform; 	
		$lobjDisciplinaryactionmasterModel = $this->lobjdisciplinaryactionmastermodel;	
		//Get Disciplinary Actions List
		$larrDisciplinaryactionList = $lobjDisciplinaryactionmasterModel->fnGetDisciplinaryActionTypeList();	
		//Set the Search Type List to the dropdown field
		$lobjSearchForm->field1->addMultiOptions($larrDisciplinaryactionList);
				
		//Get Disciplinary Action Details
		$lobjDisciplinaryActionMasterDetails = $lobjDisciplinaryactionmasterModel->fnGetDisciplinaryActionMasterDetails();
		
		//Disciplinary Action Details - Pagination
		$lintpagecount = $this->gintPageCount;  // Records per page		
		$lobjPaginator = new App_Model_Definitiontype(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsams->disciplinaryactionmasterpaginatorresult)) {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($this->gobjsessionsams->disciplinaryactionmasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($lobjDisciplinaryActionMasterDetails,$lintpage,$lintpagecount);
		}	
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjSearchForm->isValid ( $larrformData )) {
				$larrSearchResult = $lobjDisciplinaryactionmasterModel->fnSearchDisciplinaryActionMasterDetails( $larrformData );			
				$this->view->lobjPaginator = $lobjPaginator->fnPagination($larrSearchResult,$lintpage,$lintpagecount);
		    	$this->gobjsessionsams->disciplinaryactionmasterpaginatorresult = $larrSearchResult;		    	
			}
		}
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {		
			$this->_redirect( $this->baseUrl . '/records/disciplinaryactionmaster/index');
		}
	}	
	//Action To Update Student Disciplinary Action Details
	public function updatedisciplinaryactionmasterAction(){	
		//Get Vales For UpdUser & UpdDate
		$lstrUpdUser = 1;
		$lstrUpdDate = date('Y-m-d h:m:s');	
		//Get Definition Id
		$lintIdDefinition = $this->_getParam('idDefinition');
		$this->view->idDefinition = $lintIdDefinition;		
		$lobjDisciplinaryactionmasterForm = $this->lobjdisciplinaryactionmasterform; //intialize search lobjuserForm		
		$this->view->lobjDisciplinaryactionForm = $lobjDisciplinaryactionmasterForm; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		//Initialize Disciplinary Action Model
		$lobjDisciplinaryactionmasterModel = $this->lobjdisciplinaryactionmastermodel;
		
		$lintIdDefType = $lobjDisciplinaryactionmasterModel->fnGetDefinitionTypeId();
		$this->view->GroupType = $lintIdDefType;
		
		//Get Disciplinary Actions List
		$larrDisciplinaryactionList = $lobjDisciplinaryactionmasterModel->fnGetDisciplinaryActionTypeList();
		
		//Get Disciplinary Action Master Details
		$larrDisciplinaryActionDetails = $lobjDisciplinaryactionmasterModel->fnGetDisciplinaryActionMasterDetailsByDefintionId($lintIdDefinition);
			//Set Values To Form
		$lobjDisciplinaryactionmasterForm->idDisciplinaryActionType->addMultiOptions($larrDisciplinaryactionList);
		
		/*if(count($larrDisciplinaryActionDetails['idDisciplinaryActionMaster']) == 1){	
			$lobjDisciplinaryactionmasterForm->idDisciplinaryActionMaster->setValue($larrDisciplinaryActionDetails['idDisciplinaryActionMaster']);
			$lobjDisciplinaryactionmasterForm->DiscMsgCollegeHead->setValue($larrDisciplinaryActionDetails['DiscMsgCollegeHead']);
			$lobjDisciplinaryactionmasterForm->DiscMsgParent->setValue($larrDisciplinaryActionDetails['DiscMsgParent']);
			$lobjDisciplinaryactionmasterForm->DiscMsgSponor->setValue($larrDisciplinaryActionDetails['DiscMsgSponor']);
			$lobjDisciplinaryactionmasterForm->DiscMsgRegistar->setValue($larrDisciplinaryActionDetails['DiscMsgRegistar']);
		}*/
		//Set UpdUser & UpdDateValues
		$lobjDisciplinaryactionmasterForm->idDisciplinaryActionType->setValue($lintIdDefinition);
		$lobjDisciplinaryactionmasterForm->UpdUser->setValue($lstrUpdUser);
		$lobjDisciplinaryactionmasterForm->UpdDate->setValue($lstrUpdDate);
		$lobjDisciplinaryactionmasterForm->idDisciplinaryActionType->setAttrib('readonly',true);
		if($larrDisciplinaryActionDetails["idDisciplinaryActionMaster"]!=""){
			$this->view->idDisciplinaryActionMaster = $larrDisciplinaryActionDetails["idDisciplinaryActionMaster"];
			
			$lobjDisciplinaryactionmasterForm->populate($larrDisciplinaryActionDetails);
		}else{
			$this->view->idDisciplinaryActionMaster = "";
		}

		//Disciplinary Action Master Update
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			$larrformData['UpdDate'] =  date('Y-m-d h:m:s');			
			unset($larrformData['Save']);			
			if($larrformData['idDisciplinaryActionMaster'] != "")$larrSearchResult = $lobjDisciplinaryactionmasterModel->fnUpdateDisciplinaryActionMasterDetails($larrformData['idDisciplinaryActionMaster'],$larrformData);
			else $larrSearchResult = $lobjDisciplinaryactionmasterModel->fnSaveDisciplinaryActionDetails($larrformData);
			
			// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Disciplinary Messaging setup add Id=' . $lintIdDefinition,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				
			$this->_redirect( $this->baseUrl . '/records/disciplinaryactionmaster/index');
		}
	}
	
}