<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 20/10/2015
 * Time: 2:10 PM
 */
class Records_RepeatPaperReportController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_RepeatPaperReport();
        $this->academicprogressmodel = new Records_Model_DbTable_Academicprogressremarks();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Student Repeat Paper Report');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])){
                $form = new Records_Form_RepeatPaperReportSearch(array('programid'=>$formData['program']));
                $this->view->form = $form;

                $list = $this->model->getStudent($formData);

                if ($list){
                    foreach ($list as $key => $loop){
                        $repeatSub = $this->model->getRepeatSubject($loop['IdStudentRegistration']);

                        if ($repeatSub) {
                            foreach ($repeatSub as $key3 => $repeatSubLoop){
                                $countrepsubject = $this->model->getCountRepeatSubject($loop['IdStudentRegistration'], $repeatSubLoop['IdSubject']);
                                $repeatSub[$key3]['repcount'] = (count($countrepsubject)-1);
                            }

                            $list[$key]['repeatsub'] = $repeatSub;
                        }

                        $replaceSub = $this->model->getReplaceSubject($loop['IdStudentRegistration']);

                        $repSubArr = array();
                        if ($replaceSub){
                            foreach ($replaceSub as $key2 => $replaceSubLoop){
                                $subjectRepeat = $this->academicprogressmodel->getSubjectReplace($replaceSubLoop['replace_subject']);
                                $subject = $this->academicprogressmodel->getSubjectById($subjectRepeat['IdSubject']);
                                $subject['replacesubject']=$replaceSubLoop['SubCode'];
                                $repSubArr[]=$subject;
                            }
                        }

                        if (count($repSubArr) > 0){
                            $list[$key]['replacesub'] = $repSubArr;
                        }
                    }
                }

                $form->populate($formData);
                $this->view->list = $list;
            }else{
                $form = new Records_Form_RepeatPaperReportSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Records_Form_RepeatPaperReportSearch();
            $this->view->form = $form;
        }
    }

    public function printAction(){

        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Records_Form_RepeatPaperReportSearch(array('programid'=>$formData['program']));
            $this->view->form = $form;

            $list = $this->model->getStudent($formData);

            if ($list){
                foreach ($list as $key => $loop){
                    $repeatSub = $this->model->getRepeatSubject($loop['IdStudentRegistration']);

                    if ($repeatSub) {
                        foreach ($repeatSub as $key3 => $repeatSubLoop){
                            $countrepsubject = $this->model->getCountRepeatSubject($loop['IdStudentRegistration'], $repeatSubLoop['IdSubject']);
                            $repeatSub[$key3]['repcount'] = (count($countrepsubject)-1);
                        }

                        $list[$key]['repeatsub'] = $repeatSub;
                    }

                    $replaceSub = $this->model->getReplaceSubject($loop['IdStudentRegistration']);

                    $repSubArr = array();
                    if ($replaceSub){
                        foreach ($replaceSub as $key2 => $replaceSubLoop){
                            $subjectRepeat = $this->academicprogressmodel->getSubjectReplace($replaceSubLoop['replace_subject']);
                            $subject = $this->academicprogressmodel->getSubjectById($subjectRepeat['IdSubject']);
                            $subject['replacesubject']=$replaceSubLoop['SubCode'];
                            $repSubArr[]=$subject;
                        }
                    }

                    if (count($repSubArr) > 0){
                        $list[$key]['replacesub'] = $repSubArr;
                    }
                }
            }

            $form->populate($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_repeat_paper_report.xls';
    }
}