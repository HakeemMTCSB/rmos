<?php

class Records_RecordsconfigurationController extends Base_Base { //Controller for the REcords Module

    private $_gobjlog;
    private $lobjRecordConfigurationForm;
    private $lobjdeftype;
    private $lobjrecordconfigModel;
    private $lobjrecordconfigmesgModel;
    private $lobjrecordquitreasonForm;
    private $lobjquitreasonModel;
    private $lobjrecorddeferreasonForm;
    private $lobjrecorddeferreasonModel;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->fnsetObj();
    }

    public function fnsetObj() {
        $this->lobjRecordConfigurationForm = new Records_Form_Recordconfiguration();
        $this->lobjdeftype = new App_Model_Definitiontype();
        $this->lobjrecordconfigModel = new Records_Model_DbTable_Recordconfig();
        $this->lobjrecordconfigmesgModel = new Records_Model_DbTable_Recordconfigmsg();
        $this->lobjrecordquitreasonForm = new Records_Form_Recordquitreason();
        $this->lobjquitreasonModel = new Records_Model_DbTable_Recordquitreason();
        $this->lobjrecorddeferreasonForm = new Records_Form_Recorddeferreason();
        $this->lobjrecorddeferreasonModel = new Records_Model_DbTable_Recorddeferreason();
    }

    public function indexAction() {
        $this->view->lobjRecordConfigurationForm = $this->lobjRecordConfigurationForm;
        $this->view->lobjrecordquitreasonForm = $this->lobjrecordquitreasonForm;
        $this->view->lobjrecorddeferreasonForm= $this->lobjrecorddeferreasonForm;
        $IdUniversity = $this->gobjsessionsis->idUniversity;
        $recconfigDetail = $this->lobjrecordconfigModel->fnfetchConfiguration($IdUniversity);
        if (!empty($recconfigDetail)) {
            $this->view->lobjRecordConfigurationForm->populate($recconfigDetail);
            $recordMsgList = $this->lobjrecordconfigmesgModel->fnfetchAllmessage($recconfigDetail['IdRecordConfig']);
            $this->view->msgList = $recordMsgList;

            //Quit reasons
            $quitreasonsList = $this->lobjquitreasonModel->fnfetchAllReasons($IdUniversity);
            $this->view->quitreasonsList = $quitreasonsList;

            //defer reasons
            $deferreasonsList = $this->lobjrecorddeferreasonModel->fnfetchAllDeferReasons($IdUniversity);
            $this->view->deferreasonsList = $deferreasonsList;
        }
    }

    public function getmodulestatusAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $moduleName = $this->_getParam('modulename');
        $lobjdeftype = new App_Model_Definitiontype();
        $modulestatuslist = $this->lobjdeftype->fnGetDefinationMs($moduleName);
        $modList = array();
        foreach ($modulestatuslist as $module) {
            $module['name'] = $module['value'];
            unset($module['value']);
            $modList[] = $module;
        }
        echo Zend_Json_Encoder::encode($modList);
    }

    public function savedeferreasonsAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->_request->isPost()) { // save opeartion
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            $dateTime = date('Y-m-d H:i:s');
            $formData = $this->_request->getPost();
            $temp = array();
            $regInfoArray = array();
            $IdUniversity = $this->gobjsessionsis->idUniversity;
            $msgDet = $this->lobjrecorddeferreasonModel->fnfetchdeferReasons($IdUniversity);
            $len = count($formData['ReasonDeferGrid']);
            for ($i = 0; $i < $len; $i++) {
                $temp['IdUniversity'] = $IdUniversity;
                $temp['ReasonDefer'] = $formData['ReasonDeferGrid'][$i];
                $temp['DeferDescription'] = $formData['DeferDescriptionGrid'][$i];
                $temp['UpdUser'] = $userId;
                $temp['UpdDate'] = $dateTime;
                $regInfoArray[] = $temp;
            }           
            if (empty($msgDet)) {
                foreach ($regInfoArray as $reg) {
                    $this->lobjrecorddeferreasonModel->fnadddeferreasons($reg);
                }
            } else {
                $msgDet = $this->lobjrecorddeferreasonModel->fndeletedeferReasons($IdUniversity);
                foreach ($regInfoArray as $reg) {
                    $this->lobjrecorddeferreasonModel->fnadddeferreasons($reg);
                }
            }
            exit;
        }
    }

    public function savequitreasonsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->_request->isPost()) { // save opeartion
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            $dateTime = date('Y-m-d H:i:s');
            $formData = $this->_request->getPost();
            $temp = array();
            $regInfoArray = array();
            $IdUniversity = $this->gobjsessionsis->idUniversity;
            $msgDet = $this->lobjquitreasonModel->fnfetchquitReasons($IdUniversity);
            $len = count($formData['ReasonGrid']);
            for ($i = 0; $i < $len; $i++) {
                $temp['IdUniversity'] = $IdUniversity;
                $temp['Reason'] = $formData['ReasonGrid'][$i];
                $temp['QuitDescription'] = $formData['QuitDescriptionGrid'][$i];
                $temp['UpdUser'] = $userId;
                $temp['UpdDate'] = $dateTime;
                $regInfoArray[] = $temp;
            }
            if (empty($msgDet)) {
                foreach ($regInfoArray as $reg) {
                    $this->lobjquitreasonModel->fnaddquitreasons($reg);
                }
            } else {
                $msgDet = $this->lobjquitreasonModel->fndeletequitReasons($IdUniversity);
                foreach ($regInfoArray as $reg) {
                    $this->lobjquitreasonModel->fnaddquitreasons($reg);
                }
            }
            exit;
        }
    }

    public function saverecordstatusAction() {
    	$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->view->lobjRecordConfigurationForm = $this->lobjRecordConfigurationForm;
        if ($this->_request->isPost()) { // save opeartion
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            $dateTime = date('Y-m-d H:i:s');
            $formData = $this->_request->getPost();
            $temp = array();
            $len = count($formData['RecordModuleGrid']);
            $regInfoArray = array();
            $temp = array();

            $recconfigId = '';

            $IdUniversity = $this->gobjsessionsis->idUniversity;
            $recconfigDetail = $this->lobjrecordconfigModel->fnfetchConfiguration($IdUniversity);

            if (empty($recconfigDetail)) {
                $recArray['ApprovalCreditTransfer'] = $formData['ApprovalCreditTransfer'];
                $recArray['RevertCreditTransfer'] = $formData['RevertCreditTransfer'];
                $recArray['EserviceCreditTransfer'] = $formData['EserviceCreditTransfer'];
                $recArray['ApprovalChangeProgram'] = $formData['ApprovalChangeProgram'];
                $recArray['RevertChangeProgram'] = $formData['RevertChangeProgram'];
                $recArray['EserviceChangeProgram'] = $formData['EserviceChangeProgram'];
                $recArray['ApprovalChangeStatus'] = $formData['ApprovalChangeStatus'];
                $recArray['RevertChangeStatus'] = $formData['RevertChangeStatus'];
                $recArray['EserviceChangeStatus'] = $formData['EserviceChangeStatus'];
                $recArray['IdUniversity'] = $this->gobjsessionsis->idUniversity;
                $recArray['UpdUser'] = $userId;
                $recArray['UpdDate'] = $dateTime;
                $recconfigId = $this->lobjrecordconfigModel->fnaddrecordconfiguation($recArray);
            } else {
                $recconfigId = $recconfigDetail['IdRecordConfig'];
                $recArray['ApprovalCreditTransfer'] = $formData['ApprovalCreditTransfer'];
                $recArray['RevertCreditTransfer'] = $formData['RevertCreditTransfer'];
                $recArray['EserviceCreditTransfer'] = $formData['EserviceCreditTransfer'];
                $recArray['ApprovalChangeProgram'] = $formData['ApprovalChangeProgram'];
                $recArray['RevertChangeProgram'] = $formData['RevertChangeProgram'];
                $recArray['EserviceChangeProgram'] = $formData['EserviceChangeProgram'];
                $recArray['ApprovalChangeStatus'] = $formData['ApprovalChangeStatus'];
                $recArray['RevertChangeStatus'] = $formData['RevertChangeStatus'];
                $recArray['EserviceChangeStatus'] = $formData['EserviceChangeStatus'];
                $recArray['IdUniversity'] = $this->gobjsessionsis->idUniversity;
                $recArray['UpdUser'] = $userId;
                $recArray['UpdDate'] = $dateTime;
                $this->lobjrecordconfigModel->fnupdaterecordconfiguation($recArray,$recconfigId);
                $this->lobjrecordconfigmesgModel->fnDeleteRecMesg($recconfigId);
                echo "Saved";
            }

            for ($i = 0; $i < $len; $i++) {
                $temp['RecordModule'] = $formData['RecordModuleGrid'][$i];
                $temp['RecordModuleStatus'] = $formData['RecordModuleStatusGrid'][$i];
                $temp['Email'] = $formData['EmailGrid'][$i];
                $temp['Description'] = $formData['DescriptionGrid'][$i];
                $temp['IdRecordConfig'] = $recconfigId;
                $temp['UpdUser'] = $userId;
                $temp['UpdDate'] = $dateTime;
                $regInfoArray[] = $temp;
            }

            foreach ($regInfoArray as $reg) {
                $this->lobjrecordconfigmesgModel->fnaddrecordconfiguationmsg($reg);
            }
            exit;
        }
    }

}

?>
