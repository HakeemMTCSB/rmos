<?php

class Records_ChangeStatusProcessController extends Base_Base { //Controller for the User Module

	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
		$session = Zend_Registry::get('sis');
		
		//title
		$this->view->title= $this->view->translate("Change Status Processing");		 
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
		
		$form = new Records_Form_ProcessChangeStatusSearch();
		$this->view->form = $form;
		
		$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		
		//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if($form->isValid($formData)){
				
				$db = Zend_Db_Table::getDefaultAdapter();
				
				$this->view->IdProgramScheme = $formData['IdProgramScheme'];
								
			    $student_list = $studentRegDB->getStudentChangeStatus($formData);
					
				$form->populate($formData);
				$this->view->paginator = $student_list;
				
				$session->result = $formData;
			}
		}else{ 
    		
    		//populate by session 
	    	if (isset($session->result)) {    	
	    		$form->populate($session->result);			
	    		$student_list = $studentRegDB->getStudentChangeStatus($session->result);												
				$this->view->paginator = $student_list;		
	    	}
    		
    	}
    	
	}
	
	
	public function processAction()
	{	
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
			
			$studentDB = new Registration_Model_DbTable_Studentregistration();
			$semesterDB = new Records_Model_DbTable_SemesterMaster();
			$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
			$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
			$schemeDB = new GeneralSetup_Model_DbTable_Schemesetup();
			
					
			for($i=0; $i<count($formData['IdStudentRegistration']); $i++){
				
					$IdStudentRegistration = $formData['IdStudentRegistration'][$i];
					$current_semId = $formData['IdSemester'][$IdStudentRegistration];
					$idProgram = $formData['IdProgram'][$IdStudentRegistration];
					
					//get student info
					$student = $studentDB->getRowData($IdStudentRegistration);
				
					//get consecutive_sem
					$scheme = $schemeDB->getProgramSchemeDetails($idProgram);
					$consecutive_sem = $scheme['consecutive_sem'];
				
					//get student previous semester	list	
					$semester_list = $semesterDB->getSemesterByScheme($student,$consecutive_sem);
					
					$not_register = 0;
					
					if(count($semester_list)>0){	
											
						$sem_tbl = '<table border=1>';
						foreach($semester_list as $sem){
							
							//foreach sem get semester status
							$semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$sem['IdSemesterMaster']);
							
							if(!$semester_status){
								
								//update semester status to not registered
								$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
									            'idSemester' => $sem['IdSemesterMaster'],
									            'IdSemesterMain' => $sem['IdSemesterMaster'],								
									            'studentsemesterstatus' => 131, 	//Not Register idDefType = 32 (student semester status)
						                        'Level'=>0,
									            'UpdDate' => date ( 'Y-m-d H:i:s'),
									            'UpdUser' => $auth->getIdentity()->iduser
	       						 );		
								$semesterStatusDB->addData($data);
								
								$not_register = $not_register + 1;
								
								$semester_status = 'Not Register - added new';
								
							}else{
								
								if($semester_status['studentsemesterstatus']==131){
									$not_register = $not_register + 1;
								}
								$semester_status = $semester_status['studentsemesterstatus'];
							}
							
							$sem_tbl .= '<tr>';
							$sem_tbl .= '<td>'.$sem['IdSemesterMaster'].'</td>';
							$sem_tbl .= '<td>'.$sem['SemesterMainName'].'</td>';
							$sem_tbl .= '<td>'.$sem['SemesterMainStartDate'].'</td>';
							$sem_tbl .= '<td>'.$semester_status.'</td>';
							$sem_tbl .= '</tr>';
							
						}//end foreach semester
						
						$sem_tbl .= '</table><br>';
						
						echo $sem_tbl;
												
					}//end if semester
					
					echo '<br>:::'.$not_register.'<br>';
					
					if($not_register >= $consecutive_sem){
						
						//set this semester student semester status as dormant
						echo '$not_register > $consecutive_sem => set current semester status =  DORMANT';
						//$this->setSemesterStatus($IdStudentRegistration,$current_semId,844);
						
					}else{
						
						//check any course registered at current semester
						$subjects = $studentRegSubjectDB->getTotalRegisteredBySemester($IdStudentRegistration,$current_semId);
						echo 'check any course registered at current semester';
							
						if(count($subjects)>0){
							//there are registered subject														
							//set current semester status =  REGISTERED
							echo 'there are registered subject. set current semester status =  REGISTERED';
							//$this->setSemesterStatus($IdStudentRegistration,$current_semId,130);
							
						}else{
							//no subject registered
							//check if on previous sem, student semester status = DORMANT
							echo 'no subject registered => check if on previous sem status ';
							$this->checkDormantStatus($IdStudentRegistration,$current_semId); 
						}
					}
					
			
			}//end for
		
			
			
    	}
    	exit;
	}
	
	
	public function checkDormantStatus($IdStudentRegistration,$current_semId){
		
		//get student previous semester status
		$prev_sem_status = '';
		
		$studentSemesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
		
		$prev_sem = $studentSemesterStatusDB->getStudentPreviousSemester($IdStudentRegistration);
		
		if(isset($prev_sem) && $prev_sem['studentsemesterstatus']==844){//Dormant
			//set current semester status =  DORMANT
			echo 'prev sem : dormant => set current semester status =  DORMANT';
			//$this->setSemesterStatus($IdStudentRegistration,$current_semId,844);
			
		}else{
			//set current semester status =  NOT REGISTERED
			echo 'prev sem != dormant set current semester status =  NOT REGISTERED';
			//$this->setSemesterStatus($IdStudentRegistration,$current_semId,131);
		}
		
	}
	
	public function setSemesterStatus($IdStudentRegistration,$IdSemester,$status){
		
		
		$auth = Zend_Auth::getInstance();
		
		$cmsStatus = new Cms_Status();		
		$studentSemesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
		
		//update semester status to not registered
		$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
			            'idSemester' => $IdSemester,
			            'IdSemesterMain' => $IdSemester,								
			            'studentsemesterstatus' => $status, 
                        'Level'=>0,
			            'UpdDate' => date ( 'Y-m-d H:i:s'),
			            'UpdUser' => $auth->getIdentity()->iduser
       	 );		
	       						 
		$cmsStatus->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemester,$data,$message='Semester Status Process',$type=2);
		
	}
	
	public function getProgramSchemeAction()
	{	
    	$idProgram = $this->_getParam('idProgram', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $programDb = new Registration_Model_DbTable_Program();
        $result = $programDb->getProgramSchemeList($idProgram);
      	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
}

?>