<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 26/10/2015
 * Time: 4:03 PM
 */
class Records_IdCollectionController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_IdCollection();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('ID Card Collection');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $form = new Records_Form_IdCollectionSearch(array('programid'=>$formData['Program']));
                $this->view->form = $form;

                $list = $this->model->getIdCollection($formData);
                $this->view->list = $list;

                $form->populate($formData);
            }else{
                $form = new Records_Form_IdCollectionSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Records_Form_IdCollectionSearch();
            $this->view->form = $form;
        }
    }

    public function exportAction(){
        $this->_helper->layout->disableLayout();
        $this->view->role = $this->auth->getIdentity()->IdRole;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->getIdCollection($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_Id_Collection.xls';
    }

    public function updateStatusAction(){
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['send']) && count($formData['send']) > 0) {
                foreach ($formData['send'] as $key => $send) {
                    $data = array(
                        'idc_status' => $send
                    );
                    $this->model->updateIdCardCollection($data, $key);
                }
            }

            if (isset($formData['tno']) && count($formData['tno']) > 0) {
                foreach ($formData['tno'] as $key2 => $tno){
                    $data = array(
                        'idc_trackingno'=>$tno
                    );
                    $this->model->updateIdCardCollection($data, $key2);
                }
            }
        }

        $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));

        //redirect here
        $this->_redirect($this->baseUrl . '/records/id-collection/index/');
    }
}