<?php
class Records_StudentstatusController extends Base_Base { //Controller for the User Module

	private $lobjStudentprofileForm;
	private $lobjStudentprofile;
	private $lobjUser;
	public function init() { //initialization function
		$this->fnsetObj();	
		 
	}
	public function fnsetObj(){
		
		$this->lobjStudentprofileForm = new Records_Form_Studentstatus();
		$this->lobjStudentprofile = new Records_Model_DbTable_Studentstatus();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		
		$lobjApplicantNameList = $this->lobjStudentprofile->fnGetApplicantNameList();
		$lobjform->field5->addMultiOptions($lobjApplicantNameList);
		
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); 
		$ProgramList=$lobjplacementtest->fnGetProgramMaterList();
		$lobjform->field8->addMultiOptions($ProgramList);
		
		
		$larrresult = $this->lobjStudentprofile->fngetStudentprofileDtls(); //get user details
		
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->studentprofilepaginatorresult);
   	    	
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->studentprofilepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->studentprofilepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentprofile ->fnSearchStudentApplication( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentprofilepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/records/studentstatus/index');
		}

	}

	public function studentstatusAction() { //Action for the updation and view of the  details		
		$this->view->lobjStudentprofileForm = $this->lobjStudentprofileForm;		 
		$lintIdApplication = ( int ) $this->_getParam ( 'id' );
		$this->view->IdApplication = $lintIdApplication;		
		$larrresult = $this->lobjStudentprofile->fnViewStudentAppnDetails($lintIdApplication);
		$this->lobjdeftype = new App_Model_Definitiontype();
		$DetainAction = $this->lobjdeftype->fnGetDefinations('Detain Action');	
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjStudentprofileForm->UpdDate->setValue ( $ldtsystemDate );
		$this->view->lobjStudentprofileForm->IdApplication->setValue ( $lintIdApplication );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjStudentprofileForm->UpdUser->setValue($auth->getIdentity()->iduser);
		foreach($DetainAction as $larrdefmsresult) {
			$this->lobjStudentprofileForm->Action->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}	
		$statusArray = $this->lobjStudentprofile->fnViewStudentStatusDetails($lintIdApplication);
		$editCheck = 0;
		if(count($statusArray['IdApplication']) == 1){
				$this->view->lobjStudentprofileForm->Action->setValue($statusArray['IdAction']);
				$this->view->lobjStudentprofileForm->ToEffectiveDate->setValue($statusArray['ToEffectiveDate']);
				$this->view->lobjStudentprofileForm->Comments->setValue($statusArray['Comments']);
				$editCheck = $statusArray['idStudentDetain'];
		}
		if($larrresult) {
			$this->view->StudentName = $larrresult['FName'].' '.$larrresult['MName'].' '.$larrresult['LName'];
			$this->view->SemesterMasterName	 = $larrresult['SemesterMasterName'];	
			$this->view->IdApplication = $larrresult['IdApplication'];
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			$larrformData['IdSemester'] = $larrresult['IdSemester'];
			$larrformData['IdAction'] = $larrformData['Action'];
		
			if ($this->_request->isPost ()) {			
				unset ( $larrformData ['Save'] );
				unset ( $larrformData ['Action'] );
				if ($this->lobjStudentprofileForm->isValid ( $larrformData )) {						
					if($editCheck == 0){ 
						$insertId  = $this->lobjStudentprofile->fnSaveDetails($larrformData);
					}
					else{ 
						$this->lobjStudentprofile->fnUpdateDetails($larrformData,$editCheck);
					}
					$this->lobjStudentprofile->fnUpdateRegistrationDetails($larrresult['IdApplication'],$larrresult['IdSemester']); 						
					$this->_redirect( $this->baseUrl . '/records/studentstatus/index');
				}
			}
		}
		/*
		$AcademicLandscapeDtls = $this->lobjStudentprofile->fnViewAcademicLandscapeDtls($lintIdApplication);
		
		if($AcademicLandscapeDtls[0]['LandscapeType']==44){			
			$AcademicLandscapeBlockSubjectDtls = $this->lobjStudentprofile->fnViewAcademicLandscapeBlockSubjectDtls($lintIdApplication);
			$this->view->AcademicLandscapeDtls=$AcademicLandscapeBlockSubjectDtls;			
		}else if($AcademicLandscapeDtls[0]['LandscapeType']==43){			
			$AcademicLandscapeSubjectDtls = $this->lobjStudentprofile->fnViewAcademicLandscapeSubjectDtls($lintIdApplication);
			$this->view->AcademicLandscapeDtls=$AcademicLandscapeSubjectDtls;			
		}
		
		if($larrresult) {
		$this->view->ProgramName = $larrresult['ProgramName'];
		$this->view->CollegeName = $larrresult['CollegeName'];
		$this->view->SemesterMasterName	 = $larrresult['SemesterMasterName'];
		$this->view->year = $larrresult['year'];
		$this->view->IdApplication = $larrresult['IdApplication'];
		
		$lobjcountry =$this->lobjUser->fnGetCountryList();
		$this->lobjStudentprofileForm->Nationality->addMultiOptions($lobjcountry);
		
		$lobjcountry =$this->lobjUser->fnGetCountryList();
		$this->lobjStudentprofileForm->PermCountry->addMultiOptions($lobjcountry);
		
		$lobjState =$this->lobjUser->fnGetStateList();
		$this->lobjStudentprofileForm->PermState->addMultiOptions($lobjState);
		
		$lobjcountryList =$this->lobjUser->fnGetCountryList();
		$this->lobjStudentprofileForm->CorrsCountry->addMultiOptions($lobjcountryList);
		
		$lobjStateList =$this->lobjUser->fnGetStateList();
		$this->lobjStudentprofileForm->CorrsState->addMultiOptions($lobjStateList);
		
		$lobjCommonModel = new App_Model_Common();
		

		$larrStateCityList = $lobjCommonModel->fnGetCityList($larrresult['PermState']);		
		//$this->lobjStudentprofileForm->PermCity->addMultiOptions($larrStateCityList);	
		$this->view->PermCity  =  $larrresult['PermCity'];
	

		if($larrresult['CorrsState']){
		$larrStateCityList = $lobjCommonModel->fnGetCityList($larrresult['CorrsState']);
	
		$this->lobjStudentprofileForm->CorrsCity->addMultiOptions($larrStateCityList);
		}
		
		$this->lobjStudentprofileForm->populate($larrresult);

		if(isset($larrresult ['HomePhone']) && !empty($larrresult ['HomePhone']) ){
		
			$arrHomePhone = explode("-",$larrresult ['HomePhone']);
			$this->view->lobjStudentprofileForm->HomePhonecountrycode->setValue ( $arrHomePhone[0] );
			$this->view->lobjStudentprofileForm->HomePhonestatecode->setValue ( $arrHomePhone[1] );
			$this->view->lobjStudentprofileForm->HomePhone->setValue ( $arrHomePhone[2] );
		}
		if(isset($larrresult ['CellPhone']) && !empty($larrresult ['CellPhone']) ){
			$arrCellPhone = explode("-",$larrresult ['CellPhone']);
			$this->view->lobjStudentprofileForm->CellPhonecountrycode->setValue ( $arrCellPhone[0] );
			$this->view->lobjStudentprofileForm->CellPhonestatecode->setValue ( $arrCellPhone[1] );
			if (@$arrCellPhone[2])$this->view->lobjStudentprofileForm->CellPhone->setValue ( $arrCellPhone[2] );
		}
		if(isset($larrresult ['Fax']) && !empty($larrresult ['Fax']) ){
			$arrFax = explode("-",$larrresult ['Fax']);
			$this->view->lobjStudentprofileForm->Faxcountrycode->setValue ( $arrFax[0] );
			$this->view->lobjStudentprofileForm->Faxstatecode->setValue ( $arrFax[1] );
			$this->view->lobjStudentprofileForm->Fax->setValue ( $arrFax[2] );
		}	
		/*foreach($larrresult as $larrresult) {
		$this->view->StudentName = $larrresult['FName'].' '.$larrresult['MName'].' '.$larrresult['LName'];
		$this->view->DateOfBirth = date('d-m-Y',strtotime($larrresult['DateOfBirth']));
		}*/
		/*}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjStudentprofileForm->isValid ( $larrformData )) {
						
					$lintiIdApplication = $larrformData ['IdApplication'];
					 $this->lobjStudentprofile->fnupdateStudentAppn($lintiIdApplication, $larrformData );
					//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
					$this->_redirect( $this->baseUrl . '/records/studentstatus/index');
				}
			}
		}*/
		$this->view->lobjStudentprofileForm = $this->lobjStudentprofileForm;
	}

	
}