<?php

class Records_VisaMigrateController extends Base_Base { //Controller for the User Module
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_VisaMigrate();
    }
    
    public function indexAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        echo 'Please Wait.....  <br/>';
        
        $migrateList = $this->model->getMigrateData();
        
        if ($migrateList){
            foreach ($migrateList as $migrateLoop){
                $passportData = array(
                    'sp_id'=>$migrateLoop['sp_id'], 
                    'p_passport_no'=>$migrateLoop['vm_passport_no'], 
                    'p_country_issue'=>$migrateLoop['appl_nationality'], 
                    'p_expiry_date'=>$migrateLoop['vm_passexp_dt'], 
                    'p_active'=>0, 
                    'p_createddt'=>date('Y-m-d H:i:s'), 
                    'p_createdby'=>1
                );
                $p_id = $this->model->insertStudentPassport($passportData);
                
                if ($migrateLoop['vm_studentpass_dt1'] != '0000-00-00'){
                   $visaData1 = array(
                       'sp_id'=>$migrateLoop['sp_id'], 
                       'av_trans_id'=>$migrateLoop['transaction_id'], 
                       'av_status'=>0, 
                       'av_expiry'=>$migrateLoop['vm_studentpass_dt1'], 
                       'upd_date'=>date('Y-m-d H:i:s'), 
                       'upd_by'=>1, 
                       'p_id'=>$p_id, 
                       'av_reference_no'=>$migrateLoop['vm_studentpass_ref'], 
                       'av_active'=>0, 
                   );
                   $this->model->insertStudentVisa($visaData1);
                }
                
                if ($migrateLoop['vm_studentpass_dt2'] != '0000-00-00'){
                   $visaData2 = array(
                       'sp_id'=>$migrateLoop['sp_id'], 
                       'av_trans_id'=>$migrateLoop['transaction_id'], 
                       'av_status'=>0, 
                       'av_expiry'=>$migrateLoop['vm_studentpass_dt2'], 
                       'upd_date'=>date('Y-m-d H:i:s'), 
                       'upd_by'=>1, 
                       'p_id'=>$p_id, 
                       'av_reference_no'=>$migrateLoop['vm_studentpass_ref'], 
                       'av_active'=>0, 
                   );
                   $this->model->insertStudentVisa($visaData2);
                }
                
                if ($migrateLoop['vm_studentpass_dt3'] != '0000-00-00'){
                   $visaData3 = array(
                       'sp_id'=>$migrateLoop['sp_id'], 
                       'av_trans_id'=>$migrateLoop['transaction_id'], 
                       'av_status'=>0, 
                       'av_expiry'=>$migrateLoop['vm_studentpass_dt3'], 
                       'upd_date'=>date('Y-m-d H:i:s'), 
                       'upd_by'=>1, 
                       'p_id'=>$p_id, 
                       'av_reference_no'=>$migrateLoop['vm_studentpass_ref'], 
                       'av_active'=>0, 
                   );
                   $this->model->insertStudentVisa($visaData3);
                }
                
                if ($migrateLoop['vm_studentpass_dt4'] != '0000-00-00'){
                   $visaData4 = array(
                       'sp_id'=>$migrateLoop['sp_id'], 
                       'av_trans_id'=>$migrateLoop['transaction_id'], 
                       'av_status'=>0, 
                       'av_expiry'=>$migrateLoop['vm_studentpass_dt4'], 
                       'upd_date'=>date('Y-m-d H:i:s'), 
                       'upd_by'=>1, 
                       'p_id'=>$p_id, 
                       'av_reference_no'=>$migrateLoop['vm_studentpass_ref'], 
                       'av_active'=>0, 
                   );
                   $this->model->insertStudentVisa($visaData4);
                }
                
                if ($migrateLoop['vm_studentpass_dt5'] != '0000-00-00'){
                   $visaData5 = array(
                       'sp_id'=>$migrateLoop['sp_id'], 
                       'av_trans_id'=>$migrateLoop['transaction_id'], 
                       'av_status'=>0, 
                       'av_expiry'=>$migrateLoop['vm_studentpass_dt5'], 
                       'upd_date'=>date('Y-m-d H:i:s'), 
                       'upd_by'=>1, 
                       'p_id'=>$p_id, 
                       'av_reference_no'=>$migrateLoop['vm_studentpass_ref'], 
                       'av_active'=>0, 
                   );
                   $this->model->insertStudentVisa($visaData5);
                }
                
                if ($migrateLoop['vm_studentpass_dt6'] != '0000-00-00'){
                   $visaData6 = array(
                       'sp_id'=>$migrateLoop['sp_id'], 
                       'av_trans_id'=>$migrateLoop['transaction_id'], 
                       'av_status'=>0, 
                       'av_expiry'=>$migrateLoop['vm_studentpass_dt6'], 
                       'upd_date'=>date('Y-m-d H:i:s'), 
                       'upd_by'=>1, 
                       'p_id'=>$p_id, 
                       'av_reference_no'=>$migrateLoop['vm_studentpass_ref'], 
                       'av_active'=>0, 
                   );
                   $this->model->insertStudentVisa($visaData6);
                }
            }
        }
        
        echo 'Migration Done .....  <br/>';
        exit;
    }
}
?>