<?php
class Records_DisciplinaryactionController extends Base_Base { //Controller for the Create Appraisal Module 
	
	private $_gobjlog;
	public function init(){
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();			 
	}
	public function fnsetObj(){
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$idUniversity = $this->gstrsessionSIS->idUniversity;
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();		
		$this->lobjdisciplinaryactionmodel = new Application_Model_DbTable_Disciplinaryaction();		
	}

	
	// action for search and view
	public function indexAction() { 
		$lobjSearchForm = 	$this->view->lobjSearchForm = $this->lobjform; 		
		$lobjSearchForm->field17->setAttrib("OnChange","fnGetDetails");			
		//Initialize Hostel Master Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;
		
		$larrDisciplinaryactionList = $lobjDisciplinaryactionModel->fnGetDisciplinaryActionTypeList();
		$this->view->lobjSearchForm->field1->addMultiOptions($larrDisciplinaryactionList);
		$this->lobjSubjectsMarksEntrynModelbulk = new Examination_Model_DbTable_Subjectsmarksentry();
			$larSemesterNameCombo = $lobjDisciplinaryactionModel->fngetSemesterNameCombo();	
		$this->view->lobjSearchForm->field10->addMultiOptions($larSemesterNameCombo);
		//Get Disciplinary Action Details
		$lobjDisciplinaryActionDetails = $lobjDisciplinaryactionModel->fnGetStudentDisciplinaryActionDetails();
		
		//Disciplinary Action Details - Pagination
		$lintpagecount = $this->gintPageCount;  // Records per page		
		$lobjPaginator = new App_Model_Definitiontype(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsams->disciplinaryactionpaginatorresult)) {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($this->gobjsessionsams->disciplinaryactionpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($lobjDisciplinaryActionDetails,$lintpage,$lintpagecount);
		}		
		//Disciplinary Action Search
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjSearchForm->isValid ( $larrformData )) {
				$larrSearchResult = $lobjDisciplinaryactionModel->fnSearchDsiciplinaryActionDetails( $larrformData );
				
				//Disciplinary Action Details Search - Pagination
				$this->view->lobjPaginator = $lobjPaginator->fnPagination($larrSearchResult,$lintpage,$lintpagecount);
		    	$this->gobjsessionsams->disciplinaryactionpaginatorresult = $larrSearchResult;	    	
	
			}
		}		
		//Disciplinary Action Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/records/disciplinaryaction/index');
			
		}
	}
	
	//Action To Save Disciplinary Action Details 
	public function newstudentdisciplinaryAction(){
		//Pass BaseUrl & Module Name To View
		
		$lintIdStudent = "";
		$lsrtAlertMsg1 = "";$lsrtAlertMsg2 = "";

		$auth = Zend_Auth::getInstance();			
		//Get Vales For UpdUser & UpdDate
		$lstrUpdUser = $auth->getIdentity()->iduser;
		$lstrUpdDate = date('Y-m-d h:m:s');		
		//Initial Disciplinary Form
		$lobjDisciplinaryactionForm = new Application_Form_Disciplinaryaction();
		$this->view->lobjDisciplinaryactionForm = $lobjDisciplinaryactionForm;		
		
		$fnGetStudentsList = $this->lobjdisciplinaryactionmodel->fnGetStudentsList();	
		$this->view->lobjDisciplinaryactionForm->IdStudent->addMultiOptions($fnGetStudentsList);
		$this->view->lobjDisciplinaryactionForm->IdStudent->setAttrib('required',"true");
 		
		$lobjDisciplinaryactionForm->IdDispciplinaryActionType->setAttrib('onChange','fnGetDisciplianryActionMailSettings(this.value)');
		
		//Set UpdUser , UpdDate & Default Disciplinary Emailing Address Values
		$lobjDisciplinaryactionForm->UpdUser->setValue($lstrUpdUser);
		$lobjDisciplinaryactionForm->UpdDate->setValue(date('Y-m-d',strtotime($lstrUpdDate)));
		
		$lobjDisciplinaryactionForm->DateOfMistake->setValue(date('Y-m-d'));
		//Initilial Disciplinary Action Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;
		
		$lintIdDefType = $lobjDisciplinaryactionModel->fnGetDefinitionTypeId();
		$this->view->GroupType = $lintIdDefType;
		
		$lobjDisciplinaryactionForm->HiddenRadioGroup->setValue("Name");
		
		//Get Disciplinary Actions List
		$larrDisciplinaryactionList = $lobjDisciplinaryactionModel->fnGetDisciplinaryActionTypeList();
			
		$lobjDisciplinaryactionForm->IdDispciplinaryActionType->addMultiOptions($larrDisciplinaryactionList);
		$lobjDisciplinaryactionForm->ReportingDate->setValue(date('d-m-Y'));
		
		$this->lobjSubjectsMarksEntrynModelbulk = new Examination_Model_DbTable_Subjectsmarksentry();
		$larSubjectNameCombo = $this->lobjSubjectsMarksEntrynModelbulk->fngetSubjectNameCombo();	
		$lobjDisciplinaryactionForm->idSubject->addMultiOptions($larSubjectNameCombo);
		
		$larSemesterNameCombo = $lobjDisciplinaryactionModel->fngetSemesterNameCombo();	
		$lobjDisciplinaryactionForm->idSemester->addMultiOptions($larSemesterNameCombo);
		$lobjDisciplinaryactionForm->SemesterId->addMultiOptions($larSemesterNameCombo);
		
		$larActionTypeCombo = $lobjDisciplinaryactionModel->fngetDisceplinaryActionCombo();	
		$lobjDisciplinaryactionForm->DisplinaryAction->addMultiOptions($larActionTypeCombo);	
		
		//Disciplinary Action Update Operation
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();
			//echo "<pre>";print_r($larrFormData);
			unset($larrFormData['Save']);
			if ($lobjDisciplinaryactionForm->isValid($larrFormData)) {	
				

				
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Disciplinary Action Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				
			$lstruploaddir = "/documents/Disciplinary/";	   	
	   		for($lintii = 0;$lintii<count($_FILES['uploadeddocuments']['name']);$lintii++)
			{  	
				if($_FILES['uploadeddocuments']['error'][$lintii] != UPLOAD_ERR_NO_FILE)
				{
					$lstrfilename = pathinfo(basename($_FILES['uploadeddocuments']['name'][$lintii]), PATHINFO_FILENAME);
					$lstrext = pathinfo(basename($_FILES['uploadeddocuments']['name'][$lintii]), PATHINFO_EXTENSION);
					
					$username = "Disciplinary";//empty($lobjFormData['subjid'][$i])?"":$lobjFormData['applicantid']."_";
					
					$filename = $username."_".date('YmdHis').".".$lstrext;
					$filename = str_replace(' ','_',$lstrfilename)."_".$filename;	
					$file = realpath('.').$lstruploaddir . $filename;
					
					if (move_uploaded_file($_FILES['uploadeddocuments']['tmp_name'][$lintii], $file)) 
					{
						echo "success";
					} 
					else 
					{
			    		echo "error";
					}
				}
	  		}
				
				
				$lstrMsg = $lobjDisciplinaryactionModel->fnSaveDisciplinaryActionDetails($lobjDisciplinaryactionForm->getValues(),$filename,$larrFormData);
				
				$lintstudentId = $larrFormData["IdStudent"];
				
				//Send Email To Student
				$lstrMailingMsg = $lobjDisciplinaryactionModel->fnSendEmailToStudent($larrFormData["IdStudent"],"Disciplinary Action");
		
				$lsrtAlertMsg1 = $this->view->translate("Failed")." ".$this->view->translate->_("To")." ".$this->view->translate->_("Send");
				$lsrtAlertMsg1 .= " ".$this->view->translate("Mail").". ".$this->view->translate("Please")." ".$this->view->translate("Check");
				$lsrtAlertMsg1 .= " ".$this->view->translate("Your")." ".$this->view->translate("Mailling")." ".$this->view->translate("Credentials");
				
				$lsrtAlertMsg2 = $this->view->translate("Failed")." ".$this->view->translate->_("To")." ".$this->view->translate->_("Send");
				$lsrtAlertMsg2 .= " ".$this->view->translate("Mail").". ".$this->view->translate("Please")." ".$this->view->translate("Create");
				$lsrtAlertMsg2 .= " ".$this->view->translate("The")." ".$this->view->translate("Email")." ".$this->view->translate("Template");
				
				if($lstrMailingMsg == "error"){
					echo "<script>alert('".$lsrtAlertMsg1."');</script>";
				}else if($lstrMailingMsg == "No Template Found"){
					echo "<script>alert('".$lsrtAlertMsg2."');</script>";
				}				
				if($lstrMsg == "Exceeded"){
					echo "<script> 
					          varstudentId = '$lintstudentId';
				        	  fnAskToTerminateStudent(varstudentId); 
				     	</script>";	
				}else{
					$this->_redirect( $this->baseUrl . '/records/disciplinaryaction/index');
				}			
				
			}
		}
	}
	
	//Action To Update Student Disciplinary Action Details
	public function updatestudentdisciplinaryAction(){
		
		
		$lsrtAlertMsg1 = "";$lsrtAlertMsg2 = "";
		
		//Get Disciplinary Action Id
		$lintIdDisciplinary = $this->_getParam('IdDisciplinary');
		$this->view->IdDisciplinary = $lintIdDisciplinary;
		
		
		//Get Student Name
		$lstrStudentName = $this->_getParam('StudentName');
		$this->view->StudentName = $lstrStudentName;
		
		//Get Student Id
		$lstrStudentId = $this->_getParam('StudentId');
		$this->view->Stdid = $lstrStudentId;
		
		$lobjDisciplinaryactionForm = new Application_Form_Disciplinaryaction();
		
		$this->view->lobjDisciplinaryactionForm = $lobjDisciplinaryactionForm; //send the lobjuserForm object to the view
		
		
		//Initialize SAM Common Model
 	//	$lobjSAMCommonModel = new Sam_Model_Samcommon();
		
		//Initialize Disciplinary Action Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;
		
		$lintIdDefType = $lobjDisciplinaryactionModel->fnGetDefinitionTypeId();
		$this->view->GroupType = $lintIdDefType;
			
		//Get Students List
		$larrStudentsList = $lobjDisciplinaryactionModel->fnGetStudentsList();
		
		//Get Disciplinary Actions List
		$larrDisciplinaryactionList = $lobjDisciplinaryactionModel->fnGetDisciplinaryActionTypeList();
		
		$lstrIdStudent = $lobjDisciplinaryactionModel->fnGetStudentId($lstrStudentId);
		
		//Get Student Disciplinary Action Details
		$lobjStudentDisciplinaryactionDetails = $lobjDisciplinaryactionModel->fnGetStudentDisciplinaryActionDetailsById($lintIdDisciplinary);
	
		//Populate all the values to the form according to id
		$lobjDisciplinaryactionForm->populate($lobjStudentDisciplinaryactionDetails);
		
		
		//Set The Values to Respective Fields
		$lobjDisciplinaryactionForm->StudentName->setValue($lstrStudentName);
		$lobjDisciplinaryactionForm->ReadOnlyField->setValue($lstrIdStudent);
		$this->view->IdStudent = $lobjStudentDisciplinaryactionDetails['IDApplication'];
		$this->view->IdDispciplinaryActionType = $lobjStudentDisciplinaryactionDetails['IdDispciplinaryActionType'];
		$lobjDisciplinaryactionForm->IdDispciplinaryActionType->addMultiOptions($larrDisciplinaryactionList);
		$lobjDisciplinaryactionForm->IdDispciplinaryActionType->setValue($lobjStudentDisciplinaryactionDetails['IdDispciplinaryActionType']);
		if($lobjStudentDisciplinaryactionDetails['PenaltyAmount'] == "0.00"){
			$lobjDisciplinaryactionForm->PenaltyAmount->setValue("");
		}else{
			$lobjDisciplinaryactionForm->PenaltyAmount->setValue($lobjStudentDisciplinaryactionDetails['PenaltyAmount']);
		}
		
		if($lobjStudentDisciplinaryactionDetails['UploadNotice'] == "NULL" || $lobjStudentDisciplinaryactionDetails['UploadNotice'] == ""){
			$this->view->lstrFileExists = "";
		}else{
			$this->view->lstrFileExists = $lobjStudentDisciplinaryactionDetails['UploadNotice'];
		}
		
			
		if($lobjStudentDisciplinaryactionDetails['DiscMsgCollegeHead'] == "1"){
			$lobjDisciplinaryactionForm->DiscMsgCollegeHead->setAttrib('checked',true);
		}
		
		if($lobjStudentDisciplinaryactionDetails['DiscMsgParent'] == "1"){
			$lobjDisciplinaryactionForm->DiscMsgParent->setAttrib('checked',true);
		}
		
		if($lobjStudentDisciplinaryactionDetails['DiscMsgSponor'] == "1"){
			$lobjDisciplinaryactionForm->DiscMsgSponor->setAttrib('checked',true);
		}
		
		if($lobjStudentDisciplinaryactionDetails['DiscMsgRegistrar'] == "1"){
			$lobjDisciplinaryactionForm->DiscMsgRegistrar->setAttrib('checked',true);
		}
		
		$lobjDisciplinaryactionForm->IdDispciplinaryActionType->setAttrib('disabled','disabled');
		
		$this->lobjSubjectsMarksEntrynModelbulk = new Examination_Model_DbTable_Subjectsmarksentry();
		$larSubjectNameCombo = $this->lobjSubjectsMarksEntrynModelbulk->fngetSubjectNameCombo();	
		$lobjDisciplinaryactionForm->idSubject->addMultiOptions($larSubjectNameCombo);
		
		$larSemesterNameCombo = $lobjDisciplinaryactionModel->fngetSemesterNameCombo();	
		$lobjDisciplinaryactionForm->idSemester->addMultiOptions($larSemesterNameCombo);
		$lobjDisciplinaryactionForm->SemesterId->addMultiOptions($larSemesterNameCombo);
		
		$larActionTypeCombo = $lobjDisciplinaryactionModel->fngetDisceplinaryActionCombo();	
		$lobjDisciplinaryactionForm->DisplinaryAction->addMultiOptions($larActionTypeCombo);	
		
		$larrresult = $lobjDisciplinaryactionModel->fnViewDistributionDetails($lintIdDisciplinary);
		$this->view->larrresult=$larrresult;
		
		//Disciplinary Action Update
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			
			
			
			$lstruploaddir = "/documents/Disciplinary/";	   	
	   		for($lintii = 0;$lintii<count($_FILES['uploadeddocuments']['name']);$lintii++)
			{  	
				if($_FILES['uploadeddocuments']['error'][$lintii] != UPLOAD_ERR_NO_FILE)
				{
					$lstrfilename = pathinfo(basename($_FILES['uploadeddocuments']['name'][$lintii]), PATHINFO_FILENAME);
					$lstrext = pathinfo(basename($_FILES['uploadeddocuments']['name'][$lintii]), PATHINFO_EXTENSION);
					
					$username = "Disciplinary";//empty($lobjFormData['subjid'][$i])?"":$lobjFormData['applicantid']."_";
					
					$filename = $username."_".date('YmdHis').".".$lstrext;
					$filename = str_replace(' ','_',$lstrfilename)."_".$filename;	
					$file = realpath('.').$lstruploaddir . $filename;
					
					if (move_uploaded_file($_FILES['uploadeddocuments']['tmp_name'][$lintii], $file)) 
					{
						echo "success";
						/*$documentnames[$lintii]['uploadedfilename'] = $filename;
						$documentnames[$lintii]['filename'] = $lstrfilename;
						$documentnames[$lintii]['type'] = $_FILES['uploadeddocuments']['type'][$subid][$lintii];
						$documentnames[$lintii]['size'] = $_FILES['uploadeddocuments']['size'][$subid][$lintii];
						$documentnames[$lintii]['UpdDate'] = $lobjFormData['UpdDate'];
						$documentnames[$lintii]['UpdUser'] = $lobjFormData['UpdUser'];
						$documentnames[$lintii]['FileLocation'] = $lstruploaddir;
						$documentnames[$lintii]['Comments'] = $lobjFormData['comments'][$subid][$lintii];
						//$documentnames[$lintii]['documentcategory'] = $lobjFormData['documentcategory'][$subid][$lintii];	
						$documentnames[$lintii]['documentcategory'] = 0;*/		
					
						//if(count($documentnames) > 0)$this->lobjCredittransfer->fnaddCreditTransferDocumentDetails($documentnames,$lastsubjcreditId);
					} 
					else 
					{
			    		echo "error";
					}
				}
	  		}
			
			
			
			
			$lstrMsg = $lobjDisciplinaryactionModel->fnUpdateDisciplinaryActionDetails($larrformData['IdDisciplinaryAction'],$larrformData, $lobjDisciplinaryactionForm->getValues(),$filename );
			
			$lintstudentId = $larrformData["IdStudent"];
			//Initialize SAM Common Model
 			//$lobjSAMCommonModel = new Sam_Model_Samcommon();
			//$lstrOfflineMsgSysStatus = $lobjSAMCommonModel->fnCheckOfflineMessagingStatus("Disciplinary Action");
			
			//Send Email To Student			
			$lstrMailingMsg = $lobjDisciplinaryactionModel->fnSendEmailToStudent($larrformData["IdStudent"],"Disciplinary Action");
		
			$lsrtAlertMsg1 = $this->view->translate("Failed")." ".$this->view->translate->_("To")." ".$this->view->translate->_("Send");
				$lsrtAlertMsg1 .= " ".$this->view->translate("Mail").". ".$this->view->translate("Please")." ".$this->view->translate("Check");
				$lsrtAlertMsg1 .= " ".$this->view->translate("Your")." ".$this->view->translate("Mailling")." ".$this->view->translate("Credentials");
				
				$lsrtAlertMsg2 = $this->view->translate("Failed")." ".$this->view->translate->_("To")." ".$this->view->translate->_("Send");
				$lsrtAlertMsg2 .= " ".$this->view->translate("Mail").". ".$this->view->translate("Please")." ".$this->view->translate("Create");
				$lsrtAlertMsg2 .= " ".$this->view->translate("The")." ".$this->view->translate("Email")." ".$this->view->translate("Template");
				
			if($lstrMailingMsg == "error"){
				echo "<script>alert('".$lsrtAlertMsg1."');</script>";
			}else if($lstrMailingMsg == "No Template Found"){
				echo "<script>alert('".$lsrtAlertMsg2."');</script>";
			}
				
			/*if($lstrOfflineMsgSysStatus == "Set"){
				$larrMessage = array(
										"title" => "Disciplinary Action",
										"message" => "Check Your Discplinary Action For Complete Details",
										"from" => $larrformData["UpdUser"],
										"to" => $larrformData["IdStudent"],
									);
				$lobjSAMCommonModel->fnSendOfflineMessageToStudent($larrMessage);
			}*/
			if($lstrMsg == "Exceeded"){
					echo "<script> 
					          varstudentId = '$lintstudentId';
				        	  fnAskToTerminateStudent(varstudentId); 
				     	</script>";	
				}else{
					$this->_redirect($this->baseUrl.'/records/disciplinaryaction/index');
				}
			$this->_redirect($this->baseUrl.'/records/disciplinaryaction/index');
		}
	}
	
	//Action To Download File
	public function downloadfileAction(){
		//disable layout
		$this->_helper->layout->disableLayout(); 
  		
		//disable view
		$this->_helper->viewRenderer->setNoRender(true);
		
		//Get File Name To Download
		$lstrfilename = $this->_getParam('file');
		
		$larrfilename = explode('.', $lstrfilename);
		
		$lstrfilepath = realpath('.')."/documents/Disciplinary/$lstrfilename";
		
		header("Content-type: application/octet-stream;charset=utf-8;encoding=utf-8");
		header("Content-Disposition: attachment; filename=$larrfilename[0].$larrfilename[1]");
		readfile($lstrfilepath);
  	}
  	
  	
	
	//Action To Terminate Student From The Hostel
	public function terminatestudentAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		//Initialize Disciplinary Action Model			
		$lintIdStudent = $this->_getParam('studentId');		
		$lstrStudentDetails = $this->lobjdisciplinaryactionmodel ->fnTerminateStudent($lintIdStudent);
	}
	
	//Action To Get List Of Student Names Or Ids
	public function getstudentdetailsAction(){
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrStudentDetails = "";
		
		//Initialize SAM Common Model
 		$lobjSAMCommonModel = new Sam_Model_Samcommon();
				
		//Get Filter Type
		$lstrType = $this->_getParam('Type');
		
		if($lstrType == "Name"){
			//Get List Of Students Names
			$larrStudentDetails = $lobjSAMCommonModel->fnResetArrayFromValuesToNames($lobjSAMCommonModel->fnGetHostelAllotedStudentNamesList());	
		}else if($lstrType == "Id"){
			//Get List Of Students Ids
			$larrStudentDetails = $lobjSAMCommonModel->fnResetArrayFromValuesToNames($lobjSAMCommonModel->fnGetHostelAllotedStudentIdsList());
		}
		
		echo Zend_Json_Encoder::encode($larrStudentDetails);
	}
	
	
	
	//Action To Get List Of Student Names Or Ids
	public function getstudentdetailsbyidAction(){		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$lstrStudentDetails = "";
		//Initialize SAM Common Model
 		$lobjSAMCommonModel = $this->lobjdisciplinaryactionmodel;				
		//Get Filter Type
		$lstrType = $this->_getParam('Type');	
		//Get Student Id
		$lintIdStudent = $this->_getParam('studentId');		
		if($lstrType == "Name"){
			//Get Student Id
			$lstrStudentDetails = $lobjSAMCommonModel->fnGetStudentId($lintIdStudent);	
		}else if($lstrType == "Id"){			//Get Student Name			
			$lstrStudentDetails = $lobjSAMCommonModel->fnGetStudentNamebyid($lintIdStudent);
		}		
		echo $lstrStudentDetails;
	}
	
	//Action To Get Email Address For Respective Disciplinary action Type
	public function getdisciplinaryactionmailsettingdetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		//Initialize Disciplinary Action Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;		
		//Get Disciplinary Action Type Id
		$lintIdDisciplinaryActionType = $this->_getParam('idDisciplinaryActionType');		
		$lstrEmailSettingsDetails = $lobjDisciplinaryactionModel->fnGetDisciplinaryActionTypeEmailByDisciplinaryActionTypeId($lintIdDisciplinaryActionType);
		echo Zend_Json_Encoder::encode($lstrEmailSettingsDetails);
	}
	
	public function deletedistributiondtlsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$iddisciplinayactiondetails = $this->_getParam('iddisciplinayactiondetails');
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;
		$larrDelete = $lobjDisciplinaryactionModel->fndeletedistributiondtls($iddisciplinayactiondetails);	
		echo "1";
	}
	
	public function getsubjectdetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Initialize Disciplinary Action Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;		
		//Get Disciplinary Action Type Id
		 $idstudent = $this->_getParam('idstudent');
     	
		$lstrsubjectdetails = $this->lobjCommon->fnResetArrayFromValuesToNames($lobjDisciplinaryactionModel->fnGetSubjectDetails($idstudent));
		echo Zend_Json_Encoder::encode($lstrsubjectdetails);
	}
	public function getsemesterdetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		//Initialize Disciplinary Action Model
		$lobjDisciplinaryactionModel = $this->lobjdisciplinaryactionmodel;		
		//Get Disciplinary Action Type Id
		$idstudent = $this->_getParam('idstudent');		
		$lstrsubjectdetails = $this->lobjCommon->fnResetArrayFromValuesToNames($lobjDisciplinaryactionModel->fnGetSemesterDetails($idstudent));
		echo Zend_Json_Encoder::encode($lstrsubjectdetails);
	}
}