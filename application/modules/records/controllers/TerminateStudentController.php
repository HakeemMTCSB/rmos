<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_TerminateStudentController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_TerminateStudent();
    }
    
    public function indexAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = $this->view->translate('Change Status');
        $form = new Registration_Form_SearchStudent(array('terminate'=>true));
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['Search'])){
                $form->populate($formData);
                
                $list = $this->model->studentList($formData);
                
                //define semester status
                if ($list){
                    foreach ($list as $key => $loop){
                        $semStatusInfo = $this->model->getSemesterStatus($loop['IdStudentRegistration']);
                        $list[$key]['semStatus'] = $semStatusInfo['semStatus'];

                        if ($semStatusInfo['studentsemesterstatus']==250){
                            unset($list[$key]);
                        }
                        
                        if (isset($formData['semester_status']) && $formData['semester_status']!=''){
                            if ($semStatusInfo['studentsemesterstatus'] != $formData['semester_status']){
                                unset($list[$key]);
                            }
                        }
                    }
                }
            }else{
                $list = false;
            }
        }else{
            $list = false;
        }
        
        $this->view->paginator = $list;
    }
    
    public function terminateAction(){
        $auth = Zend_Auth::getInstance();
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $errorMessage = '';
            
            if (isset($formData['check']) && count($formData['check'])>0){
                foreach ($formData['check'] as $key => $loop){
                    //get student info
                    $studentInfo = $this->model->studentList(null, $key);
                    $cursem = $this->model->getCurrentSemester($studentInfo['IdScheme']);
                    
                    $check = false;
                    if ($formData['profileStatus'][$key]==92){
                        $check = $this->model->checkDuplicateActive($studentInfo['registrationId'], $formData['profileStatus'][$key]);
                    }
                    
                    if (!$check){
                        //update profile status
                        $data1 = array(
                            'profileStatus'=>$formData['profileStatus'][$key]
                        );
                        $this->model->updateProfileStatus($data1, $key);

                        //update semester status
                        $lobjChangeSemesterStatus = new Records_Model_DbTable_SemesterChangestatus();
                        $reg_sem = $lobjChangeSemesterStatus->getStudentRegisterSemester($key);

                        if(count($reg_sem)>0){
                            $level= count($reg_sem);
                        }else{
                            $level=1;
                        }

                        $data = array();
                        $data['IdStudentRegistration'] = $key;
                        $data['idSemester'] = $cursem['IdSemesterMaster'];
                        $data['IdSemesterMain'] = $cursem['IdSemesterMaster'];
                        $data['Level'] = $level;
                        $data['UpdDate'] = date('Y-m-d H:i:s');
                        $data['UpdUser'] = $auth->getIdentity()->iduser;
                        $data['validReason'] = 0;
                        $data['Reason'] = $formData['remark'][$key];
                        $data['studentsemesterstatus'] = $formData['semStatus'][$key];

                        $cmsStatus = new Cms_Status();

                        $message = $formData['remark'][$key];
                        $cmsStatus->copyDeleteAddSemesterStatus($key, $cursem['IdSemesterMaster'], $data, $message, 1);

                        //get subject register
                        $subList = $lobjChangeSemesterStatus->getRegisterSubjec($key, $cursem['IdSemesterMaster']);

                        //store subject into subject history
                        if ($subList){
                            $arrIdRegSub = array();
                            $arrIdRegSubDtl = array();

                            foreach ($subList as $subLoop){
                                $arrSub = array(
                                    'IdStudentRegSubjects'=>$subLoop['IdStudentRegSubjects'], 
                                    'IdStudentRegistration'=>$subLoop['IdStudentRegistration'], 
                                    'initial'=>$subLoop['initial'], 
                                    'IdSubject'=>$subLoop['IdSubject'], 
                                    'credit_hour_registered'=>$subLoop['credit_hour_registered'], 
                                    'SubjectsApproved'=>$subLoop['SubjectsApproved'], 
                                    'SubjectsApprovedComments'=>$subLoop['SubjectsApprovedComments'], 
                                    'IdSemesterMain'=>$subLoop['IdSemesterMain'], 
                                    'IdSemesterDetails'=>$subLoop['IdSemesterDetails'], 
                                    'SemesterLevel'=>$subLoop['SemesterLevel'], 
                                    'IdLandscapeSub'=>$subLoop['IdLandscapeSub'], 
                                    'IdBlock'=>$subLoop['IdBlock'], 
                                    'BlockLevel'=>$subLoop['BlockLevel'], 
                                    'subjectlandscapetype'=>$subLoop['subjectlandscapetype'], 
                                    'IdGrade'=>$subLoop['IdGrade'], 
                                    'UpdUser'=>$subLoop['UpdUser'], 
                                    'UpdDate'=>$subLoop['UpdDate'], 
                                    'UpdRole'=>$subLoop['UpdRole'], 
                                    'Active'=>2, 
                                    'exam_status'=>$subLoop['exam_status'], 
                                    'exam_status_updateby'=>$subLoop['exam_status_updateby'], 
                                    'exam_status_updatedt'=>$subLoop['exam_status_updatedt'], 
                                    'final_course_mark'=>$subLoop['final_course_mark'], 
                                    'grade_id'=>$subLoop['grade_id'], 
                                    'grade_point'=>$subLoop['grade_point'], 
                                    'grade_name'=>$subLoop['grade_name'], 
                                    'grade_desc'=>$subLoop['grade_desc'], 
                                    'grade_status'=>$subLoop['grade_status'], 
                                    'mark_approval_status'=>$subLoop['mark_approval_status'], 
                                    'mark_approveby'=>$subLoop['mark_approveby'], 
                                    'mark_approvedt'=>$subLoop['mark_approvedt'], 
                                    'IdCourseTaggingGroup'=>$subLoop['IdCourseTaggingGroup'], 
                                    'cgpa_calculation'=>$subLoop['cgpa_calculation'], 
                                    'replace_subject'=>$subLoop['replace_subject'], 
                                    'appeal_mark'=>$subLoop['appeal_mark'], 
                                    'exam_scaling_mark'=>$subLoop['exam_scaling_mark'], 
                                    'migrate_date'=>$subLoop['migrate_date'], 
                                    'migrate_by'=>$subLoop['migrate_by'], 
                                    'message'=>$subLoop['message'], 
                                    'createddt'=>$subLoop['createddt'], 
                                    'createdby'=>$subLoop['createdby'],
                                    'pre_active'=>$subLoop['Active']
                                );    
                                $subId = $lobjChangeSemesterStatus->insertRegisterSubjectHistory($arrSub);

                                array_push($arrIdRegSub, $subId);

                                //subject detail
                                $subDtlList = $lobjChangeSemesterStatus->getSubjectDetail($subLoop['IdStudentRegSubjects'], $key, $cursem['IdSemesterMaster']);

                                if ($subDtlList){
                                    foreach ($subDtlList as $subDtlLoop){
                                        $arrSubDtl = array(
                                            'id'=>$subDtlLoop['id'], 
                                            'regsub_id'=>$subDtlLoop['regsub_id'], 
                                            'student_id'=>$subDtlLoop['student_id'], 
                                            'semester_id'=>$subDtlLoop['semester_id'], 
                                            'subject_id'=>$subDtlLoop['subject_id'], 
                                            'item_id'=>$subDtlLoop['item_id'], 
                                            'section_id'=>$subDtlLoop['section_id'], 
                                            'ec_country'=>$subDtlLoop['ec_country'], 
                                            'ec_city'=>$subDtlLoop['ec_city'], 
                                            'invoice_id'=>$subDtlLoop['invoice_id'], 
                                            'created_date'=>$subDtlLoop['created_date'], 
                                            'created_by'=>$subDtlLoop['created_by'], 
                                            'created_role'=>$subDtlLoop['created_role'], 
                                            'ses_id'=>$subDtlLoop['ses_id'], 
                                            'status'=>$subDtlLoop['status'], 
                                            'modifyby'=>$subDtlLoop['modifyby'], 
                                            'modifydt'=>$subDtlLoop['modifydt'], 
                                            'message'=>$subDtlLoop['message'], 
                                            'createddt'=>$subDtlLoop['createddt'], 
                                            'createdby'=>$subDtlLoop['createdby']
                                        );
                                        $subDtlId = $lobjChangeSemesterStatus->insertSubjectDetailHistory($arrSubDtl);

                                        array_push($arrIdRegSubDtl, $subDtlId);

                                        //drop subjet detail
                                        $lobjChangeSemesterStatus->dropSubjectDetail($subDtlLoop['id']);
                                    }
                                }

                                //drop subject
                                $lobjChangeSemesterStatus->dropSubject($subLoop['IdStudentRegSubjects']);
                            }
                        }
                    }else{
                        $errorMessage .= '<br/>'.$studentInfo['registrationId'].' - '.$studentInfo['appl_fname'].' '.$studentInfo['appl_lname'];
                    }
                }
                
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'Data Updated'));
                
                if ($errorMessage != ''){
                    $this->_helper->flashMessenger->addMessage(array('error' => 'Some Student Cannot Terminate : '.$errorMessage));
                }
                
                $this->_redirect($this->baseUrl.'/records/terminate-student/');
            }else{
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('error' => 'Please Check Atleast One'));
                $this->_redirect($this->baseUrl.'/records/terminate-student/');
            }
        }else{
            //redirect here
            $this->_redirect($this->baseUrl.'/records/terminate-student/');
        }
        exit;
    }

    public function clearenceAction(){
        $this->view->title = $this->view->translate('Clearence');
        $id = $this->_getParam('id', 0);

        if ($id != 0){
            $auth = Zend_Auth::getInstance();
            $this->view->role = $auth->getIdentity()->IdRole;

            $list = $this->model->studentList(null, $id);
            $this->view->appdetail = $list;

            $kmc = $this->model->getKmc($id);
            $celFin = $this->model->getClearanceFinance($id);
            $ict = $this->model->getIct($id);
            $logistic = $this->model->getLogistic($id);

            $this->view->kmc = $kmc;
            $this->view->celFin = $celFin;
            $this->view->ict = $ict;
            $this->view->logistic = $logistic;

            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();
                //var_dump($formData);
                //exit;
                if ($formData['type']=='kmc'){
                    $data = array(
                        'IdStudentRegistration'=>$id,
                        'type'=>$formData['type'],
                        'kmc_return'=>$formData['kmc_return'],
                        'kmc_penalty'=>$formData['kmc_penalty'],
                        'kmc_paiddt'=>date('Y-m-d', strtotime($formData['kmc_paiddt'])),
                        'kmc_status'=>$formData['kmc_status'],
                        'kmc_remarks'=>$formData['kmc_remarks'],
                        'kmc_createddt'=>date('Y-m-d H:i:s'),
                        'kmc_createdby'=>$auth->getIdentity()->iduser
                    );

                    if ($kmc){
                        $this->model->updateKmc($data, $id);
                    }else{
                        $this->model->insertKmc($data);
                    }
                }else if($formData['type']=='finance'){
                    $data = array(
                        'IdStudentRegistration'=>$id,
                        'type'=>$formData['type'],
                        'dcf_security_deposit'=>$formData['dcf_security_deposit'],
                        'dcf_bond'=>$formData['dcf_bond'],
                        'dcf_fee'=>$formData['dcf_fee'],
                        'dcf_status'=>$formData['dcf_status'],
                        'dcf_remarks'=>$formData['dcf_remarks'],
                        'dcf_createddt'=>date('Y-m-d H:i:s'),
                        'dcf_createdby'=>$auth->getIdentity()->iduser
                    );

                    if ($celFin){
                        $this->model->updateFinance($data, $id);
                    }else{
                        $this->model->insertFinance($data);
                    }
                }else if($formData['type']=='ict'){
                    $data = array(
                        'IdStudentRegistration'=>$id,
                        'type'=>$formData['type'],
                        'ict_status'=>$formData['ict_status'],
                        'ict_remarks'=>$formData['ict_remarks'],
                        'ict_createddt'=>date('Y-m-d H:i:s'),
                        'ict_createdby'=>$auth->getIdentity()->iduser
                    );

                    if ($ict){
                        $this->model->updateIct($data, $id);
                    }else{
                        $this->model->insertIct($data);
                    }
                }else if($formData['type']=='logistic'){
                    $data = array(
                        'IdStudentRegistration'=>$id,
                        'type'=>$formData['type'],
                        'log_status'=>$formData['logistic_status'],
                        'log_remarks'=>$formData['logistic_remarks'],
                        'log_createddt'=>date('Y-m-d H:i:s'),
                        'log_createdby'=>$auth->getIdentity()->iduser
                    );

                    if ($logistic){
                        $this->model->updateLogistic($data, $id);
                    }else{
                        $this->model->insertLogistic($data);
                    }
                }

                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
                $this->_redirect($this->baseUrl.'/records/terminate-student/clearence/id/'.$id);
            }
        }else{
            $this->_redirect($this->baseUrl.'/records/terminate-student/');
        }
    }

    public function printClearanceAction(){
        $id = $this->_getParam('id', 0);

        global $appdetail;
        global $kmc;
        global $celFin;
        global $ict;
        global $logistic;

        $appdetail = $this->model->studentList(null, $id);
        $kmc = $this->model->getKmc($id);
        $celFin = $this->model->getClearanceFinance($id);
        $ict = $this->model->getIct($id);
        $logistic = $this->model->getLogistic($id);

        $fieldValues = array(
            '$[LOGO]'=> "images/logo_text_high.jpg"
        );

        $html_template_path = DOCUMENT_PATH."/template/clearance.html";
        $output_filename = $appdetail["registrationId"]."-clearance.pdf";

        require_once 'dompdf_config.inc.php';
        error_reporting(0);

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);
        }

        //echo $html;
        //exit;

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();

        $dompdf->stream($output_filename);

        exit;
    }
}
?>