<?php
class Records_SemrecordController extends Base_Base { //Controller for the User Module

	
	public function krsAction(){
		$this->view->title = $this->view->translate("Kartu Rencana Studi");
		$sessionNamespace = new Zend_Session_Namespace('krs_Searchs');
		$auth = Zend_Auth::getInstance();
		$form = new Registration_Form_SearchStudentKrs();
		$this->view->form = $form;

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent($formData,$formData['semester_id']);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			$paginator->setItemCountPerPage($this->gintPageCount);
			//$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			
			
			$sessionNamespace->filter = $formData;
					
			$this->view->paginator = $paginator;
		}elseif(isset($sessionNamespace->filter)){
			
			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent($sessionNamespace->filter,$formData['semester_id']);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			$paginator->setItemCountPerPage($this->gintPageCount);
			//$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($sessionNamespace->filter);
					
			$this->view->paginator = $paginator;			
		}else{			 
		    if($auth->getIdentity()->IdRole=="445"){
			    $studentRegDB = new Registration_Model_DbTable_Studentregistration();
			    $student_list = $studentRegDB->getListStudent();
			    
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				//$paginator->setItemCountPerPage($this->gintPageCount);
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				$this->view->paginator = $paginator;
		    }
		}
	}

	    public function viewKrsAction(){
	    	
			$exec_time =  ini_get('max_execution_time');
	    	
	    	set_time_limit(10);
	    	
    	
    	$this->view->title = "Kartu Rencana Studi";
    	
    	global $subject_list;

    	$type = $this->_getParam('type', 0);
    	
    	$idstudentsemsterstatus = $this->_getParam('idstudentsemsterstatus', 0);
    	$this->view->idstudentsemsterstatus = $idstudentsemsterstatus;
    	
    	$IdStudentRegistration = $this->_getParam('IdStudentRegistration', 0);
    	$this->view->IdStudentRegistration = $IdStudentRegistration;    	
    	
    	//To get Student Academic Info        
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $student = $studentRegDB->getStudentInfo($IdStudentRegistration);

        //get semester info
    	$semesterStatusDb = new App_Model_Record_DbTable_Studentsemesterstatus();
    	$semester = $semesterStatusDb->getSemesterInfo($idstudentsemsterstatus);
    	
    	//get subjects
    	$registerSubjectDB = new App_Model_Record_DbTable_StudentRegSubjects();
    	$subject_list  = $registerSubjectDB->getActiveRegisteredCourse($semester["IdSemesterMain"],$IdStudentRegistration);
    	$total_credit_hours = $registerSubjectDB->getTotalCreditHoursActiveRegisteredCourse($semester["IdSemesterMain"],$IdStudentRegistration);
				
    	//get info dekan faculty
    	$programDb = new App_Model_Record_DbTable_Program();
    	$program = $programDb->getCollegeDean($student["IdProgram"]);
    	
    	//get info college
    	$collegedB = new GeneralSetup_Model_DbTable_Collegemaster();
        $college = $collegedB->getFullInfoCollege($student["IdCollege"]);
        	
    	//get salutation
    	$defDB = new App_Model_General_DbTable_Definationms();
    	$dean_front_salutation = $defDB->getData($program["FrontSalutation"]);
    	$dean_back_salutation  = $defDB->getData($program["BackSalutation"]);    	
    	$academic_front_salutation = $defDB->getData($student["FrontSalutation"]);
    	$academic_back_salutation  = $defDB->getData($student["BackSalutation"]);
    	    	
    	//get photo student
    	$uploadFileDb = new App_Model_Application_DbTable_UploadFile();
    	$file = $uploadFileDb->getFile($student["transaction_id"],51);
  			
		$studentdetails['photo_raw'] = $file;
			
		$fnImage = new icampus_Function_General_Image();
		$photo_url = $fnImage->getImagePath($file['pathupload'],150,192);
		  	    	
    	if(!$photo_url){    		
    		$photo_url = "/var/www/html/triapp/public/images/no_image.gif";
    	}else{
    		$photo_url = MAIN_PATH.$fnImage->getImagePath($file['pathupload'],150,192);
    	}

    	/* ------------------------------
    	 * start create directrory folder
    	 * ------------------------------ */
    	   
		//$location_path
		$location_intake_path = DOCUMENT_PATH."/student/".$student["IdIntake"];
		
        //create directory to locate file			
		if (!is_dir($location_intake_path)) {
	    	mkdir($location_intake_path, 0775);
		}
		
		
        //$location_path
		$location_program_path = $location_intake_path."/".$student["ProgramCode"];
		
        //create directory to locate file			
		if (!is_dir($location_program_path)) {
	    	mkdir($location_program_path, 0775);
		}
		
		//output_directory_path
		$output_directory_path = $location_program_path."/".$student["registrationId"];
		
        //create directory to locate file			
		if (!is_dir($output_directory_path)) {
	    	mkdir($output_directory_path, 0775);
		}			
		
		//creating folder student
		if($student["repository"]==''){
			$studentRegDB->updateData(array('repository'=>"student/".$student["IdIntake"]."/".$student["ProgramCode"]."/".$student["registrationId"]),$IdStudentRegistration);
		}		
				
		//output filename 
		$output_filename = $student["registrationId"]."_kartu_rencana_studi";
		
		if($type==2){
			$output_filename .="_detail";
		}	
		
		$output_filename .= ".pdf";
		
		//to rename output file			
		$output_file_path = $output_directory_path."/".$output_filename;
		 		
		
		/* ------------------------------
    	 * end  create directrory folder
    	 * ------------------------------ */

		
		
		//file type
		if($type==1){
			$file_type = 53;
			
			//get subjects
			$registerSubjectDB = new App_Model_Record_DbTable_StudentRegSubjects();
			$subject_list  = $registerSubjectDB->getSemesterSubjectRegistered($semester["IdSemesterMain"],$IdStudentRegistration);
				
			//get subject course group schedule
			$groupSchduleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
			foreach ($subject_list as $index=>$subject){
				$schedule = $groupSchduleDb->getSchedule($subject['IdCourseTaggingGroup']);
				
				if($schedule){
					$subject_list[$index]['start_sc_date'] = $schedule[0]['sc_date'];
					$subject_list[$index]['end_sc_date'] = $schedule[sizeof($schedule)-1]['sc_date'];
					
					$subject_list[$index]['schedule'] = $schedule;
				}
			}
			
		}else
		if($type==2){
			$file_type = 67;
			
			//get subjects
			$registerSubjectDB = new App_Model_Record_DbTable_StudentRegSubjects();
			$subject_list  = $registerSubjectDB->getSemesterSubjectRegistered($semester["IdSemesterMain"],$IdStudentRegistration);
				
			//get subject course group schedule
			$groupSchduleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
			foreach ($subject_list as $index=>$subject){
				$schedule = $groupSchduleDb->getSchedule($subject['IdCourseTaggingGroup']);
				
				if($schedule){
					$subject_list[$index]['start_sc_date'] = $schedule[0]['sc_date'];
					$subject_list[$index]['end_sc_date'] = $schedule[sizeof($schedule)-1]['sc_date'];
					
					$subject_list[$index]['schedule'] = $schedule;
				}
			}
			

			
		}else{
			$file_type = 53;
		}
		
		//check kalo dah generate ke sebelum ni?
		Global $kode_sandi;
    	$documentDb = new App_Model_Application_DbTable_ApplicantDocument();
    	$document = $documentDb->getData($student["transaction_id"],$file_type); 
    	
		if(isset($document["ad_id"])){			
			$kode_sandi = $document["ad_kode_sandi"];
			
			if($kode_sandi==""){
				$kode_sandi = md5($document['ad_id']);
			}
			
		}else{	
		
			//insert info file into applicant_documents	
			$fileData["ad_appl_id"]=$student["transaction_id"];
			$fileData["ad_type"]=$file_type;
			$fileData["ad_filepath"]="student/".$student["IdIntake"]."/".$student["ProgramCode"]."/".$student["registrationId"];
			$fileData["ad_filename"]=$output_filename;
			$fileData["ad_createddt"]=date("Y-m-d H:i:s");
			$id_document = $documentDb->addData($fileData);
			$kode_sandi = md5($id_document);
			
			//update document kode sandi
			$fileKode["ad_kode_sandi"]=$kode_sandi;
	    	$documentDb->updateData($fileKode,$id_document);
		}
		
		
		
		
		$fieldValues = array(
    	  '$[PROGRAM]'=>$student["ArabicName"],
    	  '$[FACULTY]'=>'FAKULTAS '.$student["CollegeName"],
    	  '$[NIM]'=>$student["registrationId"],
    	  '$[NAME]'=>$student["appl_fname"].' '.$student["appl_mname"].' '.$student["appl_lname"],
    	  '$[PERIODE]'=>$semester["SemesterMainName"],
    	  '$[ACADEMIC_ADVISOR]'=>$academic_front_salutation["BahasaIndonesia"].' '.$student["AcademicAdvisor"].' '.$academic_back_salutation["BahasaIndonesia"],
    	  '$[DEAN]'=>$dean_front_salutation["BahasaIndonesia"].' '.$program["FullName"].' '.$dean_back_salutation["BahasaIndonesia"],
    	  '$[PHOTO]'=>$photo_url,
    	  '$[TOTAL_SUBJECT]'=>$total_credit_hours,
    	  '$[KODE_SANDI]'=>$kode_sandi,
		  '$[ADDRESS]'=>ucwords(strtolower($college["Add1"])).' '.ucwords(strtolower($college["Add2"])).' '.ucwords(strtolower($college["CityName"])).' '.ucwords(strtolower($college["StateName"])),
		  '$[PHONE]'=>$college["Phone1"],
		  '$[EMAIL]'=>$college["Email"]
    	);

    	//print_r ($fieldValues);exit;
    	require_once 'dompdf_config.inc.php';
		
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//template path
		if($type==1){//summary
			$html_template_path = DOCUMENT_PATH."/template/kartu_rencana_studi_fkg.html";
		}else
		if($type==2){//detail
			$html_template_path = DOCUMENT_PATH."/template/kartu_rencana_studi_fkg_detail.html";
		}else{//normal
			$html_template_path = DOCUMENT_PATH."/template/kartu_rencana_studi.html";
		}
		
		$html = file_get_contents($html_template_path);			
    		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}
	
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
		
		$dompdf = $dompdf->output();
		//$dompdf->stream($output_filename);						
		
		file_put_contents($output_file_path, $dompdf);
		
		$this->view->file_path = $output_file_path;
		
		set_time_limit($exec_time);
		
    }	
    
    public function printbulkAction(){
    	global $datapdf;
		$this->view->title = $this->view->translate("KRS (Current Semester)");
		
		$auth = Zend_Auth::getInstance();


		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
	
			foreach($formData["toprint"] as $idstudreg=>$idsemstatus){
				$datapdf[$idstudreg] = $this->prepareData($idsemstatus,$idstudreg);
			}

    	} 
    	
		$html_template_path = DOCUMENT_PATH."/template/kartu_rencana_studi_bulk.html";
		
		$html = file_get_contents($html_template_path);	
		    	
		try{
			require_once 'dompdf_config.inc.php';
			
			$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
			$autoloader->pushAutoloader('DOMPDF_autoload');
			
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->set_paper('a4', 'potrait');
			$dompdf->render();
			
			$dompdf = $dompdf->stream("krs-bulk.pdf");
		
		}catch (Exception $e) {
			$status = false;	
		}
		
		exit;    	
    }   
    
    private function prepareData($idsemstatus,$idstudreg){
    	//global $subject_list;
    	    	
    	$idstudentsemsterstatus = $idsemstatus;
    	$IdStudentRegistration = $idstudreg; 	
    	
    	
    	//To get Student Academic Info        
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $student = $studentRegDB->getStudentInfo($IdStudentRegistration);

        //get semester info
    	$semesterStatusDb = new App_Model_Record_DbTable_Studentsemesterstatus();
    	$semester = $semesterStatusDb->getSemesterInfo($idstudentsemsterstatus);
    	
    	//get subjects
    	$registerSubjectDB = new App_Model_Record_DbTable_StudentRegSubjects();
    	$subject_list  = $registerSubjectDB->getActiveRegisteredCourse($semester["IdSemesterMain"],$IdStudentRegistration);
    	$total_credit_hours = $registerSubjectDB->getTotalCreditHoursActiveRegisteredCourse($semester["IdSemesterMain"],$IdStudentRegistration);
				
    	//get info dekan faculty
    	$programDb = new App_Model_Record_DbTable_Program();
    	$program = $programDb->getCollegeDean($student["IdProgram"]);
    	
    	//get info college
    	$collegedB = new GeneralSetup_Model_DbTable_Collegemaster();
        $college = $collegedB->getFullInfoCollege($student["IdCollege"]);
        	
    	//get salutation
    	$defDB = new App_Model_General_DbTable_Definationms();
    	$dean_front_salutation = $defDB->getData($program["FrontSalutation"]);
    	$dean_back_salutation  = $defDB->getData($program["BackSalutation"]);    	
    	$academic_front_salutation = $defDB->getData($student["FrontSalutation"]);
    	$academic_back_salutation  = $defDB->getData($student["BackSalutation"]);
    	    	
    	//get photo student
    	$uploadFileDb = new App_Model_Application_DbTable_UploadFile();
    	$file = $uploadFileDb->getFile($student["transaction_id"],51);
  			
		$studentdetails['photo_raw'] = $file;
			
		$fnImage = new icampus_Function_General_Image();
		$photo_url = $fnImage->getImagePath($file['pathupload'],150,192);
		  	    	
    	if(!$photo_url){    		
    		$photo_url = "/var/www/html/triapp/public/images/no_image.gif";
    	}else{
    		$photo_url = MAIN_PATH.$fnImage->getImagePath($file['pathupload'],150,192);
    	}

    	
    	
			
		$kode_sandi = md5(date("Ymd"));
			
 	
		$fieldValues = array(
    	  'PROGRAM'=>$student["ArabicName"],
    	  'FACULTY'=>'FAKULTAS '.$student["CollegeName"],
    	  'NIM'=>$student["registrationId"],
    	  'NAME'=>$student["appl_fname"].' '.$student["appl_mname"].' '.$student["appl_lname"],
    	  'PERIODE'=>$semester["SemesterMainName"],
    	  'ACADEMIC_ADVISOR'=>$academic_front_salutation["BahasaIndonesia"].' '.$student["AcademicAdvisor"].' '.$academic_back_salutation["BahasaIndonesia"],
    	  'DEAN'=>$dean_front_salutation["BahasaIndonesia"].' '.$program["FullName"].' '.$dean_back_salutation["BahasaIndonesia"],
    	  'PHOTO'=>$photo_url,
    	  'TOTAL_SUBJECT'=>$total_credit_hours,
    	  'KODE_SANDI'=>$kode_sandi,
		  'ADDRESS'=>ucwords(strtolower($college["Add1"])).' '.ucwords(strtolower($college["Add2"])).' '.ucwords(strtolower($college["CityName"])).' '.ucwords(strtolower($college["StateName"])),
		  'PHONE'=>$college["Phone1"],
		  'EMAIL'=>$college["Email"],
		  'SUBJECTS'=>$subject_list	
    	);  

    	return $fieldValues;
    }
}