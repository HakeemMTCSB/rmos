<?php
class Records_AcademicprogressController extends Base_Base {
	
	private $_gobjlog;
	private $_gobjlogger;


	public function init() {
		$this->fnsetObj();
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object

	}

	public function fnsetObj(){
		$this->lobjStudentprofileForm = new Records_Form_Studentprofile();
		$this->lobjStudentprofile = new Records_Model_DbTable_Studentprofile();
                $this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		//$this->lobjLandscape = new Records_Model_DbTable_Studentprofile();
		$this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
		$this->studentprofileForm = new Registration_Form_Studentprofile();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
                $this->lobjacademicprogress = new Records_Model_DbTable_Academicprogress();
                $this->lobjlandscape = new GeneralSetup_Model_DbTable_Landscape();
                $this->lsemesterModel  = new GeneralSetup_Model_DbTable_Semester();
                $this->lobjplacementtest = new Application_Model_DbTable_Placementtest(); 
                $this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();  //local object for Schemesetup Model
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$this->Branchofficevenue = new GeneralSetup_Model_DbTable_Branchofficevenue();
                $this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
                $this->lobjfacultymodel = new GeneralSetup_Model_DbTable_Collegemaster();

	}
	
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		//$lobjApplicantNameList = $this->lobjStudentprofile->fnGetRegStudentList();                
		//$lobjform->field5->addMultiOptions($lobjApplicantNameList);
//		$lobjCollegelist = $this->lobjStudentprofile->fnGetCollegeList();                
//		$lobjform->field12->addMultiOptions($lobjCollegelist);
                
                // SET THE PROGRAM VALUE
                $larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
		$this->view->lobjform->field8->addMultiOptions($larProgramNameCombo);
                
                // Set Scheme Form value
		$schemeList = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display all schemesetup details in list
		foreach($schemeList as $larrschemearr) {
			$this->view->lobjform->field23->addMultiOption($larrschemearr['IdScheme'],$larrschemearr['EnglishDescription']);
		}
		// Set Intake Form value
		$larrintakelist = $this->lobjintake->fngetallIntake();
		$this->view->lobjform->field24->addMultiOptions($larrintakelist); 
		
                // SET THE STUDENT STATUS
		$larrdefmsstatus= $this->lobjdeftype->fnGetProfilestatus('Student Status');
		$lobjform->field25->addMultiOptions($larrdefmsstatus);
                
		// SET THE SEMESTER VALUE
                $lobjsemesterlist = $this->lsemesterModel->getAllsemesterList();
		$lobjform->field1->addMultiOptions($lobjsemesterlist);
                
                // SET THE FACULTY VALUE
                $lobjfacultylist = $this->lobjfacultymodel->fnGetCollegeList();
		$lobjform->field27->addMultiOptions($lobjfacultylist);
                              

                
		$larrresult = $this->lobjAddDropSubjectModel->fngetRegisteredStudentDtls();
		
		for($i = 0;$i<count($larrresult);$i++){
			$studentcurrentsem = $this->lobjAddDropSubjectModel->fngetstudentcurrentsem($larrresult[$i]['IdStudentRegistration']);	
			if(isset($studentcurrentsem[0]['SemesterCode']) && $studentcurrentsem[0]['SemesterCode']!=NULL){
				$larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterCode'];
			}
			else if(isset($studentcurrentsem[0]['SemesterMainCode']) && $studentcurrentsem[0]['SemesterMainCode']!=NULL){
				$larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterMainCode'];
			}
		}
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->studentprofilepaginatorresult);
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->studentprofilepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->studentprofilepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentprofile ->fnSearchRegisteredStudentApplicationAP($larrformData); //searching the values for the user
			for($i = 0;$i<count($larrresult);$i++){
					$studentcurrentsem = $this->lobjAddDropSubjectModel->fngetstudentcurrentsem($larrresult[$i]['IdStudentRegistration']);	
				if(isset($studentcurrentsem[0]['SemesterCode']) && $studentcurrentsem[0]['SemesterCode']!=NULL){
					$larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterCode'];
				}
				else if(isset($studentcurrentsem[0]['SemesterMainCode']) && $studentcurrentsem[0]['SemesterMainCode']!=NULL){
					$larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterMainCode'];
				}
			}
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentprofilepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			$this->_redirect( $this->baseUrl . '/records/academicprogress/index');
		}
	}
        
        
        public function studentlandscapedetailAction() { // action for search and view
            $lintidreg = $this->_getParam('idstudent');        
            $this->view->idstudent = $lintidreg;   
            $this->view->larrresult = $this->lobjAddDropSubjectModel->fngetRegisteredStudentDtls();
            
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            
            // INSERT PROCESS FOR ACADEMIC PROGRESS STARTS
            // BEFORE INSERTING DELETE ALL Academic records based on studentID
            $this->lobjacademicprogress->deleteRecords($lintidreg);
            
            // FETCH STUDENT DETAILS
            $studentdetails = $this->lobjacademicprogress->fetchStudentRegDetails($lintidreg);            
            $academicprogressID = $this->lobjacademicprogress->insertAcademicProgress($studentdetails);
            //asd($studentdetails);
            // get the courses based on landscape and program ID
            $landscapeID = $studentdetails['IdLandscape'];
            $programID = $studentdetails['IdProgram'];            
            // hide $this->lobjacademicprogress->fetchCoursesInsert($lintidreg,$academicprogressID,$landscapeID,$programID,$studentdetails,$userId);            
            $this->lobjacademicprogress->academicprogressCoursesInsert($lintidreg,$academicprogressID,$landscapeID,$programID,$studentdetails,$userId);  
            // get the registeredCourses for studentID and set thier isregistered to '1'.
            // hide $this->lobjacademicprogress->fetchCoursesRegister($lintidreg);
            // INSERT PROCESS FOR ACADEMIC PROGRESS ENDS
            
            
            
            // finally, fetch the records to display based on landscape type whether it is level, block or semester.            
            
            $this->view->studentdetails = $this->lobjacademicprogress->fetchStudentDetails($lintidreg);
            
            $this->view->minCreditGrad = $this->lobjacademicprogress->fetchTotalHours($landscapeID);            
            $this->view->totalCreditTaken = $this->lobjacademicprogress->fetchTotalCreditHours($lintidreg,'pass','fail');
            $this->view->totalCreditEarn = $this->lobjacademicprogress->fetchTotalCreditHours($lintidreg,'pass','CT'); 
            $this->view->totalCreditToComplete = $this->lobjacademicprogress->fetchTotalCredit($lintidreg,'0');
            
            // SEMESTER BASED
            if($studentdetails['LandscapeType']=='43') {
            $semsters = $semstersunreg = array();    
	    $larrresult = $this->lobjacademicprogress->fetchRegisteredAcademicProgress($lintidreg);
            $semsters = array();
            foreach ($larrresult as $item) {
                    $key = $item['Semester'];
                    if (!isset($semsters[$key])) {
                            $semsters[$key][] = $item;
                    } else {
                            $semsters[$key][] = $item;
                    }
            }
            $this->view->larrsemesterlist = $semsters;            
            
            $larrresultUnreg = $this->lobjacademicprogress->fetchUnRegisteredAcademicProgress($lintidreg);
            $semstersunreg = array();
            foreach ($larrresultUnreg as $item) {
                    $key = $item['Semester'];
                    if (!isset($semstersunreg[$key])) {
                            $semstersunreg[$key][] = $item;
                    } else {
                            $semstersunreg[$key][] = $item;
                    }
            }
            $this->view->larrsemesterlistunreg = $semstersunreg;
            
            $larrresultDummy = $this->lobjacademicprogress->fetchDummyRegisteredAcademicProgress($lintidreg);
            $this->view->larrsemesterlistregsummy = $larrresultDummy;
            
            echo $this->render('semesteracademicprogress');        
            
            }
            // SEMESTER BASED ENDS
            
            
            // LEVEL BASED
            if($studentdetails['LandscapeType']=='42') {
                $larrfinalunReg = $larrfinal = array();
                
                $larrresult = $this->lobjacademicprogress->fngetlandscapelevel($lintidreg,$landscapeID,'1');
		$larrresult = $this->groupArray($larrresult, "blockname");
		foreach($larrresult as $key => $larrlevel) {
			$larrfinal[$key] = $this->groupArray($larrlevel, "Semester");
		}
		$this->view->larrlevellist = $larrfinal;
                
                $larrresultUnreg= $this->lobjacademicprogress->fngetlandscapelevel($lintidreg,$landscapeID,'0');
		$larrresultUnreg = $this->groupArray($larrresultUnreg, "blockname");
		foreach($larrresultUnreg as $key => $larrlevelunreg) {
			$larrfinalunReg[$key] = $this->groupArray($larrlevelunreg, "Semester");
		}
		$this->view->larrlevellistUnreg = $larrfinalunReg; 
                
                $larrresultDummy = $this->lobjacademicprogress->fetchDummyRegisteredAcademicProgress($lintidreg);
                $this->view->larrsemesterlistregsummy = $larrresultDummy;
                
                echo $this->render('levelacademicprogress'); 
            }
            // LEVEL BASED ENDS
            
            
            
            // BLOCK BASED
            if($studentdetails['LandscapeType']=='44') {
                
                $larrfinalblockunReg = $larrfinal = array();
                $larrresultblock = $this->lobjacademicprogress->fngetlandscapeblock($lintidreg,$landscapeID,'1');
		$larrresultblock = $this->groupArray($larrresultblock, "Year_Level_Block");
		foreach($larrresultblock as $key => $larrblock) {
			$larrfinal[$key] = $this->groupArray($larrblock, "Semester");
		}
		$this->view->larrblocklist = $larrfinal;
               
                
                $larrresulblocktUnreg = $this->lobjacademicprogress->fngetlandscapeblock($lintidreg,$landscapeID,'0');
		$larrresulblocktUnreg = $this->groupArray($larrresulblocktUnreg, "Year_Level_Block");
		foreach($larrresulblocktUnreg as $key => $larrblockunreg) {
			$larrfinalblockunReg[$key] = $this->groupArray($larrblockunreg, "Semester");
		}
                
		$this->view->larrblocklistUnreg = $larrfinalblockunReg;
                
                
                $larrresultDummy = $this->lobjacademicprogress->fetchDummyRegisteredAcademicProgress($lintidreg);
                $this->view->larrsemesterlistregsummy = $larrresultDummy;
                
                echo $this->render('blockacademicprogress'); 
            }
            // BLOCK BASED ENDS
            
            
            
            
        }
        
 public function exportacademicprogressAction(){
 	set_time_limit( 0 );
 	$lintidreg = $this->_getParam('idstudent');
    	    $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
		           
            
            // INSERT PROCESS FOR ACADEMIC PROGRESS STARTS
            // BEFORE INSERTING DELETE ALL Academic records based on studentID
            $this->lobjacademicprogress->deleteRecords($lintidreg);
            // FETCH STUDENT DETAILS
            $studentdetails = $this->lobjacademicprogress->fetchStudentRegDetails($lintidreg);            
            $academicprogressID = $this->lobjacademicprogress->insertAcademicProgress($studentdetails);
            
            // get the courses based on landscape and program ID
            $landscapeID = $studentdetails['IdLandscape'];
            $programID = $studentdetails['IdProgram'];            
            //$this->lobjacademicprogress->fetchCoursesInsert($lintidreg,$academicprogressID,$landscapeID,$programID,$studentdetails);            
            $this->lobjacademicprogress->academicprogressCoursesInsert($lintidreg,$academicprogressID,$landscapeID,$programID,$studentdetails,$userId); 
            
            
            // get the registeredCourses for studentID and set thier isregistered to '1'.
           // $this->lobjacademicprogress->fetchCoursesRegister($lintidreg);
            // INSERT PROCESS FOR ACADEMIC PROGRESS ENDS
            
            
            
            // finally, fetch the records to display based on landscape type whether it is level, block or semester.            
            
            $this->view->studentdetails = $this->lobjacademicprogress->fetchStudentDetails($lintidreg); 
            
            $this->view->minCreditGrad = $this->lobjacademicprogress->fetchTotalHours($landscapeID);            
            $this->view->totalCreditTaken = $this->lobjacademicprogress->fetchTotalCreditHours($lintidreg,'pass','fail');
            $this->view->totalCreditEarn = $this->lobjacademicprogress->fetchTotalCreditHours($lintidreg,'pass','CT'); 
            $this->view->totalCreditToComplete = $this->lobjacademicprogress->fetchTotalCredit($lintidreg,'0');
            
            // SEMESTER BASED
            if($studentdetails['LandscapeType']=='43') {
            $semsters = $semstersunreg = array();    
	    $larrresult = $this->lobjacademicprogress->fetchRegisteredAcademicProgress($lintidreg);
            $semsters = array();
            foreach ($larrresult as $item) {
                    $key = $item['Semester'];
                    if (!isset($semsters[$key])) {
                            $semsters[$key][] = $item;
                    } else {
                            $semsters[$key][] = $item;
                    }
            }
            $this->view->larrsemesterlist = $semsters;            
            
            $larrresultUnreg = $this->lobjacademicprogress->fetchUnRegisteredAcademicProgress($lintidreg);
            $semstersunreg = array();
            foreach ($larrresultUnreg as $item) {
                    $key = $item['Semester'];
                    if (!isset($semstersunreg[$key])) {
                            $semstersunreg[$key][] = $item;
                    } else {
                            $semstersunreg[$key][] = $item;
                    }
            }
            $this->view->larrsemesterlistunreg = $semstersunreg;     
            
            $larrresultDummy = $this->lobjacademicprogress->fetchDummyRegisteredAcademicProgress($lintidreg);
            $this->view->larrsemesterlistregsummy = $larrresultDummy;
            
            $html =  $this->render('semesteracademicprogressexcel');        
            
            }
            // SEMESTER BASED ENDS
            
            
            // LEVEL BASED
            if($studentdetails['LandscapeType']=='42') {
                $larrfinalunReg = $larrfinal = array();
                
                $larrresult = $this->lobjacademicprogress->fngetlandscapelevel($lintidreg,$landscapeID,'1');
		$larrresult = $this->groupArray($larrresult, "blockname");
		foreach($larrresult as $key => $larrlevel) {
			$larrfinal[$key] = $this->groupArray($larrlevel, "Semester");
		}
		$this->view->larrlevellist = $larrfinal;
                
                $larrresultUnreg= $this->lobjacademicprogress->fngetlandscapelevel($lintidreg,$landscapeID,'0');
		$larrresultUnreg = $this->groupArray($larrresultUnreg, "blockname");
		foreach($larrresultUnreg as $key => $larrlevelunreg) {
			$larrfinalunReg[$key] = $this->groupArray($larrlevelunreg, "Semester");
		}
		$this->view->larrlevellistUnreg = $larrfinalunReg;   
                
                $larrresultDummy = $this->lobjacademicprogress->fetchDummyRegisteredAcademicProgress($lintidreg);
                $this->view->larrsemesterlistregsummy = $larrresultDummy;
                
                $html= $this->render('levelacademicprogressexcel'); 
            }
            // LEVEL BASED ENDS
            
            
            
            // BLOCK BASED
            if($studentdetails['LandscapeType']=='44') {
                
                $larrfinalblockunReg = $larrfinal = array();
                $larrresultblock = $this->lobjacademicprogress->fngetlandscapeblock($lintidreg,$landscapeID,'1');
		$larrresultblock = $this->groupArray($larrresultblock, "Year_Level_Block");
		foreach($larrresultblock as $key => $larrblock) {
			$larrfinal[$key] = $this->groupArray($larrblock, "Semester");
		}
		$this->view->larrblocklist = $larrfinal;
                //asd($larrfinal);
                
                $larrresulblocktUnreg = $this->lobjacademicprogress->fngetlandscapeblock($lintidreg,$landscapeID,'0');
		$larrresulblocktUnreg = $this->groupArray($larrresulblocktUnreg, "Year_Level_Block");
		foreach($larrresulblocktUnreg as $key => $larrblockunreg) {
			$larrfinalblockunReg[$key] = $this->groupArray($larrblockunreg, "Semester");
		}
                
		$this->view->larrblocklistUnreg = $larrfinalblockunReg;
                
                $larrresultDummy = $this->lobjacademicprogress->fetchDummyRegisteredAcademicProgress($lintidreg);
                $this->view->larrsemesterlistregsummy = $larrresultDummy;
                //asd($larrfinalblockunReg,false); 
                
            $html=$this->render('blockacademicprogressexcel'); 
            }
            // BLOCK BASED ENDS
            
            $studentName = 'Academic_Progress.xls';
            if($studentdetails['name']!='') {  
            $studentName = str_replace(' ','_',$studentdetails['name']).'_Academic_Progress.xls'; }
        
        $this->getResponse()->setRawHeader( "Content-Type: application/vnd.ms-excel; charset=UTF-8" )
            ->setRawHeader( "Content-Disposition: attachment; filename=".$studentName )
           ->setRawHeader( "Content-Transfer-Encoding: binary" )
            ->setRawHeader( "Expires: 0" )
            ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
            ->setRawHeader( "Pragma: public" )
            ->sendResponse();
        echo $html; 
        exit();
    }
	
        
        /**
	 * Function to display semester academic progress
	 * @author Vipul
	 */
	 public function semesteracademicprogressAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  }
          
        /**
	 * Function to display level academic progress
	 * @author Vipul
	 */
	 public function levelacademicprogressAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  } 
          
          
        /**
	 * Function to display block academic progress
	 * @author Vipul
	 */
	 public function blockacademicprogressAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  }  
        
	public function groupArray($input,$field) {
	$return = array();
		foreach ($input as $item) {
			$key = $item[$field];
			if (!isset($return[$key])) {
				$return[$key][] = $item;
			} else {
				$return[$key][] = $item;
			}
		}
		return $return;
	}
	

        
}
