<?php
class Records_CredittransferappController extends Base_Base{
    private $_gobjlog;
    private $lobjcredittransferForm;
    private $lobjstudentregistrationModel;
    private $lobjsubjectmasterModel;
    private $lobjcreditTransferModel;
    private $lobjcreditTransferSubjectModel;
    private $lobjautocreditTransferModel;
    private $lobjlandscapeModel;
    private $lobjstudentprogramchange;
    private $lobjrecordconfigModel;
    private $lobjstudentsubjectmodel;
    private $lobjSemesterModel;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->fnsetObj();
    }

    public function fnsetObj() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->lobjcredittransferForm = new Records_Form_Credittransferapp();
        $this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
        $this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
        $this->lobjcreditTransferModel = new Records_Model_DbTable_Credittransfer();
        $this->lobjcreditTransferSubjectModel = new Records_Model_DbTable_Credittransfersubject();
        $this->lobjautocreditTransferModel = new Application_Model_DbTable_Autocredittransfer();
        $this->lobjlandscapeModel = new GeneralSetup_Model_DbTable_Landscape();
        $this->lobjstudentprogramchange = new Records_Model_DbTable_Studentprogramchange();
        $this->lobjrecordconfigModel = new Records_Model_DbTable_Recordconfig();
	$this->lobjstudentsubjectmodel = new Registration_Model_DbTable_Studentsubjects();
        $this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
    }

    public function credittransferapplicationAction() {
        $this->view->title = $this->view->translate("Credit Transfer/Exemption Application");
        $form = new Records_Form_SearchCreditTransfer();
        //$this->view->ctAppList = $ctAppList;

        $cacheObj = new Cms_Cache();
        $cache = $cacheObj->get();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['Program'] = $this->view->escape(strip_tags($formData['Program']));
            $formData['ProgramScheme'] = $this->view->escape(strip_tags($formData['ProgramScheme']));
            $formData['StudentName'] = $this->view->escape(strip_tags($formData['StudentName']));
            $formData['StudentCategory'] = $this->view->escape(strip_tags($formData['StudentCategory']));
            $formData['AppType'] = $this->view->escape(strip_tags($formData['AppType']));
            $formData['AppStatus'] = $this->view->escape(strip_tags($formData['AppStatus']));
            $formData['StudentId'] = $this->view->escape(strip_tags($formData['StudentId']));
            $formData['ApplicationID'] = $this->view->escape(strip_tags($formData['ApplicationID']));
            $formData['DateFrom'] = $this->view->escape(strip_tags($formData['DateFrom']));
            $formData['DateTo'] = $this->view->escape(strip_tags($formData['DateTo']));
            $formData['Semester'] = $this->view->escape(strip_tags($formData['Semester']));

            if (isset($formData['search'])){
                $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp($formData);
                //$this->gobjsessionsis->credittransferappcontroller = $ctAppList;
                $cache->save($ctAppList, 'credittransferappcontroller');
            }else{
                $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp();
            }
        }else{
            $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp();
        }
        
        if(!$this->_getParam('search')){
            //unset($this->gobjsessionsis->credittransferappcontroller);
            $cache->remove('credittransferappcontroller');
        }
        
        $lintpagecount = $this->gintPageCount;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page',1); // Paginator instance

        $cached_results = $cache->load('credittransferappcontroller');

        if($cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($ctAppList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }

    public function editcredittransferapplicationAction() {
        $ctId = $this->_getParam('IdCreditTransfer', 0);
        $this->view->title = $this->view->translate('Edit Credit Transfer/Exemption Application');
        $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
        //var_dump($getCtInfo); exit;
        $this->view->ctInfo = $getCtInfo;
        $form = new Records_Form_EditCreditTransfer(array('landscapeId'=>$getCtInfo['IdLandscape'], 'apptypeId'=>$getCtInfo['applicationType']));
        $this->view->form = $form;
        
        //address
        $address = '';
        $address .= (($getCtInfo['appl_address1']!='' || $getCtInfo['appl_address1']!=null) ? $getCtInfo['appl_address1']:'');
        $address .= (($getCtInfo['appl_address2']!='' || $getCtInfo['appl_address2']!=null) ? ',<br/>'.$getCtInfo['appl_address2']:'');
        $address .= (($getCtInfo['appl_address3']!='' || $getCtInfo['appl_address3']!=null) ? ',<br/>'.$getCtInfo['appl_address3']:'');
        $address .= (($getCtInfo['appl_postcode']!='' || $getCtInfo['appl_postcode']!=null) ? ',<br/>'.$getCtInfo['appl_postcode']:'');
        $address .= (($getCtInfo['CityName']!='' || $getCtInfo['CityName']!=null) ? ',<br/>'.$getCtInfo['CityName']:'');
        $address .= (($getCtInfo['StateName']!='' || $getCtInfo['StateName']!=null) ? ',<br/>'.$getCtInfo['StateName']:'');
        $address .= (($getCtInfo['CountryName']!='' || $getCtInfo['CountryName']!=null) ? ',<br/>'.$getCtInfo['CountryName']:'');
        
        $this->view->address = $address;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);
        
        $this->view->userInfo = $userInfo;
        
        //ct subject
        $ctSubject = $this->lobjstudentregistrationModel->getCtSub($getCtInfo['IdCreditTransfer']);
        $this->view->ctSubject = $ctSubject;
        //var_dump($userInfo); exit;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            //delete ct subject
            if (isset($formData['deleteColVal']) && count($formData['deleteColVal']) > 0){
                foreach ($formData['deleteColVal'] as $deleteColVal){
                    //cn when delete course
                    $CtSubInfo = $this->lobjstudentregistrationModel->getCtSubById($deleteColVal);
                    
                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceClass->generateCreditNote($getCtInfo['IdStudentRegistration'], $CtSubInfo['EquivalentCourse'], $CtSubInfo['invoiceid_0'], 0);
                    
                    //delete process
                    $this->lobjstudentregistrationModel->deleteCtSub($deleteColVal);
                    $this->lobjstudentregistrationModel->deleteCtSubMore($deleteColVal);
                    
                    //upload file delete
                    $docList = $this->lobjstudentregistrationModel->getUploadFile($deleteColVal);
                    
                    if ($docList){
                        foreach ($docList as $docLoop){
                            $fileLocation = DOCUMENT_PATH.$docLoop['FileLocation'];
                            
                            //delete fizikal file
                            unlink($fileLocation);

                            //delete file data
                            $this->lobjstudentregistrationModel->deleteUploadFile($docLoop['IdCreditTransferSubjectsDocs']);
                        }
                    }
                }
            }
            
            //update charge
            if (isset($formData['Charge']) && count($formData['Charge']) > 0){
                //var_dump($formData); exit;
                foreach ($formData['Charge'] as $key=>$value){
                    $dataCharge = array(
                        'charge'=>$value
                    );
                    
                    $this->lobjstudentregistrationModel->updateSubject($dataCharge, $key);
                    
                    $info = $this->lobjstudentregistrationModel->getCourseById($key);
                    //var_dump($info); exit;
                    
                    if ($dataCharge['charge']==0){
                        if ($info['invoiceid_0'] != 0){
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceClass->generateCreditNote($getCtInfo['IdStudentRegistration'], $info['EquivalentCourse'], $info['invoiceid_0'], 0);
                        
                            $dataCN= array(
                                'invoiceid_0'=>0
                            );
                            
                            $this->lobjstudentregistrationModel->updateSubject($dataCN, $key);
                        }
                    }else{
                        if ($info['invoiceid_0'] == 0){
                            //generate invoice
                            if ($getCtInfo['applicationType']==780){
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $invoiceid = $invoiceClass->generateOtherInvoiceStudent($getCtInfo['IdStudentRegistration'], $getCtInfo['semesterId'],'CT',$info['EquivalentCourse'],0);
                            }else{
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $invoiceid = $invoiceClass->generateOtherInvoiceStudent($getCtInfo['IdStudentRegistration'], $getCtInfo['semesterId'],'EX',$info['EquivalentCourse'],0);
                            }

                            $invData = array(
                                'invoiceid_0'=>$invoiceid
                            );
                            $this->lobjstudentregistrationModel->updateCtSub($invData, $key);
                        }
                    }
                }
            }
            
            //update credit hours
            if (isset($formData['Credithours']) && count($formData['Credithours']) > 0){
                foreach ($formData['Credithours'] as $key=>$value){
                    $dataCharge = array(
                        'EquivalentCreditHours'=>$value
                    );
                    
                    $this->lobjstudentregistrationModel->updateSubject($dataCharge, $key);
                }
            }
            
            if (isset($formData['courseeq']) && count($formData) > 0){
                foreach ($formData['courseeq'] as $courseeq){
                    $ctSubData = array(
                        'IdCreditTransfer'=>$ctId,
                        'IdSubject'=>null,
                        'IdSemesterMain'=>null,
                        'creditStatus'=>null,
                        'ApplicationDate'=>date('Y-m-d'),
                        'AppliedBy'=>$userId,
                        'ApprovedDate'=>null,
                        'Approvedby'=>null,
                        'ApplicationStatus'=>783,
                        'IdInstitution'=>null,
                        'IdQualification'=>null,
                        'IdSpecialization'=>null,
                        'EquivalentCourse'=>$courseeq,
                        'EquivalentGrade'=>$formData['gred'][$courseeq],
                        'EquivalentCreditHours'=>$formData['credithours'][$courseeq],
                        'Marks'=>null,
                        'Comments'=>$formData['remarks'][$courseeq],
                        'disapprovalComment'=>null,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'charge'=>$formData['charge'][$courseeq],
                        //'charge2'=>$formData['chargeex'][$courseeq],
                        'charge2'=>1,
                        'institutionName'=>$formData['institutionName'][$courseeq]
                    );
                    $IdCreditTransferSubjects = $this->lobjstudentregistrationModel->insertCtSub($ctSubData);
                    
                    //upload file
                    $folder = '/credittransfer/'.$IdCreditTransferSubjects;
                    $dir = DOCUMENT_PATH.$folder;
            
                    if ( !is_dir($dir) ){
                        if ( mkdir_p($dir) === false )
                        {
                            throw new Exception('Cannot create attachment folder ('.$dir.')');
                        }
                    }

                    //upload file proses
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
                    $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                    $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                    $adapter->addValidator('NotExists', false, $dir);
                    $adapter->setDestination($dir);

                    $files = $adapter->getFileInfo();
                    
                    //var_dump($files); exit;
                    
                    if ($files){
                        $i = 0;
                        foreach ($files as $filesLoop){
                            if (isset($files['uploadFile'.$courseeq.'_'.$i.'_'])){
                                $fileOriName = $files['uploadFile'.$courseeq.'_'.$i.'_']['name'];
                                $fileRename = date('YmdHis').'_'.$fileOriName;
                                $filepath = $files['uploadFile'.$courseeq.'_'.$i.'_']['destination'].'/'.$fileRename;
                                
                                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                                $adapter->isUploaded('uploadFile'.$courseeq.'_'.$i.'_');
                                $adapter->receive('uploadFile'.$courseeq.'_'.$i.'_');
                                
                                if ($files['uploadFile'.$courseeq.'_'.$i.'_']['size']!=null){
                                    $uploadData = array(
                                        'IdCreditTransferSubjects'=>$IdCreditTransferSubjects, 
                                        'FileLocation'=>$folder.'/'.$fileRename, 
                                        'FileName'=>$fileOriName, 
                                        'UploadedFilename'=>$fileRename, 
                                        'FileSize'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['size'], 
                                        'MIMEType'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['type'], 
                                        'UpdDate'=>date('Y-m-d'), 
                                        'UpdUser'=>$userId, 
                                        'Comments'=>''
                                    );
                                    $this->lobjstudentregistrationModel->insertUploadFile($uploadData);
                                }
                            }
                            $i++;
                        }
                    }
                    
                    if (isset($formData['course'][$courseeq]) && count($formData['course'][$courseeq]) > 0){
                        foreach ($formData['course'][$courseeq] as $courseLoop){
                            $ctSubMoreData = array();
                            
                            $ctSubMoreData['csm_creditTransferId']=$ctId;
                            $ctSubMoreData['csm_creditTransferSubId']=$IdCreditTransferSubjects;
                            $ctSubMoreData['csm_type']=$getCtInfo['InstitutionType'];
                            
                            if ($getCtInfo['InstitutionType']==783){
                                $ctSubMoreData['csm_subjectexternal']=$courseLoop;
                            }else{
                                $ctSubMoreData['csm_subjectinternal']=$courseLoop;
                            }
                            
                            $ctSubMoreData['csm_upduser']=$userId;
                            $ctSubMoreData['csm_upddate']=date('Y-m-d');
                            
                            $this->lobjstudentregistrationModel->insertCtSubMore($ctSubMoreData);
                        }
                    }
                }
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => "Information Update"));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$ctId);
        }
    }

    public function addcredittransferapplicationAction() {
        $this->view->title = $this->view->translate('Credit Transfer/Exemption Application');
        $form = new Records_Form_AddCreditTransfer();
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $ctAppData = array(
                'IdStudentRegistration'=>$formData['studRegId'],
                'IdCTApplication'=>$this->generateApplicationID($formData['AppType']),
                'IdApplication'=>0,
                'IdProgram'=>$formData['programId'],
                'Status'=>null,
                'ApplicationStatus'=>784,
                'DateApplied'=>date('Y-m-d'),
                'AppliedBy'=>$userId,
                'DateApproved'=>null,
                'UpdDate'=>date('Y-m-d'),
                'UpdUser'=>$userId,
                'Comments'=>null,
                'InstitutionType'=>$formData['InstitionType'],
                'semesterId'=>$formData['Semester'],
                'applicationType'=>$formData['AppType'],
                'source'=>'Admin'
            );
            $appid = $this->lobjstudentregistrationModel->insertCtApp($ctAppData);
            
            //store history
            $statusHistoryData = array(
                'cth_ctid'=>$appid,
                'cth_oldstatus'=>0,
                'cth_newstatus'=>784,
                'cth_upddate'=>date('Y-m-d'),
                'cth_upduser'=>$userId
            );
            $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
            
            //todo insert ct subject
            if (isset($formData['courseeq']) && count($formData) > 0){
                foreach ($formData['courseeq'] as $courseeq){
                    $ctSubData = array(
                        'IdCreditTransfer'=>$appid,
                        'IdSubject'=>null,
                        'IdSemesterMain'=>null,
                        'creditStatus'=>null,
                        'ApplicationDate'=>date('Y-m-d'),
                        'AppliedBy'=>$userId,
                        'ApprovedDate'=>null,
                        'Approvedby'=>null,
                        'ApplicationStatus'=>784,
                        'IdInstitution'=>null,
                        'IdQualification'=>null,
                        'IdSpecialization'=>null,
                        'EquivalentCourse'=>$courseeq,
                        'EquivalentGrade'=>$formData['gred'][$courseeq],
                        'EquivalentCreditHours'=>$formData['credithours'][$courseeq],
                        'Marks'=>null,
                        'Comments'=>$formData['remarks'][$courseeq],
                        'disapprovalComment'=>null,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'charge'=>$formData['charge'][$courseeq],
                        //'charge2'=>$formData['chargeex'][$courseeq],
                        'charge2'=>1,
                        'institutionName'=>$formData['institutionName'][$courseeq]
                    );
                    $IdCreditTransferSubjects = $this->lobjstudentregistrationModel->insertCtSub($ctSubData);
                    
                    //generate invoice
                    if ($formData['charge'][$courseeq]!=0){
                        if ($formData['AppType']==780){
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceid = $invoiceClass->generateOtherInvoiceStudent($formData['studRegId'],$formData['Semester'],'CT',$courseeq,0);
                        }else{
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceid = $invoiceClass->generateOtherInvoiceStudent($formData['studRegId'],$formData['Semester'],'EX',$courseeq,0);
                        }

                        $invData = array(
                            'invoiceid_0'=>$invoiceid
                        );
                        $this->lobjstudentregistrationModel->updateCtSub($invData, $IdCreditTransferSubjects);
                    }
                    
                    //upload file
                    $folder = '/credittransfer/'.$IdCreditTransferSubjects;
                    $dir = DOCUMENT_PATH.$folder;
            
                    if ( !is_dir($dir) ){
                        if ( mkdir_p($dir) === false )
                        {
                            throw new Exception('Cannot create attachment folder ('.$dir.')');
                        }
                    }

                    //upload file proses
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
                    $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                    $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                    $adapter->addValidator('NotExists', false, $dir);
                    $adapter->setDestination($dir);

                    $files = $adapter->getFileInfo();
                    
                    if ($files){
                        $i = 0;
                        foreach ($files as $filesLoop){
                            if (isset($files['uploadFile'.$courseeq.'_'.$i.'_'])){
                                $fileOriName = $files['uploadFile'.$courseeq.'_'.$i.'_']['name'];
                                $fileRename = date('YmdHis').'_'.$fileOriName;
                                $filepath = $files['uploadFile'.$courseeq.'_'.$i.'_']['destination'].'/'.$fileRename;
                                
                                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                                $adapter->isUploaded('uploadFile'.$courseeq.'_'.$i.'_');
                                $adapter->receive('uploadFile'.$courseeq.'_'.$i.'_');
                                
                                if ($files['uploadFile'.$courseeq.'_'.$i.'_']['size']!=null){
                                    $uploadData = array(
                                        'IdCreditTransferSubjects'=>$IdCreditTransferSubjects, 
                                        'FileLocation'=>$folder.'/'.$fileRename, 
                                        'FileName'=>$fileOriName, 
                                        'UploadedFilename'=>$fileRename, 
                                        'FileSize'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['size'], 
                                        'MIMEType'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['type'], 
                                        'UpdDate'=>date('Y-m-d'), 
                                        'UpdUser'=>$userId, 
                                        'Comments'=>''
                                    );
                                    $this->lobjstudentregistrationModel->insertUploadFile($uploadData);
                                }
                            }
                            $i++;
                        }
                    }
                    
                    if (isset($formData['course'][$courseeq]) && count($formData['course'][$courseeq]) > 0){
                        foreach ($formData['course'][$courseeq] as $courseLoop){
                            $ctSubMoreData = array();
                            
                            $ctSubMoreData['csm_creditTransferId']=$appid;
                            $ctSubMoreData['csm_creditTransferSubId']=$IdCreditTransferSubjects;
                            $ctSubMoreData['csm_type']=$formData['InstitionType'];
                            
                            if ($formData['InstitionType']==783){
                                $ctSubMoreData['csm_subjectexternal']=$courseLoop;
                            }else{
                                $ctSubMoreData['csm_subjectinternal']=$courseLoop;
                            }
                            
                            $ctSubMoreData['csm_upduser']=$userId;
                            $ctSubMoreData['csm_upddate']=date('Y-m-d');
                            
                            $this->lobjstudentregistrationModel->insertCtSubMore($ctSubMoreData);
                        }
                    }
                }
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => "Information Saved"));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$appid);
        }
    }
    
    public function credittransferapplicationapprovalAction() {       
        $this->view->title = $this->view->translate("Credit Transfer/Exemption Approval");
        $form = new CourseRegistration_Form_SearchCreditTransfer();
        //$this->view->ctAppList = $ctAppList;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['Program'] = $this->view->escape(strip_tags($formData['Program']));
            $formData['ProgramScheme'] = $this->view->escape(strip_tags($formData['ProgramScheme']));
            $formData['StudentName'] = $this->view->escape(strip_tags($formData['StudentName']));
            $formData['StudentCategory'] = $this->view->escape(strip_tags($formData['StudentCategory']));
            $formData['AppType'] = $this->view->escape(strip_tags($formData['AppType']));
            $formData['AppStatus'] = $this->view->escape(strip_tags($formData['AppStatus']));
            $formData['StudentId'] = $this->view->escape(strip_tags($formData['StudentId']));
            $formData['ApplicationID'] = $this->view->escape(strip_tags($formData['ApplicationID']));
            $formData['DateFrom'] = $this->view->escape(strip_tags($formData['DateFrom']));
            $formData['DateTo'] = $this->view->escape(strip_tags($formData['DateTo']));
            $formData['Semester'] = $this->view->escape(strip_tags($formData['Semester']));

            if (isset($formData['search'])){
                $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp($formData);
                $this->gobjsessionsis->credittransferappcontroller = $ctAppList;
            }else{
                $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp();
            }
        }else{
            $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp();
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->credittransferappcontroller);
        }
        
        $lintpagecount = 1000;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->credittransferappcontroller)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->credittransferappcontroller,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($ctAppList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }
    
    public function ctApproveAction(){
        $this->view->title = $this->view->translate('Credit Transfer/Exemption Approval');
        $ctId = $this->_getParam('IdCreditTransfer', 0);
        $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
        $this->view->ctInfo = $getCtInfo;
        $form = new Records_Form_ApproveCreditTransfer();
        $this->view->form = $form;
        
        //address
        $address = '';
        $address .= (($getCtInfo['appl_address1']!='' || $getCtInfo['appl_address1']!=null) ? $getCtInfo['appl_address1']:'');
        $address .= (($getCtInfo['appl_address2']!='' || $getCtInfo['appl_address2']!=null) ? ',<br/>'.$getCtInfo['appl_address2']:'');
        $address .= (($getCtInfo['appl_address3']!='' || $getCtInfo['appl_address3']!=null) ? ',<br/>'.$getCtInfo['appl_address3']:'');
        $address .= (($getCtInfo['appl_postcode']!='' || $getCtInfo['appl_postcode']!=null) ? ',<br/>'.$getCtInfo['appl_postcode']:'');
        $address .= (($getCtInfo['CityName']!='' || $getCtInfo['CityName']!=null) ? ',<br/>'.$getCtInfo['CityName']:'');
        $address .= (($getCtInfo['StateName']!='' || $getCtInfo['StateName']!=null) ? ',<br/>'.$getCtInfo['StateName']:'');
        $address .= (($getCtInfo['CountryName']!='' || $getCtInfo['CountryName']!=null) ? ',<br/>'.$getCtInfo['CountryName']:'');
        
        $this->view->address = $address;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);
        
        $this->view->userInfo = $userInfo;
        
        //ct subject
        $ctSubject = $this->lobjstudentregistrationModel->getCtSub($getCtInfo['IdCreditTransfer']);
        $this->view->ctSubject = $ctSubject;
        //var_dump(count($ctSubject)); exit;
        
        //ct history
        $historyList = $this->lobjstudentregistrationModel->getHistory($ctId);
        $this->view->historyList = $historyList;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $count = 0;
            foreach($formData['AppStatus'] as $key=>$val){
                $ctSubjectData = array(
                    'creditStatus'=>$val
                );
                $this->lobjstudentregistrationModel->updateCtSubject($ctSubjectData, $key);
                
                if ($val == 1){
                    $count++;
                }
            }
            
            //update charge
            /*if (isset($formData['charge']) && count($formData['charge']) > 0){
                foreach ($formData['charge'] as $key=>$value){
                    $dataCharge = array(
                        'charge'=>$value
                    );
                    
                    $this->lobjstudentregistrationModel->updateSubject($dataCharge, $key);
                }
            }*/
            
            //update charge2
            if (isset($formData['chargeex']) && count($formData['chargeex']) > 0){
                foreach ($formData['chargeex'] as $key=>$value){
                    $dataCharge = array(
                        'charge2'=>$value
                    );
                    
                    $this->lobjstudentregistrationModel->updateSubject($dataCharge, $key);
                }
            }
            
            if ($count != 0){
                
                $url = $this->baseUrl . '/records/credittransferapp/ct-approve/IdCreditTransfer/'.$ctId;
                $this->approve($ctId, $url);
                        
                //update status
                $statusData = array(
                    'ApplicationStatus'=>786,
                    'Comments'=>$formData['Remarks'],
                    'DateApproved'=>date('Y-m-d'),
                    'UpdDate'=>date('Y-m-d'),
                    'ApprovedBy'=>$userId,
                    'UpdUser'=>$userId
                );
                $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
                
                $statusHistoryData = array(
                    'cth_ctid'=>$ctId,
                    'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                    'cth_newstatus'=>$statusData['ApplicationStatus'],
                    'cth_upddate'=>date('Y-m-d'),
                    'cth_upduser'=>$userId
                );
                $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
                
                $ctSubject2 = $this->lobjstudentregistrationModel->getCtSub2($getCtInfo['IdCreditTransfer']);
                
                if ($ctSubject2){
                    foreach ($ctSubject2 as $ctSubjectLoop){
                        if ($ctSubjectLoop['charge2']==1){
                            //generate invoice
                            if ($getCtInfo['applicationType']==780){
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $invoiceid = $invoiceClass->generateOtherInvoiceStudent($getCtInfo['IdStudentRegistration'],$getCtInfo['semesterId'],'CT',$ctSubjectLoop['EquivalentCourse'],1);
                            }else{
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $invoiceid = $invoiceClass->generateOtherInvoiceStudent($getCtInfo['IdStudentRegistration'],$getCtInfo['semesterId'],'EX',$ctSubjectLoop['EquivalentCourse'],1);
                            }

                            $invData = array(
                                'invoiceid_1'=>$invoiceid
                            );
                            $this->lobjstudentregistrationModel->updateCtSub($invData, $ctSubjectLoop['IdCreditTransferSubjects']);
                        }
                    }
                }
                
                /*if ($ctSubject2){
                    foreach ($ctSubject2 as $ctSubjectLoop){
                        if ($ctSubjectLoop['charge']==0){
                            if ($ctSubjectLoop['invoiceid_0'] != 0){
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $invoiceClass->generateCreditNote($getCtInfo['IdStudentRegistration'], 0, $ctSubjectLoop['invoiceid_0'], 0);
                            }
                        }
                    }
                }*/
                
                $message = "Application has been approved";
            }else{
                //update status
                $statusData = array(
                    'ApplicationStatus'=>787,
                    'Comments'=>$formData['Remarks'],
                    'DateApproved'=>date('Y-m-d'),
                    'UpdDate'=>date('Y-m-d'),
                    'ApprovedBy'=>$userId,
                    'UpdUser'=>$userId
                );
                $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
                
                $statusHistoryData = array(
                    'cth_ctid'=>$ctId,
                    'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                    'cth_newstatus'=>$statusData['ApplicationStatus'],
                    'cth_upddate'=>date('Y-m-d'),
                    'cth_upduser'=>$userId
                );
                $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
                
                if ($ctSubject){
                    foreach ($ctSubject as $ctSubjectLoop){
                        if ($ctSubjectLoop['charge']==0){
                            if ($ctSubjectLoop['invoiceid_0'] != 0){
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $invoiceClass->generateCreditNote($getCtInfo['IdStudentRegistration'], $ctSubjectLoop['EquivalentCourse'], $ctSubjectLoop['invoiceid_0'], 0);
                            
                                $dataCN= array(
                                    'invoiceid_0'=>0
                                );

                                $this->lobjstudentregistrationModel->updateSubject($dataCN, $ctSubjectLoop['IdCreditTransferSubjects']);
                            }
                        }
                    }
                }
                
                $message = "Application has been rejected";
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => $message));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/ct-approve/IdCreditTransfer/'.$ctId);
        }
    }
    
    public function cancelCtAppAction(){
        $ctId = $this->_getParam('IdCreditTransfer', 0);
        
        if ($ctId != 0){
            //user info
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            
            //get ct info
            $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
            $ctSubject = $this->lobjstudentregistrationModel->getCtSub($ctId);
            
            //cancel proses
            $statusData = array(
                'ApplicationStatus'=>785,
                'Comments'=>'Application cancel',
                'DateApproved'=>date('Y-m-d'),
                'UpdDate'=>date('Y-m-d'),
                'ApprovedBy'=>$userId,
                'UpdUser'=>$userId
            );
            $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
            
            $statusHistoryData = array(
                'cth_ctid'=>$ctId,
                'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                'cth_newstatus'=>$statusData['ApplicationStatus'],
                'cth_upddate'=>date('Y-m-d'),
                'cth_upduser'=>$userId
            );
            $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
            
            if ($ctSubject){
                foreach ($ctSubject as $ctSubjectLoop){
                    if ($ctSubjectLoop['invoiceid_0'] != 0){
                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        $invoiceClass->generateCreditNote($getCtInfo['IdStudentRegistration'], $ctSubjectLoop['EquivalentCourse'], $ctSubjectLoop['invoiceid_0'], 0);
                    
                        $dataCN= array(
                            'invoiceid_0'=>0
                        );

                        $this->lobjstudentregistrationModel->updateSubject($dataCN, $ctSubjectLoop['IdCreditTransferSubjects']);
                    }
                }
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Application has been cancel <br/><br/>Info :<br/>Name : '.$getCtInfo['appl_fname'].' '.$getCtInfo['appl_lname'].'<br/>Student ID : '.$getCtInfo['registrationId']));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/credittransferapplication'); 
        }else{
            
        }
    }
    
    public function findStudentAction(){
        $searchElement = $this->_getParam('term', '');
        $searchType = $this->_getParam('searchType', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $studentList = $this->lobjstudentregistrationModel->findStudent($searchElement, $searchType);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);
        
        if ($studentList){
            foreach($studentList as $student) {
                
                $address = (($student['appl_address1']!='' || $student['appl_address1']!=null) ? $student['appl_address1']:'');
                $address .= (($student['appl_address2']!='' || $student['appl_address2']!=null) ? ', '.$student['appl_address2']:'');
                $address .= (($student['appl_address3']!='' || $student['appl_address3']!=null) ? ', '.$student['appl_address3']:'');
                $address .= (($student['appl_postcode']!='' || $student['appl_postcode']!=null) ? ', '.$student['appl_postcode']:'');
                $address .= (($student['CityName']!='' || $student['CityName']!=null) ? ', '.$student['CityName']:'');
                $address .= (($student['StateName']!='' || $student['StateName']!=null) ? ', '.$student['StateName']:'');
                $address .= (($student['CountryName']!='' || $student['CountryName']!=null) ? ', '.$student['CountryName']:'');

                $status = $this->lobjstudentregistrationModel->getDefination2(155, 'Applied');
                
                //current sem
                $curSem = $this->lobjstudentregistrationModel->getCurSem($student['schemeid']);
                //var_dump($curSem);
                $student_array[] = array(
                    'id' => $student['IdStudentRegistration'],
                    'ApplicationID'=>'Will generate after save',
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeId'=>$student['IdProgramScheme'],
                    'LandscapeId'=>$student['IdLandscape'],
                    'StatusName'=>$student['StatusName'],
                    'IntakeName'=>$student['IntakeName'],
                    'BranchName'=>$student['BranchName'],
                    'ProgramName'=>$student['ProgramName'],
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeName'=>$student['mop'].' '.$student['mos'].' '.$student['pt'],
                    'fundingMethod'=>$student['afMethodName'],
                    'sponsorshipName'=>$student['af_sponsor_name'],
                    'scholarshipTypeName'=>$student['typeScholarshipName'],
                    'scholarshipTypeId'=>$student['af_type_scholarship'],
                    'applyScholarship'=>$student['applScholarshipName'],
                    'securedScholarship'=>$student['af_scholarship_secured'],
                    'Address'=>$address,
                    'PhoneHome'=>$student['appl_phone_home'],
                    'PhoneMobile'=>$student['appl_phone_mobile'],
                    'PhoneOffice'=>$student['appl_phone_office'],
                    'Email'=>$student['appl_email'],
                    'IdType'=>$student['IdTypeName'],
                    'IdNo'=>$student['appl_idnumber'],
                    'AppliedDate'=>date('d-m-Y'),
                    'AppliedBy'=>$userInfo['loginName'],
                    'AppliedStatus'=>$status['DefinitionDesc'],
                    'CurrentSem'=>$curSem['IdSemesterMaster'],
                    'label' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname'],
                    'value' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
                );
            }
        }else{
            $student_array[] = array();
        }
        
        $json = Zend_Json::encode($student_array);
		
	echo $json;
	exit();
    }
    
    public function getCourseAction(){
        $landscapeId = $this->_getParam('landscapeId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        //get landscape
        $landscapeInfo = $this->lobjstudentregistrationModel->getLandscape($landscapeId);
        
        if ($landscapeInfo['LandscapeType']==44){
            $courseList = $this->lobjstudentregistrationModel->getBlockSubject($landscapeId);
        }else{
            $courseList = $this->lobjstudentregistrationModel->getSubject($landscapeId);
        }
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
    
    public function getCourseInfoAction(){
        $courseId = $this->_getParam('courseId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseInfo = $this->lobjstudentregistrationModel->getCourseInfo($courseId);
        
        $json = Zend_Json::encode($courseInfo);
		
	echo $json;
	exit();
    }
    
    public function getCourseMoreAction(){
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseList = $this->lobjstudentregistrationModel->getEquivalentSub();
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
    
    public function getGredAction(){
        $appTypeId = $this->_getParam('appTypeId', 0);
        $landscapeId = $this->_getParam('landscapeId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $gredList = array();
        
        if ($appTypeId == 780){
            $landscapeInfo = $this->lobjstudentregistrationModel->getLandscape($landscapeId);
            
            if ($landscapeInfo['AssessmentMethod'] == 'point'){
                $gred = $this->lobjstudentregistrationModel->getGred();
                if ($gred){
                    $i=0;
                    foreach ($gred as $gredLoop){
                        $gredList[$i]['name']=$gredLoop['DefinitionDesc'];
                        $i++;
                    }
                }
            }else{
                $gredList[0]['name']='CT';
            }
        }else{
            $gredList[0]['name']='EX';
        }
        
        $json = Zend_Json::encode($gredList);
		
	echo $json;
	exit();
    }
    
    public function getSemesterAction(){
        $programId = $this->_getParam('programId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $programInfo = $this->lobjstudentregistrationModel->getProgramForSem($programId);
        
        $semList = $this->lobjstudentregistrationModel->getSemBasedOnList($programInfo['IdScheme']);
        
        $json = Zend_Json::encode($semList);
		
	echo $json;
	exit();
    }
    
    public function getFileUploadAction(){
        $ctSubId = $this->_getParam('ctSubId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $uploadList = $this->lobjstudentregistrationModel->getUploadFile($ctSubId);
        
        $json = Zend_Json::encode($uploadList);
		
	echo $json;
	exit();
    }
    
    public function deleteUploadFileAction(){
        $uploadId = $this->_getParam('id', 0);
        $ctId = $this->_getParam('ctId', 0);
        if ($uploadId != 0){
            $info = $this->lobjstudentregistrationModel->deleteUploadFileInfo($uploadId);
            $fileLocation = DOCUMENT_PATH.$info['FileLocation'];
            
            //delete fizikal file
            unlink($fileLocation);
            
            //delete file data
            $this->lobjstudentregistrationModel->deleteUploadFile($uploadId);
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'File '.$info['FileName'].' has been deleted'));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$ctId);
        }else{
            $this->_helper->flashMessenger->addMessage(array('error' => 'Delete file unsuccessful'));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$ctId);
        }
    }
    
    public function uploadFileAction(){
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); 
            //var_dump($_FILES);
            //exit;
            
            $folder = '/credittransfer/'.$formData['ctSubId'];
            $dir = DOCUMENT_PATH.$folder;

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false )
                {
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
            $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();
            //var_dump($files); exit;
            if ($files){

                $fileOriName = $files['file']['name'];
                $fileRename = date('YmdHis').'_'.$fileOriName;
                $filepath = $files['file']['destination'].'/'.$fileRename;

                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                $adapter->isUploaded();
                $adapter->receive();

                $uploadData = array(
                    'IdCreditTransferSubjects'=>$formData['ctSubId'], 
                    'FileLocation'=>$folder.'/'.$fileRename, 
                    'FileName'=>$fileOriName, 
                    'UploadedFilename'=>$fileRename, 
                    'FileSize'=>$files['file']['size'], 
                    'MIMEType'=>$files['file']['type'], 
                    'UpdDate'=>date('Y-m-d'), 
                    'UpdUser'=>$userId, 
                    'Comments'=>''
                );
                $this->lobjstudentregistrationModel->insertUploadFile($uploadData);
                
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'File '.$fileOriName.' has been upload'));
                $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$formData['ctId']);
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('error' => 'File upload error'));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$formData['ctId']);
        }
    }
    
    public function printapplicationAction(){
        $this->_helper->layout()->setLayout('/printstudentsummary');
        $ctId = $this->_getParam('id', 0);
        $this->view->title = $this->view->translate('Credit Transfer Application');
        $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
        //var_dump($getCtInfo); exit;
        $this->view->ctInfo = $getCtInfo;
        $form = new Records_Form_EditCreditTransfer(array('landscapeId'=>$getCtInfo['IdLandscape'], 'apptypeId'=>$getCtInfo['applicationType']));
        $this->view->form = $form;

        //address
        $address = '';
        $address .= (($getCtInfo['appl_address1']!='' || $getCtInfo['appl_address1']!=null) ? $getCtInfo['appl_address1']:'');
        $address .= (($getCtInfo['appl_address2']!='' || $getCtInfo['appl_address2']!=null) ? ',<br/>'.$getCtInfo['appl_address2']:'');
        $address .= (($getCtInfo['appl_address3']!='' || $getCtInfo['appl_address3']!=null) ? ',<br/>'.$getCtInfo['appl_address3']:'');
        $address .= (($getCtInfo['appl_postcode']!='' || $getCtInfo['appl_postcode']!=null) ? ',<br/>'.$getCtInfo['appl_postcode']:'');
        $address .= (($getCtInfo['CityName']!='' || $getCtInfo['CityName']!=null) ? ',<br/>'.$getCtInfo['CityName']:'');
        $address .= (($getCtInfo['StateName']!='' || $getCtInfo['StateName']!=null) ? ',<br/>'.$getCtInfo['StateName']:'');
        $address .= (($getCtInfo['CountryName']!='' || $getCtInfo['CountryName']!=null) ? ',<br/>'.$getCtInfo['CountryName']:'');

        $this->view->address = $address;

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);

        $this->view->userInfo = $userInfo;

        //ct subject
        $ctSubject = $this->lobjstudentregistrationModel->getCtSub($getCtInfo['IdCreditTransfer']);
        $this->view->ctSubject = $ctSubject;
    }
    
    function generateApplicationID($type){
        
        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        //bermulanya sebuah penciptaan id
        
        //check sequence
        if ($type == 780){
            $checkSequence = $this->lobjstudentregistrationModel->getSequence(1, date('Y'));

            if (!$checkSequence){
                $dataSeq = array(
                    'tsi_type'=>1, 
                    'tsi_seq_no'=>1, 
                    'tsi_seq_year'=>date('Y'), 
                    'tsi_upd_date'=>date('Y-m-d'), 
                    'tsi_upd_user'=>$userId
                );
                $this->lobjstudentregistrationModel->insertSequence($dataSeq);
            }

            //generate applicant ID
            $applicationid_format = array();

            //get sequence
            $sequence = $this->lobjstudentregistrationModel->getSequence(1, date('Y'));

            $config = $this->lobjstudentregistrationModel->getConfig(1);

            //format
            $format_config = explode('|',$config['creditTransferIdFormat']);
            //var_dump($format_config);
            for($i=0; $i<count($format_config); $i++){
                $format = $format_config[$i];
                if($format=='CT'){ //prefix
                    $result = $config['creditTransferPrefix'];
                }elseif($format=='YYYY'){
                    $result = date('Y');
                }elseif($format=='YY'){
                    $result = date('y');
                }elseif($format=='seqno'){				
                    $result = sprintf("%05d", $sequence['tsi_seq_no']);
                }
                array_push($applicationid_format,$result);
            }

            $applicationID = implode("",$applicationid_format);

            //update sequence		
            $this->lobjstudentregistrationModel->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        
        }else{
            $checkSequence = $this->lobjstudentregistrationModel->getSequence(2, date('Y'));

            if (!$checkSequence){
                $dataSeq = array(
                    'tsi_type'=>2, 
                    'tsi_seq_no'=>1, 
                    'tsi_seq_year'=>date('Y'), 
                    'tsi_upd_date'=>date('Y-m-d'), 
                    'tsi_upd_user'=>$userId
                );
                $this->lobjstudentregistrationModel->insertSequence($dataSeq);
            }

            //generate applicant ID
            $applicationid_format = array();

            //get sequence
            $sequence = $this->lobjstudentregistrationModel->getSequence(2, date('Y'));

            $config = $this->lobjstudentregistrationModel->getConfig(1);

            //format
            $format_config = explode('|',$config['exemptionIdFormat']);
            //var_dump($format_config);
            for($i=0; $i<count($format_config); $i++){
                $format = $format_config[$i];
                if($format=='EX'){ //prefix
                    $result = $config['exemptionPrefix'];
                }elseif($format=='YYYY'){
                    $result = date('Y');
                }elseif($format=='YY'){
                    $result = date('y');
                }elseif($format=='seqno'){				
                    $result = sprintf("%05d", $sequence['tsi_seq_no']);
                }
                array_push($applicationid_format,$result);
            }

            $applicationID = implode("",$applicationid_format);

            //update sequence		
            $this->lobjstudentregistrationModel->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        }

        return $applicationID;
    }
    
    public function revertApplicationAction(){
        $ctId = $this->_getParam('ctId', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($ctId != 0){
            $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
            $ctSubject = $this->lobjstudentregistrationModel->getCtSub($ctId);
            
            $statusData = array(
                'ApplicationStatus'=>784,
                'Comments'=>'',
                'UpdDate'=>date('Y-m-d'),
                'UpdUser'=>$userId
            );
            $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
            
            $statusHistoryData = array(
                'cth_ctid'=>$ctId,
                'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                'cth_newstatus'=>$statusData['ApplicationStatus'],
                'cth_upddate'=>date('Y-m-d'),
                'cth_upduser'=>$userId
            );
            $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
            
            if ($getCtInfo['subjectreg_id']!=''){
                $idRegSub = explode('|', $getCtInfo['subjectreg_id']);

                if (count($idRegSub) > 0){
                    foreach ($idRegSub as $idRegSubLoop){
                        $this->lobjstudentregistrationModel->deleteRegAuditPaper($idRegSubLoop);
                        $this->lobjstudentregistrationModel->deleteRegAuditPaperHistory($idRegSubLoop);
                    }
                }
            }
            
            if ($ctSubject){
                foreach ($ctSubject as $ctSubjectLoop){
                    if ($ctSubjectLoop['charge2']==1){
                        if ($ctSubjectLoop['invoiceid_1'] != 0){
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceClass->generateCreditNote($getCtInfo['IdStudentRegistration'], $ctSubjectLoop['EquivalentCourse'], $ctSubjectLoop['invoiceid_1'], 0);
                        
                            $dataCN= array(
                                'invoiceid_1'=>0
                            );

                            $this->lobjstudentregistrationModel->updateSubject($dataCN, $ctSubjectLoop['IdCreditTransferSubjects']);
                        }
                    }
                }
            }
        }
        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Revert success'));
        $this->_redirect($this->baseUrl . '/records/credittransferapp/ct-approve/IdCreditTransfer/'.$ctId);
    }
    
    public function getcoursetotransferAction(){
        $programId = $this->_getParam('programid', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseList = $this->lobjstudentregistrationModel->getCourseToTransfer($programId);
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
    
    public function approve($ctId, $url=false){
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
        $ctSubject2 = $this->lobjstudentregistrationModel->getCtSub2($ctId);

        $gradeModel = new Examination_Model_DbTable_Grade();
        
        //check subject register
        if ($ctSubject2){
            $courseArr = array();
            foreach ($ctSubject2 as $CourseLoop){
                array_push($courseArr, $CourseLoop['EquivalentCourse']);
            }
        }
        
        $checkSubReg = $this->lobjstudentregistrationModel->checkSubReg($getCtInfo['IdStudentRegistration'], $courseArr, $getCtInfo['semesterId']);
        //var_dump($checkSubReg); exit;
        if ($checkSubReg){
            //redirect here
            if ($url!=false){
                $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot approve this application because subject has been register'));
                $this->_redirect($url);
            }

            $return = false;
        }else{
            
            $this->updateStudentSemesterStatus($getCtInfo['IdStudentRegistration'],$getCtInfo['semesterId'],130);
            
            if ($ctSubject2){
                $subregId = array();
                
                if ($getCtInfo['applicationType']==780){
                    $examstatus = 'CT';
                }else{
                    $examstatus = 'EX';
                }
                
                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $checkInvoiceSetup = $invoiceClass->getActivityOtherData($getCtInfo['IdProgram'], 1);
                
                if ($checkInvoiceSetup){
                    $active = 0;
                }else{
                    $active = 1;
                }
                
                foreach ($ctSubject2 as $getCourseLoop){
                    $checkSubReg2 = $this->lobjstudentregistrationModel->checkSubReg2($getCtInfo['IdStudentRegistration'], $getCourseLoop['EquivalentCourse'], $getCtInfo['semesterId']);
                    
                    if ($checkSubReg2){
                        $this->lobjstudentregistrationModel->deleteWithdrawSubject($checkSubReg2['IdStudentRegSubjects']);
                    }
                    
                    $auditSub = array(
                        'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'], 
                        'IdSubject'=>$getCourseLoop['EquivalentCourse'], 
                        'IdSemesterMain'=>$getCtInfo['semesterId'], 
                        'SemesterLevel'=>count($this->lobjstudentregistrationModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1, 
                        //'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'credit_hour_registered'=>$getCourseLoop['EquivalentCreditHours'],
                        'exam_status'=>$examstatus,
                        'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'mark_approval_status'=>2,
                        'UpdUser'=>$userId, 
                        'UpdDate'=>date('Y-m-d'), 
                        'Active'=>$active
                    );

                    if ($getCourseLoop['EquivalentGrade']!='CT' && $getCourseLoop['EquivalentGrade']!='EX'){
                        $gradeInfo = $gradeModel->getGradeCTEX($getCtInfo['semesterId'], $getCtInfo['IdProgram'], $getCourseLoop['EquivalentCourse'], $getCourseLoop['EquivalentGrade']);

                        $auditSub['grade_point']=$gradeInfo['GradePoint'];
                    }

                    $studRegSubId = $this->lobjstudentregistrationModel->registerAuditPaper($auditSub);

                    $auditSubHis = array(
                        'IdStudentRegSubjects'=>$studRegSubId,
                        'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'],
                        'IdSubject'=>$getCourseLoop['EquivalentCourse'],
                        'IdSemesterMain'=>$getCtInfo['semesterId'],
                        'SemesterLevel'=>count($this->lobjstudentregistrationModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1,
                        //'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'credit_hour_registered'=>$getCourseLoop['EquivalentCreditHours'],
                        'exam_status'=>$examstatus,
                        'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'mark_approval_status'=>2,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'Active'=>$active
                    );
                    if ($getCourseLoop['EquivalentGrade']!='CT' && $getCourseLoop['EquivalentGrade']!='EX'){
                        $gradeInfo = $gradeModel->getGradeCTEX($getCtInfo['semesterId'], $getCtInfo['IdProgram'], $getCourseLoop['EquivalentCourse'], $getCourseLoop['EquivalentGrade']);

                        $auditSubHis['grade_point']=$gradeInfo['GradePoint'];
                    }
                    $this->lobjstudentregistrationModel->historyRegisterAuditPaper($auditSubHis);

                    array_push($subregId, $studRegSubId);
                }

                $implodeSubRegId = implode('|',$subregId);

                $imlplodeData = array(
                    'subjectreg_id'=>$implodeSubRegId
                );
                $this->lobjstudentregistrationModel->updateCtAppStatus($imlplodeData, $ctId);
            }

            $return = true;
        }
        
        return $return;
    }
    
    public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
    	
        //check current status
        $semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
        $semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);

        if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
            //nothing to update
        }else{
            //add new status & keep old status into history table
            $cms = new Cms_Status();
            $auth = Zend_Auth::getInstance();

            $data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
                'idSemester' => $IdSemesterMain,
                'IdSemesterMain' => $IdSemesterMain,								
                'studentsemesterstatus' => $newstatus, //Register idDefType = 32 (student semester status)
                'Level'=>0,
                'UpdDate' => date ( 'Y-m-d H:i:s'),
                'UpdUser' => $auth->getIdentity()->iduser
            );				

            $message = 'Audit Paper : Approval';
            $cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,3);
        }
    }
}

?>
