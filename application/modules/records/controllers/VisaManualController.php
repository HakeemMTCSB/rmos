<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 5/11/2015
 * Time: 10:31 AM
 */
class Records_VisaManualController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Records_Model_DbTable_VisaMigrate();
        $this->conn = Zend_Db_Table::getDefaultAdapter();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Visa Reminder');

        //echo $date=date('Y-m-d', strtotime('+6 month'));
        $this->view->sixmonthdate = date('d-m-Y', strtotime( '6 month' , strtotime(date('Y-m-d'))));
        $this->view->twomonthdate = date('d-m-Y', strtotime( '2 month' , strtotime(date('Y-m-d'))));

        $sended = false;
        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->visaReminder();
            $sended = true;
        }

        $this->view->sended = $sended;
    }

    public function visaReminder(){
        $invmodel = new icampus_Function_Studentfinance_Invoice();

        //echo 'Processing...';

        $remindStudent = array();

        // Notification Renewal Passport Validity: system will send x months before Student Passport expired date.
        $passport_list = $this->getStudentPassport(6,'MONTH');
        //var_dump($passport_list); //exit;
        if(count($passport_list)>0){
            foreach($passport_list as $passport){
                $passport['expiry_date']=date('d F Y', strtotime($passport['p_expiry_date']));
                $passport['before_expiry_date']=date('d F Y', strtotime( '-7 week' , strtotime($passport['p_expiry_date'])));
                $passport['expiry_desc']='';
                $this->sendMail($passport,91);

                $cursem = $this->getCurrentSemester($passport['IdScheme']);

                //@todo finance part
                $invmodel->generateInvoice($passport['IdStudentRegistration'], 143, $cursem['IdSemesterMaster']);
                $invmodel->generateInvoice($passport['IdStudentRegistration'], 144, $cursem['IdSemesterMaster']);
                $invmodel->generateInvoice($passport['IdStudentRegistration'], 146, $cursem['IdSemesterMaster']);

                $passport['type']='Student Passport';
                array_push($remindStudent, $passport);
            }
        }

        // Notification Renewal Student Pass (VISA): system will send x weeks before Student Pass expired date.
        $visa_list = $this->getStudentPass(2,'MONTH');
        //var_dump($visa_list); //exit;
        if(count($visa_list)>0){
            foreach($visa_list as $visa){
                $visa['expiry_date']=date('d F Y', strtotime($visa['av_expiry']));
                $visa['before_expiry_date']=date('d F Y', strtotime( '-7 week' , strtotime($visa['av_expiry'])));
                $visa['expiry_desc']='';
                //var_dump($visa); exit;
                $this->sendMail($visa,93);

                $cursem = $this->getCurrentSemester($visa['IdScheme']);

                //@todo finance part
                $invmodel->generateInvoice($visa['IdStudentRegistration'], 80, $cursem['IdSemesterMaster']);

                $visa['type']='Student Visa';
                array_push($remindStudent, $visa);
            }
        }


        // Notification Renewal Dependent Pass: system will send x weeks before Dependent Pass expired date.
        $dep_pass_list = $this->getDependentPass(2,'MONTH');
        //var_dump($dep_pass_list); //exit;
        if(count($dep_pass_list)>0){
            foreach($dep_pass_list as $sdp){
                $sdp['expiry_date']=date('d F Y', strtotime($sdp['sdp_expiry_date']));
                $sdp['before_expiry_date']=date('d F Y', strtotime( '-7 week' , strtotime($sdp['sdp_expiry_date'])));
                $sdp['expiry_desc']='';
                $this->sendMail($sdp,92);

                $cursem = $this->getCurrentSemester($sdp['IdScheme']);

                //@todo finance part
                $invmodel->generateInvoice($sdp['IdStudentRegistration'], 105, $cursem['IdSemesterMaster']);

                $sdp['type']='Dependent Passport';
                array_push($remindStudent, $sdp);
            }
        }

        $this->view->remindStudent = $remindStudent;
    }

    public function sendMail($student,$tpl_id){

        $templateData = $this->getTemplate($tpl_id);
        //var_dump($templateData); exit;
        if($templateData){
            $templateMail = $templateData['tpl_content'];

            $templateMail = $this->parseTag($student,$tpl_id,$templateMail);

            $email_receipient = $student["appl_email"];
            $email_personal_receipient = $student["appl_email_personal"];
            $name_receipient  = $student["appl_fname"].' '.$student["appl_lname"];

            //email personal
            $data_inceif = array(
                //'recepient_email' => $email_personal_receipient,
                'recepient_email' => 'norhisham@inceif.org',
                'subject' => $templateData["tpl_name"],
                'content' => $templateMail	,
                'attachment_path'=>null,
                'attachment_filename'=>null	,
                'date_que' => date("Y-m-d H:i:s")
            );
            //add email to email que
            $this->addData($data_inceif);

            //email admission
            /*$data_inceif_admin = array(
                //'recepient_email' => $templateData['email_from'],
                'recepient_email' => 'nurazeta@meteor.com.my',
                'subject' => $templateData["tpl_name"],
                'content' => $templateMail	,
                'attachment_path'=>null,
                'attachment_filename'=>null	,
                'date_que' => date("Y-m-d H:i:s")
            );
            //add email to email que
            $this->addData($data_inceif_admin);*/

            $data_compose = array(
                'comp_subject'=> $templateData["tpl_name"],
                'comp_module' => 'records',
                'comp_tpl_id' => $templateData["tpl_id"],
                'comp_type' => 585,
                'comp_rectype' => 'students',
                'comp_lang' => 'en_US',
                'comp_totalrecipients' => 1,
                'comp_rec' => $student['IdStudentRegistration'],
                'comp_content' => $templateMail,
                'created_by' => 1,
                'created_date' => date('Y-m-d H:i:s')
            );
            $comId = $this->insertCommpose($data_compose);

            $data_compose_rec = array(
                'cr_comp_id' => $comId,
                'cr_subject' => $templateData["tpl_name"],
                'cr_content' => $templateMail,
                'cr_rec_id' => $student['IdStudentRegistration'],
                'cr_email' => 'norhisham@inceif.org',
                //'cr_email' => $student['appl_email_personal'],
                'cr_status' => 1,
                'cr_datesent' => date('Y-m-d H:i:s')
            );
            $this->insertComposeRec($data_compose_rec);
        }//end if email template exist
    }

    protected function getStudentPassport($expr,$unit){

        $select = $this->conn->select()->from(array('p' => 'student_passport'), array('value'=>'*', ''))
            ->join(array('sr' => 'tbl_studentregistration'),'sr.sp_id=p.sp_id',array('registrationId', 'IdStudentRegistration'))
            ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_salutation','appl_email_personal','appl_email'))
            ->joinLeft(array('pr'=>'tbl_program'), 'sr.IdProgram=pr.IdProgram')
            ->where('sr.profileStatus=92')
            ->where('sr.student_type=740')
            ->where('p.p_expiry_date = DATE_ADD(CURDATE(),INTERVAL '.$expr.' '.$unit.')')
            ->where('p_active = ?', 1);
        $student_list = $this->conn->fetchAll($select);

        return $student_list;
    }

    protected function getStudentPass($expr,$unit){

        $select = $this->conn->select()->from(array('v' => 'student_visa'))
            ->join(array('sr' => 'tbl_studentregistration'),'sr.sp_id=v.sp_id',array('registrationId', 'IdStudentRegistration'))
            ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_salutation','appl_email_personal','appl_email'))
            ->joinLeft(array('p'=>'tbl_program'), 'sr.IdProgram=p.IdProgram')
            ->where('sr.profileStatus=92')
            ->where('sr.student_type=740')
            ->where('v.av_expiry = DATE_ADD(CURDATE(),INTERVAL '.$expr.' '.$unit.')')
            ->where('av_active = ?', 1);
        $student_list = $this->conn->fetchAll($select);

        return $student_list;
    }

    protected function getDependentPass($expr,$unit){

        $select = $this->conn->select()->from(array('sdp' => 'student_dependent_pass'))
            ->join(array('sr' => 'tbl_studentregistration'),'sr.sp_id=sdp.sp_id',array('registrationId', 'IdStudentRegistration'))
            ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname','appl_salutation','appl_email_personal','appl_email'))
            ->joinLeft(array('p'=>'tbl_program'), 'sr.IdProgram=p.IdProgram')
            ->where('sr.profileStatus=92')
            ->where('sr.student_type=740')
            ->where('sdp.sdp_expiry_date = DATE_ADD(CURDATE(),INTERVAL '.$expr.' '.$unit.')')
            ->where('sdp_active = ?', 1);
        $student_list = $this->conn->fetchAll($select);

        return $student_list;
    }

    protected function addData($data){
        $this->conn->insert('email_que', $data);
    }

    public function getTemplate($tpl_id,$locale='en_US'){

        $select = $this->conn->select()
            ->from(array('ct'=>'comm_template'))
            ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id=ct.tpl_id')
            ->where('ct.tpl_id  = ?',$tpl_id)
            ->where('ctc.locale = ?',$locale);

        $row = $this->conn->fetchRow($select);
        return $row;
    }

    public function getTemplateTag($tplId){
        $lstrSelect = $this->conn->select()
            ->from(array("a"=>"comm_template_tags"),array("value"=>"a.*"))
            ->where('a.tpl_id = '.$tplId.' or a.tpl_id = 0')
            ->order("a.tpl_id");
        $larrResult = $this->conn->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function parseTag($student, $tpl_id, $content){
        $templateTags = $this->getTemplateTag($tpl_id);

        foreach ($templateTags as $tag){
            $tag_name = $tag['tag_name'];

            if($tag_name=='Salutation') {
                $content = str_replace("[".$tag['tag_name']."]", $student['appl_salutation'], $content);
            }
            else if($tag_name=='Student Name') {
                $content = str_replace("[".$tag['tag_name']."]", $student['appl_fname'].' '.$student['appl_lname'], $content);
            }
            else if($tag_name=='Student ID') {
                $content = str_replace("[".$tag['tag_name']."]", $student['registrationId'], $content);
            }else if($tag_name=='Expiry Date') {
                $content = str_replace("[".$tag['tag_name']."]", $student['expiry_date'], $content);
            }else if($tag_name=='Expiry Desc') {
                $content = str_replace("[".$tag['tag_name']."]", $student['expiry_desc'], $content);
            }else if($tag_name=='Before Expiry Date'){
                $content = str_replace("[".$tag['tag_name']."]", $student['before_expiry_date'], $content);
            }
        }

        return $content;
    }

    public function insertCommpose($bind){
        //$db = Zend_Db_Table::getDefaultAdapter();
        $this->conn->insert('comm_compose', $bind);
        $id = $this->conn->lastInsertId('comm_compose', 'comp_id');
        return $id;
    }

    public function insertComposeRec($bind){
        //$db = Zend_Db_Table::getDefaultAdapter();
        $this->conn->insert('comm_compose_recipients', $bind);
        $id = $this->conn->lastInsertId('comm_compose_recipients', 'cr_id');
        return $id;
    }

    public function getCurrentSemester($idScheme){
        $date = date('Y-m-d');

        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date);

        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
}