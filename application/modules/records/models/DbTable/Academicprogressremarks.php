<?php
class Records_Model_DbTable_Academicprogressremarks extends Zend_Db_Table {
    
    public function getSubjectById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectmaster'), array('value'=>'*'))
            ->where('a.IdSubject = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSubjectReplace($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegSubjects = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
}
