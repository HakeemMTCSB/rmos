<?php

class Records_Model_DbTable_Clearance extends Zend_Db_Table_Abstract {

    protected $_kmc = 'department_clearance_kmc';
    protected $_finance = 'department_clearance_finance';
    
    public function addData($table,$data){	
        $db = Zend_Db_Table::getDefaultAdapter();	
        $id = $db->insert($table,$data);
        return $id;
    }

    public function updateData($table,$data,$id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update($table,$data, 'IdChangeStatusApplication = '. (int)$id);
    }

    public function deleteData($table,$id){		
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->delete($table,$this->_primary .' =' . (int)$id);
    }
}

?>