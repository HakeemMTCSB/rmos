<?php

class Records_Model_DbTable_Studentprofile extends Zend_Db_Table
{ //Model Class for Users Details
    protected $_name = 'student_profile';
    protected $_primary = 'id';

    protected $_dependentTables = array('App_Model_Record_DbTable_StudentRegistration');


    public function getData($id = null)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_profile"), array("a.*"));

        if ($id != null) {
            $lstrSelect->where('a.id =?', $id);
            $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        } else {
            $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        }


        return $larrResult;
    }

    public function fngetStudentprofileDtls()
    { //Function to get the user details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"), array("a.*"))
            ->join(array("b" => "tbl_studentapplication"), 'a.registrationId=b.registrationId')
            ->join(array("c" => "tbl_program"), 'c.IdProgram=b.IDCourse')
            ->join(array("d" => "tbl_semester"), 'd.IdSemester=a.IdSemester', array("d.SemesterCode AS Semester"))
            //->where("b.Registered = 1")
            //->where("b.Offered = 1")
            //->where("b.Accepted = 1")
            ->order("b.FName");
        //echo $lstrSelect;die();
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetApplicantNameList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sa" => "tbl_studentapplication"), array("key" => "sa.IdApplication", "value" => "CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
            ->where("sa.Active = 1")
            ->order("sa.FName");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetLandscapeList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sa" => "tbl_definationms"), array("sa.*"))
            ->join(array("sb" => "tbl_landscape"), 'sb.LandscapeType = sa.idDefinition', array("key" => "sb.IdLandscape", "value" => "CONCAT_WS('-',IFNULL(sa.DefinitionDesc,''),IFNULL(sb.IdLandscape,''))"))
            ->where("sa.Status = 1");

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetCollegeList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_collegemaster"), array("key" => "a.IdCollege", "value" => "a.CollegeName"));


        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetRegStudentList()
    { //Function to get the user details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"))
            ->join(array("b" => "student_profile"), 'a.IdApplication=b.appl_id');


        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnSearchStudentApplication($post = array())
    { //Function to get the user details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"))
            ->join(array("sa" => "tbl_studentapplication"), 'a.IdApplication =sa.IdApplication ')
            ->join(array("p" => "tbl_program"), 'p.IdProgram=sa.IDCourse', array("p.*"))
            ->join(array("q" => "tbl_semester"), 'q.IdSemester=sa.idCollege', array("q.ShortName AS Semester"));

        if (isset($post['field5']) && !empty($post['field5'])) {
            $lstrSelect = $lstrSelect->where("sa.IdApplication = ?", $post['field5']);

        }
        if (isset($post['field12']) && !empty($post['field12'])) {
            $lstrSelect = $lstrSelect->where("sa.idCollege = ?", $post['field12']);

        }
        if (isset($post['field8']) && !empty($post['field8'])) {
            $lstrSelect = $lstrSelect->where("sa.IDCourse = ?", $post['field8']);

        }
        $lstrSelect->where('sa.ICNumber like "%" ? "%"', $post['field2'])
            ->where('sa.StudentId like "%" ? "%"', $post['field4'])
            ->where("sa.Registered = 1")
            ->where("sa.Offered = 1")
            ->where("sa.Accepted = 1")
            ->order("sa.FName");
        //echo $lstrSelect;die();
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnViewStudentAppnDetails($lintIdApplication)
    {

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"), array("a.registrationId", "IdStudentRegistration"))
            ->join(array("sa" => "student_profile"), 'a.IdApplication =sa.appl_id')
            ->joinLeft(array("c" => "tbl_countries"), 'c.idCountry = sa.appl_province', array("c.CountryName AS CountryName"))
            ->join(array("p" => "tbl_program"), 'a.IdProgram = p.IdProgram')
            //->join(array("cm"=>"tbl_collegemaster"),'sa.idCollege  = cm.IdCollege')
            ->joinleft(array("l" => "tbl_landscape"), 'a.IdLandscape = l.IdLandscape')
            //->join(array("se"=>"tbl_semester"),'l.IdStartSemester  = se.IdSemester')
            //->join(array("sm"=>"tbl_semestermaster"),'se.Semester = sm.IdSemesterMaster')
            ->joinleft(array("dec" => "tbl_definationms"), 'dec.idDefinition = l.LandscapeType', array("dec.DefinitionDesc"))
            ->where('a.IdStudentRegistration = ?', $lintIdApplication);
        //->where('l.Active = 123');
        //echo $lstrSelect;
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);

        return $larrResult;
    }

    public function fnSearchRegisteredStudentApplication($post = array())
    { //Function to get the user details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sr" => "tbl_studentregistration"), array('sr.registrationId AS StudentId', 'sr.registrationId', 'sr.IdStudentRegistration', 'sr.FName', 'sr.MName', 'sr.LName'))
            ->join(array("a" => "student_profile"), 'sr.IdApplication=a.appl_id', array('appl_fname', 'appl_mname', 'appl_lname'))
            ->joinLeft(array("p" => "tbl_program"), 'sr.IdProgram = p.IdProgram', array("p.ProgramName"))
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition = sr.profileStatus', array('d.DefinitionDesc'));
        if (isset($post['field3']) && !empty($post['field3'])) {
            $lstrSelect->where('LOWER(a.appl_fname) like "%" ? "%"', strtolower($post['field3']))
                ->orwhere('LOWER(a.appl_mname) like "%" ? "%"', strtolower($post['field3']))
                ->orwhere('LOWER(a.appl_lname) like "%" ? "%"', strtolower($post['field3']));
        }
        if (isset($post['field2']) && !empty($post['field2'])) {
            $lstrSelect->where('sr.registrationId like "%" ? "%"', $post['field2']);
        }
        if (isset($post['field8']) && !empty($post['field8'])) {
            $lstrSelect = $lstrSelect->where("sr.IdProgram = ?", $post['field8']);

        }
        if (isset($post['field4']) && !empty($post['field4'])) {
            $lstrSelect->where('sr.registrationId like "%" ? "%"', $post['field4']);
        }
        if (isset($post['field25']) && !empty($post['field25'])) {
            $lstrSelect->where('sr.profileStatus = ?', $post['field25']);
        }
        if (isset($post['field1']) && !empty($post['field1'])) {

            $semIDs = explode('_', $post['field1']);
            if ($semIDs['1'] == 'main') {
                $where = "s.IdSemesterMain = '" . $semIDs['0'] . "' ";
            }
            if ($semIDs['1'] == 'detail') {
                $where = "s.idSemester = '" . $semIDs['0'] . "' ";
            }

            $lstrSelect = $lstrSelect->joinLeft(array("s" => "tbl_studentsemesterstatus"), 'sr.IdStudentRegistration = s.IdStudentRegistration', array())
                ->where($where)
                //->orwhere('s.IdSemesterMain = ?',$post['field1'])
                ->order('s.UpdDate DESC');

        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }

    /**
     * Academic progress search student screen
     */
    public function fnSearchRegisteredStudentApplicationAP($post = array())
    {


        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        // asd($post);
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sr" => "tbl_studentregistration"), array('sr.registrationId AS StudentId', 'sr.registrationId', 'sr.IdStudentRegistration', 'sr.FName', 'sr.MName', 'sr.LName'))
            ->join(array("a" => "student_profile"), 'sr.IdApplication=a.appl_id', array('a.appl_fname', 'a.appl_mname', 'a.appl_lname'))
            ->joinLeft(array("p" => "tbl_program"), 'sr.IdProgram = p.IdProgram', array("p.ProgramName"))
            ->joinLeft(array('sm' => 'tbl_studentsemesterstatus'), ' ( sm.IdStudentRegistration = sr.IdStudentRegistration )')
            //->joinLeft(array('prg'=>'tbl_program'),' ( prg.IdProgram=sr.IdProgram )',array())
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition = sr.profileStatus', array('d.DefinitionDesc'));
        $wh = "1=1 ";

        if (isset($post['field23']) && !empty($post['field23'])) {
            //$lstrSelect = $lstrSelect->join(array('prg'=>'tbl_program'),'prg.IdProgram=sr.IdProgram');
            $lstrSelect = $lstrSelect->join(array('schm' => 'tbl_scheme'), 'p.IdScheme=schm.IdScheme');
            //$lstrSelect = $lstrSelect->where("schm.IdScheme = ?",$post['field23']);
            $wh .= " AND schm.IdScheme = '" . $post['field23'] . "' ";
        }

        if (isset($post['field8']) && !empty($post['field8'])) {
            //$lstrSelect = $lstrSelect->where("sa.ProgramOfferred = ?",$post['field8']);
            $wh .= " AND sr.IdProgram = '" . $post['field8'] . "' ";
        }

        if (isset($post['field24']) && !empty($post['field24'])) {
            //$lstrSelect = $lstrSelect->where("sa.intake = ?",$post['field24']);
            $wh .= " AND sr.intake = '" . $post['field24'] . "' ";
        }

        if (isset($post['field4']) && !empty($post['field4'])) {
            //$lstrSelect = $lstrSelect->where('sa.FName like "%" ? "%"',$post['field4']);
            $wh .= " AND sr.appl_fname LIKE '%" . $post['field4'] . "%' ";
        }

        if (isset($post['field3']) && !empty($post['field3'])) {
            //$lstrSelect = $lstrSelect->where("sa.IdApplicant = ?",$post['field3']);
            $wh .= " AND sr.registrationId LIKE '%" . $post['field3'] . "%' ";
        }

        if (isset($post['field27']) && !empty($post['field27'])) {
            //$lstrSelect = $lstrSelect->join(array('prg'=>'tbl_program'),'prg.IdProgram=sr.IdProgram');
            $lstrSelect = $lstrSelect->join(array('cm' => 'tbl_collegemaster'), 'p.IdCollege = cm.IdCollege');
            //$lstrSelect = $lstrSelect->where("schm.IdScheme = ?",$post['field23']);
            $wh .= " AND cm.IdCollege = '" . $post['field27'] . "' ";
        }


        if (isset($post['field25']) && !empty($post['field25'])) {
            $wh .= " AND sr.profileStatus = '" . $post['field25'] . "' ";
        }

        if (isset($post['field1']) && !empty($post['field1'])) {
            $lstrSelect = $lstrSelect->joinLeft(array('s' => 'tbl_studentsemesterstatus'), ' ( s.IdStudentRegistration = sr.IdStudentRegistration )', array());
            $semIDs = explode('_', $post['field1']);
            if ($semIDs['1'] == 'main') {
                $wh .= " AND s.IdSemesterMain = '" . $semIDs['0'] . "' ";
            }
            if ($semIDs['1'] == 'detail') {
                $wh .= " AND s.idSemester = '" . $semIDs['0'] . "' ";
            }
            // $wh .= " AND sr.profileStatus = '".$post['field25']."' ";
            //$lstrSelect = $lstrSelect->joinLeft(array("s"=>"tbl_studentsemesterstatus"),'sr.IdStudentRegistration = s.IdStudentRegistration',array())
            // -> where($where)
            //->orwhere('s.IdSemesterMain = ?',$post['field1'])
            //->order('s.UpdDate DESC');
            //->limit('1');
        }

        $lstrSelect = $lstrSelect->where($wh);
        $lstrSelect = $lstrSelect->group(array("sr.IdStudentRegistration"))->order("sr.FName ASC");
        //echo $lstrSelect; die;
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;


    }


    public function getstudentapplicationid($IdRegistration, $OldIdStudentRegistration)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentapplication"), array('a.IdApplication'))
            ->where("a.registrationId = ?", $IdRegistration)
            ->orwhere("a.registrationId = ?", $OldIdStudentRegistration);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetqualificationdetails($applicationId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studenteducationdetails"), array('a.*'))
            ->joinLeft(array("b" => "tbl_institution"), 'a.IdInstitute = b.idInstitution', array("b.*", "b.InstitutionName AS Institute"))
            ->joinLeft(array("c" => "tbl_countries"), 'b.Country = c.idCountry', array('c.CountryName'))
            ->joinLeft(array("d" => "tbl_state"), 'b.State = d.idState', array('d.StateName'))
            ->joinLeft(array('e' => "tbl_definationms"), 'a.IdResultItem = e.idDefinition', array('e.DefinitionDesc'))
            ->joinLeft(array('f' => 'tbl_qualificationmaster'), 'a.IdQualification = f.IdQualification', array('f.QualificationLevel'))
            ->where("a.IdApplication = ?", $applicationId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetprofilestatus($lintIdApplication)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"), array('a.UpdDate'))
            ->join(array("b" => "tbl_definationms"), 'a.profileStatus = b.idDefinition', array("b.DefinitionDesc"))
            ->where("a.IdStudentRegistration = ?", $lintIdApplication);

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetLandscapeDetails($lintIdApplication)
    {

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sa" => "tbl_studentapplication"))
            ->join(array("m" => "tbl_studentregistration"), 'sa.IdApplication = m.IdApplication')
            ->join(array("l" => "tbl_landscape"), 'l.IdLandscape = m.IdLandscape')
            ->join(array("dec" => "tbl_definationms"), 'dec.idDefinition = l.LandscapeType', array("dec.DefinitionDesc"))
            ->where('sa.IdApplication = ?', $lintIdApplication)
            ->where('l.Active = 123');

        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnViewAcademicLandscapeDtls($lintIdApplication)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sa" => "tbl_studentapplication"), array("sa.*"))
            ->join(array("l" => "tbl_landscape"), 'sa.IDCourse = l.IdProgram', array("l.LandscapeType"))
            //->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
            // ->join(array("lbs"=>"tbl_landscapeblocksemester"),'l.IdLandscape = lbs.IdLandscape',array("lbs.semesterid"))
            //->join(array("ls"=>"tbl_landscapesubject"),'l.IdLandscape = ls.IdLandscape')
            //->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject')
            //->join(array("dms"=>"tbl_definationms"),'ls.SubjectType = dms.idDefinition')
            ->where('sa.IdApplication = ?', $lintIdApplication)
            ->where('l.Active = 123');

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnViewAcademicLandscapeBlockSubjectDtls($lintIdApplication)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sa" => "tbl_studentapplication"), array("sa.*"))
            ->join(array("sr" => "tbl_studentregistration"), 'sa.IdApplication = sr.IdApplication AND sr.Semesterstatus=0')
            ->join(array("l" => "tbl_landscape"), 'sa.IDCourse = l.IdProgram', array("l.LandscapeType"))
            ->join(array("lbs" => "tbl_landscapeblocksemester"), 'l.IdLandscape = lbs.IdLandscape', array("lbs.semesterid"))
            ->join(array("ls" => "tbl_landscapeblocksubject"), 'l.IdLandscape = ls.IdLandscape')
            ->join(array("srs" => "tbl_studentregsubjects"), 'sr.IdStudentRegistration=srs.IdStudentRegistration AND ls.subjectid=srs.IdSubject')
            ->join(array("sm" => "tbl_subjectmaster"), 'srs.IdSubject = sm.IdSubject', array("sm.SubjectName", "sm.SubCode", "sm.CreditHours", "CONCAT_WS(' ',IFNULL(sm.SubjectName,''),IFNULL(sm.SubCode,'')) as subjectwithcode"))
            ->where('sa.IdApplication = ?', $lintIdApplication)
            ->where('l.Active = 123')
            ->group('sm.IdSubject');

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*	public function fnViewAcademicLandscapeSubjectDtls($lintIdApplication){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $lstrSelect = $lobjDbAdpt->select()
                              ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
                              ->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
                              ->join(array("ls"=>"tbl_landscapesubject"),'l.IdLandscape = ls.IdLandscape')
                              ->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject')
                              ->join(array("dms"=>"tbl_definationms"),'ls.SubjectType = dms.idDefinition')
                              ->where('sa.IdApplication = ?',$lintIdApplication)
                              ->where('l.Active = 123');
            $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
            return $larrResult;
        }*/

//	public function fnViewAcademicLandscapeSubjectDtls($lintIdApplication){
//		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//		$lstrSelect = $lobjDbAdpt->select()
//		 				 ->from(array("sa"=>"tbl_studentregistration"),array("sa.*"))
//		 				 ->join(array("ls"=>"tbl_studentregsubjects"),'sa.IdStudentRegistration = ls.IdStudentRegistration')
//		 				 ->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject',array("sm.SubjectName","sm.SubCode","sm.CreditHours","CONCAT_WS(' ',IFNULL(sm.SubjectName,''),IFNULL(sm.SubCode,'')) as subjectwithcode"))
//		 				 ->where('sa.IdApplication = ?',$lintIdApplication)
//		 				 ->where('sm.Active  = 1');
//		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 				 
//		return $larrResult;
//	}

//    public function fnupdateStudentAppn($lintiIdApplication, $larrformData ) { //Function for updating the user
//    	
//    $larrformData['HomePhone'] = $larrformData['HomePhonecountrycode']."-".$larrformData['HomePhonestatecode']."-".$larrformData['HomePhone'];
//		unset($larrformData['HomePhonecountrycode']);
//		unset($larrformData['HomePhonestatecode']);
//	$larrformData['CellPhone'] = $larrformData['CellPhonecountrycode']."-".$larrformData['CellPhonestatecode']."-".$larrformData['CellPhone'];
//		unset($larrformData['CellPhonecountrycode']);
//		unset($larrformData['CellPhonestatecode']);
//	$larrformData['Fax'] = $larrformData['Faxcountrycode']."-".$larrformData['Faxstatecode']."-".$larrformData['Fax'];
//		unset($larrformData['Faxcountrycode']);
//		unset($larrformData['Faxstatecode']);
//        unset($larrformData['Landscape']);		
// 
//			
//	$where = 'IdApplication = '.$lintiIdApplication;
//	$this->update($larrformData,$where);
//    }
//    if (isset($post['field12']) && !empty($post['field12'])) {
//      $lstrSelect = $lstrSelect->where("sa.idCollege = ?", $post['field12']);
//    }
//    if (isset($post['field8']) && !empty($post['field8'])) {
//      $lstrSelect = $lstrSelect->where("sa.IDCourse = ?", $post['field8']);
//    }
//    $lstrSelect->where('sa.ICNumber like "%" ? "%"', $post['field2'])
//            ->where('sa.StudentId like "%" ? "%"', $post['field4'])
//            ->where("sa.Registered = 1")
//            ->where("sa.Offered = 1")
//            ->where("sa.Accepted = 1")
//            ->order("sa.FName");
//    //echo $lstrSelect;die();
//    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
//    return $larrResult;
//  }

//  public function fnViewStudentAppnDetails($lintIdApplication) {
//
//    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//    $lstrSelect = $lobjDbAdpt->select()
//                    ->from(array("a" => "tbl_studentregistration"), array("a.registrationId"))
//                    ->join(array("sa" => "tbl_studentapplication"), 'a.IdApplication =sa.IdApplication')
//                    ->join(array("c" => "tbl_countries"), 'c.idCountry = sa.PermCountry', array("c.CountryName AS CountryName"))
//                    ->join(array("p" => "tbl_program"), 'sa.IDCourse = p.IdProgram')
//                    ->join(array("cm" => "tbl_collegemaster"), 'sa.idCollege  = cm.IdCollege')
//                    ->join(array("l" => "tbl_landscape"), 'sa.IDCourse = l.IdProgram')
//                    ->join(array("se" => "tbl_semester"), 'l.IdStartSemester  = se.IdSemester')
//                    ->join(array("sm" => "tbl_semestermaster"), 'se.Semester = sm.IdSemesterMaster')
//                    ->join(array("dec" => "tbl_definationms"), 'dec.idDefinition = l.LandscapeType', array("dec.DefinitionDesc"))
//                    ->where('sa.IdApplication = ?', $lintIdApplication)
//                    ->where('l.Active = 123');
//
//    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
//    return $larrResult;
//  }

//  public function fngetLandscapeDetails($lintIdApplication) {
//
//    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//    $lstrSelect = $lobjDbAdpt->select()
//                    ->from(array("sa" => "tbl_studentapplication"))
//                    ->join(array("m" => "tbl_studentregistration"), 'sa.IdApplication = m.IdApplication')
//                    ->join(array("l" => "tbl_landscape"), 'l.IdLandscape = m.IdLandscape')
//                    ->join(array("dec" => "tbl_definationms"), 'dec.idDefinition = l.LandscapeType', array("dec.DefinitionDesc"))
//                    ->where('sa.IdApplication = ?', $lintIdApplication)
//                    ->where('l.Active = 123');
//
//    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
//    return $larrResult;
//  }

//  public function fnViewAcademicLandscapeDtls($lintIdApplication) {
//    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//    $lstrSelect = $lobjDbAdpt->select()
//                    ->from(array("sa" => "tbl_studentapplication"), array("sa.*"))
//                    ->join(array("l" => "tbl_landscape"), 'sa.IDCourse = l.IdProgram', array("l.LandscapeType"))
//                    //->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
//                    // ->join(array("lbs"=>"tbl_landscapeblocksemester"),'l.IdLandscape = lbs.IdLandscape',array("lbs.semesterid"))
//                    //->join(array("ls"=>"tbl_landscapesubject"),'l.IdLandscape = ls.IdLandscape')
//                    //->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject')
//                    //->join(array("dms"=>"tbl_definationms"),'ls.SubjectType = dms.idDefinition')
//                    ->where('sa.IdApplication = ?', $lintIdApplication)
//                    ->where('l.Active = 123');
//
//    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
//    return $larrResult;
//  }

//  public function fnViewAcademicLandscapeBlockSubjectDtls($lintIdApplication) {
//    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//    $lstrSelect = $lobjDbAdpt->select()
//                    ->from(array("sa" => "tbl_studentapplication"), array("sa.*"))
//                    ->join(array("sr" => "tbl_studentregistration"), 'sa.IdApplication = sr.IdApplication AND sr.Semesterstatus=0')
//                    ->join(array("l" => "tbl_landscape"), 'sa.IDCourse = l.IdProgram', array("l.LandscapeType"))
//                    ->join(array("lbs" => "tbl_landscapeblocksemester"), 'l.IdLandscape = lbs.IdLandscape', array("lbs.semesterid"))
//                    ->join(array("ls" => "tbl_landscapeblocksubject"), 'l.IdLandscape = ls.IdLandscape')
//                    ->join(array("srs" => "tbl_studentregsubjects"), 'sr.IdStudentRegistration=srs.IdStudentRegistration AND ls.subjectid=srs.IdSubject')
//                    ->join(array("sm" => "tbl_subjectmaster"), 'srs.IdSubject = sm.IdSubject', array("sm.SubjectName", "sm.SubCode", "sm.CreditHours", "CONCAT_WS(' ',IFNULL(sm.SubjectName,''),IFNULL(sm.SubCode,'')) as subjectwithcode"))
//                    ->where('sa.IdApplication = ?', $lintIdApplication)
//                    ->where('l.Active = 123')
//                    ->group('sm.IdSubject');
//
//    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
//    return $larrResult;
//  }

    /* 	public function fnViewAcademicLandscapeSubjectDtls($lintIdApplication){
      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
      $lstrSelect = $lobjDbAdpt->select()
      ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
      ->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
      ->join(array("ls"=>"tbl_landscapesubject"),'l.IdLandscape = ls.IdLandscape')
      ->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject')
      ->join(array("dms"=>"tbl_definationms"),'ls.SubjectType = dms.idDefinition')
      ->where('sa.IdApplication = ?',$lintIdApplication)
      ->where('l.Active = 123');
      $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
      return $larrResult;
      } */

    public function fnViewAcademicLandscapeSubjectDtls($lintIdApplication)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sa" => "tbl_studentregistration"), array("sa.*"))
            ->join(array("ls" => "tbl_studentregsubjects"), 'sa.IdStudentRegistration = ls.IdStudentRegistration')
            ->join(array("sm" => "tbl_subjectmaster"), 'ls.IdSubject = sm.IdSubject', array("sm.SubjectName", "sm.SubCode", "sm.CreditHours", "CONCAT_WS(' ',IFNULL(sm.SubjectName,''),IFNULL(sm.SubCode,'')) as subjectwithcode"))
            ->where('sa.IdApplication = ?', $lintIdApplication)
            ->where('sm.Active  = 1');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnupdateStudentAppn($lintiIdApplication, $larrformData)
    { //Function for updating the user
        $larrformData['HomePhone'] = $larrformData['HomePhonecountrycode'] . "-" . $larrformData['HomePhonestatecode'] . "-" . $larrformData['HomePhone'];
        unset($larrformData['HomePhonecountrycode']);
        unset($larrformData['HomePhonestatecode']);
        $larrformData['CellPhone'] = $larrformData['CellPhonecountrycode'] . "-" . $larrformData['CellPhonestatecode'] . "-" . $larrformData['CellPhone'];
        unset($larrformData['CellPhonecountrycode']);
        unset($larrformData['CellPhonestatecode']);
        $larrformData['Fax'] = $larrformData['Faxcountrycode'] . "-" . $larrformData['Faxstatecode'] . "-" . $larrformData['Fax'];
        unset($larrformData['Faxcountrycode']);
        unset($larrformData['Faxstatecode']);
        unset($larrformData['Landscape']);


        $where = 'IdApplication = ' . $lintiIdApplication;
        $this->update($larrformData, $where);
    }

    public function fnUpdateLandscape($lastrecieptid, $resultcode)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_studentregistration";
        $where = "IdApplication = '$lastrecieptid'";
        $postData = array('IdLandscape' => $resultcode);
        $db->update($table, $postData, $where);
    }

    public function fnGetCollegeDetails($idstudent)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_collegemaster"))
            ->join(array("b" => "tbl_program"), 'a.IdCollege = b.IdCollege', array("key" => "b.IdProgram", "value" => "b.ProgramName"))
            ->where("b.IdCollege =" . $idstudent);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetStudentProfileList()
    { //Function to get the student profile details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sp" => "student_profile"))
            ->join(array('sr' => 'tbl_studentregistration'), 'sp.appl_id=sr.IdApplication')
            ->join(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=sr.Status', array('deftn.DefinitionCode'))
            ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('prg.ArabicName', 'ProgramName'))
            ->join(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeId'));


        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetStudentRegisterdNim($appl_id)
    {

    }

    public function fnGetStudentProfileByNim($nim)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sp" => "student_profile"))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdApplication = sp.appl_id', array('registrationId'))
            ->where('sr.registrationId = ?', $nim);


        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnGetStudentProfileByIdStudentRegistration($IdStudentRegistration)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sp" => "student_profile"))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdApplication = sp.appl_id', array('registrationId'))
            ->where('sr.IdStudentRegistration = ?', $IdStudentRegistration);


        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnGetStudentProfileByApplicationId($appl_id)
    {

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("sp" => "student_profile"))
            ->joinLeft(array('city' => 'tbl_city'), 'sp.appl_city = city.idCity', array('cityName' => 'city.CityName'))
            ->where('sp.appl_id =?', $appl_id);


        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function addData($data)
    {
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        return $id = $db->lastInsertId();
    }

    public function updateData($data, $id)
    {
        $this->update($data, 'id = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $this->delete($this->_primary . ' =' . (int)$id);
    }

    public function getProfile($sp_id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('sp' => 'student_profile'))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.sp_id = sp.id')
            ->join(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('program_id' => 'p.IdProgram', 'program_name' => 'p.ProgramName', 'nama_program' => 'p.ArabicName', 'program_code' => 'p.ProgramCode'))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sr.IdProgramScheme', array())
            ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=ps.mode_of_program', array('ProgramMode' => 'DefinitionDesc'))
            ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=ps.mode_of_study', array('StudyMode' => 'DefinitionDesc'))
            ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=ps.program_type', array('ProgramType' => 'DefinitionDesc'))
            ->joinLeft(array("salute" => "tbl_definationms"), 'sp.appl_salutation=salute.idDefinition', array('Salutation' => 'DefinitionDesc'))
            ->joinLeft(array('city' => 'tbl_city'), 'city.idCity=sp.appl_city', array('CityName'))
            ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=sp.appl_state', array('StateName'))
            ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=sp.appl_country', array('CountryName'))
            ->where("sp.id  = ?", $sp_id);

        $row = $db->fetchRow($select);

        if ($row) {
            return $row;
        } else {
            return null;
        }
    }

    public function getSection()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_section"), array("value" => "a.*"))
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getStudentInfo($studentregistrationId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c' => 'registry_values'), 'b.appl_category = c.id', array('StudentCategory' => 'c.name', 'IdStudentCategory' => 'c.id'))
            ->joinLeft(array('d' => 'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e' => 'registry_values'), 'd.mode_of_program = e.id', array('mop' => 'e.name'))
            ->joinLeft(array('f' => 'registry_values'), 'd.mode_of_study = f.id', array('mos' => 'f.name'))
            ->joinLeft(array('g' => 'registry_values'), 'd.program_type = g.id', array('pt' => 'g.name', 'IdProgramType' => 'd.program_type'))
            ->joinLeft(array('h' => 'tbl_program'), 'd.IdProgram = h.IdProgram')
            ->joinLeft(array('j' => 'tbl_intake'), 'a.IdIntake = j.IdIntake', array('IntakeName' => 'j.IntakeDesc'))
            ->joinLeft(array('i' => 'registry_values'), 'b.appl_salutation = i.id', array('saluteName' => 'i.name'))
            ->joinLeft(array('k' => 'registry_values'), 'b.appl_idnumber_type = k.id', array('IdTypeName' => 'k.name'))
            ->joinLeft(array('l' => 'registry_values'), 'b.appl_marital_status = l.id', array('maritalStatusName' => 'l.name'))
            ->joinLeft(array('m' => 'registry_values'), 'b.appl_religion = m.id', array('religionName' => 'm.name'))
            ->joinLeft(array('n' => 'tbl_definationms'), 'n.idDefinition=a.profileStatus', array('statusName' => 'n.DefinitionDesc'))
            ->joinLeft(array('o' => 'tbl_intake'), 'a.IdIntake = o.IdIntake', array('intakeName' => 'o.IntakeDesc'))
            ->joinLeft(array('p' => 'registry_values'), 'p.id=b.appl_category', array('studentCategory' => 'p.name'))
            ->joinLeft(array('q' => 'tbl_countries'), 'b.appl_nationality = q.idCountry', array('nationalityName' => 'q.CountryName'))
            ->joinLeft(array('r' => 'fee_structure'), 'r.fs_id=a.fs_id', array('feeStructureName' => 'r.fs_name'))
            ->joinLeft(array('s' => 'tbl_sponsor_tag'), 's.StudentId = a.IdStudentRegistration', array())
            ->joinLeft(array('t' => 'tbl_sponsor'), 't.idsponsor = s.Sponsor', array("CONCAT_WS(' ',t.fName, t.lName) as sponsorship"))
            ->joinLeft(array('u' => 'tbl_scholarship_studenttag'), 'u.sa_cust_id = a.IdStudentRegistration', array())
            ->joinLeft(array('v' => 'tbl_scholarship_sch'), 'v.sch_Id = u.sa_scholarship_type', array('applScholarshipName' => 'v.sch_name'))
            ->joinLeft(array('w' => 'discount_type_tag'), 'w.IdStudentRegistration = a.IdStudentRegistration', array())
            ->joinLeft(array('x' => 'discount_type'), 'x.dt_id = w.dt_id', array('discountName' => 'x.dt_discount'))
            ->where('a.IdStudentRegistration = ?', $studentregistrationId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getSectionById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_section"), array("value" => "a.*"))
            ->where('id = ?', $id)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getModeOfStudy($programId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_scheme"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.mode_of_study = b.idDefinition', array('mosName' => 'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId)
            ->group("a.mode_of_study")
            ->order("a.IdProgramScheme");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getModeOfProgram($programId, $modeofstudy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_scheme"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mopName' => 'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.mode_of_study = ?', $modeofstudy)
            ->group("a.mode_of_program")
            ->order("a.IdProgramScheme");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getProgramType($programId, $modeofstudy, $modeofprogram)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_scheme"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.program_type = b.idDefinition', array('ptName' => 'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.mode_of_study = ?', $modeofstudy)
            ->where('a.mode_of_program = ?', $modeofprogram)
            ->group("a.program_type")
            ->order("a.IdProgramScheme");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getIntake()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_intake"), array("value" => "a.*"))
            ->order("a.IdIntake");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getEducationDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_qualification"), array("value" => "a.*"))
            ->joinLeft(array('qm' => 'tbl_qualificationmaster'), ('qm.IdQualification=a.ae_qualification'), array('QualificationLevel'))
            ->joinLeft(array('d' => 'tbl_definationms'), ('d.idDefinition=a.ae_class_degree'), array('class_degree' => 'DefinitionDesc'))
            ->joinLeft(array('i' => 'tbl_institution'), ('i.idInstitution=a.ae_institution'), array('InstitutionName'))
            ->joinLeft(array('c' => 'tbl_countries'), ('c.idCountry=a.ae_institution_country'), array('CountryName'))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getEnglishProficiencyDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_english_proficiency"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getQuantitativeProficiencyDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_quantitative_proficiency"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getResearchAndPublicationDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_research_publication"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getEmploymentDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_employment"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.ae_status = b.idDefinition', array('employmentStatusName' => 'b.DefinitionDesc'))
            ->joinLeft(array('c' => 'tbl_definationms'), 'a.ae_position = c.idDefinition', array('employmentPositionName' => 'c.DefinitionDesc'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'a.ae_industry = d.idDefinition', array('employmentIndustryName' => 'd.DefinitionDesc'))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getIndustryWorkingExperience($spId, $industry)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_working_experience"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId)
            ->where('a.aw_industry_id = ?', $industry);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getHealthCondition($spId, $status)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_health_condition"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId)
            ->where('a.ah_status = ?', $status);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getFinancialParticular($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_financial"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getVisaDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_visa"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getRefereesDetails($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_referees"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getRelationshipInformation($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_relationship_information"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_staffmaster'), 'a.ri_staff_id = b.IdStaff')
            ->joinLeft(array('c' => 'tbl_definationms'), 'b.IdLevel = c.idDefinition', array('staff_position' => 'c.DefinitionDesc'))
            ->where('a.ri_sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getStaffInformation($staffId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_staffmaster"), array("value" => "a.*"))
            //->joinLeft(array('b'=>'tbl_definationms'), 'a.IdLevel = b.idDefinition', array('staff_position'=>'b.DefinitionDesc'))
            ->where('a.IdStaff = ?', $staffId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getEmergencyContactInfo($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_emergency_contact"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.ec_type = b.idDefinition', array('ecTypeName' => 'b.DefinitionDesc'))
            ->joinLeft(array('c' => 'tbl_countries'), 'a.ec_country = c.idCountry')
            ->joinLeft(array('d' => 'tbl_state'), 'a.ec_state = d.idState')
            ->joinLeft(array('e' => 'tbl_city'), 'a.ec_state = e.idCity')
            ->where('a.ec_sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getCurricularActivities($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_curricular_activities"), array("value" => "a.*"))
            ->where('a.ca_sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getAttributeInfo($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_attribute"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.sa_type = b.idDefinition', array('saTypeName' => 'b.DefinitionDesc'))
            ->joinLeft(array('c' => 'tbl_definationms'), 'a.sa_category = c.idDefinition', array('saCategoryName' => 'c.DefinitionDesc'))
            ->where('a.sa_sp_id = ?', $spId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getCommentInfo($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_comment"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.scm_type = b.idDefinition', array('scmTypeName' => 'b.DefinitionDesc'))
            ->where('a.scm_sp_id = ?', $spId)
            ->where('a.scm_type = ?', 753);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getCommentInfoPrivate($spId, $auth)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_comment"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.scm_type = b.idDefinition', array('scmTypeName' => 'b.DefinitionDesc'))
            ->where('a.scm_sp_id = ?', $spId)
            ->where('a.scm_type = ?', 752)
            ->where('a.scm_user = ?', $auth);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function updateStudentProfile($bind, $spId, $table, $spIdName)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $update = $lobjDbAdpt->update($table, $bind, $spIdName . ' = ' . $spId);
        return $update;
    }

    public function insertStudentProfile($table, $bind)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $insert = $lobjDbAdpt->insert($table, $bind);
        $id = $lobjDbAdpt->lastInsertId();
        return $id;
    }

    public function deleteStudentProfile($table, $where)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $delete = $lobjDbAdpt->delete($table, $where);
        return $delete;
    }

    public function getState($countryId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_state"), array("value" => "a.*"))
            ->where('a.idCountry = ?', $countryId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getCity($stateId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_city"), array("value" => "a.*"))
            ->where('a.idState = ?', $stateId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getStaffPosition($staffId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_staffmaster"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.IdLevel = b.idDefinition', array('staff_position' => 'b.DefinitionDesc'))
            ->where('a.IdStaff = ?', $staffId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function insertUploadData($data)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $insert = $lobjDbAdpt->insert("student_documents", $data);
        return $insert;
    }

    public function getDclInfo($stdCtgy, $programSchemeId, $sectionId, $docType)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            ->where('a.dcl_stdCtgy = ?', $stdCtgy)
            ->where('a.dcl_programScheme = ?', $programSchemeId)
            ->where('a.dcl_sectionid = ?', $sectionId)
            ->where('a.dcl_uplType = ?', $docType);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getDocumentList($spId, $type, $sectionId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_documents"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $spId)
            ->where('a.ad_type = ?', $type)
            ->where('a.ad_section_id = ?', $sectionId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getDocumentList2($spId, $type, $sectionId, $tableId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_documents"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.ad_type=b.idDefinition', array('filename' => 'b.DefinitionDesc'))
            ->where('a.sp_id = ?', $spId)
            ->where('a.ad_type = ?', $type)
            ->where('a.ad_section_id = ?', $sectionId)
            ->where('a.ad_table_id = ?', $tableId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getWorkExperience($sp_id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_working_experience"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $sp_id);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getHealthData($sp_id, $ah_status)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_health_condition"), array("value" => "a.*"))
            ->where('a.sp_id = ?', $sp_id)
            ->where('a.ah_status = ?', $ah_status);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;

    }

    public function getStudentPhoto($spId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "student_documents"), array("value" => "a.*"))
            //->join(array('b'=>'tbl_documentchecklist_dcl'), 'a.ad_dcl_id = b.dcl_Id')
            ->where('a.sp_id = ?', $spId)
            ->where('a.ad_type = ?', 67)
            ->where('a.ad_section_id = ?', 2)
            ->order('a.ad_id DESC');
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getStudentInfoBySpid($sp_id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"), array("value" => "a.*"))
            ->join(array('b' => 'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c' => 'tbl_definationms'), 'b.appl_category = c.idDefinition', array('StudentCategory' => 'c.DefinitionDesc', 'IdStudentCategory' => 'c.idDefinition'))
            ->join(array('d' => 'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop' => 'e.DefinitionDesc'))
            ->join(array('f' => 'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos' => 'f.DefinitionDesc'))
            ->joinLeft(array('g' => 'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt' => 'g.DefinitionDesc', 'IdProgramType' => 'd.program_type'))
            ->join(array('h' => 'tbl_program'), 'd.IdProgram = h.IdProgram')
            ->join(array('j' => 'tbl_intake'), 'a.IdIntake = j.IdIntake', array('IntakeName' => 'j.IntakeDesc'))
            ->joinLeft(array('i' => 'tbl_definationms'), 'b.appl_salutation = i.idDefinition', array('saluteName' => 'i.DefinitionDesc'))
            ->joinLeft(array('k' => 'tbl_definationms'), 'b.appl_idnumber_type = k.idDefinition', array('IdTypeName' => 'k.DefinitionDesc'))
            ->joinLeft(array('l' => 'tbl_definationms'), 'b.appl_marital_status = l.idDefinition', array('maritalStatusName' => 'l.DefinitionDesc'))
            ->joinLeft(array('m' => 'tbl_definationms'), 'b.appl_religion = m.idDefinition', array('religionName' => 'm.DefinitionDesc'))
            ->joinLeft(array('n' => 'tbl_countries'), 'b.appl_nationality = n.idCountry', array('nationality' => 'n.CountryName'))
            ->where('b.id = ?', $sp_id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function studentPassportInfo($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'student_passport'), array('value' => '*'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.p_visa_type = b.idDefinition', array('visaType' => 'b.DefinitionDesc'))
            ->joinLeft(array('c' => 'tbl_countries'), 'a.p_country_issue = c.idCountry', array('countryName' => 'c.CountryName'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'a.p_type = d.idDefinition', array('passportType' => 'd.DefinitionDesc'))
            ->where('a.sp_id = ?', $id)
            ->order('a.p_active DESC')
            ->order('a.p_expiry_date DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function studentPassInfo($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'student_visa'), array('value' => '*'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.av_status = b.idDefinition', array('visaType' => 'b.DefinitionDesc'))
            ->joinLeft(array('c' => 'student_passport'), 'a.p_id = c.p_id')
            ->where('a.sp_id = ?', $id)
            ->order('a.av_active DESC')
            ->order('a.av_expiry DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function dependantPassportInfo($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'student_dependent_pass'), array('value' => '*'))
            ->joinLeft(array('b' => 'student_visa'), 'a.av_id = b.av_id')
            ->joinLeft(array('c' => 'student_passport'), 'a.p_id = c.p_id')
            ->joinLeft(array('d' => 'tbl_countries'), 'a.sdp_country_issue = d.idCountry', array('countryName' => 'd.CountryName'))
            ->joinLeft(array('e' => 'tbl_countries'), 'a.sdp_nationality = e.idCountry', array('nationalityName' => 'e.CountryName'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'a.sdp_relationship = f.idDefinition', array('relationshipName' => 'f.DefinitionDesc'))
            ->where('a.sp_id = ?', $id)
            ->order('a.sdp_active DESC')
            ->order('a.sdp_expiry_date DESC')
            ->group('a.sdp_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInstitution($countryid)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_institution'), array('value' => '*'))
            ->where('a.Country = ?', $countryid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getQualification()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_qualificationmaster'), array('value' => '*'))
            ->where('a.qualification_type_id = ?', 608)
            ->where('a.Active = ?', 1);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getRoomList($type = '263')
    { //default 263=>student
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('rt' => 'tbl_hostel_room_type'))
            ->join(array('rc' => 'tbl_room_charges'), 'rc.IdHostelRoom=rt.IdHostelRoomType', array('Rate'))
            ->joinLeft(array('df' => 'tbl_definationms'), 'rt.OccupancyType = df.idDefinition', array('occTypeName' => 'df.DefinitionDesc'))
            ->where("rc.CustomerType = ?", $type);
        $row = $db->fetchAll($select);

        return $row;
    }

    public function getAccomodationDetails($sp_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'student_accomodation'), array('value' => '*'))
            ->where('a.sp_id = ?', $sp_id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateAccomodationDetails($bind, $id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('student_accomodation', $bind, 'sp_id = ' . $id);
        return $update;
    }

    public function insertAccomodationDetails($bind)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('student_accomodation', $bind);
        $id = $db->lastInsertId('student_accomodation', 'acd_id');
        return $id;
    }

    public function getBranch()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_branchofficevenue'), array('value' => '*'))
            ->order('a.IdBranch');

        $return = $db->fetchAll($select);
        return $return;
    }

    public function updateStudent($bind, $id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = ' . $id);
        return $update;
    }

    public function getApplicantFeeStructure($program_id, $scheme, $intake_id, $student_category = 579, $throw = 1)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('fs' => 'fee_structure'))
            ->join(array('fsp' => 'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = ' . $program_id . ' and fsp.fsp_idProgramScheme = ' . $scheme)
            ->where("fs.fs_intake_start = '" . $intake_id . "'")
            ->where("fs.fs_student_category = '" . $student_category . "'");
        //echo $selectData; exit;
        $row = $db->fetchRow($selectData);

        if (!$row) {

            $ex_msg = "No Fee Structure setup for program(" . $program_id . "), scheme(" . $scheme . "), intake(" . $intake_id . "), category(" . $student_category . ")";

            if ($throw == 1) {
                throw new Exception($ex_msg);
            } else {
                return false;
            }

        } else {
            return $row;
        }
    }

    public function getDefinationDesc($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_definationms'), array('a.DefinitionDesc'))
            ->where('a.idDefinition = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    public function getCountriesById($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_countries'), array('a.CountryName'))
            ->where('a.idCountry = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    public function getCityById($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_city'), array('a.CityName'))
            ->where('a.idCity = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    public function getStateById($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_state'), array('a.StateName'))
            ->where('a.idState = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }


    public function getStudentData($IdStudentRegistration)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_studentregistration"), array("value" => "a.*"))
            ->join(array('b' => 'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c' => 'tbl_definationms'), 'b.appl_category = c.idDefinition', array('StudentCategory' => 'c.DefinitionDesc', 'IdStudentCategory' => 'c.idDefinition'))
            ->join(array('d' => 'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop' => 'e.DefinitionDesc'))
            ->join(array('f' => 'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos' => 'f.DefinitionDesc'))
            ->joinLeft(array('g' => 'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt' => 'g.DefinitionDesc', 'IdProgramType' => 'd.program_type'))
            ->join(array('h' => 'tbl_program'), 'd.IdProgram = h.IdProgram')
            ->joinLeft(array('o' => 'tbl_definationms'), 'h.Award = o.idDefinition', array('awardName' => 'o.DefinitionDesc'))
            ->join(array('j' => 'tbl_intake'), 'a.IdIntake = j.IdIntake', array('IntakeName' => 'j.IntakeDesc'))
            ->joinLeft(array('i' => 'tbl_definationms'), 'b.appl_salutation = i.idDefinition', array('saluteName' => 'i.DefinitionDesc'))
            ->joinLeft(array('k' => 'tbl_definationms'), 'b.appl_idnumber_type = k.idDefinition', array('IdTypeName' => 'k.DefinitionDesc'))
            ->joinLeft(array('l' => 'tbl_definationms'), 'b.appl_marital_status = l.idDefinition', array('maritalStatusName' => 'l.DefinitionDesc'))
            ->joinLeft(array('m' => 'tbl_definationms'), 'b.appl_religion = m.idDefinition', array('religionName' => 'm.DefinitionDesc'))
            ->joinLeft(array('n' => 'tbl_countries'), 'b.appl_nationality = n.idCountry', array('nationality' => 'n.CountryName'))
            ->joinLeft(array('tcp' => 'tbl_countries'), 'tcp.idCountry=b.appl_country', array('PCountryName' => 'CountryName'))
            ->joinLeft(array('tcc' => 'tbl_countries'), 'tcc.idCountry=b.appl_ccountry', array('CCountryName' => 'CountryName'))
            ->joinLeft(array('tsp' => 'tbl_state'), 'tsp.idState=b.appl_state', array('PStateName' => 'StateName'))
            ->joinLeft(array('tsc' => 'tbl_state'), 'tsc.idState=b.appl_cstate', array('CStateName' => 'StateName'))
            ->joinLeft(array('tctp' => 'tbl_city'), 'tctp.idCity=b.appl_city', array('PCityName' => 'CityName'))
            ->joinLeft(array('tctc' => 'tbl_city'), 'tctc.idCity=b.appl_ccity', array('CCityName' => 'CityName'))
            ->join(array('sm' => 'tbl_semestermaster'), 'sm.IdSemesterMaster=a.IdSemestersyllabus', array('SemesterMainStartDate'))
            ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function deleteEducationDetails($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('student_qualification', 'ae_id = ' . $id);
        return $delete;
    }
}