<?php
class Records_Model_DbTable_Studentstatus extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
	     
	public function fnGetApplicantNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("key"=>"sa.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
		 				 ->where("sa.Active = 1")
		 				 ->where("sa.Registered = 1")
		 				 ->where("sa.Termination = 0")
		 				 ->where("sa.Accepted = 1")
		 				 ->order("sa.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
     
	public function fngetStudentprofileDtls() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=sa.IDCourse',array("p.*"))
       								 ->where("sa.Active = 1")
		 							 ->where("sa.Registered = 1")
		 							 ->where("sa.Termination = 0")
		 							 ->where("sa.Accepted = 1")
       								->order("sa.FName");
       					//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
	
	public function fnSearchStudentApplication($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=sa.IDCourse',array("p.*"));
       								
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("sa.IdApplication = ?",$post['field5']);
										
			}
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("sa.IDCourse = ?",$post['field8']);
										
			}				
       			$lstrSelect	->where('sa.ICNumber like "%" ? "%"',$post['field2'])
				       			 ->where("sa.Active = 1")
						 		 ->where("sa.Registered = 1")
						 		 ->where("sa.Termination = 0")
						 		 ->where("sa.Accepted = 1")
				       			->order("sa.FName");
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	
	public function fnViewStudentAppnDetails($lintIdApplication){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
		 				 ->join(array("c"=>"tbl_countries"),'c.idCountry = sa.PermCountry',array("c.CountryName AS CountryName"))
		 				 ->join(array("p"=>"tbl_program"),'sa.IDCourse = p.IdProgram')
		 				 ->join(array("cm"=>"tbl_collegemaster"),'sa.IdCollege  = cm.IdCollege')
		 				 ->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram')
		 				 ->join(array("se"=>"tbl_semester"),'l.IdStartSemester  = se.IdSemester')
		 				 ->join(array("sm"=>"tbl_semestermaster"),'se.Semester = sm.IdSemesterMaster')
		 				 ->where('sa.IdApplication = ?',$lintIdApplication)
		 				 ->where('l.Active = 123');
	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	
public function fnSaveDetails($larrformData){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
			$lstrTable = "tbl_studentdetain";	
			$larrformData['Active']	 = 1;
			return $lobjDbAdpt->insert($lstrTable,$larrformData);
		}
	
	public function fnViewStudentStatusDetails($lintIdApplication){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
		 				 ->join(array("sm"=>"tbl_studentdetain"),'sa.IdApplication= sm.IdApplication AND sm.Active =1')
		 				 ->where('sa.IdApplication = ?',$lintIdApplication);	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
		
	}
	
	public function fnUpdateDetails($larrformData,$editCheck){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
		$lstrTable = "tbl_studentdetain";
		$lstrWhere = "idStudentDetain = ".$editCheck;
		$lstrMsg = $lobjDbAdpt->update($lstrTable,$larrformData,$lstrWhere);
		return $lstrMsg;
	}
	public function fnUpdateRegistrationDetails($idApplication,$idSemester){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
		$lstrTable = "tbl_studentregistration";
		$larrformData['Semesterstatus'] = 2;
		$lstrWhere = "IdApplication = ".$idApplication." AND IdSemester = ".$idSemester ;
		$lstrMsg = $lobjDbAdpt->update($lstrTable,$larrformData,$lstrWhere);
		return $lstrMsg;	
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	



  
 

	
    public function fnViewAcademicLandscapeDtls($lintIdApplication){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
		 				 ->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
		 				 //->join(array("ls"=>"tbl_landscapesubject"),'l.IdLandscape = ls.IdLandscape')
		 				// ->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject')
		 				// ->join(array("dms"=>"tbl_definationms"),'ls.SubjectType = dms.idDefinition')
		 				 ->where('sa.IdApplication = ?',$lintIdApplication)
		 				 ->where('l.Active = 123');
		 				 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 				 
		return $larrResult;
	}
	
	
	public function fnViewAcademicLandscapeBlockSubjectDtls($lintIdApplication){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
		 				 ->join(array("sr"=>"tbl_studentregistration"),'sa.IdApplication = sr.IdApplication')	
		 				 ->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
		 				 ->join(array("lbs"=>"tbl_landscapeblocksemester"),'l.IdLandscape = lbs.IdLandscape AND lbs.semesterid = sr.IdSemester')
		 				 ->join(array("ls"=>"tbl_landscapeblocksubject"),'l.IdLandscape = ls.IdLandscape AND lbs.blockid = ls.blockid')
		 				 ->join(array("sm"=>"tbl_subjectmaster"),'ls.subjectid = sm.IdSubject')		 				
		 				 ->where('sa.IdApplication = ?',$lintIdApplication)
		 				 ->where('l.Active = 123');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 				 
		return $larrResult;
	}
	
	public function fnViewAcademicLandscapeSubjectDtls($lintIdApplication){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
		 				 ->join(array("l"=>"tbl_landscape"),'sa.IDCourse = l.IdProgram',array("l.LandscapeType"))
		 				 ->join(array("ls"=>"tbl_landscapesubject"),'l.IdLandscape = ls.IdLandscape')
		 				 ->join(array("sm"=>"tbl_subjectmaster"),'ls.IdSubject = sm.IdSubject')
		 				 ->join(array("dms"=>"tbl_definationms"),'ls.SubjectType = dms.idDefinition')
		 				 ->where('sa.IdApplication = ?',$lintIdApplication)
		 				 ->where('l.Active = 123');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 				 
		return $larrResult;
	}
	
	
	
    
    public function fnupdateStudentAppn($lintiIdApplication, $larrformData ) { //Function for updating the user
    	
    $larrformData['HomePhone'] = $larrformData['HomePhonecountrycode']."-".$larrformData['HomePhonestatecode']."-".$larrformData['HomePhone'];
		unset($larrformData['HomePhonecountrycode']);
		unset($larrformData['HomePhonestatecode']);
	$larrformData['CellPhone'] = $larrformData['CellPhonecountrycode']."-".$larrformData['CellPhonestatecode']."-".$larrformData['CellPhone'];
		unset($larrformData['CellPhonecountrycode']);
		unset($larrformData['CellPhonestatecode']);
	$larrformData['Fax'] = $larrformData['Faxcountrycode']."-".$larrformData['Faxstatecode']."-".$larrformData['Fax'];
		unset($larrformData['Faxcountrycode']);
		unset($larrformData['Faxstatecode']);			
			
	$where = 'IdApplication = '.$lintiIdApplication;
	$this->update($larrformData,$where);
    }
	
}