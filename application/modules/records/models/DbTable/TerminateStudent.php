<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_TerminateStudent extends Zend_Db_Table_Abstract {
    
    public function studentList($formData = null, $id=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))        		
            ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('appl_fname','appl_mname','appl_lname','appl_religion'))
            ->joinLeft(array('deftn' =>'tbl_definationms'), 'deftn.idDefinition=sa.profileStatus', array('deftn.DefinitionCode','Status','DefinitionDesc')) //Application status
            ->joinLeft(array('defination' => 'tbl_definationms'), 'defination.idDefinition=sa.profileStatus', array('profileStatusName'=>'DefinitionCode')) //Application STtsu
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('ArabicName','ProgramName','ProgramCode', 'IdScheme'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sa.IdIntake', array('intk.IntakeDesc','IntakeDefaultLanguage'))
            ->joinLeft(array('s'=>'tbl_staffmaster'),'s.IdStaff = sa.AcademicAdvisor',array('advisor'=>'Fullname'))
            ->joinLeft(array('lk'=>'sis_setup_detl'),"lk.ssd_id=p.appl_religion",array("religion"=>"ssd_name"))
            ->joinLeft(array('defination2' => 'tbl_definationms'), 'defination2.idDefinition=p.appl_category', array('stdCtgy'=>'DefinitionDesc'))
            ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sa.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
            ->where('sa.profileStatus != ?', 96)
            ->where('sa.profileStatus != ?', 248)
            ->where('sa.profileStatus != ?', 249)
            ->order("sa.registrationId DESC");
        
        if ($formData != null){
            if (isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $select->where('sa.IdProgram = ?', $formData['IdProgram']);
            }
            if (isset($formData['IdProgramScheme']) && $formData['IdProgramScheme']!=''){
                $select->where('sa.IdProgramScheme = ?', $formData['IdProgramScheme']);
            }
            if (isset($formData['IdIntake']) && $formData['IdIntake']!=''){
                $select->where('sa.IdIntake = ?', $formData['IdIntake']);
            }
            if (isset($formData['IdBranch']) && $formData['IdBranch']!=''){
                $select->where('sa.IdBranch = ?', $formData['IdBranch']);
            }
            if (isset($formData['applicant_name']) && $formData['applicant_name']!=''){
                $select->where('concat(p.appl_fname, " ", p.appl_lname) like "%'.$formData['applicant_name'].'%"');
            }
            if (isset($formData['student_id']) && $formData['student_id']!=''){
                $select->where('sa.registrationId like "%'.$formData['student_id'].'%"');
            }
            if (isset($formData['profile_status']) && $formData['profile_status']!=''){
                $select->where('sa.profileStatus = ?', $formData['profile_status']);
            }
        }
        
        if ($id != null){
            $select->where('sa.IdStudentRegistration = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }else{
            $result = $db->fetchAll($select);
            return $result;
        }
    }

    public function studentList2($formData = null, $id=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))
            ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('appl_fname','appl_mname','appl_lname','appl_religion'))
            ->joinLeft(array('deftn' =>'tbl_definationms'), 'deftn.idDefinition=sa.profileStatus', array('deftn.DefinitionCode','Status','DefinitionDesc')) //Application status
            ->joinLeft(array('defination' => 'tbl_definationms'), 'defination.idDefinition=sa.profileStatus', array('profileStatusName'=>'DefinitionCode')) //Application STtsu
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('ArabicName','ProgramName','ProgramCode', 'IdScheme'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sa.IdIntake', array('intk.IntakeDesc','IntakeDefaultLanguage'))
            ->joinLeft(array('s'=>'tbl_staffmaster'),'s.IdStaff = sa.AcademicAdvisor',array('advisor'=>'Fullname'))
            ->joinLeft(array('lk'=>'sis_setup_detl'),"lk.ssd_id=p.appl_religion",array("religion"=>"ssd_name"))
            ->joinLeft(array('defination2' => 'tbl_definationms'), 'defination2.idDefinition=p.appl_category', array('stdCtgy'=>'DefinitionDesc'))
            ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sa.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
            ->order("sa.registrationId DESC");

        if ($formData != null){
            if (isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $select->where('sa.IdProgram = ?', $formData['IdProgram']);
            }
            if (isset($formData['IdProgramScheme']) && $formData['IdProgramScheme']!=''){
                $select->where('sa.IdProgramScheme = ?', $formData['IdProgramScheme']);
            }
            if (isset($formData['IdIntake']) && $formData['IdIntake']!=''){
                $select->where('sa.IdIntake = ?', $formData['IdIntake']);
            }
            if (isset($formData['IdBranch']) && $formData['IdBranch']!=''){
                $select->where('sa.IdBranch = ?', $formData['IdBranch']);
            }
            if (isset($formData['applicant_name']) && $formData['applicant_name']!=''){
                $select->where('concat(p.appl_fname, " ", p.appl_lname) like "%'.$formData['applicant_name'].'%"');
            }
            if (isset($formData['student_id']) && $formData['student_id']!=''){
                $select->where('sa.registrationId like "%'.$formData['student_id'].'%"');
            }
            if (isset($formData['profile_status']) && $formData['profile_status']!=''){
                $select->where('sa.profileStatus = ?', $formData['profile_status']);
            }
        }

        if ($id != null){
            $select->where('sa.IdStudentRegistration = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }else{
            $result = $db->fetchAll($select);
            return $result;
        }
    }
    
    public function getSemesterStatus($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.IdSemesterMain = b.IdSemesterMaster')
            ->join(array('c'=>'tbl_definationms'), 'a.studentsemesterstatus = c.idDefinition', array('semStatus'=>'c.DefinitionDesc'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->order('SemesterMainStartDate DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function semesterStatusOption($status){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_profilesemesterstatus_tagging'), array('value'=>'*'))
            ->join(array('b'=>'tbl_definationms'), 'a.pst_semesterstatus=b.idDefinition')
            ->where('a.pst_profilestatus = ?', $status);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateProfileStatus($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = '.$id);
        return $update;
    }
    
    public function getCurrentSemester($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('"'.date('Y-m-d').'" BETWEEN a.SemesterMainStartDate AND a.SemesterMainEndDate');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function checkDuplicateActive($regId, $status){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->where('a.registrationId = ?', $regId)
            ->where('a.profileStatus = ?', $status);
        
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getKmc($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_kmc'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.kmc_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getClearanceFinance($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_finance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.dcf_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getIct($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_ict'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.ict_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getLogistic($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_logistic'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.log_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertKmc($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('department_clearance_kmc', $data);
        $id = $db->lastInsertId('department_clearance_kmc', 'kmc_id');
        return $id;
    }

    public function updateKmc($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('department_clearance_kmc', $data, 'IdStudentRegistration = '.$id);
        return $update;
    }

    public function insertFinance($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('department_clearance_finance', $data);
        $id = $db->lastInsertId('department_clearance_finance', 'dcf_id');
        return $id;
    }

    public function updateFinance($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('department_clearance_finance', $data, 'IdStudentRegistration = '.$id);
        return $update;
    }

    public function insertIct($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('department_clearance_ict', $data);
        $id = $db->lastInsertId('department_clearance_ict', 'ict_id');
        return $id;
    }

    public function updateIct($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('department_clearance_ict', $data, 'IdStudentRegistration = '.$id);
        return $update;
    }

    public function insertLogistic($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('department_clearance_logistic', $data);
        $id = $db->lastInsertId('department_clearance_logistic', 'log_id');
        return $id;
    }

    public function updateLogistic($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('department_clearance_logistic', $data, 'IdStudentRegistration = '.$id);
        return $update;
    }
}
?>