<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_Report extends Zend_Db_Table_Abstract {
    
    public function getIntake(){
        $case = 'ORDER BY CASE a.sem_seq WHEN "JAN" THEN 1'
                .' WHEN "FEB" THEN 2 WHEN "MAR" THEN 3'
                .' WHEN "APR" THEN 4 WHEN "MAY" THEN 5'
                .' WHEN "JUN" THEN 6 WHEN "JUL" THEN 7'
                .' WHEN "AUG" THEN 8 WHEN "SEP" THEN 9'
                .' WHEN "OCT" THEN 10 WHEN "NOV" THEN 11'
                .' WHEN "DEC" THEN 12 ELSE 13 END';
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->order('a.sem_year DESC');
            //->order($case);
        
        //$select .= ' '.$case;
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('a.seq_no')
            ->order('a.ProgramName');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDefinationList($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id)
            ->order('a.defOrder');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramScheme($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentRecords($filter=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*', 'spId'=>'a.sp_id'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id=b.id', array('*', 'religionother'=>'appl_religion_others'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'b.appl_salutation=c.idDefinition', array('salutation'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'b.appl_idnumber_type=d.idDefinition', array('idnumbertype'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.appl_marital_status=e.idDefinition', array('maritalstatus'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'student_financial'), 'a.sp_id=f.sp_id')
            ->joinLeft(array('g'=>'tbl_definationms'), 'f.af_method=g.idDefinition', array('financemethod'=>'g.DefinitionDesc'))
            //->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = a.IdStudentRegistration', array())
            //->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
            ->joinLeft(array('h'=>'tbl_definationms'), 'f.af_type_scholarship=h.idDefinition', array('shoclarshiptype'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_scholarship_sch'), 'f.af_scholarship_apply=i.sch_Id')
            ->joinLeft(array('j'=>'tbl_program'), 'a.IdProgram=j.IdProgram', array('programname'=>'concat(j.ProgramName, " (", j.ProgramCode, ")")', 'idscheme'=>'j.IdScheme'))
            ->joinLeft(array('k'=>'tbl_program_scheme'), 'a.IdProgramScheme=k.IdProgramScheme')
            ->joinLeft(array('l'=>'tbl_definationms'), 'k.mode_of_program = l.idDefinition', array('mop'=>'l.DefinitionDesc'))
            ->joinLeft(array('m'=>'tbl_definationms'), 'k.mode_of_study = m.idDefinition', array('mos'=>'m.DefinitionDesc'))
            ->joinLeft(array('n'=>'tbl_definationms'), 'k.program_type = n.idDefinition', array('pt'=>'n.DefinitionDesc'))
            ->joinLeft(array('o'=>'tbl_intake'), 'a.IdIntake=o.IdIntake', array('intakename'=>'o.IntakeDesc'))
            ->joinLeft(array('p'=>'tbl_definationms'), 'b.appl_race=p.idDefinition', array('race'=>'p.DefinitionDesc'))
            ->joinLeft(array('q'=>'tbl_countries'), 'b.appl_nationality=q.idCountry', array('nationality'=>'q.CountryName'))
            ->joinLeft(array('r'=>'tbl_city'), 'b.appl_city=r.idCity', array('permanentcity'=>'r.CityName'))
            ->joinLeft(array('s'=>'tbl_state'), 'b.appl_state=s.idState', array('permanentstate'=>'s.StateName'))
            ->joinLeft(array('t'=>'tbl_countries'), 'b.appl_country=t.idCountry', array('permanentcountry'=>'t.CountryName'))
            ->joinLeft(array('u'=>'tbl_city'), 'b.appl_ccity=u.idCity', array('correspondencecity'=>'u.CityName'))
            ->joinLeft(array('v'=>'tbl_state'), 'b.appl_cstate=v.idState', array('correspondencestate'=>'v.StateName'))
            ->joinLeft(array('w'=>'tbl_countries'), 'b.appl_ccountry=w.idCountry', array('correspondencecountry'=>'w.CountryName'))
            ->joinLeft(array('x'=>'tbl_definationms'), 'b.appl_religion=x.idDefinition', array('religion'=>'x.DefinitionDesc'))
            ->joinLeft(array('y'=>'fee_structure'), 'a.fs_id=y.fs_id', array('baseplan'=>'y.fs_name'))
            ->joinLeft(array('z'=>'tbl_semestermaster'), 'a.IdSemester=z.IdSemesterMaster', array('semestermastername'=>'concat(z.SemesterMainName, " - ", z.SemesterMainCode)'))
            ->joinLeft(array('aa'=>'tbl_definationms'), 'a.profileStatus=aa.idDefinition', array('status'=>'aa.DefinitionDesc'))
            ->joinLeft(array('ab'=>'applicant_transaction'), 'a.transaction_id=ab.at_trans_id', array('applieddate'=>'ab.at_create_date'))
            ->joinLeft(array('ac'=>'tbl_branchofficevenue'), 'a.IdBranch=ac.IdBranch', array('branchname'=>'ac.BranchName'))
            ->joinLeft(array('ad'=>'tbl_definationms'), 'b.appl_bumiputera=ad.idDefinition', array('bumiputera'=>'ad.DefinitionDesc'))
            ->joinLeft(array('ae'=>'tbl_programmajoring'), 'a.IdProgramMajoring=ae.IDProgramMajoring', array('programmajoring'=>'ae.EnglishDescription'))
            ->joinLeft(array('af'=>'tbl_definationms'), 'b.appl_category=af.idDefinition', array('type_of_nationality'=>'af.DefinitionDesc'))
            ->where('a.registrationId != ?', '1409999')
            ->group('a.IdStudentRegistration');
        
        if ($filter != 0){
            if (isset($filter['intake']) && $filter['intake']!=''){
                $select->where('a.IdIntake IN (?)', $filter['intake']);
            }
            if (isset($filter['program']) && $filter['program']!=''){
                $select->where('a.IdProgram = ?', $filter['program']);
            }
            if (isset($filter['programscheme']) && $filter['programscheme']!=''){
                $select->where('a.IdProgramScheme = ?', $filter['programscheme']);
            }
            if (isset($filter['studentcategory']) && $filter['studentcategory']!=''){
                $select->where('b.appl_category = ?', $filter['studentcategory']);
            }
            if (isset($filter['status']) && $filter['status']!=''){
                $select->where('a.profileStatus = ?', $filter['status']);
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getQualification($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_qualification'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_qualificationmaster'), 'a.ae_qualification=b.IdQualification', array('b.QualificationLevel'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.ae_class_degree=c.idDefinition', array('classdegree'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_institution'), 'a.ae_institution=d.idInstitution', array('institutionname'=>'d.InstitutionName'))
            ->where('a.sp_id = ?', $id)
            ->order('b.QualificationRank DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getEnglishProfeciency($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_english_proficiency'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_qualificationmaster'), 'a.ep_test=b.IdQualification', array('testname'=>'b.QualificationLevel'))
            ->where('a.sp_id = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getWorkingIndustry($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_working_experience'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.aw_industry_id=b.idDefinition', array('industry'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.aw_years=c.idDefinition', array('years'=>'c.DefinitionDesc'))
            ->where('a.sp_id = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getEmployment($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_employment'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ae_status = b.idDefinition', array('employmentstatusname'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.ae_position = c.idDefinition', array('positionname'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.ae_industry = d.idDefinition', array('industryname'=>'d.DefinitionDesc'))
            ->where('a.sp_id = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getVisa($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_visa'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.av_status = b.idDefinition', array('visastatusname'=>'b.DefinitionDesc'))
            ->where('a.sp_id = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getHealthCondition($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_health_condition'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ah_status = b.idDefinition', array('healthstatusname'=>'b.DefinitionDesc'))
            ->where('a.sp_id = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCurrentSem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $id)
            ->where('a.SemesterMainStartDate <= ?', date('Y-m-d'))
            ->where('a.SemesterMainEndDate >= ?', date('Y-m-d'));
        //echo $select; exit;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemesterStatus($studId,$semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.studentsemesterstatus=b.IdDefinition', array('status'=>'DefinitionDesc'))
            ->where('a.IdStudentRegistration = ?', $studId)
            ->where('a.IdSemesterMain = ?', $semId)
            ->order('a.idstudentsemsterstatus DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getLastSemesterStatus($studId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.studentsemesterstatus=b.IdDefinition', array('status'=>'DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemesterMain=c.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $studId)
            ->order('c.SemesterMainStartDate DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function countSemesterStatus($scheme, $date1, $date2, $semType=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.SemesterMainStartDate BETWEEN "'.$date1.'" AND "'.$date2.'"')
            //->where('a.SemesterMainEndDate >= ?', $endDate)
            ->where('a.IsCountable = ?', 1)
            ->where('a.SemesterFunctionType IS NULL');
        
        if ($semType!=0){
            $select->where('a.SemesterType = ?', $semType);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getFirstSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('b.SemesterMainStartDate'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'b.IdSemesterMaster = a.IdSemesterMain')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('b.IsCountable = ?', 1)
            ->order('b.SemesterMainStartDate ASC')
            ->group('a.IdSemesterMain');
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    public function getLastSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('b.SemesterMainEndDate'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'b.IdSemesterMaster = a.IdSemesterMain')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('b.IsCountable = ?', 1)
            ->order('b.SemesterMainEndDate DESC')
            ->group('a.IdSemesterMain');
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    public function getActiveSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'b.IdSemesterMaster = a.IdSemesterMain')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('b.IsCountable = ?', 1)
            ->order('b.SemesterMainEndDate ASC')
            ->group('a.IdSemesterMain');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getIntakeId($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('a.IdIntake'))
            ->where('a.IdStudentRegistration = ?', $id);
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    public function getIntakeDetail($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->where('a.IdIntake = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemesterStart($year, $seq, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $seq)
            ->where('a.IdScheme = ?', $scheme);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCurrentYearSemester($id, $scheme, $arr){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'b.IdSemesterMaster = a.IdSemesterMain')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('b.IsCountable = ?', 1)
            ->where('b.IdSemesterMaster IN (?)', $arr)
            ->where('b.IdScheme = ?', $scheme)
            ->order('b.SemesterMainEndDate ASC')
            ->group('a.IdSemesterMain');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkSemStatusBasedOnSubjectReg($id, $semesterId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.IdSemesterMain = ?', $semesterId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getOneYearSemList($date, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.SemesterMainStartDate >= ?', $date)
            ->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $scheme)
            ->order('a.SemesterMainStartDate ASC')
            ->limit(3);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCountry(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_countries'), array('value'=>'*'))
            ->order('a.CountryName ASC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getReligion(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', 23)
            ->where('a.Status = ?', 1);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getVisaReport($formData=false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id=b.id')
            //->joinLeft(array('c'=>'student_passport'), 'a.sp_id=c.sp_id')
            ->joinLeft(array('d'=>'tbl_countries'), 'b.appl_nationality=d.idCountry', array('nationName'=>'d.CountryName'))
            ->joinLeft(array('e'=>'tbl_intake'), 'a.IdIntake=e.IdIntake', array('intakeName'=>'e.IntakeDesc'))
            ->joinLeft(array('f'=>'tbl_program'), 'a.IdProgram=f.IdProgram', array('programName'=>'f.ProgramName'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'b.appl_religion=g.idDefinition', array('religionName'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'a.profileStatus=h.idDefinition', array('statusName'=>'h.DefinitionDesc'))
            ->where('b.appl_category = ?', 580);
        
        if ($formData != false){
            if (isset($formData['intake']) && $formData['intake']!=''){
                $select->where('a.IdIntake in (?)', $formData['intake']);
            }
            if (isset($formData['program']) && $formData['program']!=''){
                $select->where('a.IdProgram = ?', $formData['program']);
            }
            if (isset($formData['programscheme']) && $formData['programscheme']!=''){
                $select->where('a.IdProgramScheme = ?', $formData['programscheme']);
            }
            if (isset($formData['programscheme']) && $formData['programscheme']!=''){
                $select->where('a.IdProgramScheme = ?', $formData['programscheme']);
            }
            if (isset($formData['nationality']) && $formData['nationality']!=''){
                $select->where('b.appl_nationality = ?', $formData['nationality']);
            }
            if (isset($formData['status']) && $formData['status']!=''){
                $select->where('a.profileStatus = ?', $formData['status']);
            }
            if (isset($formData['gender']) && $formData['gender']!=''){
                $select->where('b.appl_gender = ?', $formData['gender']);
            }
            if (isset($formData['religion']) && $formData['religion']!=''){
                $select->where('b.appl_religion = ?', $formData['religion']);
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCorrectionData(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'semesterstatuscorrection'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.studentid=b.registrationId AND a.program=b.IdProgram')
            ->join(array('c'=>'tbl_program'), 'b.IdProgram=c.IdProgram');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateStatusBesar($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = '.$id);
        return $update;
    }
    
    public function checkStatusKecil($id, $semid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.idSemester = ?', $semid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateStatusKecil($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentsemesterstatus', $bind, 'idstudentsemsterstatus = '.$id);
        return $update;
    }
    
    public function insertStatusKecil($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentsemesterstatus', $bind);
        $id = $db->lastInsertId('tbl_studentsemesterstatus', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function getStudentPassport($spid){
    	$db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('p' => 'student_passport'))      		  							 
            ->where('p.sp_id = ?', $spid)
            ->where('p_active = ?', 1);	  
        $student_list = $db->fetchRow($select);

        return $student_list;
    }
    
    public function getStudentPass($spid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('v' => 'student_visa'))      		  							 
            ->where('v.sp_id = ?', $spid)
            ->where('v.av_active = ?', 1);	  
        $student_list = $db->fetchRow($select);

        return $student_list;
    }
    
    public function getDependentPass($spid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('sdp' => 'student_dependent_pass'))
            ->joinLeft(array('a'=>'tbl_definationms'), 'sdp.sdp_relationship=a.idDefinition', array('relation'=>'a.DefinitionDesc'))
            ->where('sdp.sp_id = ?', $spid)
            ->where('sdp.sdp_active = ?', 1);	  
        $student_list = $db->fetchAll($select);

        return $student_list;
    }

    public function getSemesterRegistered($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.studentsemesterstatus = ?', 130);

        $result = $db->fetchAll($select);
        return $result;
    }

    static function getConvocation($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_list'))
            ->joinLeft(array('b'=>'convocation'), 'a.c_id = b.c_id')
            ->where('a.idStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
}
?>