<?php

class Records_Model_DbTable_Credittransfersubject extends Zend_Db_Table { //Model Class for Credit transfer subjects

    protected $_name = 'tbl_credittransfersubjects';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnaddcreditSubjects($data) {
        $lstrTable = "tbl_credittransfersubjects";
        $this->lobjDbAdpt->insert($lstrTable, $data);
    }

    public function fngetCreditTranferApplication() {
        $select = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_credittransfer'), array('a.IdCreditTransfer', 'a.IdStudentRegistration','a.IdCTApplication'))
                        ->joinLeft(array('c' => 'tbl_studentregistration'), 'a.IdStudentRegistration = c.IdStudentRegistration')
                        ->join(array('b' => 'student_profile'), 'c.sp_id = b.id')
                        ->joinLeft(array('d' => 'tbl_semester'), 'c.idSemester = d.IdSemester', array("d.SemesterCode AS semestername"))
                        ->joinLeft(array('e' => 'tbl_semestermaster'), 'c.IdSemesterMain = e.IdSemesterMaster', array("e.SemesterMainCode AS mainsemestername"))
                        ->joinLeft(array('f' => 'tbl_studentapplication'), 'a.IdStudentRegistration = f.IdStudentRegistration', array("f.IdApplicant"))
                        ->joinLeft(array('g' => 'tbl_definationms'), 'a.ApplicationStatus = g.idDefinition', array("g.DefinitionDesc"))
                        ->where("c.profileStatus IN (92,248,253)");

        $result = $this->lobjDbAdpt->fetchAll($select);
        return $result;
    }

    public function fngetSearchTranferApplication($post) {
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_credittransfer'), array('a.IdCreditTransfer', 'a.IdStudentRegistration','a.IdCTApplication'))
                        //->joinLeft(array('b' => 'tbl_credittransfersubjects'), 'a.IdCreditTransfer = b.IdCreditTransfer', array("b.idSemester", "b.ApplicationStatus"))
                        ->joinLeft(array('c' => 'tbl_studentregistration'), 'a.IdStudentRegistration = c.IdStudentRegistration', array("c.FName", "c.LName", 'c.registrationId'))
                        ->joinLeft(array('d' => 'tbl_semester'), 'c.idSemester = d.IdSemester', array("d.SemesterCode AS semestername"))
                        ->joinLeft(array('e' => 'tbl_semestermaster'), 'c.IdSemesterMain = e.IdSemesterMaster', array("e.SemesterMainCode AS mainsemestername"))
                        ->joinLeft(array('f' => 'tbl_studentapplication'), 'a.IdStudentRegistration = f.IdStudentRegistration', array("f.IdApplicant"))
                        ->joinLeft(array('g' => 'tbl_definationms'), 'a.ApplicationStatus = g.idDefinition', array("g.DefinitionDesc"))
                        ->where("c.profileStatus IN (92,248,253)");


        if (isset($post['field3']) && !empty($post['field3'])) {
            $lstrSelect = $lstrSelect->where('c.registrationId like "%" ? "%"', $post['field3']);
        }
        if (isset($post['field4']) && !empty($post['field4'])) {
            $lstrSelect = $lstrSelect->where('a.IdCTApplication like "%" ? "%"', $post['field4']);
        }

        if (isset($post['field2']) && !empty($post['field2'])) {
            $wh_condition = "c.FName like '%".$post['field2']."%' OR  c.MName like '%".$post['field2']."%' OR  c.LName like '%".$post['field2']."%' ";
            $lstrSelect = $lstrSelect->where($wh_condition);
        }
        
       
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function getAlldetailByAppId($IdcreditTransferApp){
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_credittransfer'), array('a.IdCreditTransfer', 'a.IdStudentRegistration'))
                        ->joinLeft(array('b' => 'tbl_credittransfersubjects'), 'a.IdCreditTransfer = b.IdCreditTransfer', array("b.*"))
                        ->joinLeft(array('c' => 'tbl_studentregistration'), 'a.IdStudentRegistration = c.IdStudentRegistration', array("c.FName", "c.LName", 'c.registrationId', 'c.IdProgram'))
                        ->joinLeft(array('d' => 'tbl_semester'), 'b.idSemester = d.IdSemester', array("d.SemesterCode AS semestername"))
                        ->joinLeft(array('e' => 'tbl_semestermaster'), 'b.IdSemesterMain = e.IdSemesterMaster', array("e.SemesterMainCode AS mainsemestername"))
                        ->joinLeft(array('f' => 'tbl_studentapplication'), 'a.IdStudentRegistration = f.IdStudentRegistration', array("f.IdApplicant"))
                        ->joinLeft(array('g' => 'tbl_definationms'), 'b.ApplicationStatus = g.idDefinition', array("g.DefinitionDesc"))
                        ->joinLeft(array('h' => 'tbl_subjectmaster'), 'b.IdCourse = h.IdSubject', array("h.SubjectName as course"))
                        ->joinLeft(array('i' => 'tbl_institution'), 'b.IdInstitution = i.idInstitution', array("i.InstitutionName"))
                        ->joinLeft(array('j' => 'tbl_qualificationmaster'), 'b.IdQualification = j.IdQualification', array("j.QualificationLevel"))
                        ->joinLeft(array('k' => 'tbl_specialization'), 'b.IdSpecialization = k.IdSpecialization', array("k.Specialization"))
                        ->joinLeft(array('l' => 'tbl_subjectmaster'), 'b.EquivalentCourse = l.IdSubject', array("l.SubjectName AS equivalentcourse"))
                        ->joinLeft(array('m' => 'tbl_subjectgradepoint'), 'b.EquivalentGrade = m.Idsubjectgradepoint', array("m.subjectgrade"))
                        ->joinLeft(array('n' => 'tbl_user'), 'b.AppliedBy = n.iduser', array("n.loginName"))
                        ->joinLeft(array('o' => 'tbl_program'), 'c.IdProgram = o.IdProgram', array("o.IdScheme"))
                        ->where("a.IdCreditTransfer =?",$IdcreditTransferApp);        
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }



    public function delcredittranferApp($IdcreditTransferApp){
        $where = $this->lobjDbAdpt->quoteInto('IdCreditTransfer = ?', $IdcreditTransferApp);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }
    
    public function checkCoursesReg($getSemCourse,$studentId) { 
        
        $wh = " IdStudentRegistration = '".$studentId."' AND IdSubject = '".$getSemCourse[0]['IdCourse']."' ";        
        if($getSemCourse[0]['IdSemester']!='') { $wh .= "AND IdSemesterDetails = '".$getSemCourse[0]['IdSemester']."' ";  }
        if($getSemCourse[0]['IdSemesterMain']!='') { $wh .= "AND IdSemesterMain = '".$getSemCourse[0]['IdSemesterMain']."' ";  }
        
        $lstrSelect = $this->lobjDbAdpt->select()
                            ->from(array('a' => 'tbl_studentregsubjects'), array('COUNT(a.IdStudentRegSubjects) as totalCourses')) 
                            ->where($wh);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
        
    }
    
     public function getCreditSubjectDetalByID($condition_cs) {
            $sql = $this->lobjDbAdpt->select()
                   ->from(array('a' => 'tbl_credittransfersubjects'),array('a.*'))                   
                   ->where($condition_cs);
            $result = $this->lobjDbAdpt->fetchAll($sql);
            return $result;
        }


    public function checkcourseregistered($Idcourse,$studentId){
        $sql = $this->lobjDbAdpt->select()
                   ->from(array('a' => 'tbl_studentregsubjects'),array('a.*'))
                   ->where("a.IdStudentRegistration =?",$studentId)
                   ->where("a.IdSubject =?",$Idcourse);
            $result = $this->lobjDbAdpt->fetchRow($sql);
            return $result;
    }

    public function updatecourseregistered($data,$id){
        $lstrTable = "tbl_studentregsubjects";
        $where = "IdStudentRegSubjects =".$id;
        $this->lobjDbAdpt->update($lstrTable, $data, $where);
    }

    public function checkvalidcourse($studentId,$Idcourse){
        $sql = $this->lobjDbAdpt->select()
                   ->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer'))
                   ->joinLeft(array('b' => 'tbl_credittransfersubjects'), 'a.IdCreditTransfer = b.IdCreditTransfer',array())
                   ->joinLeft(array('c' => 'tbl_studentregsubjects'), 'b.IdCourse <> c.IdSubject',array())
                   ->where("a.IdStudentRegistration =?",$studentId)
                   ->where("c.IdGrade = 'CT'")
                   ->where("a.ApplicationStatus IN (193)")
                   ->where("b.IdCourse =?",$Idcourse);        
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
    }

    public function checkvalidcourseinedit($studentId,$Idcourse,$IdCreditTransfer){
        $sql = $this->lobjDbAdpt->select()
                   ->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer'))
                   ->joinLeft(array('b' => 'tbl_credittransfersubjects'), 'a.IdCreditTransfer = b.IdCreditTransfer',array())
                   ->joinLeft(array('c' => 'tbl_studentregsubjects'), 'b.IdCourse <> c.IdSubject',array())
                   ->where("a.ApplicationStatus IN (193)")
                   ->where("a.IdStudentRegistration =?",$studentId)
                   ->where("c.IdGrade = 'CT'")
                   ->where("b.IdCourse =?",$Idcourse)
                   ->where("a.IdCreditTransfer !=?",$IdCreditTransfer);
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
    }

}
