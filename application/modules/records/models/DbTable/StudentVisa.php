<?php

class Records_Model_DbTable_StudentVisa extends Zend_Db_Table_Abstract {

    protected $_name = 'student_visa';
    protected $_primary = 'av_id';
   	   
	public function getDataById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('v' => $this->_name))               
                ->where('v.av_id = ?',(int)$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
    } 

   	public function getStudentPassByPassportId($pid) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('v' => $this->_name))               
                ->where('v.p_id = ?',(int)$pid)
                ->order('v.av_active desc')
                ->order('v.av_expiry desc');
             
        $result = $db->fetchAll($sql);        
        return $result;
    }  

    public function getListDataBySpId($spid) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('v' => $this->_name)) 
                ->where('v.sp_id = ?',(int)$spid);
             
        $result = $db->fetchAll($sql);        
        return $result;
    }  
	   
	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
		return $id =  $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($where){
		 $this->delete( $where );
	}
	
	public function getLastExpiryDate($spid) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        //get last visa expiry date
        $sql = $db->select()
                ->from(array('v' => $this->_name))  
                ->where('sp_id=?',$spid)
                ->where('av_expiry')
                ->order('av_expiry DESC');
             
        $result = $db->fetchRow($sql); 
        
        if($result)
        	return $result;
        else
        	return null;
    } 
    
	public function getActiveVisa($spid) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('v' => $this->_name)) 
                ->where('v.sp_id = ?',$spid)              
                ->where('v.av_active = 1');
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    
    public function deleteStudentPassport($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('student_passport', 'p_id = '.$id);
        return $delete;
    }
	
    public function deleteStudentPass($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('student_visa', 'av_id = '.$id);
        return $delete;
    }
    
    public function deleteStudentDependentPass($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('student_dependent_pass', 'sdp_id = '.$id);
        return $delete;
    }
}

?>