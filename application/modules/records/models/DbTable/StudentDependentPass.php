<?php

class Records_Model_DbTable_StudentDependentPass extends Zend_Db_Table_Abstract {

    protected $_name = 'student_dependent_pass';
    protected $_primary = 'sdp_id';
   
	
    
	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
		return $id =  $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($where){
		 $this->delete( $where );
	}
	
	public function getDataById($id){
		$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sdp' => $this->_name))     
                 ->where('sdp.sdp_id = ?',(int)$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
	}
	
	public function getListDataByAvId($avid){
		$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sdp' => $this->_name))     
                ->join(array('co' => 'tbl_countries'),'co.idCountry = sdp.sdp_country_issue', array('CountryName'))
                ->join(array("ds" => "tbl_definationms"),'ds.idDefinition=sdp.sdp_relationship', array('relationship'=>'DefinitionDesc','hubungan'=>'BahasaIndonesia'))
                ->join(array("c" => "tbl_countries"),'c.idCountry=sdp.sdp_nationality', array('nationality'=>'CountryName'))
                ->where('sdp.av_id = ?',(int)$avid)
                ->order('sdp.sdp_active desc')
                ->order('sdp.sdp_expiry_date desc');
             
        $result = $db->fetchAll($sql);        
        return $result;
	}
	
	public function checkDuplicate($refno) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sdp' => $this->_name))               
                ->where('sdp.sdp_passport_no = ?',$refno);
             
        $result = $db->fetchRow($sql);        
        return $result;
    } 
	
	public function getActivePass($spid) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('s' => $this->_name))     
                  ->where('s.sp_id = ?',$spid)               
                ->where('s.sdp_active = 1');
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    
    
	public function updateStatus($data,$where){
		 $this->update($data, $where);
	}
}

?>