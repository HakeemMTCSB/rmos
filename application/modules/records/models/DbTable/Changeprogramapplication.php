<?php
class Records_Model_DbTable_Changeprogramapplication extends Zend_Db_Table {
    protected $_name = 'tbl_change_program_application';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function getAllchangeprogramApplication(){
        $select = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_change_program_application'), array('a.IdChangeProgramApplication','a.IdCPApplication', 'a.IdStudent','a.ApplicationStatus'))
                        ->joinLeft(array('b' => 'tbl_studentregistration'), 'a.IdStudent = b.IdStudentRegistration', array("b.FName", "b.LName", "b.MName",'b.registrationId'))
                        ->joinLeft(array('c' => 'tbl_definationms'), 'a.ApplicationStatus = c.idDefinition', array('c.DefinitionDesc'))
        				->order('a.IdCPApplication');
        $result = $this->lobjDbAdpt->fetchAll($select);
        return $result;
    }
    
	public function getAllActivechangeprogramApplication(){
        $select = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_change_program_application'), array('a.IdChangeProgramApplication','a.IdCPApplication', 'a.IdStudent','a.ApplicationStatus'))
                        ->joinLeft(array('b' => 'tbl_studentregistration'), 'a.IdStudent = b.IdStudentRegistration', array("b.FName", "b.LName", "b.MName",'b.registrationId'))
                        ->joinLeft(array('c' => 'tbl_definationms'), 'a.ApplicationStatus = c.idDefinition', array('c.DefinitionDesc'));
                        //->where('a.ApplicationStatus = ?',236);
        $result = $this->lobjDbAdpt->fetchAll($select);
        return $result;
    }

    public function fngetSearchchangeprogramApplication($post){       
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_change_program_application'), array('a.IdChangeProgramApplication','a.IdCPApplication', 'a.IdStudent','a.ApplicationStatus'))
                        ->joinLeft(array('b' => 'tbl_studentregistration'), 'a.IdStudent = b.IdStudentRegistration', array("b.FName", "b.LName", "b.MName",'b.registrationId'))
                        ->joinLeft(array('c' => 'tbl_definationms'), 'a.ApplicationStatus = c.idDefinition', array('c.DefinitionDesc'))
						->order('a.IdCPApplication');
        if (isset($post['field2']) && !empty($post['field2'])) {
            $wh_condition = "b.FName like '%".$post['field2']."%' OR  b.MName like '%".$post['field2']."%' OR  b.LName like '%".$post['field2']."%' ";
            $lstrSelect = $lstrSelect->where($wh_condition);
        }
        
        if (isset($post['field3']) && !empty($post['field3'])) {
            $lstrSelect = $lstrSelect->where('b.registrationId like "%" ? "%"', $post['field3']);
        }

        if (isset($post['field4']) && !empty($post['field4'])) {
            $lstrSelect = $lstrSelect->where('b.ExtraIdField1 like "%" ? "%"', $post['field4']);
        }

        if (isset($post['field1']) && !empty($post['field1'])) {
            $lstrSelect = $lstrSelect->where('a.CurrentProgram =?', $post['field1']);
        }

        if (isset($post['field6']) && !empty($post['field6'])) {
            $lstrSelect = $lstrSelect->where('a.IdCPApplication like "%" ? "%"', $post['field6']);
        }

        if (isset($post['field5']) && !empty($post['field5'])) {
            $lstrSelect = $lstrSelect->where('b.profileStatus =?', $post['field5']);
        }

        if (isset($post['field8']) && !empty($post['field8'])) {           
            $lstrSelect = $lstrSelect->where('a.ApplicationStatus =?', $post['field8']);
        }        
        
        $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $result;
        
    }
    
	public function fngetSearchActivechangeprogramApplication($post){       
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array('a' => 'tbl_change_program_application'), array('a.IdChangeProgramApplication','a.IdCPApplication', 'a.IdStudent','a.ApplicationStatus'))
                        ->joinLeft(array('b' => 'tbl_studentregistration'), 'a.IdStudent = b.IdStudentRegistration', array("b.FName", "b.LName", "b.MName",'b.registrationId'))
                        ->joinLeft(array('c' => 'tbl_definationms'), 'a.ApplicationStatus = c.idDefinition', array('c.DefinitionDesc'));
                        //->where('a.ApplicationStatus = ?',236);

        if (isset($post['field2']) && !empty($post['field2'])) {
            $wh_condition = "b.FName like '%".$post['field2']."%' OR  b.MName like '%".$post['field2']."%' OR  b.LName like '%".$post['field2']."%' ";
            $lstrSelect = $lstrSelect->where($wh_condition);
        }
        
        if (isset($post['field3']) && !empty($post['field3'])) {
            $lstrSelect = $lstrSelect->where('b.registrationId like "%" ? "%"', $post['field3']);
        }

        if (isset($post['field4']) && !empty($post['field4'])) {
            $lstrSelect = $lstrSelect->where('b.ExtraIdField1 like "%" ? "%"', $post['field4']);
        }

        if (isset($post['field1']) && !empty($post['field1'])) {
            $lstrSelect = $lstrSelect->where('a.CurrentProgram =?', $post['field1']);
        }

        if (isset($post['field6']) && !empty($post['field6'])) {
            $lstrSelect = $lstrSelect->where('a.IdCPApplication like "%" ? "%"', $post['field6']);
        }

        if (isset($post['field5']) && !empty($post['field5'])) {
            $lstrSelect = $lstrSelect->where('b.profileStatus =?', $post['field5']);
        }

        if (isset($post['field8']) && !empty($post['field8'])) {           
            $lstrSelect = $lstrSelect->where('a.ApplicationStatus =?', $post['field8']);
        }        
        
        $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $result;
        
    }
}

?>
