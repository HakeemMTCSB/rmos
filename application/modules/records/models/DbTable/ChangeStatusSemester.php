<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/2/2016
 * Time: 2:57 PM
 */
class Records_Model_DbTable_ChangeStatusSemester extends Zend_Db_Table {

    public function getCurrentSemester($idscheme){
        $date = date('Y-m-d');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idscheme)
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgramInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSemById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdSemesterMaster = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSemesterBetween($id, $idscheme, $date1, $date2){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdSemesterMaster != ?', $id)
            ->where('a.IdScheme = ?', $idscheme)
            ->where('a.IsCountable = ?', 1)
            ->where('a.SemesterMainStartDate <= ?', $date1)
            ->where('a.SemesterMainStartDate >= ?', $date2);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function deleteSemesterStatus($studentid, $semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentsemesterstatus', 'IdStudentRegistration = '.$studentid.' AND IdSemesterMain = '.$semId);
        return $delete;
    }
}