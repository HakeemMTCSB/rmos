<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_Visitingstudent extends Zend_Db_Table_Abstract {

    public function insertStudentProfile($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('student_profile', $bind);
        $id = $db->lastInsertId('student_profile', 'id');
        return $id;
    }
    
    public function insertStudentRegistration($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregistration', $bind);
        $id = $db->lastInsertId('tbl_studentregistration', 'IdStudentRegistration');
        return $id;
    }
    
    public function getProgramScheme($programId, $mop, $mos, $pt){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.mode_of_program = ?', $mop)
            ->where('a.mode_of_study = ?', $mos)
            ->where('a.program_type = ?', $pt);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getTheStudentRegistrationDetail($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('a.registrationId', 'a.IdProgram','at_pes_id'=>'a.registrationId','transaction_id','IdApplication','*'))
            ->joinLeft(array('StudentProfile' => 'student_profile'), "a.sp_id = StudentProfile.id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name",'StudentProfile.*' ))
            ->joinLeft(array('b' => 'tbl_program'), "a.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme"))
            ->joinLeft(array('c' => 'tbl_intake'), "a.IdIntake = c.IdIntake", array("c.IntakeDesc"))
            ->joinLeft(array('d' => 'tbl_definationms'), "a.profileStatus = d.idDefinition", array("d.DefinitionDesc"))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=a.IdProgramScheme', array())
            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("fs" => "fee_structure"),'fs.fs_id=a.fs_id', array('fs_name'))
            ->joinLeft(array('o'=>'student_financial'), 'a.IdStudentRegistration = o.sp_id')
            ->joinLeft(array('u'=>'tbl_definationms'), 'o.af_method = u.idDefinition', array('afMethodName'=>'u.DefinitionDesc'))
            ->joinLeft(array('v'=>'tbl_definationms'), 'o.af_type_scholarship = v.idDefinition', array('typeScholarshipName'=>'v.DefinitionDesc'))
            ->joinLeft(array('w'=>'tbl_scholarship_sch'), 'o.af_scholarship_apply = w.sch_Id', array('applScholarshipName'=>'w.sch_name'))
            ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=StudentProfile.appl_category', array('StudentCategory'=>'DefinitionDesc'))
            ->joinLeft(array("st" => "tbl_definationms"),'st.idDefinition=a.profileStatus', array('StatusName'=>'DefinitionDesc'))
            ->where("a.IdStudentRegistration =?", $id);
        
        $result = $db->fetchRow($sql);
        return $result;
    }
    
    public function updateStudent($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = '.$id);
        return $update;
    }
    
    public function getProgram($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getIntake($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->where('a.IdIntake = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemester($year, $month, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month)
            ->where('a.IdScheme = ?', $scheme);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getApplicantFeeStructure($program_id, $scheme, $intake_id, $student_category=579,$throw=1){
		
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('fs'=>'fee_structure'))
            ->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
            ->where("fs.fs_intake_start = '".$intake_id."'")
            ->where("fs.fs_student_category = '".$student_category."'");
        //echo $selectData; exit;
        $row = $db->fetchRow($selectData);

        if(!$row){

            $ex_msg = "No Fee Structure setup for program(".$program_id."), scheme(".$scheme."), intake(".$intake_id."), category(".$student_category.")";

            if ($throw==1){
                throw new Exception($ex_msg);
            }else{
                return false;
            }

        }else{
            return $row;
        }
    }
    
    public function getMos($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_study=b.idDefinition', array('mos'=>'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId)
            ->group('a.mode_of_study');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getMop($programId, $mos){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program=b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.mode_of_study = ?', $mos)
            ->group('a.mode_of_program');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getPt($programId, $mos, $mop){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.program_type=b.idDefinition', array('pt'=>'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.mode_of_study = ?', $mos)
            ->where('a.mode_of_program = ?', $mop)
            ->group('a.program_type');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getIntakeDetail($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->where('a.IdIntake = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateStudentProfile($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('student_profile', $bind, 'id = '.$id);
        return $update;
    }
    
    public function getCurrentSemester(){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date)
            ->where('a.IdScheme = ?', 12);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getFeeId($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = "SELECT fspi_fee_id as fi_id FROM `fee_structure_program_item` a
                    join fee_item b on b.fi_id = a.fspi_fee_id
                    join tbl_fee_category c on c.fc_id = b.fi_fc_id
                    WHERE a.fspi_fs_id = ".$id." and c.fc_code = 'AF'
                    group by fspi_fee_id;";
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id)
            ->order('a.defOrder ASC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertCommpose($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('comm_compose', $bind);
        $id = $db->lastInsertId('comm_compose', 'comp_id');
        return $id;
    }
    
    public function insertComposeRec($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('comm_compose_recipients', $bind);
        $id = $db->lastInsertId('comm_compose_recipients', 'cr_id');
        return $id;
    }

    public function insertComposeAttach($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('comm_compose_attachment', $bind);
        $id = $db->lastInsertId('comm_compose_attachment', 'ca_id');
        return $id;
    }
    
    public function getEmailHistory($id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>'comm_compose'))
            ->joinLeft(array('b'=>'comm_compose_recipients'), 'b.cr_comp_id = a.comp_id')
            ->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('created_by_name'=>'Fullname'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'd.idDefinition=a.comp_type', array('comp_type_name' => 'd.DefinitionDesc'))
            //->where('a.comp_module = ?', 'records')
            ->where('a.comp_rectype = ?', 'students')
            ->where('a.comp_type = ?', 585)
            ->where('b.cr_rec_id = ?', $id)
            ->order('a.created_date DESC');
            
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getEmailContent($id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('b'=>'comm_compose_recipients'), array('value'=>'*'))
            ->where('b.cr_id = ?', $id);
            
        $result = $db->fetchRow($select);
        return $result;
    }
}
?>