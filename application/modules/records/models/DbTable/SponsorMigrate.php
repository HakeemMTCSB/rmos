<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_Model_DbTable_SponsorMigrate extends Zend_Db_Table_Abstract {
    
    public function getMigrateData(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsormigrate'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.studentid=b.registrationId AND a.programid=b.IdProgram')
            ->joinLeft(array('c'=>'tbl_program'), 'b.IdProgram=c.IdProgram');
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCurSem($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('"'.date('Y-m-d').'" BETWEEN a.SemesterMainStartDate AND a.SemesterMainEndDate');
        //echo $select;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertSponsor($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_application', $bind);
        $id = $db->lastInsertId('sponsor_application', 'sa_id');
        return $id;
    }
    
    public function insertSponsorDetails($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_application_details', $bind);
        $id = $db->lastInsertId('sponsor_application_details', 'id_detail');
        return $id;
    }
}
?>