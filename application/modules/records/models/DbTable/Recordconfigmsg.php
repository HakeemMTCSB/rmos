<?php

class Records_Model_DbTable_Recordconfigmsg extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_record_config_messages';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnaddrecordconfiguationmsg($data) {
        $this->insert($data);
    }

    public function fnfetchAllmessage($IdRecordConfig) {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_config_messages"), array("a.*"))
                        ->joinLeft(array("b" => "tbl_definationtypems"),"a.RecordModule = defTypeDesc",array("b.idDefType"))
                        ->joinLeft(array("c" => "tbl_definationms"),"(b.idDefType = c.idDefType AND c.idDefinition = a.RecordModuleStatus)",array("c.DefinitionDesc"))
                        ->where("a.IdRecordConfig = ?", $IdRecordConfig);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnDeleteRecMesg($id){
        $where = $this->lobjDbAdpt->quoteInto('IdRecordConfig = ?', $id);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }

}

?>
