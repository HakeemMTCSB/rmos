<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 26/10/2015
 * Time: 4:05 PM
 */
class Records_Model_DbTable_IdCollection extends Zend_Db_Table_Abstract {

    public function getIdCollection($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_idcard_courier'), array('value'=>'*'))
            ->joinLeft(array('d'=>'tbl_studentregistration'), 'a.idc_studentid = d.IdStudentRegistration')
            ->joinLeft(array('e'=>'student_profile'), 'd.sp_id = e.id')
            ->joinLeft(array('f'=>'tbl_program'), 'd.IdProgram = f.IdProgram')
            ->joinLeft(array('g'=>'tbl_program_scheme'), 'd.IdProgramScheme = g.IdProgramScheme')
            ->joinLeft(array('h'=>'tbl_definationms'), 'g.mode_of_program = h.idDefinition', array('mop'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'g.mode_of_study = i.idDefinition', array('mos'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'g.program_type = j.idDefinition', array('pt'=>'j.DefinitionDesc'))
            ->joinLeft(array('l'=>'tbl_countries'), 'a.idc_country = l.idCountry', array('countryname'=>'l.CountryName'))
            ->joinLeft(array('m'=>'tbl_state'), 'a.idc_state = m.idState', array('statename'=>'m.StateName'))
            ->joinLeft(array('n'=>'tbl_city'), 'a.idc_city = n.idCity', array('cityname'=>'n.CityName'));

        if ($search != false){
            if (isset($search['Program']) && $search['Program'] != ''){
                $select->where('d.IdProgram = ?', $search['Program']);
            }
            if (isset($search['Programscheme']) && $search['ProgramScheme'] != ''){
                $select->where('d.IdProgramScheme IN (?)', $search['ProgramScheme']);
            }
            if (isset($search['Intake']) && $search['Intake'] != ''){
                $select->where('d.IdIntake IN (?)', $search['Intake']);
            }
            if (isset($search['Method']) && $search['Method'] != ''){
                $select->where('a.idc_method IN (?)', $search['Method']);
            }
            if (isset($search['Status']) && $search['Status'] != ''){
                $select->where('a.idc_status IN (?)', $search['Status']);
            }
            if (isset($search['Name']) && $search['Name'] != ''){
                $select->where('CONCAT_WS(" ",e.appl_fname, e.appl_lname) = "%'.$search['Name'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateIdCardCollection($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_idcard_courier', $data, 'idc_id = '.$id);
        return $update;
    }
}