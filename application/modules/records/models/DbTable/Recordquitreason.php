<?php
class Records_Model_DbTable_Recordquitreason extends Zend_Db_Table_Abstract {
    protected $_name = 'tbl_record_reason_quit';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnfetchquitReasons($idUniversity){
           $lstrSelect = $this->lobjDbAdpt->select()
                            ->from(array("a" => "tbl_record_reason_quit"), array("a.*"))
                            ->where("a.IdUniversity =?",$idUniversity);
           $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
           return $larrResult;
    }

    public function fnaddquitreasons($data) {
        $this->insert($data);
    }

    public function fnfetchAllReasons($Iduniversity) {
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_reason_quit"), array("a.*"))
                        ->where("a.IdUniversity = ?", $Iduniversity);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fndeletequitReasons($id){
        $where = $this->lobjDbAdpt->quoteInto('IdUniversity = ?', $id);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }

    public function getQuitReasons($Iduniversity=1){
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_reason_quit"), array('key' => 'a.IdRecordResonQuit','name' => 'a.Reason'))
                        ->where("a.IdUniversity = ?", $Iduniversity);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getQuitDescription($id){
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_reason_quit"), array('a.Reason'))
                        ->where("a.IdRecordResonQuit = ?", $id);
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;

    }
    
    public function getReason($id){
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a'=>'tbl_definationms'), array('key'=>'a.idDefinition', 'name'=>'a.DefinitionDesc'))
            ->where('a.idDefType = ?', $id);
        
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
    
    public function getDefination($id){
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a'=>'tbl_definationms'), array('key'=>'a.idDefinition', 'name'=>'a.DefinitionDesc'))
            ->where('a.idDefinition = ?', $id);
        
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }
}
?>
