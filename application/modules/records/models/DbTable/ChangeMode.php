<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_ChangeMode extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_advisement';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }
    
    public function findStudent($searchElement, $searchType = 1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->join(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.profileStatus = c.idDefinition', array('StatusName'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_intake'), 'a.IdIntake = d.IdIntake', array('IntakeName'=>'IntakeDesc'))
            ->joinLeft(array('e'=>'tbl_branchofficevenue'), 'a.IdBranch = e.IdBranch', array('BranchName'=>'BranchName'))
            ->joinLeft(array('f'=>'tbl_program'), 'a.IdProgram = f.IdProgram', array('ProgramName'=>'f.ProgramName', 'schemeid'=>'f.IdScheme'))
            ->joinLeft(array('g'=>'tbl_program_scheme'), 'a.IdProgramScheme = g.IdProgramScheme')
            ->joinLeft(array('h'=>'tbl_definationms'), 'g.mode_of_program = h.idDefinition', array('mop'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'g.mode_of_study = i.idDefinition', array('mos'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'g.program_type = j.idDefinition', array('pt'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_city'), 'b.appl_city = k.idCity', array('CityName'=>'CityName'))
            ->joinLeft(array('l'=>'tbl_state'), 'b.appl_state = l.idState', array('StateName'=>'StateName'))
            ->joinLeft(array('m'=>'tbl_countries'), 'b.appl_country = m.idCountry', array('CountryName'=>'CountryName'))
            ->joinLeft(array('n'=>'tbl_definationms'), 'b.appl_idnumber_type = n.idDefinition', array('IdTypeName'=>'n.DefinitionDesc'))
            ->where('a.profileStatus = ?', 92);
            //->where('a.IdStudentRegistration NOT IN (SELECT tbl_auditpaperapplication.apa_studregid FROM tbl_auditpaperapplication WHERE apa_status = 823)');

        if ($searchType == 1){
            $select->where('a.registrationId like "%'.$searchElement.'%"');
        }else{
            $select->where('b.appl_fname like "%'.$searchElement.'%" OR b.appl_lname like "%'.$searchElement.'%"');
        }

        $select->order('a.registrationId');
        //echo $searchElement;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getUser($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_user'), array('value'=>'*'))
            ->where('iduser = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getDefination2($deftypeid, $code){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationtypems'), array('value'=>'*'))
            ->join(array('b'=>'tbl_definationms'), 'a.idDefType = b.idDefType')
            ->where('a.idDefType = ?', $deftypeid)
            ->where('b.DefinitionCode = ?', $code);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getNewMop($programid, $programType){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mopName'=>'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programid)
            ->where('a.program_type = ?', $programType)
            ->group('a.mode_of_program');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getNewMos($programid, $programType, $mop){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_study = b.idDefinition', array('mosName'=>'b.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programid)
            ->where('a.program_type = ?', $programType)
            ->where('a.mode_of_program = ?', $mop)
            ->group('a.mode_of_study');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertChangeModeApplication($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_changemodeapplication', $bind);
        $id = $db->lastInsertId();
        return $id;
    }
    
    public function insertChangeModeUpload($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_changemodeupload', $bind);
        $id = $db->lastInsertId();
        return $id;
    }
    
    public function getChangeModeApplication($cmaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_changemodeapplication'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.cma_studregid = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_definationms'), 'b.profileStatus = d.idDefinition', array('StatusName'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_intake'), 'b.IdIntake = e.IdIntake', array('IntakeName'=>'IntakeDesc'))
            ->joinLeft(array('f'=>'tbl_branchofficevenue'), 'b.IdBranch = f.IdBranch', array('BranchName'=>'BranchName'))
            ->joinLeft(array('g'=>'tbl_program'), 'b.IdProgram = g.IdProgram', array('ProgramName'=>'ProgramName'))
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'b.IdProgramScheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->joinLeft(array('l'=>'tbl_city'), 'c.appl_city = l.idCity', array('CityName'=>'CityName'))
            ->joinLeft(array('m'=>'tbl_state'), 'c.appl_state = m.idState', array('StateName'=>'StateName'))
            ->joinLeft(array('n'=>'tbl_countries'), 'c.appl_country = n.idCountry', array('CountryName'=>'CountryName'))
            ->joinLeft(array('o'=>'tbl_definationms'), 'a.cma_status = o.idDefinition', array('AppStatusName'=>'o.DefinitionDesc'))
            ->joinLeft(array('p'=>'tbl_semestermaster'), 'a.cma_semesterid = p.IdSemesterMaster', array('SemesterName'=>'p.SemesterMainName'))
            ->joinLeft(array('q'=>'tbl_user'), 'a.cma_upduser = q.iduser', array('UserName'=>'q.loginName'))
            //->joinLeft(array('r'=>'tbl_definationms'), 'a.applicationType = r.idDefinition', array('ApplicationTypeName'=>'r.DefinitionDesc'))
            ->joinLeft(array('s'=>'tbl_definationms'), 'c.appl_idnumber_type = s.idDefinition', array('IdTypeName'=>'s.DefinitionDesc'))
            ->joinLeft(array('t'=>'tbl_definationms'), 'a.cma_currentmop = t.idDefinition', array('currentMopName'=>'t.DefinitionDesc'))
            ->joinLeft(array('u'=>'tbl_definationms'), 'a.cma_currentmos = u.idDefinition', array('currentMosName'=>'u.DefinitionDesc'))
            ->joinLeft(array('v'=>'tbl_user'), 'a.cma_appliedby = v.iduser', array('appliedName'=>'v.loginName'))
            ->joinLeft(array('w'=>'tbl_user'), 'a.cma_approvedby = w.iduser', array('approvedName'=>'w.loginName'))
            ->joinLeft(array('x'=>'tbl_definationms'), 'a.cma_mop = x.idDefinition', array('changeMopName'=>'x.DefinitionDesc'))
            ->joinLeft(array('y'=>'tbl_definationms'), 'a.cma_mos = y.idDefinition', array('changeMosName'=>'y.DefinitionDesc'))
            ->where('a.cma_id = ?', $cmaId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getFileUpload($cmaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_changemodeupload'), array('value'=>'*'))
            ->where('a.cmu_cmaid = ?', $cmaId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateChangeModeApplication($bind, $cmaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_changemodeapplication', $bind, 'cma_id = '.$cmaId);
        return $update;
    }
    
    public function deleteUploadFile($cmuId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_changemodeupload', 'cmu_id = '.$cmuId);
        return $delete;
    }
    
    public function getDelUplFileInfo($cmuId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_changemodeupload'), array('value'=>'*'))
            ->where('a.cmu_id = ?', $cmuId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getChangeModeAppList($bind=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_changemodeapplication'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.cma_studregid = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_definationms'), 'b.Status = d.idDefinition', array('StatusName'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_intake'), 'b.IdIntake = e.IdIntake', array('IntakeName'=>'IntakeDesc'))
            ->joinLeft(array('f'=>'tbl_branchofficevenue'), 'b.IdBranch = f.IdBranch', array('BranchName'=>'BranchName'))
            ->joinLeft(array('g'=>'tbl_program'), 'b.IdProgram = g.IdProgram', array('ProgramName'=>'ProgramName'))
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'b.IdProgramScheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->joinLeft(array('l'=>'tbl_city'), 'c.appl_city = l.idCity', array('CityName'=>'CityName'))
            ->joinLeft(array('m'=>'tbl_state'), 'c.appl_state = m.idState', array('StateName'=>'StateName'))
            ->joinLeft(array('n'=>'tbl_countries'), 'c.appl_country = n.idCountry', array('CountryName'=>'CountryName'))
            ->joinLeft(array('o'=>'tbl_definationms'), 'a.cma_status = o.idDefinition', array('AppStatusName'=>'o.DefinitionDesc'))
            ->joinLeft(array('p'=>'tbl_semestermaster'), 'a.cma_semesterid = p.IdSemesterMaster', array('SemesterName'=>'p.SemesterMainName'))
            ->joinLeft(array('q'=>'tbl_user'), 'a.cma_upduser = q.iduser', array('UserName'=>'q.loginName'))
            //->joinLeft(array('r'=>'tbl_definationms'), 'a.applicationType = r.idDefinition', array('ApplicationTypeName'=>'r.DefinitionDesc'))
            ->joinLeft(array('s'=>'tbl_definationms'), 'c.appl_idnumber_type = s.idDefinition', array('IdTypeName'=>'s.DefinitionDesc'))
            ->joinLeft(array('t'=>'tbl_definationms'), 'a.cma_currentmop = t.idDefinition', array('currentMopName'=>'t.DefinitionDesc'))
            ->joinLeft(array('u'=>'tbl_definationms'), 'a.cma_currentmos = u.idDefinition', array('currentMosName'=>'u.DefinitionDesc'));
        
        if ($bind!=null){
            if (isset($bind['Program']) && $bind['Program'] != ''){
                $select->where('b.IdProgram = ?', $bind['Program']);
            }
            if (isset($bind['ProgramScheme']) && $bind['ProgramScheme'] != ''){
                $select->where('b.IdProgramScheme = ?', $bind['ProgramScheme']);
            }
            if (isset($bind['Semester']) && $bind['Semester'] != ''){
                $select->where('a.cma_semesterid = ?', $bind['Semester']);
            }
            if (isset($bind['IdNo']) && $bind['IdNo'] != ''){
                $select->where('b.registrationId like "%'.$bind['IdNo'].'%"');
            }
            if (isset($bind['ApplicationID']) && $bind['ApplicationID'] != ''){
                $select->where('b.cma_appid like "%'.$bind['ApplicationID'].'%"');
            }
            if (isset($bind['AppStatus']) && $bind['AppStatus'] != ''){
                $select->where('a.cma_status = ?', $bind['AppStatus']);
            }
            if (isset($bind['StudentCategory']) && $bind['StudentCategory']!=''){
                $select->where('c.appl_category = ?', $bind['StudentCategory']);
            }
            if (isset($bind['StudentId']) && $bind['StudentId']!=''){
                $select->where('b.registrationId like "%'.$bind['StudentId'].'%"');
            }
            if (isset($bind['StudentName']) && $bind['StudentName']!=''){
                $select->where('CONCAT_WS(" ", c.appl_fname, c.appl_lname) like "%'.$bind['StudentName'].'%"');
            }
            if (isset($bind['DateFrom']) && $bind['DateFrom'] != '' && isset($bind['DateTo']) && $bind['DateTo'] != ''){
                $select->where('a.cma_applieddate BETWEEN "'.date('Y-m-d', strtotime($bind['DateFrom'])).'" AND "'.date('Y-m-d', strtotime($bind['DateTo'])).'"');
            }
        }
        
        $select->order('a.cma_applieddate DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.Active = ?', 1)
            ->order('a.seq_no')
            ->order('a.ProgramName');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemester(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'));

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDefination($deftypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramSchemeBasedOnProgram($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop_name'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos_name'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('programType'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSequence($type, $year){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sequence_id'))
            ->where('a.tsi_type = ?', $type)
            ->where('a.tsi_seq_year = ?', $year);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertSequence($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_sequence_id', $bind);
        
        $id = $db->lastInsertId('tbl_sequence_id', 'tsi_id');
        return $id;
    }
    
    public function getConfig($idUniversity=1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('c'=>'tbl_config'))
            ->where("idUniversity = ?",$idUniversity);
        $row = $db->fetchRow($select);
        return $row;		
    }
    
    public function updateSequence($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_sequence_id', $bind, 'tsi_id = '.$id);
        return $update;
    }
    
    public function getNewProgramScheme($programId, $mop, $mos, $pt){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.mode_of_program = ?', $mop)
            ->where('a.mode_of_study = ?', $mos)
            ->where('a.program_type = ?', $pt);
        //echo $select; exit;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getNewLandscape($programId, $progSchemId, $idIntake){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscape'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.IdProgramScheme = ?', $progSchemId)
            ->where('a.IdStartSemester = ?', $idIntake);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateStudentProgramScheme($bind, $studentId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = '.$studentId);
        return $update;
    }
    
    public function getCurSem($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('"'.date('Y-m-d').'" BETWEEN a.SemesterMainStartDate AND a.SemesterMainEndDate');
        //echo $select;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramForSem($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $programId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemBasedOnList($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.display = ?', 1)
            ->where('a.IdScheme = ?', $idScheme)
            ->order('a.AcademicYear DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function cancelProforma($bind, $transId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('proforma_invoice_main', $bind, 'trans_id = '.$transId);
        $db->update('applicant_transaction', array('at_tution_fee' => null, 'at_processing_fee' => null), 'at_trans_id = '.$transId);
        return $update;
    }
    
    public function checkScheme($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $programId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateTransData($bind, $transId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('applicant_transaction', $bind, 'at_trans_id = '.$transId);
        return $update;
    }
    
    public function updateProgramData($bind, $transId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('applicant_program', $bind, 'ap_at_trans_id = '.$transId);
        return $update;
    }
    
    public function getApplicantFeeStructure( $program_id, $scheme, $intake_id, $student_category=579,$throw=1){
		
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('fs'=>'fee_structure'))
            ->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
            ->where("fs.fs_intake_start = '".$intake_id."'")
            ->where("fs.fs_student_category = '".$student_category."'");
        //echo $selectData; exit;
        $row = $db->fetchRow($selectData);

        if(!$row){

            $ex_msg = "No Fee Structure setup for program(".$program_id."), scheme(".$scheme."), intake(".$intake_id."), category(".$student_category.")";

            if ($throw==1){
                throw new Exception($ex_msg);
            }else{
                return false;
            }

        }else{
            return $row;
        }
    }
    
    public function getSlipTemplate($language='en_US'){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'comm_template'), array('value'=>'*'))
            ->join(array('b'=>'comm_template_content'), 'a.tpl_id=b.tpl_id')
            ->where('a.tpl_id = ?', 95)
            ->where('b.locale = ?', $language);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getTags($module = 'records'){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'comm_template_tags'), array('value'=>'*'))
            ->where('a.module = ?', $module);
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>