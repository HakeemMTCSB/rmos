<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_VisaMigrate extends Zend_Db_Table_Abstract {
    
    public function getMigrateData(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'visamigrate'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.vm_student_id=b.registrationId AND a.vm_program=b.IdProgram')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertStudentPassport($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('student_passport', $bind);
        $id = $db->lastInsertId('student_passport', 'p_id');
        return $id;
    }
    
    public function insertStudentVisa($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('student_visa', $bind);
        $id = $db->lastInsertId('student_visa', 'av_id');
        return $id;
    }
}
?>