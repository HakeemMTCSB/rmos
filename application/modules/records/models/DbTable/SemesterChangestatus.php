<?php

class Records_Model_DbTable_SemesterChangestatus extends Zend_Db_Table { //Model Class for Users Details

    protected $_name = 'tbl_studentsemesterstatus';

    public function fnGetStudentStatusList() {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
                        ->from(array("a" => "tbl_definationms"), array("key" => "a.idDefinition", "value" => "a.DefinitionDesc"))
                        ->join(array("b" => "tbl_definationtypems"), "a.idDefType = b.idDefType AND defTypeDesc='Student Semester Status'")
                        ->where("a.Status = 1");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetSemesterStudentStatusList() {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
                        ->from(array("sa" => "tbl_studentapplication"), array("sa.IdApplication AS IdApplication", "sa.ICNumber AS ICNumber", "CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,'')) AS StudentName", "sa.ApplicationDate AS ApplicationDate"))
                        ->join(array("p" => "tbl_program"), 'sa.IDCourse = p.IdProgram')
                        ->where("sa.Active = 1")
                        ->where("sa.Offered = 1")
                        ->where("sa.Accepted = 1")
                        ->where("sa.Termination = 0");

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnViewChangestatus($lintIdApplication) {

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $select = $lobjDbAdpt->select()
                        ->from(array("sss" => "tbl_studentsemesterstatus"), array("sss.*"))
                        ->join(array("sa" => "tbl_studentapplication"), 'sss.IdApplication = sa.IdApplication')
                        ->join(array("dms" => "tbl_definationms"), 'sss.studentsemesterstatus = dms.idDefinition')
                        ->join(array("p" => "tbl_program"), 'sa.IDCourse = p.IdProgram')
                        ->join(array("s" => "tbl_semester"), 'sss.idSemester = s.IdSemester')
                        ->join(array("sm" => "tbl_semestermaster"), 's.Semester = sm.IdSemesterMaster')
                        ->join(array("cm" => "tbl_collegemaster"), 'sa.IdCollege  = cm.IdCollege')
                        ->where("sss.IdApplication = ?", $lintIdApplication);

        return $result = $lobjDbAdpt->fetchAll($select);
    }

    public function fnStudentChangestatusEdit($lintIdsemstatus) {

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $select = $lobjDbAdpt->select()
                        ->from(array("sss" => "tbl_studentsemesterstatus"), array("sss.*"))
                        ->join(array("sa" => "tbl_studentapplication"), 'sss.IdApplication = sa.IdApplication')
                        ->join(array("dms" => "tbl_definationms"), 'sss.studentsemesterstatus = dms.idDefinition')
                        ->join(array("p" => "tbl_program"), 'sa.IDCourse = p.IdProgram')
                        ->join(array("s" => "tbl_semester"), 'sss.idSemester = s.IdSemester')
                        ->join(array("sm" => "tbl_semestermaster"), 's.Semester = sm.IdSemesterMaster')
                        ->join(array("cm" => "tbl_collegemaster"), 'sa.IdCollege  = cm.IdCollege')
                        ->where("sss.idstudentsemsterstatus = ?", $lintIdsemstatus);

        return $result = $lobjDbAdpt->fetchRow($select);
    }

    public function fngetSemesterNameCombo() {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
                        ->from(array("sa" => "tbl_semestermaster"), array("key" => "b.IdSemester", "value" => "CONCAT_WS(' ',IFNULL(sa.SemesterMasterName,''),IFNULL(b.year,''))"))
                        ->join(array('b' => 'tbl_semester'), 'sa.IdSemesterMaster = b.Semester')
                        ->where("sa.Active = 1");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnaddChangeStatus($larrformData) { //Function for adding the user details to the table
        $larrformData['studentsemesterstatus'] = $larrformData['Status'];
        unset($larrformData['Status']);
        $this->insert($larrformData);
    }

    public function fnViewStudentAppnDetails($lintIdApplication) {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
                        ->from(array("sa" => "tbl_studentapplication"), array("sa.*"))
                        ->join(array("p" => "tbl_program"), 'sa.IDCourse = p.IdProgram')
                        ->join(array("cm" => "tbl_collegemaster"), 'sa.IdCollege  = cm.IdCollege')
                        ->where('sa.IdApplication = ?', $lintIdApplication);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnUpdateSemesterStatus($larrformData, $semesterid) { //Function for updating the user
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_studentsemesterstatus";
        $where = 'IdApplication = ' . $larrformData['IdApplication'] . ' AND idSemester = ' . $semesterid;
        $msg = $db->update($table, $larrformData, $where);
        return $msg;
    }

    public function fngetsemester($semid, $Idapplicant) {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $select = $lobjDbAdpt->select()
                        ->from(array("c" => "tbl_studentsemesterstatus"), array("c.*"))
                        ->where("c.idSemester= ?", $semid)
                        ->where("c.IdApplication= ?", $Idapplicant);

        return $result = $lobjDbAdpt->fetchRow($select);
    }
    
    public function checkValidchangeStatusApplication($IdStudent){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $select = $lobjDbAdpt->select()
                        ->from(array("c" => "tbl_credittransfer"), array("c.IdCreditTransfer"))
                        ->where("c.IdStudentRegistration= ?", $IdStudent)
                        ->where("c.ApplicationStatus= ?", 193);
        $result1 = $lobjDbAdpt->fetchAll($select);
        
        $select1 = $lobjDbAdpt->select()
                        ->from(array("c" => "tbl_change_program_application"), array("c.IdChangeProgramApplication"))
                        ->where("c.IdStudent= ?", $IdStudent)
                        ->where("c.ApplicationStatus= ?", 193);
        $result2 = $lobjDbAdpt->fetchAll($select1);

        $retArray = array_merge($result1,$result2);
        return $retArray;
    }
    
	public function getStudentRegisterSemester($IdStudentRegistration){
    	
    		$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentsemesterstatus'),array('a.*'))
							->where('a.IdStudentRegistration  = ?',$IdStudentRegistration)
							->where('a.studentsemesterstatus = ?',130);
			$result = $db->fetchAll($sql);
			return $result;
    }
    
    /*
     * @param : type is refer to transaction type null: Add & Drop 1:Change Status
     * Type was added to cater change status reversal function 
     */
    public function copyanddeletesemesterstatus($IdStudentRegistration,$IdSemesterMain,$message=null,$type=null){
    	
    		$db = Zend_Db_Table::getDefaultAdapter();
    		
    		$auth = Zend_Auth::getInstance();
    		
    		$sql = $db->select()	
					->from(array('sss' => 'tbl_studentsemesterstatus'))
					->where('IdStudentRegistration  = ?',$IdStudentRegistration)
					->where('IdSemesterMain = ?',$IdSemesterMain);
			$result = $db->fetchRow($sql);
				
            
            if($result){
            	
            	//add semesterstatus history
            	
            	$result['Type']=$type; //transaction from null: Add & Drop 1:Change Status 2:Process Semester Status
				$result['message']=$message;
				$result['createddt'] = date('Y-m-d H:i:s');
	            $result['createdby'] = $auth->getIdentity()->iduser;
		
				$db->insert('tbl_studentsemesterstatus_history',$result);
								
				//delete
    			$where = "IdStudentRegistration='".$IdStudentRegistration."' AND IdSemesterMain='".$IdSemesterMain."'";    					    		
    			$db->delete('tbl_studentsemesterstatus',$where);
            }
			 	
			
    }
    
	public function getChangeStatusLatestSemesterStatus($IdStudent){
        $db = Zend_Db_Table::getDefaultAdapter();
        		$sql = $db->select()	
		                   ->from(array("a" => "tbl_studentsemesterstatus_history"))
		                   ->where("a.IdStudentRegistration =?",$IdStudent)
		                   ->where('a.Type=?',1)
		                   ->order('a.idstudentsemsterstatus DESC');
        $larrResult = $db->fetchRow($sql);
        return $larrResult;
    }
    
	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($where){
		 $this->delete( $where );
	}

        public function getRegisterSubjec($studentId, $semesterId){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
                ->where('a.IdStudentRegistration = ?', $studentId)
                ->where('a.IdSemesterMain = ?', $semesterId);
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function insertRegisterSubjectHistory($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('tbl_studentregsubjects_history', $bind);
            $id = $db->lastInsertId('tbl_studentregsubjects_history', 'id');
            return $id;
        }
        
        public function getSubjectDetail($subjectId, $studentId, $semesterId){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregsubjects_detail'))
                ->where('a.regsub_id = ?', $subjectId)
                ->where('a.student_id = ?', $studentId)
                ->where('a.semester_id = ?', $semesterId);
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function insertSubjectDetailHistory($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('tbl_studentregsubjects_detail_history', $bind);
            $id = $db->lastInsertId('tbl_studentregsubjects_detail_history', 'srsdh_id');
            return $id;
        }
        
        public function dropSubject($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $delete = $db->delete('tbl_studentregsubjects', 'IdStudentRegSubjects = '.$id);
            return $delete;
        }
        
        public function dropSubjectDetail($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $delete = $db->delete('tbl_studentregsubjects_detail', 'id = '.$id);
            return $delete;
        }
        
        public function getDropSubject($array){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregsubjects_history'), array('value'=>'*'))
                ->where('a.id IN (?)', $array);
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getDropSubjectDetail($array){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregsubjects_detail_history'), array('value'=>'*'))
                ->where('a.srsdh_id IN (?)', $array);
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function revertDropSubject($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $insert = $db->insert('tbl_studentregsubjects', $bind);
            return $insert;
        }
        
        public function deleteDropSubjectHistory($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $delete = $db->delete('tbl_studentregsubjects_history', 'id = '.$id);
            return $delete;
        }
        
        public function revertDropSubjectDtl($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $insert = $db->insert('tbl_studentregsubjects_detail', $bind);
            return $insert;
        }
        
        public function deleteDropSubjectDtlHistory($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $delete = $db->delete('tbl_studentregsubjects_detail_history', 'srsdh_id = '.$id);
            return $delete;
        }
}