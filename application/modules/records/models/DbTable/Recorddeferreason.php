<?php
class Records_Model_DbTable_Recorddeferreason extends Zend_Db_Table_Abstract {
    protected $_name = 'tbl_record_reason_defer';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnfetchdeferReasons($idUniversity){
           $lstrSelect = $this->lobjDbAdpt->select()
                            ->from(array("a" => "tbl_record_reason_defer"), array("a.*"))
                            ->where("a.IdUniversity =?",$idUniversity);
           $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
           return $larrResult;
    }

    public function fnadddeferreasons($data) {
        $this->insert($data);
    }

    public function fnfetchAllDeferReasons($Iduniversity) {
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_reason_defer"), array("a.*"))
                        ->where("a.IdUniversity = ?", $Iduniversity);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fndeletedeferReasons($id){
        $where = $this->lobjDbAdpt->quoteInto('IdUniversity = ?', $id);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }

    public function getDeferReasons($Iduniversity=1){
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_reason_defer"), array('key' => 'a.IdRecordResonDefer', 'name' => 'a.ReasonDefer'))
                        ->where("a.IdUniversity = ?", $Iduniversity);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function getDeferDescription($id){
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_reason_defer"), array('a.ReasonDefer'))
                        ->where("a.IdRecordResonDefer = ?", $id);
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }


    public function getReason($id){
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a'=>'tbl_definationms'), array('key'=>'a.idDefinition', 'name'=>'a.DefinitionDesc'))
            ->where('a.idDefType = ?', $id)
            ->where('a.Status = ?', 1);
        
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
    
    public function getDefination($id){
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a'=>'tbl_definationms'), array('key'=>'a.idDefinition', 'name'=>'a.DefinitionDesc'))
            ->where('a.idDefinition = ?', $id);
        
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }
}
?>

