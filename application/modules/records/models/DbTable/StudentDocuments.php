<?php

class Records_Model_DbTable_StudentDocuments extends Zend_Db_Table_Abstract {

    protected $_name = 'student_documents';
    protected $_primary = 'ad_id';
   
	public function getDataById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sd' => $this->_name));
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
	public function getDocumentById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sd' => $this->_name))
                ->where('ad_id = ?',$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    

	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
		return $id =  $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($where){
		 $this->delete( $where );
	}
	
	public function getDocument($sp_id,$type,$tbl_id,$tblname) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sd' => $this->_name)) 
                ->where('sd.sp_id = ?',$sp_id)
                ->where('sd.ad_type = ?',$type)
                ->where('sd.ad_table_name = ?',$tblname)
                ->where('sd.ad_table_id = ?',$tbl_id) ;
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    
    
	public function getDocuments($sp_id,$type,$tbl_id,$tblname) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('sd' => $this->_name)) 
                ->where('sd.sp_id = ?',$sp_id)
                ->where('sd.ad_type = ?',$type)
                ->where('sd.ad_table_name = ?',$tblname)
                ->where('sd.ad_table_id = ?',$tbl_id) ;
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
}

?>