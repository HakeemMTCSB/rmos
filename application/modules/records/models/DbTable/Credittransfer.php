<?php
class Records_Model_DbTable_Credittransfer extends Zend_Db_Table { //Model Class for Credit transfer subjects
	protected $_name = 'tbl_credittransfer';
	private $lobjDbAdpt;

        public function init() {
          $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        }
	
        
        /**
         * Function to get credit transfer details based on studentID
         * @param type $id
         * @return type
         * @author Vipul
         */
        
        public function getCreditTransferDetails($studentId,$IdcreditTransferApp){
        $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer','a.IdStudentRegistration'))
               ->joinLeft(array('d' => 'tbl_credittransfersubjects'),"a.IdCreditTransfer = d.IdCreditTransfer AND d.IdCreditTransfer='".$IdcreditTransferApp."' ", array("d.*")) 
               ->joinLeft(array('sm' => 'tbl_semestermaster'),"sm.IdSemesterMaster = d.IdSemesterMain", array("sm.SemesterMainCode")) 
               ->joinLeft(array('si' => 'tbl_semester'),"si.IdSemester = d.IdSemester", array("si.SemesterCode"))
               ->joinLeft(array('sub' => 'tbl_subjectmaster'),"sub.IdSubject = d.IdCourse", array("sub.SubjectName"))
               ->joinLeft(array('dftnss' => 'tbl_definationms'),"dftnss.idDefinition = d.AppliedBy", array("dftnss.DefinitionDesc as username"))
               ->joinLeft(array('dft' => 'tbl_definationms'),"dft.idDefinition = a.ApplicationStatus", array("dft.DefinitionDesc as DefinitionDesc"))
               ->joinLeft(array('apvdby' => 'tbl_definationms'),"apvdby.idDefinition = d.Approvedby", array("apvdby.DefinitionDesc as apvdbyusername"))                 
               ->joinLeft(array('insti' => 'tbl_institution'),"insti.idInstitution = d.IdInstitution", array("insti.InstitutionName"))
               ->joinLeft(array('qual' => 'tbl_qualificationmaster'),"qual.IdQualification = d.IdQualification", array("qual.QualificationLevel"))
               ->joinLeft(array('spl' => 'tbl_specialization'),"spl.IdSpecialization = d.IdSpecialization", array("spl.Specialization"))
               ->joinLeft(array('submast' => 'tbl_subjectmaster'),"submast.IdSubject = d.EquivalentCourse", array("submast.SubjectName as EquivalentCourse")) 
               ->joinLeft(array('grade' => 'tbl_subjectgradepoint'),"grade.Idsubjectgradepoint = d.EquivalentGrade", array("grade.subjectgrade as EquivalentGrade"))  
                
               ->where("a.IdStudentRegistration =?",$studentId)
               ->where("d.IdCreditTransfer !=?",'')
               ->order("d.ApplicationDate");
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
        }


        public function fnSaveCreditTransfer($data){
            $lstrTable = "tbl_credittransfer";
            $this->lobjDbAdpt->insert($lstrTable,$data);
            return $this->lobjDbAdpt->lastInsertId();
        }

        public function getListCTApplication(){
            $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer'));
            $result = $this->lobjDbAdpt->fetchAll($sql);
            return $result;

        }
         
        
        /**
         * Function to update credit status
         * @Author vipul
         */
        
        public function saveCreditTransfer($commentData, $selData, $type, $creditID, $studentId) { 
            $table_update_sa = "tbl_credittransfersubjects";
            $table_studregsubj = "tbl_studentregsubjects";
            $flag = false;
            //asd($selData,false);
            $creditObj = new Records_Model_DbTable_Credittransfersubject();
            foreach($selData as $key=>$values) { 
                $getData = explode('-',$values);
                $getID = $getData[0]; // autoincrementId 
                $getAppValue = $getData[1];
                if($getAppValue=='1') { // 1 is for approve, insert course into student regsubj table 
                  $flag = true;
                  // get the courseID, semesterID
                  $condition_cs = " a.IdCreditTransferSubjects = '".$getID."' ";  
                  $getSemCourse  =  $creditObj->getCreditSubjectDetalByID($condition_cs);
                  
                  
                  // check for course already in table
                  $checkSemCourse  =  $creditObj->checkCoursesReg($getSemCourse,$studentId);

                  


                  if($checkSemCourse[0]['totalCourses']=='0') {
                      
                      $ret = $creditObj->checkcourseregistered($getSemCourse[0]['IdCourse'],$studentId);                      
                      if(!empty($ret)){
                          $data = array(
                                'IdSemesterDetails'=>$getSemCourse[0]['IdSemester'],
                                'IdSemesterMain'=>$getSemCourse[0]['IdSemesterMain'],
                                'IdGrade'=>'CT',
                              );
                          $creditObj->updatecourseregistered($data,$ret['IdStudentRegSubjects']);
                      }  else{
                          $lstudregsubjArr = array('IdStudentRegistration'=>$studentId,
                                               'IdSubject'=>$getSemCourse[0]['IdCourse'],
                                               'IdSemesterDetails'=>$getSemCourse[0]['IdSemester'],
                                               'IdSemesterMain'=>$getSemCourse[0]['IdSemesterMain'],
                                               'IdGrade'=>'CT',
                                               'UpdDate'=>date('Y-m-d H:i:s'),
                                               'UpdUser'=>'1',
                                             );
                          $this->lobjDbAdpt->insert($table_studregsubj,$lstudregsubjArr);
                      }
                  } 
                }
                
                $lAppStudArrs = array( 'creditStatus'=>$getAppValue);
                $where ='IdCreditTransferSubjects = '.$getID;
                $this->lobjDbAdpt->update($table_update_sa,$lAppStudArrs,$where);
            } 
            
            if(count($commentData)>0 && count($selData)>0) { 
                foreach($commentData as $key=>$values) {
                    $getData = explode('-',$values);
                    $getID = $getData[0]; // autoincrementId
                    $getAppValue = $getData[1];
                    $lAppStudArrs = array( 'disapprovalComment'=>$getAppValue);
                    $where ='IdCreditTransferSubjects = '.$getID;
                    $this->lobjDbAdpt->update($table_update_sa,$lAppStudArrs,$where);
                }
            }
            
            if($flag) {
            	$data = array( 'ApplicationStatus'=>'243');
                $where ='IdCreditTransfer = '.$creditID;
                $this->lobjDbAdpt->update("tbl_credittransfer",$data,$where);
            }
            else {
            	$data = array( 'ApplicationStatus'=>'244');
                $where ='IdCreditTransfer = '.$creditID;
                $this->lobjDbAdpt->update("tbl_credittransfer",$data,$where);
            }
            
            
            
        }

        public function getDetailApplication($appId){
            $sql = $this->lobjDbAdpt->select()
                   ->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer','a.IdStudentRegistration','a.ApplicationStatus AS AppStatus'))
                   ->joinLeft(array('sr' => 'tbl_studentregistration'), 'a.IdStudentRegistration = sr.IdStudentRegistration')
                   ->join(array('sp' => 'student_profile'), 'sr.sp_id = sp.id')
                   ->joinLeft(array('d' => 'tbl_credittransfersubjects'),"a.IdCreditTransfer = d.IdCreditTransfer", array("d.*"))
                   ->joinLeft(array('sm' => 'tbl_semestermaster'),"sm.IdSemesterMaster = d.IdSemesterMain", array("sm.SemesterMainCode"))
                   ->joinLeft(array('si' => 'tbl_semester'),"si.IdSemester = d.IdSemesterMain", array("si.SemesterCode"))               
                   ->joinLeft(array('dftnss' => 'tbl_definationms'),"dftnss.idDefinition = d.AppliedBy", array("dftnss.DefinitionDesc as username"))
                   ->joinLeft(array('dft' => 'tbl_definationms'),"dft.idDefinition = d.ApplicationStatus", array("dft.DefinitionDesc as DefinitionDesc"))
                   ->joinLeft(array('def' => 'tbl_definationms'),"def.idDefinition = a.ApplicationStatus", array("def.DefinitionDesc as ApplicationStatusMain"))
                   ->joinLeft(array('c' => 'tbl_studentregistration'),"a.IdStudentRegistration = c.IdStudentRegistration",array("c.registrationId","c.ExtraIdField1","c.FName","c.LName","c.MName"))
                   ->joinLeft(array('x' => 'tbl_intake'),"c.IdIntake = x.IdIntake",array('x.IntakeDesc'))
                   ->joinLeft(array('e' => 'tbl_program'),"c.IdProgram = e.IdProgram",array('e.ProgramName',"e.IdScheme"))
                   ->joinLeft(array('f' => 'tbl_scheme'),"e.IdScheme = f.IdScheme",array('f.EnglishDescription'))
                   ->joinLeft(array('y' => 'tbl_definationms'),"c.profileStatus = y.idDefinition",array('y.DefinitionDesc as profilestatus'))
                   ->where("a.IdCreditTransfer =?",$appId)
                   ->order("d.ApplicationDate");
            $result = $this->lobjDbAdpt->fetchAll($sql);
            return $result;
        }
        
        
        /**
         * Function to revert credit status
         * @Author vipul
         */
        public function revertCreditTransfer($type,$creditID,$studentId) {
             
             $creditObj = new Records_Model_DbTable_Credittransfersubject();
             
             // DELETE RECORDS FOR THE STUDENT WHICH HAVE GRADE AS 'CT' for that IdCreditTransfer.
             $condition_cs = " a.IdCreditTransfer = '".$creditID."' AND a.creditStatus = '1' ";  
             $getSemCourse  =  $creditObj->getCreditSubjectDetalByID($condition_cs);           
             foreach ($getSemCourse as $values) { 
                 $wh_del = " IdStudentRegistration = '".$studentId."'  AND IdSubject='".$values['IdCourse']."' AND IdGrade='CT' ";
                 $this->lobjDbAdpt->delete('tbl_studentregsubjects', $wh_del);
             }


             // UPDATE CREDIT STATUS TO NULL
             $table_update_sa = "tbl_credittransfersubjects";
             $lAppStudArrs = array( 'creditStatus'=>NULL, 'disapprovalComment'=>NULL);  
             $where_1 ='IdCreditTransfer = '.$creditID;
             $this->lobjDbAdpt->update($table_update_sa,$lAppStudArrs,$where_1);
             
             // UPDATE APPLICATION STATUS TO ENTRY
             $lAppStudArrs_appstat= array( 'ApplicationStatus'=>'193');  
             $where_2 ='IdCreditTransfer = '.$creditID;
             $this->lobjDbAdpt->update('tbl_credittransfer',$lAppStudArrs_appstat,$where_2);
             
        }
        
        
        /**
         * Function to revert credit status
         * @Author vipul
         */
        public function updateCreditAppStatus($IdCreditTransfer,$type) {
            
            if($type=='entry') { 
                 $lAppStudArrs_appstat= array( 'ApplicationStatus'=>'193');  
                 $where_2 ='IdCreditTransfer = '.$IdCreditTransfer;
                 $this->lobjDbAdpt->update('tbl_credittransfer',$lAppStudArrs_appstat,$where_2);
            }
            
            if($type=='disapprove') { 
                 $lAppStudArrs_appstat= array( 'ApplicationStatus'=>'244');  
                 $where_2 ='IdCreditTransfer = '.$IdCreditTransfer;
                 $this->lobjDbAdpt->update('tbl_credittransfer',$lAppStudArrs_appstat,$where_2);
            }
            
            if($type=='approve') { 
                 $lAppStudArrs_appstat= array( 'ApplicationStatus'=>'243');  
                 $where_2 ='IdCreditTransfer = '.$IdCreditTransfer;
                 $this->lobjDbAdpt->update('tbl_credittransfer',$lAppStudArrs_appstat,$where_2);
            }
            
            
        }
        
        
        
   
}
