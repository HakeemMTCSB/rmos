<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 7/11/2016
 * Time: 2:35 PM
 */
class Records_Model_DbTable_AuditTrail extends Zend_Db_Table_Abstract{

    public function getAuditTrailInvoice(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.creator = b.iduser', array('createdby'=>'b.loginName'))
            ->joinLeft(array('c'=>'tbl_user'), 'a.approve_by = c.iduser', array('approvedby'=>'c.loginName'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.cancel_by = d.iduser', array('canceledby'=>'d.loginName'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAuditTrailDiscount(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.dcnt_creator = b.iduser', array('createdby'=>'b.loginName'))
            ->joinLeft(array('c'=>'tbl_user'), 'a.dcnt_approve_by = c.iduser', array('approvedby'=>'c.loginName'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.dcnt_cancel_by = d.iduser', array('canceledby'=>'d.loginName'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAuditTrailCreditNote(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'credit_note'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.cn_creator = b.iduser', array('createdby'=>'b.loginName'))
            ->joinLeft(array('c'=>'tbl_user'), 'a.cn_approver = c.iduser', array('approvedby'=>'c.loginName'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.cn_cancel_by = d.iduser', array('canceledby'=>'d.loginName'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAuditTrailReceipt(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'receipt'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.rcp_create_by = b.iduser', array('createdby'=>'b.loginName'))
            ->joinLeft(array('c'=>'tbl_user'), 'a.cancel_by = c.iduser', array('canceledby'=>'c.loginName'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAuditTrailCourseRegistration(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_history'))
            ->join(array('b'=>'tbl_user'), 'a.createdby = b.iduser', array('createdby'=>'b.loginName'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAuditTrailCreditTransfer(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_credittransfer'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.AppliedBy = b.iduser', array('createdby'=>'b.loginName'))
            ->joinLeft(array('c'=>'tbl_user'), 'a.ApprovedBy = c.iduser', array('approvedby'=>'c.loginName'));

        $result = $db->fetchAll($select);
        return $result;
    }
}