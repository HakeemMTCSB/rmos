<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_Model_DbTable_SemesterStatus extends Zend_Db_Table {
    
    public function init() {
        $this->conn = Zend_Db_Table::getDefaultAdapter();
    }
    
    public function getStudentInfo($id){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregistration'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram=b.IdProgram')
            ->where('a.IdStudentRegistration = ?', $id);
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getIntake($id){
        $sql = $this->conn->select()->from(array('a'=>'tbl_intake'))
            ->where('a.IdIntake = ?', $id);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getSemester($idScheme, $year, $month){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getCurrentSemester($idScheme){
        $date = date('Y-m-d');
        
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function getSemesterList($idScheme, $date1, $date2){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.IsCountable = ?', 1)
            ->where('a.SemesterMainStartDate BETWEEN "'.$date1.'" AND "'.$date2.'"')
            ->order('a.SemesterMainStartDate ASC');
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function checkSemesterStatus($idstudent, $idsemester){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentsemesterstatus'))
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.idSemester = ?', $idsemester);
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function checkSubjectRegister($idstudent, $idsemester){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('a.IdSemesterMain = ?', $idsemester)
            ->where('a.Active <> ?', 3);
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function insertSemesterStatus($data){
        $this->conn->insert('tbl_studentsemesterstatus', $data);
        $id = $this->conn->lastInsertId('tbl_studentsemesterstatus', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function insertSemesterStatusHistory($data){
        $this->conn->insert('tbl_studentsemesterstatus_history', $data);
        $id = $this->conn->lastInsertId('tbl_studentsemesterstatus_history', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function updateSemesterStatus($data, $id){
        $update = $this->conn->update('tbl_studentsemesterstatus', $data, 'idstudentsemsterstatus = '.$id);
        return $update;
    }
    
    public function getLastSemesterRegister($idstudent){
        $sql = $this->conn->select()->from(array('a'=>'tbl_studentregsubjects'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.IdSemesterMain=b.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $idstudent)
            ->where('b.IsCountable = ?', 1)
            ->where('a.Active <> ?', 3)
            ->order('b.SemesterMainStartDate DESC');
        
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
    
    public function listUnregisteredSemester($scheme, $date1, $date2, $semType=0){
        $sql = $this->conn->select()->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.SemesterMainStartDate BETWEEN "'.$date1.'" AND "'.$date2.'"')
            ->where('a.IsCountable = ?', 1)
            ->where('a.SemesterFunctionType IS NULL');
        
        if ($semType!=0){
            $sql->where('a.SemesterType = ?', $semType);
        }
        
        $sql->order('a.SemesterMainStartDate ASC');
        
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
}