<?php
class Records_Model_DbTable_Changestatus extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentstatus';
	
	
	public function fnGetStudentStatusList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
		 				 ->join(array("b"=>"tbl_definationtypems"),"a.idDefType = b.idDefType AND defTypeDesc='Semester Status'")
		 				 ->where("a.Status = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetSemesterStudentStatusList(){
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								 ->from(array("sa"=>"tbl_studentapplication"),array("sa.IdApplication AS IdApplication","sa.ICNumber AS ICNumber","CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,'')) AS StudentName","sa.ApplicationDate AS ApplicationDate"))
		 							 ->join(array("sr"=>"tbl_studentregistration"),'sa.IdApplication = sr.IdApplication ')
		 							 ->join(array("p"=>"tbl_program"),'sa.IDCourse = p.IdProgram');
       				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fngetStudentprofileDtls() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                             ->from(array("a"=>"tbl_studentregistration"),array('a.registrationId'))
       								 ->join(array("sa"=>"tbl_studentapplication"),'sa.IdApplication = a.IdApplication',array("sa.IdApplication AS IdApplication","sa.ICNumber AS ICNumber","CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,'')) AS StudentName","sa.ApplicationDate AS ApplicationDate"))
		 							 ->join(array("p"=>"tbl_program"),'sa.IDCourse = p.IdProgram')
		 							 ->join(array("cm"=>"tbl_collegemaster"),'sa.IdCollege  = cm.IdCollege')
		 							 ->joinLeft(array("ss"=>"tbl_studentstatus"),'sa.IdApplication  = ss.IdApplication',array('ss.Status'))
		 							// ->where("cm.CollegeType = 1")
		 							 ->where("sa.Registered = 1")
		 							 ->order("sa.FName");
       					//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
     
	public function fnGetApplicantNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("key"=>"sa.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
		 				// ->where("sa.Active = 1");
		 				->order("sa.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
    public function fnGetFacultyNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_collegemaster"),array("key"=>"sa.IdCollege","value"=>"sa.CollegeName"))
		 				 ->where("sa.Active = 1")
		 				->order("sa.CollegeName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
     
	public function fnSearchStudentApplication($post = array()) { //Function to get the user details
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								 ->from(array("sa"=>"tbl_studentapplication"),array("sa.IdApplication AS IdApplication","sa.ICNumber AS ICNumber","CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,'')) AS StudentName","sa.ApplicationDate AS ApplicationDate"))
                                     ->join(array("q"=>"tbl_studentregistration"),'q.IdApplication = sa.IdApplication')
       								 ->join(array("p"=>"tbl_program"),'sa.IDCourse = p.IdProgram')
		 							 ->join(array("cm"=>"tbl_collegemaster"),'sa.IdCollege  = cm.IdCollege')
		 							 ->joinLeft(array("ss"=>"tbl_studentstatus"),'sa.IdApplication  = ss.IdApplication',array('ss.Status'));
       								
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("sa.IdApplication = ?",$post['field5']);
										
			}
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("sa.IDCourse = ?",$post['field8']);
										
			}
	       if(isset($post['field1']) && !empty($post['field1']) ){
				$lstrSelect = $lstrSelect->where("sa.IdCollege = ?",$post['field1']);
										
			}		
			if(isset($post['field3']) && !empty($post['field3']) ){
				$lstrSelect = $lstrSelect->where("q.registrationId = ?",$post['field3']);
										
			}			
       			$lstrSelect	->where('sa.ICNumber like "%" ? "%"',$post['field2'])
       						->where("sa.Active = 1")
		 					->where("sa.Registered = 1")
		 					->order("sa.FName");;
       						//->where('sa.Active = 1')
		 					//->where("cm.CollegeType = 1");
		 		//$lstrSelect->order("sa.FName");
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
  
 
	public function fnViewStudentAppnDetails($lintIdApplication){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("sr"=>"tbl_studentregistration"),array("sr.registrationId"))
		 				 ->join(array("sa"=>"tbl_studentapplication"),'sa.IdApplication = sr.IdApplication')
		 				 ->join(array("p"=>"tbl_program"),'sa.IDCourse = p.IdProgram')
		 				 ->join(array("cm"=>"tbl_collegemaster"),'sa.IdCollege  = cm.IdCollege')
		 				 ->join(array("se"=>"tbl_semester"),'sr.IdSemester = se.IdSemester')
		 				 ->join(array("sm"=>"tbl_semestermaster"),'se.Semester = sm.IdSemesterMaster')
		 				 ->where('sa.IdApplication = ?',$lintIdApplication);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	public function fnViewChangestatus($lintIdApplication){
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_studentstatus"),array("a.*"))
					->where("a.IDApplication = ?",$lintIdApplication);		   
		 return $result = $lobjDbAdpt->fetchRow($select);	
		
	}
	
	public function fnaddChangeStatus($larrformData) { //Function for adding the user details to the table
		unset($larrformData['IdStudentStatus']);
		$this->insert($larrformData);
	}	
	
	public function fnupdateChangeStatus($larrformData) { //Function for updating the user
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_studentstatus";
	    $where = 'IdStudentStatus = '.$larrformData['IdStudentStatus'];
	    $msg = $db->update($table,$larrformData,$where);
	   return $msg;
    }
    
	public function fnupdateStudentAppn($larrformData,$lintIdApplication) { 
		$db = Zend_Db_Table::getDefaultAdapter();
    	if($larrformData['Status']==92)
    	{
    		$larrformData = array('Active'=>"1");
    	}else if($larrformData['Status']==93)
    	{
    		$larrformData = array('Active'=>"0");
    	}else if($larrformData['Status']==94)
    	{
    		$larrformData = array('Active'=>"0",
    							  'Termination'=>"1");
    	}else if($larrformData['Status']==95)
    	{
    		$larrformData = array('Active'=>"0",
    							  'Termination'=>"0");
    	}else if($larrformData['Status']==96)
    	{
    		$larrformData = array('Active'=>"1",
    							  'Termination'=>"0");
    	}
    	
    	$table = "tbl_studentapplication";
		$where = 'IdApplication = '.$lintIdApplication;
		$db->update($table,$larrformData,$where);
    }
	
   
}