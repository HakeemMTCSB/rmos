<?php

class Records_Model_DbTable_SemesterMaster extends Zend_Db_Table {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_semestermaster';
	protected $_primary = "IdSemesterMaster";
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
    public function getNextSemesterList($current_date = null,$registrationId)
    {
        /*
         * Get status Id
         */ 
        
        $statusId = $this->getStatusId('PreRegister');
        
        /*
         * Get semester list that not yet registered
         * where date semester start > today         
         */
        
        $db = $this->lobjDbAdpt;
        
        $select =$db->select()
        ->from(array('a' => $this->_name))
        ->joinRight('tbl_studentsemesterstatus',"a.IdSemesterMaster = IdSemesterMain 
            AND IdStudentRegistration ='".$registrationId."' 
            AND studentsemesterstatus <> $statusId")
        ->where("a.SemesterMainStartDate >'".$current_date."'")
        ->group('IdSemesterMain')
        ->order('a.SemesterMainStartDate');
        
        $result = $db->fetchAll($select);
		
		return $result;
	}
	
	public function getLevel($registrationId)
    {
        /**
         * List of status 
         */
        $defination = array('Register','PreRegister','Completed');
        
        /**
         * Select status
         */
        $select = $this->lobjDbAdpt->select()
            ->from('tbl_definationms','idDefinition')
            ->where('DefinitionCode IN (\''.implode("','",$defination).'\')')
            ->where('IdDefType = 32');
            
        $result = $this->lobjDbAdpt->fetchAll($select);
        
        $massage_result = NULL;
        
        /*
         * Convert array to string
         */ 
        foreach($result as $key => $value)
        {
            $massage_result.= "'".$value['idDefinition']."',";            
        }
        
        /*
         *get current max level 
         */
        $select_status = $this->lobjDbAdpt->select()
            ->from('tbl_studentsemesterstatus','MAX(Level)')
            ->where('studentsemesterstatus IN ('.rtrim($massage_result,',').')')
            ->where("IdStudentRegistration ='".$registrationId."'");
        
        $status_result = $this->lobjDbAdpt->fetchAll($select_status);
        
        $max_level = $status_result[0]['MAX(Level)'] + 1;
        return $max_level;
    }
    
    public function getStatusId($definatonCode)
    {
        $select = $this->lobjDbAdpt->select()
        ->from('tbl_definationms','idDefinition')
        ->where('DefinitionCode = ?',$definatonCode)
        ->where('IdDefType = 32');
        
        $result = $this->lobjDbAdpt->fetchAll($select);
        
        $idDefinition = $result[0]['idDefinition'];
        return $idDefinition;
    }
    
    public function getSemesterByScheme($student,$consecutive_sem=1){
    	
    	$student['SemesterMainStartDate'] = '2014-06-01';
    	 
    	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))               
                  ->where('sm.IdScheme = ?',(int)$student['IdScheme'])
                  ->where('sm.Branch = ?',(int)$student['IdBranch'])
                  ->where('sm.SemesterMainEndDate < CURDATE()') //kene enable kan ni saje off sebab xde data
                  ->where('sm.SemesterMainStartDate >= ?',$student['SemesterMainStartDate'])
                 // ->orwhere('(sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE())')
                  ->order('sm.SemesterMainStartDate DESC')
                  ->limit($consecutive_sem,0);             
        $result = $db->fetchAll($sql); 

        if(count($result)==0){
        	
        $sql2 = $db->select()
                  ->from(array('sm' => $this->_name))               
                  ->where('sm.IdScheme = ?',(int)$student['IdScheme'])    
                  ->where('sm.Branch = ?',0)		             
                  ->where('sm.SemesterMainEndDate < CURDATE() AND sm.SemesterMainStartDate >= ?',$student['SemesterMainStartDate'])
                //  ->orwhere('(sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE())')
                  ->order('sm.SemesterMainStartDate DESC')
                  ->limit($consecutive_sem,0);             
        		$result2 = $db->fetchAll($sql2); 
        		$result = $result2;
        
        }
        return $result;
    }
	
    public function getStudentSemesterStatus($student){
    	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sss' => 'tbl_studentsemesterstatus')) 
                  ->where('sss.IdStudentRegistration = ?',(int)$student['IdStudentRegistration']);    
        $result = $db->fetchRow($sql); 
    }
    
 	public function getStudentCurrentSemester($student)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$student['IdProgramScheme']) 
                  ->where('sm.Branch = ?',(int)$student['IdBranch'])
                  ->order('sm.SemesterMainStartDate desc');    
         
        $result = $db->fetchRow($sql); 
        
        if(!$result){
        	$sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$student['IdProgramScheme'])
                  ->where('sm.Branch = ?',0)
                  ->order('sm.SemesterMainStartDate desc');
        	$result = $db->fetchRow($sql); 
        }
        
        return $result;
    	
    }
    
	
}

