<?php 

class Records_Model_DbTable_StudentRegSubjectsHistory extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_main = 'tbl_studentregsubjects_history';
	protected $_primary_main = "id";
	protected $_detail = 'tbl_studentregsubjects_detail_history';
	protected $_primary_detail = "srsdh_id";


	public function searchCourseRegistration($post=null){
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			
		    $select =$db->select()
	 				 ->from(array('srs'=>$this->_main))	
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('registrationId'))		 
	 				 ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubjectName'))
	 				 ->joinLeft(array('p'=>'tbl_program'),'srs.audit_program=p.IdProgram',array('auditprogram'=>'ProgramCode'))
	 				 ->joinLeft(array('smr'=>'tbl_subjectmaster'),'smr.IdSubject=srs.IdSubject',array('subcode_replace'=>'SubCode'))
	 				 ->join(array("sem"=>"tbl_semestermaster"),'sem.IdSemesterMaster=srs.IdSemesterMain',array('semester'=>"SemesterMainName"));
	 				 //->join(array('u'=>'tbl_user'),'u.iduser = srs.createdby',array()) 
	 				 //->join(array('s'=>'tbl_staffmaster'),'u.IdStaff = s.IdStaff',array('createdby'=>'Fullname')) 
	 			
	 		 
 		    if (isset($post['Sorting']) && !empty($post['Sorting'])) {         
 		    	if($post['Sorting']==1){
 		    		 $select->order('sem.SemesterMainStartDate ASC');
 		    	}else 
 		    	if($post['Sorting']==2){
 		    		$select->order('sem.SemesterMainStartDate DESC');
 		    	}else 
 		    	if($post['Sorting']==3){
 		    		$select->order('srs.createddt ASC');
 		    	}else 
 		    	if($post['Sorting']==4){
 		    		$select->order('srs.createddt DESC');
 		    	}
 		    }		 
	 			

	  		if (isset($post['student_id']) && !empty($post['student_id'])) {         
	             $select->where("sr.registrationid = ?",$post['student_id']);
	        }
	        
		    if (isset($post['IdSemester']) && !empty($post['IdSemester'])) {         
	             $select->where("srs.IdSemesterMain = ?", $post['IdSemester']);
	        }
	        
			
			if (isset($post['subject_code']) && !empty($post['subject_code'])) {         
	             $select->where("sm.SubCode LIKE ?", '%'.$post['subject_code'].'%');
	        } 
	        
		   
	        $row = $db->fetchAll($select);
		 	
		 	
		 	return $row;
				 
	}
	
	
	public function getDetailItemByCourse($IdStudentRegistration,$idSemester,$idSubject,$post=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()
	 				 ->from(array('srsdh'=>$this->_detail))		
	 				 ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition=srsdh.item_id', array('item_name'=>'DefinitionDesc'))
	 				 ->joinLeft(array('srsd' => 'tbl_studentregsubjects_detail'), 'srsd.id=srsdh.id', array('invoice_id_main'=>'invoice_id'))
	 				 ->where('srsdh.student_id = ?',$IdStudentRegistration)	
	 				 ->where('srsdh.semester_id = ?',$idSemester)	
	 				 ->where('srsdh.subject_id = ?',$idSubject);

 		if (isset($post['Sorting']) && !empty($post['Sorting'])) { 
 	    	if($post['Sorting']==3){
 	    		$select->order('srsdh.createddt ASC');
 	    	}else 
 	    	if($post['Sorting']==4){
 	    		$select->order('srsdh.createddt DESC');
 	    	}
 	    }		 
	 			
 		    
 		if (isset($post['status']) && !empty($post['status'])) {         
             $select->where("srsdh.status = ?", $post['status']);
        }

       
	 	$row = $db->fetchAll($select);
		return $row;
	 		
	}
	
	public function getUser($iduser){
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()
	 				 ->from(array('u'=>'tbl_user'))	
	 				 ->join(array('s' => 'tbl_staffmaster'), 's.IdStaff=u.IdStaff', array('FullName'))	
	 				 ->where('u.iduser = ?',$iduser);
	 	$row = $db->fetchRow($select);
	 	return $row;
	}
}

?>