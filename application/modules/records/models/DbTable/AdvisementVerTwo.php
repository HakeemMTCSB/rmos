<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Records_Model_DbTable_AdvisementVerTwo extends Zend_Db_Table_Abstract {
    
    public function getLandscape($idProgram, $idProgramScheme, $intake){
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
            ->from(array('a'=>'tbl_landscape'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $idProgram)
            ->where('a.IdProgramScheme = ?', $idProgramScheme)
            ->where('a.IdStartSemester = ?', $intake)
            ->where('a.Active = ?', 1);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCourse($studentId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_subjectmaster'), 'a.IdSubject=b.IdSubject', array('SubjectName', 'SubCode', 'CreditHours'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemesterMain=c.IdSemesterMaster', array('SemesterMainStartDate'))
            ->where('a.IdStudentRegistration = ?', $studentId)
            ->where('a.Active = 1 OR a.Active = 4')
            ->order('c.SemesterMainStartDate ASC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDeferSemester($studentId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.idSemester=b.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $studentId)
            ->where('a.studentsemesterstatus = ?', 250)
            ->where('a.validReason = ?', 1);
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>