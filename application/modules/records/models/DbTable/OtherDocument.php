<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_OtherDocument extends Zend_Db_Table_Abstract {

    public function insertOtherDocument($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('student_otherdocument', $bind);
        $id = $db->lastInsertId('student_otherdocument', 'sod_id');
        return $id;
    }
    
    public function insertOtherDocumentUpload($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('student_otherdocument_upload', $bind);
        $id = $db->lastInsertId('student_otherdocument_upload', 'sou_id');
        return $id;
    }
    
    public function getOtherDocument($studId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_otherdocument'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.sod_type = b.idDefinition', array('sodType'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_user'), 'a.sod_updby = c.iduser', array('updBy'=>'c.loginName'))
            ->where('a.sod_spid = ?', $studId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getOtherDocumentUpload($sodId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'student_otherdocument_upload'), array('value'=>'*'))
            ->where('a.sou_sodid = ?', $sodId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>