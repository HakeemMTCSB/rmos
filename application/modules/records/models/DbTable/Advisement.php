<?php

class Records_Model_DbTable_Advisement extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_advisement';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function insertData($data) {
        
        $this->insert($data);
       
    }
    
    public function getList($student_id) {
        
        $db = $this->lobjDbAdpt;
        
        $sql = $db->select()
                ->from(array('a' => $this->_name),array('a.*'))
                ->join(array('b' => 'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.*'))
                ->where('a.registration_id = ?',(int)$student_id)
                ->order('a.semester ASC');
         //echo $sql;      
        $result = $db->fetchAll($sql);
        
        return $result;
    }
    
    public function updateData($data,$id) {
        $where = 'advisement_id = '.$id;
        $this->update($data,$where);
    }
    
    public function deleteData($id) {
        $where = 'advisement_id ='.$id;
        $table = $this->_name;
        
        $this->delete($where);
    }
    
    public function deleteAll($id) {
        $where = 'registration_id = '.$id;
        
        $this->delete($where);
    }
    
    public function getGrade($program_id,$subject_id,$subject_grade){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  
 
        //1st : check setup yang base on effective semester, program & subject
        $select_grade = $db->select()
                          ->from(array('gsm'=>'tbl_gradesetup_main'))
                          ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
                          ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
                          ->join(array('sm' => 'tbl_semestermaster'),'gsm.IdSemester = sm.IdSemesterMaster',array('*'))
                          ->where('sm.SemesterMainStartDate <= NOW()')	
                          ->where('gsm.BasedOn = 1 ') //program	& subject	
                          ->where('gsm.IdProgram = ?',$program_id)
                          ->where('gsm.IdSubject = ?',$subject_id)
                          ->where('d.DefinitionCode = ?',$subject_grade);
                        
        $grade = $db->fetchRow($select_grade);
		   		   
		   
        if(!$grade){		  
                    
            //check setup yang base on effective semester, program
            $select_grade2 = $db->select()
                                 ->from(array('gsm'=>'tbl_gradesetup_main'))
                                 ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
                                 ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
                                 ->join(array('sm' => 'tbl_semestermaster'),'gsm.IdSemester = sm.IdSemesterMaster',array('*'))
                                 ->where('sm.SemesterMainStartDate <= NOW()')
                                 ->where('gsm.BasedOn = 2 ') //program		
                                 ->where('gsm.IdProgram = ?',$program_id)
                                 ->where('d.DefinitionCode = ?',$subject_grade);
                                  
            $grade2 = $db->fetchRow($select_grade2);				
            
            if(!$grade2){
                
                    
                    //get progrm award
                    $programDB = new GeneralSetup_Model_DbTable_Program();
                    $program = $programDB->fngetProgramData($program_id);
                
                     //check setup yang base on effective semester, award
                     $select_grade3 = $db->select()
                                                  ->from(array('gsm'=>'tbl_gradesetup_main'))
                                                  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
                                                  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
                                                  ->join(array('sm' => 'tbl_semestermaster'),'gsm.IdSemester = sm.IdSemesterMaster',array('*'))
                                                  ->where('sm.SemesterMainStartDate <= NOW()')	
                                                  ->where('gsm.BasedOn = 0 ') //award	
                                                  ->where('gsm.IdAward = ?',$program['Award'])
                                                  ->where('d.DefinitionCode = ?',$subject_grade);
                                                 
                        
                    $grade3 = $db->fetchRow($select_grade3);
                    $grade = $grade3;
                    
        
            }else{
                $grade = $grade2;
            }
               
        }
	       
	 	   //throw exception if grade never been setup
		   if(!$grade){
		   	throw new exception('There is no grade setup for subject id: '.$subject_id);
		   }
	       return $grade;
	}
        
    static function getGradePoint($program_id,$subject_id,$subject_grade){
		
		  $db = Zend_Db_Table::getDefaultAdapter();
		  
 
        //1st : check setup yang base on effective semester, program & subject
        $select_grade = $db->select()
                          ->from(array('gsm'=>'tbl_gradesetup_main'))
                          ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
                          ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
                          ->join(array('sm' => 'tbl_semestermaster'),'gsm.IdSemester = sm.IdSemesterMaster',array('*'))
                          ->where('sm.SemesterMainStartDate <= NOW()')	
                          ->where('gsm.BasedOn = 1 ') //program	& subject	
                          ->where('gsm.IdProgram = ?',$program_id)
                          ->where('gsm.IdSubject = ?',$subject_id)
                          ->where('d.DefinitionCode = ?',$subject_grade);
                        
        $grade = $db->fetchRow($select_grade);
		   		   
		   
        if(!$grade){		  
                    
            //check setup yang base on effective semester, program
            $select_grade2 = $db->select()
                                 ->from(array('gsm'=>'tbl_gradesetup_main'))
                                 ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
                                 ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
                                 ->join(array('sm' => 'tbl_semestermaster'),'gsm.IdSemester = sm.IdSemesterMaster',array('*'))
                                 ->where('sm.SemesterMainStartDate <= NOW()')
                                 ->where('gsm.BasedOn = 2 ') //program		
                                 ->where('gsm.IdProgram = ?',$program_id)
                                 ->where('d.DefinitionCode = ?',$subject_grade);
                                  
            $grade2 = $db->fetchRow($select_grade2);				
            
            if(!$grade2){
                
                    
                    //get progrm award
                    $programDB = new GeneralSetup_Model_DbTable_Program();
                    $program = $programDB->fngetProgramData($program_id);
                
                     //check setup yang base on effective semester, award
                     $select_grade3 = $db->select()
                                                  ->from(array('gsm'=>'tbl_gradesetup_main'))
                                                  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
                                                  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('GradeName'=>'DefinitionDesc'))
                                                  ->join(array('sm' => 'tbl_semestermaster'),'gsm.IdSemester = sm.IdSemesterMaster',array('*'))
                                                  ->where('sm.SemesterMainStartDate <= NOW()')	
                                                  ->where('gsm.BasedOn = 0 ') //award	
                                                  ->where('gsm.IdAward = ?',$program['Award'])
                                                  ->where('d.DefinitionCode = ?',$subject_grade);
                                                 
                        
                    $grade3 = $db->fetchRow($select_grade3);
                    $grade = $grade3;
                    
        
            }else{
                $grade = $grade2;
            }
               
        }
	       
        //throw exception if grade never been setup
        if(!$grade){
            return '0.00';
        }else{
            return $grade['GradePoint'];
        }
    }
    
    private function calculateSubjectPoint(array $data,$program_id) {
    
        $total_point = 0;
        $total_credit = 0;
        
      
        foreach ($data as $key => $value) {
           
            foreach ($value as $index => $val)
            {
                //var_dump($val); //exit;
                if (isset($val['grade']) && $val['grade'] != null){
                    $grade = $val['grade'];
                    $val["exam_status"] = $val['grade'];
                }else{
                    $grade = $val['grade_name'];
                }
                //var_dump($val);
                if ($val["exam_status"]!="EX" && $val["exam_status"]!="CT" && $val["exam_status"]!="U" && $val["exam_status"]!="IP" && $val["exam_status"]!=""){
                
                    $grade = $this->getGrade($program_id,$val['IdSubject'],$grade);

                    $current_point = $grade['GradePoint'] * $val['CreditHours'];

                    $total_point  = $total_point + $current_point;
                    $total_credit = $total_credit + $val['CreditHours'];
                }
            }
        }
        if ($total_credit == 0){
            return $gpa = 0;
        }else{
            return $gpa = $total_point / $total_credit;
        }
        //return true;
    
    }
    
    public function calculateGPA($program_id,array $data) {
       
        foreach ($data as $key => $value) 
        {
            //var_dump($value);
            $gpa[$key]['gpa'] = $this->calculateSubjectPoint($value,$program_id);
        }
        //exit;
        return $gpa;
    }
    
    public function countCourseSemester($student_id) {
        $db = $this->lobjDbAdpt;
        
        $sql = $db->select()
                ->from(array('a' => $this->_name),array('Total' =>'COUNT(a.semester)','a.semester'))
                ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject', array('Totalc'=>'SUM(b.CreditHours)'))
                ->where('a.registration_id = ?',(int)$student_id)
                ->group('a.semester')
                ->order('a.semester ASC');
         //echo $sql;      
        $result = $db->fetchAll($sql);
        
        return $result;
    }
    
    public function calculateCGPA($program_id, $registration_id) {
        
        $db  = $this->lobjDbAdpt;
        $sql = $db->select()
                ->from(array('a' => $this->_name), array('value'=>'a.*', 'grade'=>'MIN(a.grade)', 'a.IdSubject', 'a.semester', 'a.registration_id'))
                ->joinLeft(array('b'=> 'tbl_subjectmaster'), 'a.IdSubject=b.IdSubject')
                ->where('a.registration_id = ?', (int)$registration_id)
                ->group('a.IdSubject');
        
        $result = $db->fetchAll($sql);
        
        $total  = $this->calculateCgpaPoint($program_id,$result);
    
        return $total;
    }
    
    private function calculateCgpaPoint($program_id, $result) {
    
        $total_point = 0;
        $total_credit = 0;
        
        //var_dump($result);
        foreach ($result as $index => $val)
        {
            if (isset($val['grade']) && $val['grade'] != null){
                $grade = $val['grade'];
                $val["exam_status"] = $val['grade'];
            }else{
                $grade = $val['grade_name'];
            }
            //var_dump($val);
            if ($val["exam_status"]!="EX" && $val["exam_status"]!="CT" && $val["exam_status"]!="U"){
            
                //var_dump($val);
                $grade = $this->getGrade($program_id,$val['IdSubject'],$val['grade']);
                //var_dump($grade);
                $current_point = $grade['GradePoint'] * $val['CreditHours'];

                $total_point  = $total_point + $current_point;
                $total_credit = $total_credit + $val['CreditHours'];
            }
        }
       
        if ($total_credit == 0){
            $cgpa = 0;
        }else{
            $cgpa = $total_point / $total_credit;
        }
        $credit_earn = $total_credit;
        
        $total = array('cgpa' => $cgpa, 'credit_earn' => $credit_earn);
        
        return $total;
        //return true;
    }
    
    public function calculateCGPA2($program_id,array $data){
        $total_point = 0;
        $total_credit = 0;
        
        foreach ($data as $value){
            unset($value['gpa']);
            foreach ($value as $value2){
                foreach ($value2 as $val){
                    //var_dump($val); //exit;
                    if (isset($val['grade']) && $val['grade'] != null){
                        $grade = $val['grade'];
                        $val["exam_status"] = $val['grade'];
                    }else{
                        $grade = $val['grade_name'];
                    }
                    //var_dump($val);
                    if ($val["exam_status"]!="EX" && $val["exam_status"]!="CT" && $val["exam_status"]!="U" && $val["exam_status"]!="IP" && $val["exam_status"]!=""){

                        $grade = $this->getGrade($program_id,$val['IdSubject'],$grade);

                        $current_point = $grade['GradePoint'] * $val['CreditHours'];

                        $total_point  = $total_point + $current_point;
                        $total_credit = $total_credit + $val['CreditHours'];
                    }
                }
            }
        }
        
        if ($total_credit == 0){
            $cgpa = 0;
        }else{
            $cgpa = $total_point / $total_credit;
        }
        $credit_earn = $total_credit;
        
        $total = array('cgpa' => $cgpa, 'credit_earn' => $credit_earn);
        
        return $total;
    }
    
    public function getRepeatSub($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectmaster'), array('value'=>'*'))
            ->where('a.IdSubject = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getListBySem($student_id, $semesterId) {
        
        $db = $this->lobjDbAdpt;
        
        $sql = $db->select()
                ->from(array('a' => $this->_name),array('a.*'))
                ->join(array('b' => 'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.*'))
                ->where('a.registration_id = ?',(int)$student_id)
                ->where('a.semester = ?', $semesterId);
       
        $result = $db->fetchAll($sql);
        return $result;
    }
    
    public function checkCourseDuplicate($student_id, $semesterId, $subjectId){
        $db = $this->lobjDbAdpt;
        
        $sql = $db->select()
                ->from(array('a' => $this->_name),array('a.*'))
                ->join(array('b' => 'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.*'))
                ->where('a.registration_id = ?',(int)$student_id)
                ->where('a.semester = ?', $semesterId)
                ->where('a.IdSubject = ?', $subjectId);
       
        $result = $db->fetchRow($sql);
        return $result;
    }
}

?>
