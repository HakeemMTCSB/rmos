<?php

class Records_Model_DbTable_Changestatusapplication extends Zend_Db_Table { //Model Class for Credit transfer subjects

    protected $_name = 'tbl_change_status_application';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function getListCSApplication(){
        $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_change_status_application'),array('a.IdChangeStatusApplication'));
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
    }

    public function saveCSApplication($data){
        $lstrTable = "tbl_change_status_application";
        $this->lobjDbAdpt->insert($lstrTable,$data);
        return $this->lobjDbAdpt->lastInsertId();
    }

    public function getAllCSApplication(){
        $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_change_status_application'),array('a.IdChangeStatusApplication','a.CSApplicationCode'))               
               ->joinLeft(array('b' => 'tbl_studentregistration'),"a.IdStudentRegistration = b.IdStudentRegistration",array('b.registrationId'))
               ->join(array('sp' => 'student_profile'),"sp.id=b.sp_id",array('appl_fname','appl_lname'))
               ->joinLeft(array('c' => 'tbl_definationms'),"a.ApplicationStatus = c.idDefinition",array('c.DefinitionDesc'));
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
    }

    public function fnSearchCSApplication($post=null){        
        $lstrSelect = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_change_status_application'))
               ->joinLeft(array('b' => 'tbl_studentregistration'),"a.IdStudentRegistration = b.IdStudentRegistration",array('registrationId'))
               ->joinLeft(array('c' => 'tbl_definationms'),"a.ApplicationStatus = c.idDefinition",array('c.DefinitionDesc'))
               ->joinLeft(array('d' => 'tbl_definationms'),"a.IdApplyingFor = d.idDefinition",array('ApplicationType'=>'DefinitionDesc'))
               ->join(array('sp' => 'student_profile'),"sp.id=b.sp_id",array('appl_fname','appl_lname'))
               ->join(array('p'=>'tbl_program'),'p.IdProgram=b.IdProgram',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','nama_program'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
               ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=a.EffectiveSemesterFrom',array('SemesterMainName','SemesterMainCode'))
               ->order('a.UpdDate DESC');

       
        if (isset($post['student_name']) && !empty($post['student_name'])) {
            $wh_condition = "sp.appl_fname like '%".$post['student_name']."%' OR  sp.appl_lname like '%".$post['student_name']."%' ";
            $lstrSelect = $lstrSelect->where($wh_condition);
        }
        
        if (isset($post['student_id']) && !empty($post['student_id'])) {            
            $lstrSelect = $lstrSelect->where("b.registrationId LIKE '%".$post['student_id']."%'");
        }

        if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {
            $lstrSelect = $lstrSelect->where('b.IdProgram =?', $post['IdProgram']);
        }
        
   		if (isset($post['IdProgramScheme']) && !empty($post['IdProgramScheme'])) {
            $lstrSelect = $lstrSelect->where('b.IdProgramScheme =?', $post['IdProgramScheme']);
        }

    	if (isset($post['student_category']) && !empty($post['student_category'])) {
            $lstrSelect = $lstrSelect->where('ap.appl_category =?', $post['student_category']);
        }
        
        if (isset($post['field6']) && !empty($post['field6'])) {
            $lstrSelect = $lstrSelect->where('a.CSApplicationCode like "%" ? "%"', $post['field6']);
        }

        if (isset($post['field5']) && !empty($post['field5'])) {
            $lstrSelect = $lstrSelect->where('b.profileStatus =?', $post['field5']);
        }

        if (isset($post['status']) && !empty($post['status'])) {
            $lstrSelect = $lstrSelect->where('a.ApplicationStatus =?', $post['status']);
        }

        //echo $lstrSelect;
        $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $result;
    }

    function fnGetApplicationDetail($lintIdApplication){
         $lstrSelect = $this->lobjDbAdpt->select()
                ->from(array('a' => 'tbl_change_status_application'))
                ->joinLeft(array('sr' => 'tbl_studentregistration'),"a.IdStudentRegistration = sr.IdStudentRegistration",array('registrationId','IdProgram','IdIntake','IdStudentRegistration','IdBranch','IdProgramScheme', 'profileStatus'))
                ->joinLeft(array('b'=>'student_profile'),'sr.sp_id = b.id')
                ->joinLeft(array('c'=>'tbl_definationms'), 'b.appl_category = c.idDefinition', array('StudentCategory'=>'c.DefinitionDesc','IdStudentCategory' => 'c.idDefinition'))
                ->joinLeft(array('d'=>'tbl_program_scheme'), 'sr.IdProgramScheme = d.IdProgramScheme')
                ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
                ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
                ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','IdProgramType' => 'd.program_type'))
                ->joinLeft(array('h'=>'tbl_program'), 'sr.IdProgram = h.IdProgram')
                ->joinLeft(array('j'=>'tbl_intake'), 'sr.IdIntake = j.IdIntake', array('IntakeName'=>'j.IntakeDesc'))
                ->joinLeft(array('i'=>'tbl_definationms'), 'sr.profileStatus = i.idDefinition', array('ProfileStatus'=>'i.DefinitionDesc'))    
                ->joinLeft(array('k' => 'tbl_definationms'),"a.ApplicationStatus = k.idDefinition",array('DefinitionDesc as appstatus'))
                ->joinLeft(array('l' => 'tbl_definationms'),"a.IdApplyingFor = l.idDefinition",array('DefinitionDesc as applicationtype'))      
                ->joinLeft(array('m' => 'tbl_user'),"a.AppliedBy = m.iduser",array('loginName'))   
                ->joinLeft(array('n' => 'tbl_user'),"a.ApprovedBy = n.iduser")
                ->joinLeft(array('o'=>'tbl_staffmaster'), 'n.IdStaff = o.IdStaff', array('ApprovedBy'=>'o.FullName'))
                ->joinLeft(array('p'=>'tbl_staffmaster'), 'm.IdStaff = p.IdStaff', array('loginName'=>'p.FullName'))
                ->where("a.IdChangeStatusApplication =?",$lintIdApplication);
               
        $result = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $result;
    }


    function fnUpdateApplicationstatus($id,$statusdata){
        $where = 'IdChangeStatusApplication = '.$id;
        $this->update($statusdata,$where);
    }
    
    public function deleteMainSemester($Idstudent,$idmainsem){
        $tbl = 'tbl_studentsemesterstatus';
        $where = 'IdStudentRegistration = '.$Idstudent.' AND IdSemesterMain='.$idmainsem;
        $this->lobjDbAdpt->delete($tbl,$where);
    }

    public function deleteSemester($Idstudent,$idsem){
        $tbl = 'tbl_studentsemesterstatus';
        $where = 'IdStudentRegistration = '.$Idstudent.' AND IdSemester='.$idsem;
        $this->lobjDbAdpt->delete($tbl,$where);
    }

    public function checkeffectivesemester($idstudent,$ideffectivesem){
        $lstrSelect = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_change_status_application'),array('a.IdChangeStatusApplication'))
               ->where("a.IdStudentRegistration =?",$idstudent)
               ->where("a.IdEffectiveSemester =?",$ideffectivesem)
               ->where("a.ApplicationStatus =?",193)
               ->orwhere("a.ApplicationStatus =?",243);
        $result = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $result;

    }


 	public function getStudent($data=null) {
 			
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array('sr' => 'tbl_studentregistration'))                        
                        ->join(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('appl_fname','appl_lname','appl_idnumber'))
                        ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ArabicName','ProgramName','IdScheme'))
                        ->join(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('IntakeDesc','IntakeDefaultLanguage'))  
                        ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sr.IdProgramScheme')
                        ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
                        ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
                        ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
                        ->joinLeft(array('i'=>'tbl_definationms'), 'sr.profileStatus = i.idDefinition', array('ProfileStatus'=>'i.DefinitionDesc'))
                         ->joinLeft(array('c'=>'tbl_definationms'), 'sp.appl_category = c.idDefinition', array('StudentCategory'=>'c.DefinitionDesc','IdStudentCategory' => 'c.idDefinition'))
                        ->where("sr.profileStatus = 92")                        
                        ->order("sp.appl_fname ASC");

 	 	if (isset($data['searchType']) && $data['searchType'] != '') {
 	 		
 	 		if($data['searchType']==1){
            	$wh_condition = "sp.appl_fname like '%" . $data['searchElement'] . "%' OR  sp.appl_mname like '%" . $data['searchElement'] . "%' OR  sp.appl_lname like '%" . $data['searchElement'] . "%' ";
            	$lstrSelect = $lstrSelect->where($wh_condition);
 	 		}else{
 	 			 $lstrSelect = $lstrSelect->where('sr.registrationId like "%" ? "%"', $data['searchElement']);
 	 		}
        }
                        
        /*if (isset($data['Name'])  && $data['Name'] != '') {
            $wh_condition = "sp.appl_fname like '%" . $data['Name'] . "%' OR  sp.appl_mname like '%" . $data['Name'] . "%' OR  sp.appl_lname like '%" . $data['Name'] . "%' ";
            $lstrSelect = $lstrSelect->where($wh_condition);
        }

        if (isset($data['StudentId']) && $data['StudentId'] != '') {
            $lstrSelect = $lstrSelect->where('a.registrationId like "%" ? "%"', $data['StudentId']);
        }
      
        if (isset($data['IdNric']) && $data['IdNric'] != '') {
            $lstrSelect = $lstrSelect->where('sp.appl_idnumber like "%" ? "%"', $data['IdNric']);
        }*/
        $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
        
 		foreach($result as $index=>$row){
 			//get current semester for each student based on scheme
 			$curdate = date('Y-m-d');
			
 			try{
 				
				$sql = $this->lobjDbAdpt->select()	
							->from(array('sm'=>'tbl_semestermaster'),array('IdSemesterMaster'))	
							->where('sm.IdScheme = ?',$row['IdProgramScheme'])						
							->where('sm.SemesterMainStartDate <= ?',$curdate)
							->where('sm.SemesterMainEndDate >= ?',$curdate);
				$sem = $this->lobjDbAdpt->fetchRow($sql);
			
				if($sem){
		        	//check if already apply at current semester     	
		 			$select = $this->lobjDbAdpt->select()
			 					   ->from(array('cs'=>'tbl_change_status_application'),array('IdStudentRegistration'))
			 					   ->where('cs.IdStudentRegistration = ?',$row['IdStudentRegistration'])
			 					   ->where('cs.EffectiveSemesterFrom = ?',$sem['IdSemesterMaster']); 
			 		$change_status = $this->lobjDbAdpt->fetchRow($select);	
			 		
			 		if($change_status){
			 			//$result[$index]['IdApplyingFor']=$change_status['IdApplyingFor'];
			 			unset($result[$index]);
			 		}
				}else{
					throw new Exception('Current semester info is not available');
				}
		 		
			}catch (Exception $e) {
		    	//echo 'Caught exception: ',  $e->getMessage(), "\n";
		    	$result[$index]['exception'] = $e->getMessage();
			} 	
        }
        
        return $result;
    }


    public function copyAuditTrail($IdChangeStatusApplication,$status){
    	
    		$db = Zend_Db_Table::getDefaultAdapter();
    		
    		$auth = Zend_Auth::getInstance();
    	
    		$sql = 'SELECT * FROM tbl_change_status_application WHERE IdChangeStatusApplication ="'.$IdChangeStatusApplication.'"';						
			$result = $db->fetchRow($sql);
			
			 $result['approval_status'] = $status;
			 $result['createddt'] = date('Y-m-d H:i:s');
             $result['createdby'] = $auth->getIdentity()->iduser;
              
			$db->insert('tbl_change_status_audit_trail',$result);
			
			
    }
    
 	public function isDuplicateCode($code){
        $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_change_status_application'),array('a.IdChangeStatusApplication'))->where('a.CSApplicationCode = ?',$code);
        $result = $this->lobjDbAdpt->fetchRow($sql);
        return $result;
    }
    
    public function getKmc($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_kmc'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.kmc_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdChangeStatusApplication = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getClearanceFinance($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_finance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.dcf_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdChangeStatusApplication = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getIct($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_ict'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.ict_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdChangeStatusApplication = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getLogistic($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'department_clearance_logistic'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_user'), 'a.log_createdby = b.iduser')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdStaff = c.IdStaff', array('name'=>'c.FullName'))
            ->where('a.IdChangeStatusApplication = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getDeferSlipTemplate($id = 162, $locale='en_US'){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'comm_template'), array('value'=>'*'))
            ->joinLeft(array('b'=>'comm_template_content'), 'a.tpl_id=b.tpl_id')
            ->where('a.tpl_id = ?', $id)
            ->where('locale = ?', $locale);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getTags($module = 'records'){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'comm_template_tags'), array('value'=>'*'))
            ->where('a.module = ?', $module);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemesterById($semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdSemesterMaster = ?', $semesterid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemesterByYearAndMonth($schemeid, $year, $month){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $schemeid)
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month);
        
        $result = $db->fetchRow($select);
        return $result;
    }

    public function uploadfile($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_change_status_application_upload', $data);
        //var_dump($data); exit;
        return true;
    }

    public function deleteUpl($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_change_status_application_upload', 'upl_id = '.$id);
        return $delete;
    }

    public function viewUpl($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('pm'=>'tbl_change_status_application_upload'))
            ->where("pm.upl_changestatus_id = ?", (int)$id);

        $row = $db->fetchAll($selectData);
        return $row;
    }
}

?>
