<?php
class Records_Model_DbTable_Studentprogramchange extends Zend_Db_Table { //Model Class for Credit transfer subjects
	
	private $lobjDbAdpt;
 		public function init() {
          $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        }
        
        public function fngetprogram($id){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_studentregistration'),array())
        				->joinLeft(array('b'=>'tbl_program'),'a.IdProgram = b.IdProgram',array("key"=>"b.IdProgram","value"=>"b.ProgramName"))
        				->where('a.IdStudentRegistration = ?',$id);
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;
        				
        }
        
        public function fngetstudentcurrentsem($id){
        	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a'=>'tbl_studentsemesterstatus'),array('a.*'))
							->joinLeft(array('e'=>'tbl_studentregistration'),'(e.IdStudentRegistration ="'.$id.'")',array('e.IdProgram'))
							->joinLeft(array('f'=>'tbl_program'),'e.IdProgram = f.IdProgram',array('f.IdScheme','f.Award'))
							->joinLeft(array('b'=>'tbl_semester'),'a.idSemester = b.IdSemester',array('b.SemesterCode'))
							->joinLeft(array('c'=>'tbl_semestermaster'),'a.IdSemesterMain = c.IdSemesterMaster',array('c.SemesterMainCode'))
							->joinLeft(array('d'=>'tbl_definationms'),'a.studentsemesterstatus = d.idDefinition',array('d.DefinitionDesc'))
							->order('a.idstudentsemsterstatus DESC')
							->limit('1')
							->where('a.IdStudentRegistration = ?',$id);
			$result = $db->fetchAll($sql);
			return $result;
        }
        
        public function fngetallprogram($IdProgram,$IdScheme,$Award){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_program'),array('key'=>'IdProgram','name'=>'ProgramName'))
        				->where('a.IdProgram != ?',$IdProgram)
        				->where('a.Award =?',$Award)
        				->where('a.IdScheme =?',$IdScheme);
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;
        }
        
        public function fngetstudentsemestersmain($Semester){
        	$db = Zend_Db_Table::getDefaultAdapter();
        	if($Semester['idSemester']!=''){
        		$sql = $db->select()
					  ->from(array('a'=>'tbl_semester'),array('a.SemesterEndDate'))
					  ->where('a.IdSemester = ?',$Semester['idSemester']);
			    $result = $db->fetchAll($sql);
			    $sql = $db->select()
					  ->from(array('a'=>'tbl_semestermaster'),array('key'=>'IdSemesterMaster','value'=>'SemesterMainCode'))
					  ->where('a.SemesterMainStartDate  > ?',$result[0]['SemesterEndDate'])
					  ->where('a.Scheme = ?',$Semester['IdScheme'])
                                          ->where("a.DummyStatus IS NULL")
					  ->group('a.SemesterMainCode');
				$semester = $db->fetchAll($sql);
				return $semester;
        	}
        	else if($Semester['IdSemesterMain']!=''){
        		$sql = $db->select()
					  ->from(array('a'=>'tbl_semestermaster'),array('a.SemesterMainEndDate'))
					  ->where('a.IdSemesterMaster = ?',$Semester['IdSemesterMain'])
                                          ->where("a.DummyStatus IS NULL");
			    $result = $db->fetchAll($sql);
			    $sql = $db->select()
					  ->from(array('a'=>'tbl_semestermaster'),array('key'=>'IdSemesterMaster','value'=>'SemesterMainCode'))
					  ->where('a.SemesterMainStartDate  > ?',$result[0]['SemesterMainEndDate'])
					  ->where('a.Scheme = ?',$Semester['IdScheme'])
                                          ->where("a.DummyStatus IS NULL")
					  ->group('a.SemesterMainCode');
				$semester = $db->fetchAll($sql);
				return $semester;
        	}
			
        }
        
        public function fngetstudentsemestersDetail($Semester){
        $db = Zend_Db_Table::getDefaultAdapter();
        	if($Semester['idSemester']!=''){
        		$sql = $db->select()
					  ->from(array('a'=>'tbl_semester'),array('a.SemesterEndDate'))
					  ->where('a.IdSemester = ?',$Semester['idSemester']);
			    $result = $db->fetchAll($sql);
			    $sql = $db->select()
					  ->from(array('a'=>'tbl_semester'),array('key'=>'IdSemester','value'=>'SemesterCode'))
					  ->join(array('b'=>'tbl_program'),'a.Program = b.IdProgram AND b.IdScheme = "'.$Semester['IdScheme'].'"',array())
					  ->where('a.SemesterStartDate  > ?',$result[0]['SemesterEndDate'])
					  ->group('a.SemesterCode');
				$semester = $db->fetchAll($sql);
				return $semester;
        	}
        	else if($Semester['IdSemesterMain']!=''){
        		$sql = $db->select()
					  ->from(array('a'=>'tbl_semestermaster'),array('a.SemesterMainEndDate'))
					  ->where('a.IdSemesterMaster = ?',$Semester['IdSemesterMain'])
                                          ->where("a.DummyStatus IS NULL");
			    $result = $db->fetchAll($sql);
			    $sql = $db->select()
					  ->from(array('a'=>'tbl_semester'),array('key'=>'IdSemester','value'=>'SemesterCode'))
					  ->join(array('b'=>'tbl_program'),'a.Program = b.IdProgram AND b.IdScheme = "'.$Semester['IdScheme'].'"',array())
					  ->where('a.SemesterStartDate  > ?',$result[0]['SemesterMainEndDate'])                                          
					  ->group('a.SemesterCode');
				$semester = $db->fetchAll($sql);
				return $semester;
        	}
        }
        
        public function fngetstudentprogramcourse($IdProgram,$StudentId){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_landscape'),array())
        				->joinLeft(array('b'=>'tbl_landscapesubject'),'a.IdLandscape = b.IdLandscape AND b.IdProgram = "'.$IdProgram.'"',array('b.IdSemester','b.CreditHours'))
        				->joinLeft(array('c'=>'tbl_subjectmaster'),'b.IdSubject = c.IdSubject',array('c.SubjectName','c.IdSubject'))
        				->joinLeft(array('d'=>'tbl_studentregsubjects'),'b.IdSubject = d.IdSubject AND d.IdStudentRegistration ="'.$StudentId.'"',array('d.IdSemesterMain','d.IdSemesterDetails'))
        				->joinLeft(array('e'=>'tbl_semester'),'e.IdSemester = d.IdSemesterDetails',array('e.SemesterCode'))
        				->joinLeft(array('f'=>'tbl_semestermaster'),'f.IdSemesterMaster = d.IdSemesterMain',array('f.SemesterMainCode'))
        				->joinLeft(array('g'=>'tbl_credittransfer'),'g.IdStudentRegistration = "'.$StudentId.'"',array())
        				->joinLeft(array('h'=>'tbl_credittransfersubjects'),'g.IdCreditTransfer = h.IdCreditTransfer',array('h.IdCreditTransfer'))
        				->joinLeft(array('i'=>'tbl_definationms'),'i.idDefinition = b.SubjectType',array('i.DefinitionDesc'))
        				->where('a.IdProgram = ?',$IdProgram)
        				->where('a.Active = 123');
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;
        }
        
        public function fngetstudentprogramcourseedit($IdProgram,$StudentId){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_landscape'),array())
        				->joinLeft(array('b'=>'tbl_landscapesubject'),'a.IdLandscape = b.IdLandscape AND b.IdProgram = "'.$IdProgram.'"',array('b.IdSemester','b.CreditHours'))
        				->joinLeft(array('c'=>'tbl_subjectmaster'),'b.IdSubject = c.IdSubject',array('c.SubjectName','c.IdSubject'))
        				->joinLeft(array('d'=>'tbl_studentregsubjects'),'b.IdSubject = d.IdSubject AND d.IdStudentRegistration ="'.$StudentId.'"',array('d.IdSemesterMain','d.IdSemesterDetails'))
        				->joinLeft(array('e'=>'tbl_semester'),'e.IdSemester = d.IdSemesterDetails',array('e.SemesterCode'))
        				->joinLeft(array('f'=>'tbl_semestermaster'),'f.IdSemesterMaster = d.IdSemesterMain',array('f.SemesterMainCode'))
        				->joinLeft(array('g'=>'tbl_credittransfer'),'g.IdStudentRegistration = "'.$StudentId.'"',array())
        				->joinLeft(array('h'=>'tbl_credittransfersubjects'),'g.IdCreditTransfer = h.IdCreditTransfer',array('h.IdCreditTransfer'))
        				->joinLeft(array('i'=>'tbl_definationms'),'i.idDefinition = b.SubjectType',array('i.DefinitionDesc'))
        				->joinLeft(array('j'=>'tbl_change_program_application'),'j.IdStudent="'.$StudentId.'"',array())
        				->joinLeft(array('k'=>'tbl_change_program_course_transferred'),'j.IdChangeProgramApplication=k.IdChangeProgram AND b.IdSubject=k.IdCourse',array())
        				->joinLeft(array('l'=>'tbl_definationms'),'l.idDefinition=k.Type',array('l.DefinitionDesc AS Type'))
        				->where('a.IdProgram = ?',$IdProgram)
        				->where('a.Active = 123');
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;
        }
        
        public function fngetstudentnewprogramcourse($NewProgram){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_landscape'),array('a.IdLandscape'))
        				->joinLeft(array('b'=>'tbl_landscapesubject'),'a.IdLandscape = b.IdLandscape AND b.IdProgram = "'.$NewProgram.'"',array('b.IdSemester','b.CreditHours'))
        				->joinLeft(array('c'=>'tbl_subjectmaster'),'b.IdSubject = c.IdSubject',array('c.SubjectName','c.IdSubject'))
        				->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = b.SubjectType',array('d.DefinitionDesc'))
        				->where('a.IdProgram = ?',$NewProgram)
        				->where('a.Active = 123');
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;				
        }
        
        public function fngetListCPApplication(){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_change_program_application'),array('a.IdChangeProgramApplication'));
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;	
        }
        
        public function fnSaveChangeprogram($data){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_change_program_application";
            $lobjDbAdpt->insert($lstrTable,$data);
            return $lobjDbAdpt->lastInsertId();
        }
        public function fnSaveChangeprogramSubjects($subjectdata){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_change_program_course_transferred";
            $lobjDbAdpt->insert($lstrTable,$subjectdata);
			return;
        }
        
        public function getDetailApplication($IdchangeProgramApp){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
                   ->from(array('a' => 'tbl_change_program_application'),array('a.*'))
                   ->joinLeft(array('d' => 'tbl_change_program_course_transferred'),"a.IdChangeProgramApplication = d.IdChangeProgram", array("d.*"))
                   ->joinLeft(array('dftnss' => 'tbl_definationms'),"dftnss.idDefinition = a.AppliedBy", array("dftnss.DefinitionDesc as username"))
                   ->joinLeft(array('dft' => 'tbl_definationms'),"dft.idDefinition = a.ApplicationStatus", array("dft.DefinitionDesc as DefinitionDesc"))
                   ->joinLeft(array('c' => 'tbl_studentregistration'),"a.IdStudent = c.IdStudentRegistration",array("c.registrationId","c.IdStudentRegistration","c.ExtraIdField1","c.FName","c.LName","c.MName"))
                   ->joinLeft(array('x' => 'tbl_intake'),"c.IdIntake = x.IdIntake",array('x.IntakeDesc'))
                   ->joinLeft(array('e' => 'tbl_program'),"c.IdProgram = e.IdProgram",array('e.ProgramName',"e.IdScheme"))
                   ->joinLeft(array('g' => 'tbl_program'),"a.NewProgram = g.IdProgram",array('g.ProgramName AS NewProgramName',"g.IdScheme"))
                   ->joinLeft(array('f' => 'tbl_scheme'),"e.IdScheme = f.IdScheme",array('f.EnglishDescription'))
                   ->joinLeft(array('y' => 'tbl_definationms'),"c.profileStatus = y.idDefinition",array('y.DefinitionDesc as profilestatus'))
                   ->where("a.IdChangeProgramApplication =?",$IdchangeProgramApp)
                   ->order("a.ApplicationDate");
            $result = $lobjDbAdpt->fetchAll($sql);
            return $result;
        }
        
        public function fngetstudentinfo($IdStudentreg){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_studentregistration'))        				
		                ->join(array('b'=>'student_profile'),'a.sp_id = b.id')
		                ->joinLeft(array('c'=>'tbl_definationms'), 'b.appl_category = c.idDefinition', array('StudentCategory'=>'c.DefinitionDesc','IdStudentCategory' => 'c.idDefinition'))
		                ->join(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
		                ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
		                ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
		                ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','IdProgramType' => 'd.program_type'))
		                ->join(array('h'=>'tbl_program'), 'd.IdProgram = h.IdProgram')
		                ->join(array('j'=>'tbl_intake'), 'a.IdIntake = j.IdIntake', array('IntakeName'=>'j.IntakeDesc'))
		                ->joinLeft(array('i'=>'tbl_definationms'), 'a.profileStatus = i.idDefinition', array('ProfileStatus'=>'i.DefinitionDesc'))
        				->where('a.IdStudentRegistration = ?',$IdStudentreg);
        	$result = $lobjDbAdpt->fetchRow($sql);
        	return $result;	
        }
        
        public function fngetstudentcredittransferapp($IdStudentRegistration){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_credittransfer'),array('a.IdCreditTransfer'))
        				->where('a.ApplicationStatus = ?',193)
        				->where('a.IdStudentRegistration = ?',$IdStudentRegistration);
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;	
        }
        
        public function fncreatenewstudent($studentinfo){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_studentregistration";
            $lobjDbAdpt->insert($lstrTable,$studentinfo);
			return $lobjDbAdpt->lastInsertId();
        }
        
        public function fnregistersemester($newstudentId,$data){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_studentsemesterstatus";
            $lobjDbAdpt->insert($lstrTable,$data);
			return;
        }
        
        public function fnregistersubjects($data){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_studentregsubjects";
            $lobjDbAdpt->insert($lstrTable,$data);
			return;
        }
        
        public function fnupdateapplication($approvedata,$IdchangeProgramApp){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_change_program_application";
        	$where ='IdChangeProgramApplication = '.$IdchangeProgramApp;
        	$lobjDbAdpt->update($lstrTable,$approvedata,$where);
			return;
        }
        
        public function fncreatesemester($dummysemData){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$lstrTable = "tbl_semestermaster";
            $lobjDbAdpt->insert($lstrTable,$dummysemData);
			return $lobjDbAdpt->lastInsertId();
        }
        
        public function fngetcredittransfercourse($IdChangeProgramApplication,$studentid){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_change_program_course_transferred'),array('a.*'))
        				->where('a.IdChangeProgram = ?',$IdChangeProgramApplication);
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;	
        }
        
        public function fngetchangeprogramconfig(){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$sql = $lobjDbAdpt->select()
        				->from(array('a'=>'tbl_record_config'),array('a.ApprovalChangeProgram'));
        	$result = $lobjDbAdpt->fetchAll($sql);
        	return $result;	
        }
        
		public function getProgramCurrentSemester($Idscheme,$idBranch){
			
			$curdate = date('Y-m-d');
			
        	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('sm'=>'tbl_semestermaster'))	
							->where('sm.IdScheme = ?',$Idscheme)	
							->where('sm.Branch = ?',$idBranch)						
							->where('sm.SemesterMainStartDate <= ?',$curdate)
							->where('sm.SemesterMainEndDate >= ?',$curdate)
							->order('sm.SemesterMainStartDate desc');
			$result = $db->fetchRow($sql);
			
			if(!$result){					
			$sql2 = $db->select()	
							->from(array('sm'=>'tbl_semestermaster'))	
							->where('sm.IdScheme = ?',$Idscheme)	
							->where('sm.Branch = ?',0)														
							->where('sm.SemesterMainStartDate <= ?',$curdate)
							->where('sm.SemesterMainEndDate >= ?',$curdate)							
							->order('sm.SemesterMainStartDate desc');
				$result = $db->fetchRow($sql2);
			}
			return $result;
        }
        
		public function getProgramNextSemester($idSemester,$idScheme){
			
			$curdate = date('Y-m-d');
			
        	$db = Zend_Db_Table::getDefaultAdapter();
        	
        	$sql1 = $db->select()	
						->from(array('sm'=>'tbl_semestermaster'))
						->where('sm.IdSemesterMaster = ?',$idSemester);
			$result1 = $db->fetchRow($sql1);
							
			$sql = $db->select()	
							->from(array('sm'=>'tbl_semestermaster'))	
							->where('sm.IdScheme = ?',$idScheme)
							->where('sm.SemesterMainStartDate >= ?',$result1['SemesterMainStartDate'])							
							->order('sm.SemesterMainStartDate asc')
							->limit(2,0);
			$result = $db->fetchAll($sql);
			return $result;
        }
        
       
 		public function getStudentSemesterStatus($id,$idSemester){
        	$db = Zend_Db_Table::getDefaultAdapter();
        	
			$sql = $db->select()	
							->from(array('a'=>'tbl_studentsemesterstatus'))
							->join(array('e'=>'tbl_definationms'), 'a.studentsemesterstatus= e.idDefinition', array('semester_status'=>'e.DefinitionDesc'))
							->where('a.IdStudentRegistration = ?',$id)
							->where('a.IdSemesterMain = ?',$idSemester);
			$result = $db->fetchRow($sql);
			return $result;
        }
        
        
		public function getProgramSemester($idSemester){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('sm'=>'tbl_semestermaster'))	
							->where('sm.IdSemesterMaster = ?',$idSemester);
			$result = $db->fetchRow($sql);
			return $result;
        }
}