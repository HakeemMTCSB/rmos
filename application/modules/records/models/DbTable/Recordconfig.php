<?php

class Records_Model_DbTable_Recordconfig extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_record_config';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnaddrecordconfiguation($data) {
        $this->insert($data);
        return $this->lobjDbAdpt->lastInsertId();
    }
    
	public function fnupdaterecordconfiguation($data,$IdRecordConfig) {
        $where = 'IdRecordConfig = '.$IdRecordConfig;
		$this->update($data,$where);
    }

    public function fnfetchConfiguration($IdUniversity) {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $this->lobjDbAdpt->select()
                        ->from(array("a" => "tbl_record_config"), array("a.*"))
                        ->where("a.IdUniversity = ?", $IdUniversity);
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

}

?>
