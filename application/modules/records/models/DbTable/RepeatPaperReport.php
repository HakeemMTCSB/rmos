<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20/10/2015
 * Time: 2:12 PM
 */
class Records_Model_DbTable_RepeatPaperReport extends Zend_Db_Table_Abstract {

    public function getStudent($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration=b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_program_scheme'), 'b.IdProgramScheme=e.IdProgramScheme')
            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
            ->where('a.Active = 4 OR a.Active = 9 OR a.replace_subject IS NOT NULL')
            ->group('a.IdStudentRegistration')
            ->order('a.IdStudentRegSubjects DESC');

        if ($search != false){
            if (isset($search['program']) && $search['program']!=''){
                $select->where('b.IdProgram = ?', $search['program']);
            }
            if (isset($search['programscheme']) && $search['programscheme']!=''){
                $select->where('b.IdProgramScheme = ?', $search['programscheme']);
            }
            if (isset($search['intake']) && $search['intake']!=''){
                $select->where('b.IdIntake = ?', $search['intake']);
            }
            if (isset($search['studentid']) && $search['studentid']!=''){
                $select->where('b.registrationId like "%'.$search['studentid'].'%"');
            }
            if (isset($search['studentname']) && $search['studentname']!=''){
                $select->where('concat(c.appl_fname, " ", c.appl_lname) like "%'.$search['studentname'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getRepeatSubject($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->where('a.Active = 4 OR a.Active = 9')
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->group('a.IdSubject');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getReplaceSubject($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->where('a.replace_subject IS NOT NULL')
            ->where('a.IdStudentRegistration = ?', $studentid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCountRepeatSubject($studentid, $subjectid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->where('a.IdSubject = ?', $subjectid)
            ->where('a.IdStudentRegistration = ?', $studentid);

        $result = $db->fetchAll($select);
        return $result;
    }
}