<?php

class Records_Model_DbTable_StudentPassport extends Zend_Db_Table_Abstract {

    protected $_name = 'student_passport';
    protected $_primary = 'p_id';
   
	public function getTotalStudentPassport($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('p' => $this->_name))               
                ->where('p.sp_id = ?',(int)$id);
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
	public function getDataById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('p' => $this->_name))               
                ->where('p.p_id = ?',(int)$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    
	public function getData($spId) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('p' => $this->_name)) 
                ->joinLeft(array('co' => 'tbl_countries'),'co.idCountry = p.p_country_issue', array('CountryName'))
                ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=p.p_type', array('type'=>'DefinitionDesc','typeMy'=>'BahasaIndonesia'))
                ->joinLeft(array("df" => "tbl_definationms"),'df.idDefinition=p.p_visa_type', array('visatype'=>'DefinitionDesc','visatypeMy'=>'BahasaIndonesia'))
                ->where('p.sp_id=?',$spId)
                ->order('p_active desc')
                ->order('p_expiry_date desc');  
                
             
        $result = $db->fetchAll($sql);        
        return $result;
    }
    
	public function addData($data) {
		$db = Zend_Db_Table::getDefaultAdapter();		
		$db->insert($this->_name, $data);
		return $id =  $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($where){
		 $this->delete( $where );
	}
	
	public function getVisaInfo($id){
		$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('v' => 'student_visa'))               
                ->where('v.sp_id = ?',(int)$id);
             
        $result = $db->fetchRow($sql);        
        return $result;
	}
	
	public function checkDuplicate($refno) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('p' => $this->_name))               
                ->where('p.p_passport_no = ?',$refno);
             
        $result = $db->fetchRow($sql);        
        return $result;
    } 
    
	public function getActivePassport($spid) {
        
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                ->from(array('p' => $this->_name))   
                ->where('p.sp_id =?',$spid)            
                ->where('p.p_active = 1');
             
        $result = $db->fetchRow($sql);        
        return $result;
    }
    
	
}

?>