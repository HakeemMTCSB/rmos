<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Records_Model_DbTable_Auditpaper extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_advisement';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }
    
    public function findStudent($searchElement, $searchType = 1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->join(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.profileStatus = c.idDefinition', array('StatusName'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_intake'), 'a.IdIntake = d.IdIntake', array('IntakeName'=>'IntakeDesc'))
            ->joinLeft(array('e'=>'tbl_branchofficevenue'), 'a.IdBranch = e.IdBranch', array('BranchName'=>'BranchName'))
            ->joinLeft(array('f'=>'tbl_program'), 'a.IdProgram = f.IdProgram', array('ProgramName'=>'f.ProgramName', 'schemeid'=>'f.IdScheme'))
            ->joinLeft(array('g'=>'tbl_program_scheme'), 'a.IdProgramScheme = g.IdProgramScheme')
            ->joinLeft(array('h'=>'tbl_definationms'), 'g.mode_of_program = h.idDefinition', array('mop'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'g.mode_of_study = i.idDefinition', array('mos'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'g.program_type = j.idDefinition', array('pt'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_city'), 'b.appl_city = k.idCity', array('CityName'=>'CityName'))
            ->joinLeft(array('l'=>'tbl_state'), 'b.appl_state = l.idState', array('StateName'=>'StateName'))
            ->joinLeft(array('m'=>'tbl_countries'), 'b.appl_country = m.idCountry', array('CountryName'=>'CountryName'))
            ->joinLeft(array('n'=>'tbl_definationms'), 'b.appl_idnumber_type = n.idDefinition', array('IdTypeName'=>'n.DefinitionDesc'))
            ->where('a.profileStatus = ?', 92);
            //->where('a.IdStudentRegistration NOT IN (SELECT tbl_auditpaperapplication.apa_studregid FROM tbl_auditpaperapplication WHERE apa_status = 823)');

        if ($searchType == 1){
            $select->where('a.registrationId like "%'.$searchElement.'%"');
        }else{
            $select->where('b.appl_fname like "%'.$searchElement.'%" OR b.appl_lname like "%'.$searchElement.'%"');
        }

        $select->order('a.registrationId');
        //echo $searchElement;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getUser($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_user'), array('value'=>'*'))
            ->where('iduser = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getDefination2($deftypeid, $code){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationtypems'), array('value'=>'*'))
            ->join(array('b'=>'tbl_definationms'), 'a.idDefType = b.idDefType')
            ->where('a.idDefType = ?', $deftypeid)
            ->where('b.DefinitionCode = ?', $code);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCourseInfo($subjectId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectmaster'), array('value'=>'*'))
            ->where('a.IdSubject = ?', $subjectId);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertApApp($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_auditpaperapplication', $bind);
        $id = $db->lastInsertId('tbl_auditpaperapplication', 'apa_id');
        return $id;
    }
    
    public function updateApApp($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_auditpaperapplication', $bind, 'apa_id = '.$id);
        return $update;
    }
    
    public function insertApAppCourse($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_auditpaperapplicationcourse', $bind);
        $id = $db->lastInsertId('tbl_auditpaperapplicationcourse', 'apc_id');
        return $id;
    }
    
    public function updateApAppCourse($bind, $apcId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_auditpaperapplicationcourse', $bind, 'apc_id = '.$apcId);
        //$id = $db->lastInsertId('tbl_auditpaperapplicationcourse', 'apc_id');
        return $update;
    }
    
    public function getAuditPaperApp($apaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplication'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.apa_studregid = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_definationms'), 'b.profileStatus = d.idDefinition', array('StatusName'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_intake'), 'b.IdIntake = e.IdIntake', array('IntakeName'=>'IntakeDesc'))
            ->joinLeft(array('f'=>'tbl_branchofficevenue'), 'b.IdBranch = f.IdBranch', array('BranchName'=>'BranchName'))
            ->joinLeft(array('g'=>'tbl_program'), 'b.IdProgram = g.IdProgram', array('ProgramName'=>'ProgramName'))
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'b.IdProgramScheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->joinLeft(array('l'=>'tbl_city'), 'c.appl_city = l.idCity', array('CityName'=>'CityName'))
            ->joinLeft(array('m'=>'tbl_state'), 'c.appl_state = m.idState', array('StateName'=>'StateName'))
            ->joinLeft(array('n'=>'tbl_countries'), 'c.appl_country = n.idCountry', array('CountryName'=>'CountryName'))
            ->joinLeft(array('o'=>'tbl_definationms'), 'a.apa_status = o.idDefinition', array('AppStatusName'=>'o.DefinitionDesc'))
            ->joinLeft(array('p'=>'tbl_semestermaster'), 'a.apa_semesterid = p.IdSemesterMaster', array('SemesterName'=>'p.SemesterMainName'))
            ->joinLeft(array('q'=>'tbl_user'), 'a.apa_upduser = q.iduser', array('UserName'=>'q.loginName'))
            ->joinLeft(array('r'=>'tbl_definationms'), 'a.apa_type = r.idDefinition', array('ApaTypeName'=>'r.DefinitionDesc'))
            ->joinLeft(array('s'=>'tbl_definationms'), 'c.appl_idnumber_type = s.idDefinition', array('IdTypeName'=>'s.DefinitionDesc'))
            ->joinLeft(array('t'=>'tbl_user'), 'a.apa_appliedby = t.iduser', array('appliedName'=>'t.loginName'))
            ->joinLeft(array('u'=>'tbl_user'), 'a.apa_approvedby = u.iduser', array('approvedName'=>'u.loginName'))
            ->where('a.apa_id = ?', $apaId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function auditPaperApplicationList($bind=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplication'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.apa_studregid = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->join(array('d'=>'tbl_definationms'), 'a.apa_status = d.idDefinition', array('StatusName'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_program'), 'b.IdProgram = e.IdProgram', array('ProgramName'=>'e.ProgramCode'))
            ->joinLeft(array('f'=>'tbl_semestermaster'), 'a.apa_semesterid = f.IdSemesterMaster', array('semesterName'=>'f.SemesterMainName'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'a.apa_type = g.idDefinition', array('apaTypeName'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_auditpaperapplicationcourse'), 'a.apa_id=h.apc_apaid')
            ->joinLeft(array('i'=>'tbl_subjectmaster'), 'h.apc_courseid=i.IdSubject', array('subject'=>'concat(i.SubCode, " - ", i.SubjectName)'))
            ->order('a.apa_id DESC');
        
        if ($bind!=null){
            if (isset($bind['Program']) && $bind['Program']!=''){
                $select->where($db->quoteInto('b.IdProgram = ?', $bind['Program']));
            }
            if (isset($bind['ProgramScheme']) && $bind['ProgramScheme']!=''){
                $select->where($db->quoteInto('b.IdProgramScheme = ?', $bind['ProgramScheme']));
            }
            if (isset($bind['Semester']) && $bind['Semester']!=''){
                $select->where($db->quoteInto('a.apa_semesterid = ?', $bind['Semester']));
            }
            if (isset($bind['IdNo']) && $bind['IdNo']!=''){
                $select->where($db->quoteInto('c.appl_idnumber like ?', '%'.$bind['IdNo'].'%'));
            }
            if (isset($bind['StudentId']) && $bind['StudentId']!=''){
                $select->where($db->quoteInto('b.registrationId like ?', '%'.$bind['StudentId'].'%'));
            }
            if (isset($bind['StudentName']) && $bind['StudentName']!=''){
                $select->where($db->quoteInto('CONCAT_WS(" ",c.appl_fname, c.appl_lname) like ?', '%'.$bind['StudentName'].'%'));
            }
            if (isset($bind['ApplicationID']) && $bind['ApplicationID']!=''){
                $select->where($db->quoteInto('a.apa_appid like ?', '%'.$bind['ApplicationID'].'%'));
            }
            if (isset($bind['StudentCategory']) && $bind['StudentCategory']!=''){
                $select->where($db->quoteInto('c.appl_category = ?', $bind['StudentCategory']));
            }
            if (isset($bind['AppStatus']) && $bind['AppStatus']!=''){
                $select->where($db->quoteInto('a.apa_status = ?', $bind['AppStatus']));
            }
            if (isset($bind['AppType']) && $bind['AppType']!=''){
                $select->where($db->quoteInto('a.apa_type = ?', $bind['AppType']));
            }
            if (isset($bind['course']) && $bind['course']!=''){
                $select->where($db->quoteInto('h.apc_courseid = ?', $bind['course']));
            }
            if (isset($bind['DateFrom']) && $bind['DateFrom']!='' && isset($bind['DateTo']) && $bind['DateTo']!=''){
                $select->where('a.apa_applieddate BETWEEN "'.date('Y-m-d', strtotime($bind['DateFrom'])).'" AND "'.date('Y-m-d', strtotime($bind['DateTo'])).'"');
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getAuditAppCourse($apaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplicationcourse'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.apc_courseid = b.IdSubject', array('CourseName'=>'b.SubjectName', 'CourseCode'=>'b.Subcode'))
            ->joinLeft(array('c'=>'tbl_course_tagging_group'), 'a.apc_coursesection = c.IdCourseTaggingGroup', array('CourseSectionName'=>'c.GroupName'))
            ->joinLeft(array('d'=>'tbl_countries'), 'a.apc_country = d.idCountry', array('Country'=>'d.CountryName'))
            ->joinLeft(array('e'=>'tbl_city'), 'a.apc_city = e.idCity', array('City'=>'e.CityName'))
            ->joinLeft(array('f'=>'tbl_program'), 'a.apc_program = f.IdProgram')
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'a.apc_programscheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->where('a.apc_apaid = ?', $apaId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getAuditAppCourse2($apaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplicationcourse'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.apc_courseid = b.IdSubject', array('CourseName'=>'b.SubjectName', 'CourseCode'=>'b.Subcode'))
            ->joinLeft(array('c'=>'tbl_course_tagging_group'), 'a.apc_coursesection = c.IdCourseTaggingGroup', array('CourseSectionName'=>'c.GroupName'))
            ->joinLeft(array('d'=>'tbl_countries'), 'a.apc_country = d.idCountry', array('Country'=>'d.CountryName'))
            ->joinLeft(array('e'=>'tbl_city'), 'a.apc_city = e.idCity', array('City'=>'e.CityName'))
            ->where('a.apc_apaid = ?', $apaId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkSubReg($regId, $idSubject, $semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $regId)
            ->where('a.IdSubject = ?', $idSubject)
            ->where('a.IdSemesterMain = ?', $semId)
            ->where('a.Active <> ?', 3);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function deleteCourse($apcId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_auditpaperapplicationcourse', 'apc_id = '.$apcId);
        return $delete;
    }
    
    public function storeHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_auditpaperhistory', $bind);
        
        $id = $db->lastInsertId('tbl_auditpaperhistory', 'aph_id');
        return $id;
    }
    
    public function updateAppStatus($bind, $apaId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_auditpaperapplication', $bind, 'apa_id = '.$apaId);
        return $update;
    }
    
    public function getSequence($type, $year){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sequence_id'))
            ->where('a.tsi_type = ?', $type)
            ->where('a.tsi_seq_year = ?', $year);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertSequence($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_sequence_id', $bind);
        
        $id = $db->lastInsertId('tbl_sequence_id', 'tsi_id');
        return $id;
    }
    
    public function getConfig($idUniversity=1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('c'=>'tbl_config'))
            ->where("idUniversity = ?",$idUniversity);
        $row = $db->fetchRow($select);
        return $row;		
    }
    
    public function updateSequence($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_sequence_id', $bind, 'tsi_id = '.$id);
        return $update;
    }
    
    public function checkSemStatus($IdReg, $IdSem){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $IdReg)
            ->where('a.idSemester = ?', $IdSem);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertSemStatus($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentsemesterstatus', $bind);
        $id = $db->lastInsertId('tbl_studentsemesterstatus', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function insertSemStatusHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentsemesterstatus_history', $bind);
        $id = $db->lastInsertId('tbl_studentsemesterstatus_history', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function deleteSemStatus($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentsemesterstatus', 'idstudentsemsterstatus = '.$id);
        return $delete;
    }
    
    public function getSemStatusBasedRegId($IdReg){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $IdReg);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function registerAuditPaper($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregsubjects', $bind);
        $id = $db->lastInsertId('tbl_studentregsubjects', 'IdStudentRegSubjects');
        return $id;
    }
    
    public function historyRegisterAuditPaper($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregsubjects_history', $bind);
        $id = $db->lastInsertId('tbl_studentregsubjects_history', 'id');
        return $id;
    }
    
    public function deleteRegAuditPaper($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentregsubjects', 'IdStudentRegSubjects = '.$id);
        return $delete;
    }
    
    public function deleteRegAuditPaperHistory($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentregsubjects_history', 'IdStudentRegSubjects = '.$id);
        return $delete;
    }
    
    public function updateSubject($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_auditpaperapplicationcourse', $bind, 'apc_id = '.$id);
        return $update;
    }
    
    public function getCurSem($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('"'.date('Y-m-d').'" BETWEEN a.SemesterMainStartDate AND a.SemesterMainEndDate');
        //echo $select;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramForSem($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $programId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemBasedOnList($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'));
                
        if ($idScheme!=null){
            $select->where('a.IdScheme = ?', $idScheme);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getlandscape($programId, $programSchemeId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscape'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $programId)
            //->where('a.IdStartSemester = ?', $intakeId)
            ->where('a.IdProgramScheme = ?', $programSchemeId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getBlockSubject($landscapeId){
        $date = date('Y-m-d');
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscapeblocksubject'), array('key'=>'IdLandscapeblocksubject' ,'value'=>'*'))
            ->join(array('b'=>'tbl_subjectmaster'), 'a.subjectid = b.IdSubject')
            ->where('a.IdLandscape = ?', $landscapeId)
            ->where('b.audit = ?', 1)
            ->where('a.subjectid IN (SELECT c.IdSubject FROM tbl_subjectsoffered as c WHERE c.IdSemester IN (SELECT d.IdSemesterMaster FROM tbl_semestermaster as d WHERE d.SemesterMainStartDate <= "'.$date.'" AND d.SemesterMainEndDate >= "'.$date.'"))')
            ->order('b.SubjectName');
        //echo $select.'<br/>';
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSubject($landscapeId){
        $date = date('Y-m-d');
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscapesubject'), array('key'=>'IdLandscapeSub' ,'value'=>'*'))
            ->join(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->where('a.IdLandscape = ?', $landscapeId)
            ->where('b.audit = ?', 1)
            ->where('a.IdSubject IN (SELECT c.IdSubject FROM tbl_subjectsoffered as c WHERE c.IdSemester IN (SELECT d.IdSemesterMaster FROM tbl_semestermaster as d WHERE d.SemesterMainStartDate <= "'.$date.'" AND d.SemesterMainEndDate >= "'.$date.'"))')
            ->order('b.SubjectName');
        //echo $select.'<br/>';
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSubjectList($arr){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectmaster'), array('value'=>'*'))
            ->where('a.IdSubject IN (?)', $arr)
            ->group('a.IdSubject');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramSchemeBasedOnProgram($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop_name'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos_name'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('programType'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $programId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id=b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram=c.IdProgram')
            ->where('a.IdStudentRegistration = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getGroupQuota($idSubject, $idProgram, $idScheme, $noscheme=1){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();	
        $select = $db ->select()
            ->from(array('cg'=>'tbl_course_tagging_group'))
            ->join(array('cgp'=>'course_group_program'),'cg.`IdCourseTaggingGroup` = cgp.group_id')
            ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=cgp.program_scheme_id')
            ->join(array('def'=>'tbl_definationms'),'def.idDefinition=ps.mode_of_study',array('progmode'=>'DefinitionDesc'))
            ->where('cg.IdSubject = ?',$idSubject)
            ->where('cg.IdSemester IN (SELECT a.IdSemesterMaster FROM tbl_semestermaster as a WHERE a.SemesterMainStartDate <= "'.$date.'" AND a.SemesterMainEndDate >= "'.$date.'")')
            ->where('cgp.program_id = ?',$idProgram)
            ->group('cg.IdCourseTaggingGroup');

        if($noscheme==1){
            $select = $select->where('cgp.program_scheme_id = ?',$idScheme);
        }

        $rows = $db->fetchAll($select);	

        foreach($rows as $index=>$row){
            //get total student tagged to this group
            $select2 = $db ->select()
                ->from(array('srs'=>'tbl_studentregsubjects'),array('total'=>'count(IdCourseTaggingGroup)'))
                ->where('srs.IdCourseTaggingGroup = ?',$row['IdCourseTaggingGroup']);
            
            $result = $db->fetchRow($select2);
            
            if($result['total']<$row['maxstud']){
                $rows[$index]['quota']=true;
            }else{
                $rows[$index]['quota']=false;
            }
        }
        
        return $rows;
    }
    
    public function country(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_countries'), array('key'=>'a.idCountry', 'name'=>'a.CountryName'))
            ->order('a.CountryName');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function city($countryid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_city'), array('key'=>'a.idCity', 'name'=>'a.CityName'))
            ->where('a.idState IN (SELECT b.idState FROM tbl_state as b WHERE b.idCountry = '.$countryid.')')
            ->order('a.CityName')
            ->group('a.idCity');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertExamCenter($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('exam_registration', $bind);
        $id = $db->lastInsertId('exam_registration', 'er_id');
        return $id;
    }
    
    public function deleteExamCenter($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('exam_registration', 'er_id = '.$id);
        return $delete;
    }
    
    public function getItemRegistration($programid, $programschemeid){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_registration_item'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.item_id = b.idDefinition', array('itemName'=>'b.DefinitionDesc'))
            ->where('a.semester_id IN (SELECT a.IdSemesterMaster FROM tbl_semestermaster as a WHERE a.SemesterMainStartDate <= "'.$date.'" AND a.SemesterMainEndDate >= "'.$date.'")')
            ->where('a.program_id = ?', $programid)
            ->where('a.program_scheme_id = ?', $programschemeid);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertItemReg($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregsubjects_detail', $bind);
        $id = $db->lastInsertId('tbl_studentregsubjects_detail', 'id');
        return $id;
    }
    
    public function insertItemRegHis($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregsubjects_detail_history', $bind);
        $id = $db->lastInsertId('tbl_studentregsubjects_detail_history', 'srsdh_id');
        return $id;
    }
    
    public function deleteItemReg($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentregsubjects_detail', 'regsub_id = '.$id);
        return $delete;
    }
    
    public function deleteItemRegHis($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentregsubjects_detail_history', 'regsub_id = '.$id);
        return $delete;
    }
    
    public function getApplicantFeeStructure($program_id, $scheme, $intake_id, $student_category=579,$throw=1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('fs'=>'fee_structure'))
            ->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
            ->where("fs.fs_intake_start = '".$intake_id."'")
            ->where("fs.fs_student_category = '".$student_category."'");
        //echo $selectData; exit;
        $row = $db->fetchRow($selectData);

        if(!$row){

            $ex_msg = "No Fee Structure setup for program(".$program_id."), scheme(".$scheme."), intake(".$intake_id."), category(".$student_category.")";

            if ($throw==1){
                throw new Exception($ex_msg);
            }else{
                return false;
            }

        }else{
            return $row;
        }
    }
    
    public function insertCourseItem($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_auditpaperapplicationitem', $bind);
        $id = $db->lastInsertId('tbl_auditpaperapplicationitem', 'api_id');
        return $id;
    }
    
    public function getCourseItem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplicationitem'), array('value'=>'*'))
            ->where('a.api_apa_id = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateCourseItem($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_auditpaperapplicationitem', $bind, 'api_id = '.$id);
        return $update;
    }
    
    public function deleteCourseItem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_auditpaperapplicationitem', 'api_id = '.$id);
        return $delete;
    }
    
    public function getUntickCourseItem($id, $arr){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplicationitem'), array('value'=>'*'))
            ->where('a.api_apa_id = ?', $id)
            ->where('a.api_item NOT IN (?)', $arr);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getTickCourseItemById($id, $item){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_auditpaperapplicationitem'), array('value'=>'*'))
            ->where('a.api_apa_id = ?', $id)
            ->where('a.api_item = ?', $item);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCurrentIntake($sem, $year){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->where('a.sem_seq = ?', $sem)
            ->where('a.sem_year = ?', $year);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    
	public function updateData($data,$id){
		 $db = Zend_Db_Table::getDefaultAdapter();
		$db->update('tbl_auditpaperapplicationcourse',$data,"apc_id ='".$id."'");
	}
}
?>