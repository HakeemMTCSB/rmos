<?php
class Records_Form_EmergencyContactForm extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        
        //emergency contact type
        $ec_type = new Zend_Form_Element_Select('ec_type');
        $ec_type->removeDecorator("DtDdWrapper");
        $ec_type->setAttrib('class', 'select');
        $ec_type->setAttrib('id', 'ec_type');
        $ec_type->removeDecorator("Label");

        $defModel = new App_Model_General_DbTable_Definationms();
        $ecTypeList = $defModel->getDataByType(144);

        $ec_type->addMultiOption('', '-- Select --');

        if ($ecTypeList){
            foreach ($ecTypeList as $ecTypeLoop){
                $ec_type->addMultiOption($ecTypeLoop['idDefinition'], $ecTypeLoop['DefinitionDesc']);
            }
        }

        //emergency contact name
        $ec_name = new Zend_Form_Element_Text('ec_name');
        $ec_name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //emergency contact address 1
        $ec_add1 = new Zend_Form_Element_Text('ec_add1');
        $ec_add1->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //emergency contact address 2
        $ec_add2 = new Zend_Form_Element_Text('ec_add2');
        $ec_add2->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //emergency contact address 3
        $ec_add3 = new Zend_Form_Element_Text('ec_add3');
        $ec_add3->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //emergency contact country
        $ec_country = new Zend_Form_Element_Select('ec_country');
        $ec_country->removeDecorator("DtDdWrapper");
        $ec_country->setAttrib('class', 'select');
        $ec_country->setAttrib('id', 'ec_country');
        $ec_country->setAttrib('onchange', 'get_state(this.value, "ec_state");');
        $ec_country->removeDecorator("Label");

        $countryModel = new GeneralSetup_Model_DbTable_Countrymaster();

        $countryList = $countryModel->fetchAll(null, 'CountryName');

        $ec_country->addMultiOption('', '-- Select --');

        if ($countryList){
            foreach ($countryList as $countryLoop){
                $ec_country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }

        //emergency contact state
        $ec_state = new Zend_Form_Element_Select('ec_state');
        $ec_state->removeDecorator("DtDdWrapper");
        $ec_state->setAttrib('class', 'select');
        $ec_state->setAttrib('id', 'ec_state');
        $ec_state->setAttrib('onchange', 'get_state(this.value, "ec_city"); ecStateOthers(this.value);');
        $ec_state->removeDecorator("Label");

        $stateModel = new GeneralSetup_Model_DbTable_State();

        $stateList = $stateModel->fetchAll();

        $ec_state->addMultiOption('', '-- Select --');

        if ($stateList){
            foreach ($stateList as $stateLoop){
                $ec_state->addMultiOption($stateLoop['idState'], $stateLoop['StateName']);
            }
        }

        $ec_state_others = new Zend_Form_Element_Text('ec_state_others');
        $ec_state_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //emergency contact city
        $ec_city = new Zend_Form_Element_Select('ec_city');
        $ec_city->removeDecorator("DtDdWrapper");
        $ec_city->setAttrib('class', 'select');
        $ec_city->setAttrib('id', 'ec_city');
        $ec_city->removeDecorator("Label");
        $ec_city->setAttrib('onchange', 'ecCityOthers(this.value);');

        $cityModel = new GeneralSetup_Model_DbTable_City();

        $cityList = $cityModel->fetchAll();

        $ec_city->addMultiOption('', '-- Select --');

        if ($cityList){
            foreach ($cityList as $cityLoop){
                $ec_city->addMultiOption($cityLoop['idCity'], $cityLoop['CityName']);
            }
        }

        $ec_city->addMultiOption(99, 'Others');

        $ec_city_others = new Zend_Form_Element_Text('ec_city_others');
        $ec_city_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //postcode
        $ec_postcode = new Zend_Form_Element_Text('ec_postcode');
        $ec_postcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //phone mobile
        $ec_phone_mobile = new Zend_Form_Element_Text('ec_phone_mobile');
        $ec_phone_mobile->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //phone home
        $ec_phone_home = new Zend_Form_Element_Text('ec_phone_home');
        $ec_phone_home->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //phone office
        $ec_phone_office = new Zend_Form_Element_Text('ec_phone_office');
        $ec_phone_office->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //phone office
        $ec_email = new Zend_Form_Element_Text('ec_email');
        $ec_email->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //copy permanent address
        $ec_permanent_add = new Zend_Form_Element_Checkbox('ec_permanent_add');
        $ec_permanent_add->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        $ec_permanent_add->setCheckedValue(1);  
        $ec_permanent_add->setUncheckedValue(0);
        $ec_permanent_add->setAttrib('onchange', 'ecCopyPerAdd(this.value);');
        
        //form elements
        $this->addElements(array(
            $ec_type,
            $ec_name,
            $ec_add1,
            $ec_add2,
            $ec_add3,
            $ec_country,
            $ec_state,
            $ec_city,
            $ec_postcode,
            $ec_phone_mobile,
            $ec_phone_home,
            $ec_phone_office,
            $ec_permanent_add,
            $ec_state_others,
            $ec_city_others,
            $ec_email
        ));
    }
}