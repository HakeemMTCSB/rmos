<?php 

class Records_Form_ProcessChangeStatusSearch extends Zend_Form
{
		
    protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

			
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake'),
		    'required'=>true
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- Please Select --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
				
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme'),
                        'required'=>true,
                        'onchange'=>'getProgramScheme(this)'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		 //Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
			'registerInArrayValidator'=>false
		));
		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));		

		
		 //Scheme
		$this->addElement('select','IdScheme', array(
			'label'=>$this->getView()->translate('Scheme')
		));		
		 
        $schemeDb =  new GeneralSetup_Model_DbTable_Schemesetup();
		$this->IdScheme->addMultiOption(null,$this->getView()->translate('-- All --'));				
		foreach($schemeDb->fnGetSchemeDetails() as $scheme){
			if($this->_locale=='ms_MY'){
				$this->IdScheme->addMultiOption($scheme["IdScheme"],$scheme["ArabicDescription"]);
			}else{
				$this->IdScheme->addMultiOption($scheme["IdScheme"],$scheme["EnglishDescription"]);
			}
		}
		
		
		
		//Name
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name'),
			'class'=>'input-txt'

		));
		
		//Student ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID'),
			'class'=>'input-txt'
		));
				
		
		//Status
		/*$this->addElement('select','semester_status', array(
			'label'=>$this->getView()->translate('Semester Status')
		));
		
		$maintenanceModelObj =  new GeneralSetup_Model_DbTable_Maintenance();
		$status = $maintenanceModelObj->fnfetchProfileStatus("a.idDefType = '32'");
		
		$this->semester_status->addMultiOption(null,"-- All --");		
		foreach($status as $s){
			$this->semester_status->addMultiOption($s["key"],$s["value"]);
		}
		*/
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        	//button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>