<?php
class Records_Form_VisaReport extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');
        
        $model = new Records_Model_DbTable_Report();
        
        //Intake
        $intake = new Zend_Form_Element_Select('intake');
	$intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
        $intake->setAttrib('multiple', 'true');
	$intake->removeDecorator("Label");
        
        $intakeList = $model->getIntake();
        
        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }
        
        //programme
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getProgramScheme(this.value)');
	$program->removeDecorator("Label");
        
        $program->addMultiOption('', '--All--');
        
        $programList = $model->getProgram();
        
        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }
        
        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
	$programscheme->removeDecorator("Label");
        
        $programscheme->addMultiOption('', '--All--');
        
        if ($programid){
            $programSchemeList = $model->getProgramScheme($programid);
            
            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }
        
        //student category
        $nationality = new Zend_Form_Element_Select('nationality');
	$nationality->removeDecorator("DtDdWrapper");
        $nationality->setAttrib('class', 'select');
	$nationality->removeDecorator("Label");
        
        $nationality->addMultiOption('', '--All--');
        
        $countryList = $model->getCountry();
        
        if ($countryList){
            foreach ($countryList as $countryLoop){
                $nationality->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //student status
        $status = new Zend_Form_Element_Select('status');
	$status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'select');
	$status->removeDecorator("Label");
        
        $status->addMultiOption('', '--All--');
        
        $statusList = $model->getDefinationList(20);
        
        if ($statusList){
            foreach ($statusList as $statusLoop){
                $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
            }
        }
        
        //gender
        $gender = new Zend_Form_Element_Select('gender');
	$gender->removeDecorator("DtDdWrapper");
        $gender->setAttrib('class', 'select');
	$gender->removeDecorator("Label");
        
        $gender->addMultiOption('', '--All--');
        $gender->addMultiOption(1, 'Male');
        $gender->addMultiOption(2, 'Female');
        
        //religion
        $religion = new Zend_Form_Element_Select('religion');
	$religion->removeDecorator("DtDdWrapper");
        $religion->setAttrib('class', 'select');
	$religion->removeDecorator("Label");
        
        $religion->addMultiOption('', '--All--');
        
        $religionList = $model->getReligion();
        
        if ($religionList){
            foreach ($religionList as $religionLoop){
                $religion->addMultiOption($religionLoop['idDefinition'], $religionLoop['DefinitionDesc']);
            }
        }
        
        $religion->addMultiOption(99, 'Others');
        
        $this->addElements(array(
            $intake,
            $program,
            $programscheme,
            $nationality,
            $status,
            $gender,
            $religion
        ));
    }
}
?>
