<?php 

class Records_Form_PassportDetailForm extends Zend_Form
{
		
    protected $_locale;
    protected $_idPassport;
	
    public function setLocale($value) {
        $this->_locale = $value;
    }
	
    public function setIdPassport($value2) {
        $this->_idPassport = $value2;
    }
		
    public function init(){
						
        $this->setMethod('post');
        $this->setAttrib('id','form1');
        $this->setAttrib('enctype','multipart/form-data');

        if(isset($this->_idPassport) && $this->_idPassport!=''){
            $this->setAttrib('action', "/records/visa/edit");
        }else{
            $this->setAttrib('action', "/records/visa/add");
        }

        $this->addElement('hidden','p_id');
        $this->addElement('hidden','sp_id');

        //Passport Type
        $this->addElement('select','p_type', array(
            //'label'=>$this->getView()->translate('Passport Type'),
            'required'=>true
        ));

        $defDB = new App_Model_General_DbTable_Definationms();

        $this->p_type->addMultiOption(null,"-- Please Select --");
        
        foreach($defDB->getDataByType(159) as $def){
            $this->p_type->addMultiOption($def["idDefinition"],$def["DefinitionDesc"]);
        }	

        //passport no
        $this->addElement('text','p_passport_no', array(
            //'label'=>$this->getView()->translate('Passport No.'),
            'required'=>true
        ));
        
        if(isset($this->_idPassport) && $this->_idPassport!=''){
            //$this->p_passport_no->setAttrib('disabled',true);
        }

        //country
        $this->addElement('select','p_country_issue', array(
            //'label'=>$this->getView()->translate('Country of Issue'),
            'required'=>true
        ));

        $countryDB = new App_Model_General_DbTable_Country();

        $this->p_country_issue->addMultiOption(null,"-- Please Select --");		
        foreach($countryDB->getData() as $c){
            $this->p_country_issue->addMultiOption($c["idCountry"],$c["CountryName"]);
        }	

        //issue date
        $this->addElement('text','p_date_issue', array(
            //'label'=>$this->getView()->translate('Date of Issue'),
            'required'=>true
        ));	

        //Visa Type
        $this->addElement('select','p_visa_type', array(
            //'label'=>$this->getView()->translate('Visa Type'),
            'required'=>true
        ));

        $defDB = new App_Model_General_DbTable_Definationms();

        $this->p_visa_type->addMultiOption(null,"-- Please Select --");		
        
        foreach($defDB->getDataByType(108) as $def){
            $this->p_visa_type->addMultiOption($def["idDefinition"],$def["DefinitionDesc"]);
        }

        //Expiry Date
        $this->addElement('text','p_expiry_date', array(
            //'label'=>$this->getView()->translate('Expiry Date'),
            'required'=>true
        ));	

        /*$this->passport_65 = new Zend_Form_Element_File('passport_65');
        //$this->passport_65->setLabel('Front Page Passport')
        $this->passport_65->setMultiFile(1)->addValidator('Count', 1);
        $this->passport_65->setAttrib('name','passport_65[]')
            ->removeDecorator("DtDdWrapper")->removeDecorator("Label");*/
    }
}
?>