<?php
class Records_Form_EditCreditTransfer extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $landscapeId = $this->getAttrib('landscapeId');
        $appTypeId = $this->getAttrib('apptypeId');
        //$gstrtranslate =Zend_Registry::get('Zend_Translate');
		
        //student name or id
        $StudentIDName = new Zend_Form_Element_Text('StudentIDName');
	$StudentIDName->setAttrib('class', 'span-7')
            //->setAttrib('onchange', 'findStudent(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //radio button id or name
        $IdOrStudent = new Zend_Form_Element_Radio('IdOrStudent');
        $IdOrStudent->removeDecorator("DtDdWrapper");
        $IdOrStudent->setAttrib('id', 'IdOrStudent');
	$IdOrStudent->removeDecorator("Label");
        $IdOrStudent->addMultiOptions(array(
            '1' => 'Student ID',
            '2' => 'Name'
	));
        $IdOrStudent->setSeparator('&nbsp;&nbsp;');
        $IdOrStudent->setValue("1");
        
        //profile status
        $ProfileStatus = new Zend_Form_Element_Text('ProfileStatus');
	$ProfileStatus->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //id type
        $IdType = new Zend_Form_Element_Text('IdType');
	$IdType->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Intake
        $Intake = new Zend_Form_Element_Text('Intake');
	$Intake->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Id no
        $IdNo = new Zend_Form_Element_Text('IdNo');
	$IdNo->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Branch
        $Branch = new Zend_Form_Element_Text('Branch');
	$Branch->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Programme
        $Programme = new Zend_Form_Element_Text('Programme');
	$Programme->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Programme scheme
        $ProgScheme = new Zend_Form_Element_Text('ProgScheme');
	$ProgScheme->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * contect info stuff
         */
        
        //Address
        $Address = new Zend_Form_Element_Textarea('Address');
	$Address->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->setAttrib("style","width: 270px; height: 98px;")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //phone home
        $PhoneHome = new Zend_Form_Element_Text('PhoneHome');
	$PhoneHome->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Phone Mobile
        $PhoneMobile = new Zend_Form_Element_Text('PhoneMobile');
	$PhoneMobile->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Phone Office
        $PhoneOffice = new Zend_Form_Element_Text('PhoneOffice');
	$PhoneOffice->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Email
        $Email = new Zend_Form_Element_Text('Email');
	$Email->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * add Application stuff
         */
        
        //Application ID
        $ApplicationID = new Zend_Form_Element_Text('ApplicationID');
	$ApplicationID->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //call student registration class
        $studentRegModel = new Registration_Model_DbTable_Studentregistration();
        
        //semester
        $Semester = new Zend_Form_Element_Select('Semester');
	$Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('id', 'Semester');
	$Semester->removeDecorator("Label");
        
        $Semester->addMultiOption('', '-- Select --');
        
        $semesterList = $studentRegModel->getSemester();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $Semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
            }
        }
        
        $Course = new Zend_Form_Element_Select('Course[]');
	$Course->removeDecorator("DtDdWrapper");
        $Course->setAttrib('class', 'select');
        //$Course->setAttrib('name', 'Course[]');
        $Course->setAttrib('multiple', 'multiple');
        $Course->setAttrib('id', 'Course');
	$Course->removeDecorator("Label");
        
        //$Course->addMultiOption('', '-- Select --');
        
        
        $lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
        
        /*$courseEquiList = $studentRegModel->getEquivalentSub();
        
        if ($courseEquiList){
            foreach ($courseEquiList as $courseEquiLoop){
                $Course->addMultiOption($courseEquiLoop['IdSubject'], $courseEquiLoop['SubCode'].' - '.$courseEquiLoop['SubjectName']);
            }
        }*/
        
        $CourseEquivalent = new Zend_Form_Element_Select('CourseEquivalent');
	$CourseEquivalent->removeDecorator("DtDdWrapper");
        $CourseEquivalent->setAttrib('class', 'select');
        $CourseEquivalent->setAttrib('onchange', 'courseInfo(this.value);');
        $CourseEquivalent->setAttrib('id', 'CourseEquivalent');
	$CourseEquivalent->removeDecorator("Label");
        
        $CourseEquivalent->addMultiOption('', '-- Select --');
        
        //get landscape
        $landscapeInfo = $lobjstudentregistrationModel->getLandscape($landscapeId);
        
        if ($landscapeInfo['LandscapeType']==44){
            $courseList = $lobjstudentregistrationModel->getBlockSubject($landscapeId);
        }else{
            $courseList = $lobjstudentregistrationModel->getSubject($landscapeId);
        }
        
        if ($courseList){
            foreach ($courseList as $courseLoop){
                $CourseEquivalent->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
            }
        }
        
        //application type
        $AppType = new Zend_Form_Element_Select('AppType');
	$AppType->removeDecorator("DtDdWrapper");
        $AppType->setAttrib('class', 'select');
        $AppType->setAttrib('id', 'AppType');
	$AppType->removeDecorator("Label");
        
        $AppType->addMultiOption('', '-- Select --');
        
        $appTypeList = $studentRegModel->getDefination(153);
        
        if ($appTypeList){
            foreach ($appTypeList as $appTypeLoop){
                $AppType->addMultiOption($appTypeLoop['idDefinition'], $appTypeLoop['DefinitionDesc']);
            }
        }
        
        //applied by
        $AppliedBy = new Zend_Form_Element_Text('AppliedBy');
	$AppliedBy->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //institution type
        $InstitionType = new Zend_Form_Element_Select('InstitionType');
	$InstitionType->removeDecorator("DtDdWrapper");
        $InstitionType->setAttrib('class', 'select');
        $InstitionType->setAttrib('id', 'InstitionType');
	$InstitionType->removeDecorator("Label");
        
        $InstitionType->addMultiOption('', '-- Select --');
        
        $insTypeList = $studentRegModel->getDefination(154);
        
        if ($insTypeList){
            foreach ($insTypeList as $insTypeLoop){
                $InstitionType->addMultiOption($insTypeLoop['idDefinition'], $insTypeLoop['DefinitionDesc']);
            }
        }
        
        //applied Date
        $AppliedDate = new Zend_Form_Element_Text('AppliedDate');
	$AppliedDate->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //applied status
        $AppliedStatus = new Zend_Form_Element_Text('AppliedStatus');
	$AppliedStatus->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //gred
        $Gred = new Zend_Form_Element_Select('Gred');
	$Gred->removeDecorator("DtDdWrapper");
        $Gred->setAttrib('class', 'select');
        $Gred->setAttrib('id', 'Gred');
	$Gred->removeDecorator("Label");
        
        $Gred->addMultiOption('', '-- Select --');
        
        $gredList = array();
        
        if ($appTypeId == 780){
            $landscapeInfo = $studentRegModel->getLandscape($landscapeId);
            
            if ($landscapeInfo['AssessmentMethod'] == 'point'){
                $gred = $studentRegModel->getGred();
                if ($gred){
                    $i=0;
                    foreach ($gred as $gredLoop){
                        $gredList[$i]['name']=$gredLoop['DefinitionDesc'];
                        $i++;
                    }
                }
            }else{
                $gredList[0]['name']='CT';
            }
        }else{
            $gredList[0]['name']='EX';
        }
        
        if ($gredList){
            foreach ($gredList as $gredLoop){
                $Gred->addMultiOption($gredLoop['name'], $gredLoop['name']);
            }
        }
        
        $Remarks = new Zend_Form_Element_Textarea('Remarks');
	$Remarks->setAttrib('class', 'span-7')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $InstitutionName = new Zend_Form_Element_Text('InstitutionName');
	$InstitutionName->setAttrib('class', 'span-7')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $ExternalSubject = new Zend_Form_Element_Text('ExternalSubject');
	$ExternalSubject->setAttrib('class', 'span-7')
            ->setAttrib("name", "ExternalSubject[]")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //programme
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getCourseToTransfer(this.value);');
	$program->removeDecorator("Label");
        
        $program->addMultiOption('', '-- Select --');
        
        $ProgramList = $lobjstudentregistrationModel->getProgram();
        
        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }
        
        $this->addElements(array(
            $StudentIDName,
            $IdOrStudent,
            $ProfileStatus,
            $IdType,
            $Intake,
            $IdNo,
            $Branch,
            $Programme,
            $ProgScheme,
            $Address,
            $PhoneHome,
            $PhoneMobile,
            $PhoneOffice,
            $Email,
            $ApplicationID,
            $Semester,
            $Course,
            $CourseEquivalent,
            $AppType,
            $AppliedBy,
            $InstitionType,
            $AppliedDate,
            $AppliedStatus,
            $Gred,
            $Remarks,
            $InstitutionName,
            $ExternalSubject,
            $program
        ));
    }
}
?>
