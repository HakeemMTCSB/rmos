<?php
class Records_Form_Recordquitreason extends Zend_Dojo_Form {
    public function init() {
        $gstrtranslate = Zend_Registry :: get('Zend_Translate');

        $Reason = new Zend_Form_Element_Text('Reason');
        $Reason->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $Reason->setAttrib('required', "false");
        $Reason->setAttrib('maxlength', '50');
        $Reason->removeDecorator("DtDdWrapper");
        $Reason->removeDecorator("Label");
        $Reason->removeDecorator('HtmlTag');

        $QuitDescription = new Zend_Form_Element_Textarea('QuitDescription');
        $QuitDescription->removeDecorator("DtDdWrapper");
        $QuitDescription->setAttrib('required', "false");
        $QuitDescription->removeDecorator("Label");
        $QuitDescription->removeDecorator('HtmlTag');
        $QuitDescription->setAttrib("rows", "5");
        $QuitDescription->setAttrib("cols", "30");

        $Addquitreason = new Zend_Form_Element_Button('Addquitreason');
        $Addquitreason->setAttrib('class', 'NormalBtn');
        $Addquitreason->setAttrib('dojoType', "dijit.form.Button");
        $Addquitreason->label = $gstrtranslate->_("Add");
        $Addquitreason->setAttrib('OnClick', 'addQuitConfigentry()')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

        $Clearquitreason = new Zend_Form_Element_Button('Clearquitreason');
        $Clearquitreason->setAttrib('class', 'NormalBtn');
        $Clearquitreason->setAttrib('dojoType', "dijit.form.Button");
        $Clearquitreason->label = $gstrtranslate->_("Clear");
        $Clearquitreason->setAttrib('OnClick', 'clearQuitpageAdd()');
        $Clearquitreason->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
        
        $this->addElements(array(
            $Addquitreason,$Clearquitreason,$QuitDescription,$Reason
                )
        );
    }
}

?>
