<?php
class Records_Form_FinancialParticularForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //funding method
        $af_method = new Zend_Form_Element_Select('af_method');
	$af_method->removeDecorator("DtDdWrapper");
        $af_method->setAttrib('class', 'select');
        $af_method->setAttrib('id', 'af_method');
	$af_method->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $fundingMethodList = $defModel->getDataByType(106);
        
        $af_method->addMultiOption('', '-- Select --');
        
        if (count($fundingMethodList) > 0){
            foreach ($fundingMethodList as $fundingMethodLoop){
                $af_method->addMultiOption($fundingMethodLoop['idDefinition'], $fundingMethodLoop['DefinitionDesc']);
            }
        }
        
        //name sponsorship
        $af_sponsor_name = new Zend_Form_Element_Text('af_sponsor_name');
	$af_sponsor_name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //select sholarship
        $af_type_scholarship = new Zend_Form_Element_Select('af_type_scholarship');
	$af_type_scholarship->removeDecorator("DtDdWrapper");
        $af_type_scholarship->setAttrib('class', 'select');
        $af_type_scholarship->setAttrib('id', 'af_type_scholarship');
	$af_type_scholarship->removeDecorator("Label");
        
        $scholarshipTypeList = $defModel->getDataByType(107);
        
        $af_type_scholarship->addMultiOption('', '-- Select --');
        
        if (count($scholarshipTypeList) > 0){
            foreach ($scholarshipTypeList as $scholarshipTypeLoop){
                $af_type_scholarship->addMultiOption($scholarshipTypeLoop['idDefinition'], $scholarshipTypeLoop['DefinitionDesc']);
            }
        }
        
        //scholarhip secured
        $af_scholarship_secured = new Zend_Form_Element_Text('af_scholarship_secured');
	$af_scholarship_secured->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //applying for scholarship
        $af_scholarship_apply = new Zend_Form_Element_Select('af_scholarship_apply');
	$af_scholarship_apply->removeDecorator("DtDdWrapper");
        $af_scholarship_apply->setAttrib('class', 'select');
        $af_scholarship_apply->setAttrib('id', 'af_scholarship_apply');
	$af_scholarship_apply->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $af_method,
            $af_sponsor_name,
            $af_type_scholarship,
            $af_scholarship_secured,
            $af_scholarship_apply
        ));
    }
}