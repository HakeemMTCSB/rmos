<?php
class Records_Form_AttributesForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //attribut type
        $sa_type = new Zend_Form_Element_Select('sa_type');
	$sa_type->removeDecorator("DtDdWrapper");
        $sa_type->setAttrib('class', 'select');
        $sa_type->setAttrib('id', 'sa_type');
	$sa_type->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $attributeTypeList = $defModel->getDataByType(145);
        
        $sa_type->addMultiOption('', '-- Select --');
        
        if ($attributeTypeList){
            foreach ($attributeTypeList as $attributeTypeLoop){
                $sa_type->addMultiOption($attributeTypeLoop['idDefinition'], $attributeTypeLoop['DefinitionDesc']);
            }
        }
        
        //attribite category
        $sa_category = new Zend_Form_Element_Select('sa_category');
	$sa_category->removeDecorator("DtDdWrapper");
        $sa_category->setAttrib('class', 'select');
        $sa_category->setAttrib('id', 'sa_category');
	$sa_category->removeDecorator("Label");
        
        $attributeCategoryList = $defModel->getDataByType(147);
        
        $sa_category->addMultiOption('', '-- Select --');
        
        if ($attributeCategoryList){
            foreach ($attributeCategoryList as $attributeCategoryLoop){
                $sa_category->addMultiOption($attributeCategoryLoop['idDefinition'], $attributeCategoryLoop['DefinitionDesc']);
            }
        }
        
        //form elements
        $this->addElements(array(
            $sa_type,
            $sa_category
        ));
    }
}