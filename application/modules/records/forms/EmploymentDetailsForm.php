<?php
class Records_Form_EmploymentDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //employment status
        $ae_status = new Zend_Form_Element_Select('ae_status');
	$ae_status->removeDecorator("DtDdWrapper");
        $ae_status->setAttrib('class', 'select');
        $ae_status->setAttrib('id', 'appl_salutation');
	$ae_status->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $employmentStatusList = $defModel->getDataByType(105);
        
        $ae_status->addMultiOption('', '-- Select --');
        
        if (count($employmentStatusList) > 0){
            foreach ($employmentStatusList as $employmentStatusLoop){
                $ae_status->addMultiOption($employmentStatusLoop['idDefinition'], $employmentStatusLoop['DefinitionDesc']);
            }
        }
        
        //company name
        $ae_comp_name = new Zend_Form_Element_Text('ae_comp_name');
	$ae_comp_name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //company address
        $ae_comp_address = new Zend_Form_Element_Textarea('ae_comp_address');
        $ae_comp_address->setAttrib('rows', '5');
	$ae_comp_address->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //company phone number
        $ae_comp_phone = new Zend_Form_Element_Text('ae_comp_phone');
	$ae_comp_phone->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //company fax
        $ae_comp_fax = new Zend_Form_Element_Text('ae_comp_fax');
	$ae_comp_fax->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //designation
        $ae_designation = new Zend_Form_Element_Text('ae_designation');
	$ae_designation->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //position level
        $ae_position = new Zend_Form_Element_Radio('ae_position');
        $ae_position->removeDecorator("DtDdWrapper");
        $ae_position->setAttrib('id', 'IdProgram');
	$ae_position->removeDecorator("Label");
        $ae_position->setSeparator('<br/>');
        $ae_position->setValue("1");
        
        $employmentPositionList = $defModel->getDataByType(110);
        
        if (count($employmentPositionList) > 0){
            foreach ($employmentPositionList as $employmentPositionLoop){
                $ae_position->addMultiOption($employmentPositionLoop['idDefinition'], ' '.$employmentPositionLoop['DefinitionDesc'].' - '.$employmentPositionLoop['Description']);
            }
        }
        
        //year of service
        $emply_year_service = new Zend_Form_Element_Text('emply_year_service');
	$emply_year_service->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //industry
        $ae_industry = new Zend_Form_Element_Select('ae_industry');
	$ae_industry->removeDecorator("DtDdWrapper");
        $ae_industry->setAttrib('class', 'select');
        $ae_industry->setAttrib('id', 'ae_industry');
	$ae_industry->removeDecorator("Label");
        
        $employmenIndustryList = $defModel->getDataByType(114);
        
        $ae_industry->addMultiOption('', '-- Select --');
        
        if (count($employmenIndustryList) > 0){
            foreach ($employmenIndustryList as $employmenIndustryLoop){
                $ae_industry->addMultiOption($employmenIndustryLoop['idDefinition'], $employmenIndustryLoop['DefinitionDesc']);
            }
        }
        
        //job description
        $ae_job_desc = new Zend_Form_Element_Textarea('ae_job_desc');
        $ae_job_desc->setAttrib('rows', '5');
	$ae_job_desc->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $ae_status,
            $ae_comp_name,
            $ae_comp_address,
            $ae_comp_phone,
            $ae_comp_fax,
            $ae_designation,
            $ae_position,
            $emply_year_service,
            $ae_industry,
            $ae_job_desc
        ));
    }
}