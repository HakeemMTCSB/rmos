<?php
class Records_Form_Report extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');
        
        $model = new Records_Model_DbTable_Report();
        
        //Intake
        $intake = new Zend_Form_Element_Select('intake');
	$intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
        $intake->setAttrib('multiple', 'true');
	$intake->removeDecorator("Label");
        
        $intakeList = $model->getIntake();
        
        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }
        
        //programme
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getProgramScheme(this.value)');
	$program->removeDecorator("Label");
        
        $program->addMultiOption('', '--All--');
        
        $programList = $model->getProgram();
        
        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }
        
        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
	$programscheme->removeDecorator("Label");
        
        $programscheme->addMultiOption('', '--All--');
        
        if ($programid){
            $programSchemeList = $model->getProgramScheme($programid);
            
            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }
        
        //student category
        $studentcategory = new Zend_Form_Element_Select('studentcategory');
	$studentcategory->removeDecorator("DtDdWrapper");
        $studentcategory->setAttrib('class', 'select');
	$studentcategory->removeDecorator("Label");
        
        $studentcategory->addMultiOption('', '--All--');
        
        $studCatList = $model->getDefinationList(95);
        
        if ($studCatList){
            foreach ($studCatList as $studCatLoop){
                $studentcategory->addMultiOption($studCatLoop['idDefinition'], $studCatLoop['DefinitionDesc']);
            }
        }
        
        //student status
        $status = new Zend_Form_Element_Select('status');
	$status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'select');
	$status->removeDecorator("Label");
        
        $status->addMultiOption('', '--All--');
        
        $statusList = $model->getDefinationList(20);
        
        if ($statusList){
            foreach ($statusList as $statusLoop){
                $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
            }
        }
        
        //semester status
        $semesterstatus = new Zend_Form_Element_Select('semesterstatus');
	$semesterstatus->removeDecorator("DtDdWrapper");
        $semesterstatus->setAttrib('class', 'select');
	$semesterstatus->removeDecorator("Label");
        
        $semesterstatus->addMultiOption('', '--All--');
        
        $semstatusList = $model->getDefinationList(32);
        
        if ($semstatusList){
            foreach ($semstatusList as $semstatusLoop){
                $semesterstatus->addMultiOption($semstatusLoop['idDefinition'], $semstatusLoop['DefinitionDesc']);
            }
        }
        
        $this->addElements(array(
            $intake,
            $program,
            $programscheme,
            $studentcategory,
            $status,
            $semesterstatus
        ));
    }
}
?>
