<?php
class Records_Form_QuantitativeProficiencyForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //level
        $qp_level = new Zend_Form_Element_Text('qp_level');
	$qp_level->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //grade marks
        $qp_grade_mark = new Zend_Form_Element_Text('qp_grade_mark');
	$qp_grade_mark->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //institution
        $qp_institution = new Zend_Form_Element_Text('qp_institution');
	$qp_institution->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $qp_level,
            $qp_grade_mark,
            $qp_institution
        ));
    }
}