<?php
class Records_Form_CurricularActivitiesForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //ca seq
        $ca_seq = new Zend_Form_Element_Text('ca_seq');
	$ca_seq->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //ca date
        $ca_date = new Zend_Dojo_Form_Element_DateTextBox('ca_date');
        $ca_date->setAttrib('dojoType',"dijit.form.DateTextBox");
        $ca_date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $ca_date->removeDecorator("DtDdWrapper");
        $ca_date->setAttrib('title',"dd-mm-yyyy");
        $ca_date->removeDecorator("Label");
        $ca_date->setAttrib('class', 'input-txt datepicker');
        
        //ca description
        $ca_desc = new Zend_Form_Element_Textarea('ca_desc');
        $ca_desc->setAttrib('rows', '5');
	$ca_desc->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $ca_seq,
            $ca_date,
            $ca_desc
        ));
    }
}