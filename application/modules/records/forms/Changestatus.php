<?php
class Records_Form_Changestatus extends Zend_Form { //Formclass for the user module
    
	public function init() {
    	
            $edit = $this->getAttrib('edit');
            
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$IdChangeStatusApplication = new Zend_Form_Element_Hidden('IdChangeStatusApplication');
        $IdChangeStatusApplication->removeDecorator("DtDdWrapper");
        $IdChangeStatusApplication->removeDecorator("Label");
        $IdChangeStatusApplication->removeDecorator('HtmlTag');
        
       
        $lobjdeftype = new App_Model_Definitiontype();
        $profilestatusList = $lobjdeftype->fnGetDefinationMs("Semester Status");

        $tempList = array();
        $i = 0;
        foreach($profilestatusList as $list){
            if($list['key'] == 250 || $list['key'] == 835 || $list['key'] == 836){
                $tempList[$i]['key'] = $list['key'];
                $tempList[$i]['value'] = $list['value'];
            }
            $i++;
        }
        
        
        //250 defer 835:quit 836:quit:deceased
        $IdApplyingFor = new Zend_Form_Element_Select('IdApplyingFor');
        $IdApplyingFor->setAttrib('required',"true");
        $IdApplyingFor->removeDecorator("DtDdWrapper");
        $IdApplyingFor->removeDecorator("Label");
        $IdApplyingFor->removeDecorator('HtmlTag');
        $IdApplyingFor->setAttrib('class', 'select');
        $IdApplyingFor->setAttrib('OnChange', "displayElement(this)");
        
        $IdApplyingFor->addMultioption('','-- Please Select --');
        foreach($tempList as $list){
       		$IdApplyingFor->addMultioption($list['key'],$list['value']);
        }

        $EffectiveSemesterFrom= new Zend_Form_Element_Hidden('EffectiveSemesterFrom');
        $EffectiveSemesterFrom->removeDecorator("DtDdWrapper");
        $EffectiveSemesterFrom ->removeDecorator("Label");
        $EffectiveSemesterFrom->removeDecorator('HtmlTag');        
           
        //can select semester (change to drop down option)
        
        if ($edit==true){
            $EffectiveSemesterFromName= new Zend_Form_Element_Text('EffectiveSemesterFromName');
            $EffectiveSemesterFromName->setAttrib('required',"true");
            $EffectiveSemesterFromName->removeDecorator("DtDdWrapper");
            $EffectiveSemesterFromName ->removeDecorator("Label");
            $EffectiveSemesterFromName->removeDecorator('HtmlTag');	
        }else{
            $EffectiveSemesterFromName = new Zend_Form_Element_Select('EffectiveSemesterFromName');
            $EffectiveSemesterFromName->setAttrib('required',"true");
            $EffectiveSemesterFromName->removeDecorator("DtDdWrapper");
            $EffectiveSemesterFromName->setAttrib('class', 'select');
            $EffectiveSemesterFromName->removeDecorator("Label");
            $EffectiveSemesterFromName->removeDecorator('HtmlTag');

            $EffectiveSemesterFromName->addMultiOption('', '-- Please Select --');
        }
		
		$EffectiveSemesterTo= new Zend_Form_Element_Select('EffectiveSemesterTo');
        $EffectiveSemesterTo->removeDecorator("DtDdWrapper");
        $EffectiveSemesterTo ->removeDecorator("Label");
        $EffectiveSemesterTo->removeDecorator('HtmlTag');

        $Reason = new Zend_Form_Element_Select('Reason');
        $Reason->setAttrib('required',"true");
        $Reason->removeDecorator("DtDdWrapper");
        $Reason ->removeDecorator("Label");
        $Reason->setAttrib('class', 'select');
        $Reason->removeDecorator('HtmlTag');
        
        $Reason->addMultiOption('', '-- Please Select --');
        
        $ValidReason = new Zend_Form_Element_Checkbox('ValidReason');    
        $ValidReason->setValue(1);
        $ValidReason->removeDecorator("DtDdWrapper");
        $ValidReason ->removeDecorator("Label");
        $ValidReason->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');        
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";       

        $this->addElements(array($IdChangeStatusApplication,$IdApplyingFor,$EffectiveSemesterFrom,$EffectiveSemesterFromName,$EffectiveSemesterTo,$Reason,$ValidReason,$Save));
   		
    }
}