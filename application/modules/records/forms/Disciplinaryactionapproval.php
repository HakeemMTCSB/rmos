<?php
class Records_Form_Disciplinaryactionapproval extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdStudentStatus = new Zend_Form_Element_Hidden('IdStudentStatus');
        $IdStudentStatus->removeDecorator("DtDdWrapper");
        $IdStudentStatus->removeDecorator("Label");
        $IdStudentStatus->removeDecorator('HtmlTag');
    	
    	$IdApplication = new Zend_Form_Element_Hidden('IdApplication');
        $IdApplication->removeDecorator("DtDdWrapper");
        $IdApplication->removeDecorator("Label");
        $IdApplication->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
     
       
		$Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
		
			
        //form elements
        $this->addElements(array($IdStudentStatus,
        						  $IdApplication,
        						  $UpdDate,
        						  $UpdUser,        						  
        						  $Save,
        						  $Clear
        						 ));

    }
}