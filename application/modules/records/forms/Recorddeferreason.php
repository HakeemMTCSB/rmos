<?php
class Records_Form_Recorddeferreason extends Zend_Dojo_Form {
    public function init() {
        $gstrtranslate = Zend_Registry :: get('Zend_Translate');

        $ReasonDefer = new Zend_Form_Element_Text('ReasonDefer');
        $ReasonDefer->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ReasonDefer->setAttrib('required', "false");
        $ReasonDefer->setAttrib('maxlength', '50');
        $ReasonDefer->removeDecorator("DtDdWrapper");
        $ReasonDefer->removeDecorator("Label");
        $ReasonDefer->removeDecorator('HtmlTag');

        $DeferDescription = new Zend_Form_Element_Textarea('DeferDescription');
        $DeferDescription->removeDecorator("DtDdWrapper");
        $DeferDescription->setAttrib('required', "false");
        $DeferDescription->removeDecorator("Label");
        $DeferDescription->removeDecorator('HtmlTag');
        $DeferDescription->setAttrib("rows", "5");
        $DeferDescription->setAttrib("cols", "30");

        $Adddeferreason = new Zend_Form_Element_Button('Adddeferreason');
        $Adddeferreason->setAttrib('class', 'NormalBtn');
        $Adddeferreason->setAttrib('dojoType', "dijit.form.Button");
        $Adddeferreason->label = $gstrtranslate->_("Add");
        $Adddeferreason->setAttrib('OnClick', 'addDeferConfigentry()')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

        $Cleardeferreason = new Zend_Form_Element_Button('Cleardeferreason');
        $Cleardeferreason->setAttrib('class', 'NormalBtn');
        $Cleardeferreason->setAttrib('dojoType', "dijit.form.Button");
        $Cleardeferreason->label = $gstrtranslate->_("Clear");
        $Cleardeferreason->setAttrib('OnClick', 'cleardeferpageAdd()');
        $Cleardeferreason->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

        $this->addElements(array(
            $Adddeferreason,$Cleardeferreason,$DeferDescription,$ReasonDefer
                )
        );
    }
}

?>
