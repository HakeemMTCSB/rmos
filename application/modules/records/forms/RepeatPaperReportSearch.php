<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 20/10/2015
 * Time: 3:24 PM
 */
class Records_Form_RepeatPaperReportSearch extends Zend_Dojo_Form {

    public function init() {
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');

        $model = new Records_Model_DbTable_Report();

        //program
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getProgramScheme(this.value);');
        $program->removeDecorator("Label");

        $programList = $model->getProgram();

        $program->addMultiOption('', '-- Select --');

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }

        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
        $programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        $programscheme->removeDecorator("Label");

        $programscheme->addMultiOption('', '-- Select --');

        if ($programid){
            $programSchemeList = $model->getProgramScheme($programid);

            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }

        //intake
        $intake = new Zend_Form_Element_Select('intake');
        $intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
        $intake->removeDecorator("Label");

        $intake->addMultiOption('', '-- Select --');

        $intakeList = $model->getIntake();

        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }

        //Student Id
        $studentid = new Zend_Form_Element_Text('studentid');
        $studentid->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //Student Name
        $studentname = new Zend_Form_Element_Text('studentname');
        $studentname->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $program,
            $programscheme,
            $intake,
            $studentid,
            $studentname
        ));
    }
}