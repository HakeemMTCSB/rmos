<?php
class Records_Form_EditAuditPaper extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        $programId = $this->getAttrib('programId');
        $schemeId = $this->getAttrib('programschemeId');
        $intakeId = $this->getAttrib('intakeId');
        $semesterId = $this->getAttrib('semesterId');
        $subjectId = $this->getAttrib('subjectId');
        $countryId = $this->getAttrib('countryId');
        
        $model = new Registration_Model_DbTable_Studentregistration();
        $model2 = new Records_Model_DbTable_Auditpaper();
        $auditPaperModel = new Records_Model_DbTable_Auditpaper();
		
        //student name or id
        $StudentIDName = new Zend_Form_Element_Text('StudentIDName');
	$StudentIDName->setAttrib('class', 'span-7')
            //->setAttrib('onchange', 'findStudent(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //radio button id or name
        $IdOrStudent = new Zend_Form_Element_Radio('IdOrStudent');
        $IdOrStudent->removeDecorator("DtDdWrapper");
        $IdOrStudent->setAttrib('id', 'IdOrStudent');
	$IdOrStudent->removeDecorator("Label");
        $IdOrStudent->addMultiOptions(array(
            '1' => 'Student ID',
            '2' => 'Name'
	));
        $IdOrStudent->setSeparator('&nbsp;&nbsp;');
        $IdOrStudent->setValue("1");
        
        //profile status
        $ProfileStatus = new Zend_Form_Element_Text('ProfileStatus');
	$ProfileStatus->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //id type
        $IdType = new Zend_Form_Element_Text('IdType');
	$IdType->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Intake
        $Intake = new Zend_Form_Element_Text('Intake');
	$Intake->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Id no
        $IdNo = new Zend_Form_Element_Text('IdNo');
	$IdNo->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Branch
        $Branch = new Zend_Form_Element_Text('Branch');
	$Branch->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Programme
        $Programme = new Zend_Form_Element_Text('Programme');
	$Programme->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Programme scheme
        $ProgScheme = new Zend_Form_Element_Text('ProgScheme');
	$ProgScheme->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * contect info stuff
         */
        
        //Address
        $Address = new Zend_Form_Element_Textarea('Address');
	$Address->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->setAttrib("style","width: 270px; height: 98px;")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //phone home
        $PhoneHome = new Zend_Form_Element_Text('PhoneHome');
	$PhoneHome->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Phone Mobile
        $PhoneMobile = new Zend_Form_Element_Text('PhoneMobile');
	$PhoneMobile->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Phone Office
        $PhoneOffice = new Zend_Form_Element_Text('PhoneOffice');
	$PhoneOffice->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Email
        $Email = new Zend_Form_Element_Text('Email');
	$Email->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * add Application stuff
         */
        
        //Application ID
        $ApplicationID = new Zend_Form_Element_Text('ApplicationID');
	$ApplicationID->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //call student registration class
        $studentRegModel = new Registration_Model_DbTable_Studentregistration();
        
        //semester
        $Semester = new Zend_Form_Element_Select('Semester');
	$Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('id', 'Semester');
	$Semester->removeDecorator("Label");
        
        $Semester->addMultiOption('', '-- Select --');
        
        $semesterList = $studentRegModel->getSemester();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $Semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
            }
        }
        
        //program
        $Program = new Zend_Form_Element_Select('Program');
	$Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->setAttrib('id', 'Program');
	$Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getProgramScheme(this.value);');
        
        $Program->addMultiOption('', '-- Select --');
        
        $ProgramList = $model->getProgram();
        
        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $Program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }
        
        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
	$ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
        $ProgramScheme->setAttrib('onchange', 'getCourse(); getItemReg();');
        $ProgramScheme->setAttrib('id', 'ProgramScheme');
	$ProgramScheme->removeDecorator("Label");
        
        $ProgramScheme->addMultiOption('', '-- Select --');
        
        $pslist = $auditPaperModel->getProgramSchemeBasedOnProgram($programId);
        
        if ($pslist){
            foreach ($pslist as $psloop){
                $ProgramScheme->addMultiOption($psloop['IdProgramScheme'], $psloop['mop_name'].' '.$psloop['mos_name'].' '.$psloop['programType']);
            }
        }
        
        $Course = new Zend_Form_Element_Select('Course');
	$Course->removeDecorator("DtDdWrapper");
        $Course->setAttrib('class', 'select');
        $Course->setAttrib('onchange', 'courseInfo(this.value); getCourseSection();');
        //$Course->setAttrib('multiple', 'multiple');
        $Course->setAttrib('id', 'Course');
	$Course->removeDecorator("Label");
        
        $Course->addMultiOption('', '-- Select --');
        
        $landscapeInfo = $auditPaperModel->getlandscape($programId, $schemeId);
        //var_dump($landscapeInfo); exit;
        if ($landscapeInfo){
            $subarr = array();
            foreach ($landscapeInfo as $landscapeLoop){
                if ($landscapeLoop['LandscapeType']==44){
                    $courseList = $auditPaperModel->getBlockSubject($landscapeLoop['IdLandscape'], $semesterId);
                
                    if ($courseList){
                        foreach ($courseList as $courseLoop){
                            array_push($subarr, $courseLoop['subjectid']);
                        }
                    }
                }else{
                    $courseList = $auditPaperModel->getSubject($landscapeLoop['IdLandscape'], $semesterId);
                
                    if ($courseList){
                        foreach ($courseList as $courseLoop){
                            array_push($subarr, $courseLoop['IdSubject']);
                        }
                    }
                }
                
                if (count($subarr)>0){
                    $subList = $model2->getSubjectList($subarr);
                }else{
                    $subList = array();
                }
            }
        }else{
            $subList = array();
        }
        
        if ($subList){
            foreach ($subList as $courseLoop){
                $Course->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
            }
        }
        
        $CourseEquivalent = new Zend_Form_Element_Select('CourseEquivalent');
	$CourseEquivalent->removeDecorator("DtDdWrapper");
        $CourseEquivalent->setAttrib('class', 'select');
        $CourseEquivalent->setAttrib('onchange', 'courseInfo(this.value);');
        $CourseEquivalent->setAttrib('id', 'CourseEquivalent');
	$CourseEquivalent->removeDecorator("Label");
        
        $CourseEquivalent->addMultiOption('', '-- Select --');
        
        //application type
        $AppType = new Zend_Form_Element_Select('AppType');
	$AppType->removeDecorator("DtDdWrapper");
        $AppType->setAttrib('class', 'select');
        $AppType->setAttrib('onchange', 'getGred(this.value);');
        $AppType->setAttrib('id', 'AppType');
	$AppType->removeDecorator("Label");
        
        $AppType->addMultiOption('', '-- Select --');
        
        $appTypeList = $studentRegModel->getDefination(153);
        
        if ($appTypeList){
            foreach ($appTypeList as $appTypeLoop){
                $AppType->addMultiOption($appTypeLoop['idDefinition'], $appTypeLoop['DefinitionDesc']);
            }
        }
        
        //applied by
        $AppliedBy = new Zend_Form_Element_Text('AppliedBy');
	$AppliedBy->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //institution type
        $InstitionType = new Zend_Form_Element_Select('InstitionType');
	$InstitionType->removeDecorator("DtDdWrapper");
        $InstitionType->setAttrib('class', 'select');
        $InstitionType->setAttrib('onchange', 'showHideInstitution(this.value);');
        $InstitionType->setAttrib('id', 'InstitionType');
	$InstitionType->removeDecorator("Label");
        
        $InstitionType->addMultiOption('', '-- Select --');
        
        $insTypeList = $studentRegModel->getDefination(154);
        
        if ($insTypeList){
            foreach ($insTypeList as $insTypeLoop){
                $InstitionType->addMultiOption($insTypeLoop['idDefinition'], $insTypeLoop['DefinitionDesc']);
            }
        }
        
        //applied Date
        $AppliedDate = new Zend_Form_Element_Text('AppliedDate');
	$AppliedDate->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //applied status
        $AppliedStatus = new Zend_Form_Element_Text('AppliedStatus');
	$AppliedStatus->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //gred
        $Gred = new Zend_Form_Element_Select('Gred');
	$Gred->removeDecorator("DtDdWrapper");
        $Gred->setAttrib('class', 'select');
        $Gred->setAttrib('id', 'Gred');
	$Gred->removeDecorator("Label");
        
        $Gred->addMultiOption('', '-- Select --');
        
        $Remarks = new Zend_Form_Element_Textarea('Remarks');
	$Remarks->setAttrib('class', 'span-7')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $InstitutionName = new Zend_Form_Element_Text('InstitutionName');
	$InstitutionName->setAttrib('class', 'span-7')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $ExternalSubject = new Zend_Form_Element_Text('ExternalSubject');
	$ExternalSubject->setAttrib('class', 'span-7')
            ->setAttrib("name", "ExternalSubject[]")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //course section
        $CourseSection = new Zend_Form_Element_Select('CourseSection');
	$CourseSection->removeDecorator("DtDdWrapper");
        $CourseSection->setAttrib('class', 'select');
	$CourseSection->removeDecorator("Label");
        
        $CourseSection->addMultiOption('', '-- Select --');
        
        if ($subjectId){
            $courseSecList = $model2->getGroupQuota($subjectId, $programId, $schemeId);
            
            if ($courseSecList){
                foreach ($courseSecList as $courseSecLoop){
                    if ($courseSecLoop['quota']==true){
                        $full = '';
                        $disabled = '';
                        
                        $CourseSection->addMultiOption(
                            $courseSecLoop['IdCourseTaggingGroup'], 
                            $courseSecLoop['GroupName'].' '.$full
                        );
                    }else{
                        $disabled = 'disabled';
                        $full = '(FULL)';
                        $CourseSection->addMultiOption(
                            $courseSecLoop['IdCourseTaggingGroup'], 
                            $courseSecLoop['GroupName'].' '.$full
                        )->setAttrib('disable', array($courseSecLoop['IdCourseTaggingGroup'], $courseSecLoop['GroupName'].' '.$full));
                    }
                }
            }
        }
        
        //exam center country
        $ExamCenterCountry = new Zend_Form_Element_Select('ExamCenterCountry');
	$ExamCenterCountry->removeDecorator("DtDdWrapper");
        $ExamCenterCountry->setAttrib('class', 'select');
	$ExamCenterCountry->removeDecorator("Label");
        $ExamCenterCountry->setAttrib('onchange', 'getExamCenterCity(this.value);');
        
        $ExamCenterCountry->addMultiOption('', '-- Select --');
        
        $country = $model2->country();
        
        if ($country){
            foreach ($country as $countryLoop){
                $ExamCenterCountry->addMultiOption($countryLoop['key'], $countryLoop['name']);
            }
        }
        
        //exam center city
        $ExamCenterCity = new Zend_Form_Element_Select('ExamCenterCity');
		$ExamCenterCity->removeDecorator("DtDdWrapper");
        $ExamCenterCity->setAttrib('class', 'select');
		$ExamCenterCity->removeDecorator("Label");
        $ExamCenterCity->setAttrib('onchange', 'cityOthers(this.value);');
        
        $ExamCenterCity->addMultiOption('', '-- Select --');
        
        if ($countryId){
        	$examcenterDB =new GeneralSetup_Model_DbTable_ExamCenter();
            $city = $examcenterDB->getcity($countryId, 0);
            if ($city){
                foreach ($city as $cityLoop){
                    $ExamCenterCity->addMultiOption($cityLoop['key'], $cityLoop['city']);
                }
            }
        }
        
        //$ExamCenterCity->addMultiOption(99, 'Others');
        
        $ExamCenterCityOthers = new Zend_Form_Element_Text('ExamCenterCityOthers');
		$ExamCenterCityOthers->setAttrib('class', 'span-7')
            ->setAttrib("placeholder", "Please specified")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $this->addElements(array(
            $StudentIDName,
            $IdOrStudent,
            $ProfileStatus,
            $IdType,
            $Intake,
            $IdNo,
            $Branch,
            $Programme,
            $ProgScheme,
            $Address,
            $PhoneHome,
            $PhoneMobile,
            $PhoneOffice,
            $Email,
            $ApplicationID,
            $Semester,
            $Course,
            $CourseEquivalent,
            $AppType,
            $AppliedBy,
            $InstitionType,
            $AppliedDate,
            $AppliedStatus,
            $Gred,
            $Remarks,
            $InstitutionName,
            $ExternalSubject,
            $Program,
            $ProgramScheme,
            $CourseSection,
            $ExamCenterCountry,
            $ExamCenterCity,
            $ExamCenterCityOthers
        ));
    }
}
?>
