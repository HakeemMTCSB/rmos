<?php
class Records_Form_PersonalDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        $countryId = $this->getAttrib('country');
        $ccountryId = $this->getAttrib('ccountry');
        $stateId = $this->getAttrib('state');
        $cstateId = $this->getAttrib('cstate');
        
        $model = new Records_Model_DbTable_Studentprofile();
        
        //senior student
        $senior_student = new Zend_Form_Element_Radio('senior_student');
        $senior_student->removeDecorator("DtDdWrapper");
        $senior_student->setAttrib('id', 'senior_student');
	$senior_student->removeDecorator("Label");
        $senior_student->addMultiOptions(array(
            '1' => 'Yes',
            '0' => 'No'
	));
        $senior_student->setSeparator('&nbsp;&nbsp;');
        $senior_student->setValue("1");
        
        //salutation
        $appl_salutation = new Zend_Form_Element_Select('appl_salutation');
	$appl_salutation->removeDecorator("DtDdWrapper");
        $appl_salutation->setAttrib('class', 'select');
        $appl_salutation->setAttrib('id', 'appl_salutation');
	$appl_salutation->removeDecorator("Label");
        $appl_salutation->setAttrib('required', true);
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $salutationList = $defModel->getDataByType(17);
        
        $appl_salutation->addMultiOption('', '-- Select --');
        
        if (count($salutationList) > 0){
            foreach ($salutationList as $salutationLoop){
                $appl_salutation->addMultiOption($salutationLoop['idDefinition'], $salutationLoop['DefinitionDesc']);
            }
        }

		//password
        $appl_password = new Zend_Form_Element_Password('appl_password');
		$appl_password->setAttrib('class', 'input-txt')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label");
			
        //first name
        $appl_fname = new Zend_Form_Element_Text('appl_fname');
        $appl_fname->setAttrib('required', true);
	$appl_fname->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //last name
        $appl_lname = new Zend_Form_Element_Text('appl_lname');
        $appl_lname->setAttrib('required', true);
	$appl_lname->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //id type
        $appl_idnumber_type = new Zend_Form_Element_Radio('appl_idnumber_type');
        $appl_idnumber_type->setAttrib('required', true);
        $appl_idnumber_type->removeDecorator("DtDdWrapper");
        $appl_idnumber_type->setAttrib('id', 'IdProgram');
	$appl_idnumber_type->removeDecorator("Label");
        $appl_idnumber_type->setSeparator('&nbsp;&nbsp;');
        $appl_idnumber_type->setValue("1");
        
        $idNumberTypeList = $defModel->getDataByType(117);
        
        if (count($idNumberTypeList) > 0){
            foreach ($idNumberTypeList as $idNumberTypeLoop){
                $appl_idnumber_type->addMultiOption($idNumberTypeLoop['idDefinition'], $idNumberTypeLoop['DefinitionDesc']);
            }
        }
        
        //id number
        $appl_idnumber = new Zend_Form_Element_Text('appl_idnumber');
        $appl_idnumber->setAttrib('required', true);
	$appl_idnumber->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //passpost expired date
        $appl_passport_expiry = new Zend_Dojo_Form_Element_DateTextBox('appl_passport_expiry');
        //$appl_passport_expiry->setAttrib('dojoType',"dijit.form.DateTextBox");
        $appl_passport_expiry->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $appl_passport_expiry->removeDecorator("DtDdWrapper");
        $appl_passport_expiry->setAttrib('title',"dd-mm-yyyy");
        $appl_passport_expiry->removeDecorator("Label");
        $appl_passport_expiry->setAttrib('class', 'input-txt datepicker');
        
        //gender
        $appl_gender = new Zend_Form_Element_Radio('appl_gender');
        $appl_gender->removeDecorator("DtDdWrapper");
        $appl_gender->setAttrib('id', '$appl_gender');
	$appl_gender->removeDecorator("Label");
        $appl_gender->addMultiOptions(array(
            '1' => 'Male',
            '2' => 'Female'
	));
        $appl_gender->setSeparator('&nbsp;&nbsp;');
        $appl_gender->setValue("1");
        $appl_gender->setAttrib('required', true);
        
        //date of birth
        $appl_dob = new Zend_Dojo_Form_Element_DateTextBox('appl_dob');
        //$appl_dob->setAttrib('dojoType',"dijit.form.DateTextBox");
        $appl_dob->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $appl_dob->removeDecorator("DtDdWrapper");
        $appl_dob->setAttrib('title',"dd-mm-yyyy");
        $appl_dob->removeDecorator("Label");
        $appl_dob->setAttrib('class', 'input-txt');
        $appl_dob->setAttrib('required', true);
        //$appl_dob->setAttrib('onclose', 'get_age(this.value);');
        
        //age
        $appl_age = new Zend_Form_Element_Text('appl_age');
	$appl_age->setAttrib('class', 'input-txt');
        $appl_age->setAttrib('disable', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //marital status
        $appl_marital_status = new Zend_Form_Element_Select('appl_marital_status');
	$appl_marital_status->removeDecorator("DtDdWrapper");
        $appl_marital_status->setAttrib('class', 'select');
        $appl_marital_status->setAttrib('id', 'appl_marital_status');
	$appl_marital_status->removeDecorator("Label");
        $appl_marital_status->setAttrib('required', true);
        
        $maritalStatusList = $defModel->getDataByType(65);
        
        $appl_marital_status->addMultiOption('', '-- Select --');
        
        if (count($maritalStatusList) > 0){
            foreach ($maritalStatusList as $maritalStatusLoop){
                $appl_marital_status->addMultiOption($maritalStatusLoop['idDefinition'],$maritalStatusLoop['DefinitionDesc']);
            }
        }
        
        //religion
        $appl_religion = new Zend_Form_Element_Select('appl_religion');
	$appl_religion->removeDecorator("DtDdWrapper");
        $appl_religion->setAttrib('class', 'select');
        $appl_religion->setAttrib('onchange', 'religionOthers(this.value);');
        $appl_religion->setAttrib('id', 'appl_religion');
	$appl_religion->removeDecorator("Label");
        $appl_religion->setAttrib('required', true);
        
        $religionList = $defModel->getDataByType(23);
        
        $appl_religion->addMultiOption('','-- Select --');
        
        if (count($religionList) > 0){
            foreach ($religionList as $religionLoop){
                $appl_religion->addMultiOption($religionLoop['idDefinition'], $religionLoop['DefinitionDesc']);
            }
        }
        
        $appl_religion->addMultiOption(99,'Others');
        
        //religion others
        $appl_religion_others = new Zend_Form_Element_Text('appl_religion_others');
	$appl_religion_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //nationality
        $appl_nationality = new Zend_Form_Element_Select('appl_nationality');
	$appl_nationality->removeDecorator("DtDdWrapper");
        $appl_nationality->setAttrib('class', 'select');
        $appl_nationality->setAttrib('onchange', 'disableValueTypeNationality(this.value);');
        $appl_nationality->setAttrib('id', 'appl_nationality');
	$appl_nationality->removeDecorator("Label");
        $appl_nationality->setAttrib('required', true);
        
        $countryModel = new GeneralSetup_Model_DbTable_Countrymaster();
        
        $countryList = $countryModel->fetchAll(null, 'CountryName');
        
        $appl_nationality->addMultiOption('','-- Select --');
        
        if (count($countryList) > 0){
            foreach ($countryList as $countryLoop){
                $appl_nationality->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //nationality type
        $appl_type_nationality = new Zend_Form_Element_Select('appl_type_nationality');
	$appl_type_nationality->removeDecorator("DtDdWrapper");
        $appl_type_nationality->setAttrib('class', 'select');
        $appl_type_nationality->setAttrib('id', 'appl_type_nationality');
	$appl_type_nationality->removeDecorator("Label");
        $appl_type_nationality->setAttrib('required', true);
        
        $nationalityTypeList = $defModel->getDataByType(113);
        
        $appl_type_nationality->addMultiOption('','-- Select --');
        
        if (count($nationalityTypeList) > 0){
            foreach ($nationalityTypeList as $nationalityTypeLoop){
                $appl_type_nationality->addMultiOption($nationalityTypeLoop['idDefinition'], $nationalityTypeLoop['DefinitionDesc']);
            }
        }
        
        //race
        $appl_race = new Zend_Form_Element_Select('appl_race');
	$appl_race->removeDecorator("DtDdWrapper");
        $appl_race->setAttrib('class', 'select');
        $appl_race->setAttrib('id', 'appl_race');
	$appl_race->removeDecorator("Label");
        //$appl_race->setAttrib('required', true);
        
        $raceList = $defModel->getDataByType(60);
        
        $appl_race->addMultiOption('', '-- Select --');
        
        if (count($raceList) > 0){
            foreach ($raceList as $raceLoop){
                $appl_race->addMultiOption($raceLoop['idDefinition'], $raceLoop['DefinitionDesc']);
            }
        }
        
        //bumiputra eligibility
        $appl_bumiputera = new Zend_Form_Element_Select('appl_bumiputera');
	$appl_bumiputera->removeDecorator("DtDdWrapper");
        $appl_bumiputera->setAttrib('class', 'select');
        $appl_bumiputera->setAttrib('id', 'appl_bumiputera');
	$appl_bumiputera->removeDecorator("Label");
        //$appl_bumiputera->setAttrib('required', true);
        
        $bumiputeraList = $defModel->getDataByType(102);
        
        $appl_bumiputera->addMultiOption('', '-- Select --');
        
        if (count($bumiputeraList) > 0){
            foreach ($bumiputeraList as $bumiputeraLoop){
                $appl_bumiputera->addMultiOption($bumiputeraLoop['idDefinition'], $bumiputeraLoop['DefinitionDesc']);
            }
        }
        
        //Email
        $appl_email = new Zend_Form_Element_Text('appl_email');
		$appl_email->setAttrib('class', 'input-txt disabled')
			->setAttrib('disabled','disabled')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		//Email Personal
        $appl_email_personal = new Zend_Form_Element_Text('appl_email_personal');
		$appl_email_personal->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
                $appl_email_personal->setAttrib('required', true);
        
        //line 1 (P)
        $appl_address1 = new Zend_Form_Element_Text('appl_address1');
	$appl_address1->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        $appl_address1->setAttrib('required', true);
        
        //line 2 (P)
        $appl_address2 = new Zend_Form_Element_Text('appl_address2');
	$appl_address2->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //line 3(P)
        $appl_address3 = new Zend_Form_Element_Text('appl_address3');
	$appl_address3->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //postcode / zip code
        $appl_postcode = new Zend_Form_Element_Text('appl_postcode');
	$appl_postcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //city
        $appl_city = new Zend_Form_Element_Select('appl_city');
	$appl_city->removeDecorator("DtDdWrapper");
        $appl_city->setAttrib('class', 'select');
        $appl_city->setAttrib('onchange', 'cityOthers(this.value);');
        $appl_city->setAttrib('id', 'appl_city');
	$appl_city->removeDecorator("Label");
        
        $appl_city->addMultiOption('', '-- Select --');
        
        if ($stateId != null){
            $cityList = $model->getCity($stateId);
            if ($cityList){
                foreach ($cityList as $cityLoop){
                    $appl_city->addMultiOption($cityLoop['idCity'], $cityLoop['CityName']);
                }
            }
        }
        
        $appl_city->addMultiOption(99, 'Others');
        
        //city others
        $appl_city_others = new Zend_Form_Element_Text('appl_city_others');
	$appl_city_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //country
        $appl_country = new Zend_Form_Element_Select('appl_country');
	$appl_country->removeDecorator("DtDdWrapper");
        $appl_country->setAttrib('class', 'select');
        $appl_country->setAttrib('id', 'appl_country');
	$appl_country->removeDecorator("Label");
        $appl_country->setAttrib('onchange', 'get_state(this.value, "appl_state");');
        $appl_country->setAttrib('required', true);
        
        $appl_country->addMultiOption('', '-- Select --');
        
        if (count($countryList) > 0){
            foreach ($countryList as $countryLoop){
                $appl_country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //state
        $appl_state = new Zend_Form_Element_Select('appl_state');
	$appl_state->removeDecorator("DtDdWrapper");
        $appl_state->setAttrib('class', 'select');
        $appl_state->setAttrib('id', 'appl_state');
	$appl_state->removeDecorator("Label");
        $appl_state->setAttrib('onchange', 'get_city(this.value, "appl_city"); stateOthers(this.value);');
        
        $appl_state->addMultiOption('', '-- Select --');
        
        if ($countryId!=null){
            $stateList = $model->getState($countryId);
            if ($stateList){
                foreach ($stateList as $stateLoop){
                    $appl_state->addMultiOption($stateLoop['idState'], $stateLoop['StateName']);
                }
            }
        }
        
        $appl_state->addMultiOption(99, 'Others');
        
        //state others
        $appl_state_others = new Zend_Form_Element_Text('appl_state_others');
	$appl_state_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //line 1 (C)
        $appl_caddress1 = new Zend_Form_Element_Text('appl_caddress1');
	$appl_caddress1->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //line 2 (C)
        $appl_caddress2 = new Zend_Form_Element_Text('appl_caddress2');
	$appl_caddress2->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //line 3(C)
        $appl_caddress3 = new Zend_Form_Element_Text('appl_caddress3');
	$appl_caddress3->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspondent postcode / zip code
        $appl_cpostcode = new Zend_Form_Element_Text('appl_cpostcode');
	$appl_cpostcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspodent city
        $appl_ccity = new Zend_Form_Element_Select('appl_ccity');
	$appl_ccity->removeDecorator("DtDdWrapper");
        $appl_ccity->setAttrib('class', 'select');
        $appl_ccity->setAttrib('onchange', 'ccityOthers(this.value);');
        $appl_ccity->setAttrib('id', 'appl_ccity');
	$appl_ccity->removeDecorator("Label");
        
        $appl_ccity->addMultiOption('', '-- Select --');
        
        if ($cstateId != null){
            $cityList = $model->getCity($cstateId);
            if ($cityList){
                foreach ($cityList as $cityLoop){
                    $appl_ccity->addMultiOption($cityLoop['idCity'], $cityLoop['CityName']);
                }
            }
        }
        
        $appl_ccity->addMultiOption(99, 'Others');
        
        //correspodent city others
        $appl_ccity_others = new Zend_Form_Element_Text('appl_ccity_others');
	$appl_ccity_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspondent country
        $appl_ccountry = new Zend_Form_Element_Select('appl_ccountry');
	$appl_ccountry->removeDecorator("DtDdWrapper");
        $appl_ccountry->setAttrib('class', 'select');
        $appl_ccountry->setAttrib('id', 'appl_ccountry');
        $appl_ccountry->setAttrib('onchange', 'get_state(this.value, "appl_cstate");');
	$appl_ccountry->removeDecorator("Label");
        
        $appl_ccountry->addMultiOption('', '-- Select --');
        
        if (count($countryList) > 0){
            foreach ($countryList as $countryLoop){
                $appl_ccountry->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //correspondent state
        $appl_cstate = new Zend_Form_Element_Select('appl_cstate');
	$appl_cstate->removeDecorator("DtDdWrapper");
        $appl_cstate->setAttrib('class', 'select');
        $appl_cstate->setAttrib('id', 'appl_cstate');
        $appl_cstate->setAttrib('onchange', 'get_city(this.value, "appl_ccity"); cstateOthers(this.value);');
	$appl_cstate->removeDecorator("Label");
        
        $appl_cstate->addMultiOption('', '-- Select --');
        
        if ($ccountryId!=null){
            $stateList = $model->getState($ccountryId);
            if ($stateList){
                foreach ($stateList as $stateLoop){
                    $appl_cstate->addMultiOption($stateLoop['idState'], $stateLoop['StateName']);
                }
            }
        }
        
        $appl_cstate->addMultiOption(99, 'Others');
        
        //correspondent state others
        $appl_cstate_others = new Zend_Form_Element_Text('appl_cstate_others');
	$appl_cstate_others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //permanent phone number
        $appl_phone_home = new Zend_Form_Element_Text('appl_phone_home');
	$appl_phone_home->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //permanent mobile
        $appl_phone_mobile = new Zend_Form_Element_Text('appl_phone_mobile');
	$appl_phone_mobile->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //permanent phone office
        $appl_phone_office = new Zend_Form_Element_Text('appl_phone_office');
	$appl_phone_office->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //permanent fax
        $appl_fax = new Zend_Form_Element_Text('appl_fax');
	$appl_fax->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspondent phone number
        $appl_cphone_home = new Zend_Form_Element_Text('appl_cphone_home');
	$appl_cphone_home->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspondent mobile
        $appl_cphone_mobile = new Zend_Form_Element_Text('appl_cphone_mobile');
	$appl_cphone_mobile->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspondent phone office
        $appl_cphone_office = new Zend_Form_Element_Text('appl_cphone_office');
	$appl_cphone_office->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //correspondent fax
        $appl_cfax = new Zend_Form_Element_Text('appl_fax');
	$appl_cfax->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $senior_student,
            $appl_salutation,
            $appl_password,
            $appl_fname,
            $appl_lname,
            $appl_idnumber_type,
            $appl_idnumber,
            $appl_passport_expiry,
            $appl_gender,
            $appl_dob,
            $appl_age,
            $appl_marital_status,
            $appl_religion,
            $appl_religion_others,
            $appl_nationality,
            $appl_type_nationality,
            $appl_race,
            $appl_bumiputera,
            $appl_email,
            $appl_email_personal,
            $appl_address1,
            $appl_address2,
            $appl_address3,
            $appl_postcode,
            $appl_city,
            $appl_city_others,
            $appl_country,
            $appl_state,
            $appl_state_others,
            $appl_caddress1,
            $appl_caddress2,
            $appl_caddress3,
            $appl_cpostcode,
            $appl_ccity,
            $appl_ccity_others,
            $appl_ccountry,
            $appl_cstate,
            $appl_cstate_others,
            $appl_phone_home,
            $appl_phone_mobile,
            $appl_phone_office,
            $appl_fax,
            $appl_cphone_home,
            $appl_cphone_mobile,
            $appl_cphone_office,
            $appl_cfax
        ));
    }
}