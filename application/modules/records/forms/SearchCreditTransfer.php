<?php
class Records_Form_SearchCreditTransfer extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        //load model
        $model = new Registration_Model_DbTable_Studentregistration();
        $definationDB = new App_Model_General_DbTable_Definationms();
        
        //program
        $Program = new Zend_Form_Element_Select('Program');
	$Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->setAttrib('id', 'Program');
	$Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getProgramScheme(this.value);');
        
        $Program->addMultiOption('', '-- Select --');
        
        $ProgramList = $model->getProgram();
        
        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $Program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }
        
        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
	$ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
        $ProgramScheme->setAttrib('id', 'ProgramScheme');
	$ProgramScheme->removeDecorator("Label");
        
        $ProgramScheme->addMultiOption('', '-- Select --');
        
        //semester
        $Semester = new Zend_Form_Element_Select('Semester');
	$Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('id', 'Semester');
	$Semester->removeDecorator("Label");
        
        $Semester->addMultiOption('', '-- Select --');
        
        $semesterList = $model->getSemester();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $Semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
            }
        }
        
        //application id
        $ApplicationID = new Zend_Form_Element_Text('ApplicationID');
	$ApplicationID->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //application type
        $AppType = new Zend_Form_Element_Select('AppType');
	$AppType->removeDecorator("DtDdWrapper");
        $AppType->setAttrib('class', 'select');
        //$AppType->setAttrib('onchange', 'getGred(this.value);');
        $AppType->setAttrib('id', 'AppType');
	$AppType->removeDecorator("Label");
        
        $AppType->addMultiOption('', '-- Select --');
        
        $appTypeList = $model->getDefination(153);
        
        if ($appTypeList){
            foreach ($appTypeList as $appTypeLoop){
                $AppType->addMultiOption($appTypeLoop['idDefinition'], $appTypeLoop['DefinitionDesc']);
            }
        }
        
        //id no ic/passport
        $IdNo = new Zend_Form_Element_Text('IdNo');
	$IdNo->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //application status
        $AppStatus = new Zend_Form_Element_Select('AppStatus');
	$AppStatus->removeDecorator("DtDdWrapper");
        $AppStatus->setAttrib('class', 'select');
        $AppStatus->setAttrib('id', 'AppStatus');
	$AppStatus->removeDecorator("Label");
        
        $AppStatus->addMultiOption('', '-- Select --');
        
        $appStatusList = $definationDB->getDataByType(155);
        
        if ($appStatusList){
            foreach ($appStatusList as $appStatusLoop){
                $AppStatus->addMultiOption($appStatusLoop['idDefinition'], $appStatusLoop['DefinitionDesc']);
            }
        }
        
        //date from
        $DateFrom = new Zend_Form_Element_Text('DateFrom');
	$DateFrom->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //date to
        $DateTo = new Zend_Form_Element_Text('DateTo');
	$DateTo->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student id
        $StudentId = new Zend_Form_Element_Text('StudentId');
	$StudentId->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student name
        $StudentName = new Zend_Form_Element_Text('StudentName');
	$StudentName->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student category
        $StudentCategory = new Zend_Form_Element_Select('StudentCategory');
	$StudentCategory->removeDecorator("DtDdWrapper");
        $StudentCategory->setAttrib('class', 'select');
        $StudentCategory->setAttrib('id', 'StudentCategory');
	$StudentCategory->removeDecorator("Label");
        
        $StudentCategory->addMultiOption('', '-- Select --');
        
        $studCatList = $definationDB->getDataByType(95);
        
        if ($studCatList){
            foreach ($studCatList as $studCatLoop){
                $StudentCategory->addMultiOption($studCatLoop['idDefinition'], $studCatLoop['DefinitionDesc']);
            }
        }
		
        $this->addElements(array(
            $Program,
            $ProgramScheme,
            $Semester,
            $ApplicationID,
            $AppType,
            $IdNo,
            $AppStatus,
            $DateFrom,
            $DateTo,
            $StudentId,
            $StudentName,
            $StudentCategory
        ));
    }
}
?>
