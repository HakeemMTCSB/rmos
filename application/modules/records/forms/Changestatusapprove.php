<?php
class Records_Form_Changestatusapprove extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $gstrtranslate =Zend_Registry::get('Zend_Translate');

        $Remarks = new Zend_Form_Element_Textarea('Remarks');
        $Remarks->setAttrib('style','width:420px;height:100px;')
        		->setAttrib('required','true')
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');



        /*$Remarks = new Zend_Form_Element_Text('Remarks');
        $Remarks->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Remarks->setAttrib('required',"true");
        $Remarks->setAttrib('maxlength','200');
        $Remarks->removeDecorator("DtDdWrapper");
        $Remarks->removeDecorator("Label");
        $Remarks->removeDecorator('HtmlTag');*/

        $ApprovedBy = new Zend_Form_Element_Hidden('ApprovedBy');
        //$ApprovedBy->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApprovedBy->setAttrib('required',"false");
        $ApprovedBy->setAttrib('maxlength','50');
        $ApprovedBy->removeDecorator("DtDdWrapper");
        $ApprovedBy->removeDecorator("Label");
        $ApprovedBy->removeDecorator('HtmlTag');
        
        $IdEffectiveSemester = new Zend_Form_Element_Hidden('IdEffectiveSemester');
        //$IdEffectiveSemester->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $IdEffectiveSemester->setAttrib('required',"false");
        $IdEffectiveSemester->setAttrib('maxlength','50');
        $IdEffectiveSemester->removeDecorator("DtDdWrapper");
        $IdEffectiveSemester->removeDecorator("Label");
        $IdEffectiveSemester->removeDecorator('HtmlTag');
        
        
        $IdApplyingFor = new Zend_Form_Element_Hidden('IdApplyingFor');
        //$IdApplyingFor->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $IdApplyingFor->setAttrib('required',"false");
        $IdApplyingFor->setAttrib('maxlength','50');
        $IdApplyingFor->removeDecorator("DtDdWrapper");
        $IdApplyingFor->removeDecorator("Label");
        $IdApplyingFor->removeDecorator('HtmlTag');



        
        
        $IdStudentRegistration = new Zend_Form_Element_Hidden('IdStudentRegistration');
        //$IdStudentRegistration->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $IdStudentRegistration->setAttrib('required',"false");
        $IdStudentRegistration->setAttrib('maxlength','50');
        $IdStudentRegistration->removeDecorator("DtDdWrapper");
        $IdStudentRegistration->removeDecorator("Label");
        $IdStudentRegistration->removeDecorator('HtmlTag');
        
        
        $ApprovedByName = new Zend_Form_Element_Text('ApprovedByName');
        $ApprovedByName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApprovedByName->setAttrib('required',"false");
        $ApprovedByName->setAttrib('maxlength','50');
        $ApprovedByName->removeDecorator("DtDdWrapper");
        $ApprovedByName->removeDecorator("Label");
        $ApprovedByName->removeDecorator('HtmlTag');




        $DateApproved = new Zend_Form_Element_Text('DateApproved');
        $DateApproved->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $DateApproved->setAttrib('required',"false");
        $DateApproved->setAttrib('maxlength','50');
        $DateApproved->removeDecorator("DtDdWrapper");
        $DateApproved->removeDecorator("Label");
        $DateApproved->removeDecorator('HtmlTag');
        
        $IdChangeStatusApplication  = new Zend_Form_Element_Hidden('IdChangeStatusApplication');
    	$IdChangeStatusApplication->removeDecorator("DtDdWrapper");
    	$IdChangeStatusApplication->setAttrib('dojoType',"dijit.form.TextBox");
    	$IdChangeStatusApplication->removeDecorator("Label");
    	$IdChangeStatusApplication->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
    	$UpdUser->removeDecorator("DtDdWrapper");
    	$UpdUser->setAttrib('dojoType',"dijit.form.TextBox");
    	$UpdUser->removeDecorator("Label");
    	$UpdUser->removeDecorator('HtmlTag');
        
        $UpdDate  = new Zend_Form_Element_Hidden('UpdDate');
    	$UpdDate->removeDecorator("DtDdWrapper");
    	$UpdDate->setAttrib('dojoType',"dijit.form.TextBox");
    	$UpdDate->removeDecorator("Label");
    	$UpdDate->removeDecorator('HtmlTag');

        $Approve = new Zend_Form_Element_Submit('Approve');
	$Approve->label = $gstrtranslate->_("Approve");
	$Approve->dojotype="dijit.form.Button";
	$Approve->removeDecorator("DtDdWrapper");
	$Approve->removeDecorator('HtmlTag');
	$Approve->class = "NormalBtn";
        $Approve->setAttrib('onclick','return validateApproval()');

        $DisApprove = new Zend_Form_Element_Submit('DisApprove');
		$DisApprove->label = $gstrtranslate->_("Disapprove");
		$DisApprove->dojotype="dijit.form.Button";
		$DisApprove->removeDecorator("DtDdWrapper");
		$DisApprove->removeDecorator('HtmlTag')
		->class = "manualbtn";
        $DisApprove->setAttrib('onclick','return validateApproval()');

        $Revert = new Zend_Form_Element_Submit('Revert');
		$Revert->label = $gstrtranslate->_("Revert");
		$Revert->dojotype="dijit.form.Button";
		$Revert->removeDecorator("DtDdWrapper");
		$Revert->removeDecorator('HtmlTag')
		->class = "NormalBtn";

        $this->addElements(array($Remarks,$Approve,$DisApprove,$ApprovedBy,$DateApproved,
                            $IdChangeStatusApplication,$UpdUser,$UpdDate,$ApprovedByName,
            $IdEffectiveSemester,$IdStudentRegistration,$Revert,$IdApplyingFor
        		));








    }
}

?>
