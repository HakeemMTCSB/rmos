<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/10/2015
 * Time: 9:59 AM
 */
class Records_Form_IdCollectionSearch extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');

        //load model
        $model = new Registration_Model_DbTable_Studentregistration();
        $definationDB = new App_Model_General_DbTable_Definationms();
        $model2 = new Records_Model_DbTable_Report();
        $lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();

        //program
        $Program = new Zend_Form_Element_Select('Program');
        $Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->setAttrib('required', true);
        $Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getProgramScheme(this.value);');

        $Program->addMultiOption('', '-- Select --');

        $ProgramList = $model->getProgram();

        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $Program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }

        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
        $ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
        $ProgramScheme->setAttrib('multiple', true);
        $ProgramScheme->removeDecorator("Label");

        //$ProgramScheme->addMultiOption('', '-- Select --');

        if ($programid){
            $programSchemeList = $model2->getProgramScheme($programid);

            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $ProgramScheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }

        //Intake
        $Intake = new Zend_Form_Element_Select('Intake');
        $Intake->removeDecorator("DtDdWrapper");
        $Intake->setAttrib('class', 'select');
        $Intake->setAttrib('multiple', true);
        $Intake->removeDecorator("Label");

        $intakeList = $model2->getIntake();

        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $Intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }

        //Method
        $Method = new Zend_Form_Element_Select('Method');
        $Method->removeDecorator("DtDdWrapper");
        $Method->setAttrib('class', 'select');
        $Method->setAttrib('multiple', true);
        $Method->removeDecorator("Label");

        //$Method->addMultiOption('', '-- Select --');
        $Method->addMultiOption(1, 'Self Collect');
        $Method->addMultiOption(2, 'Courier');

        //Status
        $Status = new Zend_Form_Element_Select('Status');
        $Status->removeDecorator("DtDdWrapper");
        $Status->setAttrib('class', 'select');
        $Status->setAttrib('multiple', true);
        $Status->removeDecorator("Label");

        $Status->addMultiOption(0, 'Not Send/Collect');
        $Status->addMultiOption(1, 'Sent/Collected');

        //student name
        $Name = new Zend_Form_Element_Text('Name');
        $Name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //student id
        $Studentid = new Zend_Form_Element_Text('Studentid');
        $Studentid->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $Program,
            $ProgramScheme,
            $Intake,
            $Method,
            $Status,
            $Name,
            $Studentid
        ));
    }
}