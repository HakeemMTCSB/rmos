<?php
class Records_Form_Studentprofile extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdApplication = new Zend_Form_Element_Hidden('IdApplication');
        $IdApplication->removeDecorator("DtDdWrapper");
        $IdApplication->removeDecorator("Label");
        $IdApplication->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $FName = new Zend_Form_Element_Text('FName');
		$FName	->setAttrib('required',"true")					
					->setAttrib('dojoType',"dijit.form.ValidationTextBox")
					->setAttrib('maxlength','50')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$MName = new Zend_Form_Element_Text('MName');
		$MName	->setAttrib('maxlength','50')
					->setAttrib('dojoType',"dijit.form.ValidationTextBox")
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');

		$LName = new Zend_Form_Element_Text('LName');
		$LName	->setAttrib('required',"true")					
					->setAttrib('dojoType',"dijit.form.ValidationTextBox")
					->setAttrib('maxlength','50')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');	

		$ICNumber = new Zend_Form_Element_Text('ICNumber');
		$ICNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ICNumber->setAttrib('required',"false")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');	

        $DateOfBirth = new Zend_Dojo_Form_Element_DateTextBox('DateOfBirth');
		$DateOfBirth->setAttrib('required',"true")
						->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('title',"dd-mm-yyyy")		 				
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');		
						
   		$Nationality = new Zend_Dojo_Form_Element_FilteringSelect('Nationality');
	    $Nationality->removeDecorator("DtDdWrapper");
        $Nationality->removeDecorator("Label");
        $Nationality->removeDecorator('HtmlTag');
        $Nationality->setAttrib('required',"true");
        $Nationality->setRegisterInArrayValidator(false);
		$Nationality->setAttrib('dojoType',"dijit.form.FilteringSelect");

        $Landscape = new Zend_Dojo_Form_Element_FilteringSelect('Landscape');
	    $Landscape->removeDecorator("DtDdWrapper");
        $Landscape->removeDecorator("Label");
        $Landscape->removeDecorator('HtmlTag');
        $Landscape->setAttrib('required',"true");
        $Landscape->setRegisterInArrayValidator(false);
	    $Landscape->setAttrib('dojoType',"dijit.form.FilteringSelect");			
		
		$Gender  = new Zend_Form_Element_Radio('Gender');
        $Gender->addMultiOptions(array('1' => 'Male','0' => 'Female'))
        				->setvalue('1')
        				->setSeparator('&nbsp;')
        				->setAttrib('dojoType',"dijit.form.RadioButton")
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');	
        				
        $EmailAddress = new Zend_Form_Element_Text('EmailAddress',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$EmailAddress->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $EmailAddress->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $PermAddressDetails = new Zend_Form_Element_Text('PermAddressDetails');
		$PermAddressDetails	->setAttrib('required',"true")						
								->setAttrib('dojoType',"dijit.form.ValidationTextBox")
								->setAttrib('maxlength','100')
								->removeDecorator("DtDdWrapper")
								->removeDecorator("Label") 					
								->removeDecorator('HtmlTag');	
		$PermCity = new Zend_Dojo_Form_Element_FilteringSelect('PermCity');
        $PermCity->removeDecorator("DtDdWrapper");
        $PermCity->removeDecorator("Label");
        $PermCity->removeDecorator('HtmlTag');
        $PermCity->setAttrib('required',"true");
        $PermCity->setRegisterInArrayValidator(false);
		$PermCity->setAttrib('dojoType',"dijit.form.FilteringSelect");	
							
		$PermCountry = new Zend_Form_Element_Select('PermCountry');
        $PermCountry->setAttrib('required',"true")		
						->setAttrib('dojoType',"dijit.form.FilteringSelect")
						->setAttrib('OnChange', "fnGetCountryStateList(this,'PermState')")
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
							
		$PermState = new Zend_Form_Element_Select('PermState');
        $PermState	->setAttrib('required',"true")		
						->setAttrib('dojoType',"dijit.form.FilteringSelect")
						->setRegisterInArrayValidator(false)
						->setAttrib('OnChange', 'fnGetStateCityListPerm')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
		$PermZip = new Zend_Form_Element_Text('PermZip');
		$PermZip	->setAttrib('required',"true")
						->setAttrib('maxlength','10')
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")	
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');	
		$CorrsAddressDetails = new Zend_Form_Element_Text('CorrsAddressDetails');
		$CorrsAddressDetails	->setAttrib('maxlength','100')
									->removeDecorator("DtDdWrapper")
									->setAttrib('dojoType',"dijit.form.ValidationTextBox")
									->removeDecorator("Label") 
									->removeDecorator("DtDdWrapper")
									->removeDecorator('HtmlTag');	
		$CorrsCity = new Zend_Dojo_Form_Element_FilteringSelect('CorrsCity');
        $CorrsCity->removeDecorator("DtDdWrapper");
       	$CorrsCity->removeDecorator("Label");
        $CorrsCity->removeDecorator('HtmlTag');
        $CorrsCity->setAttrib('required',"false");
        $CorrsCity->setRegisterInArrayValidator(false);
		$CorrsCity->setAttrib('dojoType',"dijit.form.FilteringSelect");
				
		$CorrsCountry = new Zend_Form_Element_Select('CorrsCountry');
        $CorrsCountry->setAttrib('dojoType',"dijit.form.FilteringSelect")
							->setAttrib('OnChange', "fnGetCorresCountryStateList(this,'CorrsState')")
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							
		$CorrsState = new Zend_Form_Element_Select('CorrsState');
        $CorrsState ->setRegisterInArrayValidator(false)
						->setAttrib('dojoType',"dijit.form.FilteringSelect")
						->setAttrib('OnChange', 'fnGetStateCityListCorrs')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');	
						
		$CorrsZip = new Zend_Form_Element_Text('CorrsZip');
		$CorrsZip	->setAttrib('maxlength','20')
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")				
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');	
		$HomePhonecountrycode = new Zend_Form_Element_Text('HomePhonecountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$HomePhonecountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $HomePhonecountrycode->setAttrib('maxlength','3');  
        $HomePhonecountrycode->setAttrib('style','width:30px');  
        $HomePhonecountrycode->removeDecorator("DtDdWrapper");
        $HomePhonecountrycode->removeDecorator("Label");
        $HomePhonecountrycode->removeDecorator('HtmlTag');
        
        $HomePhonestatecode = new Zend_Form_Element_Text('HomePhonestatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$HomePhonestatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $HomePhonestatecode->setAttrib('maxlength','2');
        $HomePhonestatecode->setAttrib('style','width:30px');  
        $HomePhonestatecode->removeDecorator("DtDdWrapper");
        $HomePhonestatecode->removeDecorator("Label");
        $HomePhonestatecode->removeDecorator('HtmlTag');
        				
		$HomePhone = new Zend_Form_Element_Text('HomePhone',array('regExp'=>'[\0-9()+-]+'));
		$HomePhone->setAttrib('maxlength','9')
				  		->setAttrib('style','width:93px') 
						->removeDecorator("DtDdWrapper")
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');
		$CellPhonecountrycode = new Zend_Form_Element_Text('CellPhonecountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$CellPhonecountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $CellPhonecountrycode->setAttrib('maxlength','3');  
        $CellPhonecountrycode->setAttrib('style','width:30px');  
        $CellPhonecountrycode->removeDecorator("DtDdWrapper");
        $CellPhonecountrycode->removeDecorator("Label");
        $CellPhonecountrycode->removeDecorator('HtmlTag');
        
        $CellPhonestatecode = new Zend_Form_Element_Text('CellPhonestatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$CellPhonestatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $CellPhonestatecode->setAttrib('maxlength','5');  
        $CellPhonestatecode->setAttrib('style','width:30px');  
        $CellPhonestatecode->removeDecorator("DtDdWrapper");
        $CellPhonestatecode->removeDecorator("Label");
        $CellPhonestatecode->removeDecorator('HtmlTag');
        																																																						
		$CellPhone = new Zend_Form_Element_Text('CellPhone',array('regExp'=>'[\0-9()+-]+'));
		$CellPhone	->setAttrib('maxlength','20')
						->setAttrib('style','width:93px')
						->removeDecorator("DtDdWrapper")
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');
		$Faxcountrycode = new Zend_Form_Element_Text('Faxcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$Faxcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Faxcountrycode->setAttrib('maxlength','3');  
        $Faxcountrycode->setAttrib('style','width:30px');  
        $Faxcountrycode->removeDecorator("DtDdWrapper");
        $Faxcountrycode->removeDecorator("Label");
        $Faxcountrycode->removeDecorator('HtmlTag');
        
        $Faxstatecode = new Zend_Form_Element_Text('Faxstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$Faxstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Faxstatecode->setAttrib('maxlength','5');  
        $Faxstatecode->setAttrib('style','width:30px');  
        $Faxstatecode->removeDecorator("DtDdWrapper");
        $Faxstatecode->removeDecorator("Label");
        $Faxstatecode->removeDecorator('HtmlTag');
        				
		$Fax = new Zend_Form_Element_Text('Fax',array('regExp'=>'[\0-9()+-]+'));
		$Fax	->setAttrib('maxlength','20')
						->setAttrib('style','width:93px')
						->removeDecorator("DtDdWrapper")
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');					
       
		$Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		  		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
										
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
			
        //form elements
        $this->addElements(array($IdApplication,
        						  $UpdDate,
        						  $UpdUser,
        						  $FName,
        						  $MName,
        						  $LName,
        						  $ICNumber,
        						  $DateOfBirth,
        						  $Nationality,
        						  $Gender,
        						  $EmailAddress,
        						  $PermAddressDetails,
        						  $PermCity,
        						  $PermCountry,
        						  $PermState,
        						  $PermZip,
        						  $CorrsAddressDetails,
        						  $CorrsCity,
        						  $CorrsCountry,
        						  $CorrsState,
        						  $CorrsZip,
        						  $HomePhonecountrycode,
        						  $HomePhonestatecode,
        						  $HomePhone,
        						  $CellPhonecountrycode,
        						  $CellPhonestatecode,
        						  $CellPhone,
        						  $Faxcountrycode,
        						  $Faxstatecode,
								  $Landscape,
        						  $Fax,
        						  $Save,
        						  $Clear,
        						  $Add
                                 ));

    }
}