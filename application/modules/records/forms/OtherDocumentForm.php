<?php
class Records_Form_OtherDocumentForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //comment type
        $sod_type = new Zend_Form_Element_Select('sod_type');
	$sod_type->removeDecorator("DtDdWrapper");
        $sod_type->setAttrib('class', 'select');
        $sod_type->setAttrib('required', 'true');
	$sod_type->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $DocTypeList = $defModel->getDataByType(173);
        
        $sod_type->addMultiOption('', '-- Select --');
        
        if ($DocTypeList){
            foreach ($DocTypeList as $DocTypeListLoop){
                $sod_type->addMultiOption($DocTypeListLoop['idDefinition'], $DocTypeListLoop['DefinitionDesc']);
            }
        }
        
        //comment description
        $sod_remarks = new Zend_Form_Element_Textarea('sod_remarks');
        $sod_remarks->setAttrib('rows', '5');
        $sod_remarks->setAttrib('required', 'true');
	$sod_remarks->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $sod_type,
            $sod_remarks
        ));
    }
}