<?php
class Records_Form_Changeprogram extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$lobjdeftype = new App_Model_Definitiontype();
        $lobjsemester = new GeneralSetup_Model_DbTable_Semester();
        $profilestatusList = $lobjdeftype->fnGetDefinationMs("Student Status");

        $semesterList = $lobjsemester->getAllsemesterList();
    	
    	$Name = new Zend_Form_Element_Text('Name');
        $Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Name->setAttrib('required',"false");
        $Name->setAttrib('maxlength','50');
        $Name->removeDecorator("DtDdWrapper");
        $Name->removeDecorator("Label");
        $Name->removeDecorator('HtmlTag');
        
        $StudentId = new Zend_Form_Element_Text('StudentId');
        $StudentId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $StudentId->setAttrib('required',"false");
        $StudentId->setAttrib('maxlength','50');
        $StudentId->removeDecorator("DtDdWrapper");
        $StudentId->removeDecorator("Label");
        $StudentId->removeDecorator('HtmlTag');
        
        $BeforeTotalCreditHours = new Zend_Form_Element_Text('BeforeTotalCreditHours');
        $BeforeTotalCreditHours->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $BeforeTotalCreditHours->setAttrib('required',"false");
        $BeforeTotalCreditHours->setAttrib('maxlength','50');
        $BeforeTotalCreditHours->removeDecorator("DtDdWrapper");
        $BeforeTotalCreditHours->removeDecorator("Label");
        $BeforeTotalCreditHours->removeDecorator('HtmlTag');
        
        $AfterTotalCreditHours = new Zend_Form_Element_Text('AfterTotalCreditHours');
        $AfterTotalCreditHours->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AfterTotalCreditHours->setAttrib('required',"false");
        $AfterTotalCreditHours->setAttrib('maxlength','50');
        $AfterTotalCreditHours->removeDecorator("DtDdWrapper");
        $AfterTotalCreditHours->removeDecorator("Label");
        $AfterTotalCreditHours->removeDecorator('HtmlTag');
        
        $TotalCreditHours = new Zend_Form_Element_Text('TotalCreditHours');
        $TotalCreditHours->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TotalCreditHours->setAttrib('required',"false");
        $TotalCreditHours->setAttrib('maxlength','50');
        $TotalCreditHours->removeDecorator("DtDdWrapper");
        $TotalCreditHours->removeDecorator("Label");
        $TotalCreditHours->removeDecorator('HtmlTag');
        
        $IdProfileStatus = new Zend_Dojo_Form_Element_FilteringSelect('IdProfileStatus');
        $IdProfileStatus->removeDecorator("DtDdWrapper");
        $IdProfileStatus->removeDecorator("Label");
        $IdProfileStatus->removeDecorator('HtmlTag');
        $IdProfileStatus->setAttrib('required',"false");
        $IdProfileStatus->setRegisterInArrayValidator(false);        
		$IdProfileStatus->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdProfileStatus->addmultioptions($profilestatusList);
        
        $IdSemester = new Zend_Dojo_Form_Element_FilteringSelect('IdSemester');
        $IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->removeDecorator("Label");
        $IdSemester->removeDecorator('HtmlTag');
        $IdSemester->setAttrib('required',"false");
        $IdSemester->setRegisterInArrayValidator(false);        
		$IdSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdSemester->addmultioptions($semesterList);

        $IdNric = new Zend_Form_Element_Text('IdNric');
        $IdNric->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $IdNric->setAttrib('required',"false");
        $IdNric->setAttrib('maxlength','50');
        $IdNric->removeDecorator("DtDdWrapper");
        $IdNric->removeDecorator("Label");
        $IdNric->removeDecorator('HtmlTag');
    	
    	$Reason = new Zend_Form_Element_Textarea('Reason');
		//$Reason->setAttrib('dojoType',"dijit.form.ValidationTextarea");
        $Reason->setAttrib('style','width:180px;height:100px;') 
        		->setAttrib('required','true')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $Remarks = new Zend_Form_Element_Textarea('Remarks');
        $Remarks->setAttrib('style','width:420px;height:100px;') 
        		->setAttrib('required','true')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$EffectiveSemester = new Zend_Dojo_Form_Element_FilteringSelect('EffectiveSemester');	
        $EffectiveSemester->setAttrib('required',"true");
        $EffectiveSemester->removeDecorator("DtDdWrapper");
        $EffectiveSemester ->removeDecorator("Label");
        $EffectiveSemester->removeDecorator('HtmlTag');
		$EffectiveSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$CurrentProgram = new Zend_Form_Element_Text('CurrentProgram');
        $CurrentProgram->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $CurrentProgram->setAttrib('required',"false");
        $CurrentProgram->setAttrib('maxlength','50');
        $CurrentProgram->removeDecorator("DtDdWrapper");
        $CurrentProgram->removeDecorator("Label");
        $CurrentProgram->removeDecorator('HtmlTag');
        
        $CurrentProgramId  = new Zend_Form_Element_Hidden('CurrentProgramId');
    	$CurrentProgramId->removeDecorator("DtDdWrapper");
    	$CurrentProgramId->setAttrib('dojoType',"dijit.form.TextBox");
    	$CurrentProgramId->removeDecorator("Label");
    	$CurrentProgramId->removeDecorator('HtmlTag');
		
		$NewProgram = new Zend_Dojo_Form_Element_FilteringSelect('NewProgram');	
        $NewProgram->setAttrib('required',"true");
        $NewProgram->removeDecorator("DtDdWrapper");
        $NewProgram ->removeDecorator("Label");
        $NewProgram->removeDecorator('HtmlTag');
		$NewProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Date = new Zend_Form_Element_Text('Date');
        $Date->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Date->setAttrib('required',"false");
        $Date->setAttrib('readonly',"true");
        $Date->setAttrib('maxlength','50');
        $Date->removeDecorator("DtDdWrapper");
        $Date->removeDecorator("Label");
        $Date->removeDecorator('HtmlTag');
 
        $AppliedBy  = new Zend_Form_Element_Hidden('AppliedBy');
    	$AppliedBy->removeDecorator("DtDdWrapper");
    	$AppliedBy->setAttrib('dojoType',"dijit.form.TextBox");
    	$AppliedBy->removeDecorator("Label");
    	$AppliedBy->removeDecorator('HtmlTag');
        
        $AppliedByName = new Zend_Form_Element_Text('AppliedByName');
        $AppliedByName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AppliedByName->setAttrib('required',"false");
        $AppliedByName->setAttrib('readonly',"true");
        $AppliedByName->setAttrib('maxlength','50');
        $AppliedByName->removeDecorator("DtDdWrapper");
        $AppliedByName->removeDecorator("Label");
        $AppliedByName->removeDecorator('HtmlTag');
        
        $ApproveDate = new Zend_Form_Element_Text('ApproveDate');
        $ApproveDate->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApproveDate->setAttrib('required',"false");
        $ApproveDate->setAttrib('readonly',"true");
        $ApproveDate->setAttrib('maxlength','50');
        $ApproveDate->removeDecorator("DtDdWrapper");
        $ApproveDate->removeDecorator("Label");
        $ApproveDate->removeDecorator('HtmlTag');
 
        $ApproveBy  = new Zend_Form_Element_Hidden('ApproveBy');
    	$ApproveBy->removeDecorator("DtDdWrapper");
    	$ApproveBy->setAttrib('dojoType',"dijit.form.TextBox");
    	$ApproveBy->removeDecorator("Label");
    	$ApproveBy->removeDecorator('HtmlTag');
        
    	$IdStudentreg = new Zend_Form_Element_Hidden('IdStudentreg');
    	$IdStudentreg->removeDecorator("DtDdWrapper");
    	$IdStudentreg->setAttrib('dojoType',"dijit.form.TextBox");
    	$IdStudentreg->removeDecorator("Label");
    	$IdStudentreg->removeDecorator('HtmlTag');
    	
        $ApproveByName = new Zend_Form_Element_Text('ApproveByName');
        $ApproveByName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApproveByName->setAttrib('required',"false");
        $ApproveByName->setAttrib('readonly',"true");
        $ApproveByName->setAttrib('maxlength','50');
        $ApproveByName->removeDecorator("DtDdWrapper");
        $ApproveByName->removeDecorator("Label");
        $ApproveByName->removeDecorator('HtmlTag');
        
        $ApplicationStatus  = new Zend_Form_Element_Hidden('ApplicationStatus');
    	$ApplicationStatus->removeDecorator("DtDdWrapper");
    	$ApplicationStatus->setAttrib('dojoType',"dijit.form.TextBox");
    	$ApplicationStatus->removeDecorator("Label");
    	$ApplicationStatus->removeDecorator('HtmlTag');

        $ApplicationStatusByName = new Zend_Form_Element_Text('ApplicationStatusByName');
        $ApplicationStatusByName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApplicationStatusByName->setAttrib('required',"false");
        $ApplicationStatusByName->setAttrib('readonly',"true");
        $ApplicationStatusByName->setAttrib('maxlength','50');
        $ApplicationStatusByName->removeDecorator("DtDdWrapper");
        $ApplicationStatusByName->removeDecorator("Label");
        $ApplicationStatusByName->removeDecorator('HtmlTag');
        
        $IdStudent = new Zend_Dojo_Form_Element_FilteringSelect('IdStudent');
        $IdStudent->removeDecorator("DtDdWrapper");
        $IdStudent->removeDecorator("Label");
        $IdStudent->removeDecorator('HtmlTag');
        $IdStudent->setAttrib('required',"false");
        $IdStudent->setRegisterInArrayValidator(false);
		$IdStudent->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdStudent->setAttrib('onChange',"getStudentdetail(this)");
        
        $Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";
		
		$Approve = new Zend_Form_Element_Submit('Approve');
		$Approve->label = $gstrtranslate->_("Approve");
		$Approve->dojotype="dijit.form.Button";
		$Approve->setAttrib('onclick',"return checkapp()");
		$Approve->removeDecorator("DtDdWrapper");
		$Approve->removeDecorator('HtmlTag')
		->class = "NormalBtn";
		
		$Disapprove = new Zend_Form_Element_Submit('Disapprove');
		$Disapprove->label = $gstrtranslate->_("Disapprove");
		$Disapprove->dojotype="dijit.form.Button";
		$Disapprove->setAttrib('onclick',"return checkapp()");
		//$Disapprove->setAttrib('style',"width:190px");
		$Disapprove->removeDecorator("DtDdWrapper");
		$Disapprove->removeDecorator('HtmlTag');
		//->class = "NormalBtn";
		
		$registrationId = new Zend_Form_Element_Text('registrationId');
		$registrationId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$registrationId  ->setAttrib('required',"true")
        				->setAttrib('maxlength','75')
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
		
		//form elements
        $this->addElements(array($NewProgram,
        						  $EffectiveSemester,
        						  $Reason,
        						  $Date,
        						  $AppliedBy,
        						  $ApplicationStatus,
        						  $ApplicationStatusByName,
        						  $AppliedByName,
        						  $Name,
        						  $StudentId,
        						  $IdProfileStatus,
        						  $IdSemester,
        						  $IdNric,
        						  $IdStudent,
        						  $CurrentProgram,
        						  $CurrentProgramId,
        						  $BeforeTotalCreditHours,
        						  $AfterTotalCreditHours,
        						  $TotalCreditHours,
        						  $Save,
        						  $Remarks,
        						  $ApproveDate,
        						  $ApproveBy,
        						  $ApproveByName,
        						  $Approve,
        						  $Disapprove,
        						  $IdStudentreg,
        						  $registrationId
        						  
        						  
        						 ));
    }
}