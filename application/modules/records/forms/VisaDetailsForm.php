<?php
class Records_Form_VisaDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //do you hold visa
        $av_malaysian_visa = new Zend_Form_Element_Radio('av_malaysian_visa');
        $av_malaysian_visa->removeDecorator("DtDdWrapper");
        $av_malaysian_visa->setAttrib('id', 'av_malaysian_visa');
	$av_malaysian_visa->removeDecorator("Label");
        $av_malaysian_visa->addMultiOptions(array(
            '1' => 'Yes',
            '0' => 'No'
	));
        $av_malaysian_visa->setSeparator('&nbsp;&nbsp;');
        $av_malaysian_visa->setValue("0");
        
        //visa expiry date
        $av_expiry = new Zend_Dojo_Form_Element_DateTextBox('av_expiry');
        $av_expiry->setAttrib('dojoType',"dijit.form.DateTextBox");
        $av_expiry->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $av_expiry->removeDecorator("DtDdWrapper");
        $av_expiry->setAttrib('title',"dd-mm-yyyy");
        $av_expiry->removeDecorator("Label");
        $av_expiry->setAttrib('class', 'input-txt datepicker');
        
        //visa status
        $av_status = new Zend_Form_Element_Select('av_status');
	$av_status->removeDecorator("DtDdWrapper");
        $av_status->setAttrib('class', 'select');
        $av_status->setAttrib('id', 'appl_salutation');
	$av_status->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $visaStatusList = $defModel->getDataByType(108);
        
        $av_status->addMultiOption('', '-- Select --');
        
        if (count($visaStatusList) > 0){
            foreach ($visaStatusList as $visaStatusLoop){
                $av_status->addMultiOption($visaStatusLoop['idDefinition'], $visaStatusLoop['DefinitionDesc']);
            }
        }
        
        //form elements
        $this->addElements(array(
            $av_malaysian_visa,
            $av_expiry,
            $av_status
        ));
    }
}