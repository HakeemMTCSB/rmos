<?php
class Records_Form_Studentstatus extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	
    	$IdApplication = new Zend_Form_Element_Hidden('IdApplication');
        $IdApplication->removeDecorator("DtDdWrapper");
        $IdApplication->removeDecorator("Label");
        $IdApplication->removeDecorator('HtmlTag');       
 
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $Action = new Zend_Dojo_Form_Element_FilteringSelect('Action');
	    $Action->removeDecorator("DtDdWrapper");
        $Action->removeDecorator("Label");
        $Action->removeDecorator('HtmlTag');
        $Action->setAttrib('required',"true");
        $Action->setRegisterInArrayValidator(false);
		$Action->setAttrib('dojoType',"dijit.form.FilteringSelect");	
       
		

        $ToEffectiveDate = new Zend_Dojo_Form_Element_DateTextBox('ToEffectiveDate');
		$ToEffectiveDate->setAttrib('required',"true")
						->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('title',"dd-mm-yyyy")		 				
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');		
						
   		$Comments = new Zend_Form_Element_Text('Comments');
		$Comments->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Comments->setAttrib('maxlength','250');  
        $Comments->setAttrib('style','width:170px');  
        $Comments->removeDecorator("DtDdWrapper");
        $Comments->removeDecorator("Label");
        $Comments->removeDecorator('HtmlTag');     
        
						
       
		$Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
		$Clear->label = $gstrtranslate->_("Clear");		
		$Clear	->dojotype="dijit.form.Button";
		$Clear	->class = "NormalBtn";
				
		
				

        //form elements
        $this->addElements(array($IdApplication,
        						  $UpdDate,
        						  $UpdUser,
        						  $Action,
        						  $ToEffectiveDate,
        						  $Comments,
        						  $Save,
        						  $Clear
                                 ));

    }
}