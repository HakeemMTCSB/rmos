<?php
class Records_Form_refereesDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        /*
         * referees 1 section
         */
        
        //referees 1 name
        $r_ref1_name = new Zend_Form_Element_Text('r_ref1_name');
	$r_ref1_name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 position
        $r_ref1_position = new Zend_Form_Element_Text('r_ref1_position');
	$r_ref1_position->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 address1
        $r_ref1_add1 = new Zend_Form_Element_Text('r_ref1_add1');
	$r_ref1_add1->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 address2
        $r_ref1_add2 = new Zend_Form_Element_Text('r_ref1_add2');
	$r_ref1_add2->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 address3
        $r_ref1_add3 = new Zend_Form_Element_Text('r_ref1_add3');
	$r_ref1_add3->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 poscode
        $r_ref1_postcode = new Zend_Form_Element_Text('r_ref1_postcode');
	$r_ref1_postcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //refrees 1 country
        $r_ref1_country = new Zend_Form_Element_Select('r_ref1_country');
	$r_ref1_country->removeDecorator("DtDdWrapper");
        $r_ref1_country->setAttrib('class', 'select');
        $r_ref1_country->setAttrib('id', 'r_ref1_country');
        $r_ref1_country->setAttrib('onchange', 'get_state(this.value, "r_ref1_state");');
	$r_ref1_country->removeDecorator("Label");
        
        $countryModel = new GeneralSetup_Model_DbTable_Countrymaster();
        
        $countryList = $countryModel->fetchAll(null, 'CountryName');
        
        $r_ref1_country->addMultiOption('', '-- Select --');
        
        if (count($countryList) > 0){
            foreach ($countryList as $countryLoop){
                $r_ref1_country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //referees 1 state
        $r_ref1_state = new Zend_Form_Element_Select('r_ref1_state');
	$r_ref1_state->removeDecorator("DtDdWrapper");
        $r_ref1_state->setAttrib('class', 'select');
        $r_ref1_state->setAttrib('id', 'r_ref1_state');
	$r_ref1_state->removeDecorator("Label");
        
        $stateModel = new GeneralSetup_Model_DbTable_State();
        
        $stateList = $stateModel->fetchAll();
        
        $r_ref1_state->addMultiOption('', '-- Select --');
        
        if (count($stateList) > 0){
            foreach ($stateList as $stateLoop){
                $r_ref1_state->addMultiOption($stateLoop['idState'], $stateLoop['StateName']);
            }
        }
        
        //referees 1 city
        $r_ref1_city = new Zend_Form_Element_Text('r_ref1_city');
	$r_ref1_city->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //refrees 1 phone
        $r_ref1_phone = new Zend_Form_Element_Text('r_ref1_phone');
	$r_ref1_phone->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referees 1 fax
        $r_ref1_fax = new Zend_Form_Element_Text('r_ref1_fax');
	$r_ref1_fax->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 email
        $r_ref1_email = new Zend_Form_Element_Text('r_ref1_email');
	$r_ref1_email->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * refrees 2 section
         */
        
        //referees 1 name
        $r_ref2_name = new Zend_Form_Element_Text('r_ref2_name');
	$r_ref2_name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 position
        $r_ref2_position = new Zend_Form_Element_Text('r_ref2_position');
	$r_ref2_position->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 address1
        $r_ref2_add1 = new Zend_Form_Element_Text('r_ref2_add1');
	$r_ref2_add1->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 address2
        $r_ref2_add2 = new Zend_Form_Element_Text('r_ref2_add2');
	$r_ref2_add2->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 address3
        $r_ref2_add3 = new Zend_Form_Element_Text('r_ref2_add3');
	$r_ref2_add3->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 poscode
        $r_ref2_postcode = new Zend_Form_Element_Text('r_ref2_postcode');
	$r_ref2_postcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //refrees 1 country
        $r_ref2_country = new Zend_Form_Element_Select('r_ref2_country');
	$r_ref2_country->removeDecorator("DtDdWrapper");
        $r_ref2_country->setAttrib('class', 'select');
        $r_ref2_country->setAttrib('id', 'r_ref2_country');
        $r_ref2_country->setAttrib('onchange', 'get_state(this.value, "r_ref2_state");');
	$r_ref2_country->removeDecorator("Label");
        
        $r_ref2_country->addMultiOption('', '-- Select --');
        
        if (count($countryList) > 0){
            foreach ($countryList as $countryLoop){
                $r_ref2_country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //referees 1 state
        $r_ref2_state = new Zend_Form_Element_Select('r_ref2_state');
	$r_ref2_state->removeDecorator("DtDdWrapper");
        $r_ref2_state->setAttrib('class', 'select');
        $r_ref2_state->setAttrib('id', 'r_ref2_state');
	$r_ref2_state->removeDecorator("Label");
        
        $r_ref2_state->addMultiOption('', '-- Select --');
        
        if (count($stateList) > 0){
            foreach ($stateList as $stateLoop){
                $r_ref2_state->addMultiOption($stateLoop['idState'], $stateLoop['StateName']);
            }
        }
        
        //referees 1 city
        $r_ref2_city = new Zend_Form_Element_Text('r_ref2_city');
	$r_ref2_city->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //refrees 1 phone
        $r_ref2_phone = new Zend_Form_Element_Text('r_ref2_phone');
	$r_ref2_phone->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referees 1 fax
        $r_ref2_fax = new Zend_Form_Element_Text('r_ref2_fax');
	$r_ref2_fax->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //referess 1 email
        $r_ref2_email = new Zend_Form_Element_Text('r_ref2_email');
	$r_ref2_email->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapp");
        
        //form elements
        $this->addElements(array(
            $r_ref1_name,
            $r_ref1_position,
            $r_ref1_add1,
            $r_ref1_add2,
            $r_ref1_add3,
            $r_ref1_postcode,
            $r_ref1_country,
            $r_ref1_state,
            $r_ref1_city,
            $r_ref1_phone,
            $r_ref1_fax,
            $r_ref1_email,
            $r_ref2_name,
            $r_ref2_position,
            $r_ref2_add1,
            $r_ref2_add2,
            $r_ref2_add3,
            $r_ref2_postcode,
            $r_ref2_country,
            $r_ref2_state,
            $r_ref2_city,
            $r_ref2_phone,
            $r_ref2_fax,
            $r_ref2_email
        ));
    }
}