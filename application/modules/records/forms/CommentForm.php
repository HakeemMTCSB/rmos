<?php
class Records_Form_CommentForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //comment type
        $scm_type = new Zend_Form_Element_Select('scm_type');
	$scm_type->removeDecorator("DtDdWrapper");
        $scm_type->setAttrib('class', 'select');
        $scm_type->setAttrib('id', 'appl_salutation');
	$scm_type->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $commentTypeList = $defModel->getDataByType(146);
        
        $scm_type->addMultiOption('', '-- Select --');
        
        if ($commentTypeList){
            foreach ($commentTypeList as $commentTypeLoop){
                $scm_type->addMultiOption($commentTypeLoop['idDefinition'], $commentTypeLoop['DefinitionDesc']);
            }
        }
        
        //comment description
        $scm_desc = new Zend_Form_Element_Textarea('scm_desc');
        $scm_desc->setAttrib('rows', '5');
	$scm_desc->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $scm_type,
            $scm_desc
        ));
    }
}