<?php
class Records_Form_ApproveCreditTransfer extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $Remarks = new Zend_Form_Element_Textarea('Remarks');
	$Remarks->setAttrib('class', 'span-7')
            ->setAttrib("placeholder", "Remarks")
            ->setAttrib("style","width: 572px; height: 44px;")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $this->addElements(array(
            $Remarks
        ));
    }
}
?>
