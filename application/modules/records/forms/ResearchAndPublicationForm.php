<?php
class Records_Form_ResearchAndPublicationForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //title
        $rpd_title = new Zend_Form_Element_Text('rpd_title');
	$rpd_title->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //date
        $rpd_date = new Zend_Dojo_Form_Element_DateTextBox('rpd_date');
        $rpd_date->setAttrib('dojoType',"dijit.form.DateTextBox");
        $rpd_date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $rpd_date->removeDecorator("DtDdWrapper");
        $rpd_date->setAttrib('title',"dd-mm-yyyy");
        $rpd_date->removeDecorator("Label");
        $rpd_date->setAttrib('class', 'input-txt datepicker');
        
        //publisher
        $rpd_publisher = new Zend_Form_Element_Text('rpd_publisher');
	$rpd_publisher->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $rpd_title,
            $rpd_date,
            $rpd_publisher
        ));
    }
}