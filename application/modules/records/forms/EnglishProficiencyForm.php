<?php
class Records_Form_EnglishProficiencyForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //english proficiency test
        $ep_test = new Zend_Form_Element_Select('ep_test');
	$ep_test->removeDecorator("DtDdWrapper");
        $ep_test->setAttrib('class', 'select');
        $ep_test->setAttrib('onchange', 'getDetail(this.value);');
        $ep_test->setAttrib('id', 'ae_qualification');
	$ep_test->removeDecorator("Label");
        
        //$defModel = new App_Model_General_DbTable_Definationms();
        $spModel = new Records_Model_DbTable_Studentprofile();
        $epTestList = $spModel->getQualification();
        
        $ep_test->addMultiOption('','-- Select --');
        
        if (count($epTestList) > 0){
            foreach ($epTestList as $epTestLoop){
                $ep_test->addMultiOption($epTestLoop['IdQualification'], $epTestLoop['QualificationLevel']);
            }
        }
        
        $ep_test_detail = new Zend_Form_Element_Select('ep_test_detail');
	$ep_test_detail->removeDecorator("DtDdWrapper");
        $ep_test_detail->setAttrib('class', 'select');
        $ep_test_detail->setAttrib('id', 'ae_qualification');
	$ep_test_detail->removeDecorator("Label");
        
        //date test
        $ep_date_taken = new Zend_Dojo_Form_Element_DateTextBox('ep_date_taken');
        $ep_date_taken->setAttrib('dojoType',"dijit.form.DateTextBox");
        $ep_date_taken->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $ep_date_taken->removeDecorator("DtDdWrapper");
        $ep_date_taken->setAttrib('title',"dd-mm-yyyy");
        $ep_date_taken->removeDecorator("Label");
        $ep_date_taken->setAttrib('class', 'input-txt datepicker');
        
        //score
        $ep_score = new Zend_Form_Element_Text('ep_score');
	$ep_score->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $ep_test,
            $ep_date_taken,
            $ep_score
        ));
    }
}