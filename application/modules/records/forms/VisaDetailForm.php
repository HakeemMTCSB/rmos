<?php 

class Records_Form_VisaDetailForm extends Zend_Form
{
		
    protected $_locale;
    protected $_avid;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function setAvid($value2) {
		$this->_avid = $value2;
	}
		
	public function init()
	{
						
			$this->setMethod('post');
			$this->setAttrib('id','form1');
			$this->setAttrib('enctype','multipart/form-data');
			
			if(isset($this->_avid) && $this->_avid!=''){ 
				$this->setAttrib('action', "/records/visa/edit-pass");
			}else{
				$this->setAttrib('action', "/records/visa/add-pass");
			}
	
			$this->addElement('hidden','p_id');
			$this->addElement('hidden','sp_id');
			$this->addElement('hidden','av_id');
			
			//Ref no
			$this->addElement('text','av_reference_no', array(
				//'label'=>$this->getView()->translate('Reference No.'),
				'required'=>true
			));
			if(isset($this->_avid) && $this->_avid!=''){ 
				//$this->av_reference_no->setAttrib('disabled',true);
			}
			
			//issue date
			$this->addElement('text','av_issue_date', array(
			//'label'=>$this->getView()->translate('Date of Issue'),
			'required'=>true
			));	
			
			
			//Expiry Date
			$this->addElement('text','av_expiry', array(
			//'label'=>$this->getView()->translate('Expiry Date'),
			'required'=>true
			));	
			
			//Attachment Visa Front Page
			/*$this->addElement('file','document_843', array(
			'label'=>$this->getView()->translate('Visa Front Page'),
			'required'=>false
			));	*/
			
			$this->document_843 = new Zend_Form_Element_File('document_843');
			$this->document_843->setLabel('Student Pass')
								->setMultiFile(2)
								->addValidator('Count', 2);
			
			//Attachment
			/*$this->addElement('file','document_828', array(
			'label'=>$this->getView()->translate('Personal Bond'),
			'required'=>false
			));*/	
			
			$this->document_828 = new Zend_Form_Element_File('document_828');
			$this->document_828->setLabel('Personal Bond')
								->setMultiFile(2)
								->addValidator('Count', 2);
			
			
			
			//Attachment
			/*$this->addElement('file','document_829', array(
			'label'=>$this->getView()->translate('Visa Approval Letter (VAL)'),
			'required'=>false
			));	*/
								
			$this->document_829 = new Zend_Form_Element_File('document_829');
			$this->document_829->setLabel('Visa Approval Letter (VAL)')
								->setMultiFile(2)
								->addValidator('Count', 2);
			
			//button
			/*$this->addElement('submit', 'save', array(
					'label'=>'Save',
					'decorators'=>array('ViewHelper')
			));
				
			
			$this->addDisplayGroup(array('save'),'buttons', array(
					'decorators'=>array(
							'FormElements',
							array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
							'DtDdWrapper'
					)
			));*/
		
	}
	
	
}
?>