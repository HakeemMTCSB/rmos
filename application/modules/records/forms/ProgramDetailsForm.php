<?php
class Records_Form_ProgramDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        $programId = $this->getAttrib('programId');
        $modeOfStudy = $this->getAttrib('modeOfStudy');
        $modeOfProgram = $this->getAttrib('modeOfProgram');
        
        $program = new Zend_Form_Element_Select('IdProgram');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('disable', 'true');
        $program->setAttrib('id', 'IdProgram');
	$program->removeDecorator("Label");
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        
        $progList = $progModel->fnGetProgramList();
        
        $program->addMultiOption('', '-- Select --');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        $mode_of_study = new Zend_Form_Element_Select('mode_of_study');
	$mode_of_study->removeDecorator("DtDdWrapper");
        $mode_of_study->setAttrib('class', 'select');
        $mode_of_study->setAttrib('disable', 'true');
        $mode_of_study->setAttrib('id', 'mode_of_study');
	$mode_of_study->removeDecorator("Label");
        
        $studentProfileModel = new Records_Model_DbTable_Studentprofile();
        
        $mode_of_study->addMultiOption('', '-- Select --');
        
        if ($programId!=null){
            $mosList = $studentProfileModel->getModeOfStudy($programId);

            if (count($mosList) > 0){
                foreach ($mosList as $mosLoop){
                    $mode_of_study->addMultiOption($mosLoop['mode_of_study'], $mosLoop['mosName']);
                }
            }
        }
        
        $mode_of_program = new Zend_Form_Element_Select('mode_of_program');
	$mode_of_program->removeDecorator("DtDdWrapper");
        $mode_of_program->setAttrib('class', 'select');
        $mode_of_program->setAttrib('disable', 'true');
        $mode_of_program->setAttrib('id', 'mode_of_program');
	$mode_of_program->removeDecorator("Label");
        
        $mode_of_program->addMultiOption('', '-- Select --');
        
        if ($programId!=null && $modeOfStudy!=null){
            $mopList = $studentProfileModel->getModeOfProgram($programId, $modeOfStudy);

            if (count($mopList) > 0){
                foreach ($mopList as $mopLoop){
                    $mode_of_program->addMultiOption($mopLoop['mode_of_program'], $mopLoop['mopName']);
                }
            }
        }
        
        $program_type = new Zend_Form_Element_Select('program_type');
	$program_type->removeDecorator("DtDdWrapper");
        $program_type->setAttrib('class', 'select');
        $program_type->setAttrib('disable', 'true');
        $program_type->setAttrib('id', 'program_type');
	$program_type->removeDecorator("Label");
        
        $program_type->addMultiOption('', '-- Select --');
        
        if ($programId!=null && $modeOfStudy!=null && $modeOfProgram!=null){
            $ptList = $studentProfileModel->getProgramType($programId, $modeOfStudy, $modeOfProgram);

            if (count($ptList) > 0){
                foreach ($ptList as $ptLoop){
                    $program_type->addMultiOption($ptLoop['program_type'], $ptLoop['ptName']);
                }
            }
        }
        
        $IdIntake = new Zend_Form_Element_Select('IdIntake');
	$IdIntake->removeDecorator("DtDdWrapper");
        $IdIntake->setAttrib('class', 'select');
        $IdIntake->setAttrib('disable', 'true');
        $IdIntake->setAttrib('id', 'program_type');
	$IdIntake->removeDecorator("Label");
        
        $intakeList = $studentProfileModel->getIntake();
        
        $IdIntake->addMultiOption('', '-- Select --');
        
        if (count($IdIntake)){
            foreach ($intakeList as $intakeLoop){
                $IdIntake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }
        
        //branch
        $IdBranch = new Zend_Form_Element_Select('IdBranch');
	$IdBranch->removeDecorator("DtDdWrapper");
        $IdBranch->setAttrib('class', 'select');
	$IdBranch->removeDecorator("Label");
        
        $IdBranch->addMultiOption('', '--Select--');
        
        $branchList = $studentProfileModel->getBranch();
        
        if ($branchList){
            foreach ($branchList as $branchLoop){
                $IdBranch->addMultiOption($branchLoop['IdBranch'], $branchLoop['BranchName']);
            }
        }
        
        //form elements
        $this->addElements(array(
            $program,
            $mode_of_study,
            $mode_of_program,
            $program_type,
            $IdIntake,
            $IdBranch
        ));
    }
}