<?php
class Records_Form_EducationDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //qualification level
        $ae_qualification = new Zend_Form_Element_Select('ae_qualification');
	$ae_qualification->removeDecorator("DtDdWrapper");
        $ae_qualification->setAttrib('class', 'select');
        //$ae_qualification->setAttrib('disable', 'true');
        $ae_qualification->setAttrib('id', 'ae_qualification');
	$ae_qualification->removeDecorator("Label");
        
        $qualificationModel = new Application_Model_DbTable_Qualificationsetup();
        $qualificationList = $qualificationModel->fetchAll('qualification_type_id = 607', 'QualificationRank');
        
        $ae_qualification->addMultiOption('', '-- Select --');
        
        if (count($qualificationList) > 0){
            foreach ($qualificationList as $qualificationLoop){
                $ae_qualification->addMultiOption($qualificationLoop['IdQualification'], $qualificationLoop['QualificationLevel']);
            }
        }
        
        //degree award
        $ae_degree_awarded = new Zend_Form_Element_Text('ae_degree_awarded');
	$ae_degree_awarded->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //majoring
        $ae_majoring = new Zend_Form_Element_Text('ae_majoring');
	$ae_majoring->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //class degree
        $ae_class_degree = new Zend_Form_Element_Select('ae_class_degree');
	$ae_class_degree->removeDecorator("DtDdWrapper");
        $ae_class_degree->setAttrib('class', 'select');
        //$ae_class_degree->setAttrib('disable', 'true');
        $ae_class_degree->setAttrib('id', 'ae_class_degree');
	$ae_class_degree->removeDecorator("Label");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $degreeClassList = $defModel->getDataByType(103);
        
        $ae_class_degree->addMultiOption('', '-- Select --');
        
        if (count($degreeClassList)){
            foreach ($degreeClassList as $degreeClassLoop){
                $ae_class_degree->addMultiOption($degreeClassLoop['idDefinition'], $degreeClassLoop['DefinitionDesc']);
            }
        }
        
        //result / cgpa
        $ae_result = new Zend_Form_Element_Text('ae_result');
	$ae_result->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //year of qraduation
        $ae_year_qraduate = new Zend_Form_Element_Text('ae_year_graduate');
	$ae_year_qraduate->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //medium of instruction
        $ae_medium_instruction = new Zend_Form_Element_Text('ae_medium_instruction');
	$ae_medium_instruction->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //institution country
        $ae_institution_country = new Zend_Form_Element_Select('ae_institution_country');
	$ae_institution_country->removeDecorator("DtDdWrapper");
        $ae_institution_country->setAttrib('class', 'select');
        $ae_institution_country->setAttrib('onchange', 'getInstitution(this.value);');
        $ae_institution_country->setAttrib('id', 'ae_institution_country');
	$ae_institution_country->removeDecorator("Label");
        
        $countryModel = new GeneralSetup_Model_DbTable_Countrymaster();
        
        $countryList = $countryModel->fetchAll(null, 'CountryName');
        
        $ae_institution_country->addMultiOption('','-- Select --');
        
        if (count($countryList) > 0){
            foreach ($countryList as $countryLoop){
                $ae_institution_country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
            }
        }
        
        //university
        $ae_institution = new Zend_Form_Element_Select('ae_institution');
	$ae_institution->removeDecorator("DtDdWrapper");
        $ae_institution->setAttrib('class', 'select');
        $ae_institution->setAttrib('onchange', 'institutionOthers(this.value);');
        $ae_institution->setAttrib('id', 'ae_institution');
	$ae_institution->removeDecorator("Label");
        
        $schoolModel = new GeneralSetup_Model_DbTable_Schoolmaster();
        $schoolList = $schoolModel->getInstitution();
        
        $ae_institution->addMultiOption('','-- Select --');
        
        if ($schoolList){
            foreach ($schoolList as $schoolLoop){
                $ae_institution->addMultiOption($schoolLoop['idInstitution'], $schoolLoop['InstitutionName']);
            }
        }
        
        $ae_institution->addMultiOption(999, 'Others');
        
        //institution others
        $others = new Zend_Form_Element_Text('others');
	$others->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //form elements
        $this->addElements(array(
            $ae_qualification,
            $ae_degree_awarded,
            $ae_majoring,
            $ae_class_degree,
            $ae_result,
            $ae_year_qraduate,
            $ae_medium_instruction,
            $ae_institution_country,
            $ae_institution,
            $others
        ));
    }
}