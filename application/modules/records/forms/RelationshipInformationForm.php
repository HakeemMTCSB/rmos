<?php
class Records_Form_RelationshipInformationForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //staff
        $ri_staff_id = new Zend_Form_Element_Select('ri_staff_id');
        $ri_staff_id->removeDecorator("DtDdWrapper");
        $ri_staff_id->setAttrib('class', 'select');
        $ri_staff_id->setAttrib('required', true);
        $ri_staff_id->setAttrib('id', 'ri_staff_id');
        $ri_staff_id->setAttrib('onchange', 'getStaffPosition(this.value, "staff_position");');
        $ri_staff_id->removeDecorator("Label");

        $staffModel = new GeneralSetup_Model_DbTable_Staffmaster();

        $staffList = $staffModel->fetchAll();

        $ri_staff_id->addMultiOption('', '-- Select --');

        if (count($staffList) > 0){
            foreach ($staffList as $staffLoop){
                $ri_staff_id->addMultiOption($staffLoop['IdStaff'], $staffLoop['FullName']);
            }
        }

        //staff position
        $staff_position = new Zend_Form_Element_Text('staff_position');
        $staff_position->setAttrib('disable', 'true');
        $staff_position->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //relationship
        $ri_relationship_id = new Zend_Form_Element_Select('ri_relationship_id');
        $ri_relationship_id->removeDecorator("DtDdWrapper");
        $ri_relationship_id->setAttrib('class', 'select');
        $ri_relationship_id->setAttrib('required', true);
        $ri_relationship_id->removeDecorator("Label");

        $ri_relationship_id->addMultiOption('', '-- Select --');

        $definationDB = new App_Model_General_DbTable_Definationms();
        $relationList = $definationDB->getDataByType(68);

        if ($relationList){
            foreach ($relationList as $relationLoop){
                $ri_relationship_id->addMultiOption($relationLoop['idDefinition'],$relationLoop['DefinitionDesc']);
            }
        }
        
        //form elements
        $this->addElements(array(
            $ri_staff_id,
            $staff_position,
            $ri_relationship_id
        ));
    }
}