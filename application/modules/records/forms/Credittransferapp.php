<?php
class Records_Form_Credittransferapp extends Zend_Dojo_Form { //Formclass for the user module
     public function init() {
        $gstrtranslate = Zend_Registry :: get('Zend_Translate');
        $lobjdeftype = new App_Model_Definitiontype();
        $lobjsemester = new GeneralSetup_Model_DbTable_Semester();
        $profilestatusList = $lobjdeftype->fnGetStudentStatusCT("Student Status");
        
        $semesterList = $lobjsemester->getAllsemesterList();
        
        $Name = new Zend_Form_Element_Text('Name');
        $Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Name->setAttrib('required',"false");
        $Name->setAttrib('maxlength','50');
        $Name->removeDecorator("DtDdWrapper");
        $Name->removeDecorator("Label");
        $Name->removeDecorator('HtmlTag');

        $StudentId = new Zend_Form_Element_Text('StudentId');
        $StudentId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $StudentId->setAttrib('required',"false");
        $StudentId->setAttrib('maxlength','50');
        $StudentId->removeDecorator("DtDdWrapper");
        $StudentId->removeDecorator("Label");
        $StudentId->removeDecorator('HtmlTag');


        
        $IdProfileStatus = new Zend_Dojo_Form_Element_FilteringSelect('IdProfileStatus');
        $IdProfileStatus->removeDecorator("DtDdWrapper");
        $IdProfileStatus->removeDecorator("Label");
        $IdProfileStatus->removeDecorator('HtmlTag');
        $IdProfileStatus->setAttrib('required',"false");
        $IdProfileStatus->setRegisterInArrayValidator(false);        
		$IdProfileStatus->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdProfileStatus->addmultioptions($profilestatusList);
        
        
        
        $IdSemester = new Zend_Dojo_Form_Element_FilteringSelect('IdSemester');
        $IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->removeDecorator("Label");
        $IdSemester->removeDecorator('HtmlTag');
        $IdSemester->setAttrib('required',"false");
        $IdSemester->setRegisterInArrayValidator(false);        
	$IdSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdSemester->addmultioptions($semesterList);

        $IdNric = new Zend_Form_Element_Text('IdNric');
        $IdNric->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $IdNric->setAttrib('required',"false");
        $IdNric->setAttrib('maxlength','50');
        $IdNric->removeDecorator("DtDdWrapper");
        $IdNric->removeDecorator("Label");
        $IdNric->removeDecorator('HtmlTag');

        $IdStudent = new Zend_Dojo_Form_Element_FilteringSelect('IdStudent');
        $IdStudent->removeDecorator("DtDdWrapper");
        $IdStudent->removeDecorator("Label");
        $IdStudent->removeDecorator('HtmlTag');
        $IdStudent->setAttrib('required',"false");
        $IdStudent->setRegisterInArrayValidator(false);
	$IdStudent->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdStudent->setAttrib('onChange',"getStudentdetail(this)");

        $IdSemesterAdd = new Zend_Dojo_Form_Element_FilteringSelect('IdSemesterAdd');
        $IdSemesterAdd->removeDecorator("DtDdWrapper");
        $IdSemesterAdd->removeDecorator("Label");
        $IdSemesterAdd->removeDecorator('HtmlTag');
        $IdSemesterAdd->setAttrib('required',"false");
        $IdSemesterAdd->setRegisterInArrayValidator(false);
	$IdSemesterAdd->setAttrib('dojoType',"dijit.form.FilteringSelect");
        //$IdSemesterAdd->addmultioptions($semesterList);
        
        //$courseModel = new GeneralSetup_Model_DbTable_Subjectmaster();
        //$courseList = $courseModel->fnGetCoursetList();
        $IdCourse = new Zend_Dojo_Form_Element_FilteringSelect('IdCourse');
        $IdCourse->removeDecorator("DtDdWrapper");
        $IdCourse->removeDecorator("Label");
        $IdCourse->removeDecorator('HtmlTag');
        $IdCourse->setAttrib('required',"false");
        $IdCourse->setRegisterInArrayValidator(false);
		$IdCourse->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdCourse->setAttrib('onChange',"getCreditHour(this)");
        //$IdCourse->addmultioptions($courseList);
        
        
        $autocreditModel = new Application_Model_DbTable_Autocredittransfer();
        $InstitutioneList = $autocreditModel->fngetInstitution();
        $IdInstitution = new Zend_Dojo_Form_Element_FilteringSelect('IdInstitution');
        $IdInstitution->removeDecorator("DtDdWrapper");
        $IdInstitution->removeDecorator("Label");
        $IdInstitution->removeDecorator('HtmlTag');
        $IdInstitution->setAttrib('required',"false");
        $IdInstitution->setRegisterInArrayValidator(false);
		$IdInstitution->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdInstitution->setAttrib('onChange',"getQualification(this)");
        $IdInstitution->addmultioptions($InstitutioneList);

        $IdQualification = new Zend_Dojo_Form_Element_FilteringSelect('IdQualification');
        $IdQualification->removeDecorator("DtDdWrapper");
        $IdQualification->removeDecorator("Label");
        $IdQualification->removeDecorator('HtmlTag');
        $IdQualification->setAttrib('required',"false");
        $IdQualification->setRegisterInArrayValidator(false);
		$IdQualification->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdQualification->setAttrib('onChange',"getSpecialization(this)");

        $IdSpecialization = new Zend_Dojo_Form_Element_FilteringSelect('IdSpecialization');
        $IdSpecialization->removeDecorator("DtDdWrapper");
        $IdSpecialization->removeDecorator("Label");
        $IdSpecialization->removeDecorator('HtmlTag');
        $IdSpecialization->setAttrib('required',"false");
        $IdSpecialization->setRegisterInArrayValidator(false);
		$IdSpecialization->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdSpecialization->setAttrib('onChange',"getequivalentCourse(this)");
        
        $EquivalentCourse = new Zend_Dojo_Form_Element_FilteringSelect('EquivalentCourse');
        $EquivalentCourse->removeDecorator("DtDdWrapper");
        $EquivalentCourse->removeDecorator("Label");
        $EquivalentCourse->removeDecorator('HtmlTag');
        $EquivalentCourse->setAttrib('required',"false");
        $EquivalentCourse->setRegisterInArrayValidator(false);
		$EquivalentCourse->setAttrib('dojoType',"dijit.form.FilteringSelect");


        $EquivalentCreditHours = new Zend_Form_Element_Text('EquivalentCreditHours');
        $EquivalentCreditHours->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $EquivalentCreditHours->setAttrib('required',"false");
        $EquivalentCreditHours->setAttrib('maxlength','50');
        $EquivalentCreditHours->removeDecorator("DtDdWrapper");
        $EquivalentCreditHours->removeDecorator("Label");
        $EquivalentCreditHours->removeDecorator('HtmlTag');
        
        
        $gradeModel = new Application_Model_DbTable_Subjectgradetype();
        $GradeList = $gradeModel->getAllgradePoint();
        
        $EquivalentGrade = new Zend_Dojo_Form_Element_FilteringSelect('EquivalentGrade');
        $EquivalentGrade->removeDecorator("DtDdWrapper");
        $EquivalentGrade->removeDecorator("Label");
        $EquivalentGrade->removeDecorator('HtmlTag');
        $EquivalentGrade->setAttrib('required',"false");
        //$EquivalentGrade->setRegisterInArrayValidator(false);
	$EquivalentGrade->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $EquivalentGrade->addmultioptions($GradeList);


        $CreditHours = new Zend_Form_Element_Text('CreditHours');
        $CreditHours->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $CreditHours->setAttrib('required',"false");
        $CreditHours->setAttrib('maxlength','50');
        $CreditHours->removeDecorator("DtDdWrapper");
        $CreditHours->removeDecorator("Label");
        $CreditHours->removeDecorator('HtmlTag');

        $Date = new Zend_Form_Element_Text('Date');
        $Date->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Date->setAttrib('required',"false");
        $Date->setAttrib('maxlength','50');
        $Date->removeDecorator("DtDdWrapper");
        $Date->removeDecorator("Label");
        $Date->removeDecorator('HtmlTag');

        

        $AppliedBy = new Zend_Form_Element_Hidden('AppliedBy');
        $AppliedBy->removeDecorator("DtDdWrapper");
        $AppliedBy->removeDecorator("Label");
        $AppliedBy->removeDecorator('HtmlTag');
        

        $AppliedByName = new Zend_Form_Element_Text('AppliedByName');
        $AppliedByName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AppliedByName->setAttrib('required',"false");
        $AppliedByName->setAttrib('maxlength','50');
        $AppliedByName->removeDecorator("DtDdWrapper");
        $AppliedByName->removeDecorator("Label");
        $AppliedByName->removeDecorator('HtmlTag');


        $ApplicationStatus = new Zend_Form_Element_Hidden('ApplicationStatus');        
        $ApplicationStatus->removeDecorator("DtDdWrapper");
        $ApplicationStatus->removeDecorator("Label");
        $ApplicationStatus->removeDecorator('HtmlTag');


        $ApplicationStatusByName = new Zend_Form_Element_Text('ApplicationStatusByName');
        $ApplicationStatusByName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApplicationStatusByName->setAttrib('required',"false");
        $ApplicationStatusByName->setAttrib('maxlength','50');
        $ApplicationStatusByName->removeDecorator("DtDdWrapper");
        $ApplicationStatusByName->removeDecorator("Label");
        $ApplicationStatusByName->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType', "dijit.form.Button");
        $Add->setAttrib('OnClick', 'addcredittransfer()')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

        $Comments = new Zend_Form_Element_Textarea('Comments');
        $Comments->removeDecorator("DtDdWrapper");
        //$Comments->setAttrib('required', "false");
        $Comments->removeDecorator("Label");
        $Comments->removeDecorator('HtmlTag');
        $Comments->setAttrib("rows", "5");
        $Comments->setAttrib("cols", "30");

        $Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";


        $Addautoct = new Zend_Form_Element_Button('Addautoct');
        $Addautoct->setAttrib('class', 'NormalBtn');
        $Addautoct->setAttrib('dojoType', "dijit.form.Button");
        $Addautoct->setAttrib('OnClick', 'addcredittransferautoct()');
        $Addautoct->removeDecorator("Label");
        $Addautoct->removeDecorator("DtDdWrapper");
        $Addautoct->removeDecorator('HtmlTag');
        $Addautoct->label = $gstrtranslate->_("Add");
        
        $Clearct = new Zend_Form_Element_Button('Clearct');
        $Clearct->setAttrib('class', 'NormalBtn');
        $Clearct->setAttrib('dojoType', "dijit.form.Button");
        $Clearct->label = $gstrtranslate->_("Clear");
        $Clearct->setAttrib('OnClick', 'clearpageAddautoct()');
        $Clearct->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
        
       $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         
        $this->addElements(array(
            $Name,$StudentId,$IdProfileStatus,$IdSemester,$IdNric,$IdStudent,$IdSemesterAdd,$IdCourse,
            $CreditHours,$Date,$AppliedBy,$AppliedByName,$ApplicationStatus,$ApplicationStatusByName,
            $Add,$clear,$Save,$Comments,$IdInstitution,$IdQualification,$IdSpecialization,
            $EquivalentCourse,$EquivalentCreditHours,$EquivalentGrade,$Addautoct,$Clearct,$Search

        ));
     }
}
?>
