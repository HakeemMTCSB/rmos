<?php

class Records_Form_Recordconfiguration extends Zend_Dojo_Form { //Formclass for the record configurationule
    /**
     * @see Zend_Form::init()
     */

    public function init() {

        $gstrtranslate = Zend_Registry :: get('Zend_Translate');

        $RecordModuleList = array (
                                    0 => array (
                                            'key' => 'Credit Transfer',
                                            'value' => 'Credit Transfer'
                                    ),
                                    1 => array (
                                            'key' => 'Change Program',
                                            'value' => 'Change Program'
                                    ),
                                    2 => array (
                                            'key' => 'Change Status',
                                            'value' => 'Change Status'
                                    ),
                            );

        $ApprovalCreditTransfer = new Zend_Form_Element_Checkbox('ApprovalCreditTransfer');
        $ApprovalCreditTransfer->setAttrib('dojoType', "dijit.form.CheckBox");
        $ApprovalCreditTransfer->setvalue('0');
        $ApprovalCreditTransfer->removeDecorator("DtDdWrapper");
        $ApprovalCreditTransfer->removeDecorator("Label");
        $ApprovalCreditTransfer->removeDecorator('HtmlTag');

        $RevertCreditTransfer = new Zend_Form_Element_Checkbox('RevertCreditTransfer');
        $RevertCreditTransfer->setAttrib('dojoType', "dijit.form.CheckBox");
        $RevertCreditTransfer->setvalue('0');
        $RevertCreditTransfer->removeDecorator("DtDdWrapper");
        $RevertCreditTransfer->removeDecorator("Label");
        $RevertCreditTransfer->removeDecorator('HtmlTag');

        $EserviceCreditTransfer = new Zend_Form_Element_Checkbox('EserviceCreditTransfer');
        $EserviceCreditTransfer->setAttrib('dojoType', "dijit.form.CheckBox");
        $EserviceCreditTransfer->setvalue('0');
        $EserviceCreditTransfer->removeDecorator("DtDdWrapper");
        $EserviceCreditTransfer->removeDecorator("Label");
        $EserviceCreditTransfer->removeDecorator('HtmlTag');

        $ApprovalChangeProgram = new Zend_Form_Element_Checkbox('ApprovalChangeProgram');
        $ApprovalChangeProgram->setAttrib('dojoType', "dijit.form.CheckBox");
        $ApprovalChangeProgram->setvalue('0');
        $ApprovalChangeProgram->removeDecorator("DtDdWrapper");
        $ApprovalChangeProgram->removeDecorator("Label");
        $ApprovalChangeProgram->removeDecorator('HtmlTag');

        $RevertChangeProgram = new Zend_Form_Element_Checkbox('RevertChangeProgram');
        $RevertChangeProgram->setAttrib('dojoType', "dijit.form.CheckBox");
        $RevertChangeProgram->setvalue('0');
        $RevertChangeProgram->removeDecorator("DtDdWrapper");
        $RevertChangeProgram->removeDecorator("Label");
        $RevertChangeProgram->removeDecorator('HtmlTag');

        $EserviceChangeProgram = new Zend_Form_Element_Checkbox('EserviceChangeProgram');
        $EserviceChangeProgram->setAttrib('dojoType', "dijit.form.CheckBox");
        $EserviceChangeProgram->setvalue('0');
        $EserviceChangeProgram->removeDecorator("DtDdWrapper");
        $EserviceChangeProgram->removeDecorator("Label");
        $EserviceChangeProgram->removeDecorator('HtmlTag');

        $ApprovalChangeStatus = new Zend_Form_Element_Checkbox('ApprovalChangeStatus');
        $ApprovalChangeStatus->setAttrib('dojoType', "dijit.form.CheckBox");
        $ApprovalChangeStatus->setvalue('0');
        $ApprovalChangeStatus->removeDecorator("DtDdWrapper");
        $ApprovalChangeStatus->removeDecorator("Label");
        $ApprovalChangeStatus->removeDecorator('HtmlTag');

        $RevertChangeStatus = new Zend_Form_Element_Checkbox('RevertChangeStatus');
        $RevertChangeStatus->setAttrib('dojoType', "dijit.form.CheckBox");
        $RevertChangeStatus->setvalue('0');
        $RevertChangeStatus->removeDecorator("DtDdWrapper");
        $RevertChangeStatus->removeDecorator("Label");
        $RevertChangeStatus->removeDecorator('HtmlTag');

        $EserviceChangeStatus = new Zend_Form_Element_Checkbox('EserviceChangeStatus');
        $EserviceChangeStatus->setAttrib('dojoType', "dijit.form.CheckBox");
        $EserviceChangeStatus->setvalue('0');
        $EserviceChangeStatus->removeDecorator("DtDdWrapper");
        $EserviceChangeStatus->removeDecorator("Label");
        $EserviceChangeStatus->removeDecorator('HtmlTag');


        $recordModuleList = array();
        $RecordModule = new Zend_Dojo_Form_Element_FilteringSelect('RecordModule');
        $RecordModule->removeDecorator("DtDdWrapper");
        $RecordModule->removeDecorator("Label");
        $RecordModule->removeDecorator('HtmlTag');
        $RecordModule->setAttrib('required', "false");
        $RecordModule->setRegisterInArrayValidator(false);
        $RecordModule->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $RecordModule->setAttrib('OnChange', 'getModuleStatus(this)');
        $RecordModule->addMultiOptions($RecordModuleList);
        

        $RecordModuleStatus = new Zend_Dojo_Form_Element_FilteringSelect('RecordModuleStatus');
        $RecordModuleStatus->removeDecorator("DtDdWrapper");
        $RecordModuleStatus->removeDecorator("Label");
        $RecordModuleStatus->removeDecorator('HtmlTag');
        $RecordModuleStatus->setAttrib('required', "false");
        $RecordModuleStatus->setRegisterInArrayValidator(false);
        $RecordModuleStatus->setAttrib('dojoType', "dijit.form.FilteringSelect");
        

//        $Email = new Zend_Form_Element_Text('Email', array('regExp' => "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$", 'invalidMessage' => "Not a valid email"));
//        $Email->setAttrib('dojoType', "dijit.form.ValidationTextBox");
//        $Email->setAttrib('required', "false");
//        $Email->setAttrib('maxlength', '50');
//        $Email->removeDecorator("DtDdWrapper");
//        $Email->removeDecorator("Label");
//        $Email->removeDecorator('HtmlTag');


//        $Email = new Zend_Form_Element_Textarea('Email');
//        $Email->removeDecorator("DtDdWrapper");
//        $Email->setAttrib('required', "true");
//        $Email->removeDecorator("Label");
//        $Email->removeDecorator('HtmlTag');
//        $Email->setAttrib("rows", "5");
//        $Email->setAttrib("cols", "30");
//        $Email->setAttrib('dojoType',"dijit.form.Textarea");
        
        $Email = new Zend_Form_Element_Textarea('Email');	
        $Email ->setAttrib('cols', '30')
                ->setAttrib('rows','5')
                ->setAttrib('style','width = 10%;')
                ->setAttrib('maxlength','250')
                ->setAttrib('required','true') 
                ->setAttrib('dojoType',"dijit.form.SimpleTextarea")
                ->setAttrib('style','margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
                ->removeDecorator("DtDdWrapper")
                ->removeDecorator("Label")
                ->removeDecorator('HtmlTag');
        
        
        
        $Description = new Zend_Form_Element_Textarea('Description');	
        $Description ->setAttrib('cols', '30')
                        ->setAttrib('rows','5')
                        ->setAttrib('style','width = 10%;')
                        ->setAttrib('maxlength','250')
                        ->setAttrib('required','false') 
                        ->setAttrib('dojoType',"dijit.form.SimpleTextarea")
                        ->setAttrib('style','margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
                        ->removeDecorator("DtDdWrapper")
                        ->removeDecorator("Label")
                        ->removeDecorator('HtmlTag');
        
        

//        $Description = new Zend_Form_Element_Textarea('Description');
//        $Description->removeDecorator("DtDdWrapper");
//        $Description->setAttrib('required', "false");
//        $Description->removeDecorator("Label");
//        $Description->removeDecorator('HtmlTag');
//        $Description->setAttrib("rows", "5");
//        $Description->setAttrib("cols", "30");
//        $Description->setAttrib('dojoType',"dijit.form.Textarea");

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType', "dijit.form.Button");
        $Add->setAttrib('OnClick', 'addConfigentry()')->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');


        $this->addElements(array(
            $ApprovalCreditTransfer, $RevertCreditTransfer, $EserviceCreditTransfer,
            $ApprovalChangeProgram, $RevertChangeProgram, $EserviceChangeProgram,
            $ApprovalChangeStatus, $RevertChangeStatus, $EserviceChangeStatus,
            $RecordModule, $RecordModuleStatus, $Email, $Description,
            $Add,$clear
                )
        );
    }

}

?>
