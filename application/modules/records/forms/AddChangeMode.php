<?php
class Records_Form_AddChangeMode extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
		
        //student name or id
        $StudentIDName = new Zend_Form_Element_Text('StudentIDName');
	$StudentIDName->setAttrib('class', 'span-7')
            //->setAttrib('onchange', 'findStudent(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //radio button id or name
        $IdOrStudent = new Zend_Form_Element_Radio('IdOrStudent');
        $IdOrStudent->removeDecorator("DtDdWrapper");
        $IdOrStudent->setAttrib('id', 'IdOrStudent');
	$IdOrStudent->removeDecorator("Label");
        $IdOrStudent->addMultiOptions(array(
            '1' => 'Student ID',
            '2' => 'Name'
	));
        $IdOrStudent->setSeparator('&nbsp;&nbsp;');
        $IdOrStudent->setValue("1");
        
        //profile status
        $ProfileStatus = new Zend_Form_Element_Text('ProfileStatus');
	$ProfileStatus->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //id type
        $IdType = new Zend_Form_Element_Text('IdType');
	$IdType->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Intake
        $Intake = new Zend_Form_Element_Text('Intake');
	$Intake->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Id no
        $IdNo = new Zend_Form_Element_Text('IdNo');
	$IdNo->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Branch
        $Branch = new Zend_Form_Element_Text('Branch');
	$Branch->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Programme
        $Programme = new Zend_Form_Element_Text('Programme');
	$Programme->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Programme scheme
        $ProgScheme = new Zend_Form_Element_Text('ProgScheme');
	$ProgScheme->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * contect info stuff
         */
        
        //Address
        $Address = new Zend_Form_Element_Textarea('Address');
	$Address->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->setAttrib("style","width: 270px; height: 98px;")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //phone home
        $PhoneHome = new Zend_Form_Element_Text('PhoneHome');
	$PhoneHome->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Phone Mobile
        $PhoneMobile = new Zend_Form_Element_Text('PhoneMobile');
	$PhoneMobile->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Phone Office
        $PhoneOffice = new Zend_Form_Element_Text('PhoneOffice');
	$PhoneOffice->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //Email
        $Email = new Zend_Form_Element_Text('Email');
	$Email->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        /*
         * add Application stuff
         */
        
        //Application ID
        $ApplicationID = new Zend_Form_Element_Text('ApplicationID');
	$ApplicationID->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //call student registration class
        $studentRegModel = new Registration_Model_DbTable_Studentregistration();
        
        //semester
        $Semester = new Zend_Form_Element_Select('Semester');
	$Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('id', 'Semester');
	$Semester->removeDecorator("Label");
        
        $Semester->addMultiOption('', '-- Select --');
        
        //$semesterList = $studentRegModel->getSemester();
        
        /*if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $Semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
            }
        }*/
        
        //applied by
        $AppliedBy = new Zend_Form_Element_Text('AppliedBy');
	$AppliedBy->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //applied Date
        $AppliedDate = new Zend_Form_Element_Text('AppliedDate');
	$AppliedDate->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //applied status
        $AppliedStatus = new Zend_Form_Element_Text('AppliedStatus');
	$AppliedStatus->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //remarks
        $Remarks = new Zend_Form_Element_Textarea('Remarks');
	$Remarks->setAttrib('class', 'span-7')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //current mode of program
        $CurrentMop = new Zend_Form_Element_Text('CurrentMop');
	$CurrentMop->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //current mode of study
        $CurrentMos = new Zend_Form_Element_Text('CurrentMos');
	$CurrentMos->setAttrib('class', 'span-7')
            ->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //new mode of program
        $NewMop = new Zend_Form_Element_Select('NewMop');
	$NewMop->removeDecorator("DtDdWrapper");
        $NewMop->setAttrib('class', 'select');
        $NewMop->setAttrib('onchange', 'getNewMos(this.value);');
        $NewMop->setAttrib('id', 'NewMop');
	$NewMop->removeDecorator("Label");
        
        $NewMop->addMultiOption('', '-- Select --');
        
        //new mode of study
        $NewMos = new Zend_Form_Element_Select('NewMos');
	$NewMos->removeDecorator("DtDdWrapper");
        $NewMos->setAttrib('class', 'select');
        $NewMos->setAttrib('id', 'NewMos');
	$NewMos->removeDecorator("Label");
        
        $NewMos->addMultiOption('', '-- Select --');
        
        //reason applying
        $Reason = new Zend_Form_Element_Select('Reason');
	$Reason->removeDecorator("DtDdWrapper");
        $Reason->setAttrib('class', 'select');
        $Reason->setAttrib('onchange', 'spesifiedOthers(this.value);');
        $Reason->setAttrib('id', 'Reason');
	$Reason->removeDecorator("Label");
        
        $Reason->addMultiOption('', '-- Select --');
        $reasonList = $studentRegModel->getDefination(161);
        
        if ($reasonList){
            foreach($reasonList as $reasonLoop){
                $Reason->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }
        
        $Reason->addMultiOption(99, 'Others');
        
        $this->addElements(array(
            $StudentIDName,
            $IdOrStudent,
            $ProfileStatus,
            $IdType,
            $Intake,
            $IdNo,
            $Branch,
            $Programme,
            $ProgScheme,
            $Address,
            $PhoneHome,
            $PhoneMobile,
            $PhoneOffice,
            $Email,
            $ApplicationID,
            $Semester,
            $AppliedBy,
            $AppliedDate,
            $AppliedStatus,
            $Remarks,
            $CurrentMop,
            $CurrentMos,
            $NewMop,
            $NewMos,
            $Reason
        ));
    }
}
?>