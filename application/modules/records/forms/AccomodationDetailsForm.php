<?php
class Records_Form_AccomodationDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //need room or not
        $acd_assistance = new Zend_Form_Element_Radio('acd_assistance');
        $acd_assistance->removeDecorator("DtDdWrapper");
        $acd_assistance->setAttrib('id', 'acd_assistance');
	$acd_assistance->removeDecorator("Label");
        $acd_assistance->addMultiOptions(array(
            '1' => 'Yes',
            '0' => 'No'
	));
        $acd_assistance->setSeparator('&nbsp;&nbsp;');
        $acd_assistance->setValue("0");
        
        //form elements
        $this->addElements(array(
            $acd_assistance
        ));
    }
}