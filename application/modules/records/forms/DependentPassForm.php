<?php 

class Records_Form_DependentPassForm extends Zend_Form
{
		
    protected $_locale;
    protected $_sdpid;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function setSdpid($value2) {
		$this->_sdpid = $value2;
	}
		
	public function init()
	{
						
			$this->setMethod('post');
			$this->setAttrib('id','form1');
			$this->setAttrib('enctype','multipart/form-data');
			
			if(isset($this->_sdpid) && $this->_sdpid!=''){ 
				$this->setAttrib('action', "/records/visa/edit-dependent");
			}else{
				$this->setAttrib('action', "/records/visa/add-dependent");
			}
	
			$this->addElement('hidden','sdp_id');
			$this->addElement('hidden','sp_id');
			$this->addElement('hidden','av_id');
			$this->addElement('hidden','p_id');
			
			
			//Name
			$this->addElement('text','sdp_name', array(
			//'label'=>$this->getView()->translate('Name'),
			'required'=>true
			));	
				
			//Relationship
			$this->addElement('select','sdp_relationship', array(
				//'label'=>$this->getView()->translate('Relationship'),
				'required'=>true
			));
			
			$defDB = new App_Model_General_DbTable_Definationms();
			
			$this->sdp_relationship->addMultiOption(null,"-- Please Select --");		
			foreach($defDB->getDataByType(68) as $def){
				$this->sdp_relationship->addMultiOption($def["idDefinition"],$def["DefinitionDesc"]);
			}	
	
			
			
			//Dob
			$this->addElement('text','sdp_dob', array(
			//'label'=>$this->getView()->translate('Date of Birth'),
			'required'=>true
			));	
			
			//passport no
			$this->addElement('text','sdp_passport_no', array(
			//'label'=>$this->getView()->translate('Passport No.'),
			'required'=>true
			));	
			if(isset($this->_sdpid) && $this->_sdpid!=''){ 
				//$this->sdp_passport_no->setAttrib('disabled',true);
			}
			
			//country
			$this->addElement('select','sdp_country_issue', array(
				//'label'=>$this->getView()->translate('Country of Issue'),
				'required'=>true
			));
			
			$countryDB = new App_Model_General_DbTable_Country();
			
			$this->sdp_country_issue->addMultiOption(null,"-- Please Select --");		
			foreach($countryDB->getData() as $c){
				$this->sdp_country_issue->addMultiOption($c["idCountry"],$c["CountryName"]);
			}	
			
			//issue date
			$this->addElement('text','sdp_date_issue', array(
			//'label'=>$this->getView()->translate('Date of Issue'),
			'required'=>true
			));	
			
			
			/*//passport no
			$this->addElement('text','p_prev_passport', array(
			'label'=>$this->getView()->translate('Previous Passport No.')
			));	*/
			
			//Expiry Date
			$this->addElement('text','sdp_expiry_date', array(
			//'label'=>$this->getView()->translate('Expiry Date'),
			'required'=>true
			));	
			
			//Natinality
			$this->addElement('select','sdp_nationality', array(
				//'label'=>$this->getView()->translate('Nationality'),
				'required'=>true
			));
			
			
			$this->sdp_nationality->addMultiOption(null,"-- Please Select --");		
			foreach($countryDB->getData() as $def){
				$this->sdp_nationality->addMultiOption($def["idCountry"],$def["CountryName"]);
			}	
			
			//Attachment
			/*$this->addElement('file','passport_65', array(
			'label'=>$this->getView()->translate('Front Page Passport'),
			'required'=>false
			));	*/
			
			$this->passport_65 = new Zend_Form_Element_File('passport_65');
			$this->passport_65->setLabel('Front Page Passport')
								->setMultiFile(2)
								->addValidator('Count', 2);
			
			
			//Attachment
			/*$this->addElement('file','pass_830', array(
			'label'=>$this->getView()->translate('Dependent Pass'),
			'required'=>false
			));*/	
			
			$this->pass_830 = new Zend_Form_Element_File('pass_830');
			$this->pass_830->setLabel('Dependent Pass')
								->setMultiFile(2)
								->addValidator('Count', 2);
			
			
			//button
			/*$this->addElement('submit', 'save', array(
					'label'=>'Save',
					'decorators'=>array('ViewHelper')
			));
				
			
			$this->addDisplayGroup(array('save'),'buttons', array(
					'decorators'=>array(
							'FormElements',
							array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
							'DtDdWrapper'
					)
			));*/
		
	}
	
	
}
?>