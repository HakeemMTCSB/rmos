<?php
class Records_Form_SearchChangeMode extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        //load model
        $model = new Records_Model_DbTable_ChangeMode();
        
        //program
        $Program = new Zend_Form_Element_Select('Program');
	$Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->setAttrib('id', 'Program');
        $Program->setAttrib('onchange', 'getProgramScheme(this.value);');
	$Program->removeDecorator("Label");
        
        $Program->addMultiOption('', '-- Select --');
        
        $programList = $model->getProgram();
        
        if ($programList){
            foreach ($programList as $programLoop){
                $Program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }
        
        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
	$ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
        $ProgramScheme->setAttrib('id', 'ProgramScheme');
	$ProgramScheme->removeDecorator("Label");
        
        $ProgramScheme->addMultiOption('', '-- Select --');
        
        //semester
        $Semester = new Zend_Form_Element_Select('Semester');
	$Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('id', 'Semester');
	$Semester->removeDecorator("Label");
        
        $Semester->addMultiOption('', '-- Select --');
        
        $semesterList = $model->getSemester();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $Semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
            }
        }
        
        //id no ic/passport
        $IdNo = new Zend_Form_Element_Text('IdNo');
	$IdNo->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //application id
        $ApplicationID = new Zend_Form_Element_Text('ApplicationID');
	$ApplicationID->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //application status
        $AppStatus = new Zend_Form_Element_Select('AppStatus');
	$AppStatus->removeDecorator("DtDdWrapper");
        $AppStatus->setAttrib('class', 'select');
        $AppStatus->setAttrib('id', 'AppStatus');
	$AppStatus->removeDecorator("Label");
        
        $AppStatus->addMultiOption('', '-- Select --');
        
        $appStatusList = $model->getDefination(162);
        
        if ($appStatusList){
            foreach ($appStatusList as $appStatusLoop){
                $AppStatus->addMultiOption($appStatusLoop['idDefinition'], $appStatusLoop['DefinitionDesc']);
            }
        }
        
        //date from
        $DateFrom = new Zend_Form_Element_Text('DateFrom');
	$DateFrom->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //date to
        $DateTo = new Zend_Form_Element_Text('DateTo');
	$DateTo->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student id
        $StudentId = new Zend_Form_Element_Text('StudentId');
	$StudentId->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student name
        $StudentName = new Zend_Form_Element_Text('StudentName');
	$StudentName->setAttrib('class', 'span-6')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student category
        $StudentCategory = new Zend_Form_Element_Select('StudentCategory');
	$StudentCategory->removeDecorator("DtDdWrapper");
        $StudentCategory->setAttrib('class', 'select');
        $StudentCategory->setAttrib('id', 'StudentCategory');
	$StudentCategory->removeDecorator("Label");
        
        $StudentCategory->addMultiOption('', '-- Select --');
        
        $studCatList = $model->getDefination(95);
        
        if ($studCatList){
            foreach ($studCatList as $studCatLoop){
                $StudentCategory->addMultiOption($studCatLoop['idDefinition'], $studCatLoop['DefinitionDesc']);
            }
        }
		
        $this->addElements(array(
            $Program,
            $ProgramScheme,
            $Semester,
            $IdNo,
            $ApplicationID,
            $AppStatus,
            $DateFrom,
            $DateTo,
            $StudentId,
            $StudentName,
            $StudentCategory
        ));
    }
}
?>
