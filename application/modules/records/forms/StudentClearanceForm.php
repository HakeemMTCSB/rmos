<?php 

class Records_Form_StudentClearanceForm extends Zend_Form
{
		
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
		/* $IdApplication->removeDecorator("DtDdWrapper");
        $IdApplication->removeDecorator("Label");
        $IdApplication->removeDecorator('HtmlTag');
     */ 
        
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),		   
			'onchange'=>'getProgramScheme(this);',
                    'registerInArrayValidator' => false
		));
		
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($programDb->getData()  as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}
		
	    //Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
                    'registerInArrayValidator' => false
		));
		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));			
				
		//Student Category
		$this->addElement('select','student_category', array(
			'label'=>$this->getView()->translate('Student Category'),
            'registerInArrayValidator' => false
		));
		
		$defDb = new App_Model_General_DbTable_Definationms();
		
		$this->student_category->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($defDb->getDataByType(95)  as $program){
			$this->student_category->addMultiOption($program["idDefinition"],$program["DefinitionDesc"]);
		}
		
		//Status
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Application Status')
		));
		
		$defDb = new App_Model_General_DbTable_Definationms();
		
		$this->status->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($defDb->getDataByType(72)  as $program){
			$this->status->addMultiOption($program["idDefinition"],$program["DefinitionDesc"]);
		}
		
		//Applicant Name
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));
		
		//Applicant ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID')
		));
		
		 
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper'),
		  'class'=>'btn-submit'
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper'),
		 'class'=>'btn-submit'
        ));
        
              
        
        $this->addDisplayGroup(array('Search', 'Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>