<?php 
class Migration_Model_DbTable_Migratecourse extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'result_dentist';
	protected $_primary = "id";
	protected $_db;
	
	public function getAcadYear($progcode){

		$sql = "SELECT COUNT(*) AS `Rows`, `academicyear`,ay_code FROM `result_dentist` 
			inner join tbl_academic_year on academicyear=ay_id
		where programcode='$progcode' GROUP BY `academicyear` ORDER BY `ay_code` asc";
		$rows = $this->_db->fetchAll($sql);
		return $rows;
	}
	
	public function getProgram(){
		$sql="SELECT COUNT(*) AS `Rows`, a.`programcode`,arabicname FROM `result_dentist` a
				left join tbl_program as b on a.programcode=b.programcode
				GROUP BY `programcode` ORDER BY `programcode`";
		$rows = $this->_db->fetchAll($sql);
		return $rows;		
	}
	
	public function getAcadYearSis($aycode){
		$sql ="select ay_id from tbl_academic_year where ay_code='$aycode'";
		$row = $this->_db->fetchRow($sql);
		return $row["ay_id"];
	}
	public function getAcadYearById($ayid){
		$sql ="select ay_code from tbl_academic_year where ay_id='$ayid'";
		$row = $this->_db->fetchRow($sql);
		return $row["ay_code"];
	}
		
	public function getSemester($prgcode,$ayid){
		$sql = "SELECT COUNT(*) AS `Rows`, `semesterCode` FROM `result_dentist` 
			where programcode = '$prgcode' and academicyear='$ayid' 
		GROUP BY `semesterCode` ORDER BY `semesterCode` ";
		$rows = $this->_db->fetchAll($sql);
		return $rows;		
	}
	
	public function getStudent($prgcode,$ayid,$sem,$semid){
		$sql ="SELECT COUNT(*) AS `Rows`,b.IdStudentRegistration, a.`RegistrationId`,appl_fname,b.idsemestermain,idlandscape,idstudentsemsterstatus,landscape  FROM `result_dentist` a
			inner join tbl_studentregistration as b on a.RegistrationId = b.registrationId
			left join student_profile as c on c.appl_id = b.IdApplication
			left join tbl_studentsemesterstatus as d on d.IdStudentRegistration=b.IdStudentRegistration and d.IdSemesterMain=$semid
			where a.status=0 and programcode = '$prgcode' and academicyear='$ayid' and semesterCode='$sem'
		GROUP BY a.`RegistrationId` ORDER BY a.`RegistrationId` ";
		//echo $sql;exit;
		$rows = $this->_db->fetchAll($sql);
		
		return $rows;	
	}
	
	public function getSemInfo($semname){

		$sql2 = "select * from tbl_semestermaster where semestermainname like '$semname' limit 0,1";
		
		$rowsem = $this->_db->fetchRow($sql2);
		
		return $rowsem;
	}
	public function openquery($sql){
		return $this->_db->query($sql);		
	}		
	
	public function getsemlevel($semreg,$semtocheck){
		$sql = "SELECT `SemesterMainStartDate` FROM `tbl_semestermaster` WHERE `IdSemesterMaster`=$semreg";
		//echo $sql;exit;
		$sqlc = "select * from tbl_semestermaster where isCountable=1 and IdSemesterMaster=$semtocheck";
		$countable = $this->_db->fetchRow($sqlc);
		if(!$countable){
			return 0;
		}
		$rowsem = $this->_db->fetchRow($sql);
		$sql2="select * from tbl_semestermaster where Scheme=9 and isCountable=1 and SemesterMainStartDate > '".$rowsem["SemesterMainStartDate"]."' order by SemesterMainStartDate";
		//echo $sql2;
		$rowsem2 = $this->_db->fetchAll($sql2);
		
		$level=1;
		foreach($rowsem2 as $sem2){
			if($semtocheck == $sem2["IdSemesterMaster"]){
				$level++;
				return $level;
				break;
			}
			$level++;
		}
		return $level;
	}
	
	function getBlockId($idlandscape,$semlevel){
		$sql="SELECT * FROM `tbl_landscapeblock` WHERE `idlandscape` = '$idlandscape' AND `semester` = $semlevel ";
		$row = $this->_db->fetchRow($sql);
		return  $row["idblock"];
	}
	function getSubject($nim,$ay,$semCode){
		$sql = "select a.*,b.IdSubject from ".$this->_name." a 
				left join tbl_subjectmaster as b on SubCode=SubjectCode
				where RegistrationId='$nim' and academicyear = $ay and semestercode=$semCode order by SubjectCode";
		$rows = $this->_db->fetchAll($sql);
		return  $rows;		
	}	
	function getSubjectBlock($nim,$ay,$semCode){
		//kena amik groupinfo
		$sql = "select a.*,b.IdSubject,c.blockid,d.block,grpcode,staffid from ".$this->_name." a 
				left join tbl_subjectmaster as b on SubCode=SubjectCode
				left join tbl_landscapeblocksubject as c on b.IdSubject=c.subjectid
				left join tbl_landscapeblock as d on c.blockid=d.idblock
				where status=0 and RegistrationId='$nim' and academicyear = $ay and semestercode=$semCode order by SubjectCode";
		$rows = $this->_db->fetchAll($sql);
		return  $rows;		
	}
	
	function getMigratedSubject($nim,$ay,$semCode){
		$sql = "select count(*) as Rows from ".$this->_name." a 
				where status=1 and RegistrationId='$nim' and academicyear = $ay and semestercode=$semCode";
		$row = $this->_db->fetchRow($sql);
		return  $row["Rows"];		
	}
	
	function checkExistingGroup($semid,$idsubject,$grpcode){
		$sql="SELECT * FROM `tbl_course_tagging_group` WHERE `IdSemester` = $semid AND `IdSubject` = $idsubject AND `GroupCode` = '$grpcode' ";
		$row = $this->_db->fetchRow($sql);
		return $row;
	}
	
	function getIdStaff($staffid){
		$sql="SELECT IdStaff FROM `tbl_staffmaster` WHERE `StaffId` = '$staffid' limit 0,1";
		$row = $this->_db->fetchRow($sql);
		return $row;		
	}
	
	function getStudRegParent($intake=79){
		$sql="SELECT a.*,IdLandscapeblocksubject FROM tbl_studentregsubjects  a
			inner join tbl_landscapeblocksubject as b on 			
			a.idsubject=b.subjectid
			inner join tbl_studentregistration as c on
			c.IdStudentRegistration = a.IdStudentRegistration
			where b.type = 2  
			and c.IdIntake=$intake 
			and c.IdLandscape=10
			ORDER BY `a`.`IdStudentRegSubjects` ASC
			";
		$rows = $this->_db->fetchAll($sql);
		return $rows;
	}
	
	function getChildSubject($idsubject){
		$sql="select subjectid from tbl_landscapeblocksubject where parentId='$idsubject'";
		//echo $sql;
		$rows = $this->_db->fetchAll($sql);
		return $rows;		
	}
}