<?php 
class Migration_Model_DbTable_Migrate extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'seniorraw';
	protected $_primary = "id";
	protected $_db;
	
	function App_Model_Application_DbTable_Batchagent(){
		$this->_db = Zend_Registry::get('dbapp');
	}
	
	public function getStudentPageData($intake=null,$prog=null){
		//$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$sql = $this->_db->select()
			->from(array('a' => $this->_name),array('a.*'))
			->where("a.yearIntake = ?",$intake);
		//$rows = $this->_db->fetchAll($sql);	
		return $sql;
	}
	public function getStudentList($intake=null,$prog=null){
		//$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$sql = $this->_db->select()
			->from(array('a' => $this->_name),array('a.*'))
			->joinleft(array('b' => "tbl_city"), "b.CityName=a.City",array('IdCity'))
			->where("a.yearIntake = ?",$intake);
			
		if($prog!=""){
			$sql->where("a.programCode = ?",$prog);
		}
		//echo  $sql;exit;
		$rows = $this->_db->fetchAll($sql);	
		return $rows;
	}	
	public function getIntake(){
		$sql="SELECT COUNT( * ) AS `Rows` , `yearIntake`
		FROM `seniorraw`
		GROUP BY `yearIntake`
		ORDER BY `yearIntake` 	
		";
		$row = $this->_db->fetchAll($sql);	
		
		return $row;
	}
	
	public function getIntakeSis($intakeid){
		$sql = "select IntakeId from tbl_intake where IdIntake = '$intakeid'";
		$row = $this->_db->fetchRow($sql);
		//$data["yearIntake"]=$row["IdIntake"]; 
		//$this->update($data,"yearIntake = '$intakeid'");
		return $row["IntakeId"];
		
	}
	
	public function updIntake($yearintake){
		$yearintakex=$yearintake+1;
		$data["yearIntake"]=$yearintake."/".$yearintakex;
		echo $data["yearIntake"];
		//$this->update($data,"yearIntake = '$yearintake'");
	}
	
	public function getReligion($intake=null){
		$sqlw="";
		if($intake!=""){
			$sqlw = " where yearIntake=$intake ";
		}		
		$sql="SELECT COUNT( * ) AS `Rows` , `Religion`
		FROM `seniorraw`
		$sqlw
		GROUP BY `Religion`
		ORDER BY `Religion` 	
		";
		$row = $this->_db->fetchAll($sql);	
		
		return $row;
	}	
	
	public function getReligionSis($religion){
		$sql = "select ssd_name_bahasa from sis_setup_detl where ssd_code = 'RELIGION' and ssd_id  = '$religion'";
		//echo $sql;
		$row = $this->_db->fetchRow($sql); 
		
		return $row["ssd_name_bahasa"];
		
	}	
	
	public function updReligion($religion){
		$relid=$this->getReligionSis($religion);
		if($relid==""){
			$relid=0;
		}
		$data["Religion"]=$relid;
		$this->update($data,"Religion = '$religion'");
	}
	public function getProgram($intake = null){
		$sqlw="";
		if($intake!=""){
			$sqlw = " where yearIntake=$intake ";
		}
		$sql="SELECT COUNT( * ) AS `Rows` , `programCode`
			FROM `seniorraw`
			$sqlw
			GROUP BY `programCode`
			ORDER BY `programCode` 	
		";
		$row = $this->_db->fetchAll($sql);	
		
		return $row;
	}	
	
	public function updProgram($program){
		/*$relid=$this->getReligionSis($religion);
		if($relid==""){
			$relid=0;
		}*/
		$data["programCode"]="0".$program;
		$this->update($data,"programCode = '$program'");
	}	
	
	public function getProgramSis($program){
		$sql = "select arabicname from tbl_program where ProgramCode = '$program'";

		$row = $this->_db->fetchRow($sql); 
		
		return $row["arabicname"];
		
	}

	public function getGender($intake=null){
		$sqlw="";
		if($intake!=""){
			$sqlw = " where yearIntake=$intake ";
		}		
		$sql="SELECT COUNT( * ) AS `Rows` , `gender`
			FROM `seniorraw`
			$sqlw
			GROUP BY `gender`
			ORDER BY `gender` 	
		";
		$row = $this->_db->fetchAll($sql);	
		
		return $row;
	}	

	public function getCitizen($intake=null){
		$sqlw="";
		if($intake!=""){
			$sqlw = " where yearIntake=$intake ";
		}		
		$sql="SELECT COUNT( * ) AS `Rows` , `Citizenship`
			FROM `seniorraw`
			$sqlw
			GROUP BY `Citizenship`
			ORDER BY `Citizenship` 	
		";
		$row = $this->_db->fetchAll($sql);	
		
		return $row;
	}

	public function getCity(){
		$sql="SELECT COUNT(*) AS `Rows`, `City`,idCity FROM `seniorraw` 
			left join tbl_city on CityName=City 
			where idCity is not null
			GROUP BY `City`
			ORDER BY `seniorraw`.`City`  DESC			
		";
		$row = $this->_db->fetchAll($sql);	
		
		return $row;

	}
	
	public function updCity($city){
		$data["City"]=$city;
		$this->update($data,"City like '$city %'");
	}
	
	public function createGasalSem($intake){
		$sql = "select IntakeId from tbl_intake where idintake = '".$intake."'";
		$rowi = $this->_db->fetchRow($sql);

		$rowsem = $this->getGasalSem($rowi["IntakeId"]);
		$sql2 = "select * from tbl_academic_year where ay_code='".$rowi["IntakeId"]."'";
		$roway = $this->_db->fetchRow($sql2);
		if(!is_array($rowsem)){
			echo "Create Sem gasal for ".$rowi["IntakeId"];
			$year=explode("/",$rowi["IntakeId"]);
			$sqli = "INSERT INTO `tbl_semestermaster` 
			(`idacadyear`,`SemesterMainName`, `SemesterMainDefaultLanguage`, `SemesterMainCode`, `IsCountable`, `SemesterMainStatus`, `DummyStatus`, `SemesterMainStartDate`, `SemesterMainEndDate`, `Scheme`, `UpdDate`, `UpdUser`) VALUES
			(
			'".$roway["ay_id"]."',
			'Gasal ".$rowi["IntakeId"]." (R)', 
			'Gasal ".$rowi["IntakeId"]." Reg', 
			'".$year[0]."08', 
			1, 
			0, 
			NULL, 
			'".$year[0]."-08-30', 
			'".$year[1]."-03-07', 
			9, 
			'".date("Y-m-d H:i:s")."', 
			99999);
			
			";
			$this->_db->query($sqli);
		}
	}
	
	public function getGasalSem($intakeid){

		$sql2 = "select * from tbl_semestermaster where semestermainname like 'Gasal ".$intakeid." (R)' limit 0,1";
		
		$rowsem = $this->_db->fetchRow($sql2);
		
		return $rowsem;
	}
	public function getGasalSemById($intakeid){
		$sql = "select IntakeId from tbl_intake where idintake = '".$intakeid."'";
		$rowi = $this->_db->fetchRow($sql);
		
		$rowsem = $this->getGasalSem($rowi["IntakeId"]); 
		
		return $rowsem;
	}	
	public function getTotalMigrated($intake=null,$prog=null){
		//$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$sql = $this->_db->select()
			->from(array('a' => $this->_name),array('Total'=>'count(*)'))
			->where("a.yearIntake = ?",$intake)
			->where("a.status = 1");
			
		if($prog!=""){
			$sql->where("a.programCode = ?",$prog);
		}
		
		$rows = $this->_db->fetchRow($sql);	
		return $rows;
	}	
	
	public function checknim($nim){
		$sql="select * from tbl_studentregistration where registrationId='".$nim."'";
		$row = $this->_db->fetchRow($sql);	
		return $row;
		
	}
	
	public function getsemcountable($regId){
		$sql = "SELECT idstudentsemsterstatus,ay_code, semestermainname ,level  FROM `tbl_studentsemesterstatus` a
			inner join tbl_studentregistration as d on a.IdStudentRegistration=d.IdStudentRegistration
			inner join tbl_semestermaster as b on a.IdSemesterMain=b.IdSemesterMaster
			inner join tbl_academic_year as c on b.idacadyear=c.ay_id
			WHERE registrationId = '$regId' and IsCountable=1
			ORDER BY `ay_code`,semestercounttype ASC";
		$rows = $this->_db->fetchAll($sql);	
		return $rows;		
	}
	
	public function openquery($sql){
		return $this->_db->query($sql);		
	}	
}
?>