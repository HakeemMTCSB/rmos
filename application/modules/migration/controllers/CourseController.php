<?php 
class Migration_CourseController extends Base_Base {
	
	public function indexAction(){
		$cmdb = new Migration_Model_DbTable_Migratecourse();
		$this->view->programs = $cmdb->getProgram();
		 	
	}
	
	public function programAction(){
		$this->view->prcode = $this->_getparam("id",0);
		$cmdb = new Migration_Model_DbTable_Migratecourse();
		$this->view->acadyears = $cmdb->getAcadYear($this->view->prcode);
		 	
	}	
	
	public function studentAction(){
		$this->view->prcode = $this->_getparam("p",0);
		$this->view->ay = $this->_getparam("ay",0);
		$this->view->sem = $this->_getparam("s",0);
		$cmdb = new Migration_Model_DbTable_Migratecourse();
		$aycode=$cmdb->getAcadYearById($this->view->ay);
		
		//$semname
		$year=explode("/",$aycode);
		switch($this->view->sem){
			case 1:
				$semname = "Gasal ".$aycode." (R)";
				$semfull = "Gasal ".$aycode." Reg";
				$semdates = $year[0]."-08-30";
				$semdate2 = $year[1]."-03-07";
				$semcount = 1;
				break;
			case 2:
				$semname = "Genap ".$aycode." (R)";
				$semfull = "Genap ".$aycode." Reg";
				$semdates = $year[1]."-05-01";
				$semdate2 = $year[1]."-07-30";					
				$semcount = 1;
				break;
			case 11:
				$semname = "Gasal ".$aycode." (P)";
				$semfull = "Gasal ".$aycode." Pem";
				$semdates = $year[1]."-03-08";
				$semdate2 = $year[1]."-04-15";					
				$semcount = 0;
				break;
			case 12:
				$semname = "Genap ".$aycode." (P)";
				$semfull = "Genap ".$aycode." Pem";
				$semdates = $year[1]."-07-31";
				$semdate2 = $year[1]."-08-15";					
				$semcount = 0;
				break;
			case 31:
				$semname = "Gasal ".$aycode." (V)";
				$semfull = "Gasal ".$aycode." Ver";
				$semdates = $year[1]."-04-16";
				$semdate2 = $year[1]."-04-17";					
				$semcount = 0;
				break;					
			case 32:
				$semname = "Genap ".$aycode." (V)";
				$semfull = "Genap ".$aycode." Ver";
				$semdates = $year[1]."-08-16";
				$semdate2 = $year[1]."-08-18";					
				$semcount = 0;
				break;
			case 21:
				$semname = "Gasal ".$aycode." (K)";
				$semfull = "Gasal ".$aycode." Kon";
				$semdates = $year[1]."-04-18";
				$semdate2 = $year[1]."-04-19";					
				$semcount = 0;
				break;					
			case 22:
				$semname = "Genap ".$aycode." (K)";
				$semfull = "Genap ".$aycode." Kon";
				$semdates = $year[1]."-08-19";
				$semdate2 = $year[1]."-08-10";					
				$semcount = 0;
				break;	
			case 41:
				$semname = "Gasal ".$aycode." (Vd)";
				$semfull = "Gasal ".$aycode." Vld";
				$semdates = $year[1]."-04-20";
				$semdate2 = $year[1]."-04-21";		
				$semcount = 0;
				break;	
			case 42:
				$semname = "Genap ".$aycode." (Vd)";
				$semfull = "Genap ".$aycode." Vld";
				$semdates = $year[1]."-08-21";
				$semdate2 = $year[1]."-08-22";		
				$semcount = 0;
				break;																	
			default:
				$semname="";
				$semfull="";
				$semcount="";
				break;															
		}
		
		$this->view->seminfo = $cmdb->getSemInfo($semname);	
		$this->view->students = $cmdb->getStudent($this->view->prcode,$this->view->ay,$this->view->sem,$this->view->seminfo["IdSemesterMaster"]);
			 	
	}

	function splitsubjectAction(){
		$this->view->title="Split Subject";
		$intake=$this->_getparam("intake",0);
		$cmdb = new Migration_Model_DbTable_Migratecourse();
		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
		$regstud=$cmdb->getStudRegParent($intake);
		foreach($regstud as $regs){
			
			//dapatkan anak
			$childs=$cmdb->getChildSubject($regs["IdLandscapeblocksubject"]);
			//register anak kalau parent dah ada result 
			foreach($childs as $child){
				$subject["IdSubject"] = $child["subjectid"];
				$subject["IdBlock"]   = $regs["IdBlock"];
				$subject["BlockLevel"]= $regs["BlockLevel"];
				$subject["IdStudentRegistration"] = $regs["IdStudentRegistration"];
				$subject["IdSemesterMain"] = $regs["IdSemesterMain"];
				$subject["SemesterLevel"]= $regs["SemesterLevel"];
				$subject["Active"]= $regs["Active"];
				$subject["exam_status"]= $regs["exam_status"];
				$subject["final_course_mark"]= $regs["final_course_mark"];
				$subject["grade_point"]= $regs["grade_point"];
				$subject["grade_name"]= $regs["grade_name"];
				$subject["grade_desc"]= $regs["exam_status"];
				$subject["grade_status"]= $regs["grade_status"];
				$subject["mark_approveby"]= $regs["mark_approveby"];
				$subject["mark_approvedt"]= $regs["mark_approvedt"];
				$subject["IdCourseTaggingGroup"]= $regs["IdCourseTaggingGroup"];
				$subject["Active"]= $subject["Active"];
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 595;
				$subject["subjectlandscapetype"]   = 3;
				
				$isreg=$studentRegSubjectDB->getSubjectInfo($subject["IdStudentRegistration"],$subject["IdSubject"],$subject["IdSemesterMain"]);
				 
				if(!$isreg){
					echo "<pre>";
					var_dump($child["subjectid"]);	
					echo "</pre>";
					$studentRegSubjectDB->addData($subject);
				}else{
					echo "registered<br>";
				}
				
				
			}
			
			//update tbl_studentregsubject subjectlandscapetype = 2 => bapak ada anak
			$sqlu ="update tbl_studentregsubjects set subjectlandscapetype=2 where IdStudentRegSubjects = ".$regs["IdStudentRegSubjects"];
			$cmdb->openquery($sqlu);
		}
	}
}
?>