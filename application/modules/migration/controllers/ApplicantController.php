<?php 
class Migration_ApplicantController extends Base_Base {
	
	public function indexAction(){
        //title
        $this->view->title = "Migrate Applicants";

        $applicantDb = new Application_Model_DbTable_Migrate_AdmRawData();

        $applicantList = $applicantDb->getData(1);
        $this->view->applicant = $applicantList;
    }

    public function migrateAction(){

        echo $intake = $this->_getParam('intake', null);

        $applicantDb = new Application_Model_DbTable_Migrate_AdmRawData();

        $where = array(
            'ARD_INTAKE' => $intake
        );
        $applicantList = $applicantDb->getDataApplicant($where);



//        $this->_helper->flashMessenger->addMessage(array('success' => "Migrated"));
//        $this->_redirect($this->view->url(array('module' => 'migration', 'controller' => 'applicant', 'action' => 'index'), 'default', true));
        exit;
    }
}
?>