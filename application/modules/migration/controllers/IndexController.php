<?php 
class Migration_IndexController extends Base_Base {
	
	public function indexAction(){
		$this->view->title = "Summary from Seniorstudent.xls";
		$mdb = new Migration_Model_DbTable_Migrate();
		if($this->_getparam("chintake",0)!=""){
			//$mdb->updIntake($this->_getparam("chintake",0));
		}		
		$allintake = $mdb->getIntake();
		$allreligion = $mdb->getReligion();
		$allprogram = $mdb->getProgram();
		$allgender = $mdb->getGender();
		$allcitizen = $mdb->getCitizen();
		
		$this->view->intakes = $allintake;
		$this->view->religions = $allreligion;
		$this->view->programs = $allprogram;
		$this->view->genders = $allgender;
		$this->view->citizens = $allcitizen;
		
		$allcity = $mdb->getCity();
		
		/*foreach($allcity as $city){
			$mdb->updCity($city["City"]);
		}*/
	}
	
	public function intakeAction(){
		$this->view->title = "Summary by Intake from Seniorstudent.xls";
		$this->view->intakeid=$this->_getparam("id",0);
		$mdb = new Migration_Model_DbTable_Migrate();
		$mdb->createGasalSem($this->view->intakeid);
		if($this->_getparam("chintake",0)!=""){
			//$mdb->updIntake($this->_getparam("chintake",0));
		}		
		//$allintake = $mdb->getIntake();
		$allreligion = $mdb->getReligion($this->_getparam("id",0));
		$allprogram = $mdb->getProgram($this->_getparam("id",0));
		$allgender = $mdb->getGender($this->_getparam("id",0));
		$allcitizen = $mdb->getCitizen($this->_getparam("id",0));
		
		//$this->view->intakes = $allintake;
		$this->view->religions = $allreligion;
		$this->view->programs = $allprogram;
		$this->view->genders = $allgender;
		$this->view->citizens = $allcitizen;
		
		//$allcity = $mdb->getCity();
		
		/*foreach($allcity as $city){
			$mdb->updCity($city["City"]);
		}*/
	}	
	
	public function studentlistAction(){
		$mdb = new Migration_Model_DbTable_Migrate();
		$students = $mdb->getStudentPageData($this->_getparam("id",null));
	    
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($students));
		$paginator->setItemCountPerPage(100);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
					
		$this->view->paginator = $paginator;
	}
	
	public function migrateAction() {
		$mdb = new Migration_Model_DbTable_Migrate();
		$migrateDB = new Application_Model_DbTable_Migrate();
		$transactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$profileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$familyDB  = new App_Model_Application_DbTable_ApplicantFamily();
		$aprogDB  = new App_Model_Application_DbTable_ApplicantProgram();
		$appeduDB = new App_Model_Application_DbTable_ApplicantEducation();
		$ptestDB = new App_Model_Application_DbTable_ApplicantPtest();
		$badb = new Application_Model_DbTable_Batchagent();
		$selDb = new App_Model_Application_DbTable_ApplicantSelectionDetl();
		$aaudDb= new App_Model_Application_DbTable_ApplicantAssessmentUsm();
		$auth = Zend_Auth::getInstance();		
		//dapatkan student yang nak di migrate
		$students = $mdb->getStudentList($this->_getparam("intakeid",null),$this->_getparam("progid",null));
/*		echo "<pre>";
		print_r($students);
		echo "</pre>";*/
		if($this->_getparam("reindexsem",0)==1){
			foreach ($students as $i=>$data){
				$studsems = $mdb->getsemcountable($data["StudentId"]);
				foreach ($studsems as $i=>$csems){
					$level = $i+1;
					
					$sqlu = "Update tbl_studentsemesterstatus set level=$level where idstudentsemsterstatus='".$csems["idstudentsemsterstatus"]."'";
					$mdb->openquery($sqlu);
				}
			}
			echo "Re-index semester level completed";
		}
		else{
			foreach ($students as $i=>$data){
				$error = null;
				$data_mapped[$i]['raw']["id"] = $data["id"];
				
				$data_mapped[$i]['profile']["appl_fname"] = $data["Name"];
				$data_mapped[$i]['profile']["appl_mname"] = "";
				$data_mapped[$i]['profile']["appl_lname"] = "";
				
				$data_mapped[$i]['profile']["appl_address1"] = $data["Address1"];
				$data_mapped[$i]['profile']["appl_postcode"] = $data["PostCode"];
				if($data["IdCity"]=="") $data["IdCity"]=12;
				$data_mapped[$i]['profile']["appl_state"] =  $data["IdCity"];
				$data_mapped[$i]['profile']["appl_province"] = "261";
				
				$data_mapped[$i]['profile']["appl_email"] = $data["StudentId"];
				if($data_mapped[$i]['profile']["appl_email"]==""){
					$error[] = "No Email";
				}else{
					$db = Zend_Db_Table::getDefaultAdapter();
					$scheck="select appl_id,appl_email from applicant_profile where appl_email='".$data_mapped[$i]['profile']["appl_email"]."'";
					$exist=$db->fetchRow($scheck);
					if($exist){
						$error[1] = "Email already exists";
					}
				}			
				$data_mapped[$i]['profile']["appl_phone_home"] = "";
				$data_mapped[$i]['profile']["appl_phone_mobile"] = "";
				
				$data_mapped[$i]['profile']["appl_password"] = $data["StudentId"];
				$data_mapped[$i]['profile']["appl_dob"] = date('Y-m-d', strtotime($data["DateOfBirth"]));
				$data_mapped[$i]['profile']["appl_birth_place"] = $data["PlaceOfBirth"];
				$data_mapped[$i]['profile']["appl_gender"] = $data["gender"];
				$data_mapped[$i]['profile']["appl_prefer_lang"] = 2;
				$data_mapped[$i]['profile']["appl_nationality"] = 1;
				$data_mapped[$i]['profile']["appl_religion"] = $data["Religion"];
				
				
				//parent
				$data_mapped[$i]['family']["father_af_name"] = $data["MotherName"];
				$data_mapped[$i]['family']["father_af_phone"] = "";
				$data_mapped[$i]['family']["father_af_job"] = "";
				$data_mapped[$i]['family']["appl_parent_salary"] = "";
				
				//transaction
				$data_mapped[$i]['transaction']["at_pes_id"] = $data["ApplicationID"];
				if($data["ApplicationID"]==""){
						$error[2] = "No NOPES";
				}
				$migrateDB = new Application_Model_DbTable_Migrate();
					if($migrateDB->exist($data_mapped[$i]['transaction']["at_pes_id"])){
						$error[3] = "NOPES Already exists";
				}
										
				$data_mapped[$i]['transaction']["at_appl_type"] = 9;
				//$data_mapped[$i]['transaction']["at_academic_year"] = $skr_data['aaud_academic_year'];
				$data_mapped[$i]['transaction']["at_intake"] = $data["yearIntake"];
				$data_mapped[$i]['transaction']["at_period"] = "";
				
				
	
				$data_mapped[$i]['transaction']["at_status"] = "OFFER";
				$data_mapped[$i]['transaction']["at_selection_status"] = 3;
	
				
				$data_mapped[$i]['transaction']["at_create_by"] = '98998';
				$data_mapped[$i]['transaction']["entry_type"] = 9;
				
				//program
				$data_mapped[$i]['program']["ap_prog_code"] = $data["programCode"];
				$data_mapped[$i]['program']["ap_preference"] = 1;
				$data_mapped[$i]['program']["ap_usm_status"] = 1;
				if($data_mapped[$i]['program']["ap_prog_code"] ==""){
						$error[4] = "No Program";
				}				
				//student
				$data_mapped[$i]['student']["nim"] = $data["StudentId"];
				
				$data_mapped[$i]['error'] = $error;
								
			}
			
			//Save in applicant profile and create transaction
				$totalmigrated=0;
			foreach ($data_mapped as $i=>$thedata){
					$db = Zend_Db_Table::getDefaultAdapter();
					$scheck="select appl_id,appl_email from applicant_profile where appl_email='".$thedata['profile']["appl_email"]."'";
					$exist=$db->fetchRow($scheck);		
				if(!is_array($thedata["error"])&&!$exist){
					
					/*
					 * PROFILE
					 */
					$profile["appl_fname"] = $thedata['profile']['appl_fname'];
					$profile["appl_mname"] = $thedata['profile']['appl_mname'];
					$profile["appl_lname"] = $thedata['profile']['appl_lname'];
					$profile["fromOldSys"]	= 1; 
					
					$profile["appl_email"] = $thedata['profile']["appl_email"];
					
				
					$profile["appl_password"] = $thedata['profile']["appl_email"];
					 
					$profile["appl_gender"] = $thedata['profile']["appl_gender"];
					if($profile["appl_gender"]=="")  $profile["appl_gender"]="0";
					 
					$profile["appl_dob"] = $thedata['profile']["appl_dob"];
					if($profile["appl_dob"]=="")  $profile["appl_dob"]="0";
					
					$profile["appl_nationality"] =$thedata['profile']["appl_nationality"];
					if($profile["appl_nationality"]=="")  $profile["appl_nationality"]="1";
					
					$profile["appl_religion"] = $thedata['profile']["appl_religion"];
					if($profile["appl_religion"]=="")  $profile["appl_religion"]="5";
					
					$profile["appl_phone_home"] = $thedata['profile']["appl_phone_home"];
					if($profile["appl_phone_home"]=="")  $profile["appl_phone_home"]="0";
					
					$profile["appl_phone_mobile"] = $thedata['profile']["appl_phone_mobile"];
					if($profile["appl_phone_mobile"]=="")  $profile["appl_phone_mobile"]="0";
					 
					$profile["appl_address1"] = $thedata['profile']["appl_address1"];
					
					$profile["appl_postcode"] = $thedata['profile']["appl_postcode"];
					if($profile["appl_postcode"]=="")  $profile["appl_postcode"]="0";
					 
					$profile["appl_state"] = $thedata['profile']["appl_state"];
					if($profile["appl_state"]=="") $profile["appl_state"]=1;
			
						
					$profileID=$profileDB->addData($profile);
									
					
					//FATHER INFOMATION
					$family["af_relation_type"] = 20;
					
					$family["af_name"] = $thedata['family']["father_af_name"];
					if($family["af_name"]=="")$family["af_name"]="Father";
					
					$family["af_address1"] = "";//$thedata['family']["father_af_address1"];
					if($family["af_address1"]=="")$family["af_address1"]="Address";
					
					$family["af_postcode"] = "";//$thedata['family']["father_af_ostcode"];
					if($family["af_postcode"]=="")$family["af_postcode"]=1;
					
					$family["af_state"] = "";//$thedata['family']["father_appl_state"];
					if($family["af_state"]=="")$family["af_state"]=255; //DKI Jakarta
					
					$family["af_city"] = "";//$thedata['family']["father_appl_city"];
					if($family["af_city"]=="")$family["af_city"]=733; //Jakarta Pusat
									
					$family["af_family_condition"] = "";//$thedata['family']["father_condition"];
					if($family["af_family_condition"]=="")$family["af_family_condition"]=1;
					 
					$family["af_education_level"] = "";//$thedata["ddkayh"]; //sis_setup_dtl
					if($family["af_education_level"]==""){
						$family["af_education_level"]=0;
					}
					
					$family["af_job"] = "";//$thedata['family']["pekerjaan_ayah"];
					if($family["af_job"]=="")$family["af_job"]=1;
					
					$family["af_phone"] = $thedata['family']["father_af_phone"];
					if($family["af_phone"]=="")$family["af_phone"]=1;
					
					$family["af_appl_id"] = $profileID;
			
					$familyDB->addData($family);
					 
					
				
					//TRANSACTION DATA
			
					$txndata["at_appl_id"] = $profileID;
					$txndata["at_pes_id"] = $thedata['transaction']["at_pes_id"];
					$txndata["at_appl_type"] = $thedata['transaction']["at_appl_type"];
					$txndata["at_academic_year"] = 0;
					$txndata["at_intake"] = $thedata['transaction']["at_intake"];
					$txndata["at_period"] = $thedata['transaction']["at_period"];
					$txndata["at_status"] = $thedata['transaction']["at_status"];
					$txndata["at_create_by"]=595;
					$txndata["at_create_date"]=date("Y-m-d H:i:s");
					$txndata["at_submit_date"]=date("Y-m-d H:i:s");
					$txndata["at_selection_status"]=$thedata['transaction']["at_selection_status"];
					
					//$txndata["at_payment_status"]=1;
					
					$txnID=$transactionDB->addData($txndata);
	
					//PROGRAM DATA
					 
					$prog["ap_at_trans_id"] = $txnID;
					$prog["ap_prog_code"] = str_pad($thedata['program']["ap_prog_code"],4,0,STR_PAD_LEFT);
					$prog["ap_preference"] = $thedata['program']["ap_preference"];
					$prog["ap_usm_status"] = $thedata['program']["ap_usm_status"];
					
					$apId = $aprogDB->insert($prog);
			
					
					//HIGH SCHOOL
					
					$hs["ae_institution"]="";//$thedata["kd_sla"];
					if($hs["ae_institution"]=="")$hs["ae_institution"]="0";
					$hs["ae_transaction_id"]=$txnID;
					$hs["ae_discipline_code"]="";//$thedata["jur_sla"];
					if($hs["ae_discipline_code"]=="")$hs["ae_discipline_code"]="0";
					$hs["ae_appl_id"]=$profileID;
			
					$appeduDB->insert($hs);
					
					//Todo : Register as student
					$studentreg["txnid"]=$txnID;
					$studentreg["applid"]=$profileID;
					$studentreg["intakeid"]=$txndata["at_intake"];
					$studentreg["progcode"]=$prog["ap_prog_code"];
					$studentreg["nim"]=$thedata['student']["nim"];
					
					$prdb = new GeneralSetup_Model_DbTable_Program();
					$programrow=$prdb->getProgramDataByCode($prog["ap_prog_code"]);
					$studentreg["progid"]=$programrow["IdProgram"];
					
					$regid=$this->studentdata($studentreg);
					//Todo : update seniorraw status - migrated
					if($regid!=0){
						$dataraw["regid"]=$regid;
						$dataraw["status"]=1;
						$mdb->update($dataraw,"id = ".$thedata['raw']["id"]);
						$totalmigrated++;
					}else{
						echo $studentreg["nim"]. "Already Exist<br>"; 
					}
					echo "ok -$regid<br>";
				}else{
					echo $thedata['transaction']['at_pes_id']."<br>";
					var_dump($thedata["error"]);
					echo "<br>";
				}
				 
			}		
			
			//$totalmigrated=$this->applicantdata($data_mapped);
			
			echo "total migrated = ".$totalmigrated;
		}
	}

	
	private function studentdata($stregdata){

		$auth = Zend_Auth::getInstance();	
		$data["IdApplication"]  = $stregdata["applid"];	
		$data["transaction_id"] = $stregdata["txnid"];
		$data['registrationId'] = $stregdata["nim"];
		$mdb = new Migration_Model_DbTable_Migrate();
		$sem = $mdb->getGasalSemById($stregdata["intakeid"]);
		$data['IdSemesterMain'] = $sem["IdSemesterMaster"];
		$data['IdIntake'] = $stregdata["intakeid"];
		$data['IdProgram'] = $stregdata['progid'];
		$data['IdBranch'] = 2; //Kampus A
		$data['Status'] = 198; // tbl_definationms Semester Status Active
		$data['profileStatus'] = 92; //tbl_definationms idDefType = 20 ( Student Status )
		$data['UpdUser'] = 595;
		$data['UpdDate'] = date ( 'Y-m-d H:i:s' );
				
/*		echo "<pre>";
		print_r($data);
		echo "</pre>";*/
		//$nimcheck = $mdb->checknim($data['registrationId']);											
		//if(!isset($nimcheck["registrationId"])){
			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
			$getlID = $studentRegistrationDb->addData($data);
			//---------- End insert into tbl_studentregistration	---------
								
				
		   //---------- Update applicant_profile to STUDENT ------------				       
	       $profileDB = new Application_Model_DbTable_ApplicantProfile();				        
	       $profileDB->updateData(array('appl_role' => 1),$stregdata["applid"]);
	       //---------------- End  --------------
	       
	       //----------Update transaction table student registration status ------------
	       
	       $updtrans = array('at_IdStudentRegistration' => $getlID, 
	       					 'at_registration_status' => 1, 				       						
	       					 'at_registration_date'=>date('Y-m-d H:i:s'));
	       
	       $transDB = new App_Model_Application_DbTable_ApplicantTransaction();				        
	       $transDB->updateData($updtrans,$stregdata["txnid"]);
	       //---------------- End  --------------
	       
	       // -------------- Insert student status in history table ---------------
	        $statusArray = array();
	        $statusArray['profileStatus'] = 92; //Activated tbl_definationms idDefType = 20 ( Student Status )
	        $statusArray['IdStudentRegistration'] = $getlID;
	        $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
	        $statusArray['UpdUser'] = 595;
	        $statusArray['UpdDate'] = date ( 'Y-m-d H:i:s');
	        
	        $lobjstudentHistoryModel = new Registration_Model_DbTable_Studenthistory();
	        $lobjstudentHistoryModel->addStudentProfileHistory($statusArray);
	       // -------------- End Insert student status in history table ---------------
	        
	        /* Migrate Data From applicant_profile to student_profile */
					
			$profileDB = new Application_Model_DbTable_ApplicantProfile();	        
	        $profile = $profileDB->getData($stregdata["applid"]);				        
	      
	        $profile["migrate_date"] = date ( 'Y-m-d H:i:s' );
	        $profile["migrate_by"] = 595;
	        
	        unset($profile["appl_admission_type"]);
	         
	        $studentDB = new App_Model_Student_DbTable_StudentProfile();
	       $getlID = $studentDB->addData($profile);
	        
	        /* End Migrate Data From applicant_profile to student_profile */
	        
	
	        
	        
		  /* ---------------------------------
		   * END SAVE DATA
		   * ---------------------------------
		   * */			
	        
	       return $getlID;
		/*}else{
			return 0;
		}*/
		
	}
	
	public function batchsemAction(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$sql = "SELECT a.UpdDate,b.IdSemesterMain,a.IdStudentRegistration,g.SemesterMainName, registrationId, ProgramName, IntakeDesc 
				FROM tbl_studentregistration AS a
				INNER JOIN tbl_studentregsubjects AS b ON a.IdStudentRegistration = b.IdStudentRegistration
				left join student_profile as c on a.sp_id=c.id
				left join tbl_program as d on a.IdProgram=d.IdProgram
				left join tbl_program_scheme e on a.IdProgramScheme=e.IdProgramScheme
				left join tbl_intake f on a.IdIntake=f.IdIntake
				left join tbl_semestermaster g on b.IdSemesterMain=g.IdSemesterMaster
				WHERE b.IdSemesterMain in (6, 11)
				AND b.UpdDate <= '2015-01-13 18:00:00'
				GROUP BY c.id";
		$slist = $db->fetchAll($sql);
		$i=0;
		foreach ($slist as $student){
			//check ada tak semesterstatus
			$sql1="SELECT * FROM `tbl_studentsemesterstatus` WHERE `IdStudentRegistration` = '".$student["IdStudentRegistration"]."' AND `idSemester` = ".$student["IdSemesterMain"];
			$exist = $db->fetchRow($sql1);
			
			if(!$exist){
				echo $i++;
				//insert dalam student semester status
				
					$dataSemStatus = array(
								'IdStudentRegistration' => $student['IdStudentRegistration'],
								'idSemester'				=> $student["IdSemesterMain"],
								'IdSemesterMain'		=> $student["IdSemesterMain"],
								'studentsemesterstatus'		=> 130,//registered
								'UpdUser'				=> $student['IdStudentRegistration'],
								'UpdDate'				=> $student['UpdDate'],
								'UpdRole'				=> 'Student',
								'Level'				=> 1,
				);

					$db->insert('tbl_studentsemesterstatus', $dataSemStatus);				
				echo "<pre>";
				//print_r($dataSemStatus);
				echo "</pre>";
				//echo "insert<hr>";
			}
		}
		exit;
	}
}
?>