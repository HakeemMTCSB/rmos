<?php

class CourseRegistration_TimetableController extends  Zend_Controller_Action {
	
	public function init() {
		$this->timetable = new CourseRegistration_Model_DbTable_TempTimetable();
		$this->model = new Registration_Model_DbTable_SeekingForLandscape();
	}
	
	public function indexAction() {
		$this->view->title = 'Timetable';
		//phpstorm 9 tawwwwww :p
		$results = $this->timetable->getData('NEW');
		$column = $this->timetable->getColumn();
//		echo '<pre>';
		//print_r($column);
		
		//get semester list
		$semester = $this->timetable->getSemesterList();
		 
		$this->view->semester = $semester;
		 
		$this->view->results = $results;
		$this->view->column = $column;
	}
	
	public function importCsvAction() {
		
		$Auth = Zend_Auth::getInstance();
		
		if ($this->getRequest()->isPost()) 
		{ 
		    $formData = $this->getRequest()->getPost();
			
			$adapter = new Zend_File_Transfer_Adapter_Http();

			$files = $adapter->getFileInfo();
		//echo $adapter->getFileTempName();
//			print_r($files);
			$csv = fopen($files['uploadFile']['tmp_name'],"r");
			$header = fgetcsv($csv);
		
			while(! feof($csv))
			{
				if ($row = fgetcsv($csv)) {
				
					foreach ($row as $key => $value) {
						if($value == '')
							$row[$key] = NULL;
						
						//Key 13 is date format d-M-Y
						//if(($key == 13) && ($row[$key] != NULL))
							//$row[$key] =  date('Y-m-d',strtotime($value));
					}

					$data = array(
						'program' => htmlspecialchars($row[0]),
						'program_mode' => $row[1],
						'course_code' => $row[2],
						'course_name' => $row[3],
						'merge_group' => $row[4],
						'section'     => $row[5],
						'class_size'  => $row[6],
						'class_duration' => $row[7],
						'lecturer_code'  => $row[8],
						'lecturer_name'  => $row[9],
						'lecturer_email'  => $row[10],
						'class_id'   => $row[11],
						'class_type' => $row[12],
						'updated'    => $row[13],
						'in_workload'    => $row[14],
						'class_number' => $row[15],
						'class_date'   => $row[16] == 0 ? 0 : date('Y-m-d',strtotime($row[16])),
						'class_day'    => $row[17],
						'class_start_time' => $row[18],
						'class_end_time'   => $row[19],
						'venue'      => $row[20],
						'tag1_title' => $row[21],
						'tag1_value' => $row[22],
						'tag2_title' => $row[23],
						'tag2_value' => $row[24] == '' ? 'Face to Face' : $row[24],
						'tag3_title' => $row[25],
						'tag3_value' => $row[26],
						'tag4_title' => $row[27],
						'tag4_value' => $row[28],
						'tag5_title' => $row[29],
						'tag5_value' => $row[30],
						'tag6_title' => $row[31],
						'tag6_value' => $row[32],
						'tag7_title' => $row[33],
						'tag7_value' => $row[34],
						'export_date' => $row[35],
						'schedule_id' => $row[36],
						'term' => $row[37],
						'session' => $row[38],
						'version' => $row[39],
						'uploaded_date' => date('Y-m-d H:i:s'),
						'uploaded_by' => $Auth->getIdentity()->iduser
					);
					
					//echo "<pre>";
					//print_r($data);
					$this->timetable->insertData($data);
				}
			}
			fclose($csv);
                        
                        //redirect here
                        $this->_helper->flashMessenger->addMessage(array('success' => 'Import CSV file success!'));
                        $this->_redirect($this->baseUrl . '/course-registration/timetable/index');
			exit;
		}
	}
	
	public function processAction() {
            //set unlimited
            set_time_limit(0);
            ini_set('memory_limit', '-1');
            
		$this->view->title = 'Timetable';
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		$auth = Zend_Auth::getInstance();
		$createdBy = $auth->getIdentity()->iduser;
		
		/*
		 * a)course section
		 * 1. exclude Examination
		 * 2. mark as error for invalid data
		 * 3. group by course, mode of program, lecturer
		 * 
		 * b)exam schedule
                 * 
                 * 
                 * have some work todo here
                 * 1. import data into qp table.
                 * 2. control 1 semester 1 import/process.
                 * 3. section name (taknak 'online'/'face to face')(gs only).
		 */
		
		$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
		$courseGroupProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();						
		$courseGroupScheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
	
		if ($this->getRequest()->isPost()) 
		{ 
		    $formData = $this->getRequest()->getPost();
		    
		    $semestername = $formData['semester'];
			//$this->exportScheduleIntoQpevent($semestername);
                    $classstartdate = $formData['startdate'];
                    $classenddate = $formData['enddate'];
                    
                    /*$classtime = gmdate("H:i:s", 840*60);
                    $start_day = strtoupper(date('h:i a', strtotime($classtime)));
                    var_dump($start_day);
                    $qpList = $this->timetable->getDataExportToQp();
                    var_dump($qpList);
                    exit;*/
                    
                    //todo check semester block
                    $checkblocksem = $this->timetable->checkBlockSemester($semestername);
                    
                    if ($checkblocksem){
                        $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot import. You already import '.$semestername.' semester schedule.'));
                        $this->_redirect( $this->baseUrl . '/course-registration/timetable/index');
                    }
                    
                    //todo block semester
                    $semblockdata = array(
                        'tbs_semestername'=>$semestername, 
                        'tbs_updby'=>$createdBy, 
                        'tbs_upddate'=>date('Y-m-d H:i:s')
                    );
                    $this->timetable->insertBlockSemester($semblockdata);

                        //set error
			//$this->timetable->updateError();
			
			$results = $this->timetable->getDataByGroup();
			//var_dump($results); exit;

			if($results){
				foreach($results as $n=>$data){
                                        //echo '------------------------------------------------------------------------------------------------<br/>';
                                        //var_dump($data);
					$courseCode = $data['course_code'];
					$programname = $data['programname'];
					$mof = $data['mode_of_program'];
					$lecturerId = isset($data['lecturer_code'])?$data['lecturer_code']:0;
					$programArray = explode(",",$programname);
					
					$subjectInfo = $this->timetable->getSubjectInfo($courseCode);
					
					$results[$n]['IdSubject']= $subjectInfo['IdSubject'];
				 	
					$branchArray = array(
											'INCEIF'	=> 1,
											'RHB'		=> 36,
											'MARA'		=> 35
										);
					
					 $branchId = isset($branchArray[$data['branch']]) ? $branchArray[$data['branch']]:1;

					if ( $data['branch']=='INCEIF' )
					{
						$groupName = isset($data['tag2_value'])?$data['tag2_value']:$data['mode_of_program'].' - '.$data['course_name'];
					}
					else
					{
						$groupName = isset($data['tag2_value'])?$data['tag2_value']:$data['branch'].' - '.$data['course_name'];
					}

				 	$groupCode = $data['course_code'].'-'.$semestername;
				 	
					$results[$n]['GroupName']= $groupName;
					$results[$n]['GroupCode']= $groupCode;
					

					 $results[$n]['IdBranch']= $branchId;
					 $results[$n]['maxstud']= $data['class_size'];
					 
					 //add course_tagging_group
					 
					 $dataGroup['IdLecturer'] = $lecturerId;
					 $dataGroup['IdSemester'] = 0;
					 $dataGroup['IdSubject'] = $subjectInfo['IdSubject'];
					 $dataGroup['GroupName'] = $groupName;
					 $dataGroup['GroupCode'] = $groupCode;
					 $dataGroup['IdUniversity'] = 1;
					 $dataGroup['maxstud'] = $data['class_size'];
					 $dataGroup['IdBranch'] = $branchId;
                                         $dataGroup['class_start_date'] = date('Y-m-d', strtotime($classstartdate)); //add by izham
					 $dataGroup['class_end_date'] = date('Y-m-d', strtotime($classenddate)); //add by izham
					 $dataGroup['UpdUser'] = $createdBy;
					 $dataGroup['UpdDate'] = date("Y-m-d H:i:s");
					 $dataGroup['migrate_by'] = $createdBy;
					 $dataGroup['migrate_date'] = date("Y-m-d H:i:s");
					 
					 $IdCourseTaggingGroup = $courseGroupDB->addData($dataGroup);
					 
					foreach($programArray as $key=>$value){
                                                $value = trim($value);
                                            
						$progName = $value;
                                                
						$programArray = array(
						    '1'=>'Master of Science in Islamic Finance',
						    '2'=>'Masters in Islamic Finance Practice',
						    '3'=>'PhD in Islamic Finance',
                                                    '4'=>'Industrial PhD - Islamic Finance',
						    '5'=>'Chartered Islamic Finance Professional',
						    '20'=>'Master in Islamic Finance',
                                                    '21'=>'Visiting Student',
						    '22'=>'Professional Certificate for Islamic Capital Market',
						    //'22'=>'Professional Certificate in Islamic Finance - Islamic Capital Market',
                                                    '23'=>'Professional Certificate for Shariah in Islamic Finance',
                                                    //'23'=>'Professional Certificate in Islamic Finance - Shariah in Islamic Finance',
                                                    '24'=>'Professional Certificate for Islamic Banking',
                                                    '25'=>'Professional Certificate for Islamic Wealth Management',
                                                    //'25'=>'Professional Certificate in Islamic Finance - Islamic Wealth Management',
													'26'=>'Professional Certificate in Islamic Finance'
                                                );

						if ($value == 'Professional Certificate in Islamic Finance - Islamic Capital Market'){
							$value = 'Professional Certificate in Islamic Finance';
						}else if ($value == 'Professional Certificate in Islamic Finance - Shariah in Islamic Finance'){
							$value = 'Professional Certificate in Islamic Finance';
						}else if ($value == 'Professional Certificate in Islamic Finance - Islamic Wealth Management'){
							$value = 'Professional Certificate in Islamic Finance';
						}else if ($value == 'Professional Certificate in Islamic Finance - Islamic Banking'){
							$value = 'Professional Certificate in Islamic Finance';
						}
						
						$programId = array_search($value,$programArray);
						
						$results[$n]['program'][$key]['program_name'] = $progName;
						$results[$n]['program'][$key]['programid'] = $programId;
						
						$programInfo = $this->timetable->getProgramInfo($programId);

                                                if (!$programInfo){
                                                    var_dump($value);
                                                    exit;
                                                }
						$semesterInfo = $this->timetable->getSemesterInfoScheme($semestername,$programInfo['IdScheme']);
						
						
						$results[$n]['IdSemester']= $semesterInfo['IdSemesterMaster'];
                                                
                                                if (isset($dataSem['GroupName'])){
                                                    unset($dataSem['GroupName']);
                                                }
                                                if ( $data['branch']=='INCEIF' ){
                                                    if ($programInfo['IdScheme']==11){
                                                        $groupName = $data['course_name'];
                                                        $dataSem['GroupName'] = $groupName;
                                                    }
                                                }
						
						//update semester course_tagging_group
						$dataSem['IdSemester'] = $semesterInfo['IdSemesterMaster'];
                                                //var_dump($dataSem);
						$db->update('tbl_course_tagging_group',$dataSem,'IdCourseTaggingGroup = '.$IdCourseTaggingGroup);
						
						$resultsProgram = $this->timetable->getCourseGroupProgram($progName,$courseCode,$mof,$lecturerId);
						//var_dump($resultsProgram);
                                                //exit;
						foreach($resultsProgram as $k=>$cgp){
							$results[$n]['program'][$key]['progScheme'][$k]['program'] = $cgp['tag1_value'];
							
							$mofName = isset($cgp['tag2_value'])?$cgp['tag2_value']:'Face to Face';
							$mofArray = array(
							    '577'=>'Online',
								'578'=>'Face to Face',
						    );
							
							$mofId = array_search($mofName,$mofArray);
							
							$mosName = $cgp['tag5_value'];
							$mosArray = array(
							    '664'=>'Coursework',
								'454'=>'Coursework and Dessertation',
								'663'=>'Research',
						    );
							
							$mosId = array_search($mosName,$mosArray);

							$mopsName = $cgp['tag3_value'];
							$mopsArray = array(
							    '576'=>'Part Time',
								'575'=>'Full Time',
						    );
							
							$mopsId = array_search($mopsName,$mopsArray);
							
							$results[$n]['program'][$key]['progScheme'][$k]['program_mode'] = $mofName;
							$results[$n]['program'][$key]['progScheme'][$k]['program_mode_id'] = $mofId;
							$results[$n]['program'][$key]['progScheme'][$k]['program_status'] = $mopsName;
							$results[$n]['program'][$key]['progScheme'][$k]['program_status_id'] = $mopsId;
							$results[$n]['program'][$key]['progScheme'][$k]['study_mode'] = $mosName;
							$results[$n]['program'][$key]['progScheme'][$k]['study_mode_id'] = $mosId;
							
							//get programscheme 
							$programschemeInfo = $this->timetable->getProgramScheme($programId,$mofId,$mopsId,$mosId);

							//add into course_group_program
							$dataGroupProgram['group_id'] = $IdCourseTaggingGroup;
							$dataGroupProgram['program_id'] = $programId;
							$dataGroupProgram['program_scheme_id'] = isset($programschemeInfo['IdProgramScheme'])?$programschemeInfo['IdProgramScheme']:0;
							
							$IdCourseGroup = $courseGroupProgramDb->addData($dataGroupProgram);
							
							//update timetable status
							$statusTimeTable = 'FAIL';
							if($IdCourseGroup){
								$statusTimeTable = 'DONE';
							}
							$idAll = $cgp['idAll'];
                                                        
                                                        $donearr = explode(',', $idAll);
                                                        
                                                        if ($donearr){
                                                            foreach ($donearr as $doneLoop){
                                                                if ($doneLoop != ''){
                                                                $db->update('temp_timetable', array('status'=>$statusTimeTable), "id = ".$doneLoop);
                                                                }
                                                            }
                                                        }
						
						}
						
					}

					$resultsSchedule = $this->timetable->getCourseGroupSchedule($progName,$courseCode,$mof,$lecturerId,$data['branch']);
					
					foreach($resultsSchedule as $b=>$sche){
						
						$classTypeArray = array(
						    '879'=>'Examination',
						    '830'=>'Tutorial',
						    '831'=>'Intensive Review Course (IRC)',
						    '877'=>'Pre-Examination Seminar (PES)',
						    '875'=>'Webinar',
                                                    '878'=>'Lecture',
                                                    '890'=>'Paper',
                                                    '889'=>'Course'
					    );
                                                
                                            if (trim($sche['class_type'])=="Intensive Review Course"){
                                                $sche['class_type']="Intensive Review Course (IRC)";
                                            }else if (trim($sche['class_type'])=="Pre-Examination Seminar"){
                                                $sche['class_type']="Pre-Examination Seminar (PES)";
                                            }

					    
					    $classTypeId = array_search(trim($sche['class_type']),$classTypeArray);
						
						if ( trim($sche['class_type']) == 'Lecture' )
						{
							if ( in_array( trim($sche['tag1_value']), array('Chartered Islamic Finance Professional','Masters in Islamic Finance Practice')) )
							{
								$classTypeId = '890';
							}
							else
							{
								$classTypeId = '889';
							}
						}

						if ( $classTypeId == 0 )
						{
                                                    var_dump(trim($sche['class_type']));
                                                    echo '<br/>';
							print_R($sche);
							exit;
						}

						$results[$n]['schedule'][$b]['class_day'] = $sche['class_day'];
						if($sche['class_date']){
							$results[$n]['schedule'][$b]['class_date'] = $sche['class_date'];
						}
						
						$results[$n]['schedule'][$b]['class_type'] = $sche['class_type'];
						$results[$n]['schedule'][$b]['class_type_id'] = $classTypeId;
						
						$startTime = date('H:i:00',mktime(0,$sche['class_start_time']));
						$endTime = date('H:i:00',mktime(0,$sche['class_end_time']));
						
						$results[$n]['schedule'][$b]['id'] = $sche['id'];
						$results[$n]['schedule'][$b]['class_start_time'] = $startTime;
						$results[$n]['schedule'][$b]['class_end_time'] = $endTime;
						
						$venueArray = array(
												'cimb islamic classroom' => '763',
												'case study room' => '881',
												'great eastern takaful classroom' => '884'
										);
						
						$_venue = trim(strtolower($sche['venue']));
						$venue = array_key_exists( $_venue, $venueArray ) ? $venueArray[$_venue] : ''; 
						
						//add into course_group_schedule
						$dataGroupSchedule['idGroup'] = $IdCourseTaggingGroup;
						$dataGroupSchedule['IdLecturer'] = $lecturerId;
						$dataGroupSchedule['IdBranch'] = $branchId;
						$dataGroupSchedule['idClassType'] = $classTypeId;
						$dataGroupSchedule['sc_venue'] = $venue;
						$dataGroupSchedule['sc_day'] = $sche['class_day'];
						$dataGroupSchedule['sc_date'] = ($sche['class_date']!='0000-00-00')?date('Y-m-d',strtotime($sche['class_date'])):NULL;
						$dataGroupSchedule['sc_start_time'] = $startTime;
						$dataGroupSchedule['sc_end_time'] = $endTime;
						$dataGroupSchedule['sc_createdby'] = $createdBy;
						$dataGroupSchedule['sc_createddt'] = date("Y-m-d H:i:s");
						
						//echo ' ------------------- '."\n\n";
						//print_R($sche);
						//print_R($dataGroupSchedule);
						
						$idScheduleGroup = $courseGroupScheduleDB->addData($dataGroupSchedule);
						
						//echo $idScheduleGroup;


						//update timetable status
						$statusTimeTable = 'FAIL';
						if($idScheduleGroup){
							$statusTimeTable = 'DONE';
						}
						$idAll = $sche['idAll'];
						$db->update('temp_timetable',array('status'=>$statusTimeTable),"id IN ($idAll) ");
                                                
                                                //to do insert qp event here
                                                /*if ($statusTimeTable == 'DONE'){
                                                    foreach($programArray as $key=>$value){
                                                        $value = trim($value);
                                            
                                                        $progName = $value;

                                                        $programArray = array(
                                                            '1'=>'Master of Science in Islamic Finance',
                                                            '2'=>'Masters in Islamic Finance Practice',
                                                            '3'=>'PhD in Islamic Finance',
                                                            '4'=>'Industrial PhD - Islamic Finance',
                                                            '5'=>'Chartered Islamic Finance Professional',
                                                            '20'=>'Master in Islamic Finance',
                                                            '21'=>'Visiting Student',
                                                            '22'=>'Professional Certificate for Islamic Capital Market',
                                                            '23'=>'Professional Certificate for Shariah in Islamic Finance',
                                                            '24'=>'Professional Certificate for Islamic Banking',
                                                            '25'=>'Professional Certificate for Islamic Wealth Management'
                                                        );
                                                        
                                                        $resultsProgram = $this->timetable->getCourseGroupProgram($progName,$courseCode,$mof,$lecturerId);
                                                        foreach($resultsProgram as $k=>$cgp){
                                                            
                                                            $results[$n]['program'][$key]['progScheme'][$k]['program'] = $cgp['tag1_value'];
							
                                                            $mofName = isset($cgp['tag2_value'])?$cgp['tag2_value']:'Face to Face';
                                                            $mosName = $cgp['tag5_value'];
                                                            $mopsName = $cgp['tag3_value'];
                                                        
                                                            //init variable
                                                            $classstarttime = gmdate("H:i:s", $sche['class_start_time']*60);
                                                            $classendtime = gmdate("H:i:s", $sche['class_end_time']*60);

                                                            if ($sche['class_date']=='0000-00-00' || $sche['class_date']==null){
                                                                $start_day = 0;
                                                                $end_day = 0;
                                                            }else{
                                                                $start_day = strtotime($sche['class_date'].' '.$classstarttime);
                                                                $end_day = strtotime($sche['class_date'].' '.$classendtime);
                                                            }

                                                            if ($sche['program_mode']=='Full Time'){
                                                                $program_mode = 2;
                                                            }else{
                                                                $program_mode = 1;
                                                            }

                                                            $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                                                            $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

                                                            $qpData = array(
                                                                'IdCourseTaggingGroup'=>$IdCourseTaggingGroup,
                                                                'sc_id'=>$idScheduleGroup,
                                                                'title'=>$sche['class_id'], 
                                                                'description'=>'', 
                                                                'venue'=>$sche['venue'], 
                                                                'all_day'=>0, 
                                                                'start_day'=>$start_day, 
                                                                'end_day'=>$end_day, 
                                                                'start_time'=>$start_time, 
                                                                'end_time'=>$end_time, 
                                                                'background_color'=>'', 
                                                                'border_color'=>'', 
                                                                'text_color'=>'', 
                                                                'date_created'=>strtotime(date('Y-m-d H:i:s')), 
                                                                'last_updated'=>strtotime(date('Y-m-d H:i:s')), 
                                                                'access'=>0, 
                                                                'category'=>1, 
                                                                'status'=>0, 
                                                                'repeat'=>0, 
                                                                'program_mode'=>$program_mode, 
                                                                'course_code'=>$sche['course_code'], 
                                                                'course_name'=>$sche['course_name'], 
                                                                'merge_group'=>$sche['merge_group'], 
                                                                'section'=>$sche['section'], 
                                                                'class_size'=>$sche['class_size'], 
                                                                'class_duration'=>$sche['class_duration'], 
                                                                'lecturer_code'=>$sche['lecturer_code'], 
                                                                'lecturer_name'=>$sche['lecturer_name'], 
                                                                'lecturer_email'=>$sche['lecturer_email'], 
                                                                'class_id'=>$sche['class_id'], 
                                                                'class_type'=>$sche['class_type'], 
                                                                'class_number'=>$sche['class_number'], 
                                                                'class_date'=>$start_day, 
                                                                'class_day'=>$sche['class_day'], 
                                                                'class_start_time'=>$sche['class_start_time'], 
                                                                'class_end_time'=>$sche['class_end_time'], 
                                                                'in_workload'=>$sche['in_workload'], 
                                                                'ordering'=>1, 
                                                                'tag1_title'=>$sche['tag1_title'], 
                                                                'tag1_value'=>$value, 
                                                                'tag2_title'=>$sche['tag2_title'], 
                                                                'tag2_value'=>$mofName, 
                                                                'tag3_title'=>$sche['tag3_title'], 
                                                                'tag3_value'=>$mopsName, 
                                                                'tag4_title'=>$sche['tag4_title'], 
                                                                'tag4_value'=>$sche['tag4_value'], 
                                                                'tag5_title'=>$sche['tag5_title'], 
                                                                'tag5_value'=>$mosName, 
                                                                'tag6_title'=>$sche['tag6_title'], 
                                                                'tag6_value'=>$sche['tag6_value'], 
                                                                'tag7_title'=>$sche['tag7_title'], 
                                                                'tag7_value'=>$sche['tag7_value'], 
                                                                'export_date'=>$sche['export_date'], 
                                                                'schedule_id'=>$sche['schedule_id'], 
                                                                'term'=>$sche['term'], 
                                                                'session'=>$sche['session'], 
                                                                'version'=>$sche['version'], 
                                                                'field_change1'=>0, 
                                                                'field_change2'=>0, 
                                                                'field_change3'=>0, 
                                                                'field_change4'=>0, 
                                                                'field_change5'=>0, 
                                                                'field_change6'=>0, 
                                                                'field_change7'=>0
                                                            );
                                                            $this->timetable->insertQpEvent($qpData);
                                                        }
                                                    }
                                                }*/
					}
					
				}
			}
			
			
			//Exam schedule
			
			$resultsSchedule = $this->timetable->getExamScheduleData();
			
			foreach($resultsSchedule as $data){
				$progName = $data['tag1_value'];
                                $progName = trim($progName);
				$programArray = array(
						    '1'=>'Master of Science in Islamic Finance',
						    '2'=>'Masters in Islamic Finance Practice',
						    '3'=>'PhD in Islamic Finance',
                                                    '4'=>'Industrial PhD - Islamic Finance',
						    '5'=>'Chartered Islamic Finance Professional',
						    '20'=>'Master in Islamic Finance',
                                                    '21'=>'Visiting Student',
						    '22'=>'Professional Certificate for Islamic Capital Market',
                                                    '23'=>'Professional Certificate for Shariah in Islamic Finance',
                                                    '24'=>'Professional Certificate for Islamic Banking',
                                                    '25'=>'Professional Certificate for Islamic Wealth Management',
					'26'=>'Professional Certificate in Islamic Finance'
				);

				if ($progName == 'Professional Certificate in Islamic Finance - Islamic Capital Market'){
					$progName = 'Professional Certificate in Islamic Finance';
				}else if ($progName == 'Professional Certificate in Islamic Finance - Shariah in Islamic Finance'){
					$progName = 'Professional Certificate in Islamic Finance';
				}else if ($progName == 'Professional Certificate in Islamic Finance - Islamic Wealth Management'){
					$progName = 'Professional Certificate in Islamic Finance';
				}else if ($progName == 'Professional Certificate in Islamic Finance - Islamic Banking'){
					$progName = 'Professional Certificate in Islamic Finance';
				}
				
				$programId = array_search($progName,$programArray);
				
				$programInfo = $this->timetable->getProgramInfo($programId);
				
				$semesterInfo = $this->timetable->getSemesterInfoScheme($semestername,$programInfo['IdScheme']);
				
				$courseCode = $data['course_code'];
				$subjectInfo = $this->timetable->getSubjectInfo($courseCode);
				
				$startTime = date('H:i:00',mktime(0,$data['class_start_time']));
				$endTime = date('H:i:00',mktime(0,$data['class_end_time']));
						
				$dataExam['es_semester']= $semesterInfo['IdSemesterMaster'];
				$dataExam['es_course']= $subjectInfo['IdSubject'];
				$dataExam['es_date'] = ($data['class_date']!='0000-00-00')?date('Y-m-d',strtotime($data['class_date'])):NULL;
				$dataExam['es_start_time'] = $startTime;
				$dataExam['es_end_time'] = $endTime;
				$dataExam['es_modifyby'] = $createdBy;
				$dataExam['es_modifydt'] = date("Y-m-d H:i:s");	
				
				$add = $db->insert('exam_schedule',$dataExam);
				
				//update timetable status
				$statusTimeTable = 'FAIL';
				if($add){
					$statusTimeTable = 'DONE';
				}
				$idAll = $data['idAll'];
				$db->update('temp_timetable',array('status'=>$statusTimeTable),"id IN ($idAll) ");
                                
                                //to do insert qp event here
                                /*if ($statusTimeTable == 'DONE'){
                                    //init variable
                                    $classstarttime = gmdate("H:i:s", $data['class_start_time']*60);
                                    $classendtime = gmdate("H:i:s", $data['class_end_time']*60);

                                    if ($sche['class_date']=='0000-00-00' || $data['class_date']==null){
                                        $start_day = 0;
                                        $end_day = 0;
                                    }else{
                                        $start_day = strtotime($data['class_date'].' '.$classstarttime);
                                        $end_day = strtotime($data['class_date'].' '.$classendtime);
                                    }

                                    if ($data['program_mode']=='Full Time'){
                                        $program_mode = 2;
                                    }else{
                                        $program_mode = 1;
                                    }

                                    $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                                    $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

                                    $qpData = array(
                                        //'IdCourseTaggingGroup'=>$IdCourseTaggingGroup,
                                        //'sc_id'=>$idScheduleGroup,
                                        'title'=>$data['class_id'], 
                                        'description'=>'', 
                                        'venue'=>$data['venue'], 
                                        'all_day'=>0, 
                                        'start_day'=>$start_day, 
                                        'end_day'=>$end_day, 
                                        'start_time'=>$start_time, 
                                        'end_time'=>$end_time, 
                                        'background_color'=>'', 
                                        'border_color'=>'', 
                                        'text_color'=>'', 
                                        'date_created'=>strtotime(date('Y-m-d H:i:s')), 
                                        'last_updated'=>strtotime(date('Y-m-d H:i:s')), 
                                        'access'=>0, 
                                        'category'=>1, 
                                        'status'=>0, 
                                        'repeat'=>0, 
                                        'program_mode'=>$program_mode, 
                                        'course_code'=>$data['course_code'], 
                                        'course_name'=>$data['course_name'], 
                                        'merge_group'=>$data['merge_group'], 
                                        'section'=>$data['section'], 
                                        'class_size'=>$data['class_size'], 
                                        'class_duration'=>$data['class_duration'], 
                                        'lecturer_code'=>$data['lecturer_code'], 
                                        'lecturer_name'=>$data['lecturer_name'], 
                                        'lecturer_email'=>$data['lecturer_email'], 
                                        'class_id'=>$data['class_id'], 
                                        'class_type'=>$data['class_type'], 
                                        'class_number'=>$data['class_number'], 
                                        'class_date'=>$start_day, 
                                        'class_day'=>$data['class_day'], 
                                        'class_start_time'=>$data['class_start_time'], 
                                        'class_end_time'=>$data['class_end_time'], 
                                        'in_workload'=>$data['in_workload'], 
                                        'ordering'=>1, 
                                        'tag1_title'=>$data['tag1_title'], 
                                        'tag1_value'=>$data['tag1_value'], 
                                        'tag2_title'=>$data['tag2_title'], 
                                        'tag2_value'=>$data['tag2_value'], 
                                        'tag3_title'=>$data['tag3_title'], 
                                        'tag3_value'=>$data['tag3_value'], 
                                        'tag4_title'=>$data['tag4_title'], 
                                        'tag4_value'=>$data['tag4_value'], 
                                        'tag5_title'=>$data['tag5_title'], 
                                        'tag5_value'=>$data['tag5_value'], 
                                        'tag6_title'=>$data['tag6_title'], 
                                        'tag6_value'=>$data['tag6_value'], 
                                        'tag7_title'=>$data['tag7_title'], 
                                        'tag7_value'=>$data['tag7_value'], 
                                        'export_date'=>$data['export_date'], 
                                        'schedule_id'=>$data['schedule_id'], 
                                        'term'=>$data['term'], 
                                        'session'=>$data['session'], 
                                        'version'=>$data['version'], 
                                        'field_change1'=>0, 
                                        'field_change2'=>0, 
                                        'field_change3'=>0, 
                                        'field_change4'=>0, 
                                        'field_change5'=>0, 
                                        'field_change6'=>0, 
                                        'field_change7'=>0
                                    );
                                    $this->timetable->insertQpEvent($qpData);
                                }*/
				
			}
//			echo '<pre>';
//			print_r($results);

			$this->exportScheduleIntoQpevent($semestername); //import to qp event yang terbaru
                        
		}
		$this->timetable->clearTimeTable();
		//exit;
		$this->_helper->flashMessenger->addMessage(array('success' => 'Data has been migrated'));
		//$this->gobjsessionsis->flash = array('type'=>'error','message'=>'Data has been migrated');
		$this->_redirect( $this->baseUrl . '/course-registration/timetable/index');
		
	}
	
        
        public function cancelAction(){
            
            /*$data = array(
                'status'=>'CANCEL'
            );*/
            $this->timetable->clearTimeTable();
            
            $this->_helper->flashMessenger->addMessage(array('success' => 'Import CSV file canceled'));
            $this->_redirect( $this->baseUrl . '/course-registration/timetable/index');
        }

	public function exportScheduleIntoQpevent($semestername){
		$scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();

		$semList = $this->timetable->getSemesterListByName($semestername);

		$semarr = array();
		if ($semList){
			foreach ($semList as $semLoop){
				array_push($semarr, $semLoop['IdSemesterMaster']);
			}
		}

		$groupList = $this->timetable->getGroup($semarr);

		if ($groupList){
			foreach ($groupList as $groupLoop){
				$groupProgram = $this->timetable->getGroupProgram($groupLoop['IdCourseTaggingGroup']);
				echo '------------------------------------------------------------<br/>';
				echo 'Group Name : '.$groupLoop['GroupName'].'<br/>';
				echo 'Group Code : '.$groupLoop['GroupCode'].'<br/>';
				echo '------------------------------------------------------------<br/>';

				$scheduleList = $this->timetable->getGroupSchedule($groupLoop['IdCourseTaggingGroup']);

				if ($scheduleList){
					foreach ($scheduleList as $scheduleLoop){
						//var_dump($scheduleLoop); //exit;
						//var_dump($groupProgram); exit;
						if ($groupProgram){
							foreach ($groupProgram as $groupProgramLoop){
								$classstarttime = $scheduleLoop["sc_start_time"];
								$classendtime = $scheduleLoop["sc_end_time"];

								$class_start_time = ($scheduleLoop["sc_start_time"]*60);
								$class_end_time = ($scheduleLoop["sc_end_time"]*60);

								if ($scheduleLoop["sc_date"]=='0000-00-00' || $scheduleLoop["sc_date"]==null){
									$start_day = 0;
									$end_day = 0;
									$program_mode = 2;
								}else{
									$start_day = strtotime($scheduleLoop["sc_date"].' '.$classstarttime);
									$end_day = strtotime($scheduleLoop["sc_date"].' '.$classendtime);
									$program_mode = 1;
								}

								$start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
								$end_time = strtoupper(date('h:i a', strtotime($classendtime)));

								$venue = $scheduleDB->getDefination($scheduleLoop["sc_venue"]);

								$classtype = $scheduleDB->getDefination($scheduleLoop['idClassType']);

								$lecturer = $scheduleDB->getLecturer($scheduleLoop["IdLecturer"]);

								$semesterInfo = $scheduleDB->getSemesterById($groupLoop['IdSemester']);

								$qpData = array(
									'IdCourseTaggingGroup'=>$groupLoop['IdCourseTaggingGroup'],
									'sc_id'=>$scheduleLoop['sc_id'],
									'title'=>$groupLoop['GroupName'],
									'description'=>'',
									'venue'=>isset($venue['DefinitionDesc']) ? $venue['DefinitionDesc']:'',
									'all_day'=>0,
									'start_day'=>$start_day,
									'end_day'=>$end_day,
									'start_time'=>$start_time,
									'end_time'=>$end_time,
									'background_color'=>'',
									'border_color'=>'',
									'text_color'=>'',
									'date_created'=>strtotime(date('Y-m-d H:i:s')),
									'last_updated'=>strtotime(date('Y-m-d H:i:s')),
									'access'=>0,
									'category'=>1,
									'status'=>0,
									'repeat'=>0,
									'program_mode'=>$program_mode,
									'course_code'=>$groupLoop['subject_code'],
									'course_name'=>$groupLoop['subject_name'],
									'merge_group'=>null,//to be declare
									'section'=>null,//to be declare
									'class_size'=>$groupLoop['maxstud'],
									'class_duration'=>($class_start_time-$class_end_time),
									'lecturer_code'=>$lecturer['IdStaff'],
									'lecturer_name'=>$lecturer['FullName'],
									'lecturer_email'=>$lecturer['Email'],
									'class_id'=>$groupLoop['GroupName'],
									'class_type'=>$classtype['DefinitionDesc'],
									'class_number'=>0,//to be declare
									'class_date'=>$start_day,
									'class_day'=>$scheduleLoop["sc_day"],
									'class_start_time'=>$class_start_time,
									'class_end_time'=>$class_end_time,
									'in_workload'=>0,
									'ordering'=>0,
									'tag1_title'=>'Programme',
									'tag1_value'=>$groupProgramLoop['ProgramName'],
									'tag2_title'=>'Programme Mode',
									'tag2_value'=>$groupProgramLoop['mop'],
									'tag3_title'=>'Programme Status',
									'tag3_value'=>$groupProgramLoop['mos'],
									'tag4_title'=>'Branch',
									'tag4_value'=>isset($groupProgramLoop['BranchName']) ? $groupProgramLoop['BranchName']:'',
									'tag5_title'=>'Study Mode',
									'tag5_value'=>$groupProgramLoop['pt'],
									'tag6_title'=>'Department',
									'tag6_value'=>$groupProgramLoop['EnglishDescription'],
									'tag7_title'=>'Collaborative Partner',
									'tag7_value'=>$groupLoop['BranchName'],
									'export_date'=>strtotime(date('Y-m-d H:i:s')),
									'schedule_id'=>0,
									'term'=>$semesterInfo['SemesterMainName'].' Semester',
									'session'=>0,
									'version'=>0,
									'field_change1'=>0,
									'field_change2'=>0,
									'field_change3'=>0,
									'field_change4'=>0,
									'field_change5'=>0,
									'field_change6'=>0,
									'field_change7'=>0
								);
								$scheduleDB->insertQpEvent($qpData);
							}
						}
					}
				}
			}
		}

		return true;
	}
}
?>