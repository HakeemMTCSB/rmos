<?php
/*
	Munzir Rosdi
	12/23/2014
*/
class CourseRegistration_AjaxController extends Zend_Controller_Action 
{
	public function init() 
	{
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();

		$this->db = getDB();
	}
	
	public function sectionAction()
	{
		$semester_id = $this->_getParam('idsemester');
		$subject_id = $this->_getParam('idsubject');
		$item_id = $this->_getParam('item');

		$db = $this->db;

		$groupDb = new App_Model_General_DbTable_CourseGroup();
		
		if ( empty($subject_id) ) throw new Exception('Invalid Subject ID');
		if ( empty($semester_id) ) throw new Exception('Invalid Semester ID');

		$auth = Zend_Auth::getInstance();
		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) ) throw new Exception('Invalid Student Id');
		
		$branch = $student['IdBranch'] == 1 ? null : $student['IdBranch'];

		$groups = $groupDb->getGroupList($subject_id, $semester_id,$student['IdProgram'], $student['IdProgramScheme'], $branch);

		$this->view->groups = $groups;
		$this->view->idsemester = $semester_id;
		$this->view->idsubject = $subject_id;
		$this->view->iditem = $item_id;
	}

	public function scheduleAction()
	{
		$scheduleDb = new App_Model_General_DbTable_CourseGroupSchedule();

		$id = $this->_getParam('id');
		$semester_id = $this->_getParam('idsemester');
		$subject_id = $this->_getParam('idsubject');
		$item_id = $this->_getParam('item');

		$auth = Zend_Auth::getInstance();
		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) ) throw new Exception('Invalid Student Id');
		if ( empty($id) ) throw new Exception('Invalid Schedule Id');

		$schedule = $scheduleDb->getSchedule($id, $item_id);
		$this->view->schedule = $schedule;	
		$this->view->idsemester = $semester_id;
		$this->view->idsubject = $subject_id;
		$this->view->iditem = $item_id;
	}

	public function examcenterAction()
	{
		$subject_id = $this->_getParam('idsubject');
		
		$countryModel = new App_Model_General_DbTable_Countrymaster();
		$countryList = $countryModel->fetchAll();
		
		$exam_id = $this->_getParam('idexam');

		if  ( $exam_id != '' )
		{
			$courseregDb = new CourseRegistration_Model_DbTable_CourseRegistration();
			$ec_data = $courseregDb->getECById($exam_id);
			$this->view->ec_data = $ec_data;
		}

		$this->view->countryList = $countryList;
		$this->view->idsubject = $subject_id;
	}

	public function examcentersaveAction()
	{
		$token = sha1(date('dmy'));
		if ($this->getRequest()->isPost()) 
		{ 
		    $formData = $this->getRequest()->getPost();
			
			if ( $formData['token'] != $token )
			{
				die('Invalid Token');
			}

			$courseregDb = new CourseRegistration_Model_DbTable_CourseRegistration();
			$ec_data = $courseregDb->getECById($formData['id']);

			if ( empty($ec_data) )
			{
				die('Invalid Exam Center Entry');
			}

			$auth = Zend_Auth::getInstance();
			$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
			$student = $studentRegDB->getStudentInfo($auth->getIdentity()->IdStudentRegistration);

			if ( empty($student) ) die('Invalid Student Id');

			if ( $student['IdStudentRegistration'] != $ec_data['er_idStudentRegistration'] )
			{
				die('This exam center doesn\'t belong to you');
			}
			
			if ( is_numeric($formData['city']) )
			{
				$city = $formData['city'];
				$city_others = ''; 
			}
			else
			{
				$city = 99;
				$city_others = $formData['city']; 
			}

			$data = array(
							'er_idCountry'		=> $formData['country'],
							'er_idCity'			=> $city,
							'er_idCityOthers'	=> $city_others
						);
			
			$db = getDb();
			
			$db->update('exam_registration', $data, array('er_id = ?' => $formData['id']));

			die('ok');
		}
	}

	public function getcityAction($country_id=0){
    	$country_id = $this->_getParam('country_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('ec'=>'tbl_exam_center'),array('ec_id','ec_name'))
					 ->join(array('add'=>'tbl_address'),'add.add_org_id=ec.ec_id',array('add.add_city','add.add_city_others'))
					 ->joinLeft(array('c'=>'tbl_countries'),'add.add_country=c.idCountry',array('CountryName'))
	                 ->joinLeft(array('s'=>'tbl_state'),'add.add_state=s.idState',array('StateName'))
	                 ->joinLeft(array('ct'=>'tbl_city'),'add.add_city=ct.idCity',array('CityName','idCity'))
	                 ->where('add.add_country = ?', $country_id)
					 ->where('add.add_org_name = ?', 'tbl_exam_center')
					 ->group('add.add_city');
	  
        $stmt = $db->query($select);
        $rows = $stmt->fetchAll();

		$cities = array();

		foreach ( $rows as $row )
		{
			$cities[] = array('city' => $row['CityName'], 'key' => $row['idCity']);
		}
		
		//others
	  	$select2 = $db->select()
	                 ->from(array('ec'=>'tbl_exam_center'),array('ec_id','ec_name'))
					 ->join(array('add'=>'tbl_address'),'add.add_org_id=ec.ec_id',array('add.add_city','add.add_city_others as CityName'))
					 ->joinLeft(array('c'=>'tbl_countries'),'add.add_country=c.idCountry',array('CountryName'))
	                 ->joinLeft(array('s'=>'tbl_state'),'add.add_state=s.idState',array('StateName'))
	                 ->where('add.add_country = ?', $country_id)
					 ->where('add.add_city = ?',99)
					 ->where('add.add_org_name = ?', 'tbl_exam_center')
					 ->group('add.add_city_others');
	  
        $stmt2 = $db->query($select2);
        $rows2 = $stmt2->fetchAll();
		
		foreach ( $rows2 as $row )
		{
			$cities[] = array('city' => $row['CityName'], 'key' => $row['CityName']);
		}
		

		//real others
		$others = array(array('city' => 'Others','key' => 'Others'));

		$cities = array_merge($cities, $others);
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($cities);
		
		echo $json;
		exit();
    }

    public function dissertationAction()
    {
        $subject_id = $this->_getParam('idsubject');
        $std_reg_id = $this->_getParam('stdregid');
        $item_id = $this->_getParam('item');

        $stdsubregDb = new CourseRegistration_Model_DbTable_Studentregsubjects();
        $std_reg_data = $stdsubregDb->getTotalCreditHourTaken($std_reg_id, $subject_id);

        $subMasterDb = new App_Model_Record_DbTable_SubjectMaster();
        $subData = $subMasterDb->getData($subject_id);

        $subject_credithour = $subData['CreditHours'];
        $total_credit_taken = $std_reg_data[0]['total']? $std_reg_data[0]['total']:0;

        $this->view->iditem = $item_id;
        $this->view->idsubject = $subject_id;
        $this->view->subject_credithour = $subject_credithour;
        $this->view->total_credit_taken = $total_credit_taken;
    }
}