<?php

class CourseRegistration_IndexController extends  Zend_Controller_Action
{
	public function init() {
		
		$this->locale = Zend_Registry::get('Zend_Locale');
    	$this->view->translate = Zend_Registry::get('Zend_Translate');
    	Zend_Form::setDefaultTranslator($this->view->translate);
    	
		Zend_Layout::getMvcInstance()->assign('navActive', 'coursereg');

        /*$this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;*/

		//$this->studentModel = new Portal_Model_DbTable_Student();

		$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		$student = $studentRegDB->getStudentInfo($this->_getParam('idsreg'));

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->studentinfo = $this->view->studentinfo = $student;

		$this->courseregDb = new CourseRegistration_Model_DbTable_CourseRegistration();		
	}

	public function indexAction()
	{
		$this->view->title = 'Course Registration';
		
		$student = $this->studentinfo;

		$programDb = new GeneralSetup_Model_DbTable_Program();
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
		$regitemDb = new Registration_Model_DbTable_RegistrationItem();
		$defDb = new App_Model_General_DbTable_Definationms();
		$examscheduleDb = new Examination_Model_DbTable_ExamSchedule();

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}
		
		if($landscape["LandscapeType"]==42){ //for level landscape
			$policy=$subjectDb->levelPolicy($student['IdStudentRegistration'],$student['IdLandscape']);
			$this->view->policy = $policy;
		}
		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		
		//check if has any subjects registered - ever
		$db = getDb();
		$allreg = $this->courseregDb->subjectRegistered($student['IdStudentRegistration']);
		
		if ( $semester ) 
		{
			//register this sem
			$registered = $this->courseregDb->subjectRegistered($student['IdStudentRegistration'], $semester['IdSemesterMaster']);
			
			//registered details
			$details = $this->courseregDb->getDetails($student['IdStudentRegistration'], $semester['IdSemesterMaster']);
			
			$itemRegistered = array();

			foreach ( $details as $det )
			{
				$itemRegistered[$det['subject_id']][] = $det;	
			}
			
			
		}

		//Check Barring//
		//1. Outstanding Previous Semester invoices
		$this->view->fbarred = 0;
		$prvSemester=$semesterDb->getPreviousSemester($semester);
		
		$invoiceClass = new icampus_Function_Studentfinance_Invoice();
		$os=$invoiceClass->checkOutstandingBalance($student['IdStudentRegistration'],2,$prvSemester["IdSemesterMaster"]);
		
		if($os==2) $this->view->fbarred=1; 
		
		//throw new Exception('Registration is not allowed. Please call INCEIF for details');//$this->view->barred = true;
		
		//2. From Barring Screen
		$this->view->obarred=0;
		$barlist = $this->courseregDb->checkBarring($student['IdStudentRegistration'],$semester["IdSemesterMaster"]);
		
		if($barlist) $this->view->obarred=1;
		
		//End Barring check//
		
		//views
		$this->view->totalregistered = count($allreg);
		$this->view->registered = $registered;
		$this->view->semester = $semester;
		$this->view->itemregistered = $itemRegistered;
		

	}
	
	public function registerAction()
	{
		$this->view->title = 'Course Registration';
		
		$student = $this->studentinfo;
		
		$programDb = new GeneralSetup_Model_DbTable_Program();
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
		$regitemDb = new Registration_Model_DbTable_RegistrationItem();
		$defDb = new App_Model_General_DbTable_Definationms();
		$examscheduleDb = new Examination_Model_DbTable_ExamSchedule();
        $lanscapeDetailDb = new CourseRegistration_Model_DbTable_LandscapeDetail();

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}
		
		$this->view->landscapetype = $landscape["LandscapeType"];
		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));

        //landscape detail
        $landscape_detail = $landscapeDb->getActiveLandscape($program['IdProgram'],$student['IdIntake'],$student['IdProgramScheme']);
        $semesterType = $semester['SemesterType']==171?'Short Semester': $semester['SemesterType']==172?'Long Semester':null;

        $errMsg = '';
        #171=Short, 172=Long / 1=Course, 2=Credit hour
        $wLanscapeDetail = $lanscapeDetailDb->getDatabyLandscapeId($landscape_detail[0]['IdLandscape'],1,$semesterType);
        if(count($wLanscapeDetail)==0){
            $errMsg = 'Semester rule (landscape) not found';
        }


        //process other things
		if($semester)
		{
			//Check Barring//
			//1. Outstanding Previous Semester invoices
			$this->view->fbarred = 0;
			$prvSemester=$semesterDb->getPreviousSemester($semester);
			
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
			$os=$invoiceClass->checkOutstandingBalance($student['IdStudentRegistration'],2,$prvSemester["IdSemesterMaster"]);
			
			if($os==2) $this->view->fbarred=1; 
			
			//throw new Exception('Registration is not allowed. Please call INCEIF for details');//$this->view->barred = true;
			
			//2. From Barring Screen
			$this->view->obarred=0;
			$barlist = $this->courseregDb->checkBarring($student['IdStudentRegistration'],$semester["IdSemesterMaster"]);
			
			if($barlist) $this->view->obarred=1;
			
			//End Barring check//

			//check policy
			$policy=null;
			if($landscape["LandscapeType"]==42){ //for level landscape
				$policy=$subjectDb->levelPolicy($student['IdStudentRegistration'],$student['IdLandscape']);
				$this->view->policy = $policy;
			}
			//end policy
			
			//$subjects = $subjectDb->getLandscapeCourses($student['IdLandscape']);
			$subjects = $subjectDb->getCourseOffer($student['IdLandscape'],$semester['IdSemesterMaster'],$landscape["LandscapeType"],$policy);
	
			//added
			$db = getDb();
			$registered = $this->courseregDb->subjectRegistered($student['IdStudentRegistration'], $semester['IdSemesterMaster']);
			$registeredSubjects = array();
	
			foreach ( $registered as $regged )
			{
				$registeredSubjects[] = $regged['IdSubject'];
			}
	
	
			//have reg this before and failed?
			$pastregSubjects = $this->getPastreg($student, $semester);
	
			//get register items
			$items = $regitemDb->getStudentItems($program['IdProgram'], $semester['IdSemesterMaster'], $student['IdProgramScheme']);
			
			//amounts
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
			foreach( $subjects as $x => $sub ) 
			{
				foreach ( $items as $item )
				{
					$amount = $invoiceClass->calculateInvoiceAmountByCourse($student['IdStudentRegistration'],$item['semester_id'],$sub['IdSubject'], $item['item_id']);
					$subjects[$x][$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : 0;
					$subjects[$x][$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';
				}
					
				$subjects[$x]['prev_taken'] = in_array($sub['IdSubject'], $pastregSubjects) ? 1 : 0;
				//$items[$x] = ;
	
				$examschedule = $examscheduleDb->getScheduleBySubject($semester['IdSemesterMaster'], $sub['IdSubject']);
				$subjects[$x]['exam_schedule'] = $examschedule;
			}
		}else{
			$items = null;
			$semester = null;
			$subjects = null;
			$registeredSubjects = null;
			$program = null;
		}

		//views
		$this->view->idlandscape = $student['IdLandscape'];
		$this->view->items = $items;
		$this->view->semester = $semester;
		$this->view->subjects = $subjects;
		$this->view->registeredSubjects = $registeredSubjects;
		$this->view->program = $program;

        $this->view->reqType = $wLanscapeDetail[0]['Type'];         #1=Course, 2=Credit
        $this->view->minReq = $wLanscapeDetail[0]['MinRegCourse'];
        $this->view->maxReq = $wLanscapeDetail[0]['MaxRegCourse'];
        $this->view->errMsg = $errMsg;
	}

	public function getPastreg($student, $semester)
	{
		$CODE_FAIL = 'IN'; //tak sure buyati letak apa lagi
 
		$db = getDb();
	  	$select = $db->select()
	                 ->from(array('a'=>'tbl_studentregsubjects'))
					 ->where('IdSemesterMain != ?', $semester['IdSemesterMaster'])
					 ->where('IdStudentRegistration = ?', $student['IdStudentRegistration'])
					 ->wherE('exam_status = ?', $CODE_FAIL);
					 
		$pastreg = $db->fetchAll($select);
		$pastregSubjects = array();

		foreach ( $pastreg as $preg )
		{
			$pastregSubjects[] = $preg['IdSubject'];
		}

		return $pastregSubjects;
	}

	public function reviewAction()
	{
		$this->view->title = 'Course Registration - Review';
		
		$student = $this->studentinfo;
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$courseGroupDb = new App_Model_General_DbTable_CourseGroup();
		$courseGroupScheduleDb = new App_Model_General_DbTable_CourseGroupSchedule();
		$examscheduleDb = new App_Model_Exam_DbTable_ExamSchedule();
			
		$subjectsById = '';
		$itemsById = '';

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}

		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		

		//process other things
		$subjects = $subjectDb->getCourseOffer($student['IdLandscape'],$semester['IdSemesterMaster']);
		
		//get register items
		$items = $regitemDb->getStudentItems($program['IdProgram'], $semester['IdSemesterMaster'], $student['IdProgramScheme']);

		foreach ( $items as $item )
		{
			$itemsById[$item['item_id']] = $item;
		}

		//have reg this before and failed?
		$pastregSubjects = $this->getPastreg($student, $semester);
		
		//amounts
		$invoiceClass = new icampus_Function_Studentfinance_Invoice();
		foreach( $subjects as $x => $sub ) 
		{
			foreach ( $items as $item )
			{
				$amount = $invoiceClass->calculateInvoiceAmountByCourse($student['IdStudentRegistration'],$item['semester_id'],$sub['IdSubject'], $item['item_id']);
				$subjects[$x][$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : 0;
				$subjects[$x][$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';

				$sub[$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : '-';
				$sub[$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';
			}
			
			$subjects[$x]['prev_taken'] = in_array($sub['IdSubject'], $pastregSubjects) ? 1 : 0;
			$sub['prev_taken'] = in_array($sub['IdSubject'], $pastregSubjects) ? 1 : 0;
			$subjectsById[$sub['IdSubject']] = $sub;
			//$items[$x] = ;
		}

		//views
		$this->view->items = $items;
		$this->view->semester = $semester;
		$this->view->subjects = $subjects;
		$this->view->program = $program;

		if ($this->getRequest()->isPost()) 
		{ 
		    $formData = $this->getRequest()->getPost();

			$errMsg = '';

			//subjects
			$totalsubjects = count($formData['subject']);

			if ( $totalsubjects == 0 )
			{
				$errMsg = 'You need to select atleast one subject to register';
			}
			else
			{
				$schedulecheck = $examcheck = array();

				//items
				foreach ( $formData['subject'] as $subject )
				{
					foreach ( $formData['item'][$subject] as $item )
					{
                        if ( isset($item['credit']  ))
                        {
                            if ( $item['item'] != '' )
                            {
                                $subjectsById[$subject][$item['item']]['section'] = '';
                                $subjectsById[$subject][$item['item']]['schedule'] = '';
                                $subjectsById[$subject][$item['item']]['credit'] = $item['credit'];
                            }
                        }
						else if ( isset($item['item'] ) )
						{

                            if ( $item['item'] != '' )
							{
								if ( $item['section'] == '' )
								{
									if($subject != 62){ //GD5899 dah hardcode at the first place
										if ( $itemsById[$item['item']]['item_code'] != 'EXAM' )
										{
											$errMsg = 'You did not select a <u>schedule</u> for <strong>'.$subjectsById[$subject]['SubjectName'].' - '.$itemsById[$item['item']]['item_name'].'</strong>';
											break;
										}
									}
									
									$subjectsById[$subject][$item['item']]['section'] = '';
	                                $subjectsById[$subject][$item['item']]['schedule'] = '';
	                                $subjectsById[$subject][$item['item']]['credit'] = '';
								}
                                
								else
								{
									$sectioninfo = $courseGroupDb->getInfo($item['section']);
									$schedule = $courseGroupScheduleDb->getSchedule($sectioninfo['IdCourseTaggingGroup'], $item['item']);
	
									$subjectsById[$subject][$item['item']]['section'] = $sectioninfo;
									$subjectsById[$subject][$item['item']]['schedule'] = $schedule;
									
									//exam check
									if ($itemsById[$item['item']]['item_code'] == 'EXAM' )
									{
										$examschedule = $examscheduleDb->getScheduleBySubject($semester['IdSemesterMaster'], $subject);
										
										//check
										if ( isset($examcheck[$examschedule['es_date']]) )
										{
											foreach ( $examcheck[$examschedule['es_date']] as $echk )
											{
												if ( $echk['subject'] != $subject ) 
												{
													if ( $examschedule['es_start_time'] >= $echk['start'] )
													{
														$errMsg = 'Possible exam conflict for <u>'.$subjectsById[$subject]['SubjectName'].'</u> and <u>'.$subjectsById[$echk['subject']]['SubjectName'].'</u>';

														break;
													}
												}
											}
										}

										//
										$examcheck[$examschedule['es_date']][] = array(
																						'semester_id'	=> $semester['IdSemesterMaster'],
																						'subject'		=> $subject,
																						'start'			=> $examschedule['es_start_time'],
																						'end'			=> $examschedule['es_end_time']
																					  );
																					  
										 $examschedule = $examscheduleDb->getScheduleBySubject($semester['IdSemesterMaster'], $subject);
										 
										 $subjectsById[$subject][$item['item']]['section'] = '';
										 $subjectsById[$subject][$item['item']]['schedule'] = $examschedule;
									
																					  
																					  
									}


									if($subject != 62){ //GD5899 dah hardcode at the first place
									
										//schedule check
										foreach ( $schedule as $sc )
										{
											$method = $sc['sc_date'] == '0000-00-00' || empty($sc['sc_date']) ? 'sc_day' : 'sc_date';
											
											if ( isset($schedulecheck[$sc[$method]]) )
											{
												foreach ( $schedulecheck[$sc[$method]] as $schk )
												{
													
													if ( $schk['subject'] != $subject ) 
													{
														if ( $sc['sc_start_time'] >= $schk['start'] )
														{
															
															$errMsg = 'Possible xx schedule conflict for <u>'.$subjectsById[$subject]['SubjectName'].'</u> and <u>'.$subjectsById[$schk['subject']]['SubjectName'].'</u>';
	
															$errMsg .= '<div style="overflow:hidden"><div style="width:320px; float:left; margin-right:10px;"><table class="table"><tr><th>Date</th><th>Day</th><th>Start Time</th><th>End Time</th></tr>';
															$sch1 = $courseGroupScheduleDb->getSchedule($sectioninfo['IdCourseTaggingGroup'], $item['item']);
															foreach ( $sch1 as $sc1 )
															{
																$errMsg .= '<tr><td>'.format_date($sc1['sc_date']).'</td><td>'.$sc1['sc_day'].'</td><td>'.$sc1['sc_start_time'].'</td><td>'.$sc1['sc_end_time'].'</td></tr>';
															}
															$errMsg .= '</table></div>';
	
															$errMsg .= '<div style="width:320px; float:left;"><table class="table"><tr><th>Date</th><th>Day</th><th>Start Time</th><th>End Time</th></tr>';
															$sch2 = $courseGroupScheduleDb->getSchedule($schk['group'], $schk['item']);
															foreach ( $sch2 as $sc2 )
															{
																$errMsg .= '<tr><td>'.format_date($sc2['sc_date']).'</td><td>'.$sc2['sc_day'].'</td><td>'.$sc2['sc_start_time'].'</td><td>'.$sc2['sc_end_time'].'</td></tr>';
															}
	
															$errMsg .= '</table></div>';
															$errMsg .= '</div>';
											
															break;
														}
													}
												}
											}
	
											$schedulecheck[$sc[$method]][] = array('group' => $sectioninfo['IdCourseTaggingGroup'], 'subject' => $subject, 'item' => $item['item'], 'start' => $sc['sc_start_time'], 'end' => $sc['sc_end_time']);
										}
									}
								}
							}
							
						}

						if ( $errMsg != '' ) break;
					} // item loop

				} // subject loop
			}
			
			$this->view->subjectsById = $subjectsById;
			$this->view->itemsById = $itemsById;
			$this->view->formData = $formData;

		}
		else
		{
			$errMsg = 'You cannot access this page directly';
		}

		$this->view->errMsg = $errMsg;
	}

	public function confirmAction()
	{
		$this->view->title = 'Course Registration - Summary';
		
		$student = $this->studentinfo;
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$courseGroupDb = new App_Model_General_DbTable_CourseGroup();
		$courseGroupScheduleDb = new App_Model_General_DbTable_CourseGroupSchedule();
		$studentSemesterStatusDB = new CourseRegistration_Model_DbTable_Studentsemesterstatus();
		$auth = Zend_Auth::getInstance();
		
			
		$subjectsById = '';
		$itemsById = '';

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}

		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		
		//Check Barring//
		//1. Outstanding Previous Semester invoices
		$prvSemester=$semesterDb->getPreviousSemester($semester);
		
		$invoiceClass = new icampus_Function_Studentfinance_Invoice();
		$os=$invoiceClass->checkOutstandingBalance($student['IdStudentRegistration'],2,$prvSemester["IdSemesterMaster"]);
		
		if($os==2) throw new Exception ('You are not allowed to register for new courses due to outstanding balance. Please contact Bursary.');
		
		//2. From Barring Screen
		$barlist = $this->courseregDb->checkBarring($student['IdStudentRegistration'],$semester["IdSemesterMaster"]);
		
		if($barlist) throw new Exception ('You are not allowed to register for new courses. Please contact ASAD.');
		
		//End Barring check//

		//process other things
		$subjects = $subjectDb->getCourseOffer($student['IdLandscape'],$semester['IdSemesterMaster']);
		
		//get register items
		$items = $regitemDb->getStudentItems($program['IdProgram'], $semester['IdSemesterMaster'], $student['IdProgramScheme']);

		foreach ( $items as $item )
		{
			$itemsById[$item['item_id']] = $item['item_name'];
		}

		//amounts
		$invoiceClass = new icampus_Function_Studentfinance_Invoice();
		foreach( $subjects as $x => $sub ) 
		{
			foreach ( $items as $item )
			{
				$amount = $invoiceClass->calculateInvoiceAmountByCourse($student['IdStudentRegistration'],$item['semester_id'],$sub['IdSubject'], $item['item_id']);
				$subjects[$x][$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : 0;
				$subjects[$x][$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';

				$sub[$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : 0;
				$sub[$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';
			}
			
			$subjectsById[$sub['IdSubject']] = $sub;
			//$items[$x] = ;
		}

		//views
		$this->view->items = $items;
		$this->view->semester = $semester;
		$this->view->subjects = $subjects;
		
		$db = getDb();

		if ($this->getRequest()->isPost()) 
		{ 
		    $formData = $this->getRequest()->getPost();

			$errMsg = '';
			$tagging = array();
			
			$totalsubjects = count($formData['subject']);

			if ( $totalsubjects == 0 )
			{
				$errMsg = 'You need to select atleast one subject to register';
			}
			else
			{
				$data = array(
								'student_id'	=> $auth->getIdentity()->id, 
								'reg_id'		=> $student['IdStudentRegistration'],
								'semester_id'	=> $semester['IdSemesterMaster'],
								'created_date'	=> new Zend_Db_Expr('NOW()'),
								'session_type'	=> 'subject',
								'logdata'		=> json_encode($formData)
							);

				$db->insert('tbl_studentregsubjects_session', $data);
				$session_id = $db->lastInsertId();

				
				//student semester status
				//checking semester status exist or not
				$semesterStatus = $studentSemesterStatusDB->getSemesterStatusBySemester( $student['IdStudentRegistration'],$semester['IdSemesterMaster']);
				
				if(empty($semesterStatus)){
					$dataSemStatus = array(
								'IdStudentRegistration' => $student['IdStudentRegistration'],
								'idSemester'				=> $semester['IdSemesterMaster'],
								'IdSemesterMain'		=> $semester['IdSemesterMaster'],
								'studentsemesterstatus'		=> 130,//registered
								'UpdUser'				=> $auth->getIdentity()->id,
								'UpdDate'				=> date("Y-m-d H:i:s"),
								'UpdRole'				=> $auth->getIdentity()->role,
								'Level'				=> 1,
				);

					$db->insert('tbl_studentsemesterstatus', $dataSemStatus);
					$semstatus_id = $db->lastInsertId();
				
				}
					
				
				foreach ( $formData['subject'] as $subject )
				{
					$db->beginTransaction();
					try 
					{
						//regsubjects
						$data = array(
										'IdStudentRegistration' => $student['IdStudentRegistration'],
										'IdSubject'				=> $subject,
										'IdSemesterMain'		=> $semester['IdSemesterMaster'],
										'UpdUser'				=> $auth->getIdentity()->id,
										'UpdDate'				=> date("Y-m-d H:i:s"),
										'UpdRole'				=> $auth->getIdentity()->role,
										'Active'				=> 0,
                                        'credit_hour_registered'=> $formData['ec'][$subject]['credit']
						);

						$db->insert('tbl_studentregsubjects', $data);
						$regsub_id = $db->lastInsertId();

						//add into history
						$data = array(
										'IdStudentRegistration' => $student['IdStudentRegistration'],
										'IdSubject'				=> $subject,
										'IdSemesterMain'		=> $semester['IdSemesterMaster'],
										'UpdUser'				=> $auth->getIdentity()->id,
										'UpdDate'				=> date("Y-m-d H:i:s"),
										'UpdRole'				=> $auth->getIdentity()->role,
										'Active'				=> 0
						);

						$db->insert('tbl_studentregsubjects_history', $data);
					
						if ( is_numeric($formData['ec'][$subject]['city']) )
						{
							$city = $formData['ec'][$subject]['city'];
							$city_others = ''; 
						}
						else
						{
							$city = 99;
							$city_others = $formData['ec'][$subject]['city']; 
						}

						//exam_registration
						$data = array(
											'er_idCountry'				=> $formData['ec'][$subject]['country'],
											'er_idCity'					=> $city,
											'er_idCityOthers'			=> $city_others,
											'er_idSemester'				=> $semester['IdSemesterMaster'],
											'er_idStudentRegistration'	=> $student['IdStudentRegistration'],
											'er_idSubject'				=> $subject,
											'er_status'					=> 764,
											'er_createdby'				=> 1,
											'er_createddt'				=> date("Y-m-d H:i:s")
									);

						$db->insert('exam_registration', $data);
						

						//item (details)
						$total = 0;
						foreach ( $formData['item'][$subject] as $item )
						{
							if ( $subjectsById[$subject][$item['item']]['amount'] > 0 )
							{
								$total += $subjectsById[$subject][$item['item']]['amount'];
							}

							//regsubjects_detail
							$data = array(
											'student_id'		=> $student['IdStudentRegistration'],
											'regsub_id'			=> $regsub_id,
											'semester_id'		=> $semester['IdSemesterMaster'],
											'subject_id'		=> $subject,
											'item_id'			=> $item['item'],
											'section_id'		=> $item['section'],
											'ec_country'		=> $formData['ec'][$subject]['country'],
											'ec_city'			=> $formData['ec'][$subject]['city'],
											'created_date'		=> new Zend_Db_Expr('NOW()'),
											'created_by'		=> $auth->getIdentity()->id,
											'created_role'		=> $auth->getIdentity()->role,
											'ses_id'			=> $session_id
							);

							$db->insert('tbl_studentregsubjects_detail', $data);
							
							//only once. 
							if ( !in_array( $subject, $tagging) )
							{
								//tbl_course_group_student_mapping
								if ( $item['section'] > 0 )
								{
									$data = array(
													'IdCourseTaggingGroup'	=> $item['section'],
													'IdStudent'				=> $student['IdStudentRegistration'],
													'IdSubject'				=> $subject,
													'UpdDate'				=> new Zend_Db_Expr('NOW()')
												);

									$db->insert('tbl_course_group_student_mapping', $data);
									
									$db->update('tbl_studentregsubjects', array('IdCourseTaggingGroup' => $item['section']), array('IdStudentRegSubjects = ?' => $regsub_id));

									$tagging[] = $subject;
								}
							}

						} //foreach

						//all is well
						$db->commit();
						
						if ( $total > 0 )
						{
							$invoiceClass->generateInvoiceStudent($student['IdStudentRegistration'],$semester['IdSemesterMaster'],$subject);
						}
					}
					catch (Exception $e) 
					{
						$db->rollBack();
						$db->delete('tbl_studentregsubjects_session', array('id = ?' => $session_id));

						$errMsg = $e->getMessage();

						throw $e;
					}

				}//foreach

				$this->_helper->flashMessenger->addMessage(array('success' => "Courses successfully registered"));

				//redirect
				$this->_redirect( 'course-registration/index/summary/id/'.$session_id );

			} //total
			
			$this->view->subjectsById = $subjectsById;
			$this->view->itemsById = $itemsById;
			
			$this->view->formData = $formData;
		}
		else
		{
			$errMsg = 'You cannot access this page directly';
		}

		$this->view->errMsg = $errMsg;
	}
	
	public function summaryAction()
	{
		$id = $this->_getParam('id');

		$this->view->title = 'Course Registration - Summary';
		
		$student = $this->studentinfo;
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$courseGroupDb = new App_Model_General_DbTable_CourseGroup();
		$courseGroupScheduleDb = new App_Model_General_DbTable_CourseGroupSchedule();
		$auth = Zend_Auth::getInstance();
		
			
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$courseGroupDb = new App_Model_General_DbTable_CourseGroup();
		$courseGroupScheduleDb = new App_Model_General_DbTable_CourseGroupSchedule();
		$auth = Zend_Auth::getInstance();
		
			
		$subjectsById = '';
		$itemsById = '';

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}
		
		//get summary
		$db = getDb();
	  	$select = $db->select()
	                 ->from(array('ec'=>'tbl_studentregsubjects_session'))
					 ->where('id = ?', $id )
					 ->where('student_id = ?',  $student['IdStudentRegistration']);
					 
		$summary = $db->fetchRow($select);

		if ( empty($summary) )
		{
			throw new Exception('Invalid Summary ID');
		}

		$data = json_decode($summary['logdata'],true);

		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		

		//process other things
		$subjects = $subjectDb->getLandscapeCourses($student['IdLandscape']);
		//$subjects = $subjectDb->getCourseOffer($student['IdLandscape'],$semester['IdSemesterMaster']);

		//get register items
		$items = $regitemDb->getStudentItems($program['IdProgram'], $summary['semester_id'], $student['IdProgramScheme']);

		foreach ( $items as $item )
		{
			$itemsById[$item['item_id']] = $item['item_name'];
		}
		
		//amounts
		$invoiceClass = new icampus_Function_Studentfinance_Invoice();
		foreach( $subjects as $x => $sub ) 
		{
			if ( in_array($sub['IdSubject'], $data['subject']) )
			{
				foreach ( $items as $item )
				{
					$amount = $invoiceClass->calculateInvoiceAmountByCourse($student['IdStudentRegistration'],$item['semester_id'],$sub['IdSubject'], $item['item_id']);

					$sub[$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : '-';
					$sub[$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';
					
					if (  isset($data['item'][$sub['IdSubject']][$item['item_id']]['section']) )
					{
						if($sub['IdSubject'] != 62){ //GD5899 dah hardcode at the first place
							if ( $item['item_code'] != 'EXAM' )
							{
								$sectioninfo = $courseGroupDb->getInfo($data['item'][$sub['IdSubject']][$item['item_id']]['section']);
								
								$schedule = $courseGroupScheduleDb->getSchedule($sectioninfo['IdCourseTaggingGroup'], $item['item_id']);
	
								$sub[$item['item_id']]['section'] = $sectioninfo;
								$sub[$item['item_id']]['schedule'] = $schedule;
							}
							else
							{
								$sub[$item['item_id']]['section'] = '';
								$sub[$item['item_id']]['schedule'] = '';
							}
						}else{
							$sub[$item['item_id']]['section'] = '';
							$sub[$item['item_id']]['schedule'] = '';
						}
					}
				}
			
			
				$subjectsById[$sub['IdSubject']] = $sub;
			}
		}

		//views
		$this->view->items = $items;
		$this->view->semester = $semester;
	

		$db = getDb();
	  	$select = $db->select()
	                 
					 ->from(array('s'=>'tbl_subjectmaster'), array('SubjectName', 'SubCode', 'CreditHours'))
					 ->where('IdSubject IN (?)',$data['subject']);
					 
		$subjects = $db->fetchAll($select);
		
		foreach ( $subjects as $i => $subject )
		{
		}
		
		$this->view->summary = $summary;
		$this->view->data = $data;
		$this->view->subjects = $subjects;
		$this->view->subjectsById = $subjectsById;
		$this->view->itemsById = $itemsById;
	}

	public function manageAction()
	{
		$this->view->title = 'Manage Registration';
		
		$student = $this->studentinfo;
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$defDb = new App_Model_General_DbTable_Definationms();
		$examscheduleDb = new App_Model_Exam_DbTable_ExamSchedule();
		$auth = Zend_Auth::getInstance();
		
		$errMsg = '';

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}


		//token
		$token = sha1($student['IdStudentRegistration'].'-'.date('dmy'));
		$this->view->token = $token;

		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		
		//process other things
		if($semester)
		{
			$db = getDb();
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();

			//get register items
			$items = $regitemDb->getStudentItems($program['IdProgram'], $semester['IdSemesterMaster'], $student['IdProgramScheme']);
			
			foreach ( $items as $item )
			{
				$itemsById[$item['item_id']] = $item['item_name'];
			}

			//register this sem
			$registered = $this->courseregDb->subjectRegistered($student['IdStudentRegistration'], $semester['IdSemesterMaster']);
			
			//registered details
			$details = $this->courseregDb->getDetails($student['IdStudentRegistration'], $semester['IdSemesterMaster']);
			$detail = array();

			foreach ( $details as $det )
			{
				$det_amount = $invoiceClass->calculateInvoiceAmountByCourse($student['IdStudentRegistration'],$det['semester_id'],$det['subject_id'], $det['item_id']);
				
				$det['amount'] = !empty($det_amount) ? $det_amount[0]['amount'] : 0;
				$det['currency'] = !empty($det_amount) ? $det_amount[0]['cur_code'] : '';
				
				$detail[$det['subject_id']][$det['item_id']] = $det;
				
			}
			
			//loop
			foreach ( $registered as $i => $reg )
			{
				$registered[$i]['item'] = $detail[$reg['IdSubject']];
				$registered[$i]['ec'] = $this->courseregDb->getEC($student['IdStudentRegistration'], $reg['IdSubject'], $semester['IdSemesterMaster']);
			}

			//POST
			if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'submit' ) ) 
			{ 
				$formData = $this->getRequest()->getPost();

				$errMsg = '';
				
				if ( $formData['token'] != $token )
				{
					$errMsg = 'Invalid Token. Please try again.';
				}
				else
				{
					$total = count($formData['subject']);

					if ( $total == 0 )
					{
						throw new Exception('You need to select atleast one course to drop');
					}
					
					$registeredSubjects = array();
					foreach ( $registered as $regged ) 
					{
						$registeredSubjects[] = $regged['IdSubject'];
						$regsub[$regged['IdSubject']] = $regged;
					}

					foreach ( $formData['subject'] as $subject )
					{
						if ( !in_array($subject, $registeredSubjects) )
						{
							throw new Exception('You cannot drop a course you never registered.');
						}

						//drop subject
						$db->beginTransaction();
						try 
						{
							$regsub_id = $regsub[$subject]['IdStudentRegSubjects'];

							//regsubjects
							$data = array(
											'IdStudentRegistration  = ?' => $student['IdStudentRegistration'],
											'IdSubject				= ?' => $subject,
											'IdSemesterMain			= ?' => $semester['IdSemesterMaster']
							);

							$db->delete('tbl_studentregsubjects', $data);

							//add into history
							$data = array(
											'IdStudentRegistration' => $student['IdStudentRegistration'],
											'IdSubject'				=> $subject,
											'IdSemesterMain'		=> $semester['IdSemesterMaster'],
											'UpdUser'				=> $auth->getIdentity()->id,
											'UpdDate'				=> date("Y-m-d H:i:s"),
											'UpdRole'				=> $auth->getIdentity()->role,
											'Active'				=> 2
							);

							$db->insert('tbl_studentregsubjects_history', $data);

							//exam_registration
							$data = array(
												'er_idSemester				= ?' => $semester['IdSemesterMaster'],
												'er_idStudentRegistration	= ?' => $student['IdStudentRegistration'],
												'er_idSubject				= ?' => $subject
										);
							
							$db->delete('exam_registration', $data);
							

							//item (details)
							
							//regsubjects_detail
							$data = array(
											'student_id		= ?' => $student['IdStudentRegistration'],
											'regsub_id		= ?' => $regsub_id,
											'semester_id	= ?' => $semester['IdSemesterMaster'],
											'subject_id		= ?' => $subject
							);

							$db->delete('tbl_studentregsubjects_detail', $data);
						
							$data = array(
											'IdStudent		= ?' => $student['IdStudentRegistration'],
											'IdSubject		= ?' => $subject
										);

							$db->delete('tbl_course_group_student_mapping', $data);
									

							//all is well
							$db->commit();
						}
						catch (Exception $e) 
						{
							$db->rollBack();
							$db->delete('tbl_studentregsubjects_session', array('id = ?' => $session_id));

							//$errMsg = $e->getMessage();

							throw $e;
						}
					} // foreach 

					//add into record
					unset($formData['token'], $formData['submit']);
					$data = array(
								'student_id'	=> $auth->getIdentity()->id, 
								'reg_id'		=> $student['IdStudentRegistration'],
								'semester_id'	=> $semester['IdSemesterMaster'],
								'created_date'	=> new Zend_Db_Expr('NOW()'),
								'session_type'	=> 'dropsubject',
								'logdata'		=> json_encode($formData)
							);

					$db->insert('tbl_studentregsubjects_session', $data);

					$this->_helper->flashMessenger->addMessage(array('success' => "Courses successfully dropped"));

					//redirect
					$this->_redirect( 'course-registration/index/manage/' );
				}
			}

			//views
			$this->view->registered = $registered;
			$this->view->program = $program;
			$this->view->items = $items;
			$this->view->itemsById = $itemsById;
			$this->view->semester = $semester;
		}
		else
		{
			$errMsg = 'No active semester found';
		}

		$this->view->errMsg = $errMsg;
	}

	public function manageItemAction()
	{
		$this->view->title = 'Item Registration';
		
		$student = $this->studentinfo;

		$id_subject = $this->_getParam('subject');

		if ( empty($id_subject) )
		{
			throw new Exception('Invalid Subject ID');
		}
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$defDb = new App_Model_General_DbTable_Definationms();
		$examscheduleDb = new App_Model_Exam_DbTable_ExamSchedule();
		$courseGroupDb = new App_Model_General_DbTable_CourseGroup();
		$courseGroupScheduleDb = new App_Model_General_DbTable_CourseGroupSchedule();
		$auth = Zend_Auth::getInstance();

		$errMsg = '';

		if ( empty($landscape) )
		{
			throw new Exception('You have no active landscape');
		}

		//get programme
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		
		//process other things
		if($semester)
		{
			$db = getDb();
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();

			//get register items
			$items = $regitemDb->getStudentItems($program['IdProgram'], $semester['IdSemesterMaster'], $student['IdProgramScheme']);
			
			foreach ( $items as $item )
			{
				$itemsById[$item['item_id']] = $item;
			}

			//register this sem
			$registered = $this->courseregDb->subjectRegistered($student['IdStudentRegistration'], $semester['IdSemesterMaster']);

			//registered details
			$details = $this->courseregDb->getDetails($student['IdStudentRegistration'], $semester['IdSemesterMaster']);
			$detail = array();

			foreach ( $details as $det )
			{
				$itemRegistered[$det['subject_id']][] = $det['item_id'];	
			}
			
			//
			$pastregSubjects = $this->getPastreg($student, $semester);

			//subjects
			$subjects = array();
			foreach( $registered as $x => $sub ) 
			{
				$subjects[$sub['IdSubject']] = $sub;
				foreach ( $items as $item )
				{
					$amount = $invoiceClass->calculateInvoiceAmountByCourse($student['IdStudentRegistration'],$item['semester_id'],$sub['IdSubject'], $item['item_id']);
					$subjects[$sub['IdSubject']][$item['item_id']]['amount'] = !empty($amount) ? $amount[0]['amount'] : 0;
					$subjects[$sub['IdSubject']][$item['item_id']]['currency'] = !empty($amount) ? $amount[0]['cur_code'] : '';
				}
					
				$subjects[$sub['IdSubject']]['prev_taken'] = in_array($sub['IdSubject'], $pastregSubjects) ? 1 : 0;
				//$items[$x] = ;
	
				$examschedule = $examscheduleDb->getScheduleBySubject($semester['IdSemesterMaster'], $sub['IdSubject']);
				$subjects[$sub['IdSubject']]['exam_schedule'] = $examschedule;
			}
			
			//section data
			$sectiondata = $this->courseregDb->getSectionsTagged($student['IdStudentRegistration']);
			$section = array();
			foreach ( $sectiondata  as $sec )
			{
				$section[$sec['IdSubject']] = $sec['IdCourseTaggingGroup'];
			}

			//check if subject is actually registered
			if ( !array_key_exists($id_subject, $subjects) )
			{
				throw new Exception('You didn\'t register this course');
			}

			$subject = array();
			foreach ( $registered as $reg )
			{
				if ( $reg['IdSubject'] == $id_subject )
				{
					$subject = $reg;
				}
			}

			if ( empty($subject) )
			{
				$errMsg = 'No subject data found';
			}

			//token
			$token = sha1($student['IdStudentRegistration'].'-'.$id_subject.'-'.date('dmy'));
			$this->view->token = $token;

			//POST
			$isconfirm = 0;
			if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'submit' ) ) 
			{ 
				$formData = $this->getRequest()->getPost();

				$errMsg = '';
				
				if ( $formData['token'] != $token )
				{
					$errMsg = 'Invalid Token. Please try again.';
				}
				else
				{
					$confirmdata = array();
					$isconfirm=1;

					//postdata item
					if ( isset($formData['item'][$id_subject]) && !empty($formData['item'][$id_subject]) )
					{
						foreach ( $formData['item'][$id_subject] as $form_item )
						{
							//add
							if ( !in_array($form_item['item'], $itemRegistered[$id_subject]) )
							{
								$confirmdata['add'][$form_item['item']] = $itemsById[$form_item['item']];
								$confirmdata['add'][$form_item['item']]['amount'] = $subjects[$id_subject][$form_item['item']]['amount'];
								$confirmdata['add'][$form_item['item']]['currency'] = $subjects[$id_subject][$form_item['item']]['currency'];
								
								//check if has section
								if ( $formData['section'][$id_subject][$form_item['item']] == '' )
								{
									$errMsg = 'You did not select a <u>schedule</u> for <strong>'.$itemsById[$form_item['item']]['item_name'].'</strong>';
									break;
								}
								else
								{
									$sectioninfo = $courseGroupDb->getInfo($formData['section'][$id_subject][$form_item['item']]);
									$schedule = $courseGroupScheduleDb->getSchedule($sectioninfo['IdCourseTaggingGroup'], $form_item['item']);
	
									$confirmdata['add'][$form_item['item']]['section'] = $sectioninfo;
									$confirmdata['add'][$form_item['item']]['schedule'] = $schedule;
								}

							}

						}// foreach

					} //postdata item
				
					foreach ( $itemRegistered[$id_subject] as $itemreg )
					{
						if ( $itemsById[$itemreg]['mandatory'] != 1 )
						{
							if ( !isset($formData['item'][$id_subject][$itemreg]) )
							{
								//drop
								$confirmdata['drop'][$itemreg] = $itemsById[$itemreg];
								$confirmdata['drop'][$itemreg]['amount'] = $subjects[$id_subject][$itemreg]['amount'];
								$confirmdata['drop'][$itemreg]['currency'] = $subjects[$id_subject][$itemreg]['currency'];
							}
						}
					}


					$this->view->confirmdata = $confirmdata;

				} //token
			}//post

			//POST
			if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'confirm' ) ) 
			{ 
				$formData = $this->getRequest()->getPost();
				
				if ( empty($formData['item']) )
				{
					throw new Exception('No item to add/drop');
				}

				$totaladd = !isset($formData['item']['add']) ? 0 : count($formData['item']['add']);
				$totaldrop = !isset($formData['item']['drop']) ? 0 : count($formData['item']['drop']);

				if ( ($totaladd+$totaldrop) == 0 )
				{
					$errMsg = 'You need to select atleast one item to add/drop';
				}
				else
				{
					$formdata2 = $formData;
					unset($formdata2['token'], $formdata2['confirm']);
					$data = array(
									'student_id'	=> $auth->getIdentity()->id, 
									'reg_id'		=> $student['IdStudentRegistration'],
									'semester_id'	=> $semester['IdSemesterMaster'],
									'created_date'	=> new Zend_Db_Expr('NOW()'),
									'session_type'	=> 'changeitem',
									'logdata'		=> json_encode($formdata2)
								);

					$db->insert('tbl_studentregsubjects_session', $data);
					$session_id = $db->lastInsertId();
					
					$totaladd_amt = 0;

					if ( $totaladd > 0 )
					{
						foreach ( $formData['item']['add'] as $item )
						{
							$regsub_id = $subject['IdStudentRegSubjects'];

							$totaladd_amt += $subjects[$id_subject][$item]['amount'];
							
							$db->beginTransaction();
							try 
							{
								if ( !isset($formData['itemschedule'][$item]) )
								{
									throw new Exception('Item schedule cannot be empty');
								}

								if ( empty($formData['itemschedule'][$item]) )
								{
									throw new Exception('Item schedule cannot be empty');
								}

								//regsubjects_detail
								$data = array(
												'student_id'		=> $student['IdStudentRegistration'],
												'regsub_id'			=> $regsub_id,
												'semester_id'		=> $semester['IdSemesterMaster'],
												'subject_id'		=> $id_subject,
												'item_id'			=> $item,
												'section_id'		=> $formData['itemschedule'][$item],
												'ec_country'		=> '',
												'ec_city'			=> '',
												'created_date'		=> new Zend_Db_Expr('NOW()'),
												'created_by'		=> $auth->getIdentity()->id,
												'created_role'		=> $auth->getIdentity()->role,
												'ses_id'			=> $session_id
								);

								$db->insert('tbl_studentregsubjects_detail', $data);
								
								//remove old mapping
								$db->delete('tbl_course_group_student_mapping', array(
																							'IdStudent = ?' => $student['IdStudentRegistration'],
																							'IdSubject = ?' => $id_subject
																						));
								$data = array(
													'IdCourseTaggingGroup'	=> $formData['itemschedule'][$item],
													'IdStudent'				=> $student['IdStudentRegistration'],
													'IdSubject'				=> $id_subject,
													'UpdDate'				=> new Zend_Db_Expr('NOW()')
											);
								
								$db->insert('tbl_course_group_student_mapping', $data);
								
								$db->update('tbl_studentregsubjects', array('IdCourseTaggingGroup' => $formData['itemschedule'][$item]), array('IdStudentRegSubjects = ?' => $regsub_id));
							

								//all is well
								$db->commit();
							
							}
							catch (Exception $e) 
							{
								$db->rollBack();
								$db->delete('tbl_studentregsubjects_session', array('id = ?' => $session_id));
								throw $e;
							}

						}//foreach

					}//add

					if ( $totaldrop > 0 )
					{
						foreach ( $formData['item']['drop'] as $item )
						{
							$regsub_id = $subject['IdStudentRegSubjects'];
							
							//get detail
							$select = $db->select()
													->from(array('a'=>'tbl_studentregsubjects_detail'))
													->where('a.student_id = ?', $student['IdStudentRegistration'])
													->where('a.semester_id = ?', $semester['IdSemesterMaster'])
													->where('a.subject_id = ?', $id_subject)
													->where('a.item_id = ?', $item)
													->where('a.regsub_id = ?', $regsub_id);
							$reg_detail = $db->fetchRow($select);

							if ( $itemsById[$item]['mandatory'] == 1 )
							{
								throw new Exception('You cannot drop a mandatory item ('.$itemsById[$item]['item_name'].') for this subject');
							}

							$db->beginTransaction();
							try 
							{
								//regsubjects_detail
								$cond = array(
												'student_id = ?'	=> $student['IdStudentRegistration'],
												'semester_id = ?'	=> $semester['IdSemesterMaster'],
												'subject_id = ?'	=> $id_subject,
												'item_id = ?'		=> $item
											 );

								$db->delete('tbl_studentregsubjects_detail', $cond );

								//all is well
								$db->commit();
							
							}
							catch (Exception $e) 
							{
								$db->rollBack();
								throw $e;
							}
							
							//CN
							if ( isset($reg_detail['invoice_id']) )
							{
								$invoiceClass->generateCreditNote($student['IdStudentRegistration'],$id_subject,$reg_detail['invoice_id'],$item);
							}

						}//foreach

					}//drop

					if ( $totaladd > 0 && $totaladd_amt > 0 )
					{
						$invoiceClass->generateInvoiceStudent($student['IdStudentRegistration'],$semester['IdSemesterMaster'],$id_subject);
					}

					$this->_helper->flashMessenger->addMessage(array('success' => "Registration item successfully added/dropped"));

					//redirect
					$this->_redirect( 'course-registration/index/manage/' );

				} //total
				
			}

			//views
			$this->view->registered = $registered;
			$this->view->program = $program;
			$this->view->items = $items;
			$this->view->itemRegistered = $itemRegistered;
			$this->view->itemsById = $itemsById;
			$this->view->subjects = $subjects; //all reg subjects
			$this->view->semester = $semester;
			$this->view->subject = $subject; //this subject
			$this->view->errMsg = $errMsg;
			$this->view->id_subject = $id_subject;
			$this->view->section = $section;

			if ( $isconfirm )
			{
				$this->view->title = 'Item Registration - Confirmation';
				echo $this->render('manage-item-confirm');
			}
		}
	}

	public function historyAction()
	{
		$this->view->title = 'Registration History';
		
		$student = $this->studentinfo;
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$defDb = new App_Model_General_DbTable_Definationms();

		$p_data = $this->courseregDb->getHistory($student['IdStudentRegistration']);
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage(50);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function historyViewAction()
	{
		$this->view->title = 'Registration History Detail';
		
		$student = $this->studentinfo;
		
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$landscapeDb = new App_Model_Record_DbTable_Landscape();
		$landscape = $landscapeDb->getData($student["IdLandscape"]);
		$subjectDb = new App_Model_Record_DbTable_LandscapeSubject();
		$regitemDb = new App_Model_Registration_DbTable_RegistrationItem();
		$defDb = new App_Model_General_DbTable_Definationms();
		
		$id = $this->_getParam('id');
		$history = $this->courseregDb->getHistoryData($id);

		
		if ( empty($history) )
		{
			throw new Exception('Invalid History');
		}
		
		if ( $history['student_id'] != $student['IdStudentRegistration'])
		{
			throw new Exception('You don\'t have permission to view this');
		}
		
		$data = $history['logdata'] = json_decode($history['logdata'], true);
		
		$itemsById = array();
		$items = $regitemDb->getStudentItems($student['IdProgram'], $history['semester_id'], $student['IdProgramScheme']);
		foreach ( $items as $item )
		{
			$itemsById[$item['item_id']] = $item;
		}

		//process data
		if ( $history['session_type'] == 'subject' )
		{
			$regsub = array();
			foreach ( $data['subject'] as $subject ) 
			{
				$regsub[] = $subject;
			}

			$subjects = $this->courseregDb->getSubjects($regsub);
			
			$this->view->subjects = $subjects;
		}
		else if ( $history['session_type'] == 'changeitem' )
		{
			$subject = $this->courseregDb->getSubjects(array($data['id_subject']));
			$this->view->subject = $subject[0];

		}
		else if ( $history['session_type'] == 'dropsubject' )
		{
			$subjects = $this->courseregDb->getSubjects($data['subject']);
			$this->view->subjects = $subjects;
		}

		//views
		$this->view->itemsById = $itemsById;
		$this->view->history = $history;
	}

	public static function sessionActivity($val)
	{
		switch($val) 
		{
			case 'subject':		$newval = 'Course Registration';		break;
			case 'changeitem':	$newval = 'Change Registration Items';	break;
			case 'dropsubject':	$newval = 'Drop Courses';				break;
		}
		

		return $newval;
	}
	
	public static function courseStatus($val)
	{
		switch($val) 
		{
			case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
		}

		return $newval;
	}
	
	public function registrationSlipAction()
	{
		$this->view->title = 'Registration Slip';
		
		$student = $this->studentinfo;
		
		$semid = $this->_getParam('semID');
		
//		echo "<pre>";
//		print_r($student);
		
		$db = Zend_Db_Table::getDefaultAdapter();	
	  	$select = $db->select()
	                 ->from(array('a'=>'tbl_studentregsubjects'))
	                 ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=a.IdSemesterMain',array('semester_name'=>'SemesterMainName'))
					 ->where('IdSemesterMain = ?', $semid )
					 ->where('IdStudentRegistration = ?',  $student['IdStudentRegistration'])
					 ->group('IdSemesterMain');
					 
		$semester = $db->fetchRow($select);
		
		//get registered course
		
		$selectCourse = $db->select()
	                 ->from(array('a'=>'tbl_studentregsubjects'))
	                 ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=a.IdSubject',array('*'))
	                 ->join(array('ls'=>'tbl_landscapesubject'),'ls.IdSubject=a.IdSubject',array())
	                 ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=ls.SubjectType',array('SubjectTypeName'=>'DefinitionDesc'))
					 ->where('IdSemesterMain = ?', $semid )
					 ->where('IdStudentRegistration = ?',  $student['IdStudentRegistration'])
					 ->group('ls.IdSubject');
					 
		$listCourse = $db->fetchAll($selectCourse);
		
		
		
		$registered = '
  			<table width="100%" border="1" cellspacing="0" cellpadding="5">
   				<tr >
	   				<th width="3%">No.</th>
	   				<th width="45%">Course</th>
	   				<th width="5%">Credit Hours</th>
	   				<th width="10%">Course Type</th>
	   				<th width="10%">Group</th>
	   				<th width="10%">Status</th>
   				</tr>';
		
		$bil = 1;
		$totalCreditHour = 0;
		foreach($listCourse as $data){
			
			$status = null;
			if($data['Active'] == 1 || $data['Active'] == 0){
				$status = 'Confirmed';
			}
			$registered .='<tr >
	   				<td align="center">'.$bil.'</td>
	   				<td>'.$data['SubCode'].' '.$data['SubjectName'].'</td>
	   				<td align="center">'.$data['CreditHours'].'</td>
	   				<td align="center">'.$data['SubjectTypeName'].'</td>
	   				<td align="center">N/A</td>
	   				<td align="center">'.$status.'</td>
   				</tr>';
			$totalCreditHour = $totalCreditHour+$data['CreditHours'];
			$bil++;
		}
		
   		$registered .='</table>';
   		
   		$registered_info = 'Total Credit Hours : '.$totalCreditHour;
		
		
		$fieldValues = array (
	         '$[fullname]'=>$student['appl_fname'].' '.$student['appl_lname'],
	         '$[studentid]'=>$student['registrationId'],
	         '$[icno]'=>$student['appl_idnumber'],
	         '$[program]'=>$student['ProgramName'],
	         '$[program_mode]'=>$student['ProgramMode'],
	         '$[mode_study]'=>$student['StudyMode'],
	         '$[semester]'=>$semester['semester_name'],
	         '$[registered_course]'=>$registered,
	         '$[total_registered_info]'=>$registered_info,
	         '$[date_print]'=>date('d M Y'),
        );
        
		
		$html_template_path = APPLICATION_PATH."/document/template/registration_slip.html";
					
		$html = file_get_contents($html_template_path);
		
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}
		
		$option = array(
			'content' => $html,
			'save' => false,
			'file_name' => 'Registration_slip',
			'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 180; 
  					  $img_h = 64;
                      $pdf->image("images/logo_text.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
			'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL: +603 7361 4000 #FAX: +603 7351 4094 #WEB: www.inceif.org #EMAIL: info@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
		);
		
		$pdf = generatePdf($option);
							
		exit;
	}
}
