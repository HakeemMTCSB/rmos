<?php
class CourseRegistration_CreditTransferController extends Zend_Controller_Action{
    private $_gobjlog;
    private $lobjcredittransferForm;
    private $lobjstudentregistrationModel;
    private $lobjsubjectmasterModel;
    private $lobjcreditTransferModel;
    private $lobjcreditTransferSubjectModel;
    private $lobjautocreditTransferModel;
    private $lobjlandscapeModel;
    private $lobjstudentprogramchange;
    private $lobjrecordconfigModel;
    private $lobjstudentsubjectmodel;
    private $lobjSemesterModel;

    public function init() { //initialization function
//        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
//        $this->fnsetObj();
		$this->locale = Zend_Registry::get('Zend_Locale');
    	$this->view->translate = Zend_Registry::get('Zend_Translate');
    	Zend_Form::setDefaultTranslator($this->view->translate);
    	
       Zend_Layout::getMvcInstance()->assign('navActive', 'coursereg');

        $this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;

		$this->studentModel = new Portal_Model_DbTable_Student();

		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($this->auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->studentinfo = $this->view->studentinfo = $student;
		
    }

    public function fnsetObj() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->lobjcredittransferForm = new Records_Form_Credittransferapp();
        $this->lobjstudentregistrationModel = new CourseRegistration_Model_DbTable_Studentregistration();
        $this->lobjsubjectmasterModel = new GeneralSetup_Model_DbTable_Subjectmaster();
        $this->lobjcreditTransferModel = new Records_Model_DbTable_Credittransfer();
        $this->lobjcreditTransferSubjectModel = new Records_Model_DbTable_Credittransfersubject();
        $this->lobjautocreditTransferModel = new Application_Model_DbTable_Autocredittransfer();
        $this->lobjlandscapeModel = new GeneralSetup_Model_DbTable_Landscape();
        $this->lobjstudentprogramchange = new Records_Model_DbTable_Studentprogramchange();
        $this->lobjrecordconfigModel = new Records_Model_DbTable_Recordconfig();
	$this->lobjstudentsubjectmodel = new Registration_Model_DbTable_Studentsubjects();
        $this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
    }

    public function indexAllAction() {
        $this->view->title = $this->view->translate("Credit Transfer/Exemption Application");
        $form = new CourseRegistration_Form_SearchCreditTransfer();
        //$this->view->ctAppList = $ctAppList;
        
        $idApp = $this->_getParam('id', 0);
        $this->view->AppType = $idApp;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['search'])){
                $ctAppList = $studentRegistrationDB->getAllCtApp($formData);
                $this->gobjsessionsis->credittransferappcontroller = $ctAppList;
            }else{
                $ctAppList = $studentRegistrationDB->getAllCtApp(array('userby'=>$userId,'AppType'=>$idApp));
            }
        }else{
            $ctAppList = $studentRegistrationDB->getAllCtApp(array('userby'=>$userId,'AppType'=>$idApp));
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->credittransferappcontroller);
        }
        
      /*  $lintpagecount = 1000;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->credittransferappcontroller)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->credittransferappcontroller,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($ctAppList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        */
        $this->view->paginator = $ctAppList;
        $this->view->form = $form;
    }

    public function editcredittransferapplicationAction() {
        $ctId = $this->_getParam('IdCreditTransfer', 0);
        $this->view->title = $this->view->translate('Edit Credit Transfer/Exemption Application');
        
        $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
        
        $getCtInfo = $studentRegistrationDB->getCtInfo($ctId);
        //var_dump($getCtInfo); exit;
        $this->view->ctInfo = $getCtInfo;
        $form = new CourseRegistration_Form_EditCreditTransfer(array('landscapeId'=>$getCtInfo['IdLandscape'], 'apptypeId'=>$getCtInfo['applicationType']));
        $this->view->form = $form;
        
        //address
        $address = '';
        $address .= (($getCtInfo['appl_address1']!='' || $getCtInfo['appl_address1']!=null) ? $getCtInfo['appl_address1']:'');
        $address .= (($getCtInfo['appl_address2']!='' || $getCtInfo['appl_address2']!=null) ? ',<br/>'.$getCtInfo['appl_address2']:'');
        $address .= (($getCtInfo['appl_address3']!='' || $getCtInfo['appl_address3']!=null) ? ',<br/>'.$getCtInfo['appl_address3']:'');
        $address .= (($getCtInfo['appl_postcode']!='' || $getCtInfo['appl_postcode']!=null) ? ',<br/>'.$getCtInfo['appl_postcode']:'');
        $address .= (($getCtInfo['CityName']!='' || $getCtInfo['CityName']!=null) ? ',<br/>'.$getCtInfo['CityName']:'');
        $address .= (($getCtInfo['StateName']!='' || $getCtInfo['StateName']!=null) ? ',<br/>'.$getCtInfo['StateName']:'');
        $address .= (($getCtInfo['CountryName']!='' || $getCtInfo['CountryName']!=null) ? ',<br/>'.$getCtInfo['CountryName']:'');
        
        $this->view->address = $address;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        $userInfo = $studentRegistrationDB->getUser($userId);
        
        $this->view->userInfo = $userInfo;
        
        //ct subject
        $ctSubject = $studentRegistrationDB->getCtSub($getCtInfo['IdCreditTransfer']);
        $this->view->ctSubject = $ctSubject;
        //var_dump($ctSubject); exit;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            //delete ct subject
            if (isset($formData['deleteColVal']) && count($formData['deleteColVal']) > 0){
                foreach ($formData['deleteColVal'] as $deleteColVal){
                    $studentRegistrationDB->deleteCtSub($deleteColVal);
                    $studentRegistrationDB->deleteCtSubMore($deleteColVal);
                    
                    //upload file delete
                    $docList = $studentRegistrationDB->getUploadFile($deleteColVal);
                    
                    if ($docList){
                        foreach ($docList as $docLoop){
                            $fileLocation = DOCUMENT_PATH.$docLoop['FileLocation'];
                            
                            //delete fizikal file
                            unlink($fileLocation);

                            //delete file data
                            $studentRegistrationDB->deleteUploadFile($docLoop['IdCreditTransferSubjectsDocs']);
                        }
                    }
                }
            }
            
            if (isset($formData['courseeq']) && count($formData) > 0){
                foreach ($formData['courseeq'] as $courseeq){
                    $ctSubData = array(
                        'IdCreditTransfer'=>$ctId,
                        'IdSubject'=>null,
                        'IdSemesterMain'=>null,
                        'creditStatus'=>null,
                        'ApplicationDate'=>date('Y-m-d'),
                        'AppliedBy'=>$userId,
                        'ApprovedDate'=>null,
                        'Approvedby'=>null,
                        'ApplicationStatus'=>783,
                        'IdInstitution'=>null,
                        'IdQualification'=>null,
                        'IdSpecialization'=>null,
                        'EquivalentCourse'=>$courseeq,
                        'EquivalentGrade'=>$formData['gred'][$courseeq],
                        'EquivalentCreditHours'=>$formData['credithours'][$courseeq],
                        'Marks'=>null,
                        'Comments'=>$formData['remarks'][$courseeq],
                        'disapprovalComment'=>null,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'charge'=>$formData['charge'][$courseeq],
                        'institutionName'=>$formData['institutionName'][$courseeq]
                    );
                    $IdCreditTransferSubjects = $studentRegistrationDB->insertCtSub($ctSubData);
                    
                    //upload file
                    $folder = '/credittransfer/'.$IdCreditTransferSubjects;
                    $dir = DOCUMENT_PATH.$folder;
            
                    if ( !is_dir($dir) ){
                        if ( mkdir_p($dir) === false )
                        {
                            throw new Exception('Cannot create attachment folder ('.$dir.')');
                        }
                    }

                    //upload file proses
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
                    $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                    $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                    $adapter->addValidator('NotExists', false, $dir);
                    $adapter->setDestination($dir);

                    $files = $adapter->getFileInfo();
                    
                    //var_dump($files); exit;
                    
                    if ($files){
                        $i = 0;
                        foreach ($files as $filesLoop){
                            if (isset($files['uploadFile'.$courseeq.'_'.$i.'_'])){
                                $fileOriName = $files['uploadFile'.$courseeq.'_'.$i.'_']['name'];
                                $fileRename = date('YmdHis').'_'.$fileOriName;
                                $filepath = $files['uploadFile'.$courseeq.'_'.$i.'_']['destination'].'/'.$fileRename;
                                
                                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                                $adapter->isUploaded('uploadFile'.$courseeq.'_'.$i.'_');
                                $adapter->receive('uploadFile'.$courseeq.'_'.$i.'_');
                                
                                if ($files['uploadFile'.$courseeq.'_'.$i.'_']['size']!=null){
                                    $uploadData = array(
                                        'IdCreditTransferSubjects'=>$IdCreditTransferSubjects, 
                                        'FileLocation'=>$folder.'/'.$fileRename, 
                                        'FileName'=>$fileOriName, 
                                        'UploadedFilename'=>$fileRename, 
                                        'FileSize'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['size'], 
                                        'MIMEType'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['type'], 
                                        'UpdDate'=>date('Y-m-d'), 
                                        'UpdUser'=>$userId, 
                                        'Comments'=>''
                                    );
                                    $this->lobjstudentregistrationModel->insertUploadFile($uploadData);
                                }
                            }
                            $i++;
                        }
                    }
                    
                    if (isset($formData['course'][$courseeq]) && count($formData['course'][$courseeq]) > 0){
                        foreach ($formData['course'][$courseeq] as $courseLoop){
                            $ctSubMoreData = array();
                            
                            $ctSubMoreData['csm_creditTransferId']=$ctId;
                            $ctSubMoreData['csm_creditTransferSubId']=$IdCreditTransferSubjects;
                            $ctSubMoreData['csm_type']=$getCtInfo['InstitutionType'];
                            
                            if ($getCtInfo['InstitutionType']==783){
                                $ctSubMoreData['csm_subjectexternal']=$courseLoop;
                            }else{
                                $ctSubMoreData['csm_subjectinternal']=$courseLoop;
                            }
                            
                            $ctSubMoreData['csm_upduser']=$userId;
                            $ctSubMoreData['csm_upddate']=date('Y-m-d');
                            
                            $studentRegistrationDB->insertCtSubMore($ctSubMoreData);
                        }
                    }
                }
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => "Information Update"));
            $this->_redirect($this->baseUrl . '/course-registration/credit-transfer/editcredittransferapplication/IdCreditTransfer/'.$ctId);
        }
    }

    public function indexAction() {
        $this->view->title = $this->view->translate('Credit Transfer/Exemption Application');
        $form = new CourseRegistration_Form_AddCreditTransfer();
        $this->view->form = $form;
        
        $AppType = $this->_getParam('id', 0);
        $this->view->AppType = $AppType;
        
        $this->view->form->AppType->setvalue ($AppType);
        $this->view->form->AppType->setAttrib('disabled','true');
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        $student = $this->studentinfo;
        
        $this->view->landscape_id = $student["IdLandscape"];
        
        //get programme
        $programDb = new App_Model_General_DbTable_Program();
		$program = $programDb->fngetProgramData($student['IdProgram']);
		
        //semester
        $semesterDb = new App_Model_Registration_DbTable_Semester();
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme']));
		$this->view->semester = $semester;
		
        $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
//            echo "<pre>";
//            print_r($formData);
            
            $ctAppData = array(
                'IdStudentRegistration'=>$userId,
                'IdCTApplication'=>$this->generateApplicationID($formData['AppType']),
                'IdApplication'=>0,
                'IdProgram'=>$student['IdProgram'],
                'Status'=>null,
                'ApplicationStatus'=>784,
                'DateApplied'=>date('Y-m-d'),
                'AppliedBy'=>$userId,
                'DateApproved'=>null,
                'UpdDate'=>date('Y-m-d'),
                'UpdUser'=>$userId,
                'Comments'=>null,
                'InstitutionType'=>$formData['InstitionType'],
                'semesterId'=>$formData['semesterId'],
                'applicationType'=>$AppType,
                'source'=>'Student Portal'
            );
            $appid = $studentRegistrationDB->insertCtApp($ctAppData);
            
            //store history
            $statusHistoryData = array(
                'cth_ctid'=>$appid,
                'cth_oldstatus'=>0,
                'cth_newstatus'=>784,
                'cth_upddate'=>date('Y-m-d'),
                'cth_upduser'=>$userId
            );
            $studentRegistrationDB->storeHistory($statusHistoryData);
            
            //todo insert ct subject
            if (isset($formData['courseeq']) && count($formData) > 0){
                foreach ($formData['courseeq'] as $courseeq){
                    $ctSubData = array(
                        'IdCreditTransfer'=>$appid,
                        'IdSubject'=>null,
                    	'IdCourse'=>$courseeq,
                    	'CreditHours'=>$formData['credithours'][$courseeq],
                        'IdSemesterMain'=>$formData['semesterId'],
                        'creditStatus'=>null,
                        'ApplicationDate'=>date('Y-m-d'),
                        'AppliedBy'=>$userId,
                        'ApprovedDate'=>null,
                        'Approvedby'=>null,
                        'ApplicationStatus'=>784,
                        'IdInstitution'=>null,
                        'IdQualification'=>null,
                        'IdSpecialization'=>null,
                        'EquivalentCourse'=>$formData['course'][$courseeq],
                        'EquivalentGrade'=>$formData['gred'][$courseeq],
                        'EquivalentCreditHours'=>$formData['credithours'][$courseeq],
                        'Marks'=>null,
                        'Comments'=>$formData['remarks'][$courseeq],
                        'disapprovalComment'=>null,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'charge'=>$formData['charge'][$courseeq],
                        'institutionName'=>$formData['institutionName'][$courseeq]
                    );
                    $IdCreditTransferSubjects = $studentRegistrationDB->insertCtSub($ctSubData);
                    
                    //upload file
                    $folder = '/credittransfer/'.$IdCreditTransferSubjects;
                    $dir = DOCUMENT_PATH.$folder;
            
                    if ( !is_dir($dir) ){
                        if ( mkdir_p($dir) === false )
                        {
                            throw new Exception('Cannot create attachment folder ('.$dir.')');
                        }
                    }

                    //upload file proses
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
                    $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                    $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                    $adapter->addValidator('NotExists', false, $dir);
                    $adapter->setDestination($dir);

                    $files = $adapter->getFileInfo();
                    
                    if ($files){
                        $i = 0;
                        foreach ($files as $filesLoop){
                            if (isset($files['uploadFile'.$courseeq.'_'.$i.'_'])){
                                $fileOriName = $files['uploadFile'.$courseeq.'_'.$i.'_']['name'];
                                $fileRename = date('YmdHis').'_'.$fileOriName;
                                $filepath = $files['uploadFile'.$courseeq.'_'.$i.'_']['destination'].'/'.$fileRename;
                                
                                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                                $adapter->isUploaded('uploadFile'.$courseeq.'_'.$i.'_');
                                $adapter->receive('uploadFile'.$courseeq.'_'.$i.'_');
                                
                                if ($files['uploadFile'.$courseeq.'_'.$i.'_']['size']!=null){
                                    $uploadData = array(
                                        'IdCreditTransferSubjects'=>$IdCreditTransferSubjects, 
                                        'FileLocation'=>$folder.'/'.$fileRename, 
                                        'FileName'=>$fileOriName, 
                                        'UploadedFilename'=>$fileRename, 
                                        'FileSize'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['size'], 
                                        'MIMEType'=>$files['uploadFile'.$courseeq.'_'.$i.'_']['type'], 
                                        'UpdDate'=>date('Y-m-d'), 
                                        'UpdUser'=>$userId, 
                                        'Comments'=>''
                                    );
                                    $studentRegistrationDB->insertUploadFile($uploadData);
                                }
                            }
                            $i++;
                        }
                    }
                    
                    if (isset($formData['course'][$courseeq]) && count($formData['course'][$courseeq]) > 0){
                        foreach ($formData['course'][$courseeq] as $courseLoop){
                            $ctSubMoreData = array();
                            
                            $ctSubMoreData['csm_creditTransferId']=$appid;
                            $ctSubMoreData['csm_creditTransferSubId']=$IdCreditTransferSubjects;
                            $ctSubMoreData['csm_type']=$formData['InstitionType'];
                            
                            if ($formData['InstitionType']==783){
                                $ctSubMoreData['csm_subjectexternal']=$courseLoop;
                            }else{
                                $ctSubMoreData['csm_subjectinternal']=$courseLoop;
                            }
                            
                            $ctSubMoreData['csm_upduser']=$userId;
                            $ctSubMoreData['csm_upddate']=date('Y-m-d');
                            
                            $studentRegistrationDB->insertCtSubMore($ctSubMoreData);
                        }
                    }
                }
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => "Information Saved"));
            $this->_redirect($this->baseUrl . '/course-registration/credit-transfer/editcredittransferapplication/IdCreditTransfer/'.$appid);
        }
    }
    
    public function ctApproveAction(){
        $this->view->title = $this->view->translate('Credit Transfer/Exemption Approval');
        $ctId = $this->_getParam('IdCreditTransfer', 0);
        $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
        $this->view->ctInfo = $getCtInfo;
        $form = new Records_Form_ApproveCreditTransfer();
        $this->view->form = $form;
        
        //address
        $address = '';
        $address .= (($getCtInfo['appl_address1']!='' || $getCtInfo['appl_address1']!=null) ? $getCtInfo['appl_address1']:'');
        $address .= (($getCtInfo['appl_address2']!='' || $getCtInfo['appl_address2']!=null) ? ',<br/>'.$getCtInfo['appl_address2']:'');
        $address .= (($getCtInfo['appl_address3']!='' || $getCtInfo['appl_address3']!=null) ? ',<br/>'.$getCtInfo['appl_address3']:'');
        $address .= (($getCtInfo['appl_postcode']!='' || $getCtInfo['appl_postcode']!=null) ? ',<br/>'.$getCtInfo['appl_postcode']:'');
        $address .= (($getCtInfo['CityName']!='' || $getCtInfo['CityName']!=null) ? ',<br/>'.$getCtInfo['CityName']:'');
        $address .= (($getCtInfo['StateName']!='' || $getCtInfo['StateName']!=null) ? ',<br/>'.$getCtInfo['StateName']:'');
        $address .= (($getCtInfo['CountryName']!='' || $getCtInfo['CountryName']!=null) ? ',<br/>'.$getCtInfo['CountryName']:'');
        
        $this->view->address = $address;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);
        
        $this->view->userInfo = $userInfo;
        
        //ct subject
        $ctSubject = $this->lobjstudentregistrationModel->getCtSub($getCtInfo['IdCreditTransfer']);
        $this->view->ctSubject = $ctSubject;
        //var_dump(count($ctSubject)); exit;
        
        //ct history
        $historyList = $this->lobjstudentregistrationModel->getHistory($ctId);
        $this->view->historyList = $historyList;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $count = 0;
            foreach($formData['AppStatus'] as $key=>$val){
                $ctSubjectData = array(
                    'creditStatus'=>$val
                );
                $this->lobjstudentregistrationModel->updateCtSubject($ctSubjectData, $key);
                
                if ($val == 1){
                    $count++;
                }
            }
            
            if ($count != 0){
                
                //check subject register
                if ($ctSubject){
                    foreach ($ctSubject as $CourseLoop){
                        $checkSubReg = $this->lobjstudentregistrationModel->checkSubReg($getCtInfo['IdStudentRegistration'], $CourseLoop['EquivalentCourse'], $getCtInfo['semesterId']);
                        
                        if ($checkSubReg){
                            //redirect here
                            $this->_helper->flashMessenger->addMessage(array('error' => 'Cannot approve this application because subject: '.$CourseLoop['courseEqName'].' has been register'));
                            $this->_redirect($this->baseUrl . '/records/credittransferapp/ct-approve/IdCreditTransfer/'.$ctId);
                        }
                    }
                }
                        
                //update status
                $statusData = array(
                    'ApplicationStatus'=>786,
                    'Comments'=>$formData['Remarks'],
                    'DateApproved'=>date('Y-m-d'),
                    'UpdDate'=>date('Y-m-d'),
                    'ApprovedBy'=>$userId,
                    'UpdUser'=>$userId
                );
                $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
                
                $statusHistoryData = array(
                    'cth_ctid'=>$ctId,
                    'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                    'cth_newstatus'=>$statusData['ApplicationStatus'],
                    'cth_upddate'=>date('Y-m-d'),
                    'cth_upduser'=>$userId
                );
                $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
                
                //todo credit transfer
                $semStatus = $this->lobjstudentregistrationModel->checkSemStatus($getCtInfo['IdStudentRegistration'], $getCtInfo['semesterId']);
                
                if ($semStatus){
                    if ($semStatus['studentsemesterstatus']!=130){
                        $semStatusData = array(
                            'IdStudentRegistration'=>$semStatus['IdStudentRegistration'], 
                            'idSemester'=>$semStatus['idSemester'], 
                            'IdSemesterMain'=>$semStatus['IdSemesterMain'], 
                            'IdBlock'=>$semStatus['IdBlock'], 
                            'studentsemesterstatus'=>130, 
                            'Level'=>$semStatus['Level'], 
                            'Reason'=>$semStatus['Reason'], 
                            'UpdDate'=>date('Y-m-d'), 
                            'UpdUser'=>$userId
                        );
                        $this->lobjstudentregistrationModel->insertSemStatus($semStatusData);
                        
                        $semStatusHisData = array(
                            'IdStudentRegistration'=>$semStatus['IdStudentRegistration'], 
                            'idSemester'=>$semStatus['idSemester'], 
                            'IdSemesterMain'=>$semStatus['IdSemesterMain'], 
                            'IdBlock'=>$semStatus['IdBlock'], 
                            'studentsemesterstatus'=>$semStatus['studentsemesterstatus'], 
                            'Level'=>$semStatus['Level'], 
                            'Reason'=>$semStatus['Reason'], 
                            'UpdDate'=>date('Y-m-d'), 
                            'UpdUser'=>$userId
                        );
                        $this->lobjstudentregistrationModel->insertSemStatusHistory($semStatusHisData);
                        
                        $this->lobjstudentregistrationModel->deleteSemStatus($semStatus['idstudentsemsterstatus']);
                    }
                }else{
                    $semStatusData = array(
                        'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'], 
                        'idSemester'=>$getCtInfo['semesterId'], 
                        'IdSemesterMain'=>$getCtInfo['semesterId'], 
                        'IdBlock'=>NULL, 
                        'studentsemesterstatus'=>130, 
                        'Level'=>count($this->lobjstudentregistrationModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1, 
                        'Reason'=>'Audit Paper', 
                        'UpdDate'=>date('Y-m-d'), 
                        'UpdUser'=>$userId
                    );
                    $this->lobjstudentregistrationModel->insertSemStatus($semStatusData);
                }
                
                if ($ctSubject){
                    $subregId = array();
                    foreach ($ctSubject as $getCourseLoop){
                        $auditSub = array(
                            'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'], 
                            'IdSubject'=>$getCourseLoop['EquivalentCourse'], 
                            'IdSemesterMain'=>$getCtInfo['semesterId'], 
                            'SemesterLevel'=>count($this->lobjstudentregistrationModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1, 
                            'grade_name'=>$getCourseLoop['EquivalentGrade'],
                            'UpdUser'=>$userId, 
                            'UpdDate'=>date('Y-m-d'), 
                            'Active'=>1
                        );
                        $studRegSubId = $this->lobjstudentregistrationModel->registerAuditPaper($auditSub);
                        
                        $auditSubHis = array(
                            'IdStudentRegSubjects'=>$studRegSubId,
                            'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'], 
                            'IdSubject'=>$getCourseLoop['EquivalentCourse'], 
                            'IdSemesterMain'=>$getCtInfo['semesterId'], 
                            'SemesterLevel'=>count($this->lobjstudentregistrationModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1, 
                            'grade_name'=>$getCourseLoop['EquivalentGrade'],
                            'UpdUser'=>$userId, 
                            'UpdDate'=>date('Y-m-d'), 
                            'Active'=>1
                        );
                        $this->lobjstudentregistrationModel->historyRegisterAuditPaper($auditSubHis);
                        
                        array_push($subregId, $studRegSubId);
                    }
                    
                    $implodeSubRegId = implode('|',$subregId);
                    
                    $imlplodeData = array(
                        'subjectreg_id'=>$implodeSubRegId
                    );
                    $this->lobjstudentregistrationModel->updateCtAppStatus($imlplodeData, $ctId);
                }
                
                $message = "Application has been approved";
            }else{
                //update status
                $statusData = array(
                    'ApplicationStatus'=>787,
                    'Comments'=>$formData['Remarks'],
                    'DateApproved'=>date('Y-m-d'),
                    'UpdDate'=>date('Y-m-d'),
                    'ApprovedBy'=>$userId,
                    'UpdUser'=>$userId
                );
                $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
                
                $statusHistoryData = array(
                    'cth_ctid'=>$ctId,
                    'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                    'cth_newstatus'=>$statusData['ApplicationStatus'],
                    'cth_upddate'=>date('Y-m-d'),
                    'cth_upduser'=>$userId
                );
                $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
                
                $message = "Application has been rejected";
            }
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => $message));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/ct-approve/IdCreditTransfer/'.$ctId);
        }
    }
    
    public function cancelCtAppAction(){
        $ctId = $this->_getParam('IdCreditTransfer', 0);
        
        if ($ctId != 0){
            //user info
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->registration_id;
            
            //get ct info
            $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
            
            //cancel proses
            $statusData = array(
                'ApplicationStatus'=>785,
                'Comments'=>'Application cancel',
                'DateApproved'=>date('Y-m-d'),
                'UpdDate'=>date('Y-m-d'),
                'ApprovedBy'=>$userId,
                'UpdUser'=>$userId
            );
            $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
            
            $statusHistoryData = array(
                'cth_ctid'=>$ctId,
                'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                'cth_newstatus'=>$statusData['ApplicationStatus'],
                'cth_upddate'=>date('Y-m-d'),
                'cth_upduser'=>$userId
            );
            $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Application has been cancel <br/><br/>Info :<br/>Name : '.$getCtInfo['appl_fname'].' '.$getCtInfo['appl_lname'].'<br/>Student ID : '.$getCtInfo['registrationId']));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/credittransferapplication'); 
        }else{
            
        }
    }
    
    public function findStudentAction(){
        $searchElement = $this->_getParam('term', '');
        $searchType = $this->_getParam('searchType', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $studentList = $this->lobjstudentregistrationModel->findStudent($searchElement, $searchType);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);
        
        if ($studentList){
            foreach($studentList as $student) {
                
                $address = (($student['appl_address1']!='' || $student['appl_address1']!=null) ? $student['appl_address1']:'');
                $address .= (($student['appl_address2']!='' || $student['appl_address2']!=null) ? ', '.$student['appl_address2']:'');
                $address .= (($student['appl_address3']!='' || $student['appl_address3']!=null) ? ', '.$student['appl_address3']:'');
                $address .= (($student['appl_postcode']!='' || $student['appl_postcode']!=null) ? ', '.$student['appl_postcode']:'');
                $address .= (($student['CityName']!='' || $student['CityName']!=null) ? ', '.$student['CityName']:'');
                $address .= (($student['StateName']!='' || $student['StateName']!=null) ? ', '.$student['StateName']:'');
                $address .= (($student['CountryName']!='' || $student['CountryName']!=null) ? ', '.$student['CountryName']:'');

                $status = $this->lobjstudentregistrationModel->getDefination2(155, 'Applied');
                
                //current sem
                $curSem = $this->lobjstudentregistrationModel->getCurSem($student['schemeid']);
                //var_dump($curSem);
                $student_array[] = array(
                    'id' => $student['IdStudentRegistration'],
                    'ApplicationID'=>'Will generate after save',
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeId'=>$student['IdProgramScheme'],
                    'LandscapeId'=>$student['IdLandscape'],
                    'StatusName'=>$student['StatusName'],
                    'IntakeName'=>$student['IntakeName'],
                    'BranchName'=>$student['BranchName'],
                    'ProgramName'=>$student['ProgramName'],
                    'ProgramId'=>$student['IdProgram'],
                    'ProgramSchemeName'=>$student['mop'].' '.$student['mos'].' '.$student['pt'],
                    'fundingMethod'=>$student['afMethodName'],
                    'sponsorshipName'=>$student['af_sponsor_name'],
                    'scholarshipTypeName'=>$student['typeScholarshipName'],
                    'scholarshipTypeId'=>$student['af_type_scholarship'],
                    'applyScholarship'=>$student['applScholarshipName'],
                    'securedScholarship'=>$student['af_scholarship_secured'],
                    'Address'=>$address,
                    'PhoneHome'=>$student['appl_phone_home'],
                    'PhoneMobile'=>$student['appl_phone_mobile'],
                    'PhoneOffice'=>$student['appl_phone_office'],
                    'Email'=>$student['appl_email'],
                    'IdType'=>$student['IdTypeName'],
                    'IdNo'=>$student['appl_idnumber'],
                    'AppliedDate'=>date('d-m-Y'),
                    'AppliedBy'=>$userInfo['loginName'],
                    'AppliedStatus'=>$status['DefinitionDesc'],
                    'CurrentSem'=>$curSem['IdSemesterMaster'],
                    'label' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname'],
                    'value' => $student['registrationId'] . ' - ' . $student['appl_fname'] . ' ' . $student['appl_mname'] . ' ' . $student['appl_lname']
                );
            }
        }else{
            $student_array[] = array();
        }
        
        $json = Zend_Json::encode($student_array);
		
	echo $json;
	exit();
    }
    
    public function getCourseAction(){
        $landscapeId = $this->_getParam('landscapeId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
		$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
            
        $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
        
        //get landscape
        $landscapeInfo = $studentRegistrationDB->getLandscape($landscapeId);
        
        if ($landscapeInfo['LandscapeType']==44){
            $courseList = $studentRegistrationDB->getBlockSubject($landscapeId);
        }else{
            $courseList = $studentRegistrationDB->getSubject($landscapeId);
        }
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
    
    public function getCourseInfoAction(){
        $courseId = $this->_getParam('courseId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
		$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
         $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
            
        $courseInfo = $studentRegistrationDB->getCourseInfo($courseId);
        
        $json = Zend_Json::encode($courseInfo);
		
	echo $json;
	exit();
    }
    
    public function getCourseMoreAction(){
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseList = $this->lobjstudentregistrationModel->getEquivalentSub();
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
    
    public function getGredAction(){
        $appTypeId = $this->_getParam('appTypeId', 0);
        $landscapeId = $this->_getParam('landscapeId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
		$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $gredList = array();
        
        $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
        
        if ($appTypeId == 780){
            $landscapeInfo = $studentRegistrationDB->getLandscape($landscapeId);
            
            if ($landscapeInfo['AssessmentMethod'] == 'point'){
                $gredList[0]['name']='A';
                $gredList[1]['name']='B';
                $gredList[2]['name']='C';
                $gredList[3]['name']='D';
                $gredList[4]['name']='E';
                $gredList[5]['name']='F';
            }else{
                $gredList[0]['name']='CT';
            }
        }else{
            $gredList[0]['name']='EX';
        }
        
        $json = Zend_Json::encode($gredList);
		
	echo $json;
	exit();
    }
    
    public function getSemesterAction(){
        $programId = $this->_getParam('programId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $programInfo = $this->lobjstudentregistrationModel->getProgramForSem($programId);
        
        $semList = $this->lobjstudentregistrationModel->getSemBasedOnList($programInfo['IdScheme']);
        
        $json = Zend_Json::encode($semList);
		
	echo $json;
	exit();
    }
    
    public function getFileUploadAction(){
        $ctSubId = $this->_getParam('ctSubId', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $uploadList = $this->lobjstudentregistrationModel->getUploadFile($ctSubId);
        
        $json = Zend_Json::encode($uploadList);
		
	echo $json;
	exit();
    }
    
    public function deleteUploadFileAction(){
        $uploadId = $this->_getParam('id', 0);
        $ctId = $this->_getParam('ctId', 0);
        if ($uploadId != 0){
            $info = $this->lobjstudentregistrationModel->deleteUploadFileInfo($uploadId);
            $fileLocation = DOCUMENT_PATH.$info['FileLocation'];
            
            //delete fizikal file
            unlink($fileLocation);
            
            //delete file data
            $this->lobjstudentregistrationModel->deleteUploadFile($uploadId);
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'File '.$info['FileName'].' has been deleted'));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$ctId);
        }else{
            $this->_helper->flashMessenger->addMessage(array('error' => 'Delete file unsuccessful'));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$ctId);
        }
    }
    
    public function uploadFileAction(){
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); 
            //var_dump($_FILES);
            //exit;
            
            $folder = '/credittransfer/'.$formData['ctSubId'];
            $dir = DOCUMENT_PATH.$folder;

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false )
                {
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 5)); //maybe soon we will allow more?
            $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();
            //var_dump($files); exit;
            if ($files){

                $fileOriName = $files['file']['name'];
                $fileRename = date('YmdHis').'_'.$fileOriName;
                $filepath = $files['file']['destination'].'/'.$fileRename;

                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $fileOriName);

                $adapter->isUploaded();
                $adapter->receive();

                $uploadData = array(
                    'IdCreditTransferSubjects'=>$formData['ctSubId'], 
                    'FileLocation'=>$folder.'/'.$fileRename, 
                    'FileName'=>$fileOriName, 
                    'UploadedFilename'=>$fileRename, 
                    'FileSize'=>$files['file']['size'], 
                    'MIMEType'=>$files['file']['type'], 
                    'UpdDate'=>date('Y-m-d'), 
                    'UpdUser'=>$userId, 
                    'Comments'=>''
                );
                $this->lobjstudentregistrationModel->insertUploadFile($uploadData);
                
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'File '.$fileOriName.' has been upload'));
                $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$formData['ctId']);
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('error' => 'File upload error'));
            $this->_redirect($this->baseUrl . '/records/credittransferapp/editcredittransferapplication/IdCreditTransfer/'.$formData['ctId']);
        }
    }

    public function fngetstudentdetAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $Name = $this->_getParam('Name');
        $StudentId = $this->_getParam('StudentId');
        $IdProfileStatus = $this->_getParam('IdProfileStatus');
        $IdSemester = $this->_getParam('IdSemester');
        $pieces = explode('_', $IdSemester);
        $semId = $pieces[0];
        $IdNric = $this->_getParam('IdNric');
        $searchArray = array('Name' => $Name, 'StudentId' => $StudentId, 'IdProfileStatus' => $IdProfileStatus,
            'IdSemester' => $semId, 'IdNric' => $IdNric
        );
        $studentList = $this->lobjstudentregistrationModel->getStudentforcredtTransfer($searchArray);
        echo Zend_Json_Encoder::encode($studentList);
    }


    public function fngetstudentcoursesAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $idscheme = $this->_getParam('idscheme');
        $studentId = $this->_getParam('studentId');
        
        $IdCreditTransfer = $this->_getParam('IdCreditTransfer');
        $courseList = $this->lobjlandscapeModel->fnGetcoursesByScheme($studentId);
        //print_r($courseList);
        $temp = array();
        //$i = 0;
        foreach($courseList as $course){
            if($IdCreditTransfer == ''){
                $ret = $this->lobjcreditTransferSubjectModel->checkvalidcourse($studentId,$course['key']);
                if(empty($ret)){
                    $temp[] = $course;
                }
            }else{
               $ret = $this->lobjcreditTransferSubjectModel->checkvalidcourseinedit($studentId,$course['key'],$IdCreditTransfer);
                if(empty($ret)){
                    $temp[] = $course;
                }
            }
        }
        //print_r($temp);
        echo Zend_Json_Encoder::encode($temp);
    }

    public function fngetstudentsemesterAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $idscheme = $this->_getParam('idscheme');
        $studentId = $this->_getParam('studentId');
        $idprogram = $this->_getParam('idprogram');
        $semesteList = $this->lobjSemesterModel->getStudentSemester($idscheme,$idprogram,$studentId);
        echo Zend_Json_Encoder::encode($semesteList);
    }

    public function detailstudentAction() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
        $studentId = $this->_getParam('IdStudent');
        $studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($studentId);
        $studentsemesterList = $this->lobjstudentregistrationModel->getStudentSemesters($studentId);
        $len = '';
        $len = count($studentsemesterList);
        $this->view->currsem = '';
        $this->view->studsemstatus = '';
        $this->view->studDet = '';
        if($len != ''){
            if ($studentsemesterList[$len - 1]['SemesterMainCode'] != '') {
                $this->view->currsem = $studentsemesterList[$len - 1]['SemesterMainCode'];
            }
            if ($studentsemesterList[$len - 1]['SemesterCode'] != '') {
                $this->view->currsem = $studentsemesterList[$len - 1]['SemesterCode'];
            }
            $this->view->studsemstatus = $studentsemesterList[$len - 1]['DefinitionDesc'];
        }
        $this->view->studDet = $studentDett[0];
    }

    public function getcredithourAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $id = $this->_getParam('IdCourse');
        $courseList = $this->lobjsubjectmasterModel->fnviewSubject($id);
        echo $courseList['CreditHours'];
    }

    public function getqualificationAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $IdInstitute = $this->_getParam('IdInstitute');
        $ret = $this->lobjautocreditTransferModel->getQualification($IdInstitute);
        echo Zend_Json_Encoder::encode($ret);
    }

    public function getspecializationAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $IdInstitute = $this->_getParam('IdInstitute');
        $IdQualification = $this->_getParam('IdQualification');
        $ret = $this->lobjautocreditTransferModel->getSpecialization($IdInstitute, $IdQualification);
        echo Zend_Json_Encoder::encode($ret);
    }

    public function getequivalentcourseAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $IdInstitute = $this->_getParam('IdInstitute');
        $IdQualification = $this->_getParam('IdQualification');
        $IdSpecialization = $this->_getParam('IdSpecialization');
        $ret = $this->lobjautocreditTransferModel->getequivalentCourse($IdInstitute, $IdQualification, $IdSpecialization);
        echo Zend_Json_Encoder::encode($ret);
    }

    
    
    
    /**
     * Function to show the credit transfer list based on studentID
     * @author Vipul
     * 
     */
    public function credittransferapplicationapprovalAction() {       
        $this->view->title = $this->view->translate("Credit Transfer/Exemption Approval");
        $form = new CourseRegistration_Form_SearchCreditTransfer();
        //$this->view->ctAppList = $ctAppList;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['search'])){
                $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp($formData);
                $this->gobjsessionsis->credittransferappcontroller = $ctAppList;
            }else{
                $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp();
            }
        }else{
            $ctAppList = $this->lobjstudentregistrationModel->getAllCtApp();
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->credittransferappcontroller);
        }
        
        $lintpagecount = 1000;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->credittransferappcontroller)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->credittransferappcontroller,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($ctAppList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->form = $form;
    }


    public function credittransferapproveAction(){
        $this->view->lobjcredittransferForm = $this->lobjcredittransferForm;
        $IdcreditTransferApp = $this->_getParam('IdCreditTransfer');
        $appDetail = $this->lobjcreditTransferModel->getDetailApplication($IdcreditTransferApp);       

        $this->view->dcreditdetail = $appDetail;

        $studentId = $appDetail[0]['IdStudentRegistration'];
        $studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($studentId);
        $studentsemesterList = $this->lobjstudentregistrationModel->getStudentSemesters($studentId);
        $len = count($studentsemesterList);
        $this->view->currsem = '';
        if ($studentsemesterList[$len - 1]['SemesterMainCode'] != '') {
            $this->view->currsem = $studentsemesterList[$len - 1]['SemesterMainCode'];
        }
        if ($studentsemesterList[$len - 1]['SemesterCode'] != '') {
            $this->view->currsem = $studentsemesterList[$len - 1]['SemesterCode'];
        }
        $this->view->studsemstatus = $studentsemesterList[$len - 1]['DefinitionDesc'];
    }

    /**
     * Function to show the credit transfer list based on studentID for approval
     * @author Vipul
     * 
     */
     public function detailappsAction(){
        $creditTransferModels = new Records_Model_DbTable_Credittransfer();
        $creditObj = new Records_Model_DbTable_Credittransfersubject();
        // Get Inversity Id
        $IdUniversity = $this->gobjsessionsis->idUniversity;
        $univconfigList = $this->lobjrecordconfigModel->fnfetchConfiguration($IdUniversity);
        $this->view->configDetail = $univconfigList;
        $IdcreditTransferApp = $this->_getParam('IdCreditTransfer');
        $appDetail = $this->lobjcreditTransferModel->getDetailApplication($IdcreditTransferApp);

        $this->view->dcreditdetail = $appDetail;

        $studentId = $appDetail[0]['IdStudentRegistration'];
        $studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($studentId);
        $studentsemesterList = $this->lobjstudentregistrationModel->getStudentSemesters($studentId);
        //asd($studentsemesterList);
        $len = count($studentsemesterList);
        $this->view->currsem = '';
        if($len!='0') { 
        if ($studentsemesterList[$len - 1]['SemesterMainCode'] != '') {
            $this->view->currsem = $studentsemesterList[$len - 1]['SemesterMainCode'];
        }
        if ($studentsemesterList[$len - 1]['SemesterCode'] != '') {
            $this->view->currsem = $studentsemesterList[$len - 1]['SemesterCode'];
        }
        $this->view->studsemstatus = $studentsemesterList[$len - 1]['DefinitionDesc'];
        }

         $autoCt = '0';      
         //$this->_helper->layout->disableLayout();
         $studentId = $this->_getParam('IdStudent');
         $creditDetails = $creditTransferModels->getCreditTransferDetails($studentId,$IdcreditTransferApp);         
         //asd($creditDetails);
         //$this->view->AppStatus = $creditDetails['0']['DefinitionDesc'];
         
         
         $lobjform = new App_Form_Search (); //intialize search lobjuserForm
   	 	$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
         //$arrList = array('1'=>'Approve','0'=>'DisApprove');                
	 //$this->view->lobjform->field5->addMultiOptions($arrList);
         $this->view->lobjform->Save->setAttrib('OnClick', "fnSaveCredits(this.value,'".$studentId."','".$creditDetails[0]['IdCreditTransfer']."')")
                                    ->setAttrib('class', 'dijitButton');
         $this->view->lobjform->Revert->setAttrib('OnClick', "fnRevertCredits(this.value,'".$studentId."','".$creditDetails[0]['IdCreditTransfer']."')");
         
         
         // if even one course is diapproved, show the save button. if all are approved,then display revert button and hide save button and make selectbox disable.
         $condition_cs = " a.IdCreditTransfer = '".$IdcreditTransferApp."' AND ( creditStatus = '0' ) ";  
         $getSemCourse  =  $creditObj->getCreditSubjectDetalByID($condition_cs);
         //echo 'dis'.count($getSemCourse); echo '</br>';
         $condition_cs1 = " a.IdCreditTransfer = '".$IdcreditTransferApp."' AND ( creditStatus IS NULL ) ";  
         $getSemCourse1  =  $creditObj->getCreditSubjectDetalByID($condition_cs1);         
         //echo 'entry'.count($getSemCourse1);echo '</br>';
         $condition_cs2 = " a.IdCreditTransfer = '".$IdcreditTransferApp."' AND ( creditStatus = '1' ) ";  
         $getSemCourse2  =  $creditObj->getCreditSubjectDetalByID($condition_cs2);
         //echo 'app'.count($getSemCourse2);echo '</br>';
         
         if(count($getSemCourse)>0 && count($getSemCourse1)==0 && count($getSemCourse2)==0) {
         // all course status are disapproved.    
         $this->view->displayRevert = '1';    
         $this->view->disableSelect = '0';         
         // UPDATE APPLICATION STATUS TO DISAPPROVED
         $creditTransferModels->updateCreditAppStatus($creditDetails[0]['IdCreditTransfer'],'disapprove');        
         }
         
         
        
         if( count($getSemCourse)==0 && count($getSemCourse2)==0 && count($getSemCourse1)>0 ) {             
             // all course status are NULL.  
             $this->view->displayRevert = '0';    
             $this->view->disableSelect = '0';             
             // UPDATE APPLICATION STATUS TO ENTRY
             $creditTransferModels->updateCreditAppStatus($creditDetails[0]['IdCreditTransfer'],'entry');            
         }
         
         
        
         if( count($getSemCourse2)>0 && count($getSemCourse)==0 && count($getSemCourse1)==0 ) {         
                // All courses are approved.    
                $this->view->disableSelect = '1'; 
                $this->view->displayRevert = '1';
                // UPDATE APPLICATION STATUS TO APPROVED
                $creditTransferModels->updateCreditAppStatus($creditDetails[0]['IdCreditTransfer'],'approve');     
                
         }
         
         
         if( count($getSemCourse2)>0 && count($getSemCourse)>0 && count($getSemCourse1)==0 ) {         
                // All courses are approved.    
                $this->view->disableSelect = '1'; 
                $this->view->displayRevert = '1';
                // UPDATE APPLICATION STATUS TO APPROVED
                $creditTransferModels->updateCreditAppStatus($creditDetails[0]['IdCreditTransfer'],'approve');
         }
         
         
         
         $creditDetails2 = $creditTransferModels->getCreditTransferDetails($studentId,$IdcreditTransferApp); 
         $this->view->creditDetails = $creditDetails2;
         $this->view->AppStatus = $creditDetails2['0']['DefinitionDesc'];
         if($autoCt=='0') {     // check from the credit transfer approval setup
              echo $this->render('appsunticked');
         } 
         else { 
              echo $this->render('appsticked');
         }
        // echo $this->render('appsunticked');
         //die;
     }   
    
     
      public function appsuntickedAction(){        
        $this->_helper->layout->disableLayout();
	//$this->_helper->viewRenderer->setNoRender();
      } 
      
      public function appstickedAction(){        
         $this->_helper->layout->disableLayout();
      } 
     
      public function  savecreditstransferapprovalAction() {
           $creditTransferModels = new Records_Model_DbTable_Credittransfer();
           $this->_helper->layout->disableLayout();
           $selData = $this->_getParam('selData');
           $commentData = $this->_getParam('commentData');
           $type = $this->_getParam('type');
           $creditID = $this->_getParam('creditID'); 
           $studentId = $this->_getParam('studentId');
           $creditTransferModels->saveCreditTransfer($commentData, $selData,$type,$creditID,$studentId); 
           die;          
      }
      
      
      public function  revertcreditstransferAction() {
           $creditTransferModels = new Records_Model_DbTable_Credittransfer();
           $this->_helper->layout->disableLayout();           
           $type = $this->_getParam('type');
           $creditID = $this->_getParam('creditID'); 
           $studentId = $this->_getParam('studentId');
           $creditTransferModels->revertCreditTransfer($type,$creditID,$studentId);      
           die;          
      }
    
    public function printapplicationAction(){
        $this->_helper->layout()->setLayout('/printstudentsummary');
        $ctId = $this->_getParam('id', 0);
        $this->view->title = $this->view->translate('Credit Transfer Application');
        $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
        //var_dump($getCtInfo); exit;
        $this->view->ctInfo = $getCtInfo;
        $form = new Records_Form_EditCreditTransfer(array('landscapeId'=>$getCtInfo['IdLandscape'], 'apptypeId'=>$getCtInfo['applicationType']));
        $this->view->form = $form;

        //address
        $address = '';
        $address .= (($getCtInfo['appl_address1']!='' || $getCtInfo['appl_address1']!=null) ? $getCtInfo['appl_address1']:'');
        $address .= (($getCtInfo['appl_address2']!='' || $getCtInfo['appl_address2']!=null) ? ',<br/>'.$getCtInfo['appl_address2']:'');
        $address .= (($getCtInfo['appl_address3']!='' || $getCtInfo['appl_address3']!=null) ? ',<br/>'.$getCtInfo['appl_address3']:'');
        $address .= (($getCtInfo['appl_postcode']!='' || $getCtInfo['appl_postcode']!=null) ? ',<br/>'.$getCtInfo['appl_postcode']:'');
        $address .= (($getCtInfo['CityName']!='' || $getCtInfo['CityName']!=null) ? ',<br/>'.$getCtInfo['CityName']:'');
        $address .= (($getCtInfo['StateName']!='' || $getCtInfo['StateName']!=null) ? ',<br/>'.$getCtInfo['StateName']:'');
        $address .= (($getCtInfo['CountryName']!='' || $getCtInfo['CountryName']!=null) ? ',<br/>'.$getCtInfo['CountryName']:'');

        $this->view->address = $address;

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;

        $userInfo = $this->lobjstudentregistrationModel->getUser($userId);

        $this->view->userInfo = $userInfo;

        //ct subject
        $ctSubject = $this->lobjstudentregistrationModel->getCtSub($getCtInfo['IdCreditTransfer']);
        $this->view->ctSubject = $ctSubject;
    }
    
    function generateApplicationID($type){
        
        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        //bermulanya sebuah penciptaan id
        
        $studentRegistrationDB =  new CourseRegistration_Model_DbTable_Studentregistration();
        
        //check sequence
        if ($type == 780){
            $checkSequence = $studentRegistrationDB->getSequence(1, date('Y'));

            if (!$checkSequence){
                $dataSeq = array(
                    'tsi_type'=>1, 
                    'tsi_seq_no'=>1, 
                    'tsi_seq_year'=>date('Y'), 
                    'tsi_upd_date'=>date('Y-m-d'), 
                    'tsi_upd_user'=>$userId
                );
                $studentRegistrationDB->insertSequence($dataSeq);
            }

            //generate applicant ID
            $applicationid_format = array();

            //get sequence
            $sequence = $studentRegistrationDB->getSequence(1, date('Y'));

            $config = $studentRegistrationDB->getConfig(1);

            //format
            $format_config = explode('|',$config['creditTransferIdFormat']);
            //var_dump($format_config);
            for($i=0; $i<count($format_config); $i++){
                $format = $format_config[$i];
                if($format=='CT'){ //prefix
                    $result = $config['creditTransferPrefix'];
                }elseif($format=='YYYY'){
                    $result = date('Y');
                }elseif($format=='YY'){
                    $result = date('y');
                }elseif($format=='seqno'){				
                    $result = sprintf("%05d", $sequence['tsi_seq_no']);
                }
                array_push($applicationid_format,$result);
            }

            $applicationID = implode("",$applicationid_format);

            //update sequence		
            $studentRegistrationDB->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        
        }else{
            $checkSequence = $studentRegistrationDB->getSequence(2, date('Y'));

            if (!$checkSequence){
                $dataSeq = array(
                    'tsi_type'=>2, 
                    'tsi_seq_no'=>1, 
                    'tsi_seq_year'=>date('Y'), 
                    'tsi_upd_date'=>date('Y-m-d'), 
                    'tsi_upd_user'=>$userId
                );
                $studentRegistrationDB->insertSequence($dataSeq);
            }

            //generate applicant ID
            $applicationid_format = array();

            //get sequence
            $sequence = $studentRegistrationDB->getSequence(2, date('Y'));

            $config = $studentRegistrationDB->getConfig(1);

            //format
            $format_config = explode('|',$config['exemptionIdFormat']);
            //var_dump($format_config);
            for($i=0; $i<count($format_config); $i++){
                $format = $format_config[$i];
                if($format=='EX'){ //prefix
                    $result = $config['exemptionPrefix'];
                }elseif($format=='YYYY'){
                    $result = date('Y');
                }elseif($format=='YY'){
                    $result = date('y');
                }elseif($format=='seqno'){				
                    $result = sprintf("%05d", $sequence['tsi_seq_no']);
                }
                array_push($applicationid_format,$result);
            }

            $applicationID = implode("",$applicationid_format);

            //update sequence		
            $studentRegistrationDB->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        }

        return $applicationID;
    }
    
    public function revertApplicationAction(){
        $ctId = $this->_getParam('ctId', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->registration_id;
        
        if ($ctId != 0){
            $getCtInfo = $this->lobjstudentregistrationModel->getCtInfo($ctId);
            
            $statusData = array(
                'ApplicationStatus'=>784,
                'Comments'=>'',
                'UpdDate'=>date('Y-m-d'),
                'UpdUser'=>$userId
            );
            $this->lobjstudentregistrationModel->updateCtAppStatus($statusData, $ctId);
            
            $statusHistoryData = array(
                'cth_ctid'=>$ctId,
                'cth_oldstatus'=>$getCtInfo['ApplicationStatus'],
                'cth_newstatus'=>$statusData['ApplicationStatus'],
                'cth_upddate'=>date('Y-m-d'),
                'cth_upduser'=>$userId
            );
            $this->lobjstudentregistrationModel->storeHistory($statusHistoryData);
            
            $idRegSub = explode('|', $getCtInfo['subjectreg_id']);
            
            if (count($idRegSub) > 0){
                foreach ($idRegSub as $idRegSubLoop){
                    $this->lobjstudentregistrationModel->deleteRegAuditPaper($idRegSubLoop);
                    $this->lobjstudentregistrationModel->deleteRegAuditPaperHistory($idRegSubLoop);
                }
            }
        }
        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Revert success'));
        $this->_redirect($this->baseUrl . '/records/credittransferapp/ct-approve/IdCreditTransfer/'.$ctId);
    }
}

?>
