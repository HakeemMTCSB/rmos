<?php
class CourseRegistration_Model_DbTable_CourseRegistration extends Zend_Db_Table_Abstract 
{
	protected $_name = 'tbl_studentregsubjects';
	protected $_detl = 'tbl_studentregsubjects_detail';
	protected $_section = 'tbl_course_group_student_mapping';

	private $db;

	public function init() {
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getDetails($student_id, $semester_id,$course_id=0)
	{
		$select = $this->db->select()
					 ->from(array('a'=>$this->_detl))
					 ->joinLeft(array('i'=>'tbl_definationms'), 'a.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))	
					 ->where('student_id = ?', $student_id)
					 ->where('semester_id = ?',  $semester_id);

					 if($course_id){
					 	$select->where('subject_id = ?', $course_id);
					 }
				
		$results = $this->db->fetchAll($select);

		return $results;
	}

	public function subjectRegistered($student_id, $semester_id = null)
	{
		$db = $this->db;

		$select = $db->select()
					 ->from(array('a'=>$this->_name))
					 ->join(array('s'=>'tbl_subjectmaster'), 's.IdSubject=a.IdSubject', array('SubjectName', 'SubCode', 'CreditHours'))
					 
					 ->join(array("ls"=>"tbl_landscapesubject"),'ls.IdSubject=a.IdSubject',array(''))
					 ->join(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc'))
					 ->where('IdStudentRegistration = ?', $student_id );

		if ( $semester_id != '' )
		{
			 $select->where('IdSemesterMain = ?', $semester_id);
		}
			$select->group('a.IdSubject');		 
		$results = $db->fetchAll($select);

		return $results;
	}

	public function getSectionsTagged($student_id)
	{
		$db = $this->db;

		$select = $db->select()
					 ->from(array('a'=>$this->_section))
					 ->where('IdStudent = ?', $student_id );

					 
		$results = $db->fetchAll($select);

		return $results;
	}

	public function getEC($student_id, $subject_id, $semester_id)
	{
		$db = $this->db;

		$select = $db->select()
					 ->from(array('a'=>'exam_registration'))
					 ->joinLeft(array('c'=>'tbl_countries'),'a.er_idCountry=c.idCountry',array('CountryName'))
	                 ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=a.er_idCity',array('CityName'))
					 ->where('a.er_idSemester = ?', $semester_id )
					 ->where('a.er_idStudentRegistration = ?', $student_id)
					 ->where('a.er_idSubject = ?', $subject_id);

					 
		$results = $db->fetchRow($select);

		return $results;
	}


	public function getECById($id)
	{
		$db = $this->db;

		$select = $db->select()
					 ->from(array('a'=>'exam_registration'))
					 ->joinLeft(array('c'=>'tbl_countries'),'a.er_idCountry=c.idCountry',array('CountryName'))
	                 ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=a.er_idCity',array('CityName'))
					 ->where('a.er_id = ?', $id );

		$results = $db->fetchRow($select);

		return $results;
	}

	public function getHistory($student_id)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>'tbl_studentregsubjects_session'))
					->where('a.student_id = ?', $student_id)
					->order('a.id DESC');
		
		return $select;
	}

	public function getHistoryData($id)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>'tbl_studentregsubjects_session'))
					->where('a.id = ?', $id);
		
		return $db->fetchRow($select);
	}

	public function getSubjects($ids)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>'tbl_subjectmaster'))
					->where('a.IdSubject IN (?)', $ids);

		return $db->fetchAll($select);

	}

	public function checkBarring($studId,$semId){
		$db = $this->db;

		$select = $db->select()
					 ->from(array('a'=>'tbl_barringrelease'))	
					 ->where ("tbr_appstud_id = ?",$studId)	
					 ->where ("tbr_type = 866")
					 ->where ("tbr_status = 0");
		$results = $db->fetchRow($select);

		return $results;		
	}
	public function getCurrentRegistered($idstudreg){
		$db = $this->db;
		//0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer 6: CT 9 : Pre-Repeat
		$sql = $db->select()
		     	->from(array("a"=>$this->_name))
		     	->join(array("b"=>"tbl_semestermaster"),"a.IdSemesterMain=b.IdSemesterMaster",array())
		     	->join(array('sr'=>'tbl_studentregistration'),"sr.IdStudentRegistration=a.IdStudentRegistration",array("registrationId"))
		     	->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array('appl_username'))
		     	->where("SemesterMainEndDate >= CURDATE() AND SemesterMainStartDate <= CURDATE()")
		     	->where("a.IdStudentRegistration = ?",$idstudreg)
		     	->where("a.active = 0 or a.active = 1 or a.active = 4 or a.active = 5 or a.active = 9");
		 //echo $sql;
		$results = $db->fetchAll($sql);

		return $results;		     			
		
	}
	
	public function getCurrentRegisteredSem($idstudreg){
		$db = $this->db;
		
		$sql = $db->select()
     	->from(array("a"=>"tbl_studentsemesterstatus"))
     	->join(array("b"=>"tbl_semestermaster"),"a.IdSemesterMain=b.IdSemesterMaster",array())
     	->where("SemesterMainEndDate >= CURDATE() AND SemesterMainStartDate <= CURDATE()");
     	
     	//echo $sql;
     	$result = $db->fetchRow($sql);
     	return $result;
	}
	
	public function isRegistered($idstudreg,$semid,$idsubject){
		$db = $this->db;
		//0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer 6: CT 9 : Pre-Repeat
		$sql = $db->select()
		     	->from(array("a"=>$this->_name))
		     	->where("a.IdStudentRegistration = ?",$idstudreg)
		     	->where("a.IdSemesterMain = ?",$semid)
		     	->where("a.IdSubject= ?",$idsubject)
		     	->where("a.active = 0 or a.active = 1 or a.active = 4 or a.active = 5 or a.active = 9");
     	$result = $db->fetchRow($sql);
     	//echo $sql;
     	if(is_array($result)) return true;
     	else return false;			     			
	}	
}
?>