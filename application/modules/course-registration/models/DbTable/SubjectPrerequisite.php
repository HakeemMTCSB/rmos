<?php 

class CourseRegistration_Model_DbTable_SubjectPrerequisite extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_subjectprerequisites';
	protected $_primary = "IdSubjectPrerequisites";

			
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('pre'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('pre'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	
	public function getPrerequisite($idLandscape,$IdSubject){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('pre'=>$this->_name))
	                ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=pre.IdRequiredSubject',array('req_subcode'=>'SubCode','req_credithours'=>'CreditHours'))
	                ->where('pre.IdLandscape = ?',$idLandscape)
	                ->where('pre.IdSubject = ?',$IdSubject);		                     
        
        $row = $db->fetchRow($select);
        
        if(!$row){
        	//get based on Subject
        	$select2 = $db->select()
	                ->from(array('pre'=>$this->_name))
	                ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=pre.IdRequiredSubject',array('req_subcode'=>'SubCode'))
	                ->where('pre.IdSubject = ?',$IdSubject);		                     
        
       		$row = $db->fetchRow($select2);
        }
        
        if($row){
			return $row;
        }else{
        	return null;
        }
		
	}
	
	public function checkStudentPrereq($idsubject){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql = $db->select()
				->from("tbl_studentregsubjects")
				->where("IdSubject = ?",$idsubject)
				->where("exam_status = 'C'")
				->where("grade_status = 'Pass'");
		
		$row = $db->fetchRow($sql);
	    
		if($row){
			return true;
        }else{ 
        	return false;
        }		
	}
}

?>