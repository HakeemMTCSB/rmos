<?php 

class CourseRegistration_Model_DbTable_Studentregsubjectsdetails extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregsubjects_detail';
	protected $_primary = "id";

	
	public function getRegSubjectDetail($idReg){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
				  ->from(array('a'=>'tbl_studentregsubjects_detail'))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = a.subject_id')
				  ->where('a.regsub_id = ?',$idReg);
	  	
	  $row = $db->fetchAll($select);
	   
	  return $row;
	
	}
	
}

?>