<?php
class CourseRegistration_Model_DbTable_LandscapeDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_landscape_detail';
    protected $_primary = "ld_id";

    public function getDatabyId($id=0){
        $id = (int)$id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ld'=>$this->_name) )
            ->where($this->_primary.' = ' .$id);

        $row = $db->fetchRow($select);
        return $row;

    }

    public function getDatabyLandscapeId($idLandscape=0,$join=1,$semtype=null){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ld'=>$this->_name) );
        if ( $join )
        {
            $select->join(array('d'=>'tbl_definationms'),'d.idDefinition=ld.SemesterType',array('SemesterType'=>'DefinitionDesc'));
        }

        if($semtype){
            $select->where('d.DefinitionDesc = ?',$semtype);
        }
        $select->where('IdLandscape = ?',$idLandscape);

        $rows = $db->fetchAll($select);
        return $rows;

    }

    public function getDuplicate($IdLandscape,$semesterType){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ld'=>$this->_name) )
            ->where("IdLandscape = '".$IdLandscape."'")
            ->where("SemesterType = '".$semesterType."'");

        $row = $db->fetchRow($select);
        return $row;

    }

    public function addData($data){
        $this->insert($data);
    }

    public function updateData($data,$id){
        $this->update($data,"ld_id ='".$id."'");
    }

    public function deleteData($id){
        $this->delete("ld_id='".$id."'");
    }

}
?>