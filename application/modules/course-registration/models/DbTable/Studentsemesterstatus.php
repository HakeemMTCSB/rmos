<?php 

class CourseRegistration_Model_DbTable_Studentsemesterstatus extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentsemesterstatus';
	protected $_primary = "idstudentsemsterstatus";

			
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	

	public function getRegisteredBlock($registrationId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))   
                   ->joinleft(array('b'=>'tbl_landscapeblock'),'b.idblock=s.IdBlock',array('blockname','semester_level'=>'semester'))      
                   ->joinleft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))       
                   ->where('s.IdStudentRegistration = ?',$registrationId) 
                   ->order("s.idstudentsemsterstatus")
                   ->group('s.IdBlock'); 
                  
        $result = $db->fetchAll($sql);
        
        //print_r($result);
        return $result;
	}
	
	
	public function getRegisteredSemester($registrationId,$sorts='Desc'){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))       
                   ->where('s.IdStudentRegistration = ?',$registrationId)
                   ->order("sm.SemesterMainStartDate $sorts")
                   ->group('s.IdSemesterMain'); 
                  
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function updateRegistrationData($data,$id){
		 $this->update($data, 'IdStudentRegistration = '. $id);
	}
	
	public function deleteRegisterData($id){		
	  $this->delete('IdStudentRegistration =' . (int)$id);
	}
	
		
	public function getSemesterInfo($idstudentsemsterstatus=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->joinleft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))  
                   ->where('s.idstudentsemsterstatus = ?',$idstudentsemsterstatus);                   
                  
        $result = $db->fetchRow($sql);
        return $result;
	}
	
	public function getRegisteredSemesterBlock($registrationId,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))   
                   ->joinleft(array('b'=>'tbl_landscapeblock'),'b.idblock=s.IdBlock',array('blockname','semester_level'=>'semester'))      
                   ->joinleft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))       
                   ->where('s.IdStudentRegistration = ?',$registrationId) 
                   ->where('s.IdSemesterMain = ?',$idSemester) 
                   ->order("s.idstudentsemsterstatus");
                  
                  
        $result = $db->fetchAll($sql);
        
        //print_r($result);
        return $result;
	}
	
	public function getCountableRegisteredSemester($registrationId){
		
		//utk dapatakan jumlah semester yg sudah pernah daftar, countable dan status completed atau register
		//jika dulu pernah daftar then quit/tangguh maka ia tidak dikira
		//jika semester tu not countable cth semster pembaikan maka ia tidak dikira naik level
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))                    
                   ->where('s.IdStudentRegistration = ?',$registrationId)
                   ->where('sm.IsCountable = 1')
                   ->where('(s.studentsemesterstatus = 130 OR s.studentsemesterstatus = 229)') //130:Register 229:Completed
                   ->order("sm.SemesterMainStartDate");                  
                  
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function isRegisteredSemesterStatus($IdStudentRegistration,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))   
                   ->where('s.IdStudentRegistration = ?',$IdStudentRegistration) 
                   ->where('s.IdSemesterMain = ?',$idSemester);
                                     
        $result = $db->fetchRow($sql);
        
        return $result;
	}
	
	public function checkSemesterCourseRegistration($idsemester,$progid){
		
		
 		$db = Zend_Db_Table::getDefaultAdapter();
        
       
         $select = $db->select()
                             ->from(array('sm'=>'tbl_semestermaster'))
                             ->from(array('tp'=>'tbl_program'))
                             ->join(array('ac'=>'tbl_activity_calender'),'ac.IdSemesterMain = sm.IdSemesterMaster')
                             ->where('CURDATE()    BETWEEN ac.StartDate AND ac.EndDate')
                             ->where('ac.idActivity=18')
                             ->where('sm.IdSemesterMaster = ?',$idsemester)
                             ->where('tp.idProgram=?',$progid);
     
        
        $row = $db->fetchRow($select);
        
        if (!isset($row))
        {
            $sql = $db->select()
                         ->from(array('sm'=>'tbl_semestermaster'))
                         ->from(array('tp'=>'tbl_program'))
                         ->join(array('ac'=>'tbl_activity_calender'),'ac.IdSemesterMain = sm.IdSemesterMaster')
                         ->where('CURDATE()    BETWEEN ac.StartDate AND ac.EndDate')
                         ->where('idActivity=18')
                         ->where('sm.IdSemesterMaster = ?',$idsemester);
        
            $row = $db->fetchRow($sql);
        }
        
        //echo $sql;
        return $row;		
	}	
	
	public function getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain){
		
			$db = Zend_Db_Table::getDefaultAdapter();      
			
			$sql = $db->select()	
					->from(array('sss' => 'tbl_studentsemesterstatus'))
					->where('IdStudentRegistration  = ?',$IdStudentRegistration)
					->where('IdSemesterMain = ?',$IdSemesterMain);
			$result = $db->fetchRow($sql);
			return $result;		
	}
	
	
	public function getStudentPreviousSemester($IdStudentRegistration){
		
		/*
		 * SELECT sss.* ,sm.*
			FROM `tbl_studentsemesterstatus` as sss
			JOIN tbl_semestermaster as sm ON sm.IdSemesterMaster =  sss.`IdSemesterMain`
			WHERE sss.`IdStudentRegistration` = 3296
			AND SemesterMainEndDate < '2015-09-21'
			ORDER BY sm.SemesterMainStartDate DESC
		 */
		
		$db = Zend_Db_Table::getDefaultAdapter();      
			
		$curdate = '2015-09-21';
		
		$sql = $db->select()	
				->from(array('sss' => 'tbl_studentsemesterstatus'))
				->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =  sss.IdSemesterMain')
				->where('sss.IdStudentRegistration  = ?',$IdStudentRegistration)
				->where('sm.SemesterMainEndDate < ?',$curdate)
				->order('sm.SemesterMainStartDate DESC');
		$result = $db->fetchRow($sql);
		return $result;		
	}
}

?>