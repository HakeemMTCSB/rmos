<?php
class CourseRegistration_Model_DbTable_TempTimetable extends Zend_Db_Table_Abstract
{
	protected $_name = 'temp_timetable';
    protected $_primary = "id";


	public function init() {
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function insertData(array $data) {
		$this->insert($data);
	}
	
	public function getData($status = NULL) {
		$db = $this->db;
		
		$select = $db->select();
		$select->from('temp_timetable');
		
		if($status != NULL)
			$select->where('status = ?',$status);
		
		$rows = $db->fetchAll($select);
		
		return $rows;
	}
	
	public function getColumn() {
		$cols = $this->db-> describeTable($this->_name);
		return $cols;
	}
	
	public function updateError() {
		$db = $this->db;
		
		$select = $db->select();
		$select->from('temp_timetable')
		->where("status = 'NEW'");
		
		$rows = $db->fetchAll($select);
		
		foreach($rows as $data){
			if($data['program'] == ''){
				//error
				echo $data['id'].',';
				$this->update(array('status'=>'ERROR'),'id = '.$data['id']);
			}
			
		}
		
		return $rows;
	}
	
	public function getDataByGroup() {
		$db = $this->db;
		
		$select = $db->select();
		$select->from('temp_timetable',array('course_code','merge_group','programname'=>'GROUP_CONCAT(DISTINCT tag1_value)','mode_of_program'=>'tag2_value','lecturer_code','lecturer_name','course_name','branch'=>'tag4_value','class_size'))
				->where("status = 'NEW'")
				//->where("course_code = 'FN6103'")
				->where("class_type NOT IN ('Examination')")
				->group('course_code')
				->group('tag2_value')
				->group('tag4_value')
				->group('lecturer_code')
				->order('course_code asc');
//				->limit(200);
		
		//echo $select;
		//exit;

		$rows = $db->fetchAll($select);
		
		return $rows;
	}
	
	public function getSemesterList() {
		$db = $this->db;
		
		$select = $db ->select()
                    ->from(array("a"=>"tbl_semestermaster"))
                    ->where("display = 1")
                    ->where("AcademicYear >= ?",date('Y'))
                    ->group('SemesterMainName')
                    ->order('AcademicYear desc')
                    ->order('sem_seq desc');					  
		 $row = $db->fetchAll($select);	
		
		return $row;
	}
	
	public function getProgramInfo($programId) {
		$db = $this->db;
		
		$select = $db ->select()
                    ->from(array("a"=>"tbl_program"))
                    ->where("IdProgram = ?",$programId);
		 $row = $db->fetchRow($select);	
		
		return $row;
	}
	
	public function getSubjectInfo($courseCode) {
		$db = $this->db;
		
		$select = $db ->select()
					  ->from(array("a"=>"tbl_subjectmaster"))
					  ->where("SubCode = ?",$courseCode);
		 $row = $db->fetchRow($select);	
		
		return $row;
	}
	
	public function getSemesterInfoScheme($semName, $schemeId) {
		$db = $this->db;
		
		$select = $db ->select()
					  ->from(array("a"=>"tbl_semestermaster"))
					  ->where("SemesterMainName = ?",$semName)
					  ->where("IdScheme = ?",$schemeId);
		 $row = $db->fetchRow($select);	
		
		return $row;
	}

	public function getCourseGroupProgram($progName,$courseCode,$mof,$lecturerId) {
		$db = $this->db;
		
		if($mof){
			$select = $db ->select()
						  ->from(array("a"=>"temp_timetable"),array('idAll'=>'GROUP_CONCAT(DISTINCT id)','*'))
						  ->where("tag1_value = '$progName'")
						  ->where("course_code = ?",$courseCode)
						  ->where("tag2_value = ?",$mof);
		}else{
			
			$select = $db ->select()
					  ->from(array("a"=>"temp_timetable"),array('idAll'=>'GROUP_CONCAT(DISTINCT id)','*'))
					  ->where("tag1_value = '$progName'")
					  ->where("course_code = ?",$courseCode)
					  ->where("tag2_value IS NULL");
		}
		
		if($lecturerId){
			$select->where("lecturer_code = ?",$lecturerId);
		}
                //$select->where("status = 'NEW'");
		$select->where("class_type NOT IN ('Examination')");
		$select->group('tag3_value');
		$select->group('tag1_value');
		$select->group('tag5_value');
		
		$row = $db->fetchAll($select);	
		
		return $row;
	}
	
	public function getCourseGroupSchedule($progName,$courseCode,$mof,$lecturerId,$branch) {
		$db = $this->db;
		
		$select = $db ->select()
						  ->from(array("a"=>"temp_timetable"),array('idAll'=>'GROUP_CONCAT(DISTINCT id)','*'))
						  ->where("tag1_value = '$progName'")
						  ->where("course_code = ?",$courseCode);
						  
		if($mof){
			$select->where("tag2_value = ?",$mof);
		}else{
			
			$select->where("tag2_value IS NULL");
		}
		
		$select->where("tag4_value = ?", $branch);

		if($lecturerId){
			$select->where("lecturer_code = ?",$lecturerId);
		}
                //$select->where("status = 'NEW'");
		$select->where("class_type NOT IN ('Examination')");
		$select->group('class_date');
		$select->group('class_day');

		$row = $db->fetchAll($select);	
		
		return $row;
	}
	
	public function getProgramScheme($programId,$mofId,$mopsId,$mosId) {
		$db = $this->db;
		
		$select = $db ->select()
					  ->from(array("a"=>"tbl_program_scheme"))
					  ->where("IdProgram = ?",$programId)
					  ->where("mode_of_program = ?",$mofId)
					  ->where("mode_of_study = ?",$mopsId);
		if($mosId){
			$select->where("program_type = ?",$mosId);
		}
		
//		echo $select;exit;
		 $row = $db->fetchRow($select);	
		
		return $row;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function getExamScheduleData() {
		$db = $this->db;
		
		echo $select = $db->select();
		$select->from(array("a"=>"temp_timetable"),array('idAll'=>'GROUP_CONCAT(DISTINCT id)','*'))
				->where("status = 'NEW'")
//				->where("course_code = 'IB1007'")
				->where("class_type IN ('Examination')")
				->group('course_code')
				
				->group('tag2_value')
				->group('lecturer_code')
				->order('course_code asc');
//				->limit(200);
		
		$rows = $db->fetchAll($select);
		
		return $rows;
	}
	
	public function cancelImport($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $update = $db->update('temp_timetable', $bind, 'status = "NEW"');
            return $update;
        }

	public function clearTimeTable(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$delete = $db->delete('temp_timetable');
		return $delete;
	}
        
        public function checkBlockSemester($name){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_timetableblocksemester'), array('value'=>'*'))
                ->where('a.tbs_semestername = ?', $name);
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function insertBlockSemester($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('tbl_timetableblocksemester', $bind);
            $id = $db->lastInsertId('tbl_timetableblocksemester', 'tbs_id');
            return $id;
        }
        
        public function getDataExportToQp(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'temp_timetable'), array('value'=>'*'))
                ->where('a.status = ?', 'NEW');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function insertQpEvent($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('qp_event', $bind);
            $id = $db->lastInsertId('qp_event', 'id');
            return $id;
        }

	public function getGroup($sem){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
			->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'SubjectName','faculty_id'=>'IdFaculty','subjectMainDefaultLanguage'))
			->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=a.IdSemester',array('semester_name'=>'SemesterMainName','AcademicYear'))
			->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=a.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
			->joinLeft(array('branch'=>'tbl_branchofficevenue'), 'a.IdBranch=branch.IdBranch', array('branch.BranchName'))
			->where('IdSemester in (?)', $sem);
		//->where('IdCourseTaggingGroup = ?', 1961);
		//echo $select; exit;
		$result = $db->fetchAll($select);
		return $result;
	}

	public function getGroupProgram($groupid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'course_group_program'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_program'), 'a.program_id=b.IdProgram')
			->joinLeft(array('c'=>'tbl_program_scheme'), 'a.program_scheme_id = c.IdProgramScheme')
			->joinLeft(array('d'=>'tbl_definationms'), 'c.mode_of_program=d.idDefinition', array('mop'=>'d.DefinitionDesc'))
			->joinLeft(array('e'=>'tbl_definationms'), 'c.mode_of_study=e.idDefinition', array('mos'=>'e.DefinitionDesc'))
			->joinLeft(array('f'=>'tbl_definationms'), 'c.program_type=f.idDefinition', array('pt'=>'f.DefinitionDesc'))
			->joinLeft(array('g'=>'tbl_scheme'), 'b.IdScheme=g.IdScheme')
			->where('a.group_id = ?', $groupid);
		//echo $select; exit;
		$result = $db->fetchAll($select);
		return $result;
	}

	public function getGroupSchedule($groupid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
			->where('a.idGroup = ?', $groupid);
		//echo $select.';<br/>'; //exit;
		$result = $db->fetchAll($select);
		return $result;
	}

	public function getSemesterListByName($semName) {
		$db = $this->db;

		$select = $db ->select()
			->from(array("a"=>"tbl_semestermaster"))
			->where("SemesterMainName = ?",$semName);
		//->where("IdScheme = ?",$schemeId);
		$row = $db->fetchAll($select);

		return $row;
	}
}
?>