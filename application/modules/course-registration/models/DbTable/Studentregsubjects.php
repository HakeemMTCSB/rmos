<?php 

class CourseRegistration_Model_DbTable_Studentregsubjects extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregsubjects';
	protected $_primary = "IdStudentRegSubjects";

			
	public function addData($data){		
	
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $lastId = $db->lastInsertId();
	   return $lastId;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getTotalRegister($idSubject=null,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 	$row = $db->fetchAll($select);
	 	if($row){
	 		return count($row);	
	 	}else{
	 		return 0;
	 	}
				 
	}
	
	public function getStudents($idSubject=null,$idSemester,$student_per_group){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		 $select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->joinLeft(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('srs.IdCourseTaggingGroup = 0')
	 				 ->order("registrationId")
	 				 ->limit($student_per_group,0);
	 	$row = $db->fetchAll($select);
	 	
	 	
	 	return $row;
				 
	}
	
	public function getUnTagGroupStudents($idSubject,$idSemester,$limit=""){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->joinLeft(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->joinLeft(array('ap'=>'student_profile'),'ap.appl_id=sr.IdApplication',array('appl_fname','appl_mname','appl_lname'))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('srs.IdCourseTaggingGroup = 0')//0:No Group
	 				 ->order("registrationId");
	 	if(isset($limit) && $limit!=""){
	 		$select->limit($limit);
	 	}
	 
	 	$row = $db->fetchAll($select);
	 	return $row;
				 
	}
	
	public function getTaggedGroupStudents($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->joinLeft(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->joinLeft(array('ap'=>'student_profile'),'ap.appl_id=sr.IdApplication',array('appl_fname','appl_mname','appl_lname'))	 				 
	 				 ->where('srs.IdCourseTaggingGroup = ?',$idGroup)//0:No Group
	 				 ->order("registrationId");
	 				
	 	$row = $db->fetchAll($select);
	 	return $row;
				 
	}
	
	
	public function getTotalAssigned($idSubject=null,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('IdCourseTaggingGroup != 0');
	 	$row = $db->fetchAll($select);
	 	
	 	if($row){
	 		return count($row);	
	 	}else{
	 		return null;
	 	}
				 
	}
	
	public function getTotalUnAssigned($idSubject=null,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('IdCourseTaggingGroup = 0');
	 	$row = $db->fetchAll($select);
	 	
	 	if($row){
	 		return count($row);	
	 	}else{
	 		return null;
	 	}
				 
	}
	
	
	public function updateRegistrationData($data,$id){
		 $this->update($data, 'IdStudentRegistration = '. $id);
	}
	
	public function deleteRegisterSubjects($id){		
	  $this->delete('IdStudentRegistration =' . (int)$id);
	}
	
	public function getRegStudents($idSubject=null,$idSemester,$idCollege="",$post=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		 $select =$db->select()
		 			->distinct()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array("registrationId"))	 				 
	 				 ->join(array('tp'=>'tbl_program'),'sr.IdProgram=tp.IdProgram',array())
	 			//	 ->join(array('tc'=>'tbl_collegemaster'),'tp.idCollege=tc.idCollege',array("collegename","idCollege"))
	 				 ->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array("appl_fname","appl_mname","appl_lname"))
	 				 ->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 			//	 ->joinLeft(array('at'=>'applicant_transaction'),'at.at_trans_id=sr.transaction_id',array('at_pes_id'))
	 			//	 ->joinLeft(array('lk'=>'sis_setup_detl'),"lk.ssd_id=sp.appl_religion",array("religion"=>"ssd_name"))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->order("registrationId");
	 			
	 				 
	
	    if (isset($post['applicant_name']) && !empty($post['applicant_name'])) {
         
            $select->where("(sp.appl_fname LIKE '%". $post['applicant_name']."%'");
            $select->orwhere("sp.appl_mname LIKE '%". $post['applicant_name']."%'");
            $select->orwhere("sp.appl_lname LIKE '%". $post['applicant_name']."%')");
        }
        
        if (isset($post['s_student_id']) && !empty($post['s_student_id'])) { 
        	if (isset($post['e_student_id']) && !empty($post['e_student_id'])) {           
            	$select->where("sr.registrationId >= '". $post['s_student_id']."' AND sr.registrationId <= '". $post['e_student_id']."'");
        	}else{
        		$select->where("sr.registrationId >= '". $post['s_student_id']."' AND sr.registrationId <= '". $post['s_student_id']."'");
        	}
        }
	
	    if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {           
            $select->where("sr.IdProgram = ?", $post['IdProgram']);
        }    
		
        $row = $db->fetchAll($select);
	 	
	 	
	 	return $row;
				 
	}
	
	public function dropRegisterSubjects($id){	
		
		$auth = Zend_Auth::getInstance();
		
		$reginfo = $this->getSubjectRegRaw($id);
		$studentregDB = new Registration_Model_DbTable_Studentregistration();
		$studInfo = $studentregDB->getInfo($reginfo["IdStudentRegistration"]);
		
		$authorized=true;
		if($auth->getIdentity()->IdRole=="445" || $auth->getIdentity()->IdRole=="3" ){
			if($studInfo["AcademicAdvisor"]!=$auth->getIdentity()->IdStaff){
				$authorized = false;
			}
		}
		$ssemDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$issemopen = $ssemDB->checkSemesterCourseRegistration($reginfo["IdSemesterMain"],$studInfo["IdProgram"]);
		
		if($authorized){
			if(is_array($issemopen)){
				$this->saveHistory($reginfo,"DROP");
				$this->delete('IdStudentRegSubjects =' . (int)$id);
				//echo "masuk";
				$returnVal["ERROR"]= null;
			}else{
				$returnVal["ERROR"]="POLICY ERROR : Subject can be only drop during Add&Drop Period";
				echo $returnVal["ERROR"]; 
			}
		}else{			
			$returnVal["ERROR"]= "AUTHORIZATION ERROR"; 
		}
	  	
		return $returnVal;
	}	
	
	public function saveHistory($data,$activity){
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();
		$data["hdate"]=date("Y-m-d H:i:s");
		$data["huser"]=$auth->getIdentity()->id;
		$data["hactivity"]=$activity;		
		$db->insert("studentregsubjects_history",$data);
	}
	
	public function getSubjectInfo($idStudentRegistration,$idSubject,$idSemester=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function getSubjectRegistrationInfo($idStudentRegistration,$idSemester=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->group("srs.IdStudentRegistration");
	 				 
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function getRegisteredSubject($idStudentRegistration,$semester_id){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
	  ->from(array('srs'=>'tbl_studentregsubjects'))
	  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject')
	  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	  ->where('srs.IdSemesterMain = ?',$semester_id);
	  	
	  $row = $db->fetchAll($select);
	   
	  return $row;
	
	}
	
	
	public function isRegistered($idStudentRegistration,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->order('IdStudentRegSubjects DESC');
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function getSubjectRegInfo($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration') 				
	 				 ->where('srs.IdStudentRegSubjects = ?',$IdStudentRegSubjects);
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function deleteParentSubjects($idStudentRegistration,$idSubject){	
	
	  $this->delete('IdStudentRegistration ='.$idStudentRegistration.' AND IdSubject ='.$idSubject);
	}
	
	public function getSubjectRegRaw($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	
	 				 ->where('srs.IdStudentRegSubjects = ?',$IdStudentRegSubjects);
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}	
	
	
	public function getSumCreditHour($idStudentRegistration,$semester_id){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
	  			  ->from(array('srs'=>'tbl_studentregsubjects'),array("TotalCH"=>"SUM(CreditHours)"))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject',array())
				  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				  ->where('srs.IdSemesterMain = ?',$semester_id)
				  ->where('srs.subjectlandscapetype != 2')
				  ->where('(srs.Active = 1 OR srs.Active = 4 OR srs.Active = 5 )');
				  	
	  $row = $db->fetchRow($select);
	   
	  return $row;
	
	}
	
	
	public function searchRegisterStudent($post=null){
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			
		    $select =$db->select()
		  			->distinct()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array("registrationId"))	 	
	 				 ->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')			 
	 				 ->join(array('tp'=>'tbl_program'),'sr.IdProgram=tp.IdProgram',array())	 			
	 				 ->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array("appl_fname","appl_mname","appl_lname"))	
	 				 ->order("registrationId");
	 			

		    if (isset($post['IdSemester']) && !empty($post['IdSemester'])) {         
	             $select->where("srs.IdSemesterMain = ?", $post['IdSemester']);
	        }
	        
			if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {         
	             $select->where("sr.IdProgram = ?", $post['IdProgram']);
	        } 
		  
			if (isset($post['IdSubject']) && !empty($post['IdSubject'])) {         
	             $select->where("srs.IdSubject = ?", $post['IdSubject']);
	        } 
	        
		    if (isset($post['student_name']) && !empty($post['student_name'])) {
	         
	            $select->where("(sp.appl_fname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_mname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_lname LIKE '%". $post['applicant_name']."%')");
	        }
        
	        if (isset($post['registrationId']) && !empty($post['registrationId'])) { 
	        	  $select->where("(sr.registrationId LIKE '%". $post['registrationId']."%'");	        	
	        }	      
		
	        $row = $db->fetchAll($select);
		 	
		 	
		 	return $row;
				 
	}
	
	
	public function searchStudentNotRegisterExam($post=null){
		
		    $db = Zend_Db_Table::getDefaultAdapter();
		    			
		    $select_er = "SELECT er_idStudentRegistration FROM exam_registration WHERE er_idSubject=".$post['IdSubject']." AND er_idSemester=".$post['IdSemester']." AND er_idProgram=".$post['IdProgram'];
			$row_er = $db->fetchAll($select_er);
			 
		    $select =$db->select()
		  			->distinct()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array("registrationId"))	 	
	 				 ->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')			 
	 				 ->join(array('tp'=>'tbl_program'),'sr.IdProgram=tp.IdProgram',array())	 			
	 				 ->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array("appl_fname","appl_mname","appl_lname"))
	 					 				
	 				 ->order("registrationId");
	 			

 			 if(count($row_er)>0){
 			 	 $select->where('sr.IdStudentRegistration NOT IN (?)',$row_er);
 			 }
 			 
		    if (isset($post['IdSemester']) && !empty($post['IdSemester'])) {         
	             $select->where("srs.IdSemesterMain = ?", $post['IdSemester']);
	        }
	        
			if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {         
	             $select->where("sr.IdProgram = ?", $post['IdProgram']);
	        } 
		  
			if (isset($post['IdSubject']) && !empty($post['IdSubject'])) {         
	             $select->where("srs.IdSubject = ?", $post['IdSubject']);
	        } 
	        
		    if (isset($post['student_name']) && !empty($post['student_name'])) {
	         
	            $select->where("(sp.appl_fname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_mname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_lname LIKE '%". $post['applicant_name']."%')");
	        }
        
	        if (isset($post['registrationId']) && !empty($post['registrationId'])) { 
	        	  $select->where("(sr.registrationId LIKE '%". $post['registrationId']."%'");	        	
	        }	      
		
	        $row = $db->fetchAll($select);
		 	
		 	
		 	return $row;
				 
	}
	
	public function getRegSubjectGroup($idStudentRegistration,$semester_id){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
				  ->from(array('srs'=>'tbl_studentregsubjects'))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject')
				  ->joinLeft(array('g'=>'tbl_course_tagging_group'),'g.IdCourseTaggingGroup=srs.IdCourseTaggingGroup',array('GroupName','GroupCode'))
				  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				  ->where('srs.IdSemesterMain = ?',$semester_id);
	  	
	  $row = $db->fetchAll($select);
	   
	  return $row;
	
	}
	
	
	public function isRegisteredBySemester($idStudentRegistration,$idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	
	public function getCompletedCreditHour($idStudentRegistration){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
	  			  ->from(array('srs'=>'tbl_studentregsubjects'),array("TotalCH"=>"SUM(CreditHours)"))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject',array())
				  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				  ->where('(srs.Active = 1 OR srs.Active = 4 OR srs.Active = 5 )')
				  ->where('srs.exam_status = "C"')
				  ->group('srs.IdSubject');
				  	
	  $row = $db->fetchRow($select);
	   
	  return $row['TotalCH'];
	
	}
	
	public function getTotalRegisteredBySemester($idStudentRegistration,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('(srs.Active = 1 OR srs.Active = 4 OR srs.Active = 5 )');
	 				
	 	$row = $db->fetchAll($select);
	 	
	 	return $row;
		
	}

    public function getTotalCreditHourTaken($idStudentRegistration,$idSubject){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('srs'=>'tbl_studentregsubjects'), array('total'=>'SUM(credit_hour_registered)'))
            ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
            ->where('srs.IdSubject = ?',$idSubject);

        $row = $db->fetchAll($select);

        return $row;

    }
}

?>