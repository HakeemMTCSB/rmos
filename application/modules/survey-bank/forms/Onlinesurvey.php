<?php
class SurveyBank_Form_Onlinesurvey extends Zend_Form
{
	protected $_branchid;
	protected $_semesterid;
	protected $_programid;
	protected $_semesterno;
	protected $_type;

	public function setBranchid($value) {
		$this->_branchid = $value;
	}
	
	public function setSemesterid($value) {
		$this->_semesterid = $value;
	}
	
	public function setProgramid($value) {
		$this->_programid = $value;
	}
	
	public function setSemesterno($value) {
		$this->_semesterno = $value;
	}
	
	public function setType($value) {
		$this->_type = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','onlinesurveyreport_form');
		
		$hidden = $this->createElement('hidden', 'ordering');
		$hidden->setValue(1);
		$this->addElement($hidden);
		
		//branch
		$branchDB = new App_Model_General_DbTable_Branch();
		$branchList = $branchDB->listBranch($this->_branchid);
		
		$this->addElement('select', 'branch_id', array(
			'label'=>'Branch',
			'required'=>'true',
		    'multioptions'=>$branchList,
			'value'=>$this->_branchid,
		    'onchange'=>'getProgram(this)'));

        //program
		$program = new App_Model_Record_DbTable_Program();
        $program_list = $program->getBranchProgram($this->_branchid);
      
		$this->addElement('select', 'program_id', array(
		    'label'=>'Program',
		    'required'=>'true',
			'value'=>$this->_programid,
		    'multioptions'=>$program_list,
		    'onchange'=>'getIntake(this)'));
		
		//semester
        $semester = new App_Model_Record_DbTable_Semester();
        $semester_list = $semester->listSemester();
		
		$this->addElement('select', 'semester_id', array(
			'label'=>'Intake',
			'required'=>'true',
			'value'=>$this->_semesterid,
		    'multioptions'=>$semester_list));
		
		//report type
		$type = $this->createElement('select','type');
        $type ->setLabel('Report Type')
        	->setRequired(true)
        	->setValue($this->_type)
            ->addMultiOptions(array(
            		'0' => '--Please Select--',
                    '1' => 'by Lecturer',
                    '2' => 'by Intake' ,
                        ));    
		$this->addElement($type);	
		
		$this->addElement('submit', 'submit', array(
		    'label' => 'Search'
		));
		
	}
}
?>