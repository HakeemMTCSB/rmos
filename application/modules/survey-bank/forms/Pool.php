<?php
class SurveyBank_Form_Pool extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','university_form');
		
//     
		$this->addElement('text','name', array(
			'label'=>'Question Pool Name',
			'id'=>'name',
			'class' => 'input-txt',
			'required'=>'true'));
			
		$this->addElement('textarea','desc', array(
			'label'=>'Description',
			'class' => 'input-txt',));
		
		$this->addElement('checkbox', 'status', array(
            'label'      => 'Is Active?',
            'required'   => true,
            'value' => 1
        ));
        //$this->status->setSeparator('')->setValue('1');
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'survey-bank', 'controller'=>'pool','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>