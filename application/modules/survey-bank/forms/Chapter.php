<?php
Class SurveyBank_Form_Chapter extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form1');
		
//	    Question Pool

		$this->addElement('select','idPool', array(
			'label'=>'Question Pool',
			'required'=>'true'));
		
		$poolDB = new App_Model_Question_DbTable_Pool();
		$poolData = $poolDB->getData();
		
		$this->idPool->addMultiOption(0,"-- Please select --");
		foreach ($poolData as $list){
			$this->idPool->addMultiOption($list['id'],$list['name']);
		}
		
//		Question Type
		$this->addElement('select','question_type', array(
			'label'=>'Question Type',
			'required'=>'true'));
		
		$poolDB = new App_Model_Question_DbTable_QuestionType();
		$poolData = $poolDB->getData();
		
		$this->question_type->addMultiOption(0,"-- Please select --");
		foreach ($poolData as $list){
		$this->question_type->addMultiOption($list['id'],$list['name']);
		}
		
//		Name (Bahagian A/Bahagian B)
		$this->addElement('text','name', array(
			'label'=>'Name',
			'id'=>'name',
			'required'=>'true'));
		
		
//		Active(Status)
		$this->addElement('radio', 'status', array(
            'label'      => 'Is Active?',
            'required'   => true,
            'multioptions'   => array(
                            '0' => 'No',
                            '1' => 'Yes',
                            ),
        ));
         $this->status->setSeparator('')->setValue('1');

//      buttonSave
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
//        buttonCancel
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'survey-bank', 'controller'=>'chapter','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>