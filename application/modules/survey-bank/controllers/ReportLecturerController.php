<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 3/3/2016
 * Time: 3:32 PM
 */
class SurveyBank_ReportLecturerController extends Base_Base {

    public function init() { //initialization function
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->lobjdeftype = new App_Model_Definitiontype();
        $this->model = new SurveyBank_Model_DbTable_ImportQuestion();
    }

    public function summaryAction(){
        $this->view->title = $this->view->translate('Summary');

        $session = Zend_Registry::get('sis');

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->schemeData = $schemeDb->fngetSchemes();

        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
        $this->view->branchData = $branchDb->fnGetBranchList();

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;

        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();

        //clear the session
        if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
            unset($session->result);
        }

        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();

        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();

        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getSearchDataReportSummaryLecturer($formData);

            $this->view->list = $list;
            $this->view->paginator = $list;

        }
    }

    public function rawDataAction(){

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $this->view->title = $this->view->translate('Raw Data');

        $parentid = html_entity_decode($this->_getParam( 'id', 0 ));
        $IdCourseTaggingGroup = html_entity_decode($this->_getParam( 'IdCourseTaggingGroup', 0 ));
        $IdProgram = html_entity_decode($this->_getParam( 'IdProgram', 0 ));
        $schemeId = $this->_getParam( 'idScheme', 0 );
        $mopId = $this->_getParam( 'mop', 0 );

        $this->view->parent_id = $parentid;
        $this->view->IdCourseTaggingGroup = $IdCourseTaggingGroup;
        $this->view->schemeId = $schemeId;
        $this->view->mopId = $mopId;
        $this->view->IdProgram = $IdProgram;

        $schemeDB = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->scheme = $schemeDB->fnViewSchemeDetails($schemeId);

        $defDB = new App_Model_Definitiontype();
        $this->view->mop = $defDB->fetchDetailAward($mopId);

        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
        $sectiontagging = new App_Model_Question_DbTable_SectionTagging();
        $QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();

        $SectionData = $sectiontagging->getAnswerByID($parentid);

        $program = array();
        foreach($SectionData as $row){
            $program[] = $row['program_id'];
        }

        $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
        $dataGroup = $courseGroupDb->getInfoArray($IdCourseTaggingGroup);
        $this->view->group = $dataGroup;

        $semester = $dataGroup['IdSemester'];
        $subjectId = $dataGroup['IdSubject'];

        $list = $surveyDb->getTotalResponse($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram,$mopId);
        $this->view->list = $list;


//		echo "<pre>";
//		print_r($SectionData);

        $arrayPool = array();
        $arraySumInfo = array();
        $a = 0;
        foreach($SectionData as $secData){
//			$arrayPool[$a]['branch'] = $secData['BranchName'];
            $arrayPool[$a]['pool_name'] = $secData['question_pool_name'];
            $arrayPool[$a]['main_set_id'] = $secData['id'];

            //get chapter list
            $chapterList = $QuestionSurvey->getPoolBySetDistinct($secData['id'],$secData['question_pool']);

            foreach($chapterList as $c=>$chp){
//    			$arrayPool[$a]['chapter'][$c] = $chp;

                $arrayPool[$a]['chapter'][$c]['chaptername'] = $chp['chaptername'];
                $arrayPool[$a]['chapter'][$c]['question_type'] = $chp['question_type'];
                $arrayPool[$a]['chapter'][$c]['chapterid'] = $chp['chapterid'];


                //get list of question based on main set id
                $setQuestion = $QuestionSurvey->getSetQuestion($secData['id'],$chp['chapterid']);

                foreach($setQuestion as $q=>$qs){
                    $arrayPool[$a]['chapter'][$c]['question'][$q]['question_id'] = $qs['IdQuestion'];
                    $arrayPool[$a]['chapter'][$c]['question'][$q]['subject_id'] = $dataGroup['IdSubject'];
                    $arrayPool[$a]['chapter'][$c]['question'][$q]['question'] = $qs['question'];
                    $arrayPool[$a]['chapter'][$c]['question'][$q]['order'] = 'Q'.$qs['order'];

                }

            }

            $a++;
        }

        //get student list
        $listStudent = $surveyDb->getResponseStudentList($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram,$mopId);

        $arrayStudent = array();
        foreach($listStudent as $k=>$stud){
            $arrayStudent[$k]['name'] = $stud['name'];
            $arrayStudent[$k]['registrationId'] = $stud['registrationId'];
            $arrayStudent[$k]['Nationality'] = $stud['Nationality'];
            $arrayStudent[$k]['ProgramCode'] = $stud['ProgramCode'];
            $arrayStudent[$k]['GroupName'] = $dataGroup['GroupName'];

            $programScheme = '';
            if ( isset($stud['ProgramMode']) && $stud['ProgramMode'] != '' ){
                $programScheme .= $stud['ProgramMode']."<br>";
            }
            if ( isset($stud['StudyMode']) && $stud['StudyMode'] != '' ){
                $programScheme .= $stud['StudyMode']."<br>";
            }
            if ( isset($stud['ProgramType']) && $stud['ProgramType'] != '' ){
                $programScheme .= $stud['ProgramType'];
            }

            $arrayStudent[$k]['ProgramScheme'] = $programScheme;

            $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
            //$checkCompleteTer = $surveyDb->getFeedbackStatus($stud['IdStudentRegistration'],$semester,$subjectId,$IdCourseTaggingGroup,$secData['id'],$IdProgram,$mopId);
            $studentData = array(
                'IdBranch'=> $stud['IdBranch'],
                'IdProgram'=> $stud['IdProgram'],
                'IdProgramScheme'=> $stud['IdProgramScheme'],
                'IdStudentRegistration'=> $stud['IdStudentRegistration'],
            );

            $checkCompleteTer = $surveyDb->getTerCompleteStatus($studentData,$semester,$subjectId,$IdCourseTaggingGroup);

            $arrayStudent[$k]['feedbackStatus'] = $checkCompleteTer;

            //array answer
            $m=0;
            foreach($arrayPool as $pl){
                foreach($pl['chapter'] as $chp){

                    foreach($chp['question'] as $q=>$que){

                        $sectionTaggingDB = new App_Model_Question_DbTable_SectionTagging();
                        $answerInfo = $sectionTaggingDB->getAnsweredStudent($stud['IdStudentRegistration'],$semester,$subjectId,$IdCourseTaggingGroup,$que['question_id']);

                        $arrayStudent[$k]['answer'][$m] = $answerInfo;

                        $m++;
                    }

                }
            }
        }

        $this->view->student = $arrayStudent;

//		echo "<pre>";
//		print_r($arrayStudent);


        $this->view->data = $arrayPool;

        $this->view->filename = date('Ymd').'_rawdata.xls';
    }

    public function printSummaryAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();

        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getSearchDataReportSummaryLecturer($formData);
            $this->view->list = $list;

            $this->view->paginator = $list;

        }

        $this->view->filename = date('Ymd').'_summaryreport.xls';
    }

    public function rankLecturerAction(){
        $this->view->title = $this->view->translate('Facilitator Rank');

        $session = Zend_Registry::get('sis');

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
        $this->view->branchData = $branchDb->fnGetBranchList();

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;

        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();

        //clear the session
        if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
            unset($session->result);
        }

        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();

        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
        $ranks = null;
        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $this->view->semester = $formData['semester'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getRankCourseLecturer($formData);

            if (count($list) > 0)
                $ranks = $surveyDb->ranking($list);
            //print_r($ranks);
            $this->view->list = $list;
            $this->view->ranks = $ranks;

        }
    }

    public function printRankLecturerAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();

        if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            $this->view->semester = $formData['semester'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getRankCourseLecturer($formData);

            if (count($list) > 0)
                $ranks = $surveyDb->ranking($list);

            $this->view->list = $list;
            $this->view->ranks = $ranks;
        }
        $this->view->filename = date('Ymd').'_Facilitator_Rank.xls';
    }

    public function lecturerPerformanceAction(){
        $this->view->title = $this->view->translate('Comparison of Lecturer Performance');

        $session = Zend_Registry::get('sis');

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->schemeData = $schemeDb->fngetSchemes();

        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
        $this->view->branchData = $branchDb->fnGetBranchList();

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;

        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();

        //clear the session
        if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
            unset($session->result);
        }

        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();

        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();

        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getReportByLecturer2($formData);
            $this->view->list = $list;

        }
    }

    public function printLecturerPerformanceAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();

        $this->_helper->layout->disableLayout();

        if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getReportByLecturer2($formData);
            $this->view->list = $list;
        }
        $this->view->filename = date('Ymd').'_Lecturer_Performance.xls';
    }
}