<?php

Class SurveyBank_ChapterController extends Base_Base {
	
	
	public function indexAction() {
		//title
    	$this->view->title="Section";
    	
    	//paginator
		$poolDB = new App_Model_Question_DbTable_Chapter();
		$poolData = $poolDB->getPaginateData();
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($poolData));
		$paginator->setItemCountPerPage(20);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function viewAction() {
	
		$this->view->title="Section View"; 
		 
		 $id = $this->_getParam('id', 0);
		 $id_Section = $this->_getParam('id',0);
		
		$poolDB = new App_Model_Question_DbTable_Chapter();
    	$poolData = $poolDB->getDataByID($id); 
        /*echo'<pre>';   
    	print_r($poolData);
    	echo'</pre>'; */
    	$this->view->poolData= $poolData;
    	
    	$RubricDB = new App_Model_Question_DbTable_Rubric();
    	$RubricData = $RubricDB->getAnswerByID($id_Section); 
    	/*echo'<pre>';   
    	print_r($RubricData);
    	echo'</pre>'; */
    	$this->view->RubricData= $RubricData;	
       
	}
	
	public function addAction()
    {
    	
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
        }
        
    	//title
//    	$this->view->title="Add New Section Pool";

        $id = $this->_getParam('id', 0);
        $main_set_id=$this->_getParam('main_set_id',0);
        $this->view->main_set_id = $id;
       
    	$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
    	$SectionTaggingData = $sectiontagging->getDataByID($id);
		$pool_id = $SectionTaggingData['question_pool'];
		$this->view->pool_id = $pool_id;
		
//		$poolDB = new App_Model_Question_DbTable_Pool();
//    	$poolData = $poolDB->getData($pool_id);
////    	echo "<pre>";
////    	print_r($poolData);
//    	$this->view->pool = $poolData;
    	
    	
    	$poolDB = new App_Model_Question_DbTable_QuestionType();
    	$poolData = $poolDB->getData();
    	$this->view->pool1 = $poolData;   
    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();

			$date = date('Y-m-d H:i:s');
				
			$auth  = Zend_Auth::getInstance();
			$idUpd = $auth->getIdentity()->id;
				
			$data = array(
//				'idPool'        => $formData['idPool'],
			    'main_set_id'   => $formData['main_set_id'],
			    'pool_id'       => $formData['pool_id'],
				'registration_item_id'       => $formData['registration_item_id'],
				'subsection_instruction'       => $formData['subsection_instruction'],
				'name'          => $formData['id_Section'],			    
		     	'status'        => $formData['status'],
			    'question_type' => $formData['question_type'],
				'dateUpd'       => $date,
				'updUser'       => $idUpd
			);
				
			//process form 
			$pool = new App_Model_Question_DbTable_Chapter();				
			$idChapter = $pool->addData($data);
			
			/*--------------------------rubric-----------------------------*/
			$iteration  		= $formData ["iteration"];	
//			$q["id_Pool"]       = $formData ['idPool'];
//			$q["main_set_id"]   = $formData['main_set_id'];
			$q["id_Section"]    = $idChapter;
			$q["dateUpd"]       = $date;
			$q["updUser"]       = $idUpd;
				
			for($i=1; $i<=$iteration; $i++){
						 	
				$q["ValueNumber"]= $formData["ValueNumber".$i];
				$q["description"]= $formData["description".$i];
				$q["weightage"]= $formData["weightage".$i];

				//process rubric
				$pool2 = new App_Model_Question_DbTable_Rubric();
				$pool2->addData($q);
				
//				echo "<pre>";      
//				print_r($q);
			}
//			exit;
				
			//redirect	
			$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'sectiontagging', 'action'=>'views','id'=>$main_set_id),'default',true));	
		}
	}    
     
	public function editAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
    	
        $id = $this->_getParam('id', 0);
        $this->view->id = $id;
        
        $main_set_id = $this->_getParam('main_set_id', 0);
        $this->view->main_set_id = $main_set_id;
        
        $pool=$this->_getParam('pool_id',0);
        $this->view->pool = $id;
        
		//$poolDB = new App_Model_Question_DbTable_Pool();
		//$poolData = $poolDB->getData();
		//$this->view->pool = $poolData;    	
    	
    	$poolDB = new App_Model_Question_DbTable_QuestionType();
    	$poolData = $poolDB->getData();
    	$this->view->pool1 = $poolData;
    	 
    	$poolDB = new App_Model_Question_DbTable_Chapter();
    	$topic = $poolDB->getDataByID($id);
        $this->view->topic= $topic;
        
        //display branch,program,semester,status
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$SectionData = $sectiontagging->getDataByID( $main_set_id );
        
        //get list registration item
		$regItemDB = new Registration_Model_DbTable_RegistrationItem();
		$this->view->item = $regItemDB->getStudentItems($SectionData['program_id'],$SectionData['semester_id'],$SectionData['IdProgramScheme']);
        
		//$topicDB = new App_Model_Question_DbTable_Chapter(); 
		//$topics = $topicDB->getTopicbySet($id);
		//$this->view->topics = $topics;
		//$this->view->questiontype = $topics["questiontype"];  
    	
    	$RubricDB = new App_Model_Question_DbTable_Rubric();
    	$RubricData = $RubricDB->getAnswerByID($id);

    	$this->view->RubricData= $RubricData;
    	
		//$this->view->topic= $topic;
		//$this->view->status= $poolData["status"];

    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
//			    echo "<pre>";      
//				print_r($formData);
//				exit;
				
				$date = date('Y-m-d H:i:s');
				
				$auth = Zend_Auth::getInstance();
				$idUpd = $auth->getIdentity()->id;

				$data = array(			
//				    'main_set_id'   => $formData['main_set_id'],	
                    'name'          =>$formData["id_Section"],
//				    'question_type' =>$formData["question_type"],
				    'subsection_instruction' =>$formData["instruction"],
					'registration_item_id' =>$formData["registration_item_id"],
			     	'status'        => $formData["status"],	
					'order'        => $formData["order"],				   
					'dateUpd'       => $date,
					'updUser'       => $idUpd
				);
			     	
				//process form 
				$pool = new App_Model_Question_DbTable_Chapter();
				$pool->updateData($data,$id);
				
//				$PoolId = $pool->getData($id);
                $MainId = $pool->getData($id);
				
				//delete all data
				$pool2=new App_Model_Question_DbTable_Rubric();
				$pool2->deleteDataRubric($id);
			 	 
				/*--------------------------rubric-----------------------------*/
				//to get total number of rubric data
				if($topic["question_type_id"] == 3){
				$iteration  		= $formData["iterationedit"];
				
				for($i=1; $i<=$iteration; $i++){
//					$q["id_Pool"]       = $PoolId["idPool"];
//                  $q["main_set_id"]   = $MainId["main_set_id"];
					$q["id_Section"]    = $id;
					$q["dateUpd"]       = $date;
					$q["updUser"]       = $idUpd;
				 	$q["ValueNumber"]   = $formData["ValueNumber".$i];
				 	$q["description"]   = $formData["description".$i];
				 	$q["weightage"]     = $formData["weightage".$i];
				 	
				 	//process rubric
		             $pool2->addData($q); 
			  
//						echo "<pre>";      
//			            print_r($q);
				 }			 
				}
//		           	exit;
				//redirect	
//				$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'chapter', 'action'=>'index'),'default',true));
				$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'sectiontagging', 'action'=>'views','id'=>$main_set_id),'default',true));	

		}
	}    
    
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	$id_Section = $this->_getParam ('id',0) ; 
    	
    	$main_set_id = $this->_getParam('main_set_id', 0);
    	
    	
    	if($id>0){
    		$pool = new App_Model_Question_DbTable_Chapter();
    		$pool->deleteData($id);
    	
//    	   delete answer
    		$pool2 = new App_Model_Question_DbTable_Rubric();
    		$pool2->deleteData($id_Section);
    		
//    		delete questiontagging
    		$pool3 = new App_Model_Question_DbTable_QuestionTagging();
    		$pool3->deleteQuestion($id_Section);
    	}

    	$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'sectiontagging', 'action'=>'views','id'=>$main_set_id),'default',true));
    	
    }

}