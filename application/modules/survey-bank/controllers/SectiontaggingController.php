<?php

class SurveyBank_SectiontaggingController extends Base_Base {


	public function indexAction() {
		//title
		$this->view->title="Set Form";
		
		$oBranch = new App_Model_General_DbTable_Branch();
		$this->view->branchList = $oBranch->getData();
		
		// Semester Data
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$this->view->semesterlist = $semesterDb->getListSemester();
		
		// program data
		$programDB = new App_Model_Record_DbTable_Program();
		$this->view->programs = $programDB->getData();

		if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
			
			$this->view->formData = $formData;
			 
			//paginator
			$SectionDB = new App_Model_Question_DbTable_SectionTagging();
			$paginator = $SectionDB->getPaginateData($formData);
	
		}else{
			//paginator
			$SectionDB = new App_Model_Question_DbTable_SectionTagging();
			$paginator = $SectionDB->getPaginateData();
		}

		$this->view->paginator = $paginator;
	}

	public function viewAction() {
		//title
		$this->view->title="View Set Form";

		$id = $this->_getParam( 'id', 0 );
		$this->view->id = $id;

		//display branch,program,semester,status
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
		
		//why is it taking parentID instead of ID direct
		//$SectionData = $sectiontagging->getDataByParentID($id);
		$SectionData = $sectiontagging->getDataByID($id);
		
		
		$this->view->SectionData= $SectionData;

		//display question pool
		$SectionData1 = $sectiontagging->getAnswerByIDAll( $id );

		$this->view->SectionData1= $SectionData1;
		
		//display question pool without checked
    	$SectionData2 = $sectiontagging->getAnswerByIDAll($id); 
    	
    	
    	$arrayPool = array();
    	$a = 0;
    	foreach($SectionData2 as $secData){
    		$arrayPool[$a] = $secData;
    		
    		//get list of question based on main set id
    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id']);
    		$previewStatus = count($setQuestion);
    		$arrayPool[$a]['preview'] = $previewStatus; 
    		$a++;
    	}
    	$this->view->SectionData2 = $arrayPool;
    	
	}


	public function viewsAction() {//title

		//<---list of question--->
		if ( $this->getRequest()->isPost() ) {
			$post_data = $this->getRequest()->getPost();
			if(!empty($post_data['submitorder'])) {
				$Chap = new App_Model_Question_DbTable_Chapter();
				if(!empty($post_data['order'])) {

					foreach($post_data['order'] as $id => $ord) {
						
						$Chap->update_order($id, $ord);
					}
					
				}

			} else {
				$question = $this->getRequest()->getPost( 'question' );
				$qid = $this->getRequest()->getPost( 'question_id' );

				$condition = array( 'pool_id'=>$pool_id, 'question'=>$question, 'question_id'=>$qid );

				$questionDB = new App_Model_Question_DbTable_Question();
				$this->view->question = $questionDB->getQuestion( $condition );
			}
		}

		$this->view->title="List of Question";

		$id = $this->_getParam( 'id', 0 );
		$this->view->id = $id;

		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$SectionTaggingData = $sectiontagging->getDataByID( $id );
		$pool_id = $SectionTaggingData['question_pool'];
		$this->view->poolid = $pool_id;

		//pool info
		$poolDB = new App_Model_Question_DbTable_Pool();
		$poolData = $poolDB->getData( $pool_id );
		$this->view->pool = $poolData;

		//get list of section
		$topicDB = new App_Model_Question_DbTable_Chapter();
		$topic = $topicDB->getTopicbySet( $id );
		$this->view->topic = $topic;

		//display branch,program,semester,status
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$SectionData = $sectiontagging->getDataByID( $id );
		$this->view->SectionData= $SectionData;

		//get list semester available for copy
		/*$SectionDB = new App_Model_Question_DbTable_SectionTagging();
		$publish = $SectionDB->getTaggingData();
		$this->view->semester = $publish;*/
		
		//get list registration item
		$regItemDB = new Registration_Model_DbTable_RegistrationItem();
		$this->view->item = $regItemDB->getStudentItems($SectionData['program_id'],$SectionData['semester_id'],$SectionData['IdProgramScheme']);

		$poolDB = new App_Model_Question_DbTable_QuestionType();
    	$poolData = $poolDB->getData();
    	$this->view->pool1 = $poolData; 
    	
	}

	public function updateInstructionsAction() {
		
		if ( $this->getRequest()->isPost() ) {
			$id = $this->getRequest()->getPost( 'id' );
			$section_instructions = $this->getRequest()->getPost( 'guideline' );

			$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
			$SectionTaggingData = $sectiontagging->find( $id )->current();

			if(!empty($SectionTaggingData)) {
				$SectionTaggingData->section_instructions = $section_instructions;
				$SectionTaggingData->save();
				$this->_helper->flashMessenger->addMessage(array('success' => "Instruction updated"));
				$this->_redirect( $this->view->url( array( 'module'=>'survey-bank', 'controller'=>'sectiontagging', 'action'=>'views', 'id' => $id ), 'default', true ) );
			} else {
				$this->_helper->flashMessenger->addMessage(array('error' => "Could not update instructions"));
				$this->_redirect( $this->view->url( array( 'module'=>'survey-bank', 'controller'=>'sectiontagging', 'action'=>'index' ), 'default', true ) );
			}
			
		}	

		
	}

	public function addAction() {
		//title



		if ( $this->getRequest()->isPost() ) {

			$formData = $this->getRequest()->getPost();
			//var_dump($formData); exit;

			$date = date( 'Y-m-d H:i:s' );

			$auth = Zend_Auth::getInstance();
			$idUpd = $auth->getIdentity()->id;

			$pool = $formData['question_pool'];
			$n = count( $pool );
			$i = 0;

			$sectiontagging = new App_Model_Question_DbTable_SectionTagging();

			//insert 1st parent id
			$data1 = array(
				'parent_id'     => 0,
				'form_name'     => $formData['form_name'],
				'branch_id'     => $formData['branch_id'],
				'program_id'    => $formData['program_id'],
				'IdProgramScheme'    => $formData['IdProgramScheme'],
				'semester_id'   => $formData['semester_id'],
				'status'        => $formData['status'],
				'dateUpd'       => $date,
				'updUser'       => $idUpd
			);

			$parent_id = $sectiontagging->addData( $data1 );

			while ( $i < $n ) {
				echo $poolID = $pool[$i];

				//insert child
				$data = array(
					'parent_id'     => $parent_id,
					'branch_id'     => $formData['branch_id'],
					'program_id'    => $formData['program_id'],
					'IdProgramScheme'    => $formData['IdProgramScheme'],
					'semester_id'   => $formData['semester_id'],
					'status'        => $formData['status'],
					'dateUpd'       => $date,
					'updUser'       => $idUpd,
					'question_pool' => $poolID,
				);
				$i++;

				$sectiontagging->addData( $data );
			}

			//redirect
			$this->_redirect( $this->view->url( array( 'module'=>'survey-bank', 'controller'=>'sectiontagging', 'action'=>'index' ), 'default', true ) );

		}

		$this->view->title="Set Form : Add";

		$oBranch = new App_Model_General_DbTable_Branch();
		$branch = $oBranch->getData();

		$this->view->branch = $branch;

		// program data
		$programDB = new App_Model_Record_DbTable_Program();
		$this->view->programs = $programDB->getData();

		// Semester Data
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$this->view->semesterlist = $semesterDb->getListSemester();

		// program scheme stuffs
		$DefinationMs = new App_Model_Definitiontype();
		//mode of program
		$this->view->mode_of_programs = $DefinationMs->getDefinationByType(97);

		//mode of study
		$this->view->mode_of_studies = $DefinationMs->getDefinationByType(96);

		//program type
		$this->view->mode_of_programTypes = $DefinationMs->getDefinationByType(99);


		$poolDB = new App_Model_Question_DbTable_Pool();
		$this->view->pool  = $poolDB->getData();

	}

	public function editAction() {

		//title
		$this->view->title="Set Form : Edit";

		$id = $this->_getParam( 'id', 0 );
		
		// Semester Data
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$this->view->semesterlist = $semesterDb->getListSemester();

		$poolDB = new App_Model_Question_DbTable_Pool();
		$this->view->pool  = $poolDB->getData();

		$oBranch = new App_Model_General_DbTable_Branch();
		$branch = $oBranch->getData();
		$this->view->branch = $branch;

		$oProgram = new App_Model_Record_DbTable_Program();
		$program  = $oProgram->getData();
		$this->view->program = $program;

		$poolDB = new App_Model_Question_DbTable_Pool();
		$poolData = $poolDB->getData();
		$this->view->pool = $poolData;

		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$SectionData = $sectiontagging->getDataByID( $id );

		$branch_id = $SectionData['branch_id'];
		$program_id = $SectionData['program_id'];
		$semester_id = $SectionData['semester_id'];
		$IdProgramScheme = $SectionData['IdProgramScheme'];

		$this->view->SectionData= $SectionData;

		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$SectionData1 = $sectiontagging->getAnswerByIDAll( $id );

		$this->view->SectionData1= $SectionData1;
		$this->view->parentID=$id;

		if ( $this->getRequest()->isPost() ) {

			$formData = $this->getRequest()->getPost();

			$date = date( 'Y-m-d H:i:s' );

			$auth = Zend_Auth::getInstance();
			$idUpd = $auth->getIdentity()->id;

			$pool = $formData['question_pool'];
			
			$n = count( $pool );
			$i = 0;

			$sectiontagging = new App_Model_Question_DbTable_SectionTagging();

			$data1 = array(
				'form_name'     => $formData['form_name'],
				'branch_id'     => $formData['branch_id'],
				'program_id'    => $formData['program_id'],
				'IdProgramScheme'    => $formData['IdProgramScheme'],
				'semester_id'   => $formData['semester_id'],
				'status'        => $formData['status'],
				'dateUpd'       => $date,
				'updUser'       => $idUpd
			);

			//insert parent data
			$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
			$sectiontagging->updateData( $data1, $id );


			//get parent_id
			$parent_id = $sectiontagging->getData( $id );

			//delete all data
//			$sectiontagging->deleteAnswer( $id );

			while ( $i < $n ) {
			 	$poolID = $pool[$i];

				//insert child
				$data = array(
					'parent_id'     => $id,
					'branch_id'     => $branch_id,
					'program_id'    => $program_id,
					'IdProgramScheme' => $IdProgramScheme,
					'semester_id'   => $semester_id,
					'status'        => $formData['status'],
					'dateUpd'       => $date,
					'updUser'       => $idUpd,
					'question_pool' => $poolID,
				);
				$i++;
//				$sectiontagging->addData( $data );

			}

			//redirect
			$this->_redirect( $this->view->url( array( 'module'=>'survey-bank', 'controller'=>'sectiontagging', 'action'=>'index' ), 'default', true ) );

		}
	}


	public function deleteAction() {
		$id = $this->_getParam( 'id', 0 );

		if ( $id>0 ) {
			$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
			$sectiontagging->deleteData( $id ); //delete parent
			$sectiontagging->deleteItem( $id ); //delete question pool
		}

		$this->_redirect( $this->view->url( array( 'module'=>'survey-bank', 'controller'=>'sectiontagging', 'action'=>'index' ), 'default', true ) );

	}

	public function getListPoolAjaxAction() {

		$id = $this->_getParam( 'id', 0 );

		//if ($this->getRequest()->isXmlHttpRequest()) {
		$this->_helper->layout->disableLayout();
		//}

		$ajaxContext = $this->_helper->getHelper( 'AjaxContext' );
		$ajaxContext->addActionContext( 'view', 'html' );
		$ajaxContext->initContext();

		$SectionDB = new App_Model_Question_DbTable_SectionTagging();
		$publish = $SectionDB->getListSemesterByPool( $id );

		$ajaxContext->addActionContext( 'view', 'html' )
		->addActionContext( 'form', 'html' )
		->addActionContext( 'process', 'json' )
		->initContext();

		$json = Zend_Json::encode( $publish );

		echo $json;
		exit();
	}

	public function copyDataAjaxAction() {

		$from_intake_id = $this->_getParam( 'from_intake_id', null );
		$from_type_id = $this->_getParam( 'from_type_id', null );
		$from_fs_id = $this->_getParam( 'from_fs_id', null );

		$this->_helper->layout->disableLayout();

		if ( $this->getRequest()->isPost() ) {

			$formData = $this->getRequest()->getPost();

			$idMain = $formData['from_intake_id'];
			$mainSetID = $formData['sem'];


			echo "<pre>";
			print_r( $formData );
			echo "</pre>";
			
			exit;



			//copy evaluation
			$SectionDB = new App_Model_Question_DbTable_SectionTagging();
			$chapterDB = new App_Model_Question_DbTable_Chapter();
			$rubricDB = new App_Model_Question_DbTable_Rubric();
			$questionDB = new App_Model_Question_DbTable_QuestionTagging();
			$radioDB = new App_Model_Question_DbTable_RadioOptionList();

			//list section
			$listSection = $chapterDB->getSectionByPool( $idMain );

			$auth = Zend_Auth::getInstance();
			$idUpd = $auth->getIdentity()->id;


			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();

			try{

				$dataArray = array();
				$a = 0;

				foreach ( $listSection as $data ) {
					$dataArray[$a] = $data;

					$data3['main_set_id']= $mainSetID;
					$data3['pool_id']= $data['questionpool'];
					$data3['name']= $data['chaptername'];
					$data3['status']= $data['status'];
					$data3['dateUpd']= date( 'Y-m-d H:i:s' );
					$data3['updUser']= $idUpd;
					$data3['question_type']= $data['question_type_id'];

					//insert section
					$cp_fs_id = $chapterDB->insert( $data3 );

					//list question
					$listQuestion = $SectionDB->getListQuestion( $idMain, $data['idChapter'] );
					$dataArray[$a]['question'] = $listQuestion;

					$b=0;
					foreach ( $listQuestion as $lq ) {

						$data2['main_set_id']= $mainSetID;
						$data2['section_id']= $cp_fs_id;
						$data2['IdQuestion']= $lq['IdQuestion'];
						$data2['updUser']= $idUpd;
						$data2['dateUpd']= date( 'Y-m-d H:i:s' );

						//insert rubric
						$questionID = $questionDB->insert( $data2 );

						//list radio option
						$listOption = $radioDB->getData( $idMain, $data['idChapter'], $lq['id_questiontagging'] );
						$dataArray[$a]['question'][$b]['option'] = $listOption;

						if ( $listOption ) {

							foreach ( $listOption as $lo ) {

								$data4['question_tagging_id']= $questionID;
								$data4['main_set_id']= $mainSetID;
								$data4['section_id']= $cp_fs_id;
								$data4['answer']= $lo['answer'];
								$data4['order']= 0;
								$data4['DatetimeInsert']= date( 'Y-m-d H:i:s' );
								$data4['InsertBy']= $idUpd;

								//insert radio
								$radioDB->insert( $data4 );
							}

						}
						$b++;

					}


					//list answer
					$setAnswer = $rubricDB->getAnswerByIDAll( $data['idChapter'] );
					//$dataArray[$a]['rubric'] = $setAnswer;

					foreach ( $setAnswer as $sa ) {
						$data1['id_Section']= $cp_fs_id;
						$data1['ValueNumber']= $sa['ValueNumber1'];
						$data1['description']= $sa['description1'];
						$data1['weightage']= $sa['weightage1'];
						$data1['dateUpd']= date( 'Y-m-d H:i:s' );
						$data1['updUser']= $idUpd;

						//insert rubric
						$rubricDB->insert( $data1 );

					}






					$a++;
				}

				$db->commit();

			}catch ( Exception $e ) {
				$db->rollBack();
				echo $e->getMessage();
				echo "<pre>";
				var_dump( $e->getTrace() );
				echo "</pre>";
				echo "<br />";

				throw $e;

			}

			//echo "<pre>";
			//print_r( $dataArray );


			//  $publish = $SectionDB->getListSemesterByPool($idMain);


			//redirect
			$this->_redirect( $this->view->url( array( 'module'=>'survey-bank', 'controller'=>'sectiontagging', 'action'=>'views', 'id'=>$mainSetID ), 'default', true ) );

			$ajaxContext = $this->_helper->getHelper( 'AjaxContext' );
			$ajaxContext->addActionContext( 'view', 'html' );
			$ajaxContext->initContext();

			$ajaxContext->addActionContext( 'view', 'html' )
			->addActionContext( 'form', 'html' )
			->addActionContext( 'process', 'json' )
			->initContext();

			$json = Zend_Json::encode( $result );

			echo $json;
			exit();
		}
	}

	function getProgramSchemeByProgramAction() {
		//$program_id = $this->_request->_getParam('program_id');
		$post_data['program_id'] = $this->getRequest()->getPost();
		$ProgramScheme = new GeneralSetup_Model_DbTable_Programscheme();

		$program_schemes = $ProgramScheme->getProgSchemeByProgram($post_data['program_id']);
		$prog_schemes = array();
		
		foreach($program_schemes as $program_scheme) {
			$prog_schemes[] = array( 'id'=> $program_scheme['IdProgramScheme'], 
									'value' => $program_scheme['ProgramMode'] . ', ' . 
									$program_scheme['StudyMode'] . ', ' . 
									$program_scheme['ProgramType']
									);

		}
		echo json_encode($prog_schemes);
		exit;
	}
	
	public function copyAction()
	{
		$id = $this->_getParam('id', 0);
		
		//title
    	$this->view->title= $this->view->translate("Set Form")." : ".$this->view->translate("Copy");
    	
    	// Semester Data
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$this->view->semesterlist = $semesterDb->getListSemester();

		$oBranch = new App_Model_General_DbTable_Branch();
		$branch = $oBranch->getData();
		$this->view->branch = $branch;

		$oProgram = new App_Model_Record_DbTable_Program();
		$program  = $oProgram->getData();
		$this->view->program = $program;

    	$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
    	$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
    	
		$info = $sectiontagging->getDataByID( $id );
		$this->view->info = $info;
		
		$infoDetail = $sectiontagging->getDataByParentID( $id );
		
		$SectionData = $sectiontagging->getDataByID($id);
		$this->view->SectionData= $SectionData;
		
		//display question pool without checked
    	$SectionData2 = $sectiontagging->getAnswerByIDAll($id); 
    	
    	$arrayPool = array();
    	$a = 0;
    	foreach($SectionData2 as $secData){
    		$arrayPool[$a] = $secData;
    		
    		//get list of question based on main set id
    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id']);
    		$previewStatus = count($setQuestion);
    		$arrayPool[$a]['preview'] = $previewStatus; 
    		$a++;
    	}
    	$this->view->SectionData2 = $arrayPool;
    	
		
		$auth = Zend_Auth::getInstance(); 
		
		if ($this->getRequest()->isPost()) 
		{	
			$formData = $this->getRequest()->getPost();
			
			$db = getDB();
			
			$infoChecking = $sectiontagging->getSetupTaggingDetail( $formData['branch_id'] , $formData['semester_id'],$formData['program_id'],$formData['IdProgramScheme']);
			
			if($infoChecking){
				$this->gobjsessionsis->flash = array('type'=>'error','message'=>'Duplicate Setup');
			}else{

				//structure
				$data = array(
								'form_name'				=> $formData['form_name'], 
								'parent_id'				=> 0, 
								'branch_id'				=> $formData['branch_id'], 
								'program_id'			=> $formData['program_id'], 
								'semester_id'			=> $formData['semester_id'], 
								'IdProgramScheme'		=> $formData['IdProgramScheme'], 
								'question_pool'			=> '', 
								'section_instructions'	=> $info['section_instructions'], 
								'status'				=> 1, 
								'dateUpd'				=> new Zend_Db_Expr('NOW()'),
								'updUser'				=> $auth->getIdentity()->iduser
							);
	
				$db->insert('q020_sectiontagging', $data);
				$parent_id = $db->lastInsertId();
	
				//detail
				foreach ( $infoDetail as $list )
				{
					$data = array(
									'form_name'				=> NULL, 
									'parent_id'				=> $parent_id, 
									'branch_id'				=> $formData['branch_id'], 
									'program_id'			=> $formData['program_id'], 
									'semester_id'			=> $formData['semester_id'], 
									'IdProgramScheme'		=> $formData['IdProgramScheme'], 
									'question_pool'			=> $list['question_pool'],
									'section_instructions'	=> $list['section_instructions'], 
									'status'				=> 1, 
									'dateUpd'				=> new Zend_Db_Expr('NOW()'),
									'updUser'				=> $auth->getIdentity()->iduser
								);
				
					$db->insert('q020_sectiontagging', $data);
					$sectiontagging_id = $db->lastInsertId();
	
					//chapter
					$levelselect = $db->select()->from(array('a'=>'q002_chapter'))->where('main_set_id = ?', $list['id']);
					$levelList = $db->fetchAll($levelselect);
					
					foreach ( $levelList as $level )
					{
						$data = array(
									'main_set_id'				=>  $sectiontagging_id, 
									'pool_id'					=>  $level['pool_id'], 
									'registration_item_id'		=> $level['registration_item_id'], 
									'name'						=> $level['name'], 
									'subsection_instruction'	=> $level['subsection_instruction'], 
									'status'				=> $level['status'], 
									'order'					=> $level['order'],
									'question_type'			=> $level['question_type'], 
									'dateUpd'				=> new Zend_Db_Expr('NOW()'),
									'updUser'				=> $auth->getIdentity()->iduser
									);
	
						$db->insert('q002_chapter', $data);
						$chapter_id = $db->lastInsertId();
						
						//rubric
						$rubricselect = $db->select()->from(array('a'=>'q004_rubricdetails'))
						->where('id_Section = ?', $level['id']);
						$rubricList = $db->fetchAll($rubricselect);
						
						foreach ( $rubricList as $rub )
						{
							$q = array(
								'id_Section'			=>  $chapter_id, 
								'ValueNumber'			=>  $rub['ValueNumber'], 
								'description'			=>  $rub['description'], 
								'weightage'				=> $rub['weightage'], 
								'updUser'				=> $auth->getIdentity()->iduser,
								'dateUpd'				=> new Zend_Db_Expr('NOW()')
								);
										
							$rubricDet = new App_Model_Question_DbTable_Rubric();
							$rubricDet->addData($q);
						}
						
						//question tagging
						$qtselect = $db->select()->from(array('a'=>'q003_questiontagging'))
						->where('main_set_id = ?', $level['main_set_id'])
						->where('section_id = ?', $level['id']);
						$questionTaggingList = $db->fetchAll($qtselect);
						
						foreach ( $questionTaggingList as $tag )
						{
							$data = array(
										'main_set_id'			=>  $sectiontagging_id, 
										'section_id'			=>  $chapter_id, 
										'order'					=> $tag['order'], 
										'IdQuestion'			=> $tag['IdQuestion'], 
										'dateUpd'				=> new Zend_Db_Expr('NOW()'),
										'updUser'				=> $auth->getIdentity()->iduser
										);
		
							$db->insert('q003_questiontagging', $data);
							
						}
					}
				}
	
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Data has been saved');
				$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'sectiontagging', 'action'=>'view','id'=>$parent_id),'default',true));
			}
		}

		//views
		$this->view->info = $info;
	}


}
