<?php

Class SurveyBank_PublishController extends Base_Base {
	
	private $_html;
	
	public function viewAction(){

    	$this->view->title = "Manage Publish";    	
    	
    	$oBranch = new App_Model_General_DbTable_Branch();
		$this->view->branchList = $oBranch->getData();
		
		// Semester Data
		$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$this->view->semesterlist = $semesterDb->getListSemester();
		
		// program data
		$programDB = new App_Model_Record_DbTable_Program();
		$this->view->programs = $programDB->getData();
		
		$publishcondition = new App_Model_Question_DbTable_ConditionPublish();
		$publishDB = new App_Model_Question_DbTable_ManagePublish();
		
    	$ConditionDB = $publishcondition->getData(); 
    	$this->view->ConditionDB = $ConditionDB;
		$dataArray = array();
		if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
			
			$this->view->formData = $formData;
    	
			$SectionDB = new App_Model_Question_DbTable_SectionTagging();
			$publish = $SectionDB->getPaginateData($formData);
			$dataArray = $publish;
			
			foreach($publish as $key=>$pb){
				
				$publishData = $publishDB->getPublishData($pb['id']);
				$dataArray[$key]['publishdata'] = $publishData;
//				
			}
			
		
		}else{
			$SectionDB = new App_Model_Question_DbTable_SectionTagging();
			$publish = $SectionDB->getPaginateData();
			$dataArray = $publish;
			
			foreach($publish as $key=>$pb){
				
				$publishData = $publishDB->getPublishData($pb['id']);
				$dataArray[$key]['publishdata'] = $publishData;
//				
			}
		}
		
		$this->view->publish= $dataArray;

	}
	
	public function managepublishAction() {

	    //title
		$this->view->title = "Publish"; 
        
        $id = $this->_getParam('id',0);
        $this->view->id = $id;
        $main_set_id = $this->_getParam('main_set_id',0);
        $this->view->main_set_id = $id;
        //TODO: scratch head and wonder why i bothered getting main_set_id in the first place. (clue: quick hack before demo?) do next week.

		//display branch,program,semester,status
    	$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
    	$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
    	
    	$SectionData = $sectiontagging->getDataByID($id);
    	$this->view->SectionData = $SectionData;
    	
		//display question pool without checked
    	$SectionData2 = $sectiontagging->getAnswerByIDAll($id); 
    	
    	
    	$arrayPool = array();
    	$a = 0;
    	foreach($SectionData2 as $secData){
    		$arrayPool[$a] = $secData;
    		
    		//get list of question based on main set id
    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id']);
    		$previewStatus = count($setQuestion);
    		$arrayPool[$a]['preview'] = $previewStatus; 
    		$a++;
    	}
    	$this->view->SectionData2 = $arrayPool;
    	
    	$publishcondition = new App_Model_Question_DbTable_ConditionPublish();
    	$ConditionDB = $publishcondition->getData(); 
    	$this->view->ConditionDB = $ConditionDB;
    	
    	$publish = new App_Model_Question_DbTable_ManagePublish();	
    	$PublishDB = $publish->getPublishTagging($id); 
    	$this->view->PublishDB = $PublishDB;

		$publishData = $publish->getPublishData($id);
    	$publish_list_id = array();
    	foreach($publishData as $x){
    		if($x['enable']==1){
    			array_push($publish_list_id,$x['id_Publish']);
    		}
    	}
    	$this->view->publish_list_id = $publish_list_id;
    	
    	
    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			echo "<pre>";
			print_r($formData);

			$auth = Zend_Auth::getInstance();
			$idUpd = $auth->getIdentity()->id;
			
			$publishDB = new App_Model_Question_DbTable_Publish();
		    $publishDetailsDB = new App_Model_Question_DbTable_PublishDetails();
		    $publishHistoryDB = new App_Model_Question_DbTable_PublishHistory();

		    $question_pool = $formData['id_PoolPublish'];
		    $publishDB->updateData(array('enable'=>0), array('main_set_id = ?'=>$id));
		    
		    if($formData['condition']){
		    	$condition = $formData['condition'];
		    }else{
		    	$condition = array(0);
		    }
		    
		    $publishDB->deletePublishData($id);
		    
//		    $publishexistNoCondition = $publishDB->getData($id,$condition);
		    
		    foreach($condition as $cond){
				$dataArray = array(
			    				'main_set_id'   => $id,
		        				'start_date'    => date('Y-m-d', strtotime($formData['start_date'])),
			    				'end_date'      => date('Y-m-d', strtotime($formData['end_date'])),
		     					'condition'     => $cond,
								'enable'		=> 1,
								'dateUpd'       => date('Y-m-d H:i:s'),
								'updUser'       => $idUpd
				 			  );
				$id_Publish = $publishDB->addData($dataArray);

				foreach($question_pool as $pool_id) {
					$data = array(
			    		'id_Publish'    => $id_Publish,
				    	'main_set_id'   => $id,
				    	'id_PoolPublish'=> $pool_id,
					);
	                $publishDetailsDB->addData($data);
				}

				$dataHistory = array(
									'id_Publish' 	=> $id_Publish,
				    				'main_set_id'   => $id,
			        				'start_date'    => date('Y-m-d', strtotime($formData['start_date'])),
				    				'end_date'      => date('Y-m-d', strtotime($formData['end_date'])),
			     					'condition'     => $cond,
									'enable'		=> 1,
									'dateUpd'       => date('Y-m-d H:i:s'),
									'updUser'       => $idUpd
					 			  );
					 			  
				$publishHistoryDB->addData($dataHistory);
		    }
			
			//redirect	
			$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'publish', 'action'=>'view'),'default',true));
    	}
	}
	
	public function updatePublishAction() {

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//echo "<pre>";print_r($formData);exit;
			
			$sectionTagging = $formData['sectiontagging_id']; 
			
			$n = count($sectionTagging);
			
			while($i<$n){
				
				$id = $sectionTagging[$id];

				$auth = Zend_Auth::getInstance();
				$idUpd = $auth->getIdentity()->id;
				
				$publishDB = new App_Model_Question_DbTable_Publish();
			    $publishDetailsDB = new App_Model_Question_DbTable_PublishDetails();
	
				$condition = $formData['condition'];
			    $question_pool = $formData['id_PoolPublish'];
			    $publishDB->updateData(array('enable'=>0), array('main_set_id = ?'=>$id));
			    foreach($condition as $condition_id)
	        	{
					$publishexist = $publishDB->getData($id,$condition_id);
		        	if($publishexist){	//update data if exist
	        			
		        		$dataArray = array(
											'start_date'	=> date('Y-m-d', strtotime($formData['start_date'])),
											'end_date'      => date('Y-m-d', strtotime($formData['end_date'])),
											'enable'		=> 1,
											'dateUpd'       => date('Y-m-d H:i:s'),
											'updUser'       => $idUpd
										  );
						$publishDB->updateData($dataArray, array('id_Publish = ?'=>$publishexist[0]['id_Publish']) );
						
					}else{				//insert if not exist
						
						$dataArray = array(
					    				'main_set_id'   => $id,
				        				'start_date'    => date('Y-m-d', strtotime($formData['start_date'])),
					    				'end_date'      => date('Y-m-d', strtotime($formData['end_date'])),
				     					'condition'     => $condition_id,
										'enable'		=> 1,
										'dateUpd'       => date('Y-m-d H:i:s'),
										'updUser'       => $idUpd
						 			  );
						$id_Publish = $publishDB->addData($dataArray);
						
						foreach($question_pool as $pool_id) {
							$data = array(
					    		'id_Publish'    => $id_Publish,
						    	'main_set_id'   => $id,
						    	'id_PoolPublish'=> $pool_id,
							);
			                $publishDetailsDB->addData($data);
						}
						
					}
					
				} //end foreach
				$i++;
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Evaluation published successfully"));

			//redirect	
			$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'publish', 'action'=>'view'),'default',true));
    	}
	}
	
	public function previewEvaluationAction($_poolid=0,$_main_set_id=0,$_semester_id=0){
		
		$this->_helper->layout()->disableLayout();
		
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
//		$this->_helper->layout->setLayout('preview');
                
        $QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
        
        $poolid 	 = $this->_getParam('poolid',0);		
        $main_set_id = $this->_getParam('main_set_id',0);
        $semester_id = $this->_getParam('semester_id',0);
        
        if($poolid==0){ $poolid = $_poolid; }
        if($main_set_id==0){ $main_set_id = $_main_set_id; }
        if($semester_id==0){ $semester_id = $_semester_id; }
        
        $current_semester = $semester_id;
        
    	$ldtsystemDate = date('Y-m-d H:i:s');
    	
		$auth = Zend_Auth::getInstance(); 
		$user_id = $auth->getIdentity()->id; 
		
		//$userDB = new SystemSetup_Model_DbTable_User();
		//$uData = $userDB->getData($user_id);
    	
    	//
    	$poolList = $QuestionSurvey->getOnePoolBySem($poolid,$current_semester,$main_set_id);
    	
    	$this->view->title = $title = $poolList['poolname'];
    	$this->view->poolList = $poolList;
    	$poolList = $QuestionSurvey->getPoolBySet($main_set_id,$poolid);
//    	$poolList = $QuestionSurvey->getPoolBySem($poolid,$current_semester);
    	$dataArray = array();

    	
    	$i=0;
    	foreach($poolList as $pool){
    		
    		$dataArray[$i] = $pool;
    		
    		if($pool['question_type']==3){	//is rubric
	   			$rubricList = $QuestionSurvey->getRubricDetails($pool['chapterid']);
	   			
	   			if($rubricList){			//if rubric value exist
    				$dataArray[$i]['rubric'] = $rubricList;
	   			}    		
    		}
    		
    		$questionList = $QuestionSurvey->getQuestionByChapter($pool['chapterid']);
    		
    		//get list of question
    		$dataArray[$i]['question'] = $questionList;
    		$i++;
    		 
    	}

    	
		$this->view->lobjquestion = $QuestionSurvey;
    	$this->view->question = $dataArray;
    	//echo $this->generateSurvey($title, $dataArray);
    	
    	//echo '<p><hr/></p>';
	}
	
	public function viewPdfAction(){
		
		//$this->previewEvaluationAction(1,72,12);
    	//$this->render('preview-evaluation',$data);
    	
	}
	
	public function generateSurvey($title,$data) {
		
		$html  = "";
		$html .= "<div>";
		$html .= "<h1>$title</h1>";
		
		foreach($data as $x){
			echo $x['chaptername']."<br/>";
		}
		
		$html .= "</div>";
		
		return $html;
		
	}
	
	public function generateChapter($data){
		foreach ($data as $x){
			$this->_html .= "<table>";
			$this->_html .= "<table>";
			$this->_html .= "</table>";
		}
	}
	
	public function toPdfAction(){
		
		$this->_helper->layout->disableLayout();
		
		$this->previewEvaluationAction(1,72,12);
    	$html = $this->render('preview-evaluation',$data);
    	
		require_once 'MPDF57/mpdf.php';
		
		$path -
		$html_template_path = PUBLIC_PATH.'/document/transcript/survey.html';
					
		$html = file_get_contents($html_template_path);
		
		$mpdf = new mPDF();
		$mpdf->WriteHTML($html);
		
		$mpdf->Output();
		
		//file_put_contents(PUBLIC_PATH.'/document/transcript/survey.pdf', $mpdf->Output('', 'S'));
					
		
	}
}