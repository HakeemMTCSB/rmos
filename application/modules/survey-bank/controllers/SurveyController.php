<?php

class SurveyBank_SurveyController extends Base_Base {
	
	private $Common;

	public function init()
	{
		//models
		$this->lobjquestion = new App_Model_Survey_DbTable_QuestionSurvey();
		$this->lobjsemester = new GeneralSetup_Model_DbTable_Semester();
		
		//forms
		//$this->lobjStudentApplicationForm = new App_Form_Manualapplication();
		
	}
	
	public function indexAction(){
		//title
    	$this->view->title="Online Survey";
    	
    	$poolList = $this->lobjquestion->getPool();
    	$this->view->pool = $poolList;
    	
    	//get current semester
    	$currentSem = $this->lobjsemester->currentSemester();
    	$curSem = $currentSem['id'];
    	if(empty($curSem)) {
    		$curSem = 1;
    	}
    	//get registered course
    	$registerCourse = $this->lobjquestion->getRegisteredCourse($student_id,$curSem);
    	$this->view->registerCourse = $registerCourse;
    	
    	//check course tagging
    	$courseTagging = $this->lobjquestion->getGroupTagging($student_id,$intakeID);
    	$this->view->taggingGroup = $courseTagging;
    	
//    	echo "<pre>";
//    	print_r($poolList);
//    	echo "</pre>";
    	
	}
	public function surveyAction(){
		//title
    	$ldtsystemDate = date ( 'Y-m-d H:i:s' );
    	
		$auth = Zend_Auth::getInstance(); 
		$user_id = $auth->getIdentity()->id; 
		
		$userDB = new SystemSetup_Model_DbTable_User();
		$uData = $userDB->getData($user_id);
    	$student_id = $uData['studentID'];
    	$this->view->student_id = $student_id;
    	
		
		$poolid = $this->_getParam('id', 0);
		$this->view->poolid = $poolid;
		
		$courseid = $this->_getParam('courseid', 0);
		$this->view->courseid = $courseid;
		
		//get current semester
    	$currentSem = $this->lobjsemester->currentSemester();
    	$curSem = $currentSem['id'];
    	
    	$dataAnswerSet['student_id']=$student_id;
    	$dataAnswerSet['pool_id']=$poolid;
		$dataAnswerSet['updDate']=$ldtsystemDate;
		
    	if($courseid){
			$registerCourse = $this->lobjquestion->getRegisteredCourse($student_id,$curSem,$courseid);
	    	$this->view->registerCourseInfo = $registerCourse;
	    	
			$dataAnswerSet['coursegroup_id']=$registerCourse['IdCourseTaggingGroup'];
			
    		$checkSetAnswer = $this->lobjquestion->getSetAnswer2($student_id,$poolid,$registerCourse['IdCourseTaggingGroup']);
			
    		if (empty($checkSetAnswer)){
				//insert set answer student
				$addIdSet = $this->lobjquestion->addData($dataAnswerSet,"q006_answersetstudent","id");
			}
    	}else{
    		$registerCourse = $this->lobjquestion->getRegisteredCourse($student_id,$curSem);
	    	$this->view->registerCourseInfo = $registerCourse;
	    	
			$dataAnswerSet['coursegroup_id']=0;
			
    		$checkSetAnswer2 = $this->lobjquestion->getSetAnswer2($student_id,$poolid);
//			print_r($checkSetAnswer2);
			if (empty($checkSetAnswer2)){
				
				//insert set answer student
				$addIdSet = $this->lobjquestion->addData($dataAnswerSet,"q006_answersetstudent","id");
			}
			
    	}
    	
    	
    	
    	$poolList = $this->lobjquestion->getPool($poolid);
    	$this->view->pool = $poolList;

    	$this->view->title=$poolList[0]['poolname'];
    	
    	$dataArray = array();
    	
    	$i=0;
    	
    	foreach($poolList as $pool){
    		
    		$dataArray[$i] = $pool;
    		
    		if($pool['question_type']==3){//is rubric
    			$rubricList = $this->lobjquestion->getRubricValue($pool['chapterid']);
    			$dataArray[$i]['rubric'] = $rubricList;
    		}
    		//get coursegroup
    		$getCourseGroup = $this->lobjquestion->getCourseGroup($curSem,$courseid);

			$chapterid = ($pool['chapterid']?$pool['chapterid']:0);
    		$questionList = $this->lobjquestion->getQuestion($poolid,$chapterid);
    		
    		
    		//get list of question
    		$dataArray[$i]['question'] = $questionList;
    		
    		$i++;
    		
    	
    	}

//    	echo "<pre>";
//    	print_r($dataArray);
//    	echo "</pre>";
    	
    	$this->view->question = $dataArray;
    	
    	
	}
	
public function savesurveyAction($id=null){
    	
    	// check is AJAX request or not
     	/*if (!$this->getRequest() -> isXmlHttpRequest()) {
        	$this->getResponse() -> setHttpResponseCode(404)
                              -> sendHeaders();
         	$this->renderScript('empty.phtml');
         	return false;
     	}*/
    	
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	
		if ($this->_request->isPost())
		{ 
			$lobjFormData = $this->_request->getPost();
			
			$ldtsystemDate = date ( 'Y-m-d H:i:s' );
			
			$courseid = $lobjFormData['courseid'];
			$poolid = $lobjFormData['poolid'];
			$submitSurvey = $lobjFormData['submit'];
			
			//save answer set student
			$dataAnswerSet['student_id']=$lobjFormData['student_id'];
			$dataAnswerSet['coursegroup_id']=$lobjFormData['coursegroup_id'];
			$dataAnswerSet['updDate']=$ldtsystemDate;
			
			$checkSetAnswer = $this->lobjquestion->getSetAnswer2($lobjFormData['student_id'],$poolid,$lobjFormData['coursegroup_id']);
//			echo "<pre>";
//			print_r($checkSetAnswer);exit;
			if (!empty($checkSetAnswer)){
				$addIdSet = $checkSetAnswer[0]['id'];
			}else{
				$addIdSet = $this->lobjquestion->addData($dataAnswerSet,"q006_answersetstudent","id");
			}
			
			$rowCount = count($lobjFormData['chapterid']);		
			for($a=0;$a<$rowCount;$a++){		
					
				$chapterid = $lobjFormData['chapterid'][$a];
				$questiontype = $lobjFormData['questionType'][$a];
				$numQuestion = $lobjFormData['numQuestion'][$a];
				
				for($b=1;$b<$numQuestion;$b++){
					$valueName = 'val_'.$chapterid.'_'.$b;
					$quesName = 'qs_'.$chapterid.'_'.$b;

					if($lobjFormData[$valueName]){
						$dataAnswer['setanswer_id']=$addIdSet;
						$dataAnswer['student_id']=$lobjFormData['student_id'];
						$dataAnswer['coursegroup_id']=$lobjFormData['coursegroup_id'];
						$dataAnswer['question_id']= $lobjFormData[$quesName];
						$dataAnswerSet['updDate']=$ldtsystemDate;
						
						if($questiontype == 2){//essay
							$dataAnswer['valueText']=$lobjFormData[$valueName];
							$dataAnswer['value']="";
						}else{
							$dataAnswer['value']=$lobjFormData[$valueName];
							$dataAnswer['valueText']="";
						}
						
//						echo "<pre>";
//						print_r($dataAnswer);
//						echo "</pre>";
						
						//checking ade ke tak
						$checkAnswer = $this->lobjquestion->getSetAnswer($lobjFormData['student_id'],$poolid,$lobjFormData['coursegroup_id'],$lobjFormData[$quesName]);
						
//						print_r($checkAnswer);exit;
						
						if (empty($checkAnswer)){
							//add
							$addAnswer = $this->lobjquestion->addData($dataAnswer,"q006_answerstudent","id");
							
						}else{
							//update
							$idAnswer = $checkAnswer['idAnswer'];
							$updateAnswer = $this->lobjquestion->updateDataAnswer($dataAnswer,$idAnswer);
							
							
						}
					}
					
				}
			}
//			exit;
			if($submitSurvey == 1){
				$dataAnswerSetStatus['status']=1;
			}else{
				$dataAnswerSetStatus['status']=0;
			}
			$updateStatus = $this->lobjquestion->updateSetAnswer($dataAnswerSetStatus,$addIdSet);
		}
		
//		exit;
		$data = 1;
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($data);
		
		$this->view->json = $json;
		exit;
		
		//redirect 
		    //$this->_redirect($this->view->url(array('module'=>'default','controller'=>'survey', 'action'=>'survey','id'=>$poolid,'courseid'=>$courseid),'default',true));	
		exit;

    }
    
public function submitsurveyAction($id=null){
    	
    	$studentid = $this->_getParam('studentid', 0);
    	$coursegroupid = $this->_getParam('coursegroupid', 0);
    	
    	// check is AJAX request or not
     	/*if (!$this->getRequest() -> isXmlHttpRequest()) {
        	$this->getResponse() -> setHttpResponseCode(404)
                              -> sendHeaders();
         	$this->renderScript('empty.phtml');
         	return false;
     	}*/
    	
    	
    	
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout(); 
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
		$checkSetAnswer = $this->lobjquestion->getSetAnswer($studentid,$coursegroupid);
			
		$addIdSet = $checkSetAnswer[0]['setanswer_id'];
		$dataAnswerSet['status']=1;
			
	  	$updateStatus = $this->lobjquestion->updateSetAnswer($dataAnswerSet,$addIdSet);
		
		if($updateStatus){
			$data = 1;
		}else{
			$data = 0;
		}
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($data);
		
		$this->view->json = $json;
		exit;

    }
    
    //generate course_group_mapping
    public function setstudentgroupmappingAction(){
    	
    	$sem = 4;//intake
    	$course = 32;//tamadun islam
    	$idTagGroup = 21;
    	
    	exit;
    	
    	//$listStudent = $this->lobjquestion->getListofStudentSemSubject($sem,$course);
    	
    	$excelStud = array('D0011060053F',
'D0011060054F',
'D0011060055F',
'D0011060056F',
'D0011060057F',
'D0011060059F',
'D0011060060F',
'D0011060061F',
'D0011060062F',
'D0011060063F',
'D0011060064F',
'D0011060065F',
'D0011060052F',
'D0011060067F',
'D0011060069F',
'D0011060070F',
'D0011060071F',
'D0011060050F',
'D0011060073F',
'D0011060074F',
'D0011060075F',
'D0011060076F',
'D0011060077F',
'D0011060078F',
'D0011060080F',
'D0011060081F',
'D0011060082F',
'D0011060085F',
'D0011060086F',
'D0011060087F',
'D0011060088F',
'D0011060089F',
'D0011060001F',
'D0011060091F',
'D0011060092F',
'D0011060093F',
'D0011060095F',
'D0011060096F',
'D0011060097F',
'D0011060098F',
'D0011060099F',
'D0011060100F',
'D0011060101F',
'D0011060046F',
'D0011060103F',
'D0011060104F',
'D0011060047F'

    	

    	
    	);
    	
    	foreach($excelStud as $key){
    		echo $key;
    		
    		$listStudent = $this->lobjquestion->getListofStudentSemSubject($key,$sem,$course);
    		
    		//patutnya kena check penah ada ke tak
    		
    	if($listStudent){
	    	$dataAdd = array(
		    	'IdCourseTaggingGroup'=>$idTagGroup, // PK  tbl_course_tagging_group
		    	'IdStudent'=>$listStudent['student_id'],
		    	'IdCourseRegistration'=>$listStudent['idCourseRegis'],
	    	
	    	);
	    	
	    	echo "<pre>";
	    	print_r($listStudent);
	    	echo "</pre>";
	    	
	    	$this->lobjquestion->addData($dataAdd,'tbl_course_group_student_mapping','Id');
	    		
	    	}
    	}
    	
    	
    	exit;
    	
    	
    	
    }
	
}
?>