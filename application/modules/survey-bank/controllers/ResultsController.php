<?php

Class SurveyBank_ResultsController extends Base_Base {
	
	
	
	public function indexAction(){

    	$this->view->title = "Manage Publish Section Tagging";    	
    	
    	//get published sets for us to view results
		$SectionDB = new App_Model_Question_DbTable_SectionTagging();
		$this->view->publish= $SectionDB->getTaggingData();
		$managed_publish = new App_Model_Question_DbTable_ManagePublish();
		foreach($this->view->publish as $key => $publish) {
			$this->view->publish[$key]['evaluation_components'] = $managed_publish->getInfo($publish["id"]);
		}

	}

	public function answerListingAction() {
		//title
		$this->view->title = "View Answer"; 
        
        $id = $this->_getParam('id',0);

		//display branch,program,semester,status
    	$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
    	$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
    	
    	$SectionData = $sectiontagging->getDataByID($id);
    	$this->view->SectionData = $SectionData;
    	
		//display question pool without checked
    	$SectionWithPool = $sectiontagging->getAnswerByID($id);

		$lec = '';
		$sub = '';
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$lec = $formData['lecturer'];
			$sub = $formData['subject'];
		}

		$this->view->lec = $lec;
		$this->view->sub = $sub;

    	$arrayPool = array();
    	foreach($SectionWithPool as $key => $secData){
    		$arrayPool[$secData['question_pool']] = $secData;
    		
    		//get list lecturer & subject
    		$listLecturer = $QuestionSurvey->getListLecturer($secData['branch_id'],$secData['semester_id'], $lec, $sub);
    		$arrayPool[$secData['question_pool']]['lecturer'] = $listLecturer;
    		
    		$lecturerSearch = $QuestionSurvey->searchList($secData['branch_id'],$secData['semester_id']);
    		$subSearch = $QuestionSurvey->searchList($secData['branch_id'],$secData['semester_id'], 1);
			$arrayPool[$secData['question_pool']]['lecturerSearch'] = $lecturerSearch;
			$arrayPool[$secData['question_pool']]['subSearch'] = $subSearch;

    		//get list of question based on main set id
    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id']);
    		$question_count = count($setQuestion);

    		//get answers
    		$StudentAnswer = new App_Model_Question_DbTable_StudentAnswer();
    		$setAnswer  = $StudentAnswer->setAnswerStudent($secData['id'],$secData['question_pool'],$secData['semester_id'], $lec, $sub);

    		$arrayPool[$secData['question_pool']]['answers'] = $setAnswer;

    	}
    	$this->view->section_pools = $arrayPool;
	}

    public function viewAnswerAction() {
        

        $student_id = $this->_getParam('student_id', 0);
        $idSet= $this->_getParam('idSet', 0);
        $poolid 	 = $this->_getParam('pool_id',0);
        $this->view->id = $main_set_id = $this->_getParam('id',0);
        $IdLecturer  = $this->_getParam('IdLecturer',0);
        $IdSubject  = $this->_getParam('IdSubject',0);
        
 		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
        $this->view->data = $QuestionSurvey->getSetAnswerMain($idSet);
        
    	if(!empty($IdSubject)) {
            //get subject information
            $Subject = new GeneralSetup_Model_DbTable_Subjectmaster();
            $this->view->subject_info = $Subject->getData($IdSubject);

        }

        if(!empty($IdLecturer)) {
            //get lecturer information
            $Lecturer = new GeneralSetup_Model_DbTable_Staffmaster();
            $this->view->lecturer_info = $Lecturer->fnviewStaffDetails($IdLecturer);
        }

       
        
        
        $ldtsystemDate = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance(); 
        $user_id = $auth->getIdentity()->id; 

		//$userDB = new SystemSetup_Model_DbTable_User();
		//$uData = $userDB->getData($user_id);

        $poolList = $QuestionSurvey->getPoolBySet($main_set_id,$poolid);
		$this->view->poolList = $title = $poolList[0];

        $this->view->title = $title = $poolList[0]['poolname'];

        $dataArray = array();

        $i=0;
        foreach($poolList as $pool){

          $dataArray[$i] = $pool;

    		if($pool['question_type']==3){	//is rubric
             $rubricList = $QuestionSurvey->getRubricDetails($pool['chapterid']);
	   			if($rubricList){			//if rubric value exist
                    $dataArray[$i]['rubric'] = $rubricList;
                }    		
            }

            $questionList = $QuestionSurvey->getQuestionByChapter($pool['chapterid']);
            $questionListArray = array();
            $questionListArray= $questionList;
            foreach($questionList as $a=>$que){
            	
            	//get answer
            	$answerList = $QuestionSurvey->getAnswerByQuestion($student_id,$que['IdQuestion'],$idSet,$main_set_id);
            	$questionListArray[$a]['answer']=$answerList['answer'];
            	$questionListArray[$a]['answer_text']=$answerList['answer_text'];
            }

    		//get list of question
            $dataArray[$i]['question'] = $questionListArray;
            $i++;

        }
        

        $this->view->lobjquestion = $QuestionSurvey;
        $this->view->question = $dataArray;
    }
	
}