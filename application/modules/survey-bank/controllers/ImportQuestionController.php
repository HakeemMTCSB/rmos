<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 3/3/2016
 * Time: 10:20 AM
 */
Class SurveyBank_ImportQuestionController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new SurveyBank_Model_DbTable_ImportQuestion();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Import Question');

        $pool_id= $this->_getParam('id', 0);
        $this->view->pool_id = $pool_id;

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            $dir = DOCUMENT_PATH.'/question';

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false ){
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
            $adapter->addValidator('Extension', false, array('extension' => 'csv', 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();

            if ($files){
                foreach ($files as $file=>$info){
                    if ($adapter->isValid($file)){
                        //var_dump($info); exit;
                        $fileOriName = $info['name'];
                        $fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
                        $filepath = $info['destination'].'/'.$fileRename;

                        if ($adapter->isUploaded($file)){
                            $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
                            if ($adapter->receive($file)){
                                $uploadData = array(
                                    'qu_filename'=>$fileOriName,
                                    'qu_filerename'=>$fileRename,
                                    'qu_filelocation'=>'/question/'.$fileRename,
                                    'qu_filesize'=>$info['size'],
                                    'qu_mimetype'=>$info['type'],
                                    'qu_upddate'=>date('Y-m-d H:i:s'),
                                    'qu_upduser'=>0
                                );
                                $uplid = $this->model->uploadfile($uploadData);
                            }else{
                                //redirect
                                $this->_helper->flashMessenger->addMessage(array('error' => "Upload Failed"));
                                $this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'import-question', 'action'=>'index', 'id'=>$pool_id),'default',true));
                            }
                        }else{
                            //redirect
                            $this->_helper->flashMessenger->addMessage(array('error' => "Upload Failed"));
                            $this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'import-question', 'action'=>'index', 'id'=>$pool_id),'default',true));
                        }
                    }else{
                        //redirect
                        $this->_helper->flashMessenger->addMessage(array('error' => "File is not valid"));
                        $this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'import-question', 'action'=>'index', 'id'=>$pool_id),'default',true));
                    }
                }
            }else{
                //redirect
                $this->_helper->flashMessenger->addMessage(array('error' => "No file to upload"));
                $this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'import-question', 'action'=>'index', 'id'=>$pool_id),'default',true));
            }

            if ($uplid){
                $importInfo = $this->model->getImportFile($uplid);

                $csv = fopen(DOCUMENT_PATH.$importInfo['qu_filelocation'],"r");
                $header = fgetcsv($csv);

                while(! feof($csv)){
                    if ($row = fgetcsv($csv)) {

                        foreach ($row as $key => $value) {
                            if($value == '')
                                $row[$key] = NULL;
                        }

                        $auth = Zend_Auth::getInstance();
                        $userid = $auth->getIdentity()->id;

                        $data["pool_id"] = $pool_id;
                        $data["question"] = $row[0];
                        $data["status"] = trim($row[1])=='Active' ? 1:2;
                        $data["point"] = 1;
                        $data["updUser"] = $userid;
                        $data["dateUpd"] = DATE('Y-m-d H:i:s');
                        $data["review"] = 1;

                        //print_r($q);

                        $questionDB = new App_Model_Question_DbTable_Question();
                        $questionDB->addData($data);
                    }
                }
                fclose($csv);
            }

            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Import question success"));
            $this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'question', 'action'=>'view', 'pool_id'=>$pool_id),'default',true));
        }
    }

    public function exportAction(){
        $pool_id= $this->_getParam('id', 0);

        $condition = array('pool_id'=>$pool_id,'question'=>"");
        $questionDB = new App_Model_Question_DbTable_Question();
        $this->view->question = $questionDB->getQuestion($condition);

        $this->_helper->layout->disableLayout();
        $this->view->role = $this->auth->getIdentity()->IdRole;

        $this->view->filename = date('Ymd').'_question.xls';
    }
}