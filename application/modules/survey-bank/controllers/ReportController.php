<?php
/**
 *  @author suliana 
 *  @date March 18, 2015
 */
 

ini_set('memory_limit', '-1');

class SurveyBank_ReportController extends Base_Base {
	
	public function init() { //initialization function
		$this->fnsetObj();
	}
	
	public function fnsetObj(){
		$this->lobjdeftype = new App_Model_Definitiontype();
	}

	public function courseAction(){
        $this->view->title = $this->view->translate('Overall Response by Courses');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
         $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();

			 $this->view->semester = $formData['semester'];
			 $this->view->program = $formData['program'];
			 $this->view->programscheme = $formData['programscheme'];

		    $list = $surveyDb->getSearchDataReportByCourse($formData);
		    $this->view->list = $list;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;
				
         }
    }
    
 	public function printCourseReportAction(){
        
            //set unlimited
            set_time_limit(0);
            ini_set('memory_limit', '-1');

            $this->_helper->layout->disableLayout();
            $surveyDb = new App_Model_Question_DbTable_StudentAnswer();

            if($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                $this->view->semester = $formData['semester'];
                $this->view->program = $formData['program'];

                $list = $surveyDb->getSearchDataReportByCourse($formData);
                $this->view->paginator = $list;
            }
            $this->view->filename = date('Ymd').'_reportbycourse.xls';
        }
    
	public function summaryAction(){
        $this->view->title = $this->view->translate('Summary');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();	
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();

			 $this->view->semester = $formData['semester'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getSearchDataReportSummary($formData);
//		    echo "<pre>";
//		    print_r($list);
//		    
		    $this->view->list = $list;

			$this->view->paginator = $list;
				
         }
    }
    
    public function printSummaryAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
        
        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getSearchDataReportSummary($formData);
            $this->view->list = $list;

            $this->view->paginator = $list;

        }
        
        $this->view->filename = date('Ymd').'_summaryreport.xls';
    }
    
	public function printSummaryReportAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 $surveyDb = new App_Model_Question_DbTable_StudentAnswer();	
		    $list = $surveyDb->getSearchDataReportSummary($formData);
		    $this->view->paginator = $list;	
        }
        $this->view->filename = date('Ymd').'_summaryreport.xls';
    }
    
    
public function performanceByLecturerAction(){
        $this->view->title = $this->view->translate('Graphical Presentation of Performance by Course, Facilitator and Programme Administration');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
         //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();	
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->semester = $formData['semester'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getReportByCourseLecturer($formData);
		    $this->view->list = $list;
			
         }
    }
    
	public function printPerformanceByLecturerAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
			
			$searchData = array(
				'program'=>$formData['program_id'],
				'semester'=>$formData['semester'],
				'branch'=>$formData['branch_id'],
				'mop'=>$formData['mop'],
			);
			
          	$surveyDb = new App_Model_Question_DbTable_StudentAnswer();	
		    $list = $surveyDb->getReportByCourseLecturer($searchData);
		    $this->view->paginator = $list;	
        }
        $this->view->filename = date('Ymd').'_reportperformancebylecturer.xls';
    }
	
	public function rankLecturerAction(){
        $this->view->title = $this->view->translate('Facilitator Rank');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         $ranks = null;
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 //var_dump($formData); exit;
			 $this->view->semester = $formData['semester'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getRankCourse($formData);
             //var_dump($list);
		   
			if (count($list) > 0)
				$ranks = $surveyDb->ranking($list);
             //var_dump($ranks);
			//print_r($ranks);
			$this->view->list = $list;
			$this->view->ranks = $ranks;
			
         }
    }
    
    public function printRankLecturerAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
    	 
     	if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            $this->view->semester = $formData['semester'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getRankCourse($formData);

            if (count($list) > 0)
                $ranks = $surveyDb->ranking($list);
            
            $this->view->list = $list;
            $this->view->ranks = $ranks;
        }
        $this->view->filename = date('Ymd').'_Facilitator_Rank.xls';
    }
    
    public function individualAction(){
        $this->view->title = $this->view->translate('Online Teaching Evaluation Rating (TER) Report');
        
        $parentid = html_entity_decode($this->_getParam( 'id', 0 ));
        $IdCourseTaggingGroup = html_entity_decode($this->_getParam( 'IdCourseTaggingGroup', 0 ));
        $IdProgram = html_entity_decode($this->_getParam( 'IdProgram', 0 ));
        $schemeId = $this->_getParam( 'idScheme', 0 );
        $mopId = $this->_getParam( 'mop', 0 );
        
        $this->view->parent_id = $parentid;
        $this->view->IdCourseTaggingGroup = $IdCourseTaggingGroup;
        $this->view->schemeId = $schemeId;
        $this->view->mopId = $mopId;
        $this->view->IdProgram = $IdProgram;
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $schemeDB = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->scheme = $schemeDB->fnViewSchemeDetails($schemeId);
        
         //program
        $programDb = new Registration_Model_DbTable_Program();	
        $progInfo = $programDb->getProgramInfoArray($IdProgram);
        $this->view->programData = $progInfo;

        $defDB = new App_Model_Definitiontype();
        $this->view->mop = $defDB->fetchDetailAward($mopId);
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
		
		/*$program = array();
		foreach($SectionData as $row){
			$program[] = $row['program_id'];
		}*/

		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$dataGroup = $courseGroupDb->getInfoArray($IdCourseTaggingGroup);
		$this->view->group = $dataGroup;
		
		$semester = $dataGroup['IdSemester'];
		$subjectId = $dataGroup['IdSubject'];
		
		$list = $surveyDb->getTotalResponse($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram,$mopId);
		$this->view->list = $list;
		
//		echo "<pre>";
//		print_r($list);
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
        ///var_dump($IdCourseTaggingGroup);
		$dataArray = $surveyDb->getTotalResponseData($parentid,$IdCourseTaggingGroup,$schemeId,$mopId,$IdProgram,$subjectId);

        //var_dump($dataArray['detail'][0]['chapter'][0]['question'][0]['rubric']);

        //echo '<pre/>';
        //print_r($dataArray['detail']);
		$this->view->data = $dataArray['detail'];
		$this->view->dataSummaryInfo = array_values($dataArray['summary']);
		
    }
    
	public function coursePerformanceAction(){
        $this->view->title = $this->view->translate('Comparison of Course Performance');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();	
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();

			 $this->view->semester = $formData['semester'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getReportByCourse($formData);
		    $this->view->list = $list;
			
         }
    }
    
    public function printCoursePerformanceAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
    	 
     	if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getReportByCourse($formData);
            $this->view->list = $list;
        }
        $this->view->filename = date('Ymd').'_Course_Performance.xls';
    }
    
	public function lecturerPerformanceAction(){
        $this->view->title = $this->view->translate('Comparison of Lecturer Performance');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();	
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->semester = $formData['semester'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getReportByLecturer($formData);
		    $this->view->list = $list;
			
         }
    }
    
    public function printLecturerPerformanceAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getReportByLecturer($formData);
            $this->view->list = $list;
        }
        $this->view->filename = date('Ymd').'_Lecturer_Performance.xls';
    }
    
	public function coursePerformanceQuestionnaireAction(){
        $this->view->title = $this->view->translate('Performance by Questionnaires');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();	
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->semester = $formData['semester'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getReportByCourseQuestionnaire($formData);
		    $this->view->list = $list;
			
         }
    }
    
    public function printCoursePerformanceQuestionnaireAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
    	 
     	if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getReportByCourseQuestionnaire($formData);
            $this->view->list = $list;
        }
        $this->view->filename = date('Ymd').'_Course_Performance_Questionnaire.xls';
    }
    
	public function analysisAction(){
        $this->view->title = $this->view->translate('Analysis of Feedbacks for Course, Facilitator and Programme Administration');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();	
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //branch
        $branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();	
        $this->view->branchData = $branchDb->fnGetBranchList();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //mode of programme
        $mop_list = $this->lobjdeftype->fnGetDefinationsByLocale('Mode of Program');
        $this->view->mopData = $mop_list;
       
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->semester = $formData['semester'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->program = $formData['program'];
			 $this->view->branch = $formData['branch'];
			 $this->view->mop = $formData['mop'];
				 
		    $list = $surveyDb->getReportAnalysis($formData);
		    $this->view->list = $list;
			
         }
    }
    
    public function printAnalysisAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
        $surveyDb = new App_Model_Question_DbTable_StudentAnswer();
    	 
     	if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            $this->view->semester = $formData['semester'];
            $this->view->scheme = $formData['scheme'];
            $this->view->program = $formData['program'];
            $this->view->branch = $formData['branch'];
            $this->view->mop = $formData['mop'];

            $list = $surveyDb->getReportAnalysis($formData);
            $this->view->list = $list;
        }
        $this->view->filename = date('Ymd').'_Analysis.xls';
    }
    
    
    public function printIndividualAction(){
        $this->view->title = $this->view->translate('Online Teaching Evaluation Rating (TER) Report');
        $this->_helper->layout->disableLayout();
        $parentid = html_entity_decode($this->_getParam( 'id', 0 ));
        $IdCourseTaggingGroup = html_entity_decode($this->_getParam( 'IdCourseTaggingGroup', 0 ));
        $schemeId = $this->_getParam( 'idScheme', 0 );
        $IdProgram = html_entity_decode($this->_getParam( 'IdProgram', 0 ));
        $mopId = $this->_getParam( 'mop', 0 );
        
        $this->view->parent_id = $parentid;
        $this->view->IdCourseTaggingGroup = $IdCourseTaggingGroup;
        $this->view->schemeId = $schemeId;
        $this->view->mopId = $mopId;
        $this->view->IdProgram = $IdProgram;
        
   		set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $schemeDB = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->scheme = $schemeDB->fnViewSchemeDetails($schemeId);
        
         //program
        $programDb = new Registration_Model_DbTable_Program();	
        $progInfo = $programDb->getProgramInfoArray($IdProgram);
        $this->view->programData = $progInfo;

        $defDB = new App_Model_Definitiontype();
        $this->view->mop = $defDB->fetchDetailAward($mopId);
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
		
		$SectionData = $sectiontagging->getAnswerByID($parentid,$IdProgram); 
		
		$program = array();
		foreach($SectionData as $row){
			$program[] = $row['program_id'];
		}

		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$dataGroup = $courseGroupDb->getInfoArray($IdCourseTaggingGroup);
		$this->view->group = $dataGroup;
		
		$semester = $dataGroup['IdSemester'];
		$subjectId = $dataGroup['IdSubject'];
		
		$list = $surveyDb->getTotalResponse($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram,$mopId);
		$this->view->list = $list;
		
//		echo "<pre>";
//		print_r($SectionData);
		
		$arrayPool = array();
		$arraySumInfo = array();
    	$a = 0;
		foreach($SectionData as $secData){
//			$arrayPool[$a]['branch'] = $secData['BranchName'];
    		$arrayPool[$a]['pool_name'] = $secData['question_pool_name'];
    		$arrayPool[$a]['main_set_id'] = $secData['id'];
    		
    		//get chapter list
    		$chapterList = $QuestionSurvey->getPoolBySetDistinct($secData['id'],$secData['question_pool']);
    		
//    		echo "<pre>";
//    		print_r($chapterList);
    		
    		
    		foreach($chapterList as $c=>$chp){
//    			$arrayPool[$a]['chapter'][$c] = $chp; 

    			$arrayPool[$a]['chapter'][$c]['chaptername'] = $chp['chaptername']; 
//    			$arrayPool[$a]['chapter'][$c]['chapterinstruction'] = $chp['subsection_instruction'];
    			$arrayPool[$a]['chapter'][$c]['question_type'] = $chp['question_type'];
    			
    			
    			
    			if($chp['question_type'] == 3){
	    			$rubricList = $QuestionSurvey->getRubricDetails($chp['chapterid']);
	    			$arrayPool[$a]['chapter'][$c]['rubric'] = $rubricList;
    			}
    			
    			//get list of question based on main set id
	    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id'],$chp['chapterid']);
	    		$totalWeightage = 0;
	    				$totalRespondent = 0;
	    				$totalWeightageAll = 0;
	    		foreach($setQuestion as $q=>$qs){
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['question_id'] = $qs['IdQuestion'];
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['subject_id'] = $dataGroup['IdSubject'];
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['question'] = $qs['question']; 
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['order'] = 'Q'.$qs['order']; 
	    			
	    			$program = $secData['program_id'];
	    			$branch = $secData['branch_id'];
	    			$semester = $secData['semester_id'];
	    			$subjectId = $dataGroup['IdSubject'];
	    			$main_set_id = $secData['id'];
	    			$pool_id = $secData['question_pool'];
	    			$registration_item_id = $chp['registration_item_id'];
	    			$section_id = $chp['id'];
	    			$chapterid = $chp['chapterid'];
	    			//get total answer by question
	    			if($chp['question_type'] == 3){ //likert
	    				
	    				foreach($rubricList as $r=>$rb){
	    					
			    			$totalAnswerArray = $surveyDb->getAnswerSubmitBySet($IdCourseTaggingGroup,$main_set_id,$chapterid,$subjectId,$qs['IdQuestion'],$rb['ValueNumber'],$IdProgram);
			    			
			    			$arrayPool[$a]['chapter'][$c]['question'][$q]['rubric'][$r]['rubricName'] = $rb['description'];
			    			$arrayPool[$a]['chapter'][$c]['question'][$q]['rubric'][$r]['rubricValue'] = $rb['ValueNumber'];
			    			$arrayPool[$a]['chapter'][$c]['question'][$q]['rubric'][$r]['total'] = $totalAnswerArray;
			    			
			    			$totalRespondent += $totalAnswerArray['totalAnswer'];
			    			$totalWeightage += $totalAnswerArray['sum'];
			    			
			    			
			    			
	    				}
	    				if($totalRespondent > 0){
	    					$totalWeightageAll = round($totalWeightage/$totalRespondent,2);
	    				}
	    				
	    				$arrayPool[$a]['chapter'][$c]['totalRespondent'] = $totalRespondent;
	    				$arrayPool[$a]['chapter'][$c]['totalWeightage'] = $totalWeightage;
	    				$arrayPool[$a]['chapter'][$c]['totalWeightageAll'] = $totalWeightageAll;
	    				
	    				$arraySumInfo[$c]['chaptername'] = $chp['chaptername'];
	    				$arraySumInfo[$c]['totalRespondent'] = $totalRespondent;
	    				$arraySumInfo[$c]['totalWeightage'] = $totalWeightage;
	    				$arraySumInfo[$c]['totalWeightageAll'] = $totalWeightageAll;
    				
	    				
	    			}elseif($chp['question_type'] == 2){//essay
	    				$answerArray = $surveyDb->getAnswerByQuestionType($IdCourseTaggingGroup,$subjectId, $main_set_id, $qs['IdQuestion']);
	    				
	    				
		    			$arrayPool[$a]['chapter'][$c]['question'][$q]['answer_essay'] = $answerArray;
	    			}
	    			
	    			
	    			
	    			
	    		}
	    		
    		}
    		
    		$a++;
    	}

		//echo "<pre>";
		//print_r($arraySumInfo);
		
       $this->view->filename = date('Ymd').'_Individual.xls';
		$this->view->data = $arrayPool;
		$this->view->dataSummaryInfo = array_values($arraySumInfo);
    }
    
	public function rawDataAction(){
		
		 set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	
        $this->view->title = $this->view->translate('Raw Data');
        
        $parentid = html_entity_decode($this->_getParam( 'id', 0 ));
        $IdCourseTaggingGroup = html_entity_decode($this->_getParam( 'IdCourseTaggingGroup', 0 ));
        $IdProgram = html_entity_decode($this->_getParam( 'IdProgram', 0 ));

        $schemeId = $this->_getParam( 'idScheme', 0 );
        $mopId = $this->_getParam( 'mop', 0 );
        
        $this->view->parent_id = $parentid;
        $this->view->IdCourseTaggingGroup = $IdCourseTaggingGroup;
        $this->view->schemeId = $schemeId;
        $this->view->mopId = $mopId;
        $this->view->IdProgram = $IdProgram;
        
        $schemeDB = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->scheme = $schemeDB->fnViewSchemeDetails($schemeId);
        
        $defDB = new App_Model_Definitiontype();
        $this->view->mop = $defDB->fetchDetailAward($mopId);
        
		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
		$sectiontagging = new App_Model_Question_DbTable_SectionTagging();
		$QuestionSurvey = new App_Model_Survey_DbTable_QuestionSurvey();
		
		$SectionData = $sectiontagging->getAnswerByID($parentid); 
		
		$program = array();
		foreach($SectionData as $row){
			$program[] = $row['program_id'];
		}

		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$dataGroup = $courseGroupDb->getInfoArray($IdCourseTaggingGroup);
		$this->view->group = $dataGroup;
		
		$semester = $dataGroup['IdSemester'];
		$subjectId = $dataGroup['IdSubject'];
		
		$list = $surveyDb->getTotalResponse($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram,$mopId);
		$this->view->list = $list;
		
		
//		echo "<pre>";
//		print_r($SectionData);
		
		$arrayPool = array();
		$arraySumInfo = array();
    	$a = 0;
		foreach($SectionData as $secData){
//			$arrayPool[$a]['branch'] = $secData['BranchName'];
    		$arrayPool[$a]['pool_name'] = $secData['question_pool_name'];
    		$arrayPool[$a]['main_set_id'] = $secData['id'];
    		
    		//get chapter list
    		$chapterList = $QuestionSurvey->getPoolBySetDistinct($secData['id'],$secData['question_pool']);
    		
    		foreach($chapterList as $c=>$chp){
//    			$arrayPool[$a]['chapter'][$c] = $chp; 

    			$arrayPool[$a]['chapter'][$c]['chaptername'] = $chp['chaptername']; 
    			$arrayPool[$a]['chapter'][$c]['question_type'] = $chp['question_type'];
    			$arrayPool[$a]['chapter'][$c]['chapterid'] = $chp['chapterid'];
    			
    			
    			//get list of question based on main set id
	    		$setQuestion = $QuestionSurvey->getSetQuestion($secData['id'],$chp['chapterid']);
    			
	    		foreach($setQuestion as $q=>$qs){
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['question_id'] = $qs['IdQuestion'];
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['subject_id'] = $dataGroup['IdSubject'];
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['question'] = $qs['question']; 
	    			$arrayPool[$a]['chapter'][$c]['question'][$q]['order'] = 'Q'.$qs['order']; 
	    			
	    		}
	    		
    		}
    		
    		$a++;
    	}
    	
    	//get student list
    	$listStudent = $surveyDb->getResponseStudentList($IdCourseTaggingGroup,$semester,$subjectId,$IdProgram,$mopId);
    	
    	$arrayStudent = array();
    	foreach($listStudent as $k=>$stud){
    		$arrayStudent[$k]['name'] = $stud['name'];
    		$arrayStudent[$k]['registrationId'] = $stud['registrationId'];
    		$arrayStudent[$k]['Nationality'] = $stud['Nationality'];
    		$arrayStudent[$k]['ProgramCode'] = $stud['ProgramCode'];
    		$arrayStudent[$k]['GroupName'] = $dataGroup['GroupName']; 
    		
    		$programScheme = '';
    		if ( isset($stud['ProgramMode']) && $stud['ProgramMode'] != '' ){
    			$programScheme .= $stud['ProgramMode']."<br>";
    		}
    		if ( isset($stud['StudyMode']) && $stud['StudyMode'] != '' ){
    			$programScheme .= $stud['StudyMode']."<br>";
    		}
    		if ( isset($stud['ProgramType']) && $stud['ProgramType'] != '' ){
    			$programScheme .= $stud['ProgramType'];
    		}
    		
    		$arrayStudent[$k]['ProgramScheme'] = $programScheme;
    		
    		$surveyDb = new App_Model_Question_DbTable_StudentAnswer();
    		//$checkCompleteTer = $surveyDb->getFeedbackStatus($stud['IdStudentRegistration'],$semester,$subjectId,$IdCourseTaggingGroup,$secData['id'],$IdProgram,$mopId);
    		$studentData = array(
                    'IdBranch'=> $stud['IdBranch'],
                    'IdProgram'=> $stud['IdProgram'],
                    'IdProgramScheme'=> $stud['IdProgramScheme'],
                    'IdStudentRegistration'=> $stud['IdStudentRegistration'],
                );
                
                $checkCompleteTer = $surveyDb->getTerCompleteStatus($studentData,$semester,$subjectId,$IdCourseTaggingGroup);
                
    		$arrayStudent[$k]['feedbackStatus'] = $checkCompleteTer;
    		
    		//array answer
    		$m=0;
    		foreach($arrayPool as $pl){
	    		foreach($pl['chapter'] as $chp){
	    		 
	    			foreach($chp['question'] as $q=>$que){
	    				
	    				$sectionTaggingDB = new App_Model_Question_DbTable_SectionTagging();
	    				$answerInfo = $sectionTaggingDB->getAnsweredStudent($stud['IdStudentRegistration'],$semester,$subjectId,$IdCourseTaggingGroup,$que['question_id']);
	    				
	    				$arrayStudent[$k]['answer'][$m] = $answerInfo;
	    				
	    				$m++;
	    			}
	    			
	    		}
    		}
    	}
    	
    	$this->view->student = $arrayStudent;

//		echo "<pre>";
//		print_r($arrayStudent);
		
       
		$this->view->data = $arrayPool;
		
		$this->view->filename = date('Ymd').'_rawdata.xls';
    }
    
    
    public function getProgramSchemeAction(){
        $id = $this->_getParam('id',null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $model = new SurveyBank_Model_DbTable_ImportQuestion();
        $list = $model->getProgrammeSchemeAction($id);

        $json = Zend_Json::encode($list);

        echo $json;
        exit();
    }
}
?>