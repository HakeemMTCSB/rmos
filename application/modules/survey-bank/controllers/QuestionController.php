<?php

Class SurveyBank_QuestionController extends Base_Base {
	
    public function indexAction() {
		//title
    	$this->view->title="Manage Question";
    	
    	//paginator
		$poolDB = new App_Model_Question_DbTable_Question();
		$poolData = $poolDB->getPaginateData();
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($poolData));
		$paginator->setItemCountPerPage(20);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	
    	if($id>0){
    		$pool = new App_Model_Question_DbTable_Question();
    		$pool->deleteData($id);
    	}

		$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'pool', 'action'=>'index')));
    }
    
	public function addQuestionAction()
    {    	
    	//title
    	$this->view->title="Add Question";
    	
    	 $pool_id= $this->_getParam('pool_id', 0);
    	 $id=$this->_getParam('id',0);
//    	echo $question_id=$this->_getParam('id',0);
    	$this->view->pool_id = $pool_id;    	
    	
    	//pool info
    	$poolDB = new App_Model_Question_DbTable_Pool();
    	$poolData = $poolDB->getData($pool_id);
    	$this->view->pool = $poolData;   
    	
    	$topicDB = new App_Model_Question_DbTable_Chapter(); 
    	$topic = $topicDB->getTopicbyPool($pool_id);
    	$this->view->topic = $topic; 
    	
//    	$qtDB = new App_Model_Question_DbTable_Chapter(); 
//    	$questiontype = $qtDB->getQuestionTypebySection($pool_id);
//    	$this->view->questiontype = $questiontype; 

//    	    $poolDB = new App_Model_Question_DbTable_QuestionType();
//    		$poolData = $poolDB->getData();
//    		$this->view->pool1 = $poolData;
    	 	
    	if ($this->getRequest()->isPost()) {
			
			$formdata = $this->getRequest()->getPost();
		
//			echo "<pre>";
//			print_r($formdata);
//			exit;
			$date = date('Y-m-d H:i:s');
				
				$auth = Zend_Auth::getInstance();
				$userid = $auth->getIdentity()->id;
			
//			$total = $formdata["total_language"]; 
			
			//get rubric id
//			$answerDb = new App_Model_Question_DbTable_Answer();
//			$getRubric = $answerDb->getrubricID($formdata["topic_id"]);
//			$rubric_id = $getRubric['id'];

    			    $data["pool_id"]          = $formdata["pool_id"];	
//				    $data["chapter_id"]       = $formdata["topic_id"];	
//				    $q["rubric_id"]           = $rubric_id;
//	     		    $data["question_type"]    = $formdata["question_type"];
		     		$data["question"]         = $formdata["question"];	
		     		$data["status"]           = $formdata["status"];	
		     		$data["point"]            = 1;			     			
		     		$data["updUser"]          = $userid;
		     		$data["dateUpd"]          = $date;
		     		$data["review"]           = 1;	
		     		
		     		//print_r($q);
		     		
					$questionDB = new App_Model_Question_DbTable_Question();
		            $question_id = $questionDB->addData($data); 
		            
		            /*--------------------------radiobutton-----------------------------*/
//				$iteration  		= $formdata ["iteration"];	
//				$q["pool_id"]       = $formdata ["pool_id"];
//				$q["question_id"]   = $question_id;
//				$q["dateUpd"]       = $date;
//				$q["updUser"]       = $userid;
//				
//						for($i=1; $i<=$iteration; $i++){
//						 	
//						 	$q["ValueNumber"]= $formdata["ValueNumber".$i];
//						 	$q["description"]= $formdata["description".$i];					 	
//						;
//
//						 	//process rubric
//							$pool2 = new App_Model_Question_DbTable_Answer();
//							$pool2->addAnswer($q);
//							
////							echo "<pre>";      
////				            print_r($q);
//						}
				//redirect	
				$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'question', 'action'=>'view')));
    	}
    }
    
	public function viewAction(){
		
    	$pool_id= $this->_getParam('pool_id', 0);
    	$this->view->pool_id = $pool_id;
    	
    	//pool info
    	$poolDB = new App_Model_Question_DbTable_Pool();
    	$poolData = $poolDB->getData($pool_id);
    	$this->view->pool = $poolData;   
    	
    	$topicDB = new App_Model_Question_DbTable_Chapter(); 
    	$topic = $topicDB->getTopicbyPool($pool_id);
    	$this->view->topic = $topic;   	
    
    	//title
    	$this->view->title="Question List - ".$poolData['name'];
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$question = $this->getRequest()->getPost('question');
    		$qid = $this->getRequest()->getPost('question_id');
    		
    		$condition = array('pool_id'=>$pool_id,'question'=>$question,'question_id'=>$qid);
    		
	    	$questionDB = new App_Model_Question_DbTable_Question();
	    	$this->view->question = $questionDB->getQuestion($condition);
	    	
    	}else{
    		//question
	    	$condition = array('pool_id'=>$pool_id,'question'=>"");
	    	$questionDB = new App_Model_Question_DbTable_Question();
	    	$this->view->question = $questionDB->getQuestion($condition);
    	}

    	//check for course
    	if($this->view->question==null){
    		$this->view->noticeMessage = "No question";	
    	}
	}
	
	public function manageAction(){
    	
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
        }
    	
	    $pool_id = $this->_getParam('pool_id', 0);
    	$this->view->pool_id = $pool_id;
    	
    	//$id = $this->_getParam('id',0);
        $section_id = $this->_getParam('section_id',0);
        $this->view->section_id = $section_id;
        
		$question_type_id = $this->_getParam('qtypeid',0);
        $this->view->question_type_id = $question_type_id;
		
        $main_set_id = $this->_getParam('main_set_id',0);
        $this->view->main_set_id = $main_set_id;
        
    	//pool info
    	$poolDB = new App_Model_Question_DbTable_Pool();
    	$poolData = $poolDB->getData($pool_id);
    	$this->view->pool = $poolData;
    	
    	$sectionDB = new App_Model_Question_DbTable_Chapter();
    	$sectionData = $sectionDB->getData($section_id);
    	$this->view->sectiondata = $sectionData;
    	
		//get question
    	if ($this->getRequest()->isPost()) {
    		$question = $this->getRequest()->getPost('question');
    	    $qid = $this->getRequest()->getPost('question_id');
    	    
    		$condition = array('pool_id'=>$pool_id,'question_id'=>$qid,'question'=>$question);         
	    	$questionDB = new App_Model_Question_DbTable_Question();
	    	$this->view->question = $questionDB->getmanageQuestion($condition);
    	}else{
	    	$condition = array('pool_id'=>$pool_id,'question'=>"");
	    	$questionDB = new App_Model_Question_DbTable_Question();
	    	$this->view->question = $questionDB->getmanageQuestion($condition);
    	}
    	
		//add to questiontagging
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$date = date('Y-m-d H:i:s');
			
			$auth = Zend_Auth::getInstance();
			$idUpd = $auth->getIdentity()->id;
 
			$questionList = $formData['IdQuestion'];
			$n = count($questionList);
			$i = 0;
			
			$manage = new App_Model_Question_DbTable_QuestionTagging();
			$manage->deleteQuestion($section_id);
			
			while($i < $n){
				echo $questionID = $questionList[$i];
				
				//insert to questiontagging
				$data = array(
					'order'   => $i+1,
				    'main_set_id'   => $main_set_id,
				    'section_id'    => $section_id,
				    'updUser'       => $idUpd,
					'dateUpd'       => $date,
				    'IdQuestion'    => $questionID,
				);
				$i++;
//				echo "<pre>";      
//				print_r($data);

//				//insert process form
//                $manage = new App_Model_Question_DbTable_QuestionTagging();
                $manage->addData($data);
			}
//			exit;
			
			//redirect	
			$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'sectiontagging', 'action'=>'views','id'=>$main_set_id),'default',true));	
	
    	}
	}

	private function aasort(&$array, $key){
	    $sorter = array();
	    $ret = array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii] = $va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii] = $array[$ii];
	    }
	    $array = $ret;
	}
	
	public function viewsAction() {
	
		$this->view->title="Question View"; 
		 
	    $id = $this->_getParam('id',0);
		$question_id = $this->_getParam('id',0);
		
		$poolDB = new App_Model_Question_DbTable_Question();
    	$poolData = $poolDB->getDataByID($id); 
    	$this->view->poolData= $poolData;
    
    	$RadioDB = new App_Model_Question_DbTable_Answer();
    	$RadioData = $RadioDB->getAnswerByID($id); 
    	$this->view->RadioData= $RadioData;
       
	}
	
    public function editAction(){ 	
    	
    	$this->view->title="Edit Question";
    	
    	$id=$this->_getParam('qid',0);
    	$pool_id = $this->_getParam('pool_id', 0);
    	$qid = $this->_getParam('qid', 0);
    	$question_id = $this->_getParam ('qid',0) ; 
    	
    	$poolDB = new App_Model_Question_DbTable_Pool();
    	$poolData = $poolDB->getData($pool_id);
    	$this->view->pool = $poolData;   
    	
    	$topicDB = new App_Model_Question_DbTable_Chapter(); 
    	$topic = $topicDB->getTopicbyPool($pool_id);
    	
    	$this->view->topic = $topic;  
    	 	
//    	$poolDB = new App_Model_Question_DbTable_QuestionType();
//    	$poolData = $poolDB->getData();
//    	$this->view->pool1 = $poolData;
    		
    	$poolDB = new App_Model_Question_DbTable_Question();
    	$poolData = $poolDB->getDataByID($id); 
//    	echo'<pre>';   
//    	print_r($poolData);
//    	echo'</pre>'; 
    	$this->view->poolData= $poolData;
    	$this->view->status= $poolData["status"];
    
//    	$RadioDB = new App_Model_Question_DbTable_Answer();
//    	$RadioData = $RadioDB->getAnswerByID($id); 
////    	
//    	$this->view->RadioData= $RadioData;
    	
//    	$this->view->poolData= $poolData;
    

    	if ($this->getRequest()->isPost()) {
 
		$formdata = $this->getRequest()->getPost();
    		
    		    $date = date('Y-m-d H:i:s');
				$auth = Zend_Auth::getInstance();
				$userid = $auth->getIdentity()->id;
    		
//    		echo "<pre>";
//    		print_r ($formdata);
//    		exit;
//    		
//		    $questionDB = new App_Model_Question_DbTable_Question();
//		    $questionDB = $questionDB->getQuestion();
			
//			$answerDb = new App_Model_Question_DbTable_Answer();
//			$getRubric = $answerDb->getrubricID($formdata["topic_id"]);
//			$rubric_id = $getRubric['id'];
		
			
//					$data["pool_id"]          = $formdata['pool_id'];	
//				    $data["chapter_id"]       = $formdata['topic_id'];
//				    $data["question_type"]	  = $formdata['question_type'];
				    	
		     		
		        $data = array(				    		    
			     	'question'      => $formdata["question"],	
		     		'status'        => $formdata["status"],				   
					'dateUpd'       => $date,
					'updUser'       => $userid
				);
		     		
				//update question 		
				$questionDB = new App_Model_Question_DbTable_Question(); 
				$questionDB->updateData($data,$id);
				
				//get idpool
				$QuestionId = $questionDB->getData($id);
				
				//delete radiobutton
//				$pool2 = new App_Model_Question_DbTable_Answer();
//				$pool2->deleteAnswer2($id);
//				
//				/*--------------------------radio-----------------------------*/
//				$iteration  		= $formdata ["iteration"];	
//				
//						for($i=1; $i<=$iteration; $i++){
//							
//							$q["pool_id"]       = $QuestionId["pool_id"];
//							$q["question_id"]   = $id;
//							$q["dateUpd"]       = $date;
//							$q["updUser"]       = $userid;
//						 	$q["ValueNumber"]   = $formdata["ValueNumber".$i];
//						 	$q["description"]   = $formdata["description".$i];
//						 
//							$pool2->addAnswer($q);
//				
////							echo "<pre>";      
////				            print_r($q);
//						 }
				
//				           exit;
			
				//redirect
				$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'question', 'action'=>'view', 'pool_id' => $pool_id), null, true));
    	}
    	
    	
    }
    
    public function deleteQuestionAction($qid = null){
    	$pool_id = $this->_getParam('pool_id', 0);
    	$qid = $this->_getParam('qid', 0);
    	$question_id = $this->_getParam ('qid',0) ; 
    	
    	if($qid>0){
    		$questionDB = new App_Model_Question_DbTable_Question();
    		$questionDB->deleteData($qid);
    		
//    		delete answer
//    		$pool2 = new App_Model_Question_DbTable_Answer();
//    		$pool2->deleteAnswer($question_id);
    		
    		//redirect
    		
    		$this->_redirect($this->view->url(array('module'=>'survey-bank','controller'=>'question', 'action'=>'view', 'pool_id' => $pool_id), null, true));
    	}    	
    }
    
    public function displayquestionAction(){
    	
	   if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        if ( $this->getRequest()->isPost() ) {
            $post_data = $this->getRequest()->getPost();
            
            if(!empty($post_data['question_order'])) {
                $Chap = new App_Model_Question_DbTable_QuestionTagging();
                if(!empty($post_data['question_order'])) {

                    foreach($post_data['question_order'] as $id => $ord) {
                        
                        $Chap->update_order($id, $ord);
                    }
                    
                }

            }
            $this->_helper->layout->disableLayout();
            echo "ok";exit;
        }
    	
    	$id = $this->_getParam('id',0);
        $section_id=$this->_getParam('section_id',0);
        $this->view->section_id = $id;
        
        $main_set_id=$this->_getParam('main_set_id',0);
        $this->view->main_set_id = $main_set_id;
        
        //get list of section
		$topicDB = new App_Model_Question_DbTable_Chapter();
		$topic = $topicDB->getTopicbySet( $main_set_id ,$id);
		$this->view->topic = $topic;
        
        
        //get list of question
    	 $questiontagging = new App_Model_Question_DbTable_QuestionTagging();
    	 $displayquestion = $questiontagging->getDisplayQuestion($main_set_id,$id); 
    	 $this->view->displayquestion=$displayquestion;
	}
}