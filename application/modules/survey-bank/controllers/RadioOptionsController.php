<?php

Class SurveyBank_RadioOptionsController extends Base_Base {
	
	public function indexAction() {
		
	}
	
	public function radioanswerAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
		
		$poolDB = new App_Model_Question_DbTable_Pool();
        $questionDB = new App_Model_Survey_DbTable_QuestionSurvey();
		$questiontaggingDB = new App_Model_Question_DbTable_QuestionTagging();        
		$radiooptionlistDB = new App_Model_Question_DbTable_RadioOptionList();
		
		$this->view->radiooptionlistDB = $radiooptionlistDB;
        
		$pool_id = $this->_getParam('pool_id', 0);
    	$this->view->pool_id = $pool_id;
    	
        $section_id = $this->_getParam('section_id',0);
        $this->view->section_id = $section_id;
        
        $question_type_id = $this->_getParam('qtypeid',0);
        $this->view->question_type_id = $question_type_id;
        
        $main_set_id = $this->_getParam('main_set_id',0);
        $this->view->main_set_id = $main_set_id;
        
    	$poolData = $poolDB->getData($pool_id);
    	$this->view->pool = $poolData;
    	
    	$questiontaggingData = $questiontaggingDB->getQuestionTagging($main_set_id,$section_id);
        $this->aasort($questiontaggingData, 'IdQuestion');
        $this->view->question = $questiontaggingData;
        
	}
	
	public function getRadioOptionAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
		
		$main_set_id = $this->_getParam('main_set_id',0);		
		$section_id = $this->_getParam('section_id',0);        
        $tagging_id = $this->_getParam('tagging_id',0);
        
		$radiooptionlistDB = new App_Model_Question_DbTable_RadioOptionList();
		$optionlist = $radiooptionlistDB->getData($main_set_id,$section_id,$tagging_id);
		
		echo json_encode($optionlist);
		
	}

	public function addRadioOptionAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
		
		$main_set_id = $this->_getParam('main_set_id',0);
		$section_id	 = $this->_getParam('section_id',0);
		$answer 	 = $this->_getParam('answer',0);
        $label 	 = $this->_getParam('label',0);
		$tagging_id	 = $this->_getParam('tagging_id',0);
		
		$auth = Zend_Auth::getInstance();
		$radiooptionlistDB = new App_Model_Question_DbTable_RadioOptionList();
		
		$data = array(
					 'question_tagging_id'=>$tagging_id,
					 'main_set_id'=>$main_set_id,
					 'section_id'=>$section_id,
					 'answer'=>$answer,
                     'label'=>$label,
					 'DatetimeInsert'=>date('Y-m-d H:i:s'),
					 'InsertBy'=>$auth->getIdentity()->id
					 );
		echo $radiooptionlistDB->addData($data);
		
	}
	
	public function updateRadioOptionAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
		
		$id = $this->_getParam('id',0);
		$text = $this->_getParam('newanswer',0);
        $label = $this->_getParam('newlabel',0);
		
		$radiooptionlistDB = new App_Model_Question_DbTable_RadioOptionList();
		
		$dataArray = array( 'answer'=>$text, '' );
		
		$update = $radiooptionlistDB->updateData($dataArray,$id);
		echo $update;
		
	}

	public function deleteRadioOptionAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
		
		$answer_id = $this->_getParam('answer_id',0);	//73
		
		$radiooptionlistDB = new App_Model_Question_DbTable_RadioOptionList();
		echo $radiooptionlistDB->deleteData($answer_id);
        exit;
		
	}
	
	private function aasort (&$array, $key) {
    	$sorter = array();
    	$ret = array();
    	reset($array);
    	foreach($array as $ii=>$va) {
	        $sorter[$ii] = $va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii=>$va) {
        	$ret[$ii] = $array[$ii];
    	}
    	$array=$ret;
	}
}
?>