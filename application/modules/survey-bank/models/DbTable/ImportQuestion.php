<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 3/3/2016
 * Time: 10:24 AM
 */
class SurveyBank_Model_DbTable_ImportQuestion extends Zend_Db_Table_Abstract {

    public function uploadfile($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('question_upload', $data);
        $id = $db->lastInsertId('question_upload', 'qu_id');
        return $id;
    }

    public function getImportFile($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'question_upload'), array('value'=>'*'))
            ->where('a.qu_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkDean($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_deanlist'), array('value'=>'*'))
            ->where('a.IdStaff = ?', $id)
            ->where('a.Active = ?', 1);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgramBasedOnDept($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdCollege = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgrammeSchemeAction($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
}