<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 29/12/2015
 * Time: 10:15 AM
 */
class Studentfinance_Model_DbTable_PaymentAmountCron {

    static function getDiscount($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'discount_detail'), array('discount'=>'sum(a.dcnt_amount)'))
            ->join(array('b'=>'discount'), 'a.dcnt_id = b.dcnt_id', array('currency'=>'b.dcnt_currency_id', 'b.*'))
            ->where('a.dcnt_invoice_id = ?', $id)
            ->where('a.status = ?', 'A')
            ->where('b.dcnt_status = ?', 'A');

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(b.dcnt_create_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(b.dcnt_create_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['discount'] = 0.00;
        }

        return $result;
    }

    static function getDiscountDtl($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'discount_detail'), array('discount'=>'sum(a.dcnt_amount)'))
            ->join(array('b'=>'discount'), 'a.dcnt_id = b.dcnt_id', array('currency'=>'b.dcnt_currency_id', 'b.*'))
            ->where('a.dcnt_invoice_det_id = ?', $id)
            ->where('a.status = ?', 'A')
            ->where('b.dcnt_status = ?', 'A');

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(b.dcnt_create_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(b.dcnt_create_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['discount'] = 0.00;
        }

        return $result;
    }

    static function getSelfPaid($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'receipt_invoice'), array('selfpaid'=>'sum(a.rcp_inv_amount)'))
            ->join(array('b'=>'receipt'), 'a.rcp_inv_rcp_id = b.rcp_id')
            ->where('a.rcp_inv_invoice_id = ?', $id)
            ->where('b.rcp_status = ?', 'APPROVE');

        $result = $db->fetchAll($select);

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(b.rcp_receive_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(b.rcp_receive_date) <= ?', $dateto);
        }

        if ($result == null){
            $result[0]['selfpaid'] = 0.00;
        }

        return $result;
    }

    static function getSelfPaidDtl($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'receipt_invoice'), array('selfpaid'=>'sum(a.rcp_inv_amount)'))
            ->join(array('b'=>'receipt'), 'a.rcp_inv_rcp_id = b.rcp_id')
            ->where('a.rcp_inv_invoice_dtl_id = ?', $id)
            ->where('b.rcp_status = ?', 'APPROVE');

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(b.rcp_receive_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(b.rcp_receive_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['selfpaid'] = 0.00;
        }

        return $result;
    }

    static function getAdvance($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'advance_payment_detail'), array('a.advpydet_upddate', 'a.advpydet_total_paid'))
            ->join(array('b'=>'advance_payment'), 'a.advpydet_advpy_id=b.advpy_id')
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.advpydet_inv_id = ?', $id);

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(a.advpydet_upddate) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(a.advpydet_upddate) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['advancepaid'] = 0.00;
        }*/

        return $result;
    }

    static function getAdvanceDtl($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'advance_payment_detail'), array('a.advpydet_total_paid', 'a.advpydet_upddate'))
            ->join(array('b'=>'advance_payment'), 'a.advpydet_advpy_id=b.advpy_id')
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.advpydet_inv_det_id = ?', $id);

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(a.advpydet_upddate) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(a.advpydet_upddate) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['advancepaid'] = 0.00;
        }*/

        return $result;
    }

    static function getSponsorPaid($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'), array('amount'=>'a.sp_amount'))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
            ->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
            ->where('a.sp_invoice_id = ?', $id)
            ->where('c.rcp_status = ?', 'APPROVE')
            ->where('a.sp_status = ?', 1)
            ->group('a.id');

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(c.rcp_receive_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(c.rcp_receive_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['sponsorpaid'] = 0.00;
        }*/

        return $result;
    }

    static function getSponsorPaidDtl($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'), array('amount'=>'a.sp_amount'))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
            ->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
            ->where('a.sp_invoice_det_id = ?', $id)
            ->where('c.rcp_status = ?', 'APPROVE')
            ->where('a.sp_status = ?', 1);

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(c.rcp_receive_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(c.rcp_receive_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['sponsorpaid'] = 0.00;
        }*/

        return $result;
    }

    static function getCurrency($id){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'tbl_currency'), array('*'))
            ->where('a.cur_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    static function getCreditNote($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'credit_note_detail'), array('cnpaid'=>'sum(a.amount)'))
            ->join(array('b'=>'credit_note'), 'a.cn_id = b.cn_id', array('currency'=>'b.cn_cur_id', 'b.*'))
            ->where('a.invoice_main_id = ?', $id)
            ->where('b.cn_status = ?',  'A');

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(b.cn_create_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(b.cn_create_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['cnpaid'] = 0.00;
        }

        return $result;
    }

    static function getCreditNoteDtl($id, $datefrom = null, $dateto = null){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'credit_note_detail'), array('cnpaid'=>'sum(a.amount)'))
            ->join(array('b'=>'credit_note'), 'a.cn_id = b.cn_id', array('currency'=>'b.cn_cur_id', 'b.*'))
            ->where('a.invoice_detail_id = ?', $id)
            ->where('b.cn_status = ?',  'A');

        if ($datefrom != null) {
            $datefrom = date('Y-m-d', strtotime($datefrom));
            $select->where('DATE(b.cn_create_date) >= ?', $datefrom);

        }

        if ($dateto){
            $dateto = date('Y-m-d', strtotime($dateto));
            $select->where('DATE(b.cn_create_date) <= ?', $dateto);
        }

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['cnpaid'] = 0.00;
        }

        return $result;
    }

    static function getAdvanceTransfer($id){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'advance_payment'), array('advanceamount'=>'sum(a.advpy_amount)', 'currency'=>'advpy_cur_id' , '*'))
            ->where('a.advpy_status = ?', 'A')
            ->where('a.advpy_invoice_id = ?', $id);

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['advanceamount'] = 0.00;
        }

        return $result;
    }

    static function getAdvanceTransferRefundable($id){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'advance_payment'), array('advanceamount'=>'sum(a.advpy_amount)', 'currency'=>'advpy_cur_id' , '*'))
            ->where('a.advpy_status = ?', 'A')
            ->where('a.ref_flag = ?', 1)
            ->where('a.advpy_invoice_id = ?', $id);

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['advanceamount'] = 0.00;
        }

        return $result;
    }

    public function getFeeItemMain($id){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'invoice_detail'))
            ->joinLeft(array('b'=>'fee_item'), 'a.fi_id = b.fi_id')
            ->where('a.invoice_main_id = ?', $id)
            ->where('b.fi_refundable = ?', 1);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getFeeItemDtl($id){
        $cronService = new cronService();
        $db = $cronService->conn;

        $select = $db->select()
            ->from(array('a'=>'invoice_detail'))
            ->joinLeft(array('b'=>'fee_item'), 'a.fi_id = b.fi_id')
            ->where('a.id = ?', $id)
            ->where('b.fi_refundable = ?', 1);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getBalanceMain($id, $datefrom = null, $dateto = null){
        //$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

        $cronService = new cronService();
        $db = $cronService->conn;
        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->where('a.id = ?', $id);

        $invoice = $db->fetchRow($select);

        $selfPaid = self::getSelfPaid($id, $datefrom, $dateto);

        $advPaid = self::getAdvance($id, $datefrom, $dateto);

        $disMain = self::getDiscount($id, $datefrom, $dateto);

        $sponsorPaid = self::getSponsorPaid($id, $datefrom, $dateto);

        $cnPaid = self::getCreditNote($id, $datefrom, $dateto);

        $advamount = self::getAdvanceTransfer($id);

        $advamountRef = self::getAdvanceTransferRefundable($id);

        $check = $this->getFeeItemMain($id);

        $advPaidAmount = 0.00;
        $sponsorPaidAmount = 0.00;
        $advTransferAmount = 0.00;
        if ($invoice['currency_id']!=1){
            //self paid
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($selfPaid[0]['selfpaid'] != null){
                    if ($selfPaid[0]['rcp_cur_id']!=1){
                        $selfPaidAmount = $selfPaid[0]['selfpaid'];
                    }else{
                        $exrate = $this->getRateByDate($invoice['currency_id'], $invoice['invoice_date']);
                        $selfPaidAmount = round($selfPaid[0]['selfpaid']/$exrate['cr_exchange_rate'], 2);
                    }
                }else{
                    $selfPaidAmount = 0.00;
                }
            }else{
                $selfPaidAmount = $invoice['bill_paid'];
            }

            //advance payment
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($advPaid) {
                    foreach ($advPaid as $advPaidLoop) {
                        if ($advPaidLoop['advpy_cur_id'] != 1) {
                            $advPaidAmount = $advPaidAmount + $advPaidLoop['advpydet_total_paid'];
                        } else {
                            $exrate = $this->getRateByDate($invoice['currency_id'], $advPaidLoop['advpydet_upddate']);
                            $advPaidAmount = $advPaidAmount + round($advPaidLoop['advpydet_total_paid'] / $exrate['cr_exchange_rate'], 2);
                        }
                    }
                } else {
                    $advPaidAmount = 0.00;
                }
            }else{
                $advPaidAmount = 0.00;
            }

            //discount payment
            if ($disMain[0]['discount'] != null){
                if ($disMain[0]['currency']!=1){
                    $disPaidAmount = $disMain[0]['discount'];
                }else{
                    $exrate = $this->getRateByDate($invoice['currency_id'], $disMain[0]['dcnt_create_date']);
                    $disPaidAmount = round($disMain[0]['discount']/$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $disPaidAmount = 0.00;
            }

            //sponsor payment
            if ($sponsorPaid){
                foreach ($sponsorPaid as $sponsorPaidLoop){
                    if ($sponsorPaidLoop['rcp_inv_cur_id']!=1){
                        $sponsorPaidAmount = $sponsorPaidAmount + $sponsorPaidLoop['amount'];
                    }else{
                        $exrate = $this->getRateByDate($invoice['currency_id'], $sponsorPaidLoop['rcp_receive_date']);
                        $sponsorPaidAmount = $sponsorPaidAmount + round($sponsorPaidLoop['amount']/$exrate['cr_exchange_rate'], 2);
                    }
                }
            }else{
                $sponsorPaidAmount = 0.00;
            }

            //credit note payment
            if ($cnPaid[0]['cnpaid'] != null){
                if ($cnPaid[0]['currency']!=1){
                    $cnPaidAmount = $cnPaid[0]['cnpaid'];
                }else{
                    $exrate = $this->getRateByDate($invoice['currency_id'], $cnPaid[0]['cn_create_date']);
                    $cnPaidAmount = round($cnPaid[0]['cnpaid']/$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $cnPaidAmount = 0.00;
            }

            //advance payment transfer
            if (!$check) {
                if ($advamount[0]['advanceamount'] != null) {
                    if ($advamount[0]['currency'] != 1) {
                        $advTransferAmount = $advamount[0]['advanceamount'];
                    } else {
                        $exrate = $this->getRateByDate($invoice['currency_id'], $advamount[0]['advpy_date']);
                        $advTransferAmount = round($advamount[0]['advanceamount'] / $exrate['cr_exchange_rate'], 2);
                    }
                } else {
                    $advTransferAmount = 0.00;
                }
            }else{
                if ($advamountRef[0]['advanceamount'] != null) {
                    if ($advamountRef[0]['currency'] != 1) {
                        $advTransferAmount = $advamountRef[0]['advanceamount'];
                    } else {
                        $exrate = $this->getRateByDate($invoice['currency_id'], $advamountRef[0]['advpy_date']);
                        $advTransferAmount = round($advamountRef[0]['advanceamount'] / $exrate['cr_exchange_rate'], 2);
                    }
                } else {
                    $advTransferAmount = 0.00;
                }
            }
        }else{
            //self paid
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($selfPaid[0]['selfpaid'] != null){
                    if ($selfPaid[0]['rcp_cur_id']==1){
                        $selfPaidAmount = $selfPaid[0]['selfpaid'];
                    }else{
                        $exrate = $this->getRateByDate($selfPaid[0]['rcp_cur_id'], $invoice['invoice_date']);
                        $selfPaidAmount = round($selfPaid[0]['selfpaid']*$exrate['cr_exchange_rate'], 2);
                    }
                }else{
                    $selfPaidAmount = 0.00;
                }
            }else{
                $selfPaidAmount = $invoice['bill_paid'];
            }

            //advance payment
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($advPaid) {
                    foreach ($advPaid as $advPaidLoop) {
                        if ($advPaidLoop['advpy_cur_id'] == 1) {
                            $advPaidAmount = $advPaidAmount + $advPaidLoop['advpydet_total_paid'];
                        } else {
                            $exrate = $this->getRateByDate($advPaidLoop['advpy_cur_id'], $advPaidLoop['advpydet_upddate']);
                            $advPaidAmount = $advPaidAmount + round($advPaidLoop['advpydet_total_paid'] * $exrate['cr_exchange_rate'], 2);
                        }
                    }
                } else {
                    $advPaidAmount = 0.00;
                }
            }else{
                $advPaidAmount = 0.00;
            }

            //discount payment
            if ($disMain[0]['discount'] != null){
                if ($disMain[0]['currency']==1){
                    $disPaidAmount = $disMain[0]['discount'];
                }else{
                    $exrate = $this->getRateByDate($disMain[0]['currency'], $disMain[0]['dcnt_create_date']);
                    $disPaidAmount = round($disMain[0]['discount']*$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $disPaidAmount = 0.00;
            }

            //sponsor payment
            if ($sponsorPaid){
                foreach ($sponsorPaid as $sponsorPaidLoop){
                    if ($sponsorPaidLoop['rcp_inv_cur_id']==1){
                        $sponsorPaidAmount = $sponsorPaidAmount + $sponsorPaidLoop['amount'];
                    }else{
                        $exrate = $this->getRateByDate($sponsorPaidLoop['rcp_inv_cur_id'], $sponsorPaidLoop['rcp_receive_date']);
                        $sponsorPaidAmount = $sponsorPaidAmount + round($sponsorPaidLoop['amount']*$exrate['cr_exchange_rate'], 2);
                    }
                }
            }else{
                $sponsorPaidAmount = 0.00;
            }

            //credit note payment
            if ($cnPaid[0]['cnpaid'] != null){
                if ($cnPaid[0]['currency']==1){
                    $cnPaidAmount = $cnPaid[0]['cnpaid'];
                }else{
                    $exrate = $this->getRateByDate($cnPaid[0]['currency'], $cnPaid[0]['cn_create_date']);
                    $cnPaidAmount = round($cnPaid[0]['cnpaid']*$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $cnPaidAmount = 0.00;
            }

            //advance payment transfer
            if (!$check) {
                if ($advamount[0]['advanceamount'] != null) {
                    if ($advamount[0]['currency'] == 1) {
                        $advTransferAmount = $advamount[0]['advanceamount'];
                    } else {
                        $exrate = $this->getRateByDate($advamount[0]['currency'], $advamount[0]['advpy_date']);
                        $advTransferAmount = round($advamount[0]['advanceamount'] * $exrate['cr_exchange_rate'], 2);
                    }
                } else {
                    $advTransferAmount = 0.00;
                }
            }else{
                if ($advamountRef[0]['advanceamount'] != null) {
                    if ($advamountRef[0]['currency'] == 1) {
                        $advTransferAmount = $advamountRef[0]['advanceamount'];
                    } else {
                        $exrate = $this->getRateByDate($advamountRef[0]['currency'], $advamountRef[0]['advpy_date']);
                        $advTransferAmount = round($advamountRef[0]['advanceamount'] * $exrate['cr_exchange_rate'], 2);
                    }
                } else {
                    $advTransferAmount = 0.00;
                }
            }
        }

        $balance = $invoice['bill_amount']-$selfPaidAmount-$advPaidAmount-$disPaidAmount-$sponsorPaidAmount-$cnPaidAmount+$advTransferAmount;

        $data = array(
            'bill_number'=>$invoice['bill_number'],
            'bill_balance'=>number_format($balance, 2, '.', ','),
            'bill_paid'=>number_format(($selfPaidAmount+$advPaidAmount+$sponsorPaidAmount), 2, '.', ','),
            'cn_amount'=>number_format($cnPaidAmount, 2, '.', ','),
            'dn_amount'=>number_format($disPaidAmount, 2, '.', ','),
            'sponsor'=>number_format($sponsorPaidAmount, 2, '.', ','),
            'selfpaid'=>number_format($selfPaidAmount+$advPaidAmount, 2, '.', ',')
        );

        return $data;
    }

    public function getBalanceDtl($id, $datefrom = null, $dateto = null){
        //$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

        $cronService = new cronService();
        $db = $cronService->conn;
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'), array('amountdtl'=>'a.amount'))
            ->join(array('b'=>'invoice_main'), 'a.invoice_main_id = b.id', array('b.*'))
            ->where('a.id = ?', $id);

        $invoice = $db->fetchRow($select);

        $selfPaid = self::getSelfPaidDtl($id, $datefrom, $dateto);

        $advPaid = self::getAdvanceDtl($id, $datefrom, $dateto);

        $disMain = self::getDiscountDtl($id, $datefrom, $dateto);

        $sponsorPaid = self::getSponsorPaidDtl($id, $datefrom, $dateto);

        $cnPaid = self::getCreditNoteDtl($id, $datefrom, $dateto);

        $check = $this->getFeeItemDtl($id);

        $advPaidAmount = 0.00;
        $sponsorPaidAmount = 0.00;
        if ($invoice['currency_id']!=1){
            //self paid
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($selfPaid[0]['selfpaid'] != null){
                    if ($selfPaid[0]['rcp_cur_id']!=1){
                        $selfPaidAmount = $selfPaid[0]['selfpaid'];
                    }else{
                        $exrate = $this->getRateByDate($invoice['currency_id'], $invoice['invoice_date']);
                        $selfPaidAmount = ($selfPaid[0]['selfpaid']/$exrate['cr_exchange_rate']);
                    }
                }else{
                    $selfPaidAmount = 0.00;
                }
            }else{
                $selfPaidAmount = $invoice['bill_paid'];
            }

            //advance payment
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($advPaid) {
                    foreach ($advPaid as $advPaidLoop) {
                        if ($advPaidLoop['advpy_cur_id'] != 1) {
                            $advPaidAmount = $advPaidAmount + $advPaidLoop['advpydet_total_paid'];
                        } else {
                            $exrate = $this->getRateByDate($invoice['currency_id'], $advPaidLoop['advpydet_upddate']);
                            $advPaidAmount = $advPaidAmount + ($advPaidLoop['advpydet_total_paid'] / $exrate['cr_exchange_rate']);
                        }
                    }
                } else {
                    $advPaidAmount = 0.00;
                }
            }else{
                $advPaidAmount = 0.00;
            }

            //discount payment
            if ($disMain[0]['discount'] != null){
                if ($disMain[0]['currency']!=1){
                    $disPaidAmount = $disMain[0]['discount'];
                }else{
                    $exrate = $this->getRateByDate($invoice['currency_id'], $disMain[0]['dcnt_create_date']);
                    $disPaidAmount = ($disMain[0]['discount']/$exrate['cr_exchange_rate']);
                }
            }else{
                $disPaidAmount = 0.00;
            }

            //sponsor payment
            if ($sponsorPaid){
                foreach ($sponsorPaid as $sponsorPaidLoop){
                    if ($sponsorPaidLoop['rcp_inv_cur_id']!=1){
                        $sponsorPaidAmount = $sponsorPaidAmount + $sponsorPaidLoop['amount'];
                    }else{
                        $exrate = $this->getRateByDate($invoice['currency_id'], $sponsorPaidLoop['rcp_receive_date']);
                        $sponsorPaidAmount = $sponsorPaidAmount + round($sponsorPaidLoop['amount']/$exrate['cr_exchange_rate'], 2);
                    }
                }
            }else{
                $sponsorPaidAmount = 0.00;
            }

            //credit note payment
            if ($cnPaid[0]['cnpaid'] != null){
                if ($cnPaid[0]['currency']!=1){
                    $cnPaidAmount = $cnPaid[0]['cnpaid'];
                }else{
                    $exrate = $this->getRateByDate($invoice['currency_id'], $cnPaid[0]['cn_create_date']);
                    $cnPaidAmount = ($cnPaid[0]['cnpaid']/$exrate['cr_exchange_rate']);
                }
            }else{
                $cnPaidAmount = 0.00;
            }
        }else{
            //self paid
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($selfPaid[0]['selfpaid'] != null){
                    if ($selfPaid[0]['rcp_cur_id']==1){
                        $selfPaidAmount = $selfPaid[0]['selfpaid'];
                    }else{
                        $exrate = $this->getRateByDate($selfPaid[0]['rcp_cur_id'], $invoice['invoice_date']);
                        $selfPaidAmount = ($selfPaid[0]['selfpaid']*$exrate['cr_exchange_rate']);
                    }
                }else{
                    $selfPaidAmount = 0.00;
                }
            }else{
                $selfPaidAmount = $invoice['bill_paid'];
            }

            //advance payment
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($advPaid) {
                    foreach ($advPaid as $advPaidLoop) {
                        if ($advPaidLoop['advpy_cur_id'] == 1) {
                            $advPaidAmount = $advPaidAmount + $advPaidLoop['advpydet_total_paid'];
                        } else {
                            $exrate = $this->getRateByDate($advPaidLoop['advpy_cur_id'], $advPaidLoop['advpydet_upddate']);
                            $advPaidAmount = $advPaidAmount + ($advPaidLoop['advpydet_total_paid'] * $exrate['cr_exchange_rate']);
                        }
                    }
                } else {
                    $advPaidAmount = 0.00;
                }
            }else{
                $advPaidAmount = 0.00;
            }

            //discount payment
            if ($disMain[0]['discount'] != null){
                if ($disMain[0]['currency']==1){
                    $disPaidAmount = $disMain[0]['discount'];
                }else{
                    $exrate = $this->getRateByDate($disMain[0]['currency'], $disMain[0]['dcnt_create_date']);
                    $disPaidAmount = ($disMain[0]['discount']*$exrate['cr_exchange_rate']);
                }
            }else{
                $disPaidAmount = 0.00;
            }

            //sponsor payment
            if ($sponsorPaid){
                foreach ($sponsorPaid as $sponsorPaidLoop){
                    if ($sponsorPaidLoop['rcp_inv_cur_id']==1){
                        $sponsorPaidAmount = $sponsorPaidAmount + $sponsorPaidLoop['amount'];
                    }else{
                        $exrate = $this->getRateByDate($sponsorPaidLoop['rcp_inv_cur_id'], $sponsorPaidLoop['rcp_receive_date']);
                        $sponsorPaidAmount = $sponsorPaidAmount + round($sponsorPaidLoop['amount']*$exrate['cr_exchange_rate'], 2);
                    }
                }
            }else{
                $sponsorPaidAmount = 0.00;
            }

            //credit note payment
            if ($cnPaid[0]['cnpaid'] != null){
                if ($cnPaid[0]['currency']==1){
                    $cnPaidAmount = $cnPaid[0]['cnpaid'];
                }else{
                    $exrate = $this->getRateByDate($cnPaid[0]['currency'], $cnPaid[0]['cn_create_date']);
                    $cnPaidAmount = ($cnPaid[0]['cnpaid']*$exrate['cr_exchange_rate']);
                }
            }else{
                $cnPaidAmount = 0.00;
            }
        }

        $balance = $invoice['amountdtl']-$selfPaidAmount-$advPaidAmount-$disPaidAmount-$sponsorPaidAmount-$cnPaidAmount;

        $data = array(
            'bill_number'=>$invoice['bill_number'],
            'bill_balance'=>number_format($balance, 2, '.', ''),
            'bill_paid'=>number_format(($selfPaidAmount+$advPaidAmount+$sponsorPaidAmount), 2, '.', ''),
            'cn_amount'=>number_format($cnPaidAmount, 2, '.', ''),
            'dn_amount'=>number_format($disPaidAmount, 2, '.', ''),
            'selfpaid'=>number_format($selfPaidAmount+$advPaidAmount, 2, '.', ''),
            'sponsorpaid'=>number_format($sponsorPaidAmount, 2, '.', ''),
        );

        return $data;
    }

    public function getRateByDate($currency_id,$date){

        $cronService = new cronService();
        $db = $cronService->conn;

        $dateTrans = date('Y-m-d', strtotime($date) );

        $selectData = $db->select()
            ->from(array('cr'=>'tbl_currency_rate'))
            ->where('cr.cr_cur_id = ?', $currency_id)
            ->where("cr.cr_effective_date <= '$dateTrans'")
            ->order('cr.cr_effective_date desc')
            ->limit(1);


        $row = $db->fetchRow($selectData);

        if(!$row){
            return null;
        }else{
            return $row;
        }

    }
}