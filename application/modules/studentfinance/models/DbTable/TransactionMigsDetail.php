<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_TransactionMigsDetail extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'transaction_migs_detail';
	protected $_primary = "tmd_id";


}