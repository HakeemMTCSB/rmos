<?php
class Studentfinance_Model_DbTable_CreditNote extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'credit_note';
	protected $_primary = "cn_id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name),array('*','cn_amount_cn'=>'cn.cn_amount','MigrateCodeCN'=>'MigrateCode'))
					->joinLeft(array('cnd' => 'credit_note_detail'), 'cnd.cn_id = cn.cn_id',array('*','cn_detail'=>'amount'))
					->joinLeft(array('iv' => 'invoice_main'), 'iv.id = cn.cn_invoice_id',array('*','cn_amount_inv'=>'cn_amount'))
					->joinLeft(array('cur' => 'tbl_currency'), 'cn.cn_cur_id=cur.cur_id')
					->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=cn.cn_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=cn.cn_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
					->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
					->group('cnd.cn_id');
		
		if($id!=0){
			$selectData->where("cn.cn_id = ?",$id);
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getDataByInvoice($invoice_no,$active=null,$invoice_detail_id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->join(array('s'=>'credit_note_detail'), 's.cn_id = cn.cn_id',array('*'))
					->where('cn.cn_invoice_id = ?', $invoice_no);
		if($invoice_detail_id){
			$selectData->where('s.invoice_detail_id = ?', $invoice_detail_id);
		}

		if($active!=null){
			if($active){
				$selectData->where('cn.cn_cancel_date is null');
			}else{
				$selectData->where('cn.cn_cancel_date is not null');
			}
		}
		
		//echo $selectData;
		$row = $db->fetchRow($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
	
	public function getAllDataByInvoice($invoice_no){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->where('cn.cn_invoice_id = ?', $invoice_no)
					->where('cn.cn_status = "A"');

		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
	
	public function getPaginateData($approve=null, $cancel=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('cn'=>$this->_name))
				->join(array('ap'=>'applicant_profile'),'ap.appl_id = cn.appl_id', array("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)name"))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
				->joinLeft(array('tu2'=>'tbl_user'),'tu.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
				->joinLeft(array('tu3'=>'tbl_user'),'tu.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
	
		if(isset($approve)){
			if($approve==false){
				$select->where("cn.cn_approver is null");
				$select->where("cn.cn_approve_date is null");
			}else
			if($approve==true){
				$select->where("cn.cn_approver is not null");
				$select->where("cn.cn_approve_date is not null");
			}
		}
				
		if(isset($cancel)){				
			if(!$cancel){
				$select->where("cn.cn_cancel_by is null");
				$select->where("cn.cn_cancel_date is null");
			}else 
			if($cancel){
				$select->where("cn.cn_cancel_by is not null");
				$select->where("cn.cn_cancel_date is not null");
			}
		}
				
		return $select;
	}
	
	public function getPaginateDataHistory($approve=null, $cancel=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('cn'=>$this->_name))
				->join(array('ap'=>'applicant_profile'),'ap.appl_id = cn.appl_id', array("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)name"))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
				->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
				->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
	
		if(isset($approve)){
			if($approve==false){
				$select->where("cn.cn_approver = null");
				$select->where("cn.cn_approve_date = null");
			}else
			if($approve==true){
				$select->where("cn.cn_approver is not null");
				$select->where("cn.cn_approve_date is not null");
			}
		}

		$select2 = $db->select()
				->from(array('cn'=>$this->_name))
				->join(array('ap'=>'applicant_profile'),'ap.appl_id = cn.appl_id', array("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)name"))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
				->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
				->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
				
		if(isset($cancel)){				
			if(!$cancel){
				$select2->where("cn.cn_cancel_by = null");
				$select2->where("cn.cn_cancel_date = null");
			}else 
			if($cancel){
				$select2->where("cn.cn_cancel_by is not null");
				$select2->where("cn.cn_cancel_date is not null");
			}
		}
		
		$selectUnion = $db
							->select()
							->union(array($select, $select2))
							->order('cn_create_date desc');

		return $selectUnion;
	}
	
public function getPaginateListData($where=array())
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
	                
					->joinLeft(array('cur' => 'tbl_currency'), 'a.cn_cur_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					
					
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.cn_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = a.cn_cancel_by', array())
					->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancelName'=>'tsa.FullName'))
					->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = a.cn_approver', array())
					->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('updName'=>'tsb.FullName'))
					->joinLeft(array('iv' => 'invoice_main'), 'iv.id = a.cn_invoice_id', array())
					;
		
		if ( !empty($where) )
		{
			if ( $where['type'] == 1 )
			{
				
				$select->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.cn_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=a.cn_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','ProgramCode'));
					
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('st.at_pes_id LIKE ?', '%'.$where['id'].'%');
				}

				/*$select->where('a.appl_id IS NOT NULL','')
					   ->where('a.IdStudentRegistration IS NULL','');*/
			}
			
			if ( $where['type'] == 2 )
			{
				$select->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.cn_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('pa' => 'tbl_program'), 'pa.IdProgram=sr.IdProgram', array('ProgramName','ProgramCode'));
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sp.appl_fname,sp.appl_mname,sp.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('sr.registrationId LIKE ?', '%'.$where['id'].'%');
				}

//				$select->where('a.appl_id != ?','')
//					   ->where('a.IdStudentRegistration != ?','');
			}
			
			if ( isset($where['invoice_id'] )){
				$select->where('a.cn_billing_no LIKE ?', '%'.$where['invoice_id'].'%');
			}
			
			$select->group('a.cn_id');
			$select->order('cn_id DESC');
		}

		return $select;
	}
	
	
	public function getSearchDataReport($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$scheme = $searchData['IdProgramScheme'];
		
		$selectData = $db->select()
						->from(array('adv'=>'advance_payment_detail'))
						->joinLeft(array('ra'=>'advance_payment'), 'ra.advpy_id = adv.advpydet_advpy_id',array('advpy_total_balance','advpy_cur_id','MigrateCodeAdv'=>'ra.MigrateCode','advpy_description','advpy_invoice_id','advpy_refund_id','advpy_invoice_no'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = ra.advpy_cur_id',array('cur_id','cur_code'))
						->joinLeft(array('r'=>'receipt'), 'ra.advpy_rcp_id = r.rcp_id',array('rcp_UpdDate'=>'UpdDate','rcp_no','codeMigrate'=>'r.MigrateCode','rcp_receive_date','rcp_cur_id','rcp_amount','rcp_gainloss','rcp_gainloss_amt','rcp_amount_default_currency'))
						->joinLeft(array('p'=>'payment'), 'p.p_rcp_id = r.rcp_id',array('p_migs','p_payment_mode_id','p_card_no','p_cheque_no'))
						->joinLeft(array('pm'=>'payment_mode'), 'pm.id = p.p_payment_mode_id',array('payment_mode'))
						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = ra.advpy_trans_id',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = ra.advpy_idStudentRegistration',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme OR str.IdProgramScheme = e.IdProgramScheme',array())
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			            ->joinLeft(array('ch'=>'tbl_definationms'), 'pm.payment_group = ch.idDefinition', array('payment_group_name'=>'ch.DefinitionDesc'))
//			            ->joinLeft(array('rci'=>'receipt_invoice'),'rci.rcp_inv_rcp_id=r.rcp_id',array('rcp_inv_amount'))
//		
						->joinLeft(array('iv'=>'invoice_main'),'adv.advpydet_inv_id=iv.id',array('invoice_no'=>'iv.bill_number','exchange_rate','invoice_date'))
						->joinLeft(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id=iv.id and adv.advpydet_inv_det_id = ivd.id',array('idDet'=>'ivd.id','amount','invoice_main_id'))
						->joinLeft(array('iv2'=>'invoice_main'),'ra.advpy_invoice_id=iv2.id',array('fromitem'=>'iv2.bill_description'))
						//->joinLeft(array('ivd2'=>'invoice_detail'),'ivd.invoice_main_id=iv2.id and adv.advpydet_inv_det_id = ivd.id',array('idDet'=>'ivd.id','amount','invoice_main_id'))
						//'GROUP_CONCAT(iv.bill_number) as invoice_no'
						->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = iv.semester',array('SemesterMainName'))
						->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = ivd.fi_id',array('fi_name','fi_code'))
						->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id',array('fc_desc'))
						
						->joinLeft(array('fpre'=>'fee_structure'), 'fpre.fs_id = str.fs_id',array('fs_prepayment_ac'))
						->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fpre.fs_prepayment_ac',array('ac_code','ac_desc'))
			//			->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=iv.id and ivs.invoice_detail_id = ivd.id')
			//			->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('GROUP_CONCAT(sc.SubCode) as subjectCode'))
						->joinLeft(array('mg'=>'transaction_migs'),'mg.mt_id=p.p_migs',array('mt_txn_no','mt_from'))
						->where("adv.advpydet_inv_det_id !=0")
//						->where("iv.bill_number is not null")
//						->group('iv.id')
						->where('adv.advpydet_status = "A"')
						->where('ra.advpy_status = "A"')
			//			->group('ivd.id')
			//			->group('ivd.fi_id')
			
			->order('adv.advpydet_upddate desc')
			->order('ra.advpy_fomulir desc');
							
		if($dateFrom!=''){
			$selectData->where("DATE(adv.advpydet_upddate) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(adv.advpydet_upddate) <= ?",$dateTo);
		}
		if(isset($program) && !empty($program)){
//			$selectData->where("app.ap_prog_id = ?",$program);
			$selectData->where("pa.IdProgram = ?",$program);
		}
		
		if(isset($scheme) && !empty($scheme)){
//			$selectData->where("app.ap_prog_scheme = ?",$scheme);
			$selectData->where("e.IdProgramScheme = ?",$scheme);
		}
		
		$selectData->group('adv.advpydet_id');
		$auth = Zend_Auth::getInstance();
		/*if($auth->getIdentity()->id == 1){
			echo $selectData;
		}*/
		$row = $db->fetchAll($selectData);
		if($row){
			$m=0;
				
						foreach($row as $rci){
							 if($rci['advpy_invoice_id']){
								$selectSubject = $db->select()
											->from(array('a'=>'credit_note_detail'))
											->joinLeft(array('sc'=>'credit_note'), 'sc.cn_id = a.cn_id',array('cn_billing_no'))
											->where("sc.cn_invoice_id = ?", (int)$rci['advpy_invoice_id']);
							
								$receiptSubject = $db->fetchRow($selectSubject);
								$row[$m]['cn'] = $receiptSubject['cn_billing_no'];
								
							}
//							$row[$m]['cn'] = null;
							$m++;
						}
						
						
						
					}
		return $row;
	}
	
	public function getSearchDataCN($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoiceID = $searchData['invoice_id'];
		$invoiceDetailID = $searchData['invoice_detail_id'];
		
		$selectData = $db->select()
						->from(array('a'=>'credit_note_detail'),array('cur_id','cnamountdet'=>'a.amount'))
						->join(array('sc'=>'credit_note'), 'sc.cn_id = a.cn_id',array('cnamountmain'=>'sc.cn_amount','*'))
						->where('a.invoice_main_id = ?',$invoiceID)
						->where('a.invoice_detail_id = ?',$invoiceDetailID)
						->where("sc.cn_status ='A'");
						//->group('a.cn_id')
						//->group('a.invoice_main_id')
						//->group('a.invoice_detail_id');
											
		if($dateFrom!=''){
			$selectData->where("DATE(sc.cn_create_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(sc.cn_create_date) <= ?",$dateTo);
		}

		$row = $db->fetchAll($selectData);

		$newRow = array();

		$cnAmount = 0;
		foreach($row as $row){
			$newRow = $row;
			$cnAmount += $row['cnamountdet'];
			$newRow['cnamount'] = $cnAmount;

		}

		return $newRow;
	}
	
}

