<?php

class Studentfinance_Model_DbTable_FeeItemState extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'fee_item_state';
	protected $_primary = "fis_id";

	public function getItemData($fee_id, $idState, $cur){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsp'=>$this->_name))
					->join(array('fi'=>'fee_item'),'fi.fi_id = fsp.fic_fi_id')
					->where("fsp.fic_fi_id= ?", $fee_id)
					->where("fsp.fic_idState = ?", $idState)
					->where('fsp.fic_cur_id = ?',$cur);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('fic'=>$this->_name));

		if($id!=0){
			$selectData->where("fic.fic_id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
		}else{
				
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;
		}

	}
	
	/*
	 * Get fee item by id and country
	 */
	public function getFeeItemByCountry($fic_fi_id, $fic_country_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('fic'=>$this->_name))
					->where("fic.fic_fi_id = ?",$fic_fi_id)
					->where("fic.fic_idCountry = ?",$fic_country_id);
		
				
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	/*
	 * Get country list
	 */
	public function getCountryStateFeeItem($row_format=false, $idCountry=null,$feeitem=null,$idState=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($row_format==true){
			
			$selectData = $db->select()
							 ->from(array('c'=>'tbl_state'))
							 ->joinLeft(array('fic'=>$this->_name),'fic.fic_idState = c.idState');
			
			if($idCountry){
				$selectData->where('c.idCountry = ?', $idCountry);
			}
			
			if($idState){
				$selectData->where('c.idState = ?', $idState);
			}
			
			$row = $db->fetchAll($selectData);
			
				
			if(!$row){
				return null;
			}else{
				return $row;
			}
			
		}else{
			
			$selectData = $db->select()
							 ->from(array('c'=>'tbl_state'))
							 ->joinLeft(array('fic'=>'tbl_countries'),'fic.idCountry = c.idCountry')
							 ->where('c.idCountry = ? ', $idCountry);

			if($idState){
				$selectData->where('c.idState = ?', $idState);
				
				$rows = $db->fetchRow($selectData);
			
			}else{
				$rows = $db->fetchAll($selectData);
			}
				
			if($rows){
				
				if(isset($rows[0])){
					foreach ($rows as $index=>$country){
					
						$selectData2 = $db->select()
						->from(array('fic'=>$this->_name))
						->where('fic.fic_idCountry = ? ', $idCountry)
						->where('fic.fic_idState = ?', $country['idState']);
						
						if ( $feeitem )
						{
							$selectData2->where('fic.fic_fi_id = ?',$feeitem);
						}
							
						$row = $db->fetchAll($selectData2);

		
							
						if($row){
							$rows[$index]['fees'] = $row;
					
							//fee index by currency
							foreach ($row as $cur){
								$rows[$index]['fees_by_currency'][$cur['fic_cur_id']] = $cur;
							}
						}
					}	
				}else{
					$selectData2 = $db->select()
					->from(array('fic'=>$this->_name))
					->where('fic.fic_idCountry = ? ', $idCountry)
					->where('fic.fic_idState = ?', $idState);
					
					if ( $feeitem )
					{
						$selectData2->where('fic.fic_fi_id = ?',$feeitem);
					}
					$row = $db->fetchAll($selectData2);
						
					if($row){
						$rows['fees'] = $row;
							
						//fee index by currency
						foreach ($row as $cur){
							$rows['fees_by_currency'][$cur['fic_cur_id']] = $cur;
						}
					}
				}
				
				
			}
			
			if(!$rows){
				return null;
			}else{
				return $rows;
			}
			
		}
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		$data['fic_added_by'] = $auth->getIdentity()->iduser;
		$data['fic_added_date'] = date('Y-m-d H:i:s');
			
		return parent::insert($data);
	}
	
	public function update(array $data, $condition){
	
		$auth = Zend_Auth::getInstance();
	
		$data['fic_updated_by'] = $auth->getIdentity()->iduser;
		$data['fic_updated_date'] = date('Y-m-d H:i:s');
			
		return parent::update($data,$condition);
	}
}