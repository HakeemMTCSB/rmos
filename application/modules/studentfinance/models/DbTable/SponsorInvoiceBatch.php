<?php
class Studentfinance_Model_DbTable_SponsorInvoiceBatch extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'sponsor_invoice_batch';
	protected $_primary = "id";
		
	public function getData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.created_by', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = cn.semester_id', array('SemesterMainName','SemesterMainCode'))
					->order('cn.id desc');
		
		$row = $db->fetchAll($selectData);
		
		$sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
    	$scholarDb = new Studentfinance_Model_DbTable_Scholarship();
    		
		foreach($row as $key=>$data){
			if($data['sp_type'] == 1){//sponsorship
				$infodetail = $sponsorDb->fngetsponsorbyid($data['sp_type_id']);
			}elseif($data['sp_type'] == 2){//scholarship
				$infodetail = $scholarDb->getScholarshipById($data['sp_type_id']);
			}
			
			$row[$key]['sp_name'] = $infodetail['name'];
			$row[$key]['sp_code'] = $infodetail['code'];
		}
		return $row;	
		
	}
	
	public function getDataDetail($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.created_by', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = cn.semester_id', array('SemesterMainName'))
					->where("cn.id = ?",$id);
		
			$row = $db->fetchRow($selectData);
		
		return $row;	
		
	}
	
	public function insert(array $data){
		
		return parent::insert($data);
	}
	
	public function getBatchType($type, $id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>$this->_name))
					->where('d.sp_type =?',$type)
					->where('d.sp_type_id =?',$id);
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
}

