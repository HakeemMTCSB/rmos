<?php
class Studentfinance_Model_DbTable_InvoiceDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'invoice_detail';
	protected $_primary = "id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('idtl'=>$this->_name))
					->join(array('a'=>'invoice_main'), 'a.id = idtl.invoice_main_id',array())
					->join(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					->where("idtl.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getDetail($invoice_main_id,$balcheck=0){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
		->from(array('id'=>$this->_name))
		->join(array('a'=>'invoice_main'), 'a.id = id.invoice_main_id',array('bill_number','bill_amount','bill_paid','bill_balance','status','IdStudentRegistration','trans_id','bill_description','invoice_date','fs_id','currency_id','MigrateCode'))
		->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = id.id',array('amount_subject'=>'ivs.amount'))
		->joinLeft(array('sb'=>'tbl_subjectmaster'), 'sb.IdSubject = ivs.subject_id')
		->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = id.fi_id')
		->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id')
		->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = a.semester')
		->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = id.cur_id')
		//->joinLeft(array('fsi'=>'fee_structure_item'), 'fsi.fsi_item_id = id.fi_id and fsi.fsi_structure_id = a.fs_id',array())
		->joinLeft(array('fsi'=>'fee_structure_item'), 'fsi.fsi_id = (SELECT fsifsi.fsi_id FROM fee_structure_item as fsifsi WHERE id.fi_id = fsifsi.fsi_item_id AND fsifsi.fsi_structure_id = a.fs_id LIMIT 1)',array())
		->joinLeft(array('ic'=>'tbl_definationms'), 'fsi.fsi_registration_item_id = ic.idDefinition', array('item_name'=>'ic.DefinitionDesc','item_code'=>'ic.DefinitionCode'))
		->joinLeft(array('dsct'=>'discount_detail'), 'dsct.dcnt_invoice_id = a.id and dsct.dcnt_invoice_det_id = id.id',array('dcnt_amount'))
		->where("a.id = ?", (int)$invoice_main_id);
		//->group("fsi.fsi_item_id");
		
		if ( $balcheck ) 
		{
			$selectData->where("id.balance != '0.00'");
		}

		$row = $db->fetchAll($selectData);
		return $row;
	}

	public function getInvoiceDetailItem($billing_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>'invoice_main'))
					->join(array('idtl'=>'invoice_Detail'), 'idtl.invoice_main_id = im.id')
					->where("im.bill_number ?", $billing_id);

		$row = $db->fetchRow($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
	
	public function getInvoiceDetail($invoice_id,$balcheck=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('id'=>$this->_name))
					->join(array('a'=>'invoice_main'), 'a.id = id.invoice_main_id',array('bill_number','bill_amount','bill_paid','bill_balance','status','IdStudentRegistration','trans_id','bill_description','invoice_date','fs_id','currency_id','MigrateCode','exchange_rate', 'invsem'=>'a.semester'))
					->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = id.fi_id')
                    ->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id')                    
					->joinLeft(array('hfi'=>'hostel_fee_invoices'), 'a.id = hfi.invoice_id',array('invoice_id','IdHostelregistration','monthyear','note'))
					->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = a.semester')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = id.cur_id')
					->joinLeft(array('fsi'=>'fee_structure_item'), 'fsi.fsi_item_id = id.fi_id and fsi.fsi_structure_id = a.fs_id',array())
					->joinLeft(array('ic'=>'tbl_definationms'), 'fsi.fsi_registration_item_id = ic.idDefinition', array('item_name'=>'ic.DefinitionDesc','item_code'=>'ic.DefinitionCode'))	
					->joinLeft(array('dsct'=>'discount_detail'), 'dsct.dcnt_invoice_id = a.id and dsct.dcnt_invoice_det_id = id.id',array('dcnt_amount'))
					->where("a.id = ?", $invoice_id)
					->group('id.id');
					
		if ( $balcheck ) 
		{
			//$selectData->where("id.balance > 0");
		}

		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
}
?>