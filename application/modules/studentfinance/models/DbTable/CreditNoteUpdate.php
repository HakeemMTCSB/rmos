<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 17/10/2016
 * Time: 2:19 PM
 */
class Studentfinance_Model_DbTable_CreditNoteUpdate extends Zend_Db_Table_Abstract {

    public function getCreditNote($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'credit_note'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.cn_IdStudentRegistration = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->join(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->where('a.cn_status = ?', 'A');

        if ($search != false){
            if (isset($search['name']) && $search['name']!=''){
                $select->where($db->quoteInto('CONCAT_WS(" ", c.appl_fname, c.appl_lname) like ?', '%'.$search['name'].'%'));
            }

            if (isset($search['id']) && $search['id']!=''){
                $select->where($db->quoteInto('b.registrationId like ?', '%'.$search['id'].'%'));
            }

            if (isset($search['cn_no']) && $search['cn_no']!=''){
                $select->where($db->quoteInto('a.cn_billing_no like ?', '%'.$search['cn_no'].'%'));
            }

            if (isset($search['uti_date']) && $search['uti_date']!=''){
                $select->where($db->quoteInto('a.cn_create_date = ?', date('Y-m-d', strtotime($search['uti_date']))));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateCreditNote($data , $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('credit_note', $data, 'cn_id = '.$id);
        return $update;
    }
}