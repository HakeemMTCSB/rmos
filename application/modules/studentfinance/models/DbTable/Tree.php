<?php

class Studentfinance_Model_DbTable_Tree
{
  protected $leafIndex = array();
  protected $tree      = array();
  protected $stack;

  function __construct( $data )
  {

    $this->stack = $data;

    while( count( $this->stack ) )
    {
      $this->branchify( array_shift( $this->stack ) );
    }
  }

  protected function branchify( &$leaf )
  {
    // Root-level leaf?
    if ( null === $leaf['IdParent'] )
    {
      $this->addLeaf( $this->tree, $leaf );
    }
    // Have we found this leaf's parent yet?
    else if ( isset( $this->leafIndex[$leaf['IdParent']] ) )
    {
      $this->addLeaf( $this->leafIndex[$leaf['IdParent']]['children'], $leaf );
    } else {
      // Nope, put it back on the stack
      $this->stack[] = $leaf;
    }
  }

  protected function addLeaf( &$branch, $leaf )
  {
    // Add the leaf to the branch
    $branch[] = array(
        'id'       => $leaf['id']
      , 'name'     => $leaf['name']
      , 'data'     => new stdClass
      , 'children' => array()
    );

    // Store a reference so we can do an O(1) lookup later
    $this->leafIndex[$leaf['id']] = &$branch[count($branch)-1];
  }

  protected function addChild( $branch, $leaf )
  {
    $this->leafIndex[$leaf['id']] &= $branch['children'][] = $leaf;
  }

  public function getTree()
  {
    return $this->tree;
  }
}
	
