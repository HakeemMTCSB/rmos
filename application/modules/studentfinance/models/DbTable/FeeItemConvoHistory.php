<?php
class Studentfinance_Model_DbTable_FeeItemConvoHistory extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_item_convo_history';
	protected $_primary = "fv_id";
		
	public function getData($cid=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fe'=>$this->_name),array('*','UpdDateFee'=>'fe.UpdDate'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = fe.fv_currency_id')
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = fe.fv_fi_id')
					->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
					->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
					->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = fe.UpdUser', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					;
		
		if($cid!=0){
			$selectData->where("fe.fv_convo =?",$cid);
			
			$row = $db->fetchAll($selectData);
			
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	
	public function deleteData($id=null){
		if($id!=null){
			$data = array(
				'fi_active' => 0				
			);
				
			$this->update($data, "fi_id = '".$id."'");
		}
	}	

}