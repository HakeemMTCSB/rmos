<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_ProformaInvoiceMain extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'proforma_invoice_main';
	protected $_primary = "id";

	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('a'=>$this->_name))
						->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=a.appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
						->joinLeft(array('cur' => 'tbl_currency'), 'a.currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
						->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.trans_id', array('st.at_pes_id'))
							->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = a.semester')
						->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId'))
						->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
						->where("a.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getDetail($id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
						->from(array('im'=>$this->_name))
						->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = im.semester')
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = im.currency_id')
						->where("im.id = ?", (int)$id);
	
		$row = $db->fetchRow($selectData);
		return $row;
	}

	public function getPaginateData($where=array())
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=a.appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=a.trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','ProgramCode'))
					->joinLeft(array('cur' => 'tbl_currency'), 'a.currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.trans_id', array('st.at_pes_id'))
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = a.cancel_by', array())
					->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancelName'=>'tsa.FullName'))
					->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = a.updBy', array())
					->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('updName'=>'tsb.FullName'))
					;
		
		if ( !empty($where) )
		{
			if ( $where['type'] == 1 )
			{
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('st.at_pes_id LIKE ?', '%'.$where['id'].'%');
				}

				$select->where('a.appl_id IS NOT NULL','')
					   ->where('a.IdStudentRegistration IS NULL','');
			}
			
			if ( $where['type'] == 2 )
			{
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sp.appl_fname,sp.appl_mname,sp.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('sr.registrationId LIKE ?', '%'.$where['id'].'%');
				}

				$select->where('a.appl_id != ?','')
					   ->where('a.IdStudentRegistration != ?','');
			}
			
			if ( isset($where['proforma_id'] )){
				$select->where('a.bill_number LIKE ?', '%'.$where['proforma_id'].'%');
			}
			
			if ($where['status'] != ""){
				$select->where('a.status IN (?)', $where['status']);
			}
			
			if ($where['category'] != 0){
				$select->where('a.fee_category = ?', $where['category']);
			}
			
			$select->order('id DESC');
		}

		return $select;
	}
	
	public function getApplicantProformaIssued($appl_id, $semester_id){
		
	}
	
	/*
	 * Overite Insert function
	*/
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['creator'])){
			$data['creator'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['date_create'])  ){
			$data['date_create'] = date('Y-m-d H:i:s');
		}
	
		return parent::insert( $data );
	}
    
    public function getDataByTxnId($txnId,$fee_category){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
                    ->from(array('a'=>$this->_name))
                    ->joinLeft(array('b'=>'tbl_currency'), 'a.currency_id = b.cur_id', array('currencyName'=>'b.cur_code'))
                    ->where("a.trans_id = ?", (int)$txnId)
                    ->where("a.fee_category IN (?)", $fee_category)
                    ->where("a.status = ?", 'A');

		$row = $db->fetchRow($selectData);
		return $row;
	}

	public function getFee($txnId,$fee_category){

		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
			->from(array('a'=>$this->_name))
			->joinLeft(array('b'=>'tbl_currency'), 'a.currency_id = b.cur_id', array('currencyName'=>'b.cur_code'))
			->where("a.trans_id = ?", (int)$txnId)
			->where("a.fee_category IN (".$fee_category.")")
			->where("a.status = ?", 'A');

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getDataByInvoice($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
                    ->from(array('a'=>$this->_name))
                    ->where("a.invoice_id = ?", (int)$id)
                    ->where("a.status = ?", 'A');

		$row = $db->fetchRow($selectData);
		return $row;
	}
}