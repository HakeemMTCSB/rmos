<?php
/**
 * @author Suliana <suliana@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_AccountStatement extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'proforma_invoice_main';
	protected $_primary = "id";

	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('a'=>$this->_name))
						->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=a.appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
						->joinLeft(array('cur' => 'tbl_currency'), 'a.currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
						->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.trans_id', array('st.at_pes_id'))
							->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = a.semester')
						->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId'))
						->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
						->where("a.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getDetail($id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
						->from(array('im'=>$this->_name))
						->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = im.semester')
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = im.currency_id')
						->where("im.id = ?", (int)$id);
	
		$row = $db->fetchRow($selectData);
		return $row;
	}

	public function getPaginateData($where=array())
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if ( $where['type'] == 1 )
			{
				$select = $db->select()
	                ->from(array('sa'=>'applicant_profile'), array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as name','MigrateCode'))
	                ->joinLeft(array('st' => 'applicant_transaction'), 'st.at_appl_id=sa.appl_id', array('pes_id'=>'st.at_pes_id','transID'=>'at_trans_id'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','ProgramCode'))
					->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=st.at_intake', array('IntakeDesc'))
					->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ap.program_mode', array('ProgramMode'=>'DefinitionDesc'))
					  ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_study', array('StudyMode'=>'DefinitionDesc'))
					  ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ap.program_type', array('ProgramType'=>'DefinitionDesc'))
					  ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=sa.appl_category', array('appl_category_name'=>'DefinitionDesc'))
					  ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=st.at_status', array('profile_status'=>'deftn.DefinitionDesc'));
					
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('st.at_pes_id LIKE ?', '%'.$where['id'].'%');
				}
				
				if ( isset($where['IdIntake'])  && $where['IdIntake'] != '' )
				{
					$select->where('st.at_intake = ?', $where['IdIntake']);
				}
				
				if ( isset($where['program'])  && $where['program'] != '' )
				{
					$select->where('ap.ap_prog_id = ?', $where['program']);
				}

				$select->where('sa.appl_id IS NOT NULL','');
					   
			}
			
		if ( $where['type'] == 2 )
			{
				$select = $db->select()
	                ->from(array('sr'=>'tbl_studentregistration'), array('pes_id'=>'sr.registrationId','transID'=>'sr.IdStudentRegistration'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as name','MigrateCode'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','ProgramCode'))
					->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('IntakeDesc'))
					->join(array('ap' => 'tbl_program_scheme'), 'ap.IdProgramScheme=sr.IdProgramScheme', array())
					->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ap.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
					  ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
					  ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ap.program_type', array('ProgramType'=>'DefinitionDesc'))
					  ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=sp.appl_category', array('appl_category_name'=>'DefinitionDesc'))
					  ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=sr.profileStatus', array('profile_status'=>'deftn.DefinitionDesc'));
					
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sp.appl_fname,sp.appl_mname,sp.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('sr.registrationId LIKE ?', '%'.$where['id'].'%');
				}
				if ( isset($where['IdIntake'])  && $where['IdIntake'] != '' )
				{
					$select->where('sr.IdIntake IN (?)', $where['IdIntake']);
				}
				
				if ( isset($where['program'])  && $where['program'] != '' )
				{
					$select->where('sr.IdProgram IN (?)', $where['program']);
				}
				
				if ( isset($where['status'])  && $where['status'] != '' )
				{
					$select->where('sr.ProfileStatus IN (?)', $where['status']);
				}

//				$select->where('sr.IdStudentRegistration != ?','');
			}
			
			
//			echo $select;
//exit;
		return $select;
	}
	
	public function getPaginateDataDownload($where=array())
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if ( $where['type'] == 1 )
			{
				$select = $db->select()
	                ->from(array('sa'=>'applicant_profile'), array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as name'))
	                ->joinLeft(array('st' => 'applicant_transaction'), 'st.at_appl_id=sa.appl_id', array('pes_id'=>'st.at_pes_id','transID'=>'at_trans_id'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','ProgramCode'))
					->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=st.at_intake', array('IntakeDesc'))
					->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ap.program_mode', array('ProgramMode'=>'DefinitionDesc'))
					  ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_study', array('StudyMode'=>'DefinitionDesc'))
					  ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ap.program_type', array('ProgramType'=>'DefinitionDesc'))
					  ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=sa.appl_category', array('appl_category_name'=>'DefinitionDesc'))
					  ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=st.at_status', array('profile_status'=>'deftn.DefinitionDesc'));
					
				if ( isset($where['download'])  && $where['download'] != '' )
				{
					$select->where('st.at_trans_id IN (?)', $where['download']);
				}
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('st.at_pes_id LIKE ?', '%'.$where['id'].'%');
				}

				if ( isset($where['IdIntake'])  && $where['IdIntake'] != '' )
				{
					$select->where('st.at_intake = ?', $where['IdIntake']);
				}

				if ( isset($where['program'])  && $where['program'] != '' )
				{
					$select->where('ap.ap_prog_id = ?', $where['program']);
				}

				$select->where('sa.appl_id IS NOT NULL','');
					   
			}
			
		if ( $where['type'] == 2 )
			{
				$select = $db->select()
	                ->from(array('sr'=>'tbl_studentregistration'), array('pes_id'=>'sr.registrationId','transID'=>'sr.IdStudentRegistration'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','ProgramCode'))
					->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('IntakeDesc'))
					->join(array('ap' => 'tbl_program_scheme'), 'ap.IdProgramScheme=sr.IdProgramScheme', array())
					->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ap.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
					  ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
					  ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ap.program_type', array('ProgramType'=>'DefinitionDesc'))
					  ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=sp.appl_category', array('appl_category_name'=>'DefinitionDesc'))
					  ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=sr.profileStatus', array('profile_status'=>'deftn.DefinitionDesc'));
					
				if ( isset($where['download'])  && $where['download'] != '' )
				{
					$select->where('sr.IdStudentRegistration IN (?)', $where['download']);
				}
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sp.appl_fname,sp.appl_mname,sp.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('sr.registrationId LIKE ?', '%'.$where['id'].'%');
				}
				if ( isset($where['IdIntake'])  && $where['IdIntake'] != '' )
				{
					$select->where('sr.IdIntake IN (?)', $where['IdIntake']);
				}

				if ( isset($where['program'])  && $where['program'] != '' )
				{
					$select->where('sr.IdProgram IN (?)', $where['program']);
				}

				if ( isset($where['status'])  && $where['status'] != '' )
				{
					$select->where('sr.ProfileStatus IN (?)', $where['status']);
				}

					   
			}
//			echo $select;exit;
		return $select;
	}
	
	public function getApplicantProformaIssued($appl_id, $semester_id){
		
	}
	
	/*
	 * Overite Insert function
	*/
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['creator'])){
			$data['creator'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['date_create'])  ){
			$data['date_create'] = date('Y-m-d H:i:s');
		}
	
		return parent::insert( $data );
	}
    
    public function getDataByTxnId($txnId,$fee_category){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('a'=>$this->_name))
						->joinLeft(array('b'=>'tbl_currency'), 'a.currency_id = b.cur_id', array('currencyName'=>'b.cur_code'))
						->where("a.trans_id = ?", (int)$txnId)
						->where("a.fee_category = ?", (int)$fee_category);

		$row = $db->fetchRow($selectData);
		return $row;
	}
}