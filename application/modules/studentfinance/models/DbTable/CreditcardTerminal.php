<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */

class Studentfinance_Model_DbTable_CreditcardTerminal extends Zend_Db_Table_Abstract {
	
	/**
	 * The default table name
	 */
	protected $_name = 'creditcard_terminal';
	protected $_primary = "id";
	protected $_locale;
	
	public function init() {
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
	
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						->from(array('cct'=>$this->_name))
						->joinLeft(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = cct.branch_id', array('BranchName'=>'BranchName', 'BranchName2' => 'Arabic'));
	
		if($id!=0){
			$selectData->where("cct.id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
				
		}else{
				
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getTerminalList($active=1){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						->from(array('cct'=>$this->_name))
						->joinLeft(array('br'=>'tbl_branchofficevenue'),'br.IdBranch = cct.branch_id', array('BranchName'=>'BranchName', 'BranchName2' => 'Arabic'))
						->where('cct.active = ?', $active);
	
		$row = $db->fetchAll($selectData);
				
		return $row;
	}
	
	
}