<?php
class Studentfinance_Model_DbTable_FeeItemConvo extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_item_convo';
	protected $_primary = "fv_id";
		
	public function getData($cid=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fe'=>$this->_name),array('*','UpdDateFee'=>'fe.UpdDate'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = fe.fv_currency_id')
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = fe.fv_fi_id')
					->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
					->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
					->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = fe.UpdUser', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					;
		
		if($cid!=0){
			$selectData->where("fe.fv_convo =?",$cid);
			
			$row = $db->fetchAll($selectData);
			
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	
	public function getDataFee($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fe'=>$this->_name))
					;
		
		if($id!=0){
			$selectData->where("fe.fv_id =?",$id);
			
			$row = $db->fetchRow($selectData);
			
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getDataConvoFee($convoId,$feeId){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fe'=>$this->_name))
					->join(array('fi'=>'fee_item'),'fi.fi_id = fe.fv_fi_id')
					->join(array('cv'=>'convocation'),'cv.c_id = fe.fv_convo')
					->join(array('c'=>'tbl_currency'),'c.cur_id = fe.fv_currency_id')
					->where("fe.fv_convo =?",$convoId)
					->where("fe.fv_fi_id =?",$feeId)
					;
		
		$row = $db->fetchRow($selectData);
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		return parent::insert( $data );
	}
	
	public function deleteData($id=null){
		if($id!=null){	
			$this->delete("fv_id = '".$id."'");
		}
	}	

}