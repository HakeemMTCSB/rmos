<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_ReceiptInvoice extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'receipt_invoice';
	protected $_primary = "rcp_inv_id";

	
	public function getDataFromReceipt($receipt_id, $invoice_id=null){
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('ri'=>$this->_name))
					->where('ri.rcp_inv_rcp_id = ?', $receipt_id)
					->order('ri.rcp_inv_rcp_no DESC');

		if($invoice_id!=null){
			$selectData->where('ri.rcp_inv_invoice_id = ?', $invoice_id);
		}
		
		$row = $db->fetchAll($selectData);

		return $row;
	}
	
	public function getAllInvoicePaymentRecord($invoice_id,$status=null){
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('ri'=>$this->_name))
					->joinLeft(array('b'=>'receipt'),'b.rcp_id = ri.rcp_inv_rcp_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = b.rcp_cur_id')
					->where('ri.rcp_inv_invoice_id = ?', $invoice_id)
					
					->order('ri.rcp_inv_rcp_no DESC');

					if($status){
						$selectData->where("b.rcp_status = 'APPROVE'");
					}
		$row = $db->fetchAll($selectData);

		return $row;
	}

	public function insert(array $data){

		$auth = Zend_Auth::getInstance();

		if(!isset($data['rcp_inv_create_by'])){
			$data['rcp_inv_create_by'] = $auth->getIdentity()->iduser;
		}
		
		$data['rcp_inv_create_date'] = date('Y-m-d H:i:s');

		return parent::insert($data);
	}
	
	public function getOutstandingByReceiptInv($searchData=null){
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoice_id = $searchData['invoice_id'];
		$inv_Detail = $searchData['invoice_detail_id'];
		
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('ri'=>$this->_name))
					->joinLeft(array('b'=>'receipt'),'b.rcp_id = ri.rcp_inv_rcp_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = b.rcp_cur_id')
					->joinLeft(array('a'=>'invoice_main'),'a.id = ri.rcp_inv_invoice_id')
					->where('ri.rcp_inv_invoice_id = ?', $invoice_id)
					->where('ri.rcp_inv_invoice_dtl_id = ?', $inv_Detail)
//					->where("DATE(b.rcp_receive_date) >= ?",$dateFrom)
					->where("DATE(b.rcp_receive_date) <= ?",$dateTo);
		
		$row = $db->fetchAll($selectData);
		
//		echo "<pre>";
//		print_r($row);

		return $row;
	}
	
	public function getOutstandingInv($searchData=null){
		
		/* checking balance from 
    	 * 1) receipt_invoice
    	 * 2) advance payment utilization
    	 * 3) discount
    	 * 4) CN drop by asad
		 * 5) sponsor payment 13/11/2015
    	 */

		$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoice_id = $searchData['invoice_id'];
		$inv_Detail = $searchData['invoice_detail_id'];
		$currency_id = $searchData['currency_id'];
		$invoice_date = $searchData['invoice_date'];
		
		$db = Zend_Db_Table::getDefaultAdapter();
		// 1) receipt_invoice
		$selectData = $db->select()
					->from(array('ri'=>$this->_name),array('rcp_inv_amount'))
					->joinLeft(array('b'=>'receipt'),'b.rcp_id = ri.rcp_inv_rcp_id',array('b.rcp_cur_id'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = b.rcp_cur_id',array())
					->joinLeft(array('a'=>'invoice_main'),'a.id = ri.rcp_inv_invoice_id',array('exchange_rate','currency_id'))
					->where("b.rcp_status = 'APPROVE'")
					->where('ri.rcp_inv_invoice_id = ?', $invoice_id)
					->where('ri.rcp_inv_invoice_dtl_id = ?', $inv_Detail)
					->where("DATE(b.rcp_receive_date) >= ?",$dateFrom)
					->where("DATE(b.rcp_receive_date) <= ?",$dateTo);
		
		$row = $db->fetchAll($selectData);
		
		$paidAmount=0;
		if($row){
			foreach($row as $data){
				if ($currency_id != 1){
					if ($data['rcp_cur_id'] == 1){
						$exRate = $curRateDB->getRateByDate($currency_id, $invoice_date);
						$data['rcp_inv_amount'] = round($data['rcp_inv_amount']/$exRate['cr_exchange_rate'], 2);
					}
				}else{
					if ($data['rcp_cur_id'] != 1){
						$exRate = $curRateDB->getRateByDate($data['rcp_cur_id'], $invoice_date);
						$data['rcp_inv_amount'] = round($data['rcp_inv_amount']*$exRate['cr_exchange_rate'], 2);
					}
				}

				$paidAmount += $data['rcp_inv_amount'];
			}
		}
		
		// 2) advance payment utilization
		$selectData2 = $db->select()
					->from(array('d'=>'advance_payment_detail',array('advpydet_total_paid')))
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.advpydet_inv_id',array('exchange_rate'))
					->join(array('da'=>'advance_payment'), 'd.advpydet_advpy_id = da.advpy_id',array('currency_id'=>'advpy_cur_id'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = da.advpy_cur_id',array())
					->where("da.advpy_status = 'A'")
					->where("d.advpydet_status = 'A'")
					->where('d.advpydet_inv_id = ?', $invoice_id)
					->where('d.advpydet_inv_det_id = ?', $inv_Detail)
					->where("DATE(d.advpydet_upddate) >= ?",$dateFrom)
					->where("DATE(d.advpydet_upddate) <= ?",$dateTo);
		
		$row2 = $db->fetchAll($selectData2);
		
//		echo "<pre>";
//		print_r($row2);
		if($row2){
			foreach($row2 as $data){
	//			$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
	//			$dataCur = $curRateDB->getRateByDate($data['currency_id'],$data['advpydet_upddate']);//usd
	//			
				$amountCur = $data['advpydet_total_paid'];
	////			if($data['currency_id'] == 2){
	//				$amountCur = round($amountCur * $dataCur['cr_exchange_rate'],2);
	//			}
				$paidAmount += $amountCur;
			}
		}
		
		
		// 3) discount
		$selectData3 = $db->select()
					->from(array('d'=>'discount_detail'),array('dcnt_amount'))
					->join(array('da'=>'discount'),'da.dcnt_id = d.dcnt_id',array())
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.dcnt_invoice_id',array('exchange_rate','currency_id'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array())
					->where("d.status = 'A'")
					->where('d.dcnt_invoice_id = ?', $invoice_id)
					->where('d.dcnt_invoice_det_id = ?', $inv_Detail)
					->where("DATE(da.dcnt_create_date) >= ?",$dateFrom)
					->where("DATE(da.dcnt_create_date) <= ?",$dateTo);
		
		$row3 = $db->fetchAll($selectData3);
		
		if($row3){
			foreach($row3 as $data){
				$paidAmount += $data['dcnt_amount'];
			}
		}


		//4. sponsor payment

		$selectData4 = $db->select()
			->from(array('a'=>'sponsor_invoice_detail'),array('sp_amount'))
			->join(array('b'=>'sponsor_invoice_main'),'b.sp_id = a.sp_id',array())
			->join(array('d'=>'sponsor_invoice_receipt_invoice'), 'b.sp_id = d.rcp_inv_sp_id', array())
			->join(array('e'=>'sponsor_invoice_receipt'), 'd.rcp_inv_rcp_id = e.rcp_id', array())
			->joinLeft(array('iv'=>'invoice_main'),'iv.id = a.sp_invoice_id',array('exchange_rate','currency_id'))
			->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = iv.currency_id',array())
			->where("a.sp_status = ?", 1)
			->where("e.rcp_status = 'APPROVE'")
			->where('a.sp_invoice_id = ?', $invoice_id)
			->where('a.sp_invoice_det_id = ?', $inv_Detail)
			->where("DATE(b.sp_invoice_date) >= ?",$dateFrom)
			->where("DATE(b.sp_invoice_date) <= ?",$dateTo);

		$row4 = $db->fetchAll($selectData4);

		if($row4){
			foreach($row4 as $data){
				$paidAmount += $data['sp_amount'];
			}
		}
		
//		echo "<hr>".$paidAmount."<br>";
		return $paidAmount;
	}
	
}