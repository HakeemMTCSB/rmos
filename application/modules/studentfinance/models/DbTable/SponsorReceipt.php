<?php
/**
 * @author Suliana <suliana@meteor.com.my>
 * @copyright Copyright (c) 2015, MTCSB
 */

class Studentfinance_Model_DbTable_SponsorReceipt extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'sponsor_invoice_receipt';
	protected $_primary = "rcp_id";

	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('r'=>$this->_name),array('*','rcp_UpdDate'=>'UpdDate'))
					->joinLeft(array('rb'=>'sponsor_invoice_batch'), 'rb.id = r.rcp_batch_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rcp_cur_id')
					->joinLeft(array('sp'=>'tbl_sponsor'), 'sp.idsponsor = r.rcp_type_id and r.rcp_type = 1',array("SponsorCode",'SponsorName'=>'fName'))
					->joinLeft(array('sc'=>'tbl_scholarship_sch'), 'sc.sch_Id = r.rcp_type_id and r.rcp_type = 2',array("ScholarCode"=>"sch_code",'ScholarName'=>'sch_name'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = r.rcp_create_by', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = r.cancel_by', array())
					->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancel_name'=>'tsa.FullName'))
					->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = r.UpdUser', array())
					->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('update_name'=>'tsb.FullName'))
					->join(array('ac'=>'tbl_account_code'), 'ac.ac_id = r.rcp_account_code')
					->joinLeft(array('pm'=>'payment_mode'), 'pm.id = r.p_payment_mode_id', array('payment_mode'=>'payment_mode'))
					->joinLeft(array('t'=>'creditcard_terminal'), 't.id = r.p_terminal_id', array('terminal_id'))
					->group('r.rcp_id')
					->order('r.rcp_date desc');
		
		if($id!=null){
			$selectData->where("r.rcp_id = ?", (int)$id);
		}
		
			$row = $db->fetchAll($selectData);
		
		
		if($row){
			$m=0;
				foreach ($row as $receipt){
					$db = Zend_Db_Table::getDefaultAdapter();
					
					//receipt student
					
					$selectDataStudent = $db->select()
									->from(array('a'=>'sponsor_invoice_receipt_student'))
									->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration',array('registrationId'))
									->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
									->where("a.rcp_inv_rcp_id = ?", (int)$receipt['rcp_id']);
					
					$receiptStudent = $db->fetchAll($selectDataStudent);
					
					if($receiptStudent){
						$row[$m]['receipt_student'] = $receiptStudent;
						
						
						foreach($receiptStudent as $key=>$rcpStud){
							
							$rcpStudentInv = $rcpStud['rcp_inv_id'];
							$rcpIdStudentRegistration = $rcpStud['IdStudentRegistration'];
							//receipt invoice
					
							$selectDataReceipt = $db->select()
											->from(array('a'=>'sponsor_invoice_receipt_invoice'))
											->join(array('rbm'=>'sponsor_invoice_main'), 'rbm.sp_id = a.rcp_inv_sp_id')
											->joinLeft(array('spd'=>'sponsor_invoice_detail'), 'a.rcp_inv_sponsor_dtl_id = spd.id and spd.sp_id = rbm.sp_id',array('*','sp_amount_det'=>'spd.sp_amount','spmdtlid'=>'spd.id'))
											->joinLeft(array('iv'=>'invoice_main'), 'iv.id = spd.sp_invoice_id',array('*','idMain'=>'id'))
											->joinLeft(array('ivd'=>'invoice_detail'), 'ivd.id = spd.sp_invoice_det_id',array('idDet'=>'ivd.id','*'))
											->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = iv.currency_id')
											->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = iv.semester',array('SemesterMainName'))
											->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = iv.trans_id and (iv.IdStudentRegistration is null OR iv.IdStudentRegistration = 0)',array('at_pes_id'))
											->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
											->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = iv.IdStudentRegistration',array('registrationId'))
											->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
											->where("a.rcp_inv_rcp_id = ?", (int)$receipt['rcp_id']);
											if($rcpStudentInv){
												$selectDataReceipt->where("a.rcp_inv_student = ?", (int)$rcpStudentInv);
												$selectDataReceipt->where("str.IdStudentRegistration = ?", (int)$rcpIdStudentRegistration);
											}
							
							$receiptInvoice = $db->fetchAll($selectDataReceipt);
							
							if($receiptInvoice){
								$row[$m]['receipt_student'][$key]['receipt_invoice'] = $receiptInvoice;
							}else{
								$row[$m]['receipt_student'][$key]['receipt_invoice'] = null;
							}
						}
						
					}else{
						$row[$m]['receipt_student'] = null;
					}
					
					
					$m++;
					
				}
			}

//		echo "<pre>";
//		print_r($row);
//		exit;
		return $row;
	}
	
	public function getSearchData($name='',$id='', $type=645,$receipt_no=null,$status=0){
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
						->from(array('r'=>$this->_name),array('*','rcp_UpdDate'=>'UpdDate'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rcp_cur_id')
						->joinLeft(array('u'=>'tbl_user'), 'u.iduser = r.rcp_create_by', array())
						->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
						->order('r.rcp_date desc');

		
		
		if($type == 645){
			$selectData->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.rcp_trans_id')
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id')
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'));

			if($id!='' ){
				$selectData->where("at.at_pes_id like ?",'%'.$id.'%');
			}
			if($name!=''){
				$selectData->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
			}
			
			$selectData->joinLeft(array('rci'=>'receipt_invoice'),'rci.rcp_inv_rcp_id=r.rcp_id',array(''))
						->joinLeft(array('iv'=>'invoice_main'),'rci.rcp_inv_invoice_id=iv.id and iv.trans_id = at.at_trans_id and iv.trans_id = r.rcp_trans_id',array('iv.bill_number','GROUP_CONCAT(iv.bill_number) as invoice_no'))
						
						->join(array('ac'=>'tbl_account_code'), 'ac.ac_id = r.rcp_account_code');
		}
		
		if($type == 646){
			$selectData->joinLeft(array('std'=>'tbl_studentregistration'), 'std.IdStudentRegistration = r.rcp_idStudentRegistration',array('at_pes_id'=>'std.registrationId','registrationId'))
			->joinLeft(array('sp'=>'student_profile'), 'sp.id = std.sp_id')
			->joinLeft(array('ps'=>'tbl_program'), 'ps.IdProgram=std.IdProgram',array('applicant_program' => 'ps.ProgramCode','applicant_program_name' => 'ps.ProgramName'));
			
			if($id!='' ){
				$selectData->where("std.registrationId like ?",'%'.$id.'%');
			}
			if($name!=''){
				$selectData->where("concat_ws(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname) like ?", '%'.$name.'%');
			}
			
			$selectData->joinLeft(array('rci'=>'receipt_invoice'),'rci.rcp_inv_rcp_id=r.rcp_id',array(''))
						->joinLeft(array('iv'=>'invoice_main'),'rci.rcp_inv_invoice_id=iv.id and iv.IdStudentRegistration = r.rcp_idStudentRegistration',array('iv.bill_number','GROUP_CONCAT(iv.bill_number) as invoice_no'))
						
						->join(array('ac'=>'tbl_account_code'), 'ac.ac_id = r.rcp_account_code');
						
			
		}
		
		if($receipt_no){
			$selectData->where('r.rcp_no like ?','%'.$receipt_no.'%');
		}
		
		if($status){
			$selectData->where('r.rcp_status = ?',$status);
		}
		
		$selectData->group('r.rcp_id')
		->order('r.rcp_date desc');//		echo $selectData;//exit;
		
						
		$row = $db->fetchAll($selectData);
		if($row){
				foreach ($row as &$receipt){
					$db = Zend_Db_Table::getDefaultAdapter();
					
					$selectData = $db->select()
									->from(array('p'=>'payment'))
									->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = p.p_cur_id')
									->where("p.p_rcp_id = ?", (int)$receipt['rcp_id']);
					
					$payment = $db->fetchAll($selectData);
					
					if($payment){
						$receipt['payment'] = $payment;
					}
					
					//receipt invoice
					
					$selectDataReceipt = $db->select()
									->from(array('a'=>'receipt_invoice'))
									->where("a.rcp_inv_rcp_id = ?", (int)$receipt['rcp_id']);
					
					$receiptInvoice = $db->fetchAll($selectDataReceipt);
					
					if($receiptInvoice){
						$receipt['receipt_invoice'] = $receiptInvoice;
					}
					
				}
			}

		
		return $row;
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['rcp_create_by'])){
			$data['rcp_create_by'] = $auth->getIdentity()->id;
		}
		
		$data['rcp_date'] = date('Y-m-d H:i:s');
	
		return parent::insert($data);
	}
	
	public function getSearchDataReport($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$scheme = $searchData['IdProgramScheme'];
		$accountcode = $searchData['accountcode'];
		$paymentmode = $searchData['paymentmode'];
		
		$selectData = $db->select()
						->from(array('r'=>$this->_name),array('rcp_UpdDate'=>'UpdDate','rcp_no','codeMigrate'=>'r.MigrateCode','rcp_receive_date','rcp_cur_id','rcp_amount','rcp_gainloss','rcp_gainloss_amt','rcp_amount_default_currency'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rcp_cur_id',array('cur_id','cur_code'))
						->joinLeft(array('p'=>'payment'), 'p.p_rcp_id = r.rcp_id',array('p_migs','p_payment_mode_id','p_card_no','p_cheque_no'))
						->joinLeft(array('pm'=>'payment_mode'), 'pm.id = p.p_payment_mode_id',array('payment_mode'))
						->joinLeft(array('adv'=>'advance_payment'), 'adv.advpy_rcp_id = r.rcp_id',array('advpy_amount','advpy_cur_id','advpy_total_balance','advpy_exchange_rate','advpy_id','advpy_fomulir'))
						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.rcp_trans_id and (r.rcp_idStudentRegistration is null OR r.rcp_idStudentRegistration = 0)',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = r.rcp_idStudentRegistration',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme OR str.IdProgramScheme = e.IdProgramScheme',array())
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			            ->joinLeft(array('ch'=>'tbl_definationms'), 'pm.payment_group = ch.idDefinition', array('payment_group_name'=>'ch.DefinitionDesc'))
			            
			            ->joinLeft(array('rci'=>'receipt_invoice'),'rci.rcp_inv_rcp_id=r.rcp_id',array('rcp_inv_amount','rcp_inv_invoice_dtl_id','rcp_inv_invoice_id'))
//		
						->joinLeft(array('iv'=>'invoice_main'),'rci.rcp_inv_invoice_id=iv.id',array('invoice_no'=>'iv.bill_number','exchange_rate'))
						->joinLeft(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id=iv.id and rci.rcp_inv_invoice_dtl_id = ivd.id',array('idDet'=>'ivd.id','amount','invoice_main_id'))
						//'GROUP_CONCAT(iv.bill_number) as invoice_no'
						->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = iv.semester',array('SemesterMainName'))
						->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = ivd.fi_id',array('fi_name','fi_code'))
						->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id',array('fc_desc'))
						->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fi.fi_ac_id',array('ac_code','ac_desc'))
						  ->joinLeft(array('fsd'=>'fee_structure'), 'fsd.fs_id = str.fs_id OR fsd.fs_id = at.at_fs_id',array())
				            ->joinLeft(array('acc'=>'tbl_account_code'), 'acc.ac_id = fsd.fs_prepayment_ac',array('ac_code_cc'=>'ac_code','ac_desc_cc'=>'ac_desc'))
			//			->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=iv.id and ivs.invoice_detail_id = ivd.id')
			//			->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('GROUP_CONCAT(sc.SubCode) as subjectCode'))
						->joinLeft(array('mg'=>'transaction_migs'),'mg.mt_id=p.p_migs',array('mt_txn_no','mt_from'))
						->joinLeft(array('sp'=>'sponsor_application'), 'sp.sa_cust_id = str.IdStudentRegistration', array())
			            ->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = str.IdStudentRegistration OR schtg.sa_trans_id = at.at_trans_id', array())
			            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
			            ->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = str.IdStudentRegistration', array())
			            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
			            
			            ->joinLeft(array('dcn'=>'discount_type_tag'), 'dcn.IdStudentRegistration=str.IdStudentRegistration', array())
			            ->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array('dt_discount'))
			            
						/*->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = str.IdStudentRegistration', array())
			            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
			            ->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = str.IdStudentRegistration', array())
			            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
			            
			            ->joinLeft(array('dcn'=>'discount_type_tag'), 'dcn.IdStudentRegistration=str.IdStudentRegistration', array())
			            ->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array('dt_discount'))*/
						->where("r.rcp_status = 'APPROVE'")
//						->where("str.IdStudentRegistration IN (3831,4033)")
//						->group('iv.id')
//						->group('ivd.id')
//						->group('rci.rcp_inv_id')
			//			->group('ivd.fi_id')
//			->group('ivd.id') //duplicate item 2015-03-04
			->order('r.rcp_receive_date desc')
			->order('r.rcp_no desc');
							
		if($dateFrom!=''){
			$selectData->where("DATE(r.rcp_receive_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(r.rcp_receive_date) <= ?",$dateTo);
		}
		if(isset($program) && !empty($program)){
//			$selectData->where("app.ap_prog_id = ?",$program);
			$selectData->orWhere("pa.IdProgram = ?",$program);
		}
		
		if(isset($scheme) && !empty($scheme)){
//			$selectData->where("app.ap_prog_scheme = ?",$scheme);
			$selectData->orWhere("e.IdProgramScheme = ?",$scheme);
		}
		
		if(isset($accountcode) && !empty($accountcode)){
			$selectData->where('r.rcp_account_code = ?',$accountcode);
		}
		
		if($paymentmode != 'ALL'){
			if($paymentmode == 0){
				$selectData->where('p.p_payment_mode_id = ?',$paymentmode);
				$selectData->where('r.migs_id !=0');
			}else{
				$selectData->where('pm.payment_group = ?',$paymentmode);
			}
		}
		$auth = Zend_Auth::getInstance();
		/*if($auth->getIdentity()->id == 1){
			echo $selectData;
		}*/
//		echo $selectData;
		$row = $db->fetchAll($selectData);
		$rowNew = array();
		if($row){
			$m=0;
				
			$newArray = array();
			$n=0;
			foreach($row as $rci){
				
				//advance payment
				if($rci['advpy_id']){
					
					$selectAdv = $db->select()
							->from(array('a'=>'advance_payment'),array('*','exchange_rate'=>'advpy_exchange_rate','invoice_no'=>'advpy_fomulir'))
							->joinLeft(array('r'=>'receipt'), 'r.rcp_id=advpy_rcp_id',array('rcp_no','rcp_receive_date','migs_id','rcp_cur_id','rcp_amount','rcp_gainloss','rcp_gainloss_amt','rcp_amount_default_currency'))
							->joinLeft(array('p'=>'payment'), 'p.p_rcp_id = r.rcp_id',array('p_payment_mode_id','p_cheque_no','p_card_no'))
							->joinLeft(array('pm'=>'payment_mode'), 'pm.id = p.p_payment_mode_id',array('payment_mode'))
							->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.advpy_trans_id and a.advpy_idStudentRegistration is null',array('at_pes_id'))
							->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
							->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
							->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.advpy_idStudentRegistration',array('registrationId'))
							->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
							->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
							->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme OR str.IdProgramScheme = e.IdProgramScheme',array())
				            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
				            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
				            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
				            ->joinLeft(array('ch'=>'tbl_definationms'), 'pm.payment_group = ch.idDefinition', array('payment_group_name'=>'ch.DefinitionDesc'))
				            
				            ->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.advpy_sem_id',array('SemesterMainName'))
				             ->joinLeft(array('fsd'=>'fee_structure'), 'fsd.fs_id = str.fs_id OR fsd.fs_id = at.at_trans_id',array())
				            ->joinLeft(array('acc'=>'tbl_account_code'), 'acc.ac_id = fsd.fs_prepayment_ac',array('ac_code_cc'=>'ac_code','ac_desc_cc'=>'ac_desc'))
							->where("a.advpy_id = ?", (int)$rci['advpy_id'])
							->group('a.advpy_id');
			
					$receiptAdv = $db->fetchRow($selectAdv);
					$newArray[$n] = $receiptAdv;
					$newArray[$n]['subjectCode']=null;
					$newArray[$n]['fi_name']=null;
					$newArray[$n]['rcp_inv_amount']=null;
					
					$n++;
					
				}
				
				//subject
				$selectSubject = $db->select()
							->from(array('a'=>'invoice_subject'))
							->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = a.subject_id',array('GROUP_CONCAT(sc.SubCode) as subjectCode'))
							->where("a.invoice_main_id = ?", (int)$rci['invoice_main_id'])
							->where("a.invoice_detail_id = ?", (int)$rci['idDet']);
			
				$receiptSubject = $db->fetchRow($selectSubject);
				
				
				$row[$m]['subjectCode'] = $receiptSubject['subjectCode'];
				
				$m++;
			}
						
		}
		$rowNew = array_merge(array_unique($newArray,SORT_REGULAR),$row);			
		/*echo "<pre>";
		print_r($newArray);		
		echo "<hr>";*/
//		echo "<pre>";
//		print_r($row);	
		$data = $this->nsort($rowNew, array('rcp_receive_date'));
//		$data2 = $this->nsort($data, array('rcp_no'));
		return $row;
	}
	
	
	
	
private function nsort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            arsort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}
	
public function getSearchDataReport2($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$scheme = $searchData['IdProgramScheme'];
		$accountcode = $searchData['accountcode'];
		$paymentmode = $searchData['paymentmode'];
	
		$selectData = $db->select()
						->from(array('r'=>$this->_name),array('r.*','rcp_UpdDate'=>'UpdDate'))
						->join(array('c'=>'tbl_currency'), 'c.cur_id = r.rcp_cur_id')
						->join(array('p'=>'payment'), 'p.p_rcp_id = r.rcp_id')
						->joinLeft(array('pm'=>'payment_mode'), 'pm.id = p.p_payment_mode_id')
						->join(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.rcp_trans_id')
						->join(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id')
						->join(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->join(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->join(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme')
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			            ->joinLeft(array('ch'=>'tbl_definationms'), 'pm.payment_group = ch.idDefinition', array('payment_group_name'=>'ch.DefinitionDesc'))
			            ->where("r.rcp_status = 'APPROVE'")
			            ->order('r.rcp_date desc');

							
		if($dateFrom!=''){
			$selectData->where("r.rcp_receive_date >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("r.rcp_receive_date <= ?",$dateTo);
		}
		if(isset($program) && !empty($program)){
			$selectData->where("app.ap_prog_id = ?",$program);
		}
		
		if(isset($scheme) && !empty($scheme)){
			$selectData->where("app.ap_prog_scheme = ?",$scheme);
		}
		
		if(isset($accountcode) && !empty($accountcode)){
			$selectData->where('r.rcp_account_code = ?',$accountcode);
		}
		
		if(isset($paymentmode) && !empty($accountcode)){
			$selectData->where('pm.payment_group = ?',$paymentmode);
		}
		
		
		echo $selectData;			
		$row = $db->fetchAll($selectData);
		
		if($row){
			$m=0;
				foreach ($row as &$receipt){
					
					//receipt invoice
					
					$selectDataReceipt = $db->select()
									->from(array('a'=>'receipt_invoice'))
									->joinLeft(array('iv'=>'invoice_main'), 'iv.id = a.rcp_inv_invoice_id',array('*'))
									->joinLeft(array('ivd'=>'invoice_detail'), 'ivd.id = a.rcp_inv_invoice_dtl_id',array('idDet'=>'ivd.id','*'))
									->where("a.rcp_inv_rcp_id = ?", (int)$receipt['rcp_id']);
					
					$receiptInvoice = $db->fetchAll($selectDataReceipt);
					$row[$m]['invoice_bil'] = count($receiptInvoice);
					if($receiptInvoice){
						$row[$m]['receipt_invoice'] = $receiptInvoice;
						
						$n=0;
						foreach($receiptInvoice as $rci){
						
							$selectSubject = $db->select()
										->from(array('a'=>'invoice_subject'))
										->where("a.invoice_main_id = ?", (int)$rci['invoice_main_id'])
										->where("a.invoice_detail_id = ?", (int)$rci['idDet']);
						
							$receiptSubject = $db->fetchAll($selectSubject);
							
							$row[$m]['receipt_invoice'][$n]['subject'] = $receiptSubject;
							
							$n++;
						}
						
					}
					$m++;
					
				}
			}
		
		echo "<pre>";
		print_r($row);
		
		return $row;
	}
	
public function getDataReceipt($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('r'=>$this->_name))
					->where("r.rcp_id = ?", (int)$id);
		
		$row = $db->fetchRow($selectData);
		
		return $row;
	}

	public function cancelSpmDtl($data, $id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$update = $db->update('sponsor_invoice_detail', $data, 'id = '. $id);
		return $update;
	}
}