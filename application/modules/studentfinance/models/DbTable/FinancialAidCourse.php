<?php

class Studentfinance_Model_DbTable_FinancialAidCourse extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_financial_aid_course';
    protected $_primary = "id";

    protected $_referenceMap    = array(
        'FinancialAid' => array(
            'columns'           => 'financial_aid_id',
            'refTableClass'     => 'Studentfinance_Model_DbTable_FinancialAid',
            'refColumns'        => 'id'
        ),
        'Semester' => array(
            'columns'           => 'semester_id',
            'refTableClass'     => 'Records_Model_DbTable_SemesterMaster',
            'refColumns'        => 'IdSmesterMaster'
        )

    );
}