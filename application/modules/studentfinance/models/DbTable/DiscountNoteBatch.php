<?php
class Studentfinance_Model_DbTable_DiscountNoteBatch extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'discount_batch';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.created_by', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = cn.semester_id', array('SemesterMainName'))
					->joinLeft(array('d'=>'discount_type'),'d.dt_id = cn.dcnt_id', array('dt_discount','dt_description'))
					->order('cn.id desc');
		
		if($id!=0){
			$selectData->where("cn.id = '".$id."'");
		}
		$row = $db->fetchAll($selectData);
		return $row;	
		
	}
	
	public function getDataDetail($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.created_by', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = cn.semester_id', array('SemesterMainName'))
					->where("cn.id = ?",$id);
		
			$row = $db->fetchRow($selectData);
		
		return $row;	
		
	}
	
	public function insert(array $data){
		
		return parent::insert($data);
	}
	
	
	
}

