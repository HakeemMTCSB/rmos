<?php
class Studentfinance_Model_DbTable_PaymentPlanDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'payment_plan_detail';
	protected $_primary = "ppd_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ppd'=>$this->_name))
					->where("ppd.ppd_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getPaymentPlanDetail($payment_plan_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ppd'=>$this->_name))
					->where("ppd.ppd_payment_plan_id = ?", (int)$payment_plan_id);

		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
			
			foreach ($row as $index=>$plan_detail){
				$row[$index]['invoice'] = $invoiceMainDb->getData($plan_detail['ppd_invoice_id']);
				
				$row[$index]['invoice']['detail'] = $invoiceDetailDb->getInvoiceDetail($plan_detail['ppd_invoice_id']);
			}
			
			return $row;
		}
	}


}
?>