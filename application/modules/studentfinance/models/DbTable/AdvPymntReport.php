<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/8/2016
 * Time: 11:16 AM
 */
class Studentfinance_Model_DbTable_AdvPymntReport extends Zend_Db_Table_Abstract {

    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->order('a.seq_no ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramById($idProgram){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->where('a.IdProgram = ?', $idProgram);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgramScheme($idProgram){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $idProgram)
            ->order('a.mode_of_program ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->order('a.SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAdvPymntHavingBalance($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment'))
            ->joinLeft(array('i'=>'tbl_currency'), 'a.advpy_cur_id = i.cur_id', array('i.cur_code'))
            ->joinLeft(array('j'=>'tbl_user'), 'a.advpy_creator = j.iduser', array('j.loginName'))
            ->where('a.advpy_status = ?', 'A')
            ->where('a.advpy_total_balance != ?', 0.00);

        if ($search != false){
            if (isset($search['type']) && $search['type']!=''){
                if ($search['type'] == 0){
                    $select->join(array('b'=>'applicant_transaction'), 'a.advpy_trans_id = b.at_trans_id', array('studentID'=>'b.at_pes_id'));
                    $select->join(array('c'=>'applicant_profile'), 'b.at_appl_id = c.appl_id', array('name'=>'CONCAT(c.appl_fname, " ", c.appl_lname)', ));
                    $select->join(array('d'=>'applicant_program'), 'a.advpy_trans_id = d.ap_at_trans_id', array());
                    $select->join(array('e'=>'tbl_program'), 'd.ap_prog_id = e.IdProgram', array('e.ProgramName', 'e.ProgramCode'));

                    if (isset($search['program']) && $search['program']!=''){
                        $select->where('d.ap_prog_id = ?', $search['program']);
                    }

                    if (isset($search['programscheme']) && $search['programscheme']!=''){
                        $select->where('d.ap_prog_scheme = ?', $search['programscheme']);
                    }

                    if (isset($search['name']) && $search['name']!=''){
                        $select->where($db->qouteInto('CONCAT_WS(" ", c.appl_fname, c.appl_lname) = ?', '%'.$search['name'].'%'));
                    }

                    if (isset($search['studentid']) && $search['studentid']!=''){
                        $select->where($db->qouteInto('b.at_pes_id = ?', '%'.$search['studentid'].'%'));
                    }

                    if (isset($search['advpymntno']) && $search['advpymntno']!=''){
                        $select->where($db->qouteInto('a.advpy_fomulir = ?', '%'.$search['advpymntno'].'%'));
                    }
                }else{
                    $select->join(array('f'=>'tbl_studentregistration'), 'a.advpy_idStudentRegistration = f.IdStudentRegistration', array('studentID'=>'f.registrationId'));
                    $select->join(array('g'=>'student_profile'), 'f.sp_id = g.id', array('name'=>'CONCAT(g.appl_fname, " ", g.appl_lname)', ));
                    $select->join(array('h'=>'tbl_program'), 'f.IdProgram = h.IdProgram', array('h.ProgramName', 'h.ProgramCode'));
                    $select->where('a.advpy_idStudentRegistration IS NOT NULL');

                    if (isset($search['program']) && $search['program']!=''){
                        $select->where('f.IdProgram = ?', $search['program']);
                    }

                    if (isset($search['programscheme']) && $search['programscheme']!=''){
                        $select->where('f.IdProgramScheme = ?', $search['programscheme']);
                    }

                    if (isset($search['name']) && $search['name']!=''){
                        $select->where($db->qouteInto('CONCAT_WS(" ", g.appl_fname, g.appl_lname) = ?', '%'.$search['name'].'%'));
                    }

                    if (isset($search['studentid']) && $search['studentid']!=''){
                        $select->where($db->qouteInto('f.registrationId = ?', '%'.$search['studentid'].'%'));
                    }

                    if (isset($search['advpymntno']) && $search['advpymntno']!=''){
                        $select->where($db->qouteInto('a.advpy_fomulir = ?', '%'.$search['advpymntno'].'%'));
                    }
                }
            }
        }
        //echo $select;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoice($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array('a.*', 'mainId'=>'a.id'))
            ->joinLeft(array('i'=>'tbl_currency'), 'a.currency_id = i.cur_id', array('i.cur_code'))
            ->where('a.status = ?', 'A');

        if ($search != false){
            if (isset($search['type']) && $search['type']!=''){
                if ($search['type'] == 0){
                    $select->join(array('b'=>'applicant_transaction'), 'a.trans_id = b.at_trans_id', array('studentID'=>'b.at_pes_id'));
                    $select->join(array('c'=>'applicant_profile'), 'b.at_appl_id = c.appl_id', array('name'=>'CONCAT(c.appl_fname, " ", c.appl_lname)', ));
                    $select->join(array('d'=>'applicant_program'), 'a.trans_id = d.ap_at_trans_id', array());
                    $select->join(array('e'=>'tbl_program'), 'd.ap_prog_id = e.IdProgram', array('e.ProgramName', 'e.ProgramCode'));

                    if (isset($search['program']) && $search['program']!=''){
                        $select->where('d.ap_prog_id = ?', $search['program']);
                    }

                    if (isset($search['programscheme']) && $search['programscheme']!=''){
                        $select->where('d.ap_prog_scheme = ?', $search['programscheme']);
                    }

                    if (isset($search['name']) && $search['name']!=''){
                        $select->where($db->qouteInto('CONCAT_WS(" ", c.appl_fname, c.appl_lname) = ?', '%'.$search['name'].'%'));
                    }

                    if (isset($search['studentid']) && $search['studentid']!=''){
                        $select->where($db->qouteInto('b.at_pes_id = ?', '%'.$search['studentid'].'%'));
                    }

                    if (isset($search['advpymntno']) && $search['advpymntno']!=''){
                        $select->where($db->qouteInto('a.advpy_fomulir = ?', '%'.$search['advpymntno'].'%'));
                    }
                }else{
                    $select->join(array('f'=>'tbl_studentregistration'), 'a.IdStudentRegistration = f.IdStudentRegistration', array('studentID'=>'f.registrationId'));
                    $select->join(array('g'=>'student_profile'), 'f.sp_id = g.id', array('name'=>'CONCAT(g.appl_fname, " ", g.appl_lname)', ));
                    $select->join(array('h'=>'tbl_program'), 'f.IdProgram = h.IdProgram', array('h.ProgramName', 'h.ProgramCode'));

                    if (isset($search['program']) && $search['program']!=''){
                        $select->where('f.IdProgram = ?', $search['program']);
                    }

                    if (isset($search['programscheme']) && $search['programscheme']!=''){
                        $select->where('f.IdProgramScheme = ?', $search['programscheme']);
                    }

                    if (isset($search['name']) && $search['name']!=''){
                        $select->where($db->qouteInto('CONCAT_WS(" ", g.appl_fname, g.appl_lname) = ?', '%'.$search['name'].'%'));
                    }

                    if (isset($search['studentid']) && $search['studentid']!=''){
                        $select->where($db->qouteInto('f.registrationId = ?', '%'.$search['studentid'].'%'));
                    }

                    if (isset($search['advpymntno']) && $search['advpymntno']!=''){
                        $select->where($db->qouteInto('a.advpy_fomulir = ?', '%'.$search['advpymntno'].'%'));
                    }
                }

                if (isset($search['semester']) && $search['semester']!=''){
                    $select->where('a.semester = ?', $search['semester']);
                }
            }
        }
        //echo $select;
        $result = $db->fetchAll($select);
        return $result;
    }
}