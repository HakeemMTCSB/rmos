<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 3/8/2016
 * Time: 11:02 AM
 */
class Studentfinance_Model_DbTable_FeeStructureReport extends Zend_Db_Table_Abstract {

    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->order('a.seq_no ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramById($idProgram){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->where('a.IdProgram = ?', $idProgram);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgramScheme($idProgram){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $idProgram)
            ->order('a.mode_of_program ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdScheme = ?', $idScheme)
            ->order('a.SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getIntake(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'));

        $select = $select.' ORDER BY a.sem_year DESC, CASE a.sem_seq '
            . 'WHEN "JAN" THEN 1 '
            . 'WHEN "FEB" THEN 2 '
            . 'WHEN "MAR" THEN 3 '
            . 'WHEN "APR" THEN 4 '
            . 'WHEN "MAY" THEN 5 '
            . 'WHEN "JUN" THEN 6 '
            . 'WHEN "JUL" THEN 7 '
            . 'WHEN "AUG" THEN 8 '
            . 'WHEN "SEP" THEN 9 '
            . 'WHEN "OCT" THEN 10 '
            . 'WHEN "NOV" THEN 11 '
            . 'WHEN "DEC" THEN 12 '
            . 'ELSE 13 '
            . 'END DESC';

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getFeeStructure($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'fee_structure'))
            ->joinLeft(array('b'=>'fee_structure_program'), 'a.fs_id = b.fsp_fs_id')
            ->group('a.fs_id');

        if ($search != false){
            if (isset($search['program']) && $search['program']!=''){
                $select->where('b.fsp_program_id = ?', $search['program']);
            }

            if (isset($search['programscheme']) && $search['programscheme']!=''){
                $select->where('b.fsp_idProgramScheme = ?', $search['programscheme']);
            }

            if (isset($search['intake']) && $search['intake']!=''){
                $select->where('a.fs_intake_start = ?', $search['intake']);
            }

            if (isset($search['type']) && $search['type']!=''){
                $select->where('a.fs_student_category = ?', $search['type']);
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }
}