<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_NonInvoice extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'non_invoice';
	protected $_primary = "ni_id";

	public function getData($id){

		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
		->from(array('ni'=>$this->_name))
		->where("ni.ni_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['ni_create_by'])){
			$data['ni_create_by'] = $auth->getIdentity()->iduser;
		}
	
		return parent::insert($data);
	}
	
	public function getNonInvoiceFromReceipt($receipt_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
							->from(array('ni'=>$this->_name))
							->join(array('fi'=>'fee_item'), 'fi.fi_id = ni.ni_fi_id')
							->join(array('c'=>'tbl_currency'), 'c.cur_id = ni.ni_cur_id')
							->where("ni.ni_rcp_id = ?", (int)$receipt_id);
		
		$row = $db->fetchAll($selectData);
		
		return $row;
	}
}