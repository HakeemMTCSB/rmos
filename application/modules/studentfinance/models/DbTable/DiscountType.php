<?php
class Studentfinance_Model_DbTable_DiscountType extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'discount_type';
	protected $_primary = "dt_id";
		
	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('dt'=>$this->_name));
					
		if($id!=null){
			$selectData->where("dt.dt_id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			$row = $db->fetchAll($selectData);
		}
			
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getFeeInfo($dt_id,$fi_id=0)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'discount_type_fee_info'),array('a.*'))
		->joinLeft(array('b' => 'fee_item'), 'a.FeeCode = b.fi_id', array('fi_name','fi_code','fi_id'))
		->where('a.IdDiscountType =?',$dt_id);
		
		if($fi_id){
			$lstrSelect->where('a.FeeCode =?',$fi_id);
			$result = $db->fetchRow($lstrSelect);
		}else{
			$result = $db->fetchAll($lstrSelect);
		}
		return $result;
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
				->from(array('dt'=>$this->_name))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = dt.dt_creator', array())
				->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>new Zend_Db_Expr("CONCAT_WS(' ', fName,Mname,Lname)")));
	
		return $selectData;
	}
	
	public function insert(array $data){
		
		if( !isset($data['dt_creator']) ){
			$auth = $auth = Zend_Auth::getInstance();
			
			$data['dt_creator'] = $auth->getIdentity()->iduser; 
		}
		
		if( !isset($data['dt_create_date']) ){
			$data['dt_create_date'] = date('Y-m-d H:i:a'); 
		}
		
		return parent::insert($data);
	}
	
 	public function getFinancialAidList($search_items = null)
    {
        if($search_items == null) {
            $lists = $this->fetchAll();
        } else {
            $select = $this->select();
            foreach($search_items as $field => $search_item) {
                if(empty($search_item)) { continue; }
                $select = $select->where( $field . " LIKE ?", "%". $search_item ."%" );
            }

            $lists = $this->fetchAll($select);
        }

        return $lists;
    }
    
	public function fngetstudentname($lstrstudentname){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
		->from(array("a" => "tbl_studentregistration"), array("a.IdStudentRegistration"))
		->joinLeft(array('p'=>'student_profile'),"a.sp_id=p.id",("CONCAT_WS(' ',IFNULL(p.appl_fname,''),IFNULL(p.appl_mname,''),IFNULL(p.appl_lname,'')) AS name"))
		->where("a.registrationId = ?",$lstrstudentname)
		->where('a.ProfileStatus = 92');
		$result = $db->fetchRow($select);
		return $result;
	}
	
	public function getStudentListByDiscountType($dt_id,$intake=0)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'discount_type_tag'))
		->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration')
		->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
		->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
		->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
		->where('a.dt_id =?',$dt_id);
		
		if($intake){
			$lstrSelect->where('b.IdIntake = ?',$intake);
		}
			
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
	
	public function getDataByStudent($dt_id,$id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'discount_type_tag'))
		->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration')
		->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
		->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
		->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
		->where('a.dt_id =?',$dt_id)
		->where('a.IdStudentRegistration =?',$id);
			
		$result = $db->fetchRow($lstrSelect);
		return $result;
	}

	public function getSearchDataReport($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$discounttype = $searchData['discounttype'];
		
		$selectData = $db->select()
						->from(array('r'=>'discount'))
						->joinLeft(array('rd'=>'discount_detail'), 'rd.dcnt_id = r.dcnt_id',array())
						->joinLeft(array('iv'=>'invoice_main'), 'iv.id = rd.dcnt_invoice_id',array('exchange_rate'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.dcnt_currency_id',array('cur_id','cur_code'))
						
						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.dcnt_txn_id and r.dcnt_IdStudentRegistration is null',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = r.dcnt_IdStudentRegistration',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme OR str.IdProgramScheme = e.IdProgramScheme',array())
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
						->joinLeft(array('itk'=>'tbl_intake'), 'str.IdIntake = itk.IdIntake',array('IntakeDesc'))
						->joinLeft(array('fsp'=>'fee_structure'), 'fsp.fs_id = str.fs_id',array('fs_name'))
						->joinLeft(array('dt'=>'discount_type'), 'dt.dt_id = r.dcnt_type_id',array('dt_discount'))
						->joinLeft(array('dtp'=>'discount_type_tag'), 'dtp.dt_id = r.dcnt_type_id and dtp.dt_id = dt.dt_id and dtp.IdStudentRegistration = r.dcnt_IdStudentRegistration',array('dtt_start_date','dtt_end_date'))
						->where("r.dcnt_status = 'A'")
						->where("rd.status = 'A'")
			->order('r.dcnt_create_date desc')
			->group('r.dcnt_id');
							
		if($dateFrom!=''){
			$selectData->where("DATE(r.dcnt_create_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(r.dcnt_create_date) <= ?",$dateTo);
		}
		if(isset($discounttype) && !empty($discounttype)){
			$selectData->where("r.dcnt_type_id = ?",$discounttype);
		}
	
		$row = $db->fetchAll($selectData);
		
		if($row){
			$m=0;
				
						foreach($row as $rci){
							$idDisc = $rci['dcnt_id'];
							$selectSubject = $db->select()
										->from(array('a'=>'discount_detail'))
										->joinLeft(array('b'=>'invoice_detail'), 'b.id = a.dcnt_invoice_det_id and b.invoice_main_id = a.dcnt_invoice_id',array('amount'))
										->joinLeft(array('c'=>'invoice_main'), 'c.id = a.dcnt_invoice_id',array('exchange_rate','currency_id'))
										->where("a.dcnt_id = ?", (int)$idDisc);
						
							$receiptSubject = $db->fetchAll($selectSubject);
							$total = 0;
							
							$disAmount = 0;
							foreach($receiptSubject as $b=>$r){
								$statusInvoice = $r['status'];
								if($statusInvoice == 'A' || $statusInvoice == 'W'){
									$amount = $r['amount'];
									if($r['currency_id'] == 2){
										$currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
										$currency = $currencyDb->getData($r['exchange_rate']);
			
										$amount = number_format($r['amount'] * $currency['cr_exchange_rate'],2);
									}
									$total += $amount;
									$disAmount += $r['dcnt_amount'];
									
								}else{
									unset($receiptSubject[$b]);
								}
		
								
							}
							
							
							$row[$m]['invoiceAmount'] =  $total;
							
							$row[$m]['dcnt_amount'] =  $disAmount;
							
							$m++;
						}
						
					}
					
//				echo "<pre>";
//		print_r($row);		
		return $row;
	}
	
	public function getSearchDataDetailReport($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$discounttype = $searchData['discounttype'];
		
		$selectData = $db->select()
						->from(array('rd'=>'discount_detail'))
						->joinLeft(array('r'=>'discount'), 'rd.dcnt_id = r.dcnt_id',array())
						->joinLeft(array('iv'=>'invoice_main'), 'iv.id = rd.dcnt_invoice_id',array('exchange_rate'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.dcnt_currency_id',array('cur_id','cur_code'))
						
						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.dcnt_txn_id and r.dcnt_IdStudentRegistration is null',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = r.dcnt_IdStudentRegistration',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme OR str.IdProgramScheme = e.IdProgramScheme',array())
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
						->joinLeft(array('itk'=>'tbl_intake'), 'str.IdIntake = itk.IdIntake',array('IntakeDesc'))
						->joinLeft(array('fsp'=>'fee_structure'), 'fsp.fs_id = str.fs_id',array('fs_name'))
						->joinLeft(array('dt'=>'discount_type'), 'dt.dt_id = r.dcnt_type_id',array('dt_discount'))
						->joinLeft(array('dtp'=>'discount_type_tag'), 'dtp.dt_id = r.dcnt_type_id and dtp.dt_id = dt.dt_id and dtp.IdStudentRegistration = r.dcnt_IdStudentRegistration',array('dtt_start_date','dtt_end_date'))
//						->where("r.dcnt_status = 'A'")
			->order('r.dcnt_create_date desc')
			->group('r.dcnt_id');
							
		if($dateFrom!=''){
			$selectData->where("DATE(r.dcnt_create_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(r.dcnt_create_date) <= ?",$dateTo);
		}
		if(isset($discounttype) && !empty($discounttype)){
			$selectData->where("r.dcnt_type_id = ?",$discounttype);
		}
	
		$row = $db->fetchAll($selectData);
		
		if($row){
			$m=0;
				
						foreach($row as $rci){
							$idDisc = $rci['dcnt_id'];
							$selectSubject = $db->select()
										->from(array('a'=>'discount_detail'))
										->joinLeft(array('b'=>'invoice_detail'), 'b.id = a.dcnt_invoice_det_id and b.invoice_main_id = a.dcnt_invoice_id',array('amount'))
										->joinLeft(array('c'=>'invoice_main'), 'c.id = a.dcnt_invoice_id',array('exchange_rate','currency_id'))
										->where("a.dcnt_id = ?", (int)$idDisc);
						
							$receiptSubject = $db->fetchAll($selectSubject);
							$total = 0;
							foreach($receiptSubject as $r){
								$amount = $r['amount'];
								if($r['currency_id'] == 2){
									$currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
									$currency = $currencyDb->getData($r['exchange_rate']);
		
									$amount = number_format($r['amount'] * $currency['cr_exchange_rate'],2);
								}
		
								$total += $amount;
							}
							
							
							$row[$m]['invoiceAmount'] =  $total;
							
							$m++;
						}
						
					}
					
//				echo "<pre>";
//		print_r($row);		
		return $row;
	}
}

