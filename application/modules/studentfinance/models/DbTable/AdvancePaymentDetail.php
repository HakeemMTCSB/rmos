<?php
class Studentfinance_Model_DbTable_AdvancePaymentDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'advance_payment_detail';
	protected $_primary = "advpydet_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->where("ap.advpy_id ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}

	
	/*
	 * Overite Update function
	 */
/*	public function update($data=null){
		
		return null;
	}*/
	
	public function getDataDetail($parent_id){
		$db = Zend_Db_Table::getDefaultAdapter();
	 	$selectData = $db->select()
					->from(array('apd'=>$this->_name))
					->join(array('ap'=>'advance_payment'), 'ap.advpy_id = apd.advpydet_advpy_id')
					->joinLeft(array('adj'=>'advance_payment_adjustment'), "adj.IdAdv_Det = apd.advpydet_id and advpydet_status = 'X'",array('AdjustmentCode','cancelDate'=>'UpdDate'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = ap.advpy_cur_id')
					->joinLeft(array('r'=>'receipt'), 'r.rcp_id = ap.advpy_rcp_id')
					->joinLeft(array('im'=>'invoice_main'), 'im.id = apd.advpydet_inv_id')
					->joinLeft(array('id'=>'invoice_detail'), 'apd.advpydet_inv_det_id = id.id and id.invoice_main_id = im.id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = im.creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->joinLeft(array('us'=>'tbl_user'), 'us.iduser = apd.advpydet_updby', array())
					->joinLeft(array('tss'=>'tbl_staffmaster'), 'tss.IdStaff = us.IdStaff', array('updname'=>'FullName'))
					->joinLeft(array('uc'=>'tbl_user'), 'uc.iduser = adj.UpdUser', array())
					->joinLeft(array('tsc'=>'tbl_staffmaster'), 'tsc.IdStaff = uc.IdStaff', array('cancelname'=>'FullName'))
					->where("apd.advpydet_advpy_id = ?", (int)$parent_id)
					->where('apd.advpydet_inv_id IS NOT NULL')
					->where('apd.advpydet_inv_det_id != 0');

		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
	public function getDataDetailActive($parent_id){
		$db = Zend_Db_Table::getDefaultAdapter();
	 	$selectData = $db->select()
					->from(array('apd'=>$this->_name))
					->join(array('ap'=>'advance_payment'), 'ap.advpy_id = apd.advpydet_advpy_id')
					->joinLeft(array('adj'=>'advance_payment_adjustment'), "adj.IdAdv_Det = apd.advpydet_id and advpydet_status = 'X'",array('AdjustmentCode','cancelDate'=>'UpdDate'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = ap.advpy_cur_id')
					->joinLeft(array('r'=>'receipt'), 'r.rcp_id = ap.advpy_rcp_id')
					->joinLeft(array('im'=>'invoice_main'), 'im.id = apd.advpydet_inv_id')
					->joinLeft(array('id'=>'invoice_detail'), 'apd.advpydet_inv_det_id = id.id and id.invoice_main_id = im.id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = im.creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->joinLeft(array('us'=>'tbl_user'), 'us.iduser = apd.advpydet_updby', array())
					->joinLeft(array('tss'=>'tbl_staffmaster'), 'tss.IdStaff = us.IdStaff', array('updname'=>'FullName'))
					->joinLeft(array('uc'=>'tbl_user'), 'uc.iduser = adj.UpdUser', array())
					->joinLeft(array('tsc'=>'tbl_staffmaster'), 'tsc.IdStaff = uc.IdStaff', array('cancelname'=>'FullName'))
					->where("apd.advpydet_advpy_id = ?", (int)$parent_id)
					->where('apd.advpydet_inv_id IS NOT NULL')
					->where("apd.advpydet_status = 'A'")
					->where('apd.advpydet_inv_det_id != 0');

		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
	public function getDataDetailUtilized($advID){
		$db = Zend_Db_Table::getDefaultAdapter();
	 	$selectData = $db->select()
					->from(array('apd'=>$this->_name))
					->join(array('ap'=>'advance_payment'), 'ap.advpy_id = apd.advpydet_advpy_id')
					->where("apd.advpydet_id = ?", (int)$advID);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getAllBillPayment($billing_no){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('apd'=>$this->_name))
		->joinLeft(array('ap'=>'advance_payment'), 'ap.advpy_id = apd.advpydet_advpy_id')
		->joinLeft(array('im'=>'invoice_main'), 'im.id = ap.advpy_invoice_id')
		->joinLeft(array('u'=>'tbl_user'), 'u.iduser = im.creator', array())
		->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
		->where('apd.advpydet_status = ?', 'A')
		->where('ap.advpy_status = ?', 'A')
		->where("apd.advpydet_inv_id = ?", (int)$billing_no);
		
		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
		
	}
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
			
		return parent::insert( $data );
	}
	
	public function getUtilizeData($searchData=null){
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoice_id = $searchData['invoice_id'];
		$inv_Detail = $searchData['invoice_detail_id'];
		
		$db = Zend_Db_Table::getDefaultAdapter();

		echo $selectData = $db->select()
					->from(array('d'=>'advance_payment_detail'))
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.advpydet_inv_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array())
					->where('d.advpydet_inv_id = ?', $invoice_id)
					->where('d.advpydet_inv_det_id = ?', $inv_Detail)
					->where("DATE(d.advpydet_upddate) >= ?",$dateFrom)
					->where("DATE(d.advpydet_upddate) <= ?",$dateTo);
		
		$row = $db->fetchAll($selectData);
echo "<pre>";
print_r($row);
		return $row;
	}
}
?>