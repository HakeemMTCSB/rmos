<?php
class Studentfinance_Model_DbTable_SuspendMain extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'suspend_main';
	protected $_primary = "id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('sm'=>$this->_name))
					->where("sm.id ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getBankTransactionRecord($bank_transaction_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('sm'=>$this->_name))
					->join(array('pbrd'=>'payment_bank_record_detail'), 'pbrd.id = sm.transaction_reference', array())
					->where("pbrd.pbr_id = ?", (int)$bank_transaction_id);

		$row = $db->fetchAll($selectData);				
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	/**
	 * 
	 * Save Bank transaction to main payment record and detail payment record
	 * @param array $data_bank_detail
	 * @throws Exception
	 */
	public function saveBankSuspendTransaction($data_bank_detail, $payment_bank_detail_id_ref){
		try {
			
			//get transaction data
			$transaction = $this->getTransaction($data_bank_detail['payee_id']);

			$data = array(
	       		'billing_no' => $data_bank_detail['billing_no'],
	       		'appl_id' => $transaction['at_pes_id'],
	       		'payment _description' => $data_bank_detail['bill_ref_5'],
	       		'amount' => $data_bank_detail['amount_total'],
	       		'payment_mode' => 'SPC-BNI',
	       		'transaction_reference' => $payment_bank_detail_id_ref
	       	);
	       		
	       	$payment_id = $this->insert($data);
	       		
	       	/*
	       	 * save suspend record detail
	       	 */
	       	
	       	//get invoice detail item sequence for payment detail description as we set in proforma invoice
	       	$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
	        $invoice_detail = $feeItemDb->getActiveFeeItem();
	        
	       	$suspendDetailDb = new Studentfinance_Model_DbTable_SuspendDetail();	
	       	for($i=0; $i<10; $i++){
	       		
	       		if( isset($data_bank_detail['amount_'.($i+1)]) && $data_bank_detail['amount_'.($i+1)]!=0 && $data_bank_detail['amount_'.($i+1)]!= null ){
		       		$data = array(
		       			'sm_id' => $payment_id,
		       			'item_description' => $invoice_detail[$i]['fi_name_bahasa'],
		       			'item_amount' => $data_bank_detail['amount_'.($i+1)]
		       		);
		       			
		       		$suspendDetailDb->insert($data);
	       		}
	       	}
		}catch (Exception $e) {
		    throw new Exception('Error in Suspend Main');
		}
	}

	/*
	 * Overite Insert function
	 */
	
	public function insert($data){
		
		if( !isset($data['payment_date']) ){
			$data['payment_date'] = date('Y-m-d H:i:s');
		}
			
		return parent::insert( $data );
	}
	
	private function getTransaction($payer_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//get application type
		$selectData = $db->select()
					->from(array('at'=>'applicant_transaction'))
					->where("at.at_pes_id = ?", $payer_id);
			
		$txn_row = $db->fetchRow($selectData);			

		if(!$txn_row){
			return null;
		}else{
			return $txn_row;
		}
	}

}
?>