<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 27/10/2015
 * Time: 4:11 PM
 */
class Studentfinance_Model_DbTable_SponsorPaymentReport extends Zend_Db_Table {

    public function getSponsorPayment($search = false, $type = 1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('c'=>'sponsor_invoice_detail'), array(
                'value'=>'a.*',
                'theammount'=>'c.sp_paid',
                'balamount'=>'c.sp_balance',
                //'approveddate'=>'m.sp_approve_date',
                'curid'=>'a.rcp_cur_id',
                'totalamount'=>'a.rcp_amount',
                'totalstudentamount'=>'e.rcp_inv_amount',
                'advamount'=>'a.rcp_adv_amount',
                'advamountstudent'=>'e.rcp_adv_amount',
                'dtlstatus'=>'c.sp_status',
                'canceldatemain'=>'a.cancel_date',
                'canceldatedtl'=>'c.sp_cancel_date'
            ))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'b.rcp_inv_sp_id = c.sp_id', array())
            ->join(array('a'=>'sponsor_invoice_receipt'), 'a.rcp_id = b.rcp_inv_rcp_id', array())
            //->join(array('m'=>'sponsor_invoice_main'), 'c.sp_id = m.sp_id', array())
            ->join(array('d'=>'invoice_main'), 'c.sp_invoice_id = d.id', array('d.invoice_date', 'd.bill_number', 'd.bill_description', 'd.currency_id'))
            ->joinLeft(array('e'=>'sponsor_invoice_receipt_student'), 'b.rcp_inv_student = e.rcp_inv_id', array())
            ->join(array('f'=>'tbl_studentregistration'), 'd.IdStudentRegistration = f.IdStudentRegistration', array('f.registrationId'))
            ->join(array('k'=>'student_profile'), 'f.sp_id = k.id', array('k.appl_fname', 'k.appl_lname', 'k.appl_religion'))
            ->joinLeft(array('g'=>'tbl_program'), 'f.IdProgram = g.IdProgram', array('g.ProgramName'))
            ->joinLeft(array('h'=>'tbl_intake'), 'f.IdIntake = h.IdIntake', array('h.IntakeDesc'))
            ->joinLeft(array('i'=>'tbl_semestermaster'), 'd.semester = i.IdSemesterMaster', array('i.SemesterMainName'))
            ->joinLeft(array('o'=>'tbl_program_scheme'), 'f.IdProgramScheme = o.IdProgramScheme', array())
            ->joinLeft(array('p'=>'tbl_definationms'), 'o.mode_of_program = p.idDefinition', array('mop'=>'p.DefinitionDesc'))
            ->joinLeft(array('q'=>'tbl_definationms'), 'o.mode_of_study = q.idDefinition', array('mos'=>'q.DefinitionDesc'))
            ->joinLeft(array('r'=>'tbl_definationms'), 'o.program_type = p.idDefinition', array('pt'=>'r.DefinitionDesc'))
            //->joinLeft(array('j'=>'payment_mode'), 'a.p_payment_mode_id = j.id', array())
            //->joinLeft(array('l'=>'tbl_currency_rate'), 'd.exchange_rate = l.cr_id', array())
            ->joinLeft(array('n'=>'tbl_currency'), 'a.rcp_cur_id = n.cur_id', array('cursymbol'=>'n.cur_code'))
            ->where('a.rcp_status != ?', 'ENTRY')
            //->where('f.IdStudentRegistration = ?', 3731)
            //->where('a.rcp_no = ?', 'SPM2015-00078')
            //->where('(IFNULL(a.rcp_receive_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" OR IFNULL(a.cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" OR IFNULL(c.sp_cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'") AND (IFNULL(a.rcp_receive_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'" OR IFNULL(a.cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'" OR IFNULL(c.sp_cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'")')
            ->where('(IFNULL(a.rcp_receive_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" AND IFNULL(a.rcp_receive_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'") OR (IFNULL(a.cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" AND IFNULL(a.cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'") OR (IFNULL(c.sp_cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" AND IFNULL(c.sp_cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'")')
            ->order('a.rcp_receive_date');

            if ($type == 1) {
                $select->group('c.id');
            }else{
                $select->group('e.rcp_inv_id');
            }
            //->limit(3000);

        if ($search != false){
            /*if (isset($search['DateFrom']) && $search['DateFrom']!=''){
                $select->where('a.rcp_receive_date >= ?', date('Y-m-d', strtotime($search['DateFrom'])));
            }
            if (isset($search['DateTo']) && $search['DateTo']!=''){
                $select->where('a.rcp_receive_date <= ?', date('Y-m-d', strtotime($search['DateTo'])));
            }*/
            /*if (isset($search['DateFrom']) && $search['DateFrom']!=''){
                $select->where('a.rcp_receive_date >= '.date('Y-m-d', strtotime($search['DateFrom'])).' OR a.cancel_date >= '.date('Y-m-d', strtotime($search['DateFrom'])).' OR c.sp_cancel_date >= '.date('Y-m-d', strtotime($search['DateFrom'])));
            }
            if (isset($search['DateTo']) && $search['DateTo']!=''){
                $select->where('a.rcp_receive_date <= '.date('Y-m-d', strtotime($search['DateTo'])).' OR a.cancel_date <= '.date('Y-m-d', strtotime($search['DateTo'])).' OR c.sp_cancel_date <= '.date('Y-m-d', strtotime($search['DateTo'])));
            }*/
            if (isset($search['Program']) && $search['Program']!=''){
                $select->where('f.IdProgram = ?', $search['Program']);
            }
            if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                $select->where('f.IdProgramScheme = ?', $search['ProgramScheme']);
            }
            if (isset($search['stype']) && $search['stype']!=''){
                $select->where('a.rcp_type = ?', $search['stype']);
            }
            if (isset($search['Scholarship']) && $search['Scholarship']!=''){
                $select->where('a.rcp_type_id = ?', $search['Scholarship']);
            }
            if (isset($search['PaymentMode']) && $search['PaymentMode']!=''){
                $select->where('j.payment_group = ?', $search['PaymentMode']);
            }
        }
        //echo $select; //exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSponsorAdvancePayment($search = false, $type = 1){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('c'=>'sponsor_invoice_detail'), array(
                'value'=>'a.*',
                'theammount'=>'c.sp_paid',
                'balamount'=>'c.sp_balance',
                //'approveddate'=>'m.sp_approve_date',
                'curid'=>'a.rcp_cur_id',
                'totalamount'=>'a.rcp_amount',
                'totalstudentamount'=>'e.rcp_inv_amount',
                'advamount'=>'a.rcp_adv_amount',
                'advamountstudent'=>'e.rcp_adv_amount',
                'dtlstatus'=>'c.sp_status',
                'canceldatemain'=>'a.cancel_date',
                'canceldatedtl'=>'c.sp_cancel_date'
            ))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'b.rcp_inv_sp_id = c.sp_id', array())
            ->join(array('a'=>'sponsor_invoice_receipt'), 'a.rcp_id = b.rcp_inv_rcp_id', array('a.rcp_cur_id', 'a.rcp_receive_date'))
            //->join(array('m'=>'sponsor_invoice_main'), 'c.sp_id = m.sp_id', array())
            ->join(array('d'=>'invoice_main'), 'c.sp_invoice_id = d.id', array('d.invoice_date', 'd.bill_number', 'd.bill_description', 'd.currency_id'))
            ->joinLeft(array('e'=>'sponsor_invoice_receipt_student'), 'b.rcp_inv_student = e.rcp_inv_id', array())
            ->join(array('f'=>'tbl_studentregistration'), 'd.IdStudentRegistration = f.IdStudentRegistration', array('f.registrationId'))
            ->join(array('k'=>'student_profile'), 'f.sp_id = k.id', array('k.appl_fname', 'k.appl_lname', 'k.appl_religion'))
            ->joinLeft(array('g'=>'tbl_program'), 'f.IdProgram = g.IdProgram', array('g.ProgramName'))
            ->joinLeft(array('h'=>'tbl_intake'), 'f.IdIntake = h.IdIntake', array('h.IntakeDesc'))
            ->joinLeft(array('i'=>'tbl_semestermaster'), 'd.semester = i.IdSemesterMaster', array('i.SemesterMainName'))
            //->joinLeft(array('j'=>'payment_mode'), 'a.p_payment_mode_id = j.id', array())
            //->joinLeft(array('l'=>'tbl_currency_rate'), 'd.exchange_rate = l.cr_id', array())
            ->joinLeft(array('n'=>'tbl_currency'), 'a.rcp_cur_id = n.cur_id', array('cursymbol'=>'n.cur_code'))
            ->joinLeft(array('o'=>'advance_payment'), 'a.rcp_id = o.advpy_rcp_id', array('o.advpy_cancel_date', 'o.advpy_status'))
            ->joinLeft(array('p'=>'tbl_semestermaster'), 'o.advpy_sem_id = p.IdSemesterMaster', array('semadv'=>'p.SemesterMainName'))
            ->joinLeft(array('q'=>'tbl_program_scheme'), 'f.IdProgramScheme = q.IdProgramScheme', array())
            ->joinLeft(array('r'=>'tbl_definationms'), 'q.mode_of_program = r.idDefinition', array('mop'=>'r.DefinitionDesc'))
            ->joinLeft(array('s'=>'tbl_definationms'), 'q.mode_of_study = s.idDefinition', array('mos'=>'s.DefinitionDesc'))
            ->joinLeft(array('t'=>'tbl_definationms'), 'q.program_type = t.idDefinition', array('pt'=>'t.DefinitionDesc'))
            ->where('a.rcp_status != ?', 'ENTRY')
            //->where('f.IdStudentRegistration = ?', 3731)
            //->where('a.rcp_no = ?', 'SPM2015-00078')
            //->where('(IFNULL(a.rcp_receive_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" OR IFNULL(a.cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" OR IFNULL(c.sp_cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'") AND (IFNULL(a.rcp_receive_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'" OR IFNULL(a.cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'" OR IFNULL(c.sp_cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'")')
            ->where('(IFNULL(a.rcp_receive_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" AND IFNULL(a.rcp_receive_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'") OR (IFNULL(a.cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" AND IFNULL(a.cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'") OR (IFNULL(c.sp_cancel_date, "0000-00-00") >= "'.date('Y-m-d', strtotime($search['DateFrom'])).'" AND IFNULL(c.sp_cancel_date, "0000-00-00") <= "'.date('Y-m-d', strtotime($search['DateTo'])).'")')
            ->order('a.rcp_receive_date');

        if ($type == 1) {
            $select->group('c.id');
        }else{
            $select->group('e.rcp_inv_id');
        }
        //->limit(3000);

        if ($search != false){
            /*if (isset($search['DateFrom']) && $search['DateFrom']!=''){
                $select->where('a.rcp_receive_date >= ?', date('Y-m-d', strtotime($search['DateFrom'])));
            }
            if (isset($search['DateTo']) && $search['DateTo']!=''){
                $select->where('a.rcp_receive_date <= ?', date('Y-m-d', strtotime($search['DateTo'])));
            }*/
            /*if (isset($search['DateFrom']) && $search['DateFrom']!=''){
                $select->where('a.rcp_receive_date >= '.date('Y-m-d', strtotime($search['DateFrom'])).' OR a.cancel_date >= '.date('Y-m-d', strtotime($search['DateFrom'])).' OR c.sp_cancel_date >= '.date('Y-m-d', strtotime($search['DateFrom'])));
            }
            if (isset($search['DateTo']) && $search['DateTo']!=''){
                $select->where('a.rcp_receive_date <= '.date('Y-m-d', strtotime($search['DateTo'])).' OR a.cancel_date <= '.date('Y-m-d', strtotime($search['DateTo'])).' OR c.sp_cancel_date <= '.date('Y-m-d', strtotime($search['DateTo'])));
            }*/
            if (isset($search['Program']) && $search['Program']!=''){
                $select->where('f.IdProgram = ?', $search['Program']);
            }
            if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                $select->where('f.IdProgramScheme = ?', $search['ProgramScheme']);
            }
            if (isset($search['stype']) && $search['stype']!=''){
                $select->where('a.rcp_type = ?', $search['stype']);
            }
            if (isset($search['Scholarship']) && $search['Scholarship']!=''){
                $select->where('a.rcp_type_id = ?', $search['Scholarship']);
            }
            if (isset($search['PaymentMode']) && $search['PaymentMode']!=''){
                $select->where('j.payment_group = ?', $search['PaymentMode']);
            }
        }
        //echo $select; //exit;
        $result = $db->fetchAll($select);
        return $result;
    }
}