<?php

class Studentfinance_Model_DbTable_ScholarshipCountryWeight extends Zend_Db_Table {

    protected $_name = 'tbl_scholarship_country_weight';
    protected $_primary = 'id';

    public function save(array $data){

        if( isset($data[$this->_primary]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }

    /**
     * feeds an age and returns the point based on
     */
    public function get_point( $country_id ) {
        //get the country's imf rank
        $Country = new App_Model_Country();
        $country_select = $Country->select()
                    ->where('IdCountry = ?', $country_id)
        ;

        $country = $Country->fetchRow($country_select);
        if(empty($country)) {
            return false;
        }

        $country_rank = $country->imf_ranking;
        if(empty($country_rank)) {
            return(0);
        }
        
        $select = $this->select()
            ->where('`from` <= ?', $country_rank)
            ->where('`to` >= ?', $country_rank)
        ;
        $rule = $this->fetchRow($select);
        if(empty($rule)) {
            return 0;
        } else {
            return $rule->score;
        }

    }
}