<?php
class Studentfinance_Model_DbTable_Sponsorinvoiceold extends Zend_Db_Table { //Model Class for Users Details
	//protected $_name = 'tbl_charges';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function fngetStudentApplicationDetails()//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
			$consistantresult = 'SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication';
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
 						->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'))
 						->where("b.idsponsor is not null");
					
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
   public function fngetStudentDetails($lintidstudent) { //Function to get the Program Branch details
 			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
 			$lstrSelect = $lobjDbAdpt->select()
 					   				 ->from(array('b'=>'tbl_studentapplication'))
               						 ->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
               						 ->join(array('d'=>'tbl_sponsor'),'b.idsponsor=d.idsponsor',array("CONCAT(d.fName,' ',IFNULL(d.mName,' '),' ',IFNULL(d.	lName,' ')) AS SponsorName"))
               						 ->where("b.IdApplication= ?",'app201200145');
       $result = $lobjDbAdpt->fetchRow($lstrSelect);
       return $result;
     }
     
     /*
      * function for getting the prog charges
      */
    public function fngetProgramDetails($lintidprogram) 
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
      	$currentdate = date ('Y-m-d');
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_programcharges'),array('a.Charges','a.IdCharges'))
                			->join(array('b'=>'tbl_charges'),'a.IdCharges = b.IdCharges',array('b.ChargeName'))
                			->where('b.effectiveDate <= ?',$currentdate)
                			->where('a.IdProgram = ?',$lintidprogram)
                			->where('b.Payment = 1');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
	
    /*
     * function to get the drop down of the student
     */
  public function fngetStudentDropdown() { //Function to get the Program Branch details
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(a.FName,''),IFNULL(a.MName,''),IFNULL(a.LName,''))"))
		 				  ->join(array('b'=>'tbl_studentregistration'),'a.IdApplication=b.IdApplication',array())
		                  ->where("a.idsponsor is not null")
		               		->group('a.IdApplication');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	/*
	 * function to search for the student in the sponsor manual invoice
	 */
	public function fnSearchStudentApplication($post)
	{
	 $idstudent = $post['field5'];
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
          if($post['field5']!='')
          {
			$consistantresult = "SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication AND i.IdApplication=$idstudent";
          }
          else {
          	$consistantresult = "SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication AND i.IdApplication=b.IdApplication";
          }
          $lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
 						->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'))
 						->where("b.idsponsor is not null");
         
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
    /*
     * function for fetching the subject charges
     */
    public function fnGetSubjectCharges($lintIdStudentRegistration)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentregsubjects'),array('a.*'))
                			->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject')
                			->where('a.IdStudentRegistration = ?',$lintIdStudentRegistration);
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                			
    }
    
    /*
     * function to insert into the invoicemaster table
     */
    public function fnAddInvoiceMaster($larrformData)
    {
    	    $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_invoicemaster";
            $postData = array(		
							'IdStudent' => $larrformData['Studentid'],		
            				'InvoiceNo' =>'Dec-'.' '.$larrformData['Studentid'],		
		            		'InvoiceDt' =>$larrformData['InvoiceDate'],	
		            		'InvoiceAmt'=>$larrformData['InvoiceAmount'],
            				'MonthYear'=>$larrformData['MonthYear'],
            				'AcdmcPeriod' =>$larrformData['AcademicPeriod'],		
            				'Naration' =>$larrformData['Naration'],		
		            		'UpdDate' =>$larrformData['UpdDate'],	
		            		'UpdUser'=>$larrformData['UpdUser'],
            				'Approved'=>0,
            				'Active'=>0,
            				'idsponsor'=>$larrformData['Sponsorid'],
						);			
	        $db->insert($table,$postData);
	        $lastinvoiceid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_invoicemaster','IdInvoice');
	        /////////////////inserting into the invoice details///////////////////
	         for($i=0;$i<count($larrformData['IdCharges']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicedetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
								'idAccount' => $larrformData['IdCharges'][$i],		
	            				'Amount' =>$larrformData['Charges'][$i],		
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser'],
			            		'Discount' =>0,	
	                            'Active'=>1
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   
	        ////////////////End of invoice details///////////////////////////////
	        
	       /////////////////inserting into the Subjecct Details///////////////////
	         for($i=0;$i<count($larrformData['IdSubject']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicesubjectdetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
								'IdSubject' => $larrformData['IdSubject'][$i],		
	            				'Amount' =>$larrformData['Subjectamount'][$i],		
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser']
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   
	        ////////////////End of subject Details/////////////////////////////// 
		     
		     
		     
    }
}