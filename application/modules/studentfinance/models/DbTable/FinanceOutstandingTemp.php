<?php
class Studentfinance_Model_DbTable_FinanceOutstandingTemp extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'finance_outstanding_temp';
	protected $_primary = "id";
		
	public function getData($search = false){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('a'=>$this->_name))
					->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdBranch'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->joinLeft(array('pa'=>'tbl_program'),'str.IdProgram = pa.IdProgram',array('pa.ProgramCode','pa.ProgramName','IdScheme'))
						->joinLeft(array('it'=>'tbl_intake'),'str.IdIntake = it.IdIntake',array('IntakeDesc'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'str.IdProgramScheme = e.IdProgramScheme',array())
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			            ->order('str.registrationId desc');
			            
		if($search != false){
			if (isset($search['program']) && $search['program'] != ''){
				$selectData->where('str.IdProgram = ?', $search['program']);
			}

			if (isset($search['id']) && $search['id'] != ''){
				$selectData->where('str.registrationId LIKE "%'.$search['id'].'%"');
			}
		}

		$row = $db->fetchAll($selectData);
//		echo $selectData;exit;
		return $row;
	}

	public function getRoleBarRoleList(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_barringrole'), array('value'=>'*'))
			->join(array('b'=>'tbl_definationms'), 'a.brole_role = b.idDefinition')
			->group('a.brole_role');

		$result = $db->fetchAll($select);
		return $result;
	}
}
?>