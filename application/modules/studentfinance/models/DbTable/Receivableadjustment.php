<?php
class Studentfinance_Model_DbTable_Receivableadjustment extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_receivable_adjustment';
	private $lobjDbAdpt;
	private $lobjDefCodeModel;
	private $lobjReceiptModel;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		$this->lobjReceiptModel = new Studentfinance_Model_DbTable_Receipt();
		$this->currLocale = Zend_Registry::get('Zend_Locale');
	}

	public function fngetReceiptnoDetails($lstrreceiptnumber){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "receipt"), array("*"))
				//->joinLeft(array("c" => "tbl_receipt_invoice"),'c.IdReceipt = a.IdReceipt',array("c.IdInvoice","c.IdInvoiceDetail"))
				->where("a.rcp_no = ?",$lstrreceiptnumber);
				//->where("a.Status = ?",241);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function fnAddReceivableAdjustment($larrformData,$keyID,$lstrAdjustmentNumber){
		$receiptreceivablecount = array();
		$larrdefCode = $larrformData['AdjustmentType'];
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
			
		$paramArray = array(
				'AdjustmentCode' => $lstrAdjustmentNumber,
				'AdjustmentType' => $larrformData['AdjustmentType'],
				'DocumentDate' => $larrformData['DocumentDate'],
				'IdReceipt' => $larrformData['idreceipt'],
				'Remarks' => $larrformData['Remarks'],
				'Status' => $keyID,
				'EnterBy' => $getUserIdentity->id,
				'EnterDate' => date('Y-m-d H:i:s'),
				'UpdUser'=>$getUserIdentity->id,
				'UpdDate'=>date('Y-m-d H:i:s'),
				'IdUniversity'=>$larrformData['IdUniversity']
		);
		$this->lobjDbAdpt->insert('tbl_receivable_adjustment',$paramArray);
		$lastinsertId = $this->lobjDbAdpt->lastInsertId();
		
		$lobjReceiptModel = new Studentfinance_Model_DbTable_Receipt();
		
		$where = "rcp_id = '".$larrformData['idreceipt']."'";
		
		switch ($larrdefCode) {
			case "865": //return cheque
				$this->fncreatenewreceivableadjustment($larrformData,$keyID,$lastinsertId);
			break;
			case "863": //wrong payment mode
//				$this->fnaddwrongpaymentmodereceivable($larrformData,$keyID,$lastinsertId);
				$this->fncreatenewreceivableadjustment($larrformData,$keyID,$lastinsertId);
				break;
			case "862": //cancellation w/o replacement
//				$this->fnaddwrongpaymentreceivablemode($larrformData,$keyID,$lastinsertId);
				$data['rcp_status'] = 'CANCEL';
				
				$lobjReceiptModel->update($data,$where);
			break;
		}// switch loop end
		//$receiptreceivablecount['CountRecivableadjustment'] = 1;
		
		//$lobjReceiptModel->update($receiptreceivablecount,$where);
	}

	// Function to add new receivable adjustment entry in case of Wrong Payment item
	public function fnaddwrongpaymentreceivableitem($larrformData,$keyID,$lstrAdjustmentNumber){
		$paramArray = array(
					'AdjustmentCode' => $lstrAdjustmentNumber,
					'AdjustmentType' => $larrformData['AdjustmentType'],
					'DocumentDate' => $larrformData['DocumentDate'],
					'IdReceipt' => $larrformData['idreceipt'],
					'Remarks' => $larrformData['Remarks'],
					'Status' => $keyID,
					'EnterBy' => $larrformData['EnterBy'],
					'EnterDate' => $larrformData['EnterDate'],
					'UpdUser'=>$larrformData['UpdUser'],
					'UpdDate'=>$larrformData['UpdDate'],
					'IdUniversity'=>$larrformData['IdUniversity'],
				);
		$this->lobjDbAdpt->insert('tbl_receivable_adjustment',$paramArray);
		// Now need to insert receivable adjustment invoice detail
		$lastinsertId = $this->lobjDbAdpt->lastInsertId();
		$this->fnaddreceivableinvoicedetl($larrformData,$lastinsertId);
		// Now need to insert receivable adjustment noninvoice detail
		$this->fnaddreceivablenoninvoicedetl($larrformData,$lastinsertId);

	}

	// Function add receivable adjustment noninvoice detail
	public function fnaddreceivablenoninvoicedetl($data,$lastinsertId){
		$larrnoninvoicearray = array();
		$lintLen = count($data['FeeCodegrid']);
		for($i=0;$i<$lintLen;$i++){
			$larrnoninvoicearray['IdReceipt'] = $data['idreceipt'];
			$larrnoninvoicearray['IdReceivableAdjustment'] = $lastinsertId;
			$larrnoninvoicearray['Description'] = $data['NonInvoiceDescriptiongrid'][$i];
			$larrnoninvoicearray['NonInvoiceDescriptiondefault'] = $data['NonInvoiceDescriptiondefaultgrid'][$i];
			$larrnoninvoicearray['Amount'] = $data['Amountkgrid'][$i];
			$larrnoninvoicearray['FeeCode'] = $data['FeeCodegrid'][$i];
			$larrnoninvoicearray['ChargeCode'] = $data['ChargeCodegrid'][$i];
			$larrnoninvoicearray['Status'] = 1;
			$larrnoninvoicearray['UpdUser'] = $data['UpdUser'];
			$larrnoninvoicearray['UpdDate'] = $data['UpdDate'];
			$this->lobjDbAdpt->insert('tbl_receivable_adjustment_noninvoice',$larrnoninvoicearray);
		}
	}
	// Function add receivable adjustment invoice detail
	public function fnaddreceivableinvoicedetl($data,$idreceivableadjustment){
		$larrinvoicearray = array();
		for($i=1;$i<=$data['count'];$i++){
			$larrinvoicearray['IdReceipt'] = $data['idreceipt'];
			$larrinvoicearray['IdReceivableAdjustment'] = $idreceivableadjustment;

			$key = 'invoice_'.$i;
			$pices = explode('_',$data[$key]);
			$larrinvoicearray['IdInvoice'] = $pices[0];
			$larrinvoicearray['IdInvoiceDetail'] = $pices[1];

			$totalamountkey = 'totalamount_'.$pices[0].'_'.$pices[1];
			$totalpaidkey = 'tobepaid_'.$pices[0].'_'.$pices[1];
			$totalbalancekey = 'totalbalance_'.$pices[0].'_'.$pices[1];
			$newamountpaidkey = 'newamountpaid_'.$pices[0].'_'.$pices[1];

			$larrinvoicearray['TotalAmountReceipt'] = $data[$totalamountkey];
			$larrinvoicearray['TotalPaidReceipt'] = $data[$totalpaidkey];
			$larrinvoicearray['TotalBalanceReceipt'] = $data[$totalbalancekey];

			$larrinvoicearray['NewPaidAmount'] = $data[$newamountpaidkey];
			$larrinvoicearray['UpdUser'] = $data['UpdUser'];
			$larrinvoicearray['UpdDate'] = $data['UpdDate'];
			$this->lobjDbAdpt->insert('tbl_receivable_adjustment_invoice',$larrinvoicearray);
		}
	}



	// Function to add new receivable adjustment entry in case of Wrong payment mode
	public function fnaddwrongpaymentreceivablemode($larrformData,$keyID,$lstrAdjustmentNumber){
		$paramArray = array(
					'AdjustmentCode' => $lstrAdjustmentNumber,
					'AdjustmentType' => $larrformData['AdjustmentType'],
					'DocumentDate' => $larrformData['DocumentDate'],
					'IdReceipt' => $larrformData['idreceipt'],
					'Remarks' => $larrformData['Remarks'],
					'Status' => $keyID,
					'EnterBy' => $larrformData['EnterBy'],
					'EnterDate' => $larrformData['EnterDate'],
					'UpdUser'=>$larrformData['UpdUser'],
					'UpdDate'=>$larrformData['UpdDate'],
					'IdUniversity'=>$larrformData['IdUniversity'],
				);
		$this->lobjDbAdpt->insert('tbl_receivable_adjustment',$paramArray);
	}

	// Function to add new receivable adjustment entry in case of Return Cheqeue
	public function fncreatenewreceivableadjustment($larrformData,$keyID,$lstrAdjustmentNumber){
		
		$check = count($larrformData['IdPaymentGroupgrid']);
		if($check>0){
			$update = array('p_status' => 'CANCEL');
			$where_up = "  p_rcp_id = '".$larrformData['idreceipt']."'";
			$this->lobjDbAdpt->update('payment',$update, $where_up);
			for($i=0;$i<$check;$i++) {
				$paramArray1 = array(
						'p_status' => 'ENTRY',
						'p_rcp_id' => $larrformData['idreceipt'],
//						'IdPaymentGroup' => $larrformData['IdPaymentGroupgrid'][$i],
						'p_terminal_id' => $larrformData['TerminalIdgrid'][$i],
						'p_card_no' => $larrformData['BankCardNogrid'][$i],
						'p_payment_mode_id' => $larrformData['PaymentModegrid'][$i],
						'p_cheque_no' => $larrformData['PaymentDocNogrid'][$i],
						'p_adjustment_id' => $lstrAdjustmentNumber,
						'created_date'=>date('Y-m-d H:i:s'),
						'created_by'=>$getUserIdentity->id
				);
				$this->lobjDbAdpt->insert('payment',$paramArray1);
			}
		}
	}
	
	
	// Function to add new receivable adjustment entry in case of Return Cheqeue
	public function fnaddwrongpaymentmodereceivable($larrformData,$keyID,$lstrAdjustmentNumber){
	
		$check = count($larrformData['IdPaymentGroupgrid']);
		if($check>0){
			$update = array('Status' => 'Old');
			$where_up = "  IdReceipt =' ".$larrformData['idreceipt']."' AND Status='New' ";
			$this->lobjDbAdpt->update('tbl_receipt_paymentinfo',$update, $where_up);
			for($i=0;$i<$check;$i++) {
				$paramArray1 = array(
						'Status' => 'New',
						'IdReceipt' => $larrformData['idreceipt'],
						'IdPaymentGroup' => $larrformData['IdPaymentGroupgrid'][$i],
						'PaymentMode' => $larrformData['PaymentModegrid'][$i],
						'TerminalId' => $larrformData['TerminalIdgrid'][$i],
						'BankCardNo' => $larrformData['BankCardNogrid'][$i],
						'BankCardType' => $larrformData['BankCardTypegrid'][$i],
						'PaymentMode' => $larrformData['PaymentModegrid'][$i],
						'PaymentDocNo' => $larrformData['PaymentDocNogrid'][$i],
						'PayDocBank' => $larrformData['PayDocBankgrid'][$i],
						'PayDocBankBranch' => $larrformData['PayDocBankBranchgrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_receipt_paymentinfo',$paramArray1);
			}
		}
	}

// 		if($larrformData['IdPaymentGroup'] == $lintPaymentGroupBankCard){
// 			$paramArray = array(
// 							'AdjustmentCode' => $lstrAdjustmentNumber,
// 							'AdjustmentType' => $larrformData['AdjustmentType'],
// 							'DocumentDate' => $larrformData['DocumentDate'],
// 							'IdReceipt' => $larrformData['idreceipt'],
// 							'Remarks' => $larrformData['Remarks'],
// 							'Status' => $keyID,
// 							'EnterBy' => $larrformData['EnterBy'],
// 							'EnterDate' => $larrformData['EnterDate'],
// 							'UpdUser'=>$larrformData['UpdUser'],
// 							'UpdDate'=>$larrformData['UpdDate'],
// 							'IdUniversity'=>$larrformData['IdUniversity'],
// 							'PaymentGroup'=>$larrformData['IdPaymentGroup'],
// 							'PaymentMode'=>$larrformData['PaymentMode'],
// 							'TerminalId'=>$larrformData['TerminalId'],
// 							'BankCardNo'=>$larrformData['BankCardNo'],
// 							'BankCardType'=>$larrformData['BankCardType']
// 						);
// 			$this->lobjDbAdpt->insert('tbl_receivable_adjustment',$paramArray);
// 		}else if($larrformData['IdPaymentGroup'] == $lintPaymentCashCard){
// 					$paramArray = array(
// 							'AdjustmentCode' => $lstrAdjustmentNumber,
// 							'AdjustmentType' => $larrformData['AdjustmentType'],
// 							'DocumentDate' => $larrformData['DocumentDate'],
// 							'IdReceipt' => $larrformData['idreceipt'],
// 							'Remarks' => $larrformData['Remarks'],
// 							'Status' => $keyID,
// 							'EnterBy' => $larrformData['EnterBy'],
// 							'EnterDate' => $larrformData['EnterDate'],
// 							'UpdUser'=>$larrformData['UpdUser'],
// 							'UpdDate'=>$larrformData['UpdDate'],
// 							'IdUniversity'=>$larrformData['IdUniversity'],
// 							'PaymentGroup'=>$larrformData['IdPaymentGroup'],
// 							'PaymentMode'=>$larrformData['PaymentMode'],
// 							'ChequeNo'=>$larrformData['PaymentDocNo'],
// 							'PaymentDocBank'=>$larrformData['PayDocBank'],
// 							'PayDocBankBranch'=>$larrformData['PayDocBankBranch']
// 					);
// 					$this->lobjDbAdpt->insert('tbl_receivable_adjustment',$paramArray);



	public function fncheckadjustmentcode($lstrcheck,$lIntIdUniversity){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receivable_adjustment"), array(""))
		->where("a.AdjustmentCode = ?",$lstrcheck)
		->where("a.IdUniversity=?",$lIntIdUniversity);
		$result = $this->lobjDbAdpt->fetchrow($select);
		return $result;
	}

	public function fncheckreceiptstatus($lstrReceiptStatus,$keyCheck){
		echo $select = $this->lobjDbAdpt->select()
		->from(array("a" => "receipt"), array("IdReceipt"=>"a.rcp_id","Status"=>"a.rcp_status"))
		->where("a.rcp_id = ?",$lstrReceiptStatus)
		->where("a.rcp_status=?",$keyCheck);
		$result = $this->lobjDbAdpt->fetchrow($select);
		return $result;
	}

	public function fngetbanktype(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_banktype"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetterminalid(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "creditcard_terminal"), array("IdTerminal"=>"a.id","TerminalID"=>"a.terminal_id"));
		
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetbankcard(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receivablesetup_bankcard"), array("a.IdBankCard","a.BankCardType"));
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetmodeofpayment(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receivablesetup_paymentmode"), array("a.IdPaymentMode","a.ModeOfPayment"));
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}


	public function fngetReceiptBankDetails($lstrreceiptnumber){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receipt"), array('a.PaymentDocNo','a.PayDocBankBranch','a.BankCardNo'))
		->joinLeft(array("b" => "tbl_definationms"),'a.IdPaymentGroup = b.idDefinition',array("PGkey"=>"b.idDefinition","PGname"=>"b.DefinitionCode"))
		->joinLeft(array("c" => "tbl_banktype"),'a.PayDocBank = c.AUTOINC',array("BCkey"=>"c.AUTOINC","BCname"=>"c.BANKCODE"))
		->joinLeft(array("d" => "tbl_receivablesetup_paymentmode"),'a.PaymentMode = d.IdPaymentMode',array("PMkey"=>"d.IdPaymentMode","PMname"=>"d.ModeOfPayment"))
		->joinLeft(array("e" => "tbl_receivablesetup_terminalid"),'a.TerminalId = e.IdTerminal',array("TIkey"=>"e.IdTerminal","TIname"=>"e.TerminalID"))
		->joinLeft(array("f" => "tbl_receivablesetup_bankcard"),'a.BankCardType = f.IdBankCard',array("BCTkey"=>"f.IdBankCard","BCTname"=>"f.BankCardType"))
		->where('a.IdReceipt =?',$lstrreceiptnumber);

		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);

		return $larrResult;
	}

	public function fnSearchReceivable($post = array()) {

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receivable_adjustment"), array("a.*"))
		->joinLeft(array('b'=>'tbl_user'),'b.iduser = a.UpdUser',array('b.loginName'))
		->joinLeft(array('c'=>'receipt'),'a.IdReceipt = c.rcp_id',array('rcp_no'));
		
		$select->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = a.AdjustmentType',array('d.DefinitionDesc as AdjustmentType'));
		
		/*if ( $this->currLocale == 'ar_YE')
		{
			$select->joinLeft(array('c'=>'tbl_definationms'),'c.idDefinition = a.Status',array('c.DefinitionCode','c.BahasaIndonesia as DefinitionDesc'));
			$select->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = a.AdjustmentType',array('d.BahasaIndonesia as AdjustmentType'));
		}
		else
		{
			$select->joinLeft(array('c'=>'tbl_definationms'),'c.idDefinition = a.Status',array('c.DefinitionCode','c.DefinitionDesc'));
			$select->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = a.AdjustmentType',array('d.DefinitionDesc as AdjustmentType'));
		}*/
		
		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where('a.AdjustmentCode like "%" ? "%"',$post['field2']);

		}
		if(isset($post['field5']) && !empty($post['field5']) ){
			$select = $select->where("a.AdjustmentType = ?",$post['field5']);

		}

		if(isset($post['field14']) && !empty($post['field14']) && isset($post['field15']) && !empty($post['field15'])  ){
			$select = $select->where("a.EnterDate between '".$post['field14']."' AND '".$post['field15']."' ");

		}
		$select->group("a.IdReceivableAdjustment")
		->order('EnterDate desc');
		
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetReceivableById($id){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receivable_adjustment"), array("a.*"))
		->joinLeft(array("b" => "receipt"),'b.rcp_id = a.IdReceipt',array("ReceiptCode"=>"b.rcp_no","TotalAmount"=>"b.rcp_amount",
				))
//				->joinLeft(array("c" => "tbl_bankcode"),'c.AUTOINC = b.BankCode',array("c.BANKACCT_CODE as BankAccount"))
//				->joinLeft(array("d" => "tbl_receivablesetup_terminalid"),'a.TerminalId = d.IdTerminal',array("d.TerminalID"))
				->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = a.PaymentGroup',array('e.DefinitionDesc'))
				//->joinLeft(array("e" => "tbl_receivablesetup_paymentmode"),'e.IdPaymentMode = a.PaymentMode',array("e.ModeOfPayment"))
		->where("a.IdReceivableAdjustment=?",$id);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fnUpdateReceivableAdjustment($data,$id){
		$where = 'IdReceivableAdjustment = '.$id;
		$this->lobjDbAdpt->update('tbl_receivable_adjustment',$data,$where);
	}

	public function fngetReceivableAdjustmentdet($id){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_receivable_adjustment"), array("a.*"))
					->where("a.IdReceivableAdjustment=?",$id);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetreceivableadjustmentinvoidedetl($receivableId){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_receivable_adjustment"), array(''))
					->from(array("b" => "tbl_receivable_adjustment_invoice"), array('b.*'))
					->where("a.IdReceivableAdjustment=?",$receivableId);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetreceivableadjustmentnoninvoidedetl($receivableId){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_receivable_adjustment"), array(''))
					->from(array("b" => "tbl_receivable_adjustment_noninvoice"), array('b.*'))
					->where("a.IdReceivableAdjustment=?",$receivableId);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}


	public function fngetReceivablePaymentInfoById($larrResult){
		$select = $this->lobjDbAdpt->select()
				->from(array("a" => "tbl_receipt_paymentinfo"), array("a.*"))
				//->joinLeft(array("c" => "tbl_bankcode"),'c.AUTOINC = a.BankCode',array("c.BANKACCT_CODE as BankAccount"))
				//->joinLeft(array("d" => "tbl_receivablesetup_terminalid"),'a.TerminalId = d.IdTerminal',array("d.TerminalID"))
				//->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = a.PaymentGroup',array('e.DefinitionDesc'))
				->where("a.IdReceipt=?",$larrResult[0]['IdReceipt'])
				->where("a.Status=?",'New');
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

}