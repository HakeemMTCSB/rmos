<?php
class Studentfinance_Model_DbTable_Invoicefeestructure extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_invoice_feestructure';
	private $lobjInvoicebatchModel;
	private $lobjcodegenObj;
	private $lobjSubjectsetup;
	private $lobjadddropModel;
	private $gobjsessionsis;
	private $lobjFeesetupModel;
	private $lobjDefCodeModel;

	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->lobjInvoicebatchModel = new Studentfinance_Model_DbTable_Invoicebybatch();
		$this->lobjcodegenObj = new Cms_CodeGeneration();
		$this->lobjSubjectsetup = new Application_Model_DbTable_Subjectsetup();
		$this->lobjadddropModel = new Registration_Model_DbTable_Adddropsubject();
		$this->lobjFeesetupModel = new Studentfinance_Model_DbTable_Fee();
		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		$this->gobjsessionsis = new Zend_Session_Namespace('sis');
		$this->lobjFeeCategory = new Studentfinance_Model_DbTable_Fee();
		
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	public function fngetprogram($lstrscheme){
		
		if ( $this->locale != 'ar_YE' )
		{
			$select = $this->lobjDbAdpt->select()
				->from(array("sa" => "tbl_program")
				, array("key" => "sa.IdProgram", "name" => "CONCAT_WS('-',IFNULL(sa.ProgramName,''),IFNULL(sa.ProgramCode,''))"));
		}
		else
		{
			$select = $this->lobjDbAdpt->select()
				->from(array("sa" => "tbl_program")
				, array("key" => "sa.IdProgram", "name" => "CONCAT_WS('-',IFNULL(sa.ArabicName,''),IFNULL(sa.ProgramCode,''))"));
		}
		
		$select->where("sa.IdScheme = ?",$lstrscheme)
		->where("sa.Active = 1");
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetprogramedit($lstrscheme){

		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_program"), array("a.IdProgram","a.ProgramName","a.ProgramCode"))
					->where("a.IdScheme = ?",$lstrscheme);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function getinvoiceid(){

		$select = $this->lobjDbAdpt->select()
				->from(array("a" => "tbl_invoice_feestructure"), array("a.InvoiceId","a.IdInvoiceFeeStructure"));
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetDebitACCode(){

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_ledgercode"), array("a.*"))
		->where("a.STATUS = ?",1)
		->where("a.MISCCODE = ?",'SD');
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetCreditACCode(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_ledgercode"), array("a.*"))
		->where("a.STATUS = ?",1)
		->where("a.ACCOUNTYPE = ?",'R')
		->orWhere("a.ACCOUNTYPE = ?",'C');
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fnCheckInvoiceId($lstrcheck){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
					->where("a.InvoiceId = ?",$lstrcheck);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetCostctrl(){
		
		if ( $this->locale == 'ar_YE')
		{
			$select1 = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_collegemaster"), array("a.IdCollege","a.ArabicName as CollegeName"))
						->where("a.Active = ?",1)
						->where("a.AffiliatedTo = ?",1);
		}
		else
		{
			$select1 = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_collegemaster"), array("a.IdCollege","a.CollegeName"))
						->where("a.Active = ?",1)
						->where("a.AffiliatedTo = ?",1);
		}
		
		$result1 = $this->lobjDbAdpt->fetchAll($select1);

		$select2 = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_schoolmaster"), array("a.SchoolName","a.idSchool"))
						->where("a.Active = ?",1);
		$result2 = $this->lobjDbAdpt->fetchAll($select2);

		foreach($result1 as $arr1){
			$tem['key'] = $arr1['IdCollege'].'_faculty';
			$tem['value'] = $arr1['CollegeName'];
			$List1[] = $tem;
		}

		foreach($result2 as $arr2){
			$tem['key'] = $arr2['idSchool'].'_school';
			$tem['value'] = $arr2['SchoolName'];
			$List2[] = $tem;
		}

		$finalresult = array_merge($List1,$List2);
		return $finalresult;
	}


	public function fngetCourses($idprogram){

		if ( $this->locale == 'ar_YE' )
		{
			$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_landscapesubject"), array("a.IdProgram","a.IdSubject"))
						->join(array('b'=>'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.SubCode','b.subjectMainDefaultLanguage as SubjectName','b.IdSubject as subjectid'))
						->join(array('c'=>'tbl_landscape'),'a.IdLandscape=c.IdLandscape')
						->where("c.Active=?",123)
						->where("a.IdProgram = ?",$idprogram)
						->where("b.SubjectName !='' ")
						->group("a.IdSubject");
		}
		else
		{
			$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_landscapesubject"), array("a.IdProgram","a.IdSubject"))
						->join(array('b'=>'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.SubCode','b.SubjectName','b.IdSubject as subjectid'))
						->join(array('c'=>'tbl_landscape'),'a.IdLandscape=c.IdLandscape')
						->where("c.Active=?",123)
						->where("a.IdProgram = ?",$idprogram)
						->where("b.SubjectName !='' ")
						->group("a.IdSubject");
		}

		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetInvoiceFullDetail($id){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
					->joinLeft(array('b'=>'tbl_currency'),'b.AUTOINC = a.IdCurrency',array('b.CURRDESC as currencydesc'))
					->joinLeft(array('c'=>'tbl_intake'),'c.IdIntake = a.IdIntakeFrom',array('c.IntakeDesc'))
					->joinLeft(array('d'=>'tbl_branchofficevenue'),'d.IdBranch = a.IdBranch',array('d.BranchName'))
					->joinLeft(array('e'=>'tbl_scheme'),'e.IdScheme = a.IdScheme',array('e.EnglishDescription'))

					->where("a.IdInvoiceFeeStructure = ?",$id);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetInvoiceDetailsById($id){
		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
						->where("a.IdInvoiceFeeStructure = ?",$id);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetProgramInvoiceById($id)
	{
		if ( $this->locale == 'ar_YE' )
		{
			$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure_programinfo"), array("a.*"))
						->joinLeft(array('b'=>'tbl_program'),'b.IdProgram = a.ProgramId',array('b.ArabicName as prgmname','b.ProgramCode as prgmcode'))
						->where("a.IdInvoiceFeeStructure = ?",$id);
		}
		else
		{
			$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure_programinfo"), array("a.*"))
						->joinLeft(array('b'=>'tbl_program'),'b.IdProgram = a.ProgramId',array('b.ProgramName as prgmname','b.ProgramCode as prgmcode'))
						->where("a.IdInvoiceFeeStructure = ?",$id);
		}

		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetInvoiceById($id){
		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure_invoicedetail"), array("a.*"))
						->joinLeft(array('b'=>'tbl_fee_setup'),'b.IdFeeSetup = a.FeeCode',array('b.FeeCode as feecode','b.Description as feecodedesc'))
						->joinLeft(array('c'=>'tbl_currency'),'c.AUTOINC = a.Currency',array('c.CURRDESC as currencydesc'))
						->joinLeft(array('d'=>'tbl_ledgercode'),'d.AUTOINC = a.DebitACCode',array('d.DESCRIPTION as debitdesc','d.ACCOUNTCODE as debitacc'))
						->joinLeft(array('e'=>'tbl_ledgercode'),'e.AUTOINC = a.CreditACCode',array('e.DESCRIPTION as creditdesc','e.ACCOUNTCODE as creditacc'))
						->joinLeft(array('f'=>'tbl_schoolmaster'),'f.idSchool = a.CostCtrl and a.CostCtrlType = "school" ',array('f.SchoolName as name1'))
						->joinLeft(array('g'=>'tbl_collegemaster'),'g.IdCollege = a.CostCtrl and a.CostCtrlType = "faculty"',array('g.CollegeName as name'))
						->joinLeft(array('h'=>'tbl_ledgercode'),'h.AUTOINC = a.ReversalACCode',array('h.DESCRIPTION as reversaldesc','h.ACCOUNTCODE as reversalacc'))
						->where("a.IdInvoiceFeeStructure = ?",$id);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}


	public function fnSearchInvoiceFeeStructure($post = array()) 
	{

		if ( $this->locale == 'ar_YE' )
		{
			$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
						->joinLeft(array('b'=>'tbl_scheme'),'b.IdScheme = a.IdScheme',array('b.ArabicDescription as SchemeName'))
						->joinLeft(array('c'=>'tbl_intake'),'c.IdIntake = a.IdIntakeFrom',array('c.IntakeDesc'))
						->joinLeft(array('d'=>'tbl_branchofficevenue'),'d.IdBranch = a.IdBranch',array('d.Arabic as BranchName'))
						->joinLeft(array('g'=>'tbl_definationms'),'g.idDefinition = a.StudentCategory',array('g.BahasaIndonesia as StudentCategoryName'))
						->joinLeft(array('p'=>'tbl_invoice_feestructure_programinfo'),'p.IdInvoiceFeeStructure=a.IdInvoiceFeeStructure',"GROUP_CONCAT( p.ProgramId ) as Programs");
		}
		else 
		{
			$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure"), array("a.*","GROUP_CONCAT( p.ProgramId ) as Programs"))
						->joinLeft(array('b'=>'tbl_scheme'),'b.IdScheme = a.IdScheme',array('b.EnglishDescription as SchemeName'))
						->joinLeft(array('c'=>'tbl_intake'),'c.IdIntake = a.IdIntakeFrom',array('c.IntakeDesc'))
						->joinLeft(array('d'=>'tbl_branchofficevenue'),'d.IdBranch = a.IdBranch',array('d.BranchName'))
						->joinLeft(array('g'=>'tbl_definationms'),'g.idDefinition = a.StudentCategory',array('g.DefinitionDesc as StudentCategoryName'))
						->joinLeft(array('p'=>'tbl_invoice_feestructure_programinfo'),'p.IdInvoiceFeeStructure=a.IdInvoiceFeeStructure',array());
		}
		
		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where("a.InvoiceId LIKE  '%".$post['field2']."%'  ");
		}

		if(isset($post['field5']) && !empty($post['field5']) ){
			$select = $select->where("a.IdScheme = ?",$post['field5']);
		}
		if(isset($post['field8']) && !empty($post['field8']) ){
			$select = $select->where("a.IdIntakeFrom = ?",$post['field8']);

		}

		if(isset($post['field10']) && !empty($post['field10']) ){
			$select = $select->where("a.IdBranch = ?",$post['field10']);
		}

		if(isset($post['field11']) && !empty($post['field11']) ){
			$select = $select->where("a.StudentCategory = ?",$post['field11']);
		}

		if(isset($post['field15']) && !empty($post['field15']) ){
			$select = $select->where("a.StartDate = ?",$post['field15']);
		}
		
		$select->group('a.IdInvoiceFeeStructure');
		
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}
	
	public function getPrograms($ids)
	{
		
		if ( empty($ids) ) return null;
		
 		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_program"), array("a.*"))
					->where("a.IdProgram IN (".implode(',',$ids).")");
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}
	
	public function fnaddInvoiceFeeStructure($larrformData,$random){

		$randomID = $larrformData['Random'];
		$paramArray = array(
				'InvoiceId' => $larrformData['InvoiceId'],
				'IdIntakeFrom' => $larrformData['IntakeFrom'],
				'IdIntakeTo' => $larrformData['IntakeTo'],
				'StartDate' => $larrformData['StartDate'],
				'EndDate' => $larrformData['EndDate'],
				'StudentCategory' => $larrformData['StudentCategory'],
				'IdScheme' => $larrformData['Scheme'],
				'IdBranch' => $larrformData['Branch'],
				'IdCurrency' => $larrformData['Currency'],
				'Description' => $larrformData['Description'],
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate'],
				'IdUniversity'=>$larrformData['IdUniversity'],
		);

		$this->lobjDbAdpt->insert('tbl_invoice_feestructure',$paramArray);
		$lastinsertID = $this->lobjDbAdpt->lastInsertId();

		// For Invoice Entry Detail Section
		if(isset($larrformData['FeeCodeGrid']) && $larrformData['FeeCodeGrid']!=''){
			$check = count($larrformData['FeeCodeGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArrayNew = array(
						'IdInvoiceFeeStructure'=>$lastinsertID,
						'FeeCode'=> $larrformData['FeeCodeGrid'][$i],
						//'ItemDesc'=> $larrformData['ItemDescGrid'][$i],
						'CostCtrl'=> $larrformData['CostCtrlGrid'][$i],
						'CostCtrlType'=>$larrformData['CostCtrlTypeGrid'][$i],
						'CalculationMode'=>$larrformData['CalculationModeGrid'][$i],
						'Currency'=>$larrformData['CurrencyDetailGrid'][$i],
						'Rate'=>$larrformData['RateGrid'][$i],
						'DebitACCode'=>$larrformData['DebitACCodeGrid'][$i],
						'CreditACCode'=>$larrformData['CreditACCodeGrid'][$i],
						'ReversalACCode'=>$larrformData['ReversalACCodeGrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_invoicedetail',$paramArrayNew);
			}
		}

		if(isset($larrformData['ProgramGrid']) && $larrformData['ProgramGrid']!=''){
			$check = count($larrformData['ProgramGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArrayNew1 = array(
						'IdInvoiceFeeStructure'=>$lastinsertID,
						'ProgramId'=> $larrformData['ProgramGrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programinfo',$paramArrayNew1);
			}
		}

		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_programcourse_temporary"), array("a.*"))
					->where("a.RandomId = ?",$randomID);
		$result = $this->lobjDbAdpt->fetchAll($select);
		if(count($result)>0){
			foreach($result as $values){
				$param = array(
						'IdInvoiceFeeStructure'=>$lastinsertID,
						'IdProgram'=> $values['IdProgram'],
						'IdSubject'=> $values['IdSubject'],
						'Amount'=> $values['Amount'],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programcourse',$param);
			}
			$this->lobjDbAdpt->delete('tbl_invoice_feestructure_programcourse_temporary','RandomId='.$randomID);
		}

	}

	public function fnaddInvoiceDetailTempProgramInfo($larrformData,$larrformData2,$IdProgram,$Random){

		$this->fndeleteTempSavedCourses($IdProgram,$Random);
		if(isset($larrformData) ){
			$check = count($larrformData);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'IdProgram' => $IdProgram,
						'IdSubject' => $larrformData[$i],
						'RandomId' => $Random,
						'Amount' => $larrformData2[$i],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programcourse_temporary',$paramArray);
			}
		}
	}


	public function fnaddInvoiceDetailPermProgramInfo($larrformData,$larrformData2,$IdProgram,$invoiceID){

		$this->fndeletePermSavedCourses($IdProgram,$invoiceID);
		if(isset($larrformData) ){
			$check = count($larrformData);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'IdProgram' => $IdProgram,
						'IdSubject' => $larrformData[$i],
						'IdInvoiceFeeStructure' => $invoiceID,
						'Amount' => $larrformData2[$i],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programcourse',$paramArray);
			}
		}
	}



	public function fnUpdateInvoiceFeeStructure($larrformData,$id){
		$paramArray = array(
				'IdIntakeFrom' => $larrformData['IntakeFrom'],
				'IdIntakeTo' => $larrformData['IntakeTo'],
				'StartDate' => $larrformData['StartDate'],
				'EndDate' => $larrformData['EndDate'],
				'StudentCategory' => $larrformData['StudentCategory'],
				'IdScheme' => $larrformData['Scheme'],
				'IdBranch' => $larrformData['Branch'],
				'IdCurrency' => $larrformData['Currency'],
				'Description' => $larrformData['Description'],
		);

		$where_up = "  IdInvoiceFeeStructure ='".$id."' ";
		$this->lobjDbAdpt->update('tbl_invoice_feestructure',$paramArray, $where_up);

		// For Invoice Entry Detail Section
		$this->lobjDbAdpt->delete('tbl_invoice_feestructure_invoicedetail','IdInvoiceFeeStructure='.$id);
		if(isset($larrformData['FeeCodeGrid']) && $larrformData['FeeCodeGrid']!=''){
			$check = count($larrformData['FeeCodeGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArrayNew = array(
						'IdInvoiceFeeStructure'=>$id,
						'FeeCode'=> $larrformData['FeeCodeGrid'][$i],
						'ItemDesc'=> $larrformData['ItemDescGrid'][$i],
						'CostCtrl'=> $larrformData['CostCtrlGrid'][$i],
						'CostCtrlType'=>$larrformData['CostCtrlTypeGrid'][$i],
						'CalculationMode'=>$larrformData['CalculationModeGrid'][$i],
						'Currency'=>$larrformData['CurrencyDetailGrid'][$i],
						'Rate'=>$larrformData['RateGrid'][$i],
						'DebitACCode'=>$larrformData['DebitACCodeGrid'][$i],
						'CreditACCode'=>$larrformData['CreditACCodeGrid'][$i],
						'ReversalACCode'=>$larrformData['ReversalACCodeGrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_invoicedetail',$paramArrayNew);
			}
		}

		$this->lobjDbAdpt->delete('tbl_invoice_feestructure_programinfo','IdInvoiceFeeStructure='.$id);
		if(isset($larrformData['ProgramGrid']) && $larrformData['ProgramGrid']!=''){
			$check = count($larrformData['ProgramGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArrayNew1 = array(
						'IdInvoiceFeeStructure'=>$id,
						'ProgramId'=> $larrformData['ProgramGrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programinfo',$paramArrayNew1);
			}
		}
	}


	public function fnaddCopyInvoiceFeeStructure($larrformData,$IdUniversity,$userId){

		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
					->where("a.IdInvoiceFeeStructure = ?",$larrformData['InvoiceInvoiceId']);
		$result = $this->lobjDbAdpt->fetchRow($select);

		$paramArray = array(
				'InvoiceId' => $larrformData['InvoiceId'],
				'IdIntakeFrom' => $larrformData['InvoiceIntakeFrom'],
				'IdIntakeTo' => $larrformData['InvoiceIntakeTo'],
				'StartDate' => $larrformData['InvoiceStartDate'],
				'EndDate' => $larrformData['InvoiceEndDate'],
				'StudentCategory' => $larrformData['InvocieStudentCategory'],
				'IdScheme' => $larrformData['InvoiceScheme'],
				'IdBranch' => $larrformData['InvoiceBranch'],
				'IdCurrency' => $result['IdCurrency'],
				'Description' => $result['Description'],
				'UpdUser'=> $userId,
				'UpdDate'=> date('Y-m-d'),
				'IdUniversity'=> $IdUniversity,
		);

		$this->lobjDbAdpt->insert('tbl_invoice_feestructure',$paramArray);
		$lastinsertID = $this->lobjDbAdpt->lastInsertId();

		$select1 = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_programinfo"), array("a.*"))
					->where("a.IdInvoiceFeeStructure = ?",$larrformData['InvoiceInvoiceId']);
		$result1 = $this->lobjDbAdpt->fetchAll($select1);
		if($result1){
			foreach($result1 as $values){
				$paramArrayNew1 = array(
						'IdInvoiceFeeStructure'=>$lastinsertID,
						'ProgramId'=> $values['ProgramId'],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programinfo',$paramArrayNew1);
			}
		}


		$select2 = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_invoicedetail"), array("a.*"))
					->where("a.IdInvoiceFeeStructure = ?",$larrformData['InvoiceInvoiceId']);
		$result2 = $this->lobjDbAdpt->fetchAll($select2);
		if($result2){
			foreach($result2 as $values){
				$paramArrayNew = array(
						'IdInvoiceFeeStructure'=>$lastinsertID,
						'FeeCode'=> $values['FeeCode'],
						'CostCtrl'=> $values['CostCtrl'],
						'CostCtrlType'=>$values['CostCtrlType'],
						'CalculationMode'=>$values['CalculationMode'],
						'Currency'=>$values['Currency'],
						'Rate'=>$values['Rate'],
						'DebitACCode'=>$values['DebitACCode'],
						'CreditACCode'=>$values['CreditACCode'],
						'ReversalACCode'=>$values['ReversalACCode'],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_invoicedetail',$paramArrayNew);
			}
		}

		$select3 = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoice_feestructure_programcourse"), array("a.*"))
						->where("a.IdInvoiceFeeStructure = ?",$larrformData['InvoiceInvoiceId']);
		$result3 = $this->lobjDbAdpt->fetchAll($select3);
		if($result3){
			foreach($result3 as $values){
				$paramArrayNew1 = array(
						'IdInvoiceFeeStructure'=>$lastinsertID,
						'IdProgram'=> $values['IdProgram'],
						'IdSubject'=> $values['IdSubject'],
						'Amount'=> $values['Amount'],
				);
				$this->lobjDbAdpt->insert('tbl_invoice_feestructure_programcourse',$paramArrayNew1);
			}
		}
	}



	public function fngetTempCoursesSaved($lstrprogram,$Random){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_programcourse_temporary"), array("a.*"))
					->joinLeft(array('b'=>'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.SubjectName','b.IdSubject'))
					->where("a.IdProgram = ?",$lstrprogram)
					->where("a.RandomId = ?",$Random);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}


	public function fngetPermCoursesSaved($lstrprogram,$invoiceID){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_programcourse"), array("a.*"))
					->joinLeft(array('b'=>'tbl_subjectmaster'),'b.IdSubject = a.IdSubject',array('b.SubCode','b.SubjectName','b.IdSubject'))
					->where("a.IdProgram = ?",$lstrprogram)
					->where("a.IdInvoiceFeeStructure = ?",$invoiceID);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fndeleteTempSavedCourses($IdProgram,$Random){

		$where_del = " IdProgram='".$IdProgram."' AND RandomId='".$Random."' ";
		$this->lobjDbAdpt->delete('tbl_invoice_feestructure_programcourse_temporary',$where_del);

	}

	public function fndeletePermSavedCoursesProgram($IdProgram,$invoiceID){

		$where_del = " IdProgram='".$IdProgram."' AND IdInvoiceFeeStructure='".$invoiceID."' ";
		$where_del2 = " ProgramId='".$IdProgram."' AND IdInvoiceFeeStructure='".$invoiceID."' ";
		$this->lobjDbAdpt->delete('tbl_invoice_feestructure_programcourse',$where_del);
		$this->lobjDbAdpt->delete('tbl_invoice_feestructure_programinfo',$where_del2);

	}


	public function fndeletePermSavedCourses($IdProgram,$invoiceID){
		$where_del = " IdProgram='".$IdProgram."' AND IdInvoiceFeeStructure='".$invoiceID."' ";
		$this->lobjDbAdpt->delete('tbl_invoice_feestructure_programcourse',$where_del);
	}

	public function fnsearchFeestructure($larrwhere,$batch, $feecode=''){

		$currentDate =  date('Y-m-d');
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
					->joinLeft(array('b'=>'tbl_invoice_feestructure_invoicedetail'),'b.IdInvoiceFeeStructure = a.IdInvoiceFeeStructure',array('b.*'))
					->joinLeft(array('c'=>'tbl_fee_setup'),'c.IdFeeSetup = b.FeeCode',array('c.FeeCode as FeeCodeName','c.FeeCategory'));

		foreach($larrwhere as $key=>$val){
				if($val ==''){
					$select->where("a.".$key." = ?",'');
				}else{
					$select->where("a.".$key." = ?",$val);
				}
		}

		if ( $feecode != '' )
		{
			$select->where('b.FeeCode = ?',$feecode);
		}


		if($batch['ChargingPeriod']!='0' ) {
			$dt_wh1 = "c.ChargingPeriod = '".$batch['ChargingPeriod']."' ";
			$select->where($dt_wh1);
		 }

	
//		if(isset($post['field57']) && !empty($post['field57']) ){
//                       $select->where("DATE_FORMAT(DATE(a.DT_CREATE), '%Y-%m-%d') = ?",$post['field57']);
//               }

		//$dt_wh = "StartDate <= '".$currentDate."' AND EndDate>='".$currentDate."'";
		//$select->where($dt_wh);
		
		//$this->select = $select;
	 
		$result = $this->lobjDbAdpt->fetchAll($select);
		
	
		
		$returnArray = array();
		foreach($result as $data)
		{
			if(($currentDate >= $data['StartDate']) && ($data['EndDate'] == '0000-00-00' || $data['EndDate'] == ''))
			{
				$returnArray[] = $data;
			}
			else if(($currentDate >= $data['StartDate']) && ($data['EndDate'] != '0000-00-00') && $currentDate<=$data['EndDate']) 
			{
				$returnArray[] = $data;
			}
		}
		
		return $returnArray;
	}

	// Public function to insert invoice detail and main by batch invoice generation
	public function fnInsertInvoiceOfstudent($feestruc,$student,$batch)
	{
		$studentRegModel = new Registration_Model_DbTable_Studentregistration();
		// get the current sem detail array
	
		$semdet = $studentRegModel->fngetcursem($student['IdStudentRegistration']);
		$lobjsemModel = new GeneralSetup_Model_DbTable_Semester();
		$semcode = $lobjsemModel->getsemcodeinvoice($semdet[0]);
		$student['currentsem'] = $semcode;

		/**
		 * Now there is three case according to the value of feecode and feecategory in the batch:
		 *  1: Feecode = 0 and Feecategory = 0
		 *  2: Feecode = 0 and Feecategory != 0
		 *  3: Feecode != 0 and Feecategory = 0
		 *  4: Feecode != 0 and Feecategory != 0
		 */
		if ($batch['IdFeeCategory'] == 0 && $batch['IdFeeCode'] == 0 )
		{
			// Now generate the invoices according to feesttructres feecode and feecategory
		    $this->fngenerateInvoiceWithfeecodesNew($feestruc,$student,$batch);

		}
		else if($batch['IdFeeCategory'] != 0 && $batch['IdFeeCode'] == 0 ) 
		{
			//now need to get the all feecode of batch feecategory
			$larrbatchfeecodes = $this->lobjFeesetupModel->getFeesetupBycatId($batch['IdFeeCategory']);

			$larrNewFeestructArray = array();
			// Now check the which feecode is exist in the feestructure
			foreach($feestruc as $fest){
				$ret = $this->fnCheckFeeCodeExistInfeeStruct($larrbatchfeecodes,$fest['FeeCode']);
				if($ret){
					$larrNewFeestructArray[] = $fest;
				}
			}

			if(!empty($larrNewFeestructArray)){
				$this->fngenerateInvoiceWithfeecodes($larrNewFeestructArray,$student,$batch);
			}

		}
		else if($batch['IdFeeCategory'] != 0 && $batch['IdFeeCode'] != 0 ) 
		{
			// Now check this code is available in the feestructure
            $larrNewFeestructArray = array();
            foreach($feestruc as $fe){
            	if($fe['FeeCode'] == $batch['IdFeeCode']){
                	$larrNewFeestructArray[] = $fe;
                }
            }
          
			// Now insert the invoice entries according to new calculated feestructure
            $this->fngenerateInvoiceWithfeecodes($larrNewFeestructArray,$student,$batch);
		}
	}

	
	// Generate Invoice for Student Activity
	public function StudentActivityInvoice($feestruc,$student)
	{
		$auth = Zend_Auth::getInstance();
		$studentRegModel = new Registration_Model_DbTable_Studentregistration();
		
		// get the current sem detail array
	
		$semdet = $studentRegModel->fngetcursem($student['IdStudentRegistration']);
		$lobjsemModel = new GeneralSetup_Model_DbTable_Semester();
		$semcode = $lobjsemModel->getsemcodeinvoice($semdet[0]);
		$student['currentsem'] = $semcode;

        $larrNewFeestructArray[] = $feestruc[0];

	
		// Now insert the invoice entries according to new calculated feestructure
		$batch = array(
							'Description'	=>	'Automated Student Activity Trigger',
							'BatchNo'		=>	'',
							'UpdUser'		=>	$auth->getIdentity()->iduser,
							'Currency'		=>	$feestruc[0]['Currency'],
							'IdFeeCategory'	=>	$feestruc[0]['FeeCategory']
					  );

		$this->fngenerateInvoiceWithfeecodes($larrNewFeestructArray,$student,$batch);
	
	}


	// Function to check the feecode is exist in the feecode
    public function fnCheckFeeCodeExistInfeeStruct($feecatcodearray,$feecode){
    	$flag = false;
        foreach($feecatcodearray as $feecat){
        	if($feecat['IdFeeSetup'] == $feecode){
            	$flag = true;
            }
        }
    	return $flag;
    }

	function group_assoc($array, $key) {
	    $return = array();
	    foreach($array as $v) {
	        $return[$v[$key]][] = $v;
	    }
	    return $return;
	}

	// Function to generate invoices with respects of feecode // vipul
	public function fngenerateInvoiceWithfeecodesNew($feestruc,$student,$batch){

		//Group the requests by their FeeCategory
		$FeeCategrequests = $this->group_assoc($feestruc, 'FeeCategory');

		if(count($FeeCategrequests)>0) {

		foreach($FeeCategrequests as $val) {

		$billNum = $this->fngenerateBillNum();
        $LintInvoicemainId = '';
        $larrinvoicemain = array('BILLNUM'=>$billNum,'CMPYCODE'=>$this->gobjsessionsis->idUniversity,'STUDID'=>$student['registrationId'],
								'DOCDATE'=>date ( 'Y-m-d'),'BILLDESC'=>$batch['Description'],'TOTAL_PAID'=>0,'STATUS'=>0,
                                'ID_CREATE'=>$batch['UpdUser'],'ID_CANCEL'=>0,'DT_CANCEL'=>'',
                                'BCHBILLNUM'=>$batch['BatchNo'],'DT_CREATE'=>date ( 'Y-m-d'),'SOURCE'=>'AUTO',
                                'SemesterID'=>$student['currentsem'],'IdStudentRegistration'=>$student['IdStudentRegistration'],
                                'IdFeeCategory'=>$val[0]['FeeCategory'],'CURRENCY'=>$batch['Currency']
                              );

       $table = 'tbl_invoicemain';
       // Now insert category in invoice main table
       $this->lobjDbAdpt->insert($table,$larrinvoicemain);
       $LintInvoicemainId = $this->lobjDbAdpt->lastInsertId();
       $i = 0;
       	
		foreach($val as $fee)
		{
			$i++;
			// Now need to check feecode entry in invoice is eligible or not
			$ret = $this->fncheckfeeeligibility($fee['FeeCode'],$student,$batch);
			
			if($ret){
				switch ($fee['CalculationMode']){
					case 1:
						// when calculation mode is Amount
						$this->fnInsertInvoiceAmount($fee,$student,$batch,$LintInvoicemainId,$i);
						break;

					case 2:
						$this->fnInsertInvoiceCrHours($fee,$student,$batch,$LintInvoicemainId,$i);
						// when calculation mode is CR hours
						break;

					case 3:
						// when calculation mode is Course
						$this->fnInsertInvoiceCorses($fee,$student,$batch,$LintInvoicemainId,$i);
						break;
				}
			}
		}
		}}
	}


	// Function to generate invoices with respects of feecode // vipul
	public function fngenerateInvoiceWithfeecodes($feestruc,$student,$batch)
	{
		if(count($feestruc)>0) 
		{
			$billNum = $this->fngenerateBillNum();
			$LintInvoicemainId = '';
			$larrinvoicemain = array (
									
										'BILLNUM' => $billNum,
										'CMPYCODE' => $this->gobjsessionsis->idUniversity,
										'STUDID' => $student['registrationId'],
										'DOCDATE' => date( 'Y-m-d'),
										'BILLDESC' => $batch['Description'],
										'TOTAL_PAID' => 0,
										'STATUS' => 0,
										'ID_CREATE' => $batch['UpdUser'],
										'ID_CANCEL' => 0,
										'DT_CANCEL' => '',
										'BCHBILLNUM' => $batch['BatchNo'],
										'DT_CREATE' => date ( 'Y-m-d'),
										'SOURCE' => 'AUTO',
										'SemesterID' => $student['currentsem'],
										'IdStudentRegistration' => $student['IdStudentRegistration'],
										'IdFeeCategory' => $batch['IdFeeCategory'],
										'CURRENCY' => $batch['Currency']
								  );

		   $table = 'tbl_invoicemain';
		   // Now insert category in invoice main table
		   $this->lobjDbAdpt->insert($table,$larrinvoicemain);
		   $LintInvoicemainId = $this->lobjDbAdpt->lastInsertId();
			
		 
			foreach($feestruc as $fee)
			{
				// Now need to check feecode entry in invoice is eligible or not
				$ret = $this->fncheckfeeeligibility($fee['FeeCode'],$student,$batch);
	
				if($ret)
				{
					switch ($fee['CalculationMode'])
					{
						case 1:
							// when calculation mode is Amount
							$this->fnInsertInvoiceAmount($fee,$student,$batch,$LintInvoicemainId);
							break;

						case 2:
							$this->fnInsertInvoiceCrHours($fee,$student,$batch,$LintInvoicemainId);
							// when calculation mode is CR hours
							break;

						case 3:
							// when calculation mode is Course
							$this->fnInsertInvoiceCorses($fee,$student,$batch,$LintInvoicemainId);
							break;
					}
				}
			}
			
		}
	}

	// Function to check the eligibity of feecode for the perticular student according to
	// charging period of fee code
	public function fncheckfeeeligibility($feecode,$student,$batch){
		
		
		$flag = false;
		// First get the feecode detail from the feesetup
		if($feecode!='') {
		$larrfeecodestruc = $this->lobjFeesetupModel->getFeesetupDetById($feecode);
	
		if($larrfeecodestruc['ChargingPeriod'] == ''){
			$flag = false;
		}
		else if($larrfeecodestruc['ChargingPeriod'] != '' )
		{
			$chargeperiod = $this->lobjDefCodeModel->fngetDefinationcode('Charging Period',$larrfeecodestruc['ChargingPeriod']);
			switch ($chargeperiod){
				case 'One Time':
					$ret = $this->getfeecodedetBystudent($student['IdStudentRegistration'],$feecode);
					if(empty($ret)){
						$flag = true;
					}
					break;

				case 'Per Semester':
				
					$ret = $this->getfeecodedetBystudent($student['IdStudentRegistration'],$feecode,$student['currentsem']);
					if(empty($ret)){
						$flag = true;
					}
					break;

				case 'Per Year':
					$ret = $this->getfeecodedetBystudent($student['IdStudentRegistration'],$feecode);
					$fl = false;
					if(count($ret)>0) {
						foreach($ret as $yr){
							$yearofdate = date('Y', strtotime($yr['DT_CREATE']));
							if(date('Y') == $yearofdate){
								$fl = true;
							}
						}
					}
					if($fl){
						$flag = false;
					} else {
						$flag = true;
					}
					break;

				case 'Per Event':
					$flag = true;
					break;
			}
		}
		return $flag;
		} else {
			return $flag;
		}

	}




	public function getfeecodedetBystudent($idstudent,$feecode,$sem=''){

		$wh_st = "b.STATUS ='0' OR b.STATUS ='1' ";
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_invoicedetl"), array("a.BILLNUM"))
		->joinLeft(array('b'=>'tbl_invoicemain'),'a.BILLNUM = b.AUTOINC',array('b.DT_CREATE','b.AUTOINC'))
		->where("b.IdStudentRegistration = ?",$idstudent)
		->where("a.FEECODE = ?",$feecode)
		->where($wh_st);
		if($sem!=''){
			$select->where("b.SemesterID = ?",$sem);
		}
		return $result = $this->lobjDbAdpt->fetchall($select);
	}


	// Insert the invoices the calculation mode is Amount
    public function fnInsertInvoiceAmount($fee,$student,$batch,$LintInvoicemainId,$count=0){

	   // Get Refundable Status by FeeCode
	   $refundableStatus  = $this->lobjFeeCategory->getRefundableByFeeCode($fee['FeeCode']);
       // Now insert feecode detail in invoice detail table
       $larrinvoicedetail = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],'BILLSEQ'=>$count,
                                    'FEECODE'=>$fee['FeeCode'],'IsRefundable'=>$refundableStatus,'FEEDESC'=>$batch['Description'],
                                    'CMPYCODEDB'=>$fee['IdUniversity'],'ACCTCODEDB'=>$fee['DebitACCode'],
                                    'CMPYCODECH'=>$fee['IdUniversity'],'ACCTCODECH'=>$fee['CreditACCode'],
                                    'COSTCTR'=>$fee['CostCtrl'].'_'.$fee['CostCtrlType'],'TOTAL_AMOUNT'=>$fee['Rate'],
                                    'TOTAL_BLNC'=>$fee['Rate'],'TOTAL_PAID'=>0,'IdBranch'=>$fee['IdBranch']
                                );

       $table = 'tbl_invoicedetl';
       $this->lobjDbAdpt->insert($table,$larrinvoicedetail);
       $statusdetbatch['ProcessStatus'] = 1;
       $tablebatchinvoice = 'tbl_bill_batch_invoice';
       $wherebatch['IdBillBatch'] = $batch['IdBillBatch'];
       $wherebatch['IdStudent'] = $student['IdStudentRegistration'];
       $this->lobjInvoicebatchModel->updateProcessstatus($tablebatchinvoice,$wherebatch,$statusdetbatch);

       if($fee['FeeCodeName']=='Tution Fee' || $fee['FeeCodeName']=='Exam Fee') {
	       $larrstudentcourses = $this->lobjadddropModel->getstudentlandscapecourses($student['IdStudentRegistration'],$student['IdLandscape']);
	       $i = 1;
	       foreach($larrstudentcourses as $course){
	       		$tableinvoicecourse = 'tbl_invoicecourse';
	        	$larrinvoicecourse = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],
	                                      'BILLSEQ'=>$i,'COURSE_CODE'=>$course['SubCode'],
	                                      'COURSE_DESC'=>$course['courseDescription'],'COURSE_COSTCTR'=>'',
	                                      'COURSE_AMOUNT'=>$fee['Rate'],'COURSE_CRHOUR'=>$course['CreditHours']
	                				);
	            $this->lobjDbAdpt->insert($tableinvoicecourse,$larrinvoicecourse);
	            $i++;
	       }
       }

       // Now update total amount invoice main table
       $dataformain['TOTAL_AMOUNT'] = $fee['Rate'];
       $dataformain['TOTAL_BLNC'] = $fee['Rate'];
       //$dataformain['BRANCH_ID'] = $fee['IdBranch'];
      // $dataformain['CURRENCY'] = $fee['IdCurrency'];
       $table = 'tbl_invoicemain';
       $wheremain['AUTOINC'] = $LintInvoicemainId;
       $this->lobjInvoicebatchModel->updateProcessstatus($table,$wheremain,$dataformain);
       $tabledtl = 'tbl_invoicedetl';
       $wheredtl['BILLNUM'] = $LintInvoicemainId;
       $this->lobjInvoicebatchModel->updateProcessstatus($tabledtl,$wheredtl,$dataformain);
   	}

	// Insert Invoices when calculation mode is Course
	public function fnInsertInvoiceCorses($fee,$student,$batch,$LintInvoicemainId, $count=0){

//		$billNum = $this->fngenerateBillNum();
//		$LintInvoicemainId = '';
//		$larrinvoicemain = array('BILLNUM'=>$billNum,'CMPYCODE'=>$this->gobjsessionsis->idUniversity,
//								'BRANCH_ID'=>$fee['IdBranch'],
//								'CURRENCY'=>$fee['IdCurrency'],'STUDID'=>$fee['IdCurrency'],
//								'STUDID'=>$student['registrationId'],'DOCDATE'=>$student['registrationId'],
//								'BILLDESC'=>$batch['Description'],'TOTAL_PAID'=>0,'STATUS'=>0,
//								'ID_CREATE'=>$batch['UpdUser'],'ID_CANCEL'=>$batch['UpdUser'],'DT_CANCEL'=>'',
//								'BCHBILLNUM'=>$batch['BatchNo'],'DT_CREATE'=>date ( 'Y-m-d'),'SOURCE'=>'AUTO',
//								'SemesterID'=>$student['currentsem'],'IdStudentRegistration'=>$student['IdStudentRegistration'],
//								'IdFeeCategory'=>$batch['IdFeeCategory'],'TOTAL_AMOUNT'=>$fee['Rate'],
//								'TOTAL_BLNC'=>$fee['Rate'],
//		);
//
//		$table = 'tbl_invoicemain';
//		// Now insert category in invoice main table
//		$this->lobjDbAdpt->insert($table,$larrinvoicemain);
//		$LintInvoicemainId = $this->lobjDbAdpt->lastInsertId();

		// Get Refundable Status by FeeCode
	    $refundableStatus  = $this->lobjFeeCategory->getRefundableByFeeCode($fee['FeeCode']);
		// Now insert feecode detail in invoice detail table
		$larrinvoicedetail = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],'BILLSEQ'=>$count,
								'FEECODE'=>$fee['FeeCode'],'IsRefundable'=>$refundableStatus,'FEEDESC'=>$batch['Description'],
								'CMPYCODEDB'=>$fee['IdUniversity'],'ACCTCODEDB'=>$fee['DebitACCode'],
								'CMPYCODECH'=>$fee['IdUniversity'],'ACCTCODECH'=>$fee['CreditACCode'],
								'COSTCTR'=>$fee['CostCtrl'].'_'.$fee['CostCtrlType'],'TOTAL_AMOUNT'=>$fee['Rate'],
								'TOTAL_BLNC'=>$fee['Rate'],'TOTAL_PAID'=>0,'IdBranch'=>$fee['IdBranch']
		);

		$table = 'tbl_invoicedetl';
		$this->lobjDbAdpt->insert($table,$larrinvoicedetail);
		// Now update the status batch_invoice(Anubhaw)
		$statusdet['ProcessStatus'] = 1;
		$table = 'tbl_bill_batch_invoice';
		$where['IdBillBatch'] = $batch['IdBillBatch'];
		$where['IdStudent'] = $student['IdStudentRegistration'];
		$this->lobjInvoicebatchModel->updateProcessstatus($table,$where,$statusdet);
		// Now insert the courses value in imvoice course table

		$ret = $this->checkstudentprogramininFeeStruct($student['IdProgram'],$fee['IdInvoiceFeeStructure']);
		if($ret){
			// it is the case when student's program exist in the fee structure
			// so get the program courses by fee structure
			$larrprogramcourse = $this->getProgramcoursesInFeeStruct($student['IdProgram'],$fee['IdInvoiceFeeStructure']);
			if(!empty($larrprogramcourse)){
				$larrstudentcourses = $this->lobjadddropModel->getstudentlandscapecourses($student['IdStudentRegistration'],$student['IdLandscape']);
				// create new program courses array which exist in students landscape
				$larrnewprogramcourse = array();
				foreach($larrprogramcourse as $courses){
					foreach($larrstudentcourses as $stcourse){
						if($stcourse['IdSubject'] == $courses['IdSubject']){
							$temp['IdSubject'] = $stcourse['IdSubject'];
							$temp['SubCode'] = $stcourse['SubCode'];
							$temp['courseDescription'] = $stcourse['courseDescription'];
							$temp['CreditHours'] = $stcourse['Subhrs'];
							$temp['Amount'] = $courses['Amount'];
							$larrnewprogramcourse[] =  $temp;
						}
					}
				}
				$totalamount = 0;
				// calculate total amount for invoice main and inserting course table entries
				$i = 1;
				foreach($larrnewprogramcourse as $courses){
					$courseamount = 0;
					$table = 'tbl_invoicecourse';
					if($courses['Amount']!=''){
						$courseamount = $courses['Amount'];
						$totalamount += $courseamount;
						//$totalamount += $courses['CreditHours'] * $courses['Amount'];
					}else{
						$courseamount = $fee['Rate'];
						$totalamount += $courseamount;
						//$totalamount += $courses['CreditHours'] * $fee['Rate'];
					}
					$larrinvoicecourse = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],
											'BILLSEQ'=>$i,'COURSE_CODE'=>$courses['SubCode'],
											'COURSE_DESC'=>$courses['courseDescription'],'COURSE_COSTCTR'=>'',
											'COURSE_AMOUNT'=>$courseamount,'COURSE_CRHOUR'=>$courses['Subhrs']
					);
					$this->lobjDbAdpt->insert($table,$larrinvoicecourse);
					$i++;

				}
				// Now update total amount invoice main table
				$dataformain['TOTAL_AMOUNT'] = $totalamount;
				$dataformain['TOTAL_BLNC'] = $totalamount;
				$table = 'tbl_invoicemain';
				$wheremain['AUTOINC'] = $LintInvoicemainId;
				$this->lobjInvoicebatchModel->updateProcessstatus($table,$wheremain,$dataformain);
				$tabledtl = 'tbl_invoicedetl';
				$wheredtl['BILLNUM'] = $LintInvoicemainId;
				$this->lobjInvoicebatchModel->updateProcessstatus($tabledtl,$wheredtl,$dataformain);
			}else{
				// Now insert the courses value in imvoice course table
				$larrinvoicecourse = array();
				// First get student courses
				$larrstudentcourses = $this->lobjadddropModel->getstudentlandscapecourses($student['IdStudentRegistration'],$student['IdLandscape']);
				// Now insert all these courses in course table regarding the this batch
				$i = 1;
				$courseamount = 0;
				foreach($larrstudentcourses as $courses){
					$courseamount = $fee['Rate'];
					$larrinvoicecourse = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],
									'BILLSEQ'=>$i,'COURSE_CODE'=>$courses['SubCode'],
									'COURSE_DESC'=>$courses['courseDescription'],'COURSE_COSTCTR'=>'',
									'COURSE_AMOUNT'=>$courseamount,'COURSE_CRHOUR'=>$courses['Subhrs']
					);
					$totalamount += $fee['Rate'];
					$i++;
					$table = 'tbl_invoicecourse';
					$this->lobjDbAdpt->insert($table,$larrinvoicecourse);
					// Now update total amount invoice main table
					$dataformain['TOTAL_AMOUNT'] = $totalamount;
					$dataformain['TOTAL_BLNC'] = $totalamount;
					$table = 'tbl_invoicemain';
					$wheremain['AUTOINC'] = $LintInvoicemainId;
					$this->lobjInvoicebatchModel->updateProcessstatus($table,$wheremain,$dataformain);
					$tabledtl = 'tbl_invoicedetl';
					$wheredtl['BILLNUM'] = $LintInvoicemainId;
					$this->lobjInvoicebatchModel->updateProcessstatus($tabledtl,$wheredtl,$dataformain);
				}
			}
		}
	}

	// Insert Invoices when calculation mode is Cr Course
	public function fnInsertInvoiceCrHours($fee,$student,$batch,$LintInvoicemainId, $count=0){
		
//		$billNum = $this->fngenerateBillNum();
//		$LintInvoicemainId = '';
//		$larrinvoicemain = array('BILLNUM'=>$billNum,'CMPYCODE'=>$this->gobjsessionsis->idUniversity,
//								'BRANCH_ID'=>$fee['IdBranch'],
//								'CURRENCY'=>$fee['IdCurrency'],'STUDID'=>$fee['IdCurrency'],
//								'STUDID'=>$student['registrationId'],'DOCDATE'=>$student['registrationId'],
//								'BILLDESC'=>$batch['Description'],'TOTAL_PAID'=>0,'STATUS'=>0,
//								'ID_CREATE'=>$batch['UpdUser'],'ID_CANCEL'=>$batch['UpdUser'],'DT_CANCEL'=>'',
//								'BCHBILLNUM'=>$batch['BatchNo'],'DT_CREATE'=>date ( 'Y-m-d'),'SOURCE'=>'AUTO',
//								'SemesterID'=>$student['currentsem'],'IdStudentRegistration'=>$student['IdStudentRegistration'],
//								'IdFeeCategory'=>$batch['IdFeeCategory'],'TOTAL_AMOUNT'=>$fee['Rate'],
//								'TOTAL_BLNC'=>$fee['Rate'],
//		);
//
//		$table = 'tbl_invoicemain';
//		// Now insert category in invoice main table
//		$this->lobjDbAdpt->insert($table,$larrinvoicemain);
//		$LintInvoicemainId = $this->lobjDbAdpt->lastInsertId();

		// Get Refundable Status by FeeCode
	    $refundableStatus  = $this->lobjFeeCategory->getRefundableByFeeCode($fee['FeeCode']);
		// Now insert feecode detail in invoice detail table
		$larrinvoicedetail = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],'BILLSEQ'=>$count,
								'FEECODE'=>$fee['FeeCode'],'IsRefundable'=>$refundableStatus,'FEEDESC'=>$batch['Description'],
								'CMPYCODEDB'=>$fee['IdUniversity'],'ACCTCODEDB'=>$fee['DebitACCode'],
								'CMPYCODECH'=>$fee['IdUniversity'],'ACCTCODECH'=>$fee['CreditACCode'],
								'COSTCTR'=>$fee['CostCtrl'].'_'.$fee['CostCtrlType'],'TOTAL_AMOUNT'=>$fee['Rate'],
								'TOTAL_BLNC'=>$fee['Rate'],'TOTAL_PAID'=>0,'IdBranch'=>$fee['IdBranch']
		);

		$table = 'tbl_invoicedetl';
		$this->lobjDbAdpt->insert($table,$larrinvoicedetail);
		// Now update the status batch_invoice(Anubhaw)
		$statusdet['ProcessStatus'] = 1;
		$table = 'tbl_bill_batch_invoice';
		$where['IdBillBatch'] = $batch['IdBillBatch'];
		$where['IdStudent'] = $student['IdStudentRegistration'];
		$this->lobjInvoicebatchModel->updateProcessstatus($table,$where,$statusdet);
		// Now insert the courses value in invoice course table

		$ret = $this->checkstudentprogramininFeeStruct($student['IdProgram'],$fee['IdInvoiceFeeStructure']);
		
		if($ret){
			// it is the case when student's program exist in the fee structure
			// so get the program courses by fee structure
			$larrprogramcourse = $this->getProgramcoursesInFeeStruct($student['IdProgram'],$fee['IdInvoiceFeeStructure']);
			
			if(!empty($larrprogramcourse)){
				$larrstudentcourses = $this->lobjadddropModel->getstudentlandscapecourses($student['IdStudentRegistration'],$student['IdLandscape']);
				
				// create new program courses array which exist in students landscape
				$larrnewprogramcourse = array();
				foreach($larrprogramcourse as $courses){
					foreach($larrstudentcourses as $stcourse){
						if($stcourse['IdSubject'] == $courses['IdSubject']){
							$temp['IdSubject'] = $stcourse['IdSubject'];
							$temp['SubCode'] = $stcourse['SubCode'];
							$temp['courseDescription'] = $stcourse['courseDescription'];
							$temp['CreditHours'] = $stcourse['Subhrs'];
							$temp['Amount'] = $courses['Amount'];
							$larrnewprogramcourse[] =  $temp;
						}
						else
						{
							$temp['IdSubject'] = $stcourse['IdSubject'];
							$temp['SubCode'] = $stcourse['SubCode'];
							$temp['courseDescription'] = $stcourse['courseDescription'];
							$temp['CreditHours'] = $stcourse['Subhrs'];
							$temp['Amount'] = $fee['Rate'];
							$larrnewprogramcourse[] =  $temp;
						}
					}
				}
				
				$totalamount = 0;
				// calculate total amount for invoice main and inserting course table entries
				$i = 1;
				
				foreach($larrnewprogramcourse as $courses)
				{
					$courseamount = 0;
					$table = 'tbl_invoicecourse';
					if($courses['Amount']!=''){
						$courseamount = $courses['Amount'];
						$totalamount += $courses['CreditHours'] * $courses['Amount'];
						$subhrs = $courses['CreditHours'];
					}else{
						$courseamount = $fee['Rate'];
						$totalamount += $courses['Subhrs'] * $fee['Rate'];
						$subhrs = $courses['Subhrs'];
					}
					
					$larrinvoicecourse = array(
												'BILLNUM'=>$LintInvoicemainId,
												'BRANCH_ID'=>$fee['IdBranch'],
												'BILLSEQ'=>$i,
												'COURSE_CODE'=>$courses['SubCode'],
												'COURSE_DESC'=>$courses['courseDescription'],
												'COURSE_COSTCTR'=>'',
												'COURSE_AMOUNT'=>$courseamount,
												'COURSE_CRHOUR'=>$subhrs
											);
											
					$this->lobjDbAdpt->insert($table,$larrinvoicecourse);
					$i++;

				}
				// Now update total amount invoice main table
				$dataformain['TOTAL_AMOUNT'] = $totalamount;
				$dataformain['TOTAL_BLNC'] = $totalamount;
				$table = 'tbl_invoicemain';
				$wheremain['AUTOINC'] = $LintInvoicemainId;
				$this->lobjInvoicebatchModel->updateProcessstatus($table,$wheremain,$dataformain);
				$tabledtl = 'tbl_invoicedetl';
				$wheredtl['BILLNUM'] = $LintInvoicemainId;
				$this->lobjInvoicebatchModel->updateProcessstatus($tabledtl,$wheredtl,$dataformain);
			}else{
				// Now insert the courses value in imvoice course table
				$larrinvoicecourse = array();
				// First get student courses
				$larrstudentcourses = $this->lobjadddropModel->getstudentlandscapecourses($student['IdStudentRegistration'],$student['IdLandscape']);
				
				// Now insert all these courses in course table regarding the this batch
				$i = 1;
				$courseamount = $totalamount = 0;
				foreach($larrstudentcourses as $courses){
					$courseamount = $fee['Rate'];
					$larrinvoicecourse = array('BILLNUM'=>$LintInvoicemainId,'BRANCH_ID'=>$fee['IdBranch'],
									'BILLSEQ'=>$i,'COURSE_CODE'=>$courses['SubCode'],
									'COURSE_DESC'=>$courses['courseDescription'],'COURSE_COSTCTR'=>'',
									'COURSE_AMOUNT'=>$courseamount,'COURSE_CRHOUR'=>$courses['Subhrs']
					);
					$totalamount += $courses['Subhrs'] * $fee['Rate'];
					$i++;
					$table = 'tbl_invoicecourse';
					$this->lobjDbAdpt->insert($table,$larrinvoicecourse);
					// Now update total amount invoice main table
					$dataformain['TOTAL_AMOUNT'] = $totalamount;
					$dataformain['TOTAL_BLNC'] = $totalamount;
					$table = 'tbl_invoicemain';
					$wheremain['AUTOINC'] = $LintInvoicemainId;
					$this->lobjInvoicebatchModel->updateProcessstatus($table,$wheremain,$dataformain);
					$tabledtl = 'tbl_invoicedetl';
					$wheredtl['BILLNUM'] = $LintInvoicemainId;
					$this->lobjInvoicebatchModel->updateProcessstatus($tabledtl,$wheredtl,$dataformain);
				}
			}
		}
	}

	// Get the program's courses from the fee structure
	public function getProgramcoursesInFeeStruct($idprog,$idfee){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_programcourse"), array("a.IdSubject",'a.Amount'))
					->where("a.IdProgram = ?",$idprog)
					->where('a.IdInvoiceFeeStructure =?',$idfee);
		return $larrResult = $this->lobjDbAdpt->fetchAll($select);
	}


	// Get the list of program available in the fee structure
	public function checkstudentprogramininFeeStruct($idprog,$idfee){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoice_feestructure_programinfo"), array("a.IdProgram"))
					->where("a.ProgramId= ?",$idprog)
					->where('a.IdInvoiceFeeStructure =?',$idfee);
		$larrResult = $this->lobjDbAdpt->fetchAll($select);
		if(count($larrResult)>0){
			return true;
		}else{
			return false;
		}
	}


	// function generate bill num
	public function fngenerateBillNum(){
		$this->gobjsessionsis = new Zend_Session_Namespace('sis');
		$IdUniversity = $this->gobjsessionsis->idUniversity;
		// now generate billnum
		$larrconfig = $this->lobjSubjectsetup->getConfigDetail($IdUniversity);
		//Invoice Number
		//$lstrinvoicenumber =  $larrconfig[0]['InvoiceFeeStructureCodeType'];
//		$larrformatkey = array('format'=>'InvoiceFeeStructureIdFormat',
//							'prefix'=>'InvoiceFeeStructurePrefix');
//		$table = 'tbl_invoice_feestructure';
//		$coloumn = 'InvoiceId';

		$larrformatkey = array('format'=>'StudentInvoiceIdFormat',
				'prefix'=>'StudentInvoicePrefix');
		$table = 'tbl_invoicemain';
		$coloumn = 'BILLNUM';
		$lstrcoderesult = $this->lobjcodegenObj->studentfinanceIdGenration($IdUniversity, $larrconfig[0],$table,$coloumn,$larrformatkey);
		return $lstrcoderesult;
	}

	private function flatten_array($mArray) {
		$sArray = array();

		foreach ($mArray as $row) {
			if (!(is_array($row))) {
				if ($sArray[] = $row) {

				}
			} else {
				$sArray = array_merge($sArray, self::flatten_array($row));
			}
		}
		return $sArray;
	}

	public function fnCheckInvoiceUnique($larrformData){

		 if(isset($larrformData['IntakeTo']) && $larrformData['IntakeTo']!='') {
			$intakeTo =  $larrformData['IntakeTo'];
		 } else {
			$intakeTo =  '0';
		 }
		$select = $this->lobjDbAdpt->select()
							->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
							->where("a.IdIntakeFrom = ?",$larrformData['IntakeFrom'])
							->where("a.IdIntakeTo = ?",$intakeTo)
							->where("a.StudentCategory = ?",$larrformData['StudentCategory'])
							->where("a.IdBranch = ?",$larrformData['Branch'])
							->where("a.IdScheme = ?",$larrformData['Scheme']);

		 /*if(isset($larrformData['EndDate'])&& isset($larrformData['StartDate'])) {
			$whr_Condition1 = '"'.date('Y-m-d',strtotime($larrformData['StartDate'])).'" BETWEEN a.StartDate AND a.EndDate';
			$whr_Condition2 = '"'.date('Y-m-d',strtotime($larrformData['EndDate'])).'" BETWEEN a.StartDate AND a.EndDate';
			$whr_Condition3 = 'a.StartDate BETWEEN "'.date('Y-m-d',strtotime($larrformData['StartDate'])).'" AND "'.date('Y-m-d',strtotime($larrformData['EndDate'])).'"';
			$whr_Condition4 = 'a.EndDate BETWEEN "'.date('Y-m-d',strtotime($larrformData['StartDate'])).'" AND "'.date('Y-m-d',strtotime($larrformData['EndDate'])).'"';
			$select = $select->where($whr_Condition1)
							 ->orWhere($whr_Condition2)
							 ->orWhere($whr_Condition3)
							 ->orWhere($whr_Condition4);
			}*/
		$result = $this->lobjDbAdpt->fetchAll($select);
		$flag = 0;
		foreach($result as $data){			
			if($data['EndDate'] == '0000-00-00'){				
				$flag = 1;
			}
		}
		if($flag!=1){
			foreach($result as $data){				
				if(date('Y-m-d',strtotime($data['StartDate'])) <= date('Y-m-d',strtotime($larrformData['StartDate']))){
					if(isset($larrformData['StartDate']) && $larrformData['StartDate']!=''){
						if(date('Y-m-d',strtotime($larrformData['EndDate'])) <= date('Y-m-d',strtotime($data['EndDate']))){
							$flag = 2;							
						}
					}					
				}
			}
		}
		return $flag;
	}

	public function fnCheckInvoiceUniquecopy($larrformData){

		 if(isset($larrformData['InvoiceIntakeTo']) && $larrformData['InvoiceIntakeTo']!='') {
			$intakeTo =  $larrformData['InvoiceIntakeTo'];
		 } else {
			$intakeTo =  '0';
		 }

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_invoice_feestructure"), array("a.*"))
		->where("a.IdIntakeFrom = ?",$larrformData['InvoiceIntakeFrom'])
		->where("a.IdIntakeTo = ?",$intakeTo)
		->where("a.StudentCategory = ?",$larrformData['InvocieStudentCategory'])
		->where("a.IdBranch = ?",$larrformData['InvoiceBranch'])
		->where("a.IdScheme = ?",$larrformData['InvoiceScheme']);

		/*if(isset($larrformData['InvoiceStartDate'])&& isset($larrformData['InvoiceEndDate'])) {
			$whr_Condition = '"'.date('Y-m-d',strtotime($larrformData['InvoiceStartDate'])).'" BETWEEN a.StartDate AND a.EndDate ) OR ("'.date('Y-m-d',strtotime($larrformData['InvoiceEndDate'])).'" BETWEEN a.StartDate AND a.EndDate';
			$select = $select->where($whr_Condition);
		}

		if(isset($larrformData['InvoiceEndDate'])&& isset($larrformData['InvoiceStartDate'])) {
			$whr_Condition1 = '"'.date('Y-m-d',strtotime($larrformData['InvoiceStartDate'])).'" BETWEEN a.StartDate AND a.EndDate';
			$whr_Condition2 = '"'.date('Y-m-d',strtotime($larrformData['InvoiceEndDate'])).'" BETWEEN a.StartDate AND a.EndDate';
			$whr_Condition3 = 'a.StartDate BETWEEN "'.date('Y-m-d',strtotime($larrformData['InvoiceStartDate'])).'" AND "'.date('Y-m-d',strtotime($larrformData['InvoiceEndDate'])).'"';
			$whr_Condition4 = 'a.EndDate BETWEEN "'.date('Y-m-d',strtotime($larrformData['InvoiceStartDate'])).'" AND "'.date('Y-m-d',strtotime($larrformData['InvoiceEndDate'])).'"';
			$select = $select->where($whr_Condition1)
			->orWhere($whr_Condition2)
			->orWhere($whr_Condition3)
			->orWhere($whr_Condition4);
		}
*/
		$result = $this->lobjDbAdpt->fetchAll($select);
		$flag = 0;
		foreach($result as $data){			
			if($data['EndDate'] == '0000-00-00'){				
				$flag = 1;
			}
		}
		if($flag!=1){
			foreach($result as $data){				
				if(date('Y-m-d',strtotime($data['StartDate'])) <= date('Y-m-d',strtotime($larrformData['StartDate']))){
					if(isset($larrformData['StartDate']) && $larrformData['StartDate']!=''){
						if(date('Y-m-d',strtotime($larrformData['EndDate'])) <= date('Y-m-d',strtotime($data['EndDate']))){
							$flag = 2;							
						}
					}					
				}
			}
		}
		return $flag;
	}


}