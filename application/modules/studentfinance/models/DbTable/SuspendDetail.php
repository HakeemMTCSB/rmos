<?php
class Studentfinance_Model_DbTable_SuspendDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'suspend_detail';
	protected $_primary = "id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('sd'=>$this->_name))
					->where("sd.id ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}

}
?>