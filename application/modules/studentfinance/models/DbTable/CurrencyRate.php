<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_CurrencyRate extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'tbl_currency_rate';
	protected $_primary = "cr_id";
	
	
	public function getRate($currency_id, $order=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('cr'=>$this->_name))
		->join(array('c'=>'tbl_currency'),'c.cur_id = cr.cr_cur_id')
		->join(array('u'=>'tbl_user'),'u.idUser = cr.cr_update_by', array('update_by_name'=>"concat_ws(' ',fName,mNAme,lName)"))
		->where('cr.cr_cur_id = ?', $currency_id);
	
		if($order){
			$selectData->order($order);
		}
				
		$row = $db->fetchAll($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getCurrentExchangeRate($currency_id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
			->from(array('cr'=>$this->_name))
			->where('cr.cr_cur_id = ?', $currency_id)
			->where('cr.cr_effective_date <= now()')
			->order('cr.cr_effective_date DESC');
	
	
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getList(){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
			->from(array('cr'=>$this->_name));
	
		$row = $db->fetchAll($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getExchangeRateByDate($currency_id,$start,$end){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
			->from(array('cr'=>$this->_name))
			->where('cr.cr_cur_id = ?', $currency_id)
			->where("cr.cr_effective_date >= '$start'")
			->where("cr.cr_effective_date <= '$end'")
			->order('cr.cr_effective_date DESC');
	
	
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getData($id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
			->from(array('cr'=>$this->_name))
			->where('cr.cr_id = ?', $id);
	
		$row = $db->fetchRow($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;
		}

	}


	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['cr_update_by'])){
			$data['cr_update_by'] = $auth->getIdentity()->id;
		}
	
		$data['cr_update_date'] = date('Y-m-d H:i:s');
		
		return parent::insert($data);
	}
	
	public function getRateByDate($currency_id,$date){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateTrans = date('Y-m-d', strtotime($date) );
	
		$selectData = $db->select()
			->from(array('cr'=>$this->_name))
			->where('cr.cr_cur_id = ?', $currency_id)
			->where("cr.cr_effective_date <= '$dateTrans'")
			->order('cr.cr_effective_date desc')
			->limit(1);
	

		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}

	public function getReceiptInvoice($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'receipt_invoice'), array('value'=>'*'))
			->join(array('b'=>'invoice_main'), 'a.rcp_inv_invoice_id = b.id')
			->where('rcp_inv_rcp_id = ?', $id);

		$result = $db->fetchAll($select);
		return $result;
	}
}
?>