<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/8/2016
 * Time: 10:29 AM
 */
class Studentfinance_Model_DbTable_EarlyBird extends Zend_Db_Table_Abstract {

    public function getSemester($idscheme = null, $date = null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.display = ?', 1)
            ->order('a.SemesterMainStartDate DESC');

        if ($idscheme != null && $date != null){
            $select->where('a.IdScheme = ?', $idscheme);
            $select->where('a.SemesterMainStartDate < ?', $date);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemesterById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdSemesterMaster = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgram($idscheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->where('a.IdScheme = ?', $idscheme)
            ->order('a.seq_no');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSponsor(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sponsor'))
            ->order('a.fname');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getScholar(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scholarship_sch'))
            ->order('a.sch_name');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function discountType(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount_type'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function insertEarlyBird($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('early_bird', $data);
        $id = $db->lastInsertId('early_bird', 'eb_id');
        return $id;
    }

    public function insertEarlyBirdSem($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('early_bird_prevsem', $data);
        $id = $db->lastInsertId('early_bird_prevsem', 'ebp_id');
        return $id;
    }

    public function insertEarlyBirdProgram($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('early_bird_program', $data);
        $id = $db->lastInsertId('early_bird_program', 'ebpr_id');
        return $id;
    }

    public function insertEarlyBirdScholar($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('early_bird_scholar', $data);
        $id = $db->lastInsertId('early_bird_scholar', 'ebsc_id');
        return $id;
    }

    public function insertEarlyBirdSponsor($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('early_bird_sponsor', $data);
        $id = $db->lastInsertId('early_bird_sponsor', 'ebsp_id');
        return $id;
    }

    public function getEarlyBird($id = null, $search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.eb_semester = b.IdSemesterMaster', array('b.SemesterMainName', 'b.SemesterMainCode', 'b.IdScheme', 'b.SemesterMainStartDate', 'b.SemesterMainEndDate'))
            ->joinLeft(array('c'=>'discount_type'), 'a.eb_discount_type = c.dt_id', array('c.dt_discount', 'c.dt_description'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.eb_created_by = d.iduser', array('creted_by'=>'d.loginName'))
            ->joinLeft(array('e'=>'tbl_user'), 'a.eb_update_by = e.iduser', array('updated_by'=>'e.loginName'))
            ->order('a.eb_id DESC');

        if ($search != false){
            if (isset($search['semester']) && $search['semester']!=''){
                $select->where('a.eb_semester = ?', $search['semester']);
            }
        }

        if ($id != null){
            $select->where('a.eb_id = ?', $id);
            $result = $db->fetchRow($select);
        }else{
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function getEarlyBirdSem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_prevsem'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.ebp_prev_sem = b.IdSemesterMaster', array('b.SemesterMainName', 'b.SemesterMainCode', 'b.IdScheme', 'b.SemesterMainStartDate', 'b.SemesterMainEndDate'))
            ->where('a.ebp_eb_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEarlyBirdProgram($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_program'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.ebpr_prog_id = b.IdProgram', array('b.ProgramCode', 'b.ProgramName'))
            ->where('a.ebpr_eb_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEarlyBirdScholar($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_scholar'))
            ->joinLeft(array('b'=>'tbl_scholarship_sch'), 'a.ebsc_scholar = b.sch_Id', array('b.sch_code', 'b.sch_name', 'sch_desc'))
            ->where('a.ebsc_eb_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEarlyBirdSponsor($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_sponsor'))
            ->joinLeft(array('b'=>'tbl_sponsor'), 'a.ebsp_sponsor_id = b.idsponsor', array('b.SponsorCode', 'b.fName'))
            ->where('a.ebsp_eb_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateEarlyBird($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('early_bird', $data, 'eb_id = '.$id);
        return $update;
    }

    public function deleteEarlyBird($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('early_bird', 'eb_id = '.$id);
        return $delete;
    }

    public function deleteEarlyBirdSem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('early_bird_prevsem', 'ebp_eb_id = '.$id);
        return $delete;
    }

    public function deleteEarlyBirdProgram($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('early_bird_program', 'ebpr_eb_id = '.$id);
        return $delete;
    }

    public function deleteEarlyBirdScholar($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('early_bird_scholar', 'ebsc_eb_id = '.$id);
        return $delete;
    }

    public function deleteEarlyBirdSponsor($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('early_bird_sponsor', 'ebsp_eb_id = '.$id);
        return $delete;
    }

    public function checkStudent($semid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->join(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->join(array('e'=>'tbl_program_scheme'), 'b.IdProgramScheme = e.IdProgramScheme')
            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'b.profileStatus = j.idDefinition', array('studentstatus'=>'j.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_intake'), 'b.IdIntake = i.IdIntake')
            ->joinLeft(array('k'=>'tbl_branchofficevenue'), 'b.IdBranch = k.IdBranch')
            ->where('a.IdStudentRegistration IS NOT NULL')
            //->where('a.IdStudentRegistration = ?', 4039)
            ->where('a.semester = ?', $semid)
            ->where('b.profileStatus = ?', 92)
            ->where('k.invoice = ?', 1)
            ->group('a.IdStudentRegistration');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkDiscount($invid, $ebip){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount_detail'))
            ->join(array('b'=>'discount'), 'a.dcnt_id = b.dcnt_id')
            ->where('a.dcnt_invoice_id = ?', $invid)
            ->where('b.dcnt_type_id IN (?)', $ebip)
            ->where('a.status = ?', 'A')
            ->where('b.dcnt_status = ?', 'A');

        $result = $db->fetchAll($select);
        return $result;
    }
}