<?php
class Studentfinance_Model_DbTable_InvoiceBatchDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'invoice_batch_detail';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ibd'=>$this->_name));
		
		if($id!=0){
			$selectData->where("ibd.id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
			
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
}

?>