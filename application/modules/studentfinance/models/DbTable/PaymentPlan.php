<?php
class Studentfinance_Model_DbTable_PaymentPlan extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'payment_plan';
	protected $_primary = "pp_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pp'=>$this->_name))
					->where("pp.pp_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getApplicantPaymentPlan($appl_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pp'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pp.pp_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->where("pp.pp_appl_id = ?", (int)$appl_id);

		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			return $row;
		}
	}


	/*
	 * Overite Insert function
	 */
	
	public function insert($data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['pp_creator'])){
			$data['pp_creator'] = $auth->getIdentity()->iduser;
		}
		
		if( !isset($data['pp_create_date']) ){
			$data['pp_create_date'] = date('Y-m-d H:i:s');
		}
			
		return parent::insert( $data );
	}
	

}
?>