<?php
class Studentfinance_Model_DbTable_Invoiceapproval extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_charges';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function fngetStudentApplicationDetails()//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		
			 $lstrSelect = $lobjDbAdpt->select()
					    ->from(array('a'=>'tbl_invoicemaster'))
 						//->(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.idStudent=b.IdApplication',array('b.*'))
 						->where('a.Approved =0')
 						->order('a.IdInvoice');
 						
 						
 		  $result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	public function fnGetInvoiceMasterDetails($lintIdInvoice) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_invoicedetails'),array('a.*'))    
							->join(array('b'=>'tbl_invoicemaster'),'a.IdInvoice  = b.IdInvoice')
							->join(array('c'=>'tbl_accountmaster'),'a.idAccount  = c.idAccount')            			
                			->where("a.IdInvoice = ?",$lintIdInvoice);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
     
	
	
     /*
      * function for getting the prog charges
      */
    public function fnGetChargesDetails($lintIdInvoice) 
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_invoicedetails'),array('a.*'))
                			->join(array('b'=>'tbl_charges'),'a.idAccount = b.IdCharges',array('b.ChargeName'))
                			->where("a.IdInvoice = ?",$lintIdInvoice);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
	

	
	
    /*
     * function for fetching the subject charges
     */
    public function fnGetSubjectChargesDetails($lintIdInvoice)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_invoicesubjectdetails'),array('a.*'))
                			->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject')
                			->where("a.IdInvoice = ?",$lintIdInvoice);
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                			
    }

    /*
     * function to update the invoice master table
     */
    public function updateinvoicemaster($lobjformdata)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	
    	$tableName = "tbl_invoicemaster";
    	for($i=0;$i<count($lobjformdata['IdInvoices']);$i++)
    	{
    		
    		    $idinvoice = $lobjformdata['IdInvoices'][$i];
				$postData = array('Approved' => 1);			
                $where = "IdInvoice=$idinvoice";
				$lobjDbAdpt->update($tableName,$postData,$where);
    	}
    }
    
    
    public function fngetStudentDropdown() { //Function to get the Program Branch details
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(a.FName,''),IFNULL(a.MName,''),IFNULL(a.LName,''))"))
		 				  ->join(array('b'=>'tbl_studentregistration'),'a.IdApplication=b.IdApplication',array())
		 				   ->join(array('c'=>'tbl_invoicemaster'),'c.idStudent=a.IdApplication')
 						   ->where('c.Approved =0')
		               		->group('a.IdApplication');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	public function fnSearchStudentApplication($post)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 
		  $lstrSelect = $lobjDbAdpt->select()
					    ->from(array('a'=>'tbl_invoicemaster'))
 						//->(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.idStudent=b.IdApplication',array('b.*'))
 						->where('a.InvoiceNo like  ? "%"',$post['field3'])
 						->where('a.Approved = ?',$post['field7']);
		  if($post['field5']!='')
          {
          $lstrSelect->where("b.IdApplication =?",$post['field5']);
          }
        $lstrSelect->order('a.IdInvoice'); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 						
	}
	
	
	public function fnGetInvoiceStudentDetails($lintIdInvoice)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		
			 $lstrSelect = $lobjDbAdpt->select()
					    ->from(array('a'=>'tbl_invoicemaster'))
 						->join(array('b'=>'tbl_studentapplication'),'a.idStudent=b.IdApplication',array('b.*'))
 						->where("a.IdInvoice = ?",$lintIdInvoice);
 			$result = $lobjDbAdpt->fetchRow($lstrSelect);
 			return $result;
	}
}