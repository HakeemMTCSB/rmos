<?php
class Studentfinance_Model_DbTable_Accountmaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_accountmaster';
	
	public function fngetAccountDetails() {
        $result = $this->fetchAll('Active = 1', "AccountName ASC");
       return $result;
     }
    
	public function fnviewAccountmaster($lintidAccount) {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("c"=>"tbl_accountmaster"),array("c.*"))			
		            	->where("c.idAccount= ?",$lintidAccount);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
    
    
    
	public function fnGetIdGroupSelect(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("key"=>"a.idAccount","value"=>"a.AccountName"))
								  ->where("a.Active = ?","1")
								  ->where("a.IdGroup = ?","NULL");		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnInsert($insertData) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_accountmaster";
	   	$db->insert($table,$insertData);
	   	return $db->lastInsertId("tbl_accountmaster","idAccount");	   
	} 

	Public function fnupdateaccountmaster($lvarEdit,$larrformData) {  
	    if(!$larrformData['BillingModule']) $larrformData['BillingModule'] = 0;	
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();		
	    
	    if($larrformData['IdGroup'] == '')
	    {
	    	$idgroup = 0;
	    }
	    else 
	    $idgroup = $larrformData['IdGroup'];
	 	$data = array('AccountName' => $larrformData['AccountName'],
	 	 			  'AccShortName' => $larrformData['AccShortName'],
					  'AccountType' => $larrformData['AccountType'],
					  'Description' => $larrformData['Description'],
					  'Period' => $larrformData['Period'],
					  'Revenue' => $larrformData['Revenue'],
	 				  'IdGroup' =>$idgroup,
	 				  'BillingModule'=>$larrformData['BillingModule'],
	 				  'duringRegistration'=>$larrformData['duringRegistration'],	
	 	              'AdvancePayment'=>$larrformData['AdvancePayment'],
				 	  'UpdDate' => $larrformData['UpdDate'],
				 	  'UpdUser' => $larrformData['UpdUser'],
				 	  'Active' => $larrformData['Active'],
	 	              'AccountCode'=>$larrformData['AccountCode'],
				      'Deposit'=>$larrformData['Deposit'],
				      'Refund'=>$larrformData['Refund'],
	 				  'duringProcessing'=>$larrformData['duringProcessing']				 	 
				 	  );
		$where['idAccount = ? ']= $lvarEdit;		
		return $db->update('tbl_accountmaster', $data, $where);
	}     
	public function fngetaccountdeatilsSearch($post = array()){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		if(!$post['field7']) $sqlquery ="  Active = 0 ";
		else $sqlquery	= " Active = 1 ";	
		
		$select = $this->select()
					-> setIntegrityCheck(false)
					-> join(array('a' => 'tbl_accountmaster'),array('idAccount'))
					-> where('a.AccountName like  ? "%"',$post['field2'])
					-> where('a.PrefixCode  like  ? "%"',$post['field3'])
					-> where('a.AccShortName like  ? "%"',$post['field4'])
					-> where($sqlquery);
		$result = $this->fetchAll($select);
		return $result->toArray();
		
	}
	function fnGetBillingModuleSelect(){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_definationtypems')
				 ->where('defTypeDesc = ?','BillingModule');				 		 
		$result = $this->fetchRow($select);
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = ?",$result['idDefType'])
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	function fnGetAccountcodeSelect()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("a.AccountCode"))
								  ->where('a.AccountCode is not Null');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
function fnGetPeriodSelect(){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_definationtypems')
				 ->where('defTypeDesc = ?','Charging Type');				 		 
		$result = $this->fetchRow($select);
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = ?",$result['idDefType'])
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}	
	
	/*
	 * function for generating the code for the account master
	 */
function fnGenerateCode($universityId,$uniqId,$ShortName){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $this->select()
			    ->  setIntegrityCheck(false)  
				->  from('tbl_config')
				->  where('idUniversity = ?',$universityId);  				 
		$result = 	$this->fetchRow($select);		
		$sepr	=	$result['AccountSeparator'];
		$str	=	"AccountCodeField";
		for($i=1;$i<=4;$i++){
			$check = $result[$str.$i];
			switch ($check){
				case 'Text':
				  $code	= $result['AccountCodeText'.$i];
				  break;				  
				case 'ShortName':
				  $code	= $ShortName;
				  break;
				case 'Year':
					$code	= date('Y');
				  break;
				case 'Uniqueid': 
					$code	= $uniqId;
				    break;
				default:
				   break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}		
	 	$data = array('PrefixCode' => $accCode);
		$where['idAccount = ? ']= $uniqId;		
		return $db->update('tbl_accountmaster', $data, $where);			
	}		
}