<?php
class Studentfinance_Model_DbTable_PaymentPlanInvoice extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'payment_plan_invoice';
	protected $_primary = "ppi_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ppi'=>$this->_name))
					->where("ppi.ppi_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getPaymentPlanInvoice($payment_plan_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ppi'=>$this->_name))
					->where("ppi.ppi_plan_id = ?", (int)$payment_plan_id);

		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			return $row;
		}
	}


}
?>