<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 18/8/2016
 * Time: 3:47 PM
 */
class Studentfinance_Model_DbTable_EarlyBirdCalculation extends Zend_Db_Table_Abstract {

    static function getDiscount($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount_detail'), array('discount'=>'sum(a.dcnt_amount)'))
            ->join(array('b'=>'discount'), 'a.dcnt_id = b.dcnt_id', array('currency'=>'b.dcnt_currency_id', 'b.*'))
            ->where('a.dcnt_invoice_id = ?', $id)
            ->where('b.dcnt_approve_date <= ?', $date)
            ->where('a.status = ?', 'A')
            ->where('b.dcnt_status = ?', 'A');

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['discount'] = 0.00;
        }

        return $result;
    }

    static function getDiscountDtl($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount_detail'), array('discount'=>'sum(a.dcnt_amount)'))
            ->join(array('b'=>'discount'), 'a.dcnt_id = b.dcnt_id', array('currency'=>'b.dcnt_currency_id', 'b.*'))
            ->where('a.dcnt_invoice_det_id = ?', $id)
            ->where('b.dcnt_approve_date <= ?', $date)
            ->where('a.status = ?', 'A')
            ->where('b.dcnt_status = ?', 'A');

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['discount'] = 0.00;
        }

        return $result;
    }

    static function getSelfPaid($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'receipt_invoice'), array('selfpaid'=>'a.rcp_inv_amount'))
            ->join(array('b'=>'receipt'), 'a.rcp_inv_rcp_id = b.rcp_id')
            ->where('a.rcp_inv_invoice_id = ?', $id)
            ->where('b.rcp_status = ?', 'APPROVE')
            ->where('b.rcp_receive_date <= ?', $date);

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['selfpaid'] = 0.00;
        }*/

        return $result;
    }

    static function getSelfPaidDtl($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'receipt_invoice'), array('selfpaid'=>'a.rcp_inv_amount'))
            ->join(array('b'=>'receipt'), 'a.rcp_inv_rcp_id = b.rcp_id')
            ->where('a.rcp_inv_invoice_dtl_id = ?', $id)
            ->where('b.rcp_status = ?', 'APPROVE')
            ->where('b.rcp_receive_date <= ?', $date);

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['selfpaid'] = 0.00;
        }*/

        return $result;
    }

    static function getAdvance($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment_detail'), array('a.advpydet_upddate', 'a.advpydet_total_paid'))
            ->join(array('b'=>'advance_payment'), 'a.advpydet_advpy_id=b.advpy_id')
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.advpydet_upddate <= ?', $date)
            ->where('a.advpydet_inv_id = ?', $id);
        ///echo $select;
        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['advancepaid'] = 0.00;
        }*/

        return $result;
    }

    static function getAdvanceDtl($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment_detail'), array('a.advpydet_total_paid', 'a.advpydet_upddate'))
            ->join(array('b'=>'advance_payment'), 'a.advpydet_advpy_id=b.advpy_id')
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.advpydet_upddate <= ?', $date)
            ->where('a.advpydet_inv_det_id = ?', $id);

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['advancepaid'] = 0.00;
        }*/

        return $result;
    }

    static function getSponsorPaid($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'), array('amount'=>'a.sp_amount'))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
            ->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
            ->where('a.sp_invoice_id = ?', $id)
            ->where('c.rcp_status = ?', 'APPROVE')
            ->where('a.sp_status = ?', 1)
            ->where('c.rcp_receive_date <= ?', $date)
            ->group('a.id');

        $result = $db->fetchAll($select);

        /*if ($result == null){
            $result[0]['sponsorpaid'] = 0.00;
        }*/

        return $result;
    }

    static function getSponsorPaidDtl($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'), array('sponsorpaid'=>'sum(a.sp_amount)'))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
            ->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
            ->where('a.sp_invoice_det_id = ?', $id)
            ->where('c.rcp_status = ?', 'APPROVE')
            ->where('a.sp_status = ?', 1)
            ->where('c.rcp_receive_date <= ?', $date);

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['sponsorpaid'] = 0.00;
        }

        return $result;
    }

    static function getCurrency($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_currency'), array('*'))
            ->where('a.cur_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    static function getCreditNote($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'credit_note_detail'), array('cnpaid'=>'sum(a.amount)'))
            ->join(array('b'=>'credit_note'), 'a.cn_id = b.cn_id', array('currency'=>'b.cn_cur_id', 'b.*'))
            ->where('a.invoice_main_id = ?', $id)
            ->where('b.cn_create_date <= ?', $date)
            ->where('b.cn_status = ?',  'A');

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['cnpaid'] = 0.00;
        }

        return $result;
    }

    static function getCreditNoteDtl($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'credit_note_detail'), array('cnpaid'=>'sum(a.amount)'))
            ->join(array('b'=>'credit_note'), 'a.cn_id = b.cn_id', array('currency'=>'b.cn_cur_id', 'b.*'))
            ->where('a.invoice_detail_id = ?', $id)
            ->where('b.cn_status = ?',  'A');

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['cnpaid'] = 0.00;
        }

        return $result;
    }

    static function getAdvanceTransfer($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment'), array('advanceamount'=>'sum(a.advpy_amount)', 'currency'=>'advpy_cur_id' , '*'))
            ->where('a.advpy_status = ?', 'A')
            ->where('a.advpy_invoice_id = ?', $id);

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['advanceamount'] = 0.00;
        }

        return $result;
    }

    public function getFeeItemMain($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'))
            ->where('a.invoice_main_id = ?', $id)
            ->where('a.fi_id IN (75, 74, 65)');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getFeeItemDtl($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'))
            ->where('a.id = ?', $id)
            ->where('a.fi_id IN (75, 74, 65)');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getBalanceMain($id, $date){
        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->where('a.id = ?', $id);

        $invoice = $db->fetchRow($select);

        $selfPaid = self::getSelfPaid($id, $date);

        $advPaid = self::getAdvance($id, $date);

        $disMain = self::getDiscount($id, $date);

        $sponsorPaid = self::getSponsorPaid($id, $date);

        /*var_dump($selfPaid);
        var_dump($advPaid);
        var_dump($disMain);
        var_dump($sponsorPaid);*/

        $cnPaid = self::getCreditNote($id, $date);

        //$advamount = self::getAdvanceTransfer($id, $date);

        //$check = $this->getFeeItemMain($id, $date);

        $selfPaidAmount = 0.00;
        $advPaidAmount = 0.00;
        $sponsorPaidAmount = 0.00;
        $advTransferAmount = 0.00;
        if ($invoice['currency_id']!=1){
            //self paid
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($selfPaid){
                    foreach ($selfPaid as $selfPaidLoop) {
                        if ($selfPaid[0]['rcp_cur_id'] != 1) {
                            $selfPaidAmount = $selfPaidAmount + $selfPaidLoop['selfpaid'];
                        } else {
                            $exrate = $curRateDB->getRateByDate($invoice['currency_id'], $invoice['invoice_date']);
                            $selfPaidAmount = $selfPaidAmount + round($selfPaidLoop['selfpaid'] / $exrate['cr_exchange_rate'], 2);
                        }
                    }
                }else{
                    $selfPaidAmount = 0.00;
                }
            }else{
                $selfPaidAmount = $invoice['bill_paid'];
            }

            //advance payment
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($advPaid) {
                    foreach ($advPaid as $advPaidLoop) {
                        if ($advPaidLoop['advpy_cur_id'] != 1) {
                            $advPaidAmount = $advPaidAmount + $advPaidLoop['advpydet_total_paid'];
                        } else {
                            $exrate = $curRateDB->getRateByDate($invoice['currency_id'], $advPaidLoop['advpydet_upddate']);
                            $advPaidAmount = $advPaidAmount + round($advPaidLoop['advpydet_total_paid'] / $exrate['cr_exchange_rate'], 2);
                        }
                    }
                } else {
                    $advPaidAmount = 0.00;
                }
            }else{
                $advPaidAmount = 0.00;
            }

            //discount payment
            if ($disMain[0]['discount'] != null){
                if ($disMain[0]['currency']!=1){
                    $disPaidAmount = $disMain[0]['discount'];
                }else{
                    $exrate = $curRateDB->getRateByDate($invoice['currency_id'], $disMain[0]['dcnt_create_date']);
                    $disPaidAmount = round($disMain[0]['discount']/$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $disPaidAmount = 0.00;
            }

            //sponsor payment
            if ($sponsorPaid){
                foreach ($sponsorPaid as $sponsorPaidLoop){
                    if ($sponsorPaidLoop['rcp_inv_cur_id']!=1){
                        $sponsorPaidAmount = $sponsorPaidAmount + $sponsorPaidLoop['amount'];
                    }else{
                        $exrate = $curRateDB->getRateByDate($invoice['currency_id'], $sponsorPaidLoop['rcp_receive_date']);
                        $sponsorPaidAmount = $sponsorPaidAmount + round($sponsorPaidLoop['amount']/$exrate['cr_exchange_rate'], 2);
                    }
                }
            }else{
                $sponsorPaidAmount = 0.00;
            }

            //credit note payment
            if ($cnPaid[0]['cnpaid'] != null){
                if ($cnPaid[0]['currency']!=1){
                    $cnPaidAmount = $cnPaid[0]['cnpaid'];
                }else{
                    $exrate = $curRateDB->getRateByDate($invoice['currency_id'], $cnPaid[0]['cn_create_date']);
                    $cnPaidAmount = round($cnPaid[0]['cnpaid']/$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $cnPaidAmount = 0.00;
            }

            //advance payment transfer
            /*if (!$check) {
                if ($advamount[0]['advanceamount'] != null) {
                    if ($advamount[0]['currency'] != 1) {
                        $advTransferAmount = $advamount[0]['advanceamount'];
                    } else {
                        $exrate = $curRateDB->getRateByDate($invoice['currency_id'], $advamount[0]['advpy_date']);
                        $advTransferAmount = ($advamount[0]['advanceamount'] / $exrate['cr_exchange_rate']);
                    }
                } else {
                    $advTransferAmount = 0.00;
                }
            }*/
        }else{
            //self paid
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($selfPaid[0]['selfpaid'] != null){
                    if ($selfPaid[0]['rcp_cur_id']==1){
                        $selfPaidAmount = $selfPaid[0]['selfpaid'];
                    }else{
                        $exrate = $curRateDB->getRateByDate($selfPaid[0]['rcp_cur_id'], $invoice['invoice_date']);
                        $selfPaidAmount = round($selfPaid[0]['selfpaid']*$exrate['cr_exchange_rate'], 2);
                    }
                }else{
                    $selfPaidAmount = 0.00;
                }
            }else{
                $selfPaidAmount = $invoice['bill_paid'];
            }

            //advance payment
            if ($invoice['MigrateCode']==null || ($invoice['not_include']==0 && $invoice['MigrateCode']!=null)) {
                if ($advPaid) {
                    foreach ($advPaid as $advPaidLoop) {
                        if ($advPaidLoop['advpy_cur_id'] == 1) {
                            $advPaidAmount = $advPaidAmount + $advPaidLoop['advpydet_total_paid'];
                        } else {
                            $exrate = $curRateDB->getRateByDate($advPaidLoop['advpy_cur_id'], $advPaidLoop['advpydet_upddate']);
                            $advPaidAmount = $advPaidAmount + round($advPaidLoop['advpydet_total_paid'] * $exrate['cr_exchange_rate'], 2);
                        }
                    }
                } else {
                    $advPaidAmount = 0.00;
                }
            }else{
                $advPaidAmount = 0.00;
            }

            //discount payment
            if ($disMain[0]['discount'] != null){
                if ($disMain[0]['currency']==1){
                    $disPaidAmount = $disMain[0]['discount'];
                }else{
                    $exrate = $curRateDB->getRateByDate($disMain[0]['currency'], $disMain[0]['dcnt_create_date']);
                    $disPaidAmount = round($disMain[0]['discount']*$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $disPaidAmount = 0.00;
            }

            //sponsor payment
            if ($sponsorPaid){
                foreach ($sponsorPaid as $sponsorPaidLoop){
                    if ($sponsorPaidLoop['rcp_inv_cur_id']==1){
                        $sponsorPaidAmount = $sponsorPaidAmount + $sponsorPaidLoop['amount'];
                    }else{
                        $exrate = $curRateDB->getRateByDate($sponsorPaidLoop['rcp_inv_cur_id'], $sponsorPaidLoop['rcp_receive_date']);
                        $sponsorPaidAmount = $sponsorPaidAmount + round($sponsorPaidLoop['amount']*$exrate['cr_exchange_rate'], 2);
                    }
                }
            }else{
                $sponsorPaidAmount = 0.00;
            }

            //credit note payment
            if ($cnPaid[0]['cnpaid'] != null){
                if ($cnPaid[0]['currency']==1){
                    $cnPaidAmount = $cnPaid[0]['cnpaid'];
                }else{
                    $exrate = $curRateDB->getRateByDate($cnPaid[0]['currency'], $cnPaid[0]['cn_create_date']);
                    $cnPaidAmount = round($cnPaid[0]['cnpaid']*$exrate['cr_exchange_rate'], 2);
                }
            }else{
                $cnPaidAmount = 0.00;
            }

            //advance payment transfer
            /*if (!$check) {
                if ($advamount[0]['advanceamount'] != null) {
                    if ($advamount[0]['currency'] == 1) {
                        $advTransferAmount = $advamount[0]['advanceamount'];
                    } else {
                        $exrate = $curRateDB->getRateByDate($advamount[0]['currency'], $advamount[0]['advpy_date']);
                        $advTransferAmount = ($advamount[0]['advanceamount'] * $exrate['cr_exchange_rate']);
                    }
                } else {
                    $advTransferAmount = 0.00;
                }
            }*/
        }

        $balance = $invoice['bill_amount']-$selfPaidAmount-$advPaidAmount-$disPaidAmount-$sponsorPaidAmount-$cnPaidAmount+$advTransferAmount;

        $data = array(
            'bill_number'=>$invoice['bill_number'],
            'bill_balance'=>number_format($balance, 2, '.', ''),
            'bill_paid'=>number_format(($selfPaidAmount+$advPaidAmount+$sponsorPaidAmount), 2, '.', ''),
            'cn_amount'=>number_format($cnPaidAmount, 2, '.', ''),
            'dn_amount'=>number_format($disPaidAmount, 2, '.', ''),
            'sponsor'=>number_format($sponsorPaidAmount, 2, '.', ''),
            'selfpaid'=>number_format($selfPaidAmount+$advPaidAmount, 2, '.', '')
        );

        return $data;
    }
}