<?php
class Studentfinance_Model_DbTable_SponsorInvoiceDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'sponsor_invoice_detail';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>$this->_name),array('*','sp_balance_det'=>'d.sp_balance','sp_paid_det'=>'d.sp_paid'))
					->join(array('bd' => 'sponsor_invoice_main'), 'bd.sp_id = d.sp_id')
					->join(array('b' => 'invoice_main'), 'b.id = d.sp_invoice_id',array('bill_number','bill_amount','bill_balance','dn_amount_main'=>'b.dn_amount','currency_id'))
			->join(array('c' => 'invoice_detail'), 'c.id = d.sp_invoice_det_id and c.invoice_main_id = b.id',array('amount','balance','dn_amount','fee_item_description','invoice_main_id'));
		
		if($id!=0){
			$selectData->where("d.id = ? ",$id);
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getStudentData($id,$idStudent){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'sponsor_invoice_detail'))
					->join(array('bc' => 'sponsor_invoice_main'), 'd.sp_id = bc.sp_id')
					->join(array('b' => 'invoice_main'), 'b.id = d.sp_invoice_id',array('bill_number','bill_amount','bill_balance','dn_amount_main'=>'b.dn_amount'))
			->join(array('c' => 'invoice_detail'), 'c.id = d.sp_invoice_det_id and c.invoice_main_id = b.id',array('amount','balance','dn_amount','fee_item_description'))
					->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id = b.id and ivs.invoice_detail_id = c.id',array('subject_id'))
					->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = ivs.subject_id')
					->join(array('cr'=>'tbl_currency'), 'b.currency_id = cr.cur_id',array('cur_code'))
					->where('bc.sp_batch_id =?',$id)
					->where('bc.sp_IdStudentRegistration =?',$idStudent)
					;
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getDiscountData($searchData=null){
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoice_id = $searchData['invoice_id'];
		$inv_Detail = $searchData['invoice_detail_id'];
		
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('d'=>'discount_detail'))
					->join(array('da'=>'discount'),'da.dcnt_id = d.dcnt_id')
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.dcnt_invoice_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id')
					->where('d.dcnt_invoice_id = ?', $invoice_id)
					->where('d.dcnt_invoice_det_id = ?', $inv_Detail)
					->where("DATE(da.dcnt_create_date) >= ?",$dateFrom)
					->where("DATE(da.dcnt_create_date) <= ?",$dateTo);
		
		$row = $db->fetchAll($selectData);

		return $row;
	}
	
	public function getSponsorInvoiceDetail($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'sponsor_invoice_detail'),array('*','id_sp_id'=>'id'))
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.sp_invoice_id')
					->joinLeft(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id = a.id and ivd.id = d.sp_invoice_det_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id')
					->where('d.sp_id =?',$id)
					;

		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
}

