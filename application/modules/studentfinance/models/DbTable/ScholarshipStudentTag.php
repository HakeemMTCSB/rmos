<?php

class Studentfinance_Model_DbTable_ScholarshipStudentTag extends Zend_Db_Table_Abstract
{

    //put your code here
    protected $_name = 'tbl_scholarship_studenttag';
    protected $_primary = "sa_id";

    protected $_referenceMap    = array(
        'Sponsor' => array(
            'columns'           => 'sa_af_id',
            'refTableClass'     => 'Studentfinance_Model_DbTable_ApplicantFinancial',
            'refColumns'        => 'af_id'
        ),
        'Scholarship' => array(
            'columns'           => 'sa_scholarship_type',
            'refTableClass'     => 'Studentfinance_Model_DbTable_Scholarship',
            'refColumns'        => 'id'
        ),
        'Student' => array(
            'columns'           => 'sa_cust_id',
            'refTableClass'     => 'Registration_Model_DbTable_Studentregistration',
            'refColumns'        => 'IdStudentRegistration'
        ),
        'Semester' => array(
            'columns'           => 'sa_semester_id',
            'refTableClass'     => 'GeneralSetup_Model_DbTable_Semestermaster',
            'refColumns'        => 'IdSemesterMaster'
        ),
        'Transaction' => array(
            'columns'           => 'sa_cust_id',
            'refTableClass'     => 'App_Model_Application_DbTable_ApplicantTransaction',
            'refColumns'        => 'at_trans_id'
        ),
    );

    public function getexisting($sa_cust_id, $sa_scholarship_type)
    {
        $select = $this->select()
            ->where('sa_scholarship_type = ?', $sa_scholarship_type)
            //->where('sa_semester_id = ?', $sa_semester_id)
            ->where('sa_cust_id = ?', $sa_cust_id);
        $cur_rec = $this->fetchRow($select);
        return ($cur_rec);
    }

	public function updateData($data,$id){
		$this->update($data,"sa_id ='".$id."'");
	}
        
        public function deleteData($id){
		$this->delete("sa_id ='".$id."'");
	}
	
	public function getStudentListByType($id,$intake=0,$idStudent=0,$name=null)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'tbl_scholarship_studenttag'),array('a.*','startdate'=>'sa_start_date','enddate'=>"IFNULL(sa_end_date,'0000-00-00')"))
		->join(array('sch' => 'tbl_scholarship_sch'), 'sch.sch_Id = a.sa_scholarship_type',array('*','fname'=>'sch.sch_name'))
		->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.sa_cust_id')
		->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
		->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
		->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
		->where('a.sa_status != 7')
		->where('a.sa_start_date <= ?', date('Y-m-d'))->where('(CASE IFNULL(a.sa_end_date,"0000-00-00") WHEN "0000-00-00" THEN CURDATE() ELSE a.sa_end_date END) >= ?', date('Y-m-d'));
		
		if($id){
			$lstrSelect->where('a.sa_scholarship_type = ?',$id);
		}
		
		if($intake){
			$lstrSelect->where('b.IdIntake = ?',$intake);
		}
		
		if($idStudent){
			$lstrSelect->where('b.registrationId LIKE ?','%'.$idStudent.'%');
		}
		
		if($name){
			$lstrSelect->where("CONCAT_WS(' ', ap.appl_fname,ap.appl_mname,ap.appl_lname ) LIKE ?",'%'.$name.'%');
		}
        //echo $lstrSelect;
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
        
        public function getSponsorStudent($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_studenttag'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.sa_cust_id = b.IdStudentRegistration')
                ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
                ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
                ->where('a.sa_id = ?', $id)
                ->where('a.sa_cust_type = ?', 1);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function updateStudent($bind, $id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $update = $db->update('tbl_scholarship_studenttag', $bind, 'sa_id = '.$id);
            return $update;
        }
}

