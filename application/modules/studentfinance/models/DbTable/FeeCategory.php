<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_FeeCategory extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'tbl_fee_category';
	protected $_primary = "fc_id";

	public function getData() {
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('a'=>$this->_name))
		->joinLeft(array('c'=>'tbl_definationms'), 'c.idDefinition = a.fc_group',array('DefinitionDesc'))
		->group('fc_id')
		->order('fc_seq');
		$fc_cat = $db->fetchAll($selectData);
		return($fc_cat);
	}
	
	public function getByCode($code) {
		$select = $this->select()
					->where('fc_code = ?', $code);
		$fc_cat = $this->fetchRow($select);
		return($fc_cat);
	}
	
	public function getListCategory() {
		$select = $this->select()
		->order('fc_seq');
		$fc_cat = $this->fetchAll($select);
		return($fc_cat);
	}
	
	
}