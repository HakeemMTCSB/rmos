<?php

class Studentfinance_Model_DbTable_Sponsor extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_sponsor';

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fngetallsponsor(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('spnsr' => 'tbl_sponsor'),array('spnsr.*','id'=>'idsponsor','code'=>'SponsorCode','name'=>'fName'));
		return $result = $this->lobjDbAdpt->fetchAll($lstrSelect);

	}

	public function getApplicantSponsor($appl_id)
	{
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_sponsor_tag'),array('a.*'))
		->joinLeft(array('b'=>'tbl_sponsor'), 'a.Sponsor=b.idsponsor', array('b.SponsorCode', "CONCAT_WS(' ',fName, lName) as sponsor_name"))
		->where('a.appl_id =?',$appl_id);
			
		$result = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $result;
	}
	
	public function deleteApplicantSponsor($appl_id)
	{
		$this->lobjDbAdpt->delete('tbl_sponsor_tag', 'IdSponsorTag='.$appl_id);
	}
	
	public function updateApplicantSponsor($data, $appl_id)
	{
		$this->lobjDbAdpt->update('tbl_sponsor_tag', $data, 'IdSponsorTag='.$appl_id);
	}

	public function addApplicantSponsor($data)
	{
		$this->lobjDbAdpt->insert('tbl_sponsor_tag', $data);
	}

	public function fngetdesc($desc){
		$select = $this->lobjDbAdpt->select($desc)
		->from(array("a" => "tbl_fee_setup"), array("a.*"))
		->where("a.IdFeeSetup = ?",$desc);
		$result = $this->lobjDbAdpt->fetchall($select);

		return $result[0]['Description'];
	}

	public function fngetsponsor(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_sponsor"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchall($select);

		return $result;
	}

	public function fngetsponsorid($sponsorid){
		$select = $this->lobjDbAdpt->select($sponsorid)
		->from(array("a" => "tbl_sponsor"), array("a.*"))
		->where("a.idsponsor = ?",$sponsorid);;
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result[0]['SponsorCode'];
	}
	
	public function fngetsponsorbyid($sponsorid){
		$select = $this->lobjDbAdpt->select($sponsorid)
		->from(array("a" => "tbl_sponsor"), array("a.*",'id'=>'idsponsor','code'=>'SponsorCode','name'=>'fName'))
		->where("a.idsponsor = ?",$sponsorid);;
		$result = $this->lobjDbAdpt->fetchRow($select);
		return $result;
	}

	public function fngetstudentname($lstrstudentname){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_studentregistration"), array("a.IdStudentRegistration"))
		->joinLeft(array('p'=>'student_profile'),"a.sp_id=p.id",("CONCAT_WS(' ',IFNULL(p.appl_fname,''),IFNULL(p.appl_mname,''),IFNULL(p.appl_lname,'')) AS name"))
		->where("a.registrationId = ?",$lstrstudentname)
		->where("a.ProfileStatus = 92");
		$result = $this->lobjDbAdpt->fetchRow($select);
		return $result;
	}


	public function fncheckcodeexist($strsponsorcode){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('spnsr' => 'tbl_sponsor'),array('spnsr.*'))
		->where('spnsr.SponsorCode =?',$strsponsorcode);
		return $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
	}

	public function fncheckSponsorCode($CheckSponsor){

		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_sponsor'),array('a.*'))
		->where('a.SponsorCode =?',$CheckSponsor);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}


	public function fnSearchSponsorSetup($post = array()) {

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_sponsor"), array("a.*"));
			
		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where($this->lobjDbAdpt->quoteInto("a.SponsorCode LIKE ?", "%".$post['field2']."%"));

		}

	 if(isset($post['field3']) && !empty($post['field3']) ){
	 	$select = $select->where($this->lobjDbAdpt->quoteInto("CONCAT_WS(' ', a.fName, a.lName) like ?", "%".$post['field3']."%"));

	 }

	 $result = $this->lobjDbAdpt->fetchAll($select);
	 return $result;
	}

	public function fnSearchTagStudentSponsor($post = array()) {

		$select = $this->lobjDbAdpt->select()
		->from(array('b'=>'tbl_sponsor'),array('b.SponsorCode as SponsorId','CONCAT_WS(" ",b.fName,b.lName) as Name','b.idsponsor'));
			
		if(isset($post['field5']) && !empty($post['field5']) ){
			$select = $select->where("b.idsponsor = ?",$post['field5']);

		}
		if(isset($post['field3']) && !empty($post['field3']) ){
			$select = $select->where("b.SponsorCode LIKE '%".$post['field3']."%'");

		}

		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}



	public function fnaddSponsorSetup($larrformData,$SponsorId){

		$paramArray = array(
				'SponsorCode' => $SponsorId,
				'fName' => $larrformData['FirstName'],
				'lName' => $larrformData['LastName'],
				'Add1' => $larrformData['Address1'],
				'Add2' => $larrformData['Address2'],
				'Country' => $larrformData['Country'],
				'State' => $larrformData['State'],
				'City' => $larrformData['City'],
				'zipCode' => $larrformData['Postal'],
				'CellPhone' =>$larrformData['Phone'],
				'Fax' => $larrformData['Fax'],
				'Email' => $larrformData['EmailAddress'],
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate']
		);

		$this->lobjDbAdpt->insert('tbl_sponsor',$paramArray);
		$lastinsertID = $this->lobjDbAdpt->lastInsertId();
		
		// Fee Info
		if($larrformData['FeeCodeGrid']){
			$check = count($larrformData['FeeCodeGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'idsponsor'=>$lastinsertID,
						'FeeCode'=> $larrformData['FeeCodeGrid'][$i],
						'CalculationMode'=> $larrformData['CalculationModeGrid'][$i],
						'Amount'=>$larrformData['AmountGrid'][$i],
						'Repeat'            => $larrformData['RepeatGrid'][$i],
						'MaxRepeat'            => $larrformData['MaxRepeatGrid'][$i],
						'FreqMode'            => $larrformData['FreqModeGrid'][$i],
				);

				$this->lobjDbAdpt->insert('tbl_sponsor_fee_info',$paramArray);
			}

		}

		//Coordinator
		$auth = Zend_Auth::getInstance();
		if ( isset($larrformData['coordinator_name']) )
		{
			$totalCoord = count($larrformData['coordinator_name']);
			if ( count ( $totalCoord ) > 0 )
			{
				for( $i=0; $i<=count($totalCoord); $i++ )
				{
					$data = array(
							'c_sponsor_id'=>$lastinsertID,
							'c_name'=> $larrformData['coordinator_name'][$i],
							'c_email'=> $larrformData['coordinator_email'][$i],
							'c_phone'=> $larrformData['coordinator_phone'][$i],
							'c_mobile'=> $larrformData['coordinator_mobile'][$i],
							'c_fax'=> $larrformData['coordinator_fax'][$i],
                                                        'c_active'=> $larrformData['coordinator_active'][$larrformData['coordinator_name'][$i]],
							'created_by' => $auth->getIdentity()->iduser,
							'created_date' => new Zend_Db_Expr('NOW()')
					);

					$this->lobjDbAdpt->insert('tbl_sponsor_coordinator',$data);
				}
			}
		}
	}
	
	

	public function getCoordinator($sponsor_id)
	{
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_sponsor_coordinator'),array('a.*'))
		->where('a.c_sponsor_id =?',$sponsor_id);
			
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fnupdateSponsorSetup($larrformData,$id){

		$paramArray = array(
				'fName' => $larrformData['FirstName'],
				'lName' => $larrformData['LastName'],
				'Add1' => $larrformData['Address1'],
				'Add2' => $larrformData['Address2'],
				'Country' => $larrformData['Country'],
				'State' => $larrformData['State'],
				'City' => $larrformData['City'],
				'zipCode' => $larrformData['Postal'],
				'CellPhone' => $larrformData['Phone'],
				'Fax' => $larrformData['Fax'],
				'Email' => $larrformData['EmailAddress'],
		);
		$where_up = "  idsponsor =' ".$id."' ";
		$this->lobjDbAdpt->update('tbl_sponsor',$paramArray,$where_up);

		if($larrformData['FeeCodeGrid']){
			$check = count($larrformData['FeeCodeGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'idsponsor'=>$id,
						'FeeCode'=> $larrformData['FeeCodeGrid'][$i],
						'CalculationMode'=> $larrformData['CalculationModeGrid'][$i],
						'Amount'=>$larrformData['AmountGrid'][$i],
						'Repeat'            => $larrformData['RepeatGrid'][$i],
						'MaxRepeat'            => $larrformData['MaxRepeatGrid'][$i],
						'FreqMode'            => $larrformData['FreqModeGrid'][$i],
				);

				$this->lobjDbAdpt->insert('tbl_sponsor_fee_info',$paramArray);
			}

		}

		//Coordinator
		$auth = Zend_Auth::getInstance();
		if ( isset($larrformData['coordinator_name']) )
		{
			$totalCoord = count($larrformData['coordinator_name']);
		
			if ( count ( $totalCoord ) > 0 )
			{
				for( $i=0; $i<=count($totalCoord); $i++ )
				{
					$data = array(
							'c_sponsor_id'=>$id,
							'c_name'=> $larrformData['coordinator_name'][$i],
							'c_email'=> $larrformData['coordinator_email'][$i],
							'c_phone'=> $larrformData['coordinator_phone'][$i],
							'c_mobile'=> $larrformData['coordinator_mobile'][$i],
							'c_fax'=> $larrformData['coordinator_fax'][$i],
                                                        'c_active'=> $larrformData['coordinator_active'][$larrformData['coordinator_name'][$i]],
							'created_by' => $auth->getIdentity()->iduser,
							'created_date' => new Zend_Db_Expr('NOW()')
					);

					$this->lobjDbAdpt->insert('tbl_sponsor_coordinator',$data);
				}
			}
		}
	}

	public function fngetcount($table,$coloumn,$cashierno='')
	{
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('spnsr' => $table),array("COUNT(spnsr.$coloumn) as codecount"));
		
		if ( $cashierno != '' )
		{
			$lstrSelect->where('cashierNo = ?', $cashierno);
		}

		return $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
	}

	public function fngetsponsorsetupById($id){

		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_sponsor'),array('a.*'))
		->where('a.idsponsor =?',$id);
			
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fngettagstudentById($id,$intake=0,$idStudent=0,$name=null, $checkdate = false){

		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_sponsor_tag'),array('a.*','startdate'=>'StartDate','enddate'=>"IFNULL(EndDate,'0000-00-00')"))
		->joinLeft(array('sp'=>'tbl_sponsor'),'sp.idsponsor = a.Sponsor',array('fname','lname','SponsorCode'))
		->joinLeft(array('b'=>'tbl_studentregistration'),'b.IdStudentRegistration = a.StudentId',array('b.registrationId as StudentId','IdStudentRegistration','registrationId'))
		->joinLeft(array('d'=>'fee_item'),'d.fi_id = a.FeeItem',array('d.fi_name'))
		->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )",'appl_fname','appl_mname','appl_lname')))
		->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
		->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'));

		if ($checkdate == true){
			$lstrSelect->where('a.StartDate <= ?', date('Y-m-d'))->where('(CASE IFNULL(a.EndDate,"0000-00-00") WHEN "0000-00-00" THEN CURDATE() ELSE a.EndDate END) >= ?', date('Y-m-d'));
		}

		if($id){
			$lstrSelect->where('a.Sponsor =?',$id);
		}
		
		if($intake){
			$lstrSelect->where('b.IdIntake = ?',$intake);
		}
		
		if($idStudent){
			$lstrSelect->where('b.registrationId LIKE ?','%'.$idStudent.'%');
		}
		
		if($name){
			$lstrSelect->where("CONCAT_WS(' ', ap.appl_fname,ap.appl_mname,ap.appl_lname ) LIKE ?",'%'.$name.'%');
		}
//		echo $lstrSelect;
//		echo "<hr>";
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fnaddTagStudentSponsorSetup($larrformData){


		if($larrformData['idstudentregistrationGrid']){
			$check = count($larrformData['idstudentregistrationGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'Sponsor' => $larrformData['Sponsor'],
						'StudentId' => $larrformData['idstudentregistrationGrid'][$i],
						'StartDate' => $larrformData['StartDateGrid'][$i],
						'EndDate' => $larrformData['EndDateGrid'][$i],
						'AggrementNo' => $larrformData['AggrementNoGrid'][$i],
						'Amount' => $larrformData['AmountGrid'][$i],
						//'AggrementStatus' => $larrformData['AggrementStatusGrid'][$i],
						'CalcMode' => $larrformData['CalcMode'][$i],
						'FeeItem' => $larrformData['FeeItemGrid'][$i],
						'UpdUser'=>$larrformData['UpdUser'],
						'UpdDate'=>$larrformData['UpdDate'],
						'IdUniversity'=>$larrformData['IdUniversity'],
				);
				$this->lobjDbAdpt->insert('tbl_sponsor_tag',$paramArray);
			}
		}

	}

	public function fnupdateTagStudentSponsorSetup($larrformData,$id){

		if($larrformData['idstudentregistrationGrid']){
			$check = count($larrformData['idstudentregistrationGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'Sponsor' => $id,
						'StudentId' => $larrformData['idstudentregistrationGrid'][$i],
						'StartDate' => $larrformData['StartDateGrid'][$i] == '' ? '0000-00-00':date('Y-m-d', strtotime($larrformData['StartDateGrid'][$i])),
						'EndDate' => $larrformData['EndDateGrid'][$i] == '' ? '0000-00-00':date('Y-m-d', strtotime($larrformData['EndDateGrid'][$i])),
						'AggrementNo' => $larrformData['AggrementNoGrid'][$i],
						'Amount' => $larrformData['AmountGrid'][$i],
						//'AggrementStatus' => $larrformData['AggrementStatusGrid'][$i],
						'CalcMode' => $larrformData['CalcMode'][$i],
						'FeeItem' => $larrformData['FeeItemGrid'][$i],
						'UpdUser'=>$larrformData['UpdUser'],
						'UpdDate'=>$larrformData['UpdDate'],
						'IdUniversity'=>$larrformData['IdUniversity'],
				);
				//$where_up = "  Sponsor =' ".$id."' ";
				$this->lobjDbAdpt->insert('tbl_sponsor_tag',$paramArray);
			}
		}

	}

	public function getTagStudent($id)
	{
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => 'tbl_sponsor_tag'),array('a.*'))
		->where('a.IdSponsorTag =?',$id);
			
		$result = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $result;
	}

	public function updateTagStudent( $data, $id )
	{
		$this->lobjDbAdpt->update('tbl_sponsor_tag', $data, 'IdSponsorTag = '.(int) $id);
	}
	
	public function fnCheckEmailId($larrformData){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_sponsor"), array("a.Email"))
					->where("a.Email = ?",$larrformData['EmailAddress']);;
		$result = $this->lobjDbAdpt->fetchrow($select);
		return $result;
	}
	

	public function getAppSetup($where,$all=0)
	{            //var_dump($where); exit;
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_sponsor_appform") )
					->joinLeft(array('p' => 'tbl_program'), 'a.program_id=p.IdProgram', array('p.ProgramName'))
					->joinLeft(array('ps'=>'tbl_program_scheme'), 'a.program_scheme_id = ps.IdProgramScheme', array())
					->joinLeft(array('mop'=>'tbl_definationms'), 'ps.mode_of_program = mop.idDefinition', array('mop'=>'mop.DefinitionDesc'))
					->joinLeft(array('mos'=>'tbl_definationms'), 'ps.mode_of_study = mos.idDefinition', array('mos'=>'mos.DefinitionDesc'))
					->joinLeft(array('pt'=>'tbl_definationms'), 'ps.program_type = pt.idDefinition', array('pt'=>'pt.DefinitionDesc'))
					->joinLeft(array('sc'=>'tbl_definationms'), 'a.student_category = sc.idDefinition', array('sc'=>'sc.DefinitionDesc'))
					->joinLeft(array('s' => 'tbl_scholarship_sch'),'s.sch_Id=a.sch_type', array('s.sch_name'))
					->joinLeft(array('b' => 'tbl_sponsor'),'b.idsponsor=a.sch_type', array('b.fName'))
					->joinLeft(array('c' => 'tbl_financial_aid'),'c.id=a.sch_type', array('c.name'));

		foreach ( $where as $what => $val )
		{
			$select->where($what.'=?', $val);
		}
		
		if ( $all )
		{
			$result = $this->lobjDbAdpt->fetchAll($select);
		}
		else
		{
			$result = $this->lobjDbAdpt->fetchRow($select);
		}
		return $result;
	}

	public function insertAppSetup($data)
	{
		$this->lobjDbAdpt->insert("tbl_sponsor_appform", $data);
	}

	public function updateAppSetup($data, $where )
	{
		$this->lobjDbAdpt->update("tbl_sponsor_appform", $data, $where );
	}
        
        public function updateCoordinator($bind , $id){
            $db = $this->lobjDbAdpt;
            $update = $db->update('tbl_sponsor_coordinator', $bind, 'c_id = '.$id);
            return $update;
        }
        
        public function deleteStudentTag($id){
            $db = $this->lobjDbAdpt;
            $delete = $db->delete('tbl_sponsor_tag', 'IdSponsorTag = '.$id);
            return $delete;
        }
        
	public function getDataByStudent($id,$idStudentRegistration)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		echo $lstrSelect = $db->select()
		->from(array('a' => 'tbl_scholarship_studenttag'),array('*','startdate'=>'sa_start_date','end_date'=>"IFNULL(sa_end_date,'0000-00-00')"))
		->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration')
		->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
		->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
		->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
		->where('a.sa_scholarship_type =?',$id)
		->where('a.sa_cust_id =?',$idStudentRegistration);
			
		$result = $db->fetchRow($lstrSelect);
		return $result;
	}
	
	public function getFeeInfo($id,$fi_id=0)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'tbl_sponsor_fee_info'),array('a.*'))
		->joinLeft(array('b' => 'fee_item'), 'a.FeeCode = b.fi_id', array('fi_name','fi_code','fi_id'))
		->where('a.idsponsor =?',$id);
		
		if($fi_id){
			$lstrSelect->where('a.FeeCode =?',$fi_id);
			$result = $db->fetchRow($lstrSelect);
		}else{
			$result = $db->fetchAll($lstrSelect);
		}
		
		return $result;
	}

        public function viewApp($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $selectData = $db->select()
                ->from(array('pm'=>'sponsor_application'))
                ->joinLeft(array('b'=>'sponsor_application_details'),'b.application_id=pm.sa_id')
                ->joinLeft(array('c'=>'tbl_studentregistration'),'pm.sa_cust_id=c.IdStudentRegistration')
                ->joinLeft(array('d'=>'student_profile'),'c.sp_id=d.id')
                ->joinLeft(array('e'=>'tbl_program'),'c.IdProgram=e.IdProgram')
                ->joinLeft(array('f'=>'tbl_qualificationmaster'), 'b.edulevel = f.IdQualification', array('f.QualificationLevel'))
				->joinLeft(array('g'=>'tbl_semestermaster'), 'pm.sa_semester_id = g.IdSemesterMaster')
                ->where("pm.sa_id = ?", (int)$id);


            $row = $db->fetchRow($selectData);				
            return $row;
	}
        
        public function viewApp2($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $selectData = $db->select()
                ->from(array('pm'=>'sponsor_application'))
                ->joinLeft(array('b'=>'sponsor_application_details'),'b.application_id=pm.sa_id')
                ->joinLeft(array('c'=>'applicant_transaction'),'pm.sa_cust_id=c.at_trans_id')
                ->joinLeft(array('d'=>'applicant_profile'),'c.at_appl_id=d.appl_id')
                ->joinLeft(array('g'=>'applicant_program'),'c.at_trans_id=g.ap_at_trans_id')
                ->joinLeft(array('e'=>'tbl_program'),'g.ap_prog_id=e.IdProgram')
                ->joinLeft(array('f'=>'tbl_qualificationmaster'), 'b.edulevel = f.IdQualification', array('f.QualificationLevel'))
                ->joinLeft(array('h'=>'applicant_financial'), 'pm.sa_af_id = h.af_id', array())
                ->joinLeft(array('i'=>'tbl_scholarshiptagging_sct'), 'h.af_scholarship_apply = i.sct_Id', array('i.sct_schId'))
                ->where("pm.sa_id = ?", (int)$id);


            $row = $db->fetchRow($selectData);				
            return $row;
	}
        
        public function uploadfile($data){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('sponsor_application_upload', $data);
            //var_dump($data); exit;
            return true;
        }
        
        public function deleteUpl($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $delete = $db->delete('sponsor_application_upload', 'sau_id = '.$id);
            return $delete;
        }
        
        public function viewUpl($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $selectData = $db->select()
                ->from(array('pm'=>'sponsor_application_upload'))
                ->where("pm.sau_sa_id = ?", (int)$id);

            $row = $db->fetchAll($selectData);				
            return $row;
	}
        
        public function getHighestLevelEdu($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'student_qualification'), array('value'=>'*'))
                ->join(array('b'=>'tbl_qualificationmaster'), 'a.ae_qualification = b.IdQualification')
                ->joinLeft(array('c'=>'tbl_definationms'), 'a.ae_class_degree = c.idDefinition')
                ->where('b.qualification_type_id = ?', 607)
                ->where('a.sp_id = ?', $id)
                ->order('b.QualificationRank DESC');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getHighestLevelEdu2($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_program'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_definationms'), 'a.Award = b.idDefinition', array('QualificationLevel'=>'b.DefinitionDesc'))
                ->where('a.IdProgram = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getCurrency(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
                ->where('a.idDefType = ?', 178);

            $result = $db->fetchAll($select);
            return $result;
        }

        static function getCurrencyDefination($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('a.DefinitionDesc'))
                ->where('a.idDefinition = ?', $id);

            $result = $db->fetchOne($select);
            return $result;
        }
        
        public function getSemester($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                //->where('a.IsCountable = ?', 1)
                ->where('a.IdScheme = ?', $id)
                ->order('a.SemesterMainStartDate DESC');

            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getIndustryWeightageList(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_industry'), array('value'=>'*'));
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getListSemesterRegistered($IdStudentRegistration){
        	
            $auth = Zend_Auth::getInstance(); 
            $db = Zend_Db_Table::getDefaultAdapter();

            $date = date('Y-m-d');

            $sql = $db->select()
                ->from(array('srs'=>'tbl_studentregsubjects'),array('IdSemesterMain'))
                ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array('SemesterMainName','SemesterMainCode','SemesterFunctionType','IsCountable'))
                ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
                ->where('srs.Active IN (1,4,6)')
                ->where("srs.exam_status NOT IN ('I','M')")
                ->where('sm.SemesterMainEndDate < ?',$date)
                ->order('sm.SemesterMainStartDate')
                ->group('srs.IdSemesterMain');

            $result = $db->fetchAll($sql);

            //below code was created to cater if there are subjct registered in tbl_studentregsubject but record in tbl_studentsemesterstatus 14-2-1014 yatie
            foreach($result as $index=>$semester){

                //check if studentsemesterstatus exist
                $isExist = $this->getSemesterStatusBySemester($IdStudentRegistration,$semester['IdSemesterMain']);

                if(!$isExist){
                    //create
                    $data = array(  'IdStudentRegistration' => $IdStudentRegistration,									           
                        'idSemester' => $semester['IdSemesterMain'],
                        'IdSemesterMain' => $semester['IdSemesterMain'],								
                        'studentsemesterstatus' => 130, 	//Register idDefType = 32 (student semester status)
                        'Level'=>1,
                        'UpdDate' => date ( 'Y-m-d H:i:s'),
                        'UpdUser' => $auth->getIdentity()->iduser
                    );											
                    $idstudentsemsterstatus = $this->addData2($data);
                    $result[$index]['idstudentsemsterstatus']=$idstudentsemsterstatus;
                }else{
                    $result[$index]['idstudentsemsterstatus']=$isExist['idstudentsemsterstatus'];
                }
            }
            return $result;
        }
        
        public function getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain){
            $db = Zend_Db_Table::getDefaultAdapter();      
            $sql = $db->select()	
                ->from(array('sss' => 'tbl_studentsemesterstatus'))
                ->where('IdStudentRegistration  = ?',$IdStudentRegistration)
                ->where('IdSemesterMain = ?',$IdSemesterMain);
            $result = $db->fetchRow($sql);
            return $result;		
	}
        
        public function addData2($data){
            $db = Zend_Db_Table::getDefaultAdapter();
            $id = $db->insert('tbl_studentsemesterstatus', $data);
            return $id;
	}
        
        public function getListCourseRegisteredBySemesterWithAttendanceStatus($registrationId,$idSemesterMain){
		
            $db = Zend_Db_Table::getDefaultAdapter();

            $sql = $db->select()
                ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName')) 
                ->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idProgram=sr.IdProgram AND er.er_idSubject=srs.IdSubject AND er.er_attendance_status !=396')
                ->where('sr.IdStudentRegistration = ?', $registrationId)
                ->where('srs.IdSemesterMain = ?',$idSemesterMain)        
                // ->where('srs.subjectlandscapetype != 2')  
                // ->where('er.er_attendance_status != ?',396)//absent with valid reason no need to display              
                ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5 OR srs.Active=6') //Status => 1:Register 4:Repeat 5:Refer
                ->where('srs.exam_status!= ?','I');  // kalo exam_status I=incomplete jgn display subject ni

            $result = $db->fetchAll($sql);
            return $result;
	}
        
        public function getListCourseRegisteredBySemesterWithoutAttendance($registrationId,$idSemesterMain){
		
            $db = Zend_Db_Table::getDefaultAdapter();

            $sql = $db->select()
                ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName')) 
                //->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idProgram=sr.IdProgram AND er.er_idSubject=srs.IdSubject')
                ->where('sr.IdStudentRegistration = ?', $registrationId)
                ->where('srs.IdSemesterMain = ?',$idSemesterMain)
                //->where('er.er_attendance_status != ?',396)        
               // ->where('srs.subjectlandscapetype != 2')                
                ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5 or srs.Active=6')   //Status => 1:Register 4:Repeat 5:Refer
                ->where('srs.exam_status!= ?','I'); // kalo exam_status I=incomplete jgn display subject ni

            $result = $db->fetchAll($sql);
            return $result;
	}
        
        function getStudentGrade($IdStudentRegistration,$idSemester){
		
            $db =  Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
               ->from('tbl_student_grade')
               ->where('sg_IdStudentRegistration = ?',$IdStudentRegistration)
               ->where('sg_semesterId = ?',$idSemester);

            return $rowSet = $db->fetchRow($select);
		
	}
        
        public function viewReAppList($search = false){
            $db =  Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'sponsor_reapplication'), array('value'=>'*'))
                ->join(array('b'=>'tbl_studentregistration'), 'a.sre_cust_id=b.IdStudentRegistration')
                ->join(array('c'=>'student_profile'), 'b.sp_id=c.id')
                ->joinLeft(array('d'=>'tbl_semestermaster'), 'a.sre_semester_id=d.IdSemesterMaster')
                ->joinLeft(array('pr'=>'tbl_program'), 'b.IdProgram = pr.IdProgram')
                ->joinLeft(array('prs'=>'tbl_program_scheme'), 'b.IdProgramScheme = prs.IdProgramScheme')
                ->joinLeft(array('df'=>'tbl_definationms'), 'prs.mode_of_program = df.idDefinition', array('mode'=>'df.DefinitionDesc'));
            
            if ($search != false){
                if (isset($search['Program']) && $search['Program']!=''){
                    $select->where('b.IdProgram = ?', $search['Program']);
                }
                if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                    $select->where('b.IdProgramScheme = ?', $search['ProgramScheme']);
                }
                if (isset($search['SemesterApplied']) && $search['SemesterApplied']!=''){
                    $select->where('a.sre_semester_id = ?', $search['SemesterApplied']);
                }
                if (isset($search['StudentName']) && $search['StudentName']!=''){
                    $select->where($db->quoteInto('CONCAT_WS(" ", c.appl_fname, c.appl_lname) like ?', '%'.$search['StudentName'].'%'));
                }
                if (isset($search['StudentID']) && $search['StudentID']!=''){
                    $select->where($db->quoteInto('b.registrationId like ?', '%'.$search['StudentID'].'%'));
                }
                if (isset($search['Status']) && $search['Status']!=''){
                    if ($search['Status']==2 || $search['Status']==7){
                        $select->where('a.sre_status IN (2, 7)');
                    }else{
                        $select->where('a.sre_status = ?', $search['Status']);
                    }
                }
            }
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function set_restatus($as_id, $status, $remarks=false) {
            $db =  Zend_Db_Table::getDefaultAdapter();
            $where = $db->quoteInto('sre_id IN (?)', $as_id);

            $data['sre_status'] = $status;
            if ($remarks != false){
                $data['sre_remarks'] = $remarks;
            }
            $db->update('sponsor_reapplication', $data, $where);
            return(true);
        }
        
        public function getSectionC($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_sponsor_appform'), array('value'=>'*'))
                ->where('a.type = ?', 'student')
                ->where('a.stype = ?', 3)
                ->where('a.application_type = ?', 1)
                ->where('a.sch_type = ?', 1)
                ->where('a.program_id = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getReApplication($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'sponsor_reapplication'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.sre_cust_id=b.IdStudentRegistration')
                ->joinLeft(array('c'=>'student_profile'), 'b.sp_id=c.id')
                ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
				->joinLeft(array('e'=>'tbl_semestermaster'), 'a.sre_semester_id = e.IdSemesterMaster')
                ->where('a.sre_id = ?', $id)
                ->where('a.sre_cust_type = 1');

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getSemesterById($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                ->where('a.IdSemesterMaster = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getLastSem($idScheme, $curSemId, $date, $branch){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                ->where('a.IdScheme = ?', $idScheme)
                ->where('a.IdSemesterMaster != ?', $curSemId)
                ->where('a.IsCountable = ?', 1)
                ->where('a.Branch = '.$branch.' or a.Branch = 0')
                ->where('a.SemesterMainStartDate < ?', $date)
                ->order('a.SemesterMainStartDate DESC');

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getRegSubLastSem($studentid, $semesterid){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
				->joinLeft(array('c'=>'exam_registration'), 'a.IdSubject = c.er_idSubject AND a.IdStudentRegistration = c.er_idStudentRegistration AND a.IdSemesterMain = c.er_idSemester')
				->where('a.IdStudentRegistration = ?', $studentid)
				->where('c.er_attendance_status != ?', 396)
                ->where('a.IdSemesterMain = ?', $semesterid);

            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getCity($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_city'))
                ->where('a.idCity = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }

        public function getState($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_state'))
                ->where('a.idState = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }

        public function getCountry($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_countries'))
                ->where('a.idCountry = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getLevelEducationList($type = 607){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_qualificationmaster'), array('value'=>'*'))
                ->where('a.qualification_type_id = ?', $type)
                ->order('a.QualificationRank');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getProgramList(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_program'), array('value'=>'*'))
                ->order('a.seq_no ASC')
                ->order('a.ProgramName ASC');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getHeadTemplate($pid){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_sponsor_appform'), array('value'=>'*'))
                ->where('a.type = ?', 'student')
                ->where('a.application_type = ?', 0)
                ->where('a.stype = ?', 3)
                ->where('a.sch_type = ?', 1)
                ->where('a.program_id = ?', $pid);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        static function getCityS($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_city'), array('CityName'))
                ->where('a.idCity = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getStateS($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_state'), array('StateName'))
                ->where('a.idState = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getCountryS($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_countries'), array('CountryName'))
                ->where('a.idCountry = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getDefination($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('DefinitionDesc'))
                ->where('a.idDefinition = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        public function getPublishResult($programId, $semesterId, $category = 0){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_publish_mark'), array('value'=>'*'))
                ->where('a.pm_idProgram = ?', $programId)
                ->where('a.pm_idSemester = ?', $semesterId)
                ->where('a.pm_category = ?', $category);

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getIndustryWeightage($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_industry'), array('value'=>'*'))
                ->where('a.id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getAgeWeightage($age){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_age_weight'), array('value'=>'*'))
                ->where('"'.$age.'" BETWEEN a.from AND a.to');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getAcademicWeightage($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_academic_weight'), array('value'=>'*'))
                ->where('a.id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getIncomeWeightage($income, $currency){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_income_weight'), array('value'=>'*'))
                ->where('"'.$income.'" BETWEEN a.from AND a.to')
                ->where('a.currency = ?', $currency);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getDependentWeightage($no){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_dependent_weight'), array('value'=>'*'))
                ->where('"'.$no.'" BETWEEN a.from AND a.to');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getApplicationCourse($id, $group=false){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.sac_subjectid = b.IdSubject')
                ->where('a.sac_said = ?', $id);

			if ($group == true){
				$select->group('a.sac_subjectid');
			}
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getReApplicationCourse($id, $group=false){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.src_subjectid = b.IdSubject')
                ->where('a.src_sreid = ?', $id);

			if ($group == true){
				$select->group('a.src_subjectid');
			}
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
    public function getCourseCovered($semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_financial_aid_course'), array('value'=>'*'))
            ->where('a.financial_aid_id = ?', 1)
            ->where('a.semester_id = ?', $semesterid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function storeSubjectApp($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_application_course', $bind);
        $id = $db->lastInsertId('sponsor_application_course', 'sac_id');
        return $id;
    }
    
    public function deleteSubjectApp($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('sponsor_application_course', 'sac_said = '.$id);
        return $delete;
    }
    
    public function insertSponsorCourse($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_reapplication_course', $bind);
        $id = $db->lastInsertId('sponsor_reapplication_course', 'src_id');
        return $id;
    }
    
    public function deleteSubjectReApp($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('sponsor_reapplication_course', 'src_sreid = '.$id);
        return $delete;
    }

	public function getSponsorInfo($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_sponsor'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_countries'), 'a.Country = b.idCountry', array('b.CountryName'))
			->joinLeft(array('c'=>'tbl_state'), 'a.State = c.idState', array('c.StateName'))
			->joinLeft(array('d'=>'tbl_city'), 'a.City = d.idCity', array('d.CityName'))
			->where('a.idsponsor = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function getAttachmentList($id, $statusid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_sponsor_appform_attachment'))
			->join(array('b'=>'comm_template'), 'a.att_tpl_id = b.tpl_id')
			->where('a.att_appform_id = ?', $id)
			->where('a.att_status_id = ?', $statusid);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function getAttachmentListAll(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'comm_template'))
			->where('a.tpl_module = ?', 'scholarship')
			->where('a.tpl_type = ?', 587);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function deleteAttachment($id, $statusid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$delete = $db->delete('tbl_sponsor_appform_attachment', 'att_status_id = '.$statusid.' AND att_appform_id = '.$id);
		return $delete;
	}

	public function insertAttachment($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert('tbl_sponsor_appform_attachment', $data);
		$id = $db->lastInsertId('tbl_sponsor_appform_attachment', 'att_id');
		return $id;
	}

	public function getApplicantInvoice($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'invoice_main'))
			->where('a.status = ?', 'A')
			->where('a.trans_id = ?', $id);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function deleteFormSetup($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$delete = $db->delete('tbl_sponsor_appform', 'id = '.$id);
		return $delete;
	}
}