<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_AccountCode extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'tbl_account_code';
	protected $_primary = "ac_id";


	
	public function getAccountCode($status=null, $type=null){
    $db = Zend_Db_Table::getDefaultAdapter();

    $selectData = $db->select()
        ->from(array('ac'=>$this->_name))
        ->join(array('t'=>'tbl_definationms'),'t.idDefinition = ac.ac_acc_type', array('ac_acc_type_name'=>'t.DefinitionCode'))
        ->join(array('u'=>'tbl_user'),'u.idUser = ac.ac_last_edit_by', array('last_edit_by_name'=>"concat_ws(' ',fName,mNAme,lName)"));

    if($status){
        $selectData->where('ac.ac_status = ?',$status);
    }

    if($type){
        if(is_array($type)){
            $selectData->where('ac.ac_acc_type in (?)',$type);
        }else{
            $selectData->where('ac.ac_acc_type = ?',$type);
        }
    }

    $row = $db->fetchAll($selectData);

    if(!$row){
        return null;
    }else{
        return $row;
    }
}

    public function insert(array $data){

        $auth = Zend_Auth::getInstance();

        if(!isset($data['ac_last_edit_by'])){
            $data['ac_last_edit_by'] = $auth->getIdentity()->iduser;
        }

        $data['ac_last_edit_date'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }

    public function update(array $data,$where){

        $auth = Zend_Auth::getInstance();

        if(!isset($data['ac_last_edit_by'])){
            $data['ac_last_edit_by'] = $auth->getIdentity()->iduser;
        }

        $data['ac_last_edit_date'] = date('Y-m-d H:i:s');

        return parent::update($data,$where);
    }
}
?>