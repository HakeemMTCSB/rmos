<?php
class Studentfinance_Model_DbTable_PaymentBankDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'payment_bank_record_detail';
	protected $_primary = "id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pbrd'=>$this->_name))
					->where("pbrd.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}	
	
	public function getRecordCountAmount($pbr_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pbrd'=>$this->_name), array('record'=>'count(id)','total_amount'=>'sum(amount_total)'))
					->where("pbrd.pbr_id = ?", (int)$pbr_id);
		
		$row = $db->fetchRow($selectData);
		
		if(!$row){
			return array(0,0);
		}else{
			return $row;	
		}				
		
	}
	
	public function getPaymentBankDetail($pbr_id, $account_type=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pbrd'=>$this->_name))
					->joinLeft(array('at'=>'applicant_transaction'), 'at.at_pes_id  = pbrd.payee_id')
					->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id  = at.at_appl_id')
					->where("pbrd.pbr_id = ?", (int)$pbr_id);
					
		if($account_type!= null){
			$selectData->where('pbrd.account_record_type = ? ', $account_type);
		}			

		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
	
	public function getPaginatePaymentBankDetail($pbr_id, $account_type=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('pbrd'=>$this->_name))
					->joinLeft(array('at'=>'applicant_transaction'), 'at.at_pes_id  = pbrd.payee_id')
					->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id  = at.at_appl_id')
					->where("pbrd.pbr_id = ?", (int)$pbr_id);
					
		if($account_type!= null){
			$selectData->where('pbrd.account_record_type = ? ', $account_type);
		}	
		
		return $selectData;
	}
	
	public function getPaymentBankDetailBill($pbr_id, $account_type=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pbrd'=>$this->_name))
					->where("pbrd.pbr_id = ?", (int)$pbr_id);
					
		if($account_type!= null){
			$selectData->where('pbrd.account_record_type = ? ', $account_type);
		}				

		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
}
?>