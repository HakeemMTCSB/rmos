<?php 
class Studentfinance_Model_DbTable_Policysetup extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_policysetup_initialpayment';
	private $lobjDbAdpt;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	public function getschemelist(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_scheme"), array("a.*",($this->locale=='ar_YE'?'a.ArabicDescription as SchemeName':'a.EnglishDescription as SchemeName')))
		->where("a.Active = ?",1);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function getIntakelist(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_intake"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function getprogramlist(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_program"), array("a.*",($this->locale=='ar_YE'?'a.ArabicName as ProgramName':'a.ProgramName as ProgramName')))
		->where("a.Active = ?",1);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}
	public function addpolicysetup($larrformData,$SemesterFrom,$SemesterTo,$IntakeFrom,$IntakeTo){

		$paramArray = array(
				'Scheme' => $larrformData['Scheme'],
				'StudentCategory' => $larrformData['StudentCategory'],
				'Value' => $larrformData['Value'],
				'Award' => $larrformData['Award'],
				'IdBranch' => $larrformData['IdBranch'],
				'CalculationMode' => $larrformData['CalculationMode'],
				'Amount' => $larrformData['Amount'],
				'SemesterFrom' => $SemesterFrom,
				'IntakeFrom' => $IntakeFrom,
				'SemesterTo' => $SemesterTo,
				'IntakeTo' => $IntakeTo,
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate'],
				'IdUniversity'=>$larrformData['IdUniversity'],
		);

		$this->lobjDbAdpt->insert('tbl_policysetup_initialpayment',$paramArray);
		$lastinsertID = $this->lobjDbAdpt->lastInsertId();

		if($larrformData['ProgramGrid']){
			$check = count($larrformData['ProgramGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'IdPolicySetup'=>$lastinsertID,
						'Program'=> $larrformData['ProgramGrid'][$i],
						'CalculationModeProgram'=> $larrformData['CalculationModeProgramGrid'][$i],
						'AmountByProgram'=>$larrformData['AmountByProgramGrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_policysetup_initialpayment_program',$paramArray);
			}

		}
	}

	public function fnSearchPolicySetup($post = array()) 
	{
	
		if ( $this->locale != 'ar_YE' )
		{
			$select = $this->lobjDbAdpt->select()
			->from(array("a" => "tbl_policysetup_initialpayment"), array("a.*"))
			->joinLeft(array('b'=>'tbl_intake'),'b.IdIntake = a.IntakeFrom',array('b.IntakeDesc'))
			->joinLeft(array('f'=>'tbl_definationms'),'f.idDefinition = a.Award   ',array('f.DefinitionDesc as AwardName'))
			->joinLeft(array('g'=>'tbl_definationms'),'g.idDefinition = a.StudentCategory',array('g.DefinitionDesc as StudentCategoryName'))
			->joinLeft(array('c'=>'tbl_scheme'),'c.IdScheme = a.Scheme',array('c.EnglishDescription'))
			->joinLeft(array('e'=>'tbl_branchofficevenue'),'a.IdBranch=e.IdBranch',array('e.BranchName'));
		
		}
		else
		{
			$select = $this->lobjDbAdpt->select()
			->from(array("a" => "tbl_policysetup_initialpayment"), array("a.*"))
			->joinLeft(array('b'=>'tbl_intake'),'b.IdIntake = a.IntakeFrom',array('b.IntakeDesc'))
			->joinLeft(array('f'=>'tbl_definationms'),'f.idDefinition = a.Award   ',array('f.BahasaIndonesia as AwardName'))
			->joinLeft(array('g'=>'tbl_definationms'),'g.idDefinition = a.StudentCategory',array('g.BahasaIndonesia as StudentCategoryName'))	
			->joinLeft(array('c'=>'tbl_scheme'),'c.IdScheme = a.Scheme',array('c.ArabicDescription as EnglishDescription'))
			->joinLeft(array('e'=>'tbl_branchofficevenue'),'a.IdBranch=e.IdBranch',array('e.Arabic as BranchName'));	
		}
		
	
		if( $post['IdBranch'] != '' )
		{
			$select = $select->where("a.IdBranch = ?",$post['IdBranch']);
		}
		
		if(isset($post['SchemeNot']) && !empty($post['SchemeNot']) ){
			$select = $select->where("a.Scheme = ?",$post['SchemeNot']);

		}
		
		if(isset($post['StudentCategory']) && !empty($post['StudentCategory']) ){
			$select = $select->where("a.StudentCategory = ?",$post['StudentCategory']);

		}
		
		if(isset($post['AwardNot']) && !empty($post['AwardNot']) ){
			$select = $select->where("a.Award = ?",$post['AwardNot']);

		}
		
		if(isset($post['ValueNot']) && !empty($post['ValueNot']) ){
			$select = $select->where("a.Value = ?",$post['ValueNot']);

		}
		
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function getpolicysetupById($id){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_policysetup_initialpayment"), array("a.*"))
		->joinLeft(array('b'=>'tbl_policysetup_initialpayment_program'),'b.IdPolicySetup = a.IdPolicySetup',array('b.*'))
		->joinLeft(array('c'=>'tbl_program'),'c.IdProgram = b.Program',array('c.ProgramName'))
		->where("a.IdPolicySetup = ?",$id);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;

	}

	public function updatepolicysetup($larrformData,$id){		
		$paramArray = array(
				'CalculationMode' => $larrformData['CalculationMode'],
				'Amount' => $larrformData['Amount'],
				'IdBranch'=>$larrformData['IdBranch'],
				'Currency'=>$larrformData['Currency']
		);

		$where_up = "  IdPolicySetup =' ".$id."' ";
		$this->lobjDbAdpt->update('tbl_policysetup_initialpayment',$paramArray, $where_up);

		if($larrformData['ProgramGrid']){
			//$this->lobjDbAdpt->delete('tbl_policysetup_initialpayment_program','IdPolicySetup='.$id);
			$check = count($larrformData['ProgramGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'IdPolicySetup'=>$id,
						'Program'=> $larrformData['ProgramGrid'][$i],
						'CalculationModeProgram'=> $larrformData['CalculationModeProgramGrid'][$i],
						'AmountByProgram'=>$larrformData['AmountByProgramGrid'][$i],
				);
				$this->lobjDbAdpt->insert('tbl_policysetup_initialpayment_program',$paramArray);
			}
		}
	}

	public function getfeecode(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_setup"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchall($select);

		return $result;

	}

	public function fncheckvalidation($larrformData){

		$strscheme = $larrformData['Scheme'];
		$strstudentcategory = $larrformData['StudentCategory'];
		$strvalue = $larrformData['Value'];
		$straward = $larrformData['Award'];

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_policysetup_initialpayment"), array("a.*"))
		->where("a.StudentCategory = ?",$strstudentcategory)
		->where("a.Value = ?",$strvalue)
		->where("a.Scheme = ?",$strscheme)
		->where("a.Award = ?",$straward);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function fngetintaketo($lstrintakefrom){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_intake"), array("a.*"))
		->where("a.IdIntake = ?",$lstrintakefrom);
		$result = $this->lobjDbAdpt->fetchrow($select);
		if($result['ApplicationStartDate']){
			$strstartdate = $result['ApplicationStartDate'];
			$select = $this->lobjDbAdpt->select()
			->from(array("b" => "tbl_intake"), array("key"=>"b.IdIntake","name"=>"b.IntakeDesc"))
			->where('b.ApplicationEndDate >=?',$strstartdate);
			$larrResult = $this->lobjDbAdpt->fetchAll($select);
			return $larrResult;

		}

	}
	
	public function getbranchlist()
	{
		if ( $this->locale == 'ar_YE' )
		{
			$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_branchofficevenue"), array("a.IdBranch","a.Arabic as BranchName","a.Active","a.Arabic"))
					->where("a.Active = ?",1)
					->where("a.IdType = ?",1);
		}
		else
		{
			$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_branchofficevenue"), array("a.IdBranch","a.BranchName","a.Active","a.Arabic"))
					->where("a.Active = ?",1)
					->where("a.IdType = ?",1);
		}
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}
}