<?php
class Studentfinance_Model_DbTable_InvoiceMain extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'invoice_main';
	protected $_primary = "id";
		
	/*public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>$this->_name))
					->where("im.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}*/
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('a'=>$this->_name))
						->join(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
						 ->joinLeft(array('cn' => 'credit_note'), "a.id = cn.cn_invoice_id and cn.cn_status ='A'", array('cn_id','cn_billing_no','cn_amount_main'=>'cn_amount'))
						->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=a.appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
						->joinLeft(array('cur' => 'tbl_currency'), 'a.currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
						->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.trans_id', array('st.at_pes_id'))
							->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = a.semester')
						->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId'))
						->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
						->where("a.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getDetail($id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
		->from(array('i'=>$this->_name))
		->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = i.semester')
		->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = i.currency_id')
		->where("i.id = ?", (int)$id);
	
		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getInvoiceFromReceipt($receipt_id){
	
		$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
		$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		//get distinct invoice for particular receipt
		$selectData = $db->select()
					->from(array('ir'=>'receipt_invoice'),array('invoice_id'=>'rcp_inv_invoice_id'))
					->where("ir.rcp_inv_rcp_id = ?", (int)$receipt_id)
					->group(array('rcp_inv_invoice_id'));
	
		$row = $db->fetchAll($selectData);
		
		$invoice_list = array();
		
		//get invoice
		foreach ($row as $inv){
			$select_invoice = $db->select()
							->from(array('i'=>$this->_name))
							->join(array('idtl'=>'invoice_detail'), 'idtl.invoice_main_id = i.id', array('fi_id','fee_item_description','amount','paid','balance','id_detail'=>'idtl.id','invoice_main_id'))
							->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = i.currency_id')
							->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = i.semester',array('semester'=>'SemesterMainName'))
							->where("i.id = ?", (int)$inv['invoice_id']);
			
			 $invoice = $db->fetchRow($select_invoice);

			 if($invoice){
			 	
			 	//receipt invoice
				$select_receipt_invoice = $db->select()
											->from(array('ri'=>'receipt_invoice'))
											
											->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = ri.rcp_inv_cur_id',array('cur_prefix_inv'=>'c.cur_symbol_prefix','cur_code_inv'=>'c.cur_code','cur_symbol_prefix'))
											->join(array('i'=>'invoice_main'), 'i.id = ri.rcp_inv_invoice_id', array('*'))
											->join(array('idtl'=>'invoice_detail'), 'idtl.id = ri.rcp_inv_invoice_dtl_id and idtl.invoice_main_id = i.id', array('fi_id','fee_item_description','amount','paid','balance','id_detail'=>'idtl.id','invoice_main_id'))
//											->joinLeft(array('d'=>'tbl_currency'), 'd.cur_id = idtl.cur_id',array('cur_prefix_inv'=>'d.cur_symbol_prefix','cur_code_inv'=>'d.cur_code'))
											->where('ri.rcp_inv_rcp_id = ?', $receipt_id)
											->where('ri.rcp_inv_invoice_id = ?', $invoice['id'])
											->order('ri.rcp_inv_rcp_no DESC');
				$invoice['receipt_invoice'] = $db->fetchAll($select_receipt_invoice);
				
				//subject
				$m=0;
				foreach ($invoice['receipt_invoice'] as &$fee_item){
					//invoice subject
					$subject = $invoiceSubjectDb->fetchAll( array('invoice_detail_id = ?'=>$fee_item['rcp_inv_invoice_dtl_id'], 'rcp_id=?' => $receipt_id) );
				
					
					if($subject){
						$fee_item['subject_paid'] = $subject->toArray();
						
//						$invoice['receipt_invoice'][$m]['subject']=$subject;
						
					}
					$m++;
				}
			 	
			 	$invoice_list[] = $invoice;
			 }
		}
//		echo "<pre>";
//		print_r($invoice_list);
		return $invoice_list;
	}
	
	
public function getInvoiceFromAdvance($receipt_id){
	
		$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
		$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		//get distinct invoice for particular receipt
		$selectData = $db->select()
					->from(array('ir'=>'advance_payment_detail'))
					->join(array('idtl'=>'advance_payment'), 'idtl.advpy_id = ir.advpydet_advpy_id', array('*'))
					->join(array('r'=>'receipt'), 'r.rcp_id = idtl.advpy_rcp_id', array('rcp_no'))
					->join(array('i'=>'invoice_main'), 'i.id = ir.advpydet_inv_id', array('*'))
					->join(array('idtld'=>'invoice_detail'), 'idtld.invoice_main_id = i.id and idtld.id = ir.advpydet_inv_det_id', array('fi_id','fee_item_description','amount','paid','balance','id_detail'=>'idtld.id','invoice_main_id'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = i.currency_id')
//					->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = i.semester',array('semester'=>'SemesterMainName'))
							
					->where("idtl.advpy_rcp_id = ?", (int)$receipt_id);
//					->group(array('advpy_id'));
	
		$row = $db->fetchAll($selectData);
		
//		$invoice_list = array();
//		$invoice_list = $row;
		//get invoice
		/*$n=0;
		foreach ($row as $inv){
			$select_invoice = $db->select()
							->from(array('i'=>$this->_name))
							->join(array('idtl'=>'invoice_detail'), 'idtl.invoice_main_id = i.id', array('fi_id','fee_item_description','amount','paid','balance','id_detail'=>'idtl.id','invoice_main_id'))
							->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = i.currency_id')
							->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = i.semester',array('semester'=>'SemesterMainName'))
							->where("i.id = ?", (int)$inv['advpydet_inv_id'])
							->where("idtl.id = ?", (int)$inv['advpydet_inv_det_id']);
			
			 $invoice = $db->fetchRow($select_invoice);

			 if($invoice){
			 	$row[$n]['invoice'] = $invoice;
			 }else{
			 	$row[$n]['invoice']= null;
			 }
			 $n++;
			 
		}*/
//		echo "<pre>";
//		print_r($invoice_list);
		return $row;
	}
	
	public function getInvoiceData($billing_no, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $selectData = $db->select()
					->from(array('im'=>$this->_name))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = im.currency_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where('im.id = ?', $billing_no);
					
		if($active){
			$selectData->where("im.status = 'A'");
		}

		$row = $db->fetchRow($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	public function getInvoiceDetailData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>'invoice_detail'))
					->joinLeft(array('a'=>'invoice_main'), 'a.id = im.invoice_main_id',array('bill_number','currency_id','bill_paid','bill_balance','IdStudentRegistration', 'a.invoice_date'))
					->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = im.id',array())
					->joinLeft(array('sb'=>'tbl_subjectmaster'), 'sb.IdSubject = ivs.subject_id',array('SubCode'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where('im.id = ?', $id);
					
		$row = $db->fetchRow($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	public function getAllInvoiceData($billing_no, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>$this->_name))
					->where('im.bill_number = ?', $billing_no);
					
		if($active){
			$selectData->where("im.status = 'A'");
		}

		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	public function getApplicantInvoiceData($appl_id, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>$this->_name))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = im.currency_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where('im.trans_id = ?', $appl_id)
					->orWhere('im.IdStudentRegistration = ?', $appl_id);
					
		if($active){
			$selectData->where("im.status IN ('A','W')");
		}
		$row = $db->fetchAll($selectData);
		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	public function getAllDataInvoice($appl_id, $active=false,$type=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>'invoice_detail'),array('amount','balance','paid','fee_item_description','idDetail'=>'im.id', 'idMain'=>'s.id'))
					->joinLeft(array('s'=>'invoice_main'), 's.id = im.invoice_main_id',array('*'))
					->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = s.id and ivs.invoice_detail_id = im.id',array())
					->joinLeft(array('sb'=>'tbl_subjectmaster'), 'sb.IdSubject = ivs.subject_id',array('SubCode'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = s.currency_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->group('im.id');
					
		if($type==1){
			$selectData->where('s.trans_id = ?', $appl_id);
		}elseif($type==2){
			$selectData->where('s.IdStudentRegistration = ?', $appl_id);
		}else{
			$selectData->where('s.IdStudentRegistration = ?', $appl_id);
		}	
		
		if($active){
			$selectData->where("s.status IN ('A','W')");
		}
		
//		echo $selectData;
		$row = $db->fetchAll($selectData);
		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}	
	
	public function getStudentInvoiceData($idStudentRegistration, $semester_id=null, $feeId=array()){
	  $db = Zend_Db_Table::getDefaultAdapter();
	  $selectData = $db->select()
	  ->from(array('im'=>$this->_name))
	  ->join(array('s'=>'invoice_detail'), 'im.id = s.invoice_main_id',array('*'))
	  ->join(array('c'=>'tbl_currency'), 'im.currency_id = c.cur_id',array('cur_code'))
	  ->where('im.IdStudentRegistration = ?', $idStudentRegistration)
	  ->where("im.status = 'A'");
	  
	  if($semester_id){
	    $selectData->where("im.semester = ?", $semester_id);
	  }
	  
	  if($feeId){
	    $selectData->where("s.fi_id IN (?)", $feeId);
	  }
	  $row = $db->fetchAll($selectData);
	
	  if(!$row){
	    return null;
	  }else{
	    return $row;
	  }
	
	}
	
	
	public function getStudentInvoiceDataDiscount($idStudentRegistration, $semester_id=null, $feeId=array(),$discId=0){
	  $db = Zend_Db_Table::getDefaultAdapter();
	  $selectData = $db->select()
	  ->from(array('im'=>$this->_name))
	  ->join(array('s'=>'invoice_detail'), 'im.id = s.invoice_main_id',array('*','id_detail'=>'s.id', 'amount_dtl'=>'s.amount'))
	  ->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = im.id and ivs.invoice_detail_id = s.id',array())
	  ->joinLeft(array('sb'=>'tbl_subjectmaster'), 'sb.IdSubject = ivs.subject_id',array('SubCode'))
	  ->join(array('c'=>'tbl_currency'), 'im.currency_id = c.cur_id',array('cur_code'))
	  ->where('im.IdStudentRegistration = ?', $idStudentRegistration)
	  ->where("im.status = 'A'");
	  
	  if($semester_id){
	    $selectData->where("im.semester = ?", $semester_id);
	  }
	  
	  if($feeId){
	    $selectData->where("s.fi_id IN (?)", $feeId);
	  }
	  $row = $db->fetchAll($selectData);
	  
	  $i=0;
	  $newrow = array();
	  $newrow = $row;
	  foreach($row as $key=>$row){
	  	$discountDB = new Studentfinance_Model_DbTable_DiscountNoteDetail();
	  	$checkDiscountExist = $discountDB->getInvoiceDiscountData($row['invoice_main_id'],$row['id_detail']);
	  	
	  	if($checkDiscountExist){
	  		unset($newrow[$i]);
	  	}else{
		  	$desc = $row['fee_item_description'];
		  	if($row['SubCode']){
		  		$desc = $row['SubCode'].' - '.$row['fee_item_description'];
		  	}
		  	
		  	$newrow[$i]['invoicedesc'] = $desc;
		  	
		  	$discTypeDB = new Studentfinance_Model_DbTable_DiscountType();
		  	$discRow = $discTypeDB->getFeeInfo($discId,$row['fi_id']);
		  	
		  	if($discRow['CalculationMode'] == 1){//amount
		  		$calcName = 'Amount';
		  		$discAmount = $discRow['Amount'];
		  	}elseif($discRow['CalculationMode'] == 2){
		  		$calcName = 'Percentage';
		  		$discAmount = $row['amount'] * ($discRow['Amount']/100);
		  	}
	  		$newrow[$i]['discountPercentage'] = $discRow['CalculationMode'];
	  		$newrow[$i]['discountPercentName'] = $calcName;
	  		$newrow[$i]['discountAmount'] = $discRow['Amount'];
	  		
	  		$newrow[$i]['AmountDisc'] = $discAmount;
		  	
		  	
	  	}
	  	$i++;
	  	
	  }
	  
	 $newrow = array_values($newrow);
	  if(!$newrow){
	    return null;
	  }else{
	    return $newrow;
	  }
	
	}
	
	
	public function getApplicantInvoiceBalanceAmount($appl_id, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('im'=>$this->_name))
		->where('im.appl_id = ?', $appl_id);
			
		if($active){
			$selectData->where("im.status = 'A'");
		}
	
		$rows = $db->fetchAll($selectData);
	
		if(!$rows){
			return null;
		}else{
			$balance = 0.00;
			foreach ($rows as $row){
				$balance += $row['bill_balance'];
			}
			
			return $balance;
		}
	
	}
	
	public function getFomulirInvoiceData($fomulir, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>$this->_name))
					->where('im.no_fomulir = ?', $fomulir);
					
		if($active){
			$selectData->where("im.status = 'A'");
		}

		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	public function getInvoiceFromApplId($appl_id, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('im'=>$this->_name))
		->where('im.appl_id = ?', $appl_id);
			
		if($active){
			$selectData->where("im.status = 'A'");
		}
	
		$row = $db->fetchAll($selectData);
	
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getIssuedInvoiceData($payee, $program_code=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>$this->_name))
					->where("im.no_fomulir = '".$payee."'");
					
		if($program_code!=null){
			$selectData->where('im.program_code =?',$program_code);
		}

		
		$row = $db->fetchAll($selectData);

		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	/**
	 * 
	 * Return boolean if found any proforma invoice not in invoice main table
	 * @param String $billing_no
	 */
	public function getProformaInvoiceNotInInvoice($billing_no,$payer_id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//select invoice
		/*$select_invoice = $db->select()
					->from(array('im'=>$this->_name), array('im.bill_number'))
					->where("im.bill_number = ?", $billing_no);
					
		//select proforma invoice
		$select_proforma = $db->select()
					->from(array('api'=>'applicant_proforma_invoice'))
					->where("api.billing_no = '".$billing_no."'")
					->where("api.billing_no not in (".$select_invoice.")");
	
		if($payer_id!=null){
			$select_proforma->where('api.payee_id = ?',$payer_id);
		}*/
		
		$select = $db->select()
					->from(array('api'=>'applicant_proforma_invoice'))
					->joinLeft(array('im'=>$this->_name), 'api.billing_no = im.bill_number')
					->where("im.bill_number is null")
					->where('api.billing_no = ?', $billing_no);
		
		//echo $select;
		//exit;
		$row = $db->fetchRow($select);
		
		if(!$row){
			return false;
		}else{
			return true;	
		}
		
	}

	/*
	 * Overite Insert function
	 */
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['creator'])){
			$data['creator'] = $auth->getIdentity()->iduser;
		}
		
		if( !isset($data['date_create'])  ){
			$data['date_create'] = date('Y-m-d H:i:s');
		}
				
		return parent::insert( $data );
	}
	
	public function generateApplicantInvoice($payer_id, $billing){
				
		
		try {
			
			//get proforma invoice
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('api'=>'applicant_proforma_invoice'))
					->where('api.payee_id = ?', $payer_id)
					->where('api.billing_no = ?', $billing);
			
			$row = $db->fetchAll($select);
			
			if($row!=null){
				//loop each invoice
				for($i=0; $i<sizeof($row); $i++){
					
					$proforma_invoice = $row[$i];
					
					//check not in invoice table or with status cancel 
					$select = $db->select()
						->from(array('api'=>'applicant_proforma_invoice'))
						->joinLeft(array('im'=>$this->_name), 'api.billing_no = im.bill_number')
						->where('api.billing_no = ?', $proforma_invoice['billing_no'])
						->where("im.bill_number is null or (im.bill_number is not null and im.status = 'X')");
						
					
					$row_invoice = $db->fetchRow($select);
					
					if($row_invoice){
						
						$date_invoice = null;
						if( isset($row_invoice['offer_date']) && $row_invoice['offer_date'] != '0000-00-00' ){
							$date_invoice = date('Y-m-d H:i:s', strtotime($row_invoice['offer_date']));
						}
						
						//get transaction data
						$transaction = $this->getTransaction($payer_id);
						
						//get selection rank
						$selection_rank = $this->getSelectionRank($transaction['at_trans_id'], $transaction['at_appl_type']);
						
						//get program data (code & college)
						$program = $this->getProgram($payer_id);
						
						//get fee structure
						$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
						if(!$this->islocalNationality($transaction['at_trans_id'])){
							//315 is foreigner in lookup db
							$fee_structure = $feeStructureDb->getApplicantFeeStructure($transaction['at_intake'],$program['IdProgram'],315);
							
						}else{
							//default to local
							$fee_structure = $feeStructureDb->getApplicantFeeStructure($transaction['at_intake'],$program['IdProgram']);
							
						}
						
						
						//get selected payment plan
						$paymentplanDb = new Studentfinance_Model_DbTable_FeeStructurePlan();
						$payment_plan = $paymentplanDb->getBillingPlan($fee_structure['fs_id'],$billing);
						
						//inject plan detail (installment)
						$paymentPlanDetailDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();
						$payment_plan['installment_detail'] = array();
						for($i=1;$i<=$payment_plan['fsp_bil_installment']; $i++){
							$payment_plan['installment_detail'][$i] = $paymentPlanDetailDb->getPlanData($fee_structure['fs_id'], $payment_plan['fsp_id'], $i, 1, $program['IdProgram'], $selection_rank );
							
							
						}
												
						//loop each cicilan
						for($i=1;$i<=$payment_plan['fsp_bil_installment']; $i++){
							set_time_limit(0);
							
							//total amount for each cicilan
							$total_invoice_amount = 0;
							foreach ($payment_plan['installment_detail'][$i] as $cicilan){
								$total_invoice_amount = $total_invoice_amount + $cicilan['total_amount'];
							}
							
							//desc cicilan
							$paket_info = "";
							if($payment_plan['fsp_bil_installment']==1){
								$paket_info = "Lunas";
							}else
							if($payment_plan['fsp_bil_installment']>1){
								$paket_info = "Cicilan ".$i;
							}
							
							//check for payment to calculate paid and balance
							$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
							$payment_record =  $paymentMainDb->getInvoicePaymentRecord($payment_plan['fsp_billing_no'].$i.$payer_id,$payer_id);

							//excess payment
							if( $payment_record!=null && $payment_record['amount'] > $total_invoice_amount ){
								
								$amount_paid = $total_invoice_amount;
								
								//insert main bill
								$data = array(
											'bill_number' => $payment_plan['fsp_billing_no'].$i.$payer_id,
											'appl_id' => $transaction['at_appl_id'],
											'no_fomulir' => $payer_id,
											'academic_year' => $transaction['at_academic_year'],
											//'semester' => '',
											'bill_amount' => $total_invoice_amount,
											'bill_paid' => $amount_paid,
											'bill_balance' => $total_invoice_amount - $amount_paid,
											'bill_description' => $program['ShortName']."-"."P".$selection_rank."-".$payment_plan['fsp_name']." ".$paket_info,
											'college_id' => $program['IdCollege'],
											'program_code' => $program['ProgramCode'],
											'creator' => '1',
											'fs_id' => $payment_plan['fsp_structure_id'],
											'fsp_id' => $payment_plan['fsp_id'],
											'status' => 'A',
											'date_create' => $date_invoice
										);
								
								$main_id = $this->insert($data);
								
								//insert bill detail
								$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
								foreach ($payment_plan['installment_detail'][$i] as $detail){
									
									$data_detail = array(
													'invoice_main_id' => $main_id,
													'fi_id' => $detail['fi_id'],
													'fee_item_description' => $detail['fi_name_bahasa'],
													'amount' => $detail['amount']
												);
									
									
									$invoiceDetailDb->insert($data_detail);
								}
								
								//advance payment
								$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					       		$adv_amount = $payment_record['amount'] - $total_invoice_amount;
					       		$data = array(
					       			'advpy_appl_id' => $transaction['at_appl_id'],
					       			'advpy_acad_year_id' => $transaction['at_academic_year'],
					       			'advpy_sem_id' => '',
					       			'advpy_prog_code' => $program['ProgramCode'],
					       			'advpy_fomulir' => $payer_id,
					       			'advpy_invoice_no' => $payment_plan['fsp_billing_no'].$i.$payer_id,
					       			'advpy_invoice_id' => $main_id,
					       			'advpy_payment_id' => $payment_record['id'],
					       			'advpy_description' => 'Excess Payment for invoice no:'.$payment_plan['fsp_billing_no'].$i.$payer_id,
					       			'advpy_amount' => $adv_amount,
					       			'advpy_total_paid' => 0,
					       			'advpy_total_balance' => $adv_amount,
					       			'advpy_status' => 'A'
					       		);
					       		$advancePaymentDb->insert($data);
					       		
							}else{
								
								$amount_paid = $payment_record['amount'];
								
								//insert main bill
								$data = array(
											'bill_number' => $payment_plan['fsp_billing_no'].$i.$payer_id,
											'appl_id' => $transaction['at_appl_id'],
											'no_fomulir' => $payer_id,
											'academic_year' => $transaction['at_academic_year'],
											//'semester' => '',
											'bill_amount' => $total_invoice_amount,
											'bill_paid' => $amount_paid,
											'bill_balance' => $total_invoice_amount - $amount_paid,
											'bill_description' => $program['ShortName']."-"."P".$selection_rank."-".$payment_plan['fsp_name']." ".$paket_info,
											'college_id' => $program['IdCollege'],
											'program_code' => $program['ProgramCode'],
											'creator' => '1',
											'fs_id' => $payment_plan['fsp_structure_id'],
											'fsp_id' => $payment_plan['fsp_id'],
											'status' => 'A',
											'date_create' => $date_invoice
										);
								
								$main_id = $this->insert($data);
								
								//insert bill detail
								$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
								foreach ($payment_plan['installment_detail'][$i] as $detail){
									
									$data_detail = array(
													'invoice_main_id' => $main_id,
													'fi_id' => $detail['fi_id'],
													'fee_item_description' => $detail['fi_name_bahasa'],
													'amount' => $detail['amount']
												);
									
									
									$invoiceDetailDb->insert($data_detail);
								}
							}
						}
						
						
						
					}
				}
			}
			
		}catch (Exception $e) {
			
			echo $e->getMessage();
			
			echo "<pre>";
			print_r($e->getTrace());
			echo "</pre>";
			
			throw new Exception('Error in Invoice Main');
		}
	}
	
	public function getMaxPaketNo($fomulir_no){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('im'=>$this->_name),array( 'paket' => new Zend_Db_Expr("MAX(SUBSTRING(bill_number,1,1))")))
					->where('im.no_fomulir = ?', $fomulir_no);

		$row = $db->fetchRow($selectData);

		if(!$row){
			return 1;
		}else{
			return $row[paket]+1;	
		}
		
	}
	
	private function getTransaction($payer_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//get application type
		$selectData = $db->select()
					->from(array('at'=>'applicant_transaction'))
					->where("at.at_pes_id = ?", $payer_id);
			
		$txn_row = $db->fetchRow($selectData);			

		if(!$txn_row){
			return null;
		}else{
			return $txn_row;
		}
	}
	
	private function getProgram($payer_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//get application type
		$selectData = $db->select()
					->from(array('at'=>'applicant_transaction'))
					->where("at.at_pes_id = ?", $payer_id);
							
		$txn_row = $db->fetchRow($selectData);			

		if($txn_row){
			
			$selectData = $db->select()
					->from(array('at'=>'applicant_transaction'),array())
					->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
					->join(array('p'=>'tbl_program'),'p.ProgramCode = ap.ap_prog_code')
					->where("at.at_pes_id = ?", $payer_id);
							
			if($txn_row['at_appl_type']==1){
				$selectData->where("ap.ap_usm_status = 1");			
			}
			
			$row = $db->fetchRow($selectData);

			if(!$row){
				return null;
			}else{
				return $row;
			}
		}else{
			return null;
		}	
	}
	
	private function getSelectionRank($txn_id, $application_type){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($application_type == 1){
			$selectData = $db->select()
					->from(array('aau'=>'applicant_assessment_usm'), array('rank'=>'aau.aau_rector_ranking'))
					->where("aau.aau_trans_id = ?", $txn_id);
		}else
		if($application_type == 2){
			$selectData = $db->select()
					->from(array('aa'=>'applicant_assessment'), array('rank'=>'aa.aar_rating_rector'))
					->where("aa.aar_trans_id = ?", $txn_id)
					->order('aa.aar_id desc');
		}
		
		$row = $db->fetchRow($selectData);			

		if(!$row){
			return 3;
		}else{
			return $row['rank'];
		}
	}
	
	private function islocalNationality($txn_id){
		//get profile
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
		->from(array('at'=>'applicant_transaction'))
		->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
		->where("at_trans_id = ".$txn_id);
	
		$row = $db->fetchRow($select);
	
		//nationality
		if( isset($row['appl_nationality']) ){
				
			if($row['appl_nationality']==96){
				return true;
			}else{
				return false;
			}
		}else{
			//default to local if null data
			return true;
		}
	}
	
	public function getTotalPaidInvoiceAmount($payer){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
		->from(array('im'=>'invoice_main'))
		->join(array('pi'=>'applicant_proforma_invoice'),'im.bill_number = pi.billing_no')
		->where("pi.payee_id ='".$payer."'");
		 
		$row = $db->fetchAll($select);
	
		if(!$row){
			return 0;
		}else{
			$total = 0;
			foreach ($row as $bil){
				$total = $total + $bil['bill_paid'];
			}
				
			return $total;
		}
	
	}
	
	public function getApplicantInvoice($payer){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
		->from(array('im'=>'invoice_main'))
		->join(array('pi'=>'applicant_proforma_invoice'),'im.bill_number = pi.billing_no')
		->where("pi.payee_id ='".$payer."'");
			
		$row = $db->fetchAll($select);
	
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getInvoicedProformaData($fomulir, $active=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('im'=>$this->_name))
					->join(array('pi'=>'applicant_proforma_invoice'),'pi.billing_no = im.bill_number', array())
					->where('im.no_fomulir = ?', $fomulir);
			
		if($active){
			$selectData->where("im.status = 'A'");
		}
	
		$row = $db->fetchAll($selectData);
	
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function updateTableData($table,$data,$where){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update($table,$data,$where);
	}
	
	public function getPaginateData($where=array())
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
	                ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = a.id',array())
	                ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id',array())
	                ->joinLeft(array('fc' => 'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id',array())
	                ->joinLeft(array('cn' => 'credit_note'), "a.id = cn.cn_invoice_id and cn.cn_status = 'A'", array('cn_id','cn_billing_no'))
					->join(array('cur' => 'tbl_currency'), 'a.currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = a.cancel_by', array())
					->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancelName'=>'tsa.FullName'))
					->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = a.upd_by', array())
					->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('updName'=>'tsb.FullName'))
					->joinLeft(array('rcv' => 'receipt_invoice'), 'rcv.rcp_inv_invoice_id = a.id', array())
					->joinLeft(array('rcp' => 'receipt'), "rcp.rcp_id = rcv.rcp_inv_rcp_id and rcp.rcp_status = 'APPROVE'", array('rcp_no'))
//					->joinLeft(array('advd' => 'advance_payment_detail'), 'advd.advpydet_inv_id = a.id and advd.advpydet_inv_det_id', array())
//					->joinLeft(array('adv' => 'advance_payment'), "adv.advpy_id = advd.advpydet_advpy_id and adv.advpy_status = 'A'", array('adv_no'=>'advpy_fomulir'))
					;
	
			if ( $where['type'] == 1 )
			{
				
				$select->join(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.trans_id', array('st.at_pes_id'))
					->join(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->join(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->join(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','applicant_program'=>'ProgramCode','ProgramCode'));
					//->where("a.trans_id is not null")
//					->where("a.trans_id is null");
					
				if ( $where['id'] ){
					$select->where("st.at_pes_id like ?",'%'.$where['id'].'%');
				}
				if ( $where['name']){
					$select->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$where['name'].'%');
				}

			}
			
			if ( $where['type'] == 2 )
			{
				
				$select->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId','at_pes_id'=>'sr.registrationId'))
					->join(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->join(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode','ProgramCode'));
//					->where("a.IdStudentRegistration is not null");
				if ($where['id']){
					$select->where("sr.registrationId like ?",'%'.$where['id'].'%');
				}
				if ($where['name']){
					$select->where("concat_ws(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname) like ?", '%'.$where['name'].'%');
				}
				

			}
			
			if ($where['invoice_id']){
				$select->where('a.bill_number LIKE ?', '%'.$where['invoice_id'].'%');
			}
			
			if ($where['status'] != ""){
				$select->where('a.status IN (?)', $where['status']);
			}
			
			if ($where['category'] != 0){
				$select->where('fc.fc_id = ?', $where['category']);
			}
			$select->group('a.id');
			$select->order('a.invoice_date asc');
	
//echo $select;
		return $select;
	}
	
	public function getOutstandingInvoiceReport($searchData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$semester = isset($searchData['semester'])?$searchData['semester']:0;
		$semester_id = isset($searchData['semester_id'])?$searchData['semester_id']:0;
		
		/*$selectData2 = $db->select()
					->from(array('a'=>'fee_structure_program_item'),array('fspi_fee_id'))
					->where("fspi_type = 'application'")
					->group('fspi_fee_id');
		$itemExcluded = $db->fetchAll($selectData2);*/
		
//		$itemExcluded = array(24,25,34,35,43,44,75); //exclude fee item from application/with proforma
		
		$db = Zend_Db_Table::getDefaultAdapter();
	// STUDENTS
				//registrationId, student_fullname, appl_email, appl_fullname, at_pes_id, dt_discount, sponsorship

		$students = $db->select()
					->from(array('ivd'=>'invoice_detail'),array('fi_id','idDetail'=>'ivd.id','balance','amount','cn_amount', new Zend_Db_Expr('str.registrationId, CONCAT_WS(\' \',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname, sa.appl_email, NULL as appl_fullname, 0 as at_pes_id'),'dcnt.dt_discount','CONCAT_WS(\' \',sptt.fName, sptt.lName) as sponsorship'))
					 ->join(array('a'=>'invoice_main'),"ivd.invoice_main_id = a.id ",array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','status','invMainID'=>'a.id','MigrateCode','not_include','currency_id','bill_description'))
					 
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
					->joinLeft(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration AND (a.IdStudentRegistration IS NOT NULL OR a.IdStudentRegistration != 0)',array())
					->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array())
					->joinLeft(array('pa'=>'tbl_program'),'str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
					
					->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
					->joinLeft(array('e'=>'tbl_program_scheme'), 'str.IdProgramScheme = e.IdProgramScheme',array())
					->joinLeft(array('sts'=>'tbl_definationms'), 'str.profileStatus = sts.idDefinition', array('profileStatus'=>'sts.DefinitionDesc'))
					->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
					->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
					->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
					->joinLeft(array('fi'=>'fee_item'),'ivd.fi_id = fi.fi_id',array('fi_name'))
					

					//sponsorship,sch_name,dt_discount
					->joinLeft(array('sp'=>'sponsor_application'), 'sp.sa_cust_id = str.IdStudentRegistration', array())
					->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = str.IdStudentRegistration', array())
					->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
					->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = str.IdStudentRegistration', array())
					->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array())
					->joinLeft(array('dcn'=>'discount_type_tag'), 'dcn.IdStudentRegistration=str.IdStudentRegistration', array())
					->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array())
					
					->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
					->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode'))
					->where('(str.IdStudentRegistration != 0 OR str.IdStudentRegistration IS NOT NULL)')
					->where("DATE(a.invoice_date) >= ?",$dateFrom)
					->where("DATE(a.invoice_date) <= ?",$dateTo)
					->where("a.status IN ('A','W')")
					->where("IFNULL(fi.fi_exclude,0) = 0") 
					->where("a.not_include = 0")
			//->where("str.IdStudentRegistration IN (787)")
//			->where("a.id IN (42351)")
					//->order(new Zend_Db_Expr('NULL'), '')
					->group('ivd.id');
		
		if($semester){
			$students->where('a.semester IN (?)', $semester);
		}
		
		if($semester_id){
			$students->where('a.semester IN (?)', $semester_id);
		}
		
		if($program != 'ALL'){
			$students->where('pa.IdProgram =?', $program);
		}

//echo $students;
		// APPLICANTS

		$applicants = $db->select()
					->from(array('ivd'=>'invoice_detail'),
					
					//registrationId, student_fullname, appl_email, appl_fullname, at_pes_id, dt_discount, sponsorship
					array('fi_id','idDetail'=>'ivd.id','balance','amount','cn_amount', new Zend_Db_Expr('NULL as registrationId, NULL as student_fullname, ap.appl_email,CONCAT_WS(\' \',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname, at.at_pes_id, NULL as dt_discount, NULL as sponsorship')))

					 ->join(array('a'=>'invoice_main'),"ivd.invoice_main_id = a.id ",array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','status','invMainID'=>'a.id','MigrateCode','not_include','currency_id','bill_description'))
					 
					
					 ->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
					 ->joinLeft(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					 ->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.trans_id',array())
					 ->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array())
					 ->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
					  
					 ->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						
					 ->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
					 ->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme ',array())
					 ->joinLeft(array('sts'=>'tbl_definationms'), 'sts.idDefinition=at.at_status', array('profileStatus'=>'sts.DefinitionDesc'))
			         ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			         ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			         ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			         ->joinLeft(array('fi'=>'fee_item'),'ivd.fi_id = fi.fi_id',array('fi_name'))
					
					->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_trans_id = at.at_trans_id', array())
					->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
					
			         
					 ->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
			         ->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode'))
					 ->where("DATE(a.invoice_date) >= ?",$dateFrom)
					 ->where("DATE(a.invoice_date) <= ?",$dateTo)
					 ->where("a.status IN ('A','W')")
					 ->where('( a.IdStudentRegistration = 0 OR a.IdStudentRegistration IS NULL )')
//->where("a.IdStudentRegistration IN (787)")
//->where("a.trans_id IN (2173)")

					->where("IFNULL(fi.fi_exclude,0) = 0") 
					->where("a.not_include = 0") 
					//->order('a.invoice_date asc')
					//->order('a.bill_number')
					//->order(new Zend_Db_Expr('NULL'), '')
					->group('ivd.id');

		if($semester){
			$applicants->where('a.semester IN (?)', $semester);
		}
		
		if($semester_id){
			$applicants->where('a.semester IN (?)', $semester_id);
		}
		
		if($program != 'ALL'){
			$applicants->where('pa.IdProgram =?', $program);
		}
		
	
		$selectAll = $db->select()->union(array( $applicants,$students), Zend_Db_Select::SQL_UNION_ALL )->order('invoice_date asc');//new Zend_Db_Expr('NULL'), ''

		if(!$selectAll){
			return null;
		}else{
			return $db->fetchAll($selectAll);	
		}
		
	}
	
public function getOutstandingAmountByDays($searchData=null){
	
//	print_r($searchData);
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program_id'];
		$min = $searchData['min'];
		$max = $searchData['max'];
		
		$selectData = $db->select()
					->from(array('ivd'=>'invoice_detail'),array('balance','id','invoice_main_id','amount','cn_amount'))
					->join(array('a'=>'invoice_main'),'ivd.invoice_main_id = a.id',array('invoice_date','currency_id','exchange_rate','status','MigrateCode','IdStudentRegistration'))
					->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.trans_id',array())
					->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array())
					->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
					->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration AND (a.IdStudentRegistration IS NOT NULL OR a.IdStudentRegistration != 0)',array())
					->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array())
					->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array())
						
						
					/* ->from(array('a'=>'invoice_main'),array('invoice_date','currency_id','exchange_rate'))
					 ->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id = a.id',array('balance'))*/
					 
					->where("DATE(a.invoice_date) >= ?",$dateFrom)
					->where("DATE(a.invoice_date) <= ?",$dateTo)
					->where("a.not_include = 0")
					->where("a.status IN ('A','W')")
					->where("ivd.fi_id = ?",$searchData['fi_id'])
					->order('a.invoice_date asc')
					->group('ivd.id');
					
		if($searchData['semester']){
			$selectData->where('a.semester IN (?)', $searchData['semester']);
		}
		
		if($searchData['semester_id']){
			$selectData->where('a.semester IN (?)', $searchData['semester_id']);
		}
		
		if($program != 'ALL'){
			$selectData->where('pa.IdProgram =?', $program);
		}
//			echo $selectData;		
		$row = $db->fetchAll($selectData);
		
		$newArray = array();
		$amountAll = 0;
		$i=0;
		foreach($row as $key=>$data){
			
			if($data['balance'] == 0){
				unset($row[$key]);
			}
			
			
			$newbalance = 0;
    		//get receipt_invoice
	    	$receiptInvDB = new Studentfinance_Model_DbTable_ReceiptInvoice();
			$searchRecInv = array(
			    	'date_from'=>$dateFrom,
			    	'date_to'=>$dateTo,
			    	'invoice_id'=>$data['invoice_main_id'],
			    	'invoice_detail_id'=>$data['id']
		    	);
		    	
	    	$receiptInvData = $receiptInvDB->getOutstandingByReceiptInv($searchRecInv);
	    	
//	    	echo "<pre>";
//	    	print_r($receiptInvData);
			$paidInvAll = 0;
	    	if($receiptInvData){
		    	
		    	foreach($receiptInvData as $rcp){
		    		$paidInv = $rcp['rcp_inv_amount'];
			    	if($rcp['rcp_inv_cur_id'] == 2){ //usd
						$currencyRate = $currencyRateDB->getData($rcp['exchange_rate']);
						$paidInv = $paidInv * $currencyRate['cr_exchange_rate'];
			    	}
		    	
		    		$paidInvAll += $paidInv;
		    		
		    	}
	    	}
	    	
	    	$amount = $data['amount'];
	    	$cnamount = $data['cn_amount'];
	    	
    		if($data['currency_id'] == 2){ //usd
				$currencyRate = $currencyRateDB->getData($data['exchange_rate']);
				$amount =  $amount * $currencyRate['cr_exchange_rate'];
				$cnamount =  $cnamount * $currencyRate['cr_exchange_rate'];
	    	}
	    	
	    	$amount = $amount - $cnamount;
	    	
	    	$newbalance = $amount - $paidInvAll;
	    	
	    	if($data['balance'] == 0){
	    		//do nothing
	    	}else{
		    	if($newbalance > '0'){
			    	
			    	$days = (strtotime( $searchData['date_to']) - strtotime($data['invoice_date'])) / (60 * 60 * 24);
					if($days >= $min && $days <= $max  ){
						
//						echo '<br>studid = '.$data['IdStudentRegistration'].' c '.$searchData['fi_id'].' n '.$data['id'].' m '.$newbalance.' = '.$amount.'-'.$cnamount.'-'.$paidInvAll.'+min'.$min.'='.$max.'='.$days.'+<br>';
			    	
						$amountAll += $newbalance;
					}
		    	}
	    	}
		
			$i++;
			
		}
		
		return $amountAll;	
		
	}
	
public function getInvoiceReport($searchData=null){
		//echo 'sama';
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$program = $searchData['program'];
		$intake = isset($searchData['intake']) ? $searchData['intake'] : 0;
		$semester = isset($searchData['semester']) ? $searchData['semester'] : 0;
		$intake_id = isset($searchData['intake_id']) ? $searchData['intake_id'] : 0;
		$semester_id = isset($searchData['semester_id']) ? $searchData['semester_id'] : 0;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ivd'=>'invoice_detail'),array('fi_id','balance','amount','idDetail'=>'ivd.id'))
					 ->join(array('a'=>'invoice_main'),'ivd.invoice_main_id = a.id',array('curidmain'=>'a.currency_id', 'invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','a.invoice_date','bill_balance','exchange_rate','bill_amount','statusInv'=>'a.status','invMainID'=>'a.id','MigrateCode','bill_description'))
					 ->join(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
					 ->join(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
					
					 ->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.trans_id',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration AND (a.IdStudentRegistration IS NOT NULL OR a.IdStudentRegistration != 0)',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->join(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
						->join(array('ti'=>'tbl_intake'),'ti.IdIntake = str.IdIntake OR ti.IdIntake = at.at_intake',array('IntakeDesc'))
						->join(array('e'=>'tbl_program_scheme'), 'str.IdProgramScheme = e.IdProgramScheme OR app.ap_prog_scheme = e.IdProgramScheme ',array())
						->joinLeft(array('sts'=>'tbl_definationms'), 'str.profileStatus = sts.idDefinition OR sts.idDefinition=at.at_status', array('profileStatus'=>'sts.DefinitionDesc'))
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
			            ->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = ivd.fi_id',array('fi_name'))
			             ->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id',array('fc_desc'))
					 	->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fi.fi_ac_id',array('ac_code','ac_desc'))
					 	
					 	->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = a.IdStudentRegistration', array())
			            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
			            ->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = a.IdStudentRegistration', array())
			            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
			            
//			            ->joinLeft(array('dcn'=>'discount_type_tag'), 'dcn.IdStudentRegistration=a.IdStudentRegistration', array('dtt_start_date',"dtt_end_date"=>"IFNULL(dtt_end_date,'0000-00-00')"))
//			            ->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array('dt_discount'))
			             
			            ->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
			            ->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode'))
//			            ->joinLeft(array('scr'=>'tbl_studentregsubjects'), 'scr.IdStudentRegistration = a.IdStudentRegistration and scr.IdSubject = ivs.subject_id',array('Active'))
//			            ->joinLeft(array('scrd'=>'tbl_studentregsubjects_history'), 'scrd.IdStudentRegistration = a.IdStudentRegistration and scrd.IdSubject = ivs.subject_id and scrd.Active = 2',array('Drop'=>'Active'))
			            ->joinLeft(array('cn'=>'credit_note_detail'), "cn.invoice_main_id = a.id and cn.invoice_detail_id = ivd.id",array('cn_cur_id'=>'cur_id','cn_amount'=>'amount'))
			            ->joinLeft(array('cna'=>'credit_note'), "cna.cn_id = cn.cn_id and cna.cn_status = 'A'",array('cn_type','type_amount'))
			            ->joinLeft(array('dcntt'=>'discount_detail'), "dcntt.dcnt_invoice_id = a.id and dcntt.dcnt_invoice_det_id = ivd.id",array('dcnt_amount'))
			            ->joinLeft(array('dcnn'=>'discount'), "dcnn.dcnt_id = dcntt.dcnt_id and dcnn.dcnt_status = 'A'",array())
						->joinLeft(array('ctr1'=>'tbl_countries'), 'ap.appl_nationality = ctr1.idCountry', array('apctr'=>'CountryName'))
						->joinLeft(array('ctr2'=>'tbl_countries'), 'sa.appl_nationality = ctr2.idCountry', array('sactr'=>'CountryName'))
						->joinLeft(array('df1'=>'tbl_definationms'), 'ap.appl_category = df1.idDefinition', array('apstdctgy'=>'DefinitionDesc'))
						->joinLeft(array('df2'=>'tbl_definationms'), 'sa.appl_category = df2.idDefinition', array('sastdctgy'=>'DefinitionDesc'))
					//->where("DATE(a.invoice_date) >= ?",$dateFrom)
					//->where("DATE(a.invoice_date) <= ?",$dateTo)
					->where("(((DATE(a.invoice_date) >= '".$dateFrom."') AND (DATE(a.invoice_date) <= '".$dateTo."')) OR ((DATE(cna.cn_create_date) >= '".$dateFrom."') AND (DATE(cna.cn_create_date) <= '".$dateTo."')) OR ((DATE(dcnn.dcnt_approve_date) >= '".$dateFrom."') AND (DATE(dcnn.dcnt_approve_date) <= '".$dateTo."')))")
					//->where("(DATE(cna.cn_approve_date) >= ".$dateFrom.") AND (DATE(cna.cn_approve_date) <= ".$dateTo.")")
					->where("a.status IN ('A','W')")
					->order('a.invoice_date asc')
					//->where('a.bill_number = "IN14/02286"')//,3698,3572,4029
					//->where('a.IdStudentRegistration = ?', 4642)//,3698,3572,4029
					->order('a.bill_number')
					->group('ivd.id');
//					->limit(200);

		if($intake && $semester){
			$selectData->where('ti.IdIntake IN ('.implode(',', $intake).') OR a.semester IN ('.implode(',', $semester).')');
		}

		if($intake && !$semester){
			$selectData->where('ti.IdIntake IN (?)', $intake);
		}

		if(!$intake && $semester){
			$selectData->where('a.semester IN (?)', $semester);
		}

		if($intake_id && $semester_id){
			$selectData->where('ti.IdIntake IN ('.implode(',', $intake_id).') OR a.semester IN ('.implode(',', $semester_id).')');
		}

		if($intake_id && !$semester_id){
			$selectData->where('ti.IdIntake IN (?)', $intake_id);
		}

		if(!$intake_id && $semester_id){
			$selectData->where('a.semester IN (?)', $semester_id);
		}
		
		/*if($intake_id){
			$selectData->where('ti.IdIntake IN (?)', $intake_id);
		}*/
		
		/*if($semester){
			$selectData->where('a.semester IN (?)', $semester);
		}*/
		
		/*if($semester_id){
			$selectData->where('a.semester IN (?)', $semester_id);
		}*/
		
		if($program != 'ALL'){
			$selectData->where('pa.IdProgram =?', $program);
		}
			//echo $selectData;	exit;
		$row = $db->fetchAll($selectData);
	
		if(!$row){
			return null;
		}else{
			return $row;	
		}
		
	}
	
	
public function getStudentInvoiceDataFinancialAid($type,$typeid,$idStudentRegistration, $semester_id=null, $feeId=array(),$invoiceId=array(),$receiptCurrency=0){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  $selectData = $db->select()
	  ->from(array('im'=>$this->_name), array('*', 'id_main'=>'im.id'))
	  ->join(array('s'=>'invoice_detail'), 'im.id = s.invoice_main_id',array('*','id_detail'=>'s.id'))
	  ->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = im.id and ivs.invoice_detail_id = s.id',array())
	  ->joinLeft(array('sb'=>'tbl_subjectmaster'), 'sb.IdSubject = ivs.subject_id',array('SubCode','IdSubject'))
	  ->join(array('c'=>'tbl_currency'), 'im.currency_id = c.cur_id',array('cur_code'))
	  ->join(array('fi'=>'fee_item'), 'fi.fi_id = s.fi_id',array('fi_name','fi_code'))
	  ->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = im.semester',array('SemesterMainName','SemesterMainCode'))
	  ->joinLeft(array('reg'=>'tbl_studentregistration'), 'reg.IdStudentRegistration = im.IdStudentRegistration',array('IdIntake'))
	  ->where('im.IdStudentRegistration = ?', $idStudentRegistration)
	  ->where("im.status = 'A' or im.status = 'W'")
	  ->where("im.sp_id = 0")
	  //->where('s.balance > 0')
	  ->order('im.invoice_date asc');
//	  ->where('im.IdStudentRegistration = 3596');
	  
	  /*if($semester_id){
	    $selectData->where("im.semester = ?", $semester_id);
	  }*/
	  
 	  if($feeId){
	    $selectData->where("s.fi_id IN (?)", $feeId);
	  }
	  
	  if($invoiceId){
	    $selectData->where("s.id IN (?)", $invoiceId);
	  }

	  $row = $db->fetchAll($selectData);

//	  echo "<pre>";
//	  print_r($row);

	  if($row){
		
	
		  $newrow = array();
		 
		  foreach($row as $key =>$row){
		  	
		  	$i = $row['id_detail'];
		  	$newrow[$i] = $row;
		  	$desc = '';
		  	$desc .= ($row['SubCode']) ? $row['SubCode'].' - ' : ' ';
		  	$desc .= $row['fi_name'];
		  	
		  	$newrow[$i]['invoicedesc'] = $desc;
		  	
		  	if($type == 1){
		  		$sponsorDB = new Studentfinance_Model_DbTable_Sponsor();
		  		$discRow = $sponsorDB->getFeeInfo($typeid,$row['fi_id']);
		  	}elseif($type == 2){
		  		$scholarDB = new Studentfinance_Model_DbTable_Scholarship();
		  		$discRow = $scholarDB->getFeeInfo($typeid,$row['fi_id']);
		  	}

		  	$newrow[$i]['FreqMode'] = $discRow['FreqMode'];

		  	//checking repeat

		  	if($row['IdSubject']){
		  		if($discRow['Repeat'] == 1){ //Yes
		  		/*TO confirm:kena check max repeat by subject/overall
		  		 * check subject repeat
		  		 * check maximum repeat number
		  		 */

			  		$repeatData = $this->checkRepeatSubject($idStudentRegistration,$row['semester'],$row['IdSubject']);
			  		$newrow[$i]['repeatSubject'] = $repeatData;
//			  		echo "<pre>";
//			  		print_r($repeatData);
					if ($row['tag_to_open_payment']==0) {
						if ($repeatData['repeat'] == 1 && $repeatData['totalRepeat'] > $discRow['MaxRepeat']) {
							unset($newrow[$i]);
						}
					}
		  		}

		  	}
		  	
		  	
		  	
		  	$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
		  	$dataRate = $currencyRateDb->getCurrentExchangeRate(2);
		  	
		  	if($discRow){
		  		$discAmount = isset($discRow['Amount'])?$discRow['Amount']:0;
		  		$discAmountConvert = $discAmount;
			  	/*
			  	 * disabled calculation 18/8/2015 - use invoice balance
			  	 * if($discRow['CalculationMode'] == 1){//amount
			  		$calcName = 'Amount';
			  		$discAmount = isset($discRow['Amount'])?$discRow['Amount']:0;
			  		
			  		if($receiptCurrency){
				  		if($receiptCurrency > $row['currency_id']){ // 2=1
				  			$discAmountConvert = number_format($discAmount / $dataRate['cr_exchange_rate'],2);
				  		}else if($receiptCurrency < $row['currency_id']){
				  			$discAmountConvert = number_format($discAmount * $dataRate['cr_exchange_rate'],2);
				  		}else{
				  			$discAmountConvert = $discAmount;
				  		}
			  		}
			  		
			  	}elseif($discRow['CalculationMode'] == 2){
			  		$calcName = 'Percentage';
			  		$discAmount = $row['balance'] * ($discRow['Amount']/100);
			  		$discAmountConvert = $discAmount;
			  		
			  		if($receiptCurrency){
				  		if($receiptCurrency > $row['currency_id']){
				  			$discAmountConvert = number_format($discAmount / $dataRate['cr_exchange_rate'],2);
				  		}else if($receiptCurrency < $row['currency_id']){
				  			$discAmountConvert = number_format($discAmount * $dataRate['cr_exchange_rate'],2);
				  		}else{
				  			$discAmountConvert = $discAmount;
				  		}
			  		}
			  	}*/
		  	}
		  	
//	  		$newrow[$i]['percentage'] = isset($discRow['CalculationMode'])?$discRow['CalculationMode']:0;
//	  		$newrow[$i]['percentName'] = isset($calcName)?$calcName:0;
			//$newrow[$i]['SemesterFirst'] = $semesterData['IdSemesterMaster'];
	  		$newrow[$i]['Amount'] = isset($discRow['Amount'])?$discRow['Amount']:0;
	  		$newrow[$i]['AmountInvoice'] = isset($discAmount)?$discAmount:0;
	  		$newrow[$i]['AmountInvoiceConvert'] = isset($discAmountConvert)?$discAmountConvert:0;

			  /*if($newrow[$i]['semester'] != $semesterData['IdSemesterMaster']){ tak sure perlu ketak sebab dah unset kat atas ???????????
				  unset($newrow[$i]);
			  }*/
	  		
	  		
	  		//checking invoice aiready paid by sponsor or not
		  		$spInvoiceMainDB = new Studentfinance_Model_DbTable_SponsorInvoice();
		  		$checkInv = $spInvoiceMainDB->getInvoiceSponsor($row['id_detail']);
//		  		$newrow[$i]['lala'] = $checkInv;
				  		
				  		if(count($checkInv) > 1){
//				  			unset($newrow[$i]); disable 14/08/2015 sbb mmg amik invoice yang ada balance je.
				  		}
//		  	$i++;

			  //checking frequency mode
			  if($discRow['FreqMode'] == 1){ // one time/1st semester only
				  /*
		  		 * get 1st semester student
		  		 * get only invoice generated for 1st semester only
		  		 */
				  //$semesterData = $this->getFirstSemesterStudent($idStudentRegistration);
				  $semesterData = $this->getIntake($row['IdIntake']);
				  $semesterData2 = $this->getSemesterById($row['semester']);

				  //if($row['semester'] != $semesterData['IdSemesterMaster']){
				  if ($row['tag_to_open_payment']==0) {
					  if ($semesterData2['AcademicYear'] != $semesterData['sem_year'] || $semesterData2['sem_seq'] != $semesterData['sem_seq']) {
						  //echo 'huhu';
						  unset($newrow[$i]);
					  }
				  }
			  }else{

			  }

			  $pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
			  $balance = $pamodel->getBalanceDtl($row['id_detail']);

			  //$newrow[$i]['bill_balance'] = $balance['bill_balance'];
			  if (isset($newrow[$i]) && $newrow[$i] != null) {
				  $newrow[$i]['balance'] = $balance['bill_balance'];
				  //var_dump($newrow[$i]);
				  //var_dump($balance);

			  }
			  if ($row['tag_to_open_payment']==0) {
				  if ($balance['bill_balance'] <= 0) {
					  unset($newrow[$i]);
				  }
			  }
		  }
//		  $newrow = array_values($newrow);
	  }else{
	  	$newrow = null;
	  }
//	  echo "<pre>";
//	  print_r($row);
	//var_dump($newrow);
//
//	echo "<pre>";
//	print_r($newrow);
//	  exit;
	  if(!$newrow){
	    return null;
	  }else{
	    return $newrow;
	  }
	
	}
	
	public function getFirstSemesterStudent($studentId){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
			->from(array('a'=>'tbl_studentsemesterstatus'))
			->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = a.IdSemesterMain')
			->where("a.IdStudentRegistration =?",$studentId)
			->where('a.studentsemesterstatus = 130')
			->order('s.SemesterMainStartDate asc')
			->limit(1);

		$row = $db->fetchRow($select);
	
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}

	public function getIntake($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_intake'))
			->where('a.IdIntake = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function getSemesterById($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
			->from(array('a'=>'tbl_semestermaster'))
			->where("a.IdSemesterMaster = ?", $id)
			->limit(1);

		$row = $db->fetchRow($select);

		if(!$row){
			return null;
		}else{
			return $row;
		}

	}
	
	public function checkRepeatSubject($studentId,$semesterID,$subjectID){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
			->from(array('a'=>'tbl_studentregsubjects'))
			->where("a.IdStudentRegistration =?",$studentId)
			->where('a.Active = 4');
			
		$row = $db->fetchAll($select);

		$select2 = $db ->select()
			->from(array('a'=>'tbl_studentregsubjects'))
			->where("a.IdStudentRegistration =?",$studentId)
			->where("a.IdSemesterMain =?",$semesterID)
			->where("a.IdSubject =?",$subjectID)
			->where('a.Active = 4');
	
		$row2 = $db->fetchRow($select2);
		$repeat=0;
		if($row2){
			$repeat =1;
		}
		
		if(!$row){
			return null;
		}else{
			return array('repeat'=>$repeat,'totalRepeat'=>count($row));
		}
	
	}

	public function getSponsorPayment($searchData){
		$invoiceID = $searchData['invoice_id'];
		$invoiceDetailID = $searchData['invoice_detail_id'];

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'sponsor_invoice_detail'), array('sponsorpaid'=>'sum(a.sp_amount)'))
			->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
			->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
			->where('a.sp_invoice_det_id = ?', $invoiceDetailID)
			->where('a.sp_invoice_id = ?', $invoiceID)
			->where('a.sp_status = ?', 1)
			->where('c.rcp_status = ?', 'APPROVE');

		if (isset($searchData['date_from']) && $searchData['date_from']!=''){
			$select->where('c.rcp_receive_date >= ?', date('Y-m-d', strtotime($searchData['date_from'])));
		}
		if (isset($searchData['date_to']) && $searchData['date_to']!=''){
			$select->where('c.rcp_receive_date <= ?', date('Y-m-d', strtotime($searchData['date_to'])));
		}

		$result = $db->fetchAll($select);

		if ($result == null){
			$result[0]['sponsorpaid'] = 0.00;
		}

		return $result;
	}

	public function getSponsorPaymentStudent($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'sponsor_invoice_receipt_student'), array('value'=>'*'))
			->where('a.rcp_inv_id = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function getSponsorPaymentReceipt($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'sponsor_invoice_receipt'), array('value'=>'*'))
			->joinLeft(array('b'=>'tbl_currency'), 'b.cur_id = a.rcp_cur_id')
			->where('a.rcp_id = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}

	public function getSponsorPaymentInvoice($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'sponsor_invoice_receipt_invoice'), array('value'=>'*'))
			->join(array('b'=>'sponsor_invoice_detail'), 'b.id = rcp_inv_sponsor_dtl_id')
			->join(array('c'=>'invoice_main'), 'c.id = b.sp_invoice_id')
			->where('a.rcp_inv_student = ?', $id);

		$result = $db->fetchAll($select);
		return $result;
	}

	public function checkAbsentWithValidReason($invId){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
			->join(array('b'=>'tbl_studentregsubjects'), 'a.regsub_id = b.IdStudentRegSubjects')
			->join(array('c'=>'exam_registration'), 'b.IdSemesterMain = c.er_idSemester AND b.IdStudentRegistration = c.er_idStudentRegistration AND b.IdSubject = c.er_idSubject')
			->where('a.invoice_id = ?', $invId)
			->where('c.er_attendance_status = ?', 396);

		$result = $db->fetchAll($select);
		return $result;
	}
}
?>