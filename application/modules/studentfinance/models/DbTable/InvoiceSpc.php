<?php
/**
 *  @author alif 
 *  @date Jan 15, 2014
 */
 
class Studentfinance_Model_DbTable_InvoiceSpc extends Zend_Db_Table {

  protected $_name = 'invoice_spc';

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  public function getData($id=""){

    $db = $this->lobjDbAdpt;

    $select = $db ->select()
    ->from($this->_name)
    ->order("id desc");
    	
    if($id)	{
      $select->where("id ='".$id."'");
      $row = $db->fetchRow($select);
    }else{
      $row = $db->fetchAll($select);
    }

    if($row){
      return $row;
    }else{
      return null;
    }

  }
  
  public function getSpcByInvoiceBatch($batch_id){
    $db = $this->lobjDbAdpt;
    
    $select = $db ->select()
                ->from(array('spc'=>$this->_name))
                ->join(array('ibd'=>'invoice_batch_detail'), 'ibd.invoice_id = spc.invoice_id and ibd.invoice_batch_id = '.$batch_id, array())
                ->order("spc.id desc");
    
    $row = $db->fetchAll($select);
    
    if($row){
      return $row;
    }else{
      return null;
    }
    
    
  }
}
 ?>