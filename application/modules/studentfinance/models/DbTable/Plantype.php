<?php
class Studentfinance_Model_DbTable_Plantype extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_plantype';
	
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnAddPlanType($larrformData) {
		$this->insert($larrformData);
	}
	
	public function fnSearchPlanType($larrformData) {
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	$lstrSelect = $lobjDbAdpt->select()
       							 ->from(array("sa"=>"tbl_plantype"),array("sa.Planname AS Planname","sa.Description AS Description","sa.IdPlantype"))
								->where('sa.Description like "%" ? "%"',$larrformData['field2'])
								->where('sa.Planname  like "%" ? "%"',$larrformData['field3'])
								->where('sa.Active = ?',$larrformData['field7']);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	public function fnGetSemesterStudentStatusList(){
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       				->from(array("sa"=>"tbl_plantype"),array("sa.Planname AS Planname","sa.Description AS Description","sa.IdPlantype As IdPlantype"))
       				 ->order("sa.Planname");				
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fnEditPlanType($IdPlantype){
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       				->from(array("sa"=>"tbl_plantype"),array("sa.*"))
       				->where('sa.IdPlantype = ?',$IdPlantype);				
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
    public function fnupdatePlanType($IdPlantype,$larrformData) { //Function for updating the user   
		$where = 'IdPlantype = '.$IdPlantype;
		$this->update($larrformData,$where);
    }
}