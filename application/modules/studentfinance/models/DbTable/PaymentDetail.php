<?php
class Studentfinance_Model_DbTable_PaymentDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'payment_detail';
	protected $_primary = "id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('pD'=>$this->_name))
					->where("pD.id ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}

}
?>