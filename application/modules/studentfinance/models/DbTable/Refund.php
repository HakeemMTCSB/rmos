<?php
class Studentfinance_Model_DbTable_Refund extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'refund';
	protected $_primary = "rfd_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('adv' => 'advance_payment'), 'adv.advpy_id = cn.rfd_avdpy_id')
					->joinLeft(array('rfdd' => 'refund_detail'), 'rfdd.rfdd_refund_id = cn.rfd_id')
					->joinLeft(array('cur' => 'tbl_currency'), 'cn.rfd_currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=cn.rfd_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=cn.rfd_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->where("cn.rfd_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getApplicantRefund($applicant_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('r'=>$this->_name))
					->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = r.rfd_appl_id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = r.rfd_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creator_name'=>'Fullname'))
					->joinLeft(array('u2'=>'tbl_user'), 'u2.iduser = r.rfd_approver_id', array())
					->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = u2.IdStaff', array('approver_name'=>'Fullname'))
					->joinLeft(array('rc'=>'refund_cheque'), 'rc.rchq_id = r.rdf_refund_cheque_id')
					->where("r.rfd_appl_id = ?", (int)$applicant_id);

		$row = $db->fetchAll($selectData);
						
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getPaginateData($process=false){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('r'=>$this->_name))
					->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = r.rfd_appl_id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = r.rfd_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creator_name'=>'Fullname'))
					->joinLeft(array('u2'=>'tbl_user'), 'u2.iduser = r.rfd_approver_id', array())
					->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = u2.IdStaff', array('approver_name'=>'Fullname'))
					->joinLeft(array('rc'=>'refund_cheque'), 'rc.rchq_id = r.rdf_refund_cheque_id');
					
		if(isset($process) && $process==true){
			$selectData->where("rfd_approver_id is not null");
			$selectData->where("rfd_approve_date is not null");	
		}else{
			$selectData->where("rfd_approver_id is null");
			$selectData->where("rfd_approve_date is null");	
		}
		
		return $selectData;
	}

	/*
	 * Overite Insert function
	 */
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['rfd_creator'])){
			$data['rfd_creator'] = $auth->getIdentity()->iduser;
		}
		
		$data['rfd_create_date'] = date('Y-m-d H:i:s');
			
		return parent::insert( $data );
	}
	
	public function getQuitRefund($no_fom){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('r'=>$this->_name))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = r.rfd_appl_id')
						->joinLeft(array('u'=>'tbl_user'), 'u.iduser = r.rfd_creator', array())
						->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creator_name'=>'Fullname'))
						->joinLeft(array('u2'=>'tbl_user'), 'u2.iduser = r.rfd_approver_id', array())
						->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = u2.IdStaff', array('approver_name'=>'Fullname'))
						->joinLeft(array('rc'=>'refund_cheque'), 'rc.rchq_id = r.rdf_refund_cheque_id')
						->where("r.rfd_fomulir = ?", $no_fom);
		
		$row = $db->fetchAll($selectData);
		
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getRefundAmount($no_fom){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('r'=>$this->_name))
		->where("r.rfd_fomulir = ?", $no_fom)
		->where("r.rfd_status = 'A'");
		
		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return 0;
		}else{
			$total = 0;
			foreach ($row as $bil){
				$total = $total + $bil['rfd_amount'];
			}
				
			return $total;
		}
	}
	
	
public function getPaginateListData($where=array())
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('cur' => 'tbl_currency'), 'a.rfd_currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.rfd_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = a.rfd_cancel_by', array())
					->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancelName'=>'tsa.FullName'))
					->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = a.rfd_approver_id', array())
					->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('updName'=>'tsb.FullName'))
					->joinLeft(array('adv' => 'advance_payment'), 'adv.advpy_id = a.rfd_avdpy_id', array())
					;
		
		if ( !empty($where) )
		{
			if ( $where['type'] == 1 )
			{
				
				$select->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.rfd_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=a.rfd_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','ProgramCode'));
					
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('st.at_pes_id LIKE ?', '%'.$where['id'].'%');
				}

				/*$select->where('a.appl_id IS NOT NULL','')
					   ->where('a.IdStudentRegistration IS NULL','');*/
			}
			
			if ( $where['type'] == 2 )
			{
				$select->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.rfd_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('pa' => 'tbl_program'), 'pa.IdProgram=sr.IdProgram', array('ProgramName','ProgramCode'));
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sp.appl_fname,sp.appl_mname,sp.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('sr.registrationId LIKE ?', '%'.$where['id'].'%');
				}

//				$select->where('a.appl_id != ?','')
//					   ->where('a.IdStudentRegistration != ?','');
			}
			
			if ( isset($where['refund_id'] )){
				$select->where('a.rfd_fomulir LIKE ?', '%'.$where['refund_id'].'%');
			}
			
			$select->group('a.rfd_id');
			$select->order('rfd_id DESC');
		}

		return $select;
	}
	
public function getSearchDataReport($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		
		$selectData = $db->select()
						->from(array('r'=>'refund'))
						->joinLeft(array('adv' => 'advance_payment'), 'adv.advpy_id = r.rfd_avdpy_id')
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rfd_currency_id',array('cur_id','cur_code'))
						
						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.rfd_trans_id',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = r.rfd_IdStudentRegistration',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						->joinLeft(array('e'=>'tbl_program_scheme'), 'app.ap_prog_scheme = e.IdProgramScheme OR str.IdProgramScheme = e.IdProgramScheme',array())
			            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
			            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
			            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
						->joinLeft(array('itk'=>'tbl_intake'), 'str.IdIntake = itk.IdIntake',array('IntakeDesc'))
						->joinLeft(array('rt'=>'tbl_definationms'), 'r.rfd_type = rt.idDefinition', array('refundtype'=>'rt.DefinitionDesc'))
						->joinLeft(array('rpt'=>'tbl_definationms'), 'r.rfd_process_type = rpt.idDefinition', array('processtype'=>'rpt.DefinitionDesc'))
//						->where("r.dcnt_status = 'A'")
			->order('r.rfd_date desc');
							
		if($dateFrom!=''){
			$selectData->where("DATE(r.rfd_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(r.rfd_date) <= ?",$dateTo);
		}
	
		$row = $db->fetchAll($selectData);
		
		return $row;
	}
	
}
?>