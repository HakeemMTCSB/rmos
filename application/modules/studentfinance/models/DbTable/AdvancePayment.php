<?php
class Studentfinance_Model_DbTable_AdvancePayment extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'advance_payment';
	protected $_primary = "advpy_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('a'=>$this->_name))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.advpy_cur_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->joinLeft(array('pmt'=>'receipt'), 'pmt.rcp_id = a.advpy_rcp_id')
					->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = 	a.advpy_trans_id and (a.advpy_idStudentRegistration is null OR a.advpy_idStudentRegistration = 0)',array('at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as applicant_fullname"))
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.advpy_idStudentRegistration',array('registrationId'))
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
//						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("a.advpy_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	

	/*
	 * Overite Insert function
	 */
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['advpy_creator'])){
			$data['advpy_creator'] = $auth->getIdentity()->iduser;
		}
		
		$data['advpy_create_date'] = date('Y-m-d H:i:s');
			
		return parent::insert( $data );
	}
	
	/*
	 * Get Advance payment with balance
	 */
	/*public function getBalanceAvdPayment($fomulir){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->where("ap.advpy_fomulir = ?",$fomulir)
					->where("ap.advpy_total_balance > 0");

		$row = $db->fetchAll($selectData);				
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}*/
	
	public function getApplicantBalanceAvdPayment($appl_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->joinLeft(array('pmt'=>'payment_main'), 'pmt.id = ap.advpy_payment_id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("ap.advpy_appl_id = ?",$appl_id)
					->where("ap.advpy_total_balance !='0.00'");

		$row = $db->fetchAll($selectData);				
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getBalanceAvdPayment($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = ap.advpy_cur_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->joinLeft(array('pmt'=>'receipt'), 'pmt.rcp_id = ap.advpy_rcp_id')
					->joinLeft(array('inv'=>'invoice_main'), 'inv.id = ap.advpy_invoice_id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("ap.advpy_id = ?",$id)
					->where("ap.advpy_total_balance >= 0");

		$row = $db->fetchAll($selectData);				
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}

	/*
	 * Get applicant advance payment
	 */
	
	public function getApplicantAvdPayment($transID,$type=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->joinLeft(array('r'=>'refund'), 'ap.advpy_refund_id = r.rfd_id', array('rfd_fomulir','rfd_amount'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rfd_currency_id',array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("ap.advpy_status = 'A'");
					
		if($type == 1){
			$selectData->where("ap.advpy_trans_id  = ?",$transID);
		}elseif($type == 2){
			$selectData->where("ap.advpy_idStudentRegistration  = ?",$transID);
		}
					

		$row = $db->fetchAll($selectData);
                //var_dump($type);
		//echo $selectData; exit;
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}

	public function getApplicantAvdPaymentAll($transID,$type=0){

		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
			->from(array('ap'=>$this->_name))
			->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
			->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
			->joinLeft(array('r'=>'refund'), 'ap.advpy_refund_id = r.rfd_id', array('rfd_fomulir','rfd_amount'))
			->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rfd_currency_id',array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
			->joinLeft(array('cadv'=>'tbl_currency'), 'cadv.cur_id = ap.advpy_cur_id',array('adv_cur_code'=>'cadv.cur_code'));
			//->where("ap.advpy_status = 'A'");

		if($type == 1){
			$selectData->where("ap.advpy_trans_id  = ?",$transID);
		}elseif($type == 2){
			$selectData->where("ap.advpy_idStudentRegistration  = ?",$transID);
		}


		$row = $db->fetchAll($selectData);
		//var_dump($type);
		//echo $selectData; exit;
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	/*
	 * Get Sum Advance payment
	*/
	public function getSumAvdPayment($fomulir){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('ap'=>$this->_name), array('total_amount' => new Zend_Db_Expr('SUM(ap.advpy_amount)')))
		->where("ap.advpy_fomulir = ?",$fomulir);
	
		$row = $db->fetchRow($selectData);
		
	
	
		if(!$row){
			return null;
		}else{
			return $row['total_amount'];
		}
	}
	
	/*
	 * Get advance payment transfered from invoice paid
	 */
	public function getAdvancePaymentFromInvoice($invoice_id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
			->from(array('ap'=>$this->_name))
			->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
			->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
			->where("ap.advpy_invoice_id = ?",$invoice_id);
	
		$row = $db->fetchRow($selectData);
	
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getSearchData($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$name = $searchData['name'];
		$id = $searchData['id'];
		$type = $searchData['type'];
		$receipt_no = $searchData['receipt_id'];
		
		$selectData = $db->select()
						->from(array('a'=>$this->_name))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.advpy_cur_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
						->joinLeft(array('r'=>'receipt'), 'a.advpy_rcp_id = r.rcp_id',array('rcp_no'))
						->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.advpy_creator', array())
						->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
						
						->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.advpy_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_name','CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('pp' => 'tbl_program'), 'pp.IdProgram=ap.ap_prog_id', array('ProgramName','applicant_program'=>'ProgramCode'))
					
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.advpy_idStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode'))
						
						/*->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.advpy_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					
					
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.advpy_idStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id OR p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode'))*/
					
//						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.advpy_trans_id')
//						->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id')
//						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
//						->joinLeft(array('std'=>'tbl_studentregistration'), 'std.IdStudentRegistration = a.advpy_idStudentRegistration',array('registrationId'))
//						->joinLeft(array('sp'=>'student_profile'), 'sp.id = std.sp_id')
//						->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR std.IdProgram =pa.IdProgram ',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
						
						->order('a.advpy_date desc');
						
		if ( $type== 645 )
			{
				
				/*$selectData->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.advpy_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_name','CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','applicant_program'=>'ProgramCode'))
					->where('a.advpy_idStudentRegistration is NULL');*/
					
				if($id!='' ){
					$selectData->where("st.at_pes_id like ?",'%'.$id.'%');
				}
				if($name!=''){
					$selectData->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
				}
				
			}
			
			if ( $type == 646 )
			{
				/*$selectData->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.advpy_idStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name','CONCAT_WS(" ",sp.appl_fname, sp.appl_mname, sp.appl_lname) as applicant_name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode'))
					->where('a.advpy_idStudentRegistration is NOT NULL');*/
					
				if($id!='' ){
					$selectData->where("sr.registrationId like ?",'%'.$id.'%');
				}
				if($name!=''){
					$selectData->where("concat_ws(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname) like ?", '%'.$name.'%');
				}
				
			}
			

		if($receipt_no){
			$selectData->where('r.rcp_no like ?','%'.$receipt_no.'%')
			->where('r.rcp_status = "APPROVE"');
			
		}
//		echo $selectData;
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	
	public function getDepositFee($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$name = $searchData['name'];
		$id = $searchData['id'];
		$type = $searchData['type'];
		$program = $searchData['program'];
		
		$selectData = $db->select()
						->from(array('ab'=>'invoice_main'),array('idMain'=>'ab.id','*'))
						->joinLeft(array('a'=>'invoice_detail'), 'ab.id = a.invoice_main_id',array('idDetail'=>'a.id','*'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = ab.currency_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
						->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = a.fi_id',array())
//						->joinLeft(array('r'=>'receipt'), 'a.advpy_rcp_id = r.rcp_id',array('rcp_no'))
						->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ab.creator', array())
						->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
						->where('fi.fi_refundable = 1')
						->where('ab.adv_transfer_id = 0 ')
						->where("ab.status = 'A'");
							
						
	if ( $type== 645 )
			{
				
				$selectData->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=ab.trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','applicant_program'=>'ProgramCode'))
					->where("ab.trans_id is not null")
					->where("ab.IdStudentRegistration is null");
					
				if($id!='' ){
					$selectData->where("st.at_pes_id like ?",'%'.$id.'%');
				}
				if($name!=''){
					$selectData->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
				}
				
				if($program){
					$selectData->where('ap.ap_prog_id = ?',$program);
				}
				
			}
			
			if ( $type == 646 )
			{
				$selectData->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=ab.IdStudentRegistration', array('sr.registrationId','at_pes_id'=>'sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode'))
					->where("ab.IdStudentRegistration is not null");
				if($id!='' ){
					$selectData->where("sr.registrationId like ?",'%'.$id.'%');
				}
				if($name!=''){
					$selectData->where("concat_ws(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname) like ?", '%'.$name.'%');
				}
				
				if($program){
					$selectData->where('p.IdProgram = ?',$program);
				}
				
			}
			
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	public function getNegatifInvoiceBalance($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$name = $searchData['name'];
		$id = $searchData['id'];
		$type = $searchData['type'];
		$program = $searchData['program'];
		
		$selectData = $db->select()
						->from(array('a'=>'invoice_main'))
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code','cur_symbol_prefix','cur_symbol_suffix'))
						//->where('a.bill_balance < 0')
						->where("a.status = 'A'")
						//->limit("100")
						->order('a.invoice_date desc');
						
		if ( $type== 645 )
			{
				
				$selectData->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','applicant_program'=>'ProgramCode'));
//					->where("a.trans_id is not null")
//					->where("a.IdStudentRegistration is null");
					
				if($id!='' ){
					$selectData->where("st.at_pes_id like ?",'%'.$id.'%');
				}
				if($name!=''){
					$selectData->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
				}
				
				if($program){
					$selectData->where('ap.ap_prog_id = ?',$program);
				}
				
			}
			
			if ( $type == 646 )
			{
				$selectData->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId','at_pes_id'=>'sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode'));
//					->where("a.IdStudentRegistration is not null");
				if($id!='' ){
					$selectData->where("sr.registrationId like ?",'%'.$id.'%');
				}
				if($name!=''){
					$selectData->where("concat_ws(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname) like ?", '%'.$name.'%');
				}
				
				if($program){
					$selectData->where('p.IdProgram = ?',$program);
				}
				
			}
			//echo $selectData; exit;
		$row = $db->fetchAll($selectData);
		return $row;
	}
}
?>