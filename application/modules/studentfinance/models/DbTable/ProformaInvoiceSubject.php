<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_ProformaInvoiceSubject extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'proforma_invoice_subject';
	protected $_primary = "id";
	
	public function getData($proforma_main_id, $proforma_detail_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('a'=>$this->_name))
						->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=a.subject_id')
						->where("a.proforma_invoice_main_id = ?", (int)$proforma_main_id)
						->where("a.proforma_invoice_detail_id = ?", (int)$proforma_detail_id);
		
		$row = $db->fetchAll($selectData);
		return $row;
	}
}