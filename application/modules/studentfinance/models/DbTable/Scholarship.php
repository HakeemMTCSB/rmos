<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_Model_DbTable_Scholarship extends Zend_Db_Table_Abstract
{

    //put your code here
    protected $_name = 'tbl_scholarship_sch';
    protected $_fee_info = 'tbl_scholarship_fee_info';
    protected $_primary = "sch_Id";

    /*
     * list of Scholarhip
     * total
     * @on 16/6/2014
     */
    public function getScholarshipList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_scholarship_sch"), array("value" => "a.*",'id'=>'sch_Id','code'=>'sch_code','name'=>'sch_name'))
            ->order("a.sch_Id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get Section Configuration based on id
     * 
     * @on 16/6/2014
     */
    public function getScholarshipById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_scholarship_sch"), array("value" => "a.*",'id'=>'sch_Id','code'=>'sch_code','name'=>'sch_name'))
            ->where('a.sch_Id = ?', $id)
            ->order("a.sch_Id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * get program scheme list based on program list
     * 
     * @on 16/6/2014 
     */
    public function getProgSchemeBasedOnProg($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_scheme"), array("value" => "a.*"))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $id)
            ->order("a.IdProgramScheme");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function contribution_score($dob, $industry_type)
    {
        $date = explode('-', $dob);
        $year = end($date);
        if (strlen($year) != 4) {
            $date_part = explode('-', $dob);
            $year = $date_part[0];
        }
        $age = date('Y') - (int)$year;

        $AgeWeight = new Studentfinance_Model_DbTable_ScholarshipAgeWeight();
        $age_score = $AgeWeight->get_point($age);

        $IndustryWeight = new Studentfinance_Model_DbTable_ScholarshipIndustryWeight();
        $industry_score = $IndustryWeight->get_point($industry_type);

        //TODO:process minmax later
        $contribution_score = $industry_score + $age_score;

        if ($contribution_score < 20) {
            $contribution_score = 20;
        } else if ($contribution_score > 60) {
            $contribution_score = 60;
        }
        return ($contribution_score);
    }

    public function affordability_score($income, $dependent_count, $country)
    {
        $DependentWeight = new Studentfinance_Model_DbTable_ScholarshipDependentWeight();
        $dependent_score = $DependentWeight->get_point($dependent_count);

        $IncomeWeight = new Studentfinance_Model_DbTable_ScholarshipIncomeWeight();
        $CountryWeight = new Studentfinance_Model_DbTable_ScholarshipCountryWeight();
        $wi = $income * $CountryWeight->get_point($country);
        $income_score = $IncomeWeight->get_point($wi);

        $affordability_score = $income_score + $dependent_score;
        if ($affordability_score < 20) {
            $affordability_score = 20;
        } else if ($affordability_score > 60) {
            $affordability_score = 60;
        }
        return ($affordability_score);

    }

    public function academic_score($result, $qualification_rank)
    {
        if ($qualification_rank <= 4) {
            $academic_type = 4;
        } else {
            $cgpa = $result;
            if ($cgpa > 3.5) {
                $academic_type = 1;
            } else if ($cgpa > 3.0) {
                $academic_type = 2;
            } else if ($cgpa > 2.5) {
                $academic_type = 3;
            } else {
                $academic_type = 4;
            }
        }

        $AcademicWeight = new Studentfinance_Model_DbTable_ScholarshipAcademicWeight();
        $academic_score = $AcademicWeight->get_point($academic_type);
        return ($academic_score);
    }
    
	public function getDataByStudent($id,$idStudentRegistration)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'tbl_scholarship_studenttag'),array('*','startdate'=>'sa_start_date','end_date'=>"IFNULL(sa_end_date,'0000-00-00')"))
		->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration')
		->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
		->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
		->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
		->where('a.sa_scholarship_type =?',$id)
		->where('a.sa_cust_id =?',$idStudentRegistration);
			
		$result = $db->fetchRow($lstrSelect);
		return $result;
	}
	
	public function getFeeInfo($id,$fi_id=0)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $db->select()
		->from(array('a' => 'tbl_scholarship_fee_info'),array('a.*','FreqMode'=>'IFNULL(FreqMode,1)'))
		->joinLeft(array('b' => 'fee_item'), 'a.FeeCode = b.fi_id', array('fi_name','fi_code','fi_id'))
		->where('a.sct_Id =?',$id);
		
		if($fi_id){
			$lstrSelect->where('a.FeeCode =?',$fi_id);
			
			$result = $db->fetchRow($lstrSelect);
		}else{
			$result = $db->fetchAll($lstrSelect);
		}
		
//		echo $lstrSelect ."<hr>";
		return $result;
	}
        
    public function getScholarship(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_scholarship_sch"), array('id'=>'sch_Id','code'=>'sch_code','name'=>'sch_name'))
            ->order("a.sch_Id");
        
        $result = $db->fetchAll($select);
        return $result;
    }
	
    public function getSponsorship(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_sponsor"), array('id'=>'idsponsor','code'=>'SponsorCode','name'=>'fName'))
            ->order("a.idsponsor");
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getFinancialAid(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_financial_aid"), array('id'=>'id','code'=>'code','name'=>'name'))
            ->order("a.id");
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('seq_no')
            ->order('ProgramName');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            //->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $id)
            ->order('a.SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemesterAll(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->order('a.SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getIntakeAll(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->order('a.IdIntake DESC');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSubjectItem($regsubid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->where('a.regsub_id = ?', $regsubid)
            ->group('a.invoice_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkCourse($said, $regsubid, $subid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->where('a.sac_said = ?', $said)
            ->where('a.sac_regsubid = ?', $regsubid)
            ->where('a.sac_subjectid = ?', $subid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkReCourse($said, $regsubid, $subid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->where('a.src_sreid = ?', $said)
            ->where('a.src_regsubid = ?', $regsubid)
            ->where('a.src_subjectid = ?', $subid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function deleteSponsorSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('sponsor_application_course', 'sac_id = '.$id);
        return $delete;
    }

    public function deleteSponsorReSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('sponsor_reapplication_course', 'src_id = '.$id);
        return $delete;
    }

    public function getInvoiceDetails($id, $subjectid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'), array('value'=>'*'))
            ->join(array('b'=>'fee_item'), 'a.fi_id = b.fi_id')
            ->join(array('c'=>'invoice_subject'), 'a.fi_id = b.fi_id AND a.id = invoice_detail_id', array('amt'=>'c.amount'))
            ->where('a.invoice_main_id = ?', $id)
            ->where('c.subject_id = ?', $subjectid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoiceMain($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array('value'=>'*'))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function financialAidCoverage($feeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_financial_aid_coverage'), array('value'=>'*'))
            ->where('a.fee_code = ?', $feeid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function saveSponsorInvMain($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_invoice_main', $data);
        $id = $db->lastInsertId('sponsor_invoice_main', 'sp_id');
        return $id;
    }

    public function saveSponsorInvDetail($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_invoice_detail', $data);
        $id = $db->lastInsertId('sponsor_invoice_detail', 'id');
        return $id;
    }

    public function updateApplicationCourse($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_application_course', $data, 'sac_id = '.$id);
        return $update;
    }

    public function updateReApplicationCourse($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_reapplication_course', $data, 'src_id = '.$id);
        return $update;
    }

    public function getGradeSetup($idProgram){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_gradesetup_main'), array('value'=>'*'))
            ->join(array('b'=>'tbl_gradesetup'), 'a.IdGradeSetUpMain = b.IdGradeSetUpMain')
            ->joinLeft(array('c'=>'tbl_definationms'), 'b.Grade = c.idDefinition')
            ->where('a.IdProgram = ?', $idProgram)
            ->where('b.Grade != ?', 942)
            ->group('b.Grade')
            ->order('b.Rank ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDefinationByType($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
}
