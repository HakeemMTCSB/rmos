<?php
class Studentfinance_Model_DbTable_Billapprovalmodel extends Zend_Db_Table 
{ //Model Class for Users Details
	protected $_name = 'tbl_vouchermaster';
	
	public function fnaddvoudherdetails($post)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_vouchermaster";
		
	}
	
	public function fnGetVoucherTypeList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("tbl_vouchertype"=>"tbl_vouchertype"),array("key"=>"tbl_vouchertype.idVoucherType","value"=>"tbl_vouchertype.VoucherType"))
		 				 ->where("tbl_vouchertype.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetVoucherMasterDetails(){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tbl_vouchermaster" => "tbl_vouchermaster"))
								  ->join(array("tbl_studentregistration"=>"tbl_studentregistration"),'tbl_vouchermaster.idstudent=tbl_studentregistration.IdStudentRegistration',array("tbl_studentregistration.IdApplication","tbl_studentregistration.registrationId"))
								  ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),'tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication',array("tbl_studentapplication.IdApplication","tbl_studentapplication.FName","tbl_studentapplication.MName","tbl_studentapplication.LName"))
								  ->join(array("tbl_vouchertype"=>"tbl_vouchertype"),'tbl_vouchermaster.Vouchertype = tbl_vouchertype.idVoucherType')
								  ->join(array("tbl_voucherdetails"=>"tbl_voucherdetails"),'tbl_vouchermaster.idvoucher = tbl_voucherdetails.idvoucher',array("sum(tbl_voucherdetails.amount) as totamt"))
								  ->where("tbl_vouchermaster.approved = 0")
								  ->group("tbl_vouchermaster.idstudent");
								  
								//  echo $lstrSelect;exit;
								  
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;		
		
	}
	
	public function fnSearchVoucherMasterDetails($post = array()){
		
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       							->from(array("tbl_vouchermaster"=>"tbl_vouchermaster"))
       							->join(array("tbl_voucherdetails"=>"tbl_voucherdetails"),'tbl_vouchermaster.idvoucher = tbl_voucherdetails.idvoucher',array("sum(tbl_voucherdetails.amount) as totamt"))
       							->join(array("tbl_studentregistration"=>"tbl_studentregistration"),'tbl_vouchermaster.idstudent=tbl_studentregistration.IdStudentRegistration')
       							->join(array("tbl_studentapplication"=>"tbl_studentapplication"),'tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication',array("tbl_studentapplication.IdApplication","tbl_studentapplication.FName","tbl_studentapplication.MName","tbl_studentapplication.LName"))
								->join(array("tbl_vouchertype"=>"tbl_vouchertype"),'tbl_vouchermaster.Vouchertype = tbl_vouchertype.idVoucherType')
								->where("tbl_vouchermaster.approved = ?",$post['field7'])								
								->group("tbl_vouchermaster.idstudent");
   
			if(isset($post['field2']) && !empty($post['field2']) ){
				$lstrSelect = $lstrSelect->where("tbl_studentapplication.StudentId = ?",$post['field2']);										
			}	
			if(isset($post['field3']) && !empty($post['field3']) ){				
				$lstrSelect = $lstrSelect->where("tbl_studentapplication.FName like ?",'%'.$post['field3'].'%');								
			}			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$lstrSelect = $lstrSelect->where("tbl_vouchermaster.Vouchertype = ?",$post['field1']);										
			}
			if(isset($post['field14']) && !empty($post['field14']) ){
				$lstrSelect = $lstrSelect->where("tbl_vouchermaster.Voucherdate = ?",$post['field14']);				
			}
			
       // echo $lstrSelect;
       	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
		
	}
	
	public function fnupdateVoucherBillaction($lintidvoucher,$approved){		
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_vouchermaster";		
		$larrformdata['approved']=$approved;
		$where = "idvoucher = '".$lintidvoucher."'";
		$db->update($table,$larrformdata,$where);	
			
	}
}