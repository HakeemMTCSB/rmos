<?php

class Studentfinance_Model_DbTable_PromotionFee extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_promotion_fee_detail';
    protected $_primary = 'id';
    private $lobjDbAdpt;
    protected $_locale;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $registry = Zend_Registry:: getInstance();
        $this->_locale = $registry->get('Zend_Locale');
	}


    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }


    public function getFeeInfo($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $db->select()
            ->from(array('a' => 'tbl_promotion_fee_detail'),array('a.*'))
            ->join(array('b' => 'registry_values'), 'a.calc_type = b.id', array('name'))
            ->where('a.promotion_id =?',$id);

            $result = $db->fetchAll($lstrSelect);


        return $result;
    }

    public function deleteData($id)
    {
        $this->delete($this->_primary . ' =' . (int)$id);
    }


}