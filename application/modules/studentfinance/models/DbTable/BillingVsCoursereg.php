<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 28/3/2016
 * Time: 8:49 AM
 */
class Studentfinance_Model_DbTable_BillingVsCoursereg extends Zend_Db_Table_Abstract {

    public function programList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('a.seq_no ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function programById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getIntake(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'));

        $select .= " ORDER BY a.sem_year DESC, CASE a.sem_seq WHEN 'JAN' THEN 1
                    WHEN 'FEB' THEN 2
                    WHEN 'MAR' THEN 3
                    WHEN 'APR' THEN 4
                    WHEN 'MAY' THEN 5
                    WHEN 'JUN' THEN 6
                    WHEN 'JUL' THEN 7
                    WHEN 'AUG' THEN 8
                    WHEN 'SEP' THEN 9
                    WHEN 'OCT' THEN 10
                    WHEN 'NOV' THEN 11
                    WHEN 'DEC' THEN 12
                    END DESC";

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $scheme);

        $select .= " ORDER BY a.AcademicYear DESC, CASE a.sem_seq WHEN 'JAN' THEN 1
                    WHEN 'FEB' THEN 2
                    WHEN 'MAR' THEN 3
                    WHEN 'APR' THEN 4
                    WHEN 'MAY' THEN 5
                    WHEN 'JUN' THEN 6
                    WHEN 'JUL' THEN 7
                    WHEN 'AUG' THEN 8
                    WHEN 'SEP' THEN 9
                    WHEN 'OCT' THEN 10
                    WHEN 'NOV' THEN 11
                    WHEN 'DEC' THEN 12
                    END DESC";

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoice($search = false){
        $dateFrom = date('Y-m-d', strtotime($search['datefrom']));
        $dateTo = date('Y-m-d', strtotime($search['dateto']));

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ivd'=>'invoice_detail'),array('fi_id','balance','ivd.amount','idDetail'=>'ivd.id'))
            ->join(array('a'=>'invoice_main'),'ivd.invoice_main_id = a.id',array('invoice_no'=>'a.bill_number','id','semester','IdStudentRegistration','invoice_date','bill_balance','exchange_rate','bill_amount','statusInv'=>'a.status','invMainID'=>'a.id','MigrateCode','bill_description'))
            ->join(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id',array('cur_id','cur_code'))
            ->join(array('cr'=>'tbl_currency_rate'), 'cr.cr_id = a.exchange_rate',array('cr_exchange_rate'))
            ->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.trans_id',array('at_pes_id'))
            ->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
            ->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
            ->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = a.IdStudentRegistration AND (a.IdStudentRegistration IS NOT NULL OR a.IdStudentRegistration != 0)',array('registrationId'))
            ->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
            ->join(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram OR str.IdProgram = pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
            ->joinLeft(array('sms'=>'tbl_semestermaster'), 'sms.IdSemesterMaster = a.semester',array('SemesterMainName'))
            ->join(array('ti'=>'tbl_intake'),'ti.IdIntake = str.IdIntake OR ti.IdIntake = at.at_intake',array('IntakeDesc'))
            ->join(array('e'=>'tbl_program_scheme'), 'str.IdProgramScheme = e.IdProgramScheme OR app.ap_prog_scheme = e.IdProgramScheme ',array())
            ->joinLeft(array('sts'=>'tbl_definationms'), 'str.profileStatus = sts.idDefinition OR sts.idDefinition=at.at_status', array('profileStatus'=>'sts.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
            ->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = ivd.fi_id',array('fi_name'))
            ->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id',array('fc_desc'))
            ->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fi.fi_ac_id',array('ac_code','ac_desc'))
            ->joinLeft(array('schtg'=>'tbl_scholarship_studenttag'), 'schtg.sa_cust_id = a.IdStudentRegistration', array())
            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array('scholarship'=>'sca.sch_name'))
            ->joinLeft(array('spt'=>'tbl_sponsor_tag'), 'spt.StudentId = a.IdStudentRegistration', array())
            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array("CONCAT_WS(' ',sptt.fName, sptt.lName) as sponsorship"))
            ->join(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = ivd.id',array('subject_id'))
            ->joinLeft(array('sc'=>'tbl_subjectmaster'), 'sc.IdSubject = ivs.subject_id',array('SubCode', 'CreditHours'))
            ->joinLeft(array('cn'=>'credit_note_detail'), "cn.invoice_main_id = a.id and cn.invoice_detail_id = ivd.id",array('cn_cur_id'=>'cur_id','cn_amount'=>'amount'))
            ->joinLeft(array('cna'=>'credit_note'), "cna.cn_id = cn.cn_id and cna.cn_status = 'A'",array('cn_type','type_amount'))
            ->joinLeft(array('dcntt'=>'discount_detail'), "dcntt.dcnt_invoice_id = a.id and dcntt.dcnt_invoice_det_id = ivd.id",array('dcnt_amount'))
            ->joinLeft(array('dcnn'=>'discount'), "dcnn.dcnt_id = dcntt.dcnt_id and dcnn.dcnt_status = 'A'",array())
            //->joinLeft(array('reg'=>'tbl_studentregsubjects'), 'reg.IdStudentRegistration = str.IdStudentRegistration AND reg.IdSemesterMain = a.semester AND reg.IdSubject = ivs.subject_id', array('courseStatus'=>'reg.Active', 'examStatus'=>'reg.exam_status'))
            ->joinLeft(array('regdtl'=>'tbl_studentregsubjects_detail'), 'a.id = regdtl.invoice_id', array())
            ->joinLeft(array('reg'=>'tbl_studentregsubjects'), 'reg.IdStudentRegSubjects = regdtl.regsub_id', array('courseStatus'=>'reg.Active', 'examStatus'=>'reg.exam_status', 'credit_hour_registered'))
            ->where("(((DATE(a.invoice_date) >= '".$dateFrom."') AND (DATE(a.invoice_date) <= '".$dateTo."')) OR ((DATE(cna.cn_approve_date) >= '".$dateFrom."') AND (DATE(cna.cn_approve_date) <= '".$dateTo."')))")
            ->where("a.status IN ('A','W')")
            ->order('a.invoice_date asc')
            ->order('a.bill_number')
            ->group('ivd.id');

        if ($search != false){
            if (isset($search['program']) && $search['program'] != '' && $search['program'] != 'ALL'){
                $select->where('pa.IdProgram = ?', $search['program']);
            }
            if (isset($search['intake']) && count($search['intake']) > 0){
                $select->where('ti.IdIntake in (?)', $search['intake']);
            }
            if (isset($search['semester']) && count($search['semester']) > 0){
                $select->where('a.semester in (?)', $search['semester']);
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }
}