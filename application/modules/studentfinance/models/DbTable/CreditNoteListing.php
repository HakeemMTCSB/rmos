<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Studentfinance_Model_DbTable_CreditNoteListing extends Zend_Db_Table_Abstract {
    
    public function getCreditNoteListing($formData = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'credit_note'), array('value'=>'*', 'AMOUNT1'=>'a.cn_amount', 'AMOUNT2'=>'j.bill_amount', 'currencyid'=>'c.cur_id', 'AMOUNT3'=>'c.cn_amount','cn_type','MigrateCodeCN'=>'a.MigrateCode', 'j.invoice_date'))
            ->joinLeft(array('b'=>'credit_note_detail'), 'a.cn_id=b.cn_id')
            ->joinLeft(array('c'=>'invoice_detail'), 'b.invoice_detail_id=c.id')
            ->joinLeft(array('d'=>'fee_item'), 'c.fi_id=d.fi_id')
            ->joinLeft(array('e'=>'tbl_studentregistration'), 'a.cn_IdStudentRegistration=e.IdStudentRegistration')
            ->joinLeft(array('f'=>'student_profile'), 'e.sp_id=f.id')
            ->joinLeft(array('g'=>'tbl_program'), 'e.IdProgram=g.IdProgram', array('ProgramCodeStudent'=>'g.ProgramCode'))
            ->joinLeft(array('h'=>'invoice_subject'), 'b.invoice_detail_id=h.invoice_detail_id')
            ->joinLeft(array('i'=>'tbl_subjectmaster'), 'h.subject_id=i.IdSubject')
            ->joinLeft(array('j'=>'invoice_main'), 'b.invoice_main_id=j.id')
            ->joinLeft(array('sm'=>'tbl_semestermaster'), 'sm.IdSemesterMaster = j.semester',array('SemesterMainName'))
            ->joinLeft(array('l'=>'tbl_currency_rate'), 'j.exchange_rate=l.cr_id', array('rate'=>'l.cr_exchange_rate'))
            ->joinLeft(array('k'=>'tbl_studentregsubjects'), 'h.subject_id=i.IdSubject AND a.cn_IdStudentRegistration=k.IdStudentRegistration AND j.semester=k.IdSemesterMain')
            ->joinLeft(array('m'=>'applicant_transaction'), 'a.cn_trans_id=m.at_trans_id')
            ->joinLeft(array('n'=>'applicant_profile'), 'm.at_appl_id=n.appl_id', array('applicant_name'=>'CONCAT(n.appl_fname, " ", n.appl_lname)'))
            ->joinLeft(array('o'=>'applicant_program'), 'a.cn_trans_id=o.ap_at_trans_id', array())
            ->joinLeft(array('p'=>'tbl_program'), 'o.ap_prog_id=p.IdProgram', array('ProgramCodeApplicant'=>'p.ProgramCode'))
            //->where('e.registrationId <> ?', 1409999)
            ->where("a.cn_status = 'A'")
            ->order('a.cn_create_date desc');
        
        if ($formData != false){
            if (isset($formData['date_from']) && $formData['date_from']!=''){
                $select->where('DATE(a.cn_create_date) >= ?', date('Y-m-d', strtotime($formData['date_from'])));
            }
            if (isset($formData['date_to']) && $formData['date_to']!=''){
                $select->where('DATE(a.cn_create_date) <= ?', date('Y-m-d', strtotime($formData['date_to'])));
            }
            if (isset($formData['program']) && $formData['program']!=''){
                $select->where('e.IdProgram = ?', $formData['program']);
            }
            if (isset($formData['programscheme']) && $formData['programscheme']!=''){
                $select->where('e.IdProgramScheme = ?', $formData['programscheme']);
            }
            if (isset($formData['account_code']) && $formData['account_code']!=''){
                $select->where('d.fi_ac_id = ?', $formData['account_code']);
            }
        }
        
        $select->group('b.cn_id');
        //echo $select; //exit;
        $return = $db->fetchAll($select);
        return $return;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('a.seq_no')
            ->order('a.ProgramName');
        
        $return = $db->fetchAll($select);
        return $return;
    }
    
    public function getProgramSchemeList($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $id)
            ->order('b.DefinitionDesc');
        
        $return = $db->fetchAll($select);
        return $return;
    }
    
    public function getAccountCode(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_account_code'), array('value'=>'*'));
        
        $return = $db->fetchAll($select);
        return $return;
    }
}
?>

