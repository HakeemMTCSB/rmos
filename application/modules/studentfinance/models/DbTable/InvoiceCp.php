<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/3/2016
 * Time: 11:06 AM
 */
class Studentfinance_Model_DbTable_InvoiceCp extends Zend_Db_Table {

    public function getSemester(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'));

        $select .= " ORDER BY a.AcademicYear DESC,
            CASE a.sem_seq WHEN 'JAN' THEN 1
            WHEN 'FEB' THEN 2
            WHEN 'MAR' THEN 3
            WHEN 'APR' THEN 4
            WHEN 'MAY' THEN 5
            WHEN 'JUN' THEN 6
            WHEN 'JUL' THEN 7
            WHEN 'AUG' THEN 8
            WHEN 'SEP' THEN 9
            WHEN 'OCT' THEN 10
            WHEN 'NOV' THEN 11
            WHEN 'DEC' THEN 12
            END DESC, a.IdScheme ASC";

        //echo $select; exit;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getItem($code){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('fi'=>'fee_item'))
            ->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
            ->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
            ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
            ->where('fi.fi_active = ?', 1)
            ->where('fc.fc_code = ?', $code);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function sequence($year){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_seq_cp'), array('value'=>'*'))
            ->where('a.isc_year = ?', $year);

        $result = $db->fetchRow($select);

        if (!$result){
            $data = array(
                'isc_constant1'=>'INV',
                'isc_constant2'=>'',
                'isc_year'=>$year,
                'isc_seq'=>1
            );
            $db->insert('invoice_seq_cp', $data);

            $result = $this->sequence($year);
        }

        return $result;
    }

    public function updateSeq($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('invoice_seq_cp', $data, 'isc_id = '.$id);
        return $update;
    }

    public function getStudent($regId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->join(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.profileStatus = c.idDefinition', array('status_name'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_program'), 'a.IdProgram = d.IdProgram', array('ProgramCode'))
            ->where('a.registrationId like "%'.$regId.'%"');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function insertInvoice($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('invoice_main_cp', $data);
        $id = $db->lastInsertId('invoice_main_cp', 'id');
        return $id;
    }

    public function updateInvoice($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('invoice_main_cp', $data, 'id = '.$id);
        return $update;
    }

    public function insertInvoiceDtl($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('invoice_detail_cp', $data);
        $id = $db->lastInsertId('invoice_detail_cp', 'id');
        return $id;
    }

    public function getInvoice($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main_cp'), array('value'=>'*', 'invoice_type'=>'a.student_type', 'invoiceid'=>'a.id'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_semestermaster'), 'a.semester = d.IdSemesterMaster')
            ->joinLeft(array('e'=>'tbl_currency'), 'a.currency_id = e.cur_id')
            ->joinLeft(array('f'=>'tbl_branchofficevenue'), 'a.branch_id = f.IdBranch')
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getInvoiceDtl($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail_cp'), array('value'=>'*'))
            ->joinLeft(array('b'=>'fee_item'), 'a.fi_id = b.fi_id')
            ->joinLeft(array('c'=>'tbl_currency'), 'a.cur_id = c.cur_id')
            ->where('a.invoice_main_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function uploadfile($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('invoice_cp_upload', $data);
        //var_dump($data); exit;
        return true;
    }

    public function getUpload($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_cp_upload'), array('value'=>'*'))
            ->where('a.icu_invcp_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkDuplicate($bill_number){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main_cp'), array('value'=>'*'))
            ->where('a.bill_number = ?', $bill_number);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoiceList($where=false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main_cp'), array('value'=>'*', 'invoice_type'=>'a.student_type', 'invoiceid'=>'a.id'))
            ->join(array('ivd' => 'invoice_detail_cp'), 'ivd.invoice_main_id = a.id', array())
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
            ->joinLeft(array('fc' => 'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id', array())
            ->joinLeft(array('cn' => 'credit_note'), "a.id = cn.cn_invoice_id and cn.cn_status = 'A'", array('cn_id', 'cn_billing_no'))
            ->join(array('cur' => 'tbl_currency'), 'a.currency_id=cur.cur_id', array('cur.cur_symbol_prefix'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.creator', array())
            ->joinLeft(array('ts' => 'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName' => 'ts.FullName'))
            ->joinLeft(array('ua' => 'tbl_user'), 'ua.iduser = a.cancel_by', array())
            ->joinLeft(array('tsa' => 'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancelName' => 'tsa.FullName'))
            ->joinLeft(array('ub' => 'tbl_user'), 'ub.iduser = a.upd_by', array())
            ->joinLeft(array('tsb' => 'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('updName' => 'tsb.FullName'))
            ->joinLeft(array('rcv' => 'receipt_invoice'), 'rcv.rcp_inv_invoice_id = a.id', array())
            ->joinLeft(array('rcp' => 'receipt'), "rcp.rcp_id = rcv.rcp_inv_rcp_id and rcp.rcp_status = 'APPROVE'", array('rcp_no'))
            ->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.IdStudentRegistration', array('sr.registrationId', 'at_pes_id' => 'sr.registrationId'))
            ->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as studentname'))
            ->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName', 'applicant_program' => 'ProgramCode', 'ProgramCode'))
            ->joinLeft(array('br'=>'tbl_branchofficevenue'), 'a.branch_id = br.IdBranch');


        if ($where != false) {
            if (isset($where['branch']) && $where['branch'] != '') {
                $select->where("a.branch_id = ?", $where['branch']);
            }

            if (isset($where['invoice_id']) && $where['invoice_id'] != '') {
                $select->where('a.bill_number LIKE ?', '%' . $where['invoice_id'] . '%');
            }

            if (isset($where['status']) && $where['status'] != '') {
                $select->where('a.status IN (?)', $where['status']);
            }
        }

        $select->group('a.id');
        $select->order('a.invoice_date asc');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function insertStudent($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('invoice_cp_student', $data);
        $id = $db->lastInsertId('invoice_cp_student', 'ics_id');
        return $id;
    }

    public function getInvStdList($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_cp_student'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.ics_student_id = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.profileStatus = e.idDefinition', array('statusname'=>'e.DefinitionDesc'))
            ->where('a.ics_invcp_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getBranch(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_branchofficevenue'))
            ->order('BranchName ASC');

        $result = $db->fetchAll($select);
        return $result;
    }
}