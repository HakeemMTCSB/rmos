<?php
class Studentfinance_Model_DbTable_Accountgroup extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_accountgroup';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	
	public function fngetAccountgroup() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect =  "SELECT IdAccountGroup,GroupName,GroupCode,Description,IdParent from tbl_accountgroup where Active = 1";			
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fngetaccountdeatilsSearch($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("tbl_accountgroup"),array("IdAccountGroup","GroupName","Description","IdParent","GroupCode"))
       								->where('GroupName   like "%" ? "%"',$post['field2'])
       								->where('Description   like "%" ? "%"',$post['field3'])
       								->where('GroupCode   like "%" ? "%"',$post['field4'])
       								->where('Active = ?',$post['field7']);
       //echo $lstrSelect;die();
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	public function fnInsertAccountGroup($insertData) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_accountgroup";
	   $db->insert($table,$insertData);   
	  return $lintgroupid = $this->lobjDbAdpt->lastInsertId();
	} 
	

	
    public function fnUpdateAccountGroup($IdAccountGroup,$larrformData) { //Function for updating the user   
		$where = 'IdAccountGroup = '.$IdAccountGroup;
		$this->update($larrformData,$where);
    }
	
	public function fnGetGroupList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_accountgroup"),array("key"=>"a.IdAccountGroup","value"=>"a.GroupName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.GroupName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetGroupListEdit($IdAccountGroup){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_accountgroup"),array("key"=>"a.IdAccountGroup","value"=>"a.GroupName"))
				 				 ->where("a.Active = 1")
				 				 ->where("a.IdAccountGroup != ?",$IdAccountGroup)
				 				 ->order("a.GroupName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnEditAccountGroup($IdAccountGroup) {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("c"=>"tbl_accountgroup"),array("c.*"))			
		            	->where("c.IdAccountGroup = ?",$IdAccountGroup);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
    
    public function fnCheckstartletter($strletter){
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
						->from(array("tbl_accountgroup"),array("IdAccountGroup"))			
		            	->where('GroupCode like  ? "%"',$strletter)
		            	->where("IdParent = 0");	
		return $result = $lobjDbAdpt->fetchAll($select);
    	
    }
    
	public function fnLevelcode($IdAccountGroup) {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
					 ->from(array("c"=>"tbl_accountgroup"),array("c.IdParent","substring(c.GroupName,1,1) as GroupName"))
		             ->where("c.IdAccountGroup = ?",$IdAccountGroup);
		return $result = $lobjDbAdpt->fetchRow($select);
		
		
    }
	public function fnLevelgroupcode($idparent) {  
		
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
					 ->from(array("c"=>"tbl_accountgroup"),array("c.IdParent","substring(c.GroupName,1,1) as GroupN","c.GroupCode","c.IdAccountGroup","count(c.IdAccountGroup) as count"))
		             ->where("c.IdAccountGroup = ?",$idparent);
		return $result = $lobjDbAdpt->fetchRow($select);
		
    }
    
	public function fnparentcount($idparent,$strletter) {  
		
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
					 ->from(array("c"=>"tbl_accountgroup"),array("count(c.IdParent) as count"))
		             ->where("c.IdParent = ?",$idparent)
		             ->where('c.GroupName like  ? "%"',$strletter);
		             echo $select;
		return $result = $lobjDbAdpt->fetchRow($select);		
		
    }
    
	public function fnGenerateAccCodes($idUniversity,$linitlastid,$objIC){
									 
			$result = 	$objIC->fnGetInitialConfigDetails($idUniversity);//GeneralbaseSeparator
			$sepr	=	$result['GeneralbaseSeparator'];
			$str	=	"generalbaseField";
			$strText=	"generalbaseText";
			for($i=1;$i<=4;$i++){
				$check = $result[$str.$i];
				$Text = $result[$strText.$i];
				switch ($check){
					case 'Year':
					  $code	= date('Y');
					  break;
					case 'Uniqueid':
					  $code	= $linitlastid;
					  break;	
					case 'University':
					  $code	= $idUniversity;
					  break;						
					case 'Text':
					  $code	= $Text;
					  break;	
					default:
					  break;
				}
				if($i == 1) $accCode 	 =  $code;
				else 		$accCode	.=	$sepr.$code;
			}				
			return $accCode;	
		}	

}