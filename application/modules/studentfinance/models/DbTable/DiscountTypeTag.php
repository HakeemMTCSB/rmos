<?php
class Studentfinance_Model_DbTable_DiscountTypeTag extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'discount_type_tag';
	protected $_primary = "dtt_id";
		
	public function getData($dtt_id){

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()
		->from(array('a' => 'discount_type_tag'))	
		->where('a.dtt_id =?',$dtt_id);
			
		$result = $db->fetchRow($lstrSelect);
		return $result;
	}
	
	public function getDiscountTag($dt_id){

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()
		->from(array('a' => 'discount_type_tag'))
		->joinLeft(array('b'=>'discount_type'),'b.dt_id = a.dt_id',array('dt_discount'))
		->joinLeft(array('c'=>'tbl_studentregistration'),'c.IdStudentRegistration = a.IdStudentRegistration',array('c.registrationId as StudentId'))
		->joinLeft(array('p'=>'student_profile'), 'p.id=c.sp_id', array('p.appl_fname','p.appl_mname','p.appl_lname'))		
		->where('a.dt_id =?',$dt_id);
			
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
	
	public function fnaddTagStudentDiscount($larrformData){

		$db = Zend_Db_Table::getDefaultAdapter();
		if($larrformData['idstudentregistrationGrid']){
			$check = count($larrformData['idstudentregistrationGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'dt_id' => $larrformData['dt_id'],
						'IdStudentRegistration' => $larrformData['idstudentregistrationGrid'][$i],
						'dtt_start_date' => $larrformData['StartDateGrid'][$i],
						'dtt_end_date' => $larrformData['EndDateGrid'][$i],
						'dtt_createdby'=>$larrformData['UpdUser'],
						'dtt_createddt'=>$larrformData['UpdDate']
				);
				$db->insert('discount_type_tag',$paramArray);
			}
		}

	}
	
	public function updateTagStudent($data,$dtt_id){
		
		$this->update($data,"dtt_id='".$dtt_id."'");
	}
	
	public function deleteData($where){
		
		$this->delete($where);
	}
}

?>