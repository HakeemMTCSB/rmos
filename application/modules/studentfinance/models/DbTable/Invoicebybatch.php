<?php
class Studentfinance_Model_DbTable_Invoicebybatch extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_bill_batch';
	private $lobjmaintenModel;

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		$this->lobjmaintenModel  = new GeneralSetup_Model_DbTable_Maintenance();
		$this->lobjFeeCategory = new Studentfinance_Model_DbTable_Fee();
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	/*
	 * Add Batch entry
	 * last updated: 17/2/2014
	 */
	public function addNewBatch($data)
	{
		$batcharray = $data;
		unset($data['IdStudent']);
		unset($data['Save']);
		unset($data['studIdgrid']);
		unset($data['studnamegrid']);
		unset($data['name']);
		$data['ProcessStatus'] = 0;
		//$lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		$larrDefId = $this->lobjmaintenModel->fngetDefKeyValue('StatusInvoiceByBatch','Entry');
		$data['Status'] = $larrDefId[0]['key'];
		$table = "tbl_bill_batch";
		$this->lobjDbAdpt->insert($table,$data);
		$lintBatchId = $this->lobjDbAdpt->lastInsertId();
		
		// Now insert student detail regarding this batch
		if ( !empty($batcharray['studIdgrid']) )
		{
			$this->addBatchStudents($batcharray['studIdgrid'],$lintBatchId);
		}
		else
		{
			//If generate by by student
			$studentsData = array(
									'IdSemester'		=>	$data['IdSemester'],
									'IdProgram'			=>	$data['IDProgram'],
									'IdIntake'			=>	$data['IdIntakeFrom'],
									'StudentCategory'	=> 	$data['IdCategory'],
									'IdBranch'			=>	$data['IdBranch']
							);
							
			$students = $this->getStudent($studentsData,1);
			if ( !empty($students) )
			{
				$allstudents = array();
				foreach ( $students as $stud )
				{
					$allstudents[] = $stud['key'];
				}
				
				
				$this->addBatchStudents($allstudents,$lintBatchId);
			}
		}
	}

	// Function to insert students of the batch
	public function addBatchStudents($students,$batchId)
	{
		$len = count($students);
		$batchstuddet = array();
		for($j=0; $j < $len; $j++) {
			$batchstuddet['IdBillBatch'] = $batchId;
			$batchstuddet['IdStudent'] = $students[$j];
			$batchstuddet['ProcessStatus'] = 0;
			$table = "tbl_bill_batch_invoice";
			$this->lobjDbAdpt->insert($table,$batchstuddet);
		}
	}

	//function get all batch detail with status 0
	public function getAllBatches(){
		$limit = 10;
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_bill_batch"), array("a.*"))
		->where('a.ProcessStatus =?',0)
		->order('a.IdBillBatch')
		->limit($limit);

		return $result = $this->lobjDbAdpt->fetchAll($select);
	}

	// function to generate the invoices of the students of the batch
	public function generateBatchesInvoice($batches)
	{
		foreach($batches as $batch)
		{
			// now get the student of the batches
			$studentList = $this->getStudentsofBatch($batch['IdBillBatch']);
		
			if(!empty($studentList))
			{
				
				// First insert the batch in batchbill table with inprogress status
				$status = $this->lobjmaintenModel->fngetDefKeyValue('StatusInvoiceByBatch','Entry');
				
				$this->getstudentsByList($studentList,$batch);
				
				$this->fncreateBatchEntry($batch,$status);
				
				//update batch status
				//$status = $this->lobjmaintenModel->fngetDefKeyValue('StatusInvoiceByBatch','Entry');
				//$this->fnupdateBatchEntry($batch,$status);
			
				// now change the process status in batch table
				$statusdet['ProcessStatus'] = 1;
				$table = 'tbl_bill_batch';
				$where['IdBillBatch'] = $batch['IdBillBatch'];
				$this->updateProcessstatus($table,$where,$statusdet);
			}

		}
	}


	// function to update the status of the table
	public function updateProcessstatus($table,$where,$status){
		$Wherecond = '';
		foreach($where as $key=>$val){
			if($Wherecond == ''){
				$Wherecond = "  $key ='".$val."' ";
			}else{
				$Wherecond = $Wherecond.'AND'."  $key ='".$val."' ";
			}
		}

		$this->lobjDbAdpt->update($table,$status, $Wherecond);
	}

	// Function to update the batch entry
	public function fncreateBatchEntry($batch,$status){
		$larrbatchDet = $this->getbatchdetail($batch['BatchNo']);
		$larrBillBatch = $larrBillBatchUp = array();
		$larrcreatedname = $this->fngetuserdIdname($batch['UpdUser']);
		$larrBillBatch['BCHBILLNUM'] = $batch['BatchNo'];
		$larrBillBatch['IdBillBatch'] = $batch['IdBillBatch'];
		$larrBillBatch['BCHBRCHID'] = $larrbatchDet[0]['BRANCH_ID'];
		$larrBillBatch['ID_CREATE'] = $larrcreatedname[0]['loginName'];
		$larrBillBatch['DT_CREATE'] = date ( 'Y-m-d');
		$larrBillBatch['TOTALREC'] = $larrbatchDet[0]['detamounttotal'];
		$larrBillBatch['TOTAL_AMOUNT'] = $larrbatchDet[0]['totalamt'];
		$larrBillBatch['STS'] = $status[0]['value'];
		$larrBillBatch['Status'] = $status[0]['key'];
		$larrBillBatch['CreatedBy'] = $batch['UpdUser'];

		$larrBillBatchUp['STS'] = $status[0]['value'];
		$larrBillBatchUp['Status'] = $status[0]['key'];

		$table = 'tbl_billbatch';
		//insert the array of billbatch
		$this->lobjDbAdpt->insert($table,$larrBillBatch);


	}


	public function fnupdateBatchEntry($batch,$status){

		$larrBillBatch = $larrBillBatchUp = array();
		$larrBillBatchUp['STS'] = $status[0]['value'];
		$larrBillBatchUp['Status'] = $status[0]['key'];
		$table = 'tbl_billbatch';
		// if batch no exists then update the entry with the "Entry status" else insert it with "in-progress status"
		$select = $this->lobjDbAdpt->select()
									->from(array("a" => "tbl_billbatch"), array("a.AUTOINC"))
									->where('a.IdBillBatch =?',$batch['IdBillBatch']);
		
		$result = $this->lobjDbAdpt->fetchAll($select);
		if(count($result)>0) 
		{
			$where_up = "IdBillBatch='".$batch['IdBillBatch']."'";
			$this->lobjDbAdpt->update($table,$larrBillBatchUp,$where_up);
		 }
	}



	// function to process invoice generation for individual student
	public function getstudentsByList($studentList,$batch){
		$this->i = 0;
		foreach($studentList as $student){
			// Now generate this student invoice by fee structure
			$this->generateStudentInvoice($student,$batch);
			// Now insert batch detail
		}
	}

	// function to get the students of any batch
	public function getStudentsofBatch($batchId){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_bill_batch_invoice"), array("a.*"))
		->where('a.IdBillBatch =?',$batchId);
		return $result = $this->lobjDbAdpt->fetchAll($select);
	}

	// function to generate the invoice of student
	public function generateStudentInvoice($student,$batch)
	{
		//echo 'Record for the student : '.$student['IdStudent'];
		$lobjstudeRegModel = new Registration_Model_DbTable_Seniorstudentregistration();
		// Get student registration detail
		$larrstudentDet = $lobjstudeRegModel->getStudentRegDetail($student['IdStudent']);
		
	
		
		$wheerarray = array();
		// Now get the feestructure of the student
		$lobjInvoiceFeestruModel = new Studentfinance_Model_DbTable_Invoicefeestructure();
		// Now prepare where condition array on which we will search the data in fee structure for student
		$wheerarray['IdScheme'] = $larrstudentDet['IdScheme'];
		$wheerarray['IdBranch'] = $larrstudentDet['IdBranch'];
		$wheerarray['IdIntakeFrom'] = $larrstudentDet['IdIntake'];
		$wheerarray['StudentCategory'] = $batch['IdCategory'];
		
		
		$larrfeeStuctures = $lobjInvoiceFeestruModel->fnsearchFeestructure($wheerarray,$batch);
	
		if (empty($larrfeeStuctures))
		{
			throw new Exception ('Fee Structures not found.');
		}
		
	
		//asd($larrfeeStuctures);
		// now create student invoice according to feestructure
		if(!empty($larrfeeStuctures))
		{
			//check student category of fee structure
			if ( $this->checkStudentCategory($larrstudentDet['Nationality'],$larrfeeStuctures[0]['StudentCategory']) )
			{
				$lobjInvoiceFeestruModel->fnInsertInvoiceOfstudent($larrfeeStuctures,$larrstudentDet,$batch);
			}
		}
	}

	// function to get the detail of batch from batch bill number
	public function getbatchdetail($batchNo){
		$select = $this->lobjDbAdpt->select()
					->from(array("a" => "tbl_invoicemain"), array("SUM(a.TOTAL_AMOUNT) as totalamt",'a.BRANCH_ID'))
					->joinLeft(array('b'=>'tbl_invoicedetl'),'a.AUTOINC= b.BILLNUM',array('count(b.TOTAL_AMOUNT) as detamounttotal'))
					->where("a.BCHBILLNUM = ?",$batchNo);
	
		return $result = $this->lobjDbAdpt->fetchAll($select);
	}


	public function fnSearchInvoiceByBatchApproval($larrformData) {

		//$lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		$larrDefId = $this->lobjmaintenModel->fngetDefKeyValue('StatusInvoiceByBatch','Entry');
		$keyID = $larrDefId[0]['key'];

		$select = $this->lobjDbAdpt->select()
							->from(array("a" => "tbl_billbatch"), array("a.AUTOINC","a.CreatedBy","a.ID_CREATE",
									"a.DT_CREATE","a.TOTALREC","a.TOTAL_AMOUNT","a.STS","a.Status","a.BCHBILLNUM","a.CANCL_REMARK"));

		if(isset($larrformData['BatchNumber']) && !empty($larrformData['BatchNumber']) ){
			$select = $select->where("a.BCHBILLNUM LIKE  '%".$larrformData['BatchNumber']."%'");
		}

		if(isset($larrformData['CreatedBy']) && !empty($larrformData['CreatedBy'])){
			$select = $select->where("a.CreatedBy = ?",$larrformData['CreatedBy']);
		}

		if(isset($larrformData['StartDate']) && !empty($larrformData['StartDate'])){
			$select = $select->where("a.DT_CREATE >= ?",$larrformData['StartDate']);
		}

		if(isset($larrformData['EndDate']) && !empty($larrformData['EndDate'])){
			$select = $select->where("a.DT_CREATE <= ?",$larrformData['EndDate']);
		}

		if(isset($larrformData['StartDate']) && !empty($larrformData['StartDate']) && isset($larrformData['EndDate']) && !empty($larrformData['EndDate'])  ){
			$select = $select->where("a.DT_CREATE between '".$larrformData['StartDate']."' AND '".$larrformData['EndDate']."' ");
		}

		$select->where("a.Status = ?",$keyID);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
		/* if(count($result)>0){
			$i = 0;
			$j = 0;
			$finalarray = array();
			foreach($result as $value){
				$abc =  $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_invoicemain"), array("SUM(a.TOTAL_AMOUNT) as totalAMT","COUNT(a.AUTOINC) as totalNUM"));
				$abc->where("a.BCHBILLNUM = ?",$value['BatchNo']);
				$result1 = $this->lobjDbAdpt->fetchRow($abc);
				//echo "<pre>"; print_r($result1);
				if(count($result1) != 0){
					$finalarray[$i] = $value;
					$finalarray[$i]['totalAMT'] = $result1['totalAMT'];
					$finalarray[$i]['totalNUM'] = $result1['totalNUM'];
				}
				$i++;
			}
			return $finalarray;
		} */
	}

	public function fnApproveInvoiceByBatch($larrformData,$userId,$UpdDate){

		$result = $this->fngetuserdIdname($userId);

		$larrDefId = $this->lobjDefCodeModel->fngetDefKeyValue('StatusInvoiceByBatch','Approve');
		$keyID = $larrDefId[0]['key'];

		$lIntlen = count($larrformData['chkinvoicebybatch']);
		$paramArray = array();
		for($i=0;$i<$lIntlen;$i++){
			$invoicebybatch = $larrformData['chkinvoicebybatch'][$i];
			$split  = explode('#',$invoicebybatch);
			$batchnumber = $split[0];
			$remark = $split[1];
			//Table Batch Update
			$paramArray['STS'] = 'Approve';
			$paramArray['Status'] = $keyID;
			if(isset($result[0]['loginName'])){
				$paramArray['ID_APPROV'] = $result[0]['loginName'];
			}
			$paramArray['ApprovedBy'] = $userId;
			$paramArray['DT_APPROV'] = $UpdDate;
			$paramArray['CANCL_REMARK'] = $larrformData['chkremark'][$remark];
			$where_up = "BCHBILLNUM ='".$batchnumber."' ";
			$this->lobjDbAdpt->update('tbl_billbatch',$paramArray,$where_up);
			//Table Invoice Update
			$paramArray1['STATUS'] = '1';
			$where_up1 = "BCHBILLNUM ='".$batchnumber."' ";
			$this->lobjDbAdpt->update('tbl_invoicemain',$paramArray1,$where_up1);
			//Table Batch Audit Trial Update
			$paramArray2['BatchNo'] = $batchnumber;
			$paramArray2['IdBillBatch'] = $larrformData['idbillbatch'][$i];
			$paramArray2['Status'] = $keyID;
			$paramArray2['ProcessedBy'] = $userId;
			$paramArray2['ProcessedDate'] = $UpdDate;
			$this->lobjDbAdpt->insert('tbl_bill_batch_audit',$paramArray2);
		}
	}

	public function fngetuserdIdname($userId){
		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_user"), array("a.loginName"));
						$select->where("a.iduser = ?",$userId);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fnRejectInvoiceByBatch($larrformData,$userId,$UpdDate){

		$result = $this->fngetuserdIdname($userId);

		$larrDefId = $this->lobjDefCodeModel->fngetDefKeyValue('StatusInvoiceByBatch','Reject');
		$keyID = $larrDefId[0]['key'];
		$lIntlen = count($larrformData['chkinvoicebybatch']);
		$paramArray = array();
		for($i=0;$i<$lIntlen;$i++){
			$invoicebybatch = $larrformData['chkinvoicebybatch'][$i];
			$split  = explode('#',$invoicebybatch);
			$batchnumber = $split[0];
			$remark = $split[1];
			//Table Batch Update
			$paramArray['STS'] = 'Reject';
			$paramArray['Status'] = $keyID;
			if(isset($result[0]['loginName'])){
				$paramArray['ID_CANCEL'] = $result[0]['loginName'];
			}
			$paramArray['CancelBy'] = $userId;
			$paramArray['DT_CANCEL'] = $UpdDate;
			$paramArray['CANCL_REMARK'] = $larrformData['chkremark'][$remark];
			$where_up = "BCHBILLNUM ='".$batchnumber."' ";
			$this->lobjDbAdpt->update('tbl_billbatch',$paramArray,$where_up);
			//Table Invoice Update
			$paramArray1['STATUS'] = 2;
			$paramArray1['ID_CANCEL'] = $userId;
			$paramArray1['DT_CANCEL'] = $UpdDate;
			$where_up1 = "BCHBILLNUM ='".$batchnumber."' ";
			$this->lobjDbAdpt->update('tbl_invoicemain',$paramArray1,$where_up1);
			//Table Batch Audit Trial Update
			$paramArray2['BatchNo'] = $batchnumber;
			$paramArray2['IdBillBatch'] = $larrformData['idbillbatch'][$i];
			$paramArray2['Status'] = $keyID;
			$paramArray2['ProcessedBy'] = $userId;
			$paramArray2['ProcessedDate'] = $UpdDate;
			$this->lobjDbAdpt->insert('tbl_bill_batch_audit',$paramArray2);
		}
	}

	public function fngetinvoicebybatch($appId) {

		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_billbatch"), array(""))
						->joinLeft(array('b'=>'tbl_invoicemain'),'a.BCHBILLNUM = b.BCHBILLNUM',array('b.TOTAL_AMOUNT','b.BCHBILLNUM','b.BILLNUM','b.STUDID','b.STATUS','b.AUTOINC','b.CURRENCY','b.BILLDESC','b.TOTAL_BLNC','b.ID_CREATE','b.SemesterID','b.SOURCE','b.DT_CREATE'))
						->joinLeft(array('c'=>'tbl_user'),'c.iduser = b.ID_CREATE',array('c.loginName'))
						->joinLeft(array('d'=>'tbl_currency'),'d.AUTOINC = b.CURRENCY',array('d.CURRDESC'));
		$select->where("a.AUTOINC = ?",$appId);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fnSearchInvoiceByBatch($post = array()) {

		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_billbatch"), array("a.AUTOINC","a.CreatedBy","a.ID_CREATE","a.ProcessStatus",
								"a.DT_CREATE","a.TOTALREC","a.TOTAL_AMOUNT","a.STS","a.Status","a.BCHBILLNUM","a.CANCL_REMARK"));

		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where("a.BCHBILLNUM LIKE  '%".$post['field2']."%'");
		}

		if(isset($post['field1']) && !empty($post['field1'])){
			$select = $select->where("a.CreatedBy = ?",$post['field1']);
		}

		if(isset($post['field5']) && !empty($post['field5'])){
			$select = $select->where("a.Status = ?",$post['field5']);
		}

		if(isset($post['field57']) && !empty($post['field57'])){
			$select = $select->where("a.DT_CREATE >= ?",$post['field57']);
		}

		if(isset($post['field58']) && !empty($post['field58'])){
			$select = $select->where("a.DT_CREATE <= ?",$post['field58']);
		}

		if(isset($post['field57']) && !empty($post['field57']) && isset($post['field58']) && !empty($post['field58'])  ){
			$select = $select->where("a.DT_CREATE between '".$post['field57']."' AND '".$post['field58']."' ");
		}
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fncheckBankNo($code){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_bill_batch"), array("a.IdBillBatch"))
		->where('a.BatchNo =?',$code);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	

	public function getStudent($data,$key=0) 
	{

		if ( $key == 0 )
		{
			$lstrSelect = $this->lobjDbAdpt->select()
			->from(array('a' => 'tbl_studentregistration'),array('key' => "CONCAT_WS('_',IFNULL(a.IdStudentRegistration,''),IFNULL(a.FName,''),IFNULL(a.LName,''))", 'name' => "a.registrationId"));
		}
		else
		{
			$lstrSelect = $this->lobjDbAdpt->select()
			->from(array('a' => 'tbl_studentregistration'),array('key' => "a.IdStudentRegistration", 'name' => "a.registrationId"));	
		}
		
		$lstrSelect->where("a.profileStatus IN (92,248,253)"); //Active,Defer,Dormant
		if ($data['IdSemester'] != '') 
		{
			$where = "a.IdSemesterDetails = '" . $data['IdSemester'] . "' OR a.IdSemesterMain = '" . $data['IdSemester'] . "'";
			$lstrSelect = $lstrSelect->where($where);
		}

		if ( $data['IdProgram'] != '' )
		{
			$where = $this->lobjDbAdpt->quoteInto("a.IdProgram = ?",$data['IdProgram']);
			$lstrSelect = $lstrSelect->where($where);
		}
		
		if ( $data['IdBranch'] != '' )
		{
			$where = $this->lobjDbAdpt->quoteInto("a.IdBranch = ?",$data['IdBranch']);
			$lstrSelect = $lstrSelect->where($where);
		}
		

		if ( $data['IdIntake'] != '' )
		{
			$where = $this->lobjDbAdpt->quoteInto("a.IdIntake = ?",$data['IdIntake']);
			$lstrSelect = $lstrSelect->where($where);
		}
	
		//process student category
		if ( $data['StudentCategory'] != '')
		{
			$where = $this->StudentCategoryFilter($data['StudentCategory'],"a.Nationality");
			$lstrSelect = $lstrSelect->where($where);
		}
		
	
	
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
	
	/*
	 * Check Student category
	 * return true/false
	 */
	function checkStudentCategory( $nationality , $StudentCategory )
	{
		
		if ( !isset($this->StudentCat) )
		{
			$this->StudentCat = $this->lobjdeftype->fnGetDefinationCodeList('Student Category');
		}
		
		if ( !isset($this->CountryCat) )
		{
			$get_CC_FR = $this->lobjdeftype->fngetDefKeyValue('Country Category', 'FR');
			$get_CC_AR = $this->lobjdeftype->fngetDefKeyValue('Country Category', 'AR');
			
			$this->CountryCat['FR'] = $get_CC_FR['key'];
			$this->CountryCat['AR'] = $get_CC_AR['key'];
		}
		
		if ( !empty($this->StudentCat) )
		{
			foreach ( $this->StudentCat as $cat )
			{
				$list[$cat['value']] = $cat['key'];
			}
		
			
			switch( $StudentCategory )
			{
				case $list['All']:
					return true;
				break;
				
				case $list['Arabian']:
					$country = $this->lobjDbAdpt->quoteInto('CountryCategory = ?', $this->CountryCat['AR'].' AND CountryName != \'YEMEN\' AND idCountry = '.$nationality);
					$countries = $this->lobjDbAdpt->fetchRow('SELECT idCountry FROM `tbl_countries` WHERE '.$country);
					if ( !empty($countries) )
					{
						return true;
					}
				break;
				
				case $list['Others']:
					$country = $this->lobjDbAdpt->quoteInto('CountryCategory = ?', $this->CountryCat['FR'].' AND idCountry = '.$nationality);
					$countries = $this->lobjDbAdpt->fetchRow('SELECT idCountry FROM `tbl_countries` WHERE '.$country);
					if ( !empty($countries) )
					{
						return true;
					}
				break;
				
				case $list['Yemeni']:
					$country = $this->lobjDbAdpt->quoteInto('CountryIso = ?', 'YE');
					$countries = $this->lobjDbAdpt->fetchRow('SELECT idCountry FROM `tbl_countries` WHERE '.$country);
				
					if ( $nationality == $countries['idCountry'] )
					{
						return true;
					}
				break;
			}
		}
		
		
		return false;
	}
	
	/*
	 * student category filter by field
	 * return WHERE
	 */
	function StudentCategoryFilter($StudentCategory, $field='')
	{
		$list = array();
		if ( !isset($this->StudentCat) )
		{
			$this->StudentCat = $this->lobjdeftype->fnGetDefinationCodeList('Student Category');
		}
		
		if ( !isset($this->CountryCat) )
		{
			$get_CC_FR = $this->lobjdeftype->fngetDefKeyValue('Country Category', 'FR');
			$get_CC_AR = $this->lobjdeftype->fngetDefKeyValue('Country Category', 'AR');
			
			$this->CountryCat['FR'] = $get_CC_FR['key'];
			$this->CountryCat['AR'] = $get_CC_AR['key'];
		}
		
		if ( !empty($this->StudentCat) )
		{
			foreach ( $this->StudentCat as $cat )
			{
				$list[$cat['value']] = $cat['key'];
			}
			
			switch( $StudentCategory )
			{
				case $list['All']:
					return '';
				break;
				
				case $list['Arabian']:
					$country = $this->lobjDbAdpt->quoteInto('CountryCategory = ?', $this->CountryCat['AR']);
					return $field.' IN ( SELECT idCountry FROM `tbl_countries` WHERE '.$country.' AND CountryIso != \'YE\')';
				break;
				
				case $list['Others']:
					$country = $this->lobjDbAdpt->quoteInto('CountryCategory = ?', $this->CountryCat['FR']);
					return $field.' IN ( SELECT idCountry FROM `tbl_countries` WHERE '.$country.')';
				break;
				
				case $list['Yemeni']:
					$country = $this->lobjDbAdpt->quoteInto('CountryIso = ?', 'YE');
					return $field.' IN ( SELECT idCountry FROM `tbl_countries` WHERE '.$country.')';
				break;
			}
		}
		
	}


	public function fnSearchInvoiceByBatchMain($post = array()) {

		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_bill_batch"), array("a.*"))
						->joinLeft(array("b" => "tbl_fee_category"), 'a.IdFeeCategory = b.IdFeeCategory', array("b.FeeCategoryCode"))
						->joinLeft(array("c" => "tbl_fee_setup"), 'c.IdFeeSetup = a.IdFeeCode', array("c.FeeCode"))
						->joinLeft(array("d" => "tbl_definationms"), 'd.idDefinition = a.ChargingPeriod', array("d.DefinitionCode as ChargingPeriod"))
						->joinLeft(array("e" => "tbl_scheme"), 'e.IdScheme = a.IdScheme', array("e.EnglishDescription as Scheme"))
						->joinLeft(array("f" => "tbl_program"), 'f.IdProgram = a.IDProgram', array("f.ProgramName"));

		if(isset($post['field2']) && !empty($post['field2']) ){
			$select->where("a.BatchNo LIKE  '%".$post['field2']."%'");
		}

		if(isset($post['field1']) && !empty($post['field1'])){
			$select->where("a.SemesterCode = ?",$post['field1']);
		}

		if(isset($post['field23']) && !empty($post['field23'])){
			$select->where("a.IdFeeCategory = ?",$post['field23']);
		}

		if(isset($post['field24']) && !empty($post['field24'])){
			$select->where("a.IdFeeCode = ?",$post['field24']);
		}

		if(isset($post['field25']) && !empty($post['field25'])){
			$select->where("a.ChargingPeriod = ?",$post['field25']);
		}

		if(isset($post['field26']) && !empty($post['field26'])){
			$select->where("a.IdScheme = ?",$post['field26']);
		}

		$select->group("a.BatchNo")->order('a.BatchNo DESC');
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}


	public function fngetBatchCode($id) {
		$select = $this->lobjDbAdpt->select()->from(array("a" => "tbl_bill_batch"), array("a.BatchNo"))->where("a.IdBillBatch = ?",$id);
		$result = $this->lobjDbAdpt->fetchAll($select);
		if(count($result)>0) {
			 return $result[0]['BatchNo'];
		} else { return '';}
	}


	public function fngetBatchDetails($appId) {
		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_billbatch"), array("a.*"))
						//->joinLeft(array('b'=>'tbl_invoicemain'),'a.BCHBILLNUM = b.BCHBILLNUM',array('b.TOTAL_AMOUNT','b.BCHBILLNUM','b.BILLNUM','b.STUDID','b.STATUS','b.AUTOINC','b.CURRENCY','b.BILLDESC','b.TOTAL_BLNC','b.ID_CREATE','b.SemesterID','b.SOURCE','b.DT_CREATE'))
						//->joinLeft(array('c'=>'tbl_user'),'c.iduser = b.ID_CREATE',array('c.loginName'))
						//->joinLeft(array('d'=>'tbl_currency'),'d.AUTOINC = b.CURRENCY',array('d.CURRDESC'))
						;
		$select->where("a.BCHBILLNUM = ?",$appId);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}



}