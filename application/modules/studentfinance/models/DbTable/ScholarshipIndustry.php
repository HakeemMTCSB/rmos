<?php

class Studentfinance_Model_DbTable_ScholarshipIndustry extends Zend_Db_Table {

    protected $_name = 'tbl_scholarship_industry';
    protected $_primary = 'id';

    public function save(array $data){

        if( isset($data[$this->_primary]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }
}