<?php
class Studentfinance_Model_DbTable_Checkpayments extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_verifypayments';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function fnGetStudentNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('stud'=>'tbl_studentapplication'),array("key"=>"stud.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(stud.FName,''),IFNULL(stud.MName,''),IFNULL(stud.LName,''))"))								  
                       			  ->where('stud.Active = 1')
                       			  ->order("value");
								  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	public function fnGetSemesterNameList(){
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('a'=>'tbl_semester'),array("key"=>"a.IdSemester","value"=>"CONCAT_WS(' ',IFNULL(b.SemesterMasterName,''),IFNULL(a.year,''))"))
								 ->join(array('b' => 'tbl_semestermaster'),'a.Semester = b.IdSemesterMaster ')
								  ->where('a.Active = 1')
								  ->where('b.Active = 1')
								  ->order("a.year");
								  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetStudentApplicationDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								 ->from(array('stud' => 'tbl_studentapplication'),array("stud.IdApplication","stud.EmailAddress","CONCAT_WS(' ',IFNULL(stud.FName,''),IFNULL(stud.MName,''),IFNULL(stud.LName,'')) as StudentName"))
       								 ->where('stud.Active = 1')
       								->order("StudentName");
       					//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
	public function fnSearchStudentApplicationDetails($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								 ->from(array('stud' => 'tbl_studentapplication'),array("stud.IdApplication","stud.EmailAddress","CONCAT_WS(' ',IFNULL(stud.FName,''),IFNULL(stud.MName,''),IFNULL(stud.LName,'')) as StudentName"))
       								 ->where('stud.EmailAddress like "%" ? "%"',$post['field2']) ;
       							
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("stud.IdApplication = ?",$post['field5']);
										
			}	
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	public function fnviewVerifyPaymentDetails($IdApplication) { //Function for the view user 
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("vp" => "tbl_verifypayments"),array("vp.*"))	
						->join(array('a'=>'tbl_semester'),'vp.IdSemester = a.IdSemester',array("CONCAT_WS(' ',IFNULL(b.SemesterMasterName,''),IFNULL(a.year,'')) as SemName"))
						->join(array('b' => 'tbl_semestermaster'),'a.Semester = b.IdSemesterMaster')	
						->where("vp.IdApplication  = ?",$IdApplication)
						->group("vp.IdSemester");
		return $result = $lobjDbAdpt->fetchAll($select);
    }
	public function fnSearchSem($IdApplication,$IdSemester) { //Function for the view user 
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("vp" => "tbl_verifypayments"),array("vp.*"))
						->where("vp.IdSemester  = ?",$IdSemester)
						->where("vp.IdApplication  = ?",IdApplication);
		return $result = $lobjDbAdpt->fetchRow($select);
    }
	
  
 
	public function fnaddVerfyPayments($larrformData) { //Function for adding the user details to the table
		$this->insert($larrformData);
	}
	
    
    public function fnupdateCharges($lintiIdCharges,$larrformData) { //Function for updating the user   
	$where = 'IdCharges = '.$lintiIdCharges;
	$this->update($larrformData,$where);
    }
    
	public function fnGetCharges($IdCharges){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_charges"))
				 				 ->where("a.IdCharges = ?",$IdCharges);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
}