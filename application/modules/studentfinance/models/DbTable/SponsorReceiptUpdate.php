<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 17/10/2016
 * Time: 2:19 PM
 */
class Studentfinance_Model_DbTable_SponsorReceiptUpdate extends Zend_Db_Table_Abstract {

    public function getSponsorReceipt($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('r'=>'sponsor_invoice_receipt'),array('*','rcp_UpdDate'=>'UpdDate'))
            ->joinLeft(array('rb'=>'sponsor_invoice_batch'), 'rb.id = r.rcp_batch_id')
            ->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = r.rcp_cur_id')
            ->joinLeft(array('sp'=>'tbl_sponsor'), 'sp.idsponsor = r.rcp_type_id and r.rcp_type = 1',array("SponsorCode",'SponsorName'=>'fName'))
            ->joinLeft(array('sc'=>'tbl_scholarship_sch'), 'sc.sch_Id = r.rcp_type_id and r.rcp_type = 2',array("ScholarCode"=>"sch_code",'ScholarName'=>'sch_name'))
            ->joinLeft(array('u'=>'tbl_user'), 'u.iduser = r.rcp_create_by', array())
            ->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
            ->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = r.cancel_by', array())
            ->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancel_name'=>'tsa.FullName'))
            ->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = r.UpdUser', array())
            ->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('update_name'=>'tsb.FullName'))
            ->join(array('ac'=>'tbl_account_code'), 'ac.ac_id = r.rcp_account_code')
            ->joinLeft(array('pm'=>'payment_mode'), 'pm.id = r.p_payment_mode_id', array('payment_mode'=>'payment_mode'))
            ->joinLeft(array('t'=>'creditcard_terminal'), 't.id = r.p_terminal_id', array('terminal_id'))
            ->where('r.rcp_status = ?', 'APPROVE')
            ->group('r.rcp_id')
            ->order('r.rcp_date desc');

        if ($search != false){
            if (isset($search['rcp_no']) && $search['rcp_no']!=''){
                $select->where($db->quoteInto('r.rcp_no like ?', '%'.$search['rcp_no'].'%'));
            }

            if (isset($search['uti_date']) && $search['uti_date']!=''){
                $select->where($db->quoteInto('r.rcp_receive_date = ?', date('Y-m-d', strtotime($search['uti_date']))));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateSponsorReceipt($data , $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_invoice_receipt', $data, 'rcp_id = '.$id);
        return $update;
    }
}