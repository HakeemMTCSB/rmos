<?php

class Studentfinance_Model_DbTable_ScholarshipAcademicWeight extends Zend_Db_Table {

    protected $_name = 'tbl_scholarship_academic_weight';
    protected $_primary = 'id';


    public function save(array $data){

        if( isset($data[$this->_primary]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }

    /**
     * retrieves score based on $id
     */
    public function get_point( $id ) {

        $select = $this->select()
            ->where('id = ?', $id)
        ;
        $rule = $this->fetchRow($select);
        if(empty($rule)) {
            return 0;
        } else {
            return $rule->score;
        }

    }
}