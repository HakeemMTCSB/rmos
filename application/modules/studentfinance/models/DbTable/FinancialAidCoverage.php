<?php

class Studentfinance_Model_DbTable_FinancialAidCoverage extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_financial_aid_coverage';
    protected $_primary = "id";

    protected $_referenceMap    = array(
        'FinancialAid' => array(
            'columns'           => 'financial_aid_id',
            'refTableClass'     => 'Studentfinance_Model_DbTable_FinancialAid',
            'refColumns'        => 'id'
        ),
        'FeeItem' => array(
            'columns'           => 'fee_code',
            'refTableClass'     => 'Studentfinance_Model_DbTable_FeeItem',
            'refColumns'        => 'fi_id'
        ),
        'Semester' => array(
            'columns'           => 'semester_id',
            'refTableClass'     => 'Records_Model_DbTable_SemesterMaster',
            'refColumns'        => 'IdSmesterMaster'
        )

    );

    public function deleteFee($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->delete($this->_name, $this->_primary.' = '.$id);
    }
}