<?php
class Studentfinance_Model_DbTable_PaymentHistory extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'transaction_migs';
	protected $_primary = "mt_id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fi'=>$this->_name))
					->joinLeft(array('ap'=>'applicant_profile'),'ap.appl_id=fi.mt_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
					->joinLeft(array('cur'=>'tbl_currency'),'cur.cur_id=fi.mt_currency_id',array('cur.cur_code'));
		
		if($id!=0){
			$selectData->where("fi.mt_id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
			
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getPaginateData($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$codeid = $searchData['receipt_id'];
		$name = $searchData['name'];
		$id = $searchData['id'];
		$type = $searchData['type'];
		$status = $searchData['status'];
		
			$selectData = $db->select()
					->from(array('fi'=>$this->_name))
					->joinLeft(array('cur'=>'tbl_currency'),'cur.cur_id=fi.mt_currency_id',array('cur.cur_code'))
					->joinLeft(array('r'=>'receipt'),'r.migs_id = fi.mt_id',array('rcp_no','rcp_status','idReceipt'=>'rcp_id'))
					->order('fi.mt_id DESC');	
		
		
	if($type == 645){
				$selectData->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=fi.mt_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=st.at_trans_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','applicant_program'=>'ProgramCode'))
					->where("fi.mt_trans_id is not null")
					->where("mt_idStudentRegistration is null");
					
				if($id){
					$selectData->where("st.at_pes_id like ?",'%'.$id.'%');
				}
				if($name){
					$selectData->where("concat_ws(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) like ?", '%'.$name.'%');
				}
				
			
		}
		
		if($type == 646){
				$selectData->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=mt_idStudentRegistration', array('sr.registrationId','at_pes_id'=>'sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName','applicant_program'=>'ProgramCode'))
					->where("mt_idStudentRegistration is not null");
				if($id ){
					$selectData->where("sr.registrationId like ?",'%'.$id.'%');
				}
				if($name){
					$selectData->where("concat_ws(' ',sp.appl_fname,sp.appl_mname,sp.appl_lname) like ?", '%'.$name.'%');
				}
			
		}	
		if(isset($searchData['date_from']) && $searchData['date_from']!=''){
			$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
			$selectData->where("DATE(fi.mt_txn_date) >= ?",$dateFrom);
		}
		
		if(isset($searchData['date_to']) && $searchData['date_to']!=''){
			$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
			$selectData->where("DATE(fi.mt_txn_date) <= ?",$dateTo);
		}
			
		if($codeid!=''){
			$selectData->where("fi.mt_txn_no like ?",'%'.$codeid.'%');
		}	
		
		if($status!=''){
			$selectData->where("fi.mt_status like ?",$status);
		}
				
//		echo $selectData;
		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
		return $row;
	}
}