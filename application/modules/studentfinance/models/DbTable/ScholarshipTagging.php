<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_Model_DbTable_ScholarshipTagging extends Zend_Db_Table_Abstract {
    
    //put your code here
    protected $_name = 'tbl_scholarshiptagging_sct';
    protected $_primary = "sct_Id";
    
    /*
     * list of Scholarhip tagging
     * 
     * @on 17/6/2014
     */
    public function getScholarshipTagList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_scholarshiptagging_sct"),array("value"=>"a.*"))
            ->order("a.sct_Id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get Scholarship tagging based on id
     * 
     * @on 17/6/2014
     */
    public function getScholarshipTagById($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_scholarshiptagging_sct"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_program_scheme'), 'a.sct_programscheme = b. IdProgramScheme')
            ->where('a.sct_Id = ?',$id)
            ->order("a.sct_Id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get Scholarhiptagging based on intake
     * 
     * @on 17/6/2014
     */
    public function getScholarshipTagBasedOnIntake($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_scholarshiptagging_sct"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_program_scheme'), 'a.sct_programscheme = b. IdProgramScheme')
            ->join(array('c'=>'tbl_scholarship_sch'), 'a.sct_schId = c.sch_Id')
            ->join(array('d'=>'tbl_program'), 'a.sct_program = d.IdProgram')
            ->where('a.sct_intake = ?',$id)
            ->order("a.sct_Id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get Scholarship tagging based on id
     * 
     * @on 11/7/2014
     */
    public function getIntakeInfo($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_intake"),array("value"=>"a.*"))
            ->where('a.IdIntake = ?',$id)
            ->order("a.IdIntake");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
}