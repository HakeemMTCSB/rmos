<?php
class Studentfinance_Model_DbTable_RefundCheque extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'refund_cheque';
	protected $_primary = "rchq_id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('rc'=>$this->_name))
					->join(array('r'=>'refund'), 'r.rfd_id = rc.rchq_rfd_id')
					
					->joinLeft(array('cur' => 'tbl_currency'), 'r.rfd_currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=r.rfd_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=r.rfd_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('tu'=>'tbl_user'), 'tu.iduser = rc.rchq_insert_by', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = tu.IdStaff', array('issuer_name'=>'Fullname'))
					->joinLeft(array('tu2'=>'tbl_user'), 'tu2.iduser = rc.rchq_collector_update_by', array())
					->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = tu2.IdStaff', array('collector_updater'=>'Fullname'))
					->joinLeft(array('tu3'=>'tbl_user'), 'tu3.iduser = rc.rchq_cancel_by', array())
					->joinLeft(array('ts3'=>'tbl_staffmaster'), 'ts3.IdStaff = tu3.IdStaff', array('cancel_by_name'=>'Fullname'));
		
		if($id!=0){
			$selectData->where("rc.rchq_id = ?",$id);
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getDataByRefundId($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('rc'=>$this->_name))
					->join(array('r'=>'refund'), 'r.rfd_id = rc.rchq_rfd_id')
					
					->joinLeft(array('cur' => 'tbl_currency'), 'r.rfd_currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=r.rfd_trans_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=r.rfd_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('tu'=>'tbl_user'), 'tu.iduser = rc.rchq_insert_by', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = tu.IdStaff', array('issuer_name'=>'Fullname'))
					->joinLeft(array('tu2'=>'tbl_user'), 'tu2.iduser = rc.rchq_collector_update_by', array())
					->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = tu2.IdStaff', array('collector_updater'=>'Fullname'))
					->joinLeft(array('tu3'=>'tbl_user'), 'tu3.iduser = rc.rchq_cancel_by', array())
					->joinLeft(array('ts3'=>'tbl_staffmaster'), 'ts3.IdStaff = tu3.IdStaff', array('cancel_by_name'=>'Fullname'));
		
		if($id!=0){
			$selectData->where("rc.	rchq_rfd_id = ?",$id);
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	
	public function getPaginateData(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('rc'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'), 'tu.iduser = rc.rchq_insert_by', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = tu.IdStaff', array('issuer_name'=>'Fullname'))
					->joinLeft(array('tu2'=>'tbl_user'), 'tu2.iduser = rc.rchq_collector_update_by', array())
					->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = tu2.IdStaff', array('collector_updater'=>'Fullname'))
					->joinLeft(array('tu3'=>'tbl_user'), 'tu3.iduser = rc.rchq_cancel_by', array())
					->joinLeft(array('ts3'=>'tbl_staffmaster'), 'ts3.IdStaff = tu3.IdStaff', array('cancel_by_name'=>'Fullname'))
					->joinLeft(array('def'=>'tbl_definationms'), 'def.idDefType = 88 and def.iDDefinition = rc.rchq_bank_name', array('bank_name'=>'def.BahasaIndonesia'));
					
		return  $selectData;
	}
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['rchq_insert_by'])){
			$data['rchq_insert_by'] = $auth->getIdentity()->iduser;
		}
		
		$data['rchq_insert_date'] = date('Y-m-d H:i:s');
		$data['rchq_status'] = "A";
			
        return parent::insert($data);
	}		
	
}

