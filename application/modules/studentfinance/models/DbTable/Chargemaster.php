<?php
class Studentfinance_Model_DbTable_Chargemaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_charges';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function fnGetChargeTypeList(){
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('a'=>'tbl_definationms'),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->join(array('b' => 'tbl_definationtypems'),'a.idDefType = b.idDefType')
								  ->where('b.defTypeDesc like ?','Charge Type')
								  ->where('a.Status = 1')
                       			  ->where('b.Active = 1')
                       			  ->order("a.DefinitionDesc");
								  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetAccountMasterList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('a'=>'tbl_accountmaster'),array("key"=>"a.idAccount","value"=>"a.AccountName"))								  
                       			  ->where('a.Active = 1')
                       			  ->order("a.AccountName");
								  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	
	public function fngetChargesDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("cm"=>"tbl_charges"),array("cm.*"))
       								->join(array('am' => 'tbl_accountmaster'),'cm.IdAccountMaster = am.idAccount',array("am.idAccount","am.AccountName"))
       								->join(array('pm' => 'tbl_program'),'cm.IdProgram = pm.IdProgram',array("pm.ProgramName","pm.IdProgram"))
       								->order("cm.IdAccountMaster");
       					//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
     
	public function fngetChargeslist() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("cm"=>"tbl_charges"),array("key"=>"cm.IdCharges","value"=>"cm.ChargeName"))
       								->order("cm.ChargeName");	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
     
	public function fnSearchCharges($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("cm"=>"tbl_charges"),array("cm.*"))
       								->join(array('am' => 'tbl_accountmaster'),'cm.IdAccountMaster = am.idAccount',array("am.idAccount","am.AccountName"))
       								->join(array('pm' => 'tbl_program'),'cm.IdProgram = pm.IdProgram',array("pm.ProgramName","pm.IdProgram"))
       								->order("cm.IdAccountMaster");
       							
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("cm.IdProgram = ?",$post['field5']);
										
			}	
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("cm.IdAccountMaster = ?",$post['field8']);
										
			}			
       	
       				
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
  
 
	public function fnaddCharges($larrformData) { //Function for adding the user details to the table
		$this->insert($larrformData);
	}
	
    public function fnviewCharges($lintIdCharges) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_charges"),array("a.*"))	
						->join(array('am' => 'tbl_accountmaster'),'a.IdAccountMaster = am.idAccount',array("am.idAccount","am.AccountName"))
       					->join(array('pm' => 'tbl_program'),'a.IdProgram = pm.IdProgram',array("pm.ProgramName","pm.IdProgram"))						
		            	->where("a.IdCharges= ?",$lintIdCharges);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
    
    public function fnupdateCharges($lintiIdCharges,$larrformData) { //Function for updating the user   
	$where = 'IdCharges = '.$lintiIdCharges;
	$this->update($larrformData,$where);
    }
    
	public function fnGetCharges($IdCharges){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_charges"))
				 				 ->where("a.IdCharges = ?",$IdCharges);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
 public function fngetValidateAccountName($IdAccountMaster) {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("c"=>"tbl_charges"),array("c.IdAccountMaster"))			
		            	->where("c.IdAccountMaster= ?",$IdAccountMaster);
		            	echo $select;die();	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
	
}