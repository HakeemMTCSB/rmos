<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_Model_DbTable_ScholarshipReport extends Zend_Db_Table_Abstract {
    
    public function getApplicationCourse($id, $return=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.sac_subjectid = b.IdSubject')
            ->where('a.sac_said = ?', $id)
            ->where('b.IdSubject <> ?', 84)
            ->where('b.IdSubject <> ?', 85)
            ->where('b.IdSubject <> ?', 126)
            ->group('a.sac_subjectid');

        if ($return == 0){
            $result = $db->fetchAll($select);
        }else{
            $result = $db->fetchRow($select);
        }
        
        return $result;
    }
    
    public function getReApplicationCourse($id, $return=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.src_subjectid = b.IdSubject')
            ->where('a.src_sreid = ?', $id)
            ->where('b.IdSubject <> ?', 84)
            ->where('b.IdSubject <> ?', 85)
            ->where('b.IdSubject <> ?', 126)
            ->group('a.src_subjectid');

        if ($return == 0){
            $result = $db->fetchAll($select);
        }else{
            $result = $db->fetchRow($select);
        }
        
        return $result;
    }

    public function getFinancialAidList($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application'), array(
                'ID'=>'a.sa_id',
                'Student_Name'=>'concat(sp.appl_fname, " ", sp.appl_lname)',
                'Student_ID'=>'r.registrationId',
                'Status'=>'a.sa_status',
                'Semester_Applied'=>'concat(sem.SemesterMainName, " - ", sem.SemesterMainCode)',
                'App_Type'=>new Zend_Db_Expr('"Application"'),
                'Semester_Start_Date'=>'sem.SemesterMainStartDate'
            ))
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), 'a.sa_af_id = ApplicantFinancial.af_id', array())
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'a.sa_semester_id=sem.IdSemesterMaster', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array())
            ->joinLeft(array('r' => 'tbl_studentregistration'), 'a.sa_cust_id=r.IdStudentRegistration', array())
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id', array())
            ->joinLeft(array('pr'=>'tbl_program'), 'r.IdProgram = pr.IdProgram', array())
            ->joinLeft(array('prs'=>'tbl_program_scheme'), 'r.IdProgramScheme = prs.IdProgramScheme', array())
            ->joinLeft(array('df'=>'tbl_definationms'), 'prs.mode_of_program = df.idDefinition', array())
            ->where('a.sa_cust_type = ?', 1);
            //->order('a.sa_score ASC');

        if ($search != false){
            if (isset($search['Program']) && $search['Program']!=''){
                $select->where('r.IdProgram = ?', $search['Program']);
            }
            if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                $select->where('r.IdProgramScheme = ?', $search['ProgramScheme']);
            }
            if (isset($search['SemesterApplied']) && $search['SemesterApplied']!=''){
                $select->where('a.sa_semester_id = ?', $search['SemesterApplied']);
            }
            if (isset($search['StudentName']) && $search['StudentName']!=''){
                $select->where('concat(sp.appl_fname, " ", sp.appl_lname) like "%'.$search['StudentName'].'%"');
            }
            if (isset($search['StudentID']) && $search['StudentID']!=''){
                $select->where('r.registrationId like "%'.$search['StudentID'].'%"');
            }
            if (isset($search['Status']) && $search['Status']!=''){
                if ($search['Status']==2 || $search['Status']==7){
                    $select->where('a.sa_status IN (2, 7)');
                }else{
                    $select->where('a.sa_status = ?', $search['Status']);
                }
            }
        }

        $select2 = $db->select()
            ->from(array('a'=>'sponsor_reapplication'), array(
                'ID'=>'a.sre_id',
                'Student_Name'=>'concat(c.appl_fname, " ", c.appl_lname)',
                'Student_ID'=>'b.registrationId',
                'Status'=>'a.sre_status',
                'Semester_Applied'=>'concat(d.SemesterMainName, " - ", d.SemesterMainCode)',
                'App_Type'=>new Zend_Db_Expr('"Re-Application"'),
                'Semester_Start_Date'=>'d.SemesterMainStartDate'
            ))
            ->join(array('b'=>'tbl_studentregistration'), 'a.sre_cust_id=b.IdStudentRegistration', array())
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id', array())
            ->joinLeft(array('d'=>'tbl_semestermaster'), 'a.sre_semester_id=d.IdSemesterMaster', array())
            ->joinLeft(array('pr'=>'tbl_program'), 'b.IdProgram = pr.IdProgram', array())
            ->joinLeft(array('prs'=>'tbl_program_scheme'), 'b.IdProgramScheme = prs.IdProgramScheme', array())
            ->joinLeft(array('df'=>'tbl_definationms'), 'prs.mode_of_program = df.idDefinition', array());

        if ($search != false){
            if (isset($search['Program']) && $search['Program']!=''){
                $select2->where('b.IdProgram = ?', $search['Program']);
            }
            if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                $select2->where('b.IdProgramScheme = ?', $search['ProgramScheme']);
            }
            if (isset($search['SemesterApplied']) && $search['SemesterApplied']!=''){
                $select2->where('a.sre_semester_id = ?', $search['SemesterApplied']);
            }
            if (isset($search['StudentName']) && $search['StudentName']!=''){
                $select2->where('concat(c.appl_fname, " ", c.appl_lname) like "%'.$search['StudentName'].'%"');
            }
            if (isset($search['StudentID']) && $search['StudentID']!=''){
                $select2->where('b.registrationId like "%'.$search['StudentID'].'%"');
            }
            if (isset($search['Status']) && $search['Status']!=''){
                if ($search['Status']==2 || $search['Status']==7){
                    $select2->where('a.sre_status IN (2, 7)');
                }else{
                    $select2->where('a.sre_status = ?', $search['Status']);
                }
            }
        }

        $union = $db->select()
            ->union(array($select, $select2))
            ->order('Semester_Start_Date DESC')
            ->order('ID DESC');

        //echo $union; exit;

        $result = $db->fetchAll($union);
        return $result;
    }

    public function getSponsorPaid($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'), array('sponsorpaid'=>'sum(a.sp_amount)' , 'c.rcp_cur_id'))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
            ->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
            ->where('c.rcp_id = ?', $id)
            ->where('c.rcp_status = ?', 'APPROVE');
            //->where('a.sp_status = ?', 1);

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['sponsorpaid'] = 0.00;
            $result[0]['rcp_cur_id'] = 1;
        }

        return $result;
    }

    public function getSponsorPaidAdjustment($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'), array('sponsorpaid'=>'sum(a.sp_amount)' , 'c.rcp_cur_id'))
            ->join(array('b'=>'sponsor_invoice_receipt_invoice'), 'a.id = b.rcp_inv_sponsor_dtl_id')
            ->join(array('c'=>'sponsor_invoice_receipt'), 'b.rcp_inv_rcp_id = c.rcp_id')
            ->where('c.rcp_id = ?', $id)
            ->where('c.rcp_status = ?', 'APPROVE')
            ->where('a.sp_status = ?', 0);

        $result = $db->fetchAll($select);

        if ($result == null){
            $result[0]['sponsorpaid'] = 0.00;
            $result[0]['rcp_cur_id'] = 1;
        }

        return $result;
    }
}