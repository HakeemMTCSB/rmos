<?php
class Studentfinance_Model_DbTable_ItemCourse extends Zend_Db_Table_Abstract{

    public function getItemCourse($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'fee_structure_item_subject'))
            ->join(array('b'=>'tbl_subjectmaster'), 'a.fsisub_subject_id = b.IdSubject')
            ->join(array('c'=>'tbl_definationms'), 'a.fsisub_amount_calculation_type = c.idDefinition', array('fsisub_amount_calculation_type_name'=>'c.DefinitionDesc'))
            ->where('a.fsisub_fsi_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubject($code){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectmaster'))
            ->where('a.SubCode like ?', '%'.$code.'%');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addCourse($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('fee_structure_item_subject', $data);
        $id = $db->lastInsertId('fee_structure_item_subject', 'fsisub_id');
        return $id;
    }

    public function deleteCourse($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('fee_structure_item_subject', 'fsisub_id = '.$id);
        return $delete;
    }
}