<?php 
class Studentfinance_Model_DbTable_Receivableadjustmentapproval extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_receivable_adjustment';
	private $lobjrecvModel;
	private $lobjmaintenanceModel;
	private $lobjrecieptModel;
	private $lobjDefCodeModel;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->lobjrecvModel = new Studentfinance_Model_DbTable_Receivableadjustment();
		$this->lobjmaintenanceModel = new GeneralSetup_Model_DbTable_Maintenance();
		$this->lobjrecieptModel = new Studentfinance_Model_DbTable_Receipt();
		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		
		$this->lobjapplicantpersonalModel = new Application_Model_DbTable_Studentapplication();
		$this->lobjProgramCheckList = new Application_Model_DbTable_Programchecklist();
		$this->lobjStudentapproval = new Application_Model_DbTable_Studentapproval();
	}

	public function fnSearchReceivableAdjustment($larrformData) {
		
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_receivable_adjustment"), array("a.*"))
		->join(array('def' => 'tbl_definationms'), 'def.idDefinition = a.Status', array())
		->joinLeft(array('b'=>'tbl_receipt'),'b.IdReceipt = a.IdReceipt',array('b.ReceiptCode'))
		->joinLeft(array('c'=>'tbl_definationms'),'c.idDefinition = a.AdjustmentType',array('c.DefinitionDesc as adjustemt'))
		->joinLeft(array('u'=>'tbl_user'),'u.iduser=a.UpdUser','u.loginName');
		
		if(isset($larrformData['AdjustmentNumber']) && !empty($larrformData['AdjustmentNumber']) ){			
			$select = $select->where("a.AdjustmentCode LIKE  '%".$larrformData['AdjustmentNumber']."%'");

		}
		if(isset($larrformData['AdjustmentType']) && !empty($larrformData['AdjustmentType']) ){
			$select = $select->where("a.AdjustmentType = ?",$larrformData['AdjustmentType']);
		}
			
		if(isset($larrformData['DocumentDate']) && !empty($larrformData['DocumentDate'])){
			$select = $select->where("a.DocumentDate = ?",$larrformData['DocumentDate']);
		}

		if(isset($larrformData['ReceiptNumber']) && !empty($larrformData['ReceiptNumber'])){
			$select = $select->where("b.ReceiptCode LIKE  '%".$larrformData['ReceiptNumber']."%'");
		}
			
		$select->group("a.IdReceivableAdjustment");
		$select->where("def.DefinitionCode = ?",'Entry');
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}
	
	public function fnupdateReceivableAdjustment($data,$status,$userId) 
	{
		$approvaldata = array();
		$approvaldata['ProcessedBy'] = $userId;
		$approvaldata['ProcessedOn'] = date('Y-m-d');
		$approvaldata['Status'] = $status;		
		$larrdefCode = $this->lobjmaintenanceModel->fngetDefinationcode('Change Status',$status);
		
	
		foreach($data['chkaddreceipt'] as $det)
		{
			$key = 'Remark_'.$det;
			$approvaldata['Remarks'] = $data[$key];			
			
			if ($larrdefCode == 'Approve')
			{	
				// Now calculation the receivable adjustment
				$this->fncalculatereceivableadjustmentapproval($det);
			
			}
			else if($larrdefCode == 'Reject')
			{	
				$receiptreceivablecount['CountRecivableadjustment'] = 0;
				$larrrecdet = $this->lobjrecvModel->fngetReceivableById($det);				
				$this->lobjrecieptModel->fnupdateReceiptStatus($receiptreceivablecount,$larrrecdet[0]['IdReceipt']);
			}
			
			// update function for receivable adjustment
			$this->lobjrecvModel->fnUpdateReceivableAdjustment($approvaldata,$det);
		}
	}
	
	public function fncalculatereceivableadjustmentapproval($id)
	{
		
		$larrrecdet = $this->lobjrecvModel->fngetReceivableById($id);
		
	
			
		if(!empty($larrrecdet))
		{
			$larrdefCode = $this->lobjmaintenanceModel->fngetDefinationcode('Adjustment Type',$larrrecdet[0]['AdjustmentType']);		
			switch ($larrdefCode) 
			{
				case "Return Cheqeue":
					$this->fncalcalculatereturnchequeApproval($id,$larrrecdet[0]);					
				break;			
				case "Wrong Payment item":
					$this->fncalculateWrongPaymentItemApproval($id,$larrrecdet[0]);
					
					break;
				case "Cancellation w/o replacement":
					$this->fncalculatecancelapproval($id,$larrrecdet[0]);	
					break;				
				case "Wrong payment mode":				
					$this->fncalcalculatewrongpaymentapproval($id,$larrrecdet[0]);
				break;
			}
			
		
			// -----------------------------
			//amount
			$receipt = $this->lobjrecieptModel->fngetReceiptdet($larrrecdet[0]['ReceiptCode'],'a.ReceiptCode');
			$applicant = $this->lobjapplicantpersonalModel->getStudentdet($receipt[0]['PayeeCode'],'a.IdApplicant');
			
			if ( empty($applicant) )
			{
				throw new Exception('Invalid Applicant Id');
				return false;
			}
			
			$larrchecklist = $this->lobjProgramCheckList->fnViewProgramchecklist($applicant['ProvisionalProgramOffered']);
				
			$larrpreffereddet = $this->lobjStudentapproval->getstudentpreferreddet($applicant['IdApplication'],$applicant['ProvisionalProgramOffered']);
		
			$paymentamount = 0;
			//Now get the payment amount from the admission process process setup
			if(!empty($larrpreffereddet))
			{
				$larradmissionprocess = $this->lobjStudentapproval->fngetadmissiionprocessdet($larrpreffereddet);						
				
				if(!empty($larradmissionprocess))
				{
					$paymentamount = $larradmissionprocess['AmountByProgram'] == '' ? $larradmissionprocess['Amount'] : $larradmissionprocess['AmountByProgram'];
				}
			}
			
			
			// Now need to get get the amount from receipt's noninvoice section
			// first get the student application code
			$studappcode = $this->lobjStudentapproval->fngetstudentcode($applicant['IdApplication']);	
			$noninvoiceamount = $this->lobjStudentapproval->fngetReceiptNoninvoice($studappcode);
			
			$payment = 0;
			if(!empty($noninvoiceamount))
			{
				foreach($noninvoiceamount as $det)
				{
					$payment = $payment + $det['Amount'];
				}
			}
	
			
			if ( $payment < $paymentamount )
			{
				//empty the checkboxes
				if ( !empty($larrchecklist) )
				{
					foreach($larrchecklist as $rec)
					{
						$checkListtype = $this->lobjStudentapproval->fngetchecklistName($rec['ChecklistType']);	
						if ( $checkListtype == 'Processing Fee')
						{
							$data['Comments'] = "";
							$data['Confirm'] = 0;
							$data['Varified'] = 1;
							$data['Status'] = 187;		
							$where_ap = " IdApplication = '".$applicant['IdApplication']."' AND IdCheckList = '".$rec['IdCheckList']."'";
							
							$db = Zend_Db_Table :: getDefaultAdapter();
							$db->update('tbl_varifiedprogramchecklist',$data,$where_ap);
						}
					}
				}
			} 
			// -----------------	
			
			
		}
	}
	
	// Function to calculate wrong payment item
	public function fncalculateWrongPaymentItemApproval($id,$larrrecdet){
		// Now get the receivable adjustment invoice detail
		$larrReceivableInvoicedet = $this->lobjrecvModel->fngetreceivableadjustmentinvoidedetl($id);
		$ldecpaidamount = 0;
		$ldecrecamountpaid = 0;		
		$ldectotalbln = 0;
		$ldectotalpaid = 0;
		$invoicemainId = 0;
		$larrnewInvoicelist = array();
		$larrnewInvoicemainlist = array();		
		$larradvancepayment = array();
		if(!empty($larrReceivableInvoicedet)){
			foreach($larrReceivableInvoicedet as $recinvoicedet){
				$invoicemainId = $recinvoicedet['IdInvoice'];
				$ldecpaidamount = $recinvoicedet['NewPaidAmount'] - $recinvoicedet['TotalPaidReceipt'];
				//Now get the invoice detail for the receipt
				$larrreceiptInvoiceDetl = $this->lobjrecieptModel->fngetinvoicedetail($recinvoicedet['IdInvoiceDetail']);				
				$larrnewInvoicelist['TOTAL_PAID'] = $larrreceiptInvoiceDetl[0]['TOTAL_PAID'] + $ldecpaidamount;
				$larrnewInvoicelist['TOTAL_BLNC'] = $larrreceiptInvoiceDetl[0]['TOTAL_AMOUNT'] - $larrnewInvoicelist['TOTAL_PAID'];				
				$ldecrecamountpaid = $ldecrecamountpaid + $larrnewInvoicelist['TOTAL_PAID'];
				// Now update the new detail of invoice detail 
				$this->lobjrecieptModel->fnupdateinvoicedetail($larrnewInvoicelist,$recinvoicedet['IdInvoiceDetail']);
				
			}
			//Now get the invoice detail for the receipt to update the invoice detail
			$larrnewInvoiceDetl = $this->lobjrecieptModel->fngetReceiptInvoiceDet($larrrecdet['IdReceipt']);
			
			foreach($larrnewInvoiceDetl as $dtl){
				$ldectotalpaid = $ldectotalpaid + $dtl['TOTAL_PAID'];
				$ldectotalbln = $ldectotalbln + $dtl['TOTAL_BLNC'];
			}
			$larrnewInvoicemainlist['TOTAL_PAID'] = $ldectotalpaid;
			$larrnewInvoicemainlist['TOTAL_BLNC'] = $ldectotalbln;			
			$this->lobjrecieptModel->fnupdateinvoicemain($larrnewInvoicemainlist,$invoicemainId);
						
			//Now get the noninvoice detail for the receivable
			$larrreceiptInvoiceDetl = $this->lobjrecvModel->fngetreceivableadjustmentnoninvoidedetl($id);			
			foreach($larrreceiptInvoiceDetl as $recnoninvoicedet){
				$ldecrecamountpaid = $ldecrecamountpaid + $recnoninvoicedet['Amount'];
			}
			// Now update the advancepayment detail in advancepayment table			
			$larradvancepaymentdet = $this->lobjrecieptModel->fngetadvanceentryofstudent($larrrecdet['IdReceipt']);
			$larradvancepayment['TOTAL_BLNC'] = $larradvancepaymentdet[0]['TOTAL_AMOUNT'] - $ldecrecamountpaid;
			$larradvancepayment['STATUS'] = 'X';
			$this->lobjrecieptModel->fnupdateadvanceentry($larradvancepayment,$larradvancepaymentdet[0]['AUTOINC']);
		}
	}
	
	// Function to calculate wrong payment mode
	public function fncalcalculatereturnchequeApproval($id,$larrrecdet){
		$lintreceiptId = $larrrecdet['IdReceipt'];
		// Now get the receipt invoice detail
		$larrReceiptInvoicedetail = $this->lobjrecieptModel->fngetReceiptInvoiceDet($lintreceiptId);
		$lfloattotalinvoicemainamount = 0;
		$lfloatpaidinvoicemainamount = 0;
		$lfloatbalanceinvoicemainamount = 0;
		$larrupdateinvoicemainarray = array(); // For invoice main
		$larrupdateinvoicedetarray = array(); // For detail invoice	
		// loop for all invoice detail
		if(!empty($larrReceiptInvoicedetail)){
			foreach($larrReceiptInvoicedetail as $det){
				$tobepaid = $det['TobePaid'];
				$lintInvocedet = $det['IdInvoiceDetail'];
				$lintIdInvoicemain = $det['IdInvoice'];
				// Now get the detail about invoice detail data
				$larrinvoicedetail = $this->lobjrecieptModel->fngetinvoicedetail($lintInvocedet);
				$larrupdateinvoicedetarray['TOTAL_PAID'] = $larrinvoicedetail[0]['TOTAL_PAID'] - $tobepaid;
				$larrupdateinvoicedetarray['TOTAL_BLNC'] = $larrinvoicedetail[0]['TOTAL_BLNC'] + $tobepaid;
				// Now update invoice detail data
				$this->lobjrecieptModel->fnupdateinvoicedetail($larrupdateinvoicedetarray,$lintInvocedet);
				// Now get the updated invoice detail
				$larrinvoicedetailupdated = $this->lobjrecieptModel->fngetinvoicedetail($lintInvocedet);
				$lfloattotalinvoicemainamount = $lfloattotalinvoicemainamount + $larrinvoicedetailupdated[0]['TOTAL_AMOUNT'];	 // total amount
				$lfloatpaidinvoicemainamount = $lfloatpaidinvoicemainamount + $larrinvoicedetailupdated[0]['TOTAL_PAID'];	// paid amount
				$lfloatbalanceinvoicemainamount = $lfloatbalanceinvoicemainamount + $larrinvoicedetailupdated[0]['TOTAL_BLNC']; // balance amount					
			}
			
			$larrupdateinvoicemainarray['TOTAL_AMOUNT'] =  $lfloattotalinvoicemainamount;
			$larrupdateinvoicemainarray['TOTAL_PAID'] = $lfloatpaidinvoicemainamount;
			$larrupdateinvoicemainarray['TOTAL_BLNC'] = $lfloatbalanceinvoicemainamount;
			//updating invoice main detail
			$this->lobjrecieptModel->fnupdateinvoicemain($larrupdateinvoicemainarray,$lintIdInvoicemain);
			// Now Need to update advance payment detail
			$larrinvoicemaindetl = $this->lobjrecieptModel->fngetinvoicemaindetail($larrrecdet['PayeeCode']);
			$larradvancepaymentdet = $this->lobjrecieptModel->fngetadvanceentryofstudent($lintreceiptId);
			$larradvancepaymentarray['TOTAL_BLNC'] = $larradvancepaymentdet[0]['TOTAL_AMOUNT'] - $larrinvoicemaindetl[0]['TOTAL_PAID'];
			$larradvancepaymentarray['TOTAL_PAID'] = $larrinvoicemaindetl[0]['TOTAL_PAID'];
			$larradvancepaymentarray['STATUS'] = 'X';			
			$this->lobjrecieptModel->fnupdateadvanceentry($larradvancepaymentarray,$larradvancepaymentdet[0]['AUTOINC']);
		}
		// Now need to change the status of the receipt from Approve to reject		
		$rec = array();	
		$larrDefId = $this->lobjDefCodeModel->fngetDefKeyValue('Change Status','Reject');						
		$rec['Status'] = $larrDefId[0]['key'];
		if(!empty($rec)){
			$this->lobjrecieptModel->fnupdateReceiptStatus($rec,$larrrecdet['IdReceipt']);
		}
	}	
	
	// Function to calculate wrong payment mode
	public function fncalcalculatewrongpaymentapproval($id,$larrrecdet){
		$larrReceiptdetailupdated = array();
		$lintreceiptId = $larrrecdet['IdReceipt'];
		$larrReceiptdet = $this->lobjrecieptModel->fngetReceiptdet($lintreceiptId);
		$larrReceiptdetailupdated['OldIdPaymentGroup'] = $larrReceiptdet[0]['IdPaymentGroup'];
		$larrReceiptdetailupdated['OldPaymentMode'] = $larrReceiptdet[0]['PaymentMode'];
		$larrReceiptdetailupdated['OldPaymentDocNo'] = $larrReceiptdet[0]['PaymentDocNo'];
		$larrReceiptdetailupdated['OldPayDocBank'] = $larrReceiptdet[0]['PayDocBank'];
		$larrReceiptdetailupdated['OldPayDocBankBranch'] = $larrReceiptdet[0]['PayDocBankBranch'];
		$larrReceiptdetailupdated['OldTerminalId'] = $larrReceiptdet[0]['TerminalId'];
		$larrReceiptdetailupdated['OldBankCardNo'] = $larrReceiptdet[0]['BankCardNo'];
		$larrReceiptdetailupdated['OldBankCardType'] = $larrReceiptdet[0]['BankCardType'];		
		
		// Now get the receivable adjustment detail
		$larrReceivabledet = $this->lobjrecvModel->fngetReceivableAdjustmentdet($larrrecdet['IdReceivableAdjustment']);
		if(!empty($larrReceivabledet)){
			$larrReceiptdetailupdated['IdPaymentGroup'] = $larrReceivabledet[0]['PaymentGroup'];
			$larrReceiptdetailupdated['PaymentMode'] = $larrReceivabledet[0]['PaymentMode'];
			$larrReceiptdetailupdated['PaymentDocNo'] = $larrReceivabledet[0]['ChequeNo'];
			$larrReceiptdetailupdated['PayDocBank'] = $larrReceivabledet[0]['PaymentDocBank'];
			$larrReceiptdetailupdated['PayDocBankBranch'] = $larrReceivabledet[0]['PayDocBankBranch'];
			$larrReceiptdetailupdated['TerminalId'] = $larrReceivabledet[0]['TerminalId'];
			$larrReceiptdetailupdated['BankCardNo'] = $larrReceivabledet[0]['BankCardNo'];
			$larrReceiptdetailupdated['BankCardType'] = $larrReceivabledet[0]['BankCardType'];
		}
		// Now update the receipt
		$this->lobjrecieptModel->fnupdateReceiptStatus($larrReceiptdetailupdated,$lintreceiptId);
	}
	
	
	
	// Function to calculate calncellation w/o replacement
	public function fncalculatecancelapproval($id,$larrrecdet){		
		$lintreceiptId = $larrrecdet['IdReceipt'];		
		// Now get the receipt invoice detail
		$larrReceiptInvoicedetail = $this->lobjrecieptModel->fngetReceiptInvoiceDet($lintreceiptId);
		$lfloattotalinvoicemainamount = 0;
		$lfloatpaidinvoicemainamount = 0;
		$lfloatbalanceinvoicemainamount = 0;
		$larrupdateinvoicemainarray = array(); // For invoice main
		$larrupdateinvoicedetarray = array(); // For detail invoice	
		// loop for all invoice detail
		if(!empty($larrReceiptInvoicedetail)){
			foreach($larrReceiptInvoicedetail as $det){
				$tobepaid = $det['TobePaid'];
				$lintInvocedet = $det['IdInvoiceDetail'];
				$lintIdInvoicemain = $det['IdInvoice'];
				// Now get the detail about invoice detail data
				$larrinvoicedetail = $this->lobjrecieptModel->fngetinvoicedetail($lintInvocedet);
				$larrupdateinvoicedetarray['TOTAL_PAID'] = $larrinvoicedetail[0]['TOTAL_PAID'] - $tobepaid;
				$larrupdateinvoicedetarray['TOTAL_BLNC'] = $larrinvoicedetail[0]['TOTAL_BLNC'] + $tobepaid;
				// Now update invoice detail data
				$this->lobjrecieptModel->fnupdateinvoicedetail($larrupdateinvoicedetarray,$lintInvocedet);
				// Now get the updated invoice detail
				$larrinvoicedetailupdated = $this->lobjrecieptModel->fngetinvoicedetail($lintInvocedet);
				$lfloattotalinvoicemainamount = $lfloattotalinvoicemainamount + $larrinvoicedetailupdated[0]['TOTAL_AMOUNT'];	 // total amount
				$lfloatpaidinvoicemainamount = $lfloatpaidinvoicemainamount + $larrinvoicedetailupdated[0]['TOTAL_PAID'];	// paid amount
				$lfloatbalanceinvoicemainamount = $lfloatbalanceinvoicemainamount + $larrinvoicedetailupdated[0]['TOTAL_BLNC']; // balance amount					
			}
			
			$larrupdateinvoicemainarray['TOTAL_AMOUNT'] =  $lfloattotalinvoicemainamount;
			$larrupdateinvoicemainarray['TOTAL_PAID'] = $lfloatpaidinvoicemainamount;
			$larrupdateinvoicemainarray['TOTAL_BLNC'] = $lfloatbalanceinvoicemainamount;
			
			//updating invoice main detail
			$this->lobjrecieptModel->fnupdateinvoicemain($larrupdateinvoicemainarray,$lintIdInvoicemain);
			// Now Need to update advance payment detail
			
			$larrinvoicemaindetl = $this->lobjrecieptModel->fngetinvoicemaindetail($larrrecdet['PayeeCode']);
			
			
			$larradvancepaymentdet = $this->lobjrecieptModel->fngetadvanceentryofstudent($lintreceiptId);
			$larradvancepaymentarray['TOTAL_BLNC'] = $larradvancepaymentdet[0]['TOTAL_AMOUNT'] - $larrinvoicemaindetl[0]['TOTAL_PAID'];
			$larradvancepaymentarray['TOTAL_PAID'] = $larrinvoicemaindetl[0]['TOTAL_PAID'];
			$larradvancepaymentarray['STATUS'] = 'X';			
			$this->lobjrecieptModel->fnupdateadvanceentry($larradvancepaymentarray,$larradvancepaymentdet[0]['AUTOINC']);
		}
		// Now need to change the status of the receipt from Approve to reject		
		$rec = array();	
		$larrDefId = $this->lobjDefCodeModel->fngetDefKeyValue('Change Status','Reject');						
		$rec['Status'] = $larrDefId[0]['key'];
		if(!empty($rec)){
			$this->lobjrecieptModel->fnupdateReceiptStatus($rec,$larrrecdet['IdReceipt']);
		}
	}
}
