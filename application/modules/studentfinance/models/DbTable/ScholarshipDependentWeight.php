<?php

class Studentfinance_Model_DbTable_ScholarshipDependentWeight extends Zend_Db_Table {

    protected $_name = 'tbl_scholarship_dependent_weight';
    protected $_primary = 'id';

    public function save(array $data){

        if( isset($data[$this->_primary]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }

    /**
     * feeds an age and returns the point based on
     */
    public function get_point( $dependent_count ) {

        $select = $this->select()
            ->where('`from` <= ?', $dependent_count)
            ->where('`to` >= ?', $dependent_count)
        ;
        $rule = $this->fetchRow($select);
        if(empty($rule)) {
            return 0;
        } else {
            return $rule->score;
        }

    }

}