<?php

class Studentfinance_Model_DbTable_Receivableadjustmentnoninvoice extends Zend_Db_Table { 
	protected $_name = 'tbl_receivable_adjustment_invoice';

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fngetReceivableadjustmentnoninvoicedetl($IdReceivableAdjustment){
		$lstrSelect = $this->lobjDbAdpt->select()
							->from(array('a' => 'tbl_receivable_adjustment_noninvoice'),array('a.*'))
							->joinLeft(array("b" => "tbl_fee_setup"), 'a.FeeCode = b.IdFeeSetup',array("b.FeeCode"))
							->joinLeft(array("c" => "tbl_ledgercode"), 'a.ChargeCode = c.AUTOINC',array("c.CMPYCODE" , "c.ACCOUNTCODE"))							
							->where("a.IdReceivableAdjustment= ?", $IdReceivableAdjustment);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
}