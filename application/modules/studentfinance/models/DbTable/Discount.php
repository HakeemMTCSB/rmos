<?php
class Studentfinance_Model_DbTable_Discount extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'discount';
	protected $_primary = "dcnt_id";
		
	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>$this->_name));
					
		if($id!=null){
			$selectData->where("d.dcnt_id = ?",$id);
			
			$row = $db->fetchRow($selectData);
		}else{
			$row = $db->fetchAll($selectData);
		}
			
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
				->from(array('d'=>$this->_name))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.dcnt_creator', array())
				->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>new Zend_Db_Expr("CONCAT_WS(' ', fName,Mname,Lname)")));
	
		return $selectData;
	}
	
	public function getDiscountData($fomulir){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.dcnt_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>'Fullname'))
					->where('d.dcnt_fomulir_id =?',$fomulir);
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function insert(array $data){
		
		if( !isset($data['dcnt_creator']) ){
			$auth = $auth = Zend_Auth::getInstance();
			
			$data['dcnt_creator'] = $auth->getIdentity()->iduser; 
		}
		
		if( !isset($data['dcnt_create_date']) ){
			$data['dcnt_create_date'] = date('Y-m-d H:i:a'); 
		}
		
		return parent::insert($data);
	}
	
	public function getlistStudent($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'discount'))
					->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = d.dcnt_IdStudentRegistration')
					->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
					->join(array('c'=>'tbl_currency'), 'd.dcnt_currency_id = c.cur_id',array('cur_code'))
					->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
					->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.dcnt_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu1'=>'tbl_user'),'tu1.iduser = d.dcnt_approve_by', array('approve_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = d.	dcnt_cancel_by', array('cancel_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->where('d.dcnt_batch_id =?',$id);
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getlistStudentByStatus($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$arrayStatus = array('A','E','X');
		$newArray = array();
		foreach($arrayStatus as $key=>$sts){
			
			
			$selectData = $db->select()
						->from(array('d'=>'discount'),array('count(*) as total'))
						->where('d.dcnt_batch_id =?',$id)
						->where('d.dcnt_status =?',$sts);
						
			$row = $db->fetchRow($selectData);
			
			$newArray[$sts]=$row['total'];
		}

		if($newArray){
			return $newArray;
		}else{
			return null;
		}
	}
	
	public function getPaginateListData($where=array())
	{
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
	                
					->joinLeft(array('cur' => 'tbl_currency'), 'a.dcnt_currency_id=cur.cur_id',array('cur.cur_symbol_prefix'))
					
					
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.	dcnt_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('creatorName'=>'ts.FullName'))
					->joinLeft(array('ua'=>'tbl_user'), 'ua.iduser = a.dcnt_cancel_by', array())
					->joinLeft(array('tsa'=>'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancelName'=>'tsa.FullName'))
					->joinLeft(array('ub'=>'tbl_user'), 'ub.iduser = a.dcnt_approve_by', array())
					->joinLeft(array('tsb'=>'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('updName'=>'tsb.FullName'))
					->joinLeft(array('dt' => 'discount_type'), 'dt.dt_id = a.dcnt_type_id', array('dt_discount'))
					;
		
		if ( !empty($where) )
		{
			if ( $where['type'] == 1 )
			{
				
				$select->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=a.dcnt_txn_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('ap' => 'applicant_program'), 'ap.ap_at_trans_id=a.dcnt_txn_id', array('ap_prog_id'))
					->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=ap.ap_prog_id', array('ProgramName','ProgramCode'));
					
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('st.at_pes_id LIKE ?', '%'.$where['id'].'%');
				}

				/*$select->where('a.appl_id IS NOT NULL','')
					   ->where('a.IdStudentRegistration IS NULL','');*/
			}
			
			if ( $where['type'] == 2 )
			{
				$select->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=a.dcnt_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					->joinLeft(array('pa' => 'tbl_program'), 'pa.IdProgram=sr.IdProgram', array('ProgramName','ProgramCode'));
				if ( isset($where['name'])  && $where['name'] != '' )
				{
					$select->where('CONCAT_WS(" ",sp.appl_fname,sp.appl_mname,sp.appl_lname) LIKE ?', '%'.$where['name'].'%');
				}

				if ( isset($where['id'])  && $where['id'] != '' )
				{
					$select->where('sr.registrationId LIKE ?', '%'.$where['id'].'%');
				}

//				$select->where('a.appl_id != ?','')
//					   ->where('a.IdStudentRegistration != ?','');
			}
			
			if ( isset($where['dcnt_id'] )){
				$select->where('a.dcnt_fomulir_id LIKE ?', '%'.$where['dcnt_id'].'%');
			}
			
			$select->group('a.dcnt_id');
			$select->order('dcnt_id DESC');
		}

		return $select;
	}
	
	public function getSearchDataDN($searchData=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoiceID = $searchData['invoice_id'];
		$invoiceDetailID = $searchData['invoice_detail_id'];
		$date = date('Y-m-d');
		
		$selectData = $db->select()
						->from(array('a'=>'discount_detail'),array('dnamount'=>'a.dcnt_amount','*'))
						->join(array('sc'=>'discount'), 'sc.dcnt_id = a.dcnt_id')
						//->joinLeft(array('b'=>''))
						->where('a.dcnt_invoice_id = ?',$invoiceID)
						->where('a.dcnt_invoice_det_id = ?',$invoiceDetailID);
						//->where("a.status = 'A'")
						//->where("sc.dcnt_status = 'A'");

		if($dateFrom!=''){
			$selectData->where("DATE(sc.dcnt_approve_date) >= ?",$dateFrom);
		}
		
		if($dateTo!=''){
			$selectData->where("DATE(sc.dcnt_approve_date) <= ?",$dateTo);
		}			;

		//echo $selectData.'<br /><br />';
		$row = $db->fetchRow($selectData);
		
		/*
		 * hide 01/07/2015 - sbb dah cater utk discount adjustment
		 * if($row['status'] == 'A'){
			$dateDiscount = $date;
			$row['status_date'] = $dateDiscount;
		}else{ //if cancel
			$dateDiscount = date('Y-m-d',strtotime($row['status_date']));
			$row['status_date'] = $dateDiscount;
			if($dateDiscount >= $dateFrom && $dateDiscount <= $dateTo){
				unset($row);
				$row = null;
			}
		}*/
		
		return $row;
	}
	
	
}

