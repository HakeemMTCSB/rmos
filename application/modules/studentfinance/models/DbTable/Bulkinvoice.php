<?php
class Studentfinance_Model_DbTable_Bulkinvoice extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_charges';
	
	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	public function fnGetProgrambranchlist(){ // Function to create program select box
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programbranchlink"),array("key"=>"a.IdProgram","value"=>"b.ProgramName"))
				 				 ->join(array('b'=>'tbl_program'),'a.IdProgram  = b.IdProgram')
				 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnSearchStudentApplication($post,$check){ // Search students		
		if($check == 1 )$field7 = "a.idsponsor is NULL ";	
		if($check == 2 )$field7 = "a.idsponsor != 'NULL' ";		
		$select = $this -> select()
			            -> setIntegrityCheck(false)  	
			            -> from(array('a' => 'tbl_studentapplication'),array('a.*')) 
			            -> join(array('b' => 'tbl_studentregistration'),'a.IdApplication = b.IdApplication ',array(''))   
			     	    -> where($field7);;            
		if($post['field5'])   $select->where('a.FName  like "%" ? "%"',$post['field5']);
		if($post['field2'])   $select->where('a.ICNumber like  "%" ? "%"',$post['field2']);			   
		if(isset($post['field8']) && !empty($post['field8']) )	 $select->where("a.IDCourse  = ?",$post['field8']) ;
		$select->group('a.IdApplication');		
		$result = $this->fetchAll($select);
		return $result->toArray();		
	}
 	public function fngetProgramDetails($lintidprogram){ // function to get program charge details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
      	$currentdate = date ('Y-m-d');
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_programcharges'),array('a.Charges','a.IdCharges'))
                			->join(array('b'=>'tbl_charges'),'a.IdCharges = b.IdCharges',array('b.ChargeName'))
                			->where('b.effectiveDate <= ?',$currentdate)
                			->where('a.IdProgram = ?',$lintidprogram)
                			->where('b.Payment = 1');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }    
 	public function fnAddInvoiceMaster($larrformData)  { // function to insert Invoice 
    	    $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_invoicemaster";
            $postData = array(		
							'IdStudent' => $larrformData['Studentid'],		
            				'InvoiceNo' => date('M').'-'.$larrformData['Studentid'].'-'.$larrformData['idsponsor'].'-'.rand(1000,9999),	
		            		'InvoiceDt' =>$larrformData['InvoiceDate'],	
		            		'InvoiceAmt'=>$larrformData['InvoiceAmount'],
            				'MonthYear'=>$larrformData['MonthYear'],
            				'AcdmcPeriod' =>$larrformData['AcademicPeriod'],		
            				'Naration' =>$larrformData['Naration'],		
		            		'UpdDate' =>$larrformData['UpdDate'],	
		            		'UpdUser'=>$larrformData['UpdUser'],
            				'Approved'=>0,
            				'idsponsor'=>$larrformData['idsponsor'],
						);			
	        $db->insert($table,$postData);
	        return $lastinvoiceid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_invoicemaster','IdInvoice');
    }
    public function fnAddInvoiceDetails($larrformData,$idInvoice){	        //Function to insert into the invoice details///////////////////	      
	   for($i=0;$i<count($larrformData);$i++) {
			$db = Zend_Db_Table::getDefaultAdapter();
            $tabledetails = "tbl_invoicedetails";
            $postDatadetails = array(	
            				'IdInvoice'=>$idInvoice,	
							'idAccount' => $larrformData[$i]['IdCharges'],		
            				'Amount' =>$larrformData[$i]['Charges'],		
		            		'UpdDate' =>$larrformData[0]['UpdDate'],	
		            		'UpdUser'=>$larrformData[0]['UpdUser'],
		            		'Discount' =>0,	
                            'Active'=>1
						);				
	        $db->insert($tabledetails,$postDatadetails);
	     }   	       
    } 
     public function fnAddSubjectInvoiceDetails($larrformData,$lastinvoiceid){ //Function to add subject invoice
     	$db = Zend_Db_Table::getDefaultAdapter();	
     	$tabledetails = "tbl_invoicesubjectdetails";
     	$product=0; 
        for($i=0;$i<count($larrformData);$i++) {           
            $larrformData[$i]['Subjectamount'] = $larrformData[$i]['CreditHours']*$larrformData[$i]['AmtPerHour'];
            $product = $product+$larrformData[$i]['Subjectamount'];
            $postDatadetails = array(	
            				'IdInvoice'=>$lastinvoiceid,	
							'IdSubject' => $larrformData[$i]['IdSubject'],		
            				'Amount' =>$larrformData[$i]['Subjectamount'],		
		            		'UpdDate' =>$larrformData[0]['UpdDate'],	
		            		'UpdUser'=>$larrformData[0]['UpdUser']
						);			
	        $db->insert($tabledetails,$postDatadetails);
	     }   
	    return $product;
		     
    }    
	public function fngetSemester($lintstudentIds)  { //Function to get semester details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();     
		$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("sa"=>"tbl_studentregistration"),array("sa.*"))
		 				 	 ->where('sa.IdApplication = ?',$lintstudentIds)
                             ->order('sa.IdStudentRegistration desc')
                             ->limit(1);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
    }
   public function fnGetSubjectCharges($lintIdStudentRegistration)   { //Function to get subject details for subject invoice entry
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentregsubjects'),array('a.*'))
                			->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject')
                			->where('a.IdStudentRegistration = ?',$lintIdStudentRegistration);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                			
    }
   public function fngetStudentDetails($lintidstudent) {   //Function to get the students details
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
 		$lstrSelect = $lobjDbAdpt->select()
 					   				 ->from(array('b'=>'tbl_studentapplication'))
               						 ->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
               						 ->where("b.IdApplication= ?",$lintidstudent);
        $result = $lobjDbAdpt->fetchRow($lstrSelect);
        return $result;
     }  
 	public function fnUpdateAccount($sumAmt,$lintidInvoice) { //Function to update total invoice amount after subject invoice entry
     	$db = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_invoicemaster";
    	$larrformData['InvoiceAmt'] = $sumAmt;
	    $where = 'IdInvoice = '.$lintidInvoice;
	    $msg = $db->update($table,$larrformData,$where);
	    return $msg;
 	}
}