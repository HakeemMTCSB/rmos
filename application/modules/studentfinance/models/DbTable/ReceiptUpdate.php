<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 17/10/2016
 * Time: 3:08 PM
 */
class Studentfinance_Model_DbTable_ReceiptUpdate extends Zend_Db_Table_Abstract {

    public function getReceipt($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'receipt'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.rcp_IdStudentRegistration = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->join(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->where('a.rcp_status = ?', 'APPROVE');

        if ($search != false){
            if (isset($search['name']) && $search['name']!=''){
                $select->where($db->quoteInto('CONCAT_WS(" ", c.appl_fname, c.appl_lname) like ?', '%'.$search['name'].'%'));
            }

            if (isset($search['id']) && $search['id']!=''){
                $select->where($db->quoteInto('b.registrationId like ?', '%'.$search['id'].'%'));
            }

            if (isset($search['rcp_no']) && $search['rcp_no']!=''){
                $select->where($db->quoteInto('a.rcp_no like ?', '%'.$search['rcp_no'].'%'));
            }

            if (isset($search['uti_date']) && $search['uti_date']!=''){
                $select->where($db->quoteInto('a.rcp_receive_date = ?', date('Y-m-d', strtotime($search['uti_date']))));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateReceipt($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('receipt', $data, 'rcp_id = '.$id);
        return $update;
    }
}