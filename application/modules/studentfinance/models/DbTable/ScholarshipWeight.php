<?php

class Studentfinance_Model_DbTable_ScholarshipWeight extends Zend_Db_Table_Abstract {

    //put your code here
    protected $_name = 'tbl_scholarship_weight_setup';
    protected $_primary = 'id';

    public $main_types = array(
        'contribution',
        'academic',
        'affordability'
    );

    public function save($data) {

        if( isset($data["id"]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }

    /**
     * feeds an age and returns the point based on
     */
    public function get_weight( $weight_name ) {

        $select = $this->select()
            ->where('`name` =  ?', $weight_name)
        ;
        $rule = $this->fetchRow($select);
        if(empty($rule)) {
            return 0;
        } else {
            return $rule->weight;
        }

    }

}
