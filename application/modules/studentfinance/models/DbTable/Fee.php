<?php
class Studentfinance_Model_DbTable_Fee extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_fee_category';
	private $lobjDbAdpt;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnSearchFeeCategory($post = array()) {

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_category"), array("a.*"));

		if(isset($post['field2']) && !empty($post['field2']) ){
			$select->where(" a.FeeCategoryCode LIKE  '%".$post['field2']."%'  ");

		}
		if(isset($post['field3']) && !empty($post['field3']) ){
			$select->where("a.Description  LIKE  '%".$post['field3']."%'  ");
		}

		$result = $this->lobjDbAdpt->fetchAll($select);

		return $result;
	}

	public function fnSearchFeesetup($post = array()) {

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_setup"), array("a.*"))
		->joinLeft(array('b'=>'tbl_fee_category'),'b.IdFeeCategory = a.FeeCategory',array('b.FeeCategoryCode'))
		->joinLeft(array('c'=>'tbl_definationms'),'c.idDefinition = a.ChargingPeriod',array('c.DefinitionDesc'))
		->joinLeft(array('d'=>'tbl_ledgercode'),'d.AUTOINC = a.ChargeCode',array('d.CMPYCODE','d.ACCOUNTCODE','d.DESCRIPTION'));

		if(isset($post['FeeCodeNot']) && !empty($post['FeeCodeNot']) ){
			$select = $select->where("a.FeeCode LIKE  '%".$post['FeeCodeNot']."%'  ");

		}
		if(isset($post['ChargeCode']) && !empty($post['ChargeCode']) ){
			$select = $select->where("a.ChargeCode = ?",$post['ChargeCode']);

		}
		if(isset($post['DescriptionNot']) && !empty($post['DescriptionNot']) ){
			$select = $select->where("a.Description LIKE  '%".$post['DescriptionNot']."%'  ");

		}
		if(isset($post['ShortDescription']) && !empty($post['ShortDescription']) ){
			$select = $select->where("a.ShortDescription LIKE  '%".$post['ShortDescription']."%'  ");

		}
		if(isset($post['FeeCategoryNot']) && !empty($post['FeeCategoryNot']) ){
			$select = $select->where("a.FeeCategory = ?",$post['FeeCategoryNot']);
		}

		$result = $this->lobjDbAdpt->fetchAll($select);

		return $result;
	}


	public function addfeecategory($larrformData)
	{


		//$totalFee = count($larrformData['FeeCategoryCodeGrid']);
		//for($i=0;$i<$totalFee;$i++) {

		$paramArray = array(
				'FeeCategoryCode' => $larrformData['FeeCategoryCode'],
				'Description' => $larrformData['Description'],
				'SequenceOrder' => $larrformData['SequenceOrder'],
				'DefaultLanguage' => $larrformData['DefaultLanguage'],
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate'],
				'IdUniversity'=>$larrformData['IdUniversity'],
		);

		$this->lobjDbAdpt->insert('tbl_fee_category',$paramArray);

	}

	public function updatefeecategory($larrformData,$id)
	{
		$paramArray = array(
				'FeeCategoryCode' => $larrformData['FeeCategoryCode'],
				'Description' => $larrformData['Description'],
				'SequenceOrder' => $larrformData['SequenceOrder'],
				'DefaultLanguage' => $larrformData['DefaultLanguage'],
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate'],
				'IdUniversity'=>$larrformData['IdUniversity'],
		);
		$where_up = "  IdFeeCategory =' ".$id."' ";
		$this->lobjDbAdpt->update('tbl_fee_category',$paramArray, $where_up);

	}

	public function updatefeesetup($larrformData,$id){

		$paramArray = array(
				'FeeCode' => $larrformData['FeeCode'],
				'ChargeCode' => $larrformData['ChargeCode'],
				'Description' => $larrformData['Description'],
				'DefaultLanguage' => $larrformData['DefaultLanguage'],
				'ShortDescription' => $larrformData['ShortDescription'],
				'FeeCategory' => $larrformData['FeeCategory'],
				'ChargingPeriod' => $larrformData['ChargingPeriod'],
				'Refundable' => $larrformData['Refundable'],
				'Active' => $larrformData['Active'],
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate'],
				'IdUniversity'=>$larrformData['IdUniversity'],
		);

		$where_up = "  IdFeeSetup =' ".$id."' ";
		$this->lobjDbAdpt->update('tbl_fee_setup',$paramArray, $where_up);


	}

	public function checkfeecategorycode($larrformData,$id){
		$feecategorycode = $larrformData['FeeCategoryCode'];
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_category"), array("a.*"))
		->where("a.FeeCategoryCode = ?",$feecategorycode)
		->where("a.IdFeeCategory != ?",$id);

		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function checkcategorycode($larrformData){
		$feecategorycode = $larrformData['FeeCategoryCode'];
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_category"), array("a.*"))
		->where("a.FeeCategoryCode = ?",$feecategorycode);

		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function getfeecategoryById($id){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_category"), array("a.*"))
		->where("a.IdFeeCategory = ?",$id);
		$result = $this->lobjDbAdpt->fetchall($select);

		return $result;

	}

	public function getfeesetupById($id){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_setup"), array("a.*"))
		->where("a.IdFeeSetup = ?",$id);
		$result = $this->lobjDbAdpt->fetchall($select);

		return $result;

	}

	public function getcategorgcode(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_category"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchall($select);

		return $result;

	}

	public function fngetcode(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_ledgercode"), array("a.*"))
		->where("a.MISCCODE =?",'SDEP')
		->orwhere("a.MISCCODE =?",'SF');
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;

	}

	public function fngetcompanycode($IdUniversity){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_universitymaster"), array('a.IdUniversity','a.Univ_Name'))
		->where("a.IdUniversity = ?",$IdUniversity);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;

	}

	public function checkfeecode($larrformData){
		$feecode = $larrformData['FeeCode'];
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_setup"), array("a.*"))
		->where("a.FeeCode = ?",$feecode);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function addfeesetup($larrformData)
	{
		$paramArray = array(
				'FeeCode' => $larrformData['FeeCode'],
				'ChargeCode' => $larrformData['ChargeCode'],
				'Description' => $larrformData['Description'],
				'DefaultLanguage' => $larrformData['DefaultLanguage'],
				'ShortDescription' => $larrformData['ShortDescription'],
				'FeeCategory' => $larrformData['FeeCategory'],
				'ChargingPeriod' => $larrformData['ChargingPeriod'],
				'Refundable' => $larrformData['Refundable'],
				'Active' => $larrformData['Active'],
				'UpdUser'=>$larrformData['UpdUser'],
				'UpdDate'=>$larrformData['UpdDate'],
				'IdUniversity'=>$larrformData['IdUniversity'],
		);

		$this->lobjDbAdpt->insert('tbl_fee_setup',$paramArray);

	}

	public function getfeesetup()
	{
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "fee_item"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function addStudentActivitySetup($larrformData)
	{
		unset($larrformData['Save']);
		$this->lobjDbAdpt->insert('tbl_studentactivity_setup',$larrformData);
		$getlID = $this->lobjDbAdpt->lastInsertId();
		return $getlID;
	}

	public function getstudentactivityDetails($IdUniversity) 
	{

		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_studentactivity_setup"), array("a.*"))
		->where('a.IdUniversity =?',$IdUniversity);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function updatestudentactivityDetails($larrformData,$IdUniversity){

		unset($larrformData['Save']);
		unset($larrformData['IdUniversity']);
		$where = 'IdUniversity = '.$IdUniversity;
		$this->lobjDbAdpt->update('tbl_studentactivity_setup',$larrformData,$where);
	}


	public function fngetfeesetuplist(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "fee_item"), array("key" => "a.fi_id","value" => "a.fi_name"))
		->where('a.fi_active = ?',1);
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function fngetchargecode(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_ledgercode"), array("a.*"));
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function fnGetBankCodeList(){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_bankcode"), array("a.*"))
		//->joinLeft(array('b'=>'tbl_ledgercode'),'b.ACCOUNTCODE = a.BANKACCT_CODE',array('b.DESCRIPTION'))
		->where("a.MISCCODE = ?","AR");
		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}

	public function getFeesetupBycatId($catId){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_setup"), array("a.IdFeeSetup",'a.FeeCode'))
		->where("a.FeeCategory = ?",$catId);
		return $result = $this->lobjDbAdpt->fetchall($select);
	}

	public function fngetfaculty(){

		$select = $this->lobjDbAdpt->select()

				->from(array("a" => "tbl_collegemaster"), array("a.IdCollege","a.CollegeName"));

		$result = $this->lobjDbAdpt->fetchall($select);
		return $result;
	}


	public function getFeesetupDetById($catId){
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_fee_setup"), array("a.*"))
		->where("a.IdFeeSetup = ?",$catId);
		return $result = $this->lobjDbAdpt->fetchrow($select);
	}

	public function getRefundableByFeeCode($id){
		$select = $this->lobjDbAdpt->select()
						->from(array("a" => "tbl_fee_setup"), array("a.Refundable"))
						->where("a.IdFeeSetup = ?",$id);
		$getFeeSetup = $this->lobjDbAdpt->fetchall($select);
		$Refundable = '0';
		if(count($getFeeSetup)>0){
			$feeRefundableStatus = $getFeeSetup[0]['Refundable'];
			if($feeRefundableStatus=='0' || $feeRefundableStatus=='2') { $Refundable = '0'; }
			else { $Refundable = '1';  }
		} else { $Refundable = '0';  }
		return $Refundable;

	}


}