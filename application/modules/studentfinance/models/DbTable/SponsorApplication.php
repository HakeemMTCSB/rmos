<?php
class Studentfinance_Model_DbTable_SponsorApplication extends Zend_Db_Table_Abstract 
{

    //put your code here
    protected $_name = 'sponsor_application';
    protected $_primary = "sa_id";

    public $cust_types = array(
        1 => 'Student',
        2 => 'Applicant',
        3 => 'Admin'
    );

    public $status = array(
        1 => 'APPLIED',
        2 => 'OFFERED',
        3 => 'REJECTED',
        4 => 'COMPLETED',
        5 => 'INCOMPLETE',
        6 => 'CANCEL',
        7 => 'OFFERED',
        8 => 'SHORTLISTED',
        9 => 'OFFERED-ACCEPTED',
        10 => 'OFFERED-DECLINED'
    );

    public $scholarship_types = array(
        1 => 'Financial Aid (Fisabilillah Trust Fund)',
        2 => 'Financial Aid',
    );

    protected $_dependentTables = array('Studentfinance_Model_DbTable_ScholarshipDetail');

    protected $_referenceMap    = array(
        'Sponsor' => array(
            'columns'           => 'sa_af_id',
            'refTableClass'     => 'Studentfinance_Model_DbTable_ApplicantFinancial',
            'refColumns'        => 'af_id'
        ),
        'FinancialAid' => array(
            'columns'           => 'sa_scholarship_type',
            'refTableClass'     => 'Studentfinance_Model_DbTable_FinancialAid',
            'refColumns'        => 'id'
        ),
        'Student' => array(
            'columns'           => 'sa_cust_id',
            'refTableClass'     => 'Registration_Model_DbTable_Studentregistration',
            'refColumns'        => 'IdStudentRegistration'
        ),
        'Semester' => array(
            'columns'           => 'sa_semester_id',
            'refTableClass'     => 'GeneralSetup_Model_DbTable_Semestermaster',
            'refColumns'        => 'IdSemesterMaster'
        ),
        'Transaction' => array(
            'columns'           => 'sa_cust_id',
            'refTableClass'     => 'App_Model_Application_DbTable_ApplicantTransaction',
            'refColumns'        => 'at_trans_id'
        ),
    );

    public function fetch_to_be_processed($type=2, $search = false) {
        $a = new GeneralSetup_Model_DbTable_Semestermaster();
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array('st.sct_schId'))
            //->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'ApplicantFinancial.af_scholarship_apply=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), $this->_name.'.sa_semester_id=sem.IdSemesterMaster')
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            //->where($this->_name . ".sa_status = ?", 4)
            ->where($this->_name . ".sa_cust_type = ?", $type)
            ->order($this->_name . ".sa_score ASC");

        if ( $type == 1 ){
            $where->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id',array('sp.appl_fname','sp.appl_lname', 'appl_religion'))
            ->joinLeft(array('pr'=>'tbl_program'), 'r.IdProgram = pr.IdProgram')
            ->joinLeft(array('prs'=>'tbl_program_scheme'), 'r.IdProgramScheme = prs.IdProgramScheme')
            ->joinLeft(array('df'=>'tbl_definationms'), 'prs.mode_of_program = df.idDefinition', array('mode'=>'df.DefinitionDesc'));
            
            if ($search != false){
                if (isset($search['Program']) && $search['Program']!=''){
                    $where->where('r.IdProgram = ?', $search['Program']);
                }
                if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                    $where->where('r.IdProgramScheme = ?', $search['ProgramScheme']);
                }
                if (isset($search['SemesterApplied']) && $search['SemesterApplied']!=''){
                    $where->where($this->_name.'.sa_semester_id = ?', $search['SemesterApplied']);
                }
                if (isset($search['StudentName']) && $search['StudentName']!=''){
                    $where->where($db->quoteInto('CONCAT_WS(" ", sp.appl_fname, sp.appl_lname) like ?', '%'.$search['StudentName'].'%'));
                }
                if (isset($search['StudentID']) && $search['StudentID']!=''){
                    $where->where($db->quoteInto('r.registrationId like ?', '%'.$search['StudentID'].'%'));
                }
                if (isset($search['Status']) && $search['Status']!=''){
                    if ($search['Status']==2 || $search['Status']==7){
                        $where->where($this->_name.'.sa_status IN (2, 7)');
                    }else{
                        $where->where($this->_name.'.sa_status = ?', $search['Status']);
                    }
                }
            }
        }
        
        if ( $type == 2 ){
            $where->joinLeft(array('trans'=>'applicant_transaction'), $this->_name.'.sa_cust_id=trans.at_trans_id')
                ->joinLeft(array('atprofile'=>'applicant_profile'), 'trans.at_appl_id=atprofile.appl_id')
                ->joinLeft(array('atprogram'=>'applicant_program'), 'trans.at_trans_id=atprogram.ap_at_trans_id')
                ->joinLeft(array('nation'=>'tbl_countries'), 'atprofile.appl_nationality=nation.idCountry', array('nationname'=>'nation.CountryName'))
                ->joinLeft(array('af'=>'applicant_financial'), $this->_name.'.sa_af_id = af.af_id', array())
                ->joinLeft(array('tss'=>'tbl_scholarshiptagging_sct'), 'af.af_scholarship_apply = tss.sct_Id', array())
                ->joinLeft(array('int'=>'tbl_intake'), 'int.IdIntake = tss.sct_intake', array('int.IntakeDesc'));

            if ($search != false){
                if (isset($search['Program']) && $search['Program']!=''){
                    $where->where('atprogram.ap_prog_id = ?', $search['Program']);
                }
                if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                    $where->where('atprogram.ap_prog_scheme = ?', $search['ProgramScheme']);
                }
                if (isset($search['SemesterApplied']) && $search['SemesterApplied']!=''){
                    $where->where('tss.sct_intake = ?', $search['SemesterApplied']);
                }
                if (isset($search['StudentName']) && $search['StudentName']!=''){
                    $where->where($db->quoteInto('CONCAT_WS(atprofile.appl_fname, " ", atprofile.appl_lname) like ?', '%'.$search['StudentName'].'%'));
                }
                if (isset($search['StudentID']) && $search['StudentID']!=''){
                    $where->where($db->quoteInto('trans.at_pes_id like ?', '%'.$search['StudentID'].'%'));
                }
                if (isset($search['Status']) && $search['Status']!=''){
                    if ($search['Status']==2 || $search['Status']==7){
                        $where->where($this->_name.'.sa_status IN (2, 7)');
                    }else{
                        $where->where($this->_name.'.sa_status = ?', $search['Status']);
                    }
                }
            }
        }
        
        $applications = $this->fetchAll($where);

        return($applications);
    }
    
    public function fetch_to_be_processed2($type=2, $search = false) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = $db->select()
            ->from(array($this->_name))
            //->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            //->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'ApplicantFinancial.af_scholarship_apply=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), $this->_name.'.sa_semester_id=sem.IdSemesterMaster')
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            //->where($this->_name . ".sa_status = ?", 4)
            ->where($this->_name . ".sa_cust_type = ?", $type)
            ->order($this->_name . ".sa_score ASC");

        if ( $type == 1 ){
            $where->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id')
            ->joinLeft(array('pr'=>'tbl_program'), 'r.IdProgram = pr.IdProgram')
            ->joinLeft(array('prs'=>'tbl_program_scheme'), 'r.IdProgramScheme = prs.IdProgramScheme')
            ->joinLeft(array('df'=>'tbl_definationms'), 'prs.mode_of_program = df.idDefinition', array('mode'=>'df.DefinitionDesc'));
            
            if ($search != false){
                if (isset($search['Program']) && $search['Program']!=''){
                    $where->where('r.IdProgram = ?', $search['Program']);
                }
                if (isset($search['ProgramScheme']) && $search['ProgramScheme']!=''){
                    $where->where('r.IdProgramScheme = ?', $search['ProgramScheme']);
                }
                if (isset($search['SemesterApplied']) && $search['SemesterApplied']!=''){
                    $where->where($this->_name.'.sa_semester_id = ?', $search['SemesterApplied']);
                }
                if (isset($search['StudentName']) && $search['StudentName']!=''){
                    $where->where($db->quoteInto('CONCAT_WS(" ", sp.appl_fname, sp.appl_lname) like ?', '%'.$search['StudentName'].'%'));
                }
                if (isset($search['StudentID']) && $search['StudentID']!=''){
                    $where->where($db->quoteInto('r.registrationId like ?', '%'.$search['StudentID'].'%'));
                }
                if (isset($search['Status']) && $search['Status']!=''){
                    if ($search['Status']==2 || $search['Status']==7){
                        $where->where($this->_name.'.sa_status IN (2, 7)');
                    }else{
                        $where->where($this->_name.'.sa_status = ?', $search['Status']);
                    }
                }
            }
        }
        
        $applications = $db->fetchAll($where);

        return($applications);
    }

    public function fetch_to_be_shortlisted() {
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id',array('sp.appl_fname','sp.appl_lname', 'appl_religion'))
            ->where($this->_name . ".sa_status = ?", 1)
            ->where($this->_name . ".sa_cust_type = ?", 1)
        ;

        $applications = $this->fetchAll($where);

        return($applications);
    }

    public function fetch_accepted() {
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id',array('sp.appl_fname','sp.appl_lname', 'appl_religion'))
            ->where($this->_name . ".sa_status = ?", 5)
            ->where($this->_name . ".sa_cust_type = ?", 1)
        ;

        $applications = $this->fetchAll($where);

        return($applications);
    }

    public function fetch_rejected() {
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id',array('sp.appl_fname','sp.appl_lname', 'appl_religion'))
            ->where($this->_name . ".sa_status = ?", 3)
            ->where($this->_name . ".sa_cust_type = ?", 1)
        ;

        $applications = $this->fetchAll($where);

        return($applications);
    }

    public function fetch_nonacceptance() {
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id',array('sp.appl_fname','sp.appl_lname', 'appl_religion'))
            ->where($this->_name . ".sa_status = ?", 6)
            ->where($this->_name . ".sa_cust_type = ?", 1)
        ;

        $applications = $this->fetchAll($where);

        return($applications);
    }


    public function fetch_offered() {
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('r' => 'tbl_studentregistration'),$this->_name.'.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp'=> 'student_profile'),'sp.id=r.sp_id',array('sp.appl_fname','sp.appl_lname', 'appl_religion'))
            ->where($this->_name . ".sa_status = ?", 2)
            ->where($this->_name . ".sa_cust_type = ?", 1)
        ;

        $applications = $this->fetchAll($where);

        return($applications);
    }

    public function fetch_retracted()
    {
        $where = $this->select($this->_name)
            ->setIntegrityCheck(false)
            ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name')
            ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'ApplicantFinancial.af_scholarship_apply=st.sct_Id', array())
            ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
            ->joinLeft(array('r' => 'tbl_studentregistration'), $this->_name . '.sa_cust_id=r.IdStudentRegistration')
            ->joinLeft(array('sp' => 'student_profile'), 'sp.id=r.sp_id', array('sp.appl_fname', 'sp.appl_lname', 'appl_religion'))
            ->where($this->_name . ".sa_status = ?", 7)
            ->where($this->_name . ".sa_cust_type = ?", 1);

        $applications = $this->fetchAll($where);

        return ($applications);
    }

    public function get_applicant($sa_cust_id, $sa_cust_type) {
        $sponsor_applications = $this->fetchAll(array('sa_cust_id = ?' => $sa_cust_id));
        $sponsor_application = $sponsor_applications->current();

        if($sa_cust_type == 1) {
            $applicant = $sponsor_application->findParentRow('Registration_Model_DbTable_Studentregistration');
        } else if($sa_cust_type == 2) {
			$appdb = new App_Model_Application_DbTable_ApplicantTransaction();
			
			$applicant = $appdb->getTransaction($sa_cust_id,1);
            //$applicant = $sponsor_application->findParentRow('App_Model_Application_DbTable_ApplicantTransaction');
        }

        return($applicant);
    }

    public function set_status($as_id, $status, $remarks=false) {
        $where = $this->getAdapter()->quoteInto('sa_id IN (?)', $as_id);
        
        $data['sa_status'] = $status;
        if ($remarks != false){
            $data['sa_remarks'] = $remarks;
        }
        
        if ($status == 2){
            $data['sa_offerdate'] = date('Y-m-d H:i:s');
        }
        
        if ($status == 9 || $status == 10){
            $data['sa_acceptdeclinedate'] = date('Y-m-d H:i:s');
        }
        
        $this->update($data,$where);
        return(true);
    }
    
    public function revert_status($as_id, $status, $remarks=false) {
        $where = $this->getAdapter()->quoteInto('sa_id = ?', $as_id);
        
        $data['sa_status'] = $status;
        if ($remarks != false){
            $data['sa_remarks'] = $remarks;
        }
        
        if ($status == 2){
            $data['sa_offerdate'] = date('Y-m-d H:i:s');
        }
        
        if ($status == 9 || $status == 10){
            $data['sa_acceptdeclinedate'] = date('Y-m-d H:i:s');
        }
        
        $this->update($data,$where);
        return(true);
    }
    
    public function fetch_my_application($sa_cust_id) {
        $where = $this->select($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft(array('ApplicantFinancial' => 'applicant_financial'), $this->_name . ".sa_af_id = ApplicantFinancial.af_id ", 'ApplicantFinancial.af_sponsor_name as sponsor_name'  )
                    ->where($this->_name . ".sa_cust_id = ?", $sa_cust_id)
                    ;

        $applications = $this->fetchAll($where);
        return($applications);   
    }

    //checks if user has applied
    public function has_applied($sa_cust_id, $sa_cust_type) {
        $where = $this->getAdapter()->quoteInto( "sa_cust_id = ? AND sa_cust_type = ?" , $sa_cust_id, $sa_cust_type );
        $application = $this->fetchAll($where);

        if(count($application) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getcalculation($sa_id, $breakdown = false) {
        $scholarships = $this->find($sa_id);
        if($scholarships->count() > 0) {
            $scholarship = $scholarships->current();
        } else {
            return false;
        }
        //get profile
        $student_registration = $scholarship->findParentRow('Registration_Model_DbTable_Studentregistration');
        $profile = $student_registration->findParentRow('Records_Model_DbTable_Studentprofile');


        //get scholarship app detail
        $scholarship_details = $scholarship->findDependentRowset('Studentfinance_Model_DbTable_ScholarshipDetail');
        if($scholarship_details->count() > 0) {
            $scholarship_detail = $scholarship_details->current();
        } else {
            return false;
        }
        if($scholarship_detail->empstatus == 2) {
            $industry_type = 4;
        } else {
            $industry_type = $scholarship_detail->industry;
        }

        $Scholarship = new Studentfinance_Model_DbTable_Scholarship();
        $contribution_score = $Scholarship->contribution_score($profile->appl_dob, $industry_type);

        $education_level = $scholarship_detail->findParentRow('App_Model_General_DbTable_Qualificationmaster');
        $academic_score = $Scholarship->academic_score($scholarship_detail->education_result, $education_level->QualificationRank);

        $affordability_score = $Scholarship->affordability_score($scholarship_detail->income,$scholarship_detail->totaldependent, $profile->appl_nationality);

        $ScholarshipWeight = new Studentfinance_Model_DbTable_ScholarshipWeight();
        $contribution_weight = $ScholarshipWeight->get_weight('contribution');
        $academic_weight = $ScholarshipWeight->get_weight('academic');
        $affordability_weight  = $ScholarshipWeight->get_weight('affordability');

        $total_score = ($contribution_score * $contribution_weight)
                        + ($academic_score * $academic_weight)
                        + ($affordability_score * $affordability_weight)
                        ;
        if($breakdown) {
            $result['contribution_score'] = $contribution_score;
            $result['academic_score'] = $academic_score;
            $result['affordability_score'] = $affordability_score;
            $result['total_score'] = $total_score;

            return($result);
        } else {
            return($total_score);
        }
    }

    public function getexisting($student_id, $sa_scholarship_type, $semester_id) {
        $select = $this->select()
                    ->where('sa_scholarship_type = ?', $sa_scholarship_type)
                    ->where('sa_semester_id = ?', $semester_id)
                    ->where('sa_cust_id = ?', $student_id)
                ;
        $cur_rec = $this->fetchRow($select);
        return($cur_rec);
    }
    
    public function getDataByStudent(){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'sponsor_application'))
            ->where('a.sa_cust_type = 1');

        $row = $db->fetchRow($select);

        return $row;
    }

    public function updateSponsorApplication($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_application', $data, 'sa_id = '.$id);
        return $update;
    }
}

