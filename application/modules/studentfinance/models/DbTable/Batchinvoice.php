<?php 
class Studentfinance_Model_DbTable_Batchinvoice extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_invoicemaster';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnAddbatchInvoice($formData){
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_invoicemaster";
	 		$db->insert($table,$formData);
	     return     $lastinvoiceid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_invoicemaster','IdInvoice');
	       
	}
	
	public function fnGetProgram($idCollege){ //function to get AccountType
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("tbl_program"=>"tbl_program"),array("key"=>"tbl_program.IdProgram","value"=>"tbl_program.ProgramName"))
									 ->where("tbl_program.IdCollege=?",$idCollege);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fnAddbatchInvoiceDetails($formDatadetails){
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_invoicedetails";
	 		$db->insert($table,$formDatadetails);	      
	}
	
	
	
 	public function fngetStudentDropdown() { //Function to get the Program Branch details
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 	->from(array("a"=>"tbl_studentapplication"),array("value"=>"CONCAT_WS(' ',IFNULL(a.FName,''),IFNULL(a.MName,''),IFNULL(a.LName,''))"))
		 				    ->join(array('b'=>'tbl_studentregistration'),'a.IdApplication=b.IdApplication',array("key"=>"b.IdStudentRegistration",))
		                    ->where("a.idsponsor is null")
		               		->group('a.IdApplication');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetStudentApplicationDetails()//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
 						->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
 						->join(array('tbl_semester'=>'tbl_semester'),'a.IdSemestersyllabus = tbl_semester.IdSemester',array("tbl_semester.ShortName as semname")) 						
 						->join(array('d'=>'tbl_collegemaster'),'b.idCollege = d.IdCollege');
					
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fnSearchStudentApplication($post = array()) { //Function to get the user details
		
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array('a'=>'tbl_studentregistration'),array('a.*'))
       								->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
       								->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
       								->join(array('tbl_semester'=>'tbl_semester'),'a.IdSemestersyllabus = tbl_semester.IdSemester',array("tbl_semester.ShortName as semname")) 						
 									->join(array('d'=>'tbl_collegemaster'),'b.idCollege = d.IdCollege')
       								->where('b.StudentId  like "%" ? "%"',$post['field4'])
       								;
       								
			if(isset($post['field1']) && !empty($post['field1']) ){
				$lstrSelect = $lstrSelect->where("b.IdApplication = ?",$post['field1']);										
			}			
			if(isset($post['field13']) && !empty($post['field13']) ){
				$lstrSelect = $lstrSelect->where("b.IDCourse = ?",$post['field13']);										
			}			
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("b.idCollege = ?",$post['field5']);										
			}
			if(isset($post['field10']) && !empty($post['field10']) ){
				$lstrSelect = $lstrSelect->where("a.IdSemestersyllabus = ?",$post['field10']);										
			}
			
       //echo $lstrSelect;die();
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 
	 
	public function fngetSubjectNameCombo()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrselect = $lobjDbAdpt->select()
							->from(array("pg"=>"tbl_subjectmaster"),array("pg.IdSubject AS key","pg.SubjectName AS value"))
							->where("pg.Active = 1")
							->order("pg.SubjectName");
		$larrresult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrresult;
	}
	
    public function fnAddSubjectMarks($formData) { //Function for adding the Program Branch details to the table
		unset ( $formData ['Save']);
	    return $this->insert($formData);
	}
	
	public function fnDeleteSubjectMarks($larrformDatas) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_subjectmarksentry";
		$where[] = $db->quoteInto('IdStudentRegistration  = ?', $larrformDatas['IdStudentRegistration']);
	    $where[] = $db->quoteInto('idStaff  = ?', $larrformDatas['idStaff']);
	    $where[] = $db->quoteInto('idSubject  = ?', $larrformDatas['idSubject']);
	    $db->delete($table,$where);
	}
	
	public function fnGPACalculation($IdStudentRegistration,$idSubject,$subjectmarks) { //Function to get the user details
		$auth = Zend_Auth::getInstance();
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentregsubjects"),array("a.IdSubject"))
       								->join(array('b'=>'tbl_studentregistration'),'a.IdStudentRegistration  = b.IdStudentRegistration',array('b.IdStudentRegistration','b.registrationId','b.IdSemestersyllabus'))
       								->join(array('c'=>'tbl_landscape'),'b.IdLandscape   = c.IdLandscape',array('c.IdLandscape','c.LandscapeType','c.IdStartSemester','c.SemsterCount'))
                					->join(array('d'=>'tbl_program'),'c.IdProgram = d.IdProgram',array('d.IdProgram','d.ProgramName'))
                					->join(array('e'=>'tbl_subjectmaster'),'a.IdSubject = e.IdSubject',array('e.IdSubject','e.SubjectName','e.CreditHours'))
                					->join(array('f'=>'tbl_studentapplication'),'b.IdApplication = f.IdApplication',array('f.FName','f.MName','f.LName','f.IdApplication'))
                					->joinLeft(array('i'=>'tbl_subjectmarksentry'),'b.IdStudentRegistration = i.IdStudentRegistration AND a.IdSubject=i.idSubject',array('i.idSubjectMarksEntry','i.subjectmarks'))
                					->join(array('gs'=>'tbl_gradesetup'),'gs.IdProgram = c.IdProgram  AND gs.IdSemester = b.IdSemestersyllabus AND gs.IdSubject = a.IdSubject',array('gs.GradePoint','gs.IdSemester'))
                					->where('a.IdStudentRegistration = ?',$IdStudentRegistration)
                					->where('a.IdSubject = ?',$idSubject)
                					->where($subjectmarks .' '."BETWEEN gs.MinPoint and gs.MaxPoint")
       								->where("f.Offered = 1")
       								->where("f.Termination = 0")
       								->where("f.Accepted = 1")
       								->where("c.Active = 123")
									->order("e.SubjectName")
									->group("a.IdSubject");
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 

	 public function fnAddGPACalculationDtls($arrIdStudentRegistration,$IdSemester,$sumGradePoint,$gpa,$ldtsystemDate,$UpdUser){
	 			$db = Zend_Db_Table::getDefaultAdapter();
				$table = "tbl_gpacalculation";
				$countvar=count($arrIdStudentRegistration);				
				$larrAddGPA = array('GradePoint'=>$sumGradePoint,
									'IdStudentRegistration'=>$arrIdStudentRegistration,
									'Gpa'=>$gpa,
									'IdSemester'=>$IdSemester,
									'UpdDate'=>$ldtsystemDate,
									'UpdUser'=>$UpdUser									
							);
				
				$db->insert($table,$larrAddGPA);	
			}
			
		public function fnUpdateSubjectMarks($idSubjectMarksEntry,$subjectmarks){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
			$UpdateData["subjectmarks"] = $subjectmarks;
			$lstrTable = "tbl_subjectmarksentry";
			$lstrWhere = "idSubjectMarksEntry = ".$idSubjectMarksEntry;
			$lstrMsg = $lobjDbAdpt->update($lstrTable,$UpdateData,$lstrWhere);
			return $lstrMsg;
		}

}
?>