<?php

class Studentfinance_Model_DbTable_PublishingSetup extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_scholarship_student';
	protected $_program = 'tbl_scholarship_student_program';

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function getAll($post=array())
	{
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a' => $this->_name ),array('a.*'))
		->joinLeft(array('b'=> 'tbl_semestermaster'),'a.semester_id=b.IdSemesterMaster', array('SemesterMainName as semester_name'))
		->joinLeft(array('c'=>$this->_program),'c.setup_id=a.id', array('GROUP_CONCAT( c.program_id ) as Programs'));
		
		foreach ( $post as $var => $val )
		{
			$lstrSelect->where($var.'=?', $val);
		}
		
		$lstrSelect->group('c.setup_id');

		return $result = $this->lobjDbAdpt->fetchAll($lstrSelect);

	}

	public function getData($id)
	{
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => $this->_name), array("a.*"))
		->where("a.id = ?",$id);
		$result = $this->lobjDbAdpt->fetchRow($select);

		return $result;
	}

	public function addData($data)
	{
		$this->lobjDbAdpt->insert($this->_name, $data);

		$_id = $this->lobjDbAdpt->lastInsertId();

		return $_id;
	}

	public function updateData($data, $id )
	{
		$this->lobjDbAdpt->update($this->_name, $data, 'id='.(int) $id );
	}

	public function addProgram($data)
	{
		$this->lobjDbAdpt->insert($this->_program, $data);

		$_id = $this->lobjDbAdpt->lastInsertId();

		return $_id;
	}

	public function getPrograms($setup_id)
	{
		$select = $this->lobjDbAdpt->select()
		->from(array("a" => $this->_program), array("a.*"))
		->join(array('b' => 'tbl_program'), 'a.program_id=b.IdProgram', array('b.ProgramName'))
		->where("a.setup_id = ?",$setup_id);
		$result = $this->lobjDbAdpt->fetchAll($select);

		return $result;
	}
	

	public function getProgramsById($ids)
	{
		$select = $this->lobjDbAdpt->select()
					->from(array('a'=>'tbl_program'), array('IdProgram','ProgramName'))
					->where('IdProgram IN ('.$ids.')');


		$result = $this->lobjDbAdpt->fetchAll($select);

		return $result;
	}
}

