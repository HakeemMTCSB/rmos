<?php
class Studentfinance_Model_DbTable_Invoiceentry extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_invoicemain';
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fngetapplist(){
		$select = $this->select()
		->from(array('a'=>'tbl_invoicemain'),array('a.AUTOINC'));
		$result = $this->fetchAll($select);
		return (count($result));
	}

	public function fnsaveinvoiceentryApp($larrformData,$IdUniversity,$appCount,$lstrcoderesult,$lstrinvoicenumber){
		$auth = Zend_Auth::getInstance();
		$iduser = $auth->getIdentity()->iduser;
		$systemdate = date('Y-m-d');
		//$data['BILLNUM'] = "IE-".($appCount + 1);
		if($lstrinvoicenumber == 1){
			$data['BILLNUM'] = $lstrcoderesult;
		}else{
			$data['BILLNUM'] = $larrformData['InvoiceId'];
		}
		$data['CMPYCODE'] = $IdUniversity;
		$data['BRANCH_ID'] = $larrformData['IdBranch'];
		$data['CURRENCY'] = $larrformData['Currency'];
		$data['STUDID'] = $larrformData['StudentCode'];
		$data['DOCDATE'] = $systemdate;
		$data['BILLDESC'] = $larrformData['Description'];
		$data['TOTAL_AMOUNT'] = $larrformData['TotalAmount'];
		$data['IdProgram'] = $larrformData['IdProgram'];
		$data['TOTAL_PAID'] = 0;
		$data['TOTAL_BLNC'] = $larrformData['TotalAmount'];
		$data['STATUS'] = 0;
		$data['ID_CREATE'] = $iduser;
		$data['ID_CANCEL'] = 0;
		$data['DT_CANCEL'] = 0;
		$data['BCHBILLNUM'] = '';
		$data['DT_CREATE'] = $systemdate;
		$data['SOURCE'] = "MANUAL";
		$data['SemesterID'] = $larrformData['SemesterCode'];
		$data['IdStudentRegistration'] = $larrformData['StudentId'];
		$this->insert($data);
		return $this->lobjDbAdpt->lastInsertId();
	}

	public function fnsaveinvoicedetailinfo($larrformData,$IdUniversity,$appId){
		$auth = Zend_Auth::getInstance();
		$iduser = $auth->getIdentity()->iduser;
		$systemdate = date('Y-m-d');
		$table = 'tbl_invoicedetl';
		$data = array();
		if(isset($larrformData['Invoiceseqgrid'])){
			$i = 0;
			foreach($larrformData['Invoiceseqgrid'] as $invoice){
				$data['BILLNUM'] = $appId;
				$data['BRANCH_ID'] = $larrformData['IdBranch'];
				$data['BILLSEQ'] = $larrformData['Invoiceseqgrid'][$i];
				$data['FEECODE'] = $larrformData['FeeCodegrid'][$i];
				$data['IsRefundable'] = $larrformData['IsRefundablegrid'][$i];
				$data['FEEDESC'] = $larrformData['Description'];
				$data['CMPYCODEDB'] = '';
				$data['ACCTCODEDB'] = $larrformData['Debitgrid'][$i];
				$data['CMPYCODECH'] = '';
				$data['ACCTCODECH'] = $larrformData['Creditgrid'][$i];
				$data['COSTCTR'] = $larrformData['CostCtrgrid'][$i];
				$data['TOTAL_AMOUNT'] = $larrformData['Amountgrid'][$i];
				$data['TOTAL_PAID'] = 0;
				$data['TOTAL_BLNC'] = $larrformData['Amountgrid'][$i];
				$data['IdBranch'] = $larrformData['IdBranch'];
				$this->lobjDbAdpt->insert($table,$data);
				$i++;
			}
		}
	}

	public function fnupdateapproveStatus($appId){
		$data = array();
		$data['STATUS'] = 1;
		$where_up = "AUTOINC ='".$appId."' ";
		$this->lobjDbAdpt->update('tbl_invoicemain',$data,$where_up);
	}

	public function fnupdaterejectStatus($appId,$userId,$UpdDate){
		$data = array();
		$data['STATUS'] = 2;
		$data['ID_CANCEL'] = $userId;
		$data['DT_CANCEL'] = $UpdDate;
		$where_up = "AUTOINC ='".$appId."' ";
		$this->lobjDbAdpt->update('tbl_invoicemain',$data,$where_up);
	}

	public function fngetappdetails($appId){
		$select = $this->lobjDbAdpt->select()
									->from(array('a'=>'tbl_invoicemain'),array('a.AUTOINC','a.BILLNUM','a.SemesterID','a.DOCDATE','a.STATUS','a.BILLDESC','a.TOTAL_AMOUNT'))
									->joinLeft(array('b'=>'tbl_studentregistration'),'a.IdStudentRegistration = b.IdStudentRegistration',array('CONCAT_WS(" ",b.FName,b.MName,b.LName) as Name','b.registrationId'))
									->joinLeft(array('c'=>'tbl_user'),'c.iduser = a.ID_CREATE',array('c.loginName'))
									->joinLeft(array('d'=>'tbl_branchofficevenue'),'a.BRANCH_ID = d.IdBranch',array('d.BranchName'))
									->joinLeft(array('e'=>'tbl_currency'),'a.CURRENCY = e.AUTOINC',array('e.CURRDESC'))
									->where('a.AUTOINC = ?',$appId);
		$result = $this->lobjDbAdpt->fetchRow($select);
		return $result;
	}

	public function fngetinvoicedetails($appId){
		$select = $this->lobjDbAdpt->select()
								   ->from(array('a'=>'tbl_invoicedetl'),array('a.BILLSEQ','a.TOTAL_AMOUNT','a.COSTCTR','a.FEEDESC'))
								   ->joinLeft(array('b'=>'tbl_fee_setup'),'a.FEECODE = b.IdFeeSetup',array('b.FeeCode'))
								   ->joinLeft(array('c'=>'tbl_ledgercode'),'c.AUTOINC = a.ACCTCODEDB',array('CONCAT_WS("-",c.ACCOUNTCODE,c.DESCRIPTION) as Debit'))
								   ->joinLeft(array('d'=>'tbl_ledgercode'),'d.AUTOINC = a.ACCTCODECH',array('CONCAT_WS("-",d.ACCOUNTCODE,d.DESCRIPTION) as Credit'))
								   ->where('a.BILLNUM = ?',$appId)
								   ->order('a.BILLSEQ');
		$result = $this->lobjDbAdpt->fetchAll($select);

		if(count($result) != 0){
			$i = 0;
			foreach($result as $res){
				if($res['COSTCTR']!=''){
					$pieces = explode('_',$res['COSTCTR']);
					if($pieces[1] == "faculty"){
						$select1 = $this->lobjDbAdpt->select()
						->from(array('a'=>'tbl_collegemaster'),array('a.CollegeName as CostCtr'))
						->where('a.IdCollege = ?',$pieces[0]);
						$result1 = $this->lobjDbAdpt->fetchRow($select1);
						$result[$i]['CostCtr'] = $result1['CostCtr'];
					}else if($pieces[1] == "school"){
						$select1 = $this->lobjDbAdpt->select()
						->from(array('a'=>'tbl_schoolmaster'),array('a.SchoolName as CostCtr'))
						->where('a.idSchool = ?',$pieces[0]);
						$result1 = $this->lobjDbAdpt->fetchRow($select1);
						$result[$i]['CostCtr']	= $result1['CostCtr'];
					}
					$i++;
				}

			}
		}
		return $result;
	}
	
	public function fngetinvoicecourses($appId)
	{
		$select = $this->lobjDbAdpt->select()
								   ->from(array('a'=>'tbl_invoicecourse'),array('a.*'))
								   ->join(array('b'=>'tbl_subjectmaster'),'a.COURSE_CODE=b.SubCode', array('b.'.($this->locale=='ar_YE'?'subjectMainDefaultLanguage':'SubjectName').' as COURSE_NAME'))
								   ->where('a.BILLNUM = ?',$appId)
								   ->order('a.BILLSEQ');

		$result = $this->lobjDbAdpt->fetchAll($select);
		
		return $result;
	}
	
	public function fngetinvoicedetailsprevCR($appId){
		$select = $this->lobjDbAdpt->select()
								   ->from(array('a'=>'tbl_invoicedetl'),array('a.*'))
								   ->joinLeft(array('b'=>'tbl_fee_setup'),'a.FEECODE = b.IdFeeSetup',array('b.*'))
								   ->joinLeft(array('c'=>'tbl_ledgercode'),'c.AUTOINC = a.ACCTCODEDB',array('CONCAT_WS("-",c.ACCOUNTCODE,c.DESCRIPTION) as Debit'))
								   ->joinLeft(array('d'=>'tbl_ledgercode'),'d.AUTOINC = a.ACCTCODECH',array('CONCAT_WS("-",d.ACCOUNTCODE,d.DESCRIPTION) as Credit'))
								   ->joinLeft(array('e'=>'tbl_invoicemain'),'e.AUTOINC=a.BILLNUM',array('e.CURRENCY'))
								   ->where('a.BILLNUM = ?',$appId)
								   ->order('a.BILLSEQ');

		$result = $this->lobjDbAdpt->fetchAll($select);


		if(count($result) != 0){
			$i = 0;
			foreach($result as $res){
				if($res['COSTCTR']!=''){
					$pieces = explode('_',$res['COSTCTR']);
					if($pieces[1] == "faculty"){
						$select1 = $this->lobjDbAdpt->select()
						->from(array('a'=>'tbl_collegemaster'),array('a.CollegeName as CostCtr'))
						->where('a.IdCollege = ?',$pieces[0]);
						$result1 = $this->lobjDbAdpt->fetchRow($select1);
						$result[$i]['CostCtr'] = $result1['CostCtr'];
					}else if($pieces[1] == "school"){
						$select1 = $this->lobjDbAdpt->select()
						->from(array('a'=>'tbl_schoolmaster'),array('a.SchoolName as CostCtr'))
						->where('a.idSchool = ?',$pieces[0]);
						$result1 = $this->lobjDbAdpt->fetchRow($select1);
						$result[$i]['CostCtr']	= $result1['CostCtr'];
					}
					$i++;
				}

			}
			$j = 0;
			foreach($result as $res){
				$select2 = $this->lobjDbAdpt->select()
								->from(array('a'=>'tbl_crnotemain'),array())
								->joinLeft(array('b'=>'tbl_crnotedetl'),'b.BILLNUM = "'.$res['BILLNUM'].'" AND a.AUTOINC = b.CRNOTE_NUM AND b.BILLSEQ = "'.$res['BILLSEQ'].'"',array('SUM(b.CN_AMOUNT) as CN_AMOUNT'))
								->where('a.STS = ?',"Approve");
				$result2 = $this->lobjDbAdpt->fetchAll($select2);
				$result[$j]['CN_AMOUNT'] = $result2[0]['CN_AMOUNT'];
				$j++;
			}
		}
		return $result;
	}

	public function fngetappsearchlist($post){
		$select = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_invoicemain'),array('a.AUTOINC','a.BILLNUM','a.SemesterID','a.DOCDATE','a.STATUS'))
		->joinLeft(array('b'=>'tbl_studentregistration'),'a.IdStudentRegistration = b.IdStudentRegistration',array('CONCAT_WS(" ",b.FName,b.MName,b.LName) as Name','b.registrationId'))
		->joinLeft(array('d'=>'tbl_program'),'b.IdProgram = d.IdProgram',array('d.ProgramName'));

		if(isset($post['field3']) && $post['field3']){
			$select = $select->where('a.STUDID like "%" ? "%"',$post['field3']);
		}
		if(isset($post['field14']) && $post['field14']){
			$select = $select->where('a.DOCDATE =?',date('Y-m-d',strtotime($post['field14'])));
		}
		if(isset($post['field11']) && $post['field11']){
			$select = $select->where('b.IdProgram =?',$post['field11']);
		}
		if (isset($post['field12']) && !empty($post['field12'])) {
			$select = $select->joinLeft(array('prf' => 'tbl_program_scheme'), 'prf.IdProgram = b.IdProgram',array());
			$select = $select->where("prf.IdScheme = ?", $post['field12']);
		}
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetdebitList(){
		$select = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_ledgercode'),array('key'=>'a.AUTOINC','value'=>'CONCAT_WS("-",a.ACCOUNTCODE,a.DESCRIPTION)'))
		->where('a.STATUS = ?',1)
		->where('a.MISCCODE = ?',"SD");
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetcreditList(){
		$select = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_ledgercode'),array('key'=>'a.AUTOINC','value'=>'CONCAT_WS("-",a.ACCOUNTCODE,a.DESCRIPTION)'))
		->where('a.STATUS = ?',1)
		->where('a.ACCOUNTYPE = ?',"R")
		->orWhere('a.ACCOUNTYPE = ?',"C");
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fngetacccodeList(){
		$select = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_ledgercode'),array('key'=>'a.AUTOINC','value'=>'CONCAT_WS("-",a.ACCOUNTCODE,a.DESCRIPTION)'))
		->where('a.STATUS = ?',1)
		->where('a.MISCCODE = ?',"DICT");
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fnSearchInvoiceApproval($larrformData) {

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_invoicemain"), array("a.*"))
					->joinLeft(array('b'=>'tbl_user'),'b.iduser = a.ID_CREATE',array('b.loginName'))
					->joinLeft(array('c'=>'tbl_branchofficevenue'),'c.IdBranch = a.BRANCH_ID',array('c.BranchName'));

		if(isset($larrformData['BillNumber']) && !empty($larrformData['BillNumber']) ){
			$select = $select->where("a.BILLNUM LIKE  '%".$larrformData['BillNumber']."%'");

		}

		if(isset($larrformData['Branch']) && !empty($larrformData['Branch'])){
			$select = $select->where("a.BRANCH_ID = ?",$larrformData['Branch']);

		}

		if(isset($larrformData['CreatedBy']) && !empty($larrformData['CreatedBy'])){
			$select = $select->where("a.ID_CREATE = ?",$larrformData['CreatedBy']);

		}

		if(isset($larrformData['CreatedDate']) && !empty($larrformData['CreatedDate'])){
			$select = $select->where("a.DT_CREATE >= ?",$larrformData['CreatedDate']);

		}

		$select->where("a.STATUS = ?",0);
		$result = $this->lobjDbAdpt->fetchAll($select);
		return $result;
	}

	public function fnApproveInvoice($larrformData){
		$lIntlen = count($larrformData['chkaddinvoice']);
		$paramArray = array();
		for($i=0;$i<$lIntlen;$i++){
			$paramArray['STATUS'] = '1';
			$where_up = "AUTOINC ='".$larrformData['chkaddinvoice'][$i]."' ";
			$this->lobjDbAdpt->update('tbl_invoicemain',$paramArray,$where_up);
		}

	}

	public function fnRejectInvoice($larrformData,$userId,$UpdDate){
		$lIntlen = count($larrformData['chkaddinvoice']);
		$paramArray = array();
		for($i=0;$i<$lIntlen;$i++){
			$paramArray['STATUS'] = '2';
			$paramArray['ID_CANCEL'] = $userId;
			$paramArray['DT_CANCEL'] = $UpdDate;
			$where_up = "AUTOINC ='".$larrformData['chkaddinvoice'][$i]."' ";
			$this->lobjDbAdpt->update('tbl_invoicemain',$paramArray,$where_up);
		}

	}

	public function fnCheckInvoiceId($lstrcheck){
		$select = $this->select()
					->from(array('a'=>'tbl_invoicemain'),array('a.*'))
					->where('a.BILLNUM  = ?',$lstrcheck);
		$result = $this->fetchAll($select);
		return (count($result));
	}

	public function fngetInvoiceList($StudentId,$SemId,$Currency){
		$select = $this->select()
					   ->from(array('a'=>'tbl_invoicemain'),array('key'=>'a.AUTOINC','name'=>'BILLNUM'))
					   ->where('a.SemesterID = ?',$SemId)
					   ->where('a.STATUS = ?',1)
					   ->where('a.IdStudentRegistration = ?',$StudentId);

		if($Currency != ''){
			$select	= $select->where('a.CURRENCY = ?',$Currency);
		}
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

	public function fngetinvoicedetailsprevDN($appId){
		$select = $this->lobjDbAdpt->select()
								   ->from(array('a'=>'tbl_invoicedetl'),array('a.*'))
								   ->joinLeft(array('b'=>'tbl_fee_setup'),'a.FEECODE = b.IdFeeSetup',array('b.*'))
								   ->joinLeft(array('c'=>'tbl_ledgercode'),'c.AUTOINC = a.ACCTCODEDB',array('CONCAT_WS("-",c.ACCOUNTCODE,c.DESCRIPTION) as Debit'))
								   ->joinLeft(array('d'=>'tbl_ledgercode'),'d.AUTOINC = a.ACCTCODECH',array('CONCAT_WS("-",d.ACCOUNTCODE,d.DESCRIPTION) as Credit'))
								   ->joinLeft(array('e'=>'tbl_invoicemain'),'e.AUTOINC=a.BILLNUM',array('e.CURRENCY'))
								   ->where('a.BILLNUM = ?',$appId)
								   ->order('a.BILLSEQ');

		$result = $this->lobjDbAdpt->fetchAll($select);


		if(count($result) != 0){
			$i = 0;
			foreach($result as $res){
				if($res['COSTCTR']!=''){
					$pieces = explode('_',$res['COSTCTR']);
					if($pieces[1] == "faculty"){
						$select1 = $this->lobjDbAdpt->select()
						->from(array('a'=>'tbl_collegemaster'),array('a.CollegeName as CostCtr'))
						->where('a.IdCollege = ?',$pieces[0]);
						$result1 = $this->lobjDbAdpt->fetchRow($select1);
						$result[$i]['CostCtr'] = $result1['CostCtr'];
					}else if($pieces[1] == "school"){
						$select1 = $this->lobjDbAdpt->select()
						->from(array('a'=>'tbl_schoolmaster'),array('a.SchoolName as CostCtr'))
						->where('a.idSchool = ?',$pieces[0]);
						$result1 = $this->lobjDbAdpt->fetchRow($select1);
						$result[$i]['CostCtr']	= $result1['CostCtr'];
					}
					$i++;
				}

			}
			$j = 0;
			foreach($result as $res){
				$select2 = $this->lobjDbAdpt->select()
								->from(array('a'=>'tbl_dnmain'),array())
								->joinLeft(array('b'=>'tbl_dndetl'),'b.BILLNUM = "'.$res['BILLNUM'].'" AND a.AUTOINC = b.DSCNOTE_NUM AND b.BILLSEQ = "'.$res['BILLSEQ'].'"',array('SUM(b.DN_AMT) as DN_AMT'))
								->where('a.STS = ?',"Approve");
							
				$result2 = $this->lobjDbAdpt->fetchAll($select2);
				$result[$j]['DN_AMT'] = $result2[0]['DN_AMT'];
				$j++;
			}
		}
		return $result;
	}

}