<?php
class Studentfinance_Model_DbTable_Studentrelease extends Zend_Db_Table {
	protected $_name = 'tbl_student_release_status';

	public function fngetAllStudentRelease() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>"tbl_student_release_status"),array("a.IdCategory","a.IdStudentRelease","a.Reason"))
			->joinleft(array("b"=>"tbl_studentapplication"),'b.IdApplication = a.IdStudent ',array("CONCAT_WS(',',b.FName,b.LName) AS ApplicantName","b.IdApplicant AS ApplicantId"))
			->joinleft(array("c"=>"tbl_studentregistration"),'c.IdStudentRegistration = a.IdStudent',array("CONCAT_WS(',',c.FName,c.LName) AS StudentName","c.registrationId AS StudentId"))
			->join(array('d' => 'tbl_definationms'), 'a.IdReleaseType = d.idDefinition',array('d.DefinitionCode AS ReleaseType'))
			->join(array('e' => 'tbl_intake'), 'a.IdIntake = e.IdIntake',array('e.IntakeId AS Intake'))
			//->where("a.Active = 1")
			->order("a.UpdDate");

		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fnsearchStudentRelease($searchdata) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>"tbl_student_release_status"),array("a.IdCategory","a.IdStudentRelease"))
			->joinleft(array("b"=>"tbl_studentapplication"),'b.IdApplication = a.IdStudent ',array("CONCAT_WS(',',b.FName,b.LName) AS ApplicantName","b.IdApplicant AS ApplicantId"))
			->joinleft(array("c"=>"tbl_studentregistration"),'c.IdStudentRegistration = a.IdStudent',array("CONCAT_WS(',',c.FName,c.LName) AS StudentName","c.registrationId AS StudentId"))
			->join(array('d' => 'tbl_definationms'), 'a.IdReleaseType = d.idDefinition',array('d.DefinitionCode AS ReleaseType'))
			->join(array('e' => 'tbl_intake'), 'a.IdIntake = e.IdIntake',array('e.IntakeId AS Intake'))
			->order("a.UpdDate");
		if($searchdata['field3'] != "") {
			$lstrSelect = 	$lstrSelect->where("CONCAT_WS(' ',b.FName,b.LName) LIKE '%".$searchdata['field3']."%' OR CONCAT_WS(' ',c.FName,c.LName) LIKE '%".$searchdata['field3']."%'");
		}
		if($searchdata['field2'] != "") {
			$lstrSelect = 	$lstrSelect->where("b.IdApplicant LIKE '%".$searchdata['field2']."%' OR c.registrationId LIKE '%".$searchdata['field2']."%'");
		}
		if($searchdata['field5'] != "") {
			$lstrSelect = 	$lstrSelect->where("d.idDefinition = ?",$searchdata['field5']);
		}
		if($searchdata['field8'] != "") {
			$lstrSelect = 	$lstrSelect->where("e.IdIntake = ?",$searchdata['field8']);
		}

		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fninsertstudentrelease($data) {
		$this->insert($data);
	}

	public function fngetstudentlist() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>"tbl_studentregistration"),array("name" => "CONCAT_WS(' ',a.FName,a.LName,a.IdApplicant)","key" => "a.IdStudentRegistration"))
			->where("a.Status = 198");
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fngetapplicantlist() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			->from(array("a"=>"tbl_studentapplication"),array("key" => 'a.IdApplication',"name" => "CONCAT_WS(' ',a.FName,a.LName,a.IdApplicant)"))
			->where("a.Status = 197");
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}

	public function fnupdatestudentrelease($data,$IdStudentRelease) {
            
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = 'IdStudentRelease = '.$IdStudentRelease;
   		$lobjDbAdpt->update($this->_name,$data, $where);
	}
        
        
        /**
         * Function to fetch student applicant and searching
         * @author Vipul
         */
        public function fnfetchApplicantRelease($post=NULL) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
                         ->from(array("a"=>"tbl_student_release_status"),array("a.IdCategory","a.IdStudentRelease","a.IdReleaseType","a.IdIntake","a.IdStudent"))	
                         ->joinleft(array("b"=>"tbl_studentapplication"), ' b.IdApplication = a.IdStudent ',array("CONCAT_WS(' ',b.FName,b.LName) AS ApplicantName","b.IdApplicant"))			 			
			 ->joinleft(array('d' => 'tbl_definationms'), 'a.IdReleaseType = d.idDefinition',array('d.DefinitionCode AS ReleaseType'))
			 ->joinleft(array('e' => 'tbl_intake'), 'a.IdIntake = e.IdIntake',array('e.IntakeId AS Intake'));
			
                $wh = "1=1 ";        
                if (isset($post['field3']) && !empty($post['field3'])) {
                     $wh .= " AND b.FName LIKE '%" . $post['field3'] . "%' ";
                }
                
                if (isset($post['field2']) && !empty($post['field2'])) {
                   $wh .= " AND b.IdApplicant LIKE '%" . $post['field2'] . "%' ";
                }
                
                if (isset($post['field5']) && !empty($post['field5'])) {                   
                   $wh .= " AND a.IdReleaseType = '" . $post['field5'] . "' ";
                }
                
                if (isset($post['field8']) && !empty($post['field8'])) {                   
                   $wh .= " AND a.IdIntake = '" . $post['field8'] . "' ";
                }
                        
                        
                $wh .= " AND ( b.Status = 197 ) ";

                if ($post == NULL) {
                  $lstrSelect = $lstrSelect->where("b.Status = 197");
                } else {
                  $lstrSelect = $lstrSelect->where($wh);
                }       
                        
                $lstrSelect->group("b.IdApplication")->order("b.FName");               
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
        
         /**
         * Function to fetch student applicant Release Type
         * @author Vipul
         */
        public function fnfetchReleaseType($condition) {           
            $result = $this->fetchAll($condition)->toArray();
	    return $result;
        }

        public function fetchRelease($Idintake,$IdStudent,$Idcategory="1"){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $lstrSelect = $lobjDbAdpt->select()
                                ->from(array("a"=>"tbl_student_release_status"),array("a.IdReleaseType"))
                                ->joinLeft(array("b"=>"tbl_definationms"),"a.IdReleaseType = b.idDefinition",array("b.DefinitionDesc"))
                                ->where("a.IdIntake = ?",$Idintake)
                                ->where("a.IdStudent =?",$IdStudent)
                                ->where("a.IdCategory =?",$Idcategory);
            $result = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $result;
        }
        
       
        public function fetchAllReleaseforStudent($IdStudent,$Idcategory="1"){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $lstrSelect = $lobjDbAdpt->select()
                                ->from(array("a"=>"tbl_student_release_status"),array("a.IdReleaseType"))
                                ->joinLeft(array("b"=>"tbl_definationms"),"a.IdReleaseType = b.idDefinition",array("b.DefinitionDesc"))                               
                                ->where("a.IdStudent =?",$IdStudent)
                                ->where("a.IdCategory =?",$Idcategory);
            $result = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $result;
        }
         
        /**
         * Function to delete Student Release
         * @author: vipul        
         */
        
        public function fndeleterelease($id){
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $where = $lobjDbAdpt->quoteInto('IdStudentRelease = ?', $id); 
            $lobjDbAdpt->delete('tbl_student_release_status', $where);
            return 1;
        }
         
        
        

}
?>
