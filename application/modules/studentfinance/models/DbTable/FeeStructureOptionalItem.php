<?php
class Studentfinance_Model_DbTable_FeeStructureOptionalItem extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_structure_optional_item';
	protected $_primary = "fsoi_id";
		
	protected $_locale;
	
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
	
	public function getDataByStructure($structure_id, $item_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsi'=>$this->_name))
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = fsi.fsoi_fee_id', array('feeitem_name'=>'fi.fi_name'))
					->joinLeft(array('ri'=>'tbl_registration_item'), 'ri.ri_id=fsi.fsoi_item_id', array())
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = ri.item_id', array('reg_item'=>'d.DefinitionDesc'))
					->joinLeft(array('c'=>'tbl_currency'),'fsi.fsoi_currency_id=c.cur_id', array('c.cur_symbol_prefix'))
					->where("fsi.fsoi_structure_id = ?",$structure_id)
					->where("fsi.fsoi_detail_id = ?", $item_id);
		
		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
}

