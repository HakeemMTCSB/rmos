<?php
class Studentfinance_Model_DbTable_FeeResit extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_resit';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fr'=>$this->_name));
		
		if($id!=0){
			$selectData->where("fr.id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getLatestResitData($program_id,$course_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('fr'=>$this->_name))
		->where('fr.program_id =?',$program_id)
		->where('fr.subject_id =?',$course_id)
		->order('fr.effective_date desc');
		
		$row = $db->fetchRow($selectData);
		
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getResitDataHistory($program_id,$course_id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$selectData = $db->select()
			->from(array('fr'=>$this->_name))
			->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = fr.create_by', array('create_by_name'=>'CONCAT_WS (" ", tu.fName, tu.mName, tu.lName)'))
			->where('fr.program_id =?',$program_id)
			->where('fr.subject_id =?',$course_id)
			->order('fr.effective_date desc');
	
		$row = $db->fetchAll($selectData);
	
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getResitAmount($program_id,$course_id,$date_=null){
		
		if($date_==null){
			$date_ = date('Y-m-d');
		}
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('fr'=>$this->_name))
		->where('fr.program_id =?',$program_id)
		->where('fr.subject_id =?',$course_id)
		->where('fr.effective_date <= ?',$date_)
		->order('fr.effective_date desc');
						
		$row = $db->fetchRow($selectData);
		
			
		if(!$row){
			return 0.00;
		}else{
			return $row['amount'];
		}
	}
	
		
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		$data['create_by'] = $auth->getIdentity()->iduser;
		$data['create_date'] = date('Y-m-d H:i:s');
			
        return parent::insert($data);
	}		
	
}

