<?php
class Studentfinance_Model_DbTable_SuspendProcessHistory extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'suspend_process_history';
	protected $_primary = "sph_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('sph'=>$this->_name))
					->where("sph.sph_id ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	/*
	 * Overite Insert function
	*/
	
	public function insert($data=array()){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['process_by'])){
			$data['process_by'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['process_date']) ){
			$data['process_date'] = date('Y-m-d H:i:s');
		}
			
		return parent::insert( $data );
	}
}
?>