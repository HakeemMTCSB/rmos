<?php
class Studentfinance_Model_DbTable_Receipt extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_recieptmaster';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fngetrecieptnum($recieptnum){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('tbl_recieptmaster'),array('RecieptNumber')) 						
 						->where("RecieptNumber = ?",$recieptnum);	 								
 		return $result = $lobjDbAdpt->fetchRow($lstrSelect);
	}
	
	
	
	public function fngetStudentRegistrationDetails() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
 						->where("b.idsponsor is null");			
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		/*echo "<pre>";
 		print_r($result);die();*/
 		return $result;
	}
	
	public function fngetitemgroup() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_itemgroup'),(array("key"=>"a.IdItem","value"=>"a.EnglishDescription")));
        $result = $lobjDbAdpt->fetchAll($lstrSelect);
         return $result;
	}
	
	public function fngetStudentApplicationDetails()//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		
		$larrstudarrayresult = "SELECT IdStudent from tbl_invoicemaster";
		
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicemaster'),array('a.*'))
 						->join(array('e'=>'tbl_studentregistration'),'a.IdStudent  = e.IdStudentRegistration')
 						->join(array('b'=>'tbl_studentapplication'),'e.IdApplication  = b.IdApplication')
 						->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
 						->join(array('d'=>'tbl_collegemaster'),'b.idCollege = d.IdCollege')
 						/*->where("a.IdStudent NOT IN ?",new Zend_Db_Expr('('.$larrstudarrayresult.')'))*/;
					
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fnEditInvoiceDetails($IdInvoice)//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicemaster'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdStudent=b.IdApplication')
 						->where('a.IdInvoice = ?',$IdInvoice);
					
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fnEditInvoiceDetailDetails($IdInvoicedetail)//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicedetails'),array('a.*'))
 						->join(array('b'=>'tbl_itemgroup'),'a.idAccount = b.IdItem')
 						->where('a.IdInvoice  = ?',$IdInvoicedetail);
		//echo $lstrSelect;		
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
   public function fngetStudentDetails($lintidstudent) { //Function to get the Program Branch details
 			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
 			$lstrSelect = $lobjDbAdpt->select()	
 					   				 ->from(array('b'=>'tbl_studentapplication'))
               						 ->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
               						 ->where("b.IdApplication= ?",$lintidstudent);
       $result = $lobjDbAdpt->fetchRow($lstrSelect);
       return $result;
     }
     
     /*
      * function for getting the prog charges
      */
    public function fngetProgramDetails($lintidprogram) 
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
      	$currentdate = date ('Y-m-d');
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_programcharges'),array('a.Charges','a.IdCharges'))
                			->join(array('b'=>'tbl_charges'),'a.IdCharges = b.IdCharges',array('b.ChargeName'))
                			->where('b.effectiveDate <= ?',$currentdate)
                			->where('a.IdProgram = ?',$lintidprogram)
                			->where('b.Payment = 1');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
	
 /*
     * function to get the drop down of the student
     */
  public function fngetStudentDropdown() { //Function to get the Program Branch details
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_studentapplication"),array("value"=>"CONCAT_WS(' ',IFNULL(a.FName,''),IFNULL(a.MName,''),IFNULL(a.LName,''))"))
		 				 ->join(array('b'=>'tbl_studentregistration'),'a.IdApplication=b.IdApplication',array("key"=>"b.IdStudentRegistration"))
		                 ->where("a.idsponsor is null")
		               	 ->group('a.IdApplication');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	/*
	 * function to search for the student in the sponsor manual invoice
	 */
	public function fnSearchStudentApplication($post)
	{
          $idstudent = $post['field5'];
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
          
          $larrstudarrayresult = "SELECT IdStudent from tbl_invoicemaster";
          
          if($post['field5']!='')
          {
			$consistantresult = "SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication AND i.IdApplication=$idstudent";
			
          }
          else {
          	$consistantresult = "SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication AND i.IdApplication=b.IdApplication";
              }
		 $lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
 						->join(array('c'=>'tbl_invoicemaster'),'a.IdApplication = c.IdStudent')
 						->join(array('d'=>'tbl_program'),'b.IDCourse=d.IdProgram')
 						->join(array('e'=>'tbl_collegemaster'),'b.idCollege = e.IdCollege')
 						->where("c.InvoiceNo = ?",$post['field3'])
 						->where("c.IdStudent NOT IN ?",new Zend_Db_Expr('('.$larrstudarrayresult.')'))
 						->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'))
 						->where("b.idsponsor is null");
 						
	if(isset($post['field13']) && !empty($post['field13'])){
				$lstrSelect = $lstrSelect->where("b.IdCollege = ?",$post['field13']);
			}	

	if(isset($post['field11']) && !empty($post['field11'])){
				$lstrSelect = $lstrSelect->where("d.IdProgram = ?",$post['field11']);
				
			}	
			
	if(isset($post['field15']) && !empty($post['field15'])){
				$lstrSelect = $lstrSelect->where("c.InvoiceDt = ?",$post['field15']);
			}	
/*	if(isset($post['field3']) && !empty($post['field3'])){
				$lstrSelect = $lstrSelect->where("c.InvoiceNo = ?",$post['field3']);
			}	*/
		//echo $lstrSelect;die();
			
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		
 		return $result;
	}
	
	
    /*
     * function for fetching the subject charges
     */
    public function fnGetSubjectCharges($lintIdStudentRegistration)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentregsubjects'),array('a.*'))
                			->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject')
                			->where('a.IdStudentRegistration = ?',$lintIdStudentRegistration);
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                			
    }
    
    /*
     * function to insert into the invoicemaster table
     */
    public function fnAddInvoiceMaster($larrformData)
    {

    	    $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_invoicemaster";
            $InvoiceAmt =  array_sum($larrformData['Amountgrid']);

            $postData = array('IdStudent' => $larrformData['IdStudent'],		
            				'InvoiceNo' =>'Dec-'.' '.$larrformData['Studentid'],		
		            		'InvoiceDt' =>$larrformData['InvoiceDt'],	
		            		'InvoiceAmt'=>$InvoiceAmt,
           				    'IdSemester'=>$larrformData['semester'],
            				'AcdmcPeriod' =>$larrformData['AcdmcPeriod'],		
            				'Naration' =>$larrformData['Naration'],		
		            		'UpdDate' =>$larrformData['UpdDate'],	
		            		'UpdUser'=>$larrformData['UpdUser'],
            				'Approved'=>0,
            				'idsponsor'=>0,
						);			
	        $db->insert($table,$postData);
	        $lastinvoiceid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_invoicemaster','IdInvoice');

	        /////////////////inserting into the invoice details///////////////////
	         for($i=0;$i<count($larrformData['idAccountgrid']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicedetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
								'idAccount' => $larrformData['idAccountgrid'][$i],		
	            				'Amount' =>$larrformData['Amountgrid'][$i],		
	            				'Description' =>$larrformData['Descriptiongrid'][$i],	
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser'],
			            		'Discount' =>0,	
	                            'Active'=>1
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   
		     return $lastinvoiceid;
	        ////////////////End of invoice details///////////////////////////////
	        
	       /////////////////inserting into the Subjecct Details///////////////////
/*	         for($i=0;$i<count($larrformData['IdSubject']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicesubjectdetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
								'IdSubject' => $larrformData['IdSubject'][$i],		
	            				'Amount' =>$larrformData['Subjectamount'][$i],		
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser']
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   */
	        ////////////////End of subject Details/////////////////////////////// 	     
    }
    
    	public function fnUpdateInvoicemaster($larrformData) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_invoicemaster";			
			$IdInvoicemaster=$larrformData['IdInvoice'];
			$where = "IdInvoice = '$IdInvoicemaster'";			
			$postData = array('IdStudent' => $larrformData['IdStudent'],		
            				'InvoiceDt' =>$larrformData['InvoiceDt'],
           				    'IdSemester'=>$larrformData['semester'],
            				'AcdmcPeriod' =>$larrformData['AcdmcPeriod'],		
            				'Naration' =>$larrformData['Naration'],		
		            		'UpdDate' =>$larrformData['UpdDate'],	
		            		'UpdUser'=>$larrformData['UpdUser'],            				
						);				
			
			$db->update($table,$postData,$where);	
		}	
		
		public function fndeleteInvoiceDetails($Idinvoice){			
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_invoicedetails";
	    	$where = $db->quoteInto('IdInvoice = ?', $Idinvoice);
			$db->delete($table, $where);
		
		}
		
		public function fnAddInvoiceDetailsEdit($larrformData) {
			$db = Zend_Db_Table::getDefaultAdapter();
			
			for($i=0;$i<count($larrformData['idAccountgrid']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicedetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$larrformData['IdInvoice'],	
								'idAccount' => $larrformData['idAccountgrid'][$i],		
	            				'Amount' =>$larrformData['Amountgrid'][$i],		
	            				'Description' =>$larrformData['Descriptiongrid'][$i],	
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser'],
			            		'Discount' =>0,	
	                            'Active'=>1
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   
		}
		
		
		
    //*******************************************
 	public function fnGetInvoiceNO($result) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_invoicemaster"),array("a.InvoiceNo"))	
						->where("a.IdInvoice= ?",$result);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
	public function fnGetProgram($IdStudent) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("tbl_studentregistration" => "tbl_studentregistration"),array("tbl_studentregistration.IdApplication","tbl_studentregistration.IdStudentRegistration","tbl_studentregistration.registrationId"))	
						->join(array("tbl_studentapplication" =>"tbl_studentapplication"),"tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication",array("tbl_studentapplication.IDCourse","DATE_FORMAT(tbl_studentapplication.ApplicationDate,'%d-%m-%Y')as ApplicationDate","tbl_studentapplication.idCollege"))
						->join(array("tbl_program"=>"tbl_program"),"tbl_studentapplication.IDCourse = tbl_program.IdProgram",array("tbl_program.IdProgram","tbl_program.ProgramName"))
						->join(array("tbl_collegemaster"=>"tbl_collegemaster"),"tbl_studentapplication.idCollege = tbl_collegemaster.IdCollege",array("tbl_collegemaster.IdCollege","tbl_collegemaster.CollegeName"))
						->where("tbl_studentregistration.IdStudentRegistration= ?",$IdStudent);	
		return $result = $lobjDbAdpt->fetchRow($select);
		
	}
	
	public function fnGetAmount($idAccount,$idprogram,$semester){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("tbl_feesetupmaster" => "tbl_feesetupmaster"),array("tbl_feesetupmaster.IdFeesSetupMaster"))
						->join(array("tbl_feesetupdetail" => "tbl_feesetupdetail"),"tbl_feesetupmaster.IdFeesSetupMaster = tbl_feesetupdetail.IdFeessetupMaster",array("tbl_feesetupdetail.Amount"))	
						->join(array("tbl_programfees" =>"tbl_programfees"),"tbl_feesetupdetail.IdFeessetupMaster  = tbl_programfees.IdFeesSetupMaster",array(""))
						->where("tbl_programfees.IdProgram = ?",$idprogram)
						->where("tbl_feesetupdetail.DebitAccount = ?",$idAccount)
						->where("tbl_feesetupmaster.StartingIntake >= ? or tbl_feesetupmaster.EndSemester <= ?",$semester);	
						
		return $result = $lobjDbAdpt->fetchRow($select);
		

	}
	
public function fnAddReceipt($larrformData) {
	
		$this->insert($larrformData);
		 return $lastidReciept;
	}

	 public function fnupdateReceipt($IdReceipt,$larrformData) { //Function for updating the user   
		$where = 'idReciept = '.$IdReceipt;
		$this->update($larrformData,$where);
    }
public function fnEditReceipt($IdReceipt){
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       				->from(array("sa"=>" tbl_recieptmaster"),array("sa.*"))
       				->where('sa.idReciept = ?',$IdReceipt);				
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
}