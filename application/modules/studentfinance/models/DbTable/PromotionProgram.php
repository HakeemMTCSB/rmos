<?php

class Studentfinance_Model_DbTable_PromotionProgram extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_promotion_program';
    protected $_primary = 'id';
    private $lobjDbAdpt;
    protected $_locale;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $registry = Zend_Registry:: getInstance();
        $this->_locale = $registry->get('Zend_Locale');
	}


    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }

    public function getProgram($promo_id)
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a' => 'tbl_promotion_program'),array('a.*'))
            ->joinLeft(array('b' => 'tbl_program'), 'a.id_program = b.idprogram', array('idprogram','programname','programcode'))
            ->where('a.promotion_id =?',$promo_id);

        $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $result;
    }

    public function fngetcode($idprog){
        $select = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_program"), array("a.*"))
            ->where("a.idprogram = ?",$idprog);
        $result = $this->lobjDbAdpt->fetchRow($select);

        return $result[0]['programcode'];
    }

    public function deleteData($id)
    {
        $this->delete($this->_primary . ' =' . (int)$id);
    }


}