<?php

class Studentfinance_Model_DbTable_ScholarshipIncomeWeight extends Zend_Db_Table {

    protected $_name = 'tbl_scholarship_income_weight';
    protected $_primary = 'id';

    public function save(array $data){

        if( isset($data[$this->_primary]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }

    /**
     * feeds an age and returns the point based on
     */
    public function get_point( $wi ) {

        $select = $this->select()
            ->where('`from` <= ?', $wi)
            ->where('`to` >= ?', $wi)
        ;
        $rule = $this->fetchRow($select);
        if(empty($rule)) {
            return 0;
        } else {
            return $rule->score;
        }

    }
    
    public function getCurrency(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', 178);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    static function getCurrencyDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('a.DefinitionDesc'))
            ->where('a.idDefinition = ?', $id);
        
        $result = $db->fetchOne($select);
        return $result;
    }
}