<?php
class Studentfinance_Model_DbTable_Programchargemaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_programcharges';
	
	
	public function fnaddprogramcharges($lobjFormData) {
		$gridcount = count($lobjFormData['IdChargesgrid']);
		for($i=0;$i<$gridcount;$i++){
			$data = array('IdProgram' => $lobjFormData['IdProgram'],
						  'IdCharges' => $lobjFormData['IdChargesgrid'][$i],
						  'Charges' => $lobjFormData['Chargesgrid'][$i],
				 		  'UpdUser' => $lobjFormData['UpdUser'],
				 		  'UpdDate' => $lobjFormData['UpdDate']);			
			$this->insert($data);
		}
	}	
	
     public function fnprogramchargelist($lintProgram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("pc"=>"tbl_programcharges"),array("pc.*"))
		 				 ->join(array("c"=>"tbl_charges"),'pc.IdCharges = c.IdCharges ')
		 				 ->where('pc.IdProgram = ?',$lintProgram);
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
     public function fnTempprogramchargelist($lintProgram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("pc"=>"tbl_tempprogramcharges"),array("pc.*"))
		 				 ->join(array("c"=>"tbl_charges"),'pc.IdCharges = c.IdCharges ')
		 				 ->where('pc.IdProgram = ?',$lintProgram)
		 				 ->where('pc.deleteflag = 1');
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fninsertprogramcharge($programlist) {  // function to insert po details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_tempprogramcharges";
		$sessionID = Zend_Session::getId();
		foreach($programlist as $programchargelist) {
			$larrcourse = array('IdProgram'=>$programchargelist['IdProgram'],
								'IdCharges'=>$programchargelist['IdCharges'],
			                    'Charges'=>$programchargelist['Charges'],
								'UpdUser'=>$programchargelist['UpdUser'],
								'UpdDate'=>$programchargelist['UpdDate'],
								'unicode'=>$programchargelist['IdProgramCharges'],
								'Date'=>date("Y-m-d"),
								'sessionId'=>$sessionID,
								'deleteFlag'=>1,
						);
							
			$db->insert($table,$larrcourse);	
		}
	}
	
	public function fnUpdateTempprogramcharges($IdTemp) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempprogramcharges";
			$larrresult = array('deleteFlag'=>'0');
			$where = "idTemp = '".$IdTemp."'";
			$db->update($table,$larrresult,$where);	
		}
		
	public function fnSearchPrograms($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("cm"=>"tbl_program"),array("cm.*"))
       								->where('cm.ProgramName like "%" ? "%"',$post['field2'])
       								->order("cm.ProgramName");
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
}