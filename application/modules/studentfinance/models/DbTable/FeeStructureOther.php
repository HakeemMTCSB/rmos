<?php
class Studentfinance_Model_DbTable_FeeStructureOther extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_structure_other';
	protected $_primary = "fso_id";
		
	protected $_locale;
	
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
	
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsi'=>$this->_name))
					->where("fsi.fso_id = ?",$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getList(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('a'=>$this->_name))
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram = a.fso_program_id')
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = a.fso_activity_id', array('activityName'=>'d.DefinitionDesc'))
					->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = a.fso_trigger',array('triggername'=>'e.DefinitionDesc'))
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = a.fso_fee_id');

		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
public function getStructureData($fee_structure_id,$type,$programID,$level=0,$itemId=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsi'=>'fee_structure_item'))
					->joinLeft(array('fsp'=>'fee_structure_program'),'fsp.fsp_fs_id = fsi.fsi_structure_id and fsi.fsi_structure_id = '.$fee_structure_id)
					->joinLeft(array('fso'=>'fee_structure_other'),'fso.fso_fee_id = fsi.fsi_item_id and fso.fso_program_id = fsp.fsp_program_id and fso.fso_fee_id = fsi.fsi_item_id')
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = fsi.fsi_item_id')
					->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = fsi.fsi_cur_id')
					->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fso.fso_activity_id and e.idDefType = 168')
					->where("fsi.fsi_structure_id = ?",$fee_structure_id)
					->where("e.DefinitionCode = ?",$type)
					->where("e.idDefType = 168")
					->where("fso.fso_status = 1"); //active only
					
		if($itemId){
			$selectData->joinLeft(array('df' => 'tbl_definationms'), 'df.IdDefinition = fsi.fsi_registration_item_id',array('item_name'=>'df.DefinitionDesc'))
			->where("fsi.fsi_registration_item_id = ?",$itemId);
		}
		
		if($level == 0){
			$level = 869;
		}elseif($level == 1){
			$level = 870;
		}
		
		$selectData->where("fso.fso_trigger = ?",$level);
//		echo $selectData.'<hr>';
		$row = $db->fetchRow($selectData);
		
		if($row){
			return $row;
		}else{
			return null;
		}	
	}
	
	
	public function getPaginateData($search=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($search){
			$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
					->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
					->where("fi.fi_name LIKE '%".$search['fi_name']."%'")
					->where("fi.fi_name_bahasa LIKE '%".$search['fi_name_bahasa']."%'")
					->where("fi.fi_name_short LIKE '%".$search['fi_name_short']."%'")
					->where("fi.fi_code LIKE '%".$search['fi_code']."%'")
					->where("fi.fi_amount_calculation_type LIKE '%".$search['fi_amount_calculation_type']."%'")
					->where("fi.fi_frequency_mode LIKE '%".$search['fi_frequency_mode']."%'")
					->where("fi.fi_active = 1");	
		}else{
			$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake = fs.fs_intake_start', array('s_IntakeId'=>'','s_Intake'=>'IntakeDesc', 's_intake_bahasa'=>'IntakeDefaultLanguage', 'start_date'=>'ApplicationStartDate', 'end_date'=>'ApplicationEndDate'))
					->joinLeft(array('ii'=>'tbl_intake'),'ii.IdIntake = fs.fs_intake_end', array('s_IntakeId'=>'','e_Intake'=>'IntakeDesc', 'e_intake_bahasa'=>'IntakeDefaultLanguage'))
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category');
						
		}
			
		return $selectData;
	}
	
		
	public function addData($postData){
		$auth = Zend_Auth::getInstance(); 
			
		$this->insert($postData);
	}		
		

	public function updateData($postData,$id){
		
		$this->update($postData, "fso_id = '".$id."'");
	}
	
	public function updateAmount($postData,$id){
		
		$data = array(
				'fsi_amount' => $postData['fsi_amount'],
				'fsi_cur_id' => $postData['fsi_cur_id']
		);
			
		$this->update($data, "fsi_id = '".$id."'");
	}
	
	public function deleteData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$db->beginTransaction();
		try {
			//delete all item semester
			$result = $db->delete('fee_structure_item_semester', 'fsis_item_id = '.$id);
			
			
			//delete item
			$result = $db->delete('fee_structure_item', 'fsi_id = '.$id);
			
	
			$this->_db->commit();
			
		} catch (Exception $e) {
     		// rollback
     		$this->_db->rollback();
     		echo $e->getMessage();
     		
     		exit;
		}
	}	
}

