<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/3/2016
 * Time: 10:15 AM
 */
class Studentfinance_Model_DbTable_SponsorTaggingReport extends Zend_Db_Table_Abstract {

    public function getSponsorshipList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sponsor'), array('value'=>'*'))
            ->order('a.fName ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getScholarshipList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scholarship_sch'), array('value'=>'*'))
            ->order('a.sch_Id ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSponsorship($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sponsor'), array('value'=>'*'))
            ->where('a.idsponsor = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentTag($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sponsor_tag'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.StudentId = b.IdStudentregistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.profileStatus = e.idDefinition', array('statusname'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'fee_item'), 'a.FeeItem = f.fi_id')
            ->where('a.Sponsor = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getScholarship($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scholarship_sch'), array('value'=>'*'))
            ->where('a.sch_Id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentTag2($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scholarship_studenttag'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.sa_cust_id = b.IdStudentregistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.profileStatus = e.idDefinition', array('statusname'=>'e.DefinitionDesc'))
            ->where('a.sa_scholarship_type = ?', $id)
            ->where('a.sa_status != ?', 7)
            ->where('a.sa_cust_type = ?', 1);

        $result = $db->fetchAll($select);
        return $result;
    }
}