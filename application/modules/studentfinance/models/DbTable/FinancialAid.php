<?php

class Studentfinance_Model_DbTable_FinancialAid extends Zend_Db_Table_Abstract
{

    //put your code here
    protected $_name = 'tbl_financial_aid';
    protected $_primary = "id";

    protected $_dependentTables = array('Studentfinance_Model_DbTable_FinancialAidCoverage');

    /*
     * list of FinancialAids
     * 
     * @on 16/6/2014
     */
    public function getFinancialAidList($search_items = null)
    {
        if($search_items == null) {
            $lists = $this->fetchAll();
        } else {
            $select = $this->select();
            foreach($search_items as $field => $search_item) {
                if(empty($search_item)) { continue; }
                $select = $select->where( $field . " LIKE ?", "%". $search_item ."%" );
            }

            $lists = $this->fetchAll($select);
        }

        return $lists;
    }


    public function contribution_score($dob, $industry_type)
    {
        $date = explode('-', $dob);
        $year = end($date);
        if (strlen($year) != 4) {
            $date_part = explode('-', $dob);
            $year = $date_part[0];
        }
        $age = date('Y') - (int)$year;

        $AgeWeight = new Studentfinance_Model_DbTable_ScholarshipAgeWeight();
        $age_score = $AgeWeight->get_point($age);

        $IndustryWeight = new Studentfinance_Model_DbTable_ScholarshipIndustryWeight();
        $industry_score = $IndustryWeight->get_point($industry_type);

        //TODO:process minmax later
        $contribution_score = $industry_score + $age_score;

        if ($contribution_score < 20) {
            $contribution_score = 20;
        } else if ($contribution_score > 60) {
            $contribution_score = 60;
        }
        return ($contribution_score);
    }

    public function affordability_score($income, $dependent_count, $country)
    {
        $DependentWeight = new Studentfinance_Model_DbTable_ScholarshipDependentWeight();
        $dependent_score = $DependentWeight->get_point($dependent_count);

        $IncomeWeight = new Studentfinance_Model_DbTable_ScholarshipIncomeWeight();
        $CountryWeight = new Studentfinance_Model_DbTable_ScholarshipCountryWeight();
        $wi = $income * $CountryWeight->get_point($country);
        $income_score = $IncomeWeight->get_point($wi);

        $affordability_score = $income_score + $dependent_score;
        if ($affordability_score < 20) {
            $affordability_score = 20;
        } else if ($affordability_score > 60) {
            $affordability_score = 60;
        }
        return ($affordability_score);

    }

    public function academic_score($result, $qualification_rank)
    {
        if ($qualification_rank <= 4) {
            $academic_type = 4;
        } else {
            $cgpa = $result;
            if ($cgpa > 3.5) {
                $academic_type = 1;
            } else if ($cgpa > 3.0) {
                $academic_type = 2;
            } else if ($cgpa > 2.5) {
                $academic_type = 3;
            } else {
                $academic_type = 4;
            }
        }

        $AcademicWeight = new Studentfinance_Model_DbTable_ScholarshipAcademicWeight();
        $academic_score = $AcademicWeight->get_point($academic_type);
        return ($academic_score);
    }
}