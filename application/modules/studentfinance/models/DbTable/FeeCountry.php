<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_FeeCountry extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'fee_item_country';
	protected $_primary = "fic_id";
	
	
	public function getItemData($fee_id, $idCountry, $cur){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsp'=>$this->_name))
					->join(array('fi'=>'fee_item'),'fi.fi_id = fsp.fic_fi_id')
					->where("fsp.fic_fi_id= ?", $fee_id)
					->where("fsp.fic_idCountry = ?", $idCountry)
					->where('fsp.fic_cur_id = ?',$cur);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
}