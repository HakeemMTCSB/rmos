<?php
class Studentfinance_Model_DbTable_Invoice extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_charges';
	
	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fngetStudentRegistrationDetails() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication',array("CONCAT_WS(' ',IFNULL(b.FName,''),IFNULL(b.MName,''),IFNULL(b.LName,'')) as FULLNAME"))
 						->where("b.idsponsor is null");							
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fngetinvoicenumber() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicemaster'),(array("key"=>"a.IdInvoice","value"=>"a.InvoiceNo")));
        $result = $lobjDbAdpt->fetchAll($lstrSelect);
         return $result;
	}
	
	public function fngetinvoicenumberstudent($StudentId) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		 $lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicemaster'),(array("key"=>"a.IdInvoice","value"=>"a.InvoiceNo")))
 						->join(array('b' => 'tbl_studentregistration'),'a.IdStudent = b.IdStudentRegistration')
 						->where("a.IdStudent = ?",$StudentId); 
 						
        $result = $lobjDbAdpt->fetchAll($lstrSelect);
         return $result;
	}
	
	public function fngetrecieptmasterdetails() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_recieptmaster'),(array("a.RecieptNumber",'a.RecieptDate','a.idReciept')))
 						->join(array('b' => 'tbl_studentregistration'),'a.StudentId = b.IdStudentRegistration')
 						->join(array('c' => 'tbl_studentapplication'),'b.IdApplication  = c.IdApplication',array("CONCAT_WS(' ',IFNULL(c.FName,''),IFNULL(c.MName,''),IFNULL(c.LName,'')) as FULLNAME"))
 						->join(array('d' => 'tbl_program'),'a.IdProgram = d.IdProgram',array('d.ProgramName'));
 						//->join(array('e' =>'tbl_semester'),'a.SemesterId = e.IdSemester',array('e.SemesterMainName'));
		//echo $lstrSelect;
	
        $result = $lobjDbAdpt->fetchAll($lstrSelect);
         return $result;
	}
	
	
	public function fngetinvoiceAmount($IDInvoice) {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		 $lstrSelect = $lobjDbAdpt->select()
 						->from(array('tbl_invoicemaster'),(array("InvoiceAmt")))
 						->where("IdInvoice = ?",$IDInvoice);
      	 $result = $lobjDbAdpt->fetchRow($lstrSelect);
         return $result;
	}
	
	public function fnGetInvoiceAmtWords($InvoiceAmt){
	
			$db = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect  = "SELECT CONCAT(ucwords(str_numtowords(345.00)),' ','Only') as InvoiceAmt";
			$result = $db->fetchAll($lstrSelect);
			return $result;
		}
		
	public function fngetitemgroup() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_itemgroup'),(array("key"=>"a.IdItem","value"=>"a.EnglishDescription")));
        $result = $lobjDbAdpt->fetchAll($lstrSelect);
         return $result;
	}
	
	public function fngetStudentApplicationDetails() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$larrstudarrayresult = "SELECT IdStudent from tbl_invoicemaster";
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicemaster'),array('a.*'))
 						->join(array('e'=>'tbl_studentregistration'),'a.IdStudent  = e.IdStudentRegistration')
 						->join(array('b'=>'tbl_studentapplication'),'e.IdApplication  = b.IdApplication')
 						->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
 						->join(array('d'=>'tbl_collegemaster'),'b.idCollege = d.IdCollege')
 						/*->where("a.IdStudent NOT IN ?",new Zend_Db_Expr('('.$larrstudarrayresult.')'))*/;
					
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fnEditInvoiceDetails($IdInvoice)//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicemaster'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdStudent=b.IdApplication')
	                    ->join(array('c'=>'tbl_invoicetermsandconditions'),'c.IdInvoice=a.IdInvoice')
 						->where('a.IdInvoice = ?',$IdInvoice);
					
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
	public function fnEditInvoiceDetailDetails($IdInvoicedetail)//select max idsemester
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
			$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_invoicedetails'),array('a.*'))
 						->join(array('b'=>'tbl_itemgroup'),'a.idAccount = b.IdItem')
 						->where('a.IdInvoice  = ?',$IdInvoicedetail);
		//echo $lstrSelect;		
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		return $result;
	}
	
   public function fngetStudentDetails($lintidstudent) { //Function to get the Program Branch details
 			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
 			$lstrSelect = $lobjDbAdpt->select()	
 					   				 ->from(array('b'=>'tbl_studentapplication'))
               						 ->join(array('c'=>'tbl_program'),'b.IDCourse=c.IdProgram')
               						 ->where("b.IdApplication= ?",$lintidstudent);
       $result = $lobjDbAdpt->fetchRow($lstrSelect);
       return $result;
     }
     
     /*
      * function for getting the prog charges
      */
    public function fngetProgramDetails($lintidprogram) 
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
      	$currentdate = date ('Y-m-d');
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_programcharges'),array('a.Charges','a.IdCharges'))
                			->join(array('b'=>'tbl_charges'),'a.IdCharges = b.IdCharges',array('b.ChargeName'))
                			->where('b.effectiveDate <= ?',$currentdate)
                			->where('a.IdProgram = ?',$lintidprogram)
                			->where('b.Payment = 1');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
	
 /*
     * function to get the drop down of the student
     */
  public function fngetStudentDropdown() { //Function to get the Program Branch details
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(a.FName,''),IFNULL(a.MName,''),IFNULL(a.LName,''))"))
		 				  ->join(array('b'=>'tbl_studentregistration'),'a.IdApplication=b.IdApplication',array())
		                  ->where("a.idsponsor is null")
		               		->group('a.IdApplication');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	/*
	 * function to search for the student in the sponsor manual invoice
	 */
	public function fnSearchStudentApplication($post)
	{
          $idstudent = $post['field5'];
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
          
          $larrstudarrayresult = "SELECT IdStudent from tbl_invoicemaster";
          
          if($post['field5']!='')
          {
			$consistantresult = "SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication AND i.IdApplication=$idstudent";
			
          }
          else {
          	$consistantresult = "SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = b.IdApplication AND i.IdApplication=b.IdApplication";
              }
		 $lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_studentregistration'),array('a.*'))
 						->join(array('b'=>'tbl_studentapplication'),'a.IdApplication=b.IdApplication')
 						->join(array('c'=>'tbl_invoicemaster'),'a.IdApplication = c.IdStudent')
 						->join(array('d'=>'tbl_program'),'b.IDCourse=d.IdProgram')
 						->join(array('e'=>'tbl_collegemaster'),'b.idCollege = e.IdCollege')
 						->where("c.InvoiceNo = ?",$post['field3'])
 						->where("c.IdStudent NOT IN ?",new Zend_Db_Expr('('.$larrstudarrayresult.')'))
 						->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'))
 						->where("b.idsponsor is null");
 						
	if(isset($post['field13']) && !empty($post['field13'])){
				$lstrSelect = $lstrSelect->where("b.IdCollege = ?",$post['field13']);
			}	

	if(isset($post['field11']) && !empty($post['field11'])){
				$lstrSelect = $lstrSelect->where("d.IdProgram = ?",$post['field11']);
				
			}	
			
	if(isset($post['field15']) && !empty($post['field15'])){
				$lstrSelect = $lstrSelect->where("c.InvoiceDt = ?",$post['field15']);
			}	
/*	if(isset($post['field3']) && !empty($post['field3'])){
				$lstrSelect = $lstrSelect->where("c.InvoiceNo = ?",$post['field3']);
			}	*/
		//echo $lstrSelect;die();
			
 		$result = $lobjDbAdpt->fetchAll($lstrSelect);
 		
 		return $result;
	}
	
	
    /*
     * function for fetching the subject charges
     */
    public function fnGetSubjectCharges($lintIdStudentRegistration)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentregsubjects'),array('a.*'))
                			->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject')
                			->where('a.IdStudentRegistration = ?',$lintIdStudentRegistration);
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                			
    }
    
    /*
     * function to insert into the invoicemaster table
     */
    public function fnAddInvoiceMaster($larrformData)
    {

    	    $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_invoicemaster";
            $InvoiceAmt =  array_sum($larrformData['Amountgrid']);

            $postData = array('IdStudent' => $larrformData['IdStudent'],		
            				'InvoiceNo' =>'Dec-'.' '.$larrformData['Studentid'],		
		            		'InvoiceDt' =>$larrformData['InvoiceDt'],	
		            		'InvoiceAmt'=>$InvoiceAmt,
           				    'IdSemester'=>$larrformData['semester'],
            				'AcdmcPeriod' =>$larrformData['AcdmcPeriod'],		
            				'Naration' =>$larrformData['Naration'],		
		            		'UpdDate' =>$larrformData['UpdDate'],	
		            		'UpdUser'=>$larrformData['UpdUser'],
            				'Approved'=>0,
            				'idsponsor'=>0,
						);			
	        $db->insert($table,$postData);
	        $lastinvoiceid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_invoicemaster','IdInvoice');
	         for($i=0;$i<count($larrformData['idAccountgrid']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicedetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
								'idAccount' => $larrformData['idAccountgrid'][$i],		
	            				'Amount' =>$larrformData['Amountgrid'][$i],		
	            				'Description' =>$larrformData['Descriptiongrid'][$i],	
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser'],
			            		'Discount' =>0,	
	                            'Active'=>1
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }
			 //////////////////Inserting to tbl_invoiceTermsAndConditions//////////////////
            for($i=0;$i<count($larrformData['termCondition']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoiceTermsAndConditions";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
							    'idTermsandconditions' =>$larrformData['termCondition'][$i]		
	            				
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   			 
		     return $lastinvoiceid;
	        ////////////////End of invoice details///////////////////////////////
	        
	       /////////////////inserting into the Subjecct Details///////////////////
/*	         for($i=0;$i<count($larrformData['IdSubject']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicesubjectdetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$lastinvoiceid,	
								'IdSubject' => $larrformData['IdSubject'][$i],		
	            				'Amount' =>$larrformData['Subjectamount'][$i],		
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser']
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   */
	        ////////////////End of subject Details/////////////////////////////// 	     
    }
    
    	public function fnUpdateInvoicemaster($larrformData) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_invoicemaster";	
            	
			$IdInvoicemaster=$larrformData['IdInvoice'];
			$where = "IdInvoice = '$IdInvoicemaster'";			
			$postData = array('IdStudent' => $larrformData['IdStudent'],		
            				'InvoiceDt' =>$larrformData['InvoiceDt'],
           				    'IdSemester'=>$larrformData['semester'],
            				'AcdmcPeriod' =>$larrformData['AcdmcPeriod'],		
            				'Naration' =>$larrformData['Naration'],		
		            		'UpdDate' =>$larrformData['UpdDate'],	
		            		'UpdUser'=>$larrformData['UpdUser'],            				
						);				
			
			$db->update($table,$postData,$where);	
		}	
		
		public function fndeleteInvoiceDetails($Idinvoice){			
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_invoicedetails";
	    	$where = $db->quoteInto('IdInvoice = ?', $Idinvoice);
			$db->delete($table, $where);
		
		}
		
		public function fndeleteInvoicetermCondition($Idinvoice){			
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_invoicetermsandconditions";
	    	$where = $db->quoteInto('IdInvoice = ?', $Idinvoice);
			$db->delete($table, $where);
		
		}
		
		public function fnAddInvoiceDetailsEdit($larrformData) {
			$db = Zend_Db_Table::getDefaultAdapter();
			
			for($i=0;$i<count($larrformData['idAccountgrid']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoicedetails";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$larrformData['IdInvoice'],	
								'idAccount' => $larrformData['idAccountgrid'][$i],		
	            				'Amount' =>$larrformData['Amountgrid'][$i],		
	            				'Description' =>$larrformData['Descriptiongrid'][$i],	
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser'],
			            		'Discount' =>0,	
	                            'Active'=>1
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     } 
}

public function fnAddInvoiceTermEdit($larrformData) {
           // print_r($larrformData);die();
			$db = Zend_Db_Table::getDefaultAdapter();
			
		  for($i=0;$i<count($larrformData['termCondition']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_invoiceTermsAndConditions";
	            $postDatadetails = array(	
	            				'IdInvoice'=>$larrformData['IdInvoice'],	
							    'idTermsandconditions' =>$larrformData['termCondition'][$i]		
	            				
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     }   			 
		
		}
		
    //*******************************************
 	public function fnGetInvoiceNO($result) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_invoicemaster"),array("a.InvoiceNo"))	
						->where("a.IdInvoice= ?",$result);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
	public function fnGetProgram($IdStudent) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("tbl_studentregistration" => "tbl_studentregistration"),array("tbl_studentregistration.IdApplication","tbl_studentregistration.IdStudentRegistration","tbl_studentregistration.registrationId"))	
						->join(array("tbl_studentapplication" =>"tbl_studentapplication"),"tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication",array("tbl_studentapplication.IDCourse","DATE_FORMAT(tbl_studentapplication.ApplicationDate,'%d-%m-%Y')as ApplicationDate","tbl_studentapplication.idCollege"))
						->join(array("tbl_program"=>"tbl_program"),"tbl_studentapplication.IDCourse = tbl_program.IdProgram",array("tbl_program.IdProgram","tbl_program.ProgramName"))
						->join(array("tbl_collegemaster"=>"tbl_collegemaster"),"tbl_studentapplication.idCollege = tbl_collegemaster.IdCollege",array("tbl_collegemaster.IdCollege","tbl_collegemaster.CollegeName"))
						->where("tbl_studentregistration.IdStudentRegistration= ?",$IdStudent);	
		return $result = $lobjDbAdpt->fetchRow($select);
		
	}
	
	public function fnGetAmount($idAccount,$idprogram,$semester){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("tbl_feesetupmaster" => "tbl_feesetupmaster"),array("tbl_feesetupmaster.IdFeesSetupMaster"))
						->join(array("tbl_feesetupdetail" => "tbl_feesetupdetail"),"tbl_feesetupmaster.IdFeesSetupMaster = tbl_feesetupdetail.IdFeessetupMaster",array("tbl_feesetupdetail.Amount"))	
						->join(array("tbl_programfees" =>"tbl_programfees"),"tbl_feesetupdetail.IdFeessetupMaster  = tbl_programfees.IdFeesSetupMaster",array(""))
						->where("tbl_programfees.IdProgram = ?",$idprogram)
						->where("tbl_feesetupdetail.DebitAccount = ?",$idAccount)
						->where("tbl_feesetupmaster.StartingIntake >= ? or tbl_feesetupmaster.EndSemester <= ?",$semester);	
						
		return $result = $lobjDbAdpt->fetchRow($select);
	}
	
	public function fnGetInvoiceDetails($InvoiceId){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$select 	= $lobjDbAdpt->select()
						->from(array("tbl_invoicemaster" =>"tbl_invoicemaster"),array("tbl_invoicemaster.IdInvoice","tbl_invoicemaster.InvoiceAmt","tbl_invoicemaster.InvoiceDt"))
						->join(array("tbl_invoicedetails" =>"tbl_invoicedetails"),"tbl_invoicemaster.IdInvoice = tbl_invoicedetails.IdInvoice",array("tbl_invoicedetails.Amount"))
						->join(array("tbl_itemgroup" =>"tbl_itemgroup"),"tbl_invoicedetails.idAccount = tbl_itemgroup.IdItem",array("tbl_itemgroup.IdItem","tbl_itemgroup.EnglishDescription"))
						->join(array("tbl_studentregistration" =>"tbl_studentregistration"),"tbl_invoicemaster.IdStudent = tbl_studentregistration.IdStudentRegistration",array("tbl_studentregistration.registrationId","tbl_studentregistration.IdSemester"))
						->join(array("tbl_studentapplication" =>"tbl_studentapplication"),"tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication",array("tbl_studentapplication.IdApplication","tbl_studentapplication.idCollege","tbl_studentapplication.IDCourse"))
						->joinLeft(array("tbl_recieptmaster" => "tbl_recieptmaster"),"tbl_invoicemaster.IdInvoice  = tbl_recieptmaster.InvoiceId",array("tbl_recieptmaster.idReciept"))
						->joinLeft(array("tbl_recieptmasterdetails" => "tbl_recieptmasterdetails"),"tbl_recieptmaster.idReciept  = tbl_recieptmasterdetails.idReciept AND tbl_recieptmasterdetails.idItem = tbl_itemgroup.IdItem",array("IFNULL(tbl_recieptmasterdetails.AmountPaid,'0.0') as AmountPaid","tbl_recieptmasterdetails.idRecieptDetails"))
						->where("tbl_invoicemaster.IdInvoice = ?",$InvoiceId)
						->group("tbl_recieptmaster.idReciept");	
			//	echo $select;exit;
		return $result = $lobjDbAdpt->fetchAll($select);
	}
	
	
	public function fnGetInvoiceDetailsEdit($InvoiceId){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$select 	= $lobjDbAdpt->select()
						->from(array("tbl_invoicemaster" =>"tbl_invoicemaster"),array("tbl_invoicemaster.IdInvoice","tbl_invoicemaster.InvoiceAmt","tbl_invoicemaster.InvoiceDt"))
						->join(array("tbl_invoicedetails" =>"tbl_invoicedetails"),"tbl_invoicemaster.IdInvoice = tbl_invoicedetails.IdInvoice",array("tbl_invoicedetails.Amount"))
						->join(array("tbl_itemgroup" =>"tbl_itemgroup"),"tbl_invoicedetails.idAccount = tbl_itemgroup.IdItem",array("tbl_itemgroup.IdItem","tbl_itemgroup.EnglishDescription"))
						->join(array("tbl_studentregistration" =>"tbl_studentregistration"),"tbl_invoicemaster.IdStudent = tbl_studentregistration.IdStudentRegistration",array("tbl_studentregistration.registrationId","tbl_studentregistration.IdSemester"))
						->join(array("tbl_studentapplication" =>"tbl_studentapplication"),"tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication",array("tbl_studentapplication.IdApplication","tbl_studentapplication.idCollege","tbl_studentapplication.IDCourse"))
						->joinLeft(array("tbl_recieptmaster" => "tbl_recieptmaster"),"tbl_invoicemaster.IdInvoice  = tbl_recieptmaster.InvoiceId",array("tbl_recieptmaster.idReciept"))
						->join(array("tbl_recieptmasterdetails" => "tbl_recieptmasterdetails"),"tbl_recieptmaster.idReciept  = tbl_recieptmasterdetails.idReciept AND tbl_recieptmasterdetails.idItem = tbl_itemgroup.IdItem",array("IFNULL(tbl_recieptmasterdetails.AmountPaid,'0.0') as AmountPaid","tbl_recieptmasterdetails.idRecieptDetails"))
						->where("tbl_invoicemaster.IdInvoice = ?",$InvoiceId)
						->group("tbl_recieptmaster.idReciept");	
			//	echo $select;exit;
		return $result = $lobjDbAdpt->fetchAll($select);
	}
	
	public function fnAddReceipt($larrformData) {
		
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_recieptmaster";

            $postData = array('RecieptNumber' => $larrformData['RecieptNumber'],		
            				 'RecieptDate' =>$larrformData['RecieptDate'],		
		            		 'InvoiceId' =>$larrformData['InvoiceId'],	
		            		 'StudentId'=>$larrformData['StudentId'],
           				     'SemesterId'=>$larrformData['SemesterId'],
            				 'IdProgram' =>$larrformData['IdProgram'],			
		            		 'UpdDate' =>$larrformData['UpdDate'],	
		            		 'UpdUser'=>$larrformData['UpdUser'],
            				 'ApprovedBy'=>$larrformData['UpdUser'],
                             'ChangedBy'=>$larrformData['UpdUser'],
                             'IdCollege'=>$larrformData['IdCollege'],
            				 'ModeOfPayment'=>$larrformData['ModeOfPayment'],
            				 'ChequeDate'=>$larrformData['ChequeDate'],
            				 'ChequeNo'=>$larrformData['ChequeNumber'],
						);			
	        $db->insert($table,$postData);	
	        return $lastrecieptid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_recieptmaster','idReciept');
	        
	}

	public function fninsertreceiptdetails($result,$larrformData) {
		
			for($i=0;$i<count($larrformData['IdItemgrid']);$i++)
			 {
				$db = Zend_Db_Table::getDefaultAdapter();
	            $tabledetails = "tbl_recieptmasterdetails";
	            $postDatadetails = array(	
	            				'idReciept'=>$result,	
								'idItem' => $larrformData['IdItemgrid'][$i],		
	            				'AmountPaid' =>$larrformData['amountpaying'][$i],		
			            		'UpdDate' =>$larrformData['UpdDate'],	
			            		'UpdUser'=>$larrformData['UpdUser']
							);			
		        $db->insert($tabledetails,$postDatadetails);
		     } 
		
	}
	 public function fnupdateReceipt($IdInvoice,$larrformData) { //Function for updating the user  
	 	
	 		$db = Zend_Db_Table::getDefaultAdapter();
	 		
			$table1 = "tbl_recieptmaster";	
			$table2 = "tbl_recieptmasterdetails";
			
	 			
	 	for($i=0;$i<count($larrformData['Idrecieptgrid']);$i++){			
	 					
	 			
	 					$updateData = array('RecieptDate' =>$larrformData['RecieptDate'],
           				     'SemesterId'=>$larrformData['SemesterId'],
            				 'IdProgram' =>$larrformData['IdProgram'],			
		            		 'UpdDate' =>$larrformData['UpdDate'],	
		            		 'UpdUser'=>$larrformData['UpdUser'],
            				 'ApprovedBy'=>$larrformData['UpdUser'],
                             'ChangedBy'=>$larrformData['UpdUser'],
                             'IdCollege'=>$larrformData['IdCollege'],
            				 'ModeOfPayment'=>$larrformData['ModeOfPayment'],
            				 'ChequeDate'=>$larrformData['ChequeDate'],
            				 'ChequeNo'=>$larrformData['ChequeNumber'],
							);		
  
							
							$updatedatadetails = array('idItem' =>$larrformData['IdItemgrid'][$i],
           				    					 'AmountPaid'=>$larrformData['amountpaying'][$i],            									 		
		            							 'UpdDate' =>$larrformData['UpdDate'],	
		            							 'UpdUser'=>$larrformData['UpdUser'],            									
									);	
									
					$where2 = 'idRecieptDetails = '.$larrformData['IdRecieptDetailsgrid'][$i].' AND idReciept ='.$larrformData['Idrecieptgrid'][$i];
					$db->update($table2,$updatedatadetails,$where2);			
	 				$where1 = 'idReciept = '.$larrformData['Idrecieptgrid'][$i].' AND InvoiceId ='.$IdInvoice;	 					
					$db->update($table1,$updateData,$where1);	
	 			}
	 	
		
    }
	
	public function fnupdateinvoicetermconditions($IdReceipt,$larrformData) { //Function for updating the user 
        $db = Zend_Db_Table::getDefaultAdapter();	
	    $table = "tbl_invoicetermsandconditions";
		$where = 'IdInvoice = '.$IdReceipt;
		$this->update($table,$larrformData,$where);
    }
    
	public function fnEditReceipt($IdReceipt){
 			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       				->from(array("rm"=>"tbl_recieptmaster"),array("rm.*"))
       				->join(array("rd"=>"tbl_recieptmasterdetails"),'rm.idReciept=rd.idReciept')
       				->join(array("im"=>"tbl_invoicemaster"),'rm.InvoiceId=im.IdInvoice')
       				->join(array("id"=>"tbl_invoicedetails"),'im.IdInvoice = id.IdInvoice')
       				->where('rm.idReciept = ?',$IdReceipt);					
        	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
	public function fngetTermsConditions() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = $lobjDbAdpt->select()
 						->from(array('a'=>'tbl_termsandconditions'),(array("key"=>"a.idTermsandconditions","value"=>"a.Description","default"=>"a.Default")));
        $result = $lobjDbAdpt->fetchAll($lstrSelect);
         return $result;
	}
	
	public function fnGenerateAccCodes($idUniversity,$lastrecieptid,$objIC,$studentid){			 
			$result = 	$objIC->fnGetInitialConfigDetails($idUniversity);//GeneralbaseSeparator
			$sepr	=	$result['ReceiptTypeSeparator'];
			$str	=	"ReceiptTypeField";
			for($i=1;$i<=4;$i++){
				$check = $result[$str.$i];
				switch ($check){
					case 'Year':
					  $code	= date('Y');
					  break;
					case 'Receiptid':
					  $code	= $lastrecieptid;
					  break;	
					case 'Date':
					  $code	= date('Y-m-d');
					  break;						
					case 'Student id':
					  $code	= $studentid;
					  break;	
					default:
					  break;
				}
				if($i == 1) $accCode 	 =  $code;
				else 		$accCode	.=	$sepr.$code;
			}				
			return $accCode;	
		}

   public function fnUpdateRecieptCode($lastrecieptid,$resultcode) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_recieptmaster";		
			$where = "idReciept = '$lastrecieptid'";			
			$postData = array('RecieptNumber' => $resultcode);
			$db->update($table,$postData,$where);	
		}
		
    public function fnGetEditReceiptDetails($idReciept) {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' =>'tbl_recieptmaster'),array('a.*'))
                			->where('a.idReciept = ?',$idReciept);
       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;                			
    }
    
    public function getPreregSubject($trans_id){
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select = $db->select()
					 ->from(array('im' =>'invoice_main'),array('invoice_main_id'=>'id'))
					// ->join(array('id' => 'invoice_detail'),'id.invoice_main_id=im.id',array())
					 ->join(array('ins' => 'invoice_subject'),'im.id=ins.invoice_main_id',array('subject_id'))
                	 ->where('im.trans_id= ?',$trans_id)
                	 ->group('ins.subject_id');
        $subjects = $db->fetchAll($select);
        
        foreach($subjects as $index=>$subject){
      
        	    $select2 = $db->select()	
							 ->from(array('ins' => 'invoice_subject'),array())
							 ->join(array('ind' => 'invoice_detail'),'ins.invoice_detail_id=ind.id',array('fi_id'))
							 ->where('ins.invoice_main_id = ?',$subject['invoice_main_id'])
		                	 ->where('ins.subject_id= ?',$subject['subject_id'])
		                	 ->group('ins.subject_id')
		                	 ->group('ind.fi_id');;
        		$item = $db->fetchAll($select2);
          		$subjects[$index]['item']=$item;
        }
       
		return $subjects;                			
    }
    
	public function getPreregisterSubject($trans_id){
		
		/*
	 	 *  select pim.*,pis.subject_id
			FROM proforma_invoice_main as pim 
			JOIN proforma_invoice_subject as pis ON pis.proforma_invoice_main_id = pim.id
			WHERE pim.fee_category = 8
			//tukar AND (pim.status!='X') 4-2-2015
			AND (pim.status='A')
			AND pim.trans_id=1455
			group by pis.subject_id
	 	 */
		
		
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select = $db->select()
					 ->from(array('pim' => 'proforma_invoice_main'),array('invoice_main_id'=>'id'))
					 ->join(array('pis' => 'proforma_invoice_subject'),'pis.proforma_invoice_main_id = pim.id',array('subject_id'))
                	 ->where('pim.fee_category = ?',8)
                	 ->where('pim.status="A"')
                	 ->where('pim.trans_id = ?',$trans_id)
                	 ->group('pis.subject_id');
        $subjects = $db->fetchAll($select);
        
       
        foreach($subjects as $index=>$subject){
        	
        	$select2 = $db->select()	
         			 ->from(array('pis' => 'proforma_invoice_subject'),array())					
					 ->join(array('pid' => 'proforma_invoice_detail'),'pid.id=pis.proforma_invoice_detail_id',array('fi_id'))
                	 ->where('pis.proforma_invoice_main_id = ?',$subject['invoice_main_id'])
                	 ->where('pis.subject_id = ?',$subject['subject_id'])
                	 ->group('pis.subject_id')
                	 ->group('pid.fi_id');
        	$item = $db->fetchAll($select2);
        	$subjects[$index]['item']=$item;
        }        
       
       
		return $subjects;                			
    }
	
	
		
	

}