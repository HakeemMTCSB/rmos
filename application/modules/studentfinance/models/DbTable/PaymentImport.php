<?php
/**
 *  @author alif 
 *  @date Jun 28, 2013
 */
 
class Studentfinance_Model_DbTable_PaymentImport extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'payment_import';
	protected $_primary = "id";

	public function getData($id=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('pi'=>$this->_name));
		
		if($id!=null){
			$selectData->where("pi.id = ?", (int)$id);
			
			$row = $db->fetchAll($selectData);
		}else{
			$row = $db->fetchRow($selectData);
		}

		if(!$row){
			return null;
		}else{
			return $row;
		}
		
		
		return $row;
	}
	
	public function insert($data){
	
		if( !isset($data['payment_date']) ){
			$data['payment_date'] = date('Y-m-d H:i:s');
		}
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['importer'])){
			$data['importer'] = $auth->getIdentity()->iduser;
		}
		
		if( !isset($data['import_date']) ){
			$data['import_date'] = date('Y-m-d H:i:s');
		}
			
		return parent::insert( $data );
	}
}
 ?>