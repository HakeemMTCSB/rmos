<?php
/**
 *  @author alif 
 *  @date Feb 27, 2014
 */
 
class Studentfinance_Model_DbTable_StudentSemesterPayment extends Zend_Db_Table_Abstract {
  /**
   * The default table name
   */
  protected $_name = 'rpt_student_semester_payment';
  protected $_primary = "id";

  
  public function getData($id){
    
    $db = Zend_Db_Table::getDefaultAdapter();
    
    $select = $db->select()
              ->from(array('rpt'=>$this->_name))
              ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = rpt.semester_id',array('semester_name'=>'SemesterMainName','semester_code'=>'SemesterMainCode'))
              ->join(array('fc'=>'tbl_collegemaster'),'fc.IdCollege = rpt.faculty_id',array('faculty_name'=>'ArabicName','faculty_code'=>'CollegeCode'))
              ->join(array('p'=>'tbl_program'),'p.IdProgram = rpt.program_id', array('program_name'=>'ArabicName','program_code'=>'ProgramCode'))
              ->where('rpt.id = ?', $id);

    $rows = $db->fetchRow($select);
    
    if($rows){
      return $rows;
    }else{
      return null;
    }
    
    
  }
}
 ?>