<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */

class Studentfinance_Model_DbTable_PaymentMode extends Zend_Db_Table_Abstract {
	
	/**
	 * The default table name
	 */
	protected $_name = 'payment_mode';
	protected $_primary = "id";
	protected $_locale;
	
	public function init() {
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
	
	
	public function getData($id=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
						->from(array('pm'=>$this->_name))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = pm.payment_group', array('payment_category_name'=>'d.DefinitionDesc','payment_category_name2'=>'d.Description'));
	
		if($id!=0){
			$selectData->where("pm.id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
				
		}else{
				
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}
	
	public function getPaymentModeList($paymentGroup=null, $active=1){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
			->from(array('pm'=>$this->_name))
			->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = pm.payment_group', array('payment_category_name'=>'DefinitionDesc','payment_category_name2'=>'Description'))
			->where('pm.active = ?',$active);
		
		if($paymentGroup){
			$selectData->where('pm.payment_group = ?', $paymentGroup);
		}
		
		//echo $selectData;
		$row = $db->fetchAll($selectData);
		
		return $row;
		
	}
	
/*
	 * Get the list of payment mode of the respected payment group and current user's university
	*/
	public function fnGetPaymentMode($idGroupPayment){
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
		->from(array("a" => "payment_mode"), array("key" => "a.id" , "name"=> 'a.payment_mode'))
		->where('a.payment_group =?',$idGroupPayment);
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
}