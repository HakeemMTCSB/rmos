<?php
class Studentfinance_Model_DbTable_ScholarshipDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'sponsor_application_details';
    protected $_primary = 'id_detail';

    protected $_referenceMap    = array(
        'EducationLevel' => array(
            'columns'           => 'education_id',
            'refTableClass'     => 'App_Model_General_DbTable_Qualificationmaster',
            'refColumns'        => 'IdQualification'
        ),
        'Scholarship' => array(
            'columns'           => 'application_id',
            'refTableClass'     => 'Studentfinance_Model_DbTable_SponsorApplication',
            'refColumns'        => 'sa_id'
        ),

    );

    public function getData($id)
    {

        $select = $this->select()
            ->where("pm.sa_id = ?", (int) $id);

        $row = $this->fetchRow($select);
        return $row;
    }

    public function save(array $data){

        if( isset($data[$this->_primary]) ) {
            $where  = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $data[$this->_primary]);
            $this->update($data, $where);
            $id = $data[$this->_primary];
        } else {
            $id = $this->insert($data);
        }

        return $id;
    }

}