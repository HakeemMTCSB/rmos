<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_ProformaInvoiceDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'proforma_invoice_detail';
	protected $_primary = "id";

	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('id'=>$this->_name))
						->where("id.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getDetail($proforma_main_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('id'=>$this->_name))
						->join(array('fi'=>'fee_item'), 'fi.fi_id = id.fi_id')
						->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = id.cur_id')
						->where("id.proforma_invoice_main_id = ?", (int)$proforma_main_id);
		
		$row = $db->fetchAll($selectData);
		return $row;
	}
	
	
}