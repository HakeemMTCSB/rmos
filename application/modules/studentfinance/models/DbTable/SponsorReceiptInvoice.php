<?php
/**
 * @author Suliana <suliana@meteor.com.my>
 * @copyright Copyright (c) 2015, MTCSB
 */
class Studentfinance_Model_DbTable_SponsorReceiptInvoice extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'sponsor_invoice_receipt_invoice';
	protected $_primary = "rcp_inv_id";

	
	public function getDataFromReceipt($receipt_id, $invoice_id=null){
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('ri'=>$this->_name))
					->where('ri.rcp_inv_rcp_id = ?', $receipt_id)
					->order('ri.rcp_inv_rcp_no DESC');

		if($invoice_id!=null){
			$selectData->where('ri.rcp_inv_invoice_id = ?', $invoice_id);
		}
		
		$row = $db->fetchAll($selectData);

		return $row;
	}
	
	public function insert(array $data){

		$auth = Zend_Auth::getInstance();

		if(!isset($data['rcp_inv_create_by'])){
			$data['rcp_inv_create_by'] = $auth->getIdentity()->iduser;
		}
		
		$data['rcp_inv_create_date'] = date('Y-m-d H:i:s');

		return parent::insert($data);
	}
	
}