<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_TransactionMigs extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'transaction_migs';
	protected $_primary = "mt_id";
	
	
	/*
	 * Overite Insert function
	*/
	
	public function insert(array $data){
	
		if( !isset($data['mt_txn_date'])  ){
			$data['mt_txn_date'] = date('Y-m-d H:i:s');
		}
	
		return parent::insert( $data );
	}


}