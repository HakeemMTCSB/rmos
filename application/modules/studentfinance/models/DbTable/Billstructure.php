<?php
class Studentfinance_Model_DbTable_Billstructure extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_billstructuremaster';

	public function fngetBillStructureDetails($idBillStructureMaster = "") {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("bsm" => "tbl_billstructuremaster"),array("bsm.*"))
								  ->join(array("sm"=>"tbl_semestermaster"),"sm.IdSemesterMaster = bsm.IdIntakeFrom ",array("sm.SemesterMasterName AS IntakeFromName"))
 								  ->join(array("smto"=>"tbl_semestermaster"),"smto.IdSemesterMaster = bsm.IdIntakeTo ",array("smto.SemesterMasterName AS IntakeToName"))
								  ->where("bsm.Active = ?","1") ;	
		if($idBillStructureMaster)	$lstrSelect			 ->where("bsm.idBillStructureMaster  = ?",$idBillStructureMaster) ;			  	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  
       return $larrResult;
     }
	public function fnGetIdGroupSelect(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("key"=>"a.idAccount","value"=>"a.AccountName"))
								  ->where("a.Active = ?","1")
								  ->where("a.IdGroup = ?","NULL");		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}	
	function fnGetBillingModuleSelect(){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_definationtypems')
				 ->where('defTypeDesc = ?','BillingModule');				 		 
		$result = $this->fetchRow($select);
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = ?",$result['idDefType'])
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
 	public function   fnGetProgramSelect(){
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_program"),array("key"=>"a.IdProgram","value"=>"a.ProgramName"))
								  ->where("a.Active = ?","1");
								 // ->where("a.AccountType = ?",$AccountType);		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;    	
    }
	public function fnGetidVoucherTypeSelect()  {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_vouchertype"),array("key"=>"a.idVoucherType","value"=>"a.VoucherType"))
								  ->where("a.Active = ?","1");		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	} 
	function fnGetPeriodSelect(){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_definationtypems')
				 ->where('defTypeDesc = ?','Charging Type');				 		 
		$result = $this->fetchRow($select);
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = ?",$result['idDefType'])
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnGetIdAccountSelect($AccountType){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("key"=>"a.idAccount","value"=>"a.AccountName"))
								  ->where("a.Active = ?","1")
								  ->where("a.AccountType = ?",$AccountType);		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnInsertstructreData($insertData) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_billstructuremaster";
	   	$db->insert($table,$insertData);
	   	return $db->lastInsertId("tbl_billstructuremaster","idBillStructureMaster");	   
	} 
	public function fnInsertProgramData($insertData,$insertPrgmData) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_billprogram";      
       	for($lvari=0;$lvari<count($insertData);$lvari++){
       		$insertPrgmData['idProgram'] = $insertData[$lvari];
		   	$db->insert($table,$insertPrgmData);
       	}	   		   
	} 
	public function fnInsertDetailsData($larrmDetailsData,$larrmDetailsDataInsert) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_billstructuredetails";      
       	for($lvari=0;$lvari<count($larrmDetailsData['idVoucherType']);$lvari++){
       		$larrmDetailsDataInsert['idVoucherType'] = $larrmDetailsData['idVoucherType'][$lvari];
       		$larrmDetailsDataInsert['itemDescription'] = $larrmDetailsData['itemDescription'][$lvari];
       		$larrmDetailsDataInsert['idCalculationMethod'] = $larrmDetailsData['idCalculationMethod'][$lvari];
       		$larrmDetailsDataInsert['Amount'] = $larrmDetailsData['Amount'][$lvari];
       		$larrmDetailsDataInsert['idDebitAccount'] = $larrmDetailsData['idDebitAccount'][$lvari];
			$larrmDetailsDataInsert['idCreditAccount'] = $larrmDetailsData['idCreditAccount'][$lvari];
       		$db->insert($table,$larrmDetailsDataInsert);
       	}	   		   
	} 
	
	public function fngetBillProgramDetails($idBillStructureMaster){	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("bsm" => "tbl_billprogram"),array("bsm.idBillProgram","bsm.IdProgram"))
								  ->join(array("pgm"=>"tbl_program"),"pgm.IdProgram = bsm.IdProgram",array("pgm.ProgramName"))
 								  ->where("bsm.Active = ?","1") ;	
		$lstrSelect			 ->where("bsm.idBillStructureMaster  = ?",$idBillStructureMaster) ;			  	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);  
       return $larrResult;
     }
	
	public function fngetBillStructureDetailsSearch($post = array()){	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					 ->from(array("bsm" => "tbl_billstructuremaster"),array("bsm.*"))
					 ->join(array("sm"=>"tbl_semestermaster"),"sm.IdSemesterMaster = bsm.IdIntakeFrom ",array("sm.SemesterMasterName AS IntakeFromName"))
 					 ->join(array("smto"=>"tbl_semestermaster"),"smto.IdSemesterMaster = bsm.IdIntakeTo ",array("smto.SemesterMasterName AS IntakeToName"))
					 ->where("bsm.Active = ?","1");
		if($post['field2'])	 $lstrSelect->	where("bsm.BillMasterCode  LIKE '%".$post['field2']."%'");
		if($post['field5'])	 $lstrSelect->	where("bsm.IdIntakeFrom  = ?",$post['field5']);
		if($post['field8'])	 $lstrSelect->	where("bsm.IdIntakeTo  = ?",$post['field8']);
		if($post['field15'])	 $lstrSelect->	where("bsm.IdCategory  = ?",$post['field15']);
					 
					 
		return $result = $lobjDbAdpt->fetchAll($lstrSelect);		
	}
	public function fngetBillDetails($idBillStructureMaster) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("bsd" => "tbl_billstructuredetails"),array("bsd.*"))
								  ->join(array("vt"=>"tbl_vouchertype"),"vt.idVoucherType = bsd.idVoucherType",array("vt.VoucherType AS VoucherTypeName"))
 								  ->join(array("dm"=>"tbl_definationms"),"dm.idDefinition = bsd.idCalculationMethod",array("dm.DefinitionDesc"))
 								  ->join(array("amd"=>"tbl_accountmaster"),"amd.idAccount = bsd.idDebitAccount",array("amd.AccountName AS AccountNameD"))
 								  ->join(array("amc"=>"tbl_accountmaster"),"amc.idAccount = bsd.idCreditAccount",array("amc.AccountName AS AccountNameC"))
								  ->where("bsd.Active = ?","1") 	
							      ->where("bsd.idBillStructureMaster  = ?",$idBillStructureMaster) ;			  	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);  
       return $larrResult;
     }  
     
	Public function fnUpdatestructreData($data,$idBillStructureMaster) {  	   
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();		 
		$where['idBillStructureMaster = ? ']= $idBillStructureMaster;		
		return $db->update('tbl_billstructuremaster', $data, $where);
	} 
	
	Public function fnUpdateUnsetall($idBillStructureMaster) {  	   
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();	
	    $data['Active']	 = 0;
		$where['idBillStructureMaster = ? ']= $idBillStructureMaster;		
		$db->update('tbl_billprogram', $data, $where);			
		$db->update('tbl_billstructuredetails', $data, $where);
	} 
	
	
	
	Public function fnUpdateProgramData($larridProgramgrid,$larridBillProgramgrid,$idBillStructureMaster) {  	   
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();		    
	    $auth = Zend_Auth::getInstance();
	    for($lvars=0;$lvars<count($larridProgramgrid);$lvars++){	    	
	    	if($larridBillProgramgrid[$lvars]){
	    		$data['UpdUser']	 = $auth->getIdentity()->iduser;	
	    		$data['UpdDate']	 = date ( 'Y-m-d H:i:s' );
	    		$data['Active']	 = 1;
	    		$data['idProgram']	 = $larridProgramgrid[$lvars];
				$where['idBillProgram = ? ']= $larridBillProgramgrid[$lvars];		
				$db->update('tbl_billprogram', $data, $where);	    		
	    	}else{
	    		$insertPrgmData['idBillStructureMaster'] = $idBillStructureMaster;
	    		$insertPrgmData['idProgram'] = $larridProgramgrid[$lvars];
	    		$insertPrgmData['UpdUser'] = $auth->getIdentity()->iduser;	
	    		$insertPrgmData['UpdDate'] = date ( 'Y-m-d H:i:s' );
	    		$insertPrgmData['Active'] = 1;
		   		$db->insert('tbl_billprogram',$insertPrgmData);
	    	}
	    	
	    }	  
	} 
	
	Public function fnUpdateStructureDetails($larrmDetailsData,$ididBillStructureDetailsgrid) {  	   
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();		    
	    $auth = Zend_Auth::getInstance();
	    for($lvars=0;$lvars<count($larrmDetailsData['idVoucherType']);$lvars++){	
	    	$larrmDetailsDataInsert['idVoucherType'] = $larrmDetailsData['idVoucherType'][$lvars];
       		$larrmDetailsDataInsert['itemDescription'] = $larrmDetailsData['itemDescription'][$lvars];
       		$larrmDetailsDataInsert['idCalculationMethod'] = $larrmDetailsData['idCalculationMethod'][$lvars];
       		$larrmDetailsDataInsert['Amount'] = $larrmDetailsData['Amount'][$lvars];
       		$larrmDetailsDataInsert['idDebitAccount'] = $larrmDetailsData['idDebitAccount'][$lvars];
			$larrmDetailsDataInsert['idCreditAccount'] = $larrmDetailsData['idCreditAccount'][$lvars];
			$larrmDetailsDataInsert['UpdUser'] = $larrmDetailsData['UpdUser'][0];
			$larrmDetailsDataInsert['UpdDate'] = $larrmDetailsData['UpdDate'][0];
			$larrmDetailsDataInsert['Active'] = 1;   	
	    	if($ididBillStructureDetailsgrid[$lvars]){	    		
				$where['idBillStructureDetails = ? ']= $ididBillStructureDetailsgrid[$lvars];		
				$db->update('tbl_billstructuredetails', $larrmDetailsDataInsert, $where);	    		
	    	}else{	    		
				$larrmDetailsDataInsert['idBillStructureMaster'] = $larrmDetailsData['idBillStructureMaster'][0];				
		   		$db->insert('tbl_billstructuredetails',$larrmDetailsDataInsert);
	    	}	    	
	    }	  
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
public function fngetAccountDetails() {
        $result = $this->fetchAll('Active = 1', "AccountName ASC");
       return $result;
     }
    
public function fnviewAccountmaster($lintidAccount) {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("c"=>"tbl_accountmaster"),array("c.*"))			
		            	->where("c.idAccount= ?",$lintidAccount);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }

    





Public function fnupdateaccountmaster($lvarEdit,$larrformData) {  
	    if(!$larrformData['BillingModule']) $larrformData['BillingModule'] = 0;	
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();		
	    
	    if($larrformData['IdGroup'] == '')
	    {
	    	$idgroup = 0;
	    }
	    else 
	    $idgroup = $larrformData['IdGroup'];
	 	$data = array('AccountName' => $larrformData['AccountName'],
	 	 			  'AccShortName' => $larrformData['AccShortName'],
					  'AccountType' => $larrformData['AccountType'],
					  'Description' => $larrformData['Description'],
					  'Period' => $larrformData['Period'],
					  'Revenue' => $larrformData['Revenue'],
	 				  'IdGroup' =>$idgroup,
	 				  'BillingModule'=>$larrformData['BillingModule'],
	 				  'duringRegistration'=>$larrformData['duringRegistration'],	
	 	              'AdvancePayment'=>$larrformData['AdvancePayment'],
				 	  'UpdDate' => $larrformData['UpdDate'],
				 	  'UpdUser' => $larrformData['UpdUser'],
				 	  'Active' => $larrformData['Active'],
	 	              'AccountCode'=>$larrformData['AccountCode'],
				      'Deposit'=>$larrformData['Deposit'],
				      'Refund'=>$larrformData['Refund'],
	 				  'duringProcessing'=>$larrformData['duringProcessing']				 	 
				 	  );
		$where['idAccount = ? ']= $lvarEdit;		
		return $db->update('tbl_accountmaster', $data, $where);
	}     
public function fngetaccountdeatilsSearch($post = array()){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		if(!$post['field7']) $sqlquery ="  Active = 0 ";
		else $sqlquery	= " Active = 1 ";	
		
		$select = $this->select()
					-> setIntegrityCheck(false)
					-> join(array('a' => 'tbl_accountmaster'),array('idAccount'))
					-> where('a.AccountName like  ? "%"',$post['field2'])
					-> where('a.PrefixCode  like  ? "%"',$post['field3'])
					-> where('a.AccShortName like  ? "%"',$post['field4'])
					-> where($sqlquery);
		$result = $this->fetchAll($select);
		return $result->toArray();
		
	}

	
function fnGetAccountcodeSelect()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("a.AccountCode"))
								  ->where('a.AccountCode is not Null');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	
	/*
	 * function for generating the code for the account master
	 */
function fnGenerateCode($universityId,$uniqId,$ShortName){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $this->select()
			    ->  setIntegrityCheck(false)  
				->  from('tbl_config')
				->  where('idUniversity = ?',$universityId);  				 
		$result = 	$this->fetchRow($select);		
		$sepr	=	$result['AccountSeparator'];
		$str	=	"AccountCodeField";
		for($i=1;$i<=4;$i++){
			$check = $result[$str.$i];
			switch ($check){
				case 'Text':
				  $code	= $result['AccountCodeText'.$i];
				  break;				  
				case 'ShortName':
				  $code	= $ShortName;
				  break;
				case 'Year':
					$code	= date('Y');
				  break;
				case 'Uniqueid': 
					$code	= $uniqId;
				    break;
				default:
				   break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}		
	 	$data = array('PrefixCode' => $accCode);
		$where['idAccount = ? ']= $uniqId;		
		return $db->update('tbl_accountmaster', $data, $where);			
	}		
}