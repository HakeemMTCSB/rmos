<?php
class Studentfinance_Model_DbTable_Receivableadjustmentinvoice extends Zend_Db_Table { 
	protected $_name = 'tbl_receivable_adjustment_invoice';

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fngetReceivableadjustmentinvoicedetl($IdReceivableAdjustment){
		$lstrSelect = $this->lobjDbAdpt->select()
							->from(array('a' => 'tbl_receivable_adjustment'),array(''))
							->joinLeft(array('b' => 'tbl_receivable_adjustment_invoice'), 'b.IdReceivableAdjustment=b.IdReceivableAdjustment',array('b.NewPaidAmount','b.TotalAmountReceipt','b.TotalPaidReceipt','b.TotalBalanceReceipt'))
							->joinLeft(array('c' => 'tbl_invoicedetl'), 'b.IdInvoiceDetail=c.AUTOINC',array('c.BILLNUM','c.BILLSEQ','c.FEECODE','c.FEEDESC','c.ACCTCODECH'))
							->joinLeft(array('d' => 'tbl_invoicemain'),'b.IdInvoice = d.AUTOINC',array('d.DOCDATE','d.BILLNUM'))
							->where("a.IdReceivableAdjustment= ?", $IdReceivableAdjustment);
		$result = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
}