<?php

class Studentfinance_Model_DbTable_Promotion extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_promotion_setup';
    private $lobjDbAdpt;
    protected $_locale;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $registry = Zend_Registry:: getInstance();
        $this->_locale = $registry->get('Zend_Locale');
	}


	public function fnSearchPromotionSetup($post = array()) {

		$select = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_promotion_setup"), array("a.*"))
            ->join(array('aw'=>'tbl_award_level'),"a.awardid=aw.id",array("aw.gradedesc"));
			
		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where($this->lobjDbAdpt->quoteInto("a.promotion_code LIKE ?", "%".$post['field2']."%"));

		}

	 if(isset($post['field3']) && !empty($post['field3']) ){
	 	$select = $select->where($this->lobjDbAdpt->quoteInto("a.promotion_description like ?", "%".$post['field3']."%"));

	 }

	 $result = $this->lobjDbAdpt->fetchAll($select);
	 return $result;
	}



	public function fnaddPromotionSetup($larrformData){
        $auth = Zend_Auth::getInstance();
		$paramArray = array(
				'promotion_code' => $larrformData['Promotion_Code'],
				'promotion_description' => $larrformData['Promotion_description'],
				'awardid' => $larrformData['Awardid'],
				'sem_start' => date('Y-m-d',strtotime($larrformData['sem_start'])),
				'sem_end' => date('Y-m-d',strtotime($larrformData['sem_end'])),
                'reg_start' =>  date('Y-m-d',strtotime($larrformData['reg_start'])),
				'reg_end' =>  date('Y-m-d',strtotime($larrformData['reg_end'])),
				'min_payment' => $larrformData['min_payment'],
				'total_sem' => $larrformData['total_sem'],
				'adjustment_type' => $larrformData['adjustment_type'],
				'exclusive' =>$larrformData['exclusive'],
				'active' => $larrformData['active'],
				'createddt' => date ('Y-m-d H:i:s'),
				'createdby'=>$auth->getIdentity()->iduser
		);

		$this->lobjDbAdpt->insert('tbl_promotion_setup',$paramArray);
		$lastinsertID = $this->lobjDbAdpt->lastInsertId();
		

	}


	public function fnupdateSponsorSetup($larrformData,$id){

		$paramArray = array(
				'fName' => $larrformData['FirstName'],
				'lName' => $larrformData['LastName'],
				'Add1' => $larrformData['Address1'],
				'Add2' => $larrformData['Address2'],
				'Country' => $larrformData['Country'],
				'State' => $larrformData['State'],
				'City' => $larrformData['City'],
				'zipCode' => $larrformData['Postal'],
				'CellPhone' => $larrformData['Phone'],
				'Fax' => $larrformData['Fax'],
				'Email' => $larrformData['EmailAddress'],
		);
		$where_up = "  idsponsor =' ".$id."' ";
		$this->lobjDbAdpt->update('tbl_sponsor',$paramArray,$where_up);

		if($larrformData['FeeCodeGrid']){
			$check = count($larrformData['FeeCodeGrid']);
			for($i=0;$i<$check;$i++) {
				$paramArray = array(
						'idsponsor'=>$id,
						'FeeCode'=> $larrformData['FeeCodeGrid'][$i],
						'CalculationMode'=> $larrformData['CalculationModeGrid'][$i],
						'Amount'=>$larrformData['AmountGrid'][$i],
						'Repeat'            => $larrformData['RepeatGrid'][$i],
						'MaxRepeat'            => $larrformData['MaxRepeatGrid'][$i],
						'FreqMode'            => $larrformData['FreqModeGrid'][$i],
				);

				$this->lobjDbAdpt->insert('tbl_sponsor_fee_info',$paramArray);
			}

		}

		//Coordinator
		$auth = Zend_Auth::getInstance();
		if ( isset($larrformData['coordinator_name']) )
		{
			$totalCoord = count($larrformData['coordinator_name']);
		
			if ( count ( $totalCoord ) > 0 )
			{
				for( $i=0; $i<=count($totalCoord); $i++ )
				{
					$data = array(
							'c_sponsor_id'=>$id,
							'c_name'=> $larrformData['coordinator_name'][$i],
							'c_email'=> $larrformData['coordinator_email'][$i],
							'c_phone'=> $larrformData['coordinator_phone'][$i],
							'c_mobile'=> $larrformData['coordinator_mobile'][$i],
							'c_fax'=> $larrformData['coordinator_fax'][$i],
                                                        'c_active'=> $larrformData['coordinator_active'][$larrformData['coordinator_name'][$i]],
							'created_by' => $auth->getIdentity()->iduser,
							'created_date' => new Zend_Db_Expr('NOW()')
					);

					$this->lobjDbAdpt->insert('tbl_sponsor_coordinator',$data);
				}
			}
		}
	}

    public function checkDuplicate($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("p" => $this->_name))
            ->where('p.promotion_code  =?', $code);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }

    public function fngetpromotionsetupById($id){

        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a' => 'tbl_promotion_setup'),array('a.*'))
            ->where('a.promotion_id =?',$id);

        $result = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $result;
    }

    public function updatePromotion($data, $id)
    {
        $this->lobjDbAdpt->update('tbl_promotion_setup', $data, 'promotion_id='.$id);
    }


}