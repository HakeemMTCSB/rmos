<?php

class Studentfinance_Model_DbTable_ApplicantFinancial extends Zend_Db_Table_Abstract 
{

    //put your code here
    protected $_name = 'applicant_financial';
    protected $_primary = "af_id";

    protected $_dependentTables = array('SponsorApplication');

    protected $_referenceMap    = array(
        'Transaction' => array(
            'columns'           => 'appl_id',
            'refTableClass'     => 'App_Model_Application_DbTable_ApplicantTransaction',
            'refColumns'        => 'at_appl_id'
        ),
    );

    public function get_list() {
    	$sql = $this->select()
		->from($this,array('key'=>'af_id','value'=>'af_sponsor_name'))
		;
		$result = $this->fetchAll($sql);
		return $result->toArray();
	}
}