<?php
class Studentfinance_Model_DbTable_Termsandconditions extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_termsandconditions';
	public function fngetlobjTermsandconditionsDetails($idTermsandconditions = "") {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tc" => "tbl_termsandconditions"),array("tc.*"))
								   ->where("tc.Active = ?","1") ;	
		if($idTermsandconditions)	$lstrSelect			 ->where("tc.idTermsandconditions  = ?",$idTermsandconditions) ;			  	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);  
        return $larrResult;
     }
    public function fngetlobjTermsandconditionsDetailsSearch($Post) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tc" => "tbl_termsandconditions"),array("tc.*"))
								   ->where("tc.Active = ?","1") ;	
		if($Post['field2'])	$lstrSelect			 ->where("tc.Termname  LIKE '".$Post['field2']."%'") ;	
		$lstrSelect			 ->where("tc.Active  = ?",$Post['field9']) ;			  	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);  
        return $larrResult;
     }
	public function fnInserttermsandconditions($insertData) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_termsandconditions";
	   	$db->insert($table,$insertData);
	   	return $db->lastInsertId("tbl_termsandconditions","idTermsandconditions");	   
	} 
	function fnUpdatetermsandconditions($data,$idTermsandconditions){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		 
		$where['idTermsandconditions = ? ']= $idTermsandconditions;		
		return $db->update('tbl_termsandconditions', $data, $where);
	}
	
}