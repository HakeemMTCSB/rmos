<?php
class Studentfinance_Model_DbTable_RefundDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'refund_detail';
	protected $_primary = "rfdd_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('rd'=>$this->_name))
					->where("rd.rfdd_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function geRefundtData($refund_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('rd'=>$this->_name))
		->where("rd.rfdd_refund_id = ?", $refund_id);
	
		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
		
	}
		
}
?>