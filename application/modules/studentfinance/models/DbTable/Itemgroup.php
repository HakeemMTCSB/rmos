<?php
class Studentfinance_Model_DbTable_Itemgroup extends Zend_Db_Table { //Model Class for ItemGroup
	protected $_name = 'tbl_itemgroup';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	public function fnGetItemgroupDetails() { //function to get ItemGroup details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()	
		                         ->from(array("tbl_itemgroup" => "tbl_itemgroup"),array("tbl_itemgroup.*"))	
                                 ->join(array("tbl_bank" => "tbl_bank"),"tbl_itemgroup.IdBank=tbl_bank.IdBank",array("tbl_bank.*"))
		 				 		 ->where("tbl_itemgroup.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	} 
	  	
	public function fngetitemcode() {//function to get Itemcode 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()		 				 
					  ->from(array("tbl_itemgroup"=>"tbl_itemgroup"),array("key"=>"tbl_itemgroup.ItemCode","value"=>"tbl_itemgroup.ItemCode"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	public function fngetshortname(){ //function to get shortname
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("tbl_itemgroup"=>"tbl_itemgroup"),array("key"=>"tbl_itemgroup.ShortName","value"=>"tbl_itemgroup.ShortName"));
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}	
	public function fnSearchItemGroup($post = array()) { //Function for searching the Itemgroup details
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()	
			               ->from(array("tbl_itemgroup" => "tbl_itemgroup"),array("tbl_itemgroup.*"))	 				 
	                       ->join(array("tbl_bank" => "tbl_bank"),"tbl_itemgroup.IdBank=tbl_bank.IdBank",array("tbl_bank.*"));		
	                       			
			if(isset($post['field5']) && !empty($post['field5']) ){
					$lstrSelect = $lstrSelect->where("tbl_itemgroup.IdAccountGroup = ?",$post['field5']);							
			}	
			if(isset($post['field8']) && !empty($post['field8']) ){
					$lstrSelect = $lstrSelect->where("tbl_itemgroup.ShortName = ?",$post['field8']);							
			}		
			if(isset($post['field1']) && !empty($post['field1']) ){
					$lstrSelect = $lstrSelect->where("tbl_itemgroup.ItemCode = ?",$post['field1']);							
			}			
			if(isset($post['field19']) && !empty($post['field19']) ){
					$lstrSelect = $lstrSelect->where("tbl_itemgroup.IdBank = ?",$post['field19']);							
			}
						
	       	$lstrSelect	->where("tbl_itemgroup.Active = ".$post["field7"]);				
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}	
	public function fnInsertItemGroup($insertData) { //function to insert ItemGroup details		
	       	$db = Zend_Db_Table::getDefaultAdapter();
	       	$table = "tbl_itemgroup";
		    $db->insert($table,$insertData);   
		   // $lobjdb = Zend_Db_Table::getDefaultAdapter();
			return $db->lastInsertId();
		} 
	public function fnViewItemGroup($IdItem) { //function to view ItemGroup details
	    	$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('tbl_itemgroup' => 'tbl_itemgroup'),array('tbl_itemgroup.*'))
	                ->where('tbl_itemgroup.IdItem = '.$IdItem);
			$result = $db->fetchRow($select);	
			return $result;
	    }
	public function fnupdateItemGroupEDit($larrformData,$lintIdItem) { //function to update ItemGroup
    	unset($larrformData['Save']);
		$where = 'IdItem = '.$lintIdItem;
		$this->update($larrformData,$where);
	}
	
	//function to get BankId
	public function fngetbankiddetails() 
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()		 				 
									 ->from(array("tbl_bank"=>"tbl_bank"),array("key"=>"tbl_bank.IdBank","value"=>"tbl_bank.BankName"));				 		
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fngetaccountgroup(){		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()		 				 
									 ->from(array("tbl_accountgroup"=>"tbl_accountgroup"),array("key"=>"tbl_accountgroup.IdAccountGroup","value"=>"tbl_accountgroup.GroupName"));				 		
									
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;		
	}
	
	public function fngetaccountgrouptest(){		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()		 				 
									 ->from(array("tbl_accountgroup"=>"tbl_accountgroup"),array("tbl_accountgroup.*"));		 		
									
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;		
	}
	
	
	public function fngetbankaccounttype($idbank){ //function to get AccountType
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("tbl_bankdetails"=>"tbl_bankdetails"),array("key"=>"tbl_bankdetails.IdAccount","value"=>"tbl_bankdetails.AccountNumber"))
									 ->where("tbl_bankdetails.IdBank=?",$idbank);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}	 
	
	public function fngetIdAccountGroup(){ //function to get IdAccountGroup
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("tbl_accountgroup"=>"tbl_accountgroup"),array("key"=>"tbl_accountgroup.IdAccountGroup","value"=>"tbl_accountgroup.GroupName"));
									 //->join(array("tbl_definationtypems"=>"tbl_definationtypems"),"tbl_definationms.idDefType = tbl_definationtypems.idDefType AND defTypeDesc='Account Type'",array());
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fnGenerateCode($idUniversity,$IdInserted){		
		
			$page="generalbaseitem";					 
			//$result = 	$objIC->fnGetInitialConfigDetails($idUniversity);
			$db 	= 	Zend_Db_Table::getDefaultAdapter();			
			$select =   $db->select()
					->  from('tbl_config')
					->	where('idUniversity  = ?',$idUniversity);	
			$result = 	$db->fetchRow($select);	
			$sepr	=	$result['GeneralbaseitemSeparator'];
			$str	=	$page."Field";
			$strText=	$page."Text";
			for($i=1;$i<=4;$i++){
				$check = $result[$str.$i];
				$Text = $result[$strText.$i];
				switch ($check){
					case 'Year':
					  $code	= date('Y');
					  break;
					case 'Uniqueid':
					  $code	= $IdInserted;
					  break;						
					case 'University':
					  $code	= $idUniversity;
					  break;
					case 'Text':
					  $code	= $Text;
					  break;	
					default:
					  break;
				}
				if($i == 1) $accCode 	 =  $code;
				else 		$accCode	.=	$sepr.$code;
			}				
			return $accCode;	
		}	
	
 	public function fnupdateItemGroup($linitlastid,$larrformData) { //Function for updating the user    	
		$where = 'IdItem = '.$linitlastid;
		$this->update($larrformData,$where);
    }
	
	
}