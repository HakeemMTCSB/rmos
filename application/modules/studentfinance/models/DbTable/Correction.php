<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 5/10/2016
 * Time: 9:46 AM
 */
class Studentfinance_Model_DbTable_Correction extends Zend_Db_Table_Abstract {

    public function getInvoiceWrongSemesterStudent(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array(
                'a.id',
                'a.bill_number',
                'a.appl_id',
                'a.trans_id',
                'a.IdStudentRegistration',
                'c.IdScheme as IDSchemeProgram',
                'd.IdScheme as IDSchemeInvoice',
                'a.semester as SemesterInvoice',
                'e.IdSemesterMaster as SemesterProgram'
            ))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration', array())
            ->join(array('c'=>'tbl_program'), 'b.IdProgram = c.IdProgram', array())
            ->join(array('d'=>'tbl_semestermaster'), 'a.semester = d.IdSemesterMaster', array())
            ->join(array('e'=>'tbl_semestermaster'), 'd.AcademicYear = e.AcademicYear AND d.sem_seq = e.sem_seq AND e.IdScheme = c.IdScheme AND e.display = 1 AND e.special_semester = 0', array())
            ->where('c.IdScheme != d.IdScheme')
            ->where("a.status IN ('A', 'W')");

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoiceWrongSemesterApplicant(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array(
                'a.id',
                'a.bill_number',
                'a.appl_id',
                'a.trans_id',
                'a.IdStudentRegistration',
                'c.IdScheme as IDSchemeProgram',
                'd.IdScheme as IDSchemeInvoice',
                'a.semester as SemesterInvoice',
                'e.IdSemesterMaster as SemesterProgram'
            ))
            ->join(array('b'=>'applicant_program'), 'a.trans_id = b.ap_at_trans_id', array())
            ->join(array('c'=>'tbl_program'), 'b.ap_prog_id = c.IdProgram', array())
            ->join(array('d'=>'tbl_semestermaster'), 'a.semester = d.IdSemesterMaster', array())
            ->join(array('e'=>'tbl_semestermaster'), 'd.AcademicYear = e.AcademicYear AND d.sem_seq = e.sem_seq AND e.IdScheme = c.IdScheme AND e.display = 1 AND e.special_semester = 0', array())
            ->where('c.IdScheme != d.IdScheme')
            ->where("a.status IN ('A', 'W')")
            ->where("a.IdStudentRegistration IS NULL");

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateInvoice($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('invoice_main', $data, 'id = '.$id);
        return $update;
    }

    public function getDiscount(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount'), array(
                'd.rcp_inv_id',
                'a.dcnt_IdStudentRegistration',
                'a.dcnt_fomulir_id',
                'd.rcp_inv_rcp_no',
                'b.dcnt_amount',
                'c.amount',
                'd.rcp_inv_amount',
                'e.invoice_date',
                'e.currency_id',
                'f.rcp_cur_id'
            ))
            ->join(array('b'=>'discount_detail'), 'a.dcnt_id = b.dcnt_id', array())
            ->join(array('c'=>'invoice_detail'), 'b.dcnt_invoice_det_id = c.id', array())
            ->join(array('e'=>'invoice_main'), 'c.invoice_main_id = e.id', array())
            ->join(array('d'=>'receipt_invoice'), 'c.id = d.rcp_inv_invoice_dtl_id', array())
            ->join(array('f'=>'receipt'), 'd.rcp_inv_rcp_id = f.rcp_id', array())
            ->where('a.dcnt_type_id = ?', 37)
            ->where('a.dcnt_status = ?', 'A')
            ->where('a.dcnt_approve_date < "2016-09-28"');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateReceiptInvoice($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('receipt_invoice', $data, 'rcp_inv_id = '.$id);
        return $update;
    }

    public function getStudentIB2002(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->where('a.IdSemesterMain = ?', 42)
            ->where('a.IdSubject = ?', 95)
            ->where("b.registrationId IN ('1100333','1100083','1200166','1100252','1200174','1200213','1100380','1200219','1100148','1200123','1200107','1000005','1100400','1200096','1200121','1300423','1200157','1200183','1100334','1100257','1000390','1100368','1200175','1200167','1200127','1200269','1200168','1300135','1100241','1100297','1200014','1200270','1000378','1200165','1300145','1200138','0800989','0700269','1200126','1100364','0900201','1200044','1200443','1200033','1200024','1200036','1100004','1200348','0600117','1200133','0700554','1200145','1100355','0700377','1200022','1200338','1200118','0900303','1100178','1100401','1200146','0700613','1300038','1200092','1200148','1200076','1100316','1300036','1100353')");

        $result = $db->fetchAll($select);
        return $result;
    }

    public function deleteStudentRegSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_studentregsubjects', 'IdStudentRegSubjects = '.$id);
        return $delete;
    }

    public function getCanceledEbipDiscount(){
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectAdj = $db->select()
            ->from('discount_adjustment', array('IdDiscount'));

        $select = $db->select()
            ->from(array('a'=>'discount_detail'), array('value'=>'*', 'discount_amount'=>'a.dcnt_amount'))
            ->join(array('b'=>'discount'), 'a.dcnt_id = b.dcnt_id')
            ->where('a.status = "X" OR b.dcnt_status = "X"')
            ->where('b.dcnt_type_id = ?', 37)
            ->where('a.id NOT IN (?)', $selectAdj);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getStudentToUpdateGradePoint(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            //->where('a.IdIntake = ?', 67)
            ->where('a.IdProgram = ?', 2);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubjectToUpdateGradePoint($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->order('a.IdSemesterMain');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getItemRegisterOnlineStudent(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->join(array('b'=>'tbl_program_scheme'), 'a.IdProgramScheme = b.IdProgramScheme')
            ->join(array('c'=>'tbl_studentregsubjects'), 'a.IdStudentRegistration = c.IdStudentRegistration')
            ->join(array('d'=>'tbl_studentregsubjects_detail'), 'c.IdStudentRegSubjects = d.regsub_id')
            ->where('b.mode_of_program = ?', 577)
            ->where('d.item_id = ?', 890)
            ->group('a.IdStudentRegistration')
            ->group('c.IdSubject')
            ->having('count(*) > 1')
            ->order('a.IdStudentRegistration ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getItemRegisterList($id, $subid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->join(array('b'=>'tbl_studentregsubjects_detail'), 'a.IdStudentRegSubjects = b.regsub_id')
            ->join(array('c'=>'tbl_semestermaster'), 'a.IdSemesterMain = c.IdSemesterMaster')
            ->where('b.item_id = ?', 890)
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.IdSubject = ?', $subid)
            ->order('c.SemesterMainStartDate ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSponsorshipList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(
                array('a' => 'sponsor_invoice_receipt_student'), array(
                    'id' => 'a.rcp_inv_id',
                    'record_date' => 'b.rcp_receive_date',
                    'description' => 'b.rcp_description',
                    'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'a.rcp_inv_amount',
                    'document' => 'b.rcp_no',
                    'invoice_no' => 'b.rcp_no',
                    'receipt_no' => 'b.rcp_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'b.rcp_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('d' => 'tbl_studentregistration'), 'a.IdStudentRegistration = d.IdStudentRegistration', array('d.registrationId'))
            ->join(array('e' => 'student_profile'), 'd.sp_id = e.id', array('studentname'=>'concat(e.appl_fname, " ", e.appl_lname)'))
            ->join(array('f' => 'tbl_program'), 'd.IdProgram = f.IdProgram', array('f.IdProgram', 'f.ProgramName', 'f.ProgramCode'))
            ->join(array('g' => 'tbl_definationms'), 'd.profileStatus = g.IdDefinition', array('student_status'=>'g.DefinitionDesc'))
            ->join(array('h' => 'tbl_program_scheme'), 'd.IdProgramScheme = h.IdProgramScheme', array('h.IdProgramScheme'))
            ->join(array('i' => 'tbl_definationms'), 'h.mode_of_program = i.IdDefinition', array('mop'=>'i.DefinitionDesc'))
            ->join(array('j' => 'tbl_definationms'), 'h.mode_of_study = j.IdDefinition', array('mos'=>'j.DefinitionDesc'))
            ->join(array('k' => 'tbl_definationms'), 'h.program_type = k.IdDefinition', array('pt'=>'k.DefinitionDesc'))
            ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
            ->where("b.rcp_status = 'APPROVE'");

        $result = $db->fetchAll($select);
        return $result;
    }
}