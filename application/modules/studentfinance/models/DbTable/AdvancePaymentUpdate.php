<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 30/9/2016
 * Time: 11:29 AM
 */
class Studentfinance_Model_DbTable_AdvancePaymentUpdate extends Zend_Db_Table_Abstract {

    public function getAdvancePaymentUtilized($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment_detail'))
            ->join(array('b'=>'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id')
            ->join(array('c'=>'invoice_main'), 'a.advpydet_inv_id = c.id')
            ->join(array('d'=>'tbl_studentregistration'), 'c.IdStudentRegistration = d.IdStudentRegistration')
            ->join(array('e'=>'student_profile'), 'd.sp_id = e.id')
            ->join(array('f'=>'tbl_program'), 'd.IdProgram = f.IdProgram')
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A');

        if ($search != false){
            if (isset($search['name']) && $search['name']!=''){
                $select->where($db->quoteInto('CONCAT_WS(" ", e.appl_fname, e.appl_lname) like ?', '%'.$search['name'].'%'));
            }

            if (isset($search['id']) && $search['id']!=''){
                $select->where($db->quoteInto('d.registrationId like ?', '%'.$search['id'].'%'));
            }

            if (isset($search['adv_no']) && $search['adv_no']!=''){
                $select->where($db->quoteInto('b.advpy_fomulir like ?', '%'.$search['adv_no'].'%'));
            }

            if (isset($search['uti_date']) && $search['uti_date']!=''){
                $select->where($db->quoteInto('b.advpydet_upddate = ?', date('Y-m-d', strtotime($search['uti_date']))));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateAdvancePayment($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('advance_payment_detail', $data, 'advpydet_id = '.$id);
        return $update;
    }
}