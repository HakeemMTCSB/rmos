<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 8/3/2017
 * Time: 10:15 AM
 */
class Studentfinance_Model_DbTable_AdvancePaymentGainLoss extends Zend_Db_Table_Abstract {

    public function getAdvancePaymentList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment_detail'), array('a.advpydet_total_paid'))
            ->join(array('b'=>'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id', array('b.advpy_fomulir', 'b.advpy_description'))
            ->joinLeft(array('c'=>'invoice_main'), 'b.advpy_invoice_id = c.id', array('dateinv1'=>'c.invoice_date'))
            ->joinLeft(array('d'=>'receipt'), 'b.advpy_rcp_id = d.rcp_id', array('datercp'=>'d.rcp_receive_date'))
            ->join(array('e'=>'invoice_main'), 'a.advpydet_inv_id = e.id', array('dateinv2'=>'e.invoice_date', 'noinv2'=>'e.bill_number'))
            ->join(array('f'=>'tbl_studentregistration'), 'b.advpy_idStudentRegistration = f.IdStudentRegistration', array('f.IdStudentRegistration', 'f.registrationId'))
            ->join(array('g'=>'student_profile'), 'f.sp_id = g.id', array('studentName'=>'concat(g.appl_fname, " ", g.appl_lname)'))
            ->join(array('h'=>'tbl_program'), 'f.IdProgram = h.IdProgram', array('h.ProgramName'))
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('b.advpy_cur_id = ?', 2)
            ->where('e.currency_id = ?', 2)
            ->where('b.MigrateCode = ?', '')
            ->where('a.advpydet_total_paid != ?', 0.00);

        $result = $db->fetchAll($select);
        return $result;
    }
}