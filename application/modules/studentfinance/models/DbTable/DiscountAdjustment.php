<?php
class Studentfinance_Model_DbTable_DiscountAdjustment extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'discount_adjustment';
	protected $_primary = "id";
		
	/*public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
					->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
		
		if($id!=0){
			$selectData->where("cn.cn_id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}*/
	
	public function getStudentData($id,$idStudent){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'discount_detail'))
					->join(array('bc' => 'discount'), 'd.dcnt_id = bc.dcnt_id')
					->join(array('b' => 'invoice_main'), 'b.id = d.dcnt_invoice_id',array('bill_number','bill_amount','bill_balance','dn_amount_main'=>'b.dn_amount'))
			->join(array('c' => 'invoice_detail'), 'c.id = d.dcnt_invoice_det_id and c.invoice_main_id = b.id',array('amount','balance','dn_amount','fee_item_description'))
					->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id = b.id and ivs.invoice_detail_id = c.id',array('subject_id'))
					->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = ivs.subject_id')
					->join(array('cr'=>'tbl_currency'), 'b.currency_id = cr.cur_id',array('cur_code'))
					->where('bc.dcnt_batch_id =?',$id)
					->where('bc.dcnt_IdStudentRegistration =?',$idStudent)
					;
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'discount_detail'),array('*','dcnt_amount_detail'=>'dcnt_amount','dcnt_id_detail'=>'id'))
					->join(array('bc' => 'discount'), 'd.dcnt_id = bc.dcnt_id')
					->join(array('b' => 'invoice_main'), 'b.id = d.dcnt_invoice_id',array('bill_number','bill_amount','bill_balance','dn_amount_main'=>'b.dn_amount'))
			->join(array('c' => 'invoice_detail'), 'c.id = d.dcnt_invoice_det_id and c.invoice_main_id = b.id',array('amount','balance','dn_amount','fee_item_description'))
					->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id = b.id and ivs.invoice_detail_id = c.id',array('subject_id'))
					->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = ivs.subject_id')
					->join(array('cr'=>'tbl_currency'), 'b.currency_id = cr.cur_id',array('cur_code'))
					->joinLeft(array('st' => 'applicant_transaction'), 'st.at_trans_id=bc.dcnt_txn_id', array('st.at_pes_id'))
					->joinLeft(array('sa' => 'applicant_profile'), 'sa.appl_id=st.at_appl_id', array('CONCAT_WS(" ",sa.appl_fname,sa.appl_mname,sa.appl_lname) as applicant_name'))
					->joinLeft(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration=bc.dcnt_IdStudentRegistration', array('sr.registrationId'))
					->joinLeft(array('sp' => 'student_profile'), 'sr.sp_id=sp.id', array('CONCAT_WS(" ", sp.appl_fname, sp.appl_mname, sp.appl_lname) as student_name'))
					
					->where('bc.dcnt_id =?',$id)
					;
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getDiscountData($searchData=null){
		
		$dateFrom = date('Y-m-d', strtotime($searchData['date_from']));
		$dateTo = date('Y-m-d', strtotime($searchData['date_to']));
		$invoice_id = $searchData['invoice_id'];
		$inv_Detail = $searchData['invoice_detail_id'];
		
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('dac'=>'discount_adjustment'),array('*','dnaamount'=>'amount'))
					->join(array('d'=>'discount_detail'),'d.id = dac.IdDiscount')
					->join(array('da'=>'discount'),'da.dcnt_id = d.dcnt_id')
					->joinLeft(array('a'=>'invoice_main'),'a.id = d.dcnt_invoice_id')
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = a.currency_id')
					->where("dac.Status = 'APPROVE'")
					->where('d.dcnt_invoice_id = ?', $invoice_id)
					->where('d.dcnt_invoice_det_id = ?', $inv_Detail)
					->where("DATE(dac.EnterDate) >= ?",$dateFrom)
					->where("DATE(dac.EnterDate) <= ?",$dateTo);
		
		$row = $db->fetchRow($selectData);

		return $row;
	}
	
	public function getDataDetail($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'discount_detail'))
					->where('d.id =?',$id)
					;
					
		$row = $db->fetchRow($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	
}

