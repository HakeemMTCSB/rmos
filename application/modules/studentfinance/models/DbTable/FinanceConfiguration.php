<?php 

class Studentfinance_Model_DbTable_FinanceConfiguration extends Zend_Db_Table_Abstract {
	
	protected $_name = 'finance_configuration';
	protected $_history = 'finance_configuration_history';
	protected $_primary = "fin_id";
	
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}	
	
	public function addData($data) {
		$db = $this->db;		
		$this->insert($data);
		return $db->lastInsertId();
	}
	
	public function addDataHistory($data,$id) {		
		$db = $this->db;
		$data['fin_id']=$id;
		$db->insert($this->_history, $data );
		
		return $db->lastInsertId();

	}
	
	public function getData(){
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('ec'=>$this->_name))	;				 
					 	
		//echo $select;
		$row = $db->fetchRow($select);
		return $row;
	}
	
	
	
	
	
}

?>