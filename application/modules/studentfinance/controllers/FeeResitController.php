<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_FeeResitController extends Base_Base {
	
	
	public function init(){
		
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Resit Exam Charges - Program List");
    	
    	//faculty
    	$facultyDb = new App_Model_General_DbTable_Collegemaster();
    	$faculty_list = $facultyDb->getData();
    	
    	//get program
    	$programDb = new App_Model_Record_DbTable_Program();
    	foreach ($faculty_list as $index=>$faculty){
    		$programList = $programDb->searchProgramByFaculty($faculty['IdCollege']);
    		
    		if($programList){
    			$faculty_list[$index]['program_list'] = $programList;
    		}
    	} 	
    	
    	$this->view->faculty_list = $faculty_list;
    	
	}
	
	public function courseResitFeeAction(){
		
		$program_id = $this->_getParam('pid', 0);
		$this->view->pid = $program_id;
		
		//title
		$this->view->title= $this->view->translate("Resit Exam Charges - Course Resit Fee");
		
		//program
		$programDb = new App_Model_Record_DbTable_Program();
		$program = $programDb->getData($program_id);
		
		$this->view->program = $program;
		
		//faculty
		$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDb->getData($program['IdCollege']);
		$this->view->faculty = $faculty;
				
		//courses
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//semester landscape
		$select1 = $db ->select()
			->from(array('a'=>'tbl_landscape'),array())
			->join(array('b'=>'tbl_landscapesubject'), 'b.IdLandscape = a.IdLandscape', array('distinct(b.IdSubject)'))
			->join(array('c'=>'tbl_subjectmaster'), 'c.IdSubject = b.IdSubject')
			->where('a.IdProgram = ?', $program_id);
			//->order('c.SubCode');

		
		//block landscape
		$select2 = $db ->select()
		->from(array('a'=>'tbl_landscape'),array())
		->join(array('b'=>'tbl_landscapeblocksubject'), 'b.IdLandscape = a.IdLandscape', array('distinct(b.subjectid)'))
		->join(array('c'=>'tbl_subjectmaster'), 'c.IdSubject = b.subjectid')
		->where('a.IdProgram = ?', $program_id);
		//->order('c.SubCode');
		
		$select = $db->select()
		->union(array($select1, $select2))
		->group('SubCode');
		

		$stmt = $db->query($select);
		$row = $stmt->fetchAll();
		
		//get resit fee
		$resitFeeDb = new Studentfinance_Model_DbTable_FeeResit();
		foreach ($row as $index=>$course){
			$resitData = $resitFeeDb->getLatestResitData($program_id, $course['IdSubject']);
			if($resitData){
				$row[$index]['resit_fee'] = $resitData['amount'];
				$row[$index]['resit_effective_date'] = $resitData['effective_date'];
			}else{
				$row[$index]['resit_fee'] = 0.00;
			}
		}
		
		$this->view->courses = $row;
				
	}
	
	public function changeCourseFeeAction(){
		
		$this->_helper->layout->disableLayout();
		
		
		$program_id = $this->_getParam('pid', 0);
		$course_id = $this->_getParam('cid', 0);
		
		$form = new Studentfinance_Form_CourseResitFee(array('pid'=>$program_id, 'cid'=>$course_id));
		
		$form->removeElement('save');
		$form->removeElement('cancel');
		$form->removeDisplayGroup('buttons');
		
		$result = array(
			'status'=>0
		);
		
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				$resitFeeDb = new Studentfinance_Model_DbTable_FeeResit();
				
				$data = array(
					'program_id' => $formData['program_id'],
					'subject_id' => $formData['course_id'],
					'amount'=>$formData['fee'],
					'effective_date'=>date('Y-m-d', strtotime($formData['effective_date']))
				);
				
				$resitFeeDb->insert($data);
				
				$result['status'] = 2;
				$result['item']['fee'] = $formData['fee'];
				$result['item']['effective_date'] = $formData['effective_date'];
				
			}else{
				$form->populate($formData);
				$result['status'] = 1;
				$result['form'] = $form->render();
			}
			
		}else{
			$result['form'] = $form->render();
		}
		
		echo Zend_Json::encode($result);
		
		exit;
	}
	
	function courseResitFeeHistoryAction(){
		
		$this->_helper->layout->disableLayout();
		
		$program_id = $this->_getParam('pid', 0);
		$course_id = $this->_getParam('cid', 0);
		
		$resitFeeDb = new Studentfinance_Model_DbTable_FeeResit();
		$resitFeeHistory = $resitFeeDb->getResitDataHistory($program_id, $course_id);
		$this->view->history = $resitFeeHistory;
		
	}
	
}

