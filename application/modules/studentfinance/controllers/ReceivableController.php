<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_ReceivableController extends Base_Base {
	
	public function init(){

	}

	public function indexAction() {
		
	}
	
	public function paymentModeAction(){
		
		//title
		$this->view->title= $this->view->translate("Payment Mode");
		
		$paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
		$this->view->payment_mode_list = $paymentModeDb->getData();
				
	}
	
	public function paymentModeAddAction(){
		//title
		$this->view->title= $this->view->translate("Payment Mode - Add");
		
		$form = new Studentfinance_Form_PaymentMode();
		 
		$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'payment-mode'),'default',true)."'; return false;";
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			if ($form->isValid($formData)) {
				
				unset($formData['save']);
		
				$paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
				$code = $paymentModeDb->insert($formData);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new payment mode');
		
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'payment-mode'),'default',true));
			}else{
				$form->populate($formData);
			}
			 
		}
		 
		$this->view->form = $form;
	}
	
	public function paymentModeEditAction(){
		
		$id = $this->getParam('id', null);
		
		if(!$id){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'payment-mode'),'default',true));
		}
		
		//title
		$this->view->title= $this->view->translate("Payment Mode - Edit");
				
		$form = new Studentfinance_Form_PaymentMode();
			
		$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'payment-mode'),'default',true)."'; return false;";
		
		$paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				unset($formData['save']);
		
				$result = $paymentModeDb->update($formData,'id = '.$id);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success edit payment mode');
		
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'payment-mode'),'default',true));
			}else{
				$form->populate($formData);
			}
		
		}else{
			$data = $paymentModeDb->fetchRow('id = '.$id);
			$form->populate($data->toArray());
		}
			
		$this->view->form = $form;
	}
	
	
	public function creditcardTerminalAction(){
	
		//title
		$this->view->title= $this->view->translate("Credit Card Terminal");
	
		$creditcardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
		$this->view->creditcard_terminal_list = $creditcardTerminalDb->getData();
	
	}
	
	public function creditcardTerminalAddAction(){
		
		//title
		$this->view->title= $this->view->translate("Credit Card Terminal - Add");
		
		$form = new Studentfinance_Form_CreditcardTerminal();
			
		$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'creditcard-terminal'),'default',true)."'; return false;";
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				unset($formData['save']);
		
				$creditcardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
				
				$code = $creditcardTerminalDb->insert($formData);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new creditcard terminal');
		
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'creditcard-terminal'),'default',true));
			}else{
				$form->populate($formData);
			}
		
		}
			
		$this->view->form = $form;
		
	}
	
	
	public function creditcardTerminalEditAction(){
		
		$id = $this->getParam('id', null);
		
		if(!$id){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'creditcard-terminal'),'default',true));
		}
		
		//title
		$this->view->title= $this->view->translate("Credit Card Terminal - Edit");
		
		$form = new Studentfinance_Form_CreditcardTerminal();
			
		$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'creditcard-terminal'),'default',true)."'; return false;";
		
		$creditcardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				unset($formData['save']);
				
				//check for default
				if($formData['default']==1){
					$creditcardTerminalDb->update(array('default'=>0));
				}
		
				$code = $creditcardTerminalDb->update($formData,'id = '.$id);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success edit creditcard terminal');
		
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'receivable', 'action'=>'creditcard-terminal'),'default',true));
			}else{
				$form->populate($formData);
			}
		
		}else{
			$data = $creditcardTerminalDb->fetchRow('id = '.(int)$id);
			$form->populate($data->toArray());
		}
			
		$this->view->form = $form;
		
	}
}