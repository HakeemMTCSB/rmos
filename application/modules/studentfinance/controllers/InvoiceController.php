<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_InvoiceController extends Base_Base {
	
	private $_DbObj;
	private $lobjdeftype;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_InvoiceMain();
		$this->_DbObj = $db;
		$this->invoiceDB = new Studentfinance_Model_DbTable_InvoiceMain();
		$this->invoicedetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
		$this->lobjdeftype = new App_Model_Definitiontype();
		
	}
	
	public function indexAction() {
		
		//title
    	$this->view->title= $this->view->translate("Invoice");
    	
    	//get fee category
    	$feeCategoryDB = new Studentfinance_Model_DbTable_FeeCategory();
    	$this->view->fee_category = $feeCategoryDB->getListCategory();
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			
			$formData = $this->getRequest()->getPost();
			
			$p_data = $this->invoiceDB->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
	
	    	$this->view->paginator = $paginator;
			$this->view->formData = $formData;
			
			$this->view->type = $formData['type'];

		}
		
	}
	
	public function applicantAction(){
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Invoice : Applicant Detail");
    	
    	//applicant info
    	$applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
    	$this->view->profile = $applicantProfileDb->getData($id);
	}
	
	public function applicantInvoiceAction(){
		$this->_helper->layout()->disableLayout();
		
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
						->from(array('im'=>'invoice_main'),array(
							'id'=>'im.id',
							'record_date'=>'im.date_create',
							'description' => 'im.bill_description',
							'program_code' => 'im.program_code',
							'txn_type' => new Zend_Db_Expr ('"Invoice"'),
							'debit' =>'bill_amount',
							'invoice_no' => 'bill_number',
							'no_fomulir' => 'no_fomulir',
							'paid' => 'bill_paid',
							'cn' => 'cn_amount',
							'balance' => 'bill_balance',
							'status' => 'status',
							)
						)
						->where('im.appl_id = ?', $appl_id)
						->where('im.IdStudentRegistration is null')
						->order('im.date_create')
						->order('im.id');
						
		$row = $db->fetchAll($select);
		
		if(!$row){
			$row = null;
		}else{
			//get payment status
			$paymentDb = new Studentfinance_Model_DbTable_PaymentMain();
			
			foreach ($row as $index=>$data){
				
				$row[$index]['payment'] = $paymentDb->getInvoicePaymentRecord($data['invoice_no'], $data['no_fomulir']); 
			}
		}
				
		$this->view->invoice = $row;
		
	}
	
	public function applicantProgrammeOfferedAction(){
		$this->_helper->layout()->disableLayout();
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('a'=>'applicant_transaction'))
			    ->joinLeft(array('b'=>'tbl_academic_year'),'b.ay_id = a.at_academic_year')
			    ->joinLeft(array('c'=>'tbl_academic_period'),'c.ap_id = a.at_period')
			    ->joinLeft(array('d'=>'tbl_intake'),'d.IdIntake = a.at_intake')
				->where('a.at_appl_id = '. $appl_id)
				->where("a.at_status not in ('APPLY','CLOSE','PROCESS','REJECT')")
				->order('a.at_trans_id');
				
		$row = $db->fetchAll($select);
		
		if(!$row){
			$row = null;
		}else{
			
			//get offered programme
			$applicantProgrammeDb =  new App_Model_Application_DbTable_ApplicantProgram();
			
			//get assessment info
			$applicantAssessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
			$applicantAssessmentUSMDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
			
			foreach ($row as $index=>$data){
				
				//check for proforma invoice availbility
				$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
				$proforma = $proformaInvoiceDb->getTxnData($data['at_trans_id']!=0?$data['at_trans_id']:"-1");
				
				if($proforma){
					$row[$index]['proforma'] = $proforma;
				}
				
				
				$row[$index]['program_offered'] =  $applicantProgrammeDb->getOfferProgram($data['at_trans_id'],$data['at_appl_type']);
				
				if($data['at_appl_type']==1){
					$data = $applicantAssessmentUSMDb->getData($data['at_trans_id']);
					
					$row[$index]['assessment']['registration_date_start'] = $data['aaud_reg_start_date'];
					$row[$index]['assessment']['registration_date_end'] = $data['aaud_reg_end_date'];
					$row[$index]['assessment']['nomor'] = $data['aaud_nomor'];
					$row[$index]['assessment']['rank'] = $data['aau_rector_ranking'];
				}else
				if($data['at_appl_type']==2){
					$data = $applicantAssessmentDb->getData($data['at_trans_id']);
					
					$row[$index]['assessment']['registration_date_start'] = $data['aar_reg_start_date'];
					$row[$index]['assessment']['registration_date_end'] = $data['aar_reg_end_date'];
					$row[$index]['assessment']['nomor'] = $data['asd_nomor'];
					$row[$index]['assessment']['rank'] = $data['aar_rating_rector'];
				}
				
				
				
			}
		}
		
		/*echo "<pre>";
		print_r($row);
		echo "</pre>";
		exit;*/
		$this->view->data = $row;
	}

	public function applicantGenerateInvoiceAction(){
		$this->_helper->layout()->disableLayout();
		
		$this->view->title = "Generate Applicant Invoice";
		
		$txn_id = $this->_getParam('txn', null);
		$this->view->txn_id = $txn_id;
		
			
		//transaction data
		$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $txnDb->getTransactionData($txn_id);
		$this->view->txn_data = $txnData;
		
		//get offered programme
		$applicantProgrammeDb =  new App_Model_Application_DbTable_ApplicantProgram();
		$program_offer =  $applicantProgrammeDb->getOfferProgram($txnData['at_trans_id'],$txnData['at_appl_type']);
		
		//get assessment info
		$applicantAssessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
		$applicantAssessmentUSMDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
		
		if($txnData['at_appl_type']==1){
			$assessment_data = $applicantAssessmentUSMDb->getData($txnData['at_trans_id']);
			$rank = $assessment_data['aau_rector_ranking'];
		}else
		if($txnData['at_appl_type']==2){
			$assessment_data = $applicantAssessmentDb->getData($txnData['at_trans_id']);
			$rank = $assessment_data['aar_rating_rector'];
		}
		
		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoiceMainDb->generateApplicantInvoice($txnData['at_pes_id'], $formData['billing']);
		
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'applicant','id'=>$txnData['at_appl_id']),'default',true));
			
		}else{
		
			//get fee structure
			$feestructureDb = new Studentfinance_Model_DbTable_FeeStructure();
			$fee_structure = $feestructureDb->getApplicantFeeStructure($txnData['at_intake'], $program_offer['program_id'] );
			
			//get payment plan
			$paymentPlanDb = new Studentfinance_Model_DbTable_FeeStructurePlan();
			$payment_plan = $paymentPlanDb->getStructureData($fee_structure['fs_id']);
			
			//get payment plan detail
			$paymentPlanDetailDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();
			foreach ($payment_plan as $index=>$plan){
				//loop each installment
				for($installment=1; $installment<=$plan['fsp_bil_installment']; $installment++){
					$payment_plan[$index]['plan_detail'] = $paymentPlanDetailDb->getPlanData($plan['fsp_structure_id'],$plan['fsp_id'],$installment, 1, $program_offer['program_id'], $rank);
				}
			}
			
			$this->view->payment_plan = $payment_plan;
		}
		
		/*echo "<pre>";
		print_r($payment_plan);
		echo "</pre>";*/
	}
	
	public function applicantProformaInvoiceAction(){
		$this->_helper->layout()->disableLayout();
		
		$txn_id = $this->_getParam('txn', null);
		$this->view->txn_id = $txn_id;
		
		$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
		$this->view->data = $proformaInvoiceDb->getTxnData($txn_id);
	}
	
	public function applicantCancelInvoiceAction(){
		$invoice_id = $this->_getParam('id', null);
		$appl_id = $this->_getParam('appl_id', null);
		
		$auth = Zend_Auth::getInstance();
		
		//update invoice
		$data=array(
			'status'=>'X',
			'cancel_by' => $auth->getIdentity()->iduser,
			'cancel_date' => date('Y-m-d H:i:s')
		);
			
		$this->_DbObj->update($data, 'id = '.$invoice_id);
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'applicant','id'=>$appl_id),'default',true));
	}
	
	public function applicantDeleteInvoiceAction(){
		$invoice_id = $this->_getParam('id', null);
		$appl_id = $this->_getParam('appl_id', null);
	
		$auth = Zend_Auth::getInstance();
	
		//get invoice main data
		$invoice = $this->_DbObj->getData($invoice_id);
		
		//add invoice delete history
		$invoiceDeleteHistoryDb = new Studentfinance_Model_DbTable_InvoiceDeleteHistory();
		$invoice['invoice_main_id'] = $invoice['id'];
		$invoice['id'] = null;
		$invoice['delete_by'] = $auth->getIdentity()->iduser;
		$invoice['delete_date'] = date('Y-m-d H:i:s');
		
		$invoiceDeleteHistoryDb->insert($invoice);
		
		//delete
		$this->_DbObj->delete('id = '.$invoice_id);
	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'applicant','id'=>$appl_id),'default',true));
	}
	
	public function applicantPaymentPlanAction(){
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		
		$this->_helper->layout()->disableLayout();
		
		$paymentPlanDb = new Studentfinance_Model_DbTable_PaymentPlan();
		
		$data = $paymentPlanDb->getApplicantPaymentPlan($appl_id);
		$this->view->data = $data;		
		
	}
	
	public function invoicePaymentPlanAction(){
		$appl_id = $this->_getParam('appl_id', null);
		
		$msg = $this->_getParam('msg', null);
		$this->view->noticeMessage = $msg;
		
		$step = $this->_getParam('step', 1);
		$this->view->step = $step;
		
		$this->view->title = "Create Applicant Payment Plan";
		
		//set custom session
		$ses_payment_plan = new Zend_Session_Namespace('payment_plan');
		
		if($appl_id){
			Zend_Session::namespaceUnset('payment_plan');
			
			$ses_payment_plan = new Zend_Session_Namespace('payment_plan');
			$ses_payment_plan->appl_id = $appl_id;
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>1),'default',true));
		}
		
		//applicant info
    	$applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
    	$this->view->profile = $applicantProfileDb->getData($ses_payment_plan->appl_id);
		
		if( isset($ses_payment_plan->appl_id) ){
			if($step==1){
				
				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					
					if( isset($formData['invoices'])){
						$ses_payment_plan->invoice = $formData['invoices'];
						$ses_payment_plan->invoice_knockoff = null;
						$ses_payment_plan->distribution = null;
					}else{
						$ses_payment_plan->invoice = null;
						$ses_payment_plan->invoice_knockoff = null;
						$ses_payment_plan->distribution = null;
					}
					
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>2),'default',true));
					
				}else{
					//active invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceList = $invoiceMainDb->getApplicantInvoiceData($ses_payment_plan->appl_id,true);
										
					if($invoiceList){
						
						//remove paid invoice
						foreach ($invoiceList as $index=>$invoice){
							
							if($invoice['bill_balance']==0 && $invoice['bill_balance']!=null){
								unset($invoiceList[$index]);
							}
						}
						$invoiceList = array_values($invoiceList);
						
						//remove invoice with credit note
						$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
						foreach ($invoiceList as $index=>$invoice){
							if($creditNoteDb->getDataByInvoice($invoice['bill_number'])){
								unset($invoiceList[$index]);
							}
						}
						$invoiceList = array_values($invoiceList);
						
					}
						
					if(isset($ses_payment_plan->invoice)){
					
						//set checked from data session
					
						
						foreach ($invoiceList as $index=>$invoice){
							
							if(in_array($invoice['id'],array_values($ses_payment_plan->invoice))){
								$invoiceList[$index]['selected'] = true;
							}else{
								$invoiceList[$index]['selected'] = false;
							}
						}
					}
					
					$this->view->invoice_list = $invoiceList;
					
					

				}
				
				
			}else
			if($step==2){
				
				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					
					if( is_numeric($formData['installment']) && $formData['installment'] > 0 ){
						
						//reset distribution if different amount
						if( $ses_payment_plan->installment !=  $formData['installment'] ){
							$ses_payment_plan->distribution = null;	
						}
						
						$ses_payment_plan->installment = $formData['installment'];
						
					}else{
						$ses_payment_plan->installment = null;
						$ses_payment_plan->distribution = null;
					}
					
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>4),'default',true));
				}else{
					if(!isset($ses_payment_plan->invoice)){
						$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>1, 'msg'=>'Please Complete step 1 process'),'default',true));
					}
						
					if(isset($ses_payment_plan->installment)){
						$this->view->installment = $ses_payment_plan->installment;
					}
					
				}
				
			}else
			if($step==3){
				
			//redirect 
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>4),'default',true));
			exit;

				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					
					if( isset($formData['invoice']) ){
						$invoice_arr = array();
						foreach ($formData['invoice'] as $index => $invoice){
							$invoice_arr[] = array(
								'invoice_no'=>$invoice,
								'adv_amt'=> $formData['invoice_adv_pymt_amt'][$index]
							);
						}
						
						$ses_payment_plan->invoice_knockoff = $invoice_arr;
						$ses_payment_plan->distribution = null;
					}else{
						$ses_payment_plan->invoice_knockoff = null;
					}
					
					
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>4),'default',true));
				}else{
					
					if( isset($ses_payment_plan->installment) && isset($ses_payment_plan->invoice) ){
						
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
						$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
						
						$invoices = array();
						
						//get fee item setup
						$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
						$feeItemList = $feeItemDb->getData();
						
						//loop each invoice						
						foreach ($ses_payment_plan->invoice as $index=>$invoice_id){
							//get selected invoice data
							$invoices[$index] = $invoiceMainDb->getData($invoice_id);
							
							//get invoice detail
							$invoices[$index]['invoice_detail'] = $invoiceDetailDb->getInvoiceDetail($invoice_id);
							
							
						
							//sum amount to fee item
							foreach ($invoices[$index]['invoice_detail'] as $inv_dtl){
								
								foreach($feeItemList as $feeItemIndex => $feeItem){
									
									if($feeItem['fi_id']==$inv_dtl['fi_id']){
										if(isset($feeItemList[$feeItemIndex]['total_amount'])){
											$feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['amount'];
										}else{
											$feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['amount'];
										}
										break;	
									}
								}
							}
						}
						
						$this->view->invoices = $invoices;
						$this->view->fee_item = $feeItemList;
						
						//get advance payment
						$advPmntDb = new Studentfinance_Model_DbTable_AdvancePayment();
						$advancePaymentList = $advPmntDb->getApplicantBalanceAvdPayment($ses_payment_plan->appl_id);
						
						$this->view->advancePaymentList = $advancePaymentList;
						
						$advance_payment_balance = 0;
						if($advancePaymentList){
							foreach ($advancePaymentList as $avd_payment){
								$advance_payment_balance += $avd_payment['advpy_total_balance'];
							}
						}
						
						$this->view->advance_payment_balance = $advance_payment_balance;
						
						if( isset($ses_payment_plan->invoice_knockoff) ){
							$this->view->invoice_knockoff = $ses_payment_plan->invoice_knockoff;
						}
						

					}else{
						$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>2, 'msg'=>'Please Complete step 2 process'),'default',true));
					}
					
				}
				
			}else
			if($step==4){
				
				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					
					$fi_loop_index = 0;
					$installment = array();
					
					for($i=0; $i<sizeof($formData['installment']); $i++){
						$feeItem = array();			
									
						//fee item loop based in size
						for($fi_cnt = 1; $fi_cnt<=$formData['fee_item_size']; $fi_cnt++){
							//echo $formData['feeItemId']['fi_loop_index'];
							$feeItem[] = array(
								'fi_id' => $formData['feeItemId'][$fi_loop_index],
								'percentage' => $formData['feeItemPercent'][$fi_loop_index],
								'fix_amount' => $formData['feeItemAmount'][$fi_loop_index],
								'value' => $formData['feeItemValue'][$fi_loop_index],
								'tot_value' => $formData['totVal'][$fi_loop_index]
							);
							
							$fi_loop_index++;
						}
						
						$installment[] = $feeItem;
					}
					
										
					$ses_payment_plan->distribution = $installment;
										
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>5),'default',true));
				}else{
					
					if( isset($ses_payment_plan->installment) && isset($ses_payment_plan->invoice) ){
						
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
						$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
						
						$invoices = array();
						
						//get fee item setup
						$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
						$feeItemList = $feeItemDb->getData();
						
						//loop each selected invoice id and get detail data						
						foreach ($ses_payment_plan->invoice as $index=>$invoice_id){
							
							//get selected invoice data
							$invoices[$index] = $invoiceMainDb->getData($invoice_id);
							
							//get invoice detail
							$invoices[$index]['invoice_detail'] = $invoiceDetailDb->getInvoiceDetail($invoice_id);
						}
						
						//initial set fee item to paid according to invoice paid amount
						foreach ($invoices as $index => $invoice){
							$invoice_paid_amount = $invoice['bill_paid'];
							
							foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
								if( $invoice_paid_amount > $invoice_detail['amount'] ){
									$invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
									$invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
									$invoice_paid_amount = $invoice_paid_amount - $invoice_detail['amount'];
								}else{
									$invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_paid_amount;
									$invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $invoice_paid_amount;
									$invoice_paid_amount = $invoice_paid_amount - $invoice_paid_amount;
								}
							}
						}
						
						//loop each invoice & reduce fee item amount with advance payment option			
						if( isset($ses_payment_plan->invoice_knockoff) && $ses_payment_plan->invoice_knockoff!=null ){
							foreach ($invoices as $index => $invoice){
								foreach ($ses_payment_plan->invoice_knockoff as $invoice_knockoff){
									
									if($invoice['bill_number'] == $invoice_knockoff['invoice_no']){
										
										//calculate paid balance
										$invoices[$index]['bill_paid'] += $invoice_knockoff['adv_amt'];
										$invoices[$index]['bill_balance'] -= $invoice_knockoff['adv_amt'];

										//calculate fee item amount balance
										$adv_pymt_paid_amount = $invoice_knockoff['adv_amt'];
										foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
											if( $adv_pymt_paid_amount > $invoice_detail['amount'] ){
												$invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
												$invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
												$adv_pymt_paid_amount = $adv_pymt_paid_amount - $invoice_detail['amount'];
											}else{
												$invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $adv_pymt_paid_amount;
												$invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $adv_pymt_paid_amount;
												$adv_pymt_paid_amount = $adv_pymt_paid_amount - $adv_pymt_paid_amount;
											}
										}
									}
								}	
							}
						}
						
						//loop each invoice & calculate and inject array with fee item amount
						foreach ($invoices as $index => $invoice){	
							foreach ($invoice['invoice_detail'] as $inv_dtl){
								foreach($feeItemList as $feeItemIndex => $feeItem){
									if($feeItem['fi_id']==$inv_dtl['fi_id']){
										if(isset($feeItemList[$feeItemIndex]['total_amount'])){
											$feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['balance'];
										}else{
											$feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['balance'];
										}
										
										break;
									}
								}
							}
						}
						
						if( isset($ses_payment_plan->distribution) ){
							$this->view->distribution = $ses_payment_plan->distribution;
						}
						
						
						
						$this->view->invoices = $invoices;
						$this->view->fee_item = $feeItemList;
						$this->view->installment = $ses_payment_plan->installment;
						
					}else{
						$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>2, 'msg'=>'Please Complete step 2 process'),'default',true));
					}
					
				}
				
			}else
			if($step==5){
				
				$this->view->appl_id = $ses_payment_plan->appl_id;
				
				if(isset($ses_payment_plan->invoice) && isset($ses_payment_plan->distribution)){
					
					//invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					
					$invoiceList = array();
					foreach ($ses_payment_plan->invoice as $index=>$invoice_id){
						$invoiceList[] = $invoiceMainDb->getData($invoice_id);
					}
					
					//invoice detail
					foreach ($invoiceList as $index => $invoice){
						$invoiceList[$index]['invoice_detail'] = $invoiceDetailDb->getInvoiceDetail($invoice['id']);
					}
					
					
					//get advance payment
					$advPmntDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advancePaymentList = $advPmntDb->getApplicantBalanceAvdPayment($ses_payment_plan->appl_id);
					
					$this->view->advancePaymentList = $advancePaymentList;
					
					$advance_payment_balance = 0;
					if($advancePaymentList){
						foreach ($advancePaymentList as $avd_payment){
							$advance_payment_balance += $avd_payment['advpy_total_balance'];
						}
					}
					$this->view->advance_payment_balance = $advance_payment_balance;
					
					if( isset($ses_payment_plan->invoice_knockoff) ){
						
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
						
						$invoice_knockoff_list = array();
						foreach ($ses_payment_plan->invoice_knockoff as $index =>$invoice_knockoff){
							$invoice_knockoff_list[$index] = $invoice_knockoff;
							$invoice_knockoff_list[$index]['invoice_detail'] = $invoiceMainDb->getInvoiceData($invoice_knockoff['invoice_no'],true);
						}
						
						$this->view->invoice_knockoff = $invoice_knockoff_list;
					}
					
					//fee item distribution
					//get fee item setup
					$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
					$feeItemList = $feeItemDb->getData();
					
					//initial set fee item to paid according to invoice paid amount
					foreach ($invoiceList as $index => $invoice){
						$invoice_paid_amount = $invoice['bill_paid'];
						
						foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
							if( $invoice_paid_amount > $invoice_detail['amount'] ){
								$invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
								$invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
								$invoice_paid_amount = $invoice_paid_amount - $invoice_detail['amount'];
							}else{
								$invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_paid_amount;
								$invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $invoice_paid_amount;
								$invoice_paid_amount = $invoice_paid_amount - $invoice_paid_amount;
							}
						}
					}
					
					
						
					//invoice knockoff using advance payment
					if( isset($ses_payment_plan->invoice_knockoff) ){
						foreach ($invoiceList as $index => $invoice){
							
							foreach ($ses_payment_plan->invoice_knockoff as $invoice_knockoff){
								
								if($invoice['bill_number'] == $invoice_knockoff['invoice_no']){
									
									//calculate fee item amount balance
									$adv_pymt_paid_amount = $invoice_knockoff['adv_amt'];
									foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
										if( $adv_pymt_paid_amount > $invoice_detail['amount'] ){
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
											$adv_pymt_paid_amount = $adv_pymt_paid_amount - $invoice_detail['amount'];
										}else{
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $adv_pymt_paid_amount;
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $adv_pymt_paid_amount;
											$adv_pymt_paid_amount = $adv_pymt_paid_amount - $adv_pymt_paid_amount;
										}
									}
								}
							}	
						}
					}
					
					//loop each invoice & calculate and inject array with fee item amount
					foreach ($invoiceList as $index => $invoice){	
						foreach ($invoice['invoice_detail'] as $inv_dtl){
							foreach($feeItemList as $feeItemIndex => $feeItem){
								if($feeItem['fi_id']==$inv_dtl['fi_id']){
									if(isset($feeItemList[$feeItemIndex]['total_amount'])){
										$feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['balance'];
									}else{
										$feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['balance'];
									}
									break;
								}
							}
						}
					}
					
					$this->view->invoice_list = $invoiceList;
					$this->view->fee_item = $feeItemList;
										
					//fee item installment
					$this->view->installment = $ses_payment_plan->installment;
					
					//distribution
					if( isset($ses_payment_plan->distribution) ){
						$this->view->distribution = $ses_payment_plan->distribution;
					}
					
				}else{
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-payment-plan','step'=>4, 'msg'=>'Please Complete step 4 process'),'default',true));	
				}
				
			}else 
			if($step=='confirm'){
				if ($this->getRequest()->isPost()) {
					
					$formData = $this->getRequest()->getPost();

					//data from session
					$appl_id = $ses_payment_plan->appl_id;
					$invoice = $ses_payment_plan->invoice;
					$installment = $ses_payment_plan->installment;
					$invoice_knockoff = $ses_payment_plan->invoice_knockoff;
					$fee_item_distribution = $ses_payment_plan->distribution;
					
					//invoice detail
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					
					foreach ($invoice as $index=>$item){
						$invoice[$index] = $invoiceMainDb->getData($item);
						$invoice[$index]['detail'] = $invoiceDetailDb->getInvoiceDetail($item);
					}
					
					//advance payment record
					$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advance_payment_list = $advancePaymentDb->getApplicantBalanceAvdPayment($appl_id);
					
					$advance_payment = array();
					$advance_payment['record'] = $advance_payment_list;
					$advance_payment['total_amount'] = 0;
					
					foreach ($advance_payment_list as $adv_pymt){
						$advance_payment['total_amount'] += $adv_pymt['advpy_total_balance'];		
					}
					
					
					/*echo "<pre>";
					print_r($fee_item_distribution);
					echo "</pre>";
					exit;*/
					
					$auth = Zend_Auth::getInstance();
			
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->beginTransaction();
					
					try {
						/*
						 * process invoice knock-off from advance payment
						 * edited: remove advance payment in payment plan
						 */
						
						/*
						$adv_pymt_amount = $advance_payment['total_amount'];
						$adv_pymt_list = $advance_payment['record'];
						
						
						$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
						
						foreach($invoice as $inv){
						
							foreach($invoice_knockoff as $inv_knockoff){
								
								if($inv['bill_number'] == $inv_knockoff['invoice_no']){
									
									for($i=0;$i<sizeof($adv_pymt_list);$i++){
										
										$curr_advance_payment = $adv_pymt_list[$i];
										
										
										//even we have cater this in advance payment process. 
										//this is to ensure advance payment amount is sufficient to knockoff the invoice
										
										if( $curr_advance_payment['advpy_total_balance']>= $inv_knockoff['adv_amt']){
											$adv_pymt_list[$i]['advpy_total_balance'] = $adv_pymt_list[$i]['advpy_total_balance'] - $inv_knockoff['adv_amt'];
											
											//insert advance payment detail record
											$data = array(
												'advpydet_advpy_id' => $curr_advance_payment['advpy_id'],
												'advpydet_bill_no' => $inv_knockoff['invoice_no'],
												'advpydet_total_paid' => $inv_knockoff['adv_amt']
											);
											$advancePaymentDetailDb->insert($data);
											
											
											//update advance payment main record
											$data = array(
												'advpy_total_paid' => $curr_advance_payment['advpy_total_paid']+$inv_knockoff['adv_amt'],
												'advpy_total_balance' => $curr_advance_payment['advpy_total_balance']-$inv_knockoff['adv_amt'],
											);
											$advancePaymentDb->update($data, 'advpy_id = '.$curr_advance_payment['advpy_id']);
											
											
											//update invoice
											$data = array(
												'bill_paid' => $inv['bill_paid']+$inv_knockoff['adv_amt'],
												'bill_balance' => $inv['bill_balance']-$inv_knockoff['adv_amt'],
											);
											$invoiceMainDb->update($data, 'id = '.$inv['id']);
											

											//break loop for next invoice knockoff
											break 1;
										}
									}
								}
									
							}
							
						}
						
						*/
	
						
						/*
						 * save payment plan record
						 */
						
						$amount = 0;//calculate amount
						foreach ($fee_item_distribution as $installment_data){
							foreach ($installment_data as $fee_item){	
								$amount += $fee_item['value'];
							}
						}
						
						$paymentPlanDb = new Studentfinance_Model_DbTable_PaymentPlan();
						$data = array(
							'pp_appl_id' =>$appl_id,
							'pp_installment_no' =>$installment,
							'pp_amount' => $amount
						);
						$paymentplan_id = $paymentPlanDb->insert($data);
						
						
						
						$paymentPlanInvoiceDb = new Studentfinance_Model_DbTable_PaymentPlanInvoice();
						$auth = Zend_Auth::getInstance();
						
						//loop invoice
						foreach ($invoice as $inv){
							
							/*
							 * save payment plan invoice (invoice that will be canceled)
							 */
							$data = array(
								'ppi_plan_id' => $paymentplan_id,
								'ppi_invoice_id' => $inv['id'],
								'ppi_invoice_no' => $inv['bill_number']
							);
							$paymentPlanInvoiceDb->insert($data);
							
							
							/*
							 * cancel selected invoice for payment plan
							 */

							$data=array(
								'status'=>'X',
								'cancel_by' => $auth->getIdentity()->iduser,
								'cancel_date' => date('Y-m-d H:i:s')
							);	
							$this->_DbObj->update($data, 'id = '.$inv['id']);
						}
						
						
						
						/*
						 * generate new invoice from payment plan
						 */
						
						//get applicant fomulir no. from invoice
						$fomulir = array();
						foreach($invoice as $inv){
						    if( !in_array($inv['no_fomulir'],$fomulir) ){
						        $fomulir[] = $inv['no_fomulir'];
						    }
						}
						
						if(sizeof($fomulir) > 1){
							throw new Exception("Have 2 or more fomulir number. Cannot generate new invoice number");
						};
						
						//get next packet no. in invoice for given fomulir no.
						$paket_no = $this->_DbObj->getMaxPaketNo($fomulir[0]);
						
						//current academic year
						$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
						$currenctAcademicyear = $academicYearDb->getCurrentAcademicYearData();
						
						//loop installment distribution to inject fee_item definition
						
						$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
						foreach ($fee_item_distribution as $index=>$dist){
							foreach ($dist as $key=>$fee_item){
								//fee item description
								$fee_item_distribution[$index][$key]['fee_description'] = $feeItemDb->getData($fee_item['fi_id']);
							}
						}
						
						//get transaction data
						$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
						$txnData = $txnDb->getTransactionDataByFomulir($fomulir[0]);
						
						//get program info
						$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
						$applicant_program = $applicantProgramDb->getOfferProgram($txnData['at_trans_id'],$txnData['at_appl_type']);
						
						$programDb = new App_Model_Record_DbTable_Program();
						$program = $programDb->getData($applicant_program['program_id']);
						
								
						//loop installment distribution
						foreach ($fee_item_distribution as $index=>$dist){
							
							//calculate total amount
							$total_amount = 0;
							foreach ($dist as $key=>$fee_item){	
								$total_amount += $fee_item['value'];
							}
							
							//insert main bill
							$data = array(
										'bill_number' => $paket_no.($index+1).$fomulir[0],
										'appl_id' => $appl_id,
										'no_fomulir' => $fomulir[0],
										'academic_year' => $currenctAcademicyear['ay_id'],
										//'semester' => '',
										'bill_amount' => $total_amount,
										'bill_paid' => 0,
										'bill_balance' => $total_amount,
										'bill_description' => $program['ShortName']."-PP".$paket_no." Cicilan ".($index+1),
										'college_id' => $program['IdCollege'],
										'program_code' => $program['ProgramCode'],
										'creator' => '1',
										'fs_id' => 0,
										'fsp_id' => 0,
										'status' => 'A',
										'date_create' => date('Y-m-d H:i:s')
									);

							$main_id = $this->_DbObj->insert($data);

							
							//insert bill detail
							$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
							foreach ($dist as $fee_item){
								
								$data_detail = array(
												'invoice_main_id' => $main_id,
												'fi_id' => $fee_item['fi_id'],
												'fee_item_description' => $fee_item['fee_description']['fi_name_bahasa'],
												'amount' => isset($fee_item['value']) && $fee_item['value']!=""?$fee_item['value']:0.00
											);
																
								$invoiceDetailDb->insert($data_detail);
							}
							
							/*
							 * save payment plan detail
						 	 */
							$paymentPlanDetail = new Studentfinance_Model_DbTable_PaymentPlanDetail();
							$data = array(
								'ppd_payment_plan_id' => $paymentplan_id,
								'ppd_invoice_id' => $main_id,
								'ppd_installment_no' => ($index+1)
							);
							
							$paymentPlanDetail->insert($data);
						
						
						}
						
						
						$db->commit();
						
						//redirect
						$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'applicant','id'=>$txnData['at_appl_id']),'default',true));
						
					}catch (Exception $e) {
						echo "Error in Invoice Payment Plan. <br />";
						echo $e->getMessage();
						
						echo "<pre>";
						print_r($e->getTrace());
						echo "</pre>";
						
						$db->rollBack();
		    			
		    			exit;
		    			
					}
					
					
				}
				
				exit;
			}else{
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice'),'default',true));	
			}
		}else{
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice'),'default',true));	
		}
	}
	
	public function applicantPaymentPlanDetailAction(){
		
		$this->_helper->layout->disableLayout();
		
		$this->view->title = $this->view->translate("Payment Plan Detail");
		
		$payment_plan_id = $this->_getParam('id', null);
		$this->view->id = $payment_plan_id;
		
		$installment = $this->_getParam('installment', 1);
		$this->view->installment = $installment;
		

		$paymentPlanDb = new Studentfinance_Model_DbTable_PaymentPlan();
		$data = $paymentPlanDb->getData($payment_plan_id);
		
		$paymentPlanDetailDb = new Studentfinance_Model_DbTable_PaymentPlanDetail();
		
		$data['detail'] = $paymentPlanDetailDb->getPaymentPlanDetail($data['pp_id']);
		
		$this->view->data = $data;
		
	}
	
	public function printApplicantBankSlipAction(){
		
		$payment_plan_id = $this->_getParam('id', null);
		$this->view->id = $payment_plan_id;
		
		$installment = $this->_getParam('installment', 1);
		$this->view->installment = $installment;
		
		$bankslip = new icampus_Function_Application_BankSlip();
		
		$bankslip->generateBankSlip($payment_plan_id, $installment);
		exit;
	}
	
	public function printBankSlipAction(){
		$invoice_id = $this->_getParam('invoice_id', null);
		
		$bankslip = new icampus_Function_Application_BankSlip();
		
		$bankslip->printBankSlip($invoice_id);
		exit;
	}
	
	public function studentAction(){
	
		$IdStudentRegistration = $this->_getParam('id', null);
		$this->view->student_registration_id = $IdStudentRegistration;
	
		//title
		$this->view->title= $this->view->translate("Invoice : Student Detail");
		 
		//registration info
		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
//		$registration = $studentRegistrationDb->getData($IdStudentRegistration);
		$registration = $studentRegistrationDb->getTheStudentRegistrationDetail($IdStudentRegistration);
		$this->view->profile = $registration;
		
		/*//transaction info
		$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $transDB->fetchRow('at_trans_id = '.$registration['transaction_id']);
		$this->view->txn_data = $txnData;
		
		//student profile
		$studentProfileDb = new Records_Model_DbTable_Studentprofile();
		$profile = $studentProfileDb->fnGetStudentProfileByApplicationId($registration['IdApplication']);
		$this->view->profile = $profile;*/
		
	}
	
	public function studentInvoiceAction(){
		$this->_helper->layout()->disableLayout();
	
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
	
		$IdStudentRegistration = $this->_getParam('id', null);
		$this->view->IdStudentRegistration = $IdStudentRegistration;
		
		$type = 2;
		
		//registration info
		/*//transaction info
		$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $transDB->fetchRow('at_trans_id = '.$registration['transaction_id']);
		$this->view->txn_data = $txnData;*/
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select_proformainvoice = $db->select()
								->from(array('pim'=>'proforma_invoice_main'),array(
									'id'=>'pim.id',
									'record_date'=>'pim.date_create',
									'description' => 'fc.fc_desc',
									'txn_type' => new Zend_Db_Expr ('"Proforma"'),
									'debit' =>'pim.bill_amount',
									'credit' => new Zend_Db_Expr ('"0.00"'),
									'document' => 'pim.bill_number',
									'invoice_no' => 'pim.bill_number',
									'receipt_no' => new Zend_Db_Expr ('"-"'),
									'fc_seq'=> 'fc.fc_seq'
								)
						)
						->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id=pim.fee_category',array())
						->join(array('c'=>'tbl_currency'),'c.cur_id=pim.currency_id', array('cur_code','cur_id','cur_desc'))		
						//->where('im.currency_id= ?',$currency)						  
//						->where('im.appl_id = ?', $appl_id)
//						->where('im.trans_id = ?', $trans_id)
						->where("pim.invoice_id = 0")
						->where("pim.status = 'A'");
						
		if($type == 1){
			$select_proformainvoice->where('pim.trans_id = ?', $IdStudentRegistration);
		}elseif($type == 2){
			$select_proformainvoice->where('pim.IdStudentRegistration = ?', $IdStudentRegistration);
		}
		
		$select_invoice = $db->select()
								->from(array('im'=>'invoice_main'),array(
									'id'=>'im.id',
									'record_date'=>'im.date_create',
									'description' => 'fc.fc_desc',
									'txn_type' => new Zend_Db_Expr ('"Invoice"'),
									'debit' =>'bill_amount',
									'credit' => new Zend_Db_Expr ('"0.00"'),
									'document' => 'bill_number',
									'invoice_no' => 'bill_number',
									'receipt_no' => new Zend_Db_Expr ('"-"'),
									'fc_seq'=> 'fc.fc_seq'
								)
						)
						->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id=im.id', array())	
						->join(array('fi'=>'fee_item'),'fi.fi_id=ivd.fi_id', array())	
						->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id=fi.fi_fc_id',array())
						->join(array('c'=>'tbl_currency'),'c.cur_id=im.currency_id', array('cur_code','cur_id','cur_desc'))		
						//->where('im.currency_id= ?',$currency)						  
//						->where('im.appl_id = ?', $appl_id)
//						->where('im.trans_id = ?', $trans_id)
						->where("im.status != 'X'")
						->group('im.id');
						
		if($type == 1){
			$select_invoice->where('im.trans_id = ?', $IdStudentRegistration);
		}elseif($type == 2){
			$select_invoice->where('im.IdStudentRegistration = ?', $IdStudentRegistration);
		}

							
			$select_payment = $db->select()
							->from(
								array('pm'=>'receipt'),array(
										'id'=>'pm.rcp_id',
										'record_date'=>'pm.rcp_receive_date',
										'description' => 'pm.rcp_description',
										'txn_type' => new Zend_Db_Expr ('"Payment"'),
										'debit' =>new Zend_Db_Expr ('"0.00"'),
										'credit' => 'rcp_amount',
										'document' => 'rcp_no',
										'invoice_no' => 'rcp_no',
										'receipt_no' => 'rcp_no',
										'fc_seq'=> 'c.cur_id'																
									)
							)
							
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=pm.rcp_cur_id', array('cur_code','cur_id','cur_desc'))	
							//->where('pm.rcp_cur_id= ?',$currency)		
							->where("pm.rcp_status = 'APPROVE'");
		if($type == 1){
			$select_payment->where('pm.rcp_trans_id = ?', $IdStudentRegistration);
		}elseif($type == 2){
			$select_payment->where('pm.rcp_idStudentRegistration = ?', $IdStudentRegistration);
		}
									
										
			//get array payment				
			$select = $db->select()
					    ->union(array($select_proformainvoice,$select_invoice),  Zend_Db_Select::SQL_UNION_ALL)
					    //->order("CASE txn_type WHEN 'Proforma' THEN 1 WHEN 'Invoice' THEN 2 WHEN 'Payment' THEN 3 ELSE 4 END")
                                            //->order("record_date asc")
					    //->order("txn_type asc")
					   
					   ;
                        
                        $select .= "ORDER BY record_date asc, CASE txn_type WHEN 'Proforma' THEN 1 WHEN 'Invoice' THEN 2 WHEN 'Payment' THEN 3 ELSE 4 END";
                        //$select->order("record_date asc");

		$results = $db->fetchAll($select);
		
		if(!$results){
			$results = null;
		}
		
		$this->view->account = $results;
		
			//process data
			$amountCurr = array();
			$curencyArray = array();
			$balance = 0;
			
			if($results){
				foreach ( $results as $row )
				{
					$curencyArray[$row['cur_id']] = $row['cur_code'];
					
					$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
					$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
						
					if($row['cur_id'] == 2){
						$amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'],2);
						$amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'],2);
					}else{
						$amountDebit = $row['debit'];
						$amountCredit = $row['credit'];
					}
					
		
					if( $row['txn_type']=="Invoice" ){
						$balance = $balance + $amountDebit;
					}else
					if( $row['txn_type']=="Payment" ){
						
						if($row['cur_id'] == 2){
							$amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'],2);
						}else{
							$amountCredit = $row['credit'];
						}
					
						$balance = $balance - $amountCredit;
						
					}else
					if( $row['txn_type']=="Credit Note" ){
						$balance = $balance - $row['credit'];
					}else
					if( $row['txn_type']=="Debit Note" ){
						$balance = $balance + $row['debit'];
					}else
					if( $row['txn_type']=="Refund" ){
						$balance = $balance + $row['debit'];
					}
					
		
					$amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
					$amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
					$amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');
					
				}
			}
		
			
//		    $curAvailable = array_unique($curencyArray);
//		    echo "<pre>";
//			print_r($curAvailable);

		    $curAvailable = array('2'=>'USD','1'=>'MYR');
		    
			$this->view->availableCurrency= $curAvailable;
			$this->view->amountCurr = $amountCurr;
	
	}
	
	public function studentDeleteInvoiceAction(){
	  $invoice_id = $this->_getParam('id', null);
	  $idStudentRegistration = $this->_getParam('idreg', null);
	
	  $auth = Zend_Auth::getInstance();
	
	  //get invoice main data
	  $invoice = $this->_DbObj->getData($invoice_id);
	
	  //add invoice delete history
	  $invoiceDeleteHistoryDb = new Studentfinance_Model_DbTable_InvoiceDeleteHistory();
	  $invoice['invoice_main_id'] = $invoice['id'];
	  $invoice['id'] = null;
	  $invoice['delete_by'] = $auth->getIdentity()->iduser;
	  $invoice['delete_date'] = date('Y-m-d H:i:s');
	
	  $invoiceDeleteHistoryDb->insert($invoice);
	
	  //delete
	  $this->_DbObj->delete('id = '.$invoice_id);
	
	  //redirect
	  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'student','id'=>$idStudentRegistration),'default',true));
	}
	
	public function studentPaymentPlanAction(){
	
	  $appl_id = $this->_getParam('id', null);
	  $this->view->appl_id = $appl_id;
	  
	  $idStudentRegistration = $this->_getParam('idStudentRegistration', null);
	  $this->view->idStudentRegistration = $idStudentRegistration;
	
	  $this->_helper->layout()->disableLayout();
	
	  $paymentPlanDb = new Studentfinance_Model_DbTable_PaymentPlan();
	
	  $data = $paymentPlanDb->getApplicantPaymentPlan($appl_id);
	  $this->view->data = $data;
	
	}
	
	
	public function invoiceStudentPaymentPlanAction(){
	  $appl_id = $this->_getParam('appl_id', null);
	  $idStudentRegistration = $this->_getParam('idStudentRegistration', null);
	
	  $msg = $this->_getParam('msg', null);
	  $this->view->noticeMessage = $msg;
	
	  $step = $this->_getParam('step', 1);
	  $this->view->step = $step;
	
	  $this->view->title = "Create Student Payment Plan";
	
	  //set custom session
	  $ses_payment_plan = new Zend_Session_Namespace('payment_plan');
	
	  if($appl_id){
	    Zend_Session::namespaceUnset('payment_plan');
	    	
	    $ses_payment_plan = new Zend_Session_Namespace('payment_plan');
	    $ses_payment_plan->appl_id = $appl_id;
	    $ses_payment_plan->idStudentRegistration = $idStudentRegistration;
	    	
	    //redirect
	    $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>1),'default',true));
	  }
	
	  //applicant info
	  $applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
	  $this->view->profile = $applicantProfileDb->getData($ses_payment_plan->appl_id);
	  
	  //registration info
      $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
      $registration = $studentRegistrationDb->getData($ses_payment_plan->idStudentRegistration);
      $this->view->registration = $registration;
      
      
	
	  if( isset($ses_payment_plan->appl_id) ){
	    if($step==1){
	      
	      //semester list
	      $semesterDb = new Records_Model_DbTable_SemesterMaster();
	      $semester_list = $semesterDb->fetchAll(null,'SemesterMainStartDate DESC')->toArray();
	      $this->view->semester_list = $semester_list;
	       
	      $semester_id = $this->_getParam('sem', $semester_list[0]['IdSemesterMaster']);
	      $this->view->semester_id = $semester_id;
	
	      if ($this->getRequest()->isPost()) {
	        $formData = $this->getRequest()->getPost();
	        	
	        if( isset($formData['invoices'])){
	          $ses_payment_plan->invoice = $formData['invoices'];
	          $ses_payment_plan->invoice_knockoff = null;
	          $ses_payment_plan->distribution = null;
	        }else{
	          $ses_payment_plan->invoice = null;
	          $ses_payment_plan->invoice_knockoff = null;
	          $ses_payment_plan->distribution = null;
	        }
	        
	        $ses_payment_plan->semester = $semester_id;
	        	
	        //redirect
	        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>2),'default',true));
	        	
	      }else{
	        
	        
	        
	        //active invoice
	        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	        $invoiceList = $invoiceMainDb->getStudentInvoiceData($ses_payment_plan->appl_id,null,$semester_id,null,true);
	
	        if($invoiceList){
	
	          //remove paid invoice
	          foreach ($invoiceList as $index=>$invoice){
	            	
	            if($invoice['bill_balance']==0 && $invoice['bill_balance']!=null){
	              unset($invoiceList[$index]);
	            }
	          }
	          $invoiceList = array_values($invoiceList);
	
	          //remove invoice with credit note
	          $creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
	          foreach ($invoiceList as $index=>$invoice){
	            if($creditNoteDb->getDataByInvoice($invoice['bill_number'])){
	              unset($invoiceList[$index]);
	            }
	          }
	          $invoiceList = array_values($invoiceList);
	
	        }
	
	        if(isset($ses_payment_plan->invoice)){
	          	
	          //set checked from data session
	          	
	
	          foreach ($invoiceList as $index=>$invoice){
	            	
	            if(in_array($invoice['id'],array_values($ses_payment_plan->invoice))){
	              $invoiceList[$index]['selected'] = true;
	            }else{
	              $invoiceList[$index]['selected'] = false;
	            }
	          }
	        }
	        	
	        $this->view->invoice_list = $invoiceList;
	        	
	        	
	
	      }
	
	
	    }else
	      if($step==2){
	
	      if ($this->getRequest()->isPost()) {
	        $formData = $this->getRequest()->getPost();
	        	
	        if( is_numeric($formData['installment']) && $formData['installment'] > 0 ){
	
	          //reset distribution if different amount
	          if( $ses_payment_plan->installment !=  $formData['installment'] ){
	            $ses_payment_plan->distribution = null;
	          }
	
	          $ses_payment_plan->installment = $formData['installment'];
	
	        }else{
	          $ses_payment_plan->installment = null;
	          $ses_payment_plan->distribution = null;
	        }
	        	
	        //redirect
	        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>4),'default',true));
	      }else{
	        if(!isset($ses_payment_plan->invoice)){
	          $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>1, 'msg'=>'Please Complete step 1 process'),'default',true));
	        }
	
	        if(isset($ses_payment_plan->installment)){
	          $this->view->installment = $ses_payment_plan->installment;
	        }
	        	
	      }
	
	    }else
	      if($step==3){
	
	      //redirect
	    $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>4),'default',true));
	    exit;
	    	
	    if ($this->getRequest()->isPost()) {
	      $formData = $this->getRequest()->getPost();
	      	
	      if( isset($formData['invoice']) ){
	        $invoice_arr = array();
	        foreach ($formData['invoice'] as $index => $invoice){
	          $invoice_arr[] = array(
	              'invoice_no'=>$invoice,
	              'adv_amt'=> $formData['invoice_adv_pymt_amt'][$index]
	          );
	        }
	
	        $ses_payment_plan->invoice_knockoff = $invoice_arr;
	        $ses_payment_plan->distribution = null;
	      }else{
	        $ses_payment_plan->invoice_knockoff = null;
	      }
	      	
	      	
	      //redirect
	      $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>4),'default',true));
	    }else{
	      	
	      if( isset($ses_payment_plan->installment) && isset($ses_payment_plan->invoice) ){
	
	        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	        $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
	
	        $invoices = array();
	
	        //get fee item setup
	        $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
	        $feeItemList = $feeItemDb->getData();
	
	        //loop each invoice
	        foreach ($ses_payment_plan->invoice as $index=>$invoice_id){
	          //get selected invoice data
	          $invoices[$index] = $invoiceMainDb->getData($invoice_id);
	          	
	          //get invoice detail
	          $invoices[$index]['invoice_detail'] = $invoiceDetailDb->getInvoiceDetail($invoice_id);
	          	
	          	
	
	          //sum amount to fee item
	          foreach ($invoices[$index]['invoice_detail'] as $inv_dtl){
	
	            foreach($feeItemList as $feeItemIndex => $feeItem){
	              	
	              if($feeItem['fi_id']==$inv_dtl['fi_id']){
	                if(isset($feeItemList[$feeItemIndex]['total_amount'])){
	                  $feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['amount'];
	                }else{
	                  $feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['amount'];
	                }
	                break;
	              }
	            }
	          }
	        }
	
	        $this->view->invoices = $invoices;
	        $this->view->fee_item = $feeItemList;
	
	        //get advance payment
	        $advPmntDb = new Studentfinance_Model_DbTable_AdvancePayment();
	        $advancePaymentList = $advPmntDb->getApplicantBalanceAvdPayment($ses_payment_plan->appl_id);
	
	        $this->view->advancePaymentList = $advancePaymentList;
	
	        $advance_payment_balance = 0;
	        if($advancePaymentList){
	          foreach ($advancePaymentList as $avd_payment){
	            $advance_payment_balance += $avd_payment['advpy_total_balance'];
	          }
	        }
	
	        $this->view->advance_payment_balance = $advance_payment_balance;
	
	        if( isset($ses_payment_plan->invoice_knockoff) ){
	          $this->view->invoice_knockoff = $ses_payment_plan->invoice_knockoff;
	        }
	
	
	      }else{
	        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>2, 'msg'=>'Please Complete step 2 process'),'default',true));
	      }
	      	
	    }
	
	    }else
	      if($step==4){
	
	      if ($this->getRequest()->isPost()) {
	        $formData = $this->getRequest()->getPost();
	        	
	        $fi_loop_index = 0;
	        $installment = array();
	        	
	        for($i=0; $i<sizeof($formData['installment']); $i++){
	          $feeItem = array();
	          	
	          //fee item loop based in size
	          for($fi_cnt = 1; $fi_cnt<=$formData['fee_item_size']; $fi_cnt++){
	            //echo $formData['feeItemId']['fi_loop_index'];
	            $feeItem[] = array(
	                'fi_id' => $formData['feeItemId'][$fi_loop_index],
	                'percentage' => $formData['feeItemPercent'][$fi_loop_index],
	                'fix_amount' => $formData['feeItemAmount'][$fi_loop_index],
	                'value' => $formData['feeItemValue'][$fi_loop_index],
	                'tot_value' => $formData['totVal'][$fi_loop_index]
	            );
	            	
	            $fi_loop_index++;
	          }
	
	          $installment[] = $feeItem;
	        }
	        	
	
	        $ses_payment_plan->distribution = $installment;
	
	        //redirect
	        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>5),'default',true));
	      }else{
	        	
	        if( isset($ses_payment_plan->installment) && isset($ses_payment_plan->invoice) ){
	
	          $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	          $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
	
	          $invoices = array();
	
	          //get fee item setup
	          $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
	          $feeItemList = $feeItemDb->getData();
	
	          //loop each selected invoice id and get detail data
	          foreach ($ses_payment_plan->invoice as $index=>$invoice_id){
	            	
	            //get selected invoice data
	            $invoices[$index] = $invoiceMainDb->getData($invoice_id);
	            	
	            //get invoice detail
	            $invoices[$index]['invoice_detail'] = $invoiceDetailDb->getInvoiceDetail($invoice_id);
	          }
	
	          //initial set fee item to paid according to invoice paid amount
	          foreach ($invoices as $index => $invoice){
	            $invoice_paid_amount = $invoice['bill_paid'];
	            	
	            foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
	              if( $invoice_paid_amount > $invoice_detail['amount'] ){
	                $invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
	                $invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
	                $invoice_paid_amount = $invoice_paid_amount - $invoice_detail['amount'];
	              }else{
	                $invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_paid_amount;
	                $invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $invoice_paid_amount;
	                $invoice_paid_amount = $invoice_paid_amount - $invoice_paid_amount;
	              }
	            }
	          }
	
	          //loop each invoice & reduce fee item amount with advance payment option
	          if( isset($ses_payment_plan->invoice_knockoff) && $ses_payment_plan->invoice_knockoff!=null ){
	            foreach ($invoices as $index => $invoice){
	              foreach ($ses_payment_plan->invoice_knockoff as $invoice_knockoff){
	                	
	                if($invoice['bill_number'] == $invoice_knockoff['invoice_no']){
	
	                  //calculate paid balance
	                  $invoices[$index]['bill_paid'] += $invoice_knockoff['adv_amt'];
	                  $invoices[$index]['bill_balance'] -= $invoice_knockoff['adv_amt'];
	
	                  //calculate fee item amount balance
	                  $adv_pymt_paid_amount = $invoice_knockoff['adv_amt'];
	                  foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
	                    if( $adv_pymt_paid_amount > $invoice_detail['amount'] ){
	                      $invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
	                      $invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
	                      $adv_pymt_paid_amount = $adv_pymt_paid_amount - $invoice_detail['amount'];
	                    }else{
	                      $invoices[$index]['invoice_detail'][$fee_item_index]['paid'] = $adv_pymt_paid_amount;
	                      $invoices[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $adv_pymt_paid_amount;
	                      $adv_pymt_paid_amount = $adv_pymt_paid_amount - $adv_pymt_paid_amount;
	                    }
	                  }
	                }
	              }
	            }
	          }
	
	          //loop each invoice & calculate and inject array with fee item amount
	          foreach ($invoices as $index => $invoice){
	            foreach ($invoice['invoice_detail'] as $inv_dtl){
	              foreach($feeItemList as $feeItemIndex => $feeItem){
	                if($feeItem['fi_id']==$inv_dtl['fi_id']){
	                  if(isset($feeItemList[$feeItemIndex]['total_amount'])){
	                    $feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['balance'];
	                  }else{
	                    $feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['balance'];
	                  }
	
	                  break;
	                }
	              }
	            }
	          }
	
	          if( isset($ses_payment_plan->distribution) ){
	            $this->view->distribution = $ses_payment_plan->distribution;
	          }
	
	
	
	          $this->view->invoices = $invoices;
	          $this->view->fee_item = $feeItemList;
	          $this->view->installment = $ses_payment_plan->installment;
	
	        }else{
	          $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>2, 'msg'=>'Please Complete step 2 process'),'default',true));
	        }
	        	
	      }
	
	    }else
	      if($step==5){
	
	      $this->view->appl_id = $ses_payment_plan->appl_id;
	
	      if(isset($ses_payment_plan->invoice) && isset($ses_payment_plan->distribution)){
	        	
	        //invoice
	        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	        $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
	        	
	        $invoiceList = array();
	        foreach ($ses_payment_plan->invoice as $index=>$invoice_id){
	          $invoiceList[] = $invoiceMainDb->getData($invoice_id);
	        }
	        	
	        //invoice detail
	        foreach ($invoiceList as $index => $invoice){
	          $invoiceList[$index]['invoice_detail'] = $invoiceDetailDb->getInvoiceDetail($invoice['id']);
	        }
	        	
	        	
	        //get advance payment
	        $advPmntDb = new Studentfinance_Model_DbTable_AdvancePayment();
	        $advancePaymentList = $advPmntDb->getApplicantBalanceAvdPayment($ses_payment_plan->appl_id);
	        	
	        $this->view->advancePaymentList = $advancePaymentList;
	        	
	        $advance_payment_balance = 0;
	        if($advancePaymentList){
	          foreach ($advancePaymentList as $avd_payment){
	            $advance_payment_balance += $avd_payment['advpy_total_balance'];
	          }
	        }
	        $this->view->advance_payment_balance = $advance_payment_balance;
	        	
	        if( isset($ses_payment_plan->invoice_knockoff) ){
	
	          $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	
	          $invoice_knockoff_list = array();
	          foreach ($ses_payment_plan->invoice_knockoff as $index =>$invoice_knockoff){
	            $invoice_knockoff_list[$index] = $invoice_knockoff;
	            $invoice_knockoff_list[$index]['invoice_detail'] = $invoiceMainDb->getInvoiceData($invoice_knockoff['invoice_no'],true);
	          }
	
	          $this->view->invoice_knockoff = $invoice_knockoff_list;
	        }
	        	
	        //fee item distribution
	        //get fee item setup
	        $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
	        $feeItemList = $feeItemDb->getData();
	        	
	        //initial set fee item to paid according to invoice paid amount
	        foreach ($invoiceList as $index => $invoice){
	          $invoice_paid_amount = $invoice['bill_paid'];
	
	          foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
	            if( $invoice_paid_amount > $invoice_detail['amount'] ){
	              $invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
	              $invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
	              $invoice_paid_amount = $invoice_paid_amount - $invoice_detail['amount'];
	            }else{
	              $invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_paid_amount;
	              $invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $invoice_paid_amount;
	              $invoice_paid_amount = $invoice_paid_amount - $invoice_paid_amount;
	            }
	          }
	        }
	        	
	        	
	
	        //invoice knockoff using advance payment
	        if( isset($ses_payment_plan->invoice_knockoff) ){
	          foreach ($invoiceList as $index => $invoice){
	            	
	            foreach ($ses_payment_plan->invoice_knockoff as $invoice_knockoff){
	
	              if($invoice['bill_number'] == $invoice_knockoff['invoice_no']){
	                	
	                //calculate fee item amount balance
	                $adv_pymt_paid_amount = $invoice_knockoff['adv_amt'];
	                foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
	                  if( $adv_pymt_paid_amount > $invoice_detail['amount'] ){
	                    $invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice_detail['amount'];
	                    $invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
	                    $adv_pymt_paid_amount = $adv_pymt_paid_amount - $invoice_detail['amount'];
	                  }else{
	                    $invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $adv_pymt_paid_amount;
	                    $invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice_detail['amount'] - $adv_pymt_paid_amount;
	                    $adv_pymt_paid_amount = $adv_pymt_paid_amount - $adv_pymt_paid_amount;
	                  }
	                }
	              }
	            }
	          }
	        }
	        	
	        //loop each invoice & calculate and inject array with fee item amount
	        foreach ($invoiceList as $index => $invoice){
	          foreach ($invoice['invoice_detail'] as $inv_dtl){
	            foreach($feeItemList as $feeItemIndex => $feeItem){
	              if($feeItem['fi_id']==$inv_dtl['fi_id']){
	                if(isset($feeItemList[$feeItemIndex]['total_amount'])){
	                  $feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['balance'];
	                }else{
	                  $feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['balance'];
	                }
	                break;
	              }
	            }
	          }
	        }
	        	
	        $this->view->invoice_list = $invoiceList;
	        $this->view->fee_item = $feeItemList;
	
	        //fee item installment
	        $this->view->installment = $ses_payment_plan->installment;
	        	
	        //distribution
	        if( isset($ses_payment_plan->distribution) ){
	          $this->view->distribution = $ses_payment_plan->distribution;
	        }
	        	
	      }else{
	        //redirect
	        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'invoice-student-payment-plan','step'=>4, 'msg'=>'Please Complete step 4 process'),'default',true));
	      }
	
	    }else
	      if($step=='confirm'){
	      if ($this->getRequest()->isPost()) {
	        	
	        $formData = $this->getRequest()->getPost();
	
	        //data from session
	        $appl_id = $ses_payment_plan->appl_id;
	        $idStudentRegistration = $ses_payment_plan->idStudentRegistration;
	        $invoice = $ses_payment_plan->invoice;
	        $installment = $ses_payment_plan->installment;
	        $invoice_knockoff = $ses_payment_plan->invoice_knockoff;
	        $fee_item_distribution = $ses_payment_plan->distribution;
	        $semester = $ses_payment_plan->semester;
	        
	        //registration info
	        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
	        $registration = $studentRegistrationDb->getData($idStudentRegistration);
	        
	        //intake info
	        $intakeDb = new App_Model_Record_DbTable_Intake();
	        $intake = $intakeDb->fetchRow('IdIntake = '.$registration['IdIntake'])->toArray();
	        
	        //get semester info
	        $semesterDb = new Records_Model_DbTable_SemesterMaster();
	        $semester_data = $semesterDb->fetchRow('IdSemesterMaster = '.$semester)->toArray();
	         
	        //academic year
	        $academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	        $academicYear = $academicYearDb->fetchRow('ay_id = '.$semester_data['idacadyear'])->toArray();
	        
	        
	        //invoice detail
	        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	        $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
	        	
	        foreach ($invoice as $index=>$item){
	          $invoice[$index] = $invoiceMainDb->getData($item);
	          $invoice[$index]['detail'] = $invoiceDetailDb->getInvoiceDetail($item);
	        }
	        	
	        //advance payment record
	        $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
	        $advance_payment_list = $advancePaymentDb->getApplicantBalanceAvdPayment($appl_id);
	        	
	        $advance_payment = array();
	        $advance_payment['record'] = $advance_payment_list;
	        $advance_payment['total_amount'] = 0;
	        	
	        if($advance_payment_list){
    	        foreach ($advance_payment_list as $adv_pymt){
    	          $advance_payment['total_amount'] += $adv_pymt['advpy_total_balance'];
    	        }
	        }
	        
	        	
	        $auth = Zend_Auth::getInstance();
	        	
	        $db = Zend_Db_Table::getDefaultAdapter();
	        $db->beginTransaction();
	        	
	        try {
	
	          /*
	           * save payment plan record
	          */
	
	          $amount = 0;//calculate amount
	          foreach ($fee_item_distribution as $installment_data){
	            foreach ($installment_data as $fee_item){
	              $amount += $fee_item['value'];
	            }
	          }
	
	          $paymentPlanDb = new Studentfinance_Model_DbTable_PaymentPlan();
	          $data = array(
	              'pp_appl_id' =>$appl_id,
	              'pp_installment_no' =>$installment,
	              'pp_amount' => $amount
	          );
	          $paymentplan_id = $paymentPlanDb->insert($data);
	
	
	
	          $paymentPlanInvoiceDb = new Studentfinance_Model_DbTable_PaymentPlanInvoice();
	          $auth = Zend_Auth::getInstance();
	
	          //loop invoice
	          foreach ($invoice as $inv){
	            	
	            /*
	             * save payment plan invoice (invoice that will be canceled)
	            */
	            $data = array(
	                'ppi_plan_id' => $paymentplan_id,
	                'ppi_invoice_id' => $inv['id'],
	                'ppi_invoice_no' => $inv['bill_number']
	            );
	            $paymentPlanInvoiceDb->insert($data);
	            	
	            	
	            /*
	             * cancel selected invoice for payment plan
	            */
	
	            $data=array(
	                'status'=>'X',
	                'cancel_by' => $auth->getIdentity()->iduser,
	                'cancel_date' => date('Y-m-d H:i:s')
	            );
	            $this->_DbObj->update($data, 'id = '.$inv['id']);
	          }
	
	
	
	          /*
	           * generate new invoice from payment plan
	          */
	          
	          //loop installment distribution to inject fee_item definition
	          $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
	          foreach ($fee_item_distribution as $index=>$dist){
	            foreach ($dist as $key=>$fee_item){
	              //fee item description
	              $fee_item_distribution[$index][$key]['fee_description'] = $feeItemDb->getData($fee_item['fi_id']);
	            }
	          }
	
	          //get program info
	          $programDb = new App_Model_Record_DbTable_Program();
	          $program = $programDb->getData($registration['IdProgram']);
	
	          
	          //loop installment distribution
	          foreach ($fee_item_distribution as $index=>$dist){
	            	
	            //calculate total amount
	            $total_amount = 0;
	            foreach ($dist as $key=>$fee_item){
	              $total_amount += $fee_item['value'];
	            }
	            
	            //generate billing number
	            $seq_data = array(
	                date('y',strtotime($academicYear['ay_start_date'])),
	                substr($intake['IntakeId'],2,2),
	                $program['ProgramCode'], 0
	            );
	            
	            $stmt = $db->prepare("SELECT invoice_seq(?,?,?,?) AS invoice_no");
	            $stmt->execute($seq_data);
	            $invoice_no = $stmt->fetch();
	            
	            //insert main bill
	            $data = array(
	                'bill_number' => $invoice_no['invoice_no'],
	                'appl_id' => $registration['IdApplication'],
	                'IdStudentRegistration' => $registration['IdStudentRegistration'],
	                'academic_year' => $academicYear['ay_id'],
	                'semester' => $semester_data['IdSemesterMaster'],
	                'bill_amount' => $total_amount,
	                'bill_paid' => 0,
	                'bill_balance' => $total_amount,
	                'bill_description' => $program['ShortName']."-PPS"." Cicilan ".($index+1),
	                'college_id' => $program['IdCollege'],
	                'program_code' => $program['ProgramCode'],
	                'creator' => '1',
	                'fs_id' => 0,
	                'fsp_id' => 0,
	                'status' => 'A',
	                'date_create' => date('Y-m-d H:i:s')
	            );
	
	            $main_id = $this->_DbObj->insert($data);
	
	            	
	            //insert bill detail
	            $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
	            foreach ($dist as $fee_item){
	
	              $data_detail = array(
	                  'invoice_main_id' => $main_id,
	                  'fi_id' => $fee_item['fi_id'],
	                  'fee_item_description' => $fee_item['fee_description']['fi_name_bahasa'],
	                  'amount' => isset($fee_item['value']) && $fee_item['value']!=""?$fee_item['value']:0.00
	              );
	
	              $invoiceDetailDb->insert($data_detail);
	            }
	            	
	            /*
	             * save payment plan detail
	            */
	            $paymentPlanDetail = new Studentfinance_Model_DbTable_PaymentPlanDetail();
	            $data = array(
	                'ppd_payment_plan_id' => $paymentplan_id,
	                'ppd_invoice_id' => $main_id,
	                'ppd_installment_no' => ($index+1)
	            );
	            	
	            $paymentPlanDetail->insert($data);
	
	
	          }
	
	
	          $db->commit();
	
	          //redirect
	          $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice', 'action'=>'student','id'=>$registration['IdStudentRegistration']),'default',true));
	
	        }catch (Exception $e) {
	          echo "Error in Invoice Student Payment Plan. <br />";
	          echo $e->getMessage();
	
	          echo "<pre>";
	          print_r($e->getTrace());
	          echo "</pre>";
	
	          $db->rollBack();
	           
	          exit;
	           
	        }
	        	
	        	
	      }
	
	      exit;
	    }else{
	      //redirect
	      $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice'),'default',true));
	    }
	  }else{
	    //redirect
	    $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'invoice'),'default',true));
	  }
	}
	
	public function updateStatusAction(){
		
		$type = $this->_getParam('type', null);
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
//			echo "<pre>";
//			print_r($formData);

			$proforma = $formData['proforma'];

			$db = Zend_Db_Table::getDefaultAdapter();
			
			$select = $db->select()
								->from(array('a'=>'invoice_main'))
								->join(array('b'=>'invoice_detail'), 'b.invoice_main_id = a.id', array('*','idDetail'=>'b.id'))
								->joinLeft(array('rcv' => 'receipt_invoice'), 'rcv.rcp_inv_invoice_id = a.id and rcv.rcp_inv_invoice_dtl_id = b.id', array())
								->joinLeft(array('rcp' => 'receipt'), 'rcp.rcp_id = rcv.rcp_inv_rcp_id', array('*'))
								->where('a.id =?',$proforma)
								;
			$txnDataApp = $db->fetchAll($select);
				
			if($type == 1){
				$trans_id = $txnDataApp[0]['trans_id'];
			}elseif($type == 2){
				$trans_id = $txnDataApp[0]['IdStudentRegistration'];
			}
			
			
			
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			if($formData['status'] == 'S'){
		
				$upd_data = array(
					'status'=> $formData['status'],
					'remarks'=> $formData['remarks'],
					'upd_by'=> $getUserIdentity->id,
					'upd_date'=> date('Y-m-d H:i:s')
				);
				
				$this->invoiceDB->update($upd_data, array('id = ?' => $proforma) );
			}else{
				
				//exempted - auto generate cn (24/07/2015)
				if($formData['status'] == 'E'){
		
					//credit note
					$invoiceClass = new icampus_Function_Studentfinance_Invoice();
					$invoiceClass->generateCreditNoteByInvoice($trans_id,$proforma);
				}
					
				$receiptDb = new Studentfinance_Model_DbTable_Receipt();
				
				if($txnDataApp[0]['bill_paid'] > 0){
					//advance payment
					$total_advance_payment_amount = $txnDataApp[0]['bill_paid'];
					
					$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
					
					//TODO : betulkan ckit advpayment
					$advance_payment_data = array(
						'advpy_appl_id' => $txnDataApp[0]['appl_id'],
						'advpy_trans_id' => $txnDataApp[0]['trans_id'],
						'advpy_idStudentRegistration' => $trans_id,
						'advpy_rcp_id' => $txnDataApp[0]['rcp_id'],
						'advpy_description' => 'Advance payment from receipt no:'.$txnDataApp[0]['rcp_no'].' for invoice no '.$txnDataApp[0]['bill_number'],
						'advpy_cur_id' => $txnDataApp[0]['rcp_cur_id'],
						'advpy_invoice_id' => $txnDataApp[0]['invoice_main_id'],
						'advpy_amount' => $total_advance_payment_amount,
						'advpy_total_paid' => 0.00,
						'advpy_total_balance' => $total_advance_payment_amount,
						'advpy_status' => 'A'
					);
					
					$advPayID = $advancePaymentDb->insert($advance_payment_data);
					
					$advance_payment_det_data = array(
						'advpydet_advpy_id' => $advPayID,
						'advpydet_total_paid' => 0.00
					);
					
					$advancePaymentDetailDb->insert($advance_payment_det_data);
	
					//receipt update advpayment
					$receiptDb->update(array('rcp_adv_payment_amt'=>$total_advance_payment_amount, 'rcp_adv_payment_amt_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_advance_payment_amount, $txnDataApp[0]['rcp_cur_id']) ), 'rcp_id = '.$txnDataApp[0]['rcp_id']);
				
					$this->invoiceDB->update(array('bill_paid'=>0), array('id = ?' => $proforma) );
					
				}
				
				$amountCredit = 0;
				foreach($txnDataApp as $data){
					
					$upd_data_det = array(
						'cn_amount'=> $data['amount']
					);
					$amountCredit = $amountCredit + $data['amount'];
					$this->invoicedetailDB->update($upd_data_det, array('id = ?' => $data['idDetail']) );
				}
				
					//waived/exempted
					$upd_data = array(
						'status'=> $formData['status'],
						'remarks'=> $formData['remarks'],
						'cn_amount'=> $amountCredit,
						'upd_by'=> $getUserIdentity->id,
						'upd_date'=> date('Y-m-d H:i:s')
					);
					
					$this->invoiceDB->update($upd_data, array('id = ?' => $proforma) );
			
			}
				
		}
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Updated');
		$this->_redirect( $this->baseUrl . '/studentfinance/invoice');
		exit;
    }
    
	public function cancelAction(){
		
		$id = $this->_getParam('id', 0);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$info = $this->invoiceDB->getData($id);
		
		$proformainvoiceDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
		
		if($info['status'] == 'A'){
			$newStatus = 'A';
		}else{
			$newStatus = 'X';
		}		
		
		if($id != 0){
			$upd_data = array(
				'status'=> $newStatus,
				'cancel_by'=> $getUserIdentity->id,
				'cancel_date'=> date('Y-m-d H:i:s')
			);
			
			$invoiceDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoiceDb->update($upd_data, array('id = ?' => $id) );
			
			$proformaData=$proformainvoiceDb->getDataByInvoice($id);
			$proformaID = $proformaData['id'];
			$proformainvoiceDb->update(array('invoice_id'=>0), array('id = ?' => $proformaID) );
			
			
			if(isset($info['IdStudentRegistration'])){
				$transID = $info['IdStudentRegistration'];
			}else{
				$transID = $info['trans_id'];
			}

			$invoice_helper = new icampus_Function_Studentfinance_Invoice();
			$invoice_helper->generateCreditNoteByInvoice($transID,$id);

			$this->_redirect( $this->baseUrl . '/studentfinance/invoice/view/id/'.$id);
		}
	
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
		
			$proforma = $formData['invoice_id'];
			$n=0;
			while($n < count($proforma)){
				$proforma_id = $proforma[$n];
				
				$info = $this->invoiceDB->getData($proforma_id);
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
				
				/*if($info['status'] == 'A'){
					$newStatus = 'A';
				}else{
					$newStatus = 'X';
				}*/
				
				$newStatus = 'X';
			
				$upd_data = array(
					'status'=> $newStatus,
					'cancel_by'=> $getUserIdentity->id,
					'cancel_date'=> date('Y-m-d H:i:s')
				);
				
				$this->invoiceDB->update($upd_data, array('id = ?' => $proforma_id) );
				
				$proformaData=$proformainvoiceDb->getDataByInvoice($proforma_id);
				$proformaID = $proformaData['id'];
				$proformainvoiceDb->update(array('invoice_id'=>0), array('id = ?' => $proformaID) );
				
				$n++;
			}
		}
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Cancelled');
		$this->_redirect( $this->baseUrl . '/studentfinance/invoice');
		exit;
    }
    
	public function viewAction()
	{
		$this->view->title = $this->view->translate('Invoice Details');

		$id = $this->_getParam('id');

		$info = $this->invoiceDB->getData($id);
		if ( empty($info) )
		{
			throw new Exception('Invalid Invoice ID');
		}

		$subDb = new Studentfinance_Model_DbTable_InvoiceSubject();
		$details = $this->invoicedetailDB->getDetail($info['id']);
		
		foreach ($details as $key => $det)
		{
			$check = $subDb->getData($det['invoice_main_id'], $det['id']);
			if ( !empty($check) )
			{
				$details[$key]['subjects'] = $check;
			}
		}
		


		$this->view->info = $info;
		$this->view->id = $id;
		$this->view->details = $details;
	}
	
	public function addAction(){
		
		$this->view->title = $this->view->translate('Add Invoice');
		
		$payee_type_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payee Type');
		
		unset($payee_type_list[0]);
		$this->view->payee_type_list = $payee_type_list;
		
		//currency list
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$this->view->currencyList = $currencyDb->getList();
		
		
		
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
			
			/*echo "<pre>";
			print_r($formData);
			exit;*/
			
			$type = $formData['payee_type'];
			$txnId = $formData['payee_id'];
			
			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
			$registration = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
			
			$currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
			$currency = $currencyDb->getCurrentExchangeRate($formData['currency_id']);
			
			$desc = '';
			if($formData['bill_description']==''){
				$feeItemDB = new Studentfinance_Model_DbTable_FeeItem();
				$feeInfo = $feeItemDB->getData($formData['fi_id']);
				$desc = $feeInfo['fi_name'];
			}else{
				$desc = $formData['bill_description'];
			}
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			try{
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
			
				//insert invoice main
				$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
				
				$bill_no =  $this->getBillSeq(2, date('Y'));
				$data_invoice = array(
					'bill_number' => $bill_no,
					'appl_id' => 0,
					'trans_id' => 0,
					'IdStudentRegistration' => $txnId,
					'academic_year' => 0,
					'semester' => $formData['semester_id'],
					'bill_amount' => $formData['bill_amount'],
					'bill_paid' => 0,
					'bill_balance' => $formData['bill_amount'],
					'bill_description' => $desc,
					'program_id' => $registration['IdProgram'],
					'fs_id' => $registration['fs_id'],
					'currency_id' => $formData['currency_id'],
					'exchange_rate' => $currency['cr_id'],
					'invoice_date' => date('Y-m-d',strtotime($formData['invoice_date'])),
					'status' => '0',
					'proforma_invoice_id' => 0,
					'date_create'=>date('Y-m-d H:i:s')
				);
				
				$data_invoice['creator'] = $getUserIdentity->id;
				
				/*echo "<pre>";
			print_r($data_invoice);
				exit;*/
				$invoice_id = $invoiceMainDb->insert($data_invoice);
				
				//insert invoice detail
				$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
				
				$fee_item_data = $formData['fi_id'];
				
				$invoiceDetailData = array(
                    'invoice_main_id' => $invoice_id,
                    'fi_id' =>  $formData['fi_id'],
                    'fee_item_description' =>  $desc,
                    'cur_id' =>  $formData['currency_id'],
                    'amount' => $formData['bill_amount'],
					'balance' => $formData['bill_amount'],
					'exchange_rate' => $currency['cr_id']
                    //'amount_default_currency' => $fee_item_data['amount_default_currency']
				);
				
				$invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);
				
				$db->commit();
				
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Added');
				
				$this->_redirect( $this->baseUrl . '/studentfinance/invoice/view/id/'.$invoice_id);
				
			}catch (Exception $e){
        		$db->rollBack();
        		throw $e;
			}
		}
		
	}
	
public function searchPayeeAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$type = $this->_getParam('rcp_payee_type', null);
		$id = $this->_getParam('payee_id', null);
		$invoice_no = $this->_getParam('payee_invoice_no', false);
		$proforma_invoice_no = $this->_getParam('payee_proforma_no', false);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			 
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$result = null;
			
			if($formData['payee_id']!='' || $formData['payee_proforma_no']!='' || $formData['payee_invoice_no']!='' ){
			
			 
				if($formData['rcp_payee_type']==645){//applicant
					
					$select = $db->select()
								->from(array('ap'=>'applicant_profile'))
								->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at_pes_id', 'at_trans_id','fs_id'))
								->join(array('app'=>'applicant_program'), 'app.ap_at_trans_id = at.at_trans_id', array('ap_prog_id'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=app.ap_prog_id',array('program_name'=>'p.ProgramName','*'));
			
					 
					if($formData['payee_id']!=''){
						$select->where("at.at_trans_id = '".$formData['payee_id']."'");
					}
				
					 
				}else
				if($formData['rcp_payee_type']==646){ //student
			
					$select = $db->select()
								->from(array('sp'=>'student_profile'))
								->join(array('sr'=>'tbl_studentregistration'), 'sr.sp_id = sp.id', array('sr.*'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('program_name'=>'p.ProgramName','*'))
//								->where('sr.ProfileStatus = 92')
								;
			
					if($formData['payee_id']!=''){
						$select->where("sr.IdStudentRegistration = ? ",$formData['payee_id']);
					}
					
			
				}
				$row = $db->fetchRow($select);
				
				
				//get default currency
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currencyData = $currencyDb->getDefaultCurrency();
				$defaultCurrency = $currencyData['cur_id'];
					
				if($row){
				
				
					if($type == 645){
						$result = array(
							'payee_id' => $row['at_pes_id'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['appl_id'],
							'trans_id' => $row['at_trans_id'],
							'fs_id' => $row['fs_id'],
							'program_id' => $row['ap_prog_id'],
							'scheme_id' => $row['IdScheme'],
						);
					}elseif($type == 646){
						$result = array(
							'payee_id' => $row['registrationId'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['IdStudentRegistration'],
							'trans_id' => $row['IdStudentRegistration'],
							'fs_id' => $row['fs_id'],
							'program_id' => $row['IdProgram'],
							'scheme_id' => $row['IdScheme'],
						);
					}
				
				
					//search invoice
					$select_invoice = $db->select()
					->from(array('i'=>'invoice_main'),array('*','fee_item_description'=>'bill_description','amount'=>"bill_amount",'paid'=>'bill_paid','balance'=>'bill_balance','status'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = i.semester', array('SemesterName'=>'SemesterMainName'))
					//->join(array('pid'=>'invoice_detail'),'pid.invoice_main_id = i.id', array('fee_item_description'=>'pid.fee_item_description','amount','amount_default_currency','paid','balance'))
					;
//					->where("i.status = 'A'")  
//					->where("i.bill_balance > '0.00'");
					//->group('pid.invoice_main_id');//remove paid invoice

					if($type == 645){
						$select_invoice->where('i.appl_id = ?', $result['appl_id'])
								->where('i.trans_id = ?', $result['trans_id']);
					}elseif($type == 646){
						$select_invoice->where('i.IdStudentRegistration= ?', $result['trans_id']);
					}
					
				
					$invoice = $db->fetchAll($select_invoice);
					
					$totalMyr=0;
					$totalUsd=0;
					$m=0;
					foreach($invoice as $inv){
						$result['invoice'][$m] = $inv;
						
						//get current currency rate
						$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
						

						$cur_def = 0;
						if($defaultCurrency != $inv['currency_id']){
							$rate = $currencyRateDb->getData($inv['exchange_rate']);
							$cur_def = 1;
							$total_invoice_amount_default_currency = round($inv['amount'] * $rate['cr_exchange_rate'],2);
							$result['invoice'][$m]['amount_myr'] = $total_invoice_amount_default_currency;
							$result['invoice'][$m]['amount_usd'] = $inv['amount'];
							$totalMyr += round($inv['bill_balance'] * $rate['cr_exchange_rate'],2);
							$totalUsd += $inv['bill_balance'];
							
						
						}else{
							$rate = $currencyRateDb->getCurrentExchangeRate(2);
							$cur_def = 0;
							$total_invoice_amount_default_currency = round($inv['bill_amount'] / $rate['cr_exchange_rate'],2);
							$result['invoice'][$m]['amount_myr'] = $inv['amount'];
							$result['invoice'][$m]['amount_usd'] = $total_invoice_amount_default_currency;
							$totalMyr += $inv['bill_balance'];
							$totalUsd +=  round($inv['bill_balance'] / $rate['cr_exchange_rate'],2);
							
						}

						$result['invoice'][$m]['cur_def'] = $cur_def;
						$result['invoice'][$m]['amount_default'] = $inv['amount'];
						
						$stsinv = '';
						if( $inv['status'] == 'A'){
							$stsinv = 'Active';
						}elseif( $inv['status'] == 'X'){
							$stsinv = 'Cancel';
						}elseif( $inv['status'] == 'W'){
							$stsinv = 'Waived';
						}elseif( $inv['status'] == 'E'){
							$stsinv = 'Exempted';
						}elseif( $inv['status'] == 'S'){
							$stsinv = 'Sponsorship';
						}elseif( $inv['status'] == '0'){
							$stsinv = 'Entry';
						}
						
						$result['invoice'][$m]['status'] = $stsinv;
						
						$m++;
					}
				
				}
				
				$result['inv_total_myr'] = $totalMyr;
				$result['inv_total_usd'] = $totalUsd;
				
				//get current currency rate
				$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
				$dataRate  = $currencyRateDb->getCurrentExchangeRate(2);		
				$result['current_rate'] = $dataRate['cr_exchange_rate'];
		
			}
			
//			echo "<pre>";
//			print_r($result);
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($result);
				
			echo $json;
			exit();
		
		}
	}
	
	public function ajaxGetFeeItemAction(){
    
    	$fs_id = $this->_getParam('id', 0);
    	
    	if($fs_id==0){
    		echo "No Fee Structure";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	//fee structure item
    	/*$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
    	$row = $feeStructureItemDb->getStructureData($fs_id);*/
    	
    	$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$row = $feeItemDb->getActiveFeeItem();
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($row);
		
		$this->view->json = $json;
		exit;
    }
    
    public function ajaxGetSemesterAction(){
    
    	$scheme_id = $this->_getParam('id', 0);
    	
    	if($scheme_id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	
    	$semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$row = $semesterDb->fnSemesterList($scheme_id);
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($row);
		
		$this->view->json = $json;
		exit;
    }
    
	public function ajaxGetInvoiceAction(){
    
    	$id = $this->_getParam('id', 0);
    	$type = $this->_getParam('type', 0);
    	
    	if($id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select_invoice = $db->select()
					->from(array('d'=>'invoice_detail'),array('amount','fee_item_description','id'))//lalalal
					->join(array('i'=>'invoice_main'),'i.id = d.invoice_main_id', array('bill_number'))
					->joinLeft(array('is'=>'invoice_subject'),'is.invoice_main_id = i.id and is.invoice_detail_id = d.id', array('is.subject_id'))
					->joinLeft(array('s'=>'tbl_subjectmaster'),'is.subject_id = s.IdSubject', array('SubCode'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("i.status = 'A'")
//					->where("i.bill_balance > 0")
					->order('i.invoice_date desc')
					->order('i.id desc');
					if($type == 645){
						$select_invoice->where('i.trans_id = ?',$id);
					}else{
						$select_invoice->where('i.IdStudentRegistration = ?',$id);
					}
					
		$invoice = $db->fetchAll($select_invoice);
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($invoice);
		
		$this->view->json = $json;
		exit;
    }
    
	public function approveAction(){
		
		$id = $this->_getParam('id', 0);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		if($id != 0){
			$upd_data = array(
				'status'=> 'A',
				'approve_by'=> $getUserIdentity->id,
				'approve_date'=> date('Y-m-d H:i:s')
			);
			
			$invoiceDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoiceDb->update($upd_data, array('id = ?' => $id) );
				
			
		}
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Approved');
		$this->_redirect( $this->baseUrl . '/studentfinance/invoice/view/id/'.$id);
		exit;
    }
    
/*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
	
	public function generateInvoiceAction(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
//			echo "<pre>";
//			print_r($formData);
			
			$idSelect = $formData['id'];
			$n = count($idSelect);
			$i=0;
			while($i<$n){
				
				$id = $idSelect[$i];
				
				$select = $db->select()
							->from(array('a'=>'tbl_studentregsubjects_detail'),array('id','subject_id','semester_id','item_id','invoice_id','created_date','created_role','semester_id','student_id'))
							->join(array('d' => 'tbl_studentregsubjects'), 'd.IdStudentRegSubjects = a.regsub_id',array('Active','credit_hour_registered','IdCourseTaggingGroup'))
							->join(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration',array('registrationId','IdStudentRegistration','fs_id','IdBranch','IdProgramScheme'))
							->join(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = b.IdProgramScheme',array('mode_of_program'))
							
							->where('a.id = ?',$id)
							->where('a.invoice_id is null');
				$results = $db->fetchRow($select);	
				
				if($results){

					if($results['Active'] == 9){//pre-repeat
						$type = 'RPT';
					}elseif($results['Active'] == 0){//pre-registered
						$type = '';
					}
					
					$credithour = '';
					if(!isset($results['credit_hour_registered'])){
						$credithour = $results['credit_hour_registered'];
					}
					
					//get course section
					$selectTagging = $db ->select()
					  ->from(array('cg'=>'tbl_course_tagging_group'))
					  ->joinLeft(array('cgp'=>'course_group_program'),'cg.`IdCourseTaggingGroup` = cgp.group_id')
//					   ->joinLeft(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = cgp.program_scheme_id')
					  ->where('cg.IdCourseTaggingGroup = ?',$results['IdCourseTaggingGroup']);
					  
					$resultsTagging = $db->fetchRow($selectTagging);	
					$modeProgramNew = 0;
					if($resultsTagging){
						$modeProgram = $resultsTagging['program_scheme_id'];  
						if($results['IdProgramScheme'] != $modeProgram){
							$modeProgramNew = $modeProgram;
						}
					}
				
					$invoiceClass = new icampus_Function_Studentfinance_Invoice();
					$invoiceClass->generateInvoiceStudent($results['student_id'],$results['semester_id'],$results['subject_id'],$type,$credithour,1,$modeProgramNew);
				}
				
				$i++;
				
			}
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Generated');
			$this->_redirect( $this->baseUrl . '/studentfinance/migrate/invoice-not-generated');
			
		}
		
		
		
	}
	
	public function generateInvoiceByBalanceAction(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
//			echo "<pre>";
//			print_r($formData);
			
			$idSelect = $formData['id'];
			$n = count($idSelect);
			$i=0;
			while($i<$n){
				
				$id = $idSelect[$i];
				
				$arraySelected = explode(':',$id);
				
				$invoiceID = $arraySelected[0];
				$amount = $arraySelected[1];
				
				$select = $db->select()
							->from(array('d'=>'invoice_detail'))
							->join(array('a' => 'invoice_main'), 'd.invoice_main_id = a.id',array('*'))
							->join(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id = a.id and ivs.invoice_detail_id = d.id',array('*'))
							->where('d.id = ?',$invoiceID);
				$results = $db->fetchRow($select);	
				
				if($results){
					
					$curID = $results['currency_id'];
					$feeID = $results['fi_id'];
					$semesterID = $results['semester'];

					if($amount > 0){
						$invoiceClass = new icampus_Function_Studentfinance_Invoice();
						$invoiceClass->generateInvoice($results['IdStudentRegistration'],$feeID,$semesterID,$amount,$curID,$results['subject_id']);//$studentID,$feeID,$semesterID,$amount=0,$curID=0
					}
				}
				
				$i++;
				
			}
			
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Generated');
			$this->_redirect( $this->baseUrl . '/studentfinance/migrate/wrong-invoice-generated');
			
		}
		
		
		
	}
	
public function generateInvoiceAuditAction(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
//			echo "<pre>";
//			print_r($formData);
			
			$idSelect = $formData['id'];
			$n = count($idSelect);
			$i=0;
			$levelArray = array(0,1);
			while($i<$n){
				
				$id = $idSelect[$i];
				
				$select = $db->select()
							->from(array('a'=>'tbl_auditpaperapplicationitem'),array('id'=>'api_id','item_id'=>'api_item','api_invid_0','api_invid_1'))
							->join(array('d' => 'tbl_auditpaperapplicationcourse'), 'a.api_apc_id = d.apc_id',array('subject_id'=>'apc_courseid','apc_program','apc_programscheme'))
							->join(array('da' => 'tbl_auditpaperapplication'), 'da.apa_id = a.api_apa_id',array('created_date'=>'apa_upddate','apa_semesterid'))
							->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = da.apa_studregid',array('registrationId','IdStudentRegistration','fs_id','IdBranch','IdProgramScheme','IdProgram','IdIntake','student_type'))
							->join(array('sa'=>'student_profile'), 'sa.id = b.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname",'appl_category'))
							->joinLeft(array('cp'=>'tbl_program'), 'b.IdProgram=cp.IdProgram',array('IdScheme'))
							->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch',array())
							->where('a.api_id = ?',$id)
							->where('br.invoice = 1');
				$results = $db->fetchRow($select);	
				
				if($results){
					
					//get audit paper fee structure
					
						//get current intake
					$auditDB = new Records_Model_DbTable_Auditpaper();
					
					//get current sem
	                $curSem = $auditDB->getCurSem($results['IdScheme']);
	                    
	                $curIntake = $auditDB->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);
                
					 $auditDB = new Records_Model_DbTable_Auditpaper();
	                 $auditPaperFsId = $auditDB->getApplicantFeeStructure($results['apc_program'], $results['apc_programscheme'], $curIntake['IdIntake'], $results['appl_category'], 0);
                 
				if ($results['student_type'] == 740){//normal
					if ($results['IdProgramScheme'] != $results['apc_programscheme']){
                        $fsid = $auditPaperFsId['fs_id'];
                    }else{
                        $fsid = 0;
                    }
                  }else{//741 visiting
                  	$fsid = 0;
                  }
                  
					foreach($levelArray as $level){
						$invoiceClass = new icampus_Function_Studentfinance_Invoice();
						$invoiceid = $invoiceClass->generateOtherInvoiceStudent($results['IdStudentRegistration'],$results['apa_semesterid'],'AU',$results['subject_id'],$level,$fsid,$results['item_id']);
						
						if($level == 0){
							 $invData = array(
	                            'api_invid_0'=>$invoiceid
	                        );
						}elseif($level == 1){
							 $invData = array(
	                            'api_invid_1'=>$invoiceid
	                        );
						}
						if($invData){
                        	$auditDB->updateCourseItem($invData, $id);
						}
					}
				}
				
				$i++;
				
			}
			
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Audit Invoice Generated');
			$this->_redirect( $this->baseUrl . '/studentfinance/migrate/invoice-audit-not-generated');
			
		}
		
		
		
	}
	
public function ajaxGetInvoiceMigrateAction(){
    
    	$id = $this->_getParam('id', 0);
    	$type = $this->_getParam('type', 0);
    	
    	if($id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select_invoice = $db->select()
					->from(array('d'=>'invoice_detail'),array('amount','fee_item_description','id'))
					->join(array('i'=>'invoice_main'),'i.id = d.invoice_main_id', array('bill_number','status','invoice_date'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("i.status = 'A'")
					->where("i.MigrateCode is NOT NULL")
					->order('i.invoice_date desc');
					if($type == 645){
						$select_invoice->where('i.trans_id = ?',$id);
					}elseif($type == 646){
						$select_invoice->where('i.IdStudentRegistration = ?',$id);
					}
		$invoice = $db->fetchAll($select_invoice);
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($invoice);
		
		$this->view->json = $json;
		exit;
    }
    
	public function addtagAction(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$id = $this->_getParam('id', 0);

		if ($id != 0){
			$data = array(
				'tag_to_open_payment'=>1
			);
			$db->update('invoice_main', $data, 'id = '.$id);

			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Tagged');
			$this->_redirect( $this->baseUrl . '/studentfinance/invoice');
		}else{
			$this->_redirect( $this->baseUrl . '/studentfinance/invoice');
		}
	}

	public function removetagAction(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$id = $this->_getParam('id', 0);

		if ($id != 0){
			$data = array(
				'tag_to_open_payment'=>0
			);
			$db->update('invoice_main', $data, 'id = '.$id);

			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Tagged Removed');
			$this->_redirect( $this->baseUrl . '/studentfinance/invoice');
		}else{
			$this->_redirect( $this->baseUrl . '/studentfinance/invoice');
		}
	}
}