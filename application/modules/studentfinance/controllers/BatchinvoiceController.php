<?php
class Studentfinance_BatchinvoiceController extends Base_Base { //Controller for the User Module

	private $lobjInvoiceForm;
	private $lobjInvoiceModel;
	private $lobjprogram;
	private $_gobjlog;
	
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
		 
	}
	
	public function fnsetObj(){
		$this->lobjBatchinvoiceform = new Studentfinance_Form_Batchinvoice();
		$this->lobjform = new App_Form_Search (); 
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();		
		$this->lobjDeparmentmaster= new GeneralSetup_Model_DbTable_Departmentmaster();
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjBatchinvoiceModel = new Studentfinance_Model_DbTable_Batchinvoice();
		$this->lobjdeftype = new App_Model_Definitiontype();
		
		$this->lobjInvoiceModel = new Studentfinance_Model_DbTable_Invoice();
		$this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjInvoiceForm = new Studentfinance_Form_Invoice();	
		$this->lobjAccountmaster = new Studentfinance_Model_DbTable_Accountmaster();	
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		

	}
	
   public function indexAction() {
			
   	
   	$this->view->lobjform = $this->lobjform;
   	$this->view->lobjBatchinvoiceform = $this->lobjBatchinvoiceform;
			
		$lobjProgramNameList = $this->lobjAcademicstatus->fnGetProgramNameList();
		$this->view->lobjform->field13->addMultiOptions($lobjProgramNameList);	

		$lobjProgramNameList = $this->lobjAcademicstatus->fnGetProgramNameList();
		$this->view->lobjBatchinvoiceform->Program->addMultiOptions($lobjProgramNameList);	
		
		$lobjSemesterNameList = $this->lobjAcademicstatus->fnGetSemesterNameList();
		$this->view->lobjform->field10->addMultiOptions($lobjSemesterNameList);
		
		$currentsemester = $this->lobjSemesterModel->fngetidsemesterstatus();
        $this->view->lobjform->field10->setValue($currentsemester);
        
        $lobjSemesterNameList = $this->lobjAcademicstatus->fnGetSemesterNameList();
		$this->view->lobjBatchinvoiceform->Semester->addMultiOptions($lobjSemesterNameList);
		
        $currentsemester = $this->lobjSemesterModel->fngetidsemesterstatus();
        $this->view->lobjBatchinvoiceform->Semester->setValue($currentsemester);
        
		
		$lobjCollegeNameList = $this->lobjDeparmentmaster->fnGetCollegeList();
		$this->view->lobjform->field5->addMultiOptions($lobjCollegeNameList);
		$this->view->lobjform->field5->setAttrib('OnChange', 'fnGetProgramsearch(this.value)');
		$lobjCollegeNameList = $this->lobjDeparmentmaster->fnGetCollegeList();
		$this->view->lobjBatchinvoiceform->Faculty->addMultiOptions($lobjCollegeNameList);
		
		$studentdropdown = $this->lobjBatchinvoiceModel->fngetStudentDropdown();
		$this->view->lobjform->field1->addMultiOptions($studentdropdown);
			
   		$TypeOfMode = $this->lobjdeftype->fnGetDefinationMs('Mode');
		$this->view->lobjform->field20->addMultiOptions($TypeOfMode);
		
		$larrresultitemgroup = $this->lobjInvoiceModel->fngetitemgroup();
		$this->view->lobjBatchinvoiceform->idAccount->addMultiOptions($larrresultitemgroup);	
		
		$larrresult = $this->lobjBatchinvoiceModel->fngetStudentApplicationDetails();
				
		$auth = Zend_Auth::getInstance();
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->batchinvoicepaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->batchinvoicepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->batchinvoicepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {				
				$larrresult = $this->lobjBatchinvoiceModel->fnSearchStudentApplication( $this->lobjform->getValues () ); //searching the values for the user
                 $this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->batchinvoicepaginatorresult = $larrresult;
			}
		}
		
   if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				
				
				$totamt=array_sum($larrformData['Amountgrid']);	
				
				for($i=0;$i<count($larrformData['Idstudentregistration']);$i++){
					$larrformdatainsert['IdStudent']=$larrformData['Idstudentregistration'][$i];
					$larrformdatainsert['IdSemester']=$larrformData['semestergrid'][$i];
					$larrformdatainsert['InvoiceAmt']= $totamt;		 
					$larrformdatainsert['InvoiceDt']= date('Y-m-d');
					$larrformdatainsert['UpdDate']= date('Y-m-d H:i:s');
					$auth = Zend_Auth::getInstance();
					$larrformdatainsert['UpdUser']= $auth->getIdentity()->iduser;
					$larrformdatainsert['AcdmcPeriod']= 0;									
					$larrformdatainsert['Active']= 1;
					$larrformdatainsert['Approved']= 0;
					$larrformdatainsert['idsponsor']= 0;
					$larrformdatainsert['InvoiceNo']= 123;									
						
					$larrresultId = $this->lobjBatchinvoiceModel->fnAddbatchInvoice($larrformdatainsert);
			        $this->lobjstudentapplication->fnGenerateCodes($this->gobjsessionsis->idUniversity,$larrresultId,$larrformData['Idstudentregistration'][$i]);
			        for($j=0;$j<count($larrformData['idAccountgrid']);$j++){
			        	 $larrformdatadetails['idAccount']=$larrformData['idAccountgrid'][$j];
			        	 $larrformdatadetails['Amount']=$larrformData['Amountgrid'][$j];	
			        	 $larrformdatadetails['Description']=$larrformData['Descriptiongrid'][$j];		        	 
			        	 $larrformdatadetails['IdInvoice']=$larrresultId;
			        		$larrformdatadetails['UpdDate']= date('Y-m-d H:i:s');
							$auth = Zend_Auth::getInstance();
							$larrformdatadetails['UpdUser']= $auth->getIdentity()->iduser;
							$larrformdatadetails['Active']=0;
							$this->lobjBatchinvoiceModel->fnAddbatchInvoiceDetails($larrformdatadetails);
			        }
				}
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Batch Invoice Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				
				 $this->_redirect( $this->baseUrl . '/studentfinance/batchinvoice/index');
                 
			}
		}
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/batchinvoice/index');
		}
		
	}
	
	public function fngetprogramAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idCollege = $this->_getParam('idCollege');		
		$larrProgramDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjBatchinvoiceModel->fnGetProgram($idCollege));
		echo Zend_Json_Encoder::encode($larrProgramDetails);
	}

}