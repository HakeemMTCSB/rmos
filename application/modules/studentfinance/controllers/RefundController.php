<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_RefundController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_Refund();
		$this->_DbObj = $db;
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() {
			
		//title
    	$this->view->title= $this->view->translate("Refund");
    	
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$p_data = $this->_DbObj->getPaginateListData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
	
	    	$this->view->paginator = $paginator;
			$this->view->formData = $formData;
			
			$this->view->type = $formData['type'];

		}
		
	}
	
	public function approvalAction(){
		$this->_helper->layout()->disableLayout();
		
		//paginator
		$data = $this->_DbObj->getPaginateData();
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function detailAction(){
		$id = $this->_getParam('id', null);
		
		//title
    	$this->view->title= $this->view->translate("Refund Process : Detail");
    	
    	//data
    	$data = $this->_DbObj->getData($id);
    	$this->view->data = $data;
    	
    	//profile
    	$profileDb = new App_Model_Application_DbTable_ApplicantProfile();
    	$profile = $profileDb->getData($data['rfd_appl_id']);
    	$this->view->profile = $profile;
    	
    	//bank list
    	$definationDb = new App_Model_General_DbTable_Definationms();
    	$this->view->bank_list = $definationDb->getDataByType(88);
    	
    	//refund data
    	$refund_data = $this->_DbObj->getData($id);
    	
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//advance payment
			$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
			$advancePaymentData = $advancePaymentDb->getApplicantAvdPayment($data['rfd_appl_id']);
			$this->view->advance_payment_data = $advancePaymentData;
			
    		//loop each advance payment
			$refund_amount = $refund_data['rfd_amount'];
			
			$i=0;
			$j=0;
			$arr_adv_used = array();
			while($refund_amount>0 && $i<= sizeof($advancePaymentData) ){
					
				$advpymt = $advancePaymentData[$i];
				
				$adv_used = 0;
				$adv_balance = $advpymt['advpy_total_balance'];
				
				if($refund_amount>$advpymt['advpy_total_balance']){
					
					$adv_used = $advpymt['advpy_total_balance'];
					$adv_balance -= $adv_used;
					$refund_amount -= $adv_used;
					
				}else{
					
					$adv_used = $refund_amount;
					$refund_amount -= $adv_used;
					$adv_balance -= $adv_used;
				}
				
				if($adv_used>0){
					
					$arr_adv_used[$j] = array(
						'advpy_id'=> $advpymt['advpy_id'],
						'advpy_total_paid' => $adv_used,
						'advpy_total_balance' => $adv_balance	
					);
					
					$j++;
				}
				
				$i++;
			}
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			try {
				//add refund cheque
				$data = array(
					'rchq_cheque_no' => $formData['rchq_cheque_no'],
					'rchq_bank_name' => $formData['rchq_bank_name'],
					'rchq_amount' => $formData['rchq_amount'],
					'rchq_issue_date' => date('Y-m-d',strtotime($formData['cheque_issue_date'])),
				);
					
				$refundChequeDb = new Studentfinance_Model_DbTable_RefundCheque();
				$cheque_id = $refundChequeDb->insert($data);
		
				
				//update refund
				$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
				$currentAcademicYear = $academicYearDb->getCurrentAcademicYearData();
								
				$auth = Zend_Auth::getInstance();
				$data_upd = array(
						'rfd_approver_id' => $auth->getIdentity()->iduser,
						'rfd_approve_date' => date('Y-m-d H:i:s'),
						'rdf_refund_cheque_id' => $cheque_id
				);
				
				$this->_DbObj->update($data_upd,'rfd_id = '.$id);
				
				/*
				 * update advance payment
				 */
				
				foreach ($arr_adv_used as $advused){
					
					//insert advance payment detail
					$advPmtDtlDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
					$data = array(
							'advpydet_advpy_id' => $advused['advpy_id'],
							'advpydet_refund_cheque_id' => $cheque_id,
							'advpydet_total_paid' => $advused['advpy_total_paid']
					);
					
					$advPmtDtlDb->insert($data);
					
					//update advance payment main balance amount
					$avdPmtDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$avdMainData = $avdPmtDb->getData($advused['advpy_id']);
					
					$data = array(
							'advpy_total_paid' => ($avdMainData['advpy_total_paid'] + $advused['advpy_total_paid']),
							'advpy_total_balance' => ($avdMainData['advpy_total_balance'] - $advused['advpy_total_paid'])
					);
					
					$avdPmtDb->update($data, 'advpy_id = '.$advused['advpy_id']);
				}
				
						
	    	   	//$db->rollback();
				$db->commit();
			    	    
    	    }catch (exception $e) {
			    $db->rollback();
			    echo "<pre>";
			    echo $e->getMessage();
				print_r($e->getTrace());
				echo "</pre>";
				exit;
			}		
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'refund'),'default',true));
			
    	}
    
	}
	
	public function historyAction(){
		$this->_helper->layout()->disableLayout();
		
		//paginator
		$data = $this->_DbObj->getPaginateData(true);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	
	public function detailApplicantAction(){
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Refund Detail");
    	
    	//applicant info
    	$applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
    	$this->view->profile = $applicantProfileDb->getData($id);
    	
	}
	
	public function historyApplicantAction(){
		$this->_helper->layout()->disableLayout();
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Refund History");
    	
    	$data = $this->_DbObj->getApplicantRefund($id);
    	$this->view->data = $data;
		
	}
	
	public function detailStudentAction(){
		
	}
	
	public function historyStudentAction(){
		
	}

	public function processRefundAction(){
		
	}
	
	public function chequeDetailAction(){
		if($this->_request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();
		}
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Refund : Cheque Detail");
    	
    	$chequeRefundDb = new Studentfinance_Model_DbTable_RefundCheque();
    	$data = $chequeRefundDb->getData($id);
    	
    	$this->view->data = $data;
	}
	
	public function chequeUpdateAction(){
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$auth = Zend_Auth::getInstance();
			
			$data = array(
						'rchq_collector_name' => $formData['collector_name'],
						'rchq_collector_date' => date('Y-m-d H:i:s'),
						'rchq_collector_id' => $formData['collector_id'],
						'rchq_collector_update_by' => $auth->getIdentity()->iduser
					);
			
			$refundChecqueDb = new Studentfinance_Model_DbTable_RefundCheque();
			$refundChecqueDb->update($data, 'rchq_id = '.$formData['rchq_id']);
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'refund', 'action'=>'cheque-detail', 'id'=>$formData['rchq_id']),'default',true));
			
		}
	}
	
	public function entryAction(){
		//title
		$this->view->title= $this->view->translate("Refund : Entry");
		
	}
	
	public function entryDetailAction(){
		//title
		$this->view->title= $this->view->translate("Refund : Entry");
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		//applicant info
		$applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
		$this->view->profile = $applicantProfileDb->getData($id);
		
		//advance payment
		$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
		$advancePaymentData = $advancePaymentDb->getApplicantAvdPayment($id);
		$this->view->advance_payment_data = $advancePaymentData;
		
		//sum balance
		$total_balance = 0;
		for($i=0; $i<sizeof($this->view->advance_payment_data); $i++){
			$total_balance += $this->view->advance_payment_data[$i]['advpy_total_balance'];
		}
		
		$this->view->advance_payment_balance = $total_balance;
		
		//bank list
		$definationDb = new App_Model_General_DbTable_Definationms();
		$this->view->bank_list = $definationDb->getDataByType(88);
		
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
							
			//loop each advance payment
			$refund_amount = $formData['rfd_amount'];
			
			$i=0;
			$j=0;
			$arr_adv_used = array();
			while($refund_amount>0 && $i<= sizeof($advancePaymentData) ){
					
				$advpymt = $advancePaymentData[$i];
				
				$adv_used = 0;
				$adv_balance = $advpymt['advpy_total_balance'];
				
				if($refund_amount>$advpymt['advpy_total_balance']){
					
					$adv_used = $advpymt['advpy_total_balance'];
					$adv_balance -= $adv_used;
					$refund_amount -= $adv_used;
					
				}else{
					
					$adv_used = $refund_amount;
					$refund_amount -= $adv_used;
					$adv_balance -= $adv_used;
				}
				
				if($adv_used>0){
					
					$arr_adv_used[$j] = array(
						'advpy_id'=> $advpymt['advpy_id'],
						'advpy_total_paid' => $adv_used,
						'advpy_total_balance' => $adv_balance	
					);
					
					$j++;
				}
				
				echo $refund_amount."<br />";
				$i++;
			}
			
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			try {
				//add refund cheque
				/*$data = array(
				 'rchq_cheque_no' => $formData['rchq_cheque_no'],
						'rchq_bank_name' => $formData['rchq_bank_name'],
						'rchq_amount' => $formData['rchq_amount'],
						'rchq_issue_date' => date('Y-m-d',strtotime($formData['cheque_issue_date'])),
				);
					
				$refundChequeDb = new Studentfinance_Model_DbTable_RefundCheque();
				$cheque_id = $refundChequeDb->insert($data);*/
		
				
				//add refund
				$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
				$currentAcademicYear = $academicYearDb->getCurrentAcademicYearData();
				
				$data = array(
						'rfd_appl_id' => $id,
						'rfd_acad_year_id' => $currentAcademicYear!=null?$currentAcademicYear['ay_id']:0,
						'rfd_sem_id' => null,
						'rfd_fomulir' => $advancePaymentData[0]['advpy_fomulir'],
						'rfd_desc' => $formData['rfd_desc'] ,
						'rfd_amount' => $formData['rfd_amount'],
						'rdf_refund_cheque_id' => null,
						'rfd_avdpy_id' => $advancePaymentData[0]['advpy_id'],
				);
				
				$this->_DbObj->insert($data);
				
				/*
				 * update advance payment
				 */
				
				foreach ($arr_adv_used as $advused){
					
					//insert advance payment detail
					$advPmtDtlDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
					$data = array(
							'advpydet_advpy_id' => $advused['advpy_id'],
							'advpydet_refund_cheque_id' => $cheque_id,
							'advpydet_total_paid' => $advused['advpy_total_paid']
					);
					
					$advPmtDtlDb->insert($data);
					
					//update advance payment main balance amount
					$avdPmtDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$avdMainData = $avdPmtDb->getData($advused['advpy_id']);
					
					$data = array(
							'advpy_total_paid' => ($avdMainData['advpy_total_paid'] + $advused['advpy_total_paid']),
							'advpy_total_balance' => ($avdMainData['advpy_total_balance'] - $advused['advpy_total_paid'])
					);
					
					$avdPmtDb->update($data, 'advpy_id = '.$advused['advpy_id']);
				}
				
				
				$db->commit();
		
			} catch (exception $e) {
				$db->rollback();
				echo "<pre>";
				echo $e->getMessage();
				print_r($e->getTrace());
				echo "</pre>";
				exit;
			}
			
			$this->view->noticeSuccess = "Submitted. Please check refund history";
		}
				
	}
	
public function addAction(){
		
		$this->view->title = $this->view->translate('Add Refund');
		
		$payee_type_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payee Type');
		
		$this->view->processtype = $this->lobjdeftype->fnGetDefinationsByLocale('Refund Payment Mode');
		$this->view->refundtype = $this->lobjdeftype->fnGetDefinationsByLocale('Refund Type');
		
//		unset($payee_type_list[0]);
		$this->view->payee_type_list = $payee_type_list;
		
		//currency list
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$this->view->currencyList = $currencyDb->getList();
		
		
		
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
			//var_dump($formData); exit;
			$type = $formData['payee_type'];
			$txnId = $formData['payee_id'];
			$adv_id = $formData['adv_id'];
			$amount = $formData['amount'];
			
			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
			$registration = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
			
			
			$_DbObj = new Studentfinance_Model_DbTable_AdvancePayment();
			$advData = $_DbObj->getData($adv_id);
			
			$desc = '';
			if($formData['bill_description']==''){
				$desc = $advData['advpy_description'];
			}else{
				$desc = $formData['bill_description'];
			}
				
			
			$studentid_new = $formData['studentid_new'];
			$applicantid_new = $formData['applicantid_new'];

			
				//refund
				
				$dataArray = array(
					'amount'=>$amount,
					'refundtype'=>$formData['refundtype'],
					'processtype'=>$formData['processtype'],
					'studentid_new'=>$studentid_new,
					'applicantid_new'=>$applicantid_new,
					'desc'=>$desc,
					'doc_date'=>$formData['adv_date'],
					'payto'=>$formData['payto'],
					'payname'=>$formData['pay_name'],
					'rchq_cheque_no'=>$formData['rchq_cheque_no'],
					'rchq_bank_name'=>$formData['rchq_bank_name'],
					'rchq_issue_date'=>$formData['rchq_issue_date'],
				);
	//			echo "<pre>";
	//			print_r($dataArray);
	//			exit;
				$db = Zend_Db_Table::getDefaultAdapter();
				$db->beginTransaction();
				try{
					$invoiceClass = new icampus_Function_Studentfinance_Invoice();
					$rfd_id = $invoiceClass->generateRefundEntry($txnId,$adv_id,$dataArray);
					
					$db->commit();
				
					if($rfd_id){
						$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Refund Added');
						$this->_redirect( $this->baseUrl . '/studentfinance/refund/view/id/'.$rfd_id);
					}else{
						$this->gobjsessionsis->flash = array('type'=>'error','message'=>'Refund Entry Unsuccessful');
						$this->_redirect( $this->baseUrl . '/studentfinance/refund/add');
					}
					
					exit;
					
				}catch (Exception $e){
	        		$db->rollBack();
	        		throw $e;
				}
			}
		
		
	}
	
public function searchPayeeAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$type = $this->_getParam('rcp_payee_type', null);
		$id = $this->_getParam('payee_id', null);
		$invoice_no = $this->_getParam('payee_invoice_no', false);
		$proforma_invoice_no = $this->_getParam('payee_proforma_no', false);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			 
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$result = null;
			
			if($formData['payee_id']!='' || $formData['payee_proforma_no']!='' || $formData['payee_invoice_no']!='' ){
			
			 
				if($formData['rcp_payee_type']==645){//applicant
					
					$select = $db->select()
								->from(array('ap'=>'applicant_profile'))
								->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at_pes_id', 'at_trans_id','fs_id'=>'at_fs_id'))
								->join(array('app'=>'applicant_program'), 'app.ap_at_trans_id = at.at_trans_id', array('ap_prog_id'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=app.ap_prog_id',array('program_name'=>'p.ProgramName','*'));
			
					 
					if($formData['payee_id']!=''){
						$select->where("at.at_trans_id = '".$formData['payee_id']."'");
					}
				
					 
				}else
				if($formData['rcp_payee_type']==646){ //student
			
					$select = $db->select()
								->from(array('sp'=>'student_profile'))
								->join(array('sr'=>'tbl_studentregistration'), 'sr.sp_id = sp.id', array('sr.*'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('program_name'=>'p.ProgramName','*'));
								//->where('sr.ProfileStatus = 92');
			
					if($formData['payee_id']!=''){
						$select->where("sr.IdStudentRegistration = ? ",$formData['payee_id']);
					}
					
			
				}
				$row = $db->fetchRow($select);
				
				
				//get default currency
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currencyData = $currencyDb->getDefaultCurrency();
				$defaultCurrency = $currencyData['cur_id'];
					
				if($row){
				
				
					if($type == 645){
						$result = array(
							'payee_id' => $row['at_pes_id'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['appl_id'],
							'trans_id' => $row['at_trans_id'],
							'fs_id' => $row['fs_id'],
							'program_id' => $row['ap_prog_id'],
							'scheme_id' => $row['IdScheme'],
						);
					}elseif($type == 646){
						$result = array(
							'payee_id' => $row['registrationId'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['IdStudentRegistration'],
							'trans_id' => $row['IdStudentRegistration'],
							'fs_id' => $row['fs_id'],
							'program_id' => $row['IdProgram'],
							'scheme_id' => $row['IdScheme'],
						);
					}
				
				
					//search invoice
					$select_invoice = $db->select()
					->from(array('i'=>'refund'),array('*','fee_item_description'=>'rfd_desc','amount'=>"rfd_amount",'status'=>'rfd_status'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.rfd_currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("i.rfd_status = 'E'")
					;

					if($type == 645){
						$select_invoice->where('i.rfd_trans_id = ?', $result['trans_id']);
					}elseif($type == 646){
						$select_invoice->where('i.rfd_IdStudentRegistration= ?', $result['trans_id']);
					}
					
				
					$invoice = $db->fetchAll($select_invoice);
					
					$totalMyr=0;
					$totalUsd=0;
					$m=0;
					foreach($invoice as $inv){
						$result['invoice'][$m] = $inv;
						

						$result['invoice'][$m]['cur_def'] = $inv['rfd_currency_id'];
						$result['invoice'][$m]['amount_default'] = $inv['amount'];
						
						$stsinv = '';
						if( $inv['rfd_status'] == 'A'){
							$stsinv = 'Active';
						}elseif( $inv['rfd_status'] == 'X'){
							$stsinv = 'Cancel';
						}elseif( $inv['rfd_status'] == 'E'){
							$stsinv = 'Entry';
						}
						
						$result['invoice'][$m]['status'] = $stsinv;
						
						$m++;
					}
				
				}
		
			}
			
//			echo "<pre>";
//			print_r($result);
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($result);
				
			echo $json;
			exit();
		
		}
	}
	
	public function viewAction()
	{
		$this->view->title = $this->view->translate('Refund Details');

		$id = $this->_getParam('id');

		$info = $this->_DbObj->getData($id);
		
		$refundChequeDb = new Studentfinance_Model_DbTable_RefundCheque();
		$chequeinfo = $refundChequeDb->getDataByRefundId($id);
		if ( empty($info) )
		{
			throw new Exception('Invalid ID');
		}

		$this->view->info = $info;
		$this->view->cheque = $chequeinfo;
		$this->view->id = $id;
	}
	
	public function approveAction(){
		
		$id = $this->_getParam('id', 0);
		
		$info = $this->_DbObj->getData($id);
		
		$studentid_new = $info['rfd_studentid_new'];
		$applicantid_new = $info['rfd_applicantid_new'];

		$txnId = $info['rfd_IdStudentRegistration'];
		$amount = $info['rfd_amount'];
		
		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
		$registration = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		
		if($id != 0){
			$invoiceClass = new icampus_Function_Studentfinance_Invoice();
			$invoiceClass->refundApproval($id);
			
			if($studentid_new != 0){
				//generate advance payment
				
				$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
	    		$profile = $studentRegistrationDb->getTheStudentRegistrationDetail($studentid_new);
				
				$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				
				//get current semester
				$profile2 = array('ap_prog_scheme'=>$profile['IdScheme'],'branch_id'=>$profile['IdBranch']);
				$semesterDB = new Registration_Model_DbTable_Semester();
				$semester = $semesterDB->getApplicantCurrentSemester($profile2);
		
					$bill_no =  $this->getBillSeq(8, date('Y'));
					
					$advance_payment_data = array(
						'advpy_fomulir' => $bill_no,
						'advpy_appl_id' => 0,
						'advpy_trans_id' => $profile['transaction_id'],
						'advpy_idStudentRegistration' => $profile['IdStudentRegistration'],
						'advpy_sem_id'=>$semester['IdSemesterMaster'],
						'advpy_rcp_id' => 0,
						'advpy_description' => 'Transfer Balance from '.$registration['ProgramCode'].' to '.$profile['ProgramCode'],
						'advpy_cur_id' => $info['advpy_cur_id'],
						'advpy_refund_id' => $id,
						'advpy_amount' => $amount,
						'advpy_total_paid' => 0.00,
						'advpy_total_balance' => $amount,
						'advpy_status' => 'A',
						'advpy_date' => date('Y-m-d'),
						
					);
				
				$advPayID = $advancePaymentDb->insert($advance_payment_data);
				
				$advance_payment_det_data = array(
					'advpydet_advpy_id' => $advPayID,
					'advpydet_total_paid' => 0.00
				);
				
				$advancePaymentDetailDb->insert($advance_payment_det_data);
				
			}else if ($applicantid_new != 0){
				$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
				$profile = $studentRegistrationDb->getApplicantDetailByTransaction($applicantid_new);
				$profile2 = $studentRegistrationDb->getApplicantDetailByTransaction($info['rfd_trans_id']);

				$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

				//get current semester
				$profile2 = array('ap_prog_scheme'=>$profile['IdScheme'],'branch_id'=>$profile['branch_id']);
				$semesterDB = new Registration_Model_DbTable_Semester();
				$semester = $semesterDB->getApplicantCurrentSemester($profile2);

				$bill_no =  $this->getBillSeq(8, date('Y'));

				$advance_payment_data = array(
					'advpy_fomulir' => $bill_no,
					'advpy_appl_id' => 0,
					'advpy_trans_id' => $profile['at_trans_id'],
					'advpy_sem_id'=>$semester['IdSemesterMaster'],
					'advpy_rcp_id' => 0,
					'advpy_description' => 'Transfer Balance from '.$profile2['ProgramCode'].' to '.$profile['ProgramCode'],
					'advpy_cur_id' => $info['advpy_cur_id'],
					'advpy_refund_id' => $id,
					'advpy_amount' => $amount,
					'advpy_total_paid' => 0.00,
					'advpy_total_balance' => $amount,
					'advpy_status' => 'A',
					'advpy_date' => date('Y-m-d'),

				);

				$advPayID = $advancePaymentDb->insert($advance_payment_data);

				$advance_payment_det_data = array(
					'advpydet_advpy_id' => $advPayID,
					'advpydet_total_paid' => 0.00
				);

				$advancePaymentDetailDb->insert($advance_payment_det_data);
			}
		}
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Refund Approved');
		$this->_redirect( $this->baseUrl . '/studentfinance/refund/view/id/'.$id);
		exit;
    }
    
public function cancelAction(){
		
		$id = $this->_getParam('id', 0);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$formData = $this->getRequest()->getPost();
			
		$info = $this->_DbObj->getData($id);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($info['rfd_status'] == 'A'){
			
			//revert advance payment 
			$advancePaymentDB = new Studentfinance_Model_DbTable_AdvancePayment();
			$advData = $advancePaymentDB->getData($info['advpy_id']);
			$upd_data_adv = array(
				'advpy_total_balance'=> $advData['advpy_total_balance'] + $info['rfd_amount'],
				'advpy_refund_id' => 0,
			);
		
			$db->update('advance_payment',$upd_data_adv, array('advpy_id = ?' => $info['advpy_id']) );
		
			$db->delete('advance_payment_detail', array('advpydet_id = ?' => $info['rfdd_advpydet_id']) );
			
		}
		
		$upd_data = array(
				'rfd_status'=> 'X',
				'rfd_cancel_by'=> $getUserIdentity->id,
				'rfd_cancel_date'=> date('Y-m-d H:i:s')
			);
		
		$this->_DbObj->update($upd_data, array('rfd_id = ?' => $id) );
		
		$upd_data_adv = array(
				'advpy_status'=> 'X',
				'advpy_cancel_by'=> $getUserIdentity->id,
				'advpy_cancel_date'=> date('Y-m-d H:i:s')
			);
		
		$db->update('advance_payment',$upd_data_adv, array('advpy_refund_id = ?' => $advData['advpy_refund_id'],"advpy_status = 'A'") );
//		$this->_DbObj->update($upd_data_adv, array('rfd_id = ?' => $id) );
		
		$upd_data2 = array(
				'rchq_status'=> 'X',
				'rchq_cancel_by'=> $getUserIdentity->id,
				'rchq_cancel_date'=> date('Y-m-d H:i:s')
			);
		
		$db->update('refund_cheque',$upd_data2, array('rchq_rfd_id = ?' => $id) );
		
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Refund Cancelled');
		$this->_redirect( $this->baseUrl . '/studentfinance/refund/view/id/'.$id);
		exit;
    }
	
	public function editAction(){
		
		$this->view->title = $this->view->translate('Edit Cheque Info');
		
		$id = $this->_getParam('id', 0);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$refundChequeDb = new Studentfinance_Model_DbTable_RefundCheque();
		$chequeinfo = $refundChequeDb->getData($id);
		
		$this->view->info = $chequeinfo;
		$this->view->id = $id;
		$db = Zend_Db_Table::getDefaultAdapter();
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();

			$dataArray = array(
				'rchq_cheque_no'=>$formData['rchq_cheque_no'],
				'rchq_bank_name'=>$formData['rchq_bank_name'],
				'rchq_issue_date'=>$formData['rchq_issue_date'],
				'rchq_collector_name'=>$formData['rchq_collector_name'],
				'rchq_collector_date'=>$formData['rchq_collector_date'],
				'rchq_collector_id_type'=>$formData['rchq_collector_id_type'],
				'rchq_collector_update_by'=>$getUserIdentity->id,
			);

			$db->update('refund_cheque',$dataArray, array('rchq_id = ?' => $id) );
			
			
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Refund Cheque Updated');
			$this->_redirect( $this->baseUrl . '/studentfinance/refund/edit/id/'.$id);
		}
		
	}
	
public function ajaxGetStudentNotAction(){
    
    	$id = $this->_getParam('id', 0);
    	
    	if($id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select = $db->select()
					->from(array('a'=>'tbl_studentregistration'))
					->where('a.IdStudentRegistration = ?',$id);
		$allList = $db->fetchRow($select);
		
		$registrationId = $allList['registrationId'];
		$select2 = $db->select()
					->from(array('a'=>'tbl_studentregistration'))
					->join(array('StudentProfile' => 'student_profile'), "a.sp_id = StudentProfile.id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name",'StudentProfile.*' ))
					->join(array("st" => "tbl_definationms"),'st.idDefinition=a.profileStatus', array('StatusName'=>'DefinitionDesc'))
					->where('a.registrationId = ?',$registrationId)
					->where('a.IdStudentRegistration NOT IN (?)',$id)
					;
		$list = $db->fetchAll($select2);
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($list);
		
		$this->view->json = $json;
		exit;
    }

	public function ajaxGetApplicantNotAction(){

		$id = $this->_getParam('id', 0);

		if($id==0){
			echo "error";
			exit;
		}

		$this->_helper->layout->disableLayout();

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();

		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'applicant_transaction'))
			->where('a.at_trans_id = ?',$id);
		$allList = $db->fetchRow($select);

		$registrationId = $allList['at_appl_id'];
		$select2 = $db->select()
			->from(array('a'=>'applicant_transaction'))
			->join(array('StudentProfile' => 'applicant_profile'), "a.at_appl_id = StudentProfile.appl_id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name",'StudentProfile.*' ))
			->join(array("st" => "tbl_definationms"),'st.idDefinition=a.at_status', array('StatusName'=>'DefinitionDesc'))
			->where('a.at_appl_id = ?',$registrationId)
			->where('a.at_status != ?', 591)
			->where('a.at_trans_id NOT IN (?)',$id);

		$list = $db->fetchAll($select2);

		$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();

		echo $json = Zend_Json::encode($list);

		$this->view->json = $json;
		exit;
	}
    
/*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
	
}

