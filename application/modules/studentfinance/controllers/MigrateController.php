<?php
ini_set('memory_limit', '-1');

class Studentfinance_MigrateController extends Base_Base
{

    public function init()
    {
        $this->lobjdeftype = new App_Model_Definitiontype();
    }

    public function indexAction()
    {
        $this->view->title = $this->view->translate('Migrate');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'migrate_invoice_student_20150119'));
        $results = $db->fetchAll($select);

        $i = 1;


        foreach ($results as $data) {
            $regID = $data['registrationId'];

            $selectstudent = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->join(array('b' => 'student_profile'), 'b.id = a.sp_id')
                ->where('a.registrationId = ?', $regID)
                ->where('a.profileStatus = 92');
            $studentinfo = $db->fetchRow($selectstudent);

            $idStudentRegistration = $studentinfo['IdStudentRegistration'];
            $transId = $studentinfo['transaction_id'];
            $appl_id = $studentinfo['IdApplication'];

            $cur = '';
            $amount = 0;

            if ($data['amount_myr']) {
                $cur = 1;
                $amount = str_replace(',', '', $data['amount_myr']);
            } elseif ($data['amount_usd']) {
                $cur = 2;
                $amount = str_replace(',', '', $data['amount_usd']);
            }

            $program = new GeneralSetup_Model_DbTable_Program();
            $programinfo = $program->getProgramDataByCode($data['program']);

            $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
            $feeStructureData = $feeStructureDb->getApplicantFeeStructure($studentinfo['IdProgram'], $studentinfo['IdProgramScheme'], $studentinfo['IdIntake'], $studentinfo['appl_category'], 0);

            if ($feeStructureData) {
                $fs_id = $feeStructureData['fs_id'];
            } else {
                $fs_id = 0;
            }

//			$currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
//			$currencyRate = $currencyRateDB->getCurrentExchangeRate($cur);
            $exchange_rate = 2; //3.2

            $db->beginTransaction();
            try {

                //insert invoice main
                $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();


                $data_invoice = array(
                    'bill_number' => $data['invoice_no'],
                    'appl_id' => $appl_id,
                    'trans_id' => $transId,
                    'IdStudentRegistration' => $idStudentRegistration,
                    'academic_year' => '',
                    'semester' => $data['semester_id'],
                    'bill_amount' => $amount,
                    //'bill_amount_default_currency' => $proforma_invoice['bill_amount_default_currency'],
                    'bill_paid' => 0,
                    'bill_balance' => $amount,
                    'bill_description' => $data['description'],
                    'program_id' => $programinfo['IdProgram'],
                    'fs_id' => $fs_id,
                    'currency_id' => $cur,
                    'exchange_rate' => $exchange_rate,
                    'proforma_invoice_id' => '',
                    'date_create' => date('Y-m-d H:i:s', strtotime($data['invoice_date'])),
                    'MigrateCode' => date('dmy'),
                    'creator' => 1,
                );

                echo "<pre>";
                print_r($data_invoice);
                exit;

                $invoice_id = $invoiceMainDb->insert($data_invoice);


                //insert invoice detail
                $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();


                $invoiceDetailData = array(
                    'invoice_main_id' => $invoice_id,
                    'fi_id' => $data['fi_id'],
                    'fee_item_description' => $data['description'],
                    'cur_id' => $cur,
                    'amount' => $amount,
                    'balance' => $amount,
                    'exchange_rate' => $exchange_rate
                    //'amount_default_currency' => $fee_item_data['amount_default_currency']
                );

                $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                //release
                $releasetype = array(
                    0 => 866,
                    1 => 867
                );
                if ($data['release'] == 1) {

                    foreach ($releasetype as $key => $value) {

                        $barringData = array(
                            'tbr_category' => 2, //student
                            'tbr_appstud_id' => $idStudentRegistration,
                            'tbr_type' => $value,
                            'tbr_intake' => $data['semester_id'],
                            'tbr_reason' => "Release from Finance " . date('d-m-Y'),
                            'tbr_status' => 0, //release
                            'tbr_createby' => 1,
                            'tbr_createdate' => date('Y-m-d'),
                            'tbr_updby' => 1,
                            'tbr_upddate' => date('Y-m-d')
                        );

                        $db->insert('tbl_barringrelease', $barringData);
                    }

                }

                $db->commit();

            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }
            $i++;
        }
        echo $i;
        exit;

    }

    public function updateAction()
    {
        $this->view->title = $this->view->translate('Migrate');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        /* $db = Zend_Db_Table::getDefaultAdapter();
       $select = $db->select()
					->from(array('a'=>'discount_type_tag'),array('dtt_id'))
					->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration')
					->where("b.ProfileStatus != 92");
       $results = $db->fetchAll($select);  

       foreach($results as $data){
       	
       	 $select2 = $db->select()
					->from(array('a'=>'tbl_studentregistration'))
					->where("a.registrationId =?",$data['registrationId'])
					->where("a.ProfileStatus = 92");
		$results2 = $db->fetchRow($select2); 			
       		$data_update = array('IdStudentRegistration' =>$results2['IdStudentRegistration']);
			$db->update('discount_type_tag',$data_update, 'dtt_id = '.$data['dtt_id']);
       }
       
       exit;*/

        /* $db = Zend_Db_Table::getDefaultAdapter();
       $select = $db->select()
					->from(array('a'=>'data_fin_invoice'),array('*','ida'=>'a.id'))
					->where("a.doctype ='INVOICE'")
					->where('a.invoice_id = 0')
					->where("a.studid like 'A%'");
       $results = $db->fetchAll($select);  

       foreach($results as $data){
       	
       		$select2 = $db->select()
					->from(array('a'=>'applicant_transaction'))
					->where("a.at_pes_id = ?",$data['studid']);
				  $results2 = $db->fetchRow($select2); 	
				  
		if($results2){
       		$data_update = array('exist' =>0);
		}else{
			$data_update = array('exist' =>1);
		}
			$db->update('data_fin_invoice',$data_update, 'id = '.$data['ida']);
       }
       exit;*/
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'data_fin_invoice'), array('*', 'ida' => 'a.id'))
//					->join(array('b' => 'invoice_main'), 'a.invno = b.bill_number')
//					->where("a.doctype ='INVOICE'")
            ->where('a.invoice_id = 0');
        $results = $db->fetchAll($select);

        foreach ($results as $data) {

            if ($data['doctype'] == 'INVOICE') {

                $selectInv = $db->select()
                    ->from(array('a' => 'invoice_main'))
                    ->where("a.bill_number = ?", $data['invno'])
                    ->where("a.status = 'A'");
                $resultsInv = $db->fetchRow($selectInv);

                $data_update = array('invoice_id' => $resultsInv['id']);
                $db->update('data_fin_invoice', $data_update, 'id = ' . $data['ida']);
            }

            if ($data['doctype'] == 'CREDIT MEMO') {

                $selectCN = $db->select()
                    ->from(array('a' => 'credit_note'))
                    ->where("a.cn_billing_no = ?", $data['invno'])
                    ->where("a.cn_status = 'A'");
                $resultsCN = $db->fetchRow($selectCN);

                $data_update = array('invoice_id' => $resultsCN['cn_id']);
                $db->update('data_fin_invoice', $data_update, 'id = ' . $data['ida']);
            }

            if ($data['doctype'] == 'PAYMENT') {

                $selectPM = $db->select()
                    ->from(array('a' => 'receipt'))
                    ->where("a.rcp_no = ?", $data['invno'])
                    ->where("a.rcp_status = 'APPROVE'");
                $resultsPM = $db->fetchRow($selectPM);

                $data_update = array('invoice_id' => $resultsPM['rcp_id']);
                $db->update('data_fin_invoice', $data_update, 'id = ' . $data['ida']);
            }


        }

        exit;

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'migrate_invoice_student_20150119'), array('idMain' => 'b.id', '*'))
            ->join(array('b' => 'invoice_main'), 'b.bill_number = a.invoice_no');
        $results = $db->fetchAll($select);

        $i = 1;


        foreach ($results as $data) {

            if ($data['currency_id'] == 1) {
                $amountMigrate = $data['amount_myr'];
            } else {
                $amountMigrate = $data['amount_usd'];
            }


            if ($data['bill_amount'] != $amountMigrate) {
                $idMain = $data['idMain'];
                $newAmountMigrate = str_replace(',', '', $amountMigrate);

                $data_update = array('bill_amount' => $newAmountMigrate, 'bill_balance' => $newAmountMigrate);
                $db->update('invoice_main', $data_update, 'id = ' . $idMain);

                $data_update2 = array('amount' => $newAmountMigrate, 'balance' => $newAmountMigrate);
                $db->update('invoice_detail', $data_update2, 'invoice_main_id = ' . $idMain);

                $i++;
            }

        }
        echo $i;
        exit;

    }

    public function updateRegistrationItemMigratedAction()
    {
        $this->view->title = $this->view->translate('Update registration item migrated');


        $db = Zend_Db_Table::getDefaultAdapter();

        /*       SELECT *
FROM invoice_subject a
JOIN invoice_main b ON b.id = a.invoice_main_id
JOIN tbl_studentregsubjects c ON c.IdSubject = a.subject_id
AND c.IdStudentRegistration = b.IdStudentRegistration
join invoice_detail d on d.id = a.invoice_detail_id
GROUP BY a.subject_id*/

        $i = 0;
        $selectDetail = $db->select()
            ->from(array('a' => 'invoice_subject'))
            ->joinLeft(array('b' => 'invoice_main'), 'b.id = a.invoice_main_id', array('*'))
            ->join(array('c' => 'tbl_studentregsubjects'), 'c.IdSubject = a.subject_id
AND c.IdStudentRegistration = b.IdStudentRegistration')
            ->join(array('d' => 'invoice_detail'), ' d.id = a.invoice_detail_id')
//					->where('c.IdStudentRegistration = 3971')
//->where('c.initial = 1')
        ;
        $resultsDetail = $db->fetchAll($selectDetail);


        foreach ($resultsDetail as $det) {

            if ($det['fi_id'] == 89) { //exam
                $item = 879;
            } elseif ($det['fi_id'] == 90) { //paper
                $item = 890;
            }


            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects_detail'))
                ->where('a.student_id = ?', $det['IdStudentRegistration'])
                ->where('a.subject_id = ?', $det['subject_id'])
                ->where('a.item_id = ?', $item);
            $resultsa = $db->fetchRow($select);


            if (!$resultsa) {
//				echo  $det['IdStudentRegistration'].'+'.$det['fi_id'].' ='.$det['subject_id'].'<br>';


                $sem = 0;
                if ($det['semester']) {
                    $sem = $det['semester'];
                }

                //regsubjects_details
                $data = array(
                    'student_id' => $det['IdStudentRegistration'],
                    'regsub_id' => $det['IdStudentRegSubjects'],
                    'semester_id' => $sem,
                    'subject_id' => $det['subject_id'],
                    'item_id' => $item,
                    'section_id' => 0,
                    'invoice_id' => $det['invoice_main_id'],
                    'ec_country' => 0,
                    'ec_city' => 0,
                    'created_date' => $det['UpdDate'],
                    'created_by' => 665,
                    'created_role' => 'admin',
                    'ses_id' => 0
                );

                echo "<pre>";
                print_r($data);

                $db->insert('tbl_studentregsubjects_detail', $data);

                $i++;
            }
        }
        echo "<hr>";

//       }
        echo "total = " . $i;
        exit;

    }

    /*
	 * Get Bill no from mysql function
	 */
    private function getBillSeq($type, $year)
    {

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }


    public function updateExamRegistrationAction()
    {
        $this->view->title = $this->view->translate('Update exam registration - absent with valid reason');

        $db = Zend_Db_Table::getDefaultAdapter();
        echo $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'tbl_studentregsubjects'), 'a.er_idStudentRegistration = b.IdStudentRegistration and b.IdSemesterMain = a.er_idSemester and b.IdSubject = a.er_idSubject')
            ->where('a.er_idSemester IN (56,57)')
            ->where("a.er_attendance_status = '396'");
        $results = $db->fetchAll($select);

        $i = 1;


        foreach ($results as $data) {
            $idexamreg = $data['IdStudentRegSubjects'];

            $data_update = array('exam_status' => 'I');

            $db->update('tbl_studentregsubjects', $data_update, 'IdStudentRegSubjects = ' . $idexamreg);

            echo "update tbl_studentregsubjects set exam_status = 'I' where IdStudentRegSubjects = " . $idexamreg . ";<br>";
//			
//			$data_update2 = array('amount' =>$newAmountMigrate,'balance'=>$newAmountMigrate);
//			$db->update('invoice_detail',$data_update2, 'invoice_main_id = '.$idMain);

            $i++;

        }
        echo $i;
        exit;

    }

    public function migrateInvoiceNoBalanceAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Migrate - Invoice with No Balance');

        $semesterDb = new Records_Model_DbTable_SemesterMaster();
        $this->view->semester = $semesterDb->fetchAll("SemesterMainName NOT IN ('Credit Transfer','Exemption')", 'SemesterMainStartDate DESC')->toArray();

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $doctype = $formData['doctype'];
            $semesterNameSearch = $formData['semester'];
            $typeApp = $formData['type'];
            $status = $formData['status'];

            if ($doctype == 'INVOICE') {
                if ($semesterNameSearch != '0') {

                    $selectSem = $db->select()
                        ->from(array('a' => 'tbl_semestermaster'))
                        ->where('a.SemesterMainCode = ?', $semesterNameSearch);
                    $resultsSem = $db->fetchRow($selectSem);

                    $IdScheme = $resultsSem['IdScheme'];
                    $semesterName = $resultsSem['SemesterMainName'];

                    $select = $db->select()
                        ->from(array('a' => 'data_fin_invoice'))
                        ->joinLeft(array('d' => 'tbl_semestermaster'), "d.SemesterMainName  = '$semesterName' and d.IdScheme = '$IdScheme'")
                        ->where('a.semester like "%" ? "%"', $semesterName)
                        ->where('c.IdScheme = ?', $IdScheme)
                        ->where("a.doctype = 'INVOICE'")
                        ->where("a.invoice_id = 0")
                        ->group('a.invno');

                    if ($typeApp == 1) { //applicant
                        $select->join(array('b' => 'applicant_transaction'), 'a.studid = b.at_pes_id')
                            ->joinLeft(array('ba' => 'applicant_program'), 'ba.ap_at_trans_id = b.at_trans_id')
                            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= ba.ap_prog_id')
                            ->where('b.at_status = ?',$status);
                    } else {
                        $select->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
                            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
                            ->where('b.profileStatus = ?',$status);
                    }

                } else {
                    $select = $db->select()
                        ->from(array('a' => 'data_fin_invoice'))
//								->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
//								->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
//								->joinLeft(array('d' => 'applicant_transaction'), 'a.studid = d.at_pes_id')
//								->joinLeft(array('c' => 'applicant_program'), 'd.at_trans_id = c.ap_at_trans_id')

                        ->where("a.doctype = 'INVOICE'")
                        ->where("a.invoice_id = 0")
                        ->where("a.semester = '0'")
                        ->group('a.invno');

                    if ($typeApp == 1) { //applicant
                        $select->join(array('b' => 'applicant_transaction'), 'a.studid = b.at_pes_id')
                            ->joinLeft(array('ba' => 'applicant_program'), 'ba.ap_at_trans_id = b.at_trans_id')
                            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= ba.ap_prog_id')
                            ->where('b.at_status = ?',$status);
                    } else {
                        $select->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
                            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
                            ->where('b.profileStatus = ?',$status);
                    }

                }
            } else {
                $select = $db->select()
                    ->from(array('a' => 'data_fin_invoice'))
//							->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
//							->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')

                    ->where("a.doctype = '$doctype'")
                    ->where("a.invoice_id = 0");

                if ($typeApp == 1) { //applicant
                    $select->join(array('b' => 'applicant_transaction'), 'a.studid = b.at_pes_id')
                        ->joinLeft(array('ba' => 'applicant_program'), 'ba.ap_at_trans_id = b.at_trans_id')
                        ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= ba.ap_prog_id')
                        ->where('b.at_status = ?',$status);
                } else {
                    $select->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
                        ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
                        ->where('b.profileStatus = ?',$status);
                }

                if ($doctype != 'Advance') {
                    $select->group('a.invno');
                }
            }

//			echo $select;exit;
            $results = $db->fetchAll($select);


            $invoiceData = array();

            if ($results) {
                $i = 0;
                foreach ($results as $data) {

                    if ($data['doctype'] == 'INVOICE') {

                        $db->beginTransaction();
                        try {

                            //get currency rate
                            if ($data['invamt_usd'] != 0 || $data['invamt_usd'] != '0') {
                                $curID = 2;
                                $curRateInv = round($data['invamt_rm'] / $data['invamt_usd'], 2);

                                $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                                $selectCur = $db->select()
                                    ->from(array('a' => 'tbl_currency_rate'))
                                    ->where('a.cr_cur_id = 2')
                                    ->where('a.cr_exchange_rate = ?', $curRateInv);
                                $resultsCur = $db->fetchRow($selectCur);
                                $var = $data['invdate'];
                                $date = str_replace('/', '-', $var);

                                if ($resultsCur['cr_id']) {
                                    $curRateId = $resultsCur['cr_id'];
                                } else {
                                    $dataCurInsert = array(
                                        'cr_cur_id' => 2,//usd
                                        'cr_exchange_rate' => $curRateInv,
                                        'cr_min_rate' => 3.00,
                                        'cr_max_rate' => $curRateInv,
                                        'cr_effective_date' => date('Y-m-d', strtotime($date)),
                                        'cr_update_by' => 665,
                                        'cr_update_date' => date('Y-m-d H:i:s')
                                    );
                                    //	       					$curRateId = 1;
                                    $curRateId = $currencyRateDB->insert($dataCurInsert);
                                }

                                $bill_amount = $data['invamt_usd'];

                            } else {
                                $curRateId = 1;
                                $curID = 1;
                                $bill_amount = $data['invamt_rm'];

                            }

                            $invoiceData[$i]['bill_amount'] = $bill_amount;

                            $invoiceData[$i]['exchange_rate'] = $curRateId;
                            $invoiceData[$i]['currency_id'] = $curID;

                            $invoiceData[$i]['studid'] = $data['studid'];
                            if ($typeApp == 1) {
                                $invoiceData[$i]['IdStudentRegistration'] = NULL;
                                $invoiceData[$i]['appl_id'] = $data['at_appl_id'];
                                $invoiceData[$i]['trans_id'] = $data['at_trans_id'];
                                $invoiceData[$i]['fs_id'] = !isset($data['at_fs_id']) ? 0 : $data['at_fs_id'];
                            } else {
                                $invoiceData[$i]['IdStudentRegistration'] = $data['IdStudentRegistration'];
                                $invoiceData[$i]['appl_id'] = $data['IdApplication'];
                                $invoiceData[$i]['trans_id'] = $data['transaction_id'];
                                $invoiceData[$i]['fs_id'] = $data['fs_id'];
                            }

                            $invoiceData[$i]['program_id'] = $data['IdProgram'];

                            $var = $data['invdate'];
                            $date = str_replace('/', '-', $var);

                            $invoiceData[$i]['invdate'] = date('Y-m-d', strtotime($date));
                            $invoiceData[$i]['invdate2'] = $var;

                            $invdate2 = date('Y-m-d', strtotime($date));

                            if ($data['semester'] == '0' || $data['semester'] == 0) {

                                //get semestermain
                                $selectCur2 = $db->select()
                                    ->from(array('a' => 'tbl_semestermaster'))
                                    ->where('a.SemesterMainStartDate <= ?', $invdate2)
                                    ->where('a.SemesterMainEndDate >= ?', $invdate2)
                                    ->where('a.IdScheme = ?', $data['IdScheme']);
                                $resultsCur2 = $db->fetchRow($selectCur2);


                                $invoiceData[$i]['semester_id'] = $resultsCur2['IdSemesterMaster'];

                            } else {
                                $idSemester = $data['IdSemesterMaster'];
                                $invoiceData[$i]['semester_id'] = $idSemester;
                            }

                            $invoiceData[$i]['fin_id'] = $data['id'];
                            $invoiceData[$i]['invno'] = $data['invno'];


                            $invoiceData[$i]['invdesc'] = $data['invdesc'] . ' - ' . $data['invtype'];


                            $invoiceData[$i]['fi_id'] = $data['fee_code'];
                            $invoiceData[$i]['remark'] = $data['sponsor'];

                            /*echo "<pre>";
			       		print_r($invoiceData);
			       		exit;*/

                            //insert invoice main
                            $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                            $selectInv = $db->select()
                                ->from(array('a' => 'invoice_main'))
                                ->where('a.bill_number = ?', $invoiceData[$i]['invno'])
                                ->where("a.status = 'A'");

                            $resultsInv = $db->fetchRow($selectInv);

                            if (!$resultsInv) {
                                $data_invoice = array(
                                    'bill_number' => $invoiceData[$i]['invno'],
                                    'appl_id' => $invoiceData[$i]['appl_id'],
                                    'trans_id' => $invoiceData[$i]['trans_id'],
                                    'IdStudentRegistration' => $invoiceData[$i]['IdStudentRegistration'],
                                    'academic_year' => '',
                                    'semester' => $invoiceData[$i]['semester_id'],
                                    'bill_amount' => $invoiceData[$i]['bill_amount'],
                                    'bill_paid' => $invoiceData[$i]['bill_amount'],
                                    'bill_balance' => 0,
                                    'bill_description' => $invoiceData[$i]['invdesc'],
                                    'program_id' => $invoiceData[$i]['program_id'],
                                    'fs_id' => $invoiceData[$i]['fs_id'],
                                    'currency_id' => $invoiceData[$i]['currency_id'],
                                    'exchange_rate' => $invoiceData[$i]['exchange_rate'],
                                    'proforma_invoice_id' => '',
                                    'invoice_date' => $invoiceData[$i]['invdate'],
                                    'remarks' => $invoiceData[$i]['remark'],
                                    'date_create' => date('Y-m-d H:i:s'),
                                    'MigrateCode' => date('dmy'),
                                    'creator' => 665,
                                );


                                $invoice_id = $invoiceMainDb->insert($data_invoice);


                                //insert invoice detail
                                $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();


                                $invoiceDetailData = array(
                                    'invoice_main_id' => $invoice_id,
                                    'fi_id' => $invoiceData[$i]['fi_id'],
                                    'fee_item_description' => $data['invtype'],
                                    'cur_id' => $invoiceData[$i]['currency_id'],
                                    'amount' => $invoiceData[$i]['bill_amount'],
                                    'paid' => $invoiceData[$i]['bill_amount'],
                                    'balance' => 0,
                                    'exchange_rate' => $invoiceData[$i]['exchange_rate']
                                );

                                $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);
                                $i++;
                                $updData = array('invoice_id' => $invoice_id, 'migrateDate' => date('Y-m-d H:i:s'));
                                $db->update('data_fin_invoice', $updData, array('id = ?' => $data['id']));


                            }


                            $db->commit();

                        } catch (Exception $e) {
                            $db->rollBack();

                            //save error message
                            $sysErDb = new App_Model_General_DbTable_SystemError();
                            $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                            $msg['se_title'] = 'Error migrate invoice with no balance - INVOICE';
                            $msg['se_message'] = $e->getMessage();
                            $msg['se_createdby'] = 665;
                            $msg['se_createddt'] = date("Y-m-d H:i:s");
                            $sysErDb->addData($msg);

                            throw $e;
                        }
                    } elseif ($data['doctype'] == 'PAYMENT') {

                        $invoiceData[$i]['invdesc'] = $data['invdesc'];
                        $invoiceData[$i]['invno'] = $data['invno'];
                        if ($typeApp == 1) {
                            $invoiceData[$i]['IdStudentRegistration'] = NULL;
                            $invoiceData[$i]['appl_id'] = $data['at_appl_id'];
                            $invoiceData[$i]['trans_id'] = $data['at_trans_id'];
                        } else {
                            $invoiceData[$i]['IdStudentRegistration'] = $data['IdStudentRegistration'];
                            $invoiceData[$i]['appl_id'] = $data['IdApplication'];
                            $invoiceData[$i]['trans_id'] = $data['transaction_id'];
                        }

                        //receipt_invoice
                        $invdesc = $data['invdesc'];
                        $invArray = explode(',', $invdesc);

                        $m = 0;
                        foreach ($invArray as $key => $value) {
                            $selectInv = $db->select()
                                ->from(array('a' => 'invoice_main'))
                                ->join(array('b' => 'invoice_detail'), 'a.id = b.invoice_main_id', array('id_detail' => 'b.id'))
                                ->where('a.bill_number = ?', $value);

                            $resultsInv = $db->fetchRow($selectInv);
                            if ($resultsInv) {

                                $invoiceData[$i]['rcp_iv'][$m]['invoice_id'] = $resultsInv['id'];
                                $invoiceData[$i]['rcp_iv'][$m]['invoice_det_id'] = $resultsInv['id_detail'];
                                $invoiceData[$i]['rcp_iv'][$m]['bill_paid'] = $resultsInv['bill_paid'];
                                $invoiceData[$i]['rcp_iv'][$m]['currency_id'] = $resultsInv['currency_id'];
                            } else {
                                $invoiceData[$i]['rcp_iv'] = null;
                            }
                            $m++;

                        }


                        //get currency rate
                        if ($data['invamt_usd'] != 0 || $data['invamt_usd'] != '0') {
                            $curID = 2;
                            $bill_amount = $data['invamt_usd'];

                        } else {
                            $curID = 1;
                            $bill_amount = $data['invamt_rm'];

                        }

                        $bill_amount = str_replace('-', '', $bill_amount);

//		       		if($invoiceData[$i]['rcp_iv']){
                        $db->beginTransaction();
                        try {
                            $receiptDb = new Studentfinance_Model_DbTable_Receipt();
                            $paymentDb = new Studentfinance_Model_DbTable_Payment();
                            $receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();

                            $receipt_no = $invoiceData[$i]['invno'];

                            $var = $data['invdate'];
                            $date = str_replace('/', '-', $var);

                            $selectRcp = $db->select()
                                ->from(array('a' => 'receipt'))
                                ->where('a.rcp_no = ?', $receipt_no)
                                ->where("a.rcp_status = 'APPROVE'");

                            $resultsRcp = $db->fetchRow($selectRcp);

                            if (!$resultsRcp) {
                                $receipt_data = array(
                                    'rcp_no' => $receipt_no,
                                    'rcp_account_code' => 7,    //bank islam
                                    'rcp_receive_date' => date('Y-m-d', strtotime($date)),
                                    'rcp_payee_type' => 646, //student
                                    'rcp_appl_id' => $invoiceData[$i]['appl_id'],
                                    'rcp_trans_id' => $invoiceData[$i]['trans_id'],
                                    'rcp_idStudentRegistration' => $invoiceData[$i]['IdStudentRegistration'],
                                    'rcp_description' => $invoiceData[$i]['invdesc'],
                                    'rcp_amount' => $bill_amount,
                                    'rcp_amount_default_currency' => $bill_amount,
                                    'rcp_adv_payment_amt' => 0.00,
                                    'rcp_gainloss' => 0,
                                    'rcp_gainloss_amt' => 0,
                                    'rcp_cur_id' => $curID,
                                    'rcp_status' => 'APPROVE',
                                    'UpdDate' => date('Y-m-d H:i:s'),
                                    'migs_id' => 0,
                                    'rcp_create_by' => 665,
                                    'MigrateCode' => date('dmy'),
                                );
                                $receipt_id = $receiptDb->insert($receipt_data);

                                //add payment

                                $payment_data = array(
                                    'p_rcp_id' => $receipt_id,
                                    'p_payment_mode_id' => 99,
                                    'p_cheque_no' => null,
                                    'p_doc_bank' => null,
                                    'p_doc_branch' => null,
                                    'p_terminal_id' => null,
                                    'p_card_no' => null,
                                    'p_cur_id' => $curID,
                                    'p_status' => 'APPROVE',
                                    'p_migs' => 0,
                                    'p_amount' => $bill_amount,
                                    'p_amount_default_currency' => 0,
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'created_by' => 665
                                );

                                $paymentDb->insert($payment_data);
                            } else {
                                $receipt_id = $resultsRcp['rcp_id'];
                                $receipt_no = $resultsRcp['rcp_no'];
                            }

                            if ($invoiceData[$i]['rcp_iv']) {
                                foreach ($invoiceData[$i]['rcp_iv'] as $rcpInv) {

                                    $selectInv2 = $db->select()
                                        ->from(array('a' => 'receipt_invoice'))
                                        ->where('a.rcp_inv_rcp_id = ?', $receipt_id)
                                        ->where('a.rcp_inv_invoice_id = ?', $rcpInv['invoice_id'])
                                        ->where('a.rcp_inv_invoice_dtl_id = ?', $rcpInv['invoice_det_id']);

                                    $resultsInv2 = $db->fetchRow($selectInv2);


                                    if (!$resultsInv2) {
                                        $data_rcp_inv = array(
                                            'rcp_inv_rcp_id' => $receipt_id,
                                            'rcp_inv_rcp_no' => $receipt_no,
                                            'rcp_inv_invoice_id' => $rcpInv['invoice_id'],
                                            'rcp_inv_invoice_dtl_id' => $rcpInv['invoice_det_id'],
                                            'rcp_inv_amount' => $rcpInv['bill_paid'],
                                            'rcp_inv_amount_default_currency' => $rcpInv['bill_paid'],
                                            'rcp_inv_cur_id' => $rcpInv['currency_id'],
                                            'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                            'rcp_inv_create_by' => 665
                                        );

                                        $receiptInvoiceDb->insert($data_rcp_inv);
                                        $i++;
                                        $updData = array('invoice_id' => $receipt_id, 'migrateDate' => date('Y-m-d H:i:s'));
                                        $db->update('data_fin_invoice', $updData, array('id = ?' => $data['id']));
                                    }
                                }


                            }


                            $db->commit();


                        } catch (Exception $e) {
                            $db->rollBack();

                            //save error message
                            $sysErDb = new App_Model_General_DbTable_SystemError();
                            $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                            $msg['se_title'] = 'Error migrate invoice with no balance - PAYMENT';
                            $msg['se_message'] = $e->getMessage();
                            $msg['se_createdby'] = 665;
                            $msg['se_createddt'] = date("Y-m-d H:i:s");
                            $sysErDb->addData($msg);

                            throw $e;
                        }


                    } elseif ($data['doctype'] == 'CREDIT MEMO') {
                        //get currency rate
                        if ($data['invamt_usd'] != 0 || $data['invamt_usd'] != '0') {
                            $curID = 2;
                            $bill_amount = $data['invamt_usd'];

                        } else {
                            $curID = 1;
                            $bill_amount = $data['invamt_rm'];

                        }

                        $bill_amount = str_replace('-', '', $bill_amount);

                        $var = $data['invdate'];
                        $date = str_replace('/', '-', $var);

                        $creditMainDb = new Studentfinance_Model_DbTable_CreditNote();
                        $creditDetailDb = new Studentfinance_Model_DbTable_CreditNoteDetail();

                        $bill_no = $data['invno'];
                        $db->beginTransaction();
                        try {
                            $data_cn = array(
                                'cn_billing_no' => $bill_no,
                                'cn_appl_id' => $data['IdApplication'],
                                'cn_trans_id' => $data['transaction_id'],
                                'cn_IdStudentRegistration' => $data['IdStudentRegistration'],
                                'cn_amount' => $bill_amount,
                                'cn_description' => $data['invtype'] . ' ' . $data['invdesc'],
                                'cn_cur_id' => $curID,
                                'cn_invoice_id' => 0,
                                'cn_create_date' => date('Y-m-d', strtotime($date)),
                                'cn_creator' => 665,//by system
                                'cn_source' => 0,//by student portal
                                'cn_status' => 'A',//active/approve
                                'cn_approve_date' => date('Y-m-d H:i:s'),
                                'cn_approver' => 665,//by admin
                                'MigrateCode' => date('dmy'),
                            );

                            $cn_id = $creditMainDb->insert($data_cn);

                            //insert invoice detail

                            $cnDetailData = array(
                                'cn_id' => $cn_id,
                                'invoice_main_id' => 0,
                                'invoice_detail_id' => 0,
                                'amount' => $bill_amount,
                                'cur_id' => $curID
                            );

                            $cn_detail_id = $creditDetailDb->insert($cnDetailData);

                            $i++;

                            $updData = array('invoice_id' => $cn_id, 'migrateDate' => date('Y-m-d H:i:s'));
                            $db->update('data_fin_invoice', $updData, array('id = ?' => $data['id']));
                            $db->commit();
                        } catch (Exception $e) {
                            $db->rollBack();

                            //save error message
                            $sysErDb = new App_Model_General_DbTable_SystemError();
                            $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                            $msg['se_title'] = 'Error migrate invoice with no balance - CREDIT NOTE';
                            $msg['se_message'] = $e->getMessage();
                            $msg['se_createdby'] = 665;
                            $msg['se_createddt'] = date("Y-m-d H:i:s");
                            $sysErDb->addData($msg);

                            throw $e;
                        }


                    } elseif ($data['doctype'] == 'Advance') {
                        //get currency rate
                        if ($data['invamt_usd'] != 0 || $data['invamt_usd'] != '0') {
                            $curID = 2;
                            $bill_amount = $data['invamt_usd'];

                        } else {
                            $curID = 1;
                            $bill_amount = $data['invamt_rm'];

                        }

                        $bill_amount = str_replace('-', '', $bill_amount);

                        $var = $data['invdate'];
                        $date = str_replace('/', '-', $var);

                        $advanceMainDb = new Studentfinance_Model_DbTable_AdvancePayment();

                        $invdate2 = date('Y-m-d', strtotime($date));

                        if ($data['semester'] == '0' || $data['semester'] == 0) {

                            //get semestermain
                            $selectCur2 = $db->select()
                                ->from(array('a' => 'tbl_semestermaster'))
                                ->where('a.SemesterMainStartDate <= ?', $invdate2)
                                ->where('a.SemesterMainEndDate >= ?', $invdate2)
                                ->where('a.IdScheme = ?', $data['IdScheme']);
                            $resultsCur2 = $db->fetchRow($selectCur2);


                            $semesterInfo = $resultsCur2['IdSemesterMaster'];

                        }

                        $bill_no = $this->getBillSeq(8, date('Y'));
                        $db->beginTransaction();
                        try {
                            $data_adv = array(
                                'advpy_appl_id' => $data['IdApplication'],
                                'advpy_trans_id' => $data['transaction_id'],
                                'advpy_idStudentRegistration' => $data['IdStudentRegistration'],
                                'advpy_rcp_id' => 0,
                                'advpy_description' => $data['invtype'] . ' ' . $data['invdesc'],
                                'advpy_cur_id' => $curID,
                                'advpy_amount' => $bill_amount,
                                'advpy_total_paid' => 0.00,
                                'advpy_total_balance' => $bill_amount,
                                'advpy_status' => 'A',
                                'MigrateCode' => date('dmy'),
                                'advpy_create_date' => date('Y-m-d H:i:s'),
                                'advpy_date' => date('Y-m-d', strtotime($date)),
                                'advpy_creator' => 665,
                                'advpy_fomulir' => $bill_no,
                                'advpy_sem_id' => $semesterInfo
                            );

                            $adv_id = $advanceMainDb->insert($data_adv);


                            $i++;

                            $updData = array('invoice_id' => $adv_id, 'migrateDate' => date('Y-m-d H:i:s'), 'invno' => $bill_no);
                            $db->update('data_fin_invoice', $updData, array('id = ?' => $data['id']));
                            $db->commit();
                        } catch (Exception $e) {
                            $db->rollBack();

                            //save error message
                            $sysErDb = new App_Model_General_DbTable_SystemError();
                            $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                            $msg['se_title'] = 'Error migrate invoice with no balance - ADVANCE PAYMENT';
                            $msg['se_message'] = $e->getMessage();
                            $msg['se_createdby'] = 665;
                            $msg['se_createddt'] = date("Y-m-d H:i:s");
                            $sysErDb->addData($msg);

                            throw $e;
                        }


                    } elseif ($data['doctype'] == 'Refund') {
                        //get currency rate
                        if ($data['invamt_usd'] != 0 || $data['invamt_usd'] != '0') {
                            $curID = 2;
                            $bill_amount = $data['invamt_usd'];

                        } else {
                            $curID = 1;
                            $bill_amount = $data['invamt_rm'];

                        }

                        $bill_amount = str_replace('-', '', $bill_amount);

                        $var = $data['invdate'];
                        $date = str_replace('/', '-', $var);

                        $invdate2 = date('Y-m-d', strtotime($date));

                        if ($data['semester'] == '0' || $data['semester'] == 0) {

                            //get semestermain
                            $selectCur2 = $db->select()
                                ->from(array('a' => 'tbl_semestermaster'))
                                ->where('a.SemesterMainStartDate <= ?', $invdate2)
                                ->where('a.SemesterMainEndDate >= ?', $invdate2)
                                ->where('a.IdScheme = ?', $data['IdScheme']);
                            $resultsCur2 = $db->fetchRow($selectCur2);

                            $semesterInfo = $resultsCur2['IdSemesterMaster'];

                        }

                        $refundDb = new Studentfinance_Model_DbTable_Refund();

                        $bill_no = $data['invno'];

                        $db->beginTransaction();
                        try {

                            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                            $curRate = $currencyRateDB->getRateByDate($curID, $invdate2);

                            $data_dn = array(
                                'rfd_fomulir' => $bill_no,
                                'rfd_appl_id' => $data['IdApplication'],
                                'rfd_trans_id' => $data['transaction_id'],
                                'rfd_IdStudentRegistration' => $data['IdStudentRegistration'],
                                'rfd_amount' => $bill_amount,
                                'rfd_type' => 962,
                                'rfd_process_type' => 936,//others
                                'rfd_studentid_new' => 0,
                                'rfd_exchange_rate' => isset($curRate['cr_exchange_rate']) ? $curRate['cr_exchange_rate'] : 1,
                                'rfd_desc' => $data['invdesc'],
                                'rfd_avdpy_id' => 0,
                                'rfd_sem_id' => $semesterInfo,
                                'rfd_currency_id' => $curID,
                                'rfd_date' => $invdate2,
                                'rfd_pay_to' => 2,//student
                                'rfd_pay_name' => $data['studname'],
                                'rfd_creator' => 665,//by system
                                'rfd_create_date' => date('Y-m-d H:i:s'),
                                'rfd_status' => 'A',//entry
                                'MigrateCode' => date('dmy'),
                            );

                            $dn_id = $refundDb->insert($data_dn);

                            $refundDetailDb = new Studentfinance_Model_DbTable_RefundDetail();

                            $cnDetailData = array(
                                'rfdd_refund_id' => $dn_id,
                                'rfdd_adv_id' => 0,
                                'rfdd_advpydet_id' => 0,
                                'rfdd_amount' => $bill_amount
                            );
                            $dn_detail_id = $refundDetailDb->insert($cnDetailData);

                            $i++;

                            $updData = array('invoice_id' => $dn_id, 'migrateDate' => date('Y-m-d H:i:s'));
                            $db->update('data_fin_invoice', $updData, array('id = ?' => $data['id']));
                            $db->commit();
                        } catch (Exception $e) {
                            $db->rollBack();

                            //save error message
                            $sysErDb = new App_Model_General_DbTable_SystemError();
                            $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                            $msg['se_title'] = 'Error migrate invoice with no balance - Refund';
                            $msg['se_message'] = $e->getMessage();
                            $msg['se_createdby'] = 665;
                            $msg['se_createddt'] = date("Y-m-d H:i:s");
                            $sysErDb->addData($msg);

                            throw $e;
                        }


                    }


                } //foreach 
//	       	exit;
//	       	echo $i ."data migrated";
                $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i data migrated")));

            } else {
                $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("no data")));
            }

        }


    }

    public function migrateInvoiceOutstandingReleaseAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Migrate - Outstanding Invoice & Release');

        if ($this->getRequest()->isPost()) {
            $db = Zend_Db_Table::getDefaultAdapter();

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();
            $creator = $getUserIdentity->id;


            $select = $db->select()
                ->from(array('a' => 'data_fin_os_release'))
                ->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
                ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
                ->joinLeft(array('d' => 'tbl_semestermaster'), "d.SemesterMainName  = a.semester and d.IdScheme = c.IdScheme", array('IdSemesterMaster', 'SemesterMainName'))
                ->where('b.profileStatus = 92')
                ->where('a.invoice_id = 0')
                ->group('a.invno');

            $results = $db->fetchAll($select);


            $i = 0;

            $invoiceData = array();

            if ($results) {

                foreach ($results as $data) {

                    $db->beginTransaction();
                    try {

                        $IdScheme = $data['IdScheme'];
                        if ($IdScheme == 1) {
                            $semesterRelease = 6;
                        } elseif ($IdScheme == 11) {
                            $semesterRelease = 11;
                        }
                        //get currency rate

                        if ($data['invamt_usd'] != 0) {

                            $amountUsd = str_replace(',', '', $data['invamt_usd']);
                            $amountMyr = str_replace(',', '', $data['invamt_rm']);

                            $curID = 2;
                            $curRateInv = round($amountMyr / $amountUsd, 2);

                            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                            $selectCur = $db->select()
                                ->from(array('a' => 'tbl_currency_rate'))
                                ->where('a.cr_cur_id = 2')
                                ->where('a.cr_exchange_rate = ?', $curRateInv);
                            $resultsCur = $db->fetchRow($selectCur);
                            $var = $data['invdate'];
                            $date = str_replace('/', '-', $var);

                            if ($resultsCur['cr_id']) {
                                $curRateId = $resultsCur['cr_id'];
                            } else {
                                $dataCurInsert = array(
                                    'cr_cur_id' => 2,//usd
                                    'cr_exchange_rate' => $curRateInv,
                                    'cr_min_rate' => 3.00,
                                    'cr_max_rate' => $curRateInv,
                                    'cr_effective_date' => date('Y-m-d', strtotime($date)),
                                    'cr_update_by' => 665,
                                    'cr_update_date' => date('Y-m-d H:i:s')
                                );
//	       					$curRateId = 1;
                                $curRateId = $currencyRateDB->insert($dataCurInsert);
                            }

                            $bill_amount = $amountUsd;

                        } else {

                            $amountMyr = str_replace(',', '', $data['invamt_rm']);

                            $curRateId = 1;
                            $curID = 1;
                            $bill_amount = $amountMyr;

                        }

                        $invoiceData[$i]['bill_amount'] = $bill_amount;

                        $invoiceData[$i]['exchange_rate'] = $curRateId;
                        $invoiceData[$i]['currency_id'] = $curID;

                        $invoiceData[$i]['studid'] = $data['studid'];
                        $invoiceData[$i]['IdStudentRegistration'] = $data['IdStudentRegistration'];
                        $invoiceData[$i]['appl_id'] = $data['IdApplication'];
                        $invoiceData[$i]['trans_id'] = $data['transaction_id'];
                        $invoiceData[$i]['program_id'] = $data['IdProgram'];
                        $invoiceData[$i]['fs_id'] = $data['fs_id'];


                        $invoiceData[$i]['semester_id'] = $data['IdSemesterMaster'];
                        $invoiceData[$i]['fin_id'] = $data['id'];
                        $invoiceData[$i]['invno'] = $data['invno'];


                        $var = $data['invdate'];
                        $date = str_replace('/', '-', $var);

                        $invoiceData[$i]['invdate'] = date('Y-m-d', strtotime($date));
                        $invoiceData[$i]['invdate2'] = $var;

                        if ($data['invdesc']) {
                            $invDescription = $data['invdesc'] . ' - ' . $data['invtype'];
                        } else {
                            $invDescription = $data['invtype'];
                        }
                        $invoiceData[$i]['invdesc'] = $invDescription;


                        $invoiceData[$i]['fi_id'] = $data['fee_code'];


                        //insert invoice main
                        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                        $selectInv = $db->select()
                            ->from(array('a' => 'invoice_main'))
                            ->where('a.bill_number = ?', $invoiceData[$i]['invno']);
//									->where("a.status = 'A'");

                        $resultsInv = $db->fetchRow($selectInv);

                        if (!$resultsInv) {
                            $data_invoice = array(
                                'bill_number' => $invoiceData[$i]['invno'],
                                'appl_id' => $invoiceData[$i]['appl_id'],
                                'trans_id' => $invoiceData[$i]['trans_id'],
                                'IdStudentRegistration' => $invoiceData[$i]['IdStudentRegistration'],
                                'academic_year' => '',
                                'semester' => $invoiceData[$i]['semester_id'],
                                'bill_amount' => $invoiceData[$i]['bill_amount'],
                                'bill_paid' => 0,
                                'bill_balance' => $invoiceData[$i]['bill_amount'],
                                'bill_description' => $invoiceData[$i]['invdesc'],
                                'program_id' => $invoiceData[$i]['program_id'],
                                'fs_id' => $invoiceData[$i]['fs_id'],
                                'currency_id' => $invoiceData[$i]['currency_id'],
                                'exchange_rate' => $invoiceData[$i]['exchange_rate'],
                                'proforma_invoice_id' => '',
                                'invoice_date' => $invoiceData[$i]['invdate'],
                                'date_create' => date('Y-m-d H:i:s'),
                                'MigrateCode' => date('dmy'),
                                'creator' => 665,
                            );

                            /*echo "<pre>";
							print_r($data_invoice);
							exit;*/

                            $invoice_id = $invoiceMainDb->insert($data_invoice);


                            //insert invoice detail
                            $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();


                            $invoiceDetailData = array(
                                'invoice_main_id' => $invoice_id,
                                'fi_id' => $invoiceData[$i]['fi_id'],
                                'fee_item_description' => $invoiceData[$i]['invdesc'],
                                'cur_id' => $invoiceData[$i]['currency_id'],
                                'amount' => $invoiceData[$i]['bill_amount'],
                                'balance' => $invoiceData[$i]['bill_amount'],
                                'exchange_rate' => $invoiceData[$i]['exchange_rate']
                            );

                            $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                            $updData = array('invoice_id' => $invoice_id, 'migrateDate' => date('Y-m-d H:i:s'));
                            $db->update('data_fin_os_release', $updData, array('id = ?' => $data['id']));

                            $arrayreleasetype = array(0 => 866, 1 => 867);//finance 7 register
                            foreach ($arrayreleasetype as $key => $value) {

                                $selectrelease = $db->select()
                                    ->from(array('a' => 'tbl_barringrelease'))
                                    ->where('a.tbr_appstud_id = ?', $invoiceData[$i]['IdStudentRegistration'])
                                    ->where('tbr_type = ?', $value)
                                    ->where('tbr_intake = ?', $semesterRelease);
                                //release

                                $resultsRelease = $db->fetchRow($selectrelease);
                                if (!$resultsRelease) {
                                    $releasedata = array(
                                        'tbr_category' => 2,
                                        'tbr_appstud_id' => $invoiceData[$i]['IdStudentRegistration'],
                                        'tbr_type' => $value,
                                        'tbr_intake' => $semesterRelease,
                                        'tbr_reason' => 'Outstanding From Finance',
                                        'tbr_status' => 1,
                                        'tbr_createby' => 579,//release by zulhairi
                                        'tbr_createdate' => date('Y-m-d H:i:s'),
                                    );

                                    $db->insert('tbl_barringrelease', $releasedata);
                                }
                            }


                            $i++;
                        }


                        $db->commit();

                    } catch (Exception $e) {
                        $db->rollBack();

                        //save error message
                        $sysErDb = new App_Model_General_DbTable_SystemError();
                        $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                        $msg['se_title'] = 'Error migrate outstanding invoice and release';
                        $msg['se_message'] = $e->getMessage();
                        $msg['se_createdby'] = 665;
                        $msg['se_createddt'] = date("Y-m-d H:i:s");
                        $sysErDb->addData($msg);

                        throw $e;
                    }


                }


            }
            $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i data migrated")));
        }
    }

    public function migrateInvoiceAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Migrate Invoice');

        if ($this->getRequest()->isPost()) {
            $db = Zend_Db_Table::getDefaultAdapter();

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();
            $creator = $getUserIdentity->id;

            $select = $db->select()
                ->from(array('a' => 'data_fin_renewal'))
                ->join(array('b' => 'tbl_studentregistration'), 'a.studid = b.registrationId')
                ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
//						->where('b.profileStatus = 92')
                ->where('a.invoice_id = 0')
                ->order('b.profileStatus asc');

            $results = $db->fetchAll($select);


            $i = 0;

            $invoiceData = array();

            if ($results) {

                foreach ($results as $data) {

                    $db->beginTransaction();
                    try {

                        $IdScheme = $data['IdScheme'];

                        //jun 2015
                        if ($IdScheme == 1) {
                            $semesterRelease = 3;
                        } elseif ($IdScheme == 59) {
                            $semesterRelease = 59;
                        }
                        //get currency rate
                        if ($data['invamt_usd']) {
                            $curID = 2;
                            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                            $currencyRate = $currencyRateDB->getCurrentExchangeRate(2);

                            $curRateId = $currencyRate['cr_id'];
                            $bill_amount = $data['invamt_usd'];

                        } else {
                            $curRateId = 1;
                            $curID = 1;
                            $bill_amount = $data['invamt_rm'];

                        }


                        $invoiceData[$i]['bill_amount'] = $bill_amount;

                        $invoiceData[$i]['exchange_rate'] = $curRateId;
                        $invoiceData[$i]['currency_id'] = $curID;

                        $invoiceData[$i]['studid'] = $data['studid'];
                        $invoiceData[$i]['IdStudentRegistration'] = $data['IdStudentRegistration'];
                        $invoiceData[$i]['appl_id'] = $data['IdApplication'];
                        $invoiceData[$i]['trans_id'] = $data['transaction_id'];
                        $invoiceData[$i]['program_id'] = $data['IdProgram'];
                        $invoiceData[$i]['fs_id'] = $data['fs_id'];

                        $invoiceData[$i]['semester_id'] = $semesterRelease;
                        $invoiceData[$i]['fin_id'] = $data['id'];

                        $invoiceData[$i]['invdate'] = date("Y-m-d H:i:s");
                        $invoiceData[$i]['invdesc'] = $data['invdesc'];


                        $invoiceData[$i]['fi_id'] = $data['fee_code'];


                        //insert invoice main
                        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                        $selectInv = $db->select()
                            ->from(array('a' => 'invoice_main'))
                            ->where('a.IdStudentRegistration = ?', $invoiceData[$i]['IdStudentRegistration'])
                            ->where('a.MigrateCode = ?', date('dmy'))
                            ->where('a.semester = ?', $invoiceData[$i]['semester_id']);

                        $resultsInv = $db->fetchRow($selectInv);

                        if (!$resultsInv) {
                            $bill_no = $this->getBillSeq(2, date('Y'));

                            $data_invoice = array(
                                'bill_number' => $bill_no,
                                'appl_id' => $invoiceData[$i]['appl_id'],
                                'trans_id' => $invoiceData[$i]['trans_id'],
                                'IdStudentRegistration' => $invoiceData[$i]['IdStudentRegistration'],
                                'academic_year' => '',
                                'semester' => $invoiceData[$i]['semester_id'],
                                'bill_amount' => $invoiceData[$i]['bill_amount'],
                                'bill_paid' => 0,
                                'bill_balance' => $invoiceData[$i]['bill_amount'],
                                'bill_description' => $invoiceData[$i]['invdesc'],
                                'program_id' => $invoiceData[$i]['program_id'],
                                'fs_id' => $invoiceData[$i]['fs_id'],
                                'currency_id' => $invoiceData[$i]['currency_id'],
                                'exchange_rate' => $invoiceData[$i]['exchange_rate'],
                                'proforma_invoice_id' => '',
                                'invoice_date' => $invoiceData[$i]['invdate'],
                                'date_create' => date('Y-m-d H:i:s'),
                                'MigrateCode' => date('dmy'),
                                'creator' => 665,
                            );

                            /*echo "<pre>";
							print_r($data_invoice);
							exit;*/

                            $invoice_id = $invoiceMainDb->insert($data_invoice);


                            //insert invoice detail
                            $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();


                            $invoiceDetailData = array(
                                'invoice_main_id' => $invoice_id,
                                'fi_id' => $invoiceData[$i]['fi_id'],
                                'fee_item_description' => $data['invdesc'],
                                'cur_id' => $invoiceData[$i]['currency_id'],
                                'amount' => $invoiceData[$i]['bill_amount'],
                                'balance' => $invoiceData[$i]['bill_amount'],
                                'exchange_rate' => $invoiceData[$i]['exchange_rate']
                            );

                            $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                            $updData = array('invoice_id' => $invoice_id, 'migrateDate' => date('Y-m-d H:i:s'));
                            $db->update('data_fin_renewal', $updData, array('id = ?' => $data['id']));


                            $i++;
                        }


                        $db->commit();

                    } catch (Exception $e) {
                        $db->rollBack();

                        //save error message
                        $sysErDb = new App_Model_General_DbTable_SystemError();
                        $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                        $msg['se_title'] = 'Error migrate invoice';
                        $msg['se_message'] = $e->getMessage();
                        $msg['se_createdby'] = 665;
                        $msg['se_createddt'] = date("Y-m-d H:i:s");
                        $sysErDb->addData($msg);

                        throw $e;
                    }


                }


            }
            $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i data migrated")));
        }
    }

    public function regenerateWrongInvoiceAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Regenerate Wrong Invoice');

        $semesterDb = new Records_Model_DbTable_SemesterMaster();
        $this->view->semester = $semesterDb->fetchAll("SemesterMainName NOT IN ('Credit Transfer','Exemption')", 'SemesterMainStartDate DESC')->toArray();

        $programDb = new GeneralSetup_Model_DbTable_Program();
        $this->view->program = $programDb->fetchAll()->toArray();

        $db = Zend_Db_Table::getDefaultAdapter();


        if ($this->getRequest()->isPost()) {
            $db = Zend_Db_Table::getDefaultAdapter();

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();
            $creator = $getUserIdentity->id;

            //get subject registered by semester
            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects'))
                ->join(array('b' => 'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
                ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram')
                ->where('b.profileStatus = 92')
                ->where('b.IdProgram = 92');

            $results = $db->fetchAll($select);


            $i = 0;

            $invoiceData = array();

            if ($results) {

                foreach ($results as $data) {

                    $db->beginTransaction();
                    try {

                        $IdScheme = $data['IdScheme'];

                        //january 2015
                        if ($IdScheme == 1) {
                            $semesterRelease = 6;
                        } elseif ($IdScheme == 11) {
                            $semesterRelease = 11;
                        }
                        //get currency rate
                        if ($data['invamt_usd']) {
                            $curID = 2;
                            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                            $currencyRate = $currencyRateDB->getCurrentExchangeRate(2);

                            $curRateId = $currencyRate['cr_id'];
                            $bill_amount = $data['invamt_usd'];

                        } else {
                            $curRateId = 1;
                            $curID = 1;
                            $bill_amount = $data['invamt_rm'];

                        }


                        $invoiceData[$i]['bill_amount'] = $bill_amount;

                        $invoiceData[$i]['exchange_rate'] = $curRateId;
                        $invoiceData[$i]['currency_id'] = $curID;

                        $invoiceData[$i]['studid'] = $data['studid'];
                        $invoiceData[$i]['IdStudentRegistration'] = $data['IdStudentRegistration'];
                        $invoiceData[$i]['appl_id'] = $data['IdApplication'];
                        $invoiceData[$i]['trans_id'] = $data['transaction_id'];
                        $invoiceData[$i]['program_id'] = $data['IdProgram'];
                        $invoiceData[$i]['fs_id'] = $data['fs_id'];

                        $invoiceData[$i]['semester_id'] = $semesterRelease;
                        $invoiceData[$i]['fin_id'] = $data['id'];

                        $invoiceData[$i]['invdate'] = date("Y-m-d H:i:s");
                        $invoiceData[$i]['invdesc'] = $data['invdesc'];


                        $invoiceData[$i]['fi_id'] = $data['fee_code'];


                        //insert invoice main
                        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

                        $selectInv = $db->select()
                            ->from(array('a' => 'invoice_main'))
                            ->where('a.IdStudentRegistration = ?', $invoiceData[$i]['IdStudentRegistration'])
                            ->where('a.MigrateCode = ?', date('dmy'))
                            ->where('a.semester = ?', $invoiceData[$i]['semester_id']);

                        $resultsInv = $db->fetchRow($selectInv);

                        if (!$resultsInv) {
                            $bill_no = $this->getBillSeq(2, date('Y'));

                            $data_invoice = array(
                                'bill_number' => $bill_no,
                                'appl_id' => $invoiceData[$i]['appl_id'],
                                'trans_id' => $invoiceData[$i]['trans_id'],
                                'IdStudentRegistration' => $invoiceData[$i]['IdStudentRegistration'],
                                'academic_year' => '',
                                'semester' => $invoiceData[$i]['semester_id'],
                                'bill_amount' => $invoiceData[$i]['bill_amount'],
                                'bill_paid' => 0,
                                'bill_balance' => $invoiceData[$i]['bill_amount'],
                                'bill_description' => $invoiceData[$i]['invdesc'],
                                'program_id' => $invoiceData[$i]['program_id'],
                                'fs_id' => $invoiceData[$i]['fs_id'],
                                'currency_id' => $invoiceData[$i]['currency_id'],
                                'exchange_rate' => $invoiceData[$i]['exchange_rate'],
                                'proforma_invoice_id' => '',
                                'invoice_date' => $invoiceData[$i]['invdate'],
                                'date_create' => date('Y-m-d H:i:s'),
                                'MigrateCode' => date('dmy'),
                                'creator' => 665,
                            );

                            /*echo "<pre>";
								print_r($data_invoice);
								exit;*/

                            $invoice_id = $invoiceMainDb->insert($data_invoice);


                            //insert invoice detail
                            $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();


                            $invoiceDetailData = array(
                                'invoice_main_id' => $invoice_id,
                                'fi_id' => $invoiceData[$i]['fi_id'],
                                'fee_item_description' => $data['invdesc'],
                                'cur_id' => $invoiceData[$i]['currency_id'],
                                'amount' => $invoiceData[$i]['bill_amount'],
                                'balance' => $invoiceData[$i]['bill_amount'],
                                'exchange_rate' => $invoiceData[$i]['exchange_rate']
                            );

                            $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                            $updData = array('invoice_id' => $invoice_id, 'migrateDate' => date('Y-m-d H:i:s'));
                            $db->update('data_fin_renewal', $updData, array('id = ?' => $data['id']));


                            $i++;
                        }


                        $db->commit();

                    } catch (Exception $e) {
                        $db->rollBack();

                        //save error message
                        $sysErDb = new App_Model_General_DbTable_SystemError();
                        $msg['se_IdStudentRegistration'] = $data['IdStudentRegistration'];
                        $msg['se_title'] = 'Error migrate invoice';
                        $msg['se_message'] = $e->getMessage();
                        $msg['se_createdby'] = 665;
                        $msg['se_createddt'] = date("Y-m-d H:i:s");
                        $sysErDb->addData($msg);

                        throw $e;
                    }


                }


            }
            $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i data migrated")));
        }
    }

    public function updateInvoiceMainAction()
    {
        $this->view->title = $this->view->translate('Update Invoice Main');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('id', '*'))
            ->order('id desc');

        $results = $db->fetchAll($select);
        $i = 0;

//		$resultArray = array();

        foreach ($results as $data) {
            $select2 = $db->select()
                ->from(array('b' => 'invoice_detail'))
                ->where('b.invoice_main_id = ?', $data['id']);
            $results2 = $db->fetchAll($select2);

            $amountInv = 0;
            foreach ($results2 as $data2) {
                $amountInv += $data2['amount'];

            }

//			$resultArray[$i] = 	$data;
            if ($data['bill_amount'] != $amountInv && $amountInv != 0 && $data['IdStudentRegistration']) {
                $resultArray['IdStudentRegistration'] = $data['IdStudentRegistration'];
                $resultArray['id'] = $data['id'];
                $resultArray['bill_amount'] = $data['bill_amount'];
                $resultArray['amountdetails'] = $amountInv;
                $resultArray['invoice_date'] = $data['invoice_date'];
//				echo "<pre>";
//				print_r($resultArray);

                $updData = array('bill_amount' => $amountInv, 'bill_balance' => $amountInv);
                $db->update('invoice_main', $updData, array('id = ?' => $data['id']));

                $i++;
            }


        }

        echo $i;
        exit;

    }

    public function updateWrongLevelInvoiceAction()
    { //blm siap
        $this->view->title = $this->view->translate('Update Wrong Level Invoice');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_landscapesubject'), array('*'))
            ->join(array('b' => 'tbl_landscapeblock'), 'b.idblock = a.IdLevel')
            ->join(array('c' => 'tbl_landscape'), 'c.IdLandscape = a.IdLandscape')
            ->where('c.Landscapetype = 42');

        $results = $db->fetchAll($select);

        exit;
        $i = 0;

//		$resultArray = array();

        foreach ($results as $data) {
            $select2 = $db->select()
                ->from(array('b' => 'invoice_detail'))
                ->where('b.invoice_main_id = ?', $data['id']);
            $results2 = $db->fetchAll($select2);

            $amountInv = 0;
            foreach ($results2 as $data2) {
                $amountInv += $data2['amount'];

            }

//			$resultArray[$i] = 	$data;
            if ($data['bill_amount'] != $amountInv && $amountInv != 0 && $data['IdStudentRegistration']) {
                $resultArray['IdStudentRegistration'] = $data['IdStudentRegistration'];
                $resultArray['id'] = $data['id'];
                $resultArray['bill_amount'] = $data['bill_amount'];
                $resultArray['amountdetails'] = $amountInv;
                $resultArray['invoice_date'] = $data['invoice_date'];
                echo "<pre>";
                print_r($resultArray);

//				$updData = array ('bill_amount'=>$amountInv,'bill_balance'=>$amountInv);
//				$db->update('invoice_main',$updData,array('id = ?' => $data['id']));

                $i++;
            }


        }

        echo $i;
        exit;

    }

    public function updateInvoiceMoreStatusAction()
    {
        $this->view->title = $this->view->translate('Update invoice');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'data_fin_invoice'))
            ->join(array('c' => 'tbl_program'), 'c.ProgramCode = a.programme')
            ->join(array('b' => 'tbl_studentregistration'), 'b.registrationId = a.studid and b.IdProgram = c.IdProgram', array('ProgramId' => 'b.IdProgram', 'IdStudentRegistration', 'registrationId'));
        $results = $db->fetchAll($select);

        $i = 1;

        $arrayNew = array();
        $m = 0;
        foreach ($results as $data) {

            $select2 = $db->select()
                ->from(array('a' => 'invoice_main'))
                ->where('a.bill_number = ?', $data['invno']);
            $results2 = $db->fetchRow($select2);

            if ($results2) {
                if ($results2['program_id'] != $data['ProgramId']) {
                    $arrayNew[$m]['inv'] = $data['invno'];
                    $arrayNew[$m]['studid'] = $data['registrationId'];
                    $arrayNew[$m]['IdStudentBetul'] = $data['IdStudentRegistration'];
                    $arrayNew[$m]['IdStudentSalah'] = $results2['IdStudentRegistration'];
                    $arrayNew[$m]['programBetul'] = $data['ProgramId'];
                    $arrayNew[$m]['programSalah'] = $results2['program_id'];


                    $updData = array('IdStudentRegistration' => $arrayNew[$m]['IdStudentBetul'], 'program_id' => $arrayNew[$m]['programBetul']);
                    $db->update('invoice_main', $updData, array('id = ?' => $results2['id']));

                    $i++;
                }
            }

            $m++;

//        echo "<pre>";
//       print_r($arrayNew);


        }
        echo $i;
        exit;

    }

    public function invoiceNotGeneratedAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Invoice NOT Generated (Registered Courses)');

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        $this->view->semester = 0;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $programid = $formData['program'];
            $semesterid = $formData['semester'];
            $status = $formData['status'];

            /*	if($status == 0){
				$status = array(0,1);
			}*/

            $this->view->program = $formData['program'];
            $this->view->semester = $formData['semester'];
            $this->view->status = $formData['status'];

            $db = Zend_Db_Table::getDefaultAdapter();

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();
            $creator = $getUserIdentity->id;

            $invoiceClass = new icampus_Function_Studentfinance_Invoice();

            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects_detail'), array('id', 'subject_id', 'semester_id', 'item_id', 'invoice_id', 'created_date', 'created_role'))
                ->joinLeft(array('d' => 'tbl_studentregsubjects'), 'd.IdStudentRegSubjects = a.regsub_id', array('Active', 'credit_hour_registered', 'IdCourseTaggingGroup'))
                ->joinLeft(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme'))
                ->joinLeft(array('sa' => 'student_profile'), 'sa.id = b.sp_id', array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
//							->join(array('tri' => 'tbl_registration_item'), 'tri.program_scheme_id = b.IdProgramScheme and tri.semester_id = a.semester_id and ',array())
                ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=b.IdProgramScheme', array())
                ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=ps.mode_of_program', array('ProgramMode' => 'DefinitionDesc', 'ProgramModeMy' => 'BahasaIndonesia'))
                ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=ps.mode_of_study', array('StudyMode' => 'DefinitionDesc', 'StudyModeMy' => 'BahasaIndonesia'))
                ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=ps.program_type', array('ProgramType' => 'DefinitionDesc', 'ProgramTypeMy' => 'BahasaIndonesia'))
                ->joinLeft(array('c' => 'fee_structure_item'), 'c.fsi_registration_item_id = a.item_id', array('fsi_item_id', 'fsi_amount', 'fsi_registration_item_id'))
                ->joinLeft(array('f' => 'fee_item'), 'f.fi_id = c.fsi_item_id', array('fi_code'))
                ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.subject_id', array('SubjectName', 'SubCode'))
                ->joinLeft(array('df' => 'tbl_definationms'), 'df.IdDefinition = a.item_id', array('item_name' => 'df.DefinitionDesc'))
                ->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch', array())
                ->where('a.semester_id = ?', $semesterid)
                ->where('b.IdProgram = ?', $programid)
                ->where('d.Active IN (?)', $status)
                ->where('a.invoice_id is null')
                ->where("IFNULL(d.exam_status,'Z') NOT IN ('U','CT','EX')")
                ->where('br.invoice = 1')
                ->where('b.ProfileStatus = 92')
                ->group('a.id');
            if ($status == 9) {//pre-repeat
                $type = 'RPT';
                $select->where("f.fi_code = 'RPT'");
            } elseif (($status == 0)) {//pre-registered
                $type = '';
                $select->where("f.fi_code NOT IN ('RPT')");
            }
            $select->order('a.created_date desc');
//			echo $select;	
            $results = $db->fetchAll($select);
            $arrayNew = array();
            $arrayNew = $results;
            $a = 0;
            foreach ($results as $data) {

                $credithour = '';
                if (!isset($data['credit_hour_registered'])) {
                    $credithour = $data['credit_hour_registered'];
                }

                $selectTagging = $db->select()
                    ->from(array('cg' => 'tbl_course_tagging_group'))
                    ->joinLeft(array('cgp' => 'course_group_program'), 'cg.`IdCourseTaggingGroup` = cgp.group_id')
//					   ->joinLeft(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = cgp.program_scheme_id')
                    ->where('cg.IdCourseTaggingGroup = ?', $data['IdCourseTaggingGroup']);
                $resultsTagging = $db->fetchRow($selectTagging);
                $modeProgramNew = 0;
                if ($resultsTagging) {
                    $modeProgram = $resultsTagging['program_scheme_id'];
                    if ($data['IdProgramScheme'] != $modeProgram) {
                        $modeProgramNew = $modeProgram;
                    }
                }

                $arrayNew[$a]['scheme1'] = $data['IdProgramScheme'];
                $arrayNew[$a]['scheme2'] = $modeProgramNew;

                $amount = $invoiceClass->calculateInvoiceAmountByCourse($data['IdStudentRegistration'], $semesterid, $data['subject_id'], $data['fsi_registration_item_id'], $type, $credithour, $modeProgramNew);

                if ($amount) {
                    $arrayNew[$a]['amount'] = $amount[0]['amount'];
                    $arrayNew[$a]['cur_code'] = $amount[0]['cur_code'];
                } else {
                    $arrayNew[$a]['cur_code'] = null;
                }
                $a++;


            }
            /*echo "<pre>";
			print_r($arrayNew);
			exit;*/

            $this->view->list = $arrayNew;
        }
    }

    public function wrongInvoiceGeneratedAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Wrong Generated Invoice');

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        $this->view->semester = 0;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $programid = $formData['program'];
            $semesterid = $formData['semester'];
            $status = $formData['status'];

            $this->view->program = $formData['program'];
            $this->view->semester = $formData['semester'];
            $this->view->status = $formData['status'];

            $db = Zend_Db_Table::getDefaultAdapter();

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();
            $creator = $getUserIdentity->id;

            $invoiceClass = new icampus_Function_Studentfinance_Invoice();

            /*$select = $db->select()
							->from(array('a'=>'tbl_studentregsubjects_detail'),array('subject_id','semester_id','item_id','invoice_id','created_date','created_role'))
							->join(array('d' => 'tbl_studentregsubjects'), 'd.IdStudentRegSubjects = a.regsub_id',array('Active','credit_hour_registered','IdCourseTaggingGroup'))
							->join(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration',array('registrationId','IdStudentRegistration','fs_id','IdBranch','IdProgramScheme'))
							->joinLeft(array('sa'=>'student_profile'), 'sa.id = b.IdStudentRegistration',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
//							->joinLeft(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = b.IdProgramScheme',array('mode_of_program'))
							
							->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=b.IdProgramScheme', array('mode_of_program'))
			            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
			            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
			            
//							->join(array('c' => 'fee_structure_item'), 'c.fsi_registration_item_id = a.item_id',array('fsi_item_id','fsi_amount','fsi_registration_item_id'))
							
							->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.subject_id',array('SubjectName','SubCode'))
							->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = a.item_id',array('item_name'=>'df.DefinitionDesc'))
							->join(array('iv' => 'invoice_main'), 'iv.id = a.invoice_id',array('bill_number'))
							->join(array('ivd' => 'invoice_detail'), 'iv.id = ivd.invoice_main_id',array('bill_amount'=>'amount','balance','id'))
							->join(array('f' => 'fee_item'), 'f.fi_id = ivd.fi_id',array('fi_code'))
							->join(array('cr' => 'tbl_currency'), 'cr.cur_id = iv.currency_id',array('cr_cur_code'=>'cr.cur_code'))
							->join(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch',array())
							->where('a.semester_id = ?',$semesterid)
							->where('b.IdProgram = ?',$programid)
							->where('d.Active = ?',$status)
							->where('a.invoice_id is not null')
							->where('br.invoice = 1')
							->where('b.ProfileStatus = 92')
							->where("IFNULL(d.exam_status,'Z') NOT IN ('U','CT','EX')")
							->group('a.id')
							;*/
            $select = $db->select()
                ->from(array('iv' => 'invoice_main'), array('bill_number'))
                ->join(array('a' => 'tbl_studentregsubjects_detail'), 'a.invoice_id = iv.id', array('subject_id', 'semester_id', 'item_id', 'invoice_id', 'created_date', 'created_role'))
                ->join(array('d' => 'tbl_studentregsubjects'), 'd.IdStudentRegSubjects = a.regsub_id', array('Active', 'credit_hour_registered', 'IdCourseTaggingGroup'))
                ->join(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme'))
                ->joinLeft(array('sa' => 'student_profile'), 'sa.id = b.IdStudentRegistration', array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
//							->joinLeft(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = b.IdProgramScheme',array('mode_of_program'))

                ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=b.IdProgramScheme', array('mode_of_program'))
                ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=ps.mode_of_program', array('ProgramMode' => 'DefinitionDesc', 'ProgramModeMy' => 'BahasaIndonesia'))
                ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=ps.mode_of_study', array('StudyMode' => 'DefinitionDesc', 'StudyModeMy' => 'BahasaIndonesia'))
                ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=ps.program_type', array('ProgramType' => 'DefinitionDesc', 'ProgramTypeMy' => 'BahasaIndonesia'))
//							->join(array('c' => 'fee_structure_item'), 'c.fsi_registration_item_id = a.item_id',array('fsi_item_id','fsi_amount','fsi_registration_item_id'))

                ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.subject_id', array('SubjectName', 'SubCode'))
                ->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = a.item_id', array('item_name' => 'df.DefinitionDesc'))
//							->join(array('iv' => 'invoice_main'), 'iv.id = a.invoice_id',array('bill_number'))
                ->join(array('ivd' => 'invoice_detail'), 'iv.id = ivd.invoice_main_id', array('bill_amount' => 'amount', 'balance', 'id'))
                ->join(array('f' => 'fee_item'), 'f.fi_id = ivd.fi_id', array('fi_code'))
                ->join(array('cr' => 'tbl_currency'), 'cr.cur_id = iv.currency_id', array('cr_cur_code' => 'cr.cur_code'))
                ->join(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch', array())
                ->where('a.semester_id = ?', $semesterid)
                ->where('b.IdProgram = ?', $programid)
                ->where('d.Active = ?', $status)
                ->where('a.invoice_id is not null')
                ->where('br.invoice = 1')
                ->where('b.ProfileStatus = 92')
                ->where("IFNULL(d.exam_status,'Z') NOT IN ('U','CT','EX')")
                ->group('a.id');

            if ($status == 9) {//pre-repeat
                $type = 'RPT';
                $select->where("f.fi_code = 'RPT'");
            } elseif ($status == 0) {//pre-registered
                $type = '';
                $select->where("f.fi_code NOT IN ('RPT')");
            }
            $select->order('a.created_date desc');

            $results = $db->fetchAll($select);
            $arrayNew = array();
            $arrayNew = $results;
            $a = 0;
            foreach ($results as $data) {

                $credithour = '';
                if (!isset($data['credit_hour_registered'])) {
                    $credithour = $data['credit_hour_registered'];
                }

                $selectTagging = $db->select()
                    ->from(array('cg' => 'tbl_course_tagging_group'))
                    ->joinLeft(array('cgp' => 'course_group_program'), 'cg.`IdCourseTaggingGroup` = cgp.group_id')
                    ->joinLeft(array('cgps' => 'tbl_program_scheme'), 'cgps.IdProgramScheme = cgp.program_scheme_id')
                    ->where('cg.IdCourseTaggingGroup = ?', $data['IdCourseTaggingGroup']);
                $resultsTagging = $db->fetchRow($selectTagging);

                $modeProgramNew = 0;
                if ($resultsTagging) {
                    $modeProgram = $resultsTagging['program_scheme_id'];
                    if ($data['IdProgramScheme'] != $modeProgram) {
                        $modeProgramNew = $modeProgram;
                    }
                }

                $arrayNew[$a]['mof'] = $modeProgramNew;

                $amount = $invoiceClass->calculateInvoiceAmountByCourse($data['IdStudentRegistration'], $semesterid, $data['subject_id'], $data['item_id'], $type, $credithour, $modeProgramNew);

                if ($amount) {
                    $arrayNew[$a]['amount'] = $amount[0]['amount'];
                    $arrayNew[$a]['cur_code'] = $amount[0]['cur_code'];

                    if ($amount[0]['amount'] == $data['bill_amount']) {
                        unset($arrayNew[$a]);
                    }
                } else {
                    $arrayNew[$a]['cur_code'] = null;
                }

                $a++;

            }

//			echo "<pre>";
//			print_r($arrayNew);
            $this->view->list = $arrayNew;
        }
    }

    public function updateWrongAmountAction()
    {//todo

        $db = Zend_Db_Table::getDefaultAdapter();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
//			echo "<pre>";
//			print_r($formData);

            $idSelect = $formData['id'];
            $n = count($idSelect);
            $i = 0;
            while ($i < $n) {

                $id = $idSelect[$i];

                $select = $db->select()
                    ->from(array('a' => 'tbl_studentregsubjects_detail'), array('id', 'subject_id', 'semester_id', 'item_id', 'invoice_id', 'created_date', 'created_role', 'semester_id', 'student_id'))
                    ->join(array('d' => 'invoice_detail'), 'd.IdStudentRegSubjects = a.regsub_id', array('Active', 'credit_hour_registered'))
                    ->where('a.id = ?', $id)
                    ->where('a.invoice_id is null');
                $results = $db->fetchRow($select);

                if ($results) {

                    if ($results['Active'] == 9) {//pre-repeat
                        $type = 'RPT';
                    } elseif ($results['Active'] == 0) {//pre-registered
                        $type = '';
                    }

                    $credithour = '';
                    if (!isset($results['credit_hour_registered'])) {
                        $credithour = $results['credit_hour_registered'];
                    }

                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceClass->generateInvoiceStudent($results['student_id'], $results['semester_id'], $results['subject_id'], $type, $credithour, 1);
                }

                $i++;

            }

            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Invoice Generated');
            $this->_redirect($this->baseUrl . '/studentfinance/migrate/invoice-not-generated');

        }


    }

    public function addFeeItemAction()
    {
        $this->view->title = $this->view->translate('Fee Structure Add Fee Item');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'fee_structure_item'))
            ->join(array('d' => 'fee_structure_program'), 'a.fsi_structure_id = d.fsp_fs_id')
            ->where('d.fsp_program_id IN (2,5)')
            ->where('a.fsi_registration_item_id IN (831,830,877)');
        $results = $db->fetchAll($select);

        $i = 0;


        foreach ($results as $data) {

            if ($data['fsi_registration_item_id'] == 831) {
                $itemFsi = 108;
            } elseif ($data['fsi_registration_item_id'] == 830) {
                $itemFsi = 107;
            } elseif ($data['fsi_registration_item_id'] == 877) {
                $itemFsi = 106;
            }

            $data2['fsi_structure_id'] = $data['fsi_structure_id'];
            $data2['fsi_item_id'] = $itemFsi;
            $data2['fsi_amount'] = $data['fsi_amount'];
            $data2['fsi_cur_id'] = $data['fsi_cur_id'];
            $data2['fsi_registration_item_id'] = $data['fsi_registration_item_id'];
            $data2['created_by'] = 665;
            $data2['created_date'] = date('Y-m-d H:i:s');

            $select2 = $db->select()
                ->from(array('a' => 'fee_structure_item'))
                ->where('a.fsi_registration_item_id = ?', $data['fsi_registration_item_id'])
                ->where('a.fsi_item_id = ?', $itemFsi)
                ->where('a.fsi_structure_id = ?', $data['fsi_structure_id']);
            $results2 = $db->fetchRow($select2);

            if (!$results2) {
                $db->insert('fee_structure_item', $data2);
            }
            $i++;

        }
        echo $i;
        exit;

    }

    public function creditNotGeneratedAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Credit Note NOT Generated');

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        $this->view->semester = 0;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $programid = $formData['program'];
            $semesterid = $formData['semester'];
            $status = $formData['status'];
            $statustype = $formData['statustype'];

            $this->view->program = $formData['program'];
            $this->view->semester = $formData['semester'];
            $this->view->status = $formData['status'];
            $this->view->statustype = $formData['statustype'];

            $db = Zend_Db_Table::getDefaultAdapter();

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();
            $creator = $getUserIdentity->id;

            $invoiceClass = new icampus_Function_Studentfinance_Invoice();

            //drop course

            /*
			 * SELECT `as`.`invoice_id`, `d`.`registrationId`, CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) AS `student_fullname`, `s`.`SubjectName`, `s`.`SubCode`, `s`.`IdSubject` FROM `tbl_studentregsubjects_detail_history` AS `as` INNER JOIN `tbl_studentregsubjects_history` AS `ab` ON ab.IdStudentRegSubjects = as.regsub_id INNER JOIN `tbl_studentregistration` AS `d` ON ab.IdStudentRegistration = d.IdStudentRegistration INNER JOIN `student_profile` AS `sa` ON sa.id = d.sp_id INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject = as.subject_id WHERE (invoice_id is not null) AND (as.semester_id IN (3,59)) AND (ab.Active IN (2,3)) AND (ab.initial != 1) GROUP BY `as`.`invoice_id`
			 */
            if ($statustype == 1) {

                $statusType = $formData['status'];

                if ($statusType == 0 || $statusType == 4) {
                    $status = array(2, 3);
                } elseif ($statusType == 1) {
                    $status = array(0, 9);
                } elseif ($statusType == 2) {
                    $status = array(1, 4);
                }


                $select = "SELECT
	`a`.`invoice_id`,
	`d`.`registrationId`,
d.IdStudentRegistration,
d.fs_id,
ab.Active,
	CONCAT_WS(
		' ',
		sa.appl_fname,
		sa.appl_mname,
		sa.appl_lname
	) AS `student_fullname`,
	`s`.`SubjectName`,
	`s`.`SubCode`,
	`s`.`IdSubject`,
`c`.`date_create` AS `created_date`,
	`c`.`id`,
`ad`.`amount`,
	`ad`.`paid`,
	`ad`.`balance`,
`b`.`fsi_registration_item_id`,
	`b`.`fsi_registration_item_id` AS `item_id`,
	`f`.`fi_code`,
	`df`.`DefinitionDesc` AS `item_name`,
	`cr`.`cur_code`
FROM
	`tbl_studentregsubjects_detail_history` AS `a`
INNER JOIN `tbl_studentregsubjects_history` AS `ab` ON a.semester_id = ab.IdSemesterMain and a.subject_id = ab.IdSubject and ab.IdStudentRegistration = a.student_id
INNER JOIN `tbl_studentregistration` AS `d` ON ab.IdStudentRegistration = d.IdStudentRegistration
INNER JOIN `student_profile` AS `sa` ON sa.id = d.sp_id
INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject = a.subject_id
INNER JOIN `invoice_main` AS `c` ON c.id = a.invoice_id
INNER JOIN `invoice_detail` AS `ad` ON ad.invoice_main_id = a.invoice_id
LEFT JOIN `fee_structure_item` AS `b` ON b.fsi_item_id = ad.fi_id
INNER JOIN `fee_item` AS `f` ON f.fi_id = b.fsi_item_id
INNER JOIN `tbl_definationms` AS `df` ON df.IdDefinition = a.item_id
INNER JOIN `tbl_currency` AS `cr` ON cr.cur_id = c.currency_id

WHERE
	(invoice_id IS NOT NULL)
AND (a.semester_id IN($semesterid))
AND (ab.Active IN($status))

and invoice_id not in (select cn_invoice_id from credit_note where cn_status ='A')
GROUP BY
	`a`.`invoice_id`";
                /* //AND (ab.initial != 1)
				  * echo $select = $db->select()
							->from(array('as'=>'tbl_studentregsubjects_detail_history'),array('invoice_id'))
							->join(array('ab' => 'tbl_studentregsubjects_history'), 'ab.IdStudentRegSubjects = as.regsub_id',array('Active','IdStudentRegistration'))
							->join(array('d' => 'tbl_studentregistration'), 'ab.IdStudentRegistration = d.IdStudentRegistration',array('registrationId','fs_id'))
							->join(array('sa'=>'student_profile'), 'sa.id = d.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
							->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = as.subject_id',array('SubjectName','SubCode','IdSubject'))
							->join(array('c' => 'invoice_main'), 'c.id = as.invoice_id',array('created_date'=>'c.date_create','id'))
							->join(array('a' => 'invoice_detail'), 'a.invoice_main_id = as.invoice_id',array('amount','paid','balance'))
							
							->joinLeft(array('b' => 'fee_structure_item'), 'b.fsi_item_id = a.fi_id',array('fsi_registration_item_id','item_id'=>'b.fsi_registration_item_id'))
							->join(array('f' => 'fee_item'), 'f.fi_id = b.fsi_item_id',array('fi_code'))
							->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = as.item_id',array('item_name'=>'df.DefinitionDesc'))
							->join(array('cr' => 'tbl_currency'), 'cr.cur_id = c.currency_id',array('cur_code'))
							
							->where('invoice_id is not null')
							->where('d.ProfileStatus = 92')
							->where('as.semester_id = ?',$semesterid)
							
//							->where('d.IdProgram = ?',$programid)
							->where('ab.Active IN (2,3)')
							->where('ab.initial != 1')
							->group('as.invoice_id');*/

                /*$select = $db->select()
							->from(array('a'=>'invoice_detail'),array('amount','paid','balance'))
							->joinLeft(array('b' => 'fee_structure_item'), 'b.fsi_item_id = a.fi_id',array('fsi_registration_item_id','item_id'=>'b.fsi_registration_item_id'))
							->join(array('c' => 'invoice_main'), 'c.id = a.invoice_main_id',array('created_date'=>'c.date_create','id'))
							->join(array('cr' => 'tbl_currency'), 'cr.cur_id = c.currency_id',array('cur_code'))
							->join(array('d' => 'tbl_studentregistration'), 'c.IdStudentRegistration = d.IdStudentRegistration',array('registrationId','IdStudentRegistration','fs_id','IdBranch','IdProgramScheme'))
							->join(array('sa'=>'student_profile'), 'sa.id = d.sp_id',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
							->join(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=c.id and ivs.invoice_detail_id = a.id',array())
							->join(array('f' => 'fee_item'), 'f.fi_id = b.fsi_item_id',array('fi_code'))
							->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = ivs.subject_id',array('SubjectName','SubCode','IdSubject'))
							->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = b.fsi_registration_item_id',array('item_name'=>'df.DefinitionDesc'))
//							->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch',array())
							->where('c.semester = ?',$semesterid)
							->where('d.IdProgram = ?',$programid)
							->where('b.fsi_registration_item_id != 0')
							->where('c.id NOT IN (?)',$selectnot)
							->where('c.MigrateCode is null')
							->where('a.cn_amount = 0')
							->group('a.id')
//							->limit(10)
//							->where('br.invoice = 1')
							;*/
            }

            //wrong invoice
            if ($statustype == 2) {

                $select = $db->select()
                    ->from(array('a' => 'tbl_studentregsubjects_detail'), array('subject_id', 'semester_id', 'item_id', 'invoice_id', 'created_date', 'created_role'))
                    ->join(array('d' => 'tbl_studentregsubjects'), 'd.IdStudentRegSubjects = a.regsub_id', array('Active', 'credit_hour_registered', 'IdCourseTaggingGroup'))
                    ->join(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch'))
                    ->joinLeft(array('sa' => 'student_profile'), 'sa.id = b.sp_id', array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
                    ->joinLeft(array('cgps' => 'tbl_program_scheme'), 'cgps.IdProgramScheme = b.IdProgramScheme', array('mode_of_program', 'IdProgramScheme'))
//							->join(array('c' => 'fee_structure_item'), ' c.fsi_registration_item_id = a.item_id',array('fsi_item_id','fsi_amount','fsi_registration_item_id'))
//							
                    ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.subject_id', array('SubjectName', 'SubCode', 'IdSubject'))
                    ->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = a.item_id', array('item_name' => 'df.DefinitionDesc'))
                    ->joinLeft(array('iv' => 'invoice_main'), 'iv.id = a.invoice_id', array('bill_number', 'id'))
                    ->join(array('ivd' => 'invoice_detail'), 'iv.id = ivd.invoice_main_id', array('amount', 'balance', 'paid'))
//							->join(array('f' => 'fee_item'), 'f.fi_id = ivd.fi_id',array('fi_code'))
                    ->join(array('cr' => 'tbl_currency'), 'cr.cur_id = iv.currency_id', array('cur_code' => 'cr.cur_code'))
                    ->join(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch', array())
                    ->where('a.semester_id = ?', $semesterid)
                    ->where('b.IdProgram = ?', $programid)
                    ->where('d.Active = ?', $status)
                    ->where('a.invoice_id is not null')
                    ->where('ivd.cn_amount = 0')
                    ->where('br.invoice = 1')
                    ->group('a.id');

                /*$select = $db->select()
							->from(array('a'=>'tbl_studentregsubjects_detail'),array('item_id','student_id','invoice_id','semester_id','subject_id'))
							->join(array('ab' => 'tbl_studentregsubjects'), 'ab.IdStudentRegSubjects = a.regsub_id',array('Active','credit_hour_registered','IdCourseTaggingGroup'))
							
							->join(array('c' => 'invoice_main'), 'c.id = a.invoice_id',array('bill_number','created_date'=>'c.date_create','id'))
							->join(array('cr' => 'tbl_currency'), 'cr.cur_id = c.currency_id',array('cur_code'))
							->joinLeft(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = c.id',array('amount','paid','balance'))
							->join(array('f' => 'fee_item'), 'f.fi_id = ivd.fi_id',array('fi_code'))
							->joinLeft(array('b' => 'fee_structure_item'), 'b.fsi_item_id = ivd.fi_id and c.fs_id = b.fsi_structure_id',array('fsi_registration_item_id'))
							->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = b.fsi_registration_item_id',array('item_name'=>'df.DefinitionDesc'))
							->join(array('d' => 'tbl_studentregistration'), 'a.student_id = d.IdStudentRegistration',array('registrationId','IdStudentRegistration','fs_id','IdBranch'))
							->join(array('sa'=>'student_profile'), 'sa.id = d.IdStudentRegistration',array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
							->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme = d.IdProgramScheme',array())
							->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.subject_id',array('SubjectName','SubCode','IdSubject'))
							
							
//							->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch',array())
							->where('a.semester_id = ?',$semesterid)
							->where('d.IdProgram = ?',$programid)
							->where('ab.Active = ?',$status)
							->where('c.MigrateCode is null')
							->where('ivd.cn_amount = 0')
							->group('ivd.id');*/
            }


            //initial paper
            if ($statustype == 3) {

                $select = $db->select()
                    ->from(array('a' => 'tbl_studentregsubjects_detail_history'), array('subject_id', 'semester_id', 'item_id', 'invoice_id', 'created_date', 'created_role'))
                    ->join(array('d' => 'tbl_studentregsubjects_history'), 'd.IdStudentRegSubjects = a.regsub_id', array('Active', 'credit_hour_registered', 'IdCourseTaggingGroup'))
                    ->join(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch'))
                    ->joinLeft(array('sa' => 'student_profile'), 'sa.id = b.sp_id', array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
                    ->joinLeft(array('cgps' => 'tbl_program_scheme'), 'cgps.IdProgramScheme = b.IdProgramScheme', array('mode_of_program', 'IdProgramScheme'))
//							->join(array('c' => 'fee_structure_item'), ' c.fsi_registration_item_id = a.item_id',array('fsi_item_id','fsi_amount','fsi_registration_item_id'))
//							
                    ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.subject_id', array('SubjectName', 'SubCode', 'IdSubject'))
                    ->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = a.item_id', array('item_name' => 'df.DefinitionDesc'))
                    ->joinLeft(array('iv' => 'invoice_main'), 'iv.id = a.invoice_id', array('bill_number', 'id', 'amount' => 'bill_amount', 'balance' => 'bill_balance', 'paid' => 'bill_paid'))
//							->join(array('ivd' => 'invoice_detail'), 'iv.id = ivd.invoice_main_id',array('amount','balance','paid'))
//							->join(array('f' => 'fee_item'), 'f.fi_id = ivd.fi_id',array('fi_code'))
                    ->join(array('cr' => 'tbl_currency'), 'cr.cur_id = iv.currency_id', array('cur_code' => 'cr.cur_code'))
                    ->join(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch', array())
                    ->where('a.semester_id = ?', $semesterid)
//							->where('b.IdProgram = ?',$programid)
                    ->where('d.Active IN (2,3)')
                    ->where('a.invoice_id is not null')
                    ->where('iv.cn_amount = 0')
                    ->where('br.invoice = 1')
                    ->where('d.initial = 1')
                    ->group('d.id');


            }


            if ($statustype == 4) {

                $statusType = $formData['status'];

                if ($statusType == 0 || $statusType == 4) {
                    $status = array(2, 3);
                } elseif ($statusType == 1) {
                    $status = array(0, 9);
                } elseif ($statusType == 2) {
                    $status = array(1, 4);
                }


                echo $select = "SELECT
	b.srsdh_id,
	iv.id,
	iv.id as invoice_id,
	cn.cn_id,
	a.IdStudentRegistration,
	a.IdSubject,
	a.Active,
	CONCAT_WS(
		' ',
		sa.appl_fname,
		sa.appl_mname,
		sa.appl_lname
	) AS `student_fullname`,
	`s`.`SubjectName`,
	`s`.`SubCode`,
	`s`.`IdSubject`,
	`iv`.`date_create` AS `created_date`,
	`iv`.`id`,
	`ad`.`amount`,
	`ad`.`paid`,
	`ad`.`balance`,
	`bb`.`fsi_registration_item_id`,
	`bb`.`fsi_registration_item_id` AS `item_id`,
	`f`.`fi_code`,
	`df`.`DefinitionDesc` AS `item_name`,
	`cr`.`cur_code`,
	d.fs_id,
	d.registrationId
FROM
	`tbl_studentregsubjects_history` a
LEFT JOIN tbl_studentregsubjects_detail_history b ON b.student_id = a.IdStudentRegistration
AND b.semester_id = a.IdSemesterMain
AND b.subject_id = a.IdSubject
JOIN invoice_main iv ON iv.IdStudentRegistration = a.IdStudentRegistration
AND iv.semester = a.IdSemesterMain
AND iv. STATUS != 'X'
JOIN invoice_subject ivs ON ivs.invoice_main_id = iv.id
AND ivs.subject_id = a.IdSubject
JOIN invoice_detail ad ON ad.invoice_main_id = iv.id
LEFT JOIN credit_note cn ON cn.cn_invoice_id = iv.id
LEFT JOIN `fee_structure_item` AS `bb` ON bb.fsi_item_id = ad.fi_id
INNER JOIN `fee_item` AS `f` ON f.fi_id = bb.fsi_item_id
INNER JOIN `tbl_definationms` AS `df` ON df.IdDefinition = bb.fsi_registration_item_id
INNER JOIN `tbl_currency` AS `cr` ON cr.cur_id = iv.currency_id
INNER JOIN `tbl_studentregistration` AS `d` ON a.IdStudentRegistration = d.IdStudentRegistration
INNER JOIN `student_profile` AS `sa` ON sa.id = d.sp_id
INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject = a.IdSubject
WHERE
	a.Active IN (2, 3)
AND b.srsdh_id IS NULL
AND a.IdSemesterMain IN ($semesterid)
AND cn.cn_id IS NULL
GROUP BY
	ad.id";

            }

            if ($status == 0) {//pre-repeat
                $type = '';
//				$select2->where("f.fi_code = 'RPT'");
            } elseif ($status == 9) {//pre-registered
                $type = 'RPT';
//				$select2->where("f.fi_code NOT IN ('RPT')");
            }

            /*select a.student_id,ab.Active, a.semester_id, a.subject_id, a.invoice_id, b.bill_number, b.currency_id, c.fi_id, d.fi_name, f.fsi_registration_item_id,g.DefinitionDesc

from tbl_studentregsubjects_detail a
join tbl_studentregsubjects ab on ab.IdStudentRegSubjects = a.regsub_id
join invoice_main b on a.invoice_id = b.id
join invoice_detail c on c.invoice_main_id = b.id
join fee_item d on d.fi_id = c.fi_id
join fee_structure_item f on f.fsi_item_id = c.fi_id and b.fs_id = f.fsi_structure_id
join tbl_studentregistration sr on sr.IdStudentRegistration =ab.IdStudentRegistration
join tbl_program_scheme e on e.IdProgramScheme = sr.IdProgramScheme
join tbl_definationms g on g.IdDefinition = a.item_id

where a.semester_id in (6,11)
and ab.Active IN (4,9)
and d.fi_code NOT IN ('RPT')
and e.mode_of_program != 577

/*and b.bill_number = 'IN2015-01151'

group by c.id*/


//			echo $select;
//			exit;
            $results = $db->fetchAll($select);


            if ($statustype == 1) {
                $arrayNew = array();
                $arrayNew = $results;
                $a = 0;
                foreach ($results as $data) {

                    $selectTagging = $db->select()
                        ->from(array('a' => 'credit_note'))
                        ->where('a.cn_invoice_id = ?', $data['invoice_id'])
                        ->where("a.cn_status = 'A'");
                    $resultsTagging = $db->fetchRow($selectTagging);

                    if ($resultsTagging) {
                        unset($arrayNew[$a]);
                    } else {
                        $arrayNew[$a]['Active'] = $data['Active'];
                        $arrayNew[$a]['newamount'] = '-';
                    }

                    $a++;
                }
                $this->view->list = $arrayNew;
            }

            if ($statustype == 2) {
                $arrayNew = array();
                $arrayNew = $results;
                $a = 0;
                foreach ($results as $data) {

                    $credithour = '';
                    if (!isset($data['credit_hour_registered'])) {
                        $credithour = $data['credit_hour_registered'];
                    }

                    $selectTagging = $db->select()
                        ->from(array('cg' => 'tbl_course_tagging_group'))
                        ->joinLeft(array('cgp' => 'course_group_program'), 'cg.`IdCourseTaggingGroup` = cgp.group_id')
//					   ->joinLeft(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = cgp.program_scheme_id')
                        ->where('cg.IdCourseTaggingGroup = ?', $data['IdCourseTaggingGroup']);
                    $resultsTagging = $db->fetchRow($selectTagging);

                    $modeProgramNew = 0;
                    if ($resultsTagging) {
                        $modeProgram = $resultsTagging['program_scheme_id'];
                        if ($data['IdProgramScheme'] != $modeProgram) {
                            $modeProgramNew = $modeProgram;
                        }
                    }

                    $arrayNew[$a]['mof'] = $modeProgramNew;

                    $amount = $invoiceClass->calculateInvoiceAmountByCourse($data['IdStudentRegistration'], $semesterid, $data['subject_id'], $data['item_id'], $type, $credithour, $modeProgramNew);
                    if ($amount) {
                        /*if($amount[0]['amount'] == $data['amount']){
//							unset($arrayNew[$a]);
						}else{
							$arrayNew[$a]['newamount'] = $amount[0]['amount'];
						}*/
                        if ($amount[0]['amount']) {
                            $arrayNew[$a]['newamount'] = $amount[0]['amount'];
                        } else {
                            $arrayNew[$a]['newamount'] = 0;
                        }
                    } else {
                        $arrayNew[$a]['newamount'] = 0;
                    }
                    $a++;


                }


//				echo "<pre>";
//				print_r($arrayNew);
                $this->view->list = $arrayNew;
            }

            if ($statustype == 3) {
                $arrayNew = array();
                $arrayNew = $results;
                $a = 0;
                foreach ($results as $data) {
                    $arrayNew[$a]['Active'] = $data['Active'];
                    $arrayNew[$a]['newamount'] = '-';
                    $a++;
                }

//				echo "<pre>";
//				print_r($arrayNew);
                $this->view->list = $arrayNew;

            }

            if ($statustype == 4) {
                $arrayNew = array();
                $arrayNew = $results;
                $a = 0;
                foreach ($results as $data) {

                    $selectTagging = $db->select()
                        ->from(array('a' => 'credit_note'))
                        ->where('a.cn_invoice_id = ?', $data['invoice_id'])
                        ->where("a.cn_status = 'A'");
                    $resultsTagging = $db->fetchRow($selectTagging);

                    if ($resultsTagging) {
                        unset($arrayNew[$a]);
                    } else {
                        $arrayNew[$a]['Active'] = $data['Active'];
                        $arrayNew[$a]['newamount'] = '-';
                    }

                    $a++;
                }
                $this->view->list = $arrayNew;
            }
        }
    }

    public function updateCourseStatusAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Update course status based on payment');

        $semesterDb = new Records_Model_DbTable_SemesterMaster();
        $this->view->semester = $semesterDb->fetchAll("SemesterMainName NOT IN ('Credit Transfer','Exemption')", 'SemesterMainStartDate DESC')->toArray();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $semesterID = $formData['semester'];

            $db = Zend_Db_Table::getDefaultAdapter();

            $select_invoice = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects'))
                ->where('a.Active IN (0,9)')// prereg/prerepeat
                ->where('a.IdSemesterMain = ?', $semesterID);
//								->limit(5);
            $row = $db->fetchAll($select_invoice);

            $i = 0;
            foreach ($row as $data) {

                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $statusOS = $invoiceClass->checkOutstandingBalanceBySubject($data['IdStudentRegistration'], $data['IdSubject']);

                $newStatus = $data['Active'];
                if ($data['Active'] == 0) {
                    $newStatus = 1;
                } elseif ($data['Active'] == 9) {
                    $newStatus = 4;
                }

                if ($statusOS == 1) { //no os

                    //update course status

                    $data_update = array('Active' => $newStatus);
                    $db->update('tbl_studentregsubjects', $data_update, 'IdStudentRegSubjects = ' . $data['IdStudentRegSubjects']);


                    $i++;

                }

            }
            $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i data updated")));

        }

    }

    public function invoiceAuditNotGeneratedAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Invoice NOT Generated (Audit Paper)');

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

//        $this->view->semester = 0;
//
////        if ( $this->getRequest()->isPost() )
////		{
////			
//			$formData = $this->getRequest()->getPost();
//			$programid = $formData['program'];
//			$semesterid = $formData['semester'];
//		
//			$this->view->program = $formData['program'];
//			$this->view->semester = $formData['semester'];

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $invoiceClass = new icampus_Function_Studentfinance_Invoice();

        $select = $db->select()
            ->from(array('a' => 'tbl_auditpaperapplicationitem'), array('id' => 'api_id', 'item_id' => 'api_item', 'api_invid_0', 'api_invid_1'))
            ->joinLeft(array('d' => 'tbl_auditpaperapplicationcourse'), 'a.api_apc_id = d.apc_id', array('subject_id' => 'apc_courseid', 'apc_program', 'apc_programscheme'))
            ->joinLeft(array('da' => 'tbl_auditpaperapplication'), 'da.apa_id = a.api_apa_id', array('created_date' => 'apa_upddate', 'apa_semesterid'))
            ->joinLeft(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = da.apa_studregid', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme', 'IdProgram', 'IdIntake', 'student_type'))
            ->joinLeft(array('sa' => 'student_profile'), 'sa.id = b.sp_id', array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname", 'appl_category'))
            ->joinLeft(array('cp' => 'tbl_program'), 'b.IdProgram=cp.IdProgram', array('IdScheme'))
            ->joinLeft(array('fso' => 'fee_structure_other'), 'fso.fso_activity_id = 894 and fso.fso_program_id = b.IdProgram and fso.fso_status = 1', array('fso_fee_id', 'fso_proforma', 'fso_trigger'))
            ->joinLeft(array('c' => 'fee_structure_item'), 'c.fsi_registration_item_id = a.api_item', array('fsi_item_id', 'fsi_amount', 'fsi_registration_item_id'))
//							->joinLeft(array('f' => 'fee_item'), 'f.fi_id = c.fsi_item_id',array('fi_code'))
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject = d.apc_courseid', array('SubjectName', 'SubCode'))
            ->joinLeft(array('df' => 'tbl_definationms'), 'df.IdDefinition = a.api_item', array('item_name' => 'df.DefinitionDesc'))
            ->joinLeft(array('br' => 'tbl_branchofficevenue'), 'br.IdBranch = b.IdBranch', array())
//							->where('da.apa_semesterid = ?',$semesterid)
//							->where('b.IdProgram = ?',$programid)
//							->where('d.Active = ?',$status)
            ->where('a.api_invid_1 = 0')
//							->orWhere('a.api_invid_1 = 0')
            ->where('br.invoice = 1')
            ->where('da.apa_status = 825')
            ->group('a.api_id')//invoice kan masa approval
        ;
        /*if($status == 9){//pre-repeat
				$type = 'RPT';
				$select->where("f.fi_code = 'RPT'");
			}elseif($status == 0){//pre-registered
				$type = '';
				$select->where("f.fi_code NOT IN ('RPT')");
			}*/
        $select->order('da.apa_upddate desc');
//			echo $select;	
//exit;
        $results = $db->fetchAll($select);

        $levelArray = array(1);
        $arrayNew = array();
        $arrayNew = $results;
        $a = 0;
        foreach ($results as $data) {

            //get current intake
            $auditDB = new Records_Model_DbTable_Auditpaper();

            //get current sem
            $curSem = $auditDB->getCurSem($data['IdScheme']);

            $curIntake = $auditDB->getCurrentIntake($curSem['sem_seq'], $curSem['AcademicYear']);

            //get audit paper fee structure
            $auditDB = new Records_Model_DbTable_Auditpaper();
            $auditPaperFsId = $auditDB->getApplicantFeeStructure($data['apc_program'], $data['apc_programscheme'], $curIntake['IdIntake'], $data['appl_category'], 0);

            if ($data['student_type'] == 740) {//normal
                if ($data['IdProgramScheme'] != $data['apc_programscheme']) {
                    $fsid = $auditPaperFsId['fs_id'];
                } else {
                    $fsid = 0;
                }
            } else {//741 visiting
                $fsid = 0;
            }

            $arrayNew[$a]['fs_id2'] = $fsid;
            foreach ($levelArray as $level) {

                $amount = $invoiceClass->calculateOtherInvoice($data['IdStudentRegistration'], 'AU', $data['subject_id'], $level, $fsid, $data['item_id']);
                //calculateOtherInvoice($studentID,'AU',$data['subject_id'],$level=0,$fsId=0,$itemId=0)

                if ($amount) {
                    $arrayNew[$a]['level'][$level]['amount'] = $amount[0]['amount'];
                    $arrayNew[$a]['level'][$level]['cur_code'] = $amount[0]['cur_code'];
                } else {
                    $arrayNew[$a]['level'][$level]['cur_code'] = null;
                }
            }
            $a++;


//			}     
//			echo "<pre>";
//			print_r($arrayNew);
//			exit;

            $this->view->list = $arrayNew;
        }
    }

    public function editMigrationAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        //title
        $this->view->title = $this->view->translate("Data Migration");

        $resultsStatementDB = new Studentfinance_Model_DbTable_AccountStatement();

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $p_data = $resultsStatementDB->getPaginateData($formData);

            $p_List = new Zend_Paginator_Adapter_DbSelect($p_data);
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
            $paginator->setItemCountPerPage(5000);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

            $this->view->paginator = $paginator;
            $this->view->formData = $formData;

        }
    }

    public function searchPayeeAction()
    {

        $this->_helper->layout()->disableLayout();

        $type = $this->_getParam('rcp_payee_type', null);
        $id = $this->_getParam('payee_id', null);
        $invoice_no = $this->_getParam('payee_invoice_no', false);
        $proforma_invoice_no = $this->_getParam('payee_proforma_no', false);

        $sis_session = new Zend_Session_Namespace('sis');

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->addActionContext('view', 'html');
            $ajaxContext->initContext();

            $db = Zend_Db_Table::getDefaultAdapter();

            $result = null;

            if ($formData['payee_id'] != '' || $formData['payee_proforma_no'] != '' || $formData['payee_invoice_no'] != '') {


                if ($formData['rcp_payee_type'] == 645) {//applicant

                    $select = $db->select()
                        ->from(array('ap' => 'applicant_profile'))
                        ->join(array('at' => 'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at_pes_id', 'at_trans_id', 'fs_id'))
                        ->join(array('app' => 'applicant_program'), 'app.ap_at_trans_id = at.at_trans_id', array('ap_prog_id'))
                        ->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=app.ap_prog_id', array('program_name' => 'p.ProgramName', '*'));


                    if ($formData['payee_id'] != '') {
                        $select->where("at.at_trans_id = ?", $formData['payee_id']);
                    }


                } else
                    if ($formData['rcp_payee_type'] == 646) { //student

                        $select = $db->select()
                            ->from(array('sp' => 'student_profile'))
                            ->join(array('sr' => 'tbl_studentregistration'), 'sr.sp_id = sp.id', array('sr.*'))
                            ->joinLeft(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('program_name' => 'p.ProgramName', '*'));
//								->where('sr.ProfileStatus = 92');

                        if ($formData['payee_id'] != '') {
                            $select->where("sr.IdStudentRegistration = ? ", $formData['payee_id']);
                        }


                    }
                $row = $db->fetchRow($select);


                //get default currency
                $currencyDb = new Studentfinance_Model_DbTable_Currency();
                $currencyData = $currencyDb->getDefaultCurrency();
                $defaultCurrency = $currencyData['cur_id'];

                if ($row) {


                    if ($type == 645) {
                        $result = array(
                            'payee_id' => $row['at_pes_id'],
                            'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname'] . " " . $row['appl_mname'] . " " . $row['appl_lname']),
                            'payee_type' => $type,
                            'program_name' => $row['program_name'],
                            'appl_id' => $row['appl_id'],
                            'trans_id' => $row['at_trans_id'],
                            'fs_id' => $row['fs_id'],
                            'program_id' => $row['ap_prog_id'],
                            'scheme_id' => $row['IdScheme'],
                        );
                    } elseif ($type == 646) {
                        $result = array(
                            'payee_id' => $row['registrationId'],
                            'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname'] . " " . $row['appl_mname'] . " " . $row['appl_lname']),
                            'payee_type' => $type,
                            'program_name' => $row['program_name'],
                            'appl_id' => $row['IdStudentRegistration'],
                            'trans_id' => $row['IdStudentRegistration'],
                            'fs_id' => $row['fs_id'],
                            'program_id' => $row['IdProgram'],
                            'scheme_id' => $row['IdScheme'],
                        );
                    }
                }


                /*//search invoice
					$select_invoice = $db->select()
					->from(array('i'=>'credit_note'),array('*','fee_item_description'=>'cn_description','amount'=>"cn_amount",'status'=>'cn_status'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.cn_cur_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("cn_status = 'A'")
					;

					if($type == 645){
						$select_invoice->where('i.cn_trans_id = ?', $result['trans_id']);
					}elseif($type == 646){
						$select_invoice->where('i.cn_IdStudentRegistration= ?', $result['trans_id']);
					}
					
				
					$invoice = $db->fetchAll($select_invoice);
					
					$totalMyr=0;
					$totalUsd=0;
					$m=0;
					foreach($invoice as $inv){
						$result['invoice'][$m] = $inv;
						

						$result['invoice'][$m]['cur_def'] = $inv['cn_cur_id'];
						$result['invoice'][$m]['amount_default'] = $inv['amount'];
						
						$stsinv = '';
						if( $inv['cn_status'] == 'A'){
							$stsinv = 'Active';
						}elseif( $inv['cn_status'] == 'X'){
							$stsinv = 'Cancel';
						}elseif( $inv['cn_status'] == 'E'){
							$stsinv = 'Exempted';
						}
						
						$result['invoice'][$m]['status'] = $stsinv;
						
						$m++;
					}
				
				}*/

            }

//			echo "<pre>";
//			print_r($result);

            $ajaxContext->addActionContext('view', 'html')
                ->addActionContext('form', 'html')
                ->addActionContext('process', 'json')
                ->initContext();

            $json = Zend_Json::encode($result);

            echo $json;
            exit();

        }
    }

    public function ledgerAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $trans_id = $this->_getParam('id', null);
        $this->view->appl_id = $trans_id;

        $type = $this->_getParam('type', 1);
        $this->view->typeApp = $type;

        $idStudentRegistration = $this->_getParam('id_reg', null);
        $this->view->idStudentRegistration = $idStudentRegistration;

        //title
        $this->view->title = $this->view->translate("Transaction Details");

        //profile
        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
            $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($trans_id);
            $profile = array_merge($profile, $taggingFinancialAid);

        }


        $this->view->profile = $profile;

//    	echo "<pre>";
//    	print_r($profile);
//    	exit;

        //account
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'description' => 'fc.fi_name',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'bill_number',
                    'invoice_no' => 'bill_number',
                    'receipt_no' => new Zend_Db_Expr ('"-"'),
                    'subject' => 's.SubCode',
                    'fc_seq' => 'im.id',
                    'migrate_desc' => 'bill_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => 'im.exchange_rate',
                    'amount' => 'bill_amount'
                )
            )
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            //->where('im.currency_id= ?',$currency)						  

            ->where("im.status = 'A'")
            ->group('im.id');
        if ($type == 1) {
            $select_invoice->where('im.trans_id = ?', $trans_id);
        } else {
            $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
        }


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'rcp_amount',
                    'document' => 'rcp_no',
                    'invoice_no' => 'rcp_no',
                    'receipt_no' => 'rcp_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'pm.rcp_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('""'),
                    'amount' => 'rcp_amount'
                )
            )
            ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("pm.rcp_status = 'APPROVE'");
        if ($type == 1) {
            $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
        } else {
            $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
        }

        $select_credit = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn.cn_id',
                    'record_date' => 'cn.cn_create_date',
                    'description' => 'cn.cn_description',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'document' => 'cn_billing_no',
                    'invoice_no' => 'cn_billing_no',
                    'receipt_no' => 'cn_billing_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'cn.cn_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => 'i.exchange_rate',
                    'amount' => 'cn_amount'
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("cn.cn_status = 'A'");
        if ($type == 1) {
            $select_credit->where('cn.cn_trans_id = ?', $trans_id);
        } else {
            $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
        }

        $select_discount = $db->select()
            ->from(
                array('a' => 'discount_detail'), array(
                    'id' => 'dn.dcnt_id',
                    'record_date' => 'dn.dcnt_approve_date',
                    'description' => 'dn.dcnt_description',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'SUM(a.dcnt_amount)',
                    'document' => 'dn.dcnt_fomulir_id',
                    'invoice_no' => 'dn.dcnt_fomulir_id',
                    'receipt_no' => 'dn.dcnt_fomulir_id',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dn.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                    'amount' => 'SUM(a.dcnt_amount)'
                )
            )
            ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("dn.dcnt_status = 'A'")
            ->group('dn.dcnt_id');

        if ($type == 1) {
            $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        $select_refund = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'rfd_fomulir',
                    'invoice_no' => 'rfd_fomulir',
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => 'rfd_exchange_rate',
                    'amount' => 'rfd_amount'
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("rn.rfd_status = 'A'");

        if ($type == 1) {
            $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
        }

        //get array payment				
        $select = $db->select()
            ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund), Zend_Db_Select::SQL_UNION_ALL)
            ->order("record_date asc")
            ->order("txn_type asc");

        $results = $db->fetchAll($select);


        if (!$results) {
            $results = null;
        }

        $this->view->account = $results;


    }

    public function editTransactionAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $this->view->id = $id;
        $this->view->type = $type;

        $trans_id = $this->_getParam('appl_id', null);
        $this->view->appl_id = $trans_id;

        $typeApp = $this->_getParam('typeApp', 1);
        $this->view->typeApp = $typeApp;


        //get student
        //profile
        if ($typeApp == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $this->view->profileList = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
        } elseif ($typeApp == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);

            $this->view->profileList = $studentRegistrationDb->getStudentByRegistrationId($profile['registrationId']);

        }

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $role = $getUserIdentity->IdRole;
        $this->view->role = $role;

        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semester_list = $semesterDb->fnGetSemestermasterList();


        //currency list
        $currencyDb = new Studentfinance_Model_DbTable_Currency();
        $this->view->currencyList = $currencyDb->getList();

        //currency rate list
        $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
        $this->view->currencyRateList = $currencyRateDb->getList();

        $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
        $this->view->feeList = $feeItemDb->getActiveFeeItem();

        if ($type == 'Invoice') {

            $select = $db->select()
                ->from(array('a' => 'invoice_main'))
                ->join(array('i' => 'invoice_detail'), 'a.id = i.invoice_main_id', array('fi_id'))
                ->where('a.id = ?', $id);

        } elseif ($type == 'Payment') {

            $select = $db->select()
                ->from(array('a' => 'receipt'))
                ->join(array('p' => 'payment'), 'p.p_rcp_id = a.rcp_id', array('p_id', 'p_cheque_no', 'p_card_no'))
                ->where('a.rcp_id = ?', $id);

        } elseif ($type == 'Credit Note') {

            $select = $db->select()
                ->from(array('a' => 'credit_note'))
                ->where('a.cn_id = ?', $id);
        } else {
            $select = false;
        }

        if ($select != false) {
            $row = $db->fetchRow($select);
            $this->view->fee = $row;
        }

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

//			echo "<pre>";
//			print_r($formData);
//			exit;

            $auth = Zend_Auth::getInstance();

            $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();

            if ($type == 'Invoice') {

                if (isset($formData['invoice_date'])) {
                    $dataInv['invoice_date'] = date('Y-m-d', strtotime($formData['invoice_date']));
                }

                if (isset($formData['bill_amount'])) {
                    $dataInv['bill_amount'] = $formData['bill_amount'];
                }

                if (isset($formData['bill_paid'])) {
                    $dataInv['bill_paid'] = $formData['bill_paid'];
                }

                if (isset($formData['bill_balance'])) {
                    $dataInv['bill_balance'] = $formData['bill_balance'];
                }

                if (isset($formData['IdStudentRegistration'])) {
                    $dataInv['IdStudentRegistration'] = $formData['IdStudentRegistration'];
                }

                if (isset($formData['semester'])) {
                    $dataInv['semester'] = $formData['semester'];
                }

                if (isset($formData['currency_id'])) {
                    $dataInv['currency_id'] = $formData['currency_id'];
                }

                if (isset($formData['bill_description'])) {
                    $dataInv['bill_description'] = $formData['bill_description'];
                }

                if (isset($formData['exchange_rate'])) {
                    $dataInv['exchange_rate'] = $formData['exchange_rate'];
                }

                if (isset($formData['not_include'])) {
                    $dataInv['not_include'] = $formData['not_include'];
                }else{
                    $dataInv['not_include'] = 0;
                }

                $dataInv['upd_date'] = date('Y-m-d H:i:s');
                $dataInv['upd_by'] = $creator;

                $db->update('invoice_main', $dataInv, array('id =?' => $id));

                //get fee name
                $desc = '';
                if (isset($formData['fi_id'])) {
                    $feeItemDB = new Studentfinance_Model_DbTable_FeeItem();
                    $feeInfo = $feeItemDB->getData($formData['fi_id']);
                    $desc = $feeInfo['fi_name'];
                } else {
                    $desc = $formData['bill_description'];
                }

                $dataInvDetail['fee_item_description'] = $desc;

                if (isset($formData['fi_id'])) {
                    $dataInvDetail['fi_id'] = $formData['fi_id'];
                }
                if (isset($formData['bill_amount'])) {
                    $dataInvDetail['amount'] = $formData['bill_amount'];
                }
                if (isset($formData['bill_paid'])) {
                    $dataInvDetail['paid'] = $formData['bill_paid'];
                }
                if (isset($formData['bill_balance'])) {
                    $dataInvDetail['balance'] = $formData['bill_balance'];
                }
                if (isset($formData['currency_id'])) {
                    $dataInvDetail['cur_id'] = $formData['currency_id'];
                }
                if (isset($formData['exchange_rate'])) {
                    $dataInvDetail['exchange_rate'] = $formData['exchange_rate'];
                }

                $db->update('invoice_detail', $dataInvDetail, array('invoice_main_id =?' => $id));

            } elseif ($type == 'Payment') {

                if (isset($formData['rcp_receive_date'])) {
                    $dataRcp['rcp_receive_date'] = date('Y-m-d', strtotime($formData['rcp_receive_date']));
                }
                if (isset($formData['rcp_amount'])) {
                    $dataRcp['rcp_amount'] = $formData['rcp_amount'];
                }
                if (isset($formData['currency_id'])) {
                    $dataRcp['rcp_cur_id'] = $formData['currency_id'];
                }
                if (isset($formData['rcp_description'])) {
                    $dataRcp['rcp_description'] = $formData['rcp_description'];
                }
                if (isset($formData['p_cheque_no'])) {

                    if ($row['p_cheque_no']) {
                        $dataRcp3['p_cheque_no'] = $formData['p_cheque_no'];
                    } else {
                        $dataRcp3['p_card_no'] = $formData['p_cheque_no'];
                    }
                    $db->update('payment', $dataRcp3, array('p_id =?' => $row['p_id']));

                }
                if (isset($formData['rcp_gainloss'])) {
                    $dataRcp['rcp_gainloss'] = $formData['rcp_gainloss'];
                }
                if (isset($formData['rcp_gainloss_amt'])) {
                    $dataRcp['rcp_gainloss_amt'] = $formData['rcp_gainloss_amt'];
                }
                if (isset($formData['rcp_idStudentRegistration'])) {
                    $dataRcp['rcp_idStudentRegistration'] = $formData['rcp_idStudentRegistration'];
                }

                $dataRcp['UpdDate'] = date('Y-m-d H:i:s');
                $dataRcp['UpdUser'] = $creator;

                $db->update('receipt', $dataRcp, array('rcp_id =?' => $id));

            } elseif ($type == 'Credit Note') {

                $data = array(
                    'cn_create_date' => date('Y-m-d', strtotime($formData['cn_create_date'])),
                    'cn_amount' => $formData['cn_amount'],
                    'cn_cur_id' => $formData['currency_id'],
                    'cn_description' => $formData['cn_description'],
                    'cn_IdStudentRegistration' => $formData['cn_IdStudentRegistration'],
                    'cn_approve_date' => date('Y-m-d H:i:s'),
                    'cn_approver' => $creator,
                );

                $db->update('credit_note', $data, array('cn_id =?' => $id));

            }

            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Transaction updated');

            //redirect
            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'migrate', 'action' => 'ledger', 'id' => $trans_id, 'type' => $typeApp), 'default', true));
        }


        //currency
        $currencyDb = new Studentfinance_Model_DbTable_Currency();
        $listData = $currencyDb->fetchAll('cur_status = 1')->toArray();
        $this->view->currency_list = $listData;

        //registration item list
        $definationDb = new App_Model_General_DbTable_Definationms();
        $this->view->registrationItem = $definationDb->getByCode('Registration Item');

        //fee item list
        $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
        $this->view->feeItemList = $feeItemDb->getActiveFeeItem();
    }

    public function cancelTransactionAction()
    {

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $this->view->id = $id;
        $this->view->type = $type;

        $trans_id = $this->_getParam('appl_id', null);
        $this->view->appl_id = $trans_id;

        $typeApp = $this->_getParam('typeApp', 1);
        $this->view->typeApp = $typeApp;


        $db = Zend_Db_Table::getDefaultAdapter();


        $auth = Zend_Auth::getInstance();

        $auth = Zend_Auth::getInstance();
        $creator = $auth->getIdentity()->iduser;

        if ($type == 'Invoice') {

            $data = array(
                'status' => 'X',
                'cancel_by' => $creator,
                'cancel_date' => date('Y-m-d H:i:s'),
            );

            $db->update('invoice_main', $data, array('id =?' => $id));

        } elseif ($type == 'Payment') {

            $data = array(
                'rcp_status' => 'CANCEL',
                'cancel_by' => $creator,
                'cancel_date' => date('Y-m-d H:i:s'),
            );

            $db->update('receipt', $data, array('rcp_id =?' => $id));

        } elseif ($type == 'Credit Note') {

            $data = array(
                'cn_status' => 'X',
                'cn_cancel_by' => $creator,
                'cn_cancel_date' => date('Y-m-d H:i:s'),
            );

            $db->update('credit_note', $data, array('cn_id =?' => $id));

        } elseif ($type == 'Refund') {

            $data = array(
                'rfd_status' => 'X',
                'rfd_cancel_by' => $creator,
                'rfd_cancel_date' => date('Y-m-d H:i:s'),
            );

            $db->update('refund', $data, array('rfd_id =?' => $id));

        }

        $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Transaction Cancelled');

        //redirect
        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'migrate', 'action' => 'ledger', 'id' => $trans_id, 'type' => $typeApp), 'default', true));

    }

    public function updateRateMigrationAction()
    {
        $this->view->title = $this->view->translate('Migrate');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'))
            ->where("a.MigrateCode is not null")
            ->where('a.currency_id = 2');
        $results = $db->fetchAll($select);

        $i = 0;
        foreach ($results as $data) {

            $dateTrans = date('Y-m-d', strtotime($data['invoice_date']));
            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
            $currencyRate = $currencyRateDB->getRateByDate(2, $dateTrans);

            $data_update = array('exchange_rate' => $currencyRate['cr_id']);
            $db->update('invoice_main', $data_update, 'id = ' . $data['id']);


            $i++;
        }


        echo $i;
        exit;

    }

    public function scanDirectoryAction()
    {
        $dir = DOCUMENT_URL . '/report/finance/201503';
        $files1 = scandir($dir);

        echo "<pre>";
        print_r($files1);

        exit;

    }


    public function notRepeatAction()
    {
        $this->view->title = $this->view->translate('repeat paper, but bill normal subject');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_detail'), array('fi_id', 'amount', 'balance', 'cur_id', 'id'))
            ->join(array('b' => 'invoice_main'), 'a.invoice_main_id = b.id', array('bill_number'))
            ->join(array('c' => 'invoice_subject'), 'c.invoice_main_id = b.id and c.invoice_detail_id = a.id', array('subject_id'))
            ->join(array('ba' => 'tbl_studentregistration'), 'b.IdStudentRegistration = ba.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme'))
            ->join(array('f' => 'fee_item'), 'f.fi_id = a.fi_id', array('fi_code'))
            ->joinLeft(array('ca' => 'fee_structure_item'), 'ca.fsi_structure_id = ba.fs_id and ca.fsi_item_id = a.fi_id', array('fsi_item_id', 'fsi_amount', 'fsi_registration_item_id'))
            ->join(array('d' => 'tbl_studentregsubjects'), 'd.IdSubject = c.subject_id
AND d.IdStudentRegistration = b.IdStudentRegistration', array('Active', 'IdStudentRegistration', 'IdSemesterMain', 'IdCourseTaggingGroup', 'credit_hour_registered'))
            ->join(array('am' => 'tbl_studentregsubjects_detail'), 'am.invoice_id = b.id and am.regsub_id = d.IdStudentRegSubjects and am.item_id = ca.fsi_registration_item_id', array('item_id', 'invoice_id', 'created_date', 'created_role'))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=ba.IdProgramScheme', array('mode_of_program'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = c.subject_id', array('SubjectName', 'SubCode'))
            ->joinLeft(array('df' => 'tbl_definationms'), 'df.IdDefinition = am.item_id', array('item_name' => 'df.DefinitionDesc'))
            ->where('d.Active IN (4,9)')//repeat/pre repeat
            ->where("f.fi_code NOT IN ('RPT')")
            ->where("IFNULL(d.exam_status,'Z') NOT IN ('U','CT','EX')")
            ->where("b.status = 'A'")
            ->where('ba.ProfileStatus = 92');

        echo $select;
        $results = $db->fetchAll($select);
        $type = 'RPT';

        $arrayNew = array();
        $arrayNew = $results;
        $a = 0;
        $arrayNew2 = array();
        foreach ($results as $data) {

            $credithour = '';
            if (!isset($data['credit_hour_registered'])) {
                $credithour = $data['credit_hour_registered'];
            }

            $selectTagging = $db->select()
                ->from(array('cg' => 'tbl_course_tagging_group'))
                ->joinLeft(array('cgp' => 'course_group_program'), 'cg.`IdCourseTaggingGroup` = cgp.group_id')
                ->joinLeft(array('cgps' => 'tbl_program_scheme'), 'cgps.IdProgramScheme = cgp.program_scheme_id')
                ->where('cg.IdCourseTaggingGroup = ?', $data['IdCourseTaggingGroup']);
            $resultsTagging = $db->fetchRow($selectTagging);

            $modeProgramNew = 0;
            if ($resultsTagging) {
                $modeProgram = $resultsTagging['program_scheme_id'];
                if ($data['IdProgramScheme'] != $modeProgram) {
                    $modeProgramNew = $modeProgram;
                }
            }

            $arrayNew[$a]['scheme1'] = $data['IdProgramScheme'];
            $arrayNew[$a]['scheme2'] = $modeProgramNew;

            $amount = $invoiceClass->calculateInvoiceAmountByCourse($data['IdStudentRegistration'], $data['IdSemesterMain'], $data['subject_id'], $data['item_id'], $type, $credithour, $modeProgramNew);

            if ($amount) {
                $arrayNew[$a]['amountNew'] = $amount[0]['amount'];
                $arrayNew[$a]['cur_code'] = $amount[0]['cur_code'];
                $arrayNew[$a]['fi_id_new'] = $amount[0]['fi_id'];
                $arrayNew[$a]['fi_name_new'] = $amount[0]['fi_name'];
                $arrayNew[$a]['cur_id_new'] = $amount[0]['cur_id'];

                if (($amount[0]['fi_id'] == $data['fi_id'])) {
                    unset($arrayNew[$a]);
                } else {
                    //update fi_id

                    $data_update = array('fi_id' => $amount[0]['fi_id']);
                    $db->update('invoice_detail', $data_update, 'id = ' . $data['id']);
                }

            } else {
                $arrayNew[$a]['cur_code'] = NULL;
            }

            /*if($data['item_id'] == 890){//paper
					
					
				}*/

            /*if($data['item_id'] == 889){//course
					unset($arrayNew[$a]);
				}*/


            $arrayNew2 = array_values($arrayNew);

            $a++;


        }

        echo "<pre>";
        print_r($arrayNew2);

        exit;

    }


    public function updatePartDescriptionAction()
    {
        $this->view->title = $this->view->translate('update part description');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_detail'), array('fi_id', 'amount', 'balance', 'cur_id', 'id'))
            ->join(array('b' => 'invoice_main'), 'a.invoice_main_id = b.id', array('bill_number'))
            ->join(array('c' => 'invoice_subject'), 'c.invoice_main_id = b.id and c.invoice_detail_id = a.id', array('subject_id'))
            ->join(array('ba' => 'tbl_studentregistration'), 'b.IdStudentRegistration = ba.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme', 'IdLandscape'))
            ->join(array('ls' => 'tbl_landscapesubject'), 'ls.IdSubject = c.subject_id and ls.IdLandscape = ba.IdLandscape', array())
            ->joinLeft(array('l' => 'tbl_landscapeblock'), 'l.idblock=ls.IdLevel', array('l.block as Level'))
            ->join(array('f' => 'fee_item'), 'f.fi_id = a.fi_id', array('fi_code', 'fi_name'))
//					->joinLeft(array('ca' => 'fee_structure_item'), 'ca.fsi_structure_id = ba.fs_id and ca.fsi_item_id = a.fi_id',array('fsi_item_id','fsi_amount','fsi_registration_item_id'))

            ->join(array('d' => 'tbl_studentregsubjects'), 'd.IdSubject = c.subject_id
AND d.IdStudentRegistration = b.IdStudentRegistration', array('Active', 'IdStudentRegistration'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = c.subject_id', array('SubjectName', 'SubCode'))
            ->where('d.Active IN (0,1)')
            ->where("l.block != 1")
//					->where("IFNULL(d.exam_status,'Z') NOT IN ('U','CT','EX')")
            ->where("b.status = 'A'")
            ->where("b.program_id = 5")//cifp
            ->order('a.id asc')
            ->group('a.invoice_main_id');

        $results = $db->fetchAll($select);

        $arrayNew = array();
        $arrayNew = $results;
        $a = 0;
        $arrayNew2 = array();
        foreach ($results as $data) {

            if ($data['Level'] == 2) {
                $feeID = 92;
                $feeName = 'CIFP - Course Registration Part II';
            } elseif ($data['Level'] == 3) {
                $feeID = 93;
                $feeName = 'CIFP - Course Registration Part III- Articleship/PPP';

            }
            //update fi_id
            $data_update = array('fi_id' => $feeID, 'fee_item_description' => $feeName);
            $db->update('invoice_detail', $data_update, 'id = ' . $data['id']);

            $arrayNew2 = array_values($arrayNew);

            $a++;

        }

        echo "<pre>";
        print_r($arrayNew2);

        exit;

    }


    public function invoiceShouldNotAction()
    {
        $this->view->title = $this->view->translate('invoice should not generated');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_detail'), array('fi_id', 'amount', 'balance', 'cur_id', 'id'))
            ->join(array('b' => 'invoice_main'), 'a.invoice_main_id = b.id', array('bill_number'))
            ->join(array('c' => 'invoice_subject'), 'c.invoice_main_id = b.id and c.invoice_detail_id = a.id', array('subject_id'))
            ->join(array('ba' => 'tbl_studentregistration'), 'b.IdStudentRegistration = ba.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme'))
            ->joinLeft(array('sa' => 'student_profile'), 'sa.id = b.IdStudentRegistration', array("CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname) as student_fullname"))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=ba.IdProgramScheme', array('mode_of_program'))
            ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=ps.mode_of_program', array('ProgramMode' => 'DefinitionDesc', 'ProgramModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=ps.mode_of_study', array('StudyMode' => 'DefinitionDesc', 'StudyModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=ps.program_type', array('ProgramType' => 'DefinitionDesc', 'ProgramTypeMy' => 'BahasaIndonesia'))
            ->join(array('d' => 'tbl_studentregsubjects'), 'd.IdSubject = c.subject_id
AND d.IdStudentRegistration = b.IdStudentRegistration and d.IdSubject = c.subject_id', array('Active', 'IdStudentRegistration', 'IdSemesterMain', 'IdCourseTaggingGroup', 'credit_hour_registered'))
            ->join(array('f' => 'fee_item'), 'f.fi_id = a.fi_id', array('fi_code'))
            ->joinLeft(array('ca' => 'fee_structure_item'), 'ca.fsi_structure_id = ba.fs_id and ca.fsi_item_id = a.fi_id', array('fsi_item_id', 'fsi_amount', 'fsi_registration_item_id'))
            ->join(array('am' => 'tbl_studentregsubjects_detail'), 'am.invoice_id = b.id and am.regsub_id = d.IdStudentRegSubjects and am.item_id = ca.fsi_registration_item_id', array('item_id', 'invoice_id', 'created_date', 'created_role', 'idRegDetail' => 'id'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = c.subject_id', array('SubjectName', 'SubCode'))
            ->joinLeft(array('df' => 'tbl_definationms'), 'df.IdDefinition = am.item_id', array('item_name' => 'df.DefinitionDesc'))
            ->where('d.Active IN (4,9)')//repeat/pre repeat
            ->where("f.fi_code NOT IN ('RPT')")
            ->where("IFNULL(d.exam_status,'Z') NOT IN ('U','CT','EX')")
            ->where("b.status = 'A'")
            ->where("a.cn_amount != a.amount")
            ->where('ba.ProfileStatus = 92');


        $results = $db->fetchAll($select);
        $type = 'RPT';

        $arrayNew = array();
        $arrayNew = $results;
        $a = 0;
        $arrayNew2 = array();
        foreach ($results as $data) {

            $credithour = '';
            if (!isset($data['credit_hour_registered'])) {
                $credithour = $data['credit_hour_registered'];
            }

            $selectTagging = $db->select()
                ->from(array('cg' => 'tbl_course_tagging_group'))
                ->joinLeft(array('cgp' => 'course_group_program'), 'cg.`IdCourseTaggingGroup` = cgp.group_id')
                ->joinLeft(array('cgps' => 'tbl_program_scheme'), 'cgps.IdProgramScheme = cgp.program_scheme_id')
                ->where('cg.IdCourseTaggingGroup = ?', $data['IdCourseTaggingGroup']);
            $resultsTagging = $db->fetchRow($selectTagging);

            $modeProgramNew = 0;
            if ($resultsTagging) {
                $modeProgram = $resultsTagging['program_scheme_id'];
                if ($data['IdProgramScheme'] != $modeProgram) {
                    $modeProgramNew = $modeProgram;
                }
            }

            $arrayNew[$a]['scheme1'] = $data['IdProgramScheme'];
            $arrayNew[$a]['scheme2'] = $modeProgramNew;

            $amount = $invoiceClass->calculateInvoiceAmountByCourse($data['IdStudentRegistration'], $data['IdSemesterMain'], $data['subject_id'], $data['item_id'], $type, $credithour, $modeProgramNew);

            if ($amount) {
                $arrayNew[$a]['amountNew'] = $amount[0]['amount'];
                $arrayNew[$a]['cur_code'] = $amount[0]['cur_code'];
                $arrayNew[$a]['fi_id_new'] = $amount[0]['fi_id'];
                $arrayNew[$a]['fi_name_new'] = $amount[0]['fi_name'];
                $arrayNew[$a]['cur_id_new'] = $amount[0]['cur_id'];


            } else {
                $arrayNew[$a]['cur_code'] = NULL;
            }

            /*if($data['item_id'] == 890){//paper
					
					
				}*/

            /*if($data['item_id'] == 889){//course
					unset($arrayNew[$a]);
				}*/


            $arrayNew2 = array_values($arrayNew);

            $a++;


        }

//       echo "<pre>";
//       print_r($arrayNew2);
        $this->view->list = $arrayNew2;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $action = $formData['approve'];
            $id = $formData['id'];

            $i = 0;
            $n = count($id);

            while ($i < $n) {

                $idSel = $id[$i];

                $arraySelected = explode(':', $idSel);

                $dId = $arraySelected[0];
                $idReg = $arraySelected[1];

                $select = $db->select()
                    ->from(array('a' => 'invoice_detail'))
                    ->join(array('b' => 'invoice_main'), 'b.id = a.invoice_main_id')
                    ->where("a.id = ?", $dId);
                $results = $db->fetchRow($select);

                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $percentageDefined = 100;
                $dateCreated = date('Y-m-d');
                if ($action == 'CN') {

                    $generated = $invoiceClass->generateCreditNoteEntryDetail($results['IdStudentRegistration'], $dId, $percentageDefined, 0, 1, $dateCreated);//$studentID,$invoiceID,$percentageDefined=0,$amount=0,$commit=0,$dateCreated=0

                    if ($generated) {
                        $data = array(
                            'invoice_id' => NULL
                        );
                        $db->update('tbl_studentregsubjects_detail', $data, 'id = ' . $idReg);
                    }
                } elseif ($action == 'Invoice') {

                    $select = $db->select()
                        ->from(array('a' => 'tbl_studentregsubjects_detail'), array('id', 'subject_id', 'semester_id', 'item_id', 'invoice_id', 'created_date', 'created_role', 'semester_id', 'student_id'))
                        ->join(array('d' => 'tbl_studentregsubjects'), 'd.IdStudentRegSubjects = a.regsub_id', array('Active', 'credit_hour_registered', 'IdCourseTaggingGroup'))
                        ->join(array('b' => 'tbl_studentregistration'), 'a.student_id = b.IdStudentRegistration', array('registrationId', 'IdStudentRegistration', 'fs_id', 'IdBranch', 'IdProgramScheme'))
                        ->join(array('cgps' => 'tbl_program_scheme'), 'cgps.IdProgramScheme = b.IdProgramScheme', array('mode_of_program'))
                        ->where('a.id = ?', $idReg)
                        ->where('a.invoice_id is null');
                    $results = $db->fetchRow($select);

                    if ($results) {

                        if ($results['Active'] == 9) {//pre-repeat
                            $type = 'RPT';
                        } elseif ($results['Active'] == 0) {//pre-registered
                            $type = '';
                        }

                        $credithour = '';
                        if (!isset($results['credit_hour_registered'])) {
                            $credithour = $results['credit_hour_registered'];
                        }

                        //get course section
                        $selectTagging = $db->select()
                            ->from(array('cg' => 'tbl_course_tagging_group'))
                            ->joinLeft(array('cgp' => 'course_group_program'), 'cg.`IdCourseTaggingGroup` = cgp.group_id')
                            //					   ->joinLeft(array('cgps'=>'tbl_program_scheme'),'cgps.IdProgramScheme = cgp.program_scheme_id')
                            ->where('cg.IdCourseTaggingGroup = ?', $results['IdCourseTaggingGroup']);

                        $resultsTagging = $db->fetchRow($selectTagging);
                        $modeProgramNew = 0;
                        if ($resultsTagging) {
                            $modeProgram = $resultsTagging['program_scheme_id'];
                            if ($results['IdProgramScheme'] != $modeProgram) {
                                $modeProgramNew = $modeProgram;
                            }
                        }

                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        $invoiceClass->generateInvoiceStudent($results['student_id'], $results['semester_id'], $results['subject_id'], $type, $credithour, 1, $modeProgramNew);
                    }

                }
                $i++;
            }

            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Data has been updated');
            //redirect
            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'migrate', 'action' => 'invoice-should-not'), 'default', true));
        }

    }

    public function updateNegativeAmountMigratedAction()
    {
        $this->view->title = $this->view->translate('update -ve bill_amount');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('*', 'idMain' => 'a.id'))
            ->join(array('b' => 'invoice_detail'), 'b.invoice_main_id = a.id', array('*', 'idDet' => 'b.id'))
            ->where('a.MigrateCode is not null')
            ->where("a.bill_amount like '-%'");

        $results = $db->fetchAll($select);

        $a = 0;
        foreach ($results as $data) {
            $amount = str_replace('-', '', $data['bill_amount']);
            $paid = str_replace('-', '', $data['bill_paid']);
            //update
            $data_update = array('bill_amount' => $amount, 'bill_paid' => $paid);
            $db->update('invoice_main', $data_update, 'id = ' . $data['idMain']);

            $data_update2 = array('amount' => $amount, 'paid' => $paid);
            $db->update('invoice_detail', $data_update2, 'id = ' . $data['idDet']);


            $a++;

        }


        exit;

    }


    function saveArticleshipAction()
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();

        $select = "SELECT b.appl_fname,b.appl_lname, a.registrationId,a.IdStudentRegistration FROM tbl_studentregistration a JOIN student_profile b ON ( b.id=a.sp_id )
	WHERE IdStudentRegistration IN (
	SELECT student_id FROM thesis_articleship
	WHERE student_id NOT IN (
	SELECT IdStudentRegistration FROM tbl_studentregsubjects
	WHERE IdSubject IN (85) ) )";
        $results = $db->fetchAll($select);

        echo "<pre>";
        print_r($results);

        $idSemester = 6;

        foreach ($results as $data) {

            $subject["IdStudentRegistration"] = $data['IdStudentRegistration'];
            $subject["IdSubject"] = 85;
            $subject["IdSemesterMain"] = $idSemester;
            $subject["SemesterLevel"] = 0;
            $subject["IdLandscapeSub"] = 0;
            $subject["Active"] = 0; //register
            $subject["exam_status"] = NULL;
            $subject["UpdDate"] = date('Y-m-d H:i:s');
            $subject["UpdUser"] = 665;
            $IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);

            //3)add table studentregsubject audit trail/history
            $subject2['IdStudentRegSubjects'] = $IdStudentRegSubjects;
            $subject2["IdStudentRegistration"] = $data['IdStudentRegistration'];
            $subject2["IdSubject"] = 85;
            $subject2["IdSemesterMain"] = $idSemester;
            $subject2["Active"] = 0;  //1-register
            $subject2["exam_status"] = NULL;
            $subject2["UpdDate"] = date('Y-m-d H:i:s');
            $subject2["UpdUser"] = 665;

            $historyDB->addData($subject2);

            $dataitem = array(
                'student_id' => $data['IdStudentRegistration'],
                'regsub_id' => $IdStudentRegSubjects,
                'semester_id' => $idSemester,
                'subject_id' => 85,
                'item_id' => 890, //PAPER
                'section_id' => 0,
                'ec_country' => 0,
                'ec_city' => 0,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => 665,
                'created_role' => 'admin',
                'ses_id' => 0
            );


            $idRegSubjectItem = $regSubjectItem->addData($dataitem);

            $dataitem['id'] = $idRegSubjectItem;
            $dataitem["message"] = 'Student Course Regisration: Articleship';
            $dataitem['createddt'] = date("Y-m-d H:i:s");
            $dataitem['createdby'] = $auth->getIdentity()->id;

            $db->insert('tbl_studentregsubjects_detail_history', $dataitem);
        }

        $select2 = "SELECT b.appl_fname,b.appl_lname, a.registrationId,a.IdStudentRegistration FROM tbl_studentregistration a JOIN student_profile b ON ( b.id=a.sp_id ) WHERE IdStudentRegistration IN ( SELECT student_id FROM thesis_exemption WHERE student_id NOT IN ( SELECT IdStudentRegistration FROM tbl_studentregsubjects WHERE IdSubject = 84 ) )";
        $results2 = $db->fetchAll($select2);

        echo "<pre>";
        print_r($results2);

        foreach ($results2 as $data) {

            $subject["IdStudentRegistration"] = $data['IdStudentRegistration'];
            $subject["IdSubject"] = 84;
            $subject["IdSemesterMain"] = $idSemester;
            $subject["SemesterLevel"] = 0;
            $subject["IdLandscapeSub"] = 0;
            $subject["Active"] = 0; //register
            $subject["exam_status"] = NULL;
            $subject["UpdDate"] = date('Y-m-d H:i:s');
            $subject["UpdUser"] = 665;
            $IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);

            //3)add table studentregsubject audit trail/history
            $subject2['IdStudentRegSubjects'] = $IdStudentRegSubjects;
            $subject2["IdStudentRegistration"] = $data['IdStudentRegistration'];
            $subject2["IdSubject"] = 84;
            $subject2["IdSemesterMain"] = $idSemester;
            $subject2["Active"] = 0;  //1-register
            $subject2["exam_status"] = NULL;
            $subject2["UpdDate"] = date('Y-m-d H:i:s');
            $subject2["UpdUser"] = 665;

            $historyDB->addData($subject2);

            $dataitem = array(
                'student_id' => $data['IdStudentRegistration'],
                'regsub_id' => $IdStudentRegSubjects,
                'semester_id' => $idSemester,
                'subject_id' => 84,
                'item_id' => 890, //PAPER
                'section_id' => 0,
                'ec_country' => 0,
                'ec_city' => 0,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => 665,
                'created_role' => 'admin',
                'ses_id' => 0
            );


            $idRegSubjectItem = $regSubjectItem->addData($dataitem);

            $dataitem['id'] = $idRegSubjectItem;
            $dataitem["message"] = 'Student Course Regisration: PPP';
            $dataitem['createddt'] = date("Y-m-d H:i:s");
            $dataitem['createdby'] = $auth->getIdentity()->id;

            $db->insert('tbl_studentregsubjects_detail_history', $dataitem);

        }

        exit;
    }

    public function proformaGstAction()
    {
        $this->view->title = $this->view->translate('Proforma GST');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'proforma_invoice_main'), array('*', 'idMain' => 'a.id'))
            ->join(array('b' => 'proforma_invoice_detail'), 'b.proforma_invoice_main_id = a.id', array('*', 'idDet' => 'b.id'))
            ->where("a.status = 'A'")
            ->where("a.invoice_id = 0")
            ->where("a.bill_amount_gst != 0")
            ->where("b.fi_id IN (76,77,78,79)");

        $results = $db->fetchAll($select);

        $a = 0;
        foreach ($results as $data) {

            /*$amount = $data['bill_description']." (inclusive 6% GST)" ;
//				//update
			$data_update = array('bill_description' =>$amount);
			$db->update('proforma_invoice_main',$data_update, 'id = '.$data['idMain']);
			
			$amount2 = $data['fee_item_description'] ." (inclusive 6% GST)" ;
			$data_update2 = array('fee_item_description' =>$amount2);
			$db->update('proforma_invoice_detail',$data_update2, 'id = '.$data['idDet']);*/


            $a++;

        }

        echo "<pre>";
        print_r($results);

        exit;

    }

    public function updateSemesterAction()
    {
        $this->view->title = $this->view->translate('Updte Semester');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('id', 'semester', 'invoice_date'))
            ->join(array('b' => 'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration ', array())
            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram', array('IdScheme'))
            ->where('a.semester is null')
            ->where('a.proforma_invoice_id  = 0');
        $results = $db->fetchAll($select);


        foreach ($results as $key => $data) {
            $invdate2 = $data['invoice_date'];
            $selectCur2 = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where('a.SemesterMainStartDate <= ?', $invdate2)
                ->where('a.SemesterMainEndDate >= ?', $invdate2)
                ->where('a.IdScheme = ?', $data['IdScheme']);
            $resultsCur2 = $db->fetchRow($selectCur2);


            $data_update = array('semester' => $resultsCur2['IdSemesterMaster']);
            $db->update('invoice_main', $data_update, 'id = ' . $data['id']);


        }
        echo $key;
        exit;


    }

    public function updateSemesterApplicantAction()
    {
        $this->view->title = $this->view->translate('Updte Semester');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('id', 'semester'))
            ->join(array('m' => 'applicant_transaction'), 'a.trans_id = m.at_trans_id', array('at_intake'))
            ->join(array('b' => 'applicant_program'), 'a.trans_id = b.ap_at_trans_id')
            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.ap_prog_id')
            ->joinLeft(array('i' => 'tbl_intake'), 'i.IdIntake = m.at_intake', array('sem_year', 'sem_seq'))
            ->where('a.semester is null')
            ->where('a.proforma_invoice_id  != 0');
//					->where('a.IdStudentRegistration is null');
        $results = $db->fetchAll($select);

        foreach ($results as $data) {

            //get current semester
            $landscapeNewDB = new App_Model_General_DbTable_Landscape();
            $intake = $landscapeNewDB->getIntakeInfo($data['at_intake']);
            $semesterinfo = $landscapeNewDB->getSemesterInfo($data['sem_year'], $data['sem_seq']);

            $semester = $semesterinfo['IdSemesterMaster'];

            $data_update = array('semester' => $semesterinfo['IdSemesterMaster']);
            $db->update('invoice_main', $data_update, 'id = ' . $data['id']);


        }
        exit;


    }

    public function updateNoSemesterAction()
    {
        $this->view->title = $this->view->translate('Updte Semester');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('id', 'semester', 'invoice_date'))
            ->join(array('b' => 'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration ', array())
            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.IdProgram', array('IdScheme'))
            ->where('a.semester is null')
            ->where('a.proforma_invoice_id  = 0');
        $results = $db->fetchAll($select);


        foreach ($results as $key => $data) {

            $invdate2 = $data['invoice_date'];


            $selectCur2 = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where('a.SemesterMainStartDate <= ?', $invdate2)
                ->where('a.SemesterMainEndDate >= ?', $invdate2)
                ->where('a.IdScheme = ?', $data['IdScheme']);
            $resultsCur2 = $db->fetchRow($selectCur2);

            $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
            $prvSemester = $semesterDb->getPreviousSemester($resultsCur2);


            $data_update = array('semester' => $prvSemester['IdSemesterMaster']);
            $db->update('invoice_main', $data_update, 'id = ' . $data['id']);


        }
        echo $key;
        exit;


    }

    public function updateNoSemesterApplicantAction()
    {
        $this->view->title = $this->view->translate('Updte Semester');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('id', 'semester', 'invoice_date'))
            ->join(array('m' => 'applicant_transaction'), 'a.trans_id = m.at_trans_id', array('at_intake'))
            ->join(array('b' => 'applicant_program'), 'a.trans_id = b.ap_at_trans_id')
            ->joinLeft(array('c' => 'tbl_program'), 'c.IdProgram= b.ap_prog_id', array('IdScheme'))
            ->joinLeft(array('i' => 'tbl_intake'), 'i.IdIntake = m.at_intake', array('sem_year', 'sem_seq'))
            ->where('a.semester is null');
        $results = $db->fetchAll($select);


        foreach ($results as $key => $data) {

            $invdate2 = $data['invoice_date'];

            $selectCur2 = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where('a.SemesterMainStartDate <= ?', $invdate2)
                ->where('a.SemesterMainEndDate >= ?', $invdate2)
                ->where('a.IdScheme = ?', $data['IdScheme']);
            $resultsCur2 = $db->fetchRow($selectCur2);

            $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
            $prvSemester = $semesterDb->getPreviousSemester($resultsCur2);


            $data_update = array('semester' => $prvSemester['IdSemesterMaster']);
            $db->update('invoice_main', $data_update, 'id = ' . $data['id']);


        }
        echo $key;
        exit;


    }

    public function updatePaidAction()
    {
        $this->view->title = $this->view->translate('Updte Paid');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'data_fin_paid'), array('invno', 'status', 'idFin' => 'a.id'))
            ->join(array('b' => 'invoice_main'), 'a.invno = b.bill_number ', array('idMain' => 'b.id', '*'))
            ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = b.id', array('id_detail' => 'c.id', '*'))
            ->where('a.status = 0');
        $results = $db->fetchAll($select);


        foreach ($results as $key => $data) {
            $invID = $data['idMain'];
            $invDetID = $data['id_detail'];

            $data_update = array('bill_paid' => $data['bill_amount'], 'bill_balance' => 0);
            $db->update('invoice_main', $data_update, 'id = ' . $invID);

            $data_update = array('paid' => $data['amount'], 'balance' => 0);
            $db->update('invoice_detail', $data_update, 'id = ' . $invDetID);

            $data_update = array('status' => 1, 'dateUpd' => date('Y-m-d H:i:s'));
            $db->update('data_fin_paid', $data_update, 'id = ' . $data['idFin']);


        }
        echo $key;
        exit;


    }

    public function updateInvoiceNotAgeingAction()
    {
        $this->view->title = $this->view->translate('Update invoice not include in ageing report');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = 'Select invoice_no from invoice_aging';
        $results1 = $db->fetchAll($sql);
        $select = $db->select()
            ->from(array('a' => 'invoice_main'), array('*', 'idMain' => 'a.id'))
            ->where('a.bill_number NOT IN (?)', $results1)
            ->where('a.MigrateCode IS NOT NULL')
            ->where('a.not_include = 0');
        $results = $db->fetchAll($select);

        if ($results) {
            foreach ($results as $key => $data) {
                $invID = $data['idMain'];

                $data_update = array('not_include' => 1);
                $db->update('invoice_main', $data_update, 'id = ' . $invID);
            }

            echo $key;
        }
        exit;


    }

    public function cancelMigsTransactionAction()
    {
        $this->view->title = $this->view->translate('Cancel Unsuccessful Transaction MIGS');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'transaction_migs'))
            ->join(array('b' => 'receipt'), 'b.migs_id = a.mt_id')
            ->where("a.mt_rep_txn_response_code != '0'")
            ->where("a.mt_status = 'SUCCESS'")
            ->where("b.rcp_status = 'APPROVE'");
        $results = $db->fetchAll($select);

        if ($results) {
            foreach ($results as $key => $data) {
                $mt_id = $data['mt_id'];
                $rcp_id = $data['rcp_id'];
                $mt_from = $data['mt_from'];
                $trans_id = $data['mt_trans_id'];
                $IdStudentRegistration = $data['mt_idStudentRegistration'];

                $selectReceipt = $db->select()
                    ->from(array('a' => 'receipt_invoice'))
                    ->join(array('b' => 'invoice_main'), 'b.id = a.rcp_inv_invoice_id')
                    ->where("a.rcp_inv_rcp_id = ?", $rcp_id)
                    ->where("b.status = 'A'");
                $resultsReceipt = $db->fetchAll($selectReceipt);

//	       		if($mt_from == 0){//online application
                /*
       				 * 1.generate cn
       				 * 2.up kan proforma balik
       				 */


                foreach ($resultsReceipt as $rcp) {
                    $invoiceID = $rcp['rcp_inv_invoice_id'];
                    echo $mt_id . ' - ' . $invoiceDetID = $rcp['rcp_inv_invoice_dtl_id'];

                    $proformaID = $rcp['proforma_invoice_id'];

                    echo ' - ' . $rcp['bill_number'] . ' - ' . $proformaID . ' - ' . $IdStudentRegistration;
                    echo "<hr>";


                }


//	       		}elseif($mt_from == 1){//student portal
//	       			
//	       		}


                $data_update = array('not_include' => 1);
//				$db->update('invoice_main',$data_update, 'id = '.$invID);
            }

        }

        exit;
    }


    public function issueInvoiceFromProformaAction()
    {
        $this->view->title = $this->view->translate('Generate Invoice From Proforma');

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_intake'));
        $this->view->intake = $db->fetchAll($select);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $intake = $formData['intake'];

            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a' => 'proforma_invoice_main'))
                ->join(array('b' => 'applicant_transaction'), 'b.at_trans_id = a.trans_id')
                ->where("a.invoice_id = '0'")
                ->where("a.status = 'W'")
                ->where('b.at_intake = ?', $intake);
            $results = $db->fetchAll($select);
            $i = 0;
            if ($results) {

                foreach ($results as $key => $data) {

                    $id = $data['id'];
                    //generate invoice from proforma
                    $invoice_helper = new icampus_Function_Studentfinance_Invoice();


                    $auth = Zend_Auth::getInstance();

                    try {
                        $status = $invoice_helper->generateInvoiceFromProforma($id, $auth->getIdentity()->id);
                        $i++;
                    } catch (Exception $e) {
                        $result['msg'] = "Error generate invoice: " . $e->getMessage();
                    }
                }

            }
            $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i invoice generated")));

            $this->updateIdStudentRegistration($intake);
        }


    }

    public function updateIdStudentRegistration($intake)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'invoice_main'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.transaction_id = a.trans_id')
            ->where("a.IdStudentRegistration IS NULL")
            ->where("a.proforma_invoice_id != 0")
            ->where('b.IdIntake = ?', $intake);
        $results = $db->fetchAll($select);

        foreach ($results as $data) {

            $data_update = array('IdStudentRegistration' => $data['IdStudentRegistration']);
            $db->update('invoice_main', $data_update, 'id = ' . $data['id']);


        }

    }

    public function generateYearlyInvoiceAction()
    {
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $this->view->title = $this->view->translate('Generate Yearly Invoice');

        //intake
        $intakeDb = new App_Model_General_DbTable_Intake();
        $this->view->intakeData = $intakeDb->fngetIntakeList();

        $i = 0;
        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $intakeid = $formData['intake'];

            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->where("a.profileStatus = 92")
                ->where('a.IdIntake = ?', $intakeid);
            $results = $db->fetchAll($select);

            if ($results)
            {

                $i=1;
                foreach ($results as $data) {
                    $IdStudentRegistration = $data['IdStudentRegistration'];

                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceClass->generateInvoiceYearly($IdStudentRegistration, 1);
                    $i++;
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("$i invoice generated")));
        }

    }

    public function getStatusAction()
    {
        $type = $this->_getParam('type', null);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $db = Zend_Db_Table::getDefaultAdapter();

        if($type == 2){ //student
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'))
                ->where('a.idDefType = 20');

        }elseif($type == 1){
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'))
                ->where('a.idDefType = 56');
        }

        $result = $db->fetchAll($select);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }


    public function updateCnLinkAction(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
            ->from(array('a'=>'credit_note'))
            ->where('a.cn_create_date like "%2015%"')
            ->where('a.cn_status = ?', 'A')
            ->where('a.MigrateCode IS NOT NULL');

        $result1 = $db->fetchAll($select1);

        if ($result1){
            foreach ($result1 as $result1Loop){
                $invno = trim($result1Loop['cn_description']);
                var_dump($invno);

                $select2 = $db->select()
                    ->from(array('a'=>'invoice_main'))
                    ->where('a.bill_number = ?', $invno);

                $result2 = $db->fetchRow($select2);

                var_dump($result2);

                $select3 = $db->select()
                    ->from(array('a'=>'credit_note_detail'))
                    ->where('a.cn_id = ?', $result1Loop['cn_id']);

                $result3 = $db->fetchAll($select3);

                $select4 = $db->select()
                    ->from(array('a'=>'invoice_detail'))
                    ->where('a.invoice_main_id = ?', $result2['id']);

                $result4 = $db->fetchRow($select4);

                var_dump($result3);
                var_dump($result4);

                $data2 = array(
                    'cn_invoice_id'=>$result2['id']
                );
                $db->update('credit_note', $data2, 'cn_id = '.$result1Loop['cn_id']);

                if ($result3){
                    foreach ($result3 as $result3Loop){
                        $data1 = array(
                            'invoice_main_id'=>$result2['id'],
                            'invoice_detail_id'=>$result4['id']
                        );
                        $db->update('credit_note_detail', $data1, 'id = '.$result3Loop['id']);
                    }
                }
            }
        }

        exit;
    }

    public function updateInvoiceBalanceAction(){
        $db = Zend_Db_Table::getDefaultAdapter();

        $billArray = array(
            'IN2016-03040',
            'IN2016-02688',
            'IN2016-02663',
            'IN2016-02478',
            'IN2016-02352',
            'IN2016-02321',
            'IN2016-02032',
            'IN2016-01796',
            'IN2016-01843',
            'IN2016-01845',
            'IN2016-01922',
            'IN2016-01642',
            'IN2016-01643',
            'IN2016-01319',
            'IN2016-01320',
            'IN2016-01321',
            'IN2016-01097',
            'IN2016-01098',
            'IN2016-01099',
            'IN2016-01100',
            'IN2016-01102',
            'IN2016-01105',
            'IN2016-01127',
            'IN2016-01129',
            'IN2016-00978',
            'IN2016-00979',
            'IN2016-00980',
            'IN2016-00981',
            'IN2016-01013',
            'IN2016-00888',
            'IN2016-00889',
            'IN2016-00890',
            'IN2016-00843',
            'IN2016-00844',
            'IN2016-00845',
            'IN2016-00600',
            'IN2016-00567',
            'IN2016-00568',
            'IN2016-00569',
            'IN2016-00482',
            'IN2016-00238',
            'IN2016-00253',
            'IN2016-00219',
            'IN2016-00220',
            'IN2016-00026',
            'IN2016-00002',
            'IN2015-11407',
            'IN2016-02987',
            'IN2015-11319',
            'IN2015-11320',
            'IN2015-11192',
            'IN2015-11156',
            'IN2015-10644',
            'IN2015-10582',
            'IN2015-10404',
            'IN2015-10125',
            'IN2015-10041',
            'IN2015-10036',
            'IN2015-09762',
            'IN2015-09763',
            'IN2015-09764',
            'IN2015-09749',
            'IN2015-09623',
            'IN2015-09731',
            'IN2015-09406',
            'IN2015-09407',
            'IN2015-09299',
            'IN2015-09096',
            'IN2015-09097',
            'IN2015-09098',
            'IN2015-08809',
            'IN2015-08810',
            'IN2015-08800',
            'IN2015-08354',
            'IN2015-08129',
            'IN2015-08130',
            'IN2015-08204',
            'IN2015-08220',
            'IN2015-07909',
            'IN2015-07910',
            'IN2015-07911',
            'IN2015-07912',
            'IN2015-07913',
            'IN2015-07914',
            'IN2015-07928',
            'IN2015-07929',
            'IN2015-07628',
            'IN2015-07657',
            'IN2015-07658',
            'IN2015-07428',
            'IN2015-07441',
            'IN2015-07255',
            'IN2015-07256',
            'IN2015-07257',
            'IN2015-07258',
            'IN2015-07417',
            'IN2015-06864',
            'IN2015-06865',
            'IN2015-06869',
            'IN2015-06870',
            'IN2015-06964',
            'IN2015-06578',
            'IN2015-06582',
            'IN2015-06584',
            'IN2015-06585',
            'IN2015-05796',
            'IN2015-05704',
            'IN2015-05705',
            'IN2015-05645',
            'IN2015-05595',
            'IN2015-05522',
            'IN2015-05530',
            'IN2015-05471',
            'IN2015-05431',
            'IN2015-05224',
            'IN2015-05225',
            'IN2015-04997',
            'IN2015-04969',
            'IN2015-04911',
            'IN2015-04766',
            'IN2015-04479',
            'IN2015-04512',
            'IN2015-04513',
            'IN2015-04239',
            'IN2015-04010',
            'IN2015-03657',
            'IN2015-03660',
            'IN2015-03064',
            'IN2015-02897',
            'IN2015-02875',
            'IN2015-02836',
            'IN2015-02858',
            'IN2015-02861',
            'IN2015-02734',
            'IN2015-02735',
            'IN2015-02606',
            'IN2015-02685',
            'IN2015-02494',
            'IN2015-02592',
            'IN2015-02446',
            'IN2015-02028',
            'IN2015-02029',
            'IN2015-01914',
            'IN2015-01915',
            'IN2015-01800',
            'IN2015-01786',
            'IN2015-01826',
            'IN2015-01828',
            'IN2015-01307',
            'IN2015-01365',
            'IN2015-01368',
            'IN2015-01382',
            'IN2015-01247',
            'IN2015-00884',
            'IN2015-00848',
            'IN2015-00819',
            'IN2015-00069',
            'IN2015-00070',
            'IN2015-00071',
            'IN1501/00126',
            'IN1409/00490',
            'IN1409/00591',
            'IN1409/00653',
            'IN14/06863',
            'IN14/06705',
            'INV14/0430',
            'INV14/0439',
            'IN1406/00366',
            'IN14/01775',
            'IN1309/00181',
            'IN1306/00103',
            'IN12/03778',
            'IN12/00133',
            'IN10/00163'
        );

        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->where('a.bill_number IN (?)', $billArray);

        $result = $db->fetchAll($select);

        //var_dump($result);

        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
        $count = 0;
        if ($result) {
            foreach ($result as $key => $entry) {
                $selfPaid = Studentfinance_Model_DbTable_PaymentAmount::getSelfPaid($entry['id']);

                $advPaid = Studentfinance_Model_DbTable_PaymentAmount::getAdvance($entry['id']);

                $disMain = Studentfinance_Model_DbTable_PaymentAmount::getDiscount($entry['id']);

                $sponsorPaid = Studentfinance_Model_DbTable_PaymentAmount::getSponsorPaid($entry['id']);

                $cnPaid = Studentfinance_Model_DbTable_PaymentAmount::getCreditNote($entry['id']);

                $advamount = Studentfinance_Model_DbTable_PaymentAmount::getAdvanceTransfer($entry['id']);

                if ($entry['currency_id']!=1){
                    //self paid
                    if ($entry['MigrateCode']==null) {
                        if ($selfPaid[0]['selfpaid'] != null){
                            if ($selfPaid[0]['rcp_cur_id']!=1){
                                $selfPaidAmount = $selfPaid[0]['selfpaid'];
                            }else{
                                $exrate = $curRateDB->getRateByDate($selfPaid[0]['rcp_cur_id'], $selfPaid[0]['rcp_receive_date']);
                                $selfPaidAmount = ($selfPaid[0]['selfpaid']/$exrate['cr_exchange_rate']);
                            }
                        }else{
                            $selfPaidAmount = 0.00;
                        }
                    }else{
                        $selfPaidAmount = $entry['bill_paid'];
                    }

                    //advance payment
                    if ($advPaid[0]['advancepaid'] != null){
                        if ($advPaid[0]['advpy_cur_id']!=1){
                            $advPaidAmount = $advPaid[0]['advancepaid'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($advPaid[0]['advpy_cur_id'], $advPaid[0]['advpy_date']);
                            $advPaidAmount = ($advPaid[0]['advancepaid']/$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $advPaidAmount = 0.00;
                    }

                    //discount payment
                    if ($disMain[0]['discount'] != null){
                        if ($disMain[0]['currency']!=1){
                            $disPaidAmount = $disMain[0]['discount'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($disMain[0]['currency'], $disMain[0]['dcnt_create_date']);
                            $disPaidAmount = ($disMain[0]['discount']/$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $disPaidAmount = 0.00;
                    }

                    //sponsor payment
                    if ($sponsorPaid[0]['sponsorpaid'] != null){
                        if ($sponsorPaid[0]['rcp_cur_id']!=1){
                            $sponsorPaidAmount = $sponsorPaid[0]['sponsorpaid'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($sponsorPaid[0]['rcp_cur_id'], $sponsorPaid[0]['rcp_receive_date']);
                            $sponsorPaidAmount = ($sponsorPaid[0]['sponsorpaid']/$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $sponsorPaidAmount = 0.00;
                    }

                    //credit note payment
                    if ($cnPaid[0]['cnpaid'] != null){
                        if ($cnPaid[0]['currency']!=1){
                            $cnPaidAmount = $cnPaid[0]['cnpaid'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($cnPaid[0]['currency'], $cnPaid[0]['cn_create_date']);
                            $cnPaidAmount = ($cnPaid[0]['cnpaid']/$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $cnPaidAmount = 0.00;
                    }

                    //advance payment transfer
                    if ($advamount[0]['advanceamount'] != null){
                        if ($advamount[0]['currency']!=1){
                            $advTransferAmount = $advamount[0]['advanceamount'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($advamount[0]['currency'], $advamount[0]['advpy_date']);
                            $advTransferAmount = ($advamount[0]['advanceamount']/$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $advTransferAmount = 0.00;
                    }
                }else{
                    //self paid
                    if ($entry['MigrateCode']==null) {
                        if ($selfPaid[0]['selfpaid'] != null){
                            if ($selfPaid[0]['rcp_cur_id']==1){
                                $selfPaidAmount = $selfPaid[0]['selfpaid'];
                            }else{
                                $exrate = $curRateDB->getRateByDate($selfPaid[0]['rcp_cur_id'], $selfPaid[0]['rcp_receive_date']);
                                $selfPaidAmount = ($selfPaid[0]['selfpaid']*$exrate['cr_exchange_rate']);
                            }
                        }else{
                            $selfPaidAmount = 0.00;
                        }
                    }else{
                        $selfPaidAmount = $entry['bill_paid'];
                    }

                    //advance payment
                    if ($advPaid[0]['advancepaid'] != null){
                        if ($advPaid[0]['advpy_cur_id']==1){
                            $advPaidAmount = $advPaid[0]['advancepaid'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($advPaid[0]['advpy_cur_id'], $advPaid[0]['advpy_date']);
                            $advPaidAmount = ($advPaid[0]['advancepaid']*$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $advPaidAmount = 0.00;
                    }

                    //discount payment
                    if ($disMain[0]['discount'] != null){
                        if ($disMain[0]['currency']==1){
                            $disPaidAmount = $disMain[0]['discount'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($disMain[0]['currency'], $disMain[0]['dcnt_create_date']);
                            $disPaidAmount = ($disMain[0]['discount']*$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $disPaidAmount = 0.00;
                    }

                    //sponsor payment
                    if ($sponsorPaid[0]['sponsorpaid'] != null){
                        if ($sponsorPaid[0]['rcp_cur_id']==1){
                            $sponsorPaidAmount = $sponsorPaid[0]['sponsorpaid'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($sponsorPaid[0]['rcp_cur_id'], $sponsorPaid[0]['rcp_receive_date']);
                            $sponsorPaidAmount = ($sponsorPaid[0]['sponsorpaid']*$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $sponsorPaidAmount = 0.00;
                    }

                    //credit note payment
                    if ($cnPaid[0]['cnpaid'] != null){
                        if ($cnPaid[0]['currency']==1){
                            $cnPaidAmount = $cnPaid[0]['cnpaid'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($cnPaid[0]['currency'], $cnPaid[0]['cn_create_date']);
                            $cnPaidAmount = ($cnPaid[0]['cnpaid']*$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $cnPaidAmount = 0.00;
                    }

                    //advance payment transfer
                    if ($advamount[0]['advanceamount'] != null){
                        if ($advamount[0]['currency']==1){
                            $advTransferAmount = $advamount[0]['advanceamount'];
                        }else{
                            $exrate = $curRateDB->getRateByDate($advamount[0]['currency'], $advamount[0]['advpy_date']);
                            $advTransferAmount = ($advamount[0]['advanceamount']*$exrate['cr_exchange_rate']);
                        }
                    }else{
                        $advTransferAmount = 0.00;
                    }
                }

                $balance = $entry['bill_amount']-$selfPaidAmount-$advPaidAmount-$disPaidAmount-$sponsorPaidAmount-$cnPaidAmount+$advTransferAmount;

                if ($entry['bill_balance'] != $balance) {
                    $count++;
                    echo '----------------------------------------------------------------------------------------<br />';
                    var_dump($entry['bill_number']);
                    var_dump($entry['bill_balance']);
                    var_dump($entry['bill_amount']);
                    var_dump($selfPaidAmount);
                    var_dump($advPaidAmount);
                    var_dump($disPaidAmount);
                    var_dump($sponsorPaidAmount);
                    var_dump($cnPaidAmount);
                    var_dump($advTransferAmount);
                    var_dump($balance);
                    echo '----------------------------------------------------------------------------------------<br />';

                    $data = array(
                        'bill_balance'=>number_format($balance, 2, '.', ','),
                        'bill_paid'=>number_format(($selfPaidAmount+$advPaidAmount+$sponsorPaidAmount), 2, '.', ','),
                        'cn_amount'=>number_format($cnPaidAmount, 2, '.', ','),
                        'dn_amount'=>number_format($disPaidAmount, 2, '.', ',')
                    );
                    $db->update('invoice_main', $data, 'id = '.$entry['id']);
                }
            }
        }
        var_dump($count);
        exit;
    }

    public function addMigrationAction(){
        $this->view->title = $this->view->translate('Add Migration Data');

        $definationModel = new App_Model_Definitiontype();

        $payee_type_list = $definationModel->fnGetDefinationsByLocale('Payee Type');
        unset($payee_type_list[0]);
        $this->view->payee_type_list = $payee_type_list;

        $currencyDb = new Studentfinance_Model_DbTable_Currency();
        $this->view->currencyList = $currencyDb->getList();

        //payment group
        $payment_group_list = $definationModel->fnGetDefinationsByLocale('Payment Mode Grouping');
        $this->view->payment_group = $payment_group_list;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //dd($formData);
            //exit;

            if ($formData['payee_id'] == ''){
                $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'Please select student');
                $this->_redirect($this->baseUrl . '/studentfinance/migrate/add-migration');
            }

            if (!isset($formData['type_entry']) || $formData['type_entry']==''){
                $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'Please select type');
                $this->_redirect($this->baseUrl . '/studentfinance/migrate/add-migration');
            }

            $auth = Zend_Auth::getInstance();
            $getUserIdentity = $auth->getIdentity();

            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $registration = $studentRegistrationDb->getTheStudentRegistrationDetail($formData['payee_id']);

            switch($formData['type_entry']){
                case 1: //invoice
                    $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                    $currency = $currencyDb->getCurrentExchangeRate($formData['currency_id']);

                    $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
                    $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();

                    $bill_no =  $this->getBillSeq(2, date('Y', strtotime($formData['invoice_date'])));
                    $data_invoice = array(
                        'bill_number' => $bill_no,
                        'appl_id' => 0,
                        'trans_id' => 0,
                        'IdStudentRegistration' => $formData['payee_id'],
                        'academic_year' => 0,
                        'semester' => $formData['semester_id'],
                        'bill_amount' => $formData['bill_amount'],
                        'bill_paid' => 0,
                        'bill_balance' => $formData['bill_amount'],
                        'bill_description' => $formData['bill_description'],
                        'program_id' => $registration['IdProgram'],
                        'fs_id' => $registration['fs_id'],
                        'currency_id' => $formData['currency_id'],
                        'exchange_rate' => $currency['cr_id'],
                        'invoice_date' => date('Y-m-d',strtotime($formData['invoice_date'])),
                        'status' => 'A',
                        'proforma_invoice_id' => 0,
                        'date_create'=>date('Y-m-d',strtotime($formData['invoice_date'])),
                        'creator'=>$getUserIdentity->id,
                        'not_include'=>1,
                        'MigrateCode'=>999
                    );
                    $invoice_id = $invoiceMainDb->insert($data_invoice);

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invoice_id,
                        'fi_id' =>  $formData['fi_id'],
                        'fee_item_description' =>  $formData['bill_description'],
                        'cur_id' =>  $formData['currency_id'],
                        'amount' => $formData['bill_amount'],
                        'balance' => $formData['bill_amount'],
                        'exchange_rate' => $currency['cr_id']
                    );
                    $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                    $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Invoice saved');
                    $this->_redirect($this->baseUrl . '/studentfinance/migrate/edit-migration');
                    break;
                case 2: //receipt
                    $receiptDb = new Studentfinance_Model_DbTable_Receipt();
                    $paymentDb = new Studentfinance_Model_DbTable_Payment();
                    $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                    $currency = $currencyDb->getCurrentExchangeRate($formData['1_currency']);

                    $receiptDb = new Studentfinance_Model_DbTable_Receipt();
                    $receipt_no = $this->getBillSeq(5, date('Y', strtotime($formData['payment_date'])));

                    $receipt_data = array(
                        'rcp_no' => $receipt_no,
                        'rcp_account_code' => 7,
                        'rcp_date' => date('Y-m-d', strtotime($formData['payment_date'])),
                        'rcp_receive_date' => date('Y-m-d', strtotime($formData['payment_date'])),
                        'rcp_payee_type' => $formData['payee_type'],
                        'rcp_idStudentRegistration' => $registration['IdStudentRegistration'],
                        'rcp_description' => $formData['payment_description'],
                        'rcp_amount' => $formData['1_amount'],
                        'rcp_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($formData['1_amount'], $currency['cr_cur_id']),
                        'rcp_adv_payment_amt' => 0.00,
                        'rcp_cur_id' => $formData['1_currency'],
                        'rcp_status'=>'APPROVE',
                        'MigrateCode'=>999
                    );
                    $receipt_id = $receiptDb->insert($receipt_data);

                    $payment_data = array(
                        'p_rcp_id' => $receipt_id,
                        'p_payment_mode_id' => isset($formData['mode_of_payment']) ? $formData['mode_of_payment']:0,
                        'p_cur_id' => $formData['1_currency'],
                        'p_amount' => $formData['1_amount'],
                        'p_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($formData['1_amount'], $formData['1_currency'])
                    );
                    $paymentDb->insert($payment_data);

                    $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Payment saved');
                    $this->_redirect($this->baseUrl . '/studentfinance/migrate/edit-migration');
                    break;
                case 3: //advance payment
                    $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
                    $advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

                    $bill_no =  $this->getBillSeq(8, date('Y', strtotime($formData['adv_payment_date'])));

                    $currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
                    $rate = $currencyRateDb->getCurrentExchangeRate($formData['adv_currency_id']);

                    $advance_payment_data = array(
                        'advpy_fomulir' => $bill_no,
                        'advpy_appl_id' => $registration['IdApplication'],
                        'advpy_trans_id' => $registration['transaction_id'],
                        'advpy_idStudentRegistration' => $registration['IdStudentRegistration'],
                        'advpy_description' => $formData['adv_description'],
                        'advpy_cur_id' => $formData['adv_currency_id'],
                        'advpy_amount' => $formData['adv_amount'],
                        'advpy_total_paid' => 0.00,
                        'advpy_exchange_rate' => $rate['cr_id'],
                        'advpy_total_balance' => $formData['adv_amount'],
                        'advpy_status' => 'A',
                        'advpy_date' => date('Y-m-d', strtotime($formData['adv_payment_date'])),
                        'ref_flag' => 1,
                        'MigrateCode'=>999
                    );
                    $advPayID = $advancePaymentDb->insert($advance_payment_data);

                    $advance_payment_det_data = array(
                        'advpydet_advpy_id' => $advPayID,
                        'advpydet_total_paid' => 0.00
                    );
                    $advancePaymentDetailDb->insert($advance_payment_det_data);

                    $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Advance payment saved');
                    $this->_redirect($this->baseUrl . '/studentfinance/migrate/edit-migration');
                    break;
                default:
                    $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'Wrong type');
                    $this->_redirect($this->baseUrl . '/studentfinance/migrate/add-migration');
                    break;
            }

            $this->_redirect($this->baseUrl . '/studentfinance/migrate/edit-migration');
        }
    }

    /*
	 * Get Bill no from mysql function
	*/
    private function getReceiptNoSeq(){

        $seq_data = array(
            5,
            date('Y'),
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }
}