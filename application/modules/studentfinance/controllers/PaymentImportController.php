<?php
/**
 *  @author alif 
 *  @date Jun 27, 2013
 */
 
class Studentfinance_PaymentImportController extends Base_Base {

	private $_DbObj;

	public function init(){
		$db = new Studentfinance_Model_DbTable_PaymentBank();
		$this->_DbObj = $db;
	}

	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Payment Import - Search Applicant / Student");
	}
	
	public function entryAction(){
		//title
		$this->view->title= $this->view->translate("Payment Import");
		
		$txn_id = $this->_getParam('id', null);
		$this->view->txn_id = $txn_id;
		
		//txn data
		$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
		$txn = $txnDb->getTransaction($txn_id);
		$this->view->txn = $txn;
		
		//profile
		$profileDb = new App_Model_Application_DbTable_ApplicantProfile();
		$profile = $profileDb->getData($txn['at_appl_id']);
		
		//address
		$cityDb = new App_Model_General_DbTable_City();
		$city = $cityDb->getData($profile['appl_city']);
			
		$stateDb = new App_Model_General_DbTable_State();
		$state = $stateDb->getData($profile['appl_state']);

		if($profile['appl_nationality']==1 && ($profile['appl_province']==0 || $profile['appl_province'] == null) ){
			$profile['appl_province']=96;
		}
		
		$countryDb = new App_Model_General_DbTable_Country();
		$country = $countryDb->getData($profile['appl_province']);
		
		$profile['address'] = $profile['appl_address1']!=null?$profile['appl_address1']."<br />":"";
		$profile['address'] .= $profile['appl_address2']!=null?$profile['appl_address2']."<br />":"";
			
		$profile['address'] .= $profile['appl_city']!=null?$city['CityName']."<br />":"";
		$profile['address'] .= $profile['appl_state']!=null?$state['StateName']."<br />":"";
		$profile['address'] .= $profile['appl_province']!=null&&$profile['appl_province']!=0?$country['CountryName']."<br />":"";
		$profile['address'] .= $profile['appl_postcode']!=null?$profile['appl_postcode']."<br />":"";
		
		$this->view->profile = $profile;
		
		
	
		//form
		$form = new Studentfinance_Form_PaymentImport();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$db = Zend_Db_Table::getDefaultAdapter();
				$db->beginTransaction();
					
				try {
					
					/*
					 * insert payment main
					 */
					$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
					$data = array(
							'billing_no'=>'000000000000000',
							'payer'=>$txn['at_pes_id'],
							'appl_id'=>$txn['at_appl_id'],
							'payment_description'=>$formData['payment_description'],
							'amount'=>$formData['amount'],
							'payment_mode'=>$formData['payment_mode'],
							'transaction_reference'=>null,
							'payment_date'=>date('Y-m-d H:i:s', strtotime($formData['payment_date']))
					);
					
					$payment_main_id = $paymentMainDb->insert($data);
					
					
					
					/*
					 * insert payment import table
					 */
					$paymentImportDb = new Studentfinance_Model_DbTable_PaymentImport();
					
					$data = array(
								'payer'=>$txn['at_pes_id'],
								'appl_id'=> $txn['at_appl_id'],
								'payment_date'=>date('Y-m-d H:i:s', strtotime($formData['payment_date'])),
								'amount'=>$formData['amount'],
								'description'=>$formData['payment_description'],
								'payment_mode'=>$formData['payment_mode'],
								'bank_reference'=>$formData['bank_reference'],
								'payment_main_id'=>$payment_main_id
							);
					
					$paymentImportDb->insert($data);
					
					
					
					//insert advance payment
					$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					
					$data = array(
								'advpy_appl_id'=>$txn['at_appl_id'],
								'advpy_acad_year_id'=>null,
								'advpy_sem_id'=>null,
								'advpy_prog_code'=>null,
								'advpy_fomulir'=>$txn['at_pes_id'],
								'advpy_invoice_no'=>null,
								'advpy_invoice_id'=>null,
								'advpy_payment_id'=>$payment_main_id,
								'advpy_description'=>'Manual Payment Import from old system',
								'advpy_amount'=>$formData['amount'],
								'advpy_total_paid'=>0.00,
								'advpy_total_balance'=>$formData['amount'],
								'advpy_status'=>'A'
							);
					
					$advancePaymentDb->insert($data);
					
					//$db->rollBack();
					$db->commit();
				
				
				}catch (Exception $e) {
					echo "Error in PaymentImportController. <br />";
					echo $e->getMessage();
				
					echo "<pre>";
					print_r($e->getTrace());
					echo "</pre>";
				
					$db->rollBack();
					 
					$status = false;
					exit;
					 
				}
				
				//reset form
				$form->reset();
				
				//success msg
				$this->view->noticeSuccess = "Payment has been saved. Please check Payment history";
				
			}else{
				$form->populate($formData);
			}
		}
		
		//payment history
		$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
		$paymentList = $paymentMainDb->getPaymentByApplicationId($txn['at_pes_id']);
		$this->view->payment = $paymentList;
		
		$this->view->form = $form;
	}
	
}
 ?>