<?php
class Studentfinance_InvoiceapprovalController extends Base_Base { //Controller for the User Module

	private $lobjInvoiceForm;
	private $lobjInvoiceModel;
	
	public function init() {
		$this->fnsetObj();
		
	}
	
	public function fnsetObj(){
		$this->lobjInvoiceapprovalModel = new Studentfinance_Model_DbTable_Invoiceapproval();
		$this->lobjInvoiceForm = new Studentfinance_Form_Invoice();		
	}
	
   public function indexAction() {
   
		$this->view->lobjform = $this->lobjform; 
		$larrresult = $this->lobjInvoiceapprovalModel->fngetStudentApplicationDetails();
  		$this->view->hiddencount = count($larrresult);
		$studentdropdown = $this->lobjInvoiceapprovalModel->fngetStudentDropdown();
		$this->view->lobjform->field5->addMultiOptions($studentdropdown);
		
		 if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->invoiceapprovalpaginatorresult);
   	    	
		$lintpagecount = 100;//$this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->invoiceapprovalpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->invoiceapprovalpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjInvoiceapprovalModel->fnSearchStudentApplication($this->lobjform->getValues()); //searching the values for the user
				/*print_r($larrresult);
				die();*/
				$this->view->ccount  = count($larrresult);
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->invoiceapprovalpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/invoiceapproval');
		}
      if ($this->_request->isPost () && $this->_request->getPost ('Approve')) {
      
		    $lobjFormData = $this->_request->getPost();
		    $result = $this->lobjInvoiceapprovalModel->updateinvoicemaster($lobjFormData);
			$this->_redirect( $this->baseUrl . '/studentfinance/invoiceapproval/index');
		}
		
	}

	public function newinvoicedetailsAction() { //Action for creating the new user
		$this->view->lobjInvoiceForm = $this->lobjInvoiceForm;
		$lintIdInvoice =$this->_getParam('IdInvoice');
		$this->view->IdInvoice  =  $lintIdInvoice;
		$larrinvoicestudentdetails = $this->lobjInvoiceapprovalModel->fnGetInvoiceStudentDetails($lintIdInvoice);
		//print_r($larrinvoicestudentdetails);
		
		$this->view->studentname = $larrinvoicestudentdetails['FName'].' '.$larrinvoicestudentdetails['MName'].' '.$larrinvoicestudentdetails['LName'];
		$this->view->studentid = $larrinvoicestudentdetails['StudentId'];
		$this->view->invno = $larrinvoicestudentdetails['InvoiceNo'];
		$this->view->invamt = $larrinvoicestudentdetails['InvoiceAmt'];
		$this->view->InvoiceDt =date ( "d-m-Y", strtotime ($larrinvoicestudentdetails['InvoiceDt'] ) );
		$this->view->Naration = $larrinvoicestudentdetails['Naration'];
		
		$larrresultofprogram = $this->lobjInvoiceapprovalModel->fnGetInvoiceMasterDetails($lintIdInvoice);
		$this->view->subjectcharges = $larrresultofprogram;
		
	 	if ($this->_request->isPost () && $this->_request->getPost ('Approve')) {      
		    $lobjFormData['IdInvoices'][0] = $lintIdInvoice;
		    $result = $this->lobjInvoiceapprovalModel->updateinvoicemaster($lobjFormData);
			$this->_redirect( $this->baseUrl . '/studentfinance/invoiceapproval/index');
		}
		
	}

}