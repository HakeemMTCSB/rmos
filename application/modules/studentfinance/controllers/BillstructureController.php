<?php
class Studentfinance_BillstructureController extends Base_Base { //Controller for the User Module	
	private $lobjBillstructure;
	private $lobjBillstructureForm;
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();
	}
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjBillstructure = new Studentfinance_Model_DbTable_Billstructure();
		$this->lobjBillstructureForm = new Studentfinance_Form_Billstructure(); 
	}
	public function indexAction() { // action for search and view
		$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		
		$larrresult = $this->lobjBillstructure->fngetBillStructureDetails (); //get user details
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->Billstructurepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Billstructurepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Billstructurepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjBillstructure-> fngetBillStructureDetailsSearch($this->_request->getPost ());  //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Billstructurepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/billstructure/index');
		}


	}

	public function newbillstructureAction() { //Action for creating the new user
		$this->view->lobjBillstructureForm = $this->lobjBillstructureForm; 
		$auth = Zend_Auth::getInstance();	
		
		//for combobox Id IntakeFrom
		$larrGroupList	=	$this->lobjBillstructure->fnGetIdGroupSelect();
		$this->view->lobjBillstructureForm->IdIntakeFrom->addMultiOptions($larrGroupList);
		
		//for combobox Id IntakeTo
		$larrBilling	=	$this->lobjBillstructure->fnGetBillingModuleSelect();
		$this->view->lobjBillstructureForm->IdIntakeTo->addMultiOptions($larrGroupList);
		
		//for combobox Category	
		$IdCategory[0]['key'] = 0;
		$IdCategory[0]['value'] = 'Local';
		$IdCategory[1]['key'] = 1;
		$IdCategory[1]['value'] = 'International';
		$this->view->lobjBillstructureForm->IdCategory->addMultiOptions($IdCategory);
		
		//for combobox Progarm
		$larrProgramList	=	$this->lobjBillstructure->fnGetProgramSelect();
		$this->view->lobjBillstructureForm->idProgram->addMultiOptions($larrProgramList);
		
		//for combobox Voucher Type
		$larrBilling	=	$this->lobjBillstructure->fnGetidVoucherTypeSelect();		
		$this->view->lobjBillstructureForm->idVoucherType->addMultiOptions($larrBilling);
		
		//for combobox Calculation Method
		$larrBilling	=	$this->lobjBillstructure->fnGetPeriodSelect();		
		$this->view->lobjBillstructureForm->idCalculationMethod->addMultiOptions($larrBilling);	
		
		//for combobox Debit Account
		$larrGroupList	=	$this->lobjBillstructure->fnGetIdAccountSelect(0);
		$this->view->lobjBillstructureForm->idDebitAccount->addMultiOptions($larrGroupList);
		
		//for combobox Creedit Account
		$larrGroupList	=	$this->lobjBillstructure->fnGetIdAccountSelect(1);
		$this->view->lobjBillstructureForm->idCreditAccount->addMultiOptions($larrGroupList);
    	
		$lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
		$initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);	
		
		/*if($initialConfig['AccountCodeType'] == 1 ){
			$this->view->lobjBillstructureForm->PrefixCode->setAttrib('readonly','true');
			$this->view->lobjBillstructureForm->PrefixCode->setValue('xxx-xxx-xxx');
		}else{
			$this->view->lobjBillstructureForm->PrefixCode->setAttrib('required','true');
			$this->view->lobjBillstructureForm->PrefixCode->setAttrib('maxlength','6');
		}*/
		//for Accountcode 
		// $larrAccountCode=  $this->lobjBillstructure->fnGetAccountcodeSelect();
		//print_r($larrAccountCode);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post			
			unset ( $larrformData ['Save'] );		
			if ($this->lobjBillstructureForm->isValid ( $larrformData )) {								
				$larrmstructreData['BillMasterCode'] = $larrformData['BillMasterCode'];
				$larrmstructreData['IdIntakeFrom'] = $larrformData['IdIntakeFrom'];
				$larrmstructreData['IdIntakeTo'] = $larrformData['IdIntakeTo'];
				$larrmstructreData['IdCategory'] = $larrformData['IdCategory'];
				$larrmstructreData['effectiveDate'] = $larrformData['effectiveDate'];
				$larrmstructreData['Description'] = $larrformData['Description'];
				$larrmstructreData['UpdUser'] = $auth->getIdentity()->iduser;				
				$larrmstructreData['UpdDate'] = date ( 'Y-m-d H:i:s' );
				$larrmstructreData['Active']  = 1;
				$result = $this->lobjBillstructure->fnInsertstructreData($larrmstructreData); //instance for adding the lobjuserForm values to DB
								
				$insertPrgmData['UpdUser'] = $auth->getIdentity()->iduser;				
				$insertPrgmData['UpdDate'] = date ( 'Y-m-d H:i:s' );
				$insertPrgmData['Active']  = 1;
       			$insertPrgmData['idBillStructureMaster'] = $result;
				$this->lobjBillstructure->fnInsertProgramData($larrformData['idProgramgrid'],$insertPrgmData); //instance for adding the lobjuserForm values to DB
								
				$larrmDetailsData['idVoucherType'] = $larrformData['idVoucherTypegrid'];
				$larrmDetailsData['itemDescription'] = $larrformData['itemDescriptiongrid'];
				$larrmDetailsData['idCalculationMethod'] = $larrformData['idCalculationMethodgrid'];
				$larrmDetailsData['Amount'] = $larrformData['Amountgrid'];
				$larrmDetailsData['idDebitAccount'] = $larrformData['idDebitAccountgrid'];
				$larrmDetailsData['idCreditAccount'] = $larrformData['idCreditAccountgrid'];
				$larrmDetailsDataInsert['idBillStructureMaster']  =  $result;
				$larrmDetailsDataInsert['UpdUser'] = $auth->getIdentity()->iduser;				
				$larrmDetailsDataInsert['UpdDate'] = date ( 'Y-m-d H:i:s' );
				$larrmDetailsDataInsert['Active']  = 1;				
				$this->lobjBillstructure->fnInsertDetailsData($larrmDetailsData,$larrmDetailsDataInsert); //instance for adding the lobjuserForm values to DB
				
				$this->_redirect( $this->baseUrl . '/studentfinance/billstructure/index');
			}
		}

	}

	public function billstructurelistAction() { //Action for the updation and view of the user details
		$this->view->lobjBillstructureForm = $this->lobjBillstructureForm; 
		
		$idBillStructureMaster = ( int ) $this->_getParam ( 'id' );
		$this->view->idBillStructureMaster = $idBillStructureMaster;
		
		
		//for combobox Id IntakeFrom
		$larrGroupList	=	$this->lobjBillstructure->fnGetIdGroupSelect();
		$this->view->lobjBillstructureForm->IdIntakeFrom->addMultiOptions($larrGroupList);
		
		//for combobox Id IntakeTo
		$larrBilling	=	$this->lobjBillstructure->fnGetBillingModuleSelect();
		$this->view->lobjBillstructureForm->IdIntakeTo->addMultiOptions($larrGroupList);
		
		//for combobox Category	
		$IdCategory[0]['key'] = 0;
		$IdCategory[0]['value'] = 'Local';
		$IdCategory[1]['key'] = 1;
		$IdCategory[1]['value'] = 'International';
		$this->view->lobjBillstructureForm->IdCategory->addMultiOptions($IdCategory);
		
		//for combobox Progarm
		$larrProgramList	=	$this->lobjBillstructure->fnGetProgramSelect();
		$this->view->lobjBillstructureForm->idProgram->addMultiOptions($larrProgramList);
		
		//for combobox Voucher Type
		$larrBilling	=	$this->lobjBillstructure->fnGetidVoucherTypeSelect();		
		$this->view->lobjBillstructureForm->idVoucherType->addMultiOptions($larrBilling);
		
		//for combobox Calculation Method
		$larrBilling	=	$this->lobjBillstructure->fnGetPeriodSelect();		
		$this->view->lobjBillstructureForm->idCalculationMethod->addMultiOptions($larrBilling);	
		
		//for combobox Debit Account
		$larrGroupList	=	$this->lobjBillstructure->fnGetIdAccountSelect(0);
		$this->view->lobjBillstructureForm->idDebitAccount->addMultiOptions($larrGroupList);
		
		//for combobox Creedit Account
		$larrGroupList	=	$this->lobjBillstructure->fnGetIdAccountSelect(1);
		$this->view->lobjBillstructureForm->idCreditAccount->addMultiOptions($larrGroupList);
    	
		$lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
		$initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);	
		
		
		//$this->view->lobjBillstructureForm->PrefixCode->setAttrib('readonly','true');
		
		$larrresult = $this->lobjBillstructure->fngetBillStructureDetails($idBillStructureMaster); //getting bill structuer based on billstructureid
		
		$this->lobjBillstructureForm->IdIntakeFrom->setValue($larrresult[0]['IdIntakeFrom']);
		$this->lobjBillstructureForm->IdIntakeTo->setValue($larrresult[0]['IdIntakeTo']);
		$this->lobjBillstructureForm->BillMasterCode->setValue($larrresult[0]['BillMasterCode']);
		$this->lobjBillstructureForm->IdCategory->setValue($larrresult[0]['IdCategory']);
		$this->lobjBillstructureForm->effectiveDate->setValue($larrresult[0]['effectiveDate']);
		$this->lobjBillstructureForm->Description->setValue($larrresult[0]['Description']);
		$this->lobjBillstructureForm->BillMasterCode->setAttrib('readonly','true');
		$auth = Zend_Auth::getInstance();
		$larrProgramDetails = $this->lobjBillstructure->fngetBillProgramDetails($idBillStructureMaster); //getting user details based on userid
		$this->view->larrProgramDetails = $larrProgramDetails;
		
		$larrBillDetails = $this->lobjBillstructure->fngetBillDetails($idBillStructureMaster); //getting user details based on userid
		$this->view->larrBillDetails = $larrBillDetails;

		
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjBillstructureForm->isValid ( $larrformData )) {					
					$larrmstructreData['BillMasterCode'] = $larrformData['BillMasterCode'];
					$larrmstructreData['IdIntakeFrom'] = $larrformData['IdIntakeFrom'];
					$larrmstructreData['IdIntakeTo'] = $larrformData['IdIntakeTo'];
					$larrmstructreData['IdCategory'] = $larrformData['IdCategory'];
					$larrmstructreData['effectiveDate'] = $larrformData['effectiveDate'];
					$larrmstructreData['Description'] = $larrformData['Description'];
					$larrmstructreData['UpdUser'] = $auth->getIdentity()->iduser;				
					$larrmstructreData['UpdDate'] = date ( 'Y-m-d H:i:s' );
					$larrmstructreData['Active']  = 1;
					$this->lobjBillstructure->fnUpdatestructreData($larrmstructreData,$idBillStructureMaster); //instance for adding the lobjuserForm values to DB
					$this->lobjBillstructure->fnUpdateUnsetall($idBillStructureMaster);					
					$this->lobjBillstructure->fnUpdateProgramData($larrformData['idProgramgrid'],$larrformData['idBillProgramgrid'],$idBillStructureMaster); //instance for adding the lobjuserForm values to DB
					
					$larrmDetailsData['idVoucherType'] = $larrformData['idVoucherTypegrid'];
					$larrmDetailsData['itemDescription'] = $larrformData['itemDescriptiongrid'];
					$larrmDetailsData['idCalculationMethod'] = $larrformData['idCalculationMethodgrid'];
					$larrmDetailsData['Amount'] = $larrformData['Amountgrid'];
					$larrmDetailsData['idDebitAccount'] = $larrformData['idDebitAccountgrid'];
					$larrmDetailsData['idCreditAccount'] = $larrformData['idCreditAccountgrid'];
					$larrmDetailsData['idBillStructureMaster'][0]  =  $idBillStructureMaster;
					$larrmDetailsData['UpdUser'][0] = $auth->getIdentity()->iduser;				
					$larrmDetailsData['UpdDate'][0] = date ( 'Y-m-d H:i:s' );
					$larrmDetailsData['Active'][0]  = 1;	
					$this->lobjBillstructure->fnUpdateStructureDetails($larrmDetailsData,$larrformData['ididBillStructureDetailsgrid']); //instance for adding the lobjuserForm values to DB
					$this->_redirect( $this->baseUrl . '/studentfinance/billstructure/index');
				}
			}
		}
		$this->view->lobjBillstructureForm = $this->lobjBillstructureForm;
	}

	
}