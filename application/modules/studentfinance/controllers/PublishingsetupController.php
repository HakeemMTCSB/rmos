<?php
class Studentfinance_PublishingsetupController extends Zend_Controller_Action
{
    private $_gobjlog;
    private $locale;
    private $registry;
    private $auth;
    private $scholarshipModel;
    private $intakeModel;
    private $defModel;
    private $scholarshipTagModel;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	    Zend_Form::setDefaultTranslator($this->view->translate);
	   $this->loadObj();
    }
    
    public function loadObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	    $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->gobjsessionsis = Zend_Registry::get('sis');
        
        $this->scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $this->intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->scholarshipTagModel = new Studentfinance_Model_DbTable_ScholarshipTagging();
        $this->SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();

		$this->SponsorModel = new Studentfinance_Model_DbTable_Sponsor();
        $this->SponsorApplicationForm = new Studentfinance_Form_SponsorApplication();

		$this->scstudModel = new Studentfinance_Model_DbTable_PublishingSetup();
		
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster();

		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

											
    }
    
    public function indexAction()
	{
        $this->view->title = $this->view->translate("Publishing Setup");

		//intake list
        $intakeList = $this->intakeModel->fngetIntakeList();
        $this->view->intakeList = $intakeList;

		//sem list
		$semList = $this->lobjsemestermaster->fngetSemestermainDetails();
		$this->view->semList = $semList;
    }

	public function studentAction()
	{
		$semid = $this->_getParam('semid',null);

		$semInfo = $this->lobjsemestermaster->fnGetSemestermaster($semid);

		if ( empty($semInfo) )
		{
			throw new Exception('Invalid Semester ID');
		}
		
		$this->view->title = $this->view->translate("Scholarship Setup - ".$semInfo['SemesterMainName']);
		$this->view->semId = $semInfo['IdSemesterMaster'];
	
		$results = $this->scstudModel->getAll(array('a.semester_id' => $semInfo['IdSemesterMaster']));
		
		$programs = array();
		foreach ( $results as $row )
		{
			$program = explode(',', $row['Programs']);
			foreach ( $program as $program_id )
			{
				$programs[] = $program_id;
			}
		}
		//var_dump($programs); exit;
		$programList = array();
		if ( !empty($programs) && $programs[0]!='')
		{
			$getprogramList = $this->scstudModel->getProgramsById(implode(',',$programs));
			foreach ( $getprogramList as $prog )
			{
				$programList[$prog['IdProgram']] = $prog['ProgramName'];
			}
		}
		
		$this->view->programs = $programList;
		$this->view->results = $results;
	}

	public function addAction()
	{
		$semid = $this->_getParam('semid',null);
		$semInfo = $this->lobjsemestermaster->fnGetSemestermaster($semid);
		$this->view->semInfo = $semInfo;

		if ( empty($semInfo) )
		{
			throw new Exception('Invalid Semester ID');
		}

		$this->view->title = 'Add Scholarship Setup - '.$semInfo['SemesterMainName'];

		$form = new Studentfinance_Form_StudentScholarshipTagging();
		
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) 
			{
				$auth = Zend_Auth :: getInstance();

				$data = array(
								'scholarship_type'	=> $formData['scholarship_type'],
                                                                'stype'	=> $formData['stype'],
								'application_type'	=> $formData['application_type'], 
								'semester_id'		=> $semid,			
								'start_date'		=> $formData['startDate'],
								'end_date'		=> $formData['endDate'],
                                                                'course_start_date'		=> $formData['course_start_date'],
								'course_end_date'		=> $formData['course_end_date'],
								'created_by'		=> $auth->getIdentity()->iduser,
								'created_date'		=> new Zend_Db_Expr('NOW()')
				);

			
				$setup_id = $this->scstudModel->addData($data);

				//add program
				for($i=0;$i<count($formData['programlist']);)
				{
					//save file into db
					$data = array(
									'setup_id'			=> $setup_id,
									'program_id'		=> $formData['programlist'][$i],
									'added_by'			=> $auth->getIdentity()->iduser,
									'added_date'			=> new Zend_Db_Expr('NOW()')
								);
						
					$this->scstudModel->addProgram($data);

					$i++;
				}

				$this->_helper->flashMessenger->addMessage(array('success' => "New scholarship setup added"));

				$this->_redirect( $this->baseUrl . '/studentfinance/publishingsetup/student/semid/'.$semid);
			}
		}

		//view	
		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id',null);
		$semid = $this->_getParam('semid',null);
		$semInfo = $this->lobjsemestermaster->fnGetSemestermaster($semid);
		$this->view->semInfo = $semInfo;

		if ( empty($semInfo) )
		{
			throw new Exception('Invalid Semester ID');
		}
		
		$info = $this->scstudModel->getData($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Setup ID');
		}

		//program
		$program = $this->scstudModel->getPrograms($info['id']);
		$programs = array();
		foreach ( $program as $prog )
		{
			if ( !in_array($prog['program_id'], $programs) )
			{
				$programs[] = $prog['program_id'];
			}
		}

		//title
		$this->view->title = 'Edit Scholarship Setup - '.$semInfo['SemesterMainName'];

		$form = new Studentfinance_Form_StudentScholarshipTagging(array('s_type'=>$info['stype']));
		
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) 
			{
				$auth = Zend_Auth :: getInstance();

				$data = array(
                                                                'stype'	=> $formData['stype'],
								'scholarship_type'	=> $formData['scholarship_type'], 
								'application_type'	=> $formData['application_type'], 
								'semester_id'		=> $semid,			
								'start_date'		=> $formData['startDate'],
								'end_date'			=> $formData['endDate'],
                                                                'course_start_date'		=> $formData['course_start_date'],
								'course_end_date'		=> $formData['course_end_date']
				);

				$setup_id = $id;
				$this->scstudModel->updateData($data, $id);

				//add program
				for($i=0;$i<count($formData['programlist']);)
				{
					//save file into db
					$data = array(
									'setup_id'			=> $setup_id,
									'program_id'		=> $formData['programlist'][$i],
									'added_by'			=> $auth->getIdentity()->iduser,
									'added_date'		=> new Zend_Db_Expr('NOW()')
								);
						
					$this->scstudModel->addProgram($data);

					$i++;
				}

				$this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

				$this->_redirect( $this->baseUrl . '/studentfinance/publishingsetup/edit/semid/'.$semid.'/id/'.$id);
			}
		}
		
		$info['startDate'] = $info['start_date'];
		$info['endDate'] = $info['end_date'];

		$form->populate($info);

		//view	
		$this->view->form = $form;
		$this->view->info = $info;
		$this->view->programs = $programs; //for id only
		$this->view->program = $program;
		$this->view->semId = $semid;

	}

	public function deleteProgramAction()
	{
		$db = getDB();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('tbl_scholarship_student_program', 'id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}
}
?>