<?php

class Studentfinance_ScholarshipAgeWeightController extends Zend_Controller_Action
{

    public function indexAction() {
        $AgeWeight = new Studentfinance_Model_DbTable_ScholarshipAgeWeight();
        $this->view->ageweights = $AgeWeight->fetchAll();
    }

    public function addAction() {
        $AgeWeight = new Studentfinance_Model_DbTable_ScholarshipAgeWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('ageweight');
            $AgeWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-age-weight/index');
        }
    }

    public function editAction() {
        $AgeWeight = new Studentfinance_Model_DbTable_ScholarshipAgeWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('ageweight');
            $AgeWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-age-weight/index');
        }

        $id =$this->_getParam('id');
        $ageweight = $AgeWeight->find($id);
        if($ageweight->count() > 0) {
            $this->view->ageweight = $ageweight->current();
        } else {
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-age-weight/index');
        }
    }

    public function deleteAction() {
        $AgeWeight = new Studentfinance_Model_DbTable_ScholarshipAgeWeight();
        $id =$this->_getParam('id');
        $ageweight = $AgeWeight->find($id);
        $ageweight->current()->delete();
        $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record deleted"));
        $this->redirect( $this->baseUrl . '/studentfinance/scholarship-age-weight/index');
    }

}