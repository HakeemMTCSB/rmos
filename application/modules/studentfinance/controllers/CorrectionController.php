<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 5/10/2016
 * Time: 9:44 AM
 */
class Studentfinance_CorrectionController extends Zend_Controller_Action
{

    private $_gobjlog;
    private $locale;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function updateInvoiceSemesterStudentAction(){
        $model = new Studentfinance_Model_DbTable_Correction();

        $list = $model->getInvoiceWrongSemesterStudent();
        var_dump($list);
        //exit;

        if ($list){
            foreach ($list as $loop){
                $data = array(
                    'semester'=>$loop['SemesterProgram']
                );
                $model->updateInvoice($data, $loop['id']);
            }
        }

        exit;
    }

    public function updateInvoiceSemesterApplicantAction(){
        $model = new Studentfinance_Model_DbTable_Correction();

        $list = $model->getInvoiceWrongSemesterApplicant();
        var_dump($list);
        //exit;

        if ($list){
            foreach ($list as $loop){
                $data = array(
                    'semester'=>$loop['SemesterProgram']
                );
                $model->updateInvoice($data, $loop['id']);
            }
        }

        exit;
    }

    public function ebipCorrectionAction(){
        $model = new Studentfinance_Model_DbTable_Correction();
        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

        $list = $model->getDiscount();

        if ($list){
            foreach ($list as $key => $loop){
                if ($loop['currency_id']!=1){
                    $exrate = $curRateDB->getRateByDate($loop['currency_id'], $loop['invoice_date']);
                    $invoice_amt = number_format((($loop['amount'] - ($loop['amount']*0.05))*$exrate['cr_exchange_rate']), 2, '.', '');
                }else{
                    $invoice_amt = number_format(($loop['amount'] - ($loop['amount']*0.05)), 2, '.', '');
                }

                $data = array(
                    'rcp_inv_amount'=>$invoice_amt
                );

                $model->updateReceiptInvoice($data, $loop['rcp_inv_id']);

                $list[$key]['amount_convert'] = $invoice_amt;
            }
        }

        var_dump($list);
        exit;
    }

    public function ib2002Action(){
        $model = new Studentfinance_Model_DbTable_Correction();
        $list = $model->getStudentIB2002();

        if ($list){
            foreach ($list as $loop){
                $model->deleteStudentRegSubject($loop['IdStudentRegSubjects']);
            }
        }
        exit;
    }

    public function discountAdjustmentCorrectionAction(){
        $model = new Studentfinance_Model_DbTable_Correction();
        $discountDb = new Studentfinance_Model_DbTable_DiscountNoteDetail();
        $list = $model->getCanceledEbipDiscount();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        if ($list){
            foreach ($list as $loop) {
                //insert discount_adjustment
                $discAdjustDB = new Studentfinance_Model_DbTable_DiscountAdjustment();

                $adjId = $this->getCurrentAdjNoSeq();

                $upd_data_inv = array(
                    'AdjustmentCode'=> $adjId,
                    'AdjustmentType'=> 1,
                    'IdDiscount'=>$loop['id'],
                    'Remarks' => 'Discount Cancellation',
                    'cur_id'=>$loop['dcnt_currency_id'],
                    'amount'=>$loop['discount_amount'],
                    'Status'=>'APPROVE',
                    'EnterBy'=> $getUserIdentity->id,
                    'EnterDate'=> date('Y-m-d H:i:s', strtotime($loop['dcnt_approve_date'])),
                    'UpdUser'=> $getUserIdentity->id,
                    'UpdDate'=> date('Y-m-d H:i:s', strtotime($loop['dcnt_approve_date']))
                );
                $idAdjust =  $discAdjustDB->insert($upd_data_inv);

                $upd_data = array(
                    'adjustId'=> $idAdjust,
                );
                $discountDb->update($upd_data, array('id = ?' => $loop['id']));
            }
        }
        exit;
    }

    private function getCurrentAdjNoSeq(){

        $seq = 1;
        $r_num = "";

        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('s'=>'sequence'))
            ->where("s.type = 14")
            ->where("s.year = ?", date('Y'));

        $row = $db->fetchRow($selectData);

        if($row){
            $seq = $row['seq']+1;
            $db->update('sequence',array('seq'=>$seq),'year = '.date('Y').' and type = 14');
        }else{
            $db->insert('sequence',array('year'=>date('Y'),'type'=>14,'seq'=>1));
        }

        //read initial config
        $selectData2 = $db->select()
            ->from(array('i'=>'tbl_config'))
            ->where("i.idUniversity = 1")
            ->order("i.UpdDate ASC");

        $row2 = $db->fetchRow($selectData2);

        if($row2){
            $r_num = 'DADJ'.date('Y').$row2['ReceiptSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
        }

        return $r_num;
    }

    public function updateGradePointAction(){
        $model = new Studentfinance_Model_DbTable_Correction();

        $list = $model->getStudentToUpdateGradePoint();

        dd($list);

        if ($list){
            foreach ($list as $loop){
                $subjects = $model->getSubjectToUpdateGradePoint($loop['IdStudentRegistration']);

                if ($subjects){
                    foreach ($subjects as $subject){
                        if ($subject['final_course_mark']!='') {
                            $this->saveStudentSubjectMark($subject['IdStudentRegSubjects'], $subject['IdSemesterMain'], $loop['IdProgram'], $subject['IdSubject'], $subject['final_course_mark']);
                        }
                    }
                }
            }
        }
        exit;
    }

    public function saveStudentSubjectMark($IdStudentRegSubjects,$semester_id,$program_id,$subject_id,$grand_total_mark=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $message = '';

        //mappkan markah dgn grade
        if($grand_total_mark!=""){

            try{
                $cms_calculation = new Cms_ExamCalculation();
                $grade	= $cms_calculation->getGradeInfo($semester_id,$program_id,$subject_id,$grand_total_mark);

                if(isset($grade["Pass"])){
                    if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass
                    else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
                    else $grade_status='';
                }else{
                    $grade_status='';
                }

                //$data["final_course_mark"]=$grand_total_mark;
                $data["grade_point"]=$grade["GradePoint"];
                $data["grade_name"]=$grade["GradeName"];
                $data["grade_desc"]=$grade["GradeDesc"];
                $data["grade_status"]=$grade_status;
            }catch (Exception $e) {
                //$data["final_course_mark"]=NULL;
                $data["grade_point"]=NULL;
                $data["grade_name"]="";
                $data["grade_desc"]="";
                $data["grade_status"]="";
                $message = $e->getMessage();
            }
        }else{
            //$data["final_course_mark"]=NULL;
            $data["grade_point"]=NULL;
            $data["grade_name"]="";
            $data["grade_desc"]="";
            $data["grade_status"]="";
        }

        //save dalam studentregsubject
        $subRegDB = new Examination_Model_DbTable_StudentRegistrationSubject();
        $subRegDB->updateData($data,$IdStudentRegSubjects);

        $data['message'] = $message;
        return $data;
    }

    public function checkwronginvoicepaperAction(){
        $model = new Studentfinance_Model_DbTable_Correction();
        $list = $model->getItemRegisterOnlineStudent();

        if ($list){
            foreach ($list as $loop){
                $itemList = $model->getItemRegisterList($loop['IdStudentRegistration'], $loop['IdSubject']);

                dd($itemList);
            }
        }

        //dd($list);

        exit;
    }

    public function getSponsorwithGainlossAction(){
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $model = new Studentfinance_Model_DbTable_Correction();
        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
        $db = Zend_Db_Table::getDefaultAdapter();

        $list = $model->getSponsorshipList();

        if ($list){
            foreach ($list as $key => $loop){
                $selectInvoiceSpm = $db->select()
                    ->from(array('a'=>'sponsor_invoice_receipt_invoice'), array('a.rcp_inv_amount'))
                    ->join(array('b'=>'sponsor_invoice_detail'), 'a.rcp_inv_sp_id = b.sp_id', array('b.sp_amount', 'b.sp_paid', 'b.sp_receipt'))
                    ->joinLeft(array('c'=>'invoice_main'), 'b.sp_invoice_id = c.id', array('c.invoice_date'))
                    ->join(array('d'=>'sponsor_invoice_receipt'), 'a.rcp_inv_rcp_id = d.rcp_id', array())
                    ->where('a.rcp_inv_student = ?', $loop['id'])
                    ->group('b.id');

                $listInvoiceSpm = $db->fetchAll($selectInvoiceSpm);

                $amountCredit = 0.00;

                if ($listInvoiceSpm){
                    foreach ($listInvoiceSpm as $invoiceSpm){
                        if ($loop['cur_id'] == 2) {
                            $dataCur = $curRateDB->getRateByDate(2, $invoiceSpm['invoice_date']);

                            $amountCredit = round($amountCredit + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                        } else {
                            $amountCredit = $amountCredit + $invoiceSpm['sp_amount'];
                        }
                    }
                }

                $dataCur2 = $curRateDB->getRateByDate(2, $loop['record_date']);

                if ($loop['cur_id'] == 2) {
                    $amountCredit2 = round($loop['credit'] * $dataCur2['cr_exchange_rate'], 2);
                } else {
                    $amountCredit2 = $loop['credit'];
                }

                $sponsorCurAdj = ($amountCredit - $amountCredit2);

                $list[$key]['credit_myr'] = $amountCredit2;
                $list[$key]['credit_pymnt'] = $amountCredit;

                if ($sponsorCurAdj != 0 && $loop['cur_id'] == 2){
                    if ($sponsorCurAdj > 0){
                        $list[$key]['gain'] = 0.00;
                        $list[$key]['loss'] = str_replace('-', '', $sponsorCurAdj);
                    }else{
                        $list[$key]['gain'] = str_replace('-', '', $sponsorCurAdj);
                        $list[$key]['loss'] = 0.00;
                    }
                }else{
                    $list[$key]['gain'] = 0.00;
                    $list[$key]['loss'] = 0.00;
                }
            }
        }

        $this->view->list = $list;
        $this->view->filename = date('Ymd').'_sponsor_payment_gain_loss.xls';
    }

    public function documentMissingAction(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_statusattachment_sth'), array('sth_Id'));

        $select1 = $db->select()
            ->from(array('a' => 'applicant_generated_document'))
            ->where('a.sth_id NOT IN ('.$select.')')
            ->group('a.sth_id');

        $list = $db->fetchAll($select1);

        if ($list){
            foreach ($list as $loop){

                $type = 0;
                if (strpos($loop['tgd_filename'], 'Offer') !== false){
                    $type = 606;
                }else if (strpos($loop['tgd_filename'], 'Proforma') !== false){
                    $type = 605;
                }else if (strpos($loop['tgd_filename'], 'Appendix') !== false){
                    $type = 851;
                }else if (strpos($loop['tgd_filename'], 'NOC') !== false){
                    $type = 850;
                }else if (strpos($loop['tgd_filename'], 'Credit') !== false){
                    $type = 861;
                }else if (strpos($loop['tgd_filename'], 'SEV') !== false){
                    $type = 859;
                }else if (strpos($loop['tgd_filename'], 'medical') !== false){
                    $type = 860;
                }else if (strpos($loop['tgd_filename'], 'Conditional') !== false){
                    $type = 716;
                }

                $data = [
                    'sth_Id' => $loop['sth_id'],
                    'sth_stpId' => 0,
                    'sth_tplId' => 0,
                    'sth_type' => $type
                ];

                $db->insert('tbl_statusattachment_sth', $data);
            }
        }

        exit;
    }
}