<?php
class Studentfinance_ApplicationformsetupController extends Zend_Controller_Action
{
    private $_gobjlog;
    private $locale;
    private $registry;
    private $auth;
    private $scholarshipModel;
    private $intakeModel;
    private $defModel;
    private $scholarshipTagModel;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	    Zend_Form::setDefaultTranslator($this->view->translate);
	   $this->loadObj();
    }
    
    public function loadObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	    $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->gobjsessionsis = Zend_Registry::get('sis');
        
        $this->scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $this->intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->scholarshipTagModel = new Studentfinance_Model_DbTable_ScholarshipTagging();
        $this->SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();

		$this->SponsorModel = new Studentfinance_Model_DbTable_Sponsor();
        $this->SponsorApplicationForm = new Studentfinance_Form_SponsorApplication();

		$this->scstudModel = new Studentfinance_Model_DbTable_PublishingSetup();
		
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster();

		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

											
    }
    
    /*public function newAction()
	{
        $this->view->title = $this->view->translate("Application Form Setup - New Application");

		//status
		$status = $this->defModel->getByCode('Scholarship Application Status');
		$this->view->status = $status;
			

		$info_student = $this->SponsorModel->getAppSetup( array(
														'application_type'	=> 0,
														'type'				=> 'student'
												));
	
		$info_applicant = $this->SponsorModel->getAppSetup( array(
														'application_type'	=> 0,
														'type'				=> 'applicant'
												));

		$this->view->info_applicant = $info_applicant;
		$this->view->info_student = $info_student;

		//post
		if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'save' )) 
		{
            $formData = $this->getRequest()->getPost();
			$auth = Zend_Auth::getInstance();	
			$check = $this->SponsorModel->getAppSetup( array(
																'application_type'	=> $formData['application_type'],
																'type'				=> $formData['type']
														));
			
		

			if ( empty($check) ) 
			{
				$data = array(
							'application_type'	=> $formData['application_type'],
							'type'				=> $formData['type'],
							'description'		=> $formData['description'],
							'created_by'		=> $auth->getIdentity()->iduser,
							'created_date'		=> new Zend_Db_Expr('NOW()')
						);
				
				$this->SponsorModel->insertAppSetup($data);
			}
			else
			{
				$data = array(
								'description'	=> $formData['description'],
								'updated_by'	=> $auth->getIdentity()->iduser,
								'updated_date'	=> new Zend_Db_Expr('NOW()')
						);
				
				$db = getDB();
				$where = array( 
									$db->quoteInto('application_type = ?', $formData['application_type']),
									$db->quoteInto('type = ?', $formData['type'])
								);

				$this->SponsorModel->updateAppSetup($data, $where);
			}

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/studentfinance/applicationformsetup/new/#tab-'.($formData['type']=='student'? '2':'1'));
		 }
    }*/
    
    public function newAction(){
        $this->view->title = $this->view->translate("Application Form Setup - New Application");
        
        //status
        $status = $this->defModel->getByCode('Scholarship Application Status');
        $this->view->status = $status;

        //programme
        $get_program_list = new Application_Model_DbTable_ProgramScheme();
        $program_list = $get_program_list->fnGetProgramList();

        $this->view->program_list = $program_list;
    }
    
    public function applicationListAction(){
        $this->view->title = $this->view->translate("Application Form Setup - New Application");

        $pid = $this->_getParam('pid');
        $type = $this->_getParam('type');

        //status
        $status = $this->defModel->getByCode('Scholarship Application Status');
        $this->view->status = $status;

        //programme
        $progDB = new Application_Model_DbTable_ProgramScheme();
        $program = $progDB->fnGetProgramBasedOnProgId($pid);

        //get
        $results = $this->SponsorModel->getAppSetup( array(
            'application_type'=> 0,
            'type' => $type,
            'program_id'=> $pid
        ),1);

        //view
        $this->view->program = $program;
        $this->view->pid = $pid;
        $this->view->type = $type;
        $this->view->results =  $results;
    }
    
    public function applicationAddAction(){
        $this->view->title = $this->view->translate("Application Form Setup - New Application");

        $pid = $this->_getParam('pid');
        $type = $this->_getParam('type');

        //status
        $status = $this->defModel->getByCode('Scholarship Application Status');
        $this->view->status = $status;

        //programme
        $progDB = new Application_Model_DbTable_ProgramScheme();
        $program = $progDB->fnGetProgramBasedOnProgId($pid);

        //form
        $form = new Studentfinance_Form_ReapplicationAdd(array('programid'=>$pid));

        //post
        if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'save' )){
            $formData = $this->getRequest()->getPost();
            $auth = Zend_Auth::getInstance();	

            $data = array(
                'application_type' => $formData['application_type'],
                'type' => $formData['type'],
                'stype' => $formData['stype'],
                'program_id' => $formData['program_id'],
                'program_scheme_id' => $formData['programscheme'],
                'student_category' => $formData['studentcategory'],
                'sch_type' => $formData['ScholarshipType'],
                'description' => '',
                'created_by' => $auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()')
            );

            $this->SponsorModel->insertAppSetup($data);

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect( $this->baseUrl . '/studentfinance/applicationformsetup/application-list/pid/'.$formData['program_id'].'/type/'.$formData['type']);
        }

        //view
        $this->view->form = $form;
        $this->view->program = $program;
        $this->view->pid = $pid;
        $this->view->type = $type;
    }
    
    public function applicationDetailAction(){
        $this->view->title = $this->view->translate("Application Form Setup - New Application Detail");

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');

        $info = $this->SponsorModel->getAppSetup( array(
            'a.id' => $id
        ));

        if (empty($info)){
            throw new Exception('Invalid Detail Id');
        }

        //status
        if ($type=='applicant'){
            $status = $this->defModel->getByCode('Scholarship Applicant Status');
        }else{
            $status = $this->defModel->getByCode('Scholarship Application Status');
        }
        
        $this->view->status = $status;
        $this->view->type = $type;

        $this->view->info = $info;

        //post
        if ($this->getRequest()->isPost()  && $this->_request->getPost('save')){
            $formData = $this->getRequest()->getPost();
            $auth = Zend_Auth::getInstance();	

            $data = array(
                'description'	=> $formData['description'],
                'updated_by'	=> $auth->getIdentity()->iduser,
                'updated_date'	=> new Zend_Db_Expr('NOW()')
            );

            $db = getDB();
            $where = array( 
                $db->quoteInto('id = ?', $id)
            );

            $this->SponsorModel->updateAppSetup($data, $where);

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect( $this->baseUrl . '/studentfinance/applicationformsetup/application-detail/id/'.$id.'/type/'.$type );
        }
    }
	
	public function reapplicationDetailAction()
	{
        $this->view->title = $this->view->translate("Application Form Setup - Re-Application Detail");
		
		$id = $this->_getParam('id');
		
		$info = $this->SponsorModel->getAppSetup( array(
															'a.id'	=> $id
														));

		if ( empty($info) )
		{
			throw new Exception('Invalid Detail Id');
		}

		//status
		$status = $this->defModel->getByCode('Scholarship Re-application Status');
		$this->view->status = $status;
			


		$this->view->info = $info;

		//post
		if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'save' )) 
		{
			$formData = $this->getRequest()->getPost();
			$auth = Zend_Auth::getInstance();	

           $data = array(
								'description'	=> $formData['description'],
								'updated_by'	=> $auth->getIdentity()->iduser,
								'updated_date'	=> new Zend_Db_Expr('NOW()')
						);
				
			$db = getDB();
			$where = array( 
								$db->quoteInto('id = ?', $id)
							);

			$this->SponsorModel->updateAppSetup($data, $where);
		
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/studentfinance/applicationformsetup/reapplication-detail/id/'.$id );
		 }
    }

	public function reapplicationAction()
	{
        $this->view->title = $this->view->translate("Application Form Setup - Re-Application");

		//status
		$status = $this->defModel->getByCode('Scholarship Application Status');
		$this->view->status = $status;
			
		//programme
		$get_program_list = new Application_Model_DbTable_ProgramScheme();
		$program_list = $get_program_list->fnGetProgramList();
		
		$this->view->program_list = $program_list;
		
    }
	
	public function reapplicationListAction()
	{
        $this->view->title = $this->view->translate("Application Form Setup - Re-Application");

		$pid = $this->_getParam('pid');
		$type = $this->_getParam('type');

		//status
		$status = $this->defModel->getByCode('Scholarship Application Status');
		$this->view->status = $status;
			
		//programme
		$progDB = new Application_Model_DbTable_ProgramScheme();
		$program = $progDB->fnGetProgramBasedOnProgId($pid);

		//get
		$results = $this->SponsorModel->getAppSetup( array(
														'application_type'=> 1,
														'type' => $type,
														'program_id'=> $pid
												),1);
                                                                                        //var_dump($results);
		//view
		$this->view->program = $program;
		$this->view->pid = $pid;
		$this->view->type = $type;
		$this->view->results =  $results;
    }

	public function reapplicationAddAction()
	{
        $this->view->title = $this->view->translate("Application Form Setup - Re-Application");

		$pid = $this->_getParam('pid');
		$type = $this->_getParam('type');

		//status
		$status = $this->defModel->getByCode('Scholarship Application Status');
		$this->view->status = $status;
			
		//programme
		$progDB = new Application_Model_DbTable_ProgramScheme();
		$program = $progDB->fnGetProgramBasedOnProgId($pid);

		//form
                $form = new Studentfinance_Form_ReapplicationAdd();
		//$form = new Studentfinance_Form_Sponsorsetup();



		//post
		if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'save' )) 
		{
            $formData = $this->getRequest()->getPost();
			$auth = Zend_Auth::getInstance();	

			$data = array(
							'application_type'	=> $formData['application_type'],
							'type'				=> $formData['type'],
                                                        'stype'=> $formData['stype'],
							'program_id'		=> $formData['program_id'],
							'sch_type'			=> $formData['ScholarshipType'],
							'description'		=> '',
							'created_by'		=> $auth->getIdentity()->iduser,
							'created_date'		=> new Zend_Db_Expr('NOW()')
						);

			$this->SponsorModel->insertAppSetup($data);

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/studentfinance/applicationformsetup/reapplication-list/pid/'.$formData['program_id'].'/type/'.$formData['type']);
		}

		//view
		$this->view->form = $form;
		$this->view->program = $program;
		$this->view->pid = $pid;
		$this->view->type = $type;
    }


	public function statusTemplateAction()
	{
		$id = $this->_getParam('id');
		$apptype = $this->_getParam('apptype');
		$type  = $this->_getParam('type');
		$rid = $this->_getParam('rid',null);
		
		$info = $this->SponsorModel->getAppSetup(array('a.id' => $rid));

		$pid = $info['program_id'];
		
		//programme
                if ($apptype != 0){
                    $progDB = new Application_Model_DbTable_ProgramScheme();
                    $program = $progDB->fnGetProgramBasedOnProgId($pid);
                }

		
		$status = $this->defModel->getData($id);

		if ( empty($status) )
		{
			throw new Exception('Invalid Status ID');
		}

		
		
		$apptypename = $apptype == 0 ? 'New' : 'ReApplication';
		$typename = $type == 'student' ? 'Student' : 'Applicant';
                if ($apptype != 0){
                    $progname = $program['ProgramCode'];
                    $schname = $info['sch_name'];
                }


                if ($apptype != 0){
                    $template_name = 'Status ('.strtoupper($status['DefinitionCode']).'/'.$apptypename.'/'.$typename.'/'.$progname.'/'.$schname.')';
                    $status_tag = 'status-'.strtolower($status['DefinitionCode']).'-reapplication-'.$rid;
                }else{
                    $template_name = 'Status ('.strtoupper($status['DefinitionCode']).'/'.$apptypename.'/'.$typename.')';
                    $status_tag = 'status-'.strtolower($status['DefinitionCode']).'-application-'.$rid;
                }

		$commtplModel = new Communication_Model_DbTable_Template();

		$template = $commtplModel->getTemplatesByCategory('scholarship',$status_tag, 1);
		//$template = array();

		if ( empty($template) )
		{
			//create the template
			$auth = Zend_Auth::getInstance();

			$commGeneralModel = new Communication_Model_DbTable_General();
			
			$tpl_data = array(
                            'tpl_module'		=> 'scholarship',
                            'tpl_name'			=> $template_name,
                            'tpl_type'			=> $commGeneralModel->communicationTypeId('Email'),
                            'tpl_pname'			=> clean_string($template_name),
                            'created_by'		=> $auth->getIdentity()->iduser,
                            'created_date'		=> new Zend_Db_Expr('NOW()'),
                            'email_from_name'	=> '',
                            'email_from'		=> '',
							'tpl_categories'	=> $status_tag
                        );	
			
		$template_id = $commtplModel->addTemplate($tpl_data);

			foreach ( $this->locales as $locale_code => $locale ){

				$content = '';

				$cdata = array(
					'tpl_id' =>	$template_id,
					'locale' =>	$locale_code,
					'tpl_content' => ''
				);

				$commtplModel->addTemplateContent($cdata);
			}
		}
		else
		{
			$template_id = $template['tpl_id'];
		}
		
		$from_what = $apptype == 0 ? 'new' : 'reapplication';
		$more = $from_what == 'new' ? '-'.$rid : '-'.$rid;

		$this->_redirect( $this->baseUrl . '/communication/scholarship/template-detail/id/'.$template_id.'/from/applicationformsetup-'.$from_what.$more );
	}
        
    public function getScholarshipAction(){
        $id = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        
        switch ($id){
            case 1: //scholarship
                $list = $scholarshipModel->getScholarship();
                break;
            case 2: //sponsorship
                $list = $scholarshipModel->getSponsorship();
                break;
            case 3: //financial aid
                $list = $scholarshipModel->getFinancialAid();
                break;
        }
        
        $json = Zend_Json::encode($list);
		
	echo $json;
	exit();
    }

	public function loadAttachmentAction(){
		$id = $this->_getParam('id',null);
		$statusid = $this->_getParam('statusid',null);

		$this->_helper->layout->disableLayout();

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();

		$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();

		$listSelect = $this->SponsorModel->getAttachmentList($id, $statusid);

		$sellAttArr = array(0);
		if ($listSelect){
			foreach ($listSelect as $listSelectLoop){
				array_push($sellAttArr, $listSelectLoop['att_tpl_id']);
			}
		}

		$list = $this->SponsorModel->getAttachmentListAll();

		if ($list){
			foreach ($list as $key => $loop){
				if (in_array($loop['tpl_id'], $sellAttArr)){
					$list[$key]['check']=1;
				}else{
					$list[$key]['check']=0;
				}
			}
		}

		$json = Zend_Json::encode($list);

		echo $json;
		exit();
	}

	public function saveAttachmentAction(){
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$getUserIdentity = $this->auth->getIdentity();

			$this->SponsorModel->deleteAttachment($formData['form_id'], $formData['form_status_id']);

			if (isset($formData['check']) && count($formData['check']) > 0) {
				foreach ($formData['check'] as $att_id) {
					$data = array(
						'att_status_id'=>$formData['form_status_id'],
						'att_appform_id'=>$formData['form_id'],
						'att_tpl_id'=>$att_id,
						'att_created_by'=>$getUserIdentity->id,
						'att_created_date'=>date('Y-m-d H:i:s')
					);
					$this->SponsorModel->insertAttachment($data);
				}
			}

			$this->_redirect( $this->baseUrl . '/studentfinance/applicationformsetup/application-detail/id/'.$formData['form_id'].'/type/'.$formData['form_type'] );
		}
		exit;
	}

	public function deleteAction(){
		$id = $this->_getParam('id',0);
		$type = $this->_getParam('type','applicant');

		$this->SponsorModel->deleteFormSetup($id);

		$this->_redirect($this->baseUrl . '/studentfinance/applicationformsetup/application-list/pid/'.$id.'/type/'.$type);
	}
}
?>