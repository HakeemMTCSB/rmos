<?php
class Studentfinance_PlantypeController extends Base_Base { //Controller for the User Module
	private $lobjinitialconfig;
	private $lobjPlantypeForm;
	private $lobjAddPlantype;	
	private $_gobjlog;
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();	 
	}
	
	public function fnsetObj() {
	   $this->lobjPlantypeForm = new Studentfinance_Form_Plantype();
	   $this->lobjAddPlantype = new Studentfinance_Model_DbTable_Plantype();
	}

   public function indexAction() {
   	$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
    $lintpagecount = $this->gintPageCount;
	$lobjPaginator = new App_Model_Common(); // Definitiontype model
    $lintpage = $this->_getParam('page',1); // Paginator instance
    $larrresult = $this->lobjAddPlantype->fnGetSemesterStudentStatusList();
    
	if(!$this->_getParam('Search')) 
			unset($this->gobjsessionsis->plantypepaginatorresult);
			
	if(isset($this->gobjsessionsis->plantypepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->plantypepaginatorresult,$lintpage,$lintpagecount);
	} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
	}	
	
   if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjAddPlantype->fnSearchPlanType($larrformData ); 

				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->plantypepaginatorresult = $larrresult;
			}
		}
		
   		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/plantype/index');
		}
	}
	
	public function newplantypeAction() {
		$lobjPlantypeForm = new Studentfinance_Form_Plantype();
		$auth = Zend_Auth::getInstance();
	    $this->view->lobjPlantypeForm= $lobjPlantypeForm;
	    $ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjPlantypeForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjPlantypeForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
	    
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
		 if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			    unset ( $larrformData ['Save'] );
				$this->lobjAddPlantype->fnAddPlanType($larrformData);
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Plan type Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/plantype');

			}
				
		}
		
	}
	
	public function editplantypeAction() {
		$IdPlantype = ( int ) $this->_getParam ( 'id' );
		$auth = Zend_Auth::getInstance();
		$larrresult = $this->lobjAddPlantype->fnEditPlanType($IdPlantype);
		$this->view->lobjPlantypeForm = new Studentfinance_Form_Plantype();
	   	$this->view->lobjPlantypeForm->populate($larrresult);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
		 if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			    unset ( $larrformData ['Save'] );
				$this->lobjAddPlantype->fnupdatePlanType($IdPlantype, $larrformData );
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Plan Type Edit Id=' . $IdPlantype,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/plantype');

			}
				
		}
		
	}
	
	

	
}