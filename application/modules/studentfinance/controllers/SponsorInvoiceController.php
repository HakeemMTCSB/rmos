<?php

/**
 * @author Suliana
 * @version 1.0
 */
class Studentfinance_SponsorInvoiceController extends Base_Base
{

    private $_DbObj;

    public function init()
    {
        $this->fnsetObj();
    }

    public function fnsetObj()
    {
        $this->lobjdeftype = new App_Model_Definitiontype();
    }

    public function batchSponsorInvoiceAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        //title
        $this->view->title = $this->view->translate("Sponsor Invoice by Batch");

        $ses_batch_invoice = new Zend_Session_Namespace('studentfinance_batch_sponsorinvoice');
        $ses_batch_invoice->setExpirationSeconds(900);

        $step = $this->_getParam('step', 1);
        $this->view->step = $step;

        if ($step != 5 && $step != 6) {
            unset($ses_batch_invoice->student_list);
        }

        if ($step == 1) { //STEP 1

            //Zend_Session::namespaceUnset('studentfinance_batch_invoice');

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                if ($formData['semester_id'] != null) {

                    $ses_batch_invoice->semester_id = $formData['semester_id'];

                    //redirect
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                }

            }

            $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
            $this->view->semester_list = $semesterDb->fnGetSemestermasterList();

            if (isset($ses_batch_invoice->semester_id)) {
                $this->view->semester_id = $ses_batch_invoice->semester_id;
            }


        } else
            if ($step == 2) { //STEP 2

                //step validation
                if (!isset($ses_batch_invoice->semester_id)) {
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 1), 'default', true));
                }

                if ($this->getRequest()->isPost()) {

                    $formData = $this->getRequest()->getPost();

                    if ($formData['sponsortype'] != null && $formData['typeinfo'] != null) {

                        $ses_batch_invoice->sponsortype = $formData['sponsortype'];
                        $ses_batch_invoice->typeinfo = $formData['typeinfo'];
                        $ses_batch_invoice->invoice_date = $formData['invoice_date'];

                        //redirect
                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 3), 'default', true));
                    }

                }

                //semester
                $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
                $this->view->semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);

                if (isset($ses_batch_invoice->sponsortype)) {
                    $this->view->sponsortype = $ses_batch_invoice->sponsortype;
                }

                if (isset($ses_batch_invoice->typeinfo)) {
                    $this->view->typeinfo = $ses_batch_invoice->typeinfo;
                }

                if (isset($ses_batch_invoice->invoice_date)) {
                    $this->view->invoice_date = $ses_batch_invoice->invoice_date;
                }


            } else
                if ($step == 3) { //STEP 3

                    //step validation
                    if (!isset($ses_batch_invoice->semester_id)) {
                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 1), 'default', true));
                    } else
                        if (!isset($ses_batch_invoice->sponsortype)) {
                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                        } else
                            if (!isset($ses_batch_invoice->typeinfo)) {
                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                            } else
                                if (!isset($ses_batch_invoice->invoice_date)) {
                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                }

                    if ($this->getRequest()->isPost()) {

                        $formData = $this->getRequest()->getPost();

                        $ses_batch_invoice->intake = $formData['intake'];

                        //redirect
                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 4), 'default', true));

                    }

                    if (isset($ses_batch_invoice->sponsortype)) {
                        $this->view->sponsortype = $ses_batch_invoice->sponsortype;
                    }

                    //get intake with not graduated student
                    $intakeDb = new App_Model_Record_DbTable_Intake();

                    $intakeList = $intakeDb->getIntakeWithStudent(92);
                    $this->view->intake_list = $intakeList;

                    if (isset($ses_batch_invoice->intake)) {
                        $this->view->selected_intake = $ses_batch_invoice->intake;
                    }


                } else
                    if ($step == 4) { //STEP 4

                        //step validation
                        if (!isset($ses_batch_invoice->semester_id)) {
                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 1), 'default', true));
                        } else
                            if (!isset($ses_batch_invoice->sponsortype)) {
                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                            } else
                                if (!isset($ses_batch_invoice->typeinfo)) {
                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                } else
                                    if (!isset($ses_batch_invoice->invoice_date)) {
                                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                    } else
                                        if (!isset($ses_batch_invoice->intake)) {
                                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 3), 'default', true));
                                        }

                        if ($this->getRequest()->isPost()) {

                            $formData = $this->getRequest()->getPost();

                            $ses_batch_invoice->fee_item = $formData['fi_id'];

                            //redirect
                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 5), 'default', true));

                        }

                        if (isset($ses_batch_invoice->sponsortype)) {
                            $this->view->sponsortype = $ses_batch_invoice->sponsortype;
                        }

                        // fee item
                        if ($ses_batch_invoice->typeinfo == 1) {
                            $discountTypeDb = new Studentfinance_Model_DbTable_Sponsor();
                            $feeItem = $discountTypeDb->getFeeInfo($ses_batch_invoice->sponsortype);
                        } elseif ($ses_batch_invoice->typeinfo == 2) {
                            $discountTypeDb = new Studentfinance_Model_DbTable_Scholarship();
                            $feeItem = $discountTypeDb->getFeeInfo($ses_batch_invoice->sponsortype);
                        }
                        $this->view->fee_item_list = $feeItem;


                        if (isset($ses_batch_invoice->fee_item)) {
                            $this->view->fee_item = $ses_batch_invoice->fee_item;

                        }


                    } else
                        if ($step == 5) { //STEP 5

                            //step validation
                            if (!isset($ses_batch_invoice->semester_id)) {
                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 1), 'default', true));
                            } else
                                if (!isset($ses_batch_invoice->sponsortype)) {
                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                } else
                                    if (!isset($ses_batch_invoice->typeinfo)) {
                                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                    } else
                                        if (!isset($ses_batch_invoice->invoice_date)) {
                                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                        } else
                                            if (!isset($ses_batch_invoice->intake)) {
                                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 3), 'default', true));
                                            } else
                                                if (!isset($ses_batch_invoice->fee_item)) {
                                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 4), 'default', true));
                                                }

                            $sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
                            $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
                            $scholarTaggingDB = new Studentfinance_Model_DbTable_ScholarshipStudentTag();

                            if ($ses_batch_invoice->typeinfo == 1) {
                                $feeItem = $sponsorDb->getFeeInfo($ses_batch_invoice->sponsortype);
                                $student_list = $sponsorDb->fngettagstudentById($ses_batch_invoice->sponsortype);
                                $infodetail = $sponsorDb->fngetsponsorbyid($ses_batch_invoice->sponsortype);
                            } elseif ($ses_batch_invoice->typeinfo == 2) {
                                $feeItem = $scholarDb->getFeeInfo($ses_batch_invoice->sponsortype);
                                $student_list = $scholarTaggingDB->getStudentListByType($ses_batch_invoice->sponsortype);
                                $infodetail = $scholarDb->getScholarshipById($ses_batch_invoice->sponsortype);
                            }

                            $this->view->student_list = $student_list;
                            $this->view->fee_item = $feeItem;
                            $this->view->typeinfo = $ses_batch_invoice->typeinfo;
                            $this->view->info = $infodetail;

                            if (isset($ses_batch_invoice->sponsortype)) {
                                $this->view->sponsortype = $ses_batch_invoice->sponsortype;
                            }

                            $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();

                            if ($this->getRequest()->isPost()) {

                                $formData = $this->getRequest()->getPost();


                                /*$invoiceArray = array();
                                $i=0;
                                foreach($formData['id'] as $sid){
                                    $invoice = $invoiceMainDB->getInvoiceDetailData($sid);
                                    $invoiceArray[$i]['invoice']=$invoice;

                                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                                  $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($invoice['IdStudentRegistration']);

                                    $invoiceArray[$i]['student']=$profile;
                                    $i++;
                                }*/

                                $ses_batch_invoice->invoice_list = $formData['id'];


//    		  echo "<pre>";
//    		  print_r( $ses_batch_invoice->invoice_list);
//    		  exit;

                                //redirect
                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 6), 'default', true));
                            } //end post

                            //semester
                            $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
                            $semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
                            $this->view->semester = $semester;


                            //intake
                            $intakeDb = new App_Model_Record_DbTable_Intake();
                            $intake_list = null;
                            foreach ($ses_batch_invoice->intake as $intake) {
                                $intake_list[] = $intakeDb->getData($intake);
                            }
                            $this->view->intake = $intake_list;


                            $db = Zend_Db_Table::getDefaultAdapter();

                            for ($i = 0; $i < sizeof($student_list); $i++) {

//    			$date = new DateTime('+1 day');
//			 	$currentDate = $date->format('Y-m-d');

                                $currentDate = date('Y-m-d', strtotime($ses_batch_invoice->invoice_date));

                                $disStartDate = $student_list[$i]['startdate'];
                                $disEndDate = $student_list[$i]['enddate'];
                                if ($disEndDate != '0000-00-00') {
                                    $disEndDate = $disEndDate;
                                } else {
                                    $disEndDate = $currentDate;
                                }

                                $listInvoice = $invoiceMainDB->getStudentInvoiceDataFinancialAid($ses_batch_invoice->typeinfo, $ses_batch_invoice->sponsortype, $student_list[$i]['IdStudentRegistration'], $ses_batch_invoice->semester_id, $ses_batch_invoice->fee_item, $ses_batch_invoice->paymentinfo['currency']);
                                $invoiceArray = array();
                                $invoiceArray = $listInvoice;
                                $m = 0;
                                if ($listInvoice) {
                                    foreach ($listInvoice as $inv) {

                                        $invoiceArray[$m]['invdate'] = $inv['invoice_date'];
                                        $invoiceArray[$m]['d_start'] = $disStartDate;
                                        $invoiceArray[$m]['d_end'] = $disEndDate;
                                        $invdate = $inv['invoice_date'];
                                        if ($invdate >= $disStartDate) {
                                            if ($invdate <= $disEndDate) {

                                            } else {
                                                unset($invoiceArray[$m]);
                                            }

                                        } else {
                                            unset($invoiceArray[$m]);
                                        }
                                        $m++;
                                    }
                                }

                                if ($invoiceArray) {
                                    $invoiceArray = array_values($invoiceArray);
                                }
                                if ($invoiceArray) {
                                    $student_list[$i]['invoice'] = $invoiceArray;
                                } else {
                                    $student_list[$i]['invoice'] = null;
                                }


                            }

                            $student_list = array_values($student_list);
                            $this->view->student_list = $student_list;


                            if (isset($ses_batch_invoice->student_list)) {
                                $arr_std_id = array();
                                foreach ($ses_batch_invoice->student_list as $std) {
                                    $arr_std_id[] = $std['student_id'];
                                }
                                $this->view->student_selected = $arr_std_id;

                            }


                        } else
                            if ($step == 6) {// STEP 6

                                //step validation
                                if (!isset($ses_batch_invoice->semester_id)) {
                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 1), 'default', true));
                                } else
                                    if (!isset($ses_batch_invoice->sponsortype)) {
                                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                    } else
                                        if (!isset($ses_batch_invoice->typeinfo)) {
                                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                        } else
                                            if (!isset($ses_batch_invoice->invoice_date)) {
                                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 2), 'default', true));
                                            } else
                                                if (!isset($ses_batch_invoice->intake)) {
                                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 3), 'default', true));
                                                } else
                                                    if (!isset($ses_batch_invoice->fee_item)) {
                                                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 4), 'default', true));
                                                    } else
                                                        if (!isset($ses_batch_invoice->invoice_list)) {
                                                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-sponsor-invoice', 'step' => 5), 'default', true));
                                                        }

                                //semester
                                $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
                                $semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);

                                $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                                $sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
                                $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
                                $scholarTaggingDB = new Studentfinance_Model_DbTable_ScholarshipStudentTag();

                                if ($ses_batch_invoice->typeinfo == 1) {
                                    $feeItem = $sponsorDb->getFeeInfo($ses_batch_invoice->sponsortype);
                                    $student_list = $sponsorDb->fngettagstudentById($ses_batch_invoice->sponsortype);
                                    $infodetail = $sponsorDb->fngetsponsorbyid($ses_batch_invoice->sponsortype);
                                } elseif ($ses_batch_invoice->typeinfo == 2) {
                                    $feeItem = $scholarDb->getFeeInfo($ses_batch_invoice->sponsortype);
                                    $student_list = $scholarTaggingDB->getStudentListByType($ses_batch_invoice->sponsortype);
                                    $infodetail = $scholarDb->getScholarshipById($ses_batch_invoice->sponsortype);
                                }

                                if (isset($ses_batch_invoice->sponsortype)) {
                                    $this->view->sponsortype = $ses_batch_invoice->sponsortype;
                                }

                                if ($this->getRequest()->isPost()) {

                                    $db = Zend_Db_Table::getDefaultAdapter();

//					$sponsorInvoice = $ses_batch_invoice->invoicesponsor_list;

                                    /*$arrayDiscount = array();
                                    $i=0;
                                    foreach($ses_batch_invoice->invoice_list as $data){

                                        $arrayDiscount[$i]['student_id'] = $data['student_id'];
                                        $arrayDiscount[$i]['description'] = 'Discount for '.$semester['SemesterMainName'].' ('.$feeItem['dt_description'].')';
                                        $m=0;
                                        foreach($data['invoice'] as $inv){
                                            $arrayDiscount[$i]['invoice'][$inv['currency_id']][$m]=$inv['id_detail'];
                                            $arrayDiscount[$i]['amount'][$inv['currency_id']][$m]=$inv['AmountDisc'];
                                            $m++;
                                        }
                                        $i++;
                                    }*/

//				echo "<pre>";
//				print_r($ses_batch_invoice->invoicesponsor_list);
                                    $status = $this->saveBatch($ses_batch_invoice->invoicesponsor_list, $ses_batch_invoice);


                                    if ($status) {
                                        Zend_Session::namespaceUnset('studentfinance_batch_invoice');
                                    }

                                    $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Batch Discount Saved');
                                    //redirect
                                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-list'), 'default', true));
                                } // end form post

                                $this->view->fee_item = $feeItem;
                                $this->view->semester = $semester;
                                $this->view->typeinfo = $ses_batch_invoice->typeinfo;


                                //intake
                                $intakeDb = new App_Model_Record_DbTable_Intake();
                                $intake_list = null;
                                foreach ($ses_batch_invoice->intake as $intake) {
                                    $intake_list[] = $intakeDb->getData($intake);
                                }
                                $this->view->intake = $intake_list;

                                $invoiceSponsorArray = array();
                                for ($i = 0; $i < sizeof($student_list); $i++) {

                                    $IdStudentRegistration = $student_list[$i]['IdStudentRegistration'];


//    			$date = new DateTime('+1 day');
//			 	$currentDate = $date->format('Y-m-d');

                                    $currentDate = date('Y-m-d', strtotime($ses_batch_invoice->invoice_date));

                                    $disStartDate = $student_list[$i]['startdate'];
                                    $disEndDate = $student_list[$i]['enddate'];
                                    if ($disEndDate != '0000-00-00') {
                                        $disEndDate = $disEndDate;
                                    } else {
                                        $disEndDate = $currentDate;
                                    }

                                    $listInvoice = $invoiceMainDB->getStudentInvoiceDataFinancialAid($ses_batch_invoice->typeinfo, $ses_batch_invoice->sponsortype, $student_list[$i]['IdStudentRegistration'], $ses_batch_invoice->semester_id, $ses_batch_invoice->fee_item, $ses_batch_invoice->invoice_list);

                                    $invoiceArray = array();
                                    $invoiceArray = $listInvoice;


                                    $m = 0;
                                    if ($listInvoice) {
                                        foreach ($listInvoice as $inv) {

                                            $invoiceArray[$m]['invdate'] = $inv['invoice_date'];
                                            $invoiceArray[$m]['d_start'] = $disStartDate;
                                            $invoiceArray[$m]['d_end'] = $disEndDate;
                                            $invdate = $inv['invoice_date'];
                                            if ($invdate >= $disStartDate) {
                                                if ($invdate <= $disEndDate) {

                                                } else {
                                                    unset($invoiceArray[$m]);
                                                }

                                            } else {
                                                unset($invoiceArray[$m]);
                                            }

                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['invoice_detail_id'] = $inv['id_detail'];
                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['invoice_main_id'] = $inv['invoice_main_id'];
                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['invoiceAmount'] = $inv['amount'];
                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['sponsorCalcType'] = $inv['percentage'];
                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['sponsorCalcAmount'] = $inv['Amount'];
                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['amountSponsor'] = $inv['AmountInvoice'];
                                            $invoiceSponsorArray[$IdStudentRegistration][$m]['cur_id'] = $inv['cur_id'];

                                            $m++;
                                        }
                                    }
                                    exit;
                                    if ($invoiceArray) {
                                        $invoiceArray = array_values($invoiceArray);
                                    }
                                    if ($invoiceArray) {
                                        $student_list[$i]['invoice'] = $invoiceArray;
                                    } else {
                                        $student_list[$i]['invoice'] = null;
                                    }


                                }

                                $student_list = array_values($student_list);

//    		echo "<pre>";
//	    	  print_r($invoiceSponsorArray);

                                /*echo "<pre>";
                                 print_r($ses_batch_invoice->invoice_list);*/
//	    	  echo "<pre>";
//	    	  print_r($student_list);

                                //student list
                                $this->view->invoice_list = $student_list;
                                $ses_batch_invoice->invoicesponsor_list = $invoiceSponsorArray;

                            } else {

                            }

    }

    public function batchListAction()
    {

        //title
        $this->view->title = $this->view->translate("Sponsor Invoice by Batch");

        $discountTypeDb = new Studentfinance_Model_DbTable_SponsorInvoiceBatch();

        $dataStudent = $discountTypeDb->getData();
        $i = 0;
        foreach ($dataStudent as $data) {
            $id = $data['id'];
            $discountTypeDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $dataStudentnumber = $discountTypeDb->getlistStudentByStatus($id);
            $dataStudent[$i]['number'] = $dataStudentnumber;
            $i++;
        }

        $this->view->paginator = $dataStudent;

    }

    public function batchListDetailAction()
    {

        //title
        $this->view->title = $this->view->translate("Sponsor Invoice by Batch - Detail");

        $id = $this->_getParam('id', null);
        $this->view->id = $id;

        $discountTypeDb = new Studentfinance_Model_DbTable_SponsorInvoice();
        $discountBatchDb = new Studentfinance_Model_DbTable_SponsorInvoiceBatch();
        $discountDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();

        $data = $discountBatchDb->getDataDetail($id);
        $dataStudent = $discountTypeDb->getlistStudent($id);

        $arrayStudent = array();
        $arrayStudent = $dataStudent;
        foreach ($dataStudent as $key => $dataStud) {
            $studId = $dataStud['sp_IdStudentRegistration'];
            $invStud = $discountDetailDb->getStudentData($id, $studId);
            $arrayStudent[$key]['invoice'] = $invStud;
        }

        $this->view->data = $data;
        $this->view->liststudent = $arrayStudent;

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $action = $formData['approve'];
            $dcnt_id = $formData['sp_id'];

            $i = 0;
            $n = count($dcnt_id);

            while ($i < $n) {

                $dId = $dcnt_id[$i];

                if ($action == 'Approve') {
                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceClass->sponsorInvoiceApproval($dId);
                } elseif ($action == 'Cancel') {

                    $dataCancel = array(
                        'sp_status' => 'X',
                        'sp_cancel_by' => $creator,
                        'sp_cancel_date' => date('Y-m-d H:i:s')
                    );
                    $db->update('sponsor_invoice_main', $dataCancel, 'sp_id = ' . $dId);
                }


                $i++;
            }

            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Data has been updated');
            //redirect
            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'batch-list-detail', 'id' => $id), 'default', true));
        }
    }

    private function saveBatch($dataArray, $ses_batch_invoice)
    {

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();
        $db->beginTransaction();

        try {
            $btchNum = $this->getCurrentBatchNoSeq();

            $discBtachDB = new Studentfinance_Model_DbTable_SponsorInvoiceBatch();
            $dataBatch = array(
                'semester_id' => $ses_batch_invoice->semester_id,
                'sp_type' => $ses_batch_invoice->typeinfo,
                'sp_type_id' => $ses_batch_invoice->sponsortype,
                'total_student' => count($dataArray),
                'batch_number' => $btchNum,
                'invoice_date' => date('Y-m-d', strtotime($ses_batch_invoice->invoice_date)),
                'created_by' => $creator,
                'create_date' => date('Y-m-d H:i:s')

            );

            $btch_id = $discBtachDB->insert($dataBatch);
            $amountSponsor = 0;
            foreach ($dataArray as $key => $value) {
                $studentID = $key;

                $sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
                $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                $description = '';
                if ($ses_batch_invoice->typeinfo == 1) {
                    $infodetail = $sponsorDb->fngetsponsorbyid($ses_batch_invoice->sponsortype);
                    $description .= 'Sponsorship for ' . $infodetail['name'];
                } elseif ($ses_batch_invoice->typeinfo == 2) {
                    $infodetail = $scholarDb->getScholarshipById($ses_batch_invoice->sponsortype);
                    $description .= 'Scholarship for ' . $infodetail['name'];
                }

                foreach ($value as $invData) {
                    $arrayCur[] = $invData['cur_id'];
                    $amount = $invData['amountSponsor'];
                    if ($invData['cur_id'] == 2) {
                        $dataCur = $curRateDB->getRateByDate(2, $ses_batch_invoice->invoice_date);
                        $amount = round($amount * $dataCur['cr_exchange_rate'], 2);
                    }

                    $amountSponsor += $amount;

                }
                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $invoiceClass->generateSponsorInvoice($studentID, $btch_id, $description, $ses_batch_invoice->invoice_date, $ses_batch_invoice->typeinfo, $ses_batch_invoice->sponsortype, $value, 0);

            }

            //update amount/balance spnsor_invoice_batch
            $upddataBatch = array(
                'currency_id' => 1,//always myr
                'amount' => $amountSponsor,
                'balance' => $amountSponsor,
            );

            $discBtachDB->update($upddataBatch, 'id = ' . $btch_id);

            $db->commit();
        } catch (Exception $e) {

            $db->rollBack();

            //save error message
            $sysErDb = new App_Model_General_DbTable_SystemError();
            $msg['se_IdStudentRegistration'] = $studentID;
            $msg['se_title'] = 'Error generate Discount by Batch';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = $creator;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $sysErDb->addData($msg);

            throw $e;
        }
    }

    private function getCurrentBatchNoSeq()
    {

        $seq = 1;
        $r_num = "";

        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('s' => 'sequence'))
            ->where("s.type = 14")
            ->where("s.year = ?", date('Y'));

        $row = $db->fetchRow($selectData);

        if ($row) {
            $seq = $row['seq'] + 1;
            $db->update('sequence', array('seq' => $seq), 'year = ' . date('Y') . ' and type = 14');
        } else {
            $db->insert('sequence', array('year' => date('Y'), 'type' => 14, 'seq' => 1));
        }

        //read initial config
        $selectData2 = $db->select()
            ->from(array('i' => 'tbl_config'))
            ->where("i.idUniversity = 1")
            ->order("i.UpdDate ASC");

        $row2 = $db->fetchRow($selectData2);

        if ($row2) {
            $r_num = 'SBN' . date('Y') . $row2['ReceiptSeparator'] . str_pad($seq, 5, "0", STR_PAD_LEFT);
        }

        return $r_num;
    }

    public function getTypeDataAction()
    {
        $id = $this->_getParam('id', 0);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        if ($id == 1) { //sponsorship
            $sponsortypeDb = new Studentfinance_Model_DbTable_Sponsor();
            $result = $sponsortypeDb->fngetallsponsor();
        } elseif ($id == 2) { //scholarship
            $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
            $result = $scholarDb->getScholarshipList();
        }


        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function receiptAction()
    {

        //title
        $this->view->title = $this->view->translate("Sponsor Invoice - Receipt");

        //payment group
        $payment_group_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');
        $this->view->payment_group = $payment_group_list;

        //mode of payment
        $paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
        $mode_of_payment_list = $paymentModeDb->getPaymentModeList();
        $this->view->mode_of_payment = $mode_of_payment_list;

        //currency
        $currencyDb = new Studentfinance_Model_DbTable_Currency();
        $currency_list = $currencyDb->fetchAll('cur_status = 1');
        $this->view->currency_list = $currency_list;

        //credit card terminal
        $creditCardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
        $creditCardTerminal_list = $creditCardTerminalDb->getTerminalList();
        $this->view->terminal_list = $creditCardTerminal_list;

        //bank from account code
        $accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
        $bankAccountCodeList = $accountCodeDb->getAccountCode(1, array(614, 948));//hardcoded to bank acc code
        $this->view->bank_list = $bankAccountCodeList;

        $discountTypeDb = new Studentfinance_Model_DbTable_SponsorInvoiceBatch();

        $dataStudent = $discountTypeDb->getData();
        $i = 0;
        foreach ($dataStudent as $data) {
            $id = $data['id'];
            $discountTypeDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $dataStudentnumber = $discountTypeDb->getlistStudentByStatus($id);
            $dataStudent[$i]['number'] = $dataStudentnumber;
            $i++;
        }

        $this->view->paginator = $dataStudent;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();


//			echo "<pre>";
//			print_r($formData);


            $newArray = array();

            foreach ($formData['invoice_id'] as $key => $value) {

                $spinvoiceDetailDB = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
                $invoiceData = $spinvoiceDetailDB->getData($value);

                $newArray[$invoiceData['sp_IdStudentRegistration']][] = $invoiceData;

            }

//			echo "<pre>";
//				print_r($newArray);

//			exit;

            $db = Zend_Db_Table::getDefaultAdapter();
            $db->beginTransaction();

            try {

                $auth = Zend_Auth::getInstance();
                $getUserIdentity = $auth->getIdentity();

                $sponsorInvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();

                $resultData = $sponsorInvoiceDetailDb->getData($formData['invoice_id'][0]);
                $idStudent = $resultData['IdStudentRegistration'];
                //calculate total payment amount
                $total_payment_amount = $formData['payment_amount'];

                $currencyUsed = $formData['currency'];

                //get currency from code
                $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                $currency = $currencyDb->getCurrentExchangeRate($currencyUsed);

                //add receipt
                $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
                $receipt_no = $this->getReceiptNoSeq();

                $receipt_data = array(
                    'rcp_no' => $receipt_no,
                    'rcp_account_code' => $formData['receipt_account_code'],
                    'rcp_date' => date('Y-m-d H:i:s'),
                    'rcp_receive_date' => date('Y-m-d', strtotime($formData['receipt_receive_date'])),
                    'rcp_batch_id' => $resultData['sp_batch_id'],
                    'rcp_type' => $resultData['sp_type'],
                    'rcp_type_id' => $resultData['sp_type_id'],
                    'rcp_description' => $formData['receipt_description'],
                    'rcp_amount' => $total_payment_amount,
                    'rcp_gainloss' => $formData['rcp_gainloss'],
                    'rcp_gainloss_amt' => $formData['rcp_gainloss_amt'],
                    'rcp_cur_id' => $currencyUsed,
                    'rcp_exchange_rate' => $currency['cr_id'],
                    'p_payment_mode_id' => $formData['mode_of_payment'],
                    'p_cheque_no' => isset($formData['cheque_no']) ? $formData['cheque_no'] : null,
                    'p_doc_bank' => isset($formData['cheque_no']) ? $formData['cheque_no'] : null,
//					'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                    'p_terminal_id' => isset($formData['terminal_id']) ? $formData['terminal_id'] : null,
                    'p_card_no' => isset($formData['card_no']) ? $formData['card_no'] : null,
                    'rcp_create_by' => $getUserIdentity->id
                );
                $receipt_id = $receiptDb->insert($receipt_data);

                //add receipt-invoice detail
                $receiptInvoiceDb = new Studentfinance_Model_DbTable_SponsorReceiptInvoice();

                foreach ($newArray as $key => $invoiceDataVal) { //invoice_detail_id

                    $data_rcp_stud = array(
                        'rcp_inv_rcp_id' => $receipt_id,
                        'IdStudentRegistration' => $key,
                        'rcp_inv_amount' => 0,
                        'rcp_adv_amount' => 0,
                        'rcp_inv_cur_id' => 0,
                        'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                        'rcp_inv_create_by' => $getUserIdentity->id
                    );

                    $db->insert('sponsor_invoice_receipt_student', $data_rcp_stud);
                    $idStd = $db->lastInsertId();

                    $total_payment_amount = 0;
                    foreach ($invoiceDataVal as $invoiceData) {
                        $value = $invoiceData['id'];
                        $currencyItem = $invoiceData['sp_currency_id'];
                        $amountPaid = $invoiceData['sp_balance_det'];

                        $total_payment_amount += $amountPaid;

                        $data_rcp_inv = array(
                            'rcp_inv_rcp_id' => $receipt_id,
                            'rcp_inv_sp_id' => $invoiceData['sp_id'],
                            'rcp_inv_student' => $idStd,
                            'rcp_inv_sponsor_dtl_id' => $value,
                            'rcp_inv_amount' => $amountPaid,
                            'rcp_inv_cur_id' => $currencyItem,
                            'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                            'rcp_inv_create_by' => $getUserIdentity->id
                        );

                        $receiptInvoiceDb->insert($data_rcp_inv);
                    }

                    $dataAdv = array('rcp_inv_amount' => $total_payment_amount, 'rcp_inv_cur_id' => $invoiceDataVal[0]['sp_currency_id']);
                    $db->update('sponsor_invoice_receipt_student', $dataAdv, $db->quoteInto("rcp_inv_id = ?", $idStd));

                }

                $db->commit();

                $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Sponsor Receipt Added');

                $this->_redirect($this->baseUrl . '/studentfinance/sponsor-invoice/view-receipt-detail/id/' . $receipt_id);

            } catch (Exception $e) {
                $db->rollBack();
                echo $e->getMessage();
                exit;
            }

        }

    }

    public function ajaxGetInvoiceAction()
    {
        $type = $this->_getParam('type', 0);
        $id = $this->_getParam('id', 0);
        $batchno = $this->_getParam('batchno', 0);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $sponsorInvoiceDb = new Studentfinance_Model_DbTable_SponsorInvoice();
        $sponsorInvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();

        $result = $sponsorInvoiceDb->getlistInvoice($type, $id, $batchno);

        $dataArray = array();
        $dataArray = $result;
        foreach ($result as $key => $data) {
            $dataArray[$key]['detail'] = $sponsorInvoiceDetailDb->getSponsorInvoiceDetail($data['sp_id']);
        }

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($dataArray);

        echo $json;
        exit();
    }

    public function findPayeeAction()
    {
        $id = $this->_getParam('id', '');
        $type = $this->_getParam('type', 1);
        $batchno = $this->getParam('batchno', 1);
        $limit = 20;

        $this->_helper->layout->disableLayout();

        $sponsorInvoiceDb = new Studentfinance_Model_DbTable_SponsorInvoice();

        $result = $sponsorInvoiceDb->autocompleteSearch($type, $id, $batchno, $limit);

        if ($id == 1) { //sponsorship
            $sponsortypeDb = new Studentfinance_Model_DbTable_Sponsor();
            $result = $sponsortypeDb->fngetsponsorbyid($id);
        } elseif ($id == 2) { //scholarship
            $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
            $result = $scholarDb->getScholarshipById($id);
        }

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function getBatchDataAction()
    {
        $id = $this->_getParam('id', 0);
        $type = $this->_getParam('type', 0);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $sponsorBatchDb = new Studentfinance_Model_DbTable_SponsorInvoiceBatch();
        $result = $sponsorBatchDb->getBatchType($type, $id);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    /*
         * Get Bill no from mysql function
        */
    private function getReceiptNoSeq()
    {

        $seq_data = array(
            12,
            date('Y'),
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }

    public function viewReceiptDetailAction()
    {

        $id = $this->_getParam('id', null);

        if ($id == null) {
            throw new Exception('Not enough parameter');
        }

        $this->view->title = $this->view->translate("Sponsor Receipt - Detail");


        $sponsorreceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $receipt = $sponsorreceiptDb->getData($id);
        $this->view->receipt = $receipt[0];

//		echo "<pre>";
//		print_r($receipt[0]);
//		exit;


    }

    public function approve2Action()
    {

        $id = $this->_getParam('id', null);

        //receipt
        $sponsorreceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $receipt = $sponsorreceiptDb->getData($id);

        $receipt = $receipt[0];

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        /*
         * knockoff invoice
         */

        if ($receipt['rcp_status'] == 'ENTRY') {
            $sponsorinvoiceMainDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $sponsorinvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
            $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();

            $totalAmount = 0.00;
            $total_advance_payment_amount = 0;
            $total_payment_amount = $receipt['rcp_amount'];
            $total_paid_receipt = 0;
            $paidReceiptInvoice = 0;

            if ($receipt['receipt_invoice']) {
                //get receipt invoice
                foreach ($receipt['receipt_invoice'] as $inv) {
                    $invID = $inv['rcp_inv_sp_id'];//sponsor_invoice_main
                    $invDetID = $inv['rcp_inv_sponsor_dtl_id'];//sponsor_invoice_detail
                    $rcpCurrency = $receipt['rcp_cur_id'];

                    $invDetData = $sponsorinvoiceDetailDb->getData($invDetID);

                    $invCurrency = $invDetData['currency_id'];

                    $currencyDb = new Studentfinance_Model_DbTable_Currency();
                    $currency = $currencyDb->fetchRow('cur_id = ' . $invCurrency)->toArray();

                    $amountDefault = $invDetData['sp_balance'];

                    $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                    $paidReceipt = $inv['rcp_inv_amount'];
                    if ($invCurrency == 2) {
                        $dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                        $paidReceipt = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                    }

                    $balance = $amountDefault - $paidReceipt;

                    //update invoice details
                    $dataInvDetail = array(
                        'sp_paid' => $paidReceipt + $invDetData['sp_paid'],
                        'sp_balance' => $balance,
                    );

                    $sponsorinvoiceDetailDb->update($dataInvDetail, array('id =?' => $invDetID));

                    $totalAmount += $paidReceiptInvoice;

                    //update invoice main
                    $invMainData = $sponsorinvoiceMainDb->getData($invID);

                    $balanceMain = ($invMainData['sp_balance']) - ($paidReceipt);
                    $paidMain = ($invMainData['sp_paid']) + ($paidReceipt);

                    //update invoice main
                    $dataInvMain = array(
                        'sp_paid' => $paidMain,
                        'sp_balance' => $balanceMain,
                        'sp_approve_by' => $getUserIdentity->id,
                        'sp_approve_date' => date('Y-m-d H:i:s')
                    );

                    $sponsorinvoiceMainDb->update($dataInvMain, array('sp_id =?' => $invID));
                    $invDetMainID = $inv['idDet'];
                    $invMainId = $inv['idMain'];

                    //update invoice_main & invoice_detail
                    $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                    $invDet = $invoiceDetailDB->getData($invDetMainID);

                    $amountDN = $inv['rcp_inv_amount'];
                    if ($invDet['cur_id'] == 2) {
                        $dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                        $amountDN = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                    }

                    $balanceNew1 = $invDet['balance'] - $amountDN;
                    $dataUpdDetail = array('balance' => $balanceNew1);
                    $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetMainID));

                    //update amount sponsor at invoice_main

                    $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                    $invMain = $invoiceMainDB->getData($invMainId);

                    $balanceNew = $invMain['bill_balance'] - $amountDN;
                    $dataUpdMain = array('bill_balance' => $balanceNew, 'upd_by' => $creator, 'upd_date' => date('Y-m-d H:i:s'));
                    $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invMainId));


                }
            }

        }

        /*knockoff end*/


        //change receipt status to APPROVE
        if ($receipt['rcp_status'] == 'ENTRY') {
            if ($receipt['receipt_invoice']) {
                //get receipt invoice
                foreach ($receipt['receipt_invoice'] as $inv) {
                    $invID = $inv['idMain'];
                    $invDetMainID = $inv['idDet'];


                    if ($invID) {
                        if ($balanceNew == 0) {
                            $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                            $paymentClass->updateCourseStatus($invID);

                        }
                    }
                }
            }

            $sponsorreceiptDb->update(
                array(
                    'rcp_status' => 'APPROVE',
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => $getUserIdentity->id
                ), 'rcp_id = ' . $receipt['rcp_id']);

            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Receipt Approved');
        } else {
            $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'Receipt Already Approved');
        }

        $this->_redirect($this->baseUrl . '/studentfinance/sponsor-invoice/view-receipt-detail/id/' . $id);
    }

    public function cancelAction()
    {

        $id = $this->_getParam('id', 0);

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();

        $db = Zend_Db_Table::getDefaultAdapter();

        //get receipt detail
        $receiptData = $receiptDb->getDataReceipt($id);

        $auth = Zend_Auth::getInstance();

        $upd_data = array(
            'rcp_status' => 'CANCEL',
            'cancel_by' => $getUserIdentity->id,
            'cancel_date' => date('Y-m-d H:i:s')
        );

        $receiptDb->update($upd_data, array('rcp_id = ?' => $id));
        $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'Receipt has been Cancelled');
        $this->_redirect($this->baseUrl . '/studentfinance/sponsor-invoice/view-receipt-detail/id/' . $id);
//		exit;
    }

    public function receiptOpenAction()
    {

        //title
        $this->view->title = $this->view->translate("Sponsor Payment - Open Invoice");

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $ses_batch_invoice = new Zend_Session_Namespace('studentfinance_sponsorpayment');
        $ses_batch_invoice->setExpirationSeconds(900);

        //payment group
        $payment_group_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');
        $this->view->payment_group = $payment_group_list;

        //mode of payment
        $paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
        $mode_of_payment_list = $paymentModeDb->getPaymentModeList();
        $this->view->mode_of_payment = $mode_of_payment_list;

        //currency
        $currencyDb = new Studentfinance_Model_DbTable_Currency();
        $currency_list = $currencyDb->fetchAll('cur_status = 1');
        $this->view->currency_list = $currency_list;

        //credit card terminal
        $creditCardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
        $creditCardTerminal_list = $creditCardTerminalDb->getTerminalList();
        $this->view->terminal_list = $creditCardTerminal_list;

        //bank from account code
        $accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
        $bankAccountCodeList = $accountCodeDb->getAccountCode(1, array(614, 948));//hardcoded to bank acc code
        $this->view->bank_list = $bankAccountCodeList;

        $step = $this->_getParam('step', 1);
        $this->view->step = $step;

        $db = Zend_Db_Table::getDefaultAdapter();

        if ($step != 5 && $step != 6) {
            unset($ses_batch_invoice->student_list);
        }

        if ($step == 1) { //STEP 1

            //Zend_Session::namespaceUnset('studentfinance_batch_invoice');

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                if ($formData['sponsortype'] != null && $formData['typeinfo'] != null) {

                    $ses_batch_invoice->sponsortype = $formData['sponsortype'];
                    $ses_batch_invoice->typeinfo = $formData['typeinfo'];

                    //redirect
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 2), 'default', true));
                }

            }

            if (isset($ses_batch_invoice->sponsortype)) {
                $this->view->sponsortype = $ses_batch_invoice->sponsortype;
            }

            if (isset($ses_batch_invoice->typeinfo)) {
                $this->view->typeinfo = $ses_batch_invoice->typeinfo;
            }


        } else
            if ($step == 2) { //STEP 2

                //step validation
                if (!isset($ses_batch_invoice->sponsortype)) {
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 1), 'default', true));
                } elseif (!isset($ses_batch_invoice->typeinfo)) {
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 1), 'default', true));
                }

                if ($this->getRequest()->isPost()) {

                    $formData = $this->getRequest()->getPost();

                    $ses_batch_invoice->paymentinfo = $formData;

                    //redirect
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 3), 'default', true));

                }

                if (isset($ses_batch_invoice->paymentinfo)) {
                    $this->view->paymentinfo = $ses_batch_invoice->paymentinfo;
                }

                /*echo "<pre>";
                print_r($ses_batch_invoice->paymentinfo);*/

            } else
                if ($step == 3) { //STEP 3

                    //step validation
                    if (!isset($ses_batch_invoice->sponsortype)) {
                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 1), 'default', true));
                    } else
                        if (!isset($ses_batch_invoice->typeinfo)) {
                            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 1), 'default', true));
                        } else
                            if (!isset($ses_batch_invoice->paymentinfo)) {
                                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'receipt-open', 'step' => 2), 'default', true));
                            }

//    		echo "<pre>";
//    		print_r($ses_batch_invoice->paymentinfo);

                    $this->view->paymentinfo = $ses_batch_invoice->paymentinfo;

                    $sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
                    $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
                    $scholarTaggingDB = new Studentfinance_Model_DbTable_ScholarshipStudentTag();
//    		echo $ses_batch_invoice->sponsortype;
                    if ($ses_batch_invoice->typeinfo == 1) {
                        $feeItem = $sponsorDb->getFeeInfo($ses_batch_invoice->sponsortype);
                        $student_list = $sponsorDb->fngettagstudentById($ses_batch_invoice->sponsortype, 0, 0, null, true);
                        $infodetail = $sponsorDb->fngetsponsorbyid($ses_batch_invoice->sponsortype);
                        $feeItem = $sponsorDb->getFeeInfo($ses_batch_invoice->sponsortype);
                        $description = 'Sponsorship Payment';
                    } elseif ($ses_batch_invoice->typeinfo == 2) {
                        $feeItem = $scholarDb->getFeeInfo($ses_batch_invoice->sponsortype);
                        $student_list = $scholarTaggingDB->getStudentListByType($ses_batch_invoice->sponsortype);
                        $infodetail = $scholarDb->getScholarshipById($ses_batch_invoice->sponsortype);
                        $feeItem = $scholarDb->getFeeInfo($ses_batch_invoice->sponsortype);
                        $description = 'Scholarship Payment';
                    }

                    $feeItemName = array();
                    foreach ($feeItem as $fee) {
                        $feeItemName[] = $fee['fi_id'];
                    }
                    //var_dump($student_list);
                    $this->view->student_list = $student_list;
                    $this->view->typeinfo = $ses_batch_invoice->typeinfo;
                    $this->view->info = $infodetail;
                    $this->view->fee_item = $feeItem;
//    		echo "<pre>";
//    		print_r($feeItem);
                    $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();

                    if ($this->getRequest()->isPost()) {

                        $formData = $this->getRequest()->getPost();

    		  //echo "<pre>";
    		  //print_r($formData);
    		  //exit;


                        $dataChecked = $formData['id'];
                        $n = count($dataChecked);

                        $total_payment_amount = $ses_batch_invoice->paymentinfo['payment_amount'];

                        $currencyUsed = $ses_batch_invoice->paymentinfo['currency'];


                        $value = array();
                        $newarray = array();
                        $newarray['total_amount_sponsor'] = $total_payment_amount;
                        $totalAllSponsorInvoice = 0;
                        foreach ($formData['amount'] as $key => $dataC) {
                            $amountSponsor = $dataC;

                            if ($amountSponsor) {

                                $idStudent = $formData['id'][$key];

                                /*
                                 * 1. generate sponsor invoice
                                 * 2. generate receipt - status ENTRY
                                 */

                                //TODO : 03/08/2015 - kena amik dari yang checked instead of query blk data

                                $dataInvChecked = $formData['id_invoice'][$key];

                                $naIv = 1;
                                $totalInv = 0;
                                $totalInvAll = 0;

                                $m = 0;
                                $dataInvChecked = $formData['idInv'][$key];
                                //dd($formData); exit;
                                $newvalue = array();
                                foreach ($dataInvChecked as $keyI => $dataI) {
                                    $IdInvoice = $formData['id_invoice'][$key][$keyI]; //id invoice_detail
                                    
                                    if (isset($keyI)) {
                                        $totalAmountStudent = $formData['amount'][$key];
                                        $newarray[$idStudent]['total'] = $totalAmountStudent;
                                        //$invAMount = $dataInvChecked[$keyI];
                                        $invAMount = $formData['idInv2'][$key][$keyI];

                                        $receiptDate = date('Y-m-d', strtotime($ses_batch_invoice->paymentinfo['receipt_receive_date']));

                                        //get invoice info
                                        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceDetail();
                                        $invoiceData = $invoiceMainDb->getData($IdInvoice);

                                        $invAMountNew = $totalAmountStudent - $totalInv;
                                        if ($totalInv < $totalAmountStudent) {
                                            $totalInv += $invAMount;

                                            $newAmount = $invoiceData['amount'];
                                            if ($invAMountNew < $invoiceData['amount']) {
                                                $newAmount = $invAMountNew;
                                            }

                                            $value['invoice_detail_id'] = $IdInvoice;
                                            $value['invoice_main_id'] = $invoiceData['invoice_main_id'];
                                            $value['invoiceAmount'] = $invoiceData['amount'];
                                            $value['amountSponsor'] = $invAMount;
                                            $value['total'] = $totalInv;
                                            $value['amountBalance'] = $invAMountNew;
                                            $value['amountSponsorReceipt'] = $newAmount; // invoice amount sponsor
                                            $newarray[$idStudent][$m] = $value;

                                            $newvalue[] = $value;
                                            $totalInvAll += $newAmount;


                                        }

                                        $m++;

                                    }

                                }

                                //generate invoice sponsor
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                                $sp_id = $invoiceClass->generateSponsorInvoice($idStudent, 0, $description, $receiptDate, $ses_batch_invoice->typeinfo, $ses_batch_invoice->sponsortype, $newvalue, 0);
                                $newarray[$idStudent]['total_invoice'] = $totalInvAll;
                                $newarray[$idStudent]['sp_id'] = $sp_id;
                                //adv_amount per student
                                if ($totalInvAll < $totalAmountStudent) {
                                    $newarray[$idStudent]['advance_amount'] = $totalAmountStudent - $totalInvAll;
                                }
                                $totalAllSponsorInvoice += $totalAmountStudent;
                                $newarray['total_invoice_amount'] = $totalAllSponsorInvoice;

                            }

                        }

                        //advance amount sponsor
                        if ($totalAllSponsorInvoice < $total_payment_amount) {
                            $newarray['advance_sponsor'] = $total_payment_amount - $totalAllSponsorInvoice;
                        }

//    		  echo "<pre>";
//	    		  			print_r($newarray);
//    		  exit;	


                        $db->beginTransaction();

                        try {
                            //calculate total payment amount

                            //get currency from code
                            $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                            $currency = $currencyDb->getCurrentExchangeRate($currencyUsed);

                            //add receipt
                            $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
                            $receipt_no = $this->getReceiptNoSeq();

                            //invoice data


                            $receipt_data = array(
                                'rcp_no' => $receipt_no,
                                'rcp_account_code' => $ses_batch_invoice->paymentinfo['receipt_account_code'],
                                'rcp_date' => date('Y-m-d H:i:s'),
                                'rcp_receive_date' => date('Y-m-d', strtotime($ses_batch_invoice->paymentinfo['receipt_receive_date'])),
                                'rcp_batch_id' => 0,
                                'rcp_type' => $ses_batch_invoice->typeinfo,
                                'rcp_type_id' => $ses_batch_invoice->sponsortype,
                                'rcp_description' => $ses_batch_invoice->paymentinfo['receipt_description'],
                                'rcp_amount' => $total_payment_amount,
                                'rcp_adv_amount' => $newarray['advance_sponsor'],
                                'rcp_gainloss' => $ses_batch_invoice->paymentinfo['rcp_gainloss'],
                                'rcp_gainloss_amt' => $ses_batch_invoice->paymentinfo['rcp_gainloss_amt'],
                                'rcp_cur_id' => $currencyUsed,
                                'rcp_exchange_rate' => $currency['cr_id'],
                                'p_payment_mode_id' => $ses_batch_invoice->paymentinfo['mode_of_payment'],
                                'p_cheque_no' => isset($ses_batch_invoice->paymentinfo['cheque_no']) ? $ses_batch_invoice->paymentinfo['cheque_no'] : null,
                                'p_doc_bank' => isset($ses_batch_invoice->paymentinfo['cheque_no']) ? $ses_batch_invoice->paymentinfo['cheque_no'] : null,
//					'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                                'p_terminal_id' => isset($ses_batch_invoice->paymentinfo['terminal_id']) ? $ses_batch_invoice->paymentinfo['terminal_id'] : null,
                                'p_card_no' => isset($ses_batch_invoice->paymentinfo['card_no']) ? $ses_batch_invoice->paymentinfo['card_no'] : null,
                                'rcp_create_by' => $getUserIdentity->id
                            );
                            $receipt_id = $receiptDb->insert($receipt_data);
//				echo "<pre>";
//				print_r($newarray);
//				exit;			
                            unset($newarray['advance_sponsor']);
                            unset($newarray['total_invoice_amount']);
                            unset($newarray['total_amount_sponsor']);

				//echo "<pre>";
				//print_r($newarray);
				//exit;
                            foreach ($newarray as $idStudent => $data) {

                                $receipInvAmount = $data['total'];
                                $amountAdv = isset($data['advance_amount']) ? $data['advance_amount'] : 0;

                                //generate sponsor payment

                                //add receipt-invoice detail
                                $receiptInvoiceDb = new Studentfinance_Model_DbTable_SponsorReceiptInvoice();
                                //insert
                                $data_rcp_stud = array(
                                    'rcp_inv_rcp_id' => $receipt_id,
                                    'IdStudentRegistration' => $idStudent,
                                    'rcp_inv_amount' => $receipInvAmount,
                                    'rcp_adv_amount' => $amountAdv,
                                    'rcp_inv_cur_id' => $currencyUsed,
                                    'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                    'rcp_inv_create_by' => $getUserIdentity->id
                                );


                                $db->insert('sponsor_invoice_receipt_student', $data_rcp_stud);
                                $idStd = $db->lastInsertId();

                                //get sponsor invoice list - sponsor_invoice_detail

//							if($sp_id != 0){

                                $sp_id = $data['sp_id'];

                                $spinvoiceDetailDB = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
                                $invoiceData = $spinvoiceDetailDB->getSponsorInvoiceDetail($sp_id);


                                foreach ($invoiceData as $key => $dataInv) { //invoice_detail_id

                                    $currencyItem = $currencyUsed;
                                    $amountPaid = $dataInv['sp_receipt'];

                                    $data_rcp_inv = array(
                                        'rcp_inv_rcp_id' => $receipt_id,
                                        'rcp_inv_sp_id' => $sp_id,
                                        'rcp_inv_student' => $idStd,
                                        'rcp_inv_sponsor_dtl_id' => $dataInv['id_sp_id'],
                                        'rcp_inv_amount' => $amountPaid,
                                        'rcp_inv_amount_default' => $amountPaid,
                                        'rcp_inv_cur_id' => $currencyItem,
                                        'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                        'rcp_inv_create_by' => $getUserIdentity->id
                                    );
//									echo "<pre>";
//				print_r($data_rcp_inv);
//				exit;
                                    $receiptInvoiceDb->insert($data_rcp_inv);
                                }
//							}

                            }


                            $db->commit();

                            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Sponsor Receipt Added');
                        } catch (Exception $e) {

                            $db->rollBack();

                            //save error message
                            $sysErDb = new App_Model_General_DbTable_SystemError();
                            $msg['se_IdStudentRegistration'] = $idStudent;
                            $msg['se_title'] = 'Error generate Sponsor Payment for Open Invoice';
                            $msg['se_message'] = $e->getMessage();
                            $msg['se_createdby'] = $getUserIdentity->id;
                            $msg['se_createddt'] = date("Y-m-d H:i:s");
                            $sysErDb->addData($msg);

                            throw $e;
                        }


//    		  exit;
                        $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'sponsor-invoice', 'action' => 'view-receipt-detail', 'id' => $receipt_id), 'default', true));
                    } //end post


                    $db = Zend_Db_Table::getDefaultAdapter();

                    if ($feeItemName) {

                        for ($i = 0; $i < sizeof($student_list); $i++) {

//                            $currentDate = new DateTime('+1 day');
                            $currentDate = date('Y-m-d');
                            //var_dump($student_list[$i]['registrationId']);
                            if (isset($student_list[$i]['IdStudentRegistration'])) {

                                //$currentDate = date('Y-m-d',strtotime($ses_batch_invoice->invoice_date));
                                $UtilizedAmount = 0;
                                if ($ses_batch_invoice->typeinfo == 1) {

                                    if ($student_list[$i]['Amount'] != 0) {
                                        $UtilizedAmount = $student_list[$i]['Amount'] - $student_list[$i]['utilized_amount'];
                                    }

                                }

                                $student_list[$i]['sponsorbalance'] = $UtilizedAmount;

                                $disStartDate = $student_list[$i]['startdate'];
                                $disEndDate = $student_list[$i]['enddate'];

                                $student_list[$i]['startdate'] = $disStartDate;
                                $student_list[$i]['enddate'] = $disEndDate;

                                if ($disEndDate != '0000-00-00') {
                                    $disEndDate = $disEndDate;
                                } else {
                                    $disEndDate = $currentDate;
                                }


                                $listInvoice = $invoiceMainDB->getStudentInvoiceDataFinancialAid($ses_batch_invoice->typeinfo, $ses_batch_invoice->sponsortype, $student_list[$i]['IdStudentRegistration'], null, $feeItemName, array(), $ses_batch_invoice->paymentinfo['currency']);

                                if ($listInvoice) {
                                    $listInvoice = array_values($listInvoice);
                                }

                                $invoiceArray = array();
                                $invoiceArray = $listInvoice;
                                $m = 0;
                                if ($listInvoice) {
                                    foreach ($listInvoice as $inv) {

                                        //get receive_date currency rate
                                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                                        $curRateReceipt = $curRateDB->getRateByDate($inv['cur_id'],$ses_batch_invoice->paymentinfo['receipt_receive_date']);

                                        $invoiceArray[$m]['balance'] = round($inv['balance']* $curRateReceipt['cr_exchange_rate'],2);

                                        $invoiceArray[$m]['invdate'] = isset($inv['invoice_date']) ? $inv['invoice_date'] : '';
                                        $invoiceArray[$m]['d_start'] = $disStartDate;
                                        $invoiceArray[$m]['d_end'] = $disEndDate;

                                        $invdate = isset($inv['invoice_date']) ? $inv['invoice_date'] : '';

                                        if ($invdate) {
                                            if ($invdate >= $disStartDate) {

                                                //14/08/2015 <= 01-09-2015
                                                if ($invdate <= $disEndDate) {

                                                } else {

                                                    unset($invoiceArray[$m]);
                                                    //			    						unset($student_list[$m]);
                                                }

                                            } else {

                                                unset($invoiceArray[$m]);
                                                //			    					unset($student_list[$m]);
                                            }
                                        } else {
                                            unset($invoiceArray[$m]);
                                        }


                                        $m++;
                                    }
                                }


                            }

                            if ($invoiceArray) {
                                $invoiceArray = array_values($invoiceArray);
                            }

                            if (isset($student_list[$i]['Amount'])) {
                                if ($student_list[$i]['Amount'] != 0 && ($student_list[$i]['Amount'] == $student_list[$i]['utilized_amount'])) {
                                    unset($student_list[$i]);
                                }
                            }

//                            echo "<pre>";
//                            print_r($invoiceArray);

                            if ($invoiceArray) {
                                $student_list[$i]['invoice'] = $invoiceArray;
                            } else {
//                                unset($student_list[$i]);
                                //    		  	$student_list[$i]['invoice'] = null;
                            }


                        }

//                        echo "<pre>";
//                        print_r($student_list);

                        $student_list = array_values($student_list);
                        $this->view->student_list = $student_list;
                    } else {

                        $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'No Fee Tagging has been setup');
                    }

                    $this->view->fee = $feeItemName;

                    if (isset($ses_batch_invoice->student_list)) {
                        $arr_std_id = array();
                        foreach ($ses_batch_invoice->student_list as $std) {
                            $arr_std_id[] = $std['student_id'];
                        }
                        $this->view->student_selected = $arr_std_id;

                    }


                } else {

                }

    }

    public function approveAction()
    {

        $id = $this->_getParam('id', null);

        //receipt
        $sponsorreceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $receipt = $sponsorreceiptDb->getData($id);

//		echo "<pre>";
//		print_r($receipt);
//		exit;
        $receipt = $receipt[0];

//		echo "<pre>";
//		print_r($receipt);
//		exit;
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        /*
         * knockoff invoice
         */

        if ($receipt['rcp_status'] == 'ENTRY') {
            $sponsorinvoiceMainDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $sponsorinvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
            $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
//			$sponsorReceiptStudentDB = new Studentfinance_Model_DbTable_SponsorInvoiceStudent();


            foreach ($receipt['receipt_student'] as $rcpStudent) {

                $totalSponsorAmount = $rcpStudent['rcp_inv_amount'];

                $appTrxId = $rcpStudent['IdStudentRegistration'];
                $txnStudentDb = new Registration_Model_DbTable_Studentregistration();
                $txnProfile = $txnStudentDb->getData($appTrxId);

                //get current semester
                $semesterDb = new Records_Model_DbTable_SemesterMaster();
                $semesterInfo = $semesterDb->getStudentCurrentSemester($txnProfile);

                $totalAmount = 0.00;
                $total_payment_amount = $receipt['rcp_amount'];
                $total_paid_receipt = 0;
                $paidReceiptInvoice = 0;
                $paidReceipt = 0;

                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    $balanceNew1 = 0;
                    $paidNew1 = 0;
                    $balanceNew = 0;
                    $paidNew = 0;
                    $balance = 0;
                    $amountDN = 0;
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['rcp_inv_sp_id'];//sponsor_invoice_main
                        $invDetID = $inv['rcp_inv_sponsor_dtl_id'];//sponsor_invoice_detail
                        $rcpCurrency = $receipt['rcp_cur_id'];

                        $invDetData = $sponsorinvoiceDetailDb->getData($invDetID);

//                        echo "<pre>";
//                        print_r($invDetData);
                        $invCurrency = $invDetData['currency_id'];

                        $currencyDb = new Studentfinance_Model_DbTable_Currency();
//                        $currency = $currencyDb->fetchRow('cur_id = ' . $invCurrency)->toArray();
                        /*echo "<pre>";
                        print_r($currency);
                    }
                }
            }
        }
                        exit;*/
                        $amountDefault = $invDetData['sp_balance_det'];

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                        $paidReceipt = $inv['rcp_inv_amount'];

                        $balance = $amountDefault - $paidReceipt;
                        $totalSponsorAmount -= $paidReceipt;

                        //update invoice details
                        $dataInvDetail = array(
                            'sp_paid' => $paidReceipt + $invDetData['sp_paid_det'],
                            'sp_balance' => $balance,
                        );

                        if ($invCurrency != 1) {
                            $dataCur = $curRateDB->getRateByDate($invCurrency,$receipt['rcp_receive_date']);
//                            $dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
//                            $dataCur = $curRateDB->getCurrentExchangeRate($invCurrency);
                            $paidReceipt = round($inv['rcp_inv_amount'] / $dataCur['cr_exchange_rate'], 2);
//                            $paidReceipt = round($inv['rcp_inv_amount'],2);
                        }



                        $sponsorinvoiceDetailDb->update($dataInvDetail, array('id =?' => $invDetID));

                        $totalAmount += $paidReceiptInvoice;

                        //update invoice main
                        $invMainData = $sponsorinvoiceMainDb->getData($invID);

                        $balanceMain = ($invMainData['sp_balance']) - ($paidReceipt);
                        $paidMain = ($invMainData['sp_paid']) + ($paidReceipt);

                        //update invoice main
                        $dataInvMain = array(
                            'sp_paid' => $paidMain,
                            'sp_balance' => $balanceMain,
                            'sp_approve_by' => $getUserIdentity->id,
                            'sp_approve_date' => date('Y-m-d H:i:s')
                        );

                        $sponsorinvoiceMainDb->update($dataInvMain, array('sp_id =?' => $invID));
                        $invDetMainID = $inv['idDet'];
                        $invMainId = $inv['idMain'];

                        //update invoice_main & invoice_detail
                        $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                        $invDet = $invoiceDetailDB->getData($invDetMainID);

                        $amountDN = $inv['rcp_inv_amount'];
                        if ($invDet['cur_id'] != 1) {
                            $dataCur = $curRateDB->getRateByDate($invCurrency,$receipt['rcp_receive_date']);
//                            $dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
//                            $dataCur = $curRateDB->getCurrentExchangeRate($invCurrency);
//                            exit;
                            $amountDN = round($inv['rcp_inv_amount'] / $dataCur['cr_exchange_rate'], 2);
//                            $amountDN = round($inv['rcp_inv_amount'],2);


                        }

                        $balanceNew1 = $invDet['balance'] - $amountDN;
                        $paidNew1 = $invDet['paid'] + $amountDN;
                        $dataUpdDetail = array('balance' => $balanceNew1, 'sp_id' => $invDetID, 'paid' => $paidNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetMainID));

                        //update amount sponsor at invoice_main

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invMainId);

                        $balanceNew = $invMain['bill_balance'] - $amountDN;
                        $paidNew = $invMain['bill_paid'] + $amountDN;

                        $dataUpdMain = array('bill_balance' => $balanceNew, 'bill_paid' => $paidNew, 'upd_by' => $creator, 'upd_date' => date('Y-m-d H:i:s'), 'tag_to_open_payment'=>0);
                        $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invMainId));

                    }
                }

                if ($totalSponsorAmount > 0 || $rcpStudent['rcp_adv_amount'] != 0) {

                    //generetae advance payment

                    if ($rcpStudent['rcp_adv_amount'] > 0) {
                        $total_advance_payment_amount = $rcpStudent['rcp_adv_amount'];
                    } else {
                        $total_advance_payment_amount = $totalSponsorAmount;
                    }

                    $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
                    $advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

                    $bill_no = $this->getBillSeq(8, date('Y'));

                    $advance_payment_data = array(
                        'advpy_fomulir' => $bill_no,
                        'advpy_appl_id' => 0,
                        'advpy_trans_id' => 0,
                        'advpy_idStudentRegistration' => $rcpStudent['IdStudentRegistration'],
                        'advpy_sem_id' => $semesterInfo['IdSemesterMaster'],
                        'advpy_rcp_id' => $receipt['rcp_id'],
                        'advpy_description' => 'Advance payment from receipt no:' . $receipt['rcp_no'],
                        'advpy_cur_id' => $receipt['rcp_cur_id'],
                        'advpy_amount' => $total_advance_payment_amount,
                        'advpy_total_paid' => 0.00,
                        'advpy_total_balance' => $total_advance_payment_amount,
                        'advpy_status' => 'A',
                        'advpy_date' => date('Y-m-d'),

                    );


                    $advPayID = $advancePaymentDb->insert($advance_payment_data);

                    $advance_payment_det_data = array(
                        'advpydet_advpy_id' => $advPayID,
                        'advpydet_total_paid' => 0.00
                    );

                    $advancePaymentDetailDb->insert($advance_payment_det_data);

                    $dataAdv = array('rcp_adv_amount' => $total_advance_payment_amount);
                    $db->update('sponsor_invoice_receipt_student', $dataAdv, $db->quoteInto("rcp_inv_id = ?", $rcpStudent['rcp_inv_id']));

                }
            }

        }

        /*knockoff end*/


        //change receipt status to APPROVE
        if ($receipt['rcp_status'] == 'ENTRY') {

            foreach ($receipt['receipt_student'] as $rcpStudent) {
                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['idMain'];
                        $invDetMainID = $inv['idDet'];

                        if ($invID) {
                            if ($balanceNew == 0) {
                                $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                                $paymentClass->updateCourseStatus($invID);

                            }
                        }
                    }
                }
            }

            $sponsorreceiptDb->update(
                array(
                    'rcp_status' => 'APPROVE',
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => $getUserIdentity->id
                ), 'rcp_id = ' . $receipt['rcp_id']);

            $this->gobjsessionsis->flash = array('type' => 'success', 'message' => 'Receipt Approved');
        } else {
            $this->gobjsessionsis->flash = array('type' => 'error', 'message' => 'Receipt Already Approved');
        }

        $this->_redirect($this->baseUrl . '/studentfinance/sponsor-invoice/view-receipt-detail/id/' . $id);
    }


    private function getBillSeq($type, $year)
    {

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }

    public function listPaymentAction()
    {

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->view->title = $this->view->translate('Sponsor Payment');

        $sponsorReceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();

        $dataStudent = $sponsorReceiptDb->getData();

//		echo "<pre>";
//		print_r($dataStudent);
        /*exit;
        $i=0;
        foreach($dataStudent as $data){
            $id = $data['id'] ;
            $discountTypeDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $dataStudentnumber = $discountTypeDb->getlistStudentByStatus($id);
            $dataStudent[$i]['number']= $dataStudentnumber;
            $i++;
        }
        */
        $this->view->paginator = $dataStudent;
    }

    public function listPaymentDetailAction()
    {

        //title
        $this->view->title = $this->view->translate("Sponsor Payment - Detail");

        $id = $this->_getParam('id', null);
        $this->view->id = $id;

        $sponsorReceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $dataStudent = $sponsorReceiptDb->getData($id);

//		echo "<pre>";
//		print_r($dataStudent[0]);

        $this->view->liststudent = $dataStudent[0];


    }

    public function cancelSponsorInvoiceAction(){
        $receiptid = $id = $this->_getParam('id', null);

        $model = new Registration_Model_DbTable_SeekingForLandscape();

        $sponsorinvreceipt = $model->getSponsorInvReceipt($receiptid);

        $sponsorinvreceiptinv = $model->sponsorInvReceiptInv($receiptid);

        //var_dump($sponsorinvreceipt);
        //var_dump($sponsorinvreceiptinv);
        //exit;

        if ($sponsorinvreceiptinv){
            foreach ($sponsorinvreceiptinv as $loop2){
                $sponsorInvDetail = $model->sponsorInvDetail($loop2['rcp_inv_sponsor_dtl_id']);

                $invoiceDetail = $model->invoiceDetail($sponsorInvDetail['sp_invoice_det_id']);

                $paidInvDetails = $invoiceDetail['paid']-$sponsorInvDetail['sp_paid'];
                $balanceInvDetails = $invoiceDetail['balance']+$sponsorInvDetail['sp_paid'];

                $invoiceDetailData = array(
                    'paid'=> $paidInvDetails,
                    'balance'=> $balanceInvDetails
                );
                $model->updateInvoiceDetail($invoiceDetailData, $invoiceDetail['id']);

                $invoiceMain = $model->invoiceMain($sponsorInvDetail['sp_invoice_id']);

                $paidInvMain = $invoiceMain['bill_paid']-$sponsorInvDetail['sp_paid'];
                $balanceInvMain = $invoiceMain['bill_balance']+$sponsorInvDetail['sp_paid'];

                $invoiceMainData = array(
                    'bill_paid'=> $paidInvMain,
                    'bill_balance'=> $balanceInvMain
                );
                $model->updateInvoiceMain($invoiceMainData, $invoiceMain['id']);

                $cancelSponsorData = array(
                    'sp_status'=>'X',
                    'sp_cancel_by'=>1,
                    'sp_cancel_date'=>date('Y-m-d H:i:s')
                );
                $model->updateSponsorMain($cancelSponsorData, $sponsorInvDetail['sp_id']);
            }
        }

        $cancelReceiptData = array(
            'rcp_status'=>'CANCEL',
            'cancel_by'=>1,
            'cancel_date'=>date('Y-m-d H:i:s')
        );
        $model->updateReceiptMain($cancelReceiptData, $sponsorinvreceipt['rcp_id']);

        $cancelAdvData = array(
            'advpy_status'=>'X',
            'advpy_cancel_date'=>date('Y-m-d H:i:s')
        );
        $model->updateAdvancePayment($cancelAdvData, $sponsorinvreceipt['rcp_id']);

        $this->gobjsessionsis->flash = array('type' => 'access', 'message' => 'Receipt Cancelled');
        $this->_redirect($this->baseUrl.'/studentfinance/sponsor-invoice/list-payment');
    }

    public function cancelDetailAction(){
        $id = $this->_getParam('id', null);
        $rcpid = $this->_getParam('rcpid', null);

        $model = new Studentfinance_Model_DbTable_SponsorReceipt();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $data = array(
            'sp_status'=>0,
            'sp_cancel_by'=>$getUserIdentity->id,
            'sp_cancel_date'=>date('Y-m-d H:i:s')
        );
        //var_dump($data); exit;
        $model->cancelSpmDtl($data, $id);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Sponsor Receipt Canceled'));
        $this->_redirect($this->baseUrl . '/studentfinance/sponsor-invoice/view-receipt-detail/id/' . $rcpid);
        exit;
    }
}

?>