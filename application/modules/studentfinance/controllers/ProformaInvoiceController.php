<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_ProformaInvoiceController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Application_Model_DbTable_ProformaInvoice();
		$this->_DbObj = $db;

		$this->promainDB = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
		$this->promaindetDB = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
		
		$this->fnsetObj();
		
	}
	
	public function fnsetObj(){
		$this->lobjsearchform = new App_Form_Search();
		$this->lobjscheme = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		//$this->lobjinvoiceentryForm = new Studentfinance_Form_Invoiceentry();
		$this->lobjcredittransferForm = new Records_Form_Credittransferapp();
		$this->lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		//$this->lobjinvoiceentryModel = new Studentfinance_Model_DbTable_Invoiceentry();
		$this->lobjCurrency = new Hostel_Model_DbTable_Hostelconfig();
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$this->lobjSubjectsetup = new Application_Model_DbTable_Subjectsetup();
		$this->lobjcodegenObj = new Cms_CodeGeneration();
		$this->lobjFeeCategory = new Studentfinance_Model_DbTable_Fee();
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Performa Invoice");
    	
    	//rector date
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('asd'=>'applicant_selection_detl'),array("DISTINCT DATE_FORMAT(asd.asd_decree_date, '%Y-%m-%d')rector_decree_date"))
				->joinLeft(array('aad'=>'applicant_assessment_date'),"DATE_FORMAT(aad.date, '%Y-%m-%d') = DATE_FORMAT(asd.asd_decree_date, '%Y-%m-%d')")
				->joinLeft(array('u'=>'tbl_user'),'u.iduser = aad.lock_by',array('name'=>'fName'))
				->order('asd.asd_decree_date desc');
							
		$row = $db->fetchAll($select);
		if(!$row){
			$row = null;
		}
		
		$this->view->dateList = $row;
		
		//get decree nomor
		$db2 = Zend_Db_Table::getDefaultAdapter();
		
		//PSSB
		$selectPSSB = $db->select()
				->from(array('asd'=>'applicant_selection_detl'),array("nomor"=>"asd.asd_nomor","selection_id"=>"asd.asd_id", "decree_date"=>"asd.asd_decree_date", new Zend_Db_Expr ('"PSSB" AS type')))
				->where('asd.asd_type = 2')
				->group('asd.asd_nomor');
				//->order('asd.asd_decree_date desc');
		

		//USM
		$selectUSM = $db->select()
				->from(array('aaud'=>'applicant_assessment_usm_detl'),array("nomor"=>"aaud.aaud_nomor","selection_id"=>"aaud.aaud_id", "decree_date"=>"aaud.aaud_decree_date", new Zend_Db_Expr ('"USM" AS type')))
				->where('aaud.aaud_type = 2')
				->group('aaud.aaud_nomor');
				//->order('aaud.aaud_decree_date desc');

		
		//join query
		$selectALL = $db->select()
     				->union(array($selectPSSB, $selectUSM))
     				->order('decree_date desc');
     				 
		$row2 = $db->fetchAll($selectALL);
		
		//get applicant count for each nomor
		if(!$row2){
			$row2 = null;
		}else{
			foreach ($row2 as $key=>$value) {
				
				if(!isset($row2[$key]['bil'])){
					$row2[$key]['bil'] = 0;
				}
				
				//PSSB
				if($value['type']=='PSSB'){
					
					//get selection_id for particular decree nomor
					$bil_applicant = $db->select()
							->from(array('aa'=>'applicant_assessment'),array())
							->join(array('asd'=>'applicant_selection_detl'), 'aa.aar_rector_selectionid = asd.asd_id',array())
							->join(array('at'=>'applicant_transaction'),'aa.aar_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)bil') )
							->where("asd.asd_nomor = '".$value['nomor']."'")
							->where("at.at_status not in ('APPLY','CLOSE','PROCESS','REJECT')")
						    ->where("at.at_selection_status = 3");

					$row3 = $db->fetchAll($bil_applicant);
					
					$row2[$key]['bil'] = count($row3);
					
				}else//USM
				if($value['type']=='USM'){
					
					//get selection_id for particular decree nomor
					$bil_applicant2 = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)bil') )
							->where("aaud.aaud_nomor = '".$value['nomor']."'")
							->where("at.at_status not in ('APPLY','CLOSE','PROCESS','REJECT')")
						    ->where("at.at_selection_status = 3");
							
							
					$row3 = $db->fetchAll($bil_applicant2);
							
					$row2[$key]['bil'] = count($row3);
					
				}
			}
		}
		
		
		
		
		//remove nomur with 0 applicant bil
		foreach ($row2 as $key=>$nomor){
			if($nomor['bil']==0){
				unset($row2[$key]);
			}
		}
		$row2 = array_values($row2);
		
		$this->view->decreeList = $row2;
				    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
				
			$this->getPerformaInvoiceByDecree($formData['nomor'],$formData['type']);
			
						
	        exit();
    	}
    	    	
	}
	
	private function getPerformaInvoice($date){
		$proformaDb = new Application_Model_DbTable_ProformaInvoice();
			$data_arr = $proformaDb->getDataDate($date);
						
			
			/*$output ='';
			$output .="BILLING_NO,PAYEE_ID,BILL_FIRST_NAME,ADDRESS_1,BILL_REF_1,BILL_REF_2,BILL_REF_3,BILL_REF_4,BILL_REF_5,AMOUNT_TOTAL,AMOUNT_1,AMOUNT_2,AMOUNT_3,AMOUNT_4,AMOUNT_5,AMOUNT_6,AMOUNT_7,AMOUNT_8,AMOUNT_9,AMOUNT_10,AUTODEBET_ACC_D,REGISTER_NO,DUE_DATE"."\n";
			
			if($data_arr){
				foreach ($data_arr as $data){
					$output .=$data['billing_no'].",".$data['payee_id'].",".$data['name'].",".$data['address'].",".$data['ref1'].",".$data['ref2'].",".$data['ref3'].",".$data['ref4'].",".$data['ref5'].",".$data['amount_total'].",".$data['amount1'].",".$data['amount2'].",".$data['amount3'].",".$data['amount4'].",".$data['amount5'].",".$data['amount6'].",".$data['amount7'].",".$data['amount8'].",".$data['amount9'].",".$data['amount10'].",,".$data['register_no'].",,"."\n";	
				}
			}*/
			
			$filename = "SPCUTS_".date("Ymd",strtotime($data_arr[0]['offer_date']));

			$file = APPLICATION_PATH . "/tmp/" . $filename . ".csv";
 
	        $realPath = realpath( $file );
	 
	        if ( false === $realPath )
	        {
	            touch( $file );
	            chmod( $file, 0777 );
	        }
	 
	        $file = realpath( $file );
	        $handle = fopen( $file, "w" );
	        
	        $finalData[] = array('BILLING_NO','PAYEE_ID','BILL_FIRST_NAME','ADDRESS_1','BILL_REF_1','BILL_REF_2','BILL_REF_3','BILL_REF_4','BILL_REF_5','AMOUNT_TOTAL','AMOUNT_1','AMOUNT_2','AMOUNT_3','AMOUNT_4','AMOUNT_5','AMOUNT_6','AMOUNT_7','AMOUNT_8','AMOUNT_9','AMOUNT_10','AUTODEBET_ACC_D','REGISTER_NO','DUE_DATE');
	        
	        $total_amount = 0;
	        if($data_arr){
		        foreach ( $data_arr AS $key=>$row )
		        {
		        	$l = strlen($row["billing_no"]);
		        	
		            $finalData[] = array(
		            	
		                utf8_decode( chr(0).$row["billing_no"]) , // For chars with accents.
		                utf8_decode( $row["payee_id"] ),
		                utf8_decode( $row["name"] ),
		                utf8_decode( $row["address"] ),
		                utf8_decode( $row["ref1"] ),
		                utf8_decode( $row["ref2"] ),
		                utf8_decode( $row["ref3"] ),
		                utf8_decode( $row["ref4"] ),
		                utf8_decode( $row["ref5"] ),
		                utf8_decode( $row["amount_total"] ),
		                utf8_decode( $row["amount1"] ),
		                utf8_decode( $row["amount2"] ),
		                utf8_decode( $row["amount3"] ),
		                utf8_decode( $row["amount4"] ),
		                utf8_decode( $row["amount5"] ),
		                utf8_decode( $row["amount6"] ),
		                utf8_decode( $row["amount7"] ),
		                utf8_decode( $row["amount8"] ),
		                utf8_decode( $row["amount9"] ),
		                utf8_decode( $row["amount10"] ),
		                utf8_decode( ''),
		                utf8_decode( $row["register_no"] ),
		                utf8_decode( '' )
		            );
		            
		        	if($row["ref5"] == 'Paket A Lunas' ){
		            	$total_amount = $total_amount + $row["amount_total"];
		            }
		        }
	        }
	 
			if (PHP_EOL == "\r\n"){
			    $eol = "\n";
			}else{
			    $eol = "\r\n";
			}

	        foreach ( $finalData AS $finalRow )
	        {
	            
	            fwrite( $handle, implode(",", $finalRow).$eol);
	            
	        }
	 
	        fclose( $handle );
	 
	        $this->_helper->layout->disableLayout();
	        $this->_helper->viewRenderer->setNoRender();
	 
	         $this->getResponse()->setRawHeader( "Content-Type: text/csv" )
	            ->setRawHeader( "Content-Disposition: attachment; filename=".$filename."_".sizeof($data_arr)."_".$total_amount.".csv" )
	            ->setRawHeader( "Content-Transfer-Encoding: binary" )
	            ->setRawHeader( "Expires: 0" )
	            ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
	            ->setRawHeader( "Pragma: public" )
	            ->setRawHeader( "Content-Length: " . filesize( $file ) )
	            ->sendResponse();
	 
	        readfile( $file ); 
	        unlink($file);;
	}
	
	private function getPerformaInvoiceByDate($from,$to){
		
			$proformaDb = new Application_Model_DbTable_ProformaInvoice();
			$data_arr = $proformaDb->getDataRange($from,$to);
			
			
			$filename = "SPCUTS_".date("Ymd",strtotime($data_arr[0]['offer_date']));

			$file = APPLICATION_PATH . "/tmp/" . $filename . ".csv";
 
	        $realPath = realpath( $file );
	 
	        if ( false === $realPath )
	        {
	            touch( $file );
	            chmod( $file, 0777 );
	        }
	 
	        $file = realpath( $file );
	        $handle = fopen( $file, "w" );
	        
	        $finalData[] = array('BILLING_NO','PAYEE_ID','BILL_FIRST_NAME','ADDRESS_1','BILL_REF_1','BILL_REF_2','BILL_REF_3','BILL_REF_4','BILL_REF_5','AMOUNT_TOTAL','AMOUNT_1','AMOUNT_2','AMOUNT_3','AMOUNT_4','AMOUNT_5','AMOUNT_6','AMOUNT_7','AMOUNT_8','AMOUNT_9','AMOUNT_10','AUTODEBET_ACC_D','REGISTER_NO','DUE_DATE');
	        
	        $total_amount = 0;
	        if($data_arr){
		        foreach ( $data_arr AS $key=>$row )
		        {
		        	$l = strlen($row["billing_no"]);
		        	
		            $finalData[] = array(
		            	
		                utf8_decode( chr(0).$row["billing_no"]) , // For chars with accents.
		                utf8_decode( $row["payee_id"] ),
		                utf8_decode( $row["name"] ),
		                utf8_decode( $row["address"] ),
		                utf8_decode( $row["ref1"] ),
		                utf8_decode( $row["ref2"] ),
		                utf8_decode( $row["ref3"] ),
		                utf8_decode( $row["ref4"] ),
		                utf8_decode( $row["ref5"] ),
		                utf8_decode( $row["amount_total"] ),
		                utf8_decode( $row["amount1"] ),
		                utf8_decode( $row["amount2"] ),
		                utf8_decode( $row["amount3"] ),
		                utf8_decode( $row["amount4"] ),
		                utf8_decode( $row["amount5"] ),
		                utf8_decode( $row["amount6"] ),
		                utf8_decode( $row["amount7"] ),
		                utf8_decode( $row["amount8"] ),
		                utf8_decode( $row["amount9"] ),
		                utf8_decode( $row["amount10"] ),
		                utf8_decode( ''),
		                utf8_decode( $row["register_no"] ),
		                utf8_decode( '' )
		            );
		            
		        	if($row["ref5"] == 'Paket A Lunas' ){
		            	$total_amount = $total_amount + $row["amount_total"];
		            }
		        }
	        }
	 
			if (PHP_EOL == "\r\n"){
			    $eol = "\n";
			}else{
			    $eol = "\r\n";
			}

	        foreach ( $finalData AS $finalRow )
	        {
	            
	            fwrite( $handle, implode(",", $finalRow).$eol);
	            
	        }
	 
	        fclose( $handle );
	 
	        $this->_helper->layout->disableLayout();
	        $this->_helper->viewRenderer->setNoRender();
	 
	         $this->getResponse()->setRawHeader( "Content-Type: text/csv" )
	            ->setRawHeader( "Content-Disposition: attachment; filename=".$filename."_".sizeof($data_arr)."_".$total_amount.".csv" )
	            ->setRawHeader( "Content-Transfer-Encoding: binary" )
	            ->setRawHeader( "Expires: 0" )
	            ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
	            ->setRawHeader( "Pragma: public" )
	            ->setRawHeader( "Content-Length: " . filesize( $file ) )
	            ->sendResponse();
	 
	        readfile( $file ); 
	        unlink($file);
	}
	
	private function getPerformaInvoiceByDecree($nomor, $type){
		
			$proformaDb = new Application_Model_DbTable_ProformaInvoice();
			$data_arr = $proformaDb->getDataDecree($nomor,$type);
			
			
			$filename = "SPCUTS_".date("Ymd",strtotime($data_arr[0]['offer_date']));

			$file = APPLICATION_PATH . "/tmp/" . $filename . ".csv";
 
	        $realPath = realpath( $file );
	 
	        if ( false === $realPath )
	        {
	            touch( $file );
	            chmod( $file, 0777 );
	        }
	 
	        $file = realpath( $file );
	        $handle = fopen( $file, "w" );
	        
	        $finalData[] = array('BILLING_NO','PAYEE_ID','BILL_FIRST_NAME','ADDRESS_1','BILL_REF_1','BILL_REF_2','BILL_REF_3','BILL_REF_4','BILL_REF_5','AMOUNT_TOTAL','AMOUNT_1','AMOUNT_2','AMOUNT_3','AMOUNT_4','AMOUNT_5','AMOUNT_6','AMOUNT_7','AMOUNT_8','AMOUNT_9','AMOUNT_10','AUTODEBET_ACC_D','REGISTER_NO','DUE_DATE');
	        
	        $total_amount = 0;
	        if($data_arr){
		        foreach ( $data_arr AS $key=>$row )
		        {
		        	$l = strlen($row["billing_no"]);
		        	
		            $finalData[] = array(
		            	
		                $row["billing_no"] , // For chars with accents.
		                $row["payee_id"] ,
		                $row["name"] ,
		                $row["address"] ,
		                $row["ref1"] ,
		                $row["ref2"] ,
		                $row["ref3"] ,
		                $row["ref4"] ,
		                $row["ref5"] ,
		                $row["amount_total"] ,
		                $row["amount1"] ,
		                $row["amount2"] ,
		                $row["amount3"] ,
		                $row["amount4"] ,
		                $row["amount5"] ,
		                $row["amount6"] ,
		                $row["amount7"] ,
		                $row["amount8"] ,
		                $row["amount9"] ,
		                $row["amount10"] ,
		                '',
		                $row["register_no"] ,
		                '' 
		            );
		            
		            //if($row["ref5"] == 'Paket A Lunas' ){
		            	$total_amount = $total_amount + $row["amount_total"];
		            //}
		        }
	        }
	 
			if (PHP_EOL == "\r\n"){
			    $eol = "\n";
			}else{
			    $eol = "\r\n";
			}

	        foreach ( $finalData AS $finalRow )
	        {
	            
	            fwrite( $handle, implode(",", $finalRow).$eol);
	            
	        }
	        
	       fclose( $handle );
	 
	        $this->_helper->layout->disableLayout();
	        $this->_helper->viewRenderer->setNoRender();
	 
	        $this->getResponse()->setRawHeader( "Content-Type: application/csv" )
	            ->setRawHeader( "Content-Disposition: attachment; filename=".$filename."_".sizeof($data_arr)."_".$total_amount.".csv" )
	            ->setRawHeader( "Content-Transfer-Encoding: binary" )
	            ->setRawHeader( "Expires: 0" )
	            ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
	            ->setRawHeader( "Pragma: public" )
	            ->setRawHeader( "Content-Length: " . filesize( $file ) )
	            ->sendResponse();
	 
	        readfile( $file ); 
	        unlink($file);
	}
	
	public function applicantListAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Applicant Proforma Invoice - Student List");
    	
    	if ($this->getRequest()->isPost()) {	
			$formData = $this->getRequest()->getPost();
			$this->view->formData = $formData;
						
    		$proformaDb = new Application_Model_DbTable_ProformaInvoice();
			$applicantList = $proformaDb->getDecreeApplicantList($formData['nomor'],$formData['type']);
    	}else{
    		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'proforma-invoice', 'action'=>'index'),'default',true));
    	}
			
    	
    	foreach($applicantList as $index=>$data){
    		
    		//get offered program
    		$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
    		if($formData['type']=="USM"){
    			$applicantList[$index]['program_offered'] = $applicantProgramDb->getOfferProgram($data['at_trans_id'],1);
    		}else
    		if($formData['type']=="PSSB"){
    			$applicantList[$index]['program_offered'] = $applicantProgramDb->getOfferProgram($data['at_trans_id'],2);
    		}

    		//get proforma program code
    		$applicantList[$index]['proforma'] = $proformaDb->getTxnData($data['at_trans_id']);
    	}
    	
    	$this->view->applicantList = $applicantList;
    	
    	/*echo "<pre>";
    	print_r($applicantList);
    	echo "</pre>";*/
    }
    
	public function applicantDetailAction(){
    	
    	//title
    	$this->view->title= $this->view->translate("Applicant Proforma Invoice - Applicant Detail");
    	
    	if ($this->getRequest()->isPost()) {
    			
			$formData = $this->getRequest()->getPost();
			$this->view->formData = $formData;

			
			
	    	//invoices
	    	$proformaDb = new Application_Model_DbTable_ProformaInvoice();
	    	$invoices = $proformaDb->getTxnData($formData['txn_id']);
	    	$this->view->invoices = $invoices;
	    	
	    	//transaction data
	    	
	    	//program data
	    	$applicantProgram = new App_Model_Application_DbTable_ApplicantProgram();
	    	$program = $applicantProgram->getProgram($formData['txn_id']);
	    		    	
	    	if($formData['selection_type']=="PSSB"){
	    		$this->view->program = $program[0]['program_name_indonesia'];
	    		
	    		//selection data
	    		$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
	    		$assessment_data = $assessmentDb->getData($formData['txn_id']);
	    		
	    		//data for regenerate
				$data = array(
					'selection_type' => 'PSSB',
					'txn_id' => $formData['txn_id'],
					'selection_nomor' => $assessment_data['asd_nomor']
				);
				$this->view->data = $data;
			
	    	}else
    		if($formData['selection_type']=="USM"){
	    		for($i=0; $i<sizeof($program); $i++){
	    			if($program[$i]['ap_usm_status']==1){
	    				$this->view->program = $program[$i]['program_name_indonesia'];		
	    			}
	    		}
	    		
	    		//selection data
	    		$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
	    		$assessment_data = $assessmentDb->getData($formData['txn_id']);
	    		
	    		//data for regenerate
				$data = array(
					'selection_type' => 'USM',
					'txn_id' => $formData['txn_id'],
					'selection_nomor' => $assessment_data['aaud_nomor']
				);
				$this->view->data = $data;
	    	}
	    		    	  		    	
	    	//get profile
	    	$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db ->select()
						  ->from(array('at'=>'applicant_transaction'))
						  ->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
						  ->where("at_trans_id = ".$formData['txn_id']);
						  
			$row = $db->fetchRow($select);
							  
	    	//nationality
	    	if( isset($row['appl_nationality']) ){
				$countryDb = new App_Model_General_DbTable_Country();
				
				$row['nationality'] = $countryDb->getData($row['appl_nationality']);
				
			}else{
				$row['nationality'] = null;	
			}
		
			$this->view->profileData = $row;
    	}else{
    		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'proforma-invoice', 'action'=>'index'),'default',true));
    	}
		
    }
    
	public function regenerateInvoiceAction(){
    	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
    		
    		
    		//regenerate for given txn id
	    	if( isset($formData['txn_id']) && $formData['txn_id']!=null && $formData['txn_id']!=""){
	    		
				//get assessment data
				if($formData['selection_type']=="PSSB"){
					
					//regenerate performa invoice
					$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
					if(!$proformaInvoiceDb->regenerateProformaInvoice($formData['txn_id']) ){
						$errorMsg = $this->view->noticeError;
						$this->view->noticeError = $errorMsg." ".$this->view->translate("Proforma Invoice not created. Please inform Admin");
					}
				
					
					
					$param = array(
								'selection_type'=>'PSSB',
								'txn_id'=>$formData['txn_id'],
								'selection_nomor' => $formData['nomor']
							 );
					
					$this->view->detailPSSB = $param;
					
					//$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
					//$assessmentData = $assessmentDb->getData($formData['txn_id']);
					//$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'proforma-invoice', 'action'=>'applicant-detail','selection_type'=>'PSSB' ,'selection_id'=>$assessmentData['asd_id'],'txn_id'=>$formData['txn_id']),'default',true));
				}else
				if($formData['selection_type']=="USM"){
					
					//regenerate performa invoice
					$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
					if(!$proformaInvoiceDb->regenerateUSMProformaInvoice($formData['txn_id']) ){
						$errorMsg = $this->view->noticeError;
						$this->view->noticeError = $errorMsg." ".$this->view->translate("Proforma Invoice not created. Please inform Admin");
					}
									
					
					$param = array(
								'selection_type'=>'USM',
								'txn_id'=>$formData['txn_id'],
								'selection_nomor' => $formData['nomor']
							 );
					
					$this->view->detailUSM = $param;
					
					//$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
					//$assessmentData = $assessmentDb->getData($formData['txn_id']);
					//$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'proforma-invoice', 'action'=>'applicant-detail','selection_type'=>'USM','selection_id'=>$assessmentData['aaud_id'],'txn_id'=>$formData['txn_id']),'default',true));
				}
				    		
	    		
	    	}else//regenerate for all having decree nomor
	    	if(isset($formData['selection_nomor']) && $formData['selection_nomor']!=null && $formData['selection_nomor']!="" ){
	    		
	    		if($formData['selection_type']=="PSSB"){
		    		//applicant list
		    		$proformaDb = new Application_Model_DbTable_ProformaInvoice();
					$applicantList = $proformaDb->getDecreeApplicantList($formData['selection_nomor'],"PSSB");
					
					$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
					foreach ($applicantList as $applicant){
						//regenerate performa invoice
			    		$proformaInvoiceDb->regenerateProformaInvoice($applicant['at_trans_id']); 
					}
					
					$param = array(
								'type'=>'PSSB',
								'nomor' => $formData['selection_nomor']
							 );
					
					$this->view->selectionPSSB = $param;
										
		    		//$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'proforma-invoice', 'action'=>'applicant-list','selection_type'=>'PSSB','selection_id'=>$selection_id),'default',true));
	    		}else
	    		if($formData['selection_type']=="USM"){
	    			//applicant list
		    		$proformaDb = new Application_Model_DbTable_ProformaInvoice();
					$applicantList = $proformaDb->getDecreeApplicantList($formData['selection_nomor'],"USM");
					
					$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
					foreach ($applicantList as $applicant){
						//regenerate performa invoice
			    		$proformaInvoiceDb->regenerateUSMProformaInvoice($applicant['at_trans_id']); 
					}
					
					$param = array(
								'type'=>'USM',
								'nomor' => $formData['selection_nomor']
							 );
					
					$this->view->selectionUSM = $param;
					
		    		//$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'proforma-invoice', 'action'=>'applicant-list','selection_type'=>'USM','selection_id'=>$selection_id),'default',true));
	    		}
	    		
	    	}
    	}
    }
    
	/*
     * List of applicant or student
     */
    public function applicantAction()
	{
    	$this->view->title = $this->view->translate('Proforma Invoice');
    	
    	//get fee category
    	$feeCategoryDB = new Studentfinance_Model_DbTable_FeeCategory();
    	$this->view->fee_category = $feeCategoryDB->getListCategory();
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$p_data = $this->promainDB->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
	
	    	$this->view->paginator = $paginator;
			$this->view->formData = $formData;

		}
		
		/*else
		{
			$formData['type'] = 1;
			$p_data = $this->promainDB->getPaginateData(array('type'=>1));
    	}*/

		
    }

	public function viewAction()
	{
		$this->view->title = $this->view->translate('Proforma Invoice Details');

		$id = $this->_getParam('id');

		$info = $this->promainDB->getData($id);
		if ( empty($info) )
		{
			throw new Exception('Invalid Invoice ID');
		}

		$subDb = new Studentfinance_Model_DbTable_ProformaInvoiceSubject();
		$details = $this->promaindetDB->getDetail($info['id']);
		
		foreach ($details as $key => $det)
		{
			$check = $subDb->getData($det['proforma_invoice_main_id'], $det['id']);
			if ( !empty($check) )
			{
				$details[$key]['subjects'] = $check;
			}
		}
		


		$this->view->info = $info;
		$this->view->details = $details;
	}
    
    /*
     * Display applicant detail proforma invoice
     */
    public function detailAction(){
    	
    	//title
    	$this->view->title= $this->view->translate("Applicant Proforma Invoice - Application Detail");
    	
    	$txn_id = $this->_getParam('txn', null);
    	$appl_type = $this->_getParam('type', null);
    	
    	//proforma invoices
    	$proformaDb = new Application_Model_DbTable_ProformaInvoice();
    	$invoices = $proformaDb->getTxnData($txn_id);
    	$this->view->invoices = $invoices;
	    	
	    //transaction data
	    
    	
    	//program data
    	$applicantProgram = new App_Model_Application_DbTable_ApplicantProgram();
    	$program = $applicantProgram->getProgram($txn_id);
	    		    	
    	if($appl_type=="2"){
    		$this->view->program = $program[0]['program_name_indonesia'];

    		//program offer data
    		$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
    		$program_offered = $applicantProgramDb->getOfferProgram($txn_id,2);
    		
    		//selection data
    		$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
    		$assessment_data = $assessmentDb->getData($txn_id);
    		
    		//data for regenerate
			$data = array(
				'selection_type' => 'PSSB',
				'txn_id' => $txn_id,
				'selection_nomor' => $assessment_data['asd_nomor'],
				'rank' => $assessment_data['aar_rating_rector']
			);
			$this->view->data = $data;
		
    	}else
    	if($appl_type=="1"){
	    	for($i=0; $i<sizeof($program); $i++){
	    		if($program[$i]['ap_usm_status']==1){
	    			$this->view->program = $program[$i]['program_name_indonesia'];		
	    		}
	    	}
	    	
	    	//program offer data
    		$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
    		$program_offered = $applicantProgramDb->getOfferProgram($txn_id,1);
    		
    		//selection data
    		$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
    		$assessment_data = $assessmentDb->getData($txn_id);
	    	
	    	//data for regenerate
			$data = array(
				'selection_type' => 'USM',
				'txn_id' => $txn_id,
				'selection_nomor' => $assessment_data['aaud_nomor'],
				'rank' => $assessment_data['aau_rector_ranking']
			);
			$this->view->data = $data;
	    }
	    	    	  		    	
	    //get profile
	    $db = Zend_Db_Table::getDefaultAdapter();
		$select = $db ->select()
					  ->from(array('at'=>'applicant_transaction'))
					  ->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
					  ->where("at_trans_id = ".$txn_id);
						  
		$row = $db->fetchRow($select);
		
		//nationality
    	if( isset($row['appl_nationality']) ){
			$countryDb = new App_Model_General_DbTable_Country();
			
			if($row['appl_nationality']==1){
				$row['nationality'] = $countryDb->getData(96);//default 96 in tbl_countries
			}else{	
				
				$row['nationality'] = $countryDb->getData($row['appl_nationality']);
			}
		}else{
			$row['nationality'] = "Indonesia";	
		}
		
		$this->view->profileData = $row;
    }
    
	public function addoldAction(){
		$this->view->title = $this->view->translate('Proforma Invoice - Entry');
		
		$lobjdeftype = new App_Model_Definitiontype();
		$payee_type_list = $lobjdeftype->fnGetDefinationsByLocale('Payee Type');
		$this->view->payee_type_list = $payee_type_list;
		
		//payment group
		$payment_group_list = $lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');
		$this->view->payment_group = $payment_group_list;
		
		//mode of payment
		$paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
		$mode_of_payment_list = $paymentModeDb->getPaymentModeList();
		$this->view->mode_of_payment = $mode_of_payment_list;
		
		//currency
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$currency_list = $currencyDb->fetchAll('cur_status = 1');
		$this->view->currency_list = $currency_list;
		
		//credit card terminal
		$creditCardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
		$creditCardTerminal_list = $creditCardTerminalDb->getTerminalList();
		$this->view->terminal_list = $creditCardTerminal_list;
		
		//bank from account code
		$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		$bankAccountCodeList = $accountCodeDb->getAccountCode(1,614);//hardcoded to bank acc code
		$this->view->bank_list = $bankAccountCodeList;
		
		//non-invoice
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$non_invoice_list = $feeItemDb->getActiveNonInvoiceFeeItem();
		$this->view->non_invoice_list = $non_invoice_list;
		
		//receipt seq
		$this->view->receipt_no = $this->getCurrentProformaNoSeq();
				
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
			
//			echo "<pre>";
//			print_r($formData);
//			exit;

			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			
			$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			
			try {
				
				//get appl_id from transaction id or idStudentregistration
				$payee = array();
				if($formData['receipt_payee_type'] == 645){
					$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
					$txnData = $txnDb->fetchRow( array('at_pes_id = ?'=> $formData['receipt_payee_id']) );
					
					$txnProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
					$txnProfile= $txnProfileDb->fetchRow( array('appl_id = ?'=> $txnData['at_appl_id']) );
					
					$txnProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
					$txnProgram = $txnProgramDb->fetchRow( array('ap_at_trans_id = ?'=> $txnData['at_trans_id']) );
		
					if($txnData){
						$txnData = $txnData->toArray();
						
						$payee['appl_id'] = $txnData['at_appl_id'];
						$payee['txn_id'] = $txnData['at_trans_id'];
						$payee['idStudentRegistration'] = null;
					}
					
					
					
				}else
				if($formData['receipt_payee_type'] == 646){
					//TODO: get student info
				}
				
				
				if($payee == null){
					throw new Exception('Payee record not found');
				}
				
				
				//calculate total payment amount
				$total_payment_amount = $formData['total_payment'];
				//foreach ($formData['payment'] as $payment){
//					$total_payment_amount += $payment['amount'];
				//}
				
				//calculate total invoice amount
				$total_invoice_amount = 0;
				if(isset($formData['invoice'])){
					foreach ($formData['invoice'] as $invoice){
						$total_invoice_amount += $invoice['amount'];
					}
				}
								
				//calculate total non-invoice amount
				$total_non_invoice_amount = 0;
				if(isset($formData['non_invoice'])){
					foreach ($formData['non_invoice'] as $non_invoice){
						$total_non_invoice_amount += $non_invoice['amount'];
					}
				}
				
				//calculate advance payment amount
				$total_advance_payment_amount = 0;
				if( $total_payment_amount > ($total_invoice_amount+$total_non_invoice_amount) ){
					$total_advance_payment_amount = $total_payment_amount - ($total_invoice_amount+$total_non_invoice_amount);
				}
				
				//get currency from code
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currency = $currencyDb->getCurrencyFromCode($formData['receipt_currency']);
				if(!$currency){
					throw new Exception('Currency not found');
				}
				
				//add receipt
				$receiptDb = new Studentfinance_Model_DbTable_Receipt();
				$receipt_no = $this->getReceiptNoSeq();
				
				$receipt_data = array(
					'rcp_no' => $receipt_no,
					'rcp_account_code' => $formData['receipt_account_code'],	
					'rcp_date' => date('Y-m-d', strtotime($formData['receipt_date'])),
					'rcp_receive_date' => date('Y-m-d', strtotime($formData['receipt_receive_date'])),
					'rcp_payee_type' => $formData['receipt_payee_type'],
					'rcp_appl_id' => $payee['appl_id'],
					'rcp_trans_id' => $payee['txn_id'],
					'rcp_idStudentRegistration' => $payee['idStudentRegistration'],
					'rcp_description' => $formData['receipt_description'],
					'rcp_amount' => $total_payment_amount,
					'rcp_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $currency['cur_id']),
					'rcp_adv_payment_amt' => $total_advance_payment_amount,	
					'rcp_cur_id' => $currency['cur_id']
				);
				$receipt_id = $receiptDb->insert($receipt_data);
				
				
			
				//add payment (loop)
				$paymentDb = new Studentfinance_Model_DbTable_Payment();
				foreach ($formData['payment'] as $payment){
					$payment_data = array(
						'p_rcp_id' => $receipt_id,
						'p_payment_mode_id' => $payment['mode'],
						'p_cheque_no' => isset($payment['cheque_no'])?$payment['cheque_no']:null,
						'p_doc_bank' => isset($payment['payment_doc_bank'])?$payment['payment_doc_bank']:null,
						'p_doc_branch' => isset($payment['payment_doc_branch'])?$payment['payment_doc_branch']:null,
						'p_terminal_id' => isset($payment['terminal_id'])?$payment['terminal_id']:null,
						'p_card_no' => isset($payment['card_no'])?$payment['card_no']:null,
						'p_cur_id' => $payment['currency'],
						'p_amount' => $total_payment_amount,
						'p_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $payment['currency'])
					);
					
					$paymentDb->insert($payment_data);
				}
				
				$currencyPaid = $formData['receipt_currency'];
				
				//add non-invoice (loop)
				if( isset($formData['non_invoice']) ){
					$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
					
					foreach ($formData['non_invoice'] as $non_invoice){
						$non_invoice_data = array(
							'ni_fi_id' => $non_invoice['fi_id'],
							'ni_cur_id' => $non_invoice['currency'],
							'ni_amount' => $non_invoice['amount'],
							'ni_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($non_invoice['amount'],$non_invoice['currency']),
							'ni_rcp_id' => $receipt_id,
						);	
						
						$nonInvoiceDb->insert($non_invoice_data);
					}
				}
				
				//update invoice (loop)
				if(isset($formData['invoice'])){
					
					$amount_for_invoice = $total_payment_amount - $total_non_invoice_amount;
					$invoice_list = array();
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					
					$a = 0;
					foreach ($formData['invoice'] as $invoice){
						$invoice_row = $invoiceMainDb->fetchRow(array('id = ?'=>$invoice['invoice_id']));
						
						$invoicedetail_row = $invoiceDetailDb->fetchRow(array('invoice_main_id = ?'=>$invoice['invoice_id']));
						
						if($invoice_row){
							$invoice_list[$a] = $invoice_row->toArray();
							$invoice_list[$a]['details'] = $invoicedetail_row->toArray();
					
						}
						
						$a++;
					}
					
					
					$i = 0;
					while ($amount_for_invoice > 0 && $i < sizeof($invoice_list) ) {
						
						$amount_to_use = 0.00;
						$balance = $invoice_list[$i]['bill_amount'] - ($invoice_list[$i]['bill_paid'] + $amount_to_use);
						
						
						if($invoice_list[$i]['bill_balance'] < $amount_for_invoice){
							$amount_to_use = $invoice_list[$i]['bill_balance'];
						}else{
							$amount_to_use = $amount_for_invoice;
						}
						
						$bill_paid =  $invoice_list[$i]['bill_paid'] + $amount_to_use;
						
						
						$upd_data_invoice = array(
							'bill_paid' => $invoice_list[$i]['bill_paid'] + $amount_to_use,
							'bill_balance' => $invoice_list[$i]['bill_amount'] - ($invoice_list[$i]['bill_paid'] + $amount_to_use)
						);

						$invoiceMainDb->update($upd_data_invoice, array('id = ?' => $invoice_list[$i]['id']) );
						
						
						$feeID = $invoice_list[$i]['details']['fi_id'];
						
						
						
						//update invoic detail
						$upd_data_invoice_detail = array(
							'paid' => $invoice_list[$i]['bill_paid'] + $amount_to_use,
							'balance' => $invoice_list[$i]['bill_amount'] - ($invoice_list[$i]['bill_paid'] + $amount_to_use)
						);

						$invoiceDetailDb->update($upd_data_invoice_detail, array('invoice_main_id = ?' => $invoice_list[$i]['id']) );
						
						
						$amount_for_invoice -= $amount_to_use;

						$i++;
					}
					
					//add receipt-invoice detail
					$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
					
					foreach ($formData['invoice_fee_item'] as $invoice_id => $fee_item){
						
						$data_rcp_inv = array(
							'rcp_inv_rcp_id' => $receipt_id,
							'rcp_inv_rcp_no' => $receipt_no,
							'rcp_inv_invoice_id' => $invoice_id,
							'rcp_inv_invoice_dtl_id' => $fee_item['invoice_fi_id'],
							'rcp_inv_amount' => $fee_item['invoice_fi_amount'],
							'rcp_inv_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($fee_item['invoice_fi_amount'], $fee_item['invoice_fi_currency']),
							'rcp_inv_cur_id' => $fee_item['invoice_fi_currency']
						);
						$receiptInvoiceDb->insert($data_rcp_inv);

						//update invoice-subject (if any)
						if( isset($fee_item['subject']) ){
							
							$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
							foreach ($fee_item['subject'] as $subject){
								$data_invoice_subject_upd = array(
									'rcp_id' => $receipt_id,
									'rcp_no' => $receipt_no,	
									'paid' => $subject['amount'],
									'balance' => $subject['amount']
								);
								
								$where_inv_sub = array(
									'invoice_main_id = ? ' => $invoice_id,
									'invoice_detail_id = ?' => $fee_item['invoice_fi_id'],
									'subject_id = ?' => $subject['id']
								);
								
								$invoiceSubjectDb->update($data_invoice_subject_upd, $where_inv_sub);
							}
							
						}
					}
					
					
				}
				
				//add advance payment (if any)
				if($total_advance_payment_amount > 0){
					$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advance_payment_data = array(
						'advpy_appl_id' => $payee['appl_id'],
						'advpy_trans_id' => $payee['txn_id'],
						'advpy_idStudentRegistration' => $payee['idStudentRegistration'],
						'advpy_rcp_id' => $receipt_id,
						'advpy_description' => 'Advance payment from receipt no:'.$receipt_no,
						'advpy_cur_id' => $currency['cur_id'],
						'advpy_amount' => $total_advance_payment_amount,
						'advpy_total_paid' => 0.00,
						'advpy_total_balance' => $total_advance_payment_amount,
						'advpy_status' => 'A'
					);
					
					$advancePaymentDb->insert($advance_payment_data);
				}
				
				$db->commit();

				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Receipt Added');
				
				//$this->_redirect( $this->baseUrl . '/studentfinance/receipt');
			
			} catch (Exception $e) {
				$db->rollBack();
				echo $e->getMessage();
				exit;
			}
		}
	
	}
	
	public function addAction(){
		$this->view->title = $this->view->translate('Proforma Invoice - Entry');
		
		$this->view->lobjinvoiceentryForm = $this->lobjinvoiceentryForm;
		$this->view->lobjcredittransferForm = $this->lobjcredittransferForm;
//		$currencyList = $this->lobjCurrency->fnGetCurrency("Y");
//		$this->view->lobjinvoiceentryForm->Currency->setValue($currencyList[0]['cur_id']);

		$IdUniversity = $this->gobjsessionsis->idUniversity;
		$larrconfig = $this->lobjSubjectsetup->getConfigDetail($IdUniversity);

		//Invoice Number
		$bill_number = $this->getCurrentProformaNoSeq();
		$this->view->lobjinvoiceentryForm->InvoiceId->setAttrib('readonly','true');
		$this->view->lobjinvoiceentryForm->InvoiceId->setValue($bill_number);
		
//		//fee item
//		
//		$this->view->lobjinvoiceentryForm->FeeCode->addMultiOptions($this->lobjprogram->fnGetProgramList());
		
		$larrformatkey = array('format'=>'StudentInvoiceIdFormat', 'prefix'=>'StudentInvoicePrefix');
		$table = 'invoice_main';
		$coloumn = 'bill_number';
//		$lstrcoderesult = $this->lobjcodegenObj->studentfinanceIdGenration($IdUniversity, $larrconfig[0],$table,$coloumn,$larrformatkey);
		
		$lstrcoderesult = $this->getCurrentProformaNoSeq();

		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$data = array();
			$larrformData = $this->_request->getPost();
			//Check Duplicacy of InvoiceId
			$lstrcheck = $larrformData['InvoiceId'];
			$result = $this->lobjinvoiceentryModel->fnCheckInvoiceId($lstrcheck);
			if($result > 0){
				$this->view->errMsg = '0';
				$this->view->lobjinvoiceentryForm->populate($larrformData);
			}
			else{
				unset($larrformData['Save']);
				$appCount = $this->lobjinvoiceentryModel->fngetapplist();
				$appId = $this->lobjinvoiceentryModel->fnsaveinvoiceentryApp($larrformData,$IdUniversity,$appCount,$lstrcoderesult,$lstrinvoicenumber);
				$this->lobjinvoiceentryModel->fnsaveinvoicedetailinfo($larrformData,$IdUniversity,$appId);
				$this->_redirect($this->baseUrl.'/studentfinance/invoiceentry/index');
			}
		}
	}
	
	public function fngetstudentdetAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Name = $this->_getParam('Name');
		$StudentId = $this->_getParam('StudentId');
		$Type = $this->_getParam('Type');
		$searchArray = array('name' => $Name, 'id' => $StudentId, 'type' => $Type
		);
		$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
		$studentList = $studentRegistrationDB->getStudentStudentList($searchArray);
		echo Zend_Json_Encoder::encode($studentList);
	}
	
	public function detailstudentAction() {
		$this->_helper->layout->disableLayout();
		$this->view->lobjinvoiceentryForm = $this->lobjinvoiceentryForm;
		$auth = Zend_Auth::getInstance();
		$this->view->userName = $auth->getIdentity()->loginName;
		$studentId = $this->_getParam('IdStudent');
		$type = $this->_getParam('Type');
		if($type == 1){ //applicant
			$studentDett = $this->lobjstudentregistrationModel->getApplicantDetail($studentId);
		}elseif($type == 2){ //student
			$studentDett = $this->lobjstudentregistrationModel->getStudentRegistrationDetail($studentId);
		}
		
		$studentsemesterList = $this->lobjstudentregistrationModel->getStudentSemesters($studentId);
		$len = '';
		$len = count($studentsemesterList);
		$this->view->currsem = '';
		$this->view->studsemstatus = '';
		$this->view->studDet = '';
		if($len != ''){
			if ($studentsemesterList[$len - 1]['SemesterMainCode'] != '') {
				$this->view->currsem = $studentsemesterList[$len - 1]['SemesterMainCode'];
			}
			if ($studentsemesterList[$len - 1]['SemesterCode'] != '') {
				$this->view->currsem = $studentsemesterList[$len - 1]['SemesterCode'];
			}
			$this->view->studsemstatus = $studentsemesterList[$len - 1]['DefinitionDesc'];
		}
		$this->view->studDet = $studentDett[0];
	}
	
	/*
	 *  Get current proforma seq no
	 */
	private function getCurrentProformaNoSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 1")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		if($row2){
			$r_num = $row2['ProformaInvoicePrefix'].$row2['ProformaInvoiceSeparator'].date('Y').$row2['ProformaInvoiceSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	/*
	 * Get Bill no from mysql function
	*/
	private function getProformaNoSeq(){
	
		$seq_data = array(
				1,
				date('Y'),
				0,
				0,
				0
		);
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
	
		return $seq['proforma_no'];
	}
	
	public function cancelAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			$proforma = $formData['proforma_id'];
			$n=0;
			while($n < count($proforma)){
				$proforma_id = $proforma[$n];
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
			
				$upd_data = array(
					'status'=> 'X',
					'cancel_by'=> $getUserIdentity->id,
					'cancel_date'=> date('Y-m-d H:i:s')
				);
				
				$this->promainDB->update($upd_data, array('id = ?' => $proforma_id) );
				
				$n++;
			}
		}
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Proforma Invoice Cancelled');
		$this->_redirect( $this->baseUrl . '/studentfinance/proforma-invoice/applicant');
		exit;
    }
    
    public function updateStatusAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
//			echo "<pre>";
//			print_r($formData);
			
			$proforma = $formData['proforma'];
				
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
		
			$upd_data = array(
				'status'=> $formData['status'],
				'remarks'=> $formData['remarks'],
				'updBy'=> $getUserIdentity->id,
				'updDate'=> date('Y-m-d H:i:s')
			);
			
			$this->promainDB->update($upd_data, array('id = ?' => $proforma) );
			
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$select = $db->select()
                            ->from(array('a'=>'proforma_invoice_main'))
                            ->join(array('b'=>'proforma_invoice_detail'), 'b.proforma_invoice_main_id = a.id', array('*'))
                            ->where('a.id =?',$proforma);

			$txnDataApp = $db->fetchAll($select);
			
			$trans_id = $txnDataApp[0]['trans_id'];
			
			
			if($formData['status'] == 'W'){
				
				//generate invoice from proforma
				$invoice_helper = new icampus_Function_Studentfinance_Invoice();
				
				$auth = Zend_Auth::getInstance();
				
				try{
					$status = $invoice_helper->generateInvoiceFromProforma($proforma,  $auth->getIdentity()->id);
				}catch (Exception $e){
						$sysErDb = new App_Model_General_DbTable_SystemError();
					   	$msg['se_txn_id'] = $trans_id;
					   	$msg['se_title'] = 'Error generate Invoice from Waived Proforma';
					   	$msg['se_message'] = $e->getMessage();
					   	$msg['se_createdby'] = $auth->getIdentity()->id;
					   	$msg['se_createddt'] = date("Y-m-d H:i:s");
					   	$sysErDb->addData($msg);
				}
					
					
			}
			
			
                        
                        //update checklist status process
                        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
                        $getUserIdentity = $auth->getIdentity();
        
                        $info = $checklistVerificationDB->infoBox($trans_id);
                        $checklistVerificationDB->deleteStatusByTrx($trans_id); //exit;

                        //get applicant document status
                        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trans_id);

                        foreach($docChecklist as $dataDoc){

                            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$trans_id, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
                            //var_dump($list);

                            if($list){
                                foreach($list as $listLoop){
                                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trans_id, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trans_id, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                                    if (!$adsCheck){ //update
                                        $data = array(
                                            'ads_txn_id'=>$trans_id,
                                            'ads_dcl_id'=>$dataDoc['dcl_Id'],
                                            'ads_ad_id'=>$adId,
                                            //'ads_appl_id'=>'',
                                            //'ads_confirm'=>$check,
                                            'ads_section_id'=>$dataDoc['dcl_sectionid'],
                                            'ads_table_name'=>$dataDoc['table_name'],
                                            'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                                            'ads_status'=>0,
                                            'ads_comment'=>'',
                                            'ads_createBy'=>$getUserIdentity->id,
                                            'ads_createDate'=>date('Y-m-d H:i:s')
                                        );
                                        $checklistVerificationDB->insert($data);
                                    }
                                }
                            }
                        }
			
			$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
			$paymentClass->checkingApplicantDocumentChecklist($trans_id);

			foreach($txnDataApp as $txnData){
				
				$feeID = $txnData['fi_id'];
				
				$fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
				$fsProgram = $fsProgramDB->getApplicantDocumentType($trans_id,$feeID);
				$ads_id = $fsProgram['ads_id'];
				
				if($ads_id){
					$newStatus = '';
					if($formData['status'] == 'E'){
						$newStatus = 'Exempted';
					}else if($formData['status'] == 'W'){
						$newStatus = 'Waived';
					}else if($formData['status'] == 'S'){
						$newStatus = 'Sponsorship';
					}
					
					$docStatus = array(
						'ads_status'=>5,//not applicable
						'ads_comment'=> $newStatus.', '.$formData['remarks'] ,
						'ads_modBy'=>$getUserIdentity->id,
						'ads_modDate'=>date('Y-m-d H:i:s'),
					);
					
//					echo "<pre>";
//					print_r($docStatus);
					$appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
					$appDocStatusDB->update($docStatus, array('ads_id =?'=>$ads_id) );
				}
			}
			
			$paymentClass->updateStatusTransaction($trans_id);
				
				
		}
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Proforma Invoice Updated');
		$this->_redirect( $this->baseUrl . '/studentfinance/proforma-invoice/applicant');
		exit;
    }
    
	public function regenerateProformaAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$result = array('code'=>0);
		
		$trans_id = $this->_getParam('id', null);
		$level = $this->_getParam('level', null);
		
		//generate  proforma
		$invoice_helper = new icampus_Function_Studentfinance_Invoice();
		
		$auth = Zend_Auth::getInstance();
		
		//update fee structure id
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
		->from(array('a' => 'applicant_profile'))
		->join(array('b' => 'applicant_transaction'), 'b.at_appl_id = a.appl_id')
		->join(array('c' => 'applicant_program'), 'c.ap_at_trans_id = b.at_trans_id')
		->where('b.at_trans_id = ?', $trans_id);
			
		$txnData = $db->fetchRow($select);
		
		$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
		
		if($txnData['at_fs_id']==null || $txnData['at_fs_id']==0 ){
			
			try{
				$feeStructureData = $feeStructureDb->getApplicantFeeStructure($txnData['ap_prog_id'], $txnData['ap_prog_scheme'], $txnData['at_intake'], $txnData['appl_category']);
			
				$txnData['at_fs_id'] = $feeStructureData['fs_id'];
			
				//update applicnt program
				$db->update('applicant_transaction', array('at_fs_id'=>$feeStructureData['fs_id']), 'at_trans_id = '.$trans_id);
				}catch (Exception $e){
					$result['code'] = 2;
					$result['msg'] = "Error generate proforma invoice: " . $e->getMessage();
				}
		}
		
		
		try{
			$status = $invoice_helper->generateApplicantProformaInvoice($trans_id, $level);
			$result['code'] = 1;
		}catch (Exception $e){
			$result['code'] = 2;
			$result['msg'] = "Error generate proforma invoice: " . $e->getMessage();
			
			 //save error message
			   	$sysErDb = new App_Model_General_DbTable_SystemError();
			   	$msg['se_txn_id'] = $trans_id;
			   	$msg['se_title'] = 'Error generate proforma invoice Level '.$level;
			   	$msg['se_message'] = $e->getMessage();
			   	$msg['se_createdby'] = $auth->getIdentity()->iduser;
			   	$msg['se_createddt'] = date("Y-m-d H:i:s");
			   	$sysErDb->addData($msg);
   	
		}
			
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
		
		$json = Zend_Json::encode($result);
		echo $json;
		exit();
	}
    
    
}

