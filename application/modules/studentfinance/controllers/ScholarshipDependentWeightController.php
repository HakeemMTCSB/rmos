<?php

class Studentfinance_ScholarshipDependentWeightController extends Zend_Controller_Action
{

    public function indexAction() {
        $DependentWeight = new Studentfinance_Model_DbTable_ScholarshipDependentWeight();
        $this->view->dependentweights = $DependentWeight->fetchAll();
    }

    public function addAction() {
        $DependentWeight = new Studentfinance_Model_DbTable_ScholarshipDependentWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('dependentweight');
            $DependentWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-dependent-weight/index');
        }
    }

    public function editAction() {
        $DependentWeight = new Studentfinance_Model_DbTable_ScholarshipDependentWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('dependentweight');
            $DependentWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-dependent-weight/index');
        }

        $id =$this->_getParam('id');
        $dependentweight = $DependentWeight->find($id);
        if($dependentweight->count() > 0) {
            $this->view->dependentweight = $dependentweight->current();
        } else {
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-dependent-weight/index');
        }
    }

    public function deleteAction() {
        $DependentWeight = new Studentfinance_Model_DbTable_ScholarshipDependentWeight();
        $id =$this->_getParam('id');
        $dependentweight = $DependentWeight->find($id);
        $dependentweight->current()->delete();
        $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record deleted"));
        $this->redirect( $this->baseUrl . '/studentfinance/scholarship-dependent-weight/index');
    }

}