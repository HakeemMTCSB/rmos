<?php
class Studentfinance_ReceiptController extends Base_Base {
	private $lobjProgramChargemasterForm;
	private $lobjprogram;
	private $lobjChargemaster;
	private $lobjProgramChargemaster;
	private $lobjdeftype;
	private $lobjinitialconfig;	
	private $_gobjlog;
	protected $_ses_receipt;
	
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
		
		$this->_ses_receipt = new Zend_Session_Namespace('receipt');
	}
	public function fnsetObj(){
		
		$this->lobjform = new App_Form_Search ();
		$this->lobjInvoice = new Studentfinance_Model_DbTable_Invoice ();
		$this->lobjInvoiceModel = new Studentfinance_Model_DbTable_Invoice();
		$this->lobjRecieptModel = new Studentfinance_Model_DbTable_Receipt();
		$this->lobjAcademicstatus = new Examination_Model_DbTable_Academicstatus();
		$this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjInvoiceForm = new Studentfinance_Form_Receipt();	
		$this->lobjAccountmaster = new Studentfinance_Model_DbTable_Accountmaster();	
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration(); 		
		
	}
	public function indexAction() { // action for search and view
		
		$this->view->title = $this->view->translate('Receipt');
		
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			
			$this->view->search = $formData;
			
			$receipt_list = $receiptDb->getSearchData($formData['name'],$formData['id'],$formData['type'],$formData['receipt_id'],$formData['status']);
			$this->view->receipt_list = $receipt_list;
		}
		/*else{
			$receipt_list = $receiptDb->getData();
		}
		
		if($receipt_list){
			$this->view->receipt_list = $receipt_list;
		}else{
			throw new Exception('No data found');
		}*/
				
	}
	
	public function addAction(){
		$this->view->title = $this->view->translate('Receipt - Entry');

		$payee_type_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payee Type');
		$this->view->payee_type_list = $payee_type_list;
		
		//payment group
		$payment_group_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');
		$this->view->payment_group = $payment_group_list;
		
		//mode of payment
		$paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
		$mode_of_payment_list = $paymentModeDb->getPaymentModeList();
		$this->view->mode_of_payment = $mode_of_payment_list;
		
		//currency
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$currency_list = $currencyDb->fetchAll('cur_status = 1');
		$this->view->currency_list = $currency_list;
		
		//credit card terminal
		$creditCardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
		$creditCardTerminal_list = $creditCardTerminalDb->getTerminalList();
		$this->view->terminal_list = $creditCardTerminal_list;
		
		//bank from account code
		$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		$bankAccountCodeList = $accountCodeDb->getAccountCode(1,614);//hardcoded to bank acc code
		$this->view->bank_list = $bankAccountCodeList;
		
		//non-invoice
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$non_invoice_list = $feeItemDb->getActiveNonInvoiceFeeItem();
		$this->view->non_invoice_list = $non_invoice_list;
		
		//receipt seq
		$this->view->receipt_no = $this->getCurrentReceiptNoSeq();
				
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();

			/*echo "<pre>";
			print_r($formData);
			exit;*/
			
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			
			$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			
			try {
				
				//get appl_id from transaction id or idStudentregistration
				$payee = array();
				if($formData['receipt_payee_type'] == 645){
					$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
					$txnData = $txnDb->fetchRow( array('at_trans_id = ?'=> $formData['receipt_payee_id']) );
					
					$txnProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
					$txnProfile= $txnProfileDb->fetchRow( array('appl_id = ?'=> $txnData['at_appl_id']) );
					
					$txnProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
					$txnProgram = $txnProgramDb->fetchRow( array('ap_at_trans_id = ?'=> $txnData['at_trans_id']) );
		
					if($txnData){
						$txnData = $txnData->toArray();
						
						$payee['appl_id'] = $txnData['at_appl_id'];
						$payee['txn_id'] = $txnData['at_trans_id'];
						$payee['idStudentRegistration'] = null;
					}
					
					
					
				}else
				if($formData['receipt_payee_type'] == 646){
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$select = $db ->select()
								  ->from(array('a'=>'tbl_studentregistration'))
								  ->join(array('b'=>'student_profile'),'a.sp_id = b.id')
								  ->where("a.IdStudentRegistration = ?",$formData['receipt_payee_id']);
//								  ->where("a.profileStatus = 92");//active only
									  
					$txnData = $db->fetchRow($select);
					
					if($txnData){
						
						$payee['appl_id'] = $txnData['appl_id'];
						$payee['txn_id'] = $txnData['transaction_id'];
						$payee['idStudentRegistration'] = $txnData['IdStudentRegistration'];
					}
					
				}
				
				
				if($payee == null){
					throw new Exception('Payee record not found');
				}
				
				
				//calculate total payment amount
				$total_payment_amount = $formData['total_payment'];
				//foreach ($formData['payment'] as $payment){
//					$total_payment_amount += $payment['amount'];
				//}
				
				//calculate total invoice amount
				$total_invoice_amount = 0;
				if(isset($formData['invoice'])){
					foreach ($formData['invoice'] as $invoice){
						$total_invoice_amount += $invoice['amount'];
					}
				}
								
				//calculate total non-invoice amount
				$total_non_invoice_amount = 0;
				if(isset($formData['non_invoice'])){
					foreach ($formData['non_invoice'] as $non_invoice){
						$total_non_invoice_amount += $non_invoice['amount'];
					}
				}
				
				$currencyUsed = $formData['currency_used'];
				
				//get currency from code
				$currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
				$currency = $currencyDb->getCurrentExchangeRate($currencyUsed);
				if(!$currency){
					throw new Exception('Currency not found');
				}
				
				//add receipt
				$receiptDb = new Studentfinance_Model_DbTable_Receipt();
				$receipt_no = $this->getReceiptNoSeq();
				
				$receipt_data = array(
					'rcp_no' => $receipt_no,
					'rcp_account_code' => $formData['receipt_account_code'],	
					'rcp_date' => date('Y-m-d', strtotime($formData['receipt_date'])),
					'rcp_receive_date' => date('Y-m-d', strtotime($formData['receipt_receive_date'])),
					'rcp_payee_type' => $formData['receipt_payee_type'],
					'rcp_appl_id' => $payee['appl_id'],
					'rcp_trans_id' => $payee['txn_id'],
					'rcp_idStudentRegistration' => $payee['idStudentRegistration'],
					'rcp_description' => $formData['receipt_description'],
					'rcp_amount' => $total_payment_amount,
					'rcp_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $currency['cr_cur_id']),
					'rcp_adv_payment_amt' => 0.00,
					'rcp_gainloss' => $formData['rcp_gainloss'],
					'rcp_gainloss_amt' => $formData['rcp_gainloss_amt'],
					'rcp_cur_id' => $currencyUsed
				);
				$receipt_id = $receiptDb->insert($receipt_data);
				
			
				//add payment (loop)
				$paymentDb = new Studentfinance_Model_DbTable_Payment();
				foreach ($formData['payment'] as $payment){
					$payment_data = array(
						'p_rcp_id' => $receipt_id,
						'p_payment_mode_id' => $payment['mode'],
						'p_cheque_no' => isset($payment['cheque_no'])?$payment['cheque_no']:null,
						'p_doc_bank' => isset($payment['payment_doc_bank'])?$payment['payment_doc_bank']:null,
						'p_doc_branch' => isset($payment['payment_doc_branch'])?$payment['payment_doc_branch']:null,
						'p_terminal_id' => isset($payment['terminal_id'])?$payment['terminal_id']:null,
						'p_card_no' => isset($payment['card_no'])?$payment['card_no']:null,
						'p_cur_id' => $currencyUsed,
						'p_amount' => $total_payment_amount,
						'p_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $currencyUsed)
					);
					
					$paymentDb->insert($payment_data);
				}
				
				$currencyPaid = $formData['currency_used'];
				
				//add non-invoice (loop)
				if( isset($formData['non_invoice']) ){
					$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
					
					foreach ($formData['non_invoice'] as $non_invoice){
						$non_invoice_data = array(
							'ni_fi_id' => $non_invoice['fi_id'],
							'ni_cur_id' => $non_invoice['currency'],
							'ni_amount' => $non_invoice['amount'],
							'ni_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($non_invoice['amount'],$non_invoice['currency']),
							'ni_rcp_id' => $receipt_id,
						);	
						
						$nonInvoiceDb->insert($non_invoice_data);
					}
				}
				
				//add receipt-invoice detail
				$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
				$dnIdArr = array();

				/*$balanceFeeTotal = 0;
				foreach ($formData['invoice_fee_item'] as $invoice_id2 => $fee_item2){
					$amountPaid = $fee_item2['invoice_fi_amount'];
					$ebamount = $fee_item2['invoice_fi_ebamount'];

					$invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
					$invoiceData = $invoiceDetailDB->getData($invoice_id2);

					$balanceFee = ($invoiceData['amount']-($amountPaid+$ebamount));
					$balanceFeeTotal = $balanceFeeTotal + $balanceFee;
				}*/

				foreach ($formData['invoice_fee_item'] as $invoice_id => $fee_item){
					
//					$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
//					$invoiceData = $invoiceMainDB->getData($invoice_id);
					
					$invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
					$ebModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();
					$invoiceData = $invoiceDetailDB->getData($invoice_id);
					
					$currencyItem = $fee_item['invoice_fi_currency'];
					
					$amountPaid = $fee_item['invoice_fi_amount'];
					$amountNew = $fee_item['invoice_fi_amount'];
					$ebamount = $fee_item['invoice_fi_ebamount'];
					$balanceFee = ($invoiceData['amount']-($amountPaid+$ebamount));

					/*if($currencyItem == 1){ //if invoice in myr
						$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
						$dataCur = $curRateDB->getData($invoiceData['exchange_rate']);
						$amountNew = $fee_item['invoice_fi_amount'] / $dataCur['cr_exchange_rate'];
					}
					*/
					
					if($currencyPaid == $currencyItem){
						$amountNew = $fee_item['invoice_fi_amount'];
					}else{
						if($currencyPaid == 2){
							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							$dataCur = $curRateDB->getCurrentExchangeRate(2);
							$amountNew = round($fee_item['invoice_fi_amount'] * $dataCur['cr_exchange_rate'],2);
						}else if($currencyPaid == 1){
							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							$dataCur = $curRateDB->getData($invoiceData['exchange_rate']);
							$amountNew = round($fee_item['invoice_fi_amount'] / $dataCur['cr_exchange_rate'],2);
						}
					}
					
//					echo $amountNew;
					$data_rcp_inv = array(
						'rcp_inv_rcp_id' => $receipt_id,
						'rcp_inv_rcp_no' => $receipt_no,
						'rcp_inv_invoice_id' => $invoiceData['invoice_main_id'],
						'rcp_inv_invoice_dtl_id' => $invoice_id,
						'rcp_inv_amount' => $amountPaid,
						'rcp_inv_amount_default_currency' => $amountNew,
						'rcp_inv_cur_id' => $currencyPaid //$fee_item['invoice_fi_currency']
					);
					
//					echo "<pre>";
//					print_r($data_rcp_inv);
					$receiptInvoiceDb->insert($data_rcp_inv);

					if ($balanceFee <= 0.00) {
						if ($ebamount > 0.00) {
							$bill_no = $ebModel->getBillSeq(9, date('Y'));

							$data_dn = array(
								'dcnt_fomulir_id' => $bill_no,
								'dcnt_IdStudentRegistration' => $payee['idStudentRegistration'],
								'dcnt_batch_id' => 0,
								'dcnt_amount' => $ebamount,
								'dcnt_type_id' => $fee_item['invoice_fi_ebtype'],
								'dcnt_description' => $fee_item['invoice_fi_ebname'],
								'dcnt_currency_id' => $currencyItem,
								'dcnt_creator' => 665,//by system
								'dcnt_create_date' => date('Y-m-d H:i:s'),
								'dcnt_status' => 'E',//entry
							);
							$dn_id = $ebModel->insertEbDiscount($data_dn);
							array_push($dnIdArr, $dn_id);

							$cnDetailData = array(
								'dcnt_id' => $dn_id,
								'dcnt_invoice_id' => $invoiceData['invoice_main_id'],
								'dcnt_invoice_det_id' => $invoice_id,
								'dcnt_amount' => $ebamount,
								'status' => 'E',
								'dcnt_description' => $fee_item['invoice_fi_ebname']
							);
							$ebModel->insertEbDiscountDtl($cnDetailData);
						}
					}
				}

				if (count($dnIdArr) > 0){
					$receiptDnData = array(
						'ebip'=>implode(',', $dnIdArr)
					);
					$receiptDb->update($receiptDnData, 'rcp_id = '.$receipt_id);
				}
				
				$db->commit();
				
//				exit;

				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Receipt Added');
				
				$this->_redirect( $this->baseUrl . '/studentfinance/receipt/view-receipt-detail/id/'.$receipt_id);
			
			} catch (Exception $e) {
				$db->rollBack();
				echo $e->getMessage();
				exit;
			}
		}
	
	}
	
public function addoldAction(){
		$this->view->title = $this->view->translate('Receipt - Entry');
		
		$payee_type_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payee Type');
		$this->view->payee_type_list = $payee_type_list;
		
		//payment group
		$payment_group_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');
		$this->view->payment_group = $payment_group_list;
		
		//mode of payment
		$paymentModeDb = new Studentfinance_Model_DbTable_PaymentMode();
		$mode_of_payment_list = $paymentModeDb->getPaymentModeList();
		$this->view->mode_of_payment = $mode_of_payment_list;
		
		//currency
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$currency_list = $currencyDb->fetchAll('cur_status = 1');
		$this->view->currency_list = $currency_list;
		
		//credit card terminal
		$creditCardTerminalDb = new Studentfinance_Model_DbTable_CreditcardTerminal();
		$creditCardTerminal_list = $creditCardTerminalDb->getTerminalList();
		$this->view->terminal_list = $creditCardTerminal_list;
		
		//bank from account code
		$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		$bankAccountCodeList = $accountCodeDb->getAccountCode(1,614);//hardcoded to bank acc code
		$this->view->bank_list = $bankAccountCodeList;
		
		//non-invoice
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$non_invoice_list = $feeItemDb->getActiveNonInvoiceFeeItem();
		$this->view->non_invoice_list = $non_invoice_list;
		
		//receipt seq
		$this->view->receipt_no = $this->getCurrentReceiptNoSeq();
				
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
			
//			echo "<pre>";
//			print_r($formData);
//			exit;

			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			
			$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			
			try {
				
				//get appl_id from transaction id or idStudentregistration
				$payee = array();
				if($formData['receipt_payee_type'] == 645){
					$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
					$txnData = $txnDb->fetchRow( array('at_pes_id = ?'=> $formData['receipt_payee_id']) );
					
					$txnProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
					$txnProfile= $txnProfileDb->fetchRow( array('appl_id = ?'=> $txnData['at_appl_id']) );
					
					$txnProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
					$txnProgram = $txnProgramDb->fetchRow( array('ap_at_trans_id = ?'=> $txnData['at_trans_id']) );
		
					if($txnData){
						$txnData = $txnData->toArray();
						
						$payee['appl_id'] = $txnData['at_appl_id'];
						$payee['txn_id'] = $txnData['at_trans_id'];
						$payee['idStudentRegistration'] = null;
					}
					
					
					
				}else
				if($formData['receipt_payee_type'] == 646){
					//TODO: get student info
				}
				
				
				if($payee == null){
					throw new Exception('Payee record not found');
				}
				
				
				//calculate total payment amount
				$total_payment_amount = $formData['total_payment'];
				//foreach ($formData['payment'] as $payment){
//					$total_payment_amount += $payment['amount'];
				//}
				
				//calculate total invoice amount
				$total_invoice_amount = 0;
				if(isset($formData['invoice'])){
					foreach ($formData['invoice'] as $invoice){
						$total_invoice_amount += $invoice['amount'];
					}
				}
								
				//calculate total non-invoice amount
				$total_non_invoice_amount = 0;
				if(isset($formData['non_invoice'])){
					foreach ($formData['non_invoice'] as $non_invoice){
						$total_non_invoice_amount += $non_invoice['amount'];
					}
				}
				
				//calculate advance payment amount
				$total_advance_payment_amount = 0;
				if( $total_payment_amount > ($total_invoice_amount+$total_non_invoice_amount) ){
					$total_advance_payment_amount = $total_payment_amount - ($total_invoice_amount+$total_non_invoice_amount);
				}
				
				//get currency from code
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currency = $currencyDb->getCurrencyFromCode($formData['receipt_currency']);
				if(!$currency){
					throw new Exception('Currency not found');
				}
				
				//add receipt
				$receiptDb = new Studentfinance_Model_DbTable_Receipt();
				$receipt_no = $this->getReceiptNoSeq();
				
				$receipt_data = array(
					'rcp_no' => $receipt_no,
					'rcp_account_code' => $formData['receipt_account_code'],	
					'rcp_date' => date('Y-m-d', strtotime($formData['receipt_date'])),
					'rcp_receive_date' => date('Y-m-d', strtotime($formData['receipt_receive_date'])),
					'rcp_payee_type' => $formData['receipt_payee_type'],
					'rcp_appl_id' => $payee['appl_id'],
					'rcp_trans_id' => $payee['txn_id'],
					'rcp_idStudentRegistration' => $payee['idStudentRegistration'],
					'rcp_description' => $formData['receipt_description'],
					'rcp_amount' => $total_payment_amount,
					'rcp_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $currency['cur_id']),
					'rcp_adv_payment_amt' => $total_advance_payment_amount,	
					'rcp_cur_id' => $currency['cur_id']
				);
				$receipt_id = $receiptDb->insert($receipt_data);
				
				
			
				//add payment (loop)
				$paymentDb = new Studentfinance_Model_DbTable_Payment();
				foreach ($formData['payment'] as $payment){
					$payment_data = array(
						'p_rcp_id' => $receipt_id,
						'p_payment_mode_id' => $payment['mode'],
						'p_cheque_no' => isset($payment['cheque_no'])?$payment['cheque_no']:null,
						'p_doc_bank' => isset($payment['payment_doc_bank'])?$payment['payment_doc_bank']:null,
						'p_doc_branch' => isset($payment['payment_doc_branch'])?$payment['payment_doc_branch']:null,
						'p_terminal_id' => isset($payment['terminal_id'])?$payment['terminal_id']:null,
						'p_card_no' => isset($payment['card_no'])?$payment['card_no']:null,
						'p_cur_id' => $payment['currency'],
						'p_amount' => $total_payment_amount,
						'p_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $payment['currency'])
					);
					
					$paymentDb->insert($payment_data);
				}
				
				$currencyPaid = $formData['receipt_currency'];
				
				//add non-invoice (loop)
				if( isset($formData['non_invoice']) ){
					$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
					
					foreach ($formData['non_invoice'] as $non_invoice){
						$non_invoice_data = array(
							'ni_fi_id' => $non_invoice['fi_id'],
							'ni_cur_id' => $non_invoice['currency'],
							'ni_amount' => $non_invoice['amount'],
							'ni_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($non_invoice['amount'],$non_invoice['currency']),
							'ni_rcp_id' => $receipt_id,
						);	
						
						$nonInvoiceDb->insert($non_invoice_data);
					}
				}
				
				//update invoice (loop)
				if(isset($formData['invoice'])){
					
					$amount_for_invoice = $total_payment_amount - $total_non_invoice_amount;
					$invoice_list = array();
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
					
					$a = 0;
					foreach ($formData['invoice'] as $invoice){
						$invoice_row = $invoiceMainDb->fetchRow(array('id = ?'=>$invoice['invoice_id']));
						
						$invoicedetail_row = $invoiceDetailDb->fetchRow(array('invoice_main_id = ?'=>$invoice['invoice_id']));
						
						if($invoice_row){
							$invoice_list[$a] = $invoice_row->toArray();
							$invoice_list[$a]['details'] = $invoicedetail_row->toArray();
					
						}
						
						$a++;
					}
					
					
					$i = 0;
					while ($amount_for_invoice > 0 && $i < sizeof($invoice_list) ) {
						
						$amount_to_use = 0.00;
						$balance = $invoice_list[$i]['bill_amount'] - ($invoice_list[$i]['bill_paid'] + $amount_to_use);
						
						
						if($invoice_list[$i]['bill_balance'] < $amount_for_invoice){
							$amount_to_use = $invoice_list[$i]['bill_balance'];
						}else{
							$amount_to_use = $amount_for_invoice;
						}
						
						$bill_paid =  $invoice_list[$i]['bill_paid'] + $amount_to_use;
						
						
						$upd_data_invoice = array(
							'bill_paid' => $invoice_list[$i]['bill_paid'] + $amount_to_use,
							'bill_balance' => $invoice_list[$i]['bill_amount'] - ($invoice_list[$i]['bill_paid'] + $amount_to_use)
						);

						$invoiceMainDb->update($upd_data_invoice, array('id = ?' => $invoice_list[$i]['id']) );
						
						
						$feeID = $invoice_list[$i]['details']['fi_id'];
						
						
						
						//update invoic detail
						$upd_data_invoice_detail = array(
							'paid' => $invoice_list[$i]['bill_paid'] + $amount_to_use,
							'balance' => $invoice_list[$i]['bill_amount'] - ($invoice_list[$i]['bill_paid'] + $amount_to_use)
						);

						$invoiceDetailDb->update($upd_data_invoice_detail, array('invoice_main_id = ?' => $invoice_list[$i]['id']) );
						
						
						$amount_for_invoice -= $amount_to_use;

						$i++;
					}
					
					//add receipt-invoice detail
					$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
					
					foreach ($formData['invoice_fee_item'] as $invoice_id => $fee_item){
						
						$data_rcp_inv = array(
							'rcp_inv_rcp_id' => $receipt_id,
							'rcp_inv_rcp_no' => $receipt_no,
							'rcp_inv_invoice_id' => $invoice_id,
							'rcp_inv_invoice_dtl_id' => $fee_item['invoice_fi_id'],
							'rcp_inv_amount' => $total_payment_amount,
							'rcp_inv_amount_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $fee_item['invoice_fi_currency']),
							'rcp_inv_cur_id' => $fee_item['invoice_fi_currency']
						);
						$receiptInvoiceDb->insert($data_rcp_inv);

						//update invoice-subject (if any)
						if( isset($fee_item['subject']) ){
							
							$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
							foreach ($fee_item['subject'] as $subject){
								$data_invoice_subject_upd = array(
									'rcp_id' => $receipt_id,
									'rcp_no' => $receipt_no,	
									'paid' => $subject['amount'],
									'balance' => $subject['amount']
								);
								
								$where_inv_sub = array(
									'invoice_main_id = ? ' => $invoice_id,
									'invoice_detail_id = ?' => $fee_item['invoice_fi_id'],
									'subject_id = ?' => $subject['id']
								);
								
								$invoiceSubjectDb->update($data_invoice_subject_upd, $where_inv_sub);
							}
							
						}
					}
					
					
				}
				
				
				
				//add advance payment (if any)
				if($total_advance_payment_amount > 0){
					
					$bill_no =  $this->getBillSeq(8, date('Y'));
					
					$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advance_payment_data = array(
						'advpy_fomulir' => $bill_no,
						'advpy_appl_id' => $payee['appl_id'],
						'advpy_trans_id' => $payee['txn_id'],
						'advpy_idStudentRegistration' => $payee['idStudentRegistration'],
						'advpy_rcp_id' => $receipt_id,
						'advpy_description' => 'Advance payment from receipt no:'.$receipt_no,
						'advpy_cur_id' => $currency['cur_id'],
						'advpy_amount' => $total_advance_payment_amount,
						'advpy_total_paid' => 0.00,
						'advpy_total_balance' => $total_advance_payment_amount,
						'advpy_status' => 'A',
						'advpy_date' => date('Y-m-d')
					);
					
					$advancePaymentDb->insert($advance_payment_data);
				}
				
				$db->commit();

				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Receipt Added');
				
				//$this->_redirect( $this->baseUrl . '/studentfinance/receipt');
			
			} catch (Exception $e) {
				$db->rollBack();
				echo $e->getMessage();
				exit;
			}
		}
	
	}
	
	public function saveReceiptSessionAction(){
		
		$result['status'] = false;
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if( isset($formData['key']) ){
				$this->_ses_receipt->$formData['key'] = $formData['value'];
			}
			
			$result['status'] = true;
			
			$result['ses_data'] = $this->_ses_receipt->$formData['key'];
		}
		
		
		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
		
	}

	public function newreceiptAction(){
		
	    $this->view->lobjInvoiceForm = $this->lobjInvoiceForm;    
	    
		if($this->view->ReceiptType == 1) {
	    	$this->lobjInvoiceForm->RecieptNumber->setAttrib('readonly',"true");
	    	$this->lobjInvoiceForm->RecieptNumber->setValue('xxx-xxx-xxx');	    	
	    } 
	    else{	    	
	       $this->lobjInvoiceForm->RecieptNumber->setAttrib('validator','validateRecieptnum');
	    }
	    $invoiceList = $this->lobjInvoice->fngetinvoicenumber();
	    //$this->lobjInvoiceForm->InvoiceId->addMultiOptions($invoiceList);
	
	    /*$Semesterlist=$this->lobjSemesterModel->fngetSemesterList();
        $this->lobjInvoiceForm->SemesterId->addMultiOptions($Semesterlist);*/
		
        $studentdropdown = $this->lobjRecieptModel->fngetStudentDropdown();
		$this->view->lobjInvoiceForm->StudentId->addMultiOptions($studentdropdown);		
        
        $currentsemester = $this->lobjSemesterModel->fngetidsemesterstatus();
        $this->view->lobjInvoiceForm->SemesterId->setValue($currentsemester);
        	
		$larrresultitemgroup = $this->lobjRecieptModel->fngetitemgroup();
		$this->lobjInvoiceForm->ItemName->addMultiOptions($larrresultitemgroup);

        $lobjprogrmlist = $this->lobjprogram->fnGetProgramList();
		$this->lobjInvoiceForm->IdProgram->addMultiOptions($lobjprogrmlist);	

        $lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster();
		$lobjCollegeList = $lobjDepartmentmaster->fnGetCollegeList();
		$this->lobjInvoiceForm->IdCollege->addMultiOptions($lobjCollegeList);		
		
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjInvoiceForm->UpdDate->setValue($ldtsystemDate);
		$ldtsystemDate1 = date('Y-m-d');
		$this->view->lobjInvoiceForm->RecieptDate->setValue($ldtsystemDate1);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjInvoiceForm->UpdUser->setValue($auth->getIdentity()->iduser);
		
		$larrdefmsMode = $this->lobjdeftype->fnGetDefinations('Mode Of Payment');
		foreach($larrdefmsMode as $larrdefmsModedtls) {
			$this->lobjInvoiceForm->ModeOfPayment->addMultiOption($larrdefmsModedtls['idDefinition'],$larrdefmsModedtls['DefinitionDesc']);
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
		 if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			
				if($larrformData['ChequeDate'] == '') {
					$larrformData['ChequeDate'] = '0000-00-00';
				}
					
					$result = $this->lobjInvoiceModel->fnAddReceipt($larrformData);					
				 	$this->lobjInvoiceModel->fninsertreceiptdetails($result,$larrformData);
				 
				$idUniversity =$this->gobjsessionsis->idUniversity;	
				$studentid = $larrformData['StudentId'];
				$resultcode = $this->lobjInvoiceModel->fnGenerateAccCodes($idUniversity,$result,$this->lobjinitialconfig,$studentid);
				$this->lobjInvoiceModel->fnUpdateRecieptCode($result,$resultcode);
				
				//$this->_redirect( $this->baseUrl . '/studentfinance/receipt');
				
				
		 echo "<script> 
			var x=window.confirm('Receipt saved successfully, Do you wants to print Receipt?');
			if(x){
				parent.location ='".$this->view->baseUrl()."/studentfinance/receipt/printreport/idReciept/".$result."';
			}
		</script>";
		 
		 // Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Receipt Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
		echo "<script>window.location ='".$this->view->baseUrl()."/studentfinance/receipt/index';</script>"; 			
		}

				
		}
		
		}
		
		
			
	public function printreportAction() {

		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
			
			$idReciept = ( int ) $this->_getParam ( 'idReciept' );
			//print_r($idReciept);die();
			
			//object to initialize ini file
		$lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');
		
		
			try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	            
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");
	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."Receipt.jrxml"));
	            
	            
	            //Jasper Fill Manager object
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	            //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");
	          	$lobjparams->put("IDRECEIPT",$idReciept);
	           	$lobjparams->put ( "IMAGEPATH", $lstrimagepath . "trisakti-logo.png" );
	         
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java("net.sf.jasperreports.engine.JasperExportManager");	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="receipt_details.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";					
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }
			
	}
		
	
	public function editreceiptAction() {
		
		$IdReceipt = ( int ) $this->_getParam ( 'idReciept' );
		
		$this->view->lobjInvoiceForm = $this->lobjInvoiceForm;
		 
		$larrresult = $this->lobjInvoiceModel->fnEditReceipt($IdReceipt);
		
		$this->view->studentid=$larrresult['StudentId'];
		$this->view->invoiceid=$larrresult['InvoiceId'];
	
		
		if($this->view->ReceiptType == 1) {
	    	$this->lobjInvoiceForm->RecieptNumber->setAttrib('readonly',"true");	    	
	    } 
	    else{	    	
	    	//$this->lobjInvoiceForm->RecieptNumber->setAttrib('validator','validateRecieptnum');
	    }
	    
	    $invoiceList = $this->lobjInvoice->fngetinvoicenumber();
	    $this->lobjInvoiceForm->InvoiceId->addMultiOptions($invoiceList);
	    
		$Semesterlist=$this->lobjSemesterModel->fngetSemesterList();
        $this->lobjInvoiceForm->SemesterId->addMultiOptions($Semesterlist);
		
        $studentdropdown = $this->lobjRecieptModel->fngetStudentDropdown();
		$this->view->lobjInvoiceForm->StudentId->addMultiOptions($studentdropdown);		
        
        $currentsemester = $this->lobjSemesterModel->fngetidsemesterstatus();
        $this->view->lobjInvoiceForm->SemesterId->setValue($currentsemester);
        	
		$larrresultitemgroup = $this->lobjRecieptModel->fngetitemgroup();
		$this->lobjInvoiceForm->ItemName->addMultiOptions($larrresultitemgroup);

        $lobjprogrmlist = $this->lobjprogram->fnGetProgramList();
		$this->lobjInvoiceForm->IdProgram->addMultiOptions($lobjprogrmlist);	

        $lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster();
		$lobjCollegeList = $lobjDepartmentmaster->fnGetCollegeList();
		$this->lobjInvoiceForm->IdCollege->addMultiOptions($lobjCollegeList);		
		
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjInvoiceForm->UpdDate->setValue($ldtsystemDate);
		$ldtsystemDate1 = date('Y-m-d');
		$this->view->lobjInvoiceForm->RecieptDate->setValue($ldtsystemDate1);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjInvoiceForm->UpdUser->setValue($auth->getIdentity()->iduser);
		
		$larrdefmsMode = $this->lobjdeftype->fnGetDefinations('Mode Of Payment');
		foreach($larrdefmsMode as $larrdefmsModedtls) {
			$this->lobjInvoiceForm->ModeOfPayment->addMultiOption($larrdefmsModedtls['idDefinition'],$larrdefmsModedtls['DefinitionDesc']);
		}
		
		
		$this->view->lobjPlantypeForm = new  Studentfinance_Form_Receipt();
		
	   	$this->view->lobjInvoiceForm->populate($larrresult);
	   	$this->lobjInvoiceForm->InvoiceId->setvalue($larrresult['InvoiceId']);
	   	$this->lobjInvoiceForm->InvoiceId->setAttrib('readonly',"true");
	   	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
		 if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();			
			    unset ( $larrformData ['Save'] );
			    
			    $larrformData['UpdDate']=$ldtsystemDate = date('Y-m-d H:i:s');
				$this->lobjInvoiceModel->fnupdateReceipt($larrresult['InvoiceId'],$larrformData);
			
		echo "<script> 
			var x=window.confirm('Receipt saved successfully, Do you wants to print Receipt?');
			if(x){
				parent.location ='".$this->view->baseUrl()."/studentfinance/receipt/printreport/idReciept/".$IdReceipt."';
			}
		</script>";
		
		
		echo "<script>window.location ='".$this->view->baseUrl()."/studentfinance/receipt/index';</script>"; 	
			}
				
		}
	}
	
	public function fngetinvoicedetailsAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$InvoiceId = ( int ) $this->_getParam ( 'InvoiceId' );
		$larrInvoiceDetails = $this->lobjInvoiceModel->fnGetInvoiceDetails($InvoiceId);				
		echo Zend_Json_Encoder::encode($larrInvoiceDetails);		
	}
	
	public function fngetinvoicedetailseditAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$InvoiceId = ( int ) $this->_getParam ( 'InvoiceId' );
		$larrInvoiceDetails = $this->lobjInvoiceModel->fnGetInvoiceDetailsEdit($InvoiceId);				
		echo Zend_Json_Encoder::encode($larrInvoiceDetails);		
	}
	
	public function fngetinvoicenumberAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$StudentId = ( int ) $this->_getParam ( 'StudentId' );
		$larrInvoiceNumber = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjInvoiceModel->fngetinvoicenumberstudent($StudentId));
		echo Zend_Json_Encoder::encode($larrInvoiceNumber);
	}
	
	public function getrecieptnumAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$recieptnum = $this->_getParam('recieptnum');					
		$larrDetails = $this->lobjRecieptModel->fngetrecieptnum($recieptnum);	
		
		echo $larrDetails['RecieptNumber'];	
		
	}
	
	public function searchPayeeAction(){
		
		$this->_helper->layout()->disableLayout();
		$ebModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();
		$type = $this->_getParam('rcp_payee_type', null);
		$id = $this->_getParam('payee_id', null);
		$invoice_no = $this->_getParam('payee_invoice_no', false);
		$proforma_invoice_no = $this->_getParam('payee_proforma_no', false);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			 
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$result = null;
			$ebitList = false;
			
			if($formData['payee_id']!='' || $formData['payee_proforma_no']!='' || $formData['payee_invoice_no']!='' ){
			
			 
				if($formData['rcp_payee_type']==645){//applicant
					
					$select = $db->select()
								->from(array('ap'=>'applicant_profile'))
								->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at_pes_id', 'at_trans_id'))
								->join(array('app'=>'applicant_program'), 'app.ap_at_trans_id = at.at_trans_id', array('ap_prog_id'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=app.ap_prog_id',array('program_name'=>'p.ProgramName'));
			
					 
					if($formData['payee_id']!=''){
						$select->where("at.at_trans_id = '".$formData['payee_id']."'");
					}
					
					if($formData['payee_proforma_no']!=''){
						$select->join(array('pi'=>'proforma_invoice_main'), "pi.trans_id = at.at_trans_id", array());
						
						$select->where('pi.bill_number = ?', $formData['payee_proforma_no']);
					}
					
					if($formData['payee_invoice_no']!=''){
						$select->join(array('im'=>'invoice_main'), "im.trans_id = at.at_trans_id", array());
						
						$select->where('im.bill_number = ?', $formData['payee_invoice_no']);
					}
						
					 
					
					
					 
				}else
				if($formData['rcp_payee_type']==646){ //student

					$ebitList = $ebModel->getEbit($formData['payee_id']);

					$select = $db->select()
								->from(array('sp'=>'student_profile'))
								->join(array('sr'=>'tbl_studentregistration'), 'sr.sp_id = sp.id', array('sr.*'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('program_name'=>'p.ProgramName'));
//								->where('sr.ProfileStatus = 92');
			
					if($formData['payee_id']!=''){
						$select->where("sr.IdStudentRegistration = ? ",$formData['payee_id']);
					}
					
					if($formData['payee_proforma_no']!=''){
						$select->join(array('pi'=>'proforma_invoice_main'), "pi.IdStudentRegistration = sr.IdStudentRegistration", array());
						
						$select->where('pi.bill_number = ?', $formData['payee_proforma_no']);
					}
					
					if($formData['payee_invoice_no']!=''){
						$select->join(array('im'=>'invoice_main'), "im.IdStudentRegistration = sr.IdStudentRegistration", array());
						
						$select->where('im.bill_number = ?', $formData['payee_invoice_no']);
					}
			
			
				}
				$row = $db->fetchRow($select);
				
				
				//get default currency
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currencyData = $currencyDb->getDefaultCurrency();
				$defaultCurrency = $currencyData['cur_id'];
					
				if($row){
				
				
					if($type == 645){
						$result = array(
							'payee_id' => $row['at_pes_id'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['appl_id'],
							'trans_id' => $row['at_trans_id'],
						);
					}elseif($type == 646){
						$result = array(
							'payee_id' => $row['registrationId'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['IdStudentRegistration'],
							'trans_id' => $row['IdStudentRegistration'],
						);
					}
				
			
					//search proforma invoice
					//TODO:join left with table invoice
					$select_proforma_invoice = $db->select()
					->from(array('pi'=>'proforma_invoice_main'))
					->joinRight(array('pid'=>'proforma_invoice_detail'),'pid.proforma_invoice_main_id = pi.id', array('fee_item_description'=>'pid.fee_item_description'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = pi.currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where('pi.invoice_id = 0')
					->where("pi.status IN ('A','W')")  
					->group('pid.proforma_invoice_main_id');//to check proforma already generate to invoice;
					
					if($type == 645){
						$select_proforma_invoice->where('pi.appl_id = ?', $result['appl_id'])
								->where('pi.trans_id = ?', $result['trans_id']);
					}elseif($type == 646){
						$select_proforma_invoice->where('pi.IdStudentRegistration= ?', $result['trans_id']);
					}
			
					
					$proforma_invoice = $db->fetchAll($select_proforma_invoice);
				
					//proforma invoice detail
					if($proforma_invoice){
						
				
//						for($i=0; $i<sizeof($proforma_invoice); $i++){
						$i = 0;
						foreach($proforma_invoice as $proforma){
				
							$p_invoice = &$proforma_invoice[$i];
							$amountProforma = $proforma['bill_amount'];
							
							//get current currency rate
							$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
							$rateProforma = $currencyRateDb->getCurrentExchangeRate(2);//usd

							$cur_def = 0;
							
							if($defaultCurrency != $proforma['currency_id']){
								$cur_def = 1;
								$amountProforma = round($proforma['bill_amount'] * $rateProforma['cr_exchange_rate'],2);
								$proforma_invoice[$i]['amount_myr'] = $amountProforma;
								$proforma_invoice[$i]['amount_usd'] = $proforma['bill_amount'];
							
							}else{
								$cur_def = 0;
								$amountProforma = round($proforma['bill_amount'] / $rateProforma['cr_exchange_rate'],2);
								$proforma_invoice[$i]['amount_myr'] = $proforma['bill_amount'];
								$proforma_invoice[$i]['amount_usd'] = $amountProforma;
							}
							
							$proforma_invoice[$i]['amount_default'] = $proforma['bill_amount'];
							$proforma_invoice[$i]['cur_def'] = $cur_def;
							
				
							$select_proforma_invoice_detail = $db->select()
							->from(array('pid'=>'proforma_invoice_detail'))
							->where('pid.proforma_invoice_main_id = ?', $p_invoice['id']);
							
				
							$p_invoice['detail'] = $db->fetchRow($select_proforma_invoice_detail);
							$i++;
						}
							
						$result['proforma_invoice'] = $proforma_invoice;
							
					}
				
					//search invoice
					$select_invoice = $db->select()
					->from(array('i'=>'invoice_main'),array('*','bill_description','amount'=>"bill_amount",'paid'=>'bill_paid','balance'=>'bill_balance', 'invsem'=>'i.semester'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id=i.id',array())
					->join(array('cr'=>'tbl_currency_rate'),'i.exchange_rate = cr.cr_id',array('cr_exchange_rate'))
					->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=i.id AND ivs.invoice_detail_id=ivd.id',array())
					->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=ivs.subject_id',array('SubCode'))
					->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = ivd.fi_id',array('fi_name'))
					->where("i.status = 'A'")  
					//->where("i.bill_balance > '0.00'")
					->group('i.id');
					//->group('pid.invoice_main_id');//remove paid invoice

					if($type == 645){
						$select_invoice->where('i.appl_id = ?', $result['appl_id'])
								->where('i.trans_id = ?', $result['trans_id']);
					}elseif($type == 646){
						$select_invoice->where('i.IdStudentRegistration= ?', $result['trans_id']);
					}
					

					$invoice = $db->fetchAll($select_invoice);

					if ($invoice){
						foreach ($invoice as $invoiceKey => $invoiceLoop){
							$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
							$balance = $paModel->getBalanceMain($invoiceLoop['id']);

							if ($balance['bill_balance'] <= 0){
								unset($invoice[$invoiceKey]);
							}else{
								//calculate ebit discount
								$ebAmount = 0.00;
								if ($ebitList){
									foreach ($ebitList as $ebitLoop){
										if ($ebitLoop['eb_semester']==$invoiceLoop['invsem']) {
											$invDtl = $ebModel->getInvDtl($invoiceLoop['id']);

											if ($invDtl) {
												foreach ($invDtl as $invDtlLoop) {
													$ebitItem = $ebModel->checkEbitItem($ebitLoop['eb_discount_type'], $invDtlLoop['fi_id']);

													if ($ebitItem) {
														if ($ebitItem['CalculationMode'] == 1) { //amount
															$ebAmt = $ebitItem['Amount'];
														} else { //percentage
															$ebAmt = (($ebitItem['Amount'] / 100) * $invDtlLoop['amount']);
														}
														$ebAmount = $ebAmount + $ebAmt;
													}
												}
											}
										}
									}
								}

								$invoice[$invoiceKey]['ebAmount'] = number_format($ebAmount, 2, '.', '');
								$invoice[$invoiceKey]['bill_paid'] = $balance['bill_paid'];
								$invoice[$invoiceKey]['bill_balance'] = $balance['bill_balance'];
							}
						}
					}

					$totalMyr=0;
					$totalUsd=0;
					$m=0;
					foreach($invoice as $inv){
						$result['invoice'][$m] = $inv;

						$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
						$balance = $paModel->getBalanceMain($inv['id']);

						$inv['bill_paid'] = $balance['bill_paid'];
						$inv['bill_balance'] = $balance['bill_balance'];
						$inv['paid'] = $balance['bill_paid'];
						$inv['balance'] = $balance['bill_balance'];
						
						//get current currency rate
						$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
						

						$cur_def = 0;
						$exchange_rate = 0;
						
//						$rate = $currencyRateDb->getCurrentExchangeRate(2);
						$rate = $inv;
						if($inv['currency_id'] == 2){ //usd

							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							$rate = $curRateDB->getRateByDate($inv['currency_id'], $inv['invoice_date']);

							$cur_def = 0;
							$total_invoice_amount_default_currency = round($inv['bill_amount'] * $rate['cr_exchange_rate'],2);
							$result['invoice'][$m]['amount_myr'] = $total_invoice_amount_default_currency;
							$result['invoice'][$m]['amount_usd'] = $inv['amount'];
							$totalMyr += round($inv['bill_balance'] * $rate['cr_exchange_rate'],2);
							$totalUsd +=  $inv['bill_balance'] ;
							$exchange_rate = $rate['cr_exchange_rate'];
							
							/*$rate = $currencyRateDb->getData($inv['exchange_rate']);
							$cur_def = 1;
							$total_invoice_amount_default_currency = round($inv['amount'] * $rate['cr_exchange_rate'],2);
							$result['invoice'][$m]['amount_myr'] = $total_invoice_amount_default_currency;
							$result['invoice'][$m]['amount_usd'] = $inv['amount'];
							$totalMyr += round($inv['bill_balance'] * $rate['cr_exchange_rate'],2);
							$totalUsd += $inv['bill_balance'];
							$exchange_rate = $inv['exchange_rate'];*/
						
						}else{ //myr
							$cur_def = 0;
							$total_invoice_amount_default_currency = round($inv['bill_amount'] / $rate['cr_exchange_rate'],2);
							$result['invoice'][$m]['amount_myr'] = $inv['amount'];
							$result['invoice'][$m]['amount_usd'] = $total_invoice_amount_default_currency;
							$totalMyr += $inv['bill_balance'];
							$totalUsd +=  round($inv['bill_balance'] / $rate['cr_exchange_rate'],2);
							$exchange_rate = $rate['cr_exchange_rate'];
						}
						/*else{
							$rate = $currencyRateDb->getCurrentExchangeRate(2);
							$cur_def = 0;
							$total_invoice_amount_default_currency = round($inv['bill_amount'] / $rate['cr_exchange_rate'],2);
							$result['invoice'][$m]['amount_myr'] = $inv['amount'];
							$result['invoice'][$m]['amount_usd'] = $total_invoice_amount_default_currency;
							$totalMyr += $inv['bill_balance'];
							$totalUsd +=  round($inv['bill_balance'] / $rate['cr_exchange_rate'],2);
							$exchange_rate = $rate['cr_exchange_rate'];
						}*/

						$result['invoice'][$m]['cur_def'] = $cur_def;
						$result['invoice'][$m]['amount_default'] = $inv['amount'];
						$result['invoice'][$m]['cur_exchange_rate'] = $exchange_rate;
						
						$result['invoice'][$m]['fee_item_description'] = ($inv['SubCode']) ? $inv['SubCode'].' - ' : '';
						$result['invoice'][$m]['fee_item_description'] .=  $inv['fi_name'];
						$result['invoice'][$m]['paid'] = $balance['bill_paid'];
						$result['invoice'][$m]['balance'] = number_format(str_replace(',', '', $balance['bill_balance'])-$inv['ebAmount'], 2, '.', '');
						$m++;
					}
				
					
				
				}
				
				$result['inv_total_myr'] = $totalMyr;
				$result['inv_total_usd'] = $totalUsd;
				
				//get current currency rate
				$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
				$dataRate  = $currencyRateDb->getCurrentExchangeRate(2);		
				$result['current_rate'] = $dataRate['cr_exchange_rate'];
				
		
			}

			//var_dump($result);
			
//			echo "<pre>";
//			print_r($result);
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($result);
				
			echo $json;
			exit();
		
		}
	}
	
	public function viewProformaDetailAction(){
		
		$id = $this->_getParam('id', null);
		
		if($id == null){
			throw new Exception('Not enough parameter');
		}
		
		if($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
		
		
		$proformaInvoiceMainDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
		
		$proforma = $proformaInvoiceMainDb->getDetail($id);
		
		if($proforma){
			$proformaInvoiceDetailDb = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
			$proforma['fee_item'] = $proformaInvoiceDetailDb->getDetail($proforma['id']);
		}
		
		$this->view->proforma = $proforma;

	}
	
	public function viewInvoiceDetailAction(){
	
		$id = $this->_getParam('id', null);
	
		if($id == null){
			throw new Exception('Not enough parameter');
		}
	
		if($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
	
	
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	
		$invoice = $invoiceMainDb->getDetail($id);
	
		if($invoice){
			$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
			$invoice['fee_item'] = $invoiceDetailDb->getDetail($invoice['id']);
		}
		
		
		$this->view->invoice = $invoice;
	}
	
	public function selectInvoiceDetailAction(){
	
		$id = $this->_getParam('id', null);
		$curId = $this->_getParam('curId', null);
	
		if($id == null){
			throw new Exception('Not enough parameter');
		}
	
		if($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
		}
	
	
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
	
		$invoice = $invoiceMainDb->getDetail($id);

		$ebModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();

		if($invoice){
			$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
			$fee_item = $invoiceDetailDb->getInvoiceDetail($invoice['id'],0);
			$ebitList = $ebModel->getEbit($invoice['IdStudentRegistration']);

			if ($fee_item){
				foreach ($fee_item as $fee_item_key => $value){
					$pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
					$balance = $pamodel->getBalanceDtl($value['id']);

					if ($balance['bill_balance'] <= 0.00){
						unset($fee_item[$fee_item_key]);
					}else{
						$ebAmount = 0.00;
						if ($ebitList){
							foreach ($ebitList as $ebitLoop) {
								if ($ebitLoop['eb_semester'] == $value['invsem']) {
									$ebitItem = $ebModel->checkEbitItem($ebitLoop['eb_discount_type'], $value['fi_id']);

									if ($ebitItem) {
										if ($ebitItem['CalculationMode'] == 1) { //amount
											$ebAmt = $ebitItem['Amount'];
										} else { //percentage
											$ebAmt = (($ebitItem['Amount'] / 100) * $value['amount']);
										}

										$ebAmount = $ebAmount + $ebAmt;
									}

									$fee_item[$fee_item_key]['ebId'] = $ebitLoop['eb_id'];
									$fee_item[$fee_item_key]['ebType'] = $ebitLoop['eb_discount_type'];
									$fee_item[$fee_item_key]['ebName'] = $ebitLoop['eb_name'];
								}else{
									$fee_item[$fee_item_key]['ebId']=0;
									$fee_item[$fee_item_key]['ebType']=0;
									$fee_item[$fee_item_key]['ebName']='';
								}
							}
						}else{
							$fee_item[$fee_item_key]['ebId']=0;
							$fee_item[$fee_item_key]['ebType']=0;
							$fee_item[$fee_item_key]['ebName']='';
						}

						$fee_item[$fee_item_key]['ebAmount'] = number_format($ebAmount, 2, '.', '');
						$fee_item[$fee_item_key]['balance']=number_format(str_replace(',', '', $balance['bill_balance'])-$ebAmount, 2, '.', '');
						$fee_item[$fee_item_key]['balanceinv']=number_format(str_replace(',', '', $balance['bill_balance']), 2, '.', '');
					}
				}
			}

			//get applicant program
			$db = Zend_Db_Table::getDefaultAdapter();
			/*$select_program = $db->select()
								->from(array('ap'=>'applicant_program'))
								->where("ap.ap_at_trans_id = ?", (int)$invoice['trans_id']);
				
			$row_program = $db->fetchRow($select_program);
				
			$program_id = null;
			if($row_program){
				$program_id = $row_program['ap_prog_id'];
			}else{
				throw new Exception('No applicant program data');
			}*/
			//loop fee item
//			 $newArray = array();
//			 
//			 $newArray = $invoice;
			//$i=0;
			//for ($i=0; $i<sizeof($fee_item); $i++){
			foreach($fee_item as $i => $fi){
				$invoice['details'][$i] = $fee_item[$i];
				$currencyPaid = $curId;
				$currencyItem = $fee_item[$i]['cur_id'];
				
				if($currencyPaid == $currencyItem){
					$invoice['details'][$i]['amount_default']  = $fee_item[$i]['balance'];
				}else{
					if($currencyPaid == 2){
						$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
						$dataCur = $curRateDB->getCurrentExchangeRate(2);
						$amountNew = round($fee_item[$i]['balance'] / $dataCur['cr_exchange_rate'],2);
						$invoice['details'][$i]['amount_default'] = $amountNew;
					}else if($currencyPaid == 1){
						$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
						$dataCur = $curRateDB->getData($fee_item[$i]['exchange_rate']);
						$amountNew = round($fee_item[$i]['balance'] * $dataCur['cr_exchange_rate'],2);
						$invoice['details'][$i]['amount_default'] = $amountNew;
					}
				}
					
					
				
				/*if($curId == $fee_item[$i]['cur_id']){
					$invoice['details'][$i]['amount_default'] = $fee_item[$i]['balance'];
				}else{
					if($curId == 1){
						
						$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
						$dataCur = $curRateDB->getData($fee_item[$i]['exchange_rate']);
						$amountNew = round($fee_item[$i]['balance'] * $dataCur['cr_exchange_rate'],2);
							
						$invoice['details'][$i]['amount_default'] = $amountNew;
					}else if($curId == 2){
						$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
						$dataCur = $curRateDB->getCurrentExchangeRate(2);
						$amountNew = round($fee_item[$i]['balance'] / $dataCur['cr_exchange_rate'],2);
						
						$invoice['details'][$i]['amount_default'] = $amountNew;
					
					}
				}*/
			
				//add subject list for fee item that using subject as multiplication
				//$fee_item[$i] = $fee_item;
				
			/*$select_subject = $db->select()
									->from(array('is'=>'invoice_subject'))
									->join(array('subm'=>'tbl_subjectmaster'), 'subm.IdSubject = is.subject_id', array('IdSubject','SubjectName','SubCode','CreditHours'))
									->where("is.invoice_main_id = ?", $fee_item[$i]['invoice_main_id'])
									->where("is.invoice_detail_id = ?", $fee_item[$i]['id']);
		
				$subject_list = $db->fetchAll($select_subject);
				
				if($subject_list){
					$m=0;
					foreach($subject_list as $subj){
						$invoice['details'][$i]['subject'][$m] = $subj;
						
						if($currencyPaid == $currencyItem){
							$invoice['details'][$i]['subject'][$m]['amount_default']  = $subj['balance'];
						}else{
							if($currencyPaid == 2){
								$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
								$dataCur = $curRateDB->getCurrentExchangeRate(2);
								$amountNew = round($subj['balance'] / $dataCur['cr_exchange_rate'],2);
								$invoice['details'][$i]['subject'][$m]['amount_default'] = $amountNew;
							}else if($currencyPaid == 1){
								$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
								$dataCur = $curRateDB->getData($fee_item[$i]['exchange_rate']);
								$amountNew = round($subj['balance'] * $dataCur['cr_exchange_rate'],2);
								$invoice['details'][$i]['subject'][$m]['amount_default'] = $amountNew;
							}
						}
						
						
						/*if($curId == 1){
							$invoice['details'][$i]['subject'][$m]['amount_default'] = $subj['balance'];
						}else{
							//get current currency rate
							$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
							$rateProforma = $currencyRateDb->getCurrentExchangeRate(2);//usd
							
							$amountSubject = round($subj['balance'] / $rateProforma['cr_exchange_rate'],2);
							$invoice['details'][$i]['subject'][$m]['amount_default'] = $amountSubject;
						
						}
						
						
						$m++;
					}
				
				}*/
			}
			//$i++;
//			exit;

			//echo "<pre>";
			//print_r($invoice);
			$invoice['fee_item'] = $invoice;
		}

		$this->view->invoice = $invoice;
	}
	
	/*
	 * generate invoice from proforma invoice (ajax)
	 * 
	 * code return:
	 * -1: having non-invoice in fee item
	 *  0: failed
	 *  1: success
	 */
	
	public function generateInvoiceFromProformaAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$result = array('code'=>0);
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			$idArray = $formData['id'];
			
			
			//check if having fee_item of non-invoice
			$db = Zend_Db_Table::getDefaultAdapter();
			
			foreach($idArray as $id){
				
				$selectData = $db->select()
								->from(array('id'=>'proforma_invoice_detail'))
								->join(array('fi'=>'fee_item'), 'fi.fi_id = id.fi_id')
								->where('id.proforma_invoice_main_id = ?', (int)$id)
								->where('fi.fi_non_invoice = 1');
				
				$row = $db->fetchAll($selectData);
				
				if($row){
					
					$result['code'] = -1;
					
					$result['non_invoice'] = $row;
					
				}else{
					//generate invoice from proforma
					$invoice_helper = new icampus_Function_Studentfinance_Invoice();
					
					$auth = Zend_Auth::getInstance();
					
					try{
						$status = $invoice_helper->generateInvoiceFromProforma($id,  $auth->getIdentity()->id);
						$result['code'] = 1;
					}catch (Exception $e){
						$result['code'] = 2;
						$result['msg'] = "Error generate invoice: " . $e->getMessage();
					}
					
				}
			}
			
			
		}
		
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
			
		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
	}
	
	/*
	 *  Get current receipt seq no
	 */
	private function getCurrentReceiptNoSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 5")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		if($row2){
			$r_num = $row2['ReceiptPrefix'].$row2['ReceiptSeparator'].date('Y').$row2['ReceiptSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	/*
	 * Get Bill no from mysql function
	*/
	private function getReceiptNoSeq(){
	
		$seq_data = array(
				5,
				date('Y'),
				0,
				0,
				0
		);
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
	
		return $seq['receipt_no'];
	}
	
	private function updateChecklist(&$arrayAds){
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
			
		$tmp = array();
		$n=0;
		foreach($arrayAds as $arg)
		{
		    $tmp[$arg['id']]['tot'][$n] = $arg['total'];
		    $tmp[$arg['id']]['bal'][$n] = $arg['balance'];
		    $tmp[$arg['id']]['cur'] = $arg['cur'];
		    $tmp[$arg['id']]['receipt_no'] = $arg['receipt_no'];
		    $n++;
		}
		
		$output = array();
		$m=0;
		foreach($tmp as $type => $labels)
		{
			 $output[$m] = array(
		        'id' => $type,
		        'total' => number_format(array_sum($labels['tot']),2),
		    	'balance' => number_format(array_sum($labels['bal']),2),
				 'cur'=>$labels['cur'],
				 'receipt'=>$labels['receipt_no']
			    );
		
			   $m++;
		}

		//update applicant document type checklist	
		foreach($output as $out){
			if($out['balance'] == '0'){
				$docStatus = array(
					'ads_status'=>4,//completed
					'ads_comment'=> $out['receipt'].", ".$out['cur']."".$out['total'] ,
					'ads_modBy'=>$getUserIdentity->id,
					'ads_modDate'=>date('Y-m-d H:i:s'),
				);
				
//				echo "<pre>";
//				print_r($output);
				$appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
				$appDocStatusDB->update($docStatus, array('ads_id =?'=>$out['id']) );
			}
		}
	}
	
	public function viewReceiptDetailAction(){
		
		$id = $this->_getParam('id', null);
		
		if($id == null){
			throw new Exception('Not enough parameter');
		}
		
		$this->view->title = $this->view->translate("Receipt - Detail");
		
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		$receipt = $receiptDb->getData($id);
		$this->view->receipt = $receipt[0];
		
		//payment
		$paymentDb = new Studentfinance_Model_DbTable_Payment();
		$payment = $paymentDb->getReceiptPayment($id);
		$this->view->payment = $payment;
		
		//invoice
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoice_list = $invoiceMainDb->getInvoiceFromReceipt($id);
		$this->view->invoice_list = $invoice_list;
		
//		echo "<pre>";
//		print_r($invoice_list);
//		exit;
		
		//non-invoice
		$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
		$non_invoice_list = $nonInvoiceDb->getNonInvoiceFromReceipt($id);
		$this->view->non_invoice_list = $non_invoice_list;
		
	}
	
	public function printAction(){
		
		$id = $this->_getParam('id', null);
		$type = $this->_getParam('type', null);
		//var_dump($type);
		//receipt
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		$receipt = $receiptDb->getData($id);
		
		$receipt = $receipt[0];
		
		//echo "<pre>";
		//print_r($receipt); //exit;
		
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
			
		if($type == 645){
			$appTrxId = $receipt['rcp_trans_id'];
			
			$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
			$txnData= $txnDb->fetchRow( array('at_trans_id = ?'=> $appTrxId) );
			
			$txnProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
			$txnProfile= $txnProfileDb->fetchRow( array('appl_id = ?'=> $txnData['at_appl_id']) );
			
			$txnProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
			$txnProgram = $txnProgramDb->fetchRow( array('ap_at_trans_id = ?'=> $appTrxId) );
			
			$programSchemeId = $txnProgram['ap_prog_scheme'];
			$stdCtgy = $txnProfile['appl_category'];
		
		}elseif($type == 646){
			$txnStudentDb = new Registration_Model_DbTable_Studentregistration();
			$txnProfile= $txnStudentDb->getData($receipt['rcp_idStudentRegistration']);
			$appTrxId = $txnProfile['sp_id'];
			
			$programSchemeId = $txnProfile['IdProgramScheme'];
			$stdCtgy = $txnProfile['appl_category'];
		}
		
		$currency = new Zend_Currency(array('currency'=>$receipt['cur_code']));
		
		//payment
		$paymentDb = new Studentfinance_Model_DbTable_Payment();
		$payment = $paymentDb->getReceiptPayment($id);
		
		//invoice
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoice_list = $invoiceMainDb->getInvoiceFromReceipt($id);
		
		$inv = array();
		$total_payment = 0.00;
		
		if($invoice_list){
			
			foreach ($invoice_list as $invoice){
				$iv = array(
						'invoice_no' => $invoice['bill_number'],
						'amount' => 0,
						'currency' => $invoice['cur_symbol_prefix']
					);
				
				foreach ($invoice['receipt_invoice'] as $ri){
					$iv['amount'] += $ri['rcp_inv_amount'];
					$total_payment += $ri['rcp_inv_amount'];
					
					$feeID = $invoice['fi_id'];
					$receipt_no = $ri['rcp_inv_rcp_no'];
					$bill_paid = $ri['rcp_inv_amount'];
					$currencyPaid = $ri['cur_prefix_inv'];
				}

				$inv[] = $iv;
			}
		}
		
		//non-invoice
		$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
		$non_invoice_list = $nonInvoiceDb->getNonInvoiceFromReceipt($id);
		
		if( $non_invoice_list ){
			
			foreach ($non_invoice_list as $non_invoice){
				$iv = array(
						'invoice_no' => $non_invoice['fi_name'],
						'amount' => $non_invoice['ni_amount'],
						'currency' => $non_invoice['cur_symbol_prefix']
				);
				
				$total_payment += $non_invoice['ni_amount'];
			
				$inv[] = $iv;
			}
		}

		
//		$fieldValues['[Date]'] = date('j F Y', strtotime($receipt['rcp_date']));
		$fieldValues['[Receipt No]'] = $receipt['rcp_no'];
		
		$fieldValues['[Data Loop]'] = '<table width="100%" cellpadding="2" cellspacing="0" style="border:1px solid #000000;"><tr><th width="50%" style="border:1px solid #000000;">Invoice No.</th><th style="border:1px solid #000000;">Amount Paid</th></tr>';
		for ($i=0; $i<sizeof($inv); $i++){
			$invoice = $inv[$i];
			
			$fieldValues['[Data Loop]'] .= '<tr>';
			$fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">'.$invoice['invoice_no'].'</td>';
			$fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">'.$currency->toCurrency($invoice['amount'], array('display'=>false)).'&nbsp;</td>';
			$fieldValues['[Data Loop]'] .= '</tr>';
		}

		//Excess Payment
		if ( $receipt['rcp_adv_payment_amt'] > 0 )
		{
			$fieldValues['[Data Loop]'] .= '<tr>';
			$fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">Excess Payment</td>';
			$fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">'.$currency->toCurrency($receipt['rcp_adv_payment_amt'], array('display'=>false)).'&nbsp;</td>';
			$fieldValues['[Data Loop]'] .= '</tr>';

			$total_payment = $total_payment + $receipt['rcp_adv_payment_amt'];
		}

		//
		$fieldValues['[Data Loop]'] .= '<tr><td style="border:1px solid #000000;"><b>Total</b></td><td align="right" style="border:1px solid #000000;"><b>'.$currency->toCurrency($total_payment, array('currency'=>$receipt['cur_code'])).'</b>&nbsp;</td></tr>';
		$fieldValues['[Data Loop]'] .= '</table>';
		
		//number to words
		$total_amount_in_text =  convert_number_to_words($total_payment);
		

		//invoice & non-invoice data
		$fieldValues['[Amount in Text]'] = $receipt['cur_symbol_prefix'].": ".$total_amount_in_text." Only";
		
		
		$templateDb = new Communication_Model_DbTable_Template();
		$template = $templateDb->getTemplateContent(22, $this->view->locale);
		
		$templateCMS = new Cms_TemplateTags();
		
		if($type == 645){
        	$html = $templateCMS->parseContent($appTrxId, $template['tpl_id'], $template['tpl_content']);
		}elseif($type == 646){
			$html = $templateCMS->parseTag($appTrxId, $template['tpl_id'], $template['tpl_content']);
		} 
        
		
		//replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);
		}
		
		//echo $html;
		//exit;
		$option = array(
			'content' => $html,
			'save' => false,
			'file_name' => 'receipt',
			'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 180; 
					  $img_h = 59;
                      $pdf->image("images/logo_text_high.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
			'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
		);
		
		$pdf = generatePdf($option);
		
		exit;
	}

	public function approveAction(){
		
		$id = $this->_getParam('id', null);
		$type = $this->_getParam('type', null);
		
		//receipt
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		$receipt = $receiptDb->getData($id);
		//var_dump($receipt); exit;

		$receipt = $receipt[0];
		
		
		
		/*echo "<pre>";
		print_r($receipt);
		exit;*/
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
		
		$appStatus = 0;
			
		if($type == 645){
			$appTrxId = $receipt['rcp_trans_id'];
		
			
			$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
			$txnProfile = $applicantProfileDb->getApplicantRegistrationDetail($appTrxId);
			
			$appStatus = $txnProfile['at_status'];
			
		
			$programSchemeId = $txnProfile['ap_prog_scheme'];
			$stdCtgy = $txnProfile['appl_category'];
			$txnProfile['IdProgramScheme'] = $programSchemeId;
		}elseif($type == 646){
			$appTrxId = $receipt['rcp_idStudentRegistration'];
			$appStatus = 601; // enrolled
			
			$txnStudentDb = new Registration_Model_DbTable_Studentregistration();
			$txnProfile= $txnStudentDb->getData($appTrxId);
			
			$programSchemeId = $txnProfile['IdProgramScheme'];
			$stdCtgy = $txnProfile['appl_category'];
		}
//		echo $appStatus;


		//get current semester
		$semesterDb = new Records_Model_DbTable_SemesterMaster();
		$semesterInfo = $semesterDb->getStudentCurrentSemester($txnProfile);
		
		
		/*echo "<pre>";
		print_r($semesterInfo);*/
		
		/*
		 * knockoff invoice
		 */
		
		//get default currency
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$currencyData = $currencyDb->getDefaultCurrency();
		$defaultCurrency = $currencyData['cur_id'];
				
		if($receipt['rcp_status'] == 'ENTRY'){
			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
			$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
			$ebModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();
			
			$gainloss = $receipt['rcp_gainloss'];
			$gainlossAmount = $receipt['rcp_gainloss_amt'];
			$amountGainLoss = 0;
			
			
			$totalAmount = 0.00;
			$total_advance_payment_amount = 0;
			$total_payment_amount = $receipt['rcp_amount'];
			$total_paid_receipt = 0;
			$paidReceiptInvoice = 0;

			//ebip
			if ($receipt['ebip']!=null){
				$ebip = explode(',', $receipt['ebip']);
				//var_dump($ebip);
				if (count($ebip) > 0){
					foreach ($ebip as $ebipLoop){
						$ebModel->activeteEbipDiscount($ebipLoop);
					}
				}
			}
			
			if($receipt['receipt_invoice']){
			//get receipt invoice
				foreach ($receipt['receipt_invoice'] as $inv){
					$invID = $inv['rcp_inv_invoice_id'];
					$invDetID = $inv['rcp_inv_invoice_dtl_id'];
	//				$paidReceipt =  $inv['rcp_inv_amount'];
					$rcpCurrency = $inv['rcp_inv_cur_id'];
					
					$invDetData = $invoiceDetailDb->getData($invDetID);
					
	//				echo "<pre>";
	//				print_r($invDetData);
	//				exit;
					
					$invCurrency = $invDetData['cur_id'];
					
					$currencyDb = new Studentfinance_Model_DbTable_Currency();
					$currency = $currencyDb->fetchRow('cur_id = '.$invCurrency)->toArray();
	
					/*echo "<pre>";
					print_r($currency);
					exit;*/
	//				$amountNew = $paidReceipt;
	
					$amountDefault = $invDetData['balance'];
					
					$paidReceiptInvoice = $inv['rcp_inv_amount'];
					
					if($invCurrency == $rcpCurrency){
						$paidReceipt = $inv['rcp_inv_amount'];
						
					}else{
						$paidReceipt = $inv['rcp_inv_amount_default_currency'];
					}
							
					$balance  = $amountDefault  - $paidReceipt;
					
					//update invoice details
					$dataInvDetail = array(
						'paid'=> $paidReceipt +  $invDetData['paid'] ,
						'balance' => $balance,
					);
					
					
					
	//				echo "<pre>";
	//				print_r($dataInvDetail);
					$invoiceDetailDb->update($dataInvDetail, array('id =?'=>$invDetID) );
					
					$totalAmount += $paidReceiptInvoice;
					
					//update invoice main
					$invMainData = $invoiceMainDb->getData($invID);
						
					$balanceMain = ($invMainData['bill_balance']) - ($paidReceipt);
					$paidMain = ($invMainData['bill_paid']) + ($paidReceipt);
					
					//update invoice main
					$dataInvMain = array(
						'bill_paid'=> $paidMain,
						'bill_balance' => $balanceMain,
						'upd_by'=>$getUserIdentity->id,
			            'upd_date'=>date('Y-m-d H:i:s')
					);
					
	//				echo "<pre>";
	//				print_r($dataInvMain);
					$invoiceMainDb->update($dataInvMain, array('id =?'=>$invID) );
					
					/*
					 * todo : receipt_subject
					 * if($inv['subject']){
						foreach($inv['subject'] as $sbj){
							
							$dataSubject = array(
								'paid'=> $paidMain,
								'balance' => $balanceMain,
								'upd_by'=>$getUserIdentity->id,
					            'upd_date'=>date('Y-m-d H:i:s')
							);
							$invoiceSubjectDb->update($dataInvMain, array('id =?'=>$invID) );
					
						}
						
					}
					*/
					
				}	
			}
			
			
			/*
			 * get currenct exchange rate
			 * calculate advance payment amount
			 * advance payment convert to myr
			 * calculate gain/loss
			 */
			
			$exchangeCurrent = 1;
			$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
			if($receipt['rcp_cur_id'] != 1){ 
				$dataCur = $currencyRateDb->getCurrentExchangeRate($receipt['rcp_cur_id']);
				$exchangeCurrent = $dataCur['cr_exchange_rate'];
				
			}

			if( $total_payment_amount > $totalAmount ){
				$total_advance_payment_amount = $total_payment_amount - ($totalAmount);

				$total_advance_payment_amount = $total_advance_payment_amount * $exchangeCurrent;
				$total_payment_amount = $total_payment_amount * $exchangeCurrent;

				$amountGainLoss = $total_advance_payment_amount;

				if($gainloss == 'gain'){
					$amountGainLoss = $total_advance_payment_amount - $gainlossAmount;
				}elseif($gainloss == 'loss'){
					$amountGainLoss = $total_advance_payment_amount + $gainlossAmount;
				}

				$total_advance_payment_amount = $amountGainLoss;

			}
			

			
			//add advance payment (if any)
			if($total_advance_payment_amount > 0){
				$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				
					$bill_no =  $this->getBillSeq(8, date('Y'));
					
					$advance_payment_data = array(
						'advpy_fomulir' => $bill_no,
						'advpy_appl_id' => $receipt['rcp_appl_id'],
						'advpy_trans_id' => $receipt['rcp_trans_id'],
						'advpy_idStudentRegistration' => $receipt['rcp_idStudentRegistration'],
						'advpy_sem_id'=>$semesterInfo['IdSemesterMaster'],
						'advpy_rcp_id' => $receipt['rcp_id'],
						'advpy_description' => 'Advance payment from receipt no:'.$receipt['rcp_no'],
						'advpy_cur_id' => 1,//always myr
						'advpy_amount' => $total_advance_payment_amount,
						'advpy_total_paid' => 0.00,
						'advpy_total_balance' => $total_advance_payment_amount,
						'advpy_status' => 'A',
						'advpy_date' => $receipt['rcp_receive_date'],
						
					);
				
				
				$advPayID = $advancePaymentDb->insert($advance_payment_data);
				
				$advance_payment_det_data = array(
					'advpydet_advpy_id' => $advPayID,
					'advpydet_total_paid' => 0.00
				);
				
				$advancePaymentDetailDb->insert($advance_payment_det_data);

				//receipt update advpayment
				$receiptDb->update(array('rcp_adv_payment_amt'=>$total_advance_payment_amount, 'rcp_adv_payment_amt_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_advance_payment_amount, $receipt['rcp_cur_id']) ), 'rcp_id = '.$receipt['rcp_id']);
			}
		}
		
		/*knockoff end*/

		/*
		 * checking applicant document exist or not
		 * if not exist -> insert all
                 * 
                 * reference izham
		 */
		if($type == 645){
		$fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
		//$checkDocExist = $fsProgramDB->getApplicantDocumentList($appTrxId);
		//$totalDocExist = count($checkDocExist);
                $checklistVerificationDB->deleteStatusByTrx($appTrxId);
		
		//get applicant document status

                $docChecklist = $checklistVerificationDB->fnGetDocCheckList($programSchemeId, $stdCtgy, $appTrxId);

                foreach($docChecklist as $dataDoc){

                    $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$appTrxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);

                    if($list){
                        foreach($list as $listLoop){
                            $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($appTrxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                            $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($appTrxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                            if (!$adsCheck){ //update
                                $data = array(
                                    'ads_txn_id'=>$appTrxId,
                                    'ads_dcl_id'=>$dataDoc['dcl_Id'],
                                    'ads_ad_id'=>$adId,
                                    //'ads_appl_id'=>'',
                                    //'ads_confirm'=>$check,
                                    'ads_section_id'=>$dataDoc['dcl_sectionid'],
                                    'ads_table_name'=>$dataDoc['table_name'],
                                    'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                                    'ads_status'=>0,
                                    'ads_comment'=>'',
                                    'ads_createBy'=>$getUserIdentity->id,
                                    'ads_createDate'=>date('Y-m-d H:i:s')
                                );
                                $checklistVerificationDB->insert($data);
                            }
                        }
                    }
                }
		}
		
		/*end*/
		
		$currency = new Zend_Currency(array('currency'=>$receipt['cur_code']));
		
		//payment
		$paymentDb = new Studentfinance_Model_DbTable_Payment();
		$payment = $paymentDb->getReceiptPayment($id);
		
		//invoice
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoice_list = $invoiceMainDb->getInvoiceFromReceipt($id);
		
		$inv = array();
		$total_payment = 0.00;
		$total_balance = 0.00;
		
		if($invoice_list){
			
//			echo "<pre>";
//			print_r($invoice_list);
//			exit;
			
			foreach ($invoice_list as $invoice){
				$iv = array(
						'invoice_no' => $invoice['bill_number'],
						'amount' => 0,
						'currency' => $invoice['cur_symbol_prefix']
					);
				
				foreach ($invoice['receipt_invoice'] as $ri){
					$iv['amount'] += $ri['rcp_inv_amount'];
					$total_payment += $ri['rcp_inv_amount'];
					$total_balance += $ri['balance'];
					
					$feeID = $ri['fi_id'];
					$receipt_no = $ri['rcp_inv_rcp_no'];
					$bill_paid = $ri['bill_amount'];
//                   $bill_paid = $invoice['bill_amount']; // updated by su 1/9/2015
                    $bill_paid = $ri['paid'];
					$currencyPaid = $ri['cur_symbol_prefix'];
					$currencyInv = $invoice['cur_symbol_prefix'];
					if($type == 645){
					/*
					 * get fee structure program item
					 * update docuemnt checklist status
					 */
									
					$fsProgram = $fsProgramDB->getApplicantDocumentType($appTrxId,$feeID);
					$newArray1 = array();	
					$ads_id = $fsProgram['ads_id'];
					if(isset($ads_id)){
						$arrayAds [] = array(
							'id'=>$ads_id,
							'total'	=>$bill_paid,
							'balance'=>$total_balance,
							'cur'=>$currencyInv,
							'receipt_no'=>$receipt_no
						);
					}
					array_push($newArray1,$arrayAds);
					
					}
					
		
				}
				
			
				
				$inv[] = $iv;
			}
			if($type == 645){
				//update checklist
				
				$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
				$paymentClass->updateChecklist($arrayAds);
				
//				$this->updateChecklist($arrayAds);
			}
//					exit;
		}
                
                //end reference
		
		//non-invoice
		$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
		$non_invoice_list = $nonInvoiceDb->getNonInvoiceFromReceipt($id);
		
		if( $non_invoice_list ){
			
			foreach ($non_invoice_list as $non_invoice){
				$iv = array(
						'invoice_no' => $non_invoice['fi_name'],
						'amount' => $non_invoice['ni_amount'],
						'currency' => $non_invoice['cur_symbol_prefix']
				);
				
				$total_payment += $non_invoice['ni_amount'];
			
			
				$inv[] = $iv;
			}
		}
		
		if($type == 645){
			/*
			 * update applicant_transaction status
			 * only status COmplete (4) baru update
			 */
			
			$defModelDB = new App_Model_General_DbTable_Definationms();
			
			$docList = $checklistVerificationDB->fnGetChecklistStatus($appTrxId);
			if (count($docList) > 0){
                    $i=0;
                    foreach($docList as $docLoop){
                        if(($docLoop['ads_status']==4 || $docLoop['ads_status']==5) && $docLoop['dcl_mandatory']==1){
                            $i++;
                        }
                    }
                    //var_dump(count($docList));var_dump($i); exit;
                    if (count($docList)==$i){
                        
                        $getStatus = $defModelDB->getIdByDefType('Status', 'Complete');
                        
                        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                       	if (($oldStatus == '595')){//($oldStatus == '592') || hide in 21/08/2015
                        
                            $data = array(
                                'at_status'=>$getStatus['key']
                            );
                            $checklistVerificationDB->updateAppTrans($data, $appTrxId);

                            $dataHistory = array(
                                'ash_trans_id'=>$appTrxId,
                                'ash_status'=>$data['at_status'],
                                'ash_oldStatus'=>$oldStatus,
                                'ash_changeType'=>1,
                                'ash_updUser'=>$getUserIdentity->id,
                                'ash_userType'=>1,
                                'ash_updDate'=>date('Y-m-d H:i:s')
                            );

                            $checklistVerificationDB->storeStatusUpdateHistory($dataHistory);
                            $this->view->status = $getStatus['key'];
                        }
                    }else{
                        
                        $getStatus = $defModelDB->getIdByDefType('Status', 'Incomplete');
                        
                        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                        if (($oldStatus == '595')){//($oldStatus == '592') || hide in 21/08/2015
                        
                            $data = array(
                                'at_status'=>$getStatus['key']
                            );
                            $checklistVerificationDB->updateAppTrans($data, $appTrxId);

                            $dataHistory = array(
                                'ash_trans_id'=>$appTrxId,
                                'ash_status'=>$data['at_status'],
                                'ash_oldStatus'=>$oldStatus,
                                'ash_changeType'=>1,
                                'ash_updUser'=>$getUserIdentity->id,
                                'ash_userType'=>1,
                                'ash_updDate'=>date('Y-m-d H:i:s')
                            );

                            $checklistVerificationDB->storeStatusUpdateHistory($dataHistory);
                            $this->view->status = $getStatus['key'];
                        }
                    }
                }
		}
		
		
		
				
		
		//change receipt status to APPROVE
		if( $receipt['rcp_status'] == 'ENTRY' ){
			if($type == 646){
				
				if($receipt['receipt_invoice']){
				//get receipt invoice
					foreach ($receipt['receipt_invoice'] as $inv){
						$invID = $inv['rcp_inv_invoice_id'];
						
							if($invID){
								if($balanceMain <= 0){
									$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
									$paymentClass->updateCourseStatus($invID);
							
			//						$this->updateCourseStatus($invID);
								}
							}
					}
				}
			}
		
			$receiptDb->update(
			array(
				'rcp_status'=>'APPROVE',
				'UpdDate'=>date('Y-m-d H:i:s'),
				'UpdUser'=>$getUserIdentity->id
			), 'rcp_id = '.$receipt['rcp_id']);
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Receipt Approved');
		}
		else
		{
			$this->gobjsessionsis->flash = array('type'=>'error','message'=>'Receipt Already Approved');
		}

//		exit;
		$this->_redirect( $this->baseUrl . '/studentfinance/receipt/view-receipt-detail/id/'.$id);
	}
	
	public function ajaxGetPaymentModeAction($id=null){
    	
    	$id = $this->_getParam('id', 0);
    	
    	
    	// check is AJAX request or not
     	/*if (!$this->getRequest() -> isXmlHttpRequest()) {
        	$this->getResponse() -> setHttpResponseCode(404)
                              -> sendHeaders();
         	$this->renderScript('empty.phtml');
         	return false;
     	}*/
    	
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$receivableDB = new Studentfinance_Model_DbTable_PaymentMode();
		$data = $receivableDB->getPaymentModeList($id,1);
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($data);
		
		$this->view->json = $json;
		exit;

    }
    
    public function getCurrencyAction($id=null){
    	
    	$id = $this->_getParam('id', 0);
    	$invID = $this->_getParam('invID', 0);
    	
    	// check is AJAX request or not
     	/*if (!$this->getRequest() -> isXmlHttpRequest()) {
        	$this->getResponse() -> setHttpResponseCode(404)
                              -> sendHeaders();
         	$this->renderScript('empty.phtml');
         	return false;
     	}*/
    	
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
		$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
		
		//get invoice info
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invoice = $invoiceMainDb->getData($invID);
		
//		2 - 1
    	if($id > $invoice['cur_id']){
    		$data = $currencyRateDb->getCurrentExchangeRate(2);
			$amountProforma = round($invoice['balance'] / $data['cr_exchange_rate'],2);
		}else if($id < $invoice['cur_id']){
			$data = $currencyRateDb->getData($invoice['exchange_rate']);
			$amountProforma = round($invoice['balance'] * $data['cr_exchange_rate'],2);
		}else{
			$amountProforma = $invoice['amount'];
		}
            
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($amountProforma);
		
		$this->view->json = $json;
		exit;
	

    }
    
 public function getDefaultAmountCurrencyAction($id=null){
    	
    	$id = $this->_getParam('id', 0);
    	
    	
    	// check is AJAX request or not
     	/*if (!$this->getRequest() -> isXmlHttpRequest()) {
        	$this->getResponse() -> setHttpResponseCode(404)
                              -> sendHeaders();
         	$this->renderScript('empty.phtml');
         	return false;
     	}*/
    	
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
		$data = $curRateDB->getData($id);
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($data);
		
		$this->view->json = $json;
		exit;

    }
    
	public function getCurrencyInfoAction($id=null){
    	
    	$id = $this->_getParam('id', 0);
    	
    	
    	// check is AJAX request or not
     	/*if (!$this->getRequest() -> isXmlHttpRequest()) {
        	$this->getResponse() -> setHttpResponseCode(404)
                              -> sendHeaders();
         	$this->renderScript('empty.phtml');
         	return false;
     	}*/
    	
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$curRateDB = new Studentfinance_Model_DbTable_Currency();
		$data = $curRateDB->getData($id);
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($data);
		
		$this->view->json = $json;
		exit;

    }
    
    
    public function cancelAction(){
    	
    	$id = $this->_getParam('id', 0);
		
    	$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//get receipt detail
		$receiptData = $receiptDb->getDataReceipt($id);
		
		$auth = Zend_Auth::getInstance();
					
		if($receiptData['rcp_status'] == 'APPROVE'){
			
			/*
			 * 1. revert all paid invoice
			 * 2. excess payment
			*/
			
			//invoice
			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoice_list = $invoiceMainDb->getInvoiceFromReceipt($id);
			
//			echo "<pre>";print_r($invoice_list);exit;
			
			$currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
			
			foreach($invoice_list as $invList){
				
				$curID = $invList['currency_id'];
				$exchangerate = $invList['exchange_rate']; 
				
				foreach($invList['receipt_invoice'] as $inv){
					$invID = $inv['rcp_inv_invoice_id'];
					$invDetailID = $inv['rcp_inv_invoice_dtl_id'];
					
					$paidInvAmount = $inv['rcp_inv_amount'];
					$paidInvCur = $inv['rcp_inv_cur_id'];
					
					$currency = $currencyDb->getData($exchangerate);
					if($curID != $paidInvCur ){
						if($curID == 2 && $paidInvCur == 1){
							$paidInvAmount = $paidInvAmount / $currency['cr_exchange_rate'];
						}else if($curID == 1 && $paidInvCur == 2){
							$paidInvAmount = $paidInvAmount * $currency['cr_exchange_rate'];
						}
					}
					
					$newBalance1 = $inv['balance'] + $paidInvAmount;
					$newPaid1 = $inv['paid'] - $paidInvAmount;
					
					$dataRevertDet = array(
						'balance'=> $newBalance1,
						'paid'=> $newPaid1,
					);
//					echo "<pre>";
//					print_r($dataRevertDet);
					$db->update('invoice_detail', $dataRevertDet, 'id = '.$inv['id_detail']);
					
					$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                    $invMain = $invoiceMainDB->getData($inv['id']);
                        
					$newBalance = $invMain['bill_balance'] + $paidInvAmount;
					$newPaid = $invMain['bill_paid'] - $paidInvAmount;
					
					$dataRevertMain = array(
						'bill_balance'=> $newBalance,
						'bill_paid'=> $newPaid,
						'upd_by'=> $auth->getIdentity()->iduser,
						'upd_date'=>  date('Y-m-d H:i:s'),
					);
//					echo "<pre>";
//					print_r($dataRevertMain);
					
					$db->update('invoice_main', $dataRevertMain, 'id = '.$inv['id']);
				}
				
			}
			$seq_no = $this->getCurrentAdjSeq();
			$dataAdj = array(
						'AdjustmentCode'=> $seq_no,
						'AdjustmentType'=> 865, // Cancellation w/o Replacement
						'Remarks'=> 'Receipt Cancellation for '.$receiptData['rcp_no'],
						'Status'=> 'APPROVE',
						'ProcessedBy'=> $auth->getIdentity()->iduser,
						'ProcessedOn'=> date('Y-m-d H:i:s'),
						'EnterBy'=> $auth->getIdentity()->iduser,
						'EnterDate'=> date('Y-m-d H:i:s'),
						'UpdUser'=> $auth->getIdentity()->iduser,
						'UpdDate'=> date('Y-m-d H:i:s'),
						'DocumentDate'=> $receiptData['rcp_receive_date'],
						'IdReceipt'=> $receiptData['rcp_id'],
					);
//echo "<pre>";
//					print_r($dataAdj);
					$adj_id = $db->insert('tbl_receivable_adjustment', $dataAdj);
					
					$dataReceipt = array(
						'rcp_status'=> 'CANCEL',
						'cancel_by'=> $auth->getIdentity()->iduser,
						'cancel_date'=>  date('Y-m-d H:i:s'),
						'CountRecivableadjustment'=>$receiptData['CountRecivableadjustment'] + 1,
						'adjustment_id'=>$adj_id
					);
//					echo "<pre>";
//					print_r($dataReceipt);
					$db->update('receipt', $dataReceipt, 'rcp_id = '.$receiptData['rcp_id']);
					
					//recvert course status if any
					if($invList['bill_paid'] <= 0){
						$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
						$paymentClass->updateCourseStatusCancel($invList['id']);
						
//						$this->updateCourseStatusCancel($invList['id']);
					}

			$db->update('advance_payment', array('advpy_status' => 'X'), 'advpy_rcp_id = '.$id);
		}else{
			$upd_data = array(
				'rcp_status'=> 'CANCEL',
				'cancel_by'=> $getUserIdentity->id,
				'cancel_date'=> date('Y-m-d H:i:s')
			);
			
			$receiptDb = new Studentfinance_Model_DbTable_Receipt();
			$receiptDb->update($upd_data, array('rcp_id = ?' => $id) );
		}
		
		//cancel advance payment
		
		if($receiptData['advpy_id']){
			
			//cancel advance payment utilization
			
			$advPaymentDetailDB = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
			$advDetail = $advPaymentDetailDB->getDataDetail($receiptData['advpy_id']);
			
			$currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
			foreach($advDetail as $advd){
				$ivdID = $advd['advpydet_inv_id'];
				$ivdDetID = $advd['advpydet_inv_det_id'];
				
					$curID = $advd['currency_id'];
				
					$paidInvAmount = $advd['advpydet_total_paid'];
					$paidInvCur = $advd['advpy_cur_id'];
					
					$exchangerate = $advd['advpy_exchange_rate'];
					
					if($exchangerate == 0){
						$currencyRate = $currencyDb->getRateByDate($paidInvCur,$advd['rcp_date']);
						$exchangerate = $currencyRate['cr_id'];
					}
					$currency = $currencyDb->getData($exchangerate);
					if($curID != $paidInvCur ){
						if($curID == 2 && $paidInvCur == 1){
							$paidInvAmount = $paidInvAmount / $currency['cr_exchange_rate'];
						}else if($curID == 1 && $paidInvCur == 2){
							$paidInvAmount = $paidInvAmount * $currency['cr_exchange_rate'];
						}
					}
					
					$newBalance = $advd['bill_balance'] + $paidInvAmount;
					$newPaid = $advd['bill_paid'] - $paidInvAmount;
					
					$dataRevertMain = array(
						'bill_balance'=> $newBalance,
						'bill_paid'=> $newPaid,
						'upd_by'=> $auth->getIdentity()->iduser,
						'upd_date'=>  date('Y-m-d H:i:s'),
					);
//					echo "<pre>";
//					print_r($dataRevertMain);
					
					$db->update('invoice_main', $dataRevertMain, 'id = '.$ivdID);

					$newBalance1 = $inv['balance'] + $paidInvAmount;
					$newPaid1 = $inv['paid'] - $paidInvAmount;
					
					$dataRevertDet = array(
						'balance'=> $newBalance,
						'paid'=> $newPaid,
					);
//					echo "<pre>";
//					print_r($dataRevertDet);
					$db->update('invoice_detail', $dataRevertDet, 'id = '.$ivdDetID);
				
			}
			
			$upd_data = array(
				'advpy_status'=> 'X',
				'advpy_cancel_by'=> $getUserIdentity->id,
				'advpy_cancel_date'=> date('Y-m-d H:i:s')
			);
			
			$advDb = new Studentfinance_Model_DbTable_AdvancePayment();
			$advDb->update($upd_data, array('advpy_id = ?' => $receiptData['advpy_id']) );
		}
		
		
//		echo "<pre>";
//					print_r($invoice_list);
		
   
		$this->_redirect( $this->baseUrl . '/studentfinance/receipt/view-receipt-detail/id/'.$id);
//		exit;
    }
    
    private function getCurrentAdjSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 7")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		$db->update('sequence', array('seq'=>$seq), 'type = 7 and year = '.date('Y'));
		
		if($row2){
			$r_num = $row2['ReceivableAdjustmentPrefix'].date('Y').$row2['ReceivableAdjustmentSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	/*
	 * Get Bill no from mysql function
	*/
	private function getAdjSeq(){
	
		$seq_data = array(
				7,
				date('Y'),
				0,
				0,
				0
		);
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS seq_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		return $seq['seq_no'];
	}
	
	private function updateCourseStatus($invoiceID){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_invoice = $db->select()
							->from(array('ivs'=>'invoice_subject'))
							->join(array('im'=>'invoice_main'),'ivs.invoice_main_id=im.id',array('*'))
							->where('ivs.invoice_main_id = ?', $invoiceID)
							->group('ivs.invoice_main_id');
		$row = $db->fetchAll($select_invoice);
		$txnID = $row[0]['IdStudentRegistration'];
		
		
		
		foreach($row as $data){
			$subjectID = $data['subject_id'];
			
			$typeApp =  $data['invoice_type'];
			
			$select_reg = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->where('a.IdStudentRegistration = ?', $txnID)
							->where('a.IdSubject = ?', $subjectID)
							->order('a.UpdDate DESC')
							->limit('1')
							;
							
			
			$rowReg = $db->fetchRow($select_reg);
			
			$idRegSubject = $rowReg['IdStudentRegSubjects'];
			
			/*case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
			case 6:	$newval = 'Replace';		break;
			case 9: $newval = 'Pre-Repeat';		break;
			case 10:$newval = 'Pre-Replace';	break;*/
			
			$courseStatus = 0;
			if($rowReg['Active'] == 0){//pre-registered
				$courseStatus = 1;
			}elseif($rowReg['Active'] == 9){//pre-Repeat
				$courseStatus = 4; 
			}elseif($rowReg['Active'] == 10){//pre-Replace
				$courseStatus = 6;
			}else{
				$courseStatus = $rowReg['Active'] ;
			}
			
			$updData = array ('Active'=>$courseStatus);
			$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
			
			if($typeApp == 'AU'){
				$select_audit = $db->select()
							->from(array('a'=>'tbl_auditpaperapplicationitem'))
							->join(array('d' => 'tbl_auditpaperapplicationcourse'), 'a.api_apc_id = d.apc_id',array('subject_id'=>'apc_courseid','apc_program','apc_programscheme'))
							->join(array('da' => 'tbl_auditpaperapplication'), 'da.apa_id = a.api_apa_id',array('created_date'=>'apa_upddate','apa_semesterid'))
							->where('da.apa_studregid = ?', $txnID)
							->where('d.apc_courseid = ?', $subjectID)
							->where('a.api_invid_0 = ?', $invoiceID)
							->orWhere('a.api_invid_1 = ?', $invoiceID);

				$rowAudit = $db->fetchRow($select_audit);
				if($invoiceID == $rowAudit['api_invid_0']){
					$updData = array ('api_paid_0'=>1);
					$db->update('tbl_auditpaperapplicationitem',$updData,array('api_id = ?' => $rowAudit['api_id']));
				}
				
				if($invoiceID == $rowAudit['api_invid_1']){
					$updData = array ('api_paid_1'=>1);
					$db->update('tbl_auditpaperapplicationitem',$updData,array('api_id = ?' => $rowAudit['api_id']));
					
					//update course registration
					$updData = array ('Active'=>1);//registered
					$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
				}
			
			}
			
			if($typeApp == 'CT' || $typeApp == 'EX'){
				$select_ct = $db->select()
							->from(array('a'=>'tbl_credittransfer'))
							->where('a.invoiceid_0 = ?', $invoiceID)
							->orWhere('a.invoiceid_1 = ?', $invoiceID);
				
				$rowCt = $db->fetchRow($select_ct);
				
				if($invoiceID == $rowAudit['invoiceid_0']){
					$updData = array ('paid_0'=>1);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}elseif($invoiceID == $rowAudit['invoiceid_1']){
					$updData = array ('paid_1'=>1);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}
			}
			
		}
	}
	
	private function updateCourseStatusCancel($invoiceID){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_invoice = $db->select()
							->from(array('ivs'=>'invoice_subject'))
							->join(array('im'=>'invoice_main'),'ivs.invoice_main_id=im.id',array('*'))
							->where('ivs.invoice_main_id = ?', $invoiceID)
							->group('ivs.invoice_main_id');
		$row = $db->fetchAll($select_invoice);
		$txnID = $row[0]['IdStudentRegistration'];
		
		
		
		foreach($row as $data){
			$subjectID = $data['subject_id'];
			
			$typeApp =  $data['invoice_type'];
			
			$select_reg = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->where('a.IdStudentRegistration = ?', $txnID)
							->where('a.IdSubject = ?', $subjectID)
							->order('a.UpdDate DESC')
							->limit('1')
							;
							
			
			$rowReg = $db->fetchRow($select_reg);
			
			$idRegSubject = $rowReg['IdStudentRegSubjects'];
			
			/*case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
			case 6:	$newval = 'Replace';		break;
			case 9: $newval = 'Pre-Repeat';		break;
			case 10:$newval = 'Pre-Replace';	break;*/
			
			$courseStatus = 0;
			if($rowReg['Active'] == 1){//pre-registered
				$courseStatus = 0;
			}elseif($rowReg['Active'] == 4){//pre-Repeat
				$courseStatus = 9; 
			}elseif($rowReg['Active'] == 6){//pre-Replace
				$courseStatus = 10;
			}else{
				$courseStatus = $rowReg['Active'] ;
			}
			
			$updData = array ('Active'=>$courseStatus);
			$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
			
			if($typeApp == 'AU'){
				$select_audit = $db->select()
							->from(array('a'=>'tbl_auditpaperapplication'))
							->where('a.apa_subjectreg_id = ?', $idRegSubject)
							->where('a.apa_invid_0 = ?', $invoiceID)
							->orWhere('a.apa_invid_1 = ?', $invoiceID);
				
				$rowAudit = $db->fetchRow($select_audit);
				
				if($invoiceID == $rowAudit['apa_invid_0']){
					$updData = array ('apa_paid_0'=>0);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}elseif($invoiceID == $rowAudit['apa_invid_1']){
					$updData = array ('apa_paid_1'=>0);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}
			
			}
			
			if($typeApp == 'CT' || $typeApp == 'EX'){
				$select_ct = $db->select()
							->from(array('a'=>'tbl_credittransfer'))
							->where('a.invoiceid_0 = ?', $invoiceID)
							->orWhere('a.invoiceid_1 = ?', $invoiceID);
				
				$rowCt = $db->fetchRow($select_ct);
				
				if($invoiceID == $rowAudit['invoiceid_0']){
					$updData = array ('paid_0'=>0);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}elseif($invoiceID == $rowAudit['invoiceid_1']){
					$updData = array ('paid_1'=>0);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}
			}
			
		}
	}
	
/*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
    
    public function ajaxGetReceiptMigrateAction(){
    
    	$id = $this->_getParam('id', 0);
    	$type = $this->_getParam('type', 0);
    	
    	if($id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select_invoice = $db->select()
					->from(array('d'=>'receipt'),array('amount'=>'rcp_amount','fee_item_description'=>'rcp_description','id'=>'rcp_id','status'=>'rcp_status','invoice_date'=>'rcp_receive_date','bill_number'=>'rcp_no'))
					->join(array('i'=>'payment'),'i.p_rcp_id = d.rcp_id', array('p_payment_mode_id','p_cheque_no'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = d.rcp_cur_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("d.rcp_status = 'APPROVE'")
					->where("d.MigrateCode is NOT NULL")
					->order('d.rcp_receive_date desc');
					if($type == 645){
						$select_invoice->where('d.rcp_trans_id = ?',$id);
					}elseif($type == 646){
						$select_invoice->where('d.rcp_idStudentRegistration = ?',$id);
					}
		$invoice = $db->fetchAll($select_invoice);
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($invoice);
		
		$this->view->json = $json;
		exit;
    }
    

}