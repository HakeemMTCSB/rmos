<?php
class Studentfinance_PaymentHistoryController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_PaymentHistory();
		$this->_DbObj = $db;
	}
	
	public function migsAction() {
		//title
    	$this->view->title= $this->view->translate("Payment History - MIGS");
    	
    	$this->view->searchForm = new Studentfinance_Form_FeeItem();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$data = $this->_DbObj->getPaginateData($formData);
			
    	}else{
    		$data = null;
    		$formData=null;
    	}

    	$this->view->paginator = $data;	
    	$this->view->search = $formData;	
	}

	public function migsViewAction()
	{
		$this->view->title= $this->view->translate("Payment History - MIGS Detail");
		$id = $this->_getParam('id');

		$data = $this->_DbObj->getData($id);
		$this->view->data = $data;
	}
	
public function approveAction(){
		
		$id = $this->_getParam('id', null);
		$type = $this->_getParam('type', null);
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		//get migs transaction
		$migsTransactionDb = new Studentfinance_Model_DbTable_TransactionMigs();
		
		$migsTxnData = $this->_DbObj->getData($id);
		
		$migsTxnData = $migsTransactionDb->fetchRow(array('mt_id = ?'=>$id));
		
		$appl_id = $migsTxnData['mt_appl_id'];
		$trans_id = $migsTxnData['mt_trans_id'];
		$student_id = $migsTxnData['mt_idStudentRegistration'];
		
		/*
		 * 1) generate invoice
		 * 2) knockoff invoice
		 * 3) insert payment & receipt & receipt_invoice
		 * 4) update column migs_id at receipt table
		 */
		
		//total amount
		$total_payment_amount = $migsTxnData['mt_amount'];
				
		$migsTransactionDetailDb = new Studentfinance_Model_DbTable_TransactionMigsDetail();
		
		//get migs details data
		$migsTxnDataDetail = $migsTransactionDetailDb->fetchAll(array('tmd_tm_id = ?'=>$id))->toArray();
		
//		echo "<pre>";
//			print_r($migsTxnDataDetail);
			
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		
		/*add receipt, payment, receipt_invoice*/
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		$receipt_no = $this->getReceiptNoSeq();
		
		$receipt_data = array(
			'rcp_no' => $receipt_no,
			'rcp_account_code' => 7,	//bank islam
			'rcp_receive_date' => date('Y-m-d'),
			'rcp_payee_type' => 645, //applicant
			'rcp_appl_id' => $appl_id,
			'rcp_trans_id' => $trans_id,
			'rcp_idStudentRegistration' => $student_id,
			'rcp_description' => 'Payment from MIGS',
			'rcp_amount' => $total_payment_amount,
			'rcp_amount_default_currency' => 0,
			'rcp_adv_payment_amt' => 0.00,
			'rcp_gainloss' => 0,
			'rcp_gainloss_amt' => 0,
			'rcp_cur_id' => 1, //always MYR
			'rcp_status'=>'ENTRY',
			'rcp_create_by' => $getUserIdentity->id,
			'rcp_date' =>date('Y-m-d H:i:s'),
			'migs_id'=> $id //migs_id //$respond['receipt']['merchTxnRef']
		);
		$receipt_id = $receiptDb->insert($receipt_data);
		
		//add payment
		$paymentDb = new Studentfinance_Model_DbTable_Payment();
		$payment_data = array(
			'p_rcp_id' => $receipt_id,
			'p_payment_mode_id' => 0,
			'p_migs' => 1,
			'p_cheque_no' => null,
			'p_doc_bank' => null,
			'p_doc_branch' => null,
			'p_terminal_id' => null,
			'p_card_no' => null,
			'p_cur_id' => 1, //always MYR
			'p_status'=>'APPROVE',
			'p_migs'=> $id ,//migs_id //$respond['receipt']['merchTxnRef']
			'p_amount' => $total_payment_amount,
			'p_amount_default_currency' => 0,
			'created_date' =>date('Y-m-d H:i:s'),
			'created_by'=>$getUserIdentity->id
		);
		
		$paymentDb->insert($payment_data);
			
		if($migsTxnDataDetail){ // knockoff
			foreach($migsTxnDataDetail as $pro){
				
				if($pro['tmd_proforma_invoice_id']){
					$proformaID = $pro['tmd_proforma_invoice_id'];
					$type = 1;
					
					$selectDataProforma = $db->select()
								->from(array('pim'=>'proforma_invoice_detail'))
								->join(array('im'=>'proforma_invoice_main'),'pim.proforma_invoice_main_id=im.id',array('main_id'=>'im.id','semester'))
								->joinLeft(array('ivs'=>'proforma_invoice_subject'),'ivs.proforma_invoice_main_id=im.id AND ivs.proforma_invoice_detail_id=pim.id',array('subject_id'))
								->where("pim.id = ?", (int)$proformaID);
								
					$proformaMainData = $db->fetchRow($selectDataProforma);
					
					if($proformaMainData['invoice_id'] == 0){
						$invoiceClass = new icampus_Function_Studentfinance_Invoice(); 
						$invoice_id_proforma = $invoiceClass->generateInvoiceFromProforma($proformaID);
					}else{
						$proformaID = $proformaMainData['invoice_id'];
					}
					
					//add receipt-invoice detail
						$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
						
						$selectData = $db->select()
								->from(array('pim'=>'invoice_detail'), array('*','id_detail'=>'pim.id'))
								->join(array('im'=>'invoice_main'),'pim.invoice_main_id=im.id',array('main_id'=>'im.id','exchange_rate','semester'))
								->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=pim.id',array('subject_id'))
								->join(array('c'=>'tbl_currency'), 'c.cur_id = pim.cur_id')
								->where("im.proforma_invoice_id = ?", (int)$proformaID);
								
						$invoiceMainData = $db->fetchAll($selectData);
						
				}else{
					$proformaID = $pro['tmd_invoice_id'];
					$type=2;
					
					//add receipt-invoice detail
						$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
						
						$selectData = $db->select()
								->from(array('pim'=>'invoice_detail'), array('*','id_detail'=>'pim.id'))
								->join(array('im'=>'invoice_main'),'pim.invoice_main_id=im.id',array('main_id'=>'im.id','exchange_rate','semester'))
								->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=pim.id',array('subject_id'))
								->join(array('c'=>'tbl_currency'), 'c.cur_id = pim.cur_id')
								->where("pim.id = ?", (int)$proformaID);
					
						$invoiceMainData = $db->fetchAll($selectData);
						
						
				}
				
						foreach($invoiceMainData as $inv){
							$invoice_id = $inv['id_detail'];	
							$currencyItem = $inv['cur_id'];
							$currencyPaid = 1;
							
							if($inv['cur_id'] ==  1){
								$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
								$dataCur = $curRateDB->getCurrentExchangeRate(2);
								$amountNew = round($inv['amount'] / $dataCur['cr_exchange_rate'],2);
								$amountPaid = $inv['amount'];
							}else{
								$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
								$dataCur = $curRateDB->getData($inv['exchange_rate']);
								$amountPaid = round($inv['amount'] * $dataCur['cr_exchange_rate'],2);
								$amountNew = $inv['amount'];
								
							}
							
							$data_rcp_inv = array(
								'rcp_inv_rcp_id' => $receipt_id,
								'rcp_inv_rcp_no' => $receipt_no,
								'rcp_inv_invoice_id' => $inv['main_id'],
								'rcp_inv_invoice_dtl_id' => $invoice_id,
								'rcp_inv_amount' => $amountPaid,
								'rcp_inv_amount_default_currency' => $amountNew,
								'rcp_inv_cur_id' => 1,
								'rcp_inv_create_date' =>date('Y-m-d H:i:s'),
								'rcp_inv_create_by'=>$getUserIdentity->id
							);
							
						
							$receiptInvoiceDb->insert($data_rcp_inv);
						
						}
					}
					
					$this->knockoffInvoice($receipt_id,$type);
					
		}else{
			//generate advance payment
			
			$receiptDb = new Studentfinance_Model_DbTable_Receipt();
			$receipt = $receiptDb->getData($receipt_id);
//			echo "<pre>";
//			print_r($receipt);
			
			$receipt = $receipt[0];
//			exit;
			
			if($migsTxnData['mt_from'] == 0){
				$type = 1;
			}else{
				$type = 2;
			}
			if($type == 1){
		    	$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
		    	$profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
	    	}elseif($type == 2){
	    		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
		    	$profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
	    	}
    	
			//get current semester
			$profile = array('ap_prog_scheme'=>$profile['IdScheme'],'branch_id'=>$profile['IdBranch']);
			$semesterDB = new Registration_Model_DbTable_Semester();
			$semester = $semesterDB->getApplicantCurrentSemester($profile);
			
				$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				
					$bill_no =  $this->getBillSeq(8, date('Y'));
					
					$advance_payment_data = array(
						'advpy_fomulir' => $bill_no,
						'advpy_appl_id' => $receipt['rcp_appl_id'],
						'advpy_trans_id' => $receipt['rcp_trans_id'],
						'advpy_idStudentRegistration' => $receipt['rcp_idStudentRegistration'],
						'advpy_sem_id'=>$semester['IdSemesterMaster'],
						'advpy_rcp_id' => $receipt_id,
						'advpy_description' => 'Advance payment from receipt no:'.$receipt_no,
						'advpy_cur_id' => $receipt['rcp_cur_id'],
						'advpy_amount' => $total_payment_amount,
						'advpy_total_paid' => 0.00,
						'advpy_total_balance' => $total_payment_amount,
						'advpy_status' => 'A',
						'advpy_date' => date('Y-m-d'),
						
					);
				
				$advPayID = $advancePaymentDb->insert($advance_payment_data);
				
				$advance_payment_det_data = array(
					'advpydet_advpy_id' => $advPayID,
					'advpydet_total_paid' => 0.00
				);
				
				$advancePaymentDetailDb->insert($advance_payment_det_data);

				//receipt update advpayment
				$receiptDb->update(array('rcp_adv_payment_amt'=>$total_payment_amount, 'rcp_adv_payment_amt_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_payment_amount, $receipt['rcp_cur_id']) ), 'rcp_id = '.$receipt_id);
			
				
			
			
		}
				
				
				
				
				//update receipt status
				$migsTransactionDb->update(
				array(
					'mt_status'=>'SUCCESS',
					'mt_from'=>'2',
					'UpdDate'=>date('Y-m-d H:i:s'),
					'UpdUser'=>$getUserIdentity->id
				), 'mt_id = '.$id);
				

		$this->_redirect( $this->baseUrl . '/studentfinance/receipt/view-receipt-detail/id/'.$receipt_id);
	}
	
	/*
	 * Get Bill no from mysql function
	*/
	private function getReceiptNoSeq(){
	
		$seq_data = array(
				5,
				date('Y'),
				0,
				0,
				0
		);
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
	
		return $seq['receipt_no'];
	}
	
private function knockoffInvoice($receipt_id,$type){
		
		//receipt
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		$receipt = $receiptDb->getData($receipt_id);
		
		$receipt = $receipt[0];
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
		
		$appStatus = 0;
			
		$appTrxId = $receipt['rcp_idStudentRegistration'];
		$trans_id = $receipt['rcp_trans_id'];
		
		if($type == 1){
	    	$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
	    	$txnData = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
    	}elseif($type == 2){
    		$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    		$txnData = $studentRegistrationDb->getTheStudentRegistrationDetail($appTrxId);
    	}
		
    	
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
		
		$totalAmount = 0.00;
		$total_advance_payment_amount = 0;
		$total_payment_amount = $receipt['rcp_amount'];
		$total_paid_receipt = 0;
		$paidReceiptInvoice = 0;
			
			//get receipt invoice
			foreach ($receipt['receipt_invoice'] as $inv){
				$invID = $inv['rcp_inv_invoice_id'];
				$invDetID = $inv['rcp_inv_invoice_dtl_id'];
//				$paidReceipt =  $inv['rcp_inv_amount'];
				$rcpCurrency = $inv['rcp_inv_cur_id'];
				
				$invDetData = $invoiceDetailDb->getData($invDetID);
				
//				echo "<pre>";
//				print_r($invDetData);
//				exit;
				
				$invCurrency = $invDetData['cur_id'];
				
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currency = $currencyDb->fetchRow('cur_id = '.$invCurrency)->toArray();

				/*echo "<pre>";
				print_r($currency);
				exit;*/
//				$amountNew = $paidReceipt;

				$amountDefault = $invDetData['balance'];
				
				$paidReceiptInvoice = $inv['rcp_inv_amount'];
				
				if($invCurrency == 1){
					$paidReceipt = $inv['rcp_inv_amount'];
					
				}else{
					$paidReceipt = $inv['rcp_inv_amount_default_currency'];
				}
						
				$balance  = $amountDefault  - $paidReceipt;
				
				//update invoice details
				$dataInvDetail = array(
					'paid'=> $paidReceipt +  $invDetData['paid'] ,
					'balance' => $balance,
				);
				
				
				
//				echo "<pre>";
//				print_r($dataInvDetail);
				$invoiceDetailDb->update($dataInvDetail, array('id =?'=>$invDetID) );
				
				$totalAmount += $paidReceiptInvoice;
				
				//update invoice main
				$invMainData = $invoiceMainDb->getData($invID);
					
				$balanceMain = ($invMainData['bill_balance']) - ($paidReceipt);
				$paidMain = ($invMainData['bill_paid']) + ($paidReceipt);
				
				//update invoice main
				$dataInvMain = array(
					'bill_paid'=> $paidMain,
					'bill_balance' => $balanceMain,
					'upd_by'=>1,
		            'upd_date'=>date('Y-m-d H:i:s')
				);
				
//				echo "<pre>";
//				print_r($dataInvMain);
				$invoiceMainDb->update($dataInvMain, array('id =?'=>$invID) );
				
				if($type == 2){
					if($balanceMain == 0){
						$this->updateCourseStatus($invID);
					}
				}
				
				if($type == 1){
					$appTrxId = $trans_id;
					$id = $receipt_id;
					
					$checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
					
					$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
					$txnProfile = $applicantProfileDb->getApplicantRegistrationDetail($appTrxId);
					
					$appStatus = $txnProfile['at_status'];
					
				
					$programSchemeId = $txnProfile['ap_prog_scheme'];
					$stdCtgy = $txnProfile['appl_category'];
					$txnProfile['IdProgramScheme'] = $programSchemeId;
					
					
					$fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
			        $checklistVerificationDB->deleteStatusByTrx($appTrxId);
					
					//get applicant document status
	                $docChecklist = $checklistVerificationDB->fnGetDocCheckList($programSchemeId, $stdCtgy, $appTrxId);
	
	                foreach($docChecklist as $dataDoc){
	
		                $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$appTrxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
	
	                    if($list){
	                        foreach($list as $listLoop){
	                            $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($appTrxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
	                            $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($appTrxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
	
	                            if (!$adsCheck){ //update
	                                $data = array(
	                                    'ads_txn_id'=>$appTrxId,
	                                    'ads_dcl_id'=>$dataDoc['dcl_Id'],
	                                    'ads_ad_id'=>$adId,
	                                    //'ads_appl_id'=>'',
	                                    //'ads_confirm'=>$check,
	                                    'ads_section_id'=>$dataDoc['dcl_sectionid'],
	                                    'ads_table_name'=>$dataDoc['table_name'],
	                                    'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
	                                    'ads_status'=>0,
	                                    'ads_comment'=>'',
	                                    'ads_createBy'=>$getUserIdentity->id,
	                                    'ads_createDate'=>date('Y-m-d H:i:s')
	                                );
	                                $checklistVerificationDB->insert($data);
	                            }
	                        }
	                    }
	                }
	                
					$currency = new Zend_Currency(array('currency'=>$receipt['cur_code']));
		
					//payment
					$paymentDb = new Studentfinance_Model_DbTable_Payment();
					$payment = $paymentDb->getReceiptPayment($id);
					
					//invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoice_list = $invoiceMainDb->getInvoiceFromReceipt($id);
					
					$inv = array();
					$total_payment = 0.00;
					$total_balance = 0.00;
					
					if($invoice_list){
						
			//			echo "<pre>";
			//			print_r($invoice_list);
			//			exit;
						
						foreach ($invoice_list as $invoice){
							$iv = array(
									'invoice_no' => $invoice['bill_number'],
									'amount' => 0,
									'currency' => $invoice['cur_symbol_prefix']
								);
							
							foreach ($invoice['receipt_invoice'] as $ri){
								$iv['amount'] += $ri['rcp_inv_amount'];
								$total_payment += $ri['rcp_inv_amount'];
								$total_balance += $ri['balance'];
								
								$feeID = $ri['fi_id'];
								$receipt_no = $ri['rcp_inv_rcp_no'];
								$bill_paid = $ri['rcp_inv_amount'];
								$currencyPaid = $ri['cur_symbol_prefix'];
								if($type == 645){
								/*
								 * get fee structure program item
								 * update docuemnt checklist status
								 */
												
								$fsProgram = $fsProgramDB->getApplicantDocumentType($appTrxId,$feeID);
								$newArray1 = array();	
								$ads_id = $fsProgram['ads_id'];
								if(isset($ads_id)){
									$arrayAds [] = array(
										'id'=>$ads_id,
										'total'	=>$bill_paid,
										'balance'=>$total_balance,
										'cur'=>$currencyPaid,
										'receipt_no'=>$receipt_no
									);
								}
								array_push($newArray1,$arrayAds);
								
								}
								
					
							}
							
							$inv[] = $iv;
						}
							//update checklist
						$this->updateChecklist($arrayAds);
					}
					
						/*
					 * update applicant_transaction status
					 * only status COmplete (4) baru update
					 */
					
					$defModelDB = new App_Model_General_DbTable_Definationms();
					
					$docList = $checklistVerificationDB->fnGetChecklistStatus($appTrxId);
				 	if (count($docList) > 0){
			        	$i=0;
			            foreach($docList as $docLoop){
			               if(($docLoop['ads_status']==4 || $docLoop['ads_status']==5) && $docLoop['dcl_mandatory']==1){
			                     $i++;
			               }
			            }
			                    //var_dump(count($docList));var_dump($i); exit;
			         	if($appStatus == 595 || $appStatus == 592){ //only update at_status transaction if status entry/incomplete
			                    if (count($docList)==$i){
			                        
			                        $getStatus = $defModelDB->getIdByDefType('Status', 'Complete');
			                        
			                        $oldStatus = $checklistVerificationDB->universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
			                        
			                        $data = array(
			                            'at_status'=>$getStatus['key']
			                        );
			                        $checklistVerificationDB->updateAppTrans($data, $appTrxId);
			                        
			                        $dataHistory = array(
			                            'ash_trans_id'=>$appTrxId,
			                            'ash_status'=>$data['at_status'],
			                            'ash_oldStatus'=>$oldStatus,
			                            'ash_changeType'=>1,
                                                    'ash_userType'=>1,
			                            'ash_updUser'=>$getUserIdentity->id,
			                            'ash_updDate'=>date('Y-m-d H:i:s')
			                        );
			                        
			                        $checklistVerificationDB->storeStatusUpdateHistory($dataHistory);
			                    }
			                }
				 	}

				}
			}
			
			//calculate advance payment amount
			
			if( $total_payment_amount > $totalAmount ){
				$total_advance_payment_amount = $total_payment_amount - ($totalAmount);
			}
			
			//add advance payment (if any)
			if($total_advance_payment_amount > 0){
				$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				
				$advance_payment_data = array(
					'advpy_appl_id' => $receipt['rcp_appl_id'],
					'advpy_trans_id' => $receipt['rcp_trans_id'],
					'advpy_idStudentRegistration' => $receipt['rcp_idStudentRegistration'],
					'advpy_rcp_id' => $receipt['rcp_id'],
					'advpy_description' => 'Advance payment from receipt no:'.$receipt['rcp_no'],
					'advpy_cur_id' => $receipt['rcp_cur_id'],
					'advpy_amount' => $total_advance_payment_amount,
					'advpy_total_paid' => 0.00,
					'advpy_total_balance' => $total_advance_payment_amount,
					'advpy_status' => 'A'
				);
				
				$advPayID = $advancePaymentDb->insert($advance_payment_data);
				
				$advance_payment_det_data = array(
					'advpydet_advpy_id' => $advPayID,
					'advpydet_total_paid' => 0.00
				);
				
				$advancePaymentDetailDb->insert($advance_payment_det_data);

				//receipt update advpayment
				$receiptDb->update(array('rcp_adv_payment_amt'=>$total_advance_payment_amount, 'rcp_adv_payment_amt_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_advance_payment_amount, $receipt['rcp_cur_id']) ), 'rcp_id = '.$receipt['rcp_id']);
			}
			
			
			//update receipt status
			$receiptDb->update(
			array(
				'rcp_status'=>'APPROVE',
				'UpdDate'=>date('Y-m-d H:i:s'),
				'UpdUser'=>1
			), 'rcp_id = '.$receipt_id);
			
			
		
		
		/*knockoff end*/
			
		/*
		 * update status course as registered
		 */
			
			
			
	}
	
	private function updateCourseStatus($invoiceID){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_invoice = $db->select()
							->from(array('ivs'=>'invoice_subject'))
							->join(array('im'=>'invoice_main'),'ivs.invoice_main_id=im.id',array('*'))
							->where('ivs.invoice_main_id = ?', $invoiceID)
							->group('ivs.invoice_main_id');
		$row = $db->fetchAll($select_invoice);
		$txnID = $row[0]['IdStudentRegistration'];
		
		
		
		foreach($row as $data){
			$subjectID = $data['subject_id'];
			
			$typeApp =  $data['invoice_type'];
			
			$select_reg = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->where('a.IdStudentRegistration = ?', $txnID)
							->where('a.IdSubject = ?', $subjectID)
							->order('a.UpdDate DESC')
							->limit('1')
							;
							
			
			$rowReg = $db->fetchRow($select_reg);
			
			$idRegSubject = $rowReg['IdStudentRegSubjects'];
			
			/*case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
			case 6:	$newval = 'Replace';		break;
			case 9: $newval = 'Pre-Repeat';		break;
			case 10:$newval = 'Pre-Replace';	break;*/
			
			$courseStatus = 0;
			if($rowReg['Active'] == 0){//pre-registered
				$courseStatus = 1;
			}elseif($rowReg['Active'] == 9){//pre-Repeat
				$courseStatus = 4; 
			}elseif($rowReg['Active'] == 10){//pre-Replace
				$courseStatus = 6;
			}else{
				$courseStatus = $rowReg['Active'] ;
			}
			
			$updData = array ('Active'=>$courseStatus);
			$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
			
			if($typeApp == 'AU'){
				$select_audit = $db->select()
							->from(array('a'=>'tbl_auditpaperapplication'))
							->where('a.apa_subjectreg_id = ?', $idRegSubject)
							->where('a.apa_invid_0 = ?', $invoiceID)
							->orWhere('a.apa_invid_1 = ?', $invoiceID);
				
				$rowAudit = $db->fetchRow($select_audit);
				
				if($invoiceID == $rowAudit['apa_invid_0']){
					$updData = array ('apa_paid_0'=>1);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}elseif($invoiceID == $rowAudit['apa_invid_1']){
					$updData = array ('apa_paid_1'=>1);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}
			
			}
			
			if($typeApp == 'CT' || $typeApp == 'EX'){
				$select_ct = $db->select()
							->from(array('a'=>'tbl_credittransfer'))
							->where('a.invoiceid_0 = ?', $invoiceID)
							->orWhere('a.invoiceid_1 = ?', $invoiceID);
				
				$rowCt = $db->fetchRow($select_ct);
				
				if($invoiceID == $rowAudit['invoiceid_0']){
					$updData = array ('paid_0'=>1);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}elseif($invoiceID == $rowAudit['invoiceid_1']){
					$updData = array ('paid_1'=>1);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}
			}
			
		}
	}
	
private function updateChecklist(&$arrayAds){
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
			
		$tmp = array();
		$n=0;
		foreach($arrayAds as $arg)
		{
		    $tmp[$arg['id']]['tot'][$n] = $arg['total'];
		    $tmp[$arg['id']]['bal'][$n] = $arg['balance'];
		    $tmp[$arg['id']]['cur'] = $arg['cur'];
		    $tmp[$arg['id']]['receipt_no'] = $arg['receipt_no'];
		    $n++;
		}
		
		$output = array();
		$m=0;
		foreach($tmp as $type => $labels)
		{
			 $output[$m] = array(
		        'id' => $type,
		        'total' => number_format(array_sum($labels['tot']),2),
		    	'balance' => number_format(array_sum($labels['bal']),2),
				 'cur'=>$labels['cur'],
				 'receipt'=>$labels['receipt_no']
			    );
		
			   $m++;
		}

		//update applicant document type checklist	
		foreach($output as $out){
			if($out['balance'] == '0'){
				$docStatus = array(
					'ads_status'=>4,//completed
					'ads_comment'=> $out['receipt'].", ".$out['cur']."".$out['total'] ,
					'ads_modBy'=>$getUserIdentity->id,
					'ads_modDate'=>date('Y-m-d H:i:s'),
				);
				
//				echo "<pre>";
//				print_r($output);
				$appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
				$appDocStatusDB->update($docStatus, array('ads_id =?'=>$out['id']) );
			}
		}
	}
/*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
    
}

