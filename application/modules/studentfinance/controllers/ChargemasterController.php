<?php
class Studentfinance_ChargemasterController extends Base_Base { //Controller for the User Module

	private $lobjChargemasterForm;
	private $lobjChargemaster;
	
	public function init() { //initialization function
		$this->fnsetObj();	
		
	}
	public function fnsetObj(){
		
		$this->lobjChargemasterForm = new Studentfinance_Form_Chargemaster();
		$this->lobjChargemaster = new Studentfinance_Model_DbTable_Chargemaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		
		
		$lobjAccountMasterList = $this->lobjChargemaster->fnGetAccountMasterList();
		$lobjform->field8->addMultiOptions($lobjAccountMasterList);
		
		/*$lobjChargeTypeList = $this->lobjChargemaster->fnGetChargeTypeList();
		$lobjform->field5->addMultiOptions($lobjChargeTypeList);*/
		
		$lobjprogramList = $this->lobjprogram->fnGetProgramList();
		$lobjform->field5->addMultiOptions($lobjprogramList);
		
		 if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->chargemasterpaginatorresult);
		
		$larrresult = $this->lobjChargemaster->fngetChargesDetails(); //get user details
		
		/*$lobjUniversityMasterList = $this->lobjChargemaster->fnGetUniversityMasterList();
		$lobjform->field1->addMultiOptions($lobjUniversityMasterList);*/
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->chargemasterpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->chargemasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjChargemaster ->fnSearchCharges( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->chargemasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/chargemaster/index');
		}


	}

	public function newchargemasterAction() { //Action for creating the new user
		
		$this->view->lobjChargemasterForm = $this->lobjChargemasterForm; 
		
		//$this->view->lobjChargemasterForm->IdAccountMaster->setAttrib('OnChange', 'validateAccountName');

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjChargemasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjChargemasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		$lobjAccountMasterList = $this->lobjChargemaster->fnGetAccountMasterList();
		$this->lobjChargemasterForm->IdAccountMaster->addMultiOptions($lobjAccountMasterList);

		$lobjChargeTypeList = $this->lobjChargemaster->fnGetChargeTypeList();
		$this->lobjChargemasterForm->ChargeType->addMultiOptions($lobjChargeTypeList);
		
		$lobjprogramList = $this->lobjprogram->fnGetProgramList();
		$this->lobjChargemasterForm->IdProgram->addMultiOptions($lobjprogramList);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);die();
			
			unset ( $larrformData ['Save'] );
			if ($this->lobjChargemasterForm->isValid ( $larrformData )) {
				$result = $this->lobjChargemaster->fnaddCharges($larrformData); //instance for adding the lobjuserForm values to DB
				//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/studentfinance/chargemaster/index');
			}
		}

	}

	public function chargemasterlistAction() { //Action for the updation and view of the  details
		
		$this->view->lobjChargemasterForm = $this->lobjChargemasterForm;
		 
		$lintIdCharges = ( int ) $this->_getParam ( 'id' );
		$this->view->Idcharges = $lintIdCharges;
		$lobjAccountMasterList = $this->lobjChargemaster->fnGetAccountMasterList();
		$this->lobjChargemasterForm->IdAccountMaster->addMultiOptions($lobjAccountMasterList);

		$lobjChargeTypeList = $this->lobjChargemaster->fnGetChargeTypeList();
		$this->lobjChargemasterForm->ChargeType->addMultiOptions($lobjChargeTypeList);
		
		$lobjprogramList = $this->lobjprogram->fnGetProgramList();
		$this->lobjChargemasterForm->IdProgram->addMultiOptions($lobjprogramList);
		
		$larrresult = $this->lobjChargemaster->fnviewCharges($lintIdCharges); 
		
		$lobjChargeTypeList = $this->lobjChargemaster->fnGetChargeTypeList();
		$this->lobjChargemasterForm->ChargeType->addMultiOptions($lobjChargeTypeList);
		
		$this->lobjChargemasterForm->populate($larrresult);	
		$this->view->lobjChargemasterForm->IdCharges->setValue ($larrresult['IdCharges']);
	
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjChargemasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjChargemasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjChargemasterForm->isValid ( $larrformData )) {
						
					$lintiIdCharges = $larrformData ['IdCharges'];
					 $this->lobjChargemaster->fnupdateCharges($lintiIdCharges, $larrformData );
					//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
					$this->_redirect( $this->baseUrl . '/studentfinance/chargemaster/index');
				}
			}
		}
		$this->view->lobjChargemasterForm = $this->lobjChargemasterForm;
	}

public function getaccountanameAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$IdAccountMaster = $this->_getParam('IdAccountMaster');	
		$larrDetails = $this->lobjChargemaster->fngetValidateAccountName($IdAccountMaster);
		echo $larrDetails['IdAccountMaster'];
		
	}
}