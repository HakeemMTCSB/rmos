<?php
class Studentfinance_BulkinvoiceController extends Base_Base { //Controller for the User Module

	private $lobjInvoiceForm;
	private $lobjInvoiceModel;
	
	public function init() {
		$this->fnsetObj();
	}
	
	public function fnsetObj(){
		$this->lobjInvoiceModel = new Studentfinance_Model_DbTable_Bulkinvoice();
		$this->lobjInvoiceForm = new Studentfinance_Form_Bulkinvoice();		
		$this->lobjStudentregistrationModel = new Application_Model_DbTable_Studentapplication();		
		//$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();	
	}
	
   public function indexAction() {   
		$this->view->lobjform = $this->lobjform; 
		//$larrresult = $this->lobjInvoiceModel->fngetStudentApplicationDetails();		
		$lobjbranch = $this->lobjInvoiceModel->fnGetProgrambranchlist();		
		$this->lobjform->field8->addMultiOptions($lobjbranch);
		$this->lobjform->field8->setAttrib('required',"true") ;	
		//$this->lobjform->field2->setAttrib('required',"true") ;	
		

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {				
				$this->view->idProgram    = $larrformData['field8'];
				$larrresult = $this->lobjInvoiceModel->fnSearchStudentApplication($larrformData,1); //searching the values for the user
				$this->view->paginator = $larrresult;							
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/bulkinvoice/index');
		}
		
	}

	public function newstudentinvoiceAction() { //Action for creating the new user
		//$this->view->lobjInvoiceForm = $this->lobjInvoiceForm;
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		
		$lintprogramid = $larrformData['idProgram'];
		$larrprogramdetails = $this->lobjInvoiceModel->fngetProgramDetails($lintprogramid);
		
		$sum1 = 0;
		for($s=0;$s<count($larrprogramdetails);$s++){
			$sum1 = $sum1+$larrprogramdetails[$s]['Charges'];
		}
		for($ff=0;$ff<count($larrformData['studentIds']);$ff++){			
			$larrresultstudentdetails = $this->lobjInvoiceModel->fngetStudentDetails($larrformData['studentIds'][$ff]);			
			if(count($larrprogramdetails)>0){
				$IdSemester = $this->lobjInvoiceModel->fngetSemester($larrformData['studentIds']);
				$larrformData['Semester'] = $IdSemester['IdSemester'];
					
	            $larrformData['Studentid'] = $larrformData['studentIds'][$ff];		
			    $larrformData['InvoiceDate'] = date('Y-m-d  H:i:s');	
			    $larrformData['InvoiceAmount']= $sum1;
	            $larrformData['MonthYear']  = date('M Y');
	            $larrformData['AcademicPeriod'] = $larrformData['Semester'];		
	            $larrformData['Naration']   = "Narration";	
	            $larrformData['idsponsor']  = 0;	
			    $larrformData['UpdDate']    = date('Y-m-d  H:i:s');	
			    $larrformData['UpdUser']    = 1;//$larrformData['UpdUser'];
			           
				
				$idInvoice   = $this->lobjInvoiceModel->fnAddInvoiceMaster($larrformData);
				$larrprogramdetails[0]['UpdDate'] =  $larrformData['UpdDate'];
				$larrprogramdetails[0]['UpdUser'] =  $larrformData['UpdUser'];
				$this->lobjInvoiceModel->fnAddInvoiceDetails($larrprogramdetails,$idInvoice);	
					
				$larrresultsubjectcharges  = $this->lobjInvoiceModel->fnGetSubjectCharges($larrformData['Studentid'][$ff]);
							
				$larrresultsubjectcharges[0]['UpdDate'] =  $larrformData['UpdDate'];
				$larrresultsubjectcharges[0]['UpdUser'] =  $larrformData['UpdUser'];
				$totalinvoiceamount = $this->lobjInvoiceModel->fnAddSubjectInvoiceDetails($larrresultsubjectcharges,$idInvoice); 
				$totalinvoiceamount = $sum1+$totalinvoiceamount;
				$this->lobjInvoiceModel->fnUpdateAccount($totalinvoiceamount,$idInvoice);	    
			}
		}	
		$this->_redirect( $this->baseUrl . '/studentfinance/bulkinvoice/index');	
	}
	
}