<?php
/**
 * @author Suliana <suliana@meteor.com.my>
 * @copyright Copyright (c) 2015, MTCSB
 */
class Studentfinance_FeeGraduationController extends Base_Base {
	
	public function indexAction(){
	
		$this->view->title = "Convocation Fee Setup";
	
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$data = $convocationDb->getData();
		
		if(count($data)>0){
		foreach($data as $key=>$convo){
			//get award tagging
			$award = $convocationDb->getTaggingAward($convo['c_id']);
			$data[$key]['award']=$award;
		}
		}
		
		$this->view->data = $data;
	
	}
	
	public function viewAction(){
	
		$this->view->title= $this->view->translate("Convocation Fee Setup");
		
		$c_id = $this->_getParam('id',null);
		$this->view->c_id = $c_id;
		
		$convocationDb = new Graduation_Model_DbTable_Convocation();
		$this->view->convo = $convocationDb->getData($c_id);
		
		$feeConvoDb = new Studentfinance_Model_DbTable_FeeItemConvo();
		$this->view->convo_fee = $feeConvoDb->getData($c_id);
		
	
	}
	
	public function addAction(){
	
		$this->view->title = "Add Fee";
		
		$c_id = $this->_getParam('id',null);
		$this->view->c_id = $c_id;
	
		$form = new Studentfinance_Form_FeeConvo();
	
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
	
		if ($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
	
				$data = array(
						'fv_convo' => $c_id,
						'fv_fi_id' => $formData['fv_fi_id'],
						'fv_currency_id' => $formData['fv_currency_id'],
						'fv_amount' => $formData['fv_amount'],
						'UpdDate' => date('Y-m-d H:i:s'),
						'UpdUser' => $getUserIdentity->id
				);
	
				$feeConvoDb = new Studentfinance_Model_DbTable_FeeItemConvo();
	
				$idFv=$feeConvoDb->insert($data);
				
				//history
				$data = array(
						'fv_id'=>$idFv,
						'fv_convo' => $c_id,
						'fv_fi_id' => $formData['fv_fi_id'],
						'fv_currency_id' => $formData['fv_currency_id'],
						'fv_amount' => $formData['fv_amount'],
						'fv_action' => 'add',
						'UpdDate' => date('Y-m-d H:i:s'),
						'UpdUser' => $getUserIdentity->id
				);
	
				$feeConvoHisDb = new Studentfinance_Model_DbTable_FeeItemConvoHistory();
	
				$feeConvoHisDb->insert($data);
	
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new fee');
	
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-graduation', 'action'=>'view','id'=>$c_id),'default',true));
	
			}else{
				$form->populate($formData);
			}
		}
	
		$this->view->form = $form;
	}
	
	public function editAction(){
	
		$id = $this->_getParam('id',null);
		$fv_id = $this->_getParam('fv_id',null);
	
		if($id==null){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-graduation', 'action'=>'view','id'=>$id),'default',true));
		}
	
		$this->view->title = "Edit Fee Convocation";
	
		$form = new Studentfinance_Form_FeeConvo();
	
		$feeConvoDb = new Studentfinance_Model_DbTable_FeeItemConvo();
		$dataFee = $feeConvoDb->getDataFee($fv_id);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
			
		if ($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
	
				$data = array(
						'fv_currency_id' => $formData['fv_currency_id'],
						'fv_amount' => $formData['fv_amount'],
						'UpdDate' => date('Y-m-d H:i:s'),
						'UpdUser' => $getUserIdentity->id
					,
				);
	
				$feeConvoDb->update($data,array('fv_id=?'=>$fv_id));
				
				//history
				$data = array(
						'fv_id'=>$fv_id,
						'fv_convo' => $id,
						'fv_fi_id' => $dataFee['fv_fi_id'],
						'fv_currency_id' => $formData['fv_currency_id'],
						'fv_amount' => $formData['fv_amount'],
						'fv_action' => 'update',
						'UpdDate' => date('Y-m-d H:i:s'),
						'UpdUser' => $getUserIdentity->id
				);
	
				$feeConvoHisDb = new Studentfinance_Model_DbTable_FeeItemConvoHistory();
	
				$feeConvoHisDb->insert($data);
	
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update fee');
	
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-graduation', 'action'=>'view','id'=>$id),'default',true));
	
			}else{
				$form->populate($formData);
			}
		}else{
			$data = $feeConvoDb->fetchRow(array('fv_id=?'=>$fv_id))->toArray();
			$form->populate($data);
		}
	
		$this->view->form = $form;
	}
	
	public function deleteAction($id = null){
    	$id = $this->_getParam('id',null);
		$fv_id = $this->_getParam('fv_id',null);
	
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
    	$feeConvoDb = new Studentfinance_Model_DbTable_FeeItemConvo();
    	$dataFee = $feeConvoDb->getDataFee($fv_id);
    	
    	if($id!=0){
    		
    		//history
			$data = array(
					'fv_id'=>$fv_id,
					'fv_convo' => $id,
					'fv_fi_id' => $dataFee['fv_fi_id'],
					'fv_currency_id' => $dataFee['fv_currency_id'],
					'fv_amount' => $dataFee['fv_amount'],
					'fv_action' => 'delete',
					'UpdDate' => date('Y-m-d H:i:s'),
					'UpdUser' => $getUserIdentity->id
			);
	
			$feeConvoHisDb = new Studentfinance_Model_DbTable_FeeItemConvoHistory();

			$feeConvoHisDb->insert($data);
				
    		$feeConvoDb->deleteData($fv_id);
    		
    		
				
    	}
    	
    	$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success delete fee');
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-graduation', 'action'=>'view', 'id'=>$id),'default',true));
    	
    }
    
}
?>