<?php
class Studentfinance_ReceivableadjustmentController extends Base_Base { //Controller for the User Module

	private $_gobjlog;
	private $lobjReceivableAdjustmentForm;
//	private $lobjReceivableAdjustment;
//	private $lobjSubjectsetup;
//	private $lobjcodegenObj;
//	private $lobjDefCodeModel;
	public $lobjform;
	private $DefmodelObj;
//	public $lobjFeeCategory;
//	private $lobjrecieptModel;
//	private $lobjrecevableinvoiceModel;
//	private $lobjrecevablenoninvoiceModel;


	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
		
	}

	public function fnsetObj(){
		$this->lobjReceivableAdjustmentForm = new Studentfinance_Form_Receivableadjustment();
//		$this->lobjReceivableAdjustment = new Studentfinance_Model_DbTable_Receivableadjustment();	
		
//		$this->lobjcodegenObj = new Cms_CodeGeneration();
//		$this->lobjSubjectsetup = new Application_Model_DbTable_Subjectsetup();
//		$this->lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		$this->DefmodelObj = new App_Model_Definitiontype();
		$this->lobjform = new App_Form_Search();
		
//		$this->lobjFeeCategory = new Studentfinance_Model_DbTable_Fee();
//		$this->lobjrecieptModel = new Studentfinance_Model_DbTable_Receipt();
//		$this->lobjrecevableinvoiceModel = new Studentfinance_Model_DbTable_Receivableadjustmentinvoice();
//		$this->lobjrecevablenoninvoiceModel = new Studentfinance_Model_DbTable_Receivableadjustmentnoninvoice();
//		
//		//
//		$this->lobjapplicantpersonalModel = new Application_Model_DbTable_Studentapplication();
//		$this->lobjStudentapproval = new Application_Model_DbTable_Studentapproval();
//		$this->lobjProgramCheckList = new Application_Model_DbTable_Programchecklist();
	}

	public function indexAction(){
		//Form
		$this->view->title="Receivable Adjustment";
		$this->view->lobjform = $this->lobjform;
		$lobjReceivableAdjustmentDB = new Studentfinance_Model_DbTable_Receivableadjustment();	
		$larrresult = $lobjReceivableAdjustmentDB->fnSearchReceivable( $post = NULL);
		

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->ReceivableAdjustmentpaginatorresult);

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		$larrAdjustmenttype = $this->DefmodelObj->fnGetDefinationMs('Adjustment Type');
		
		foreach($larrAdjustmenttype as $larrvalues) {
			$this->view->lobjform->field5->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}

		if(isset($this->gobjsessionsis->ReceivableAdjustmentpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->ReceivableAdjustmentpaginatorresult,$lintpage,$lintpagecount);
		}
		else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		$this->view->lobjform->Search->setAttrib('OnClick','return checkdate();');

		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
			$larrformData = $this->_request->getPost ();
			$larrresult = $this->lobjReceivableAdjustment->fnSearchReceivable($larrformData);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->ReceivableAdjustmentpaginatorresult = $larrresult;
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/receivableadjustment');
		}
	}


	public function addreceivableAction(){
		//Form
		$this->view->title = "Receivable Adjustment Entry";
		$this->view->lobjReceivableAdjustmentForm = new Studentfinance_Form_Receivableadjustment();
		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity();
		$UpdDate = date('Y-m-d H:i:s');
		$lIntIdUniversity = 1;

		//Render to View
		$this->view->lobjReceivableAdjustmentForm->IdUniversity->setvalue ($lIntIdUniversity);
		$this->view->lobjReceivableAdjustmentForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjReceivableAdjustmentForm->UpdUser->setValue ($userId);

		$this->view->lobjReceivableAdjustmentForm->EnterBy->setValue ($userId);
		$EnterDate = date('Y-m-d');
		$this->view->lobjReceivableAdjustmentForm->EnterDate->setValue ($EnterDate);

		//Document Date
		$this->view->lobjReceivableAdjustmentForm->DocumentDate->setValue ($EnterDate);
		$this->view->lobjReceivableAdjustmentForm->DocumentDate->setAttrib('readonly','true');

		//Adjustment Number
//		$lobjSubjectsetup = new GeneralSetup_Model_DbTable_Initialconfiguration();
//		$larrconfig = $lobjSubjectsetup->fnGetInitialConfigDetails($lIntIdUniversity);
//		$lstradjustmentnumber =  $larrconfig['ReceivableAdjustmentCodeType'];
		
		//get seqno
		$seq_no = $this->getCurrentSeq();
		$this->view->lobjReceivableAdjustmentForm->AdjustmentNumber->setValue ($seq_no);
		
		//$lstradjustmentnumber = 1;
		/*if($lstradjustmentnumber=='1'){
			$this->view->lobjReceivableAdjustmentForm->AdjustmentNumber->setAttrib('readonly','true');
			$this->view->lobjReceivableAdjustmentForm->AdjustmentNumber->setAttrib('required','false');
			$this->view->lobjReceivableAdjustmentForm->AdjustmentNumber->setValue('xxx-xxx-xxx');
			$larrformatkey = array('format'=>'ReceivableAdjustmentIdFormat',
					'prefix'=>'ReceivableAdjustmentPrefix');
			$table = 'tbl_receivable_adjustment';
			$coloumn = 'AdjustmentCode';
			
			
			$lobjcodegenObj = new Cms_CodeGeneration();
			
			$lstrcoderesult = $lobjcodegenObj->studentfinanceIdGenration($lIntIdUniversity, $larrconfig,$table,$coloumn,$larrformatkey);
		}*/

		//Status
		$keyID = 'ENTRY';

//		$this->view->lobjReceivableAdjustmentForm->AdjustmentType->setAttrib('OnChange','showbelow(this.value);');
		//$this->view->lobjReceivableAdjustmentForm->Search->setAttrib('OnClick','return searchonreceiptno();');
		//$this->view->lobjReceivableAdjustmentForm->ReceiptNumber->setAttrib('OnBlur','return checkreceiptnumber(this.value);');
		$this->view->lobjReceivableAdjustmentForm->IdPaymentGroup->setAttrib('OnChange','fngetpaymentmode(this)');
//		$this->view->lobjReceivableAdjustmentForm->Save->setAttrib('OnClick','saveValidation();');
		$this->view->lobjReceivableAdjustmentForm->Add->setAttrib('OnClick','addValidation();');
		$this->view->lobjReceivableAdjustmentForm->Clear->setAttrib('OnClick','clearpageAdd();');

		if ($this->_request->isPost () && $this->_request->getPost ('Save')) 
		{

			$larrformData = $this->_request->getPost ();
			
			echo "<pre>";
			print_r($larrformData);
			
			//checklist
			//RC-2013-00030
			/* 
			munzir was  here 9 oct 2013
			
			$receipt = $this->lobjrecieptModel->fngetReceiptdet($larrformData['ReceiptNumber'],'a.ReceiptCode');
			$applicant = $this->lobjapplicantpersonalModel->getStudentdet($receipt[0]['PayeeCode'],'a.IdApplicant');
			$larrchecklist = $this->lobjProgramCheckList->fnViewProgramchecklist($applicant['ProvisionalProgramOffered']);
			if ( !empty($larrchecklist) )
			{
				foreach($larrchecklist as $rec)
				{
					$checkListtype = $this->lobjStudentapproval->fngetchecklistName($rec['ChecklistType']);	
					if ( $checkListtype == 'Processing Fee')
					{
						$data['Comments'] = "";
						$data['Confirm'] = 0;
						$data['Varified'] = 1;
						$data['Status'] = 187;				
						$where_ap = " IdApplication = '".$applicant['IdApplication']."' AND IdCheckList = '".$rec['IdCheckList']."'";
						
						$db = Zend_Db_Table :: getDefaultAdapter();
						$db->update('tbl_varifiedprogramchecklist',$data,$where_ap);
					}
				}
			}
			*/
			
			//Check Adjustment number
			
			$lobjReceivableAdjustment = new Studentfinance_Model_DbTable_Receivableadjustment();
			$lstrAdjustmentNumber = $larrformData['AdjustmentNumber'];
			/*$lstradjustmentnumber = 1;
			if($lstradjustmentnumber=='0'){
				$lstrcheck = $larrformData['AdjustmentNumber'];
				$result = $lobjReceivableAdjustment->fncheckadjustmentcode($lstrcheck,$lIntIdUniversity);
				if($result){
					$this->view->errMsg = '0';
					$this->view->lobjReceivableAdjustment->populate($result);
				}
				else{
					
				}
			}
			else{
				$lstrAdjustmentNumber = $lstrcoderesult;
			}*/

			//Check Recepit Status
			$lstrReceiptStatus = $larrformData['idreceipt'];
			$keyCheck = "APPROVE";
			$lstrcheckstatus = $lobjReceivableAdjustment->fncheckreceiptstatus($lstrReceiptStatus,strtoupper($keyCheck));
			
			if(!$lstrcheckstatus){
				$this->view->errMsg1 = '0';
				return $this->render('addreceivable');
			}

			//insert data
			$larrreceiptnumber = $lobjReceivableAdjustment->fnAddReceivableAdjustment($larrformData,$keyID,$lstrAdjustmentNumber);
			
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->id,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Add Receivable Adjustment' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			$this->_redirect( $this->baseUrl . '/studentfinance/receivableadjustment/');
		}
	}

	public function getreceiptdetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lstrreceiptnumber=  $this->_getParam ( 'receiptnumber' );
		
		$lobjReceivableAdjustment = new Studentfinance_Model_DbTable_Receivableadjustment();
		$lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
		
		$larrreceiptnumber = $lobjReceivableAdjustment->fngetReceiptnoDetails($lstrreceiptnumber);

		if(!empty($larrreceiptnumber)){
			if($larrreceiptnumber[0]['CountRecivableadjustment'] > 0){
				echo 'receivableexist';
			}else{
				$lstrstatus = $lobjDefCodeModel->fngetDefinationcode('Change Status',strtoupper($larrreceiptnumber[0]['rcp_status']));
				if($lstrstatus == 'ENTRY'){
					echo 'entry';
				}else if($lstrstatus == 'APPROVE'){
						echo Zend_Json_Encoder::encode($larrreceiptnumber);
				}
			}
		}else{
			echo 'not found';
		}
	}

	public function getreceiptinvoicedetlAction(){
		$this->_helper->layout->disableLayout();
		$lintIdReceipt =  $this->_getParam ( 'IdReceipt' );
		$larrReceiptInvoiceDetail = array();
		if($lintIdReceipt!=''){
			$larrReceiptInvoiceDetail = $this->lobjrecieptModel->fngetReceiptInvoiceDet($lintIdReceipt);
			if(!empty($larrReceiptInvoiceDetail)){
				$this->view->invoicedet = $larrReceiptInvoiceDetail;
			}
		}

	}

	public function getreceiptAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lstrreceiptid=  $this->_getParam ( 'receiptnumber' );
		$larrreceiptid = $this->lobjReceivableAdjustment->fngetReceiptBankDetails($lstrreceiptid);
		//print_r($larrreceiptid);
		echo Zend_Json_Encoder::encode($larrreceiptid);
	}

	public function editreceivableAction(){
		//Form
		$this->view->lobjReceivableAdjustmentForm = new Studentfinance_Form_Receivableadjustment();
		$this->view->defaultlanguage = 'EN';
		$id = $this->_getParam ( 'id' );

		//IdGroupPayment
		$DefmodelObj = new App_Model_Definitiontype();
		$lobjReceivableAdjustmentdb = new Studentfinance_Model_DbTable_Receivableadjustment();	
		
		/*$larrIdPaymentGroup = $DefmodelObj->fnGetDefinationMs('Group Payment');
		foreach($larrIdPaymentGroup as $larrvalues) {
			$this->view->lobjReceivableAdjustmentForm->IdPaymentGroup->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		//PayDocBank
		$larrPayDocBank = $lobjReceivableAdjustmentdb->fngetbanktype();
		foreach($larrPayDocBank as $larrvalues) {
			$this->view->lobjReceivableAdjustmentForm->PayDocBank->addMultiOption($larrvalues['AUTOINC'],$larrvalues['BANKCODE']);
		}

		//TerminalId
		$larrTerminalId = $lobjReceivableAdjustmentdb->fngetterminalid();
		foreach($larrTerminalId as $larrvalues) {
			$this->view->lobjReceivableAdjustmentForm->TerminalId->addMultiOption($larrvalues['IdTerminal'],$larrvalues['TerminalID']);
		}

		//Bank Card type
		$larrBankCard = $lobjReceivableAdjustmentdb->fngetbankcard();
		foreach($larrBankCard as $larrvalues) {
			$this->view->lobjReceivableAdjustmentForm->BankCardType->addMultiOption($larrvalues['IdBankCard'],$larrvalues['BankCardType']);
		}

		//Mode of Payment
		$larrPaymentMode = $lobjReceivableAdjustmentdb->fngetmodeofpayment();
		foreach($larrPaymentMode as $larrvalues) {
			$this->view->lobjReceivableAdjustmentForm->PaymentMode->addMultiOption($larrvalues['IdPaymentMode'],$larrvalues['ModeOfPayment']);
		}*/

		$larrResult = $lobjReceivableAdjustmentdb->fngetReceivableById($id);


		//$result = $this->lobjReceivableAdjustment->fngetReceivablePaymentInfoById($larrResult);
		/*$larrPaymentInfoDet = $this->lobjrecieptModel->fngetReceiptPaymentInfoDetails($larrResult[0]['IdReceipt'],'New');
		if(!empty($larrPaymentInfoDet)){
			$this->view->larrPaymentInfoDet = $larrPaymentInfoDet;
		}*/


		$this->view->lobjReceivableAdjustmentForm->AdjustmentNumber->setValue($larrResult[0]['AdjustmentCode']);
		$this->view->lobjReceivableAdjustmentForm->ReceiptNumber->setValue($larrResult[0]['ReceiptCode']);
		$this->view->lobjReceivableAdjustmentForm->PaymentDocNo->setValue($larrResult[0]['ChequeNo']);
		$this->view->lobjReceivableAdjustmentForm->PayDocBank->setValue($larrResult[0]['PaymentDocBank']);
		$this->view->lobjReceivableAdjustmentForm->IdPaymentGroup->setValue($larrResult[0]['PaymentGroup']);
		$this->view->lobjReceivableAdjustmentForm->PaymentMode->setValue($larrResult[0]['PaymentMode']);

		//$this->view->lobjReceivableAdjustmentForm->PayDocBankBranch->setAttrib('readonly','true');
		//$this->view->lobjReceivableAdjustmentForm->PayDocBank->setAttrib('readonly','true');
		//$this->view->lobjReceivableAdjustmentForm->PaymentDocNo->setAttrib('readonly','true');
		//$this->view->lobjReceivableAdjustmentForm->PaymentMode->setAttrib('readonly','true');
		//$this->view->lobjReceivableAdjustmentForm->IdPaymentGroup->setAttrib('readonly','true');

		$this->view->result = $larrResult[0];
		$this->view->lobjReceivableAdjustmentForm->populate($larrResult[0]);
		if(!empty($larrResult)){
			$lobjDefCodeModel = new GeneralSetup_Model_DbTable_Maintenance();
			$larrdefCode = $lobjDefCodeModel->fngetDefinationcode('Adjustment Type',$larrResult[0]['AdjustmentType']);
			$this->view->adjustmenttype = $larrdefCode;
			if($larrdefCode == 'Wrong Payment item'){
				$lobjrecevableinvoiceModel = new Studentfinance_Model_DbTable_Receivableadjustmentinvoice();
				$larrinvoicedetl = $lobjrecevableinvoiceModel->fngetReceivableadjustmentinvoicedetl($id);
				$this->view->invoicedet = $larrinvoicedetl;
				$lobjrecevablenoninvoiceModel = new Studentfinance_Model_DbTable_Receivableadjustmentnoninvoice();
				$larrnoninvoicedetl = $lobjrecevablenoninvoiceModel->fngetReceivableadjustmentnoninvoicedetl($id);
				$this->view->noninvoicedet = $larrnoninvoicedetl;
			}
		}
	}
	
	public function searchReceiptAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$id = $this->_getParam('idreceipt', null);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			 
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$result = null;
			
			$select = $db->select()
						->from(array('a'=>'receipt'))
						->joinLeft(array('cr'=>'tbl_currency'), 'cr.cur_id = a.rcp_cur_id', array('*'))
						->joinLeft(array('b'=>'receipt_invoice'), 'b.rcp_inv_rcp_id = a.rcp_id', array('*'))
						->joinLeft(array('c'=>'payment'), 'c.p_rcp_id = a.rcp_id', array('*'))
						->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = a.rcp_trans_id', array('payee_id'=>'at.at_pes_id'))
						->joinLeft(array('ap'=>'applicant_profile'), 'a.rcp_appl_id = ap.appl_id', array("CONCAT_WS (
							' ',
							IFNULL(ap.appl_fname,''),
							IFNULL(ap.appl_mname,''),
							IFNULL(ap.appl_lname,'')
							) as payee_name"))
						->joinLeft(array('adv'=>'advance_payment'), 'adv.advpy_rcp_id = a.rcp_id', array('advancepaymentamount'=>'adv.advpy_amount'));
			 
			$select->where("a.rcp_id = ?",$id);
			
			$result = $db->fetchRow($select);
				
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($result);
				
			echo $json;
			exit();
		
		}
	}
	
	public function getpaymentmodeAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lIntpaymentgroup = $this->getRequest()->getParam('id');
		$paymentGroupDB = new Studentfinance_Model_DbTable_PaymentMode();
		$larrpaymentModeList = $paymentGroupDB->fnGetPaymentMode($lIntpaymentgroup);
		echo Zend_Json_Encoder::encode($larrpaymentModeList);
		
	}
	
	/*
	 *  Get current  seq no
	 */
	private function getCurrentSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 6")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		if($row2){
			$r_num = $row2['ReceivableAdjustmentPrefix'].$row2['ReceivableAdjustmentSeparator'].date('Y').$row2['ReceivableAdjustmentSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	/*
	 * Get Bill no from mysql function
	*/
	private function getSeq(){
	
		$seq_data = array(
				6,
				date('Y'),
				0,
				0,
				0
		);
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS seq_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		return $seq['seq_no'];
	}
	
	public function approveAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			$adjustment = $formData['approve'];
			$n=0;
			while($n < count($adjustment)){
				$adj_id = $adjustment[$n];
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
			
				$upd_data = array(
					'Status'=> 'APPROVE',
					'UpdUser'=> $getUserIdentity->id,
					'UpdDate'=> date('Y-m-d H:i:s')
				);
				
				$adjustmentDb = new Studentfinance_Model_DbTable_Receivableadjustment();
				$adjustmentDb->update($upd_data, array('IdReceivableAdjustment = ?' => $adj_id) );
				
				$db = Zend_Db_Table::getDefaultAdapter();
				
				$select = $db->select()
								->from(array('a'=>'tbl_receivable_adjustment'))
								->where('IdReceivableAdjustment = ?',$adj_id);
				$row = $db->fetchRow($select);
				
				$upd_data_receipt = array(
					'rcp_status'=> 'CANCEL',
					'UpdUser'=> $getUserIdentity->id,
					'UpdDate'=> date('Y-m-d H:i:s'),
					'CountRecivableadjustment' => 1,
					'adjustment_id' => $adj_id
				);
				
				$adjustmentDb = new Studentfinance_Model_DbTable_Receivableadjustment();
				$adjustmentDb->update($upd_data, array('IdReceivableAdjustment = ?' => $row['IdReceipt']) );
				
				
				//advance payment
				$data_adv = array(
					'advpy_status' => 'X',
					'cancel_by'=> $getUserIdentity->id,
					'cancel_date'=> date('Y-m-d H:i:s')
				);
				
				$advPaymentDB = new Studentfinance_Model_DbTable_AdvancePayment();
				$advPaymentDB->update($data_adv,array('advpy_rcp_id = ?' => $row['IdReceipt']) );
				
				$n++;
			}
		}
		$this->_redirect( $this->baseUrl . '/studentfinance/receivableadjustment');
		exit;
    }
    
	public function cancelAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
				
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
		
			$adj_id = $formData['adjust_id'];
			
			$upd_data = array(
				'Status'=> 'CANCEL',
				'cancel_remarks'=> $formData['remarks'],
				'cancel_by'=> $getUserIdentity->id,
				'cancel_date'=> date('Y-m-d H:i:s')
			);
			
			$adjustmentDb = new Studentfinance_Model_DbTable_Receivableadjustment();
			$adjustmentDb->update($upd_data, array('IdReceivableAdjustment = ?' => $adj_id) );
				
		}
		$this->_redirect( $this->baseUrl . '/studentfinance/receivableadjustment');
		exit;
    }
	
}