<?php

class Studentfinance_ScholarshipCountryWeightController extends Zend_Controller_Action
{

    public function indexAction() {
        $CountryWeight = new Studentfinance_Model_DbTable_ScholarshipCountryWeight();
        $this->view->countryweights = $CountryWeight->fetchAll();
    }

    public function addAction() {
        $CountryWeight = new Studentfinance_Model_DbTable_ScholarshipCountryWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('countryweight');
            $CountryWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-country-weight/index');
        }

    }

    public function editAction() {
        $CountryWeight = new Studentfinance_Model_DbTable_ScholarshipCountryWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('countryweight');
            $CountryWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-country-weight/index');
        }

        $id =$this->_getParam('id');
        $countryweight = $CountryWeight->find($id);
        if($countryweight->count() > 0) {
            $this->view->countryweight = $countryweight->current();
        } else {
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-country-weight/index');
        }
    }

    public function deleteAction() {
        $CountryWeight = new Studentfinance_Model_DbTable_ScholarshipCountryWeight();
        $id =$this->_getParam('id');
        $countryweight = $CountryWeight->find($id);
        $countryweight->current()->delete();
        $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record deleted"));
        $this->redirect( $this->baseUrl . '/studentfinance/scholarship-country-weight/index');
    }

}