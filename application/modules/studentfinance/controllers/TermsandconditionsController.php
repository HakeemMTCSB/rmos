<?php
class Studentfinance_TermsandconditionsController extends Base_Base { //Controller for the User Module	
	private $lobjTermsandconditions;
	private $lobjTermsandconditionsForm;
	private $_gobjlog;
	
	public function init(){
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();
	}
	public function fnsetObj(){
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjTermsandconditions = new Studentfinance_Model_DbTable_Termsandconditions();
		$this->lobjTermsandconditionsForm = new Studentfinance_Form_Termsandconditions(); 
	}
	public function indexAction() { // action for search and view
		$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view		
		$larrresult = $this->lobjTermsandconditions->fngetlobjTermsandconditionsDetails (); //get user details
		if(!$this->_getParam('search')) unset($this->gobjsessionsis->Termsandconditionspaginatorresult);		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); 
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->Termsandconditionspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Termsandconditionspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjTermsandconditions-> fngetlobjTermsandconditionsDetailsSearch($this->_request->getPost ());  //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Termsandconditionspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/termsandconditions/index');
		}
	}
	public function newtermsandconditionsAction() { //Action for creating the new user
		$this->view->lobjTermsandconditionsForm = $this->lobjTermsandconditionsForm; 			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post			
			unset ( $larrformData ['Save'] );		
			if ($this->lobjTermsandconditionsForm->isValid ( $larrformData )) {	
				$auth = Zend_Auth::getInstance();				
				$larrformData['UpdUser'] = $auth->getIdentity()->iduser;				
				$larrformData['UpdDate'] = date ( 'Y-m-d H:i:s' );				
				$result = $this->lobjTermsandconditions->fnInserttermsandconditions($larrformData); //instance for adding the lobjuserForm values to DB	

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Terms And Conditions Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/termsandconditions/index');
			}
		}
	}
	public function termsandconditionsAction() { //Action for the updation and view of the user details
		$this->view->lobjTermsandconditionsForm = $this->lobjTermsandconditionsForm; 		
		$idTermsandconditions = ( int ) $this->_getParam ( 'id' );			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {		
			$larrformData = $this->_request->getPost ();
			unset ( $larrformData ['Save'] );
			if ($this->lobjTermsandconditionsForm->isValid ($larrformData)) {
				$auth = Zend_Auth::getInstance();				
				$larrformData['UpdUser'] = $auth->getIdentity()->iduser;				
				$larrformData['UpdDate'] = date ( 'Y-m-d H:i:s' );					
				$this->lobjTermsandconditions->fnUpdatetermsandconditions($larrformData,$idTermsandconditions); //instance for adding the lobjuserForm values to DB
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Terms And Conditions Edit Id=' . $idTermsandconditions,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/termsandconditions/index');
			}			
		}
		else{
			$this->view->idTermsandconditions = $idTermsandconditions;					
			$larrresult = $this->lobjTermsandconditions->fngetlobjTermsandconditionsDetails($idTermsandconditions); //getting bill structuer based on billstructureid		
			$this->lobjTermsandconditionsForm->Termname->setValue($larrresult[0]['Termname']);
			$this->lobjTermsandconditionsForm->Description->setValue($larrresult[0]['Description']);
			$this->lobjTermsandconditionsForm->Default->setValue($larrresult[0]['Default']);
			$this->lobjTermsandconditionsForm->Active->setValue($larrresult[0]['Active']);
		}
		
	}	
}