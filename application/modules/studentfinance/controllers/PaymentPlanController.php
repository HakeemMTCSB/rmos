<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_PaymentPlanController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_FeeStructurePlan();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Payment Plan Set-up");
    	
    	$this->view->searchForm = new Studentfinance_Form_FeeStructure();
    	
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//paginator
			$data = $feeStructureDb->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->paginator = $paginator;
		
    	}
    	    	
	}
	
	public function localAction(){
		
		$this->_helper->layout->disableLayout();
		
		$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
		
		//paginator
		$data = $feeStructureDb->getPaginateDataByCategory(314);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function foreignAction(){
		
		$this->_helper->layout->disableLayout();
		
		$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
		
		//paginator
		$data = $feeStructureDb->getPaginateDataByCategory(315);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function detailAction(){
    	$id = $this->_getParam('id', 0);
    	$this->view->id = $id;
    	
    	//title
    	$this->view->title= $this->view->translate("Payment Plan Set-up")." - ".$this->view->translate("Detail");
    	
    	//fee-structure data
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	$this->view->fee_structure = $feeStructureDb->getData($id);
    	
    	
    	
    	//fee structure plan
    	$this->view->fee_structure_plan_list = $this->_DbObj->getStructureData($id);
    	
    }
	
	public function addAction()
    {
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			$id = $this->_DbObj->addData($formData);

			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-plan', 'action'=>'detail', 'id'=>$formData['fsp_structure_id']),'default',true));
        }else{
        	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-plan', 'action'=>'index'),'default',true));
        }
    }
    
	public function editAction(){
		$code = $this->_getParam('code', null);
		
		//title
    	$this->view->title= $this->view->translate("Discipline Set-up")." - ".$this->view->translate("Edit");
    	
    	$form = new GeneralSetup_Form_SchoolDiscipline();
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
	    	if ($form->isValid($formData)) {
				
				$this->_DbObj->updateData($formData,$code);
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true)); 
			}else{
				$form->populate($formData);	
			}
    	}else{
    		if($code!=null){
    			
    			$form->populate($this->_DbObj->getData($code));
    		}
    	}
    	
    	$this->view->form = $form;
    }
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	
    	if($id!=0){
    		$this->_DbObj->deleteData($id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true));
    	
    }
    
    public function planDetailAction(){
    	
    	$msg = $this->_getParam('msg', null);
    	if($msg){
    		$this->view->noticeMessage = $msg;
    	}
    	
    	$id = $this->_getParam('id', 0);
    	$this->view->plan_id = $id;
    	$this->view->title= $this->view->translate("Payment Plan Set-up")." - ".$this->view->translate("Installment");
    	
    	//payment-plan data
    	$paymentPlanData = $this->_DbObj->getData($id);
    	$this->view->payment_plan = $paymentPlanData;
    	
    	//fee structure data
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	$this->view->fee_structure = $feeStructureDb->getData($paymentPlanData['fsp_structure_id']);
    	
    	//fee structure item
    	$feeStructureItem = new Studentfinance_Model_DbTable_FeeStructureItem();
    	$this->view->feeStructureItemList = $feeStructureItem->getStructureData($paymentPlanData['fsp_structure_id']); 
    	
    	//fee structure plan detail
    	$paymentPlanDetail = array();
    	$fsPlanDetailDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();
    	
    	for($i=0; ($i+1)<=(int)$paymentPlanData['fsp_bil_installment']; $i++){
    		$paymentPlanDetail[$i] = $fsPlanDetailDb->getPlanData($paymentPlanData['fsp_structure_id'], $id, ($i+1));
    	}
    	$this->view->paymentPlanDetail = $paymentPlanDetail;
    }
    
	public function ajaxSavePaymentPlanDetailAction(){
		$result = array('status'=>false);
		$fspd_id = $this->_getParam('fspd_id', 0);
		$plan_id = $this->_getParam('fspd_plan_id', 0);
		$installment = $this->_getParam('fspd_installment_no', 0);
		$item_id = $this->_getParam('fspd_item_id', 0);
    	
    	
    	$amount = $this->_getParam('fspd_amount', null);
    	$percentage = $this->_getParam('fspd_percentage', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
        $fspdDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();
        if($fspd_id!="" && $fspd_id!=0){
        	//update
        	$data = array(
        				'fspd_percentage' => $percentage,
        				'fspd_amount' => $amount
        			);
        	
        	$result['id'] = 0;
        	if( $fspdDb->updateData($data, $fspd_id) ){
        		$result['status'] = true;
        	}
        	
        }else{
        	//add
        	$data = array(
        				'fspd_plan_id' => $plan_id,
        				'fspd_installment_no' => $installment,
        				'fspd_item_id' => $item_id,
        				'fspd_percentage' => $percentage,
        				'fspd_amount' => $amount
        			);
        			
        	 
        	$result['id'] = $fspdDb->addData($data);
        	$result['status'] = true;
        }
	  	
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode( $result );
		
		echo $json;
		exit();
    }
}

