<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */

class Studentfinance_CurrencyController extends Base_Base {
	
	public function setupAction(){
		
		$this->view->title = "Currency Setup";
		
		
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$where = array();
			
			$search = array();
			if($formData['desc']!=""){
				$where['cur_desc like "%?%"'] = $formData['desc'];
				$search['desc'] = $formData['desc'];
			}
			
			if($formData['code']!=""){
				$where["cur_code = ?"] = $formData['code'];
				$search['code'] = $formData['code'];
			}
			
			$this->view->search = $search;
			
			
			/*echo "<pre>";
			print_r($where);
			echo "</pre>";
			exit;*/
			
			$this->view->currency = $currencyDb->fetchAll($where)->toArray();
		}else{
			$this->view->currency = $currencyDb->fetchAll()->toArray();
		}
		
	}
	
	public function addCurrencyAction(){
		$this->view->title = "Currency Setup - Add Currency";
		
		$form = new Studentfinance_Form_Currency();
		

		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			if ($form->isValid($formData)) {
				
				$data = array(
					'cur_code' => $formData['cur_code'],
					'cur_desc' => $formData['cur_desc'],
					'cur_desc_default_language' => $formData['cur_desc_default_language'],
					'cur_status' => $formData['cur_status'],
					'cur_symbol_prefix' => $formData['cur_symbol_prefix'],
					'cur_symbol_suffix' => $formData['cur_symbol_suffix'],
					'cur_decimal' => $formData['cur_decimal'],
				);
				
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				
				$currencyDb->insert($data);
				
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new currency');
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'currency', 'action'=>'setup'),'default',true));
				
			}else{
				$form->populate($formData);
			}
		}
		
		$this->view->form = $form;
		
	}
	
	public function editCurrencyAction(){
		$cur_id = $this->getParam('id', null);
		
		if($cur_id==null){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'currency', 'action'=>'setup'),'default',true));
		}
		
		
		$this->view->title = "Currency Setup - Edit Currency";
		
		$form = new Studentfinance_Form_Currency();
		
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				$data = array(
						'cur_code' => $formData['cur_code'],
						'cur_desc' => $formData['cur_desc'],
						'cur_desc_default_language' => $formData['cur_desc_default_language'],
						'cur_status' => $formData['cur_status'],
						'cur_symbol_prefix' => $formData['cur_symbol_prefix'],
						'cur_symbol_suffix' => $formData['cur_symbol_suffix'],
						'cur_decimal' => $formData['cur_decimal'],
				);
		
				
				$where = array('cur_id = ?'=>$cur_id);
				$currencyDb->update($data, $where);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success edit currency');
		
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'currency', 'action'=>'setup'),'default',true));
		
			}else{
				$form->populate($formData);
			}
		}else{
			
			$where = array('cur_id = ?'=>$cur_id);
			$data = $currencyDb->fetchRow($where)->toArray();
			
			$form->populate($data);
		}
		
		$this->view->form = $form;
		
	}
	
	public function setDefaultCurrencyAction(){
		
		$cur_id = $this->getParam('id', null);
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		
		$currencyDb->update(array('cur_default'=>'N'));
		
		$where['cur_id = ?'] = $cur_id;
		$currencyDb->update(array('cur_default'=>'Y'), $where);
		
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success setting default currency');
		
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'currency', 'action'=>'setup'),'default',true));
		
	}
	
	public function rateSetupAction(){
		
		$this->view->title = "Currency Rate Setup";
		
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
		$currency = null;
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			$where = array();
				
			$search = array();
			if($formData['desc']!=""){
				$where['cur_desc like "%?%"'] = $formData['desc'];
				$search['desc'] = $formData['desc'];
			}
				
			if($formData['code']!=""){
				$where["cur_code = ?"] = $formData['code'];
				$search['code'] = $formData['code'];
			}
				
			$this->view->search = $search;
				
			$currency = $currencyDb->fetchAll($where)->toArray();
				
			
			 
		}else{
			$currency = $currencyDb->fetchAll()->toArray();
		}
		
		
		//rate
		if($currency!=null){
			foreach ($currency as $index => $curr){
				
				$wh = array('cr_cur_id = ?' => $curr['cur_id'], 'cr_effective_Date <= now()');
				
				$rate = $currencyRateDb->fetchRow($wh, 'cr_effective_date DESC');
				
				if($rate){
					$rate = $rate->toArray();
				}
				
				$currency[$index]['rate'] = $rate;
			}
			
		}
		
		$this->view->currency = $currency;


	}
	
	public function rateSetupDetailAction(){
		
		$cur_id = $this->_getParam('id',null);
		
		$this->view->title = "Currency Rate Setup - Detail";
		
		$form = new Studentfinance_Form_CurrencyRate(array('currency'=>$cur_id));
		$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				$data = array(
						'cr_cur_id' => $formData['cr_cur_id'],
						'cr_exchange_rate' => $formData['cr_exchange_rate'],
						'cr_min_rate' => $formData['cr_min_rate'],
						'cr_max_rate' => $formData['cr_max_rate'],
						'cr_effective_date' => $formData['cr_effective_date'],
				);
		
				
				$currencyRateDb->insert($data);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add rate');
		
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'currency', 'action'=>'rate-setup-detail', 'id'=>$cur_id),'default',true));
		
			}else{
				$form->populate($formData);
			}
		}
		
		$this->view->form = $form;
		
		$currency = $currencyRateDb->getRate($cur_id,'cr.cr_effective_date ASC');
		$this->view->currency = $currency;

		
	}
	
}