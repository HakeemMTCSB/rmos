<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 28/3/2016
 * Time: 8:45 AM
 */
class Studentfinance_BillingVsCourseregController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_BillingVsCoursereg();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Billing Vs Course Registration Report');

        //intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $this->view->intakeData = $intakeDb->fngetallIntakelist();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $this->view->semester = isset($formData['semester'])?$formData['semester']:0;
            $this->view->intake = isset($formData['intake'])?$formData['intake']:0;

            $form = new Studentfinance_Form_BillingVsCoursereg(array('programid'=>$formData['program']));
            $this->view->form = $form;
            $form->populate($formData);

            $list = $this->model->getInvoice($formData);
            //var_dump($list); exit;
            $this->view->list = $list;
        }else{
            $date = date('d-m-Y');
            $populate = array(
                'datefrom'=>$date,
                'dateto'=>$date
            );
            $form = new Studentfinance_Form_BillingVsCoursereg();
            $this->view->form = $form;
            $form->populate($populate);
        }
    }

    public function printAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['semester'] = $formData['semester_id'];
            $formData['intake'] = $formData['intake_id'];

            $list = $this->model->getInvoice($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_billing_vs_course_registration_report.xls';
    }

    public function getSemesterAction(){
        $id = $this->_getParam('id', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $programInfo = $this->model->programById($id);

        if ($programInfo){
            $semesterList = $this->model->getSemester($programInfo['IdScheme']);
        }

        $json = Zend_Json::encode($semesterList);

        echo $json;
        exit();
    }
}