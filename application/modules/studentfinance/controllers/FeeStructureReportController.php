<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 3/8/2016
 * Time: 11:00 AM
 */
class Studentfinance_FeeStructureReportController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_FeeStructureReport();

        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function setupDetailsAction(){
        $this->view->title = $this->view->translate('Fee Structure Details');
        $feeStructureModel = new Studentfinance_Model_DbTable_FeeStructure();
        $feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
        $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Studentfinance_Form_FeeStructureReport(array('programId'=>$formData['program']));
            $form->populate($formData);
            $this->view->form = $form;

            $list = $this->model->getFeeStructure($formData);

            if ($list){
                foreach ($list as $key=>$loop){
                    $info = $feeStructureModel->getData($loop['fs_id']);
                    $list[$key]['Info'] = $info;

                    //fee structure program
                    $list[$key]['Program'] = $feeStructureProgramDb->getStructureData($loop['fs_id']);

                    if (count($list[$key]['Program']) > 0){
                        foreach ($list[$key]['Program'] as $key2 => $loop2){
                            //application
                            $list[$key]['Program'][$key2]['item_application'] = $feeStructureProgramDb->getItemData($loop['fs_id'], $loop2['fsp_id'], 'application');

                            //registration
                            $list[$key]['Program'][$key2]['item_registration'] = $feeStructureProgramDb->getItemData($loop['fs_id'], $loop2['fsp_id'], 'registration');
                        }
                    }

                    //fee structure item
                    $list[$key]['Item'] = $feeStructureItemDb->getStructureData($loop['fs_id']);
                }
            }

            $this->view->list = $list;
        }else{
            $form = new Studentfinance_Form_FeeStructureReport();
            $this->view->form = $form;
        }
    }

    public function setupDetailsExportAction(){
        $feeStructureModel = new Studentfinance_Model_DbTable_FeeStructure();
        $feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
        $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();

        //global $list;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Studentfinance_Form_FeeStructureReport(array('programId'=>$formData['program']));
            $form->populate($formData);
            $this->view->form = $form;

            $list = $this->model->getFeeStructure($formData);

            if ($list){
                foreach ($list as $key=>$loop){
                    $info = $feeStructureModel->getData($loop['fs_id']);
                    $list[$key]['Info'] = $info;

                    //fee structure program
                    $list[$key]['Program'] = $feeStructureProgramDb->getStructureData($loop['fs_id']);

                    if (count($list[$key]['Program']) > 0){
                        foreach ($list[$key]['Program'] as $key2 => $loop2){
                            //application
                            $list[$key]['Program'][$key2]['item_application'] = $feeStructureProgramDb->getItemData($loop['fs_id'], $loop2['fsp_id'], 'application');

                            //registration
                            $list[$key]['Program'][$key2]['item_registration'] = $feeStructureProgramDb->getItemData($loop['fs_id'], $loop2['fsp_id'], 'registration');
                        }
                    }

                    //fee structure item
                    $list[$key]['Item'] = $feeStructureItemDb->getStructureData($loop['fs_id']);
                }
            }
        }

        $content = '';
        if ($list) {
            $count = 0;
            foreach ($list as $loop) {
                $count++;
                $content .= '<h3>'.$loop['fs_name'].'</h3>';
                $content .= '<fieldset>';
                $content .= '<legend>Info</legend>';
                $content .= '<table class="table info" width="100%" cellpadding="3" cellspacing="1">';
                $content .= '<tr>';
                $content .= '<th width="200px">Fee Structure Name</th>';
                $content .= '<td>'.$loop['Info']['fs_name'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Intake</th>';
                $content .= '<td>'.$loop['Info']['s_Intake'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Semester</th>';
                $content .= '<td>'.$loop['Info']['semester_name'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Effective Date</th>';
                $content .= '<td>'.date('j F Y', strtotime($loop['Info']['fs_effective_date'])).'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Account Code (Debtor)</th>';
                $content .= '<td>'.$loop['Info']['ac_desc'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Pre Payment Account Code</th>';
                $content .= '<td>'.$loop['Info']['prepayment_code'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Currency</th>';
                $content .= '<td>'.$loop['Info']['cur_desc'].'('.$loop['Info']['cur_code'].')</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Category</th>';
                $content .= '<td>'.$loop['Info']['DefinitionDesc'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Estimated Total Fees</th>';
                $content .= '<td>';
                $content .= $loop['Info']['cur_symbol_prefix']!=""?$loop['Info']['cur_symbol_prefix']:"";
                $content .= $loop['Info']['fs_estimated_total_fee'];
                $content .= $loop['Info']['cur_symbol_suffix']!=""?$loop['Info']['cur_symbol_suffix']:"";
                $content .= '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Collaborative Partner</th>';
                $content .= '<td>'.$loop['Info']['fs_cp'] == '' ? '-' : $loop['Info']['BranchName'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<th>Status</th>';
                $content .= '<td>';

                if($loop['Info']['fs_status'] == 0){
                    $content .= 'DRAFT';
                }elseif($loop['Info']['fs_status'] == 1){
                    $content .= 'ACTIVE';
                }elseif($loop['Info']['fs_status'] == 2){
                    $content .= 'INACTIVE';
                }

                $content .= '</td>';
                $content .= '</tr>';
                $content .= '</table>';
                $content .= '</fieldset>';
                $content .= '<br />';
                $content .= '<fieldset>';
                $content .= '<legend>Programmes</legend>';

                if (isset($loop['Program']) && count($loop['Program']) > 0) {
                    foreach ($loop['Program'] as $loopProgram) {
                        $content .= '<h3>'.$loopProgram['ProgramName'].'('.$loopProgram['mode_of_program_name'].' '.$loopProgram['mode_of_study_name'].' '.$loopProgram['program_type_name'].')'.'</h3>';
                        $content .= '<fieldset>';
                        $content .= '<legend>Application</legend>';
                        $content .= '<table width="100%" class="table">';
                        $content .= '<tr>';
                        $content .= '<th width="2%"></th>';
                        $content .= '<th>Fee Item</th>';
                        $content .= '<th>Fee Category</th>';
                        $content .= '<th>Document Type</th>';
                        $content .= '<th width="100px">Amount</th>';
                        $content .= '</tr>';

                        $i = 0;

                        foreach ($loopProgram['item_application'] as $item) {
                            $i++;

                            $content .= '<tr id="app-'.$item['fspi_id'].'">';
                            $content .= '<td align="center">'.$i.'</td>';
                            $content .= '<td>'.$item['feeitem_name'].'</td>';
                            $content .= '<td>'.$item['fc_desc'].'</td>';
                            $content .= '<td>'.$item['docType'].'</td>';
                            $content .= '<td align="right">'.($item['fspi_amount'] > 0 ? $item['cur_symbol_prefix'].' '.$item['fspi_amount']:'').'</td>';
                            $content .= '</tr>';
                        }

                        $content .= '</table>';
                        $content .= '</fieldset>';
                        $content .= '<br/>';
                        $content .= '<fieldset>';
                        $content .= '<legend>Registration</legend>';
                        $content .= '<table width="100%" class="table">';
                        $content .= '<tr>';
                        $content .= '<th width="2%"></th>';
                        $content .= '<th>Fee Item</th>';
                        $content .= '<th>Fee Category</th>';
                        $content .= '<th>Document Type</th>';
                        $content .= '<th width="100px">Amount</th>';
                        $content .= '</tr>';

                        $i = 0;

                        foreach ($loopProgram['item_registration'] as $item) {
                            $i++;

                            $content .= '<tr id="reg-'.$item['fspi_id'].'">';
                            $content .= '<td align="center">'.$i.'</td>';
                            $content .= '<td>'.$item['feeitem_name'].'</td>';
                            $content .= '<td>'.$item['fc_desc'].'</td>';
                            $content .= '<td>'.$item['docType'].'</td>';
                            $content .= '<td align="right">'.($item['fspi_amount'] > 0 ? $item['cur_symbol_prefix'].' '.$item['fspi_amount']:'').'</td>';
                            $content .= '</tr>';
                        }

                        $content .= '</table>';
                        $content .= '</fieldset>';
                        $content .= '<hr>';
                    }
                }

                $content .= '</fieldset>';
                $content .= '<br />';
                $content .= '<fieldset>';
                $content .= '<legend>Fees Item</legend>';
                $content .= '<table class="table" width="100%">';
                $content .= '<thead>';
                $content .= '<tr>';
                $content .= '<th colspan="2">Fee</th>';
                $content .= '<th>Code</th>';
                $content .= '<th>Registration Item</th>';
                $content .= '<th>Calculation Type</th>';
                $content .= '<th>Frequency Type</th>';
                $content .= '<th>Part / Semester / Subject</th>';
                $content .= '<th>Currency</th>';
                $content .= '<th>Amount</th>';
                $content .= '</tr>';
                $content .= '</thead>';
                $content .= '<tbody>';

                if(isset($loop['Item']) && count($loop['Item'])>0){
                    $bil = 1;
                    $totalAmount = 0;

                    foreach ($loop['Item'] as $fee_item) {
                        $content .= '<tr>';
                        $content .= '<td>'.$bil.'</td>';
                        $content .= '<td>'.$fee_item['fi_name'].'</td>';
                        $content .= '<td align="center">'.$fee_item['fi_code'].'</td>';
                        $content .= '<td>'.$fee_item['registrationItem'].'</td>';
                        $content .= '<td align="center">'.$fee_item['calType'].'</td>';
                        $content .= '<td align="center">'.$fee_item['freqMode'].'</td>';
                        $content .= '<td>';

                        if ($fee_item['semester'] != null) {
                            $content .= '<ul class="sem">Semester';
                            foreach ($fee_item['semester'] as $sem) {
                                $content .= '<li> Sem '.$sem['fsis_semester'].'</li>';
                            }
                            $content .= '</ul>';
                        }
                        if ($fee_item['subject'] != null) {
                            $content .= '<ul class="sem">Subject';
                            foreach ($fee_item['subject'] as $subject) {
                                $content .= '<li>'.$subject['SubCode'].'</li>';
                            }
                            $content .= '</ul>';
                        }
                        if ($fee_item['level'] != null) {
                            $content .= '<ul class="sem">Part';
                            foreach ($fee_item['level'] as $sem) {
                                $content .= '<li> Part'.$sem['fsis_level'].'</li>';
                            }
                            $content .= '</ul>';
                        }

                        $content .= '</td>';
                        $content .= '<td align="center">'.$fee_item['cur_code'].'</td>';
                        $content .= '<td align="right">';

                        if ($fee_item['fi_amount_calculation_type'] == 618) {
                            $content .= 'Refer Setup';
                        } else {
                            $content .= sprintf(" %.2f", $fee_item['fsi_amount']);
                        }

                        $totalAmount += $fee_item['fsi_amount'];

                        $content .= '</td>';
                        $content .= '</tr>';

                        $bil++;
                    }

                    $content .= '<tr>';
                    $content .= '<td colspan="8" align="right">Total Amount</td>';
                    $content .= '<td align="right">'.sprintf (" %.2f", $totalAmount).'</td>';
                    $content .= '</tr>';
                }

                $content .= '</tbody>';
                $content .= '</table>';
                $content .= '</fieldset>';
                $content .= '<hr>';
                $content .= '<br />';
                $content .= '<br />';

                if ($count != count($list)){
                    $content .= '<div style="page-break-after: always;"></div>';
                }
            }
        }

        //echo $content; exit;

        $fieldValues = array(
            '$[LOGO]'=> "images/logo_text_high.jpg",
            '$[CONTENT]'=> $content
        );

        $html_template_path = DOCUMENT_PATH."/template/fee_structure_report.html";
        $output_filename = "fee_structure_report.pdf";

        require_once 'dompdf_config.inc.php';
        error_reporting(0);

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);
        }

        //echo $html; exit;

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();

        $dompdf->stream($output_filename);

        exit;
    }
}