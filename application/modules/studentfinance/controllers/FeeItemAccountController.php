<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_FeeItemAccountController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_FeeItemAccount();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
			
    	//title
    	$this->view->title= $this->view->translate("Fee Item Account : Faculty");
    	
    	//paginator
    	$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$data = $facultyDb->getFaculty();
		
		$paginator = Zend_Paginator::factory($data);
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;  	
	}
	
	public function facultyDetailAction(){
		
		$fid = $this->_getParam('fid', null);
		$this->view->fid = $fid;
		
		//title
    	$this->view->title= $this->view->translate("Fee Item Account - Program");
    	
    	//faculty info
    	$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDb->getData($fid);
		$this->view->faculty = $faculty; 
    	
    	//program list
    	$programDb = new App_Model_Record_DbTable_Program();
    	$program = $programDb->searchProgramByFaculty($fid);
    	$this->view->program = $program;
    	
	}
	
	public function programDetailAction(){
		$fid = $this->_getParam('fid', null);
		$this->view->fid = $fid;
		
		$pid = $this->_getParam('pid', null);
		$this->view->pid = $pid;
		
		//title
    	$this->view->title= $this->view->translate("Fee Item Account - Fee Item");
		
		//faculty info
    	$facultyDb = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDb->getData($fid);
		$this->view->faculty = $faculty; 
		
		//program info
		$programDb = new App_Model_Record_DbTable_Program();
		$program = $programDb->getData($pid); 
		$this->view->program = $program;
		
		//fee item
		$feeItemAccDb = new Studentfinance_Model_DbTable_FeeItemAccount();
		$data = $feeItemAccDb->getFeeItem($fid, $pid);
		$this->view->data = $data;
		
	}
	
	public function feeItemUpdateAction(){
		if($this->_request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();	
		}
		
		
		$faculty_id = $this->_getParam('f_id', null);
		$program_id = $this->_getParam('p_id', null);
		$fee_item_id = $this->_getParam('fi_id', null);
		
		$form = new Studentfinance_Form_FeeItemAccount(array('feeitemid'=>$fee_item_id,'facultyid'=>$faculty_id,'programid'=>$program_id));
		$form->removeElement('cancel');
		$form->setAction($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item-account','action'=>'fee-item-update'),'default',true));
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$fee_item_acc = $this->_DbObj->getFeeItem($formData['fiacc_faculty_id'],$formData['fiacc_program_id'],$formData['fiacc_fee_item']);
				
				if( $fee_item_acc ){
					
					$data = array(
						'fiacc_account' => $formData['fiacc_account'],
						'fiacc_bank' => $formData['fiacc_bank']
					);

					$this->_DbObj->update($data, 'fiacc_id = '.$fee_item_acc['fiacc_id']);
					
				}else{
					
					$data = array(
						'fiacc_fee_item' => $formData['fiacc_fee_item'],
						'fiacc_faculty_id' => $formData['fiacc_faculty_id'],
						'fiacc_program_id' => $formData['fiacc_program_id'],
						'fiacc_account' => $formData['fiacc_account'],
						'fiacc_bank' => $formData['fiacc_bank']
					);

					$this->_DbObj->insert($data);
				}
				
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item-account', 'action'=>'program-detail', 'fid'=> $formData['fiacc_faculty_id'], 'pid'=> $formData['fiacc_program_id']),'default',true));	
			}else{
				$form->populate($formData);
			}
        	
        }else{
        	$data = $this->_DbObj->getFeeItem($faculty_id,$program_id,$fee_item_id);
            	
        	if($data){
        		$form->populate($data);	
        	}
        	
        }
		
		$this->view->form = $form;
	}
}

