<?php

/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_PaymentBankController extends Base_Base {
	
	private $_DbObj;
	private $_ses_paymentbank;
	private $_ses_suspendacc;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_PaymentBank();
		$this->_DbObj = $db;
		
		$this->_ses_paymentbank = new Zend_Session_Namespace('studentfinance_paymentbank');
		$this->_ses_suspendacc = new Zend_Session_Namespace('studentfinance_suspend_data');
		
		$action = $this->getRequest()->getActionName();
		if($action != 'upload-transaction' && $action != 'save-transaction'){
			$this->_ses_paymentbank->data = null;
		}
	}
	
	public function indexAction() {
		$status = $this->_getParam('status', false);
    	$this->view->status = $status;
    	
		//title
    	$this->view->title= $this->view->translate("Payment Bank");
    	
    	$upload_form = new Studentfinance_Form_BankPaymentTransactionUpload();
    	$this->view->upload_form = $upload_form; 
    	
    	//paginator
    	if ($this->_request->isPost()) {

       		$formData = $this->_request->getPost();
       		
       		$data = $this->_DbObj->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
    	}else{
			$data = $this->_DbObj->getPaginateData();
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	}
		
		$this->view->paginator = $paginator;
    	    	
	}
	
	public function uploadTransactionAction(){
		//title
    	$this->view->title= $this->view->translate("Payment Bank - Upload Transaction");
    	
    	$form = new Studentfinance_Form_BankPaymentTransactionUpload();
    	
    	if ($this->_request->isPost()) {

       		$formData = $this->_request->getPost();

            if ($form->isValid($formData)) {
            	
            	$arr_in_file_data = array();
            	
				$formData = $this->getRequest()->getPost();
				
                // success - do something with the uploaded file
                $uploadedData = $form->getValues();
                $fullFilePath = $form->file->getFileName();

                //Zend_Debug::dump($uploadedData, '$uploadedData');
                //Zend_Debug::dump($fullFilePath, '$fullFilePath');
                $this->view->file = $uploadedData;
                
	            $file_uploaded = $fullFilePath;
				$lines = file($file_uploaded);
            	
				$file = fopen($fullFilePath, "r") or exit("Unable to open file!");
				
				//Output a line of the file until the end is reached
				$i=0;
				$data = array();
				$transaction = array();
				$flag_end_last_line = false;
				$last_line_count = 0;
				
				while(!feof($file)){
					$line_data = fgets($file);
					
					if($i==0){
						//ignore line 1
					}else
					if($i==1){
						//report info
						$info = explode(";", $line_data);
						
						$data['report_name'] = trim($info[0]);
						$data['vendor_id'] = trim($info[1]);
						
						$date = $info[2];
						$d = DateTime::createFromFormat("dmYHi",$date);
						$data['created_date'] = $d->format("Y-m-d H:i:00");
						
						$data['status'] = $info[3];
						
						$data['paid_period'] = trim($info[4]);
						$paidPeriod = explode("-", $info[4]);
						$dfrom = DateTime::createFromFormat("dmY",trim($paidPeriod[0]));
						$dto = DateTime::createFromFormat("dmY",trim($paidPeriod[1]));
						
						$data['paid_period_from'] = $dfrom->format("Y-m-d");
						$data['paid_period_to'] = $dto->format("Y-m-d");
						
						
					}else 
					if($i==2 || $i==3){
						//ignored line space and header
					}else{
						
						if(trim($line_data)==""){
							//last line
							$flag_end_last_line = true;
						}else
						if(!$flag_end_last_line){
							$arr_line_data = explode(";", $line_data);
							$transaction[] = $arr_line_data;
						}else
						if($last_line_count==1){//last line index 1
							$arr_line_data = explode(";", $line_data);
							
							$data['paid_record']	 = trim($arr_line_data[0]);
							$data['Unpaid_Record']	 = trim($arr_line_data[1]);
							$data['TotalRecord']	 = trim($arr_line_data[2]);
							$data['PaidAmount']		 = trim($arr_line_data[3]);
							$data['UnpaidAmount']	 = trim($arr_line_data[4]);
							$data['TotalAmount']	 = trim($arr_line_data[5]);
							
						}else
						if($flag_end_last_line){
							$last_line_count++;
						}
						
						/*$data[$i]['pt_code']				= substr($line_data, 40,9);
						$data[$i]['pt_question_set_code'] 	= substr($line_data, 49,3);
						$data[$i]['applicant_name'] 		= substr($line_data, 52,30);
						$data[$i]['applicant_answer'] 		= substr($line_data, 82,200);*/
					}
					$i++;
				}
				$data['transaction'] = $transaction;
				
				fclose($file);
				
				//move file to transaciton repository
       			$repo_path = APPLICATION_PATH."/../documents/bank_transaction/".date('mY');
       			
	       		//create directory if not exist			
				if (!is_dir($repo_path)) {
			    	mkdir($repo_path, 0775, true);
				}
				
				//move upload file
				//rename($fullFilePath,$repo_path."/".date('YmdHi')."_".$uploadedData['file']);
				//remove file
				//unlink($fullFilePath);
				
				/*
				 * status
				 */
				$new_record = 0;
				$update_record = 0;
				$error_record = 0;
				$error_record_index ="";
				
				//clear data not standard
				foreach ($data['transaction'] as $key=>$trans){
					
					
					if(sizeof($trans) != 27){
						
						unset($data['transaction'][$key]);
						$error_record++;
						
						if($error_record_index==""){
							$error_record_index .= $key;
						}else{
							$error_record_index .= ",".$key;
						}
						
						
					}else{
						/*echo "index: ".$key;
						echo "<pre>";
						print_r($trans);
						echo "</pre>";*/
					}
				}
				
				//serialize
				$data['transaction'] = array_values($data['transaction']);
				
				
				//loop to check record status
				for ($i=0;$i<sizeof($data['transaction']); $i++){
					$db = Zend_Db_Table::getDefaultAdapter();	
					 $key = $i;
					 $trans = $data['transaction'][$i];
					
					//check matching record with applicant
					$select = $db ->select()
								  ->from(array('at'=>'applicant_transaction'))
								  ->where('at.at_pes_id = ?',$trans[1]);
							  
					$row = $db->fetchRow($select);
					if($row){
						$data['transaction'][$key]['STUDENT'] = 0;
					}
					
					//check if missing 0 in applicant
					/*$select = $db ->select()
					->from(array('at'=>'applicant_transaction'))
					->where('at.at_pes_id = ?','0'.$trans[1]);
					
					$row = $db->fetchRow($select);
					if($row){
						$data['transaction'][$key]['STUDENT'] = 0;
					}*/
					
					//check matching record with student
					if(!$row){
						$select = $db ->select()
									->from(array('sr'=>'tbl_studentregistration'))
									->where('sr.registrationId = ?',$trans[1]);
						$row = $db->fetchRow($select);
						
						if($row){
							$data['transaction'][$key]['STUDENT'] = 1;
						}
					}
					
					//check if missing 0 in student
					if(!$row){
						$select = $db ->select()
						->from(array('sr'=>'tbl_studentregistration'))
						->where('sr.registrationId = ?','0'.$trans[1]);
						$row = $db->fetchRow($select);
					
						if($row){
							//repair missing '0'
							$data['transaction'][$i][1] = '0'.$trans[1];
							$data['transaction'][$key]['MISSING_0'] = 1;
							
							$data['transaction'][$key]['STUDENT'] = 1;
						}
					}
					
					if($row){
						$data['transaction'][$key]['APPLICANT_MATCHING_STATUS'] = "TRUE";
					}else{
						$data['transaction'][$key]['APPLICANT_MATCHING_STATUS'] = "FALSE";
					}
					
					//check already insert bank journal (billing+journal)
					//$dt_payment = DateTime::createFromFormat("dmY - his",trim($trans[21])); 
					
					$select = $db ->select()
								  ->from(array('pbrd'=>'payment_bank_record_detail'))
								  ->where('pbrd.billing_no = ?', $trans[0])
								  ->where('pbrd.bancs_journal_number = ?',$trans[22]);
					
					$row = $db->fetchRow($select);
					
					$duplicate_in_file = in_array($data['transaction'][$key][0]."_".$data['transaction'][$key][22], $arr_in_file_data);
					
					if($row || $duplicate_in_file){
						$data['transaction'][$key]['IMPORT_STATUS'] = "DUPLICATE_DATA";
						$update_record++;
					}else{
						$data['transaction'][$key]['IMPORT_STATUS'] = "NEW_DATA";
						$new_record++;
						
						$arr_in_file_data[] = $data['transaction'][$key][0]."_".$data['transaction'][$key][22];
					}
					
										
				}
				
				
				$data['new_record'] = $new_record;
				$data['redundant_record'] = $update_record;
				$data['error_record'] = $error_record;
												
				$this->view->data = $data;
				
				if( $error_record > 0){
					$this->view->noticeError = $this->view->translate("File contain error records. Please clean the file first before submit. Row:".$error_record_index);
				}else{
					$this->_ses_paymentbank->data = $data;
				}
				
            } else {
            	$this->view->noticeError = $this->view->translate("Unable to read file. File Error");	
            }
            
		}else{
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-bank', 'action'=>'index'),'default',true));
		}
	}
	
	public function saveTransactionAction(){
		
		ini_set('memory_limit', '512M');
		ini_set('max_execution_time', 60*5);
		
		$time_start = microtime(true);
		$status = true;
		
		if ($this->_request->isPost()) {
			
			$auth = Zend_Auth::getInstance();
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			
			try {
	       		$formData = $this->_request->getPost();
	       		
	       		
       			//save bank payment transaction record head
       			$repo_path = APPLICATION_PATH."/../documents/bank_transaction/".date('mY');
       			
       			$paymentBankDb = new Studentfinance_Model_DbTable_PaymentBank();
       			$data = array(
       						'file_path'			 => $repo_path."/".date('YmdHi')."_".$formData['file'],
       						'upload_by'			 => $auth->getIdentity()->iduser,
       						'file_created_date'	 =>$formData['file_created_date'],
			       			'period_start'		 =>$formData['period_start'],
			       			'period_end'		 =>$formData['period_end'],
			       			'total_record'		 =>$formData['total_record'],
			       			'total_amount'		 =>$formData['total_amount']
       					);
       					
       			$pbr_id = $paymentBankDb->insert($data);
       			
       			
       			$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
       			$suspendMainDb = new Studentfinance_Model_DbTable_SuspendMain();
       			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
       			
       			//loop to save data
       			foreach($this->_ses_paymentbank->data['transaction'] as $txnData){
       				
       				
       				//data sorting to db format
       				$paid_dt = explode("-", $txnData[21]);
       				$d = DateTime::createFromFormat("dmY",trim($paid_dt[0]));
       				
       				$data_bank_detail = array(
       						'pbr_id' => $pbr_id,
       						'billing_no' => $txnData[0],
       						'payee_id'	 => $txnData[1],
       						'name'		 => $txnData[2],
       						'bill_ref_1' => $txnData[3],
       						'bill_ref_2' => $txnData[4],
       						'bill_ref_3' => $txnData[5],
       						'bill_ref_4' => $txnData[6],
       						'bill_ref_5' => $txnData[7],
       						'amount_total' => $txnData[8],
       						 
       						'amount_1' => $txnData[9],
       						'amount_2' => $txnData[10],
       						'amount_3' => $txnData[11],
       						'amount_4' => $txnData[12],
       						'amount_5' => $txnData[13],
       						'amount_6' => $txnData[14],
       						'amount_7' => $txnData[15],
       						'amount_8' => $txnData[16],
       						'amount_9' => $txnData[17],
       						'amount_10' => $txnData[18],
       						 
       						'status_payment' => $txnData[19],
       						'keterangan' => $txnData[20],
       						'paid_date' => $d->format("Y-m-d"),
       						'bancs_journal_number' => $txnData[22],
       						'payment_channel' => $txnData[23],
       						'branch_id' => $txnData[24],
       						'payment_method' => $txnData[25],
       						'debet_account' => $txnData[26],
       				);
       				
       				
       				//Not duplicate data
       				if($txnData['IMPORT_STATUS'] == 'NEW_DATA'){
       					
       					//add payment record
       					if($txnData['APPLICANT_MATCHING_STATUS']=='TRUE'){
       						
       						//save bank payment transaction detail record
       						$payment_detail_id = $this->saveBankTransactionDetail($data_bank_detail,"PAYMENT");
       						
       						//save payment record
       						$payment_main_id = $paymentMainDb->saveBankPaymentTransaction($data_bank_detail, $payment_detail_id, $txnData['STUDENT']);
       						
       					}else
       					// add suspend record	
       					if($txnData['APPLICANT_MATCHING_STATUS']=='FALSE'){
       						
       						//save bank payment transaction detail record
       						$payment_detail_id = $this->saveBankTransactionDetail($data_bank_detail,"SUSPEND");
       						
       						//save suspend account
       						$suspendMainDb->saveBankSuspendTransaction($data_bank_detail, $payment_detail_id);
  
       					}
       				}
       			}
       			
       			
	       		
	       		$time_end = microtime(true);
				$time = $time_end - $time_start;
				echo "Start: ".$time_start."<br />";
				echo "End: ".$time_end."<br />";
				echo "Process in ".number_format($time,3)." seconds\n";
				//$db->rollBack();
	       		//exit;
	       		
	       		$db->commit();
	       		
	       		
			}catch (Exception $e) {
				echo "Error in PaymentController. <br />";
				echo $e->getMessage();
				
				echo "<pre>";
				print_r($e->getTrace());
				echo "</pre>";
				
				$db->rollBack();
    			
    			$status = false;
    			exit;
    			
			}
			
		}
		
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-bank', 'action'=>'index','status'=>$status),'default',true));
			
	}
	
	public function generateInvoiceAction(){
		$id = $this->_getParam('id', 0);
		
		$paymentBankDetailDb = new Studentfinance_Model_DbTable_PaymentBankDetail();
		$bill = $paymentBankDetailDb->getPaymentBankDetailBill($id, "PAYMENT");
		
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->beginTransaction();
		try{
			for($i=0;$i<sizeof($bill);$i++){
				$billing = $bill[$i];
									
				//check for processing payment status
				if($billing['finance_processing']==0){
					
					//get payment main record
					$payment = $paymentMainDb->getPaymentByBankTransaction($billing['bancs_journal_number'],$billing['billing_no']);
					
					//get invoice main record
					$invoice = $invoiceMainDb->getInvoiceData($billing['billing_no'],true);
					
					//check for invoice
					if($invoice==null){
						//issue invoice for first payment and calculate excess payment and update invoice
						$invoiceMainDb->generateApplicantInvoice($billing['payee_id'],$billing['billing_no']);
						
					}else{
						//update invoice
						$data_bank_detail = $billing;
						
						//initiall set null paid & balance value
		       			if($invoice['bill_paid']==null){
		       				$invoice['bill_paid'] = 0;
		       			}
		       			
		       			if($invoice['bill_balance']==null){
		       				$invoice['bill_balance'] = $invoice['bill_amount'];
		       			}
		       			
			       		//excess payment
						if($data_bank_detail['amount_total'] > ($invoice['bill_balance'] + $invoice['cn_amount']) ){
			       			$excess_payment = $data_bank_detail['amount_total'] - $invoice['bill_balance'];
			       			$amount_paid = $data_bank_detail['amount_total'];
			       			
			       			$invoice_paid = $invoice['bill_paid'] + $amount_paid;
			       			$invoice_balance = 0.00;
			       			
			       			//store advance payment
			       			$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				       		
				       		$data = array(
				       			'advpy_appl_id' => $invoice['appl_id'],
				       			'advpy_acad_year_id' => $invoice['academic_year'],
				       			'advpy_sem_id' => $invoice['semester'],
				       			'advpy_prog_code' => $invoice['program_code'],
				       			'advpy_fomulir' => $invoice['no_fomulir'],
				       			'advpy_invoice_no' => $invoice['bill_number'],
				       			'advpy_invoice_id' => $invoice['id'],
				       			'advpy_payment_id' => $payment['id'],
				       			'advpy_description' => 'Excess Payment for invoice no:'.$invoice['bill_number'],
				       			'advpy_amount' => $excess_payment,
				       			'advpy_total_paid' => 0,
				       			'advpy_total_balance' => $excess_payment,
				       			'advpy_status' => 'A'
				       		);
				       		$advancePaymentDb->insert($data);
			       			
			       		}else{
			       			$excess_payment = 0;
			       			$amount_paid = $data_bank_detail['amount_total'];
			       			
			       			$invoice_paid = $invoice['bill_paid'] + $amount_paid;
			       			$invoice_balance = $invoice['bill_balance'] - $amount_paid;
			       		}
			       		
			       		
		       		
		       			//update invoice
		       			$data_invoice = array(
		       				'bill_paid' => $invoice_paid,
		       				'bill_balance' => $invoice_balance
		       			);
		       			
		       			$invoiceMainDb->update($data_invoice, 'id = '.$invoice['id']);	
					}
					
					//update finance processing column in payment_bank_record
					$data = array(
						'finance_processing' => 1
					);
					
					$paymentBankDetailDb->update($data, 'id = '.$billing['id']);
				}
	
			}

			$db->commit();
			
		}catch (Exception $e) {
			echo $e->getMessage();
			
			echo "<pre>";
			print_r($e->getTrace());
			echo "</pre>";
			
			$db->rollBack();
			
			exit;
		}
				
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-bank', 'action'=>'index','status'=>$status),'default',true));	
	}
	
	public function detailPaymentAction(){
		$id = $this->_getParam('id', 0);
		
		//title
    	$this->view->title= $this->view->translate("Payment Bank")." - ".$this->view->translate("Detail Payment");
    	
    	//main
    	$paymentBankDb = new Studentfinance_Model_DbTable_PaymentBank();
    	$this->view->data = $paymentBankDb->getData($id);
    	
    	//payment record
    	$paymentBankDetailDb = new Studentfinance_Model_DbTable_PaymentBankDetail();
		$selectData = $paymentBankDetailDb->getPaginatePaymentBankDetail($id,"PAYMENT");
		$adapter = new Zend_Paginator_Adapter_DbSelect($selectData);
		
		$paginator = new Zend_Paginator($adapter);
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
		
		//payment amount
		$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
		$payment_arr = $paymentMainDb->getBankTransactionRecord($id);
		$total_payment = 0;
		
		for($p_cnt=0; $p_cnt<sizeof($payment_arr); $p_cnt++){
			 $payment = $payment_arr[$p_cnt];
			$total_payment = $total_payment + $payment['amount_total'];
		}
		$this->view->payment_record = sizeof($payment_arr);
		$this->view->payment_amount = $total_payment;
	}
	
	public function detailSuspendAction(){
		
		$id = $this->_getParam('id', 0);
		$this->view->pbr_id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Payment Bank")." - ".$this->view->translate("Detail Suspend");
    	
    	//main
    	$paymentBankDb = new Studentfinance_Model_DbTable_PaymentBank();
    	$this->view->data = $paymentBankDb->getData($id);
    	
    	//payment record
    	$paymentBankDetailDb = new Studentfinance_Model_DbTable_PaymentBankDetail();
		$selectData = $paymentBankDetailDb->getPaginatePaymentBankDetail($id,"SUSPEND");
		$adapter = new Zend_Paginator_Adapter_DbSelect($selectData);
		
		$paginator = new Zend_Paginator($adapter);
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
		
		//suspend amount
		$suspendMainDb = new Studentfinance_Model_DbTable_SuspendMain();
		$suspend_arr = $suspendMainDb->getBankTransactionRecord($id);
		$total_suspend = 0;
		
		for($s_cnt=0; $s_cnt<sizeof($suspend_arr); $s_cnt++){
			 $suspend = $suspend_arr[$s_cnt];
			$total_suspend = $total_suspend + $suspend['amount'];
		}
		$this->view->suspend_record = sizeof($suspend_arr);
		$this->view->suspend_amount = $total_suspend;
	}
	
	private function saveBankTransactionDetail($data_bank_detail, $type){
		
		$paymentBankDetailDb = new Studentfinance_Model_DbTable_PaymentBankDetail();
		
		$data_bank_detail['account_record_type'] = $type;
		
		$payment_detail_id = $paymentBankDetailDb->insert($data_bank_detail);
		
		return $payment_detail_id;
	}
	
	private function getBankTransactionDetailArray($pbr_id, $formData, $index){
		
		$paid_dt = explode("-", $formData['paid_date'][$index]);
		$d = DateTime::createFromFormat("dmY",trim($paid_dt[0]));
						
		$data_bank_detail = array(
       			'pbr_id' => $pbr_id,
	       		'billing_no' => $formData['billing_no'][$index],
	       		'payee_id' => $formData['payee_id'][$index],
	       		'name' => $formData['name'][$index],
	       		'bill_ref_1' => $formData['bill_ref_1'][$index],
	       		'bill_ref_2' => $formData['bill_ref_2'][$index],
	       		'bill_ref_3' => $formData['bill_ref_3'][$index],
	       		'bill_ref_4' => $formData['bill_ref_4'][$index],
	       		'bill_ref_5' => $formData['bill_ref_5'][$index],
	       		'amount_total' => $formData['amount_total'][$index],
       		
       			'amount_1' => $formData['amount_1'][$index],
	       		'amount_2' => $formData['amount_2'][$index],
	       		'amount_3' => $formData['amount_3'][$index],
	       		'amount_4' => $formData['amount_4'][$index],
	       		'amount_5' => $formData['amount_5'][$index],
	       		'amount_6' => $formData['amount_6'][$index],
	       		'amount_7' => $formData['amount_7'][$index],
	       		'amount_8' => $formData['amount_8'][$index],
	       		'amount_9' => $formData['amount_9'][$index],
	       		'amount_10' => $formData['amount_10'][$index],
       		
       			'status_payment' => $formData['status_payment'][$index],
	       		'keterangan' => $formData['keterangan'][$index],
	       		'paid_date' => $d->format("Y-m-d"),
	       		'bancs_journal_number' => $formData['bancs_journal_number'][$index],
	       		'payment_channel' => $formData['payment_channel'][$index],
	       		'branch_id' => $formData['branch_id'][$index],
	       		'payment_method' => $formData['payment_method'][$index],
	       		'debet_account' => $formData['debet_account'][$index]
       		);
       		
       	return $data_bank_detail;
	}
	
	public function resolvePayeeAction(){
		
		$pbr_id = $this->_getParam('id', null);
		$this->view->id = $pbr_id;
		
		//title
		$this->view->title= $this->view->translate("Payment Bank")." - ".$this->view->translate("Resolve Suspend Record");
		 
		
		$paymentBankDb = new Studentfinance_Model_DbTable_PaymentBank();
		$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
		$paymentBankRecordDetailDb = new Studentfinance_Model_DbTable_PaymentBankDetail();
		$suspendProcessHistoryDb = new Studentfinance_Model_DbTable_SuspendProcessHistory();
		
		//suspend amount
		$suspendMainDb = new Studentfinance_Model_DbTable_SuspendMain();
		$suspend_arr = $suspendMainDb->getBankTransactionRecord($pbr_id);
		$total_suspend = 0;
		
		for($s_cnt=0; $s_cnt<sizeof($suspend_arr); $s_cnt++){
			$suspend = $suspend_arr[$s_cnt];
			$total_suspend = $total_suspend + $suspend['amount'];
		}
		$this->view->suspend_record = sizeof($suspend_arr);
		$this->view->suspend_amount = $total_suspend;
		
		
		$this->view->data = $paymentBankDb->getData($pbr_id);
		
		if ($this->_request->isPost()) {
			
			if( !isset($this->_ses_suspendacc->data) ){
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-bank', 'action'=>'index'),'default',true));
			}
				
			$formData = $this->_request->getPost();
						
			//set session to use id as key of array
			$ses_data = array();
			foreach ($this->_ses_suspendacc->data as $data){
				$ses_data[$data['id']] = $data;
			}
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			
			try {
				//loop all selected data
				foreach ($formData['data'] as $index=>$pbrd_id){
						
						
						/*
						 * add suspend process history record
						 */
						$sus_his_record = array(
							'pbrd_id' => $pbrd_id,
							'billing_no' => $ses_data[$pbrd_id]['billing_no']
						);
						
						if($ses_data[$pbrd_id]['mapping_status']==1){
							$sus_his_record['new_payer_id'] = $ses_data[$pbrd_id]['payee_id_repair'];
						}else{
							$sus_his_record['new_payer_id'] = $formData['payer_id_repair'][$pbrd_id];
							$ses_data[$pbrd_id]['STUDENT'] = $formData['student'][$pbrd_id];
						}
						
						$process_id = $suspendProcessHistoryDb->insert($sus_his_record);
						
						/*
						 * add payment main record
						 */
						$data_payment_main = $ses_data[$pbrd_id];
						$data_payment_main['payee_id'] = $sus_his_record['new_payer_id'];
						$paymentMainDb->saveBankPaymentTransaction($data_payment_main,$pbrd_id,$ses_data[$pbrd_id]['STUDENT']);
						
						
						//update payment_bank_record_detail column account_record_type, suspend_process_history_id
						$data_update = array(
							'account_record_type' => 'PAYMENT',
							'suspend_process_history_id' => $process_id	
						);
						
						$paymentBankRecordDetailDb->update($data_update, 'id = '.$pbrd_id);
						
						/*
						 * Delete Suspend main record
						 */
						$suspendMainDb->delete('billing_no = '.$ses_data[$pbrd_id]['billing_no'] ." AND transaction_reference = ".$pbrd_id);
						
						
						/*
						 * process finance invoice knockoff
						 */
						
						
				}
				
				$db->commit();
				
			} catch (Exception $e){
				$db->rollBack();
				echo $e->getMessage();
			}	
			
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment-bank', 'action'=>'index'),'default',true));
				
		}else{
		
			//get suspend record
			$paymentBankDetailDb = new Studentfinance_Model_DbTable_PaymentBankDetail();
			$suspend_data = $paymentBankDetailDb->getPaymentBankDetailBill($pbr_id,"SUSPEND");
			
			$db = Zend_Db_Table::getDefaultAdapter();
			
			foreach ($suspend_data as $index=>$data){
				
				$suspend_data[$index]['mapping_status'] = 0;
				
				
				
				//try add 0
				$select = $db ->select()
								->from(array('sr'=>'tbl_studentregistration'))
								->where('sr.registrationId = ?','0'.$data['payee_id']);
				
				$row = $db->fetchRow($select);
					
				if($row){
					//repair missing '0'
					$suspend_data[$index]['payee_id_repair'] = '0'.$data['payee_id'];
					$suspend_data[$index]['STUDENT'] = 1;
					$suspend_data[$index]['mapping_status'] = 1;
					$suspend_data[$index]['mapping_remark'] = 'Add leading zero to payee id';
					
				}else{
					
					//try mapping using billing no
					$select = $db ->select()
								->from(array('im'=>'invoice_main'))
								->where('im.bill_number = ?',$data['billing_no']);
					
					$row = $db->fetchRow($select);
					
					if($row){
						if($row['IdStudentRegistration']!=""){
							$suspend_data[$index]['payee_id_repair'] = $row['IdStudentRegistration'];
							$suspend_data[$index]['STUDENT'] = 1;
							$suspend_data[$index]['mapping_status'] = 1;
						}else
						if($row['no_fomulir']!=""){
							$suspend_data[$index]['payee_id_repair'] = $row['no_fomulir'];
							$suspend_data[$index]['STUDENT'] = 0;
							$suspend_data[$index]['mapping_status'] = 1;
						}
		
						$suspend_data[$index]['mapping_remark'] = 'Mapping using Billing number';
						
					}
				}
				
				
				//inject profile data into array
				if($suspend_data[$index]['mapping_status'] == 1){
					if($suspend_data[$index]['STUDENT'] == 1){
						
						$select = $db ->select()
								->from(array('sr'=>'tbl_studentregistration'))
								->join(array('sp'=>'student_profile'), 'sp.appl_id = sr.IdApplication')
								->where('sr.registrationId = ?',$suspend_data[$index]['payee_id_repair']);
						
						$row_profile = $db->fetchRow($select);
						$suspend_data[$index]['profile'] = $row_profile;
						
					}else{
						$select = $db ->select()
						->from(array('t'=>'applicant_transaction'))
						->join(array('sp'=>'applicant_profile'), 'sp.appl_id = t.at_appl_id')
						->where('t.at_pes_id = ?',$suspend_data[$index]['payee_id_repair']);
						
						$row_profile = $db->fetchRow($select);
						$suspend_data[$index]['profile'] = $row_profile;
					}
				}
			}
			
			$this->view->suspend_data = $suspend_data;
		
			$this->_ses_suspendacc->data = $suspend_data;
		}
		/*echo "<pre>";
		print_r($suspend_data);
		echo "</pre>";*/

		
	}
	
	
	public function testAction(){
	  /*
	   *  Repair invoice paid amount in invoice main and remove advance payment if any
	  */
	  exit;
	  $db = Zend_Db_Table::getDefaultAdapter();
	  
	  $sql = "SELECT a.id as pbrd_id, a.billing_no, a.amount_total, b.id as invoice_main_id, b.bill_number, b.bill_amount, b.bill_paid, b.bill_balance  FROM `payment_bank_record_detail` a
              JOIN invoice_main b on b.bill_number = a.billing_no and b.bill_paid > b.bill_amount
              WHERE a.account_record_type = 'PAYMENT'";
	  
	  $row_wrong_amount_paid = $db->fetchAll($sql);
	  
	  
	  foreach ($row_wrong_amount_paid as $index=>$bil){
	  
	    //payment info
	    $sql_payment_main = "SELECT * FROM payment_main WHERE billing_no = ".$bil['bill_number']." AND transaction_reference = ".$bil['pbrd_id'];
	    $row_payment_main =  $db->fetchAll($sql_payment_main);
	     
	    if($row_payment_main){
	      $row_wrong_amount_paid[$index]['payment_main'] = $row_payment_main;
	  
	  
	      //advance payment on each payment
	      foreach ($row_payment_main as $payment_main){
	  
	        $sql2 = "SELECT advance_payment.* FROM advance_payment
    	          LEFT JOIN advance_payment_detail on advpydet_advpy_id = advpy_id and advpydet_id is null
    	          WHERE advpy_invoice_id = ".$bil['invoice_main_id']." AND advpy_invoice_no = ".$bil['bill_number'] ." AND 	advpy_payment_id = ".$payment_main['id'];
	         
	        $adv_payment =  $db->fetchAll($sql2);
	         
	        if($adv_payment){
	          $row_wrong_amount_paid[$index]['advance_payment'] = $adv_payment;
	        }
	      }
	  
	    }
	  
	  }
	  
	  echo "<pre>";
	  print_r($row_wrong_amount_paid);
	  echo "</pre>";
	  exit;
	  
	  //loop
	  
	  foreach ($row_wrong_amount_paid as $bill){
	  
	    //update invoice
	    $dataUpdate = array(
	        'bill_paid' => $bill['amount_total']
	    );
	  
	    $db->update('invoice_main', $dataUpdate, 'id = '.$bill['invoice_main_id']);
	  
	    //delete advance payment
	    if( isset($bill['advance_payment']) ){
	      foreach ($bill['advance_payment'] as $advpy){
	        $db->delete('advance_payment', 'advpy_payment_id = '.$advpy['advpy_id']);
	      }
	    }
	  
	  }
	  
	  
	  echo "Done";
	  
	  /*
	   * Repair Payment from payment bank record detail
	   */
	  exit;
		
	    $db = Zend_Db_Table::getDefaultAdapter();
		
	    //get duplicate billing in payment bank record
	    $sql = "select billing_no from ( 
	        SELECT
              `billing_no`,
              `bancs_journal_number`,
              COUNT(*) AS c
            FROM
              payment_bank_record_detail
            GROUP BY
              `billing_no`,
              `bancs_journal_number`
            HAVING c > 1
            ) as billing";
	    
	    $row_billing_duplicate = $db->fetchAll($sql);
	    
	    
	    //delete the second row and next
	    $to_delete_id_paymentmain = array();
	    foreach ($row_billing_duplicate as $billing){
	      $sql2 = "select id from payment_bank_record_detail where billing_no = '".$billing['billing_no']."' order by id";
	      $row = $db->fetchAll($sql2);

	      foreach ($row as $idx=>$bill){
	        if($idx!=0){
	          $db->delete('payment_bank_record_detail', 'id = '.$bill['id']);
	          $to_delete_id_paymentmain[] = $bill['id'];
	        }
	      }
	    }
	    
	    
	    
	  
	    //get duplication in payment main
	    $paymentmain_duplicate = array();
	    foreach ($to_delete_id_paymentmain as $pbrd_id){
	      //get data
	      $sql3 = "select id from payment_main where transaction_reference = ".$pbrd_id;
	      $row = $db->fetchRow($sql3);
	      if($row){
	        $paymentmain_duplicate[] = $row;
	      }
	      
	      $db->delete('payment_main', 'transaction_reference = '.$pbrd_id);
	      //echo 'transaction_reference = '.$pbrd_id;
	      
	    }
	      
	    
	  
	    //delete advance payment
	    $advancepayment_duplicate = array();
	    foreach ($paymentmain_duplicate as $pymt_id){
	      //get data
	      $sql4 = "select advpy_id from advance_payment where advpy_payment_id = ".$pymt_id['id'];
	      $row = $db->fetchRow($sql4);
	      if($row){
	        $advancepayment_duplicate[] = $row;
	      }
	      
	      $db->delete('advance_payment', 'advpy_payment_id = '.$pymt_id['id']);
	    }
	    
	    echo "done";
	    
	    exit;
		
	}
	
	
}

