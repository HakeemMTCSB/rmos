<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/8/2016
 * Time: 10:26 AM
 */
class Studentfinance_EarlyBirdController extends Zend_Controller_Action
{

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_EarlyBird();

        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Early Bird Setup');

        $form = new Studentfinance_Form_EarlyBird();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form->populate($formData);

            $list = $this->model->getEarlyBird(null, $formData);
            $this->view->list = $list;
            //var_dump($list);
        }else{
            $list = $this->model->getEarlyBird();
            $this->view->list = $list;
        }
    }

    public function addAction(){
        $this->view->title = $this->view->translate('Add Early Bird Setup');

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $form = new Studentfinance_Form_EarlyBird();
        $this->view->form = $form;

        $sponsorList = $this->model->getSponsor();
        $this->view->sponsorList = $sponsorList;

        $scholarList = $this->model->getScholar();
        $this->view->scholarList = $scholarList;

        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;

            $db = Zend_Db_Table::getDefaultAdapter();
            $db->beginTransaction();

            try {
                $data = array(
                    'eb_semester' => $formData['semester'],
                    'eb_discount_type' => $formData['discounttype'],
                    'eb_payment_date' => date('Y-m-d', strtotime($formData['paymentdate'])),
                    'eb_name' => $formData['eb_name'],
                    'eb_code' => $formData['eb_code'],
                    'eb_self_sponsor' => isset($formData['self-fund']) ? 1 : 0,
                    'eb_financial_aid' => isset($formData['financial-aid']) ? 1 : 0,
                    'eb_created_by' => $userId,
                    'eb_created_date' => date('Y-m-d')
                );
                $id = $this->model->insertEarlyBird($data);

                if (isset($formData['prev-sem']) && count($formData['prev-sem']) > 0) {
                    foreach ($formData['prev-sem'] as $prevsem) {
                        $dataPrevSem = array(
                            'ebp_eb_id' => $id,
                            'ebp_prev_sem' => $prevsem,
                            'ebp_payment_date' => date('Y-m-d', strtotime($formData['prevpaymentdate'][$prevsem]))
                        );
                        $this->model->insertEarlyBirdSem($dataPrevSem);
                    }
                }

                if (isset($formData['prog']) && count($formData['prog']) > 0) {
                    foreach ($formData['prog'] as $prog) {
                        $dataProg = array(
                            'ebpr_eb_id' => $id,
                            'ebpr_prog_id' => $prog,
                        );
                        $this->model->insertEarlyBirdProgram($dataProg);
                    }
                }

                if (isset($formData['sponsor']) && count($formData['sponsor']) > 0) {
                    foreach ($formData['sponsor'] as $sponsor) {
                        $dataSponsor = array(
                            'ebsp_eb_id' => $id,
                            'ebsp_sponsor_id' => $sponsor,
                        );
                        $this->model->insertEarlyBirdSponsor($dataSponsor);
                    }
                }

                if (isset($formData['scholar']) && count($formData['scholar']) > 0) {
                    foreach ($formData['scholar'] as $scholar) {
                        $dataScholar = array(
                            'ebsc_eb_id' => $id,
                            'ebsc_scholar' => $scholar,
                        );
                        $this->model->insertEarlyBirdScholar($dataScholar);
                    }
                }

                $db->commit();

                $this->_helper->flashMessenger->addMessage(array('success' => 'New setup has been added'));
                $this->_redirect($this->baseUrl . '/studentfinance/early-bird/edit/id/'.$id);
            } catch (Exception $e){
                $db->rollBack();

                $this->_helper->flashMessenger->addMessage(array('error' => 'Failure : '.$e->getMessage()));
                $this->_redirect($this->baseUrl . '/studentfinance/early-bird/add/');
            }
        }
    }

    public function editAction(){
        $this->view->title = $this->view->translate('Edit Early Bird Setup');

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $id = $this->_getParam('id',null);

        if ($id == null) {
            $this->_redirect($this->baseUrl . '/studentfinance/early-bird/');
        }

        $form = new Studentfinance_Form_EarlyBird();
        $this->view->form = $form;

        $earlyBird = $this->model->getEarlyBird($id);

        //var_dump($earlyBird); exit;

        if (!$earlyBird){
            $this->_redirect($this->baseUrl . '/studentfinance/early-bird/');
        }

        $this->view->earlyBird = $earlyBird;

        $populate['semester'] = $earlyBird['eb_semester'];
        $populate['discounttype'] = $earlyBird['eb_discount_type'];
        $populate['paymentdate'] = date('d-m-Y', strtotime($earlyBird['eb_payment_date']));
        $populate['eb_name'] = $earlyBird['eb_name'];
        $populate['eb_code'] = $earlyBird['eb_code'];

        $form->populate($populate);

        $ebSemester = $this->model->getEarlyBirdSem($id);

        $ebSem = array();
        $ebSemDate = array();

        if ($ebSemester){
            foreach ($ebSemester as $ebSemesterLoop){
                $ebSem[] = $ebSemesterLoop['ebp_prev_sem'];
                $ebSemDate[$ebSemesterLoop['ebp_prev_sem']] = $ebSemesterLoop['ebp_payment_date'];
            }
        }

        $this->view->ebSem = $ebSem;
        $this->view->ebSemDate = $ebSemDate;

        $ebProgram = $this->model->getEarlyBirdProgram($id);

        $ebProg = array();

        if ($ebProgram){
            foreach ($ebProgram as $ebProgramLoop){
                $ebProg[] = $ebProgramLoop['ebpr_prog_id'];
            }
        }

        $this->view->ebProg = $ebProg;

        $ebScholar = $this->model->getEarlyBirdScholar($id);

        $ebSch = array();

        if ($ebScholar){
            foreach ($ebScholar as $ebScholarLoop){
                $ebSch[] = $ebScholarLoop['ebsc_scholar'];
            }
        }

        $this->view->ebSch = $ebSch;

        $ebSponsor = $this->model->getEarlyBirdSponsor($id);

        $ebSpo = array();

        if ($ebSponsor){
            foreach ($ebSponsor as $ebSponsorLoop){
                $ebSpo[] = $ebSponsorLoop['ebsp_sponsor_id'];
            }
        }

        $this->view->ebSpo = $ebSpo;

        $semesterList = $this->model->getSemester($earlyBird['IdScheme'], $earlyBird['SemesterMainStartDate']);
        $this->view->semesterList = $semesterList;

        $programList = $this->model->getProgram($earlyBird['IdScheme']);
        $this->view->programList = $programList;

        $sponsorList = $this->model->getSponsor();
        $this->view->sponsorList = $sponsorList;

        $scholarList = $this->model->getScholar();
        $this->view->scholarList = $scholarList;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //var_dump($formData); //exit;

            $db = Zend_Db_Table::getDefaultAdapter();
            $db->beginTransaction();

            try {
                $data = array(
                    'eb_discount_type' => $formData['discounttype'],
                    'eb_payment_date' => date('Y-m-d', strtotime($formData['paymentdate'])),
                    'eb_name' => $formData['eb_name'],
                    'eb_code' => $formData['eb_code'],
                    'eb_self_sponsor' => isset($formData['self-fund']) ? 1 : 0,
                    'eb_financial_aid' => isset($formData['financial-aid']) ? 1 : 0,
                    'eb_update_by' => $userId,
                    'eb_update_date' => date('Y-m-d')
                );
                //var_dump($data); //exit;
                $id = $this->model->updateEarlyBird($data, $earlyBird['eb_id']);

                $this->model->deleteEarlyBirdSem($earlyBird['eb_id']);
                $this->model->deleteEarlyBirdProgram($earlyBird['eb_id']);
                $this->model->deleteEarlyBirdScholar($earlyBird['eb_id']);
                $this->model->deleteEarlyBirdSponsor($earlyBird['eb_id']);

                if (isset($formData['prev-sem']) && count($formData['prev-sem']) > 0) {
                    foreach ($formData['prev-sem'] as $prevsem) {
                        $dataPrevSem = array(
                            'ebp_eb_id' => $earlyBird['eb_id'],
                            'ebp_prev_sem' => $prevsem,
                            'ebp_payment_date' => date('Y-m-d', strtotime($formData['prevpaymentdate'][$prevsem]))
                        );
                        var_dump($dataPrevSem);
                        $this->model->insertEarlyBirdSem($dataPrevSem);
                    }
                }

                if (isset($formData['prog']) && count($formData['prog']) > 0) {
                    foreach ($formData['prog'] as $prog) {
                        $dataProg = array(
                            'ebpr_eb_id' => $earlyBird['eb_id'],
                            'ebpr_prog_id' => $prog,
                        );
                        $this->model->insertEarlyBirdProgram($dataProg);
                    }
                }

                if (isset($formData['sponsor']) && count($formData['sponsor']) > 0) {
                    foreach ($formData['sponsor'] as $sponsor) {
                        $dataSponsor = array(
                            'ebsp_eb_id' => $earlyBird['eb_id'],
                            'ebsp_sponsor_id' => $sponsor,
                        );
                        $this->model->insertEarlyBirdSponsor($dataSponsor);
                    }
                }

                if (isset($formData['scholar']) && count($formData['scholar']) > 0) {
                    foreach ($formData['scholar'] as $scholar) {
                        $dataScholar = array(
                            'ebsc_eb_id' => $earlyBird['eb_id'],
                            'ebsc_scholar' => $scholar,
                        );
                        $this->model->insertEarlyBirdScholar($dataScholar);
                    }
                }

                $db->commit();

                $this->_helper->flashMessenger->addMessage(array('success' => 'Data Updated'));
                $this->_redirect($this->baseUrl . '/studentfinance/early-bird/edit/id/'.$earlyBird['eb_id']);
            } catch (Exception $e){
                $db->rollBack();

                $this->_helper->flashMessenger->addMessage(array('error' => 'Failure : '.$e->getMessage()));
                $this->_redirect($this->baseUrl . '/studentfinance/early-bird/edit/id/'.$earlyBird['eb_id']);
            }
        }
    }

    public function checkAction(){
        $this->view->title = $this->view->translate('Check Early Bird');

        $processModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();
        $calculation = new Studentfinance_Model_DbTable_EarlyBirdCalculation();

        $form = new Studentfinance_Form_EarlyBird();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $studentList = $this->model->checkStudent($formData['semester']);
            //var_dump($studentList);
            if ($studentList){
                foreach ($studentList as $skey => $student){
                    $invList = $processModel->getInvoice($student['IdStudentRegistration'], $formData['semester']);
                    $checkReg = $processModel->checkRegisterCourse($student['IdStudentRegistration'], $formData['semester']);
                    //var_dump($invList);
                    if ($invList && $checkReg){
                        $balAmt = 0.00;
                        foreach ($invList as $invLoop){
                            $balance = $calculation->getBalanceMain($invLoop['id'], date('Y-m-d', strtotime($formData['paymentdate'])));
                            //var_dump($balance);
                            $balAmt = $balAmt+$balance['bill_balance'];
                        }

                        if ($balAmt > 0.00){
                            unset($studentList[$skey]);
                        }
                    }else{
                        unset($studentList[$skey]);
                    }
                }
            }

            $this->view->studentList = $studentList;
            //var_dump($studentList);
        }
    }

    public function getSemesterAction(){
        $id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $semesterList = array();

        if ($id != null){
            $semInfo = $this->model->getSemesterById($id);

            if ($semInfo){
                $semesterList = $this->model->getSemester($semInfo['IdScheme'], $semInfo['SemesterMainStartDate']);
            }
        }

        $json = Zend_Json::encode($semesterList);
        echo $json;
        exit;
    }

    public function getProgramAction(){
        $id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $programList = array();

        if ($id != null){
            $semInfo = $this->model->getSemesterById($id);

            if ($semInfo){
                $programList = $this->model->getProgram($semInfo['IdScheme']);
            }
        }

        $json = Zend_Json::encode($programList);
        echo $json;
        exit;
    }

    public function overlookedEbipAction(){
        $this->view->title = $this->view->translate('Generate Early Bird');

        $processModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();
        $calculation = new Studentfinance_Model_DbTable_EarlyBirdCalculation();

        $form = new Studentfinance_Form_EarlyBird();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form->populate($formData);

            $studentList = $this->model->checkStudent($formData['semester']);

            $list = array();
            $i = 0;

            if ($studentList){
                foreach ($studentList as $key => $student){
                    $ebitList = $processModel->getEbitOverlooked($student['IdStudentRegistration'], $formData['semester']);
                    //$invoice = $processModel->getInvoice($student['IdStudentRegistration'], $formData['semester']);
                    $invoice = $processModel->getInvoiceOverlooked($student['IdStudentRegistration'], $formData['semester']);

                    if ($invoice){
                        foreach ($invoice as $invoiceKey => $invoiceLoop){
                            $invoiceLoop['Active'] == null ? 1:$invoiceLoop['Active'];
                            $invoiceLoop['dropstatus'] == null ? 1:$invoiceLoop['dropstatus'];

                            if ($invoiceLoop['Active'] != 3 && $invoiceLoop['dropstatus'] != 2) {
                                //$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
                                //$balance = $paModel->getBalanceMain($invoiceLoop['id']);

                                $invDtl = $processModel->getInvDtl($invoiceLoop['id']);

                                //calculate ebit discount
                                if ($invDtl) {
                                    foreach ($invDtl as $invDtlLoop) {

                                        $ebAmount = 0.00;
                                        if ($ebitList) {
                                            $ebitArr = array();

                                            foreach ($ebitList as $ebitLoop) {
                                                array_push($ebitArr, $ebitLoop['eb_discount_type']);

                                                $ebitItem = $processModel->checkEbitItem($ebitLoop['eb_discount_type'], $invDtlLoop['fi_id']);

                                                if ($ebitItem) {
                                                    if ($ebitItem['CalculationMode'] == 1) { //amount
                                                        $ebAmt = $ebitItem['Amount'];
                                                    } else { //percentage
                                                        $ebAmt = (($ebitItem['Amount'] / 100) * $invDtlLoop['amount']);
                                                    }
                                                    $ebAmount = $ebAmount + $ebAmt;
                                                }
                                            }

                                            $balance = $calculation->getBalanceMain($invoiceLoop['id'], $ebitList[0]['eb_payment_date']);

                                            if ($ebAmount > 0.00) {
                                                $balance = str_replace(',', '', $balance['bill_balance']) - $ebAmount;

                                                if ($balance <= 0.00) {
                                                    $checkDis = $this->model->checkDiscount($invoiceLoop['id'], $ebitArr);

                                                    if (!$checkDis) {
                                                        $list[$i]['id'] = $student['IdStudentRegistration'];
                                                        $list[$i]['student_id'] = $student['registrationId'];
                                                        $list[$i]['student_name'] = $student['appl_fname'] . ' ' . $student['appl_lname'];
                                                        $list[$i]['program'] = $student['ProgramName'];
                                                        $list[$i]['invoice'] = $invoiceLoop;
                                                        $list[$i]['invoice_dtl'] = $invDtlLoop;
                                                        $list[$i]['ebList'] = $ebitList;
                                                        $list[$i]['ebAmount'] = number_format($ebAmount, 2, '.', '');

                                                        $i++;
                                                    } else {
                                                        //unset($studentList[$key]);
                                                    }
                                                } else {
                                                    //unset($studentList[$key]);
                                                }
                                            } else {
                                                //unset($studentList[$key]);
                                            }
                                        }
                                    }
                                } else {
                                    //unset($studentList[$key]);
                                }
                            }
                        }
                    }else{
                        //unset($studentList[$key]);
                    }
                }
            }
            //var_dump($list);
            //exit;
            $this->view->studentList = $list;
        }
    }

    public function generateEbipAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $ebModel = new Studentfinance_Model_DbTable_EarlyBirdProcess();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            var_dump($formData);

            //exit;

            if (isset($formData['check']) && count($formData['check'])) {
                foreach ($formData['check'] as $key => $value){
                    $bill_no = $ebModel->getBillSeq(9, date('Y'));

                    $data_dn = array(
                        'dcnt_fomulir_id' => $bill_no,
                        'dcnt_IdStudentRegistration' => $formData['student'][$key],
                        'dcnt_batch_id' => 0,
                        'dcnt_amount' => $formData['ebamount'][$key],
                        'dcnt_type_id' => $formData['ebtype'][$key],
                        'dcnt_description' => $formData['ebdesc'][$key],
                        'dcnt_currency_id' => $formData['ebcur'][$key],
                        'dcnt_creator' => $userId,
                        'dcnt_create_date' => date('Y-m-d H:i:s'),
                        'dcnt_approve_by' => $userId,
                        'dcnt_approve_date' => date('Y-m-d H:i:s'),
                        'dcnt_status' => 'A',//entry
                    );
                    $dn_id = $ebModel->insertEbDiscount($data_dn);

                    $cnDetailData = array(
                        'dcnt_id' => $dn_id,
                        'dcnt_invoice_id' => $formData['invoice'][$key],
                        'dcnt_invoice_det_id' => $key,
                        'dcnt_amount' => $formData['ebamount'][$key],
                        'status' => 'A',
                        'status_by' => $userId,
                        'dcnt_description' => $formData['ebdesc'][$key]
                    );
                    $ebModel->insertEbDiscountDtl($cnDetailData);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => 'EBIP Discount Generated'));
            $this->_redirect($this->baseUrl . '/studentfinance/early-bird/overlooked-ebip/');
        }else{
            $this->_redirect($this->baseUrl . '/studentfinance/early-bird/overlooked-ebip/');
        }
    }
}