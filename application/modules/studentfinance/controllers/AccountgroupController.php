<?php
class Studentfinance_AccountgroupController extends Base_Base { //Controller for the User Module
	private $lobjinitialconfig;	
	private $_gobjlog;
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();	 
	}
	
	public function fnsetObj() {
		$this->lobjAccountgroup = new Studentfinance_Model_DbTable_Accountgroup(); 
		$this->lobjAccountgroupForm = new Studentfinance_Form_Accountgroup();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
	}

   public function indexAction() {
   	
   		$lobjform = new App_Form_Search (); 
		$this->view->lobjform = $lobjform;
   		$data = $this->lobjAccountgroup->fngetAccountgroup();
		//echo "<pre>";print_r($data);die();
		$itemsByReference = array();
		//$IdAccountGroup =  ( $this->_getParam ( 'IdAccountGroup' ) );
		//echo $IdAccountGroup;
		foreach($data as $key => &$item) {
   			$itemsByReference[$item['IdAccountGroup']] = &$item;
			$itemsByReference[$item['IdAccountGroup']]['children'] = array();
  			// $itemsByReference[$item['IdAccountGroup']]['data'] = array(); //commented by pushpa
		}

		// Set items as children of the relevant parent item.
		foreach($data as $key => &$item)
   			if($item['IdParent'] && isset($itemsByReference[$item['IdParent']]))
      		$itemsByReference [$item['IdParent']]['children'][] = &$item;
      	// Remove items that were added to parents elsewhere:
		foreach($data as $key => &$item) {
   		if($item['IdParent'] && isset($itemsByReference[$item['IdParent']]))
      		unset($data[$key]);
		}
		$clean = array_values($data);
		foreach ($clean as &$item) {
    		self::recursiveChildKeysCleaner($item);
		}
		unset($item);
		$jsonresult = Zend_Json_Encoder::encode($clean);
		$jsondata = '{
    				"label":"GroupName",
					"identifier":"IdAccountGroup",
					"items":'.$jsonresult.
				  '}';
		$this->view->jsondata = $jsondata;
		
  	 if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$data = $this->lobjAccountgroup->fngetaccountdeatilsSearch($this->_request->getPost ());
				//echo "<pre>";print_r($data);die();
				$itemsByReference = array();
		//$IdAccountGroup =  ( $this->_getParam ( 'IdAccountGroup' ) );
		//echo $IdAccountGroup;
		foreach($data as $key => &$item) {
   			$itemsByReference[$item['IdAccountGroup']] = &$item;
			$itemsByReference[$item['IdAccountGroup']]['children'] = array();
  		  //$itemsByReference[$item['IdAccountGroup']]['data'] = array(); //commented by pushpa
		}

		// Set items as children of the relevant parent item.
		foreach($data as $key => &$item)
   			if($item['IdParent'] && isset($itemsByReference[$item['IdParent']]))
      		$itemsByReference [$item['IdParent']]['children'][] = &$item;

      	// Remove items that were added to parents elsewhere:
		foreach($data as $key => &$item) {
   		if($item['IdParent'] && isset($itemsByReference[$item['IdParent']]))
      		unset($data[$key]);
		}
		$clean = array_values($data);
		foreach ($clean as &$item) {
    		self::recursiveChildKeysCleaner($item);
		}
		unset($item);
		$jsonresult = Zend_Json_Encoder::encode($clean);
		$jsondata = '{
    				"label":"GroupName",
					"identifier":"IdAccountGroup",
					"items":'.$jsonresult.
				  '}';
		$this->view->jsondata = $jsondata;			
				
			}
		}

   		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/accountgroup/index');
		}
	}
	
	

	
	public function newgroupAction() {
		$countvar=0;
		$IdAccountGroup = ( int ) $this->_getParam ( 'IdAccountGroup' );
		$this->view->lobjAccountgroupForm = $this->lobjAccountgroupForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );

		$this->view->lobjAccountgroupForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAccountgroupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		$lobjparentlist = $this->lobjAccountgroup->fnGetGroupList();
		
		$this->lobjAccountgroupForm->IdParent->addMultiOptions($lobjparentlist);
			if($IdAccountGroup) {
			$this->view->lobjAccountgroupForm->IdParent->setValue ($IdAccountGroup);
		}
		if($this->view->AccountCode==1)
		{
			$this->view->lobjAccountgroupForm->GroupCode->setValue('xxx-xxx-xxx');
			$this->view->lobjAccountgroupForm->GroupCode->setAttrib('readonly','true');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); 
			unset ( $larrformData ['Save'] );		
			//print_r($larrformData);
			//exit;
			if ($this->lobjAccountgroupForm->isValid ( $larrformData )) {
				$linitlastid = $this->lobjAccountgroup->fnInsertAccountGroup($larrformData);
				//
				//echo "<pre>";print_r($linitlastid);die();
				if($this->view->AccountCode==1){
					if($this->view->base == 0){ //level based		
						if($this->view->levelbase == 0) {//fixed							
							$larrformData['GroupCode'] = $this->view->FixedText.$this->view->FixedSeparator.$linitlastid;
						}												
						if($this->view->levelbase == 1) {
							if($larrformData['IdParent'] == 0){								
								$larrcount = $this->lobjAccountgroup->fnCheckstartletter(substr($larrformData['GroupName'], 0, 1));								
								$countvar=count($larrcount);								
								$countvar=$countvar+1;								
								$larrformData['GroupCode'] =  substr($larrformData['GroupName'], 0, 1).$this->view->FirstletterSeparator.$countvar;
								
							} else {
									
									$firstlevel = $this->lobjAccountgroup->fnLevelcode($linitlastid);
									$result = $this->lobjAccountgroup->fnLevelgroupcode($firstlevel['IdParent']);
 									$countres = $this->lobjAccountgroup->fnparentcount($firstlevel['IdParent'],substr($larrformData['GroupName'], 0, 1)); 								
									$larrformData['GroupCode'] = $result['GroupCode'].$this->view->FirstletterSeparator.substr($larrformData['GroupName'], 0, 1).$this->view->FirstletterSeparator.$countres['count'];
							}
						}
					}					
					if($this->view->base == 1){//general based
						$idUniversity =$this->gobjsessionsis->idUniversity;	
						$accCode =  $this->lobjAccountgroup->fnGenerateAccCodes($idUniversity,$linitlastid,$this->lobjinitialconfig);
						$larrformData['GroupCode'] = $accCode;						
					}		
				$this->lobjAccountgroup->fnUpdateAccountGroup($linitlastid,$larrformData);								
			}
			
			// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Account Group Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/accountgroup/index');
			}
		}	
	}
	
	public function editgroupAction() {
		$this->view->lobjAccountgroupForm = $this->lobjAccountgroupForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjAccountgroupForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAccountgroupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$IdAccountGroup = ( int ) $this->_getParam ( 'IdAccountGroup' );
				
		$lobjparentlist = $this->lobjAccountgroup->fnGetGroupListEdit($IdAccountGroup);
		$this->lobjAccountgroupForm->IdParent->addMultiOptions($lobjparentlist);
	if($this->view->AccountCode==1)
		{

			$this->view->lobjAccountgroupForm->GroupCode->setAttrib('readonly','true');
		}

		$larrobj = $this->lobjAccountgroup->fnEditAccountGroup($IdAccountGroup);
		$this->view->lobjAccountgroupForm->populate($larrobj);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); 
			unset ( $larrformData ['Save'] );		
			if ($this->lobjAccountgroupForm->isValid ( $larrformData )) {
				$this->lobjAccountgroup->fnUpdateAccountGroup($IdAccountGroup, $larrformData ); //instance for adding the lobjuserForm values to DB
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Account Group Edit Id=' . $IdAccountGroup,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/accountgroup/index');
			}
		}
	}
	
	function &recursiveChildKeysCleaner(&$arr) {

    if (array_key_exists('children', $arr)) {

        $arr['children'] = array_values($arr['children']);

        foreach ($arr['children'] as &$child) {
            self::recursiveChildKeysCleaner($child);
        }
    }
    return $arr;
}

}