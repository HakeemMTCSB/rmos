<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/3/2016
 * Time: 11:03 AM
 */
class Studentfinance_InvoiceCpController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_InvoiceCp();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Invoice CP');

        //get fee category
        $feeCategoryDB = new Studentfinance_Model_DbTable_FeeCategory();
        $this->view->fee_category = $feeCategoryDB->getListCategory();

        $branchList = $this->model->getBranch();
        $this->view->branchList = $branchList;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $this->model->getInvoiceList($formData);
            $this->view->list = $list;
        }
    }

    public function addAction(){
        $this->view->title = $this->view->translate('Add Invoice CP');

        $form = new Studentfinance_Form_AddInvoiceCp();
        $this->view->form = $form;

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
            $currency = $currencyDb->getCurrentExchangeRate($formData['currency']);

            $bill_no = $this->generateId();
            $data_invoice = array(
                'bill_number' => $bill_no,
                'branch_id' => $formData['branch'],
                'appl_id' => 0,
                'trans_id' => 0,
                'academic_year' => 0,
                'semester' => $formData['invoice_semester'],
                'bill_paid' => 0,
                'bill_description' => $formData['invoice_main_desc'],
                'currency_id' => $formData['currency'],
                'exchange_rate' => $currency['cr_id'],
                'invoice_date' => date('Y-m-d',strtotime($formData['invoice_date'])),
                'status' => 'E',
                'proforma_invoice_id' => 0,
                'invoice_date'=>date('Y-m-d'),
                'date_create'=>date('Y-m-d H:i:s'),
                'creator'=>$getUserIdentity->id
            );
            $invId = $this->model->insertInvoice($data_invoice);

            if (isset($formData['student_arr']) && count($formData['student_arr']) > 0){
                foreach ($formData['student_arr'] as $key2 => $studentLoop){
                    $student_data = array(
                        'ics_invcp_id' => $invId,
                        'ics_type' => $formData['invoice_type_arr'][$key2],
                        'ics_updby'=> $getUserIdentity->id,
                        'ics_upddate' => date('Y-m-d H:i:s')
                    );

                    if ($formData['invoice_type_arr'][$key2] == 1){
                        $student_data['ics_student_id'] = $studentLoop;
                    }else{
                        $student_data['ics_name'] = $studentLoop;
                    }
                    $this->model->insertStudent($student_data);
                }
            }

            $amount = 0;
            if (count($formData['fee_item_arr']) > 0){
                foreach ($formData['fee_item_arr'] as $key => $invLoop){
                    $amount = $amount + $formData['amount_arr'][$key];

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invId,
                        'fi_id' => $invLoop,
                        'fee_item_description' => $formData['invoice_details_desc_arr'][$key],
                        'cur_id' => $formData['currency'],
                        'amount' => $formData['amount_arr'][$key],
                        'balance' => $formData['amount_arr'][$key],
                        'exchange_rate' => $currency['cr_id']
                    );
                    $this->model->insertInvoiceDtl($invoiceDetailData);
                }
            }

            $data_invoice2 = array(
                'bill_amount' => $amount,
                'bill_balance' => $amount
            );
            $this->model->updateInvoice($data_invoice2, $invId);

            //upload file
            $folder = '/invoicecp/'.$invId;
            $dir = DOCUMENT_PATH.$folder;

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false )
                {
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();

            if ($files){
                foreach ($files as $file=>$info){
                    if ($adapter->isValid($file)){
                        //var_dump($info); exit;
                        $fileOriName = $info['name'];
                        $fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
                        $filepath = $info['destination'].'/'.$fileRename;

                        if ($adapter->isUploaded($file)){
                            $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
                            if ($adapter->receive($file)){
                                $uploadData = array(
                                    'icu_invcp_id'=>$invId,
                                    'icu_filename'=>$fileOriName,
                                    'icu_filerename'=>$fileRename,
                                    'icu_filelocation'=>$folder.'/'.$fileRename,
                                    'icu_filesize'=>$info['size'],
                                    'icu_mimetype'=>$info['type'],
                                    'icu_upddate'=>date('Y-m-d H:i:s'),
                                    'icu_upduser'=>$getUserIdentity->id
                                );
                                $this->model->uploadfile($uploadData);
                            }
                        }
                    }
                }
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/studentfinance/invoice-cp/view/id/'.$invId);
        }
    }

    public function viewAction(){
        $id = $this->_getParam('id', 0);

        $this->view->title = $this->view->translate('Invoice CP Details');

        $invoice = $this->model->getInvoice($id);

        if ($invoice){
            $invoiceDtl = $this->model->getInvoiceDtl($id);
            $invoiceStd = $this->model->getInvStdList($id);
            $upload = $this->model->getUpload($id);
            $this->view->invoice = $invoice;
            $this->view->invoiceDtl = $invoiceDtl;
            $this->view->invoiceStd = $invoiceStd;
            $this->view->upload = $upload;
        }else{
            $this->_redirect($this->baseUrl . '/studentfinance/invoice-cp/index/');
        }
    }

    public function approveAction(){
        $id = $this->_getParam('id', 0);

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $data_invoice = array(
            'status' => 'A',
            'approve_date' => date('Y-m-d H:i:s'),
            'approve_by' => $getUserIdentity->id
        );
        $this->model->updateInvoice($data_invoice, $id);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Invoice Approved'));
        $this->_redirect($this->baseUrl . '/studentfinance/invoice-cp/view/id/'.$id);
    }

    public function cancelAction(){
        $id = $this->_getParam('id', 0);

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $data_invoice = array(
            'status' => 'X',
            'cancel_date' => date('Y-m-d H:i:s'),
            'cancel_by' => $getUserIdentity->id
        );
        $this->model->updateInvoice($data_invoice, $id);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Invoice Canceled'));
        $this->_redirect($this->baseUrl . '/studentfinance/invoice-cp/view/id/'.$id);
    }

    public function paymentAction(){
        $id = $this->_getParam('id', 0);
        $this->view->id = $id;

        $this->view->title = $this->view->translate('Update Payment');

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $invoice = $this->model->getInvoice($id);

        if ($invoice){
            $invoiceDtl = $this->model->getInvoiceDtl($id);
            $invoiceStd = $this->model->getInvStdList($id);
            $this->view->invoice = $invoice;
            $this->view->invoiceDtl = $invoiceDtl;
            $this->view->invoiceStd = $invoiceStd;
        }else{
            $this->_redirect($this->baseUrl . '/studentfinance/invoice-cp/index/');
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data_invoice = array(
                'bill_paid' => $formData['amount'],
                'bill_paid_date' => date('Y-m-d H:i:s'),
                'bill_paid_by' => $getUserIdentity->id
            );
            $this->model->updateInvoice($data_invoice, $id);

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Payment Updated'));
            $this->_redirect($this->baseUrl . '/studentfinance/invoice-cp/payment/id/'.$id);
        }
    }

    public function generateId(){
        $year = date('Y');
        $seqno = $this->model->sequence($year);

        $refno = strtoupper($seqno['isc_constant1'].date('y', strtotime('01-01-'.$seqno['isc_year'])).sprintf("%05d", $seqno['isc_seq']));

        $data = array(
            'isc_seq'=>($seqno['isc_seq']+1)
        );
        $this->model->updateSeq($data, $seqno['isc_id']);

        $checkDuplicate = $this->model->checkDuplicate($refno);

        if ($checkDuplicate){
            $refno = $this->generateId();
        }

        return $refno;
    }

    public function findStudentAction(){
        $searchElement = $this->_getParam('term', '');

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $list = $this->model->getStudent($searchElement);

        if ($list){
            foreach ($list as $key => $loop){

                $arr[] = array(
                    'id'=> $loop['IdStudentRegistration'],
                    'label' => $loop['appl_fname'].' '.$loop['appl_lname'].' - '.$loop['registrationId'].' - '.$loop['ProgramCode'].' - '.$loop['status_name'],
                    'value' => $loop['appl_fname'].' '.$loop['appl_lname'].' - '.$loop['registrationId'].' - '.$loop['ProgramCode'].' - '.$loop['status_name']
                );
            }
        }else{
            $arr[] = array(
                'id'=> 0,
                'label' => 'No Records',
                'value' => 'No Records'
            );
        }

        $json = Zend_Json::encode($arr);

        echo $json;
        exit();
    }
}