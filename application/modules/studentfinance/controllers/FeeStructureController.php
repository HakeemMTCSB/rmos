<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_FeeStructureController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_FeeStructure();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
	  
        $intake_id = $this->_getParam('intake', null);
        
        if($intake_id){
          $this->view->intake = $intake_id;
        }
        
		//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up");
    	
    	$this->view->searchForm = new Studentfinance_Form_FeeStructureSearch();
    	
    	$programDB = new GeneralSetup_Model_DbTable_Program();
		
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$larrresult = $programDB->getPaginateProgramDetails($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($larrresult));		
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
		}else{
						
			$larrresult =	$programDB->getProgramDetails();
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($larrresult));			
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
		
		
		$this->view->paginator = $paginator;
    	
    	//student category
    	$defTypeDb = new App_Model_Definitiontype();
    	$student_category_list = $defTypeDb->fnGetDefinations('Student Category');
    	$this->view->student_category_list = $student_category_list; 	
    	    	
	}
	
	public function listAction() {
	  
        $program_id = $this->_getParam('program_id', null);
        
        if($program_id){
          $this->view->program_id = $program_id;
        }
        
		//title
    	$this->view->title= $this->view->translate("Fee Structure List");
    	
    	$programDB = new GeneralSetup_Model_DbTable_Program();

		$programinfo = $programDB->fngetProgramDetails($program_id);
		$this->view->programinfo = $programinfo;
    	
    	$this->view->searchForm = new Studentfinance_Form_FeeStructureSearch();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
//			echo "<pre>";
//			print_r($formData);			
			//paginator
			$data = $this->_DbObj->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->paginator = $paginator;
		
    	}
    	
    	//student category
    	$defTypeDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
    	$student_category_list = $defTypeDb->getListDataByCodeType('student-category');
    	$this->view->student_category_list = $student_category_list; 	
    	    	
	}
	
	public function feeStructureCategoryAction(){
		
		$this->_helper->layout->disableLayout();
		
		$id = $this->_getParam('id', null);
		$program_id = $this->_getParam('program_id', null);
		
		$data = $this->_DbObj->getFeeStructureByProgramCategory($id,$program_id);
		$this->view->data = $data;

	}
	
	public function localAction(){
		
		$this->_helper->layout->disableLayout();
		
		$intake_id = $this->_getParam('intake', null);
		
		//paginator local
		$data = $this->_DbObj->getPaginateDataByCategory('314',$intake_id);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page_local',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function foreignAction(){
		
		$this->_helper->layout->disableLayout();
		
		$intake_id = $this->_getParam('intake', null);
		
		//paginator foreigner
		$data = $this->_DbObj->getPaginateDataByCategory('315',$intake_id);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page_foreign',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function copyAction()
	{
		$id = $this->_getParam('id', 0);
		
		//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Copy");

		$info = $this->_DbObj->getData($id);
		
		if ($this->getRequest()->isPost()) 
		{	
			$formData = $this->getRequest()->getPost();
			
			$db = getDB();

			//structure
			$data = array(
							'fs_name'					=> $formData['fs_name'], 
							'fs_description'			=> $info['fs_description'], 
							'fs_effective_date'			=> $info['fs_effective_date'], 
							'fs_intake_start'			=> $info['fs_intake_start'], 
							'fs_intake_end'				=> $info['fs_intake_end'], 
							'fs_semester_start'			=> $info['fs_semester_start'], 
							'fs_student_category'		=> $info['fs_student_category'], 
							'fs_cur_id'					=> $info['fs_cur_id'], 
							'fs_ac_id'					=> $info['fs_ac_id'], 
							'fs_estimated_total_fee'	=> $info['fs_estimated_total_fee'], 
							'fs_cp'						=> $info['fs_cp']
						);



			$db->insert('fee_structure', $data);
			$fs_id = $db->lastInsertId();

			$select = $db->select()->from(array('a'=>'fee_structure_item'))->where('fsi_structure_id = ?', $info['fs_id']);
			$itemList = $db->fetchAll($select);

			//fee_structure
			foreach ( $itemList as $list )
			{
				$data = array(
								'fsi_structure_id'	=> $fs_id,
								'fsi_item_id'		=> $list['fsi_item_id'],
								'fsi_amount'		=> $list['fsi_amount'],
								'fsi_cur_id'		=> $list['fsi_cur_id'],
								'fsi_registration_item_id'		=> $list['fsi_registration_item_id'],
							);
			
				$db->insert('fee_structure_item', $data);
				$item_id = $db->lastInsertId();

				//fee_structure_item_level
				$levelselect = $db->select()->from(array('a'=>'fee_structure_item_level'))->where('fsis_item_id = ?', $list['fsi_id']);
				$levelList = $db->fetchAll($levelselect);
				foreach ( $levelList as $level )
				{
					$data = array(
									'fsis_item_id'	=> $item_id,
									'fsis_level'	=> $level['fsis_level']
								);

					$db->insert('fee_structure_item_level', $data);
				}

				//fee_structure_item_semester
				$semselect = $db->select()->from(array('a'=>'fee_structure_item_semester'))->where('fsis_item_id = ?', $list['fsi_id']);
				$semList = $db->fetchAll($semselect);
				foreach ( $semList as $sem )
				{
					$data = array(
									'fsis_item_id'	=> $item_id,
									'fsis_semester'	=> $sem['fsis_semester']
								);

					$db->insert('fee_structure_item_semester', $data);
				}

				//fee_structure_item_subject
				$subselect = $db->select()->from(array('a'=>'fee_structure_item_subject'))->where('fsisub_fsi_id = ?', $list['fsi_id']);
				$subList = $db->fetchAll($subselect);
				foreach ( $subList as $sub )
				{
					$data = array(
									'fsisub_fsi_id'	=> $item_id,
									'fsisub_subject_id'	=> $sub['fsisub_subject_id']
								);

					$db->insert('fee_structure_item_subject', $data);
				}

				//fee_structure_optional_item
				$optionalselect = $db->select()->from(array('a'=>'fee_structure_optional_item'))
											   ->where('fsoi_structure_id = ?', $info['fs_id'])
											   ->where('fsoi_detail_id =?', $list['fsi_id']);
				$optList = $db->fetchAll($optionalselect);
				foreach ( $optList as $opt )
				{
					$data = array(
									'fsoi_item_id'		=> $opt['fsoi_item_id'],
									'fsoi_structure_id'	=> $fs_id,
									'fsoi_detail_id'	=> $item_id,
									'fsoi_currency_id'	=> $opt['fsoi_currency_id'],
									'fsoi_amount'		=> $opt['fsoi_amount'],
									'added_by'			=> 1,
									'added_date'		=> new Zend_Db_Expr('NOW()')
								);
					$db->insert('fee_structure_optional_item', $data);
				}
			}

			$select = $db->select()->from(array('a'=>'fee_structure_program'))->where('fsp_fs_id = ?', $info['fs_id']);
			$programList = $db->fetchAll($select);
			
			foreach ( $programList as $program )
			{
				$data = array(
								'fsp_fs_id'				=> $fs_id,
								'fsp_program_id'		=> $program['fsp_program_id'], 
								'fsp_first_sem_sks'		=> $program['fsp_first_sem_sks'], 
								'fsp_idProgramScheme'	=> $program['fsp_idProgramScheme']
						);

				$db->insert('fee_structure_program', $data);
				$prog_id = $db->lastInsertId();

				//fee_structure_program_item
				$progitemselect = $db->select()->from(array('a'=>'fee_structure_program_item'))
											   ->where('fspi_fs_id = ?', $info['fs_id'])
											   ->where('fspi_program_id = ?', $program['fsp_id']);
				$progitemList = $db->fetchAll($progitemselect);
				foreach ( $progitemList as $progitem )
				{
					$data = array(
									'fspi_type'			=> $progitem['fspi_type'],
									'fspi_fs_id'		=> $fs_id,
									'fspi_program_id'	=> $prog_id,
									'fspi_fee_id'		=> $progitem['fspi_fee_id'],
									'fspi_currency_id'	=> $progitem['fspi_currency_id'],
									'fspi_amount'		=> $progitem['fspi_amount'],
									'fspi_document_type'=> $progitem['fspi_document_type'],
									'added_by'			=> 1,
									'added_date'		=> new Zend_Db_Expr('NOW()')
								);

					$db->insert('fee_structure_program_item', $data);
				}
			}

			//-- fee_structure_item (done)
			//---- fee_structure_item_level (done)
			//---- fee_structure_item_semester (done)
			//---- fee_structure_item_subject (done)
			//---- fee_structure_optional_item (done)

			//-- fee_structure_plan (not used)
			//-- fee_structure_plan_detl (not used)

			//-- fee_structure_program
			//---- fee_structure_program_item

			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true));
		}

		//views
		$this->view->info = $info;
	}

	public function addAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Add");
    	    	
    	$program_id = $this->_getParam('program_id', null);
        
        if($program_id){
          $this->view->program_id = $program_id;
        }
        
        
    	$form = new Studentfinance_Form_FeeStructure(array('programid'=>$program_id));
    	$form->removeElement('fs_intake_end');
    	$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true)."'; return false;";
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
				
				$dataFs = array(
						'fs_name' => $formData['fs_name'],
						'fs_description' => $formData['fs_description'],
						'fs_intake_start' => $formData['fs_intake_start'],
						'fs_semester_start' => $formData['fs_semester_start'],
						'fs_effective_date' =>  date('Y-m-d',strtotime($formData['fs_effective_date'])),
						'fs_estimated_total_fee' => $formData['fs_estimated_total_fee'],
						'fs_student_category' => $formData['fs_student_category'],
						'fs_ac_id' => $formData['fs_ac_id'],
						'fs_prepayment_ac' => $formData['fs_prepayment_ac'],
						'fs_cur_id' => $formData['fs_cur_id'],
						'fs_cp' => $formData['fs_cp'],
						'fs_status' => $formData['fs_status'],
				
				);
				
				
				//unset($formData['save']);
				//$formData['fs_effective_date'] = date('Y-m-d',strtotime($formData['fs_effective_date']));
				$code = $this->_DbObj->insert($dataFs);
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$id = $lobjDbAdpt->lastInsertId();
				
				$dataProgram = array(
				        'fsp_fs_id' => $id,
						'fsp_program_id' => $formData['fsp_program_id'],
						'fsp_first_sem_sks' => $formData['fsp_first_sem_sks'],
						'fsp_idProgramScheme' => $formData['fsp_idProgramScheme']
				);
		
				$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
				$feeStructureProgramDb->addData($dataProgram);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail','id'=>$code),'default',true));	
			}else{
				$form->populate($formData);
			}
        	
        }
    	
        $this->view->form = $form;
    }
    
	public function editAction(){
		$id = $this->_getParam('id', 0);
		
		//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Edit");
    	
    	//form
    	$form = new Studentfinance_Form_FeeStructureSearch();
    	$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true)."'; return false;";
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
	    	if ($form->isValid($formData)) {
	    		unset($formData['save']);
	    		$formData['fs_effective_date'] = date('Y-m-d',strtotime($formData['fs_effective_date']));
				
				$this->_DbObj->update($formData, 'fs_id = '.$id);
				
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update fee structure');
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$id),'default',true)); 
			}else{
				$form->populate($formData);	
			}
			
    	}else{
    		if($id!=0){
    			
    			$form->populate($this->_DbObj->getData($id));
    		}
    	}
    	
    	$this->view->form = $form;
    }
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	
    	if($id!=0){
    		$this->_DbObj->deleteData($id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true));
    	
    }
    
    public function detailAction(){
    	$id = $this->_getParam('id', 0);
    	$this->view->fee_structure_id = $id;
    	
    	//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Detail");
    	
    	//fee-structure data
    	$this->view->fee_structure = $this->_DbObj->getData($id);
    	
    	//fee structure program
    	$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
    	$this->view->fee_structure_program = $feeStructureProgramDb->getStructureData($id);
    	
    	//fee structure item
    	$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
    	$this->view->fee_structure_item = $feeStructureItemDb->getStructureData($id);
    	
    	//currency
    	$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$listData = $currencyDb->fetchAll('cur_status = 1')->toArray();
    	$this->view->currency_list = $listData;
    	    	
    	//programme list
    	$programDb = new App_Model_Record_DbTable_Program();
		$programList = $programDb->getData();
		
		//registration item list
		$definationDb = new App_Model_General_DbTable_Definationms();
		$this->view->registrationItem = $definationDb->getByCode('Registration Item');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$arr_program = array();
		$i=0;
		if ($locale=="en_US"){
			foreach ($programList as $list){
				$arr_program[] = array('id'=>$list['IdProgram'],'name'=>$list['ProgramName']);
			}	
		}else{
			foreach ($programList as $list){
				$arr_program[] = array('id'=>$list['IdProgram'],'name'=>$list['ArabicName']);
			}
		}
		$this->view->programList = $arr_program;
		
		//fee item list
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$this->view->feeItemList = $feeItemDb->getActiveFeeItem();
		
    }
    

	public function addProgramItemAction()
	{
		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
		}

		$program_id = $this->_getParam('fspi_program_id', 0);
		$type = $this->_getParam('fspi_type');
		$fs_id = $this->_getParam('fspi_fs_id');
		$fs_idProgram = $this->_getParam('fs_program');
		$fs_idScheme = $this->_getParam('fs_scheme');
		
		$fsData = $this->_DbObj->getData($fs_id);
		$idCategory = $fsData['fs_student_category'];
		
		$auth = Zend_Auth::getInstance(); 

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$db = getDB();
			$data = array(
							'fspi_fs_id'			=> $formData['fspi_fs_id'],
							'fspi_program_id'		=> $formData['fspi_program_id'],
							'fspi_type'				=> $formData['fspi_type'],
							'fspi_fee_id'			=> $formData['fspi_fee_id'],
							'fspi_amount'			=> $formData['fspi_amount'],
							'fspi_currency_id'		=> $formData['fspi_currency_id'],
							'fspi_document_type'	=> $formData['fspi_document_type'],
							'added_by'				=>	$auth->getIdentity()->iduser,
							'added_date'			=>	new Zend_Db_Expr('NOW()')
					);
			$db->insert('fee_structure_program_item', $data);

			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fspi_fs_id']),'default',true));	
        }else{
        	$this->view->form = new Studentfinance_Form_FeeStructureProgramItem(array('programid'=>$fs_idProgram,'scheme'=>$fs_idScheme,'category'=>$idCategory));
		
		$this->view->form->populate(array('fspi_program_id' => $program_id, 'fspi_type' => $type, 'fspi_fs_id'=>$fs_id));
        }
	}

	public function viewProgramItemAction()
	{
		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
		}
		
		
		$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
		$id = $this->_getParam('id');
		$fs_id = $this->_getParam('fs_id');

		$info = $feeStructureProgramDb->getData($id);
			
		$programDB = new GeneralSetup_Model_DbTable_Program();
		$programinfo = $programDB->fngetProgramDetails($info['fsp_program_id']);
		
		//application
		$this->view->itemApplication = $feeStructureProgramDb->getItemData($fs_id, $id, 'application');
		
		//registration
		$this->view->itemRegistration = $feeStructureProgramDb->getItemData($fs_id, $id, 'registration');

		$this->view->programinfo = $programinfo[0];
		$this->view->info = $info;

	}

    public function addProgramAction(){
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
				
			$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
			$feeStructureProgramDb->addData($formData);
				
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsp_fs_id']),'default',true));	
        }
    }
    
    public function deleteProgramAction(){
    	
    	$id = $this->_getParam('id', 0);
    	
    	$feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
    	$fspData = $feeStructureProgramDb->getData($id);
    	
    	$feeStructureProgramDb->deleteData($id);

		$db = getDB();
		$db->delete('fee_structure_program_item', 'fspi_fs_id='.$id);
    			
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$fspData['fsp_fs_id']),'default',true));
    }
    
	public function addBillAction(){
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
				
			//add structure
			$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
			$feeStructureItemDb->addData($formData);
				
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new fee');
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsi_structure_id']),'default',true));	
			
        	
        }
    }
    
    public function editBillAction()
	{
    	if ($this->getRequest()->isXmlHttpRequest()) 
		{
    		$this->_helper->layout->disableLayout();
    	}
    	
    	//fsi_id, fs_id
		$structure_id = $this->_getParam('fs_id');
		$item_id = $this->_getParam('fsi_id');
		
		$this->view->fee_structure_id = $structure_id;
		$this->view->fi_id = $item_id;
		
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			$item_id = $formData['fi_id'];
			$structure_id = $formData['fsi_structure_id'];
			
			$auth = Zend_Auth::getInstance(); 
			
			$data = array(
							'fsi_item_id'			=> $formData['fsi_item_id'],
							'fsi_amount'			=> $formData['fsi_amount'],
							'fsi_registration_item_id'			=> $formData['fsi_registration_item_id'],
							'fsi_cur_id'			=> $formData['fsi_cur_id'],
							'created_by'			=>	$auth->getIdentity()->iduser,
							'created_date'			=>	new Zend_Db_Expr('NOW()')
					);
					
			$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
					
			$feeStructureItemDb->update($data, array('fsi_id =?'=>$item_id));

			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$structure_id),'default',true));	
        }
        
        $db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('a'=>'fee_structure_item'))
	                 ->where('a.fsi_id = ?', $item_id);
	    
//        $stmt = $db->query($select);
        $row = $db->fetchRow($select);
        
        $this->view->fee = $row;
        
        //currency
    	$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$listData = $currencyDb->fetchAll('cur_status = 1')->toArray();
    	$this->view->currency_list = $listData;
    	
    	//registration item list
		$definationDb = new App_Model_General_DbTable_Definationms();
		$this->view->registrationItem = $definationDb->getByCode('Registration Item');

		//fee item list
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$this->view->feeItemList = $feeItemDb->getActiveFeeItem();
    }
    
    
	public function deleteBillAction(){
    	
    	$id = $this->_getParam('id', 0);
    	
    	$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
    	$fspData = $feeStructureItemDb->getData($id);
    	
    	$feeStructureItemDb->deleteData($id);
    			
    	$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Fee Deleted');
    	
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$fspData['fsi_structure_id']),'default',true));
    }
    
    public function editAmountAction(){
    	$formData = $this->getRequest()->getPost();
				
		//edit item
		$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
		$feeStructureItemDb->updateAmount($formData, $formData['fsi_id']);
			
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsi_structure_id']),'default',true));
    }
    
    public function editSemesterAction(){
    	$formData = $this->getRequest()->getPost();
    	
    	$feeStructureItemSemesterDb = new Studentfinance_Model_DbTable_FeeStructureItemSemester();
    	
    	$feeStructureItemSemesterDb->updateItemData($formData);
    	
    	//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsi_structure_id']),'default',true));
    }
    
    public function editLevelAction(){
    	$formData = $this->getRequest()->getPost();
    	 
    	$feeStructureItemLevelDb = new Studentfinance_Model_DbTable_FeeStructureItemLevel();
    	 
    	$feeStructureItemLevelDb->updateItemData($formData);
    	 
    	//redirect
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsi_structure_id']),'default',true));
    }
    
    
    public function editOptionalItemAction()
	{
    	if ($this->getRequest()->isXmlHttpRequest()) 
		{
    		$this->_helper->layout->disableLayout();
    	}
    	
    	//fsi_id, fs_id
		$structure_id = $this->_getParam('fs_id');
		$item_id = $this->_getParam('fsi_id');
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$structure_id = $formData['fsoi_structure_id'];
			$item_id = $formData['fsoi_item_id'];

			//[fsoi_structure_id] => 88 [fsoi_item_id] => 1 [fsoi_fee_id] => 1 [fsoi_amount]
			$auth = Zend_Auth::getInstance(); 
			
			$db = getDB();
			$data = array(
							'fsoi_structure_id'		=> $formData['fsoi_structure_id'],
							'fsoi_item_id'			=> $formData['fsoi_item_id'],
							'fsoi_fee_id'			=> $formData['fsoi_fee_id'],
							'fsoi_detail_id'		=> $formData['fsoi_detail_id'],
							'fsoi_amount'			=> $formData['fsoi_amount'],
							'fsoi_currency_id'		=> $formData['fsoi_currency_id'],
							'added_by'				=>	$auth->getIdentity()->iduser,
							'added_date'			=>	new Zend_Db_Expr('NOW()')
					);
			$db->insert('fee_structure_optional_item', $data);

			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsoi_structure_id']),'default',true));	
        }

		
		$optitemDB = new Studentfinance_Model_DbTable_FeeStructureOptionalItem();
		$optitemList = $optitemDB->getDataByStructure( $structure_id, $item_id );
		
		$this->view->itemList = $optitemList;

		
		$fee_structure = $this->_DbObj->getData($structure_id);

		$form = new Studentfinance_Form_FeeStructureOptionalItem();
		$form->fsoi_structure_id->setValue($structure_id);
		$form->fsoi_detail_id->setValue($item_id);

		$regitemDB = new Registration_Model_DbTable_RegistrationItem();
		$regitemList = $regitemDB->getItems($structure_id, $fee_structure['fs_semester_start']);
		foreach ( $regitemList as $regitem )
		{
			$form->fsoi_item_id->addMultiOption($regitem['ri_id'], $regitem['item_name']);
		}

		//view
		$this->view->form = $form;
    }
    
	public function ajaxGetItemSemesterAction(){
		
    	$item_id = $this->_getParam('fsis_item_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select(array('fsis_semester'))
	                 ->from(array('fsis'=>'fee_structure_item_semester'))
	                 ->where('fsis.fsis_item_id = ?', $item_id)
	                 ->order('fsis.fsis_semester  ASC');
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();

		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    public function ajaxGetItemLevelAction(){
    
    	$item_id = $this->_getParam('fsis_item_id', 0);
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select = $db->select(array('fsis_semester'))
    	->from(array('fsis'=>'fee_structure_item_level'))
    	->where('fsis.fsis_item_id = ?', $item_id)
    	->order('fsis.fsis_level  ASC');
    	 
    	$stmt = $db->query($select);
    	$row = $stmt->fetchAll();
    
    	$ajaxContext->addActionContext('view', 'html')
    	->addActionContext('form', 'html')
    	->addActionContext('process', 'json')
    	->initContext();
    
    	$json = Zend_Json::encode($row);
    
    	echo $json;
    	exit();
    }
    
    public function ajaxGetProgramSchemeAction(){
    
    	$program_id = $this->_getParam('program_id', 0);
    	
    	if($program_id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$programSchemeDb = new GeneralSetup_Model_DbTable_Programscheme();
    	$row = $programSchemeDb->getProgSchemeByProgram($program_id);
    
    	$ajaxContext->addActionContext('view', 'html')
    	->addActionContext('form', 'html')
    	->addActionContext('process', 'json')
    	->initContext();
    
    	$json = Zend_Json::encode($row);
    
    	echo $json;
    	exit();
    }
    
    public function viewFeeStructureAction(){
    	$this->view->title = $this->view->translate("View Program Fee Structure");
    	
    	//get program list
    	$facultyDb = new App_Model_General_DbTable_Collegemaster();
    	$faculty_list = $facultyDb->getFaculty();
    	
    	//get program
    	$programDb = new GeneralSetup_Model_DbTable_Program();
    	foreach ($faculty_list as $index=>$faculty){
    		$faculty_list[$index]['program'] = $programDb->fngetFacultyProgramData($faculty['IdCollege']);
    	}
    	
    	$this->view->program_list = $faculty_list;
    	
    	
    	if ($this->getRequest()->isPost()) {
    			
    		$formData = $this->getRequest()->getPost();
    		$this->view->pid = $formData['pid'];

    		if($formData['pid']!="null"){
	    		$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
	    		$landscape_list = $landscapeDb->fnLandscapeList($formData['pid']);
	    		
	    		$this->view->landscape_list = $landscape_list;
    		}
    	}
    }
    
    public function viewFeeStructureDetailAction(){
    	
    	$program_id = $this->_getParam('pid', null);
    	$this->view->pid = $program_id;
    	
    	$landscape_id = $this->_getParam('lid', null);
    	$this->view->lid = $landscape_id;
    	
    	$fee_structure_id = $this->_getParam('fsid', null);
    	$this->view->fsid = $fee_structure_id;
    	
    	$this->view->title = $this->view->translate("View Fee Structure - Detail");
    	
    	if($program_id && $landscape_id){
    		
    		//get program
    		$programDb = new GeneralSetup_Model_DbTable_Program();
    		$program = $programDb->fngetProgramData($program_id);
    		$this->view->program = $program;
    		$programId = $program['IdProgram'];
    		
    		//get landscape info
    		$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
    		$landscape = $landscapeDb->getLandscapeDetails($landscape_id);
    		$this->view->landscape = $landscape;
    		$landscapeId = $landscape['IdLandscape'];
    		
	    	if($landscape["LandscapeType"]==43) {//Semester Based         	
	         	
				//get majoring for this program
				$progMajDb = new GeneralSetup_Model_DbTable_ProgramMajoring();	
				$majoring = $progMajDb->getData($programId);
				$this->view->majoring = $majoring;
								
				//get Compulsory Common Course
				$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
				$compulsory_course = $landscapeSubjectDb->getCommonCourse($programId,$landscapeId,1);
				$this->view->compulsory_course = $compulsory_course;		
				
				//get Not Compulsory Common Course
				$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
				$elective_course = $landscapeSubjectDb->getCommonCourse($programId,$landscapeId,2);
				$this->view->elective_course = $elective_course;         	
	         	
				if($majoring){
					foreach ($majoring as $index=>$major){
						
						//common course
						$subjects = array_merge($compulsory_course,$elective_course);
						
						//majoring course
						$compulsory_majoring_course = $landscapeSubjectDb->getMajoringCourse($programId,$landscapeId,$major["IDProgramMajoring"],1);
						$notcompulsory_majoring_course = $landscapeSubjectDb->getMajoringCourse($programId,$landscapeId,$major["IDProgramMajoring"],2);
						
						$subjects = array_merge($subjects,$compulsory_majoring_course,$notcompulsory_majoring_course);
						
						$majoring[$index]['subjects'] = $subjects;
					}
					
					$this->view->major_list = $majoring;
				}
	        
	         }else
	         if($landscape["LandscapeType"]==44){
	         		         	
	         	//get block
	         	$blockSemDB = new GeneralSetup_Model_DbTable_LandscapeBlockSemester();
	         	$sem_list = array();
	         	for($i=1; $i<=$landscape['SemsterCount']; $i++){
	         		$sem_list[$i] = $blockSemDB->getlandscapeblockBySem($landscape['IdLandscape'],$i);
	         	}
	         	
	         	
	         	//get course for block
	         	$blockCourseDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
	         	foreach ($sem_list as $index=>$block_list){
	         		
	         		foreach ($block_list as $index2=>$block){
	         			
	         			$compulsory_course = $blockCourseDb->getBlockCourse($landscape['IdLandscape'],$block["idblock"],1);
	         			$notcompulsory_course = $blockCourseDb->getBlockCourse($landscape['IdLandscape'],$block["idblock"],2);
	         			
	         			$subjects = array_merge($compulsory_course,$notcompulsory_course);
	         			
	         			$sem_list[$index][$index2]['subjects'] = $subjects;
	         		}
	         		
	         	}

	         	$this->view->block_sem = $sem_list;
	         	
	         }
	         
	         
	         //fee structure list
	         $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
	         $feeStructureList = $feeStructureDb->getProgramFeeStructureList($programId);
	         $this->view->feeStructureList = $feeStructureList;
	         
	         if(!$fee_structure_id){
	         	$fee_structure_id = $feeStructureList[0]['fs_id'];
	         }
	         $this->view->fsid = $fee_structure_id;
	         
	         //fee item
	         $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
	         $fee_stucture = $feeStructureItemDb->getStructureData($fee_structure_id);
	         	         
	         
	         
	         for($i=0; $i<$landscape['SemsterCount']; $i++){
	         	$sem_fee_item = array();
	         	foreach ($fee_stucture as $fs){
	         		
	         		//1st sem
	         		if($i==0 && $fs['fi_frequency_mode']== 302 ){
	         			$sem_fee_item[] = $fs; 
	         		}

	         		//every sem
	         		if($fs['fi_frequency_mode']== 303 || $fs['fi_frequency_mode']== 453){
	         			$sem_fee_item[] = $fs;
	         		}
	         		
	         		if($i>0 && $fs['fi_frequency_mode']== 304){
	         		  $sem_fee_item[] = $fs;
	         		}
	         		
	         		//defined semester
	         		if($fs['fi_frequency_mode']== 305){
	         			
	         			foreach ($fs['semester'] as $sem_defined){
	         				if($sem_defined['fsis_semester'] == $i+1){
	         					$sem_fee_item[] = $fs;
	         				}
	         			}
	         		}
	         	}
	         	
	         	$sem_fee[$i+1]['fee_item'] = $sem_fee_item;
	         }
	         $this->view->sem_fee = $sem_fee;
	         
	         
    	}else{
    		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true));
    	}
    	
    }
    
    public function editSubjectAction(){
    
      $fsi_id = $this->_getParam('fsi_id', 0);
      $type = $this->_getParam('type', null);
      $search = $this->_getParam('search', null);
      
      $this->_helper->layout->disableLayout();
      
      if ($this->getRequest()->isPost()) {
         
        $formData = $this->getRequest()->getPost();

        
        $feeStructureItemSubjectDb = new Studentfinance_Model_DbTable_FeeStructureItemSubject();
        
        if(isset($formData['remove_subject'])){
          foreach ($formData['remove_subject'] as $subject_id){
        
            $feeStructureItemSubjectDb->delete('fsisub_subject_id = '.$subject_id.' and fsisub_fsi_id ='.$formData['fsi_id']);
          }
        
        }
        
        if(isset($formData['add_subject'])){
          foreach ($formData['add_subject'] as $subject_id){
            
            $data = array(
                'fsisub_fsi_id' => $formData['fsi_id'],
                'fsisub_subject_id' => $subject_id
            );
            
            $feeStructureItemSubjectDb->insert($data);
          }
          
        }
        
        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fsi_structure_id']),'default',true));
        
      }
      
    
      $ajaxContext = $this->_helper->getHelper('AjaxContext');
      $ajaxContext->addActionContext('view', 'html');
      $ajaxContext->initContext();
    
      
      $db = Zend_Db_Table::getDefaultAdapter();
      
      if( $type!=null && $type=='search' ){
        
        //subject not in fsisub
        $select2 = $db->select()
          ->from(array('sm'=>'tbl_subjectmaster'))
          //->joinLeft(array('fsisub'=>'fee_structure_item_subject'),'fsisub.fsisub_subject_id = sm.IdSubject')
          //->where('fsisub.fsisub_subject_id is null')
          ->where('sm.active = 1')
          ->where('sm.SubCode like ?', "%".$search."%")
          ->order('sm.ShortName  ASC');
         
        $stmt = $db->query($select2);
        $subjectList = $stmt->fetchAll();
        $result['subjectList'] = $subjectList;
        
      }else{
        
        //subject in fsisub
        $select = $db->select()
        ->from(array('fsisub'=>'fee_structure_item_subject'))
        ->join(array('sm'=>'tbl_subjectmaster'), 'sm.IdSubject = fsisub.fsisub_subject_id')
        ->where('fsisub.fsisub_fsi_id = ?', $fsi_id)
        ->order('sm.ShortName  ASC');
         
        $stmt = $db->query($select);
        $feeItemSubjectList = $stmt->fetchAll();
        $result['fsisubList'] = $feeItemSubjectList;
        
      }
      
    
      $ajaxContext->addActionContext('view', 'html')
      ->addActionContext('form', 'html')
      ->addActionContext('process', 'json')
      ->initContext();
    
      $json = Zend_Json::encode($result);
    
      echo $json;
      exit();
    }
    
    public function copyDataAjaxAction(){
      
      $from_intake_id = $this->_getParam('from_intake_id', null);
      $from_type_id = $this->_getParam('from_type_id', null);
      $from_fs_id = $this->_getParam('from_fs_id', null);
      
      $this->_helper->layout->disableLayout();
      
      if ($this->getRequest()->isPost()) {
         
        $formData = $this->getRequest()->getPost();
        
        
        echo "<pre>";
        print_r($formData);
        echo "</pre>";
        
        //copy fee_structure
        $feeStructureDb               = new Studentfinance_Model_DbTable_FeeStructure();
        $feeStructureProgramDb        = new Studentfinance_Model_DbTable_FeeStructureProgram();
        $feeStructureItemDb           = new Studentfinance_Model_DbTable_FeeStructureItem();
        $feeStructureItemSemesterDb   = new Studentfinance_Model_DbTable_FeeStructureItemSemester();
        $feeStructureItemSubjectDb     = new Studentfinance_Model_DbTable_FeeStructureItemSubject();
        
        $select = $feeStructureDb->select()
                  ->where('fs_intake_start = ?', $formData['from_intake_id'])
                  ->where('fs_student_category = ?', $formData['from_type_id']);
        
        if($formData['from_fs_id']!=""){
          $select->where('fs_id = ?',$formData['from_fs_id']);
        }
        
        $fs_list = $feeStructureDb->fetchAll($select)->toArray();
        
        
        /*
         * start copy
         */
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->beginTransaction();
        
        try{
          
          for($i=0; $i<sizeof($fs_list);$i++){
            
            $fs = $fs_list[$i];
            
            //copy fs
            $fs_data = $fs;
            
            unset($fs_data['fs_id']);
            $fs_data['fs_intake_start'] = $formData['to_intake_id'];
            $fs_data['fs_student_category'] = $formData['to_type_id'];
            
            $cp_fs_id = $feeStructureDb->insert($fs_data);
            
            
            //copy fee_structure_program
            $select = $feeStructureProgramDb->select()
                       ->where('fsp_fs_id = ?', $fs['fs_id']);
            
            $fsp_arr = $feeStructureProgramDb->fetchAll($select)->toArray();
            
            if($fsp_arr){

              foreach ($fsp_arr as $fsp){
                $fsp_data = $fsp;
                
                unset($fsp_data['fsp_id']);
                $fsp_data['fsp_fs_id'] = $cp_fs_id;
         
                $cp_fsp_id = $feeStructureProgramDb->insert($fsp_data);
              }
            }
            
            //copy fee_structure_item
            $select = $feeStructureItemDb->select()
                      ->where('fsi_structure_id = ?', $fs['fs_id']);
            
            $fsi_arr = $feeStructureItemDb->fetchAll($select)->toArray();
            
            if($fsi_arr){

              foreach ($fsi_arr as $fsi){
                $fsi_data = $fsi;
            
                unset($fsi_data['fsi_id']);
                $fsi_data['fsi_structure_id'] = $cp_fs_id;
                
                $cp_fsi_id = $feeStructureItemDb->insert($fsi_data);
                
                
                    //copy fee_structure_item_semester
                    $select_sm = $feeStructureItemSemesterDb->select()
                                ->where('fsis_item_id = ?', $fsi['fsi_id']);
                    
                    $fsis_arr = $feeStructureItemSemesterDb->fetchAll($select_sm)->toArray();
                    
                    if($fsis_arr){
                      foreach ($fsis_arr as $fsis){
                        $fsis_data = $fsis;
                    
                        unset($fsis_data['fsis_id']);
                        $fsis_data['fsis_item_id'] = $cp_fsi_id;
                    
                        $cp_fsis_id = $feeStructureItemSemesterDb->insert($fsis_data);
                      }
                    }
                    
                    //copy fee_structure_item_subject
                    $select_sub = $feeStructureItemSubjectDb->select()
                    ->where('fsisub_fsi_id = ?', $fsi['fsi_id']);
                    
                    $fsisub_arr = $feeStructureItemSubjectDb->fetchAll($select_sub)->toArray();
                    
                    if($fsisub_arr){
                      foreach ($fsisub_arr as $fsisub){
                        $fsisub_data = $fsisub;
                    
                        unset($fsisub_data['fsisub_id']);
                        $fsisub_data['fsisub_fsi_id'] = $cp_fsi_id;
                    
                        $cp_fsisb_id = $feeStructureItemSubjectDb->insert($fsisub_data);
                      }
                    }
              }
            }
            
          }
          
          
          $db->commit();
          
        
         
        }catch (Exception $e){
          $db->rollBack();
          echo $e->getMessage();
          echo "<pre>";
          var_dump($e->getTrace());
          echo "</pre>";
          echo "<br />";
        
          throw $e;
        
        }      
        
        //redirect
        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure'),'default',true));
      }
      
      
      
      //get fs list
      $db = Zend_Db_Table::getDefaultAdapter();
      $select = $db->select()
            ->from(array('fs'=>'fee_structure'))
            ->where('fs.fs_intake_start = ?', $from_intake_id)
            ->where('fs.fs_student_category = ?', $from_type_id)
            ->order('fs.fs_name  ASC');
       
      $stmt = $db->query($select);
      $fs_list = $stmt->fetchAll();
      
      $result['from_fs_list'] = $fs_list;
      
      //get intake list
      $db = Zend_Db_Table::getDefaultAdapter();
      $select = $db->select()
      ->from(array('ink'=>'tbl_intake'))
      //->where('ink.IdIntake != ?', $from_intake_id)
      ->order('ink.ApplicationStartDate  DESC');
       
      $stmt = $db->query($select);
      $intake_list = $stmt->fetchAll();
      $result['to_intake'] = $intake_list;
      
      
      
      $ajaxContext = $this->_helper->getHelper('AjaxContext');
      $ajaxContext->addActionContext('view', 'html');
      $ajaxContext->initContext();
      
      $ajaxContext->addActionContext('view', 'html')
      ->addActionContext('form', 'html')
      ->addActionContext('process', 'json')
      ->initContext();
      
      $json = Zend_Json::encode($result);
      
      echo $json;
      exit();
      
    }
	
	public function deleteItemAction()
	{
		$db = getDB();

		$id = $this->_getParam('id');
		$type = $this->_getParam('type');
		
		if ( $id == '' )
		{
			$data = array('msg' => 'Invalid ID');	
			echo Zend_Json::encode($data);
			exit;
		}

		if ( $type == 'program' )
		{
			$db->delete('fee_structure_program_item', 'fspi_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}

		if ( $type == 'optional' )
		{
			$db->delete('fee_structure_optional_item', 'fsoi_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
	}
	
	public function addOthersAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Add Others");
    	    	
    	    	
		$form = new Studentfinance_Form_FeeStructureOther();

		$auth = Zend_Auth::getInstance();
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();

			$data = array(
							'fso_program_id'		=> $formData['fso_program_id'],
							'fso_activity_id'		=> $formData['fso_activity_id'],
							'fso_trigger'			=> $formData['fso_trigger'],
							'fso_proforma'			=> $formData['fso_proforma'],
							'fso_fee_id'			=> $formData['fso_fee_id'],
							'fso_status'			=> $formData['fso_status'],
							'fso_created_by'		=> $auth->getIdentity()->iduser,
							'fso_created_date'		=> new Zend_Db_Expr('NOW()')
					);
		
			$this->_helper->flashMessenger->addMessage(array('success' => "Other Item Added"));
			$fsOtherDB = new Studentfinance_Model_DbTable_FeeStructureOther();
			$fsOtherDB->addData($data);

			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'other-list'),'default',true));	
		}
			
		
		//view
		$this->view->form = $form;
    }
    
	public function otherListAction(){
		
		//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Others");
    	
		$fsOtherDB = new Studentfinance_Model_DbTable_FeeStructureOther();
		$data = $fsOtherDB->getList();
		$this->view->data = $data;

	}
	
	
	public function editOtherAction()
	{
		$this->view->title = $this->view->translate('Edit Other');
		
		$id = $this->_getParam('id');
		
		$fsOtherDB = new Studentfinance_Model_DbTable_FeeStructureOther();
		$info = $fsOtherDB->getData($id);
		$this->view->info = $info;

		$form = new Studentfinance_Form_FeeStructureOther();
		$form->populate($info);

		$auth = Zend_Auth::getInstance();
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();

			$data = array(
							'fso_program_id'		=> $formData['fso_program_id'],
							'fso_activity_id'		=> $formData['fso_activity_id'],
							'fso_trigger'			=> $formData['fso_trigger'],
							'fso_proforma'			=> $formData['fso_proforma'],
							'fso_fee_id'			=> $formData['fso_fee_id'],
							'fso_status'			=> $formData['fso_status'],
							'fso_created_by'		=> $auth->getIdentity()->iduser,
							'fso_created_date'		=> new Zend_Db_Expr('NOW()')
					);
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			$fsOtherDB = new Studentfinance_Model_DbTable_FeeStructureOther();
			$fsOtherDB->updateData($data, $info['fso_id']);

			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'other-list'),'default',true));	
		}
			
		
		//view
		$this->view->form = $form;
	}

	public function editCourseAction(){
        $fs_id = $this->_getParam('fs_id');
        $this->view->fs_id = $fs_id;

        $fsi_id = $this->_getParam('fsi_id');
        $this->view->fsi_id = $fsi_id;

        if ($this->getRequest()->isXmlHttpRequest()){
            $this->_helper->layout->disableLayout();
        }

        $model = new Studentfinance_Model_DbTable_ItemCourse();
        $definationDb = new App_Model_General_DbTable_Definationms();

        $itemCourseList = $model->getItemCourse($fsi_id);
        $this->view->itemCourseList = $itemCourseList;

        $calculationTypes = $definationDb->getDataByType(81);
        $this->view->calculationTypes = $calculationTypes;
    }

    public function saveCourseAction(){
        if ($this->getRequest()->isPost()) {
            $model = new Studentfinance_Model_DbTable_ItemCourse();

            $formData = $this->getRequest()->getPost();

            $data = array(
                'fsisub_fsi_id'=>$formData['fsi_id'],
                'fsisub_subject_id'=>$formData['fsisub_subject_id'],
                'fsisub_subject_amount'=>$formData['fsisub_subject_amount'],
                'fsisub_amount_calculation_type'=>$formData['fsisub_amount_calculation_type'],
            );
            $model->addCourse($data);

            //redirect
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'detail', 'id'=>$formData['fs_id']),'default',true));
        }
    }

    public function deleteCourseAction(){
        $id = $this->_getParam('id', 0);

        $model = new Studentfinance_Model_DbTable_ItemCourse();
        $model->deleteCourse($id);

        $data = array('msg' => '');

        echo Zend_Json::encode($data);

        exit;
    }

    public function findSubjectAction(){
        $searchElement = $this->_getParam('term', '');

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $model = new Studentfinance_Model_DbTable_ItemCourse();
        $subjects = $model->getSubject($searchElement);

        $subsArray = array();

        if ($subjects){
            foreach ($subjects as $subject){
                $subject['label'] = $subject['SubCode'].' - '.$subject['SubjectName'];
                $subject['value'] = $subject['SubCode'].' - '.$subject['SubjectName'];
                $subsArray[] = $subject;
            }
        }

        $json = Zend_Json::encode($subsArray);

        echo $json;
        exit();
    }
}

