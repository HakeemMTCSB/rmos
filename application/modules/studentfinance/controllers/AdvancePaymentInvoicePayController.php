<?php
/**
 *  @author alif 
 *  @date Jun 26, 2013
 */
 
class Studentfinance_AdvancePaymentInvoicePayController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
		$this->_DbObj = $db;
	}
	
	public function indexAction(){
		//title
		$this->view->title= $this->view->translate("Advance Payment: Invoice Payment");
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			
			$this->view->search = $formData;
			
			$advPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
			
			$list = $advPaymentDb->getSearchData($formData);
			$this->view->receipt_list = $list;
		}
		
	}
	
	public function paymentAction(){
		//title
		$this->view->title= $this->view->translate("Advance Payment: Invoice Payment");
		
		$appl_id = $this->_getParam('id', null);
		
		$advPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
		
		$msg = $this->_getParam('msg', null);
		$this->view->noticeMessage = $msg;
		
		$step = $this->_getParam('step', 1);
		$this->view->step = $step;
		
		//set custom session
		$ses_inv_payment = new Zend_Session_Namespace('ad_invoice_payment');
		
		if($appl_id){
			Zend_Session::namespaceUnset('ad_invoice_payment');
				
			$ses_inv_payment = new Zend_Session_Namespace('ad_invoice_payment');
			$ses_inv_payment->appl_id = $appl_id;
				
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay', 'action'=>'payment','step'=>1),'default',true));
		}
		
		$transData = $advPaymentDb->getData($ses_inv_payment->appl_id);
		$transId = $transData['advpy_idStudentRegistration']? $transData['advpy_idStudentRegistration']: $transData['advpy_trans_id'];
		
		$transType = $transData['advpy_idStudentRegistration']? 2: 1;
		$this->view->profile = $transData;
		
		$this->view->advancePaymentList = array($transData);
		
		$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
		$this->view->currencyrate = $currencyRateDb->getCurrentExchangeRate(2);
		$db = Zend_Db_Table::getDefaultAdapter();
		if( isset($ses_inv_payment->appl_id) ){
			/*if($step==1){
		
				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
						
					if( isset($formData['invoices'])){
						$ses_inv_payment->invoice = $formData['invoices'];
						$ses_inv_payment->invoice_knockoff = null;
					}else{
						$ses_inv_payment->invoice = null;
						$ses_inv_payment->invoice_knockoff = null;
					}
						
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay', 'action'=>'payment','step'=>2),'default',true));
						
				}else{
					//active invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceList = $invoiceMainDb->getApplicantInvoiceData($transId,true);
				
					if($invoiceList){
				
						//remove paid invoice
						foreach ($invoiceList as $index=>$invoice){
								
							if($invoice['balance']<=0 && $invoice['balance']!=null){
								unset($invoiceList[$index]);
							}
						}
						$invoiceList = array_values($invoiceList);
				
						//remove invoice with credit note
						$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
						foreach ($invoiceList as $index=>$invoice){
							if($creditNoteDb->getDataByInvoice($invoice['id'],true)){
								unset($invoiceList[$index]);
							}
						}
						$invoiceList = array_values($invoiceList);
				
					}
				
					if(isset($ses_inv_payment->invoice)){
							
						//set checked from data session
							
				
						foreach ($invoiceList as $index=>$invoice){
								
							if(in_array($invoice['id'],array_values($ses_inv_payment->invoice))){
								$invoiceList[$index]['selected'] = true;
							}else{
								$invoiceList[$index]['selected'] = false;
							}
						}
					}
						
					$this->view->invoice_list = $invoiceList;
				
				}
				
			}else*/
			if($step==1){
				
				$select_proforma_invoice = $db->select()
					->from(array('pi'=>'proforma_invoice_main'))
					->joinRight(array('pid'=>'proforma_invoice_detail'),'pid.proforma_invoice_main_id = pi.id', array('fee_item_description'=>'pid.fee_item_description'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = pi.currency_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where('pi.invoice_id = 0')
					->where("pi.status IN ('A','W')")  
					->group('pid.proforma_invoice_main_id')
					->where('pi.trans_id = ?', $transData['advpy_trans_id']);
//					->orWhere('pi.IdStudentRegistration= ?', $transId);//to check proforma already generate to invoice;
					
					$proforma_invoice = $db->fetchAll($select_proforma_invoice);
					$this->view->proforma = $proforma_invoice;

				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					
					if( isset($formData['invoice']) ){
						$invoice_arr = array();
						
						foreach ($formData['invoice'] as $index => $invoice){
							$invoice_arr[] = array(
									'invoice_no'=>$invoice,
									'adv_amt'=> $formData['invoice_adv_pymt_amt'][$index],
									'inv_cur_id'=> $formData['cur_id'][$index]
							);
						}
						$ses_inv_payment->invoice = $formData['invoice'];
						$ses_inv_payment->invoice_knockoff = $invoice_arr;
						$ses_inv_payment->distribution = null;
					}else{
						$ses_inv_payment->invoice_knockoff = null;
					}
					
					
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay', 'action'=>'payment','step'=>2),'default',true));
				}else{
					
					//active invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceList = $invoiceMainDb->getAllDataInvoice($transId,true,$transType);

					if($invoiceList){
				
						//remove paid invoice
						foreach ($invoiceList as $index=>$invoice){

							$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
							$balance = $paModel->getBalanceDtl($invoice['idDetail']);
							$balance2 = $paModel->getBalanceMain($invoice['idMain']);

							if(($balance['bill_balance'] <= 0.00 || $balance2['bill_balance'] <= 0.00) && $balance['bill_balance'] != null){
								unset($invoiceList[$index]);
							}else{
								$invoiceList[$index]['balance']=str_replace(',', '',$balance['bill_balance']);
								$invoiceList[$index]['paid']=str_replace(',', '',$balance['bill_paid']);
							}
						}
						$invoiceList = array_values($invoiceList);
				
						//remove invoice with credit note
						/*$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
						foreach ($invoiceList as $index=>$invoice){
							if($creditNoteDb->getDataByInvoice($invoice['id'],true,$invoice['idDetail'])){
								unset($invoiceList[$index]);
							}
						}*/
						$invoiceList = array_values($invoiceList);
				
					}
				
					if(isset($ses_inv_payment->invoice)){
							
						//set checked from data session
							
				
						foreach ($invoiceList as $index=>$invoice){
								
							if(in_array($invoice['id'],array_values($ses_inv_payment->invoice))){
								$invoiceList[$index]['selected'] = true;
							}else{
								$invoiceList[$index]['selected'] = false;
							}
						}
					}
					$this->view->invoice_list = $invoiceList;
					
					//get advance payment
					$advPmntDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advancePaymentList = $advPmntDb->getBalanceAvdPayment($ses_inv_payment->appl_id);
						
					$this->view->advancePaymentList = $advancePaymentList;
					
					$advance_payment_balance = 0;
					if($advancePaymentList){
						foreach ($advancePaymentList as $avd_payment){
							$advance_payment_balance += $avd_payment['advpy_total_balance'];
						}
					}
					$this->view->advance_payment_balance = $advance_payment_balance;
					
				}
			}else
			if($step==2){
			
				$this->view->appl_id = $ses_inv_payment->appl_id;
				
//				echo "<pre>";
//				print_r($ses_inv_payment->invoice_knockoff);
//				exit;
				
				if( isset($ses_inv_payment->invoice) ){
						
					//invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
						
					$invoiceList = array();
					/*foreach ($ses_inv_payment->invoice as $index=>$invoice_id){
						$invoiceList[] = $invoiceMainDb->getInvoiceData($invoice_id);
					}*/
					//invoice detail
					foreach ($invoiceList as $index => $invoice){
						$invoiceList[$index] = $invoiceDetailDb->getInvoiceDetailData($invoice['id']);
					}
						
						
					//get advance payment
					$advPmntDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advancePaymentList = $advPmntDb->getBalanceAvdPayment($ses_inv_payment->appl_id);
						
					$this->view->advancePaymentList = $advancePaymentList;
						
					$advance_payment_balance = 0;
					if($advancePaymentList){
						foreach ($advancePaymentList as $avd_payment){
							$advance_payment_balance += $avd_payment['advpy_total_balance'];
						}
					}
					$this->view->advance_payment_balance = $advance_payment_balance;
						
					if( isset($ses_inv_payment->invoice_knockoff) ){
				
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
				
						$invoice_knockoff_list = array();
						foreach ($ses_inv_payment->invoice_knockoff as $index =>$invoice_knockoff){
							$invoice_knockoff_list[$index] = $invoice_knockoff;
							$invoice_knockoff_list[$index]['invoice_detail'] = $invoiceMainDb->getInvoiceDetailData($invoice_knockoff['invoice_no'],true);
							
						}
				
						$this->view->invoice_knockoff = $invoice_knockoff_list;
					}
						
					//fee item distribution
					//get fee item setup
					$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
					$feeItemList = $feeItemDb->getData();
						
					//initial set fee item to paid according to invoice paid amount
//					foreach ($invoiceList as $index => $invoice){
						
				
						foreach ($invoiceList as $fee_item_index => $invoice_detail){
							$invoice_paid_amount = $invoice_detail['paid'];
							if( $invoice_paid_amount > $invoice_detail['amount'] ){
								$invoiceList[$index][$fee_item_index]['paid'] = $invoice_detail['amount'];
								$invoiceList[$index][$fee_item_index]['balance'] = 0;
								$invoice_paid_amount = $invoice_paid_amount - $invoice_detail['amount'];
							}else{
								$invoiceList[$index][$fee_item_index]['paid'] = $invoice_paid_amount;
								$invoiceList[$index][$fee_item_index]['balance'] = $invoice_detail['amount'] - $invoice_paid_amount;
								$invoice_paid_amount = $invoice_paid_amount - $invoice_paid_amount;
							}
						}
//					}
						
						
				
					//invoice knockoff using advance payment
					if( isset($ses_inv_payment->invoice_knockoff) ){
						foreach ($invoiceList as $index => $invoice){
								
							foreach ($ses_inv_payment->invoice_knockoff as $invoice_knockoff){
				
								if($invoice['id'] == $invoice_knockoff['invoice_no']){
										
									//calculate fee item amount balance
									$adv_pymt_paid_amount = $invoice_knockoff['adv_amt'];
//									foreach ($invoice['invoice_detail'] as $fee_item_index => $invoice_detail){
										if( $adv_pymt_paid_amount > $invoice['amount'] ){
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $invoice['amount'];
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = 0;
											$adv_pymt_paid_amount = $adv_pymt_paid_amount - $invoice['amount'];
										}else{
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['paid'] = $adv_pymt_paid_amount;
											$invoiceList[$index]['invoice_detail'][$fee_item_index]['balance'] = $invoice['amount'] - $adv_pymt_paid_amount;
											$adv_pymt_paid_amount = $adv_pymt_paid_amount - $adv_pymt_paid_amount;
										}
//									}
								}
							}
						}
					}
						
					//loop each invoice & calculate and inject array with fee item amount
//					foreach ($invoiceList as $index => $invoice){
						foreach ($invoiceList as $inv_dtl){
							foreach($feeItemList as $feeItemIndex => $feeItem){
								if($feeItem['fi_id']==$inv_dtl['fi_id']){
									if(isset($feeItemList[$feeItemIndex]['total_amount'])){
										$feeItemList[$feeItemIndex]['total_amount'] += $inv_dtl['balance'];
									}else{
										$feeItemList[$feeItemIndex]['total_amount'] = $inv_dtl['balance'];
									}
									break;
								}
							}
						}
//					}
						
						
					$this->view->invoice_list = $invoiceList;
					$this->view->fee_item = $feeItemList;
				
					
						
				}else{
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay', 'action'=>'payment','step'=>1, 'msg'=>'Please Complete step 1 process'),'default',true));
				}
			
			}else
			if($step=="confirm"){
				if ($this->getRequest()->isPost()) {
					//data from session
					$formData = $this->getRequest()->getPost();
					dd($formData); //exit;
					
					$appl_id = $ses_inv_payment->appl_id;
					$invoice = $ses_inv_payment->invoice;
					$invoice_knockoff = $ses_inv_payment->invoice_knockoff;
					
					//invoice detail
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
						
					foreach ($invoice as $index=>$item){
						$invData = $invoiceMainDb->getInvoiceDetailData($item);
						$invoice[$index] = $invData;
						
						$name = "bal_".$item;
						$invoice[$index]['balance'] = $formData[$name];
						
						$name2 = "paid_".$item;
						$invoice[$index]['paid'] = $invData['paid']+$formData[$name2];

						$name3 = "gain_".$item;
						$invoice[$index]['gain'] = $formData[$name3];

						$name4 = "loss_".$item;
						$invoice[$index]['loss'] = $formData[$name4];
					}

					//dd($invoice); exit;
						
					//advance payment record
					$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					$advance_payment_list = $advancePaymentDb->getBalanceAvdPayment($appl_id);
						
					$advance_payment = array();
					$advance_payment['record'] = $advance_payment_list;
					$advance_payment['total_amount'] = 0;
					$rcpId = $advance_payment_list[0]['advpy_rcp_id'];
						
					foreach ($advance_payment_list as $adv_pymt){
						$advance_payment['total_amount'] += $adv_pymt['advpy_total_balance'];
					}
					
					
					/*echo "<pre>";
					print_r($advance_payment_list);
					echo "</pre>";
					
					echo "<pre>";
					 print_r($invoice_knockoff);
					echo "</pre>";*/
//					exit;
					
						
					$auth = Zend_Auth::getInstance();
						
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->beginTransaction();
						
					try {
						/*
						 * process invoice knock-off from advance payment
						*/
						$adv_pymt_amount = $advance_payment['total_amount'];
						$adv_pymt_list = $advance_payment['record'];
						
//						echo "<pre>";
//						print_r($invoice);
//						echo "</pre>";
//						exit;
					
						$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
						$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
						$dataCurrency = $currencyRateDb->getCurrentExchangeRate(2); //usd

						//dd($invoice);
						//loop each invoice.
						foreach ($invoice as $index=>$inv){
							//dd($invoice_knockoff);exit;
							//loop each knockoff invoice
							foreach($invoice_knockoff as $inv_knockoff){

								if($inv_knockoff['invoice_no'] == $inv['id']){
									
									echo "<hr />";
									echo $inv['bill_number'];
									
									//knockoff amout it the amount that must be 0
									$knockoff_amount_adv = $inv_knockoff['adv_amt'];
									$knockoff_amount = $inv['balance'];
									echo "<br /><br />Invoice Balance xx : ".$inv['balance'];	
//									$currencyIdInv =  $inv_knockoff['inv_cur_id'];
//									$currencyIdAdv = $advance_payment_list[0]['advpy_cur_id'];
									
									/*if($currencyIdInv != $currencyIdAdv){
										$exchangeRate = $dataCurrency['cr_exchange_rate'];
										if($currencyIdInv == 2){
											$inv_amount = $inv_amount / $exchangeRate;
										}else{
											$inv_amount = $inv_amount * $exchangeRate;
										}
									}*/
									
									//while($knockoff_amount_adv>0){
									
										//loop each advance payment
										for($i=0; $i<sizeof($adv_pymt_list) && $knockoff_amount_adv>0 ;$i++){
											
											
											$curr_advance_payment = $adv_pymt_list[$i];
											
											
											/*
											 Previously we have cater this in advance payment process.
											this is to ensure advance payment amount is sufficient to knockoff the invoice
											*/
											
											//check that advance payment balance is not zero
											if( $curr_advance_payment['advpy_total_balance'] > 0 ){
												
												echo "<br /><br /> Advance Payment Amount(".$curr_advance_payment['advpy_id'].") : ".$curr_advance_payment['advpy_total_balance']." ++ ".$knockoff_amount_adv." inv ".$knockoff_amount;
												
												//adv payment is more than and equal knockoff value
												if( $curr_advance_payment['advpy_total_balance'] >= $knockoff_amount ){
													
													echo "<br /><br /> adv >= inv";	
													
													$amount_use = $knockoff_amount_adv;

													$adv_pymt_list[$i]['advpy_total_balance'] = $adv_pymt_list[$i]['advpy_total_balance'] - $amount_use;
														
													//insert advance payment detail record
													$data = array(
															'advpydet_advpy_id' => $curr_advance_payment['advpy_id'],
															'advpydet_inv_det_id' => $inv_knockoff['invoice_no'],
															'advpydet_inv_id' => $inv['invoice_main_id'],
															'advpydet_total_paid' => $amount_use,
															'advpydet_updby' => $auth->getIdentity()->id,
															'advpydet_upddate' => date ( 'Y-m-d H:i:s' ),
															'gain' => $inv['gain'],
															'loss' => $inv['loss'],
													);
													echo "<pre>";
													print_r($data);
													echo "</pre>";
													$advancePaymentDetailDb->insert($data);
														
														
													//update advance payment main record for amount used and balance remaining
													$data = array(
															'advpy_total_paid' => $curr_advance_payment['advpy_total_paid']+$amount_use,
															'advpy_total_balance' => $curr_advance_payment['advpy_total_balance']-$amount_use,
													);
													echo "<pre>";
													print_r($data);
													echo "</pre>";
													$curr_advance_payment['advpy_total_paid']+=$amount_use;
													$curr_advance_payment['advpy_total_balance']-=$amount_use;
													
													$adv_pymt_list[$i]['advpy_total_paid'] += $amount_use;
													//$adv_pymt_list[$i]['advpy_total_balance'] -= $amount_use;
													
													$advancePaymentDb->update($data, 'advpy_id = '.$curr_advance_payment['advpy_id']);
														
														
								
													//update invoice detail
													$data = array(
															'paid' => $invoice[$index]['paid'],
															'balance' => $invoice[$index]['balance'],
													);
													
													$invoiceDetailDb->update($data, 'id = '.$inv['id']);
													
													echo "<pre>";
													print_r($data);
													echo "</pre>";
													
													//update invoice
													$invMain = $invoiceMainDb->getData($inv['invoice_main_id']);
					
													$data = array(
															'bill_paid' => $invMain['bill_paid']+$invoice[$index]['paid'],
															'bill_balance' => $invMain['bill_balance']-$invoice[$index]['paid'],
															'upd_by' => $auth->getIdentity()->id,
															'upd_date' => date ( 'Y-m-d H:i:s' ),
													);
													
													echo "<pre>";
													print_r($data);
													echo "</pre>";
													
													$invoice[$index]['paid'] += $amount_use;
													$invoice[$index]['balance'] -+ $amount_use;
													$invoiceMainDb->update($data, 'id = '.$inv['invoice_main_id']);
														
													$knockoff_amount -= $amount_use;
													
													echo "<br /> Invoice Amount Deducted 1: ".$knockoff_amount;
														
													break 1;
					
												}else{
													echo "<br /><br /> adv <= inv";	
													
													//advance payment is not sufficient to knockoff invoice balance
													$adv_amount_use = $adv_pymt_list[$i]['advpy_total_balance'];
														
													$adv_pymt_list[$i]['advpy_total_balance'] = $adv_pymt_list[$i]['advpy_total_balance'] - $adv_amount_use;
														
													//insert advance payment detail record
													$data = array(
															'advpydet_advpy_id' => $curr_advance_payment['advpy_id'],
															'advpydet_inv_det_id' => $inv_knockoff['invoice_no'],
															'advpydet_inv_id' => $inv['invoice_main_id'],
															'advpydet_total_paid' => $adv_amount_use,
															'advpydet_updby' => $auth->getIdentity()->id,
															'advpydet_upddate' => date ( 'Y-m-d H:i:s' ),
															'gain' => $inv['gain'],
															'loss' => $inv['loss'],
													);
													$advancePaymentDetailDb->insert($data);
														
														
													//update advance payment main record for amount used and balance remaining
													$data = array(
															'advpy_total_paid' => $curr_advance_payment['advpy_total_paid']+$adv_amount_use,
															'advpy_total_balance' => $curr_advance_payment['advpy_total_balance']-$adv_amount_use,
													);
													echo "<pre>";
													print_r($data);
													echo "</pre>";
												
													$curr_advance_payment['advpy_total_paid']+=$adv_amount_use;
													$curr_advance_payment['advpy_total_balance']-=$adv_amount_use;
													
													$adv_pymt_list[$i]['advpy_total_paid']+=$adv_amount_use;
													//$adv_pymt_list[$i]['advpy_total_balance']-=$adv_amount_use;
													
													$advancePaymentDb->update($data, 'advpy_id = '.$curr_advance_payment['advpy_id']);
														
													//update invoice detail
													$data = array(
															'paid' => $invoice[$index]['paid'],
															'balance' => $invoice[$index]['balance'],
													);
													
													echo "<pre>";
													print_r($data);
													echo "</pre>";
													
													$invoiceDetailDb->update($data, 'id = '.$inv['id']);
													
													//update invoice
													$invMain = $invoiceMainDb->getData($inv['invoice_main_id']);
													$data = array(
															'bill_paid' => $invMain['bill_paid']+$invoice[$index]['paid'],
															'bill_balance' => $invMain['bill_balance']-$invoice[$index]['paid'],
															'upd_by' => $auth->getIdentity()->id,
															'upd_date' => date ( 'Y-m-d H:i:s' ),
													);
													
													$invoice[$index]['paid'] += $adv_amount_use;
													$invoice[$index]['balance'] -= $adv_amount_use;
													$invoiceMainDb->update($data, 'id = '.$inv['invoice_main_id']);
													
													
													echo "<pre>";
													print_r($data);
													echo "</pre>";
														
													$knockoff_amount_adv -= $adv_amount_use;
													
													echo "<br /> Invoice Amount Deducted 2: ".$adv_amount_use;
														
													break 1;	
												}
												
											}//endif
				
											
						
												
										}//end loop each advance payment
										
//										exit;
									//} //end while
								}
								
								
							}
							//end loop invoice
							
							
						}
//						exit; 

						
						
						
						$db->commit();
						
						$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Invoice Payment Successful');
						
						
					}catch (Exception $e) {
						echo "Error in Advance Payment Invoice Pay. <br />";
						echo $e->getMessage();
						
						echo "<pre>";
						print_r($e->getTrace());
						echo "</pre>";
						
						$db->rollBack();
		    			
		    			exit;
		    			
					}
					
					/*
							 * Add trigger
							 * 1) document checsklist status
							 * 2) update course status
							 * 
							 * */
						
							$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
							$paymentClass->updateChecklistDocument($rcpId);
							foreach ($invoice as $index=>$inv){
								$paymentClass->updateCourseStatus($inv['invoice_main_id']);
							}
//					exit;
					
					//redirect
						$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay', 'action'=>'index'),'default',true));
						
				}
			}else{
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay'),'default',true));
			}
		}else{
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay'),'default',true));
		}
		
	}
}
 ?>