<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/3/2016
 * Time: 10:09 AM
 */
class Studentfinance_SponsorshipTaggingReportController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_SponsorTaggingReport();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Sponsorship Tagging Report');

        $form = new Studentfinance_Form_SponsorTaggingReport();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);

            $list = array();

            if (isset($formData['sponsorship']) && count($formData['sponsorship']) > 0){
                foreach ($formData['sponsorship'] as $key => $scholarship){
                    $scholarshipInfo = $this->model->getSponsorship($scholarship);

                    $scholarStudent = $this->model->getStudentTag($scholarship);

                    if ($scholarshipInfo){
                        $list[$key]['scholarinfo'] = $scholarshipInfo;
                    }

                    if ($scholarshipInfo){
                        $list[$key]['scholarstudent'] = $scholarStudent;
                    }
                }
            }

            $this->view->list = $list;
        }
    }

    public function printAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = array();

            if (isset($formData['sponsorship']) && count($formData['sponsorship']) > 0){
                foreach ($formData['sponsorship'] as $key => $scholarship){
                    $scholarshipInfo = $this->model->getSponsorship($scholarship);

                    $scholarStudent = $this->model->getStudentTag($scholarship);

                    if ($scholarshipInfo){
                        $list[$key]['scholarinfo'] = $scholarshipInfo;
                    }

                    if ($scholarshipInfo){
                        $list[$key]['scholarstudent'] = $scholarStudent;
                    }
                }
            }

            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_sponsor_tagging_report.xls';
    }

    public function scholarshipTaggingReportAction(){
        $this->view->title = $this->view->translate('Scholarship Tagging Report');

        $form = new Studentfinance_Form_SponsorTaggingReport();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);

            $list = array();

            if (isset($formData['scholarship']) && count($formData['scholarship']) > 0){
                foreach ($formData['scholarship'] as $key => $scholarship){
                    $scholarshipInfo = $this->model->getScholarship($scholarship);

                    $scholarStudent = $this->model->getStudentTag2($scholarship);

                    if ($scholarshipInfo){
                        $list[$key]['scholarinfo'] = $scholarshipInfo;
                    }

                    if ($scholarshipInfo){
                        $list[$key]['scholarstudent'] = $scholarStudent;
                    }
                }
            }

            $this->view->list = $list;
        }
    }

    public function printScholarTagReportAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = array();

            if (isset($formData['scholarship']) && count($formData['scholarship']) > 0){
                foreach ($formData['scholarship'] as $key => $scholarship){
                    $scholarshipInfo = $this->model->getScholarship($scholarship);

                    $scholarStudent = $this->model->getStudentTag2($scholarship);

                    if ($scholarshipInfo){
                        $list[$key]['scholarinfo'] = $scholarshipInfo;
                    }

                    if ($scholarshipInfo){
                        $list[$key]['scholarstudent'] = $scholarStudent;
                    }
                }
            }

            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_scholar_tagging_report.xls';
    }
}