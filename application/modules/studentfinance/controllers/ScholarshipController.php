<?php

/* 
 * Scholarship controller
 * 
 * @on 13/6/2014
 * @authur izham
 */

class Studentfinance_ScholarshipController extends Zend_Controller_Action {
    private $_gobjlog;
    private $locale;
    private $registry;
    private $auth;
    private $scholarshipModel;
    private $intakeModel;
    private $defModel;
    private $scholarshipTagModel;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->loadObj();
    }
    
    public function loadObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->gobjsessionsis = Zend_Registry::get('sis');

        $this->scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $this->intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->scholarshipTagModel = new Studentfinance_Model_DbTable_ScholarshipTagging();
        $this->SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();

        $this->SponsorModel = new Studentfinance_Model_DbTable_Sponsor();
        $this->SponsorApplicationForm = new Studentfinance_Form_SponsorApplication();

        $this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();

        $this->locales = $this->view->locales = array(
            'en_US'	=> 'English',
            'ms_MY'	=> 'Malay'
        );
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate("Scholarship Types");
        $scholarshipList = $this->scholarshipModel->getScholarshipList();
        $intakeList = $this->intakeModel->fngetIntakeList();

        $this->view->scholarshipList = $scholarshipList;
        $this->view->intakeList = $intakeList;
    }
    
    public function addScholarshipAction(){
        $addScholarshipForm = new Studentfinance_Form_AddScholarship();

        $addScholarshipForm->Add->setAttrib('onclick','remarkValidation();');

        $this->view->title = $this->view->translate("Add Scholarship");    
        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true);

        $getUserIdentity = $this->auth->getIdentity();

        //Fee Item
        $feeList = $this->lobjFeeItem->getData();
        foreach( $feeList as $item){
            $addScholarshipForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name']);
        }

        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            if ($addScholarshipForm->isValid($formData)){
                $data = array(
                    'sch_code' => $formData['sch_code'],
                    'sch_name' => $formData['sch_name'],
                    'sch_malayName' => $formData['sch_malayName'],
                    'sch_desc' => $formData['sch_desc'],
                    'sch_amount' => $formData['sch_amount'],
                    'sch_addDate' => date('Y-m-d H:i:s'),
                    'sch_addBy' => $getUserIdentity->id
                );

                //$this->scholarshipModel->insert($data);

                $db = getDB();
                $db->insert('tbl_scholarship_sch', $data);
                $sponsor_id = $db->lastInsertId();

                // Fee Info
                if($formData['FeeCodeGrid']){
                    $check = count($formData['FeeCodeGrid']);
                    
                    for($i=0;$i<$check;$i++){
                        $paramArray = array(
                            'sct_Id' => $sponsor_id,
                            'FeeCode' => $formData['FeeCodeGrid'][$i],
                            'CalculationMode' => $formData['CalculationModeGrid'][$i],
                            'Amount' => $formData['AmountGrid'][$i],
                            'Repeat' => $formData['RepeatGrid'][$i],
                            'MaxRepeat' => $formData['MaxRepeatGrid'][$i],
                            'FreqMode' => $formData['FreqModeGrid'][$i],
                        );
                        $db->insert('tbl_scholarship_fee_info',$paramArray);
                    }
                }

                //redirect
                $this->_redirect($this->view->backURL);
            }      
        }

        $this->view->addScholarshipForm = $addScholarshipForm;
    }
    
    public function editScholarshipAction(){
        $sch_id = $this->_getParam('id',null);
        $this->view->title = $this->view->translate("Edit Scholarship");
        $addScholarshipForm = new Studentfinance_Form_AddScholarship();
        $addScholarshipForm->Add->setAttrib('onclick','remarkValidation();');

        $scholarshipInfo = $this->scholarshipModel->getScholarshipById($sch_id);
        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true);

        //Fee Item
        $feeList = $this->lobjFeeItem->getData();
        foreach( $feeList as $item){
            $addScholarshipForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name']);
        }
		
        $getFeeInfo = $this->scholarshipModel->getFeeInfo($sch_id);
        $this->view->feeinfo = $getFeeInfo;

        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($addScholarshipForm->isValid($formData)){
                $db = getDB();
                
                $data = array(
                    'sch_code' => $formData['sch_code'],
                    'sch_name' => $formData['sch_name'],
                    'sch_malayName' => $formData['sch_malayName'],
                    'sch_desc' => $formData['sch_desc'],
                    'sch_amount' => $formData['sch_amount'],
                    'sch_modDate' => date('Y-m-d H:i:s'),
                    'sch_modBy' => $getUserIdentity->id
                );
                $db->update('tbl_scholarship_sch', $data, "sch_Id = ".$sch_id);

                // Fee Info
                if($formData['FeeCodeGrid']){
                    $check = count($formData['FeeCodeGrid']);
                    for($i=0;$i<$check;$i++){
                        $paramArray = array(
                            'sct_Id' => $sch_id,
                            'FeeCode' => $formData['FeeCodeGrid'][$i],
                            'CalculationMode' => $formData['CalculationModeGrid'][$i],
                            'Amount' => $formData['AmountGrid'][$i],
                            'Repeat' => $formData['RepeatGrid'][$i],
                            'MaxRepeat' => $formData['MaxRepeatGrid'][$i],
                            'FreqMode' => $formData['FreqModeGrid'][$i],
                        );
                        $db->insert('tbl_scholarship_fee_info',$paramArray);
                    }
                }
                
                //redirect
                $this->_redirect($this->view->backURL);
            }else{
                $addScholarshipForm->populate($scholarshipInfo);
            }
        }else{
            $addScholarshipForm->populate($scholarshipInfo);
        }
        
        $this->view->addScholarshipForm = $addScholarshipForm;
    }
    
    public function deleteFeeAction(){
        $db = getDB();

        $id = $this->_getParam('id');

        if ($id != ''){
            $db->delete('tbl_scholarship_fee_info', 'IdScholarshipFeeInfo = '.(int) $id);

            $data = array('msg' => '');

            echo Zend_Json::encode($data);
            exit;
        }else{
            $data = array('msg' => 'Invalid ID');

            echo Zend_Json::encode($data);
            exit;
        }
    }

    public function deleteScholarshipAction(){
        $sch_id = $this->_getParam('id',null);
        
        if ($sch_id){
            $this->scholarshipModel->delete("sch_Id = ".$sch_id);
        }
        
        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true);
        
        //redirect
        $this->_redirect($this->view->backURL);
    }
    
    public function scholarshipAction(){
        $intake_id = $this->_getParam('id',null);
        $intakeInfo = $this->scholarshipTagModel->getIntakeInfo($intake_id);
        $this->view->title = $this->view->translate("Scholarship Setup - ".$intakeInfo['IntakeDesc']);
        
        if ($intake_id==null){
            $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true);
            //redirect
            $this->_redirect($this->view->backURL);
        } else {
            $scholarshipTaggingList = $this->scholarshipTagModel->getScholarshipTagBasedOnIntake($intake_id);
            
            $i = 0;
        
            foreach($scholarshipTaggingList as $scholarshipTagloop){
                $mod_of_study = $this->defModel->getData($scholarshipTagloop['mode_of_study']);
                $mod_of_program = $this->defModel->getData($scholarshipTagloop['mode_of_program']);
                $student_category = $this->defModel->getData($scholarshipTagloop['sct_studentcategory']);
                $programType = $this->defModel->getData($scholarshipTagloop['program_type']);

                $scholarshipTaggingList[$i]['mos_name'] = $mod_of_study['DefinitionDesc'];
                $scholarshipTaggingList[$i]['mop_name'] = $mod_of_program['DefinitionDesc'];
                $scholarshipTaggingList[$i]['sc_name'] = $student_category['DefinitionDesc'];
                $scholarshipTaggingList[$i]['programType'] = $programType['DefinitionDesc'];

                $i++;
            }
        }
        
        $this->view->scholarshipTaggingList = $scholarshipTaggingList;
    }
    
    public function addScholarshipTaggingAction(){
        $intake_id = $this->_getParam('id',null);
        $this->view->title = $this->view->translate("Add Scholarship Tagging");
        $addScholarshipTaggingForm = new Studentfinance_Form_AddScholarshipTagging();
        $scholarshipList = $this->scholarshipModel->getScholarshipList();
        $intakeInfo = $this->scholarshipTagModel->getIntakeInfo($intake_id);
        
        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'scholarship'),'default',true).'/id/'.$intake_id;
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($addScholarshipTaggingForm->isValid($formData)) {
                
                if (count($formData['sch_Id'])>0){
                    foreach($formData['sch_Id'] as $schId){
                        $data = array(
                            'sct_schId' => $schId,
                            'sct_intake' => $intake_id,
                            'sct_program' => $formData['sct_program'],
                            'sct_studentcategory' => $formData['sct_studentcategory'],
                            'sct_programscheme' => $formData['sct_programscheme'],
                            'sct_desc' => $formData['sct_desc'],
                            'sct_status' => $formData['sct_status'],
                            'sct_startDate' => $formData['sct_startDate'],
                            'sct_endDate' => $formData['sct_endDate'],
                            'sct_startDateAccept' => $formData['sct_startDateAccept'],
                            'sct_endDateAccept' => $formData['sct_endDateAccept'],
                            'sct_addDate' => date('Y-m-d H:i:s'),
                            'sct_addBy' => $getUserIdentity->id
                        );

                        $this->scholarshipTagModel->insert($data);
                    }
                    
                    //redirect
                    $this->redirect($this->view->backURL);
                }else{
                    //$this->message =
                    $this->_helper->getHelper('FlashMessenger')->addMessage('Please Enter password');
                    
                    $this->view->redirectURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'add-scholarship-tagging'),'default',true).'/id/'.$intake_id;
                    
                    // Get messages
                    $flashMsgHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
                    $messages = $flashMsgHelper->getMessages();

                    /// Assign the messages
                    $this->view->assign('messages', $messages);
                    
                    $this->redirect($this->view->redirectURL);
                }
            }
        }
        
        $addScholarshipTaggingForm->populate($intakeInfo);
        
        $this->view->addScholarshipTaggingForm = $addScholarshipTaggingForm;
        $this->view->scholarshipList = $scholarshipList;
        $this->view->intake_id = $intake_id;
    }
    
    public function editScholarshipTaggingAction(){
        $sct_id = $this->_getParam('sct_id',null);
        $intake_id = $this->_getParam('id',null);
        $this->view->title = $this->view->translate("Edit Scholarship Tagging");
        $scholarshipTagInfo = $this->scholarshipTagModel->getScholarshipTagById($sct_id);
        $editScholarshipTaggingForm = new Studentfinance_Form_EditScholarshipTagging(array('IdProgram'=>$scholarshipTagInfo['IdProgram']));
        $intakeInfo = $this->scholarshipTagModel->getIntakeInfo($intake_id);
        
        $scholarshipTagInfo['IntakeDesc'] = $intakeInfo['IntakeDesc'];
        
        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'scholarship'),'default',true).'/id/'.$intake_id;
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($editScholarshipTaggingForm->isValid($formData)) {

                $data = array(
                    'sct_schId' => $formData['sct_schId'],
                    'sct_intake' => $intake_id,
                    'sct_program' => $formData['sct_program'],
                    'sct_studentcategory' => $formData['sct_studentcategory'],
                    'sct_programscheme' => $formData['sct_programscheme'],
                    'sct_desc' => $formData['sct_desc'],
                    'sct_status' => $formData['sct_status'],
                    'sct_startDate' => $formData['sct_startDate'],
                    'sct_startDateAccept' => $formData['sct_startDateAccept'],
                    'sct_endDateAccept' => $formData['sct_endDateAccept'],
                    'sct_endDate' => $formData['sct_endDate'],
                    'sct_modDate' => date('Y-m-d H:i:s'),
                    'sct_modBy' => $getUserIdentity->id
                );
                
                $this->scholarshipTagModel->update($data, 'sct_id = '.$sct_id);
                
                //redirect
                $this->_redirect($this->view->backURL);
            }else{
                $editScholarshipTaggingForm->populate($scholarshipTagInfo);
            }
        }else{
            $editScholarshipTaggingForm->populate($scholarshipTagInfo);
        }
        
        $this->view->editScholarshipTaggingForm = $editScholarshipTaggingForm;
        $this->view->intake_id = $intake_id;
    }
    
    public function deleteScholarshipTaggingAction(){
        $sct_id = $this->_getParam('sct_id',null);
        $intake_id = $this->_getParam('id',null);
        
        if ($sct_id){
            $this->scholarshipTagModel->delete("sct_Id = ".$sct_id);
        }
        
        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'scholarship'),'default',true).'/id/'.$intake_id;
        //redirect
        $this->_redirect($this->view->backURL);
    }
    
    public function scholarshipTaggingAjaxAction(){
        $program_id = $this->_getParam('id',null);
        
        if ($program_id == ''){
            $program_id = 0;
        }
		
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	   $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $progSchemeList = $this->scholarshipModel->getProgSchemeBasedOnProg($program_id);
       
        $i = 0;
        
        foreach($progSchemeList as $progSchemeListloop){
            $mod_of_study = $this->defModel->getData($progSchemeListloop['mode_of_study']);
            $mod_of_program = $this->defModel->getData($progSchemeListloop['mode_of_program']);
            $programType = $this->defModel->getData($progSchemeListloop['program_type']);
            
            $progSchemeList[$i]['mos_name'] = $mod_of_study['DefinitionDesc'];
            $progSchemeList[$i]['mop_name'] = $mod_of_program['DefinitionDesc'];
            $progSchemeList[$i]['programType'] = $programType['DefinitionDesc'];
            
            $i++;
        }
        
        $json = Zend_Json::encode($progSchemeList);
		
    	echo $json;
    	exit();
    }
    
    public function getSemesterAppliedAction(){
        $program_id = $this->_getParam('id',null);
        
        if ($program_id == ''){
            $program_id = 0;
        }
		
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	   $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        if ($program_id != 0){   
            $programInfo = $this->scholarshipModel->getProgramById($program_id);
            $list = $this->scholarshipModel->getSemester($programInfo['IdScheme']);
        }else{
            $list = $this->scholarshipModel->getSemesterAll();
        }
        
        $json = Zend_Json::encode($list);
		
    	echo $json;
    	exit();
    }

    public function processingAction() {
        
        $templateTagsCMS = new Cms_TemplateTags();

        $sponsor_applications = false;

        if ($this->getRequest()->isPost()){
            $post_data = $this->getRequest()->getPost();

            if (isset($post_data['approve']) || isset($post_data['reject']) || isset($post_data['shortlisted']) || isset($post_data['offeraccept']) || isset($post_data['offerdecline'])){
                //dd($post_data); exit;
                if(isset($post_data['approve'])) {
                    $status = 2;
                }else if(isset($post_data['reject'])) {
                    $status = 3;
                }else if(isset($post_data['shortlisted'])){
                    $status = 8;
                }else if(isset($post_data['offeraccept'])){
                    $status = 9;
                }else if(isset($post_data['offerdecline'])){
                    $status = 10;
                }else{
                    $status = null;
                }

                if(!empty($post_data['sa_id']) && !empty($status)){
                    $this->SponsorApplication->set_status($post_data['sa_id'], $status, $post_data['remarks']);

                    foreach ($post_data['sa_id'] as $saLoop){
                        $dateData = array(
                            'sa_start_date'=>$post_data['start_date'][$saLoop]=='' ? null:date('Y-m-d', strtotime($post_data['start_date'][$saLoop])),
                            //'sa_end_date'=>$post_data['end_date'][$saLoop]=='' ? null:date('Y-m-d', strtotime($post_data['end_date'][$saLoop]))
                        );
                        $this->SponsorApplication->updateSponsorApplication($dateData, $saLoop);

                        $info = $this->SponsorModel->viewApp2($saLoop);

                        //email part
                        $info_student = $this->SponsorModel->getAppSetup(array(
                            'application_type' => 0,
                            'type' => 'applicant',
                            'stype' => 1,
                            'program_id' => $info['IdProgram'],
                            'program_scheme_id' => $info['ap_prog_scheme'],
                            'student_category' => $info['appl_category'],
                            'sch_type' => $info['sct_schId']
                        ));


                        if($status == 2){
                            $statusDefId = 969;
                        } else if ($status == 3){
                            $statusDefId = 967;
                        } else if ($status == 8){
                            $statusDefId = 968;
                        } else if ($status == 9){
                            $statusDefId = 970;
                        } else if ($status == 10){
                            $statusDefId = 971;
                        } else {
                            $statusDefId = null;
                        }

                        $statusInfo = $this->defModel->getData($statusDefId);
                        $status_tag = 'status-'.strtolower($statusInfo['DefinitionCode']).'-application-'.$info_student['id'];

                        $commtplModel = new Communication_Model_DbTable_Template();
                        $templateinfo = $commtplModel->getTemplatesByCategory('scholarship',$status_tag, 1);
                        if ($templateinfo){
                            $template = $commtplModel->getTemplateContent($templateinfo['tpl_id'], 'en_US');

                            if ($template){
                                $cmsTags = new Cms_TemplateTags();
                                //$templateMail = $cmsTags->parseTag($info['sp_id'],$template['tpl_id'],$template['tpl_content']);
                                $templateMail = $templateTagsCMS->parseContent($info['sa_cust_id'], $template['tpl_id'], $template['tpl_content']);
                                //echo $templateMail; exit;
                                $emailDb = new App_Model_System_DbTable_Email();

                                //email personal
                                $data_inceif = array(
                                    'recepient_email' => $info['appl_email'],
                                    'subject' => $templateinfo["tpl_name"],
                                    'content' => $templateMail,
                                    'attachment_path'=>null,
                                    'attachment_filename'=>null					
                                );	

                                //add email to email que
                                $email_id2 = $emailDb->addData($data_inceif);

                                $getAttachment = $this->SponsorModel->getAttachmentList($info_student['id'], $statusDefId);

                                //attachment
                                if ($getAttachment){
                                    foreach ($getAttachment as $attachment) {
                                        /*if ($info['IdProgram'] == 1) {
                                            if ($info['appl_category'] == 579) {
                                                $attachmentContent = $emailDb->getFinancialAidAttachment(273);
                                            } else {
                                                $attachmentContent = $emailDb->getFinancialAidAttachment(165);
                                            }
                                        } else if ($info['IdProgram'] == 2) {
                                            //$attachmentContent = $emailDb->getFinancialAidAttachment(166);

                                            if ($info['appl_category'] == 579) {
                                                $attachmentContent = $emailDb->getFinancialAidAttachment(166);
                                            } else {
                                                $attachmentContent = $emailDb->getFinancialAidAttachment(275);
                                            }
                                        } else {
                                            $attachmentContent = false;
                                        }*/

                                        $attachmentContent = $emailDb->getFinancialAidAttachment($attachment['att_tpl_id']);

                                        if ($attachmentContent) {
                                            $html = $templateTagsCMS->parseContent($info['sa_cust_id'], $attachmentContent['tpl_id'], $attachmentContent['tpl_content']);

                                            $name = $info['at_pes_id'] . '_financialaid_' . date('YmdHis');

                                            $url = DOCUMENT_PATH . $info['at_repository'];

                                            //option pdf
                                            $option = array(
                                                'content' => $html,
                                                'save' => true,
                                                'file_extension' => 'pdf',
                                                'save_path' => $url,
                                                'file_name' => $name,
                                                /*'css' => '@page { margin: 110px 50px 50px 50px}
                                               body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                                                                table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                                                                table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                                                                table.tftable tr {background-color:#ffffff;}
                                                                table.tftable td {font-size:10px;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;}',*/
                                                'css' => '@page { margin: 110px 50px 50px 50px}
                                                            body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                                                            table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                                                            table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
                                                            table.tftable tr {background-color:#ffffff;}
                                                            table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
                                                'header' => '<script type="text/php">
                                            if ( isset($pdf) ) {
                                                    $header = $pdf->open_object();

                                                    $w = $pdf->get_width();
                                                    $h = $pdf->get_height();
                                                    $color = array(0,0,0);

                                                    $img_w = 180; 
                                                    $img_h = 59;
                                                    $pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

                                                    // Draw a line along the bottom
                                                    $font = Font_Metrics::get_font("Helvetica");
                                                    $size = 6;
                                                    $text_height = Font_Metrics::get_font_height($font, $size)+2;
                                                    $y = $h - (3.5 * $text_height)-10;
                                                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    // Draw a second line along the bottom
                                                    $y = $h - (3.5 * $text_height)+10;
                                                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    $pdf->close_object();

                                                    $pdf->add_object($header, "all");
                                            }
                                            </script>',
                                                'footer' => '<script type="text/php">
                                            if ( isset($pdf) ) {
                                                    $footer = $pdf->open_object();

                                                    $font = Font_Metrics::get_font("Helvetica");
                                                    $size = 6;
                                                    $color = array(0,0,0);
                                                    $text_height = Font_Metrics::get_font_height($font, $size)+2;

                                                    $w = $pdf->get_width();
                                                    $h = $pdf->get_height();


                                                    // Draw a line along the bottom
                                                    $y = $h - (3.5 * $text_height)-10;
                                                    //$pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    //1st row footer
                                                    $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
                                                    $width = Font_Metrics::get_text_width($text, $font, $size);	
                                                    $y = $h - (2 * $text_height)-20;
                                                    $x = ($w - $width) / 2.0;

                                                    $pdf->page_text($x, $y, $text, $font, $size, $color);

                                                    //2nd row footer
                                                    $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
                                                    $width = Font_Metrics::get_text_width($text, $font, $size);	
                                                    $y = $h - (1 * $text_height)-20;
                                                    $x = ($w - $width) / 2.0;

                                                    $pdf->page_text($x, $y, $text, $font, $size, $color);



                                                    $pdf->close_object();

                                                    $pdf->add_object($footer, "all");
                                            }
                                            </script>'
                                            );

                                            //genarate attachment
                                            $pdf = generatePdf($option);

                                            $dataEmailAttachment = array(
                                                'eqa_emailque_id' => $email_id2,
                                                'eqa_filename' => $name . '.pdf',
                                                'eqa_path' => $attachmentContent['tpl_type'] != 619 ? APP_DOC_PATH . $info['at_repository'] . '/' . $name . '.pdf' : APP_DOC_PATH . $attachmentContent['tpl_content'],
                                                'eqa_updDate' => date('Y-m-d H:i:s'),
                                                'eqa_updUser' => $this->auth->getIdentity()->iduser
                                            );
                                            $emailDb->addEmailQueAtttachment($dataEmailAttachment);

                                            //store history generate pdf
                                            $dataGeneratePdf = array(
                                                'tgd_txn_id' => $info['sa_cust_id'],
                                                'tgd_filename' => $name . '.pdf',
                                                'sth_id' => 0,
                                                'tgd_filetype' => 990,
                                                'tgd_fileurl' => $attachmentContent['tpl_type'] != 619 ? $info['at_repository'] . '/' . $name . '.pdf' : $attachmentContent['tpl_content'],
                                                'tgd_updDate' => date('Y-m-d H:i:s'),
                                                'tgd_updUser' => $this->auth->getIdentity()->iduser
                                            );
                                            $emailDb->storePdfDocGenHistory($dataGeneratePdf);
                                        }
                                    }
                                }

                                //save to history
                                $modelV = new Records_Model_DbTable_Visitingstudent();

                                $data_compose = array(
                                    'comp_subject'=> $templateinfo["tpl_name"],
                                    'comp_module' => 'scholarship', 
                                    'comp_type' => 585, 
                                    'comp_tpl_id' => $templateinfo["tpl_id"],
                                    'comp_rectype' => 'applicants', 
                                    'comp_lang' => 'en_US', 
                                    'comp_totalrecipients' => 1, 
                                    'comp_rec' => $info['at_trans_id'], 
                                    'comp_content' => $templateMail, 
                                    'created_by' => $this->auth->getIdentity()->iduser, 
                                    'created_date' => date('Y-m-d H:i:s')
                                );
                                $comId = $modelV->insertCommpose($data_compose);

                                $data_compose_rec = array(
                                    'cr_comp_id' => $comId, 
                                    'cr_subject' => $templateinfo["tpl_name"], 
                                    'cr_content' => $templateMail, 
                                    'cr_rec_id' => $info['at_trans_id'], 
                                    'cr_email' => $info['appl_email'], 
                                    'cr_status' => 1, 
                                    'cr_datesent' => date('Y-m-d H:i:s')
                                );
                                $modelV->insertComposeRec($data_compose_rec);
                            }
                        }
                    }
                }

                $searchForm = new Studentfinance_Form_SponsorshipApplicantSearchForm();
                $this->view->searchForm = $searchForm;

                //$sponsor_applications = $this->SponsorApplication->fetch_to_be_processed();
            }else if(isset($post_data['search'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicantSearchForm(array('programid'=>$post_data['Program']));
                $this->view->searchForm = $searchForm;

                $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(2, $post_data);
                $searchForm->populate($post_data);
            }else{
                $searchForm = new Studentfinance_Form_SponsorshipApplicantSearchForm();
                $this->view->searchForm = $searchForm;

                $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed();
            }
        }else{
            $searchForm = new Studentfinance_Form_SponsorshipApplicantSearchForm();
            $this->view->searchForm = $searchForm;
            
            //$sponsor_applications = $this->SponsorApplication->fetch_to_be_processed();
            $sponsor_applications = false;
        }
        
        $appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();

        $applicant_info = array();

        if ($sponsor_applications) {
            foreach ($sponsor_applications as $key => $sponsorapplication) {
                $sponsor_applicant = $this->SponsorApplication->get_applicant($sponsorapplication->sa_cust_id, $sponsorapplication->sa_cust_type);
                if (!empty($sponsor_applicant)) {
                    $applicant_info[$key] = $sponsor_applicant;
                    $applicant_info[$key]['age'] = $this->calcAge(date('Y-m-d', strtotime($sponsor_applicant['appl_dob'])));

                    $appQualification = $appQualificationDB->getTransData($sponsor_applicant['at_appl_id'], $sponsor_applicant['at_trans_id']);
                    $applicant_info[$key]['degree_awarded'] = !empty($appQualification) ? $appQualification[0]['ae_degree_awarded'] : 'n/a';

                    $pamodel = new Studentfinance_Model_DbTable_PaymentAmount();

                    $invList = $this->SponsorModel->getApplicantInvoice($sponsor_applicant['at_trans_id']);

                    $datePaymentTo = null;
                    if (isset($post_data['paymentdateto']) && $post_data['paymentdateto']!='' && $post_data['paymentdateto']!=null){
                        $datePaymentTo = date('Y-m-d', strtotime($post_data['paymentdateto']));
                    }

                    $datePayment = null;
                    if (isset($post_data['paymentdate']) && $post_data['paymentdate']!='' && $post_data['paymentdate']!=null){
                        $datePayment = date('Y-m-d', strtotime($post_data['paymentdate']));
                    }

                    $payment = 0.00;
                    if ($invList) {
                        foreach ($invList as $invLoop) {
                            $paymentInfo = $pamodel->getBalanceMainScholarship($invLoop['id'], $datePaymentTo, $datePayment);
                            $payment = $payment + $paymentInfo;
                        }
                    }

                    $applicant_info[$key]['payment'] = number_format($payment, 2, '.', '');
                }
            }
        }

        $this->view->applications = $sponsor_applications;
        $this->view->applicant_info = $applicant_info;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }
    
    public function exportApplicationApplicantAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(2, $post_data);

            //dd($sponsor_applications); exit;
            
            $appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();

            $applicant_info = array();
            foreach($sponsor_applications as $key => $sponsorapplication) {
                $sponsor_applicant = $this->SponsorApplication->get_applicant($sponsorapplication->sa_cust_id, $sponsorapplication->sa_cust_type);
                if(!empty($sponsor_applicant)){
                    $applicant_info[$key] = $sponsor_applicant;
                    $applicant_info[$key]['age'] = $this->calcAge(date('Y-m-d', strtotime($sponsor_applicant['appl_dob'])));

                    $appQualification = $appQualificationDB->getTransData($sponsor_applicant['at_appl_id'],$sponsor_applicant['at_trans_id']);
                    $applicant_info[$key]['degree_awarded'] = !empty($appQualification) ? $appQualification[0]['ae_degree_awarded'] : 'n/a';
                    $applicant_info[$key]['institutionname'] = $appQualification[0]['InstitutionName'];
                    $applicant_info[$key]['result'] = $appQualification[0]['ae_result'];
                    $applicant_info[$key]['yeargraduate'] = $appQualification[0]['ae_year_graduate'];
                    
                    $appEngPro = $appQualificationDB->getEngPro($sponsor_applicant['at_trans_id']);
                    $applicant_info[$key]['engproname'] = $appEngPro['engproname'];
                    
                    $appEmp = $appQualificationDB->getEmp($sponsor_applicant['at_trans_id']);
                    $applicant_info[$key]['empstatusname'] = $appEmp['empstatusname'];
                    
                    $appVisa = $appQualificationDB->getVisa($sponsor_applicant['at_trans_id']);
                    $applicant_info[$key]['visastatusname'] = $appVisa['visastatusname'];
                    $applicant_info[$key]['visaexpirydate'] = ($appVisa['av_expiry']==null || $appVisa['av_expiry']=='') ? '':date('d-m-Y', strtotime($appVisa['av_expiry']));

                    $pamodel = new Studentfinance_Model_DbTable_PaymentAmount();

                    $invList = $this->SponsorModel->getApplicantInvoice($sponsor_applicant['at_trans_id']);

                    $datePayment = null;
                    if (isset($post_data['paymentdate']) && $post_data['paymentdate']!='' && $post_data['paymentdate']!=null){
                        $datePayment = date('Y-m-d', strtotime($post_data['paymentdate']));
                    }

                    $payment = 0.00;
                    if ($invList) {
                        foreach ($invList as $invLoop) {
                            $paymentInfo = $pamodel->getBalanceMainScholarship($invLoop['id'], null, $datePayment);
                            $payment = $payment + $paymentInfo;
                        }
                    }

                    $applicant_info[$key]['payment'] = number_format($payment, 2, '.', '');
                }
            }

            $this->view->sponsor_application = $sponsor_applications;
            $this->view->applicant_info = $applicant_info;
        }
        
        $this->view->status = $this->SponsorApplication->status;
        $this->view->filename = date('Ymd').'_list_of_applicant_sponsorship_application.xls';
    }
    
    public function viewApplicationApplicantAction(){
        $this->view->title = $this->view->translate('View Sponsorship Application');
        
        $id = $this->_getParam('id');
        
        $info = $this->SponsorModel->viewApp2($id);
        $this->view->info = $info;
        
        $eduLvlList = $this->SponsorModel->getLevelEducationList();
        $this->view->eduLvlList = $eduLvlList;

        $currency = $this->SponsorModel->getCurrency();
        $this->view->currency = $currency;

        $semester = $this->SponsorModel->getSemester($info['IdScheme']);
        $this->view->semester = $semester;

        $industryW = $this->SponsorModel->getIndustryWeightageList();
        $this->view->industryW = $industryW;
        
        $courseList = $this->SponsorModel->getApplicationCourse($id);
        $this->view->courseList = $courseList;
    }
    
    public function revertApplicationApplicantAction(){
        $this->view->title = $this->view->translate('Revert Sponsorship Application');
        
        $id = $this->_getParam('id');
        
        if ($this->getRequest()->isPost()){
            $post_data = $this->getRequest()->getPost();
            
            $this->SponsorApplication->revert_status($id, $post_data['revertstatus'], $post_data['remarks']);

            $templateTagsCMS = new Cms_TemplateTags();

            $info = $this->SponsorModel->viewApp2($id);

            //email part
            $info_student = $this->SponsorModel->getAppSetup(array(
                'application_type' => 0,
                'type' => 'applicant',
                'stype' => 1,
                'program_id' => $info['IdProgram'],
                'program_scheme_id' => $info['ap_prog_scheme'],
                'student_category' => $info['appl_category'],
                'sch_type' => $info['sct_schId']
            ));

            $status = $post_data['revertstatus'];

            if($status == 2){
                $statusDefId = 969;
            } else if ($status == 3){
                $statusDefId = 967;
            } else if ($status == 8){
                $statusDefId = 968;
            } else if ($status == 9){
                $statusDefId = 970;
            } else if ($status == 10){
                $statusDefId = 971;
            } else {
                $statusDefId = null;
            }

            $status = $this->defModel->getData($statusDefId);
            $status_tag = 'status-'.strtolower($status['DefinitionCode']).'-application-'.$info_student['id'];

            $commtplModel = new Communication_Model_DbTable_Template();
            $templateinfo = $commtplModel->getTemplatesByCategory('scholarship',$status_tag, 1);

            if ($templateinfo){
                $template = $commtplModel->getTemplateContent($templateinfo['tpl_id'], 'en_US');

                if ($template){
                    $cmsTags = new Cms_TemplateTags();
                    //$templateMail = $cmsTags->parseTag($info['sp_id'],$template['tpl_id'],$template['tpl_content']);
                    $templateMail = $templateTagsCMS->parseContent($info['sa_cust_id'], $template['tpl_id'], $template['tpl_content']);
                    //echo $templateMail; exit;
                    $emailDb = new App_Model_System_DbTable_Email();

                    //email personal
                    $data_inceif = array(
                        'recepient_email' => $info['appl_email'],
                        'subject' => $templateinfo["tpl_name"],
                        'content' => $templateMail,
                        'attachment_path'=>null,
                        'attachment_filename'=>null
                    );

                    //add email to email que
                    $email_id2 = $emailDb->addData($data_inceif);

                    $getAttachment = $this->SponsorModel->getAttachmentList($info_student['id'], $statusDefId);

                    //attachment
                    if ($getAttachment){
                        foreach ($getAttachment as $attachment) {
                            /*if ($info['IdProgram'] == 1) {
                                if ($info['appl_category'] == 579) {
                                    $attachmentContent = $emailDb->getFinancialAidAttachment(273);
                                } else {
                                    $attachmentContent = $emailDb->getFinancialAidAttachment(165);
                                }
                            } else if ($info['IdProgram'] == 2) {
                                //$attachmentContent = $emailDb->getFinancialAidAttachment(166);

                                if ($info['appl_category'] == 579) {
                                    $attachmentContent = $emailDb->getFinancialAidAttachment(166);
                                } else {
                                    $attachmentContent = $emailDb->getFinancialAidAttachment(275);
                                }
                            } else {
                                $attachmentContent = false;
                            }*/

                            $attachmentContent = $emailDb->getFinancialAidAttachment($attachment['att_tpl_id']);

                            if ($attachmentContent) {
                                $html = $templateTagsCMS->parseContent($info['sa_cust_id'], $attachmentContent['tpl_id'], $attachmentContent['tpl_content']);

                                $name = $info['at_pes_id'] . '_financialaid_' . date('YmdHis');

                                $url = DOCUMENT_PATH . $info['at_repository'];

                                //option pdf
                                $option = array(
                                    'content' => $html,
                                    'save' => true,
                                    'file_extension' => 'pdf',
                                    'save_path' => $url,
                                    'file_name' => $name,
                                    /*'css' => '@page { margin: 110px 50px 50px 50px}
                                   body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                                                    table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                                                    table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                                                    table.tftable tr {background-color:#ffffff;}
                                                    table.tftable td {font-size:10px;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;}',*/
                                    'css' => '@page { margin: 110px 50px 50px 50px}
                                                            body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                                                            table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                                                            table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
                                                            table.tftable tr {background-color:#ffffff;}
                                                            table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
                                    'header' => '<script type="text/php">
                                            if ( isset($pdf) ) {
                                                    $header = $pdf->open_object();

                                                    $w = $pdf->get_width();
                                                    $h = $pdf->get_height();
                                                    $color = array(0,0,0);

                                                    $img_w = 180;
                                                    $img_h = 59;
                                                    $pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

                                                    // Draw a line along the bottom
                                                    $font = Font_Metrics::get_font("Helvetica");
                                                    $size = 6;
                                                    $text_height = Font_Metrics::get_font_height($font, $size)+2;
                                                    $y = $h - (3.5 * $text_height)-10;
                                                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    // Draw a second line along the bottom
                                                    $y = $h - (3.5 * $text_height)+10;
                                                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    $pdf->close_object();

                                                    $pdf->add_object($header, "all");
                                            }
                                            </script>',
                                    'footer' => '<script type="text/php">
                                            if ( isset($pdf) ) {
                                                    $footer = $pdf->open_object();

                                                    $font = Font_Metrics::get_font("Helvetica");
                                                    $size = 6;
                                                    $color = array(0,0,0);
                                                    $text_height = Font_Metrics::get_font_height($font, $size)+2;

                                                    $w = $pdf->get_width();
                                                    $h = $pdf->get_height();


                                                    // Draw a line along the bottom
                                                    $y = $h - (3.5 * $text_height)-10;
                                                    //$pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    //1st row footer
                                                    $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
                                                    $width = Font_Metrics::get_text_width($text, $font, $size);
                                                    $y = $h - (2 * $text_height)-20;
                                                    $x = ($w - $width) / 2.0;

                                                    $pdf->page_text($x, $y, $text, $font, $size, $color);

                                                    //2nd row footer
                                                    $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
                                                    $width = Font_Metrics::get_text_width($text, $font, $size);
                                                    $y = $h - (1 * $text_height)-20;
                                                    $x = ($w - $width) / 2.0;

                                                    $pdf->page_text($x, $y, $text, $font, $size, $color);



                                                    $pdf->close_object();

                                                    $pdf->add_object($footer, "all");
                                            }
                                            </script>'
                                );

                                //genarate attachment
                                $pdf = generatePdf($option);

                                $dataEmailAttachment = array(
                                    'eqa_emailque_id' => $email_id2,
                                    'eqa_filename' => $name . '.pdf',
                                    'eqa_path' => $attachmentContent['tpl_type'] != 619 ? APP_DOC_PATH . $info['at_repository'] . '/' . $name . '.pdf' : APP_DOC_PATH . $attachmentContent['tpl_content'],
                                    'eqa_updDate' => date('Y-m-d H:i:s'),
                                    'eqa_updUser' => $this->auth->getIdentity()->iduser
                                );
                                $emailDb->addEmailQueAtttachment($dataEmailAttachment);

                                //store history generate pdf
                                $dataGeneratePdf = array(
                                    'tgd_txn_id' => $info['sa_cust_id'],
                                    'tgd_filename' => $name . '.pdf',
                                    'sth_id' => 0,
                                    'tgd_filetype' => 990,
                                    'tgd_fileurl' => $attachmentContent['tpl_type'] != 619 ? $info['at_repository'] . '/' . $name . '.pdf' : $attachmentContent['tpl_content'],
                                    'tgd_updDate' => date('Y-m-d H:i:s'),
                                    'tgd_updUser' => $this->auth->getIdentity()->iduser
                                );
                                $emailDb->storePdfDocGenHistory($dataGeneratePdf);
                            }
                        }
                    }

                    //save to history
                    $modelV = new Records_Model_DbTable_Visitingstudent();

                    $data_compose = array(
                        'comp_subject'=> $templateinfo["tpl_name"],
                        'comp_module' => 'scholarship',
                        'comp_type' => 585,
                        'comp_tpl_id' => $templateinfo["tpl_id"],
                        'comp_rectype' => 'applicants',
                        'comp_lang' => 'en_US',
                        'comp_totalrecipients' => 1,
                        'comp_rec' => $info['at_trans_id'],
                        'comp_content' => $templateMail,
                        'created_by' => $this->auth->getIdentity()->iduser,
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $comId = $modelV->insertCommpose($data_compose);

                    $data_compose_rec = array(
                        'cr_comp_id' => $comId,
                        'cr_subject' => $templateinfo["tpl_name"],
                        'cr_content' => $templateMail,
                        'cr_rec_id' => $info['at_trans_id'],
                        'cr_email' => $info['appl_email'],
                        'cr_status' => 1,
                        'cr_datesent' => date('Y-m-d H:i:s')
                    );
                    $modelV->insertComposeRec($data_compose_rec);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
            $this->_redirect( $this->baseUrl . '/studentfinance/scholarship/processing/id/'.$id);
        }
        
        $info = $this->SponsorModel->viewApp2($id);
        $this->view->info = $info;
        
        $this->view->status = $this->SponsorApplication->status;
        
        $revertStatus = false;
        
        if ($info['sa_status']==3){
            $revertStatus = array(
                array (
                    'id'=>1,
                    'value'=>$this->SponsorApplication->status[1]
                )
            );
        }else if ($info['sa_status']==8){
            $revertStatus = array(
                array (
                    'id'=>1,
                    'value'=>$this->SponsorApplication->status[1]
                ),
                array (
                    'id'=>3,
                    'value'=>$this->SponsorApplication->status[3]
                )
            );
        }else if ($info['sa_status']==2){
            $revertStatus = array(
                array (
                    'id'=>8,
                    'value'=>$this->SponsorApplication->status[8]
                ),
                array (
                    'id'=>1,
                    'value'=>$this->SponsorApplication->status[1]
                )
            );
        }else if ($info['sa_status']==9){
            $revertStatus = array(
                array (
                    'id'=>10,
                    'value'=>$this->SponsorApplication->status[10]
                )
            );
        }else if ($info['sa_status']==10){
            $revertStatus = array(
                array (
                    'id'=>9,
                    'value'=>$this->SponsorApplication->status[9]
                )
            );
        }
        
        $this->view->revertStatus = $revertStatus;
    }

    public function viewEvaluationAction() {
        //$Scholarship = new Studentfinance_Model_DbTable_Scholarship();
        $SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();
        $StudentRegistration = new Registration_Model_DbTable_Studentregistration();
        $id = $this->_getParam('id',null);
        $sponsor_apps = $SponsorApplication->find($id);
        $sponsor_app = $sponsor_apps->current();
        $sponsor_details = $sponsor_app->findDependentRowset('Studentfinance_Model_DbTable_ScholarshipDetail');
        $sponsor_detail = $sponsor_details->current();
        $student = $sponsor_app->findParentRow('Registration_Model_DbTable_Studentregistration');
        $student_info = $StudentRegistration->getData($student->IdStudentRegistration);

        $profile = $student->findParentRow('Records_Model_DbTable_Studentprofile');
        $this->view->profile = $profile;
        $calc_breakdown = $SponsorApplication->getcalculation($id, true);

        $QualificationMaster = new App_Model_General_DbTable_Qualificationmaster();
        $education_levels = $QualificationMaster->getCerts();

        $ScholarshipWeight = new Studentfinance_Model_DbTable_ScholarshipWeight();
        $this->view->weightages = $ScholarshipWeight->fetchAll();

        $this->view->calc_breakdown = $calc_breakdown;
        $this->view->sponsor_app = $sponsor_app;
        $this->view->sponsor_detail = $sponsor_detail;
        $this->view->student_info = $student_info;
        $this->view->education_levels = $education_levels;
    }
	
    public function calcAge($date){
        $from = new DateTime($date);
        $to   = new DateTime('today');

        return $from->diff($to)->y;
    }

    public function processingStudentAction(){
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            //var_dump($post_data); exit;
            if (isset($post_data['approve']) || isset($post_data['reject']) || isset($post_data['complete']) || isset($post_data['incomplete']) || isset($post_data['cancel'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
                
                if(!empty($post_data['approve'])){
                    $status = 2;
                } else if (!empty($post_data['reject'])){
                    $status = 3;
                } else if (!empty($post_data['complete'])){
                    $status = 4;
                } else if (!empty($post_data['incomplete'])){
                    $status = 5;
                } else if (!empty($post_data['cancel'])){
                    $status = 6;
                } else {
                    $status = null;
                }

                if(!empty($post_data['sa_id']) && !empty($status)){
                    $this->SponsorApplication->set_status($post_data['sa_id'], $status, $post_data['remarks']);
                    
                    foreach ($post_data['sa_id'] as $saLoop){
                        $info = $this->SponsorModel->viewApp($saLoop);
                    
                        //email part
                        $info_student = $this->SponsorModel->getAppSetup(array(
                            'application_type' => 0,
                            'type' => 'student',
                            'stype' => 3,
                            'program_id' => $info['IdProgram'],
                            'sch_type' => 1
                        ));

                        if ($status == 2){
                            $statusDefId = 774;
                        }else if ($status == 3){
                            $statusDefId = 775;
                        }else if ($status == 4){
                            $statusDefId = 949;
                        }else if ($status == 5){
                            $statusDefId = 950;
                        }else if ($status == 6){
                            $statusDefId = 951;
                        }

                        $status = $this->defModel->getData($statusDefId);
                        $status_tag = 'status-'.strtolower($status['DefinitionCode']).'-application-'.$info_student['id'];

                        $commtplModel = new Communication_Model_DbTable_Template();
                        $templateinfo = $commtplModel->getTemplatesByCategory('scholarship',$status_tag, 1);
                    
                        if ($templateinfo){
                            $template = $commtplModel->getTemplateContent($templateinfo['tpl_id'], 'en_US');
                    
                            $cmsTags = new Cms_TemplateTags();
                            $templateMail = $cmsTags->parseTag($info['sp_id'],$template['tpl_id'],$template['tpl_content']);
                            //echo $templateMail; exit;
                            $emailDb = new App_Model_System_DbTable_Email();

                            //email personal
                            $data_inceif = array(
                                'recepient_email' => $info['appl_email_personal'],
                                'subject' => $templateinfo["tpl_name"],
                                'content' => $templateMail,
                                'attachment_path'=>null,
                                'attachment_filename'=>null					
                            );	

                            //add email to email que
                            $email_id2 = $emailDb->addData($data_inceif);
                            
                            //save to history
                            $modelV = new Records_Model_DbTable_Visitingstudent();
                            
                            $data_compose = array(
                                'comp_subject'=> $templateinfo["tpl_name"],
                                'comp_module' => 'scholarship', 
                                'comp_type' => 585, 
                                'comp_tpl_id' => $templateinfo["tpl_id"],
                                'comp_rectype' => 'students', 
                                'comp_lang' => 'en_US', 
                                'comp_totalrecipients' => 1, 
                                'comp_rec' => $info['IdStudentRegistration'], 
                                'comp_content' => $templateMail, 
                                'created_by' => $this->auth->getIdentity()->iduser, 
                                'created_date' => date('Y-m-d H:i:s')
                            );
                            $comId = $modelV->insertCommpose($data_compose);

                            $data_compose_rec = array(
                                'cr_comp_id' => $comId, 
                                'cr_subject' => $templateinfo["tpl_name"], 
                                'cr_content' => $templateMail, 
                                'cr_rec_id' => $info['IdStudentRegistration'], 
                                'cr_email' => $info['appl_email_personal'], 
                                'cr_status' => 1, 
                                'cr_datesent' => date('Y-m-d H:i:s')
                            );
                            $modelV->insertComposeRec($data_compose_rec);
                        }
                    }
                }
                $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(1);
            }else if (isset($post_data['search'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm(array('programid'=>$post_data['Program']));
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(1, $post_data);
                $searchForm->populate($post_data);
            }else{
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(1);
            }
        }else{
            $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
            $this->view->searchForm = $searchForm;
            
            $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(1);
        }

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function shortlistStudentAction() {
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            
            if (!empty($post_data['shortlist'])){
                $status = 4;
            }else if (!empty($post_data['reject'])){
                $status = 3;
            }else{
                $status = null;
            }

            if(!empty($post_data['sa_id']) && !empty($status) )  {
                $this->SponsorApplication->set_status($post_data['sa_id'], $status);
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_to_be_shortlisted();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function offeredStudentAction() {

        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();

            if(!empty($post_data['retract'])) {
                $status = 7;
                if(!empty($post_data['sa_id']) && !empty($status) )  {

                    $this->SponsorApplication->set_status($post_data['sa_id'], $status);
                }
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_offered();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function acceptedStudentAction(){

        if ($this->getRequest()->isPost()){
            $post_data = $this->getRequest()->getPost();

            if (!empty($post_data['retract'])){
                $status = 7;
                if (!empty($post_data['sa_id']) && !empty($status) ){
                    $this->SponsorApplication->set_status($post_data['sa_id'], $status);
                }
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_accepted();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function retractedStudentAction(){

        if ($this->getRequest()->isPost()){
            $post_data = $this->getRequest()->getPost();

            if (!empty($post_data['retract'])){
                $status = 7;
                if(!empty($post_data['sa_id']) && !empty($status) ){
                    $this->SponsorApplication->set_status($post_data['sa_id'], $status);
                }
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_retracted();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function nonacceptanceStudentAction(){

        if ($this->getRequest()->isPost()){
            $post_data = $this->getRequest()->getPost();

            if(!empty($post_data['retract'])){
                $status = 7;
                if(!empty($post_data['sa_id']) && !empty($status) ){
                    $this->SponsorApplication->set_status($post_data['sa_id'], $status);
                }
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_nonacceptance();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function rejectedStudentAction() {

        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();

            if(!empty($post_data['retract'])){
                $status = 7;
                if(!empty($post_data['sa_id']) && !empty($status) ){
                    $this->SponsorApplication->set_status($post_data['sa_id'], $status);
                }
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_rejected();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function viewAction() {

        if ($this->getRequest()->isPost()){
            $post_data = $this->getRequest()->getPost();

            if (!empty($post_data['shortlist'])){
                $status = 4;
            } else if (!empty($post_data['reject'])){
                $status = 3;
            } else {
                $status = null;
            }

            if (!empty($post_data['sa_id']) && !empty($status)){
                $this->SponsorApplication->set_status($post_data['sa_id'], $status);
            }
        }

        $sponsor_applications = $this->SponsorApplication->fetch_processed();

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function applyAction() {

        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            $auth = Zend_Auth :: getInstance();

            if( $this->SponsorApplication->has_applied($auth->getIdentity()->iduser, 1) ) {
                $application['sa_af_id'] = $post_data['sa_af_id'];
                $application['sa_cust_id'] = $auth->getIdentity()->iduser;
                $application['sa_cust_type'] = 1;
                $application['sa_status'] = 1;
                $application['sa_createddt'] = date('Y-m-d h:i:s');
                $application['sa_createdby'] = $auth->getIdentity()->iduser;

                $this->SponsorApplication->insert($application);
                $this->gobjsessionsis->flash = array('message' => 'Application made.', 'type' => 'success');
                $this->_redirect($this->baseUrl . '/studentfinance/scholarship/my-application');
            } else {
                $this->gobjsessionsis->flash = array('message' => 'You have already applied for this sponsor.', 'type' => 'error');
            }
        }
        

        $ApplicantFinancial = new Studentfinance_Model_DbTable_ApplicantFinancial();
        $this->view->applicant_financials = $ApplicantFinancial->get_list();

         $this->view->SponsorApplicationForm = $this->SponsorApplicationForm;
    }

    public function listingAction() {
        

        //$sponsor_applications = ;
        $sponsor_applications = Zend_Paginator::factory($this->SponsorApplication->fetchAll());
        $sponsor_applications->setCurrentPageNumber($this->_getParam('page'),0);
        $applicant_info = array();
        $sponsor_info = array();
        foreach($sponsor_applications as $key => $sponsorapplication) {
            $applicant_info[$key] = $this->SponsorApplication->get_applicant($sponsorapplication->sa_cust_id, $sponsorapplication->sa_cust_type);
            $sponsor_info[$key] = $sponsorapplication->findParentRow('Studentfinance_Model_DbTable_ApplicantFinancial', 'Sponsor');
        }
        
        $this->view->applications = $sponsor_applications;
        $this->view->applicant_info = $applicant_info;
        $this->view->sponsor_info = $sponsor_info;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function myApplicationAction() {
        
        $auth = Zend_Auth :: getInstance();
        $auth->getIdentity()->iduser;

        $sponsor_applications = $this->SponsorApplication->fetch_my_application($auth->getIdentity()->iduser);
        
        $this->view->applications = $sponsor_applications;
        $this->view->status = $this->SponsorApplication->status;
    }

    public function applicationformsetupAction(){
        $this->view->title = 'Application Form Setup';

        $auth = Zend_Auth::getInstance();
        $form = new Studentfinance_Form_ApplicationFormSetup();

        $info = $this->SponsorModel->getAppSetup(1);

        //status
        $status = $this->defModel->getByCode('Scholarship Application Status');

        $this->view->status = $status;

        if ( !empty($info) ){
            $form->populate($info);
            $this->view->description = $info['description'];
        }

        if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'save' )){
            $formData = $this->getRequest()->getPost();

            if ( empty($info) ){
                $data = array(
                'semester_id' => $formData['semester_id'],
                'start_date' => $formData['start_date'],
                'end_date' => $formData['end_date'],
                'created_by' => $auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()')
                );
                $this->SponsorModel->insertAppSetup($data);
            }else{
                $data = array(
                    'semester_id' => $formData['semester_id'],
                    'start_date' => $formData['start_date'],
                    'end_date' => $formData['end_date'],
                    'updated_by' => $auth->getIdentity()->iduser,
                    'updated_date' => new Zend_Db_Expr('NOW()')
                );
                $this->SponsorModel->updateAppSetup($data, 1);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect( $this->baseUrl . '/studentfinance/scholarship/applicationformsetup/');
        }

        if ($this->getRequest()->isPost()  && $this->_request->getPost ( 'savedesc' )){
            $formData = $this->getRequest()->getPost();

            if ( empty($info) ){
                $data = array(
                    'description' => $formData['description'],
                    'semester_id' => 0,
                    'start_date' => '',
                    'end_date' => '',
                    'created_by' => $auth->getIdentity()->iduser,
                    'created_date' => new Zend_Db_Expr('NOW()')
                );
                $this->SponsorModel->insertAppSetup($data);
            }else{
                $data = array(
                    'description' => $formData['description'],
                    'updated_by' => $auth->getIdentity()->iduser,
                    'updated_date' => new Zend_Db_Expr('NOW()')
                );
                $this->SponsorModel->updateAppSetup($data, 1);
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
            $this->_redirect( $this->baseUrl . '/studentfinance/scholarship/applicationformsetup/#tab-2');
        }

        //view
        $this->view->form = $form;
    }	

    public function statusTemplateAction(){
        $id = $this->_getParam('id');

        $status = $this->defModel->getData($id);

        if ( empty($status) ){
            throw new Exception('Invalid Status ID');
        }

        $commtplModel = new Communication_Model_DbTable_Template();

        //$template = $commtplModel->getTemplatesByCategory('scholarship','status-'.strtolower($status['DefinitionCode']), 1);
        $template = array();

        if ( empty($template) ){
            //create the template
            $auth = Zend_Auth::getInstance();

            $commGeneralModel = new Communication_Model_DbTable_General();


            $tpl_data = array(
                'tpl_module' => 'scholarship',
                'tpl_name' => 'Status - '.$status['DefinitionCode'],
                'tpl_type' => $commGeneralModel->communicationTypeId('Email'),
                'tpl_pname' => clean_string('Status - '.$status['DefinitionCode']),
                'created_by' => $auth->getIdentity()->iduser,
                'created_date' => new Zend_Db_Expr('NOW()'),
                'email_from_name' => '',
                'email_from' => '',
                'tpl_categories' => 'status-'.strtolower($status['DefinitionCode'])
            );
            $template_id = $commtplModel->addTemplate($tpl_data);

            foreach ( $this->locales as $locale_code => $locale ){
                $content = '';

                $cdata = array(
                    'tpl_id' =>	$template_id,
                    'locale' =>	$locale_code,
                    'tpl_content' => ''
                );
                $commtplModel->addTemplateContent($cdata);
            }
        }else{
            $template_id = $template['tpl_id'];
        }

        $this->_redirect( $this->baseUrl . '/communication/scholarship/template-detail/id/'.$template_id.'/from/applicationformsetup' );
    }


    public function weightageSetupAction() {
        $ScholarshipWeight = new Studentfinance_Model_DbTable_ScholarshipWeight();
        if($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('mainweight');
            $ScholarshipWeight->delete('1');
            foreach($post_data as $key => $value) {
                $weight['name'] = $key;
                $weight['weight'] = $value/100;
                $ScholarshipWeight->save($weight);
            }
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
        }

        $this->view->weightages = $ScholarshipWeight->fetchAll();
    }

    public function viewApplicationAction(){
        $this->view->title = $this->view->translate('View Sponsorship Application');
        
        $id = $this->_getParam('id');
        
        $info = $this->SponsorModel->viewApp($id);
        $upl = $this->SponsorModel->viewUpl($id);
        //var_dump($info);
        $this->view->id = $id;
        $this->view->info = $info;
        $this->view->upl = $upl;
        
        //get highest education
        if ($info['IdScheme']==1){
            $education = $this->SponsorModel->getHighestLevelEdu($info['sp_id']);
            $this->view->education = $education;
        }else{
            $education = $this->SponsorModel->getHighestLevelEdu2($info['IdProgram']);

            $registered_semester = $this->SponsorModel->getListSemesterRegistered($info['IdStudentRegistration']);
            $student_grade = false;
            foreach($registered_semester as $index=>$semester){
                //get subject registered in each semester
                if($semester["IsCountable"]==1){				
                    $subject_list = $this->SponsorModel->getListCourseRegisteredBySemesterWithAttendanceStatus($info['IdStudentRegistration'],$semester['IdSemesterMain']);
                }else{			
                    $subject_list = $this->SponsorModel->getListCourseRegisteredBySemesterWithoutAttendance($info['IdStudentRegistration'],$semester['IdSemesterMain']);
                }
                //var_dump($subject_list);
                $registered_semester[$index]['subjects']=$subject_list;

                //get grade info
                $student_grade = $this->SponsorModel->getStudentGrade($info['IdStudentRegistration'],$semester['IdSemesterMain']);
            }

            if (!$student_grade){
                $student_grade['sg_cum_credithour']=0;
                $student_grade['sg_cgpa']=0;
            }

            $education['ae_result'] = $student_grade['sg_cgpa'];

            $this->view->education = $education;
        }
        
        $eduLvlList = $this->SponsorModel->getLevelEducationList();
        $this->view->eduLvlList = $eduLvlList;

        $currency = $this->SponsorModel->getCurrency();
        $this->view->currency = $currency;

        $semester = $this->SponsorModel->getSemester($info['IdScheme']);
        $this->view->semester = $semester;

        $industryW = $this->SponsorModel->getIndustryWeightageList();
        $this->view->industryW = $industryW;
        
        $courseList = $this->SponsorModel->getApplicationCourse($id);
        $this->view->courseList = $courseList;
    }
    
    public function reapplicationAction(){
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            //var_dump($post_data); exit;
            if (isset($post_data['approve']) || isset($post_data['reject']) || isset($post_data['complete']) || isset($post_data['incomplete']) || isset($post_data['cancel'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
                
                if(!empty($post_data['approve'])){
                    $status = 2;
                } else if (!empty($post_data['reject'])){
                    $status = 3;
                } else {
                    $status = null;
                }
                
                if(!empty($post_data['sre_id']) && !empty($status)){
                    $this->SponsorModel->set_restatus($post_data['sre_id'], $status, $post_data['remarks']);
                    
                    foreach ($post_data['sre_id'] as $saLoop){
                        $info = $this->SponsorModel->getReApplication($saLoop);
                        
                        //email part
                        $info_student = $this->SponsorModel->getAppSetup(array(
                            'application_type' => 1,
                            'type' => 'student',
                            'stype' => 3,
                            'program_id' => $info['IdProgram'],
                            'sch_type' => 1
                        ));
                        
                        if ($status == 2){
                            $statusDefId = 954;
                        }else if ($status == 3){
                            $statusDefId = 955;
                        }

                        $status = $this->defModel->getData($statusDefId);
                        $status_tag = 'status-'.strtolower($status['DefinitionCode']).'-reapplication-'.$info_student['id'];
                        
                        $commtplModel = new Communication_Model_DbTable_Template();
                        $templateinfo = $commtplModel->getTemplatesByCategory('scholarship',$status_tag, 1);
                        
                        if ($templateinfo){
                            $template = $commtplModel->getTemplateContent($templateinfo['tpl_id'], 'en_US');
                            
                            $cmsTags = new Cms_TemplateTags();
                            $templateMail = $cmsTags->parseTag($info['sp_id'],$template['tpl_id'],$template['tpl_content']);
                            
                            $emailDb = new App_Model_System_DbTable_Email();

                            //email personal
                            $data_inceif = array(
                                'recepient_email' => $info['appl_email_personal'],
                                'subject' => $templateinfo["tpl_name"],
                                'content' => $templateMail,
                                'attachment_path'=>null,
                                'attachment_filename'=>null					
                            );	

                            //add email to email que
                            $email_id2 = $emailDb->addData($data_inceif);

                            //save to history
                            $modelV = new Records_Model_DbTable_Visitingstudent();

                            $data_compose = array(
                                'comp_subject'=> $templateinfo["tpl_name"],
                                'comp_module' => 'scholarship', 
                                'comp_type' => 585, 
                                'comp_tpl_id' => $templateinfo["tpl_id"],
                                'comp_rectype' => 'students', 
                                'comp_lang' => 'en_US', 
                                'comp_totalrecipients' => 1, 
                                'comp_rec' => $info['IdStudentRegistration'], 
                                'comp_content' => $templateMail, 
                                'created_by' => $this->auth->getIdentity()->iduser, 
                                'created_date' => date('Y-m-d H:i:s')
                            );
                            $comId = $modelV->insertCommpose($data_compose);

                            $data_compose_rec = array(
                                'cr_comp_id' => $comId, 
                                'cr_subject' => $templateinfo["tpl_name"], 
                                'cr_content' => $templateMail, 
                                'cr_rec_id' => $info['IdStudentRegistration'], 
                                'cr_email' => $info['appl_email_personal'], 
                                'cr_status' => 1, 
                                'cr_datesent' => date('Y-m-d H:i:s')
                            );
                            $modelV->insertComposeRec($data_compose_rec);
                        }
                    }
                }
                
                $sponsor_applications = $this->SponsorModel->viewReAppList();
            }else if (isset($post_data['search'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm(array('programid'=>$post_data['Program']));
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorModel->viewReAppList($post_data);
                $searchForm->populate($post_data);
            }else{
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorModel->viewReAppList();
            }
        }else{
            $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
            $this->view->searchForm = $searchForm;
            
            $sponsor_applications = $this->SponsorModel->viewReAppList();
        }

        $this->view->applications = $sponsor_applications;
        $this->view->cust_type = $this->SponsorApplication->cust_types;
        $this->view->status = $this->SponsorApplication->status;
    }
    
    public function viewReapplicationAction(){
        $this->view->title = $this->view->translate('Financial Assistance');
        
        $id = $this->_getParam('id');
        $info = $this->SponsorModel->getReApplication($id);
        
        $semList = $this->SponsorModel->getSemester($info['IdScheme']);
        $this->view->semList = $semList;
        
        $sectionC = $this->SponsorModel->getSectionC($info['IdProgram']);
        $this->view->sectionC = $sectionC;
        
        if (!empty($info)){
            switch ($info['sre_status']){
                case 1 : $info['sre_status_name'] = 'APPLIED';	break;
                case 2 : $info['sre_status_name'] = 'APPROVED';	break;
                case 3 : $info['sre_status_name'] = 'REJECTED';	break;
                case 4 : $info['sre_status_name'] = 'COMPLETED'; break;
                case 5 : $info['sre_status_name'] = 'INCOMPLETE'; break;
                case 6 : $info['sre_status_name'] = 'CANCEL'; break;
            }
        }

        $this->view->info = $info;
        
        $semesterInfo = $this->SponsorModel->getSemesterById($info['sre_semester_id']);
        $lastSem = $this->SponsorModel->getLastSem($info['IdScheme'], $semesterInfo['IdSemesterMaster'], $semesterInfo['SemesterMainStartDate'], $info['IdBranch']);
        $lastSemsub = $this->SponsorModel->getRegSubLastSem($info['IdStudentRegistration'], $lastSem['IdSemesterMaster']);
        $this->view->lastSemsub = $lastSemsub;
        
        $publishInfo = $this->SponsorModel->getPublishResult($info['IdProgram'], $lastSem['IdSemesterMaster']);
        
        if ($publishInfo){
            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
        }else{
            $publishInfo['pm_date_new'] = strtotime('2300-01-01 00:00:00');
        }
        
        $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));
        
        $this->view->publishInfo = $publishInfo;
        
        $courseList = $this->SponsorModel->getReApplicationCourse($id);
        $this->view->courseList = $courseList;
    }
    
    public function uploadAction(){
            
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            $infoApp = $this->SponsorModel->viewApp($formData['id']);
            $dir = DOCUMENT_PATH.$infoApp['sp_repository'];

            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false ){
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }

            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();

            if ($files){
                foreach ($files as $file=>$info){
                    if ($adapter->isValid($file)){
                        //var_dump($info); exit;
                        $fileOriName = $info['name'];
                        $fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
                        $filepath = $info['destination'].'/'.$fileRename;

                        if ($adapter->isUploaded($file)){
                            $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
                            if ($adapter->receive($file)){
                                $uploadData = array(
                                    'sau_sa_id'=>$formData['id'], 
                                    'sau_filename'=>$fileOriName, 
                                    'sau_filerename'=>$fileRename, 
                                    'sau_filelocation'=>$infoApp['sp_repository'].'/'.$fileRename, 
                                    'sau_filesize'=>$info['size'], 
                                    'sau_mimetype'=>$info['type'], 
                                    'sau_upddate'=>date('Y-m-d H:i:s'), 
                                    'sau_upduser'=>0
                                );
                                //var_dump($uploadData); exit;
                                $this->SponsorModel->uploadfile($uploadData);
                            }
                        }
                    }
                }
            }

            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Upload Success"));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'view-application', 'id'=>$formData['id']),'default',true));
        }else{
            //redirect
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'processing-student'),'default',true));
        }
    }
    
    public function deleteuplAction(){
        $id = $this->_getParam('id');
        $sa_id = $this->_getParam('sa_id');

        $this->SponsorModel->deleteUpl($id);

        //redirect
        $this->_helper->flashMessenger->addMessage(array('success' => "Upload Success"));
        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'view-application', 'id'=>$sa_id),'default',true));
    }
    
    public function printAction(){
        $id = $this->_getParam('id');
        $this->printApp($id);
        exit;
    }

    public function printApp($id){
        //$id = $this->_getParam('id');

        global $globalinfo;
        global $globalProgramList;
        global $globalstudentinfo;
        global $globaleducation;
        global $globalindustryW;
        global $globalsemesterInfo;
        global $globalhead;
        //var_dump($this->studentInfo); exit;
        $globalstudentinfo = $this->SponsorModel->viewApp($id);

        $info = $this->SponsorModel->viewApp($id);
        $globalinfo = $info;

        $programList = $this->SponsorModel->getProgramList();
        $globalProgramList = $programList;

        $semesterInfo = $this->SponsorModel->getSemesterById($info['sa_semester_id']);
        $globalsemesterInfo = $semesterInfo;

        $head = $this->SponsorModel->getHeadTemplate($info['IdProgram']);
        $globalhead = $head;

        //get highest education
        if ($info['IdScheme']==1){
            $education = $this->SponsorModel->getHighestLevelEdu($info['sp_id']);
            $globaleducation = $education;
        }else{
            $education = $this->SponsorModel->getHighestLevelEdu2($info['IdProgram']);

            $registered_semester = $this->SponsorModel->getListSemesterRegistered($info['IdStudentRegistration']);
            $student_grade = false;
            foreach($registered_semester as $index=>$semester){
                //get subject registered in each semester
                if($semester["IsCountable"]==1){				
                    $subject_list = $this->SponsorModel->getListCourseRegisteredBySemesterWithAttendanceStatus($info['IdStudentRegistration'],$semester['IdSemesterMain']);
                }else{			
                    $subject_list = $this->SponsorModel->getListCourseRegisteredBySemesterWithoutAttendance($info['IdStudentRegistration'],$semester['IdSemesterMain']);
                }
                //var_dump($subject_list);
                $registered_semester[$index]['subjects']=$subject_list;

                //get grade info
                $student_grade = $this->SponsorModel->getStudentGrade($info['IdStudentRegistration'],$semester['IdSemesterMain']);
            }

            if (!$student_grade){
                $student_grade['sg_cum_credithour']=0;
                $student_grade['sg_cgpa']=0;
            }

            $education['ae_result'] = $student_grade['sg_cgpa'];

            $globaleducation = $education;
        }

        $industryW = $this->SponsorModel->getIndustryWeightageList();
        $globalindustryW = $industryW;

        $fieldValues = array(
            '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.png"
        );
        //var_dump($fieldValues); exit;
        require_once 'dompdf_config.inc.php';
        error_reporting(0);	

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        //template path	 
        $html_template_path = DOCUMENT_PATH."/template/scholarship.html";

        $html = file_get_contents($html_template_path);

        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);	
        }
        //echo $html; exit;
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();
        $output_filename = "financial_assistance.pdf";
        $dompdf->stream($output_filename);

        exit;
    }
    
    public function exportApplicationAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        $model = new Studentfinance_Model_DbTable_Scholarship();
        $apmodel = new icampus_Function_Application_AcademicProgress();
        
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            //var_dump($post_data); exit;

            if ($post_data['Program'] == ''){
                //redirect
                $this->_helper->flashMessenger->addMessage(array('error' => "Please Select Programme In Search Box Before Export!"));
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'processing-student'),'default',true));
            }

            $programInfo = $model->getProgramById($post_data['Program']);
            $this->view->programInfo = $programInfo;

            $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed2(1, $post_data);

            $grade = $model->getGradeSetup($post_data['Program']);

            if ($post_data['Program'] == 2){
                $grade[] = array(
                    'DefinitionCode'=>'CT',
                    'DefinitionDesc'=>'CT'
                );
            }

            $this->view->grade = $grade;
            
            if ($sponsor_applications){
                foreach ($sponsor_applications as $key => $sponsor_applicationsLoop){
                    $score = $this->countTotalScore($sponsor_applicationsLoop['sa_id']);
                    $sponsor_applications[$key]['score'] = $score;
                    
                    $transcriptInfo = $apmodel->getExpectedGraduateStudent($sponsor_applicationsLoop['IdStudentRegistration'], 'financialaid');
                    $sponsor_applications[$key]['transcript_info'] = $transcriptInfo;
                }
            }
            //exit;
            $this->view->sponsor_application = $sponsor_applications;
        }
        
        $this->view->status = $this->SponsorApplication->status;
        $this->view->filename = date('Ymd').'_list_of_student_sponsorship_application.xls';
    }
    
    public function exportReapplicationAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        $model = new Studentfinance_Model_DbTable_Scholarship();
        $apmodel = new icampus_Function_Application_AcademicProgress();
        
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();

            if ($post_data['Program'] == ''){
                //redirect
                $this->_helper->flashMessenger->addMessage(array('error' => "Please Select Programme In Search Box Before Export!"));
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'reapplication'),'default',true));
            }

            $programInfo = $model->getProgramById($post_data['Program']);
            $this->view->programInfo = $programInfo;

            $grade = $model->getGradeSetup($post_data['Program']);

            if ($post_data['Program'] == 2){
                $grade[] = array(
                    'DefinitionCode'=>'CT',
                    'DefinitionDesc'=>'CT'
                );
            }

            $this->view->grade = $grade;

            $sponsor_applications = $this->SponsorModel->viewReAppList($post_data);
            
            if ($sponsor_applications){
                foreach ($sponsor_applications as $key => $sponsor_applicationsLoop){
                    $transcriptInfo = $apmodel->getExpectedGraduateStudent($sponsor_applicationsLoop['IdStudentRegistration'], 'financialaid');
                    $sponsor_applications[$key]['transcript_info'] = $transcriptInfo;
                    //var_dump($sponsor_applications[$key]);
                }
            }
            //exit;
            $this->view->sponsor_application = $sponsor_applications;
        }
        
        $this->view->filename = date('Ymd').'_list_of_student_sponsorship_reapplication.xls';
    }
    
    public function countTotalScore($id){
        $info = $this->SponsorModel->viewApp($id);

        //count industry
        if ($info['empstatus']==2){
            $industyScore = 10;
        }else{
            $getIndutryW = $this->SponsorModel->getIndustryWeightage($info['industry']);

            if ($getIndutryW){
                $industyScore = $getIndutryW['score'];
            }else{
                $industyScore = 0;
            }
        }

        //count age
        $age = date('Y-m-d')-date('Y-m-d', strtotime($info['appl_dob']));
        $getAgeW = $this->SponsorModel->getAgeWeightage($age);

        if ($getAgeW){
            $ageScore = $getAgeW['score'];
        }else{
            $ageScore = 0;
        }

        //contribution score
        $cs = $industyScore+$ageScore;

        //academic merit
        if ($info['cgpa'] >= 3.5){
            $getacademicW = $this->SponsorModel->getAcademicWeightage(1);
        }else if ($info['cgpa'] <= 3.49 && $info['cgpa'] >=3.0){
            $getacademicW = $this->SponsorModel->getAcademicWeightage(2);
        }else if ($info['cgpa'] <= 2.99 && $info['cgpa'] >=2.5){
            $getacademicW = $this->SponsorModel->getAcademicWeightage(3);
        }else{
            $getacademicW = $this->SponsorModel->getAcademicWeightage(4);
        }

        $academicScore = $getacademicW['score'];

        //affordability (income)
        $incomeScore = 0; 

        if ($info['empstatus']!=2){
            $getincomeW = $this->SponsorModel->getIncomeWeightage($info['income'], $info['currency']);

            if ($getincomeW){
                $incomeScore = $getincomeW['score'];
            }
        }else{
            /*$getincomeW = $this->SponsorModel->getIncomeWeightage(0.00, $info['currency']);
            
            if ($getincomeW){
                $incomeScore = $getincomeW['score'];
            }*/
            
            $incomeScore = 30;
        }

        //affordability (dependent)
        $getDependentW = $this->SponsorModel->getDependentWeightage($info['totaldependent']);

        $dependentScore = 0;

        if ($getDependentW){
            $dependentScore = $getDependentW['score'];
        }

        //affordability score
        $as = $incomeScore+$dependentScore;

        //total score
        $ts = (0.2*$cs)+(0.3*$academicScore)+(0.5*$as);
        
        $scoreArr = array(
            'industry_score'=>$industyScore,
            'age_score'=>$ageScore,
            'contribution_score'=>$cs,
            'academic_merit'=>$academicScore,
            'academic_score'=>$academicScore,
            'income_score'=>$incomeScore,
            'dependent_score'=>$dependentScore,
            'affordability'=>$as,
            'total_score'=>$ts
        );
        
        return $scoreArr;
    }
    
    public function applicationCourseAction(){
        $this->view->title = $this->view->translate('Course Selection');
        $id = $this->_getParam('id');

        $info = $this->SponsorModel->viewApp($id);
        $getUserIdentity = $this->auth->getIdentity();
        //var_dump($info);
        $coveredCourseInfo = $this->SponsorModel->getCourseCovered($info['sa_semester_id']);

        if ($coveredCourseInfo){
            $coveredCourse = $coveredCourseInfo['total_course'];
        }else{
            $coveredCourse = 0;
        }

        $this->view->coveredCourse = $coveredCourse;

        $db = getDB();
        $select = $db->select()
            ->from(array('a' =>'tbl_studentregsubjects'))
            ->joinLeft(array('s'=>'tbl_subjectmaster'),'a.IdSubject=s.IdSubject',array('s.SubjectName','s.SubCode'))
            ->where('a.IdStudentRegistration = ?', $info['IdStudentRegistration'])
            ->where('a.IdSemesterMain = ?', $info['sa_semester_id'])
            ->where('a.Active <> ?', 3)
            ->where('a.IdSubject <> ?', 84)
            ->where('a.IdSubject <> ?', 85)
            ->where('a.IdSubject <> ?', 126);

        $courses = $db->fetchAll($select);

        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            if (isset($formData['sub']) && count($formData['sub']) > 0){

                //$this->SponsorModel->deleteSubjectApp($id);
                $dataarr = array();

                foreach ($formData['sub'] as $key => $subloop){

                    $checksub = $this->scholarshipModel->checkCourse($id, $key, $subloop);

                    if (!$checksub) {
                        $datasub = array(
                            'sac_said' => $id,
                            'sac_subjectid' => $subloop,
                            'sac_regsubid' => $key,
                            'sac_upddate' => date('Y-m-d H:i:s'),
                            'sac_updby' => $info['IdStudentRegistration'],
                            'sac_type' => 0
                        );
                        $this->SponsorModel->storeSubjectApp($datasub);

                        $this->SponsorApplication->set_status($id, 7);
                    }
                    array_push($dataarr , $key);
                }

                $courseList2 = $this->SponsorModel->getApplicationCourse($id);

                if ($courseList2){
                    foreach ($courseList2 as $courseLoop2){
                        if (!in_array($courseLoop2['sac_regsubid'] ,$dataarr)){
                            //todo cancel bayaran
                            $this->cancelSponsorInvoice($courseLoop2['sac_receiptid']);

                            $this->scholarshipModel->deleteSponsorSubject($courseLoop2['sac_id']);
                        }
                    }
                }

                $courseList3 = $this->SponsorModel->getApplicationCourse($id, true);

                if ($courseList3) {
                    foreach ($courseList3 as $courseLoop3) { //subject loop start
                        if ($courseLoop3['sac_receiptid']==0){
                            $arrinvdet = array();
                            $totalamount = 0;
                            $itemList = $this->scholarshipModel->getSubjectItem($courseLoop3['sac_regsubid']);

                            if ($itemList) {
                                foreach ($itemList as $itemLoop) {
                                    if ($itemLoop['invoice_id'] != null) {
                                        $invoiceList = $this->scholarshipModel->getInvoiceDetails($itemLoop['invoice_id'], $courseLoop3['sac_subjectid']);
                                        $invmaininfo = $this->scholarshipModel->getInvoiceMain($itemLoop['invoice_id']);

                                        $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                                        $currency = $currencyDb->getCurrentExchangeRate($invmaininfo['currency_id']);

                                        if ($invoiceList) {
                                            foreach ($invoiceList as $invoiceLoop) {

                                                $financialaidcoverage = $this->scholarshipModel->financialAidCoverage($invoiceLoop['fi_id']);

                                                //todo generate bayaran
                                                if ($financialaidcoverage) {
                                                    if ($financialaidcoverage['calculation_mode'] == 1) { //amount
                                                        if ($financialaidcoverage['amount'] > $invoiceLoop['amt']) {
                                                            if ($invmaininfo['currency_id'] != 1){
                                                                $amount = ($invoiceLoop['amt']*$currency['cr_exchange_rate']);
                                                            }else{
                                                                $amount = $invoiceLoop['amt'];
                                                            }
                                                        } else {
                                                            if ($invmaininfo['currency_id'] != 1){
                                                                $amount = ($financialaidcoverage['amount']*$currency['cr_exchange_rate']);
                                                            }else{
                                                                $amount = $financialaidcoverage['amount'];
                                                            }
                                                        }
                                                    } else { //percentage
                                                        $amount = (($financialaidcoverage['amount'] / 100) * $invoiceLoop['amt']);
                                                    }

                                                    $totalamount = ($totalamount + $amount);

                                                    $desc = $courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'] . ', ' . $invoiceLoop['fee_item_description'];

                                                    $cnDetailData = array(
                                                        //'subregdetid' =>  $itemLoop['id'],
                                                        'sp_invoice_id' => $invoiceLoop['invoice_main_id'],
                                                        'sp_invoice_det_id' => $invoiceLoop['id'],
                                                        'sp_amount' => $amount,
                                                        'sp_balance' => $amount,
                                                        'sp_receipt' => $amount,
                                                        'sp_description' => $desc
                                                    );
                                                    $arrinvdet[] = $cnDetailData; //array push
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (count($arrinvdet) > 0){
                            $invoiceclass = new icampus_Function_Studentfinance_Invoice();
                            $bill_no =  $invoiceclass->getBillSeq(11, date('Y'));

                            $data_dn = array(
                                'sp_fomulir_id' => $bill_no,
                                'sp_txn_id' => $info['transaction_id'],
                                'sp_IdStudentRegistration' => $info['IdStudentRegistration'],
                                //'sp_batch_id' => $btch_id,
                                'sp_amount' => $totalamount,
                                'sp_balance' => $totalamount,
                                'sp_type' => 3,
                                'sp_type_id' => 1,
                                'sp_invoice_date' => date('Y-m-d'),
                                'sp_description' => 'Financial Assistance',
                                'sp_currency_id' => $invmaininfo['currency_id'],
                                'sp_exchange_rate' => $invmaininfo['exchange_rate'],
                                'sp_creator'=>$getUserIdentity->id,
                                'sp_create_date'=>date('Y-m-d H:i:s'),
                                'sp_status' => 'E',//entry
                            );
                            $spinvmainid = $this->scholarshipModel->saveSponsorInvMain($data_dn);

                            //todo generate receipt
                            $receipt_no = $this->getReceiptNoSeq();

                            $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();

                            $receipt_data = array(
                                'rcp_no' => $receipt_no,
                                'rcp_account_code' => 96,
                                'rcp_date' => date('Y-m-d H:i:s'),
                                'rcp_receive_date' => date('Y-m-d'),
                                'rcp_batch_id' => 0,
                                'rcp_type' => 3,
                                'rcp_type_id' => 1,
                                'rcp_description' => 'Fisabilillah - '.$info['SemesterMainName'].' - '.$courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'],
                                'rcp_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_gainloss' => 0,
                                'rcp_gainloss_amt' => 0,
                                'rcp_cur_id' => $invmaininfo['currency_id'],
                                'rcp_exchange_rate' => $currency['cr_id'],
                                'p_payment_mode_id' => 20,
                                'p_cheque_no' => null,
                                'p_doc_bank' => null,
                                //'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                                'p_terminal_id' => null,
                                'p_card_no' => null,
                                'rcp_create_by'=>$getUserIdentity->id
                            );
                            $receipt_id = $receiptDb->insert($receipt_data);

                            //todo update receipt id
                            $receiptData = array(
                                'sac_receiptid'=>$receipt_id
                            );
                            $this->scholarshipModel->updateApplicationCourse($receiptData, $courseLoop3['sac_id']);

                            $data_rcp_stud = array(
                                'rcp_inv_rcp_id' => $receipt_id,
                                'IdStudentRegistration' => $info['IdStudentRegistration'],
                                'rcp_inv_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                'rcp_inv_create_by'=>$getUserIdentity->id
                            );
                            $db->insert('sponsor_invoice_receipt_student',$data_rcp_stud);
                            $idStd = $db->lastInsertId();

                            $receiptInvoiceDb = new Studentfinance_Model_DbTable_SponsorReceiptInvoice();

                            foreach ($arrinvdet as $arrinvdetLoop){
                                $arrinvdetLoop['sp_id']=$spinvmainid;

                                $spinvdetailid = $this->scholarshipModel->saveSponsorInvDetail($arrinvdetLoop);

                                $data_rcp_inv = array(
                                    'rcp_inv_rcp_id' => $receipt_id,
                                    'rcp_inv_sp_id' => $spinvmainid,
                                    'rcp_inv_student' => $idStd,
                                    'rcp_inv_sponsor_dtl_id' => $spinvdetailid,
                                    'rcp_inv_amount' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_amount_default' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                    'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                    'rcp_inv_create_by'=>$getUserIdentity->id
                                );
                                $receiptInvoiceDb->insert($data_rcp_inv);
                            }

                            //approve invoice
                            $this->approveSponsorInvoive($receipt_id);
                        }
                    } //subject loop end
                }
            }else{
                $this->SponsorModel->deleteSubjectApp($id);
            }

            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Data Updated"));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'application-course', 'id'=>$id),'default',true));
        }

        $this->view->courses = $courses;
        
        $courseList = $this->SponsorModel->getApplicationCourse($id);
        $this->view->courseList = $courseList;
        
        $courseArr = array();
        
        if ($courseList){
            foreach ($courseList as $courseLoop){
                array_push($courseArr, $courseLoop['IdSubject']);
            }
        }
        
        $this->view->courseArr = $courseArr;
    }
    
    public function reapplicationCourseAction(){
        $this->view->title = $this->view->translate('Course Selection');
        $id = $this->_getParam('id');

        $getUserIdentity = $this->auth->getIdentity();

        $info = $this->SponsorModel->getReApplication($id);

        $coveredCourseInfo = $this->SponsorModel->getCourseCovered($info['sre_semester_id']);

        if ($coveredCourseInfo){
            $coveredCourse = $coveredCourseInfo['total_course'];
        }else{
            $coveredCourse = 0;
        }

        $this->view->coveredCourse = $coveredCourse;

        $db = getDB();
        $select = $db->select()
            ->from(array('a' =>'tbl_studentregsubjects'))
            ->joinLeft(array('s'=>'tbl_subjectmaster'),'a.IdSubject=s.IdSubject',array('s.SubjectName','s.SubCode'))
            ->where('a.IdStudentRegistration = ?', $info['IdStudentRegistration'])
            ->where('a.IdSemesterMain = ?', $info['sre_semester_id'])
            ->where('a.Active <> ?', 3)
            ->where('a.IdSubject <> ?', 84)
            ->where('a.IdSubject <> ?', 85)
            ->where('a.IdSubject <> ?', 126);

        $courses = $db->fetchAll($select);

        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            //var_dump($formData); exit;

            if (isset($formData['sub']) && count($formData['sub']) > 0){
                
                //$this->SponsorModel->deleteSubjectReApp($id);

                $dataarr = array();
                
                foreach ($formData['sub'] as $key => $subloop){
                    $checksub = $this->scholarshipModel->checkReCourse($id, $key, $subloop);

                    if (!$checksub) {
                        $datasub = array(
                            'src_sreid' => $id,
                            'src_subjectid' => $subloop,
                            'src_regsubid' => $key,
                            'src_upddate' => date('Y-m-d H:i:s'),
                            'src_updby' => $info['IdStudentRegistration'],
                            'src_type' => 0
                        );
                        $this->SponsorModel->insertSponsorCourse($datasub);

                        $this->SponsorModel->set_restatus($id, 7);
                    }

                    array_push($dataarr, $key);
                }

                $courseList2 = $this->SponsorModel->getReApplicationCourse($id);

                if ($courseList2){
                    foreach ($courseList2 as $courseLoop2){
                        if (!in_array($courseLoop2['src_regsubid'] ,$dataarr)){
                            //todo cancel bayaran
                            $this->cancelSponsorInvoice($courseLoop2['src_receiptid']);

                            $this->scholarshipModel->deleteSponsorReSubject($courseLoop2['src_id']);
                        }
                    }
                }

                $courseList3 = $this->SponsorModel->getReApplicationCourse($id, true);

                //var_dump($courseList3); exit;

                if ($courseList3) {
                    foreach ($courseList3 as $courseLoop3) { //subject loop start
                        if ($courseLoop3['src_receiptid']==0){
                            $arrinvdet = array();
                            $totalamount = 0;
                            $itemList = $this->scholarshipModel->getSubjectItem($courseLoop3['src_regsubid']);
                            //var_dump($itemList);
                            if ($itemList) {
                                foreach ($itemList as $itemLoop) {
                                    if ($itemLoop['invoice_id'] != null) {
                                        $invoiceList = $this->scholarshipModel->getInvoiceDetails($itemLoop['invoice_id'], $courseLoop3['src_subjectid']);
                                        $invmaininfo = $this->scholarshipModel->getInvoiceMain($itemLoop['invoice_id']);
                                        //var_dump($invoiceList);
                                        $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                                        $currency = $currencyDb->getCurrentExchangeRate($invmaininfo['currency_id']);

                                        if ($invoiceList) {
                                            foreach ($invoiceList as $invoiceLoop) {

                                                $financialaidcoverage = $this->scholarshipModel->financialAidCoverage($invoiceLoop['fi_id']);

                                                //todo generate bayaran
                                                if ($financialaidcoverage) {
                                                    if ($financialaidcoverage['calculation_mode'] == 1) { //amount
                                                        if ($financialaidcoverage['amount'] > $invoiceLoop['amt']) {
                                                            if ($invmaininfo['currency_id'] != 1){
                                                                $amount = ($invoiceLoop['amt']*$currency['cr_exchange_rate']);
                                                            }else{
                                                                $amount = $invoiceLoop['amt'];
                                                            }
                                                        } else {
                                                            if ($invmaininfo['currency_id'] != 1){
                                                                $amount = ($financialaidcoverage['amount']*$currency['cr_exchange_rate']);
                                                            }else{
                                                                $amount = $financialaidcoverage['amount'];
                                                            }
                                                        }
                                                    } else { //percentage
                                                        $amount = (($financialaidcoverage['amount'] / 100) * $invoiceLoop['amt']);
                                                    }

                                                    $totalamount = ($totalamount + $amount);

                                                    $desc = $courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'] . ', ' . $invoiceLoop['fee_item_description'];

                                                    $cnDetailData = array(
                                                        //'subregdetid' =>  $itemLoop['id'],
                                                        'sp_invoice_id' => $invoiceLoop['invoice_main_id'],
                                                        'sp_invoice_det_id' => $invoiceLoop['id'],
                                                        'sp_amount' => $amount,
                                                        'sp_balance' => $amount,
                                                        'sp_receipt' => $amount,
                                                        'sp_description' => $desc
                                                    );
                                                    $arrinvdet[] = $cnDetailData; //array push
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //var_dump($arrinvdet); exit;
                        if (count($arrinvdet) > 0){
                            $invoiceclass = new icampus_Function_Studentfinance_Invoice();
                            $bill_no =  $invoiceclass->getBillSeq(11, date('Y'));

                            $data_dn = array(
                                'sp_fomulir_id' => $bill_no,
                                'sp_txn_id' => $info['transaction_id'],
                                'sp_IdStudentRegistration' => $info['IdStudentRegistration'],
                                //'sp_batch_id' => $btch_id,
                                'sp_amount' => $totalamount,
                                'sp_balance' => $totalamount,
                                'sp_type' => 3,
                                'sp_type_id' => 1,
                                'sp_invoice_date' => date('Y-m-d'),
                                'sp_description' => 'Financial Assistance',
                                'sp_currency_id' => $invmaininfo['currency_id'],
                                'sp_exchange_rate' => $invmaininfo['exchange_rate'],
                                'sp_creator'=>$getUserIdentity->id,
                                'sp_create_date'=>date('Y-m-d H:i:s'),
                                'sp_status' => 'E',//entry
                            );
                            $spinvmainid = $this->scholarshipModel->saveSponsorInvMain($data_dn);

                            //todo generate receipt
                            $receipt_no = $this->getReceiptNoSeq();

                            $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();

                            $receipt_data = array(
                                'rcp_no' => $receipt_no,
                                'rcp_account_code' => 96,
                                'rcp_date' => date('Y-m-d H:i:s'),
                                'rcp_receive_date' => date('Y-m-d'),
                                'rcp_batch_id' => 0,
                                'rcp_type' => 3,
                                'rcp_type_id' => 1,
                                'rcp_description' => 'Fisabilillah - '.$info['SemesterMainName'].' - '.$courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'],
                                'rcp_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_gainloss' => 0,
                                'rcp_gainloss_amt' => 0,
                                'rcp_cur_id' => $invmaininfo['currency_id'],
                                'rcp_exchange_rate' => $currency['cr_id'],
                                'p_payment_mode_id' => 20,
                                'p_cheque_no' => null,
                                'p_doc_bank' => null,
                                //'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                                'p_terminal_id' => null,
                                'p_card_no' => null,
                                'rcp_create_by'=>$getUserIdentity->id
                            );
                            $receipt_id = $receiptDb->insert($receipt_data);

                            //todo update receipt id
                            $receiptData = array(
                                'src_receiptid'=>$receipt_id
                            );
                            $this->scholarshipModel->updateReApplicationCourse($receiptData, $courseLoop3['src_id']);

                            $data_rcp_stud = array(
                                'rcp_inv_rcp_id' => $receipt_id,
                                'IdStudentRegistration' => $info['IdStudentRegistration'],
                                'rcp_inv_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                'rcp_inv_create_by'=>$getUserIdentity->id
                            );
                            $db->insert('sponsor_invoice_receipt_student',$data_rcp_stud);
                            $idStd = $db->lastInsertId();

                            $receiptInvoiceDb = new Studentfinance_Model_DbTable_SponsorReceiptInvoice();

                            foreach ($arrinvdet as $arrinvdetLoop){
                                $arrinvdetLoop['sp_id']=$spinvmainid;

                                $spinvdetailid = $this->scholarshipModel->saveSponsorInvDetail($arrinvdetLoop);

                                $data_rcp_inv = array(
                                    'rcp_inv_rcp_id' => $receipt_id,
                                    'rcp_inv_sp_id' => $spinvmainid,
                                    'rcp_inv_student' => $idStd,
                                    'rcp_inv_sponsor_dtl_id' => $spinvdetailid,
                                    'rcp_inv_amount' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_amount_default' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                    'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                    'rcp_inv_create_by'=>$getUserIdentity->id
                                );
                                $receiptInvoiceDb->insert($data_rcp_inv);
                            }

                            //approve invoice
                            $this->approveSponsorInvoive($receipt_id);
                        }
                    } //subject loop end
                }
            }else{
                $this->SponsorModel->deleteSubjectReApp($id);
            }

            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Data Updated"));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'reapplication-course', 'id'=>$id),'default',true));
        }

        $this->view->courses = $courses;
        
        $courseList = $this->SponsorModel->getReApplicationCourse($id);
        $this->view->courseList = $courseList;
        
        $courseArr = array();
        
        if ($courseList){
            foreach ($courseList as $courseLoop){
                array_push($courseArr, $courseLoop['IdSubject']);
            }
        }
        
        $this->view->courseArr = $courseArr;
    }

    public function getReceiptNoSeq(){

        $seq_data = array(
            12,
            date('Y'),
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }

    public function updateFisbilillahEntryAction(){

    }

    public function approveSponsorInvoive($id){
        //receipt
        $sponsorreceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $receipt = $sponsorreceiptDb->getData($id);

        $receipt = $receipt[0];

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        /*
         * knockoff invoice
         */

        if ($receipt['rcp_status'] == 'ENTRY') {
            $sponsorinvoiceMainDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $sponsorinvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
            $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();


            foreach ($receipt['receipt_student'] as $rcpStudent) {

                $totalSponsorAmount = $rcpStudent['rcp_inv_amount'];

                $appTrxId = $rcpStudent['IdStudentRegistration'];
                $txnStudentDb = new Registration_Model_DbTable_Studentregistration();
                $txnProfile = $txnStudentDb->getData($appTrxId);

                //get current semester
                $semesterDb = new Records_Model_DbTable_SemesterMaster();
                $semesterInfo = $semesterDb->getStudentCurrentSemester($txnProfile);

                $totalAmount = 0.00;
                $total_payment_amount = $receipt['rcp_amount'];
                $total_paid_receipt = 0;
                $paidReceiptInvoice = 0;
                $paidReceipt = 0;

                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    $balanceNew1 = 0;
                    $paidNew1 = 0;
                    $balanceNew = 0;
                    $paidNew = 0;
                    $balance = 0;
                    $amountDN = 0;
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['rcp_inv_sp_id'];//sponsor_invoice_main
                        $invDetID = $inv['rcp_inv_sponsor_dtl_id'];//sponsor_invoice_detail
                        $rcpCurrency = $receipt['rcp_cur_id'];

                        $invDetData = $sponsorinvoiceDetailDb->getData($invDetID);

                        $invCurrency = $invDetData['currency_id'];

                        $currencyDb = new Studentfinance_Model_DbTable_Currency();
                        $currency = $currencyDb->fetchRow('cur_id = ' . $invCurrency)->toArray();

                        $amountDefault = $invDetData['sp_balance_det'];

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                        $paidReceipt = $inv['rcp_inv_amount'];

                        if ($invCurrency != 1) {
                            //$dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                            //$paidReceipt = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                            $paidReceipt = round($inv['rcp_inv_amount'],2);
                        }

                        $balance = $amountDefault - $paidReceipt;
                        $totalSponsorAmount -= $paidReceipt;

                        //update invoice details
                        $dataInvDetail = array(
                            'sp_paid' => $paidReceipt + $invDetData['sp_paid_det'],
                            'sp_balance' => $balance,
                        );

                        $sponsorinvoiceDetailDb->update($dataInvDetail, array('id =?' => $invDetID));

                        $totalAmount += $paidReceiptInvoice;

                        //update invoice main
                        $invMainData = $sponsorinvoiceMainDb->getData($invID);

                        $balanceMain = ($invMainData['sp_balance']) - ($paidReceipt);
                        $paidMain = ($invMainData['sp_paid']) + ($paidReceipt);

                        //update invoice main
                        $dataInvMain = array(
                            'sp_paid' => $paidMain,
                            'sp_balance' => $balanceMain,
                            'sp_approve_by' => $getUserIdentity->id,
                            'sp_approve_date' => date('Y-m-d H:i:s')
                        );

                        $sponsorinvoiceMainDb->update($dataInvMain, array('sp_id =?' => $invID));
                        $invDetMainID = $inv['idDet'];
                        $invMainId = $inv['idMain'];

                        //update invoice_main & invoice_detail
                        $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                        $invDet = $invoiceDetailDB->getData($invDetMainID);

                        $amountDN = $inv['rcp_inv_amount'];
                        if ($invDet['cur_id'] != 1) {
                            //$dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                            //$amountDN = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                            $amountDN = round($inv['rcp_inv_amount'],2);
                        }

                        $balanceNew1 = $invDet['balance'] - $amountDN;
                        $paidNew1 = $invDet['paid'] + $amountDN;
                        $dataUpdDetail = array('balance' => $balanceNew1, 'sp_id' => $invDetID, 'paid' => $paidNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetMainID));

                        //update amount sponsor at invoice_main

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invMainId);

                        $balanceNew = $invMain['bill_balance'] - $amountDN;
                        $paidNew = $invMain['bill_paid'] + $amountDN;

                        $dataUpdMain = array('bill_balance' => $balanceNew, 'bill_paid' => $paidNew, 'upd_by' => $creator, 'upd_date' => date('Y-m-d H:i:s'));
                        $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invMainId));

                    }
                }

                if ($totalSponsorAmount > 0 || $rcpStudent['rcp_adv_amount'] != 0) {

                    //generetae advance payment
                    if ($rcpStudent['rcp_adv_amount'] > 0) {
                        $total_advance_payment_amount = $rcpStudent['rcp_adv_amount'];
                    } else {
                        $total_advance_payment_amount = $totalSponsorAmount;
                    }

                    $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
                    $advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

                    $bill_no = $this->getBillSeq(8, date('Y'));

                    $advance_payment_data = array(
                        'advpy_fomulir' => $bill_no,
                        'advpy_appl_id' => 0,
                        'advpy_trans_id' => 0,
                        'advpy_idStudentRegistration' => $rcpStudent['IdStudentRegistration'],
                        'advpy_sem_id' => $semesterInfo['IdSemesterMaster'],
                        'advpy_rcp_id' => $receipt['rcp_id'],
                        'advpy_description' => 'Advance payment from receipt no:' . $receipt['rcp_no'],
                        'advpy_cur_id' => $receipt['rcp_cur_id'],
                        'advpy_amount' => $total_advance_payment_amount,
                        'advpy_total_paid' => 0.00,
                        'advpy_total_balance' => $total_advance_payment_amount,
                        'advpy_status' => 'A',
                        'advpy_date' => date('Y-m-d'),

                    );


                    $advPayID = $advancePaymentDb->insert($advance_payment_data);

                    $advance_payment_det_data = array(
                        'advpydet_advpy_id' => $advPayID,
                        'advpydet_total_paid' => 0.00
                    );

                    $advancePaymentDetailDb->insert($advance_payment_det_data);

                    $dataAdv = array('rcp_adv_amount' => $total_advance_payment_amount);
                    $db->update('sponsor_invoice_receipt_student', $dataAdv, $db->quoteInto("rcp_inv_id = ?", $rcpStudent['rcp_inv_id']));

                }
            }

        }

        /*knockoff end*/


        //change receipt status to APPROVE
        if ($receipt['rcp_status'] == 'ENTRY') {

            foreach ($receipt['receipt_student'] as $rcpStudent) {
                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['idMain'];
                        $invDetMainID = $inv['idDet'];

                        if ($invID) {
                            if ($balanceNew == 0) {
                                $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                                $paymentClass->updateCourseStatus($invID);

                            }
                        }
                    }
                }
            }

            $sponsorreceiptDb->update(
                array(
                    'rcp_status' => 'APPROVE',
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => $getUserIdentity->id
                ), 'rcp_id = ' . $receipt['rcp_id']);

            return array('type' => 'success', 'message' => 'Receipt Approved');
        } else {
            return array('type' => 'error', 'message' => 'Receipt Already Approved');
        }
    }

    private function getBillSeq($type, $year)
    {

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }

    public function cancelSponsorInvoice($receiptid){
        $model = new Registration_Model_DbTable_SeekingForLandscape();

        $sponsorinvreceipt = $model->getSponsorInvReceipt($receiptid);

        $sponsorinvreceiptinv = $model->sponsorInvReceiptInv($receiptid);

        if ($sponsorinvreceiptinv){
            foreach ($sponsorinvreceiptinv as $loop2){
                $sponsorInvDetail = $model->sponsorInvDetail($loop2['rcp_inv_sponsor_dtl_id']);

                $invoiceDetail = $model->invoiceDetail($sponsorInvDetail['sp_invoice_det_id']);

                $paidInvDetails = $invoiceDetail['paid']-$sponsorInvDetail['sp_paid'];
                $balanceInvDetails = $invoiceDetail['balance']+$sponsorInvDetail['sp_paid'];

                $invoiceDetailData = array(
                    'paid'=> $paidInvDetails,
                    'balance'=> $balanceInvDetails
                );
                $model->updateInvoiceDetail($invoiceDetailData, $invoiceDetail['id']);

                $invoiceMain = $model->invoiceMain($sponsorInvDetail['sp_invoice_id']);

                $paidInvMain = $invoiceMain['bill_paid']-$sponsorInvDetail['sp_paid'];
                $balanceInvMain = $invoiceMain['bill_balance']+$sponsorInvDetail['sp_paid'];

                $invoiceMainData = array(
                    'bill_paid'=> $paidInvMain,
                    'bill_balance'=> $balanceInvMain
                );
                $model->updateInvoiceMain($invoiceMainData, $invoiceMain['id']);

                $cancelSponsorData = array(
                    'sp_status'=>'X',
                    'sp_cancel_by'=>1,
                    'sp_cancel_date'=>date('Y-m-d H:i:s')
                );
                $model->updateSponsorMain($cancelSponsorData, $sponsorInvDetail['sp_id']);
            }

            $cancelReceiptData = array(
                'rcp_status'=>'CANCEL',
                'cancel_by'=>1,
                'cancel_date'=>date('Y-m-d H:i:s')
            );
            $model->updateReceiptMain($cancelReceiptData, $sponsorinvreceipt['rcp_id']);
        }
    }

    public function previewOfferLetterAction(){
        $sa_id = $this->_getParam('id',null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $emailDb = new App_Model_System_DbTable_Email();
        $templateTagsCMS = new Cms_TemplateTags();

        $info = $this->SponsorModel->viewApp2($sa_id);

        $html = '';

        if ($info['IdProgram']==1){
            if ($info['appl_category']==579){
                $attachmentContent = $emailDb->getFinancialAidAttachment(273);
            }else{
                $attachmentContent = $emailDb->getFinancialAidAttachment(165);
            }
        }else if ($info['IdProgram']==2){
            //$attachmentContent = $emailDb->getFinancialAidAttachment(166);

            if ($info['appl_category']==579){
                $attachmentContent = $emailDb->getFinancialAidAttachment(273);
            }else{
                $attachmentContent = $emailDb->getFinancialAidAttachment(165);
            }
        }else{
            $attachmentContent = false;
        }

        if ($attachmentContent) {
            $html = $templateTagsCMS->parseContent($info['sa_cust_id'], $attachmentContent['tpl_id'], $attachmentContent['tpl_content']);
        }else{
            $html = "No Offer Letter Setup";
        }

        $json = Zend_Json::encode($html);

        echo $json;
        exit();
    }

    public function previewOfferLetterPdfAction(){
        $sa_id = $this->_getParam('id',null);
        $tpl_id = $this->_getParam('tpl_id',null);

        $emailDb = new App_Model_System_DbTable_Email();
        $templateTagsCMS = new Cms_TemplateTags();

        $info = $this->SponsorModel->viewApp2($sa_id);

        $html = '';

        $attachmentContent = $emailDb->getFinancialAidAttachment($tpl_id);

        if ($attachmentContent) {
            $html = $templateTagsCMS->parseContent($info['sa_cust_id'], $attachmentContent['tpl_id'], $attachmentContent['tpl_content']);
        }else{
            $html = "No Offer Letter Setup";
        }

        $name=$info['at_pes_id'].'_financialaid_'.date('YmdHis');

        $url = DOCUMENT_PATH.$info['at_repository'];

        //option pdf
        $option = array(
            'content' => $html,
            'save' => false,
            'file_extension' => 'pdf',
            'save_path' => $url,
            'file_name' => $name,
            'css' => '@page { margin: 110px 50px 50px 50px}
                body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
                table.tftable tr {background-color:#ffffff;}
                table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}

                ?li span { position: relative; left: -10px; }',
            'header' => '<script type="text/php">
                if ( isset($pdf) ) {
                    $header = $pdf->open_object();

                    $w = $pdf->get_width();
                    $h = $pdf->get_height();
                    $color = array(0,0,0);

                    $img_w = 180;
                    $img_h = 59;
                    $pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

                    // Draw a line along the bottom
                    $font = Font_Metrics::get_font("Helvetica");
                    $size = 6;
                    $text_height = Font_Metrics::get_font_height($font, $size)+2;
                    $y = $h - (3.5 * $text_height)-10;
                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                    // Draw a second line along the bottom
                    $y = $h - (3.5 * $text_height)+10;
                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                    $pdf->close_object();

                    $pdf->add_object($header, "all");
                }
                </script>',
            'footer' => '<script type="text/php">
                if ( isset($pdf) ) {
                    $footer = $pdf->open_object();

                    $font = Font_Metrics::get_font("Helvetica");
                    $size = 6;
                    $color = array(0,0,0);
                    $text_height = Font_Metrics::get_font_height($font, $size)+2;

                    $w = $pdf->get_width();
                    $h = $pdf->get_height();


                    // Draw a line along the bottom
                    $y = $h - (3.5 * $text_height)-10;
                    //$pdf->line(10, $y, $w - 10, $y, $color, 1);

                    //1st row footer
                    $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
                    $width = Font_Metrics::get_text_width($text, $font, $size);
                    $y = $h - (2 * $text_height)-20;
                    $x = ($w - $width) / 2.0;

                    $pdf->page_text($x, $y, $text, $font, $size, $color);

                    //2nd row footer
                    $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
                    $width = Font_Metrics::get_text_width($text, $font, $size);
                    $y = $h - (1 * $text_height)-20;
                    $x = ($w - $width) / 2.0;

                    $pdf->page_text($x, $y, $text, $font, $size, $color);



                    $pdf->close_object();

                    $pdf->add_object($footer, "all");
                }
                </script>'
        );

        //genarate attachment
        $pdf = generatePdf($option);

        exit;
    }
}