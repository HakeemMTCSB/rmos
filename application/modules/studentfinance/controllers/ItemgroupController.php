<?php
	//error_reporting(E_ALL|E_STRICT);
	//ini_set('display_errors', 'on');
class Studentfinance_ItemgroupController extends Base_Base { //Controller for the Item Group module
	private $lobjinitialconfig;	
	private $lobjbank;
	private $lobjItemgroup;
	private $lobjItemgroupForm;
	private $lobjMaintenance;
	private $_gobjlog;
	public function init() {  //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();	 
	}
	
	public function fnsetObj() {
		$this->lobjform = new App_Form_Search (); //local object for Search Form
		$this->lobjItemgroup = new Studentfinance_Model_DbTable_Itemgroup(); //local object for ItemGroup Model
		$this->lobjbank = new GeneralSetup_Model_DbTable_Bank(); //local object for Bank Model
		$this->lobjbankdetails = new GeneralSetup_Model_DbTable_Bankdetails(); //local object for Bank Details Model
		$this->lobjItemgroupForm = new Studentfinance_Form_Itemgroup(); //local object for ItemGroup Form
		$this->lobjMaintenance = new GeneralSetup_Model_DbTable_Maintenance();  //local object for Maintenance Model
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
		$this->gobjsessionsis = Zend_Registry::get('sis');
	}

public function indexAction() {   //Index Action
	
	    $this->view->lobjform = $this->lobjform; 
	     // Function to get details of ItsmGroup	
	    $larrresult =$this->lobjItemgroup->fnGetItemgroupDetails();
	   	
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionstudent->itemgroupdtlspaginatorresult); 
   	    $lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		 // Function to get ItemCode
		$larraccountgroup=$this->lobjItemgroup->fngetaccountgroup();
		//echo "<pre>";print_r($larraccountgroup);die();
		$this->lobjform->field5->addMultiOption('','Select');
		$this->lobjform->field5->addMultiOptions($larraccountgroup);
		
		
		$larritemcode=$this->lobjItemgroup->fngetitemcode();
		$this->lobjform->field1->addMultiOption('','Select');
		$this->lobjform->field1->addMultiOptions($larritemcode);
		
		 // Function to get ShortName
		$larrshortname=$this->lobjItemgroup->fngetshortname();
		$this->lobjform->field8->addMultiOption('','Select');
		$this->lobjform->field8->addMultiOptions($larrshortname);
		
			$larritembank=$this->lobjItemgroup->fngetbankiddetails();
			$this->lobjform->field19->addMultiOption('','Select');
			$this->lobjform->field19->addMultiOptions($larritembank);
		
		if(isset($this->gobjsessionstudent->itemgroupdtlspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->itemgroupdtlspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				// Function to search Itemgroup
				$larrresult = $this->lobjItemgroup->fnSearchItemGroup( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->itemgroupdtlspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/itemgroup/index');
		}	
}

public function newitemgroupAction() { //action to add new ItemGroup
	    $this->view->lobjItemgroupForm =$this->lobjItemgroupForm;
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjItemgroupForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjItemgroupForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		
		//FOR MAKING THE READONY FIELD
		if($this->view->ItemCode==1) {
			$this->view->lobjItemgroupForm->ItemCode->setValue('xxx-xxx-xxx');
			$this->view->lobjItemgroupForm->ItemCode->setAttrib('readonly','true');
		}
		
		//for filling the drop down for the bank
		$larritemgroup=$this->lobjItemgroup->fngetbankiddetails();
		$this->lobjItemgroupForm->IdBank->addMultiOptions($larritemgroup);
		
		//for filling the drop down for the account types
		/*$larraccounttype=$this->lobjItemgroup->fngetbankaccounttype();
		$this->lobjItemgroupForm->IdAccount->addMultiOptions($larraccounttype);*/
		
		$larraccountgroup=$this->lobjItemgroup->fngetaccountgroup();
		//echo "<pre>";print_r($larraccountgroup);die();
		//$this->lobjItemgroupForm->IdAccountGroup->addMultiOptions($larraccountgroup);
		self::getChildren();
		
		//http://stackoverflow.com/questions/4696222/how-can-i-get-all-children-from-a-parent-row-in-the-same-table
		//http://stackoverflow.com/questions/5720936/mysql-php-retrieve-leaf-children-with-path
		
		
		$larraccountgrouptest=$this->lobjItemgroup->fngetaccountgrouptest();	
		
		//print_r($larraccountgrouptest);
		
		if(count($larraccountgrouptest) > 0 ) {
			
					$ArraIdApplications['IdAccountGroup'][0] =  0;
					$ArraIdApplication[0]['IdAccountGroup'] =  0;
					$ArraIdApplication[0]['GroupName'] =  0;
					$ArraIdApplication[0]['IdParent'] =  0;
					$cnts = 0;
					for($i=0;$i<count($larraccountgrouptest);$i++){
						if(!in_array($larraccountgrouptest[$i]['IdAccountGroup'],$ArraIdApplications['IdAccountGroup'])){
							$ArraIdApplications['IdAccountGroup'][$cnts] = $larraccountgrouptest[$i]['IdAccountGroup'];
							$ArraIdApplication[$cnts]['IdAccountGroup'] = $larraccountgrouptest[$i]['IdAccountGroup'];
							$ArraIdApplication[$cnts]['GroupName'] = $larraccountgrouptest[$i]['GroupName'];
							$ArraIdApplication[$cnts]['IdParent'] = $larraccountgrouptest[$i]['IdParent'];
							$cnts++;
						}
					}
		}
		

	/*	
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjStudentApplicationForm->currentjoborganizationtype->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}*/
		
		
			if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			unset ( $larrformData ['Save'] );//
			if ($this->lobjItemgroupForm->isValid ( $larrformData )) {  			
			
				$firstlett=substr($larrformData['EnglishDescription'],0,1);				
				$result = $this->lobjItemgroup->fnInsertItemGroup($larrformData);
			
			if($this->view->ItemCode==1){
					
					if($this->view->Itembase == 0){ //level based
						
						if($this->view->itemlevelbase == 0) {//fixed		
												
							$larrformData['ItemCode'] = $this->view->FixeditemText.$this->view->FixeditemSeparator.$result;
						}
						if($this->view->itemlevelbase == 1) {				
							$larrformData['ItemCode'] = $firstlett.$this->view->FirstletteritemSeparator.$result;
						}
					}					
					if($this->view->Itembase == 1){//general based
						
						$larrformData['ItemCode']=$this->lobjItemgroup->fnGenerateCode($this->gobjsessionsis->idUniversity,$result);						
						
					}
		
				$this->lobjItemgroup->fnupdateItemGroup($result,$larrformData);	
							
			}			
	
			// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Revenue Item Setup  Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/itemgroup/index');	
        }  
    }  
}   
	
	public function edititemgroupAction(){  //action to edit ItemGroup
			$this->view->lobjItemgroupForm = $this->lobjItemgroupForm; //send the lobjuniversityForm object to the view
			if($this->view->ItemCode==1) {
				$this->view->lobjItemgroupForm->ItemCode->setAttrib('readonly','true');
			}
			$larraccountgroup=$this->lobjItemgroup->fngetaccountgroup();
			//$this->lobjItemgroupForm->IdAccountGroup->addMultiOptions($larraccountgroup);
			self::getChildren();
			$IdItem = $this->_getParam('id');
			
		    $this->view->lobjItemgroupForm->IdItem->setValue($IdItem);
	    	$larrresult = $this->lobjItemgroup->fnViewItemGroup($IdItem); 
	    	$this->view->IdAccount = $larrresult['IdAccount'];
	    	$this->view->bankid=$larrresult['IdAccountGroup'];
	    	$ldtsystemDate = date ( 'Y-m-d H:i:s' );
			$this->view->lobjItemgroupForm->UpdDate->setValue($ldtsystemDate );
			$auth = Zend_Auth::getInstance();
			$this->view->lobjItemgroupForm->UpdUser->setValue($auth->getIdentity()->iduser);
			
			$larritemgroup=$this->lobjItemgroup->fngetbankiddetails();
			$this->lobjItemgroupForm->IdBank->addMultiOptions($larritemgroup);
			
			
			
			$larraccounttype=$this->lobjItemgroup->fnGetBankAccountType($larrresult['IdBank']);
			$this->lobjItemgroupForm->IdAccount->addMultiOptions($larraccounttype);


		 	$this->lobjItemgroupForm->populate($larrresult);
	    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
	    		 	$larrformData = $this->_request->getPost ();
		    if ($this->lobjItemgroupForm->isValid($larrformData)) {		   
		   		$lintIdItem = $larrformData ['IdItem'];
				$result = $this->lobjItemgroup->fnupdateItemGroupEDit($larrformData,$lintIdItem);//update ItemGroup	

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Revenue Item Setup Edit Id=' . $IdItem,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->view->baseUrl . '/studentfinance/itemgroup/index');
			}
	    }
	}
	
	public function getaccounttypeAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdBank = $this->_getParam('idBank');		
		$larrBankDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjItemgroup->fnGetBankAccountType($IdBank));
		echo Zend_Json_Encoder::encode($larrBankDetails);
	}
	
	public function getChildren($parent= "", $x = 0) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $sql = "SELECT IdAccountGroup, GroupName FROM tbl_accountgroup WHERE IdParent = $x";
	   $rs = $lobjDbAdpt->fetchAll($sql);
	   foreach($rs as $obj){
	      if (self::hasChildren($obj['IdAccountGroup'])) {
	         self::getChildren($obj['GroupName'], $obj['IdAccountGroup']);
	      } else {
	      	if($parent) {
	      		$groupname = $parent.'-'.$obj['GroupName'];
	      	} else {
	      		$groupname = $obj['GroupName'];
	      	}
	      	
	      	$this->lobjItemgroupForm->IdAccountGroup->addMultiOption (  $obj['IdAccountGroup'], $obj['GroupName'].'('.$groupname.')');

	      }
	   }
	}
	
	public function hasChildren($x) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $sql = "SELECT * FROM tbl_accountgroup WHERE IdParent = $x";
	   $rs = $lobjDbAdpt->fetchAll($sql);
	   if (count($rs) > 0) {
	      return true;
	   } else {
	      return false;
	   }
	}
	
	
}