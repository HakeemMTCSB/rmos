<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 17/10/2016
 * Time: 3:25 PM
 */
class Studentfinance_SponsorReceiptUpdateController extends Zend_Controller_Action {

    private $_gobjlog;
    private $locale;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');

        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Update Utilization Date');

        $model = new Studentfinance_Model_DbTable_SponsorReceiptUpdate();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->formData = $formData;

            $list = $model->getSponsorReceipt($formData);
            $this->view->list = $list;
        }
    }

    public function updateDateAction(){
        $model = new Studentfinance_Model_DbTable_SponsorReceiptUpdate();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['utidate']) && count($formData['utidate']) > 0){
                foreach ($formData['utidate'] as $key => $date){
                    $data = array(
                        'rcp_receive_date' => date('Y-m-d', strtotime($date)),
                    );
                    $model->updateSponsorReceipt($data, $key);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => 'Date Updated'));
            $this->_redirect($this->baseUrl . '/studentfinance/sponsor-receipt-update/');
        }

        $this->_redirect($this->baseUrl . '/studentfinance/sponsor-receipt-update/');
        exit;
    }
}