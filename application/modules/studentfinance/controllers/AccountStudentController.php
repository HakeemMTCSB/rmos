<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_AccountStudentController extends Base_Base {
	
	private $_DbObj;
	private $_sis_session;
	
	public function init(){
		$db = new Application_Model_DbTable_Applicant();
		$this->_DbObj = $db;
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}
	
	public function ledgerAction(){
		
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$IdStudentRegistration = $this->_getParam('id', null);
		$this->view->idStudentRegistration = $IdStudentRegistration;
		
		//title
    	$this->view->title= $this->view->translate("Account Ledger");
		
    	
    	//student registration
    	$studentRegistrationDb = new App_Model_Record_DbTable_StudentRegistration();
    	$registration = $studentRegistrationDb->getStudentInfo($IdStudentRegistration);
    	$this->view->registration = $registration;
    	
    	//Student profile
    	$studentProfileDb = new Records_Model_DbTable_Studentprofile();
    	$profile = $studentProfileDb->fnViewStudentAppnDetails($registration['IdStudentRegistration']);
    	$this->view->profile = $profile;
    	
    	$appl_id = $registration['IdApplication'];
    	$IdStudentRegistration = $registration['IdStudentRegistration'];
    	
    	$paymentInfoHelper = new icampus_Function_Studentfinance_PaymentInfo();
    	$paymentInfoHelper->getStudentPaymentInfo($IdStudentRegistration,2, 2);
    	
    	//account
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//invoice
    	$select_invoice = $db->select()
						->from(array('im'=>'invoice_main'),array(
							'record_date'=>'im.date_create',
							'description' => 'im.bill_description',
							'txn_type' => new Zend_Db_Expr ('"Invoice"'),
							'debit' =>'bill_amount',
							'credit' => new Zend_Db_Expr ('"0.00"'),
							'document' => 'bill_number',
							'invoice_no' => 'bill_number',
							'receipt_no' => new Zend_Db_Expr ('"-"')
						)
						)
						->where('im.appl_id = ?', $appl_id)
						//->where('im.IdStudentRegistration = ?', $IdStudentRegistration)
						->where("im.status != 'X'");
		//payment				
		$select_payment = $db->select()
						->from(
							array('pm'=>'payment_main'),array(
															'record_date'=>'pm.payment_date',
															'description' => 'pm.payment_description',
															'txn_type' => new Zend_Db_Expr ('"Payment"'),
															'debit' =>new Zend_Db_Expr ('"0.00"'),
															'credit' => 'amount',
															'document' => 'pbrd.bancs_journal_number',
															'invoice_no' => 'billing_no'
														)
						)
						->joinLeft(array('pbrd'=>'payment_bank_record_detail'),'pbrd.id = pm.transaction_reference', array('receipt_no' => 'bancs_journal_number'))
						->where('pm.appl_id = ?', $appl_id);
						//->where('pm.IdStudentRegistration = ?', $IdStudentRegistration);
										
		//credit note
		$select_creditnote = $db->select()
						->from(
							array('cn'=>'credit_note'),array(
															'record_date'=>'cn.cn_create_date',
															'description' => 'cn.cn_description',
															'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
															'debit' =>new Zend_Db_Expr ('"0.00"'),
															'credit' => 'cn.cn_amount',
															'document' => new Zend_Db_Expr ('"null"'),
															'invoice_no' => 'cn.cn_billing_no',
															'receipt_no' => new Zend_Db_Expr ('"-"')
														)
						)
						->where('cn.appl_id = ?', $appl_id)
						//->where('cn.IdStudentRegistration = ?', $IdStudentRegistration)
						->where('cn.cn_approver is not null')
						->where('cn.cn_approve_date is not null')
						->where('cn.cn_cancel_date is null');
						
		//refund
		$select_refund = $db->select()
						->from(
							array('rfd'=>'refund'),array(
															'record_date'=>'rfd.rfd_approve_date',
															'description' => 'rfd.rfd_desc',
															'txn_type' => new Zend_Db_Expr ('"Refund"'),
															'debit' => 'rfd.rfd_amount',
															'credit' => new Zend_Db_Expr ('"0.00"'),
															'document' => new Zend_Db_Expr ('"null"'),
															'invoice_no' => new Zend_Db_Expr ('"-"'),
															'receipt_no' => new Zend_Db_Expr ('"-"')
														)
						)
						->where('rfd.rfd_appl_id  = ?', $appl_id)
						//->where('rfd_IdStudentRegistration = ?', $IdStudentRegistration)
						->where('rfd.rfd_approver_id is not null')
						->where('rfd.rfd_approve_date is not null')
						->where("rfd.rfd_status = 'A'");
						
		$select = $db->select()
				    ->union(array($select_invoice, $select_payment, $select_creditnote, $select_refund),  Zend_Db_Select::SQL_UNION_ALL)
				    ->order("record_date");
    
		//exit;				
		$row = $db->fetchAll($select);
		
		if(!$row){
			$row = null;
		}
		
		$this->view->account = $row;
	}
	
	public function invoiceAction(){
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		
		$IdStudentRegistration = $this->_getParam('rid', null);
		$this->view->idStudentRegistration = $IdStudentRegistration;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
						->from(
							array('im'=>'invoice_main'),
							array(
								'id'=>'im.id',
								'billing'=>'im.bill_number',
								'payer'=>'im.no_fomulir',
								'record_date'=>'im.date_create',
								'description' => 'im.bill_description',
								'txn_type' => new Zend_Db_Expr ('"Invoice"'),
								'debit' =>'bill_amount',
								'credit' => new Zend_Db_Expr ('"0.00"'),
								'document' => 'bill_number',
								'paid' => 'bill_paid',
								'balance' => 'bill_balance',
								'cn' =>'im.cn_amount'
							)
						)
						->where('im.appl_id = ?', $appl_id)
						//->where('im.IdStudentRegistration = ?', $IdStudentRegistration)
						->where("im.status != 'X'")
						->order('im.date_create')
						->order('im.id');

		$row = $db->fetchAll($select);
				
		//calculate invoice paid
		if($row){
			$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
			$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
			
			foreach ($row as $index=>$invoice){
				$payment_record = $paymentMainDb->getAllInvoicePaymentRecord($invoice['billing'],$invoice['payer']);
				$row[$index]['payment'] = $payment_record;
				
				$creditNote = $creditNoteDb->getAllDataByInvoice($invoice['billing']);
				$row[$index]['credit_note'] = $creditNote;
				
				//calculate if payment made and paid is null or 0.00 and no credit note
				if( ($invoice['paid']==null || $invoice['paid']==0.00) && $payment_record && $creditNote==null){
					
					//initialy set balance to invoice amount
					$row[$index]['balance'] = $row[$index]['debit'];
						
					//loop all payment record
					foreach ($payment_record as $pmt_record){
						
						//check if payment having excess amount
						if($pmt_record['amount'] > $row[$index]['debit']){
							
							$amount_paid = $row[$index]['debit'];
							
							$row[$index]['paid'] = $amount_paid;
							$row[$index]['balance'] = $row[$index]['balance'] - $amount_paid;
						}else{
							
							$amount_paid = $pmt_record['amount'];
							
							$row[$index]['paid'] = $row[$index]['paid'] + $amount_paid;
							$row[$index]['balance'] = $row[$index]['balance'] - $amount_paid;
						}
					}
					
					//mark update
					$row[$index]['update'] = 1;
					$row[$index]['update_record'] = "paid";
				}else
				//do correction if paid is null and balance is null
				if( $invoice['paid']==null && $invoice['balance']==null ){
					$row[$index]['paid'] = 0;
					$row[$index]['balance'] = $row[$index]['debit'];
					
					//mark update
					$row[$index]['update'] = 1;
					$row[$index]['update_record'] = "init";
				}else{
					//mark do not update
					$row[$index]['update'] = 0;
					$row[$index]['update_record'] = "nan";
				}
				
				//check for credit note
				if( $creditNote && ( $invoice['cn']==0.00 || $invoice['cn']==null || $invoice['cn']=="" ) ){

					$sum_cn = 0;
					foreach ($creditNote as $cn){
						if( $cn['cn_cancel_date']==null && isset($cn['cn_approve_date']) ){
							$sum_cn += $cn['cn_amount'];
						}
					}
					$row[$index]['cn'] = $sum_cn;
					$row[$index]['balance'] = $row[$index]['balance'] - $sum_cn;
					
					if($row[$index]['balance']<0){
						$row[$index]['balance'] = 0.00;
					}
					
					$row[$index]['update'] = $row[$index]['update'] || 1;
					$row[$index]['update_record'] .= " CN";
					
				}else{
					if(!isset($row[$index]['cn'])){
						$row[$index]['cn'] = 0.00;
						$row[$index]['update'] = $row[$index]['update'] || 1;
						$row[$index]['update_record'] .= " CN";
					}else{
						$row[$index]['update'] = $row[$index]['update'] || 0;
						$row[$index]['update_record'] .= " XCN";
					}
					
				}
			
			}
		}
		
		
		//process update invoice's balance and paid
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		foreach ($row as $invoice){
			if($invoice['update']==1){
				$data = array(
					'bill_paid' => $invoice['paid'],
					'bill_balance' => $invoice['balance'],
					'cn_amount' => $invoice['cn']
				);
				 
				$invoiceMainDb->update($data,'id = '.$invoice['id']);
			}
		}
		
		if(!$row){
			$row = null;
		}
		
		
		//get payment, advance payment detail and cn info
		$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
		
		if($row){
			foreach ($row as $index => $invoice){
				$row[$index]['payment_record'] = $paymentMainDb->getAllInvoicePaymentRecord($invoice['billing'], $invoice['payer']);
				$row[$index]['advance_payment_detail'] = $advancePaymentDetailDb->getAllBillPayment($invoice['billing']);
				$row[$index]['cn_info'] = $creditNoteDb->getAllDataByInvoice($invoice['billing']);
				
			}
		}
				
		$this->view->invoice = $row;
	}
	
	public function paymentAction(){
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
						->from(
							array('pm'=>'payment_main'),array(
															'billing_no'=>'pm.billing_no',
															'record_date'=>'pm.payment_date',
															'description' => 'pm.payment_description',
															'txn_type' => new Zend_Db_Expr ('"Payment"'),
															'debit' =>new Zend_Db_Expr ('"0.00"'),
															'credit' => 'amount',
															'document' => 'pbrd.bancs_journal_number',
															'payment_mode' => 'payment_mode'
														)
						)
						->joinLeft(array('pbrd'=>'payment_bank_record_detail'),'pbrd.id = pm.transaction_reference', array())
						->where('pm.appl_id = ?', $appl_id);
						
		$row = $db->fetchAll($select);
		
		if(!$row){
			$row = null;
		}
		
		$this->view->payment = $row;
	}
}
?>