<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_DiscountController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		//$db = new Studentfinance_Model_DbTable_Discount();
		//$this->_DbObj = $db;
	}
	
	/*
	 * Search Applicant / Student
	 */
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Discount");
	}
	
	/*
	 * Detail of Applicant / Student
	 */
	public function detailAction(){
		//title
    	$this->view->title= $this->view->translate("Discount - Detail");
    	
    	//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$discountDb = new Studentfinance_Model_DbTable_Discount();
			
			$p_data = $discountDb->getPaginateListData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
	
	    	$this->view->paginator = $paginator;
			$this->view->formData = $formData;
			
			$this->view->type = $formData['type'];

		}
		
    	
	}
	
	public function viewAction()
	{
		$this->view->title = $this->view->translate('Discount Note Details');

		$id = $this->_getParam('id');

		$discountDb = new Studentfinance_Model_DbTable_DiscountNoteDetail();
		
		$info = $discountDb->getData($id);
		if ( empty($info) )
		{
			throw new Exception('Invalid ID');
		}

		$this->view->info = $info;
		$this->view->id = $id;
	}
	
	public function cancelAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			$IdDiscount = $formData['IdDiscount'];
			
			$discountDb = new Studentfinance_Model_DbTable_DiscountNoteDetail();
			
			$proforma = $formData['invoice_id'];
			$n=0;
			while($n < count($proforma)){
				$proforma_id = $proforma[$n];
				
				$info = $discountDb->getDataDetail($proforma_id);
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
				
				//revert invoice
				$this->revertInvoice($proforma_id);
				$n++;
			}
			
			$discData = $discountDb->getData($IdDiscount);
			$totalDetail = count($discData);
			$cancelNo = 0;
			foreach($discData as $data){
				if($data['status'] == 'X'){
					$cancelNo++;
				}
			}
			
			if($cancelNo == $totalDetail){
				//update status as CANCEL
				
				$dataUpdate = array(
							'dcnt_cancel_by' => $auth->getIdentity()->iduser,
							'dcnt_cancel_date' => date('Y-m-d H:i:s'),
							'dcnt_status' => 'X'
						);
				$discountMainDb = new Studentfinance_Model_DbTable_Discount();
				$discountMainDb->update($dataUpdate, 'dcnt_id = '.(int)$IdDiscount);
			}
		}
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Discount has been Cancelled');
		$this->_redirect( $this->baseUrl . '/studentfinance/discount/view/id/'.$info['dcnt_id']);
		exit;
    }
	
	/*
	 * Entry
	 */
	public function entryAction(){
		
		$step = $this->_getParam('step', 1);
		$this->view->step = $step;
		
		$msg = $this->_getParam('msg', null);
		$this->view->noticeMessage = $msg;
		
		$this->view->title = "Discount - Entry";
		
		$txn_id = $this->_getParam('txn_id', null);
		
		
    	
		//set custom session
		$ses_discount_entry = new Zend_Session_Namespace('discount_entry');
		
		if($txn_id){
			Zend_Session::namespaceUnset('discount_entry');
			
			$ses_discount_entry = new Zend_Session_Namespace('discount_entry');
			$ses_discount_entry->txn_id = $txn_id;
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'entry','step'=>1),'default',true));
		}
    	
    	
		/*
		 * STEP FILTERING
		 */
		if( isset($ses_discount_entry->txn_id) ){
			
			//transaction data
	    	$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
	    	$txnData = $txnDb->getTransactionData($ses_discount_entry->txn_id);
	    	$this->view->txn = $txnData;
	    	
	    	//profile data
	    	$profileDB =  new App_Model_Application_DbTable_ApplicantProfile();
	    	$profile = $profileDB->getData($txnData['at_appl_id']);
	    	$this->view->profile = $profile;
	    	
			//offer status
	    	if( $txnData['at_status'] == 'OFFER' || $txnData['at_status'] == 'REGISTERED' ){
	    		$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
	
	    		$programOffered = $applicantProgramDb->getOfferProgram($txnData['at_trans_id'],$txnData['at_appl_type']);
	    		$this->view->programOffered = $programOffered;
	    	}
    	
			if($step==1){
				
				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					
					if( isset($formData['invoice'])){
						$ses_discount_entry->invoice = $formData['invoice'];
					}else{
						$ses_discount_entry->invoice = null;
					}
					
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'entry','step'=>2),'default',true));
					
		    	}else{
		    		
		    		//active invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoiceList = $invoiceMainDb->getInvoiceFromApplId($txnData['at_appl_id'],true);
										
					if($invoiceList){
						
						//remove paid invoice
						foreach ($invoiceList as $index=>$invoice){
							
							if($invoice['bill_balance']==0){
								unset($invoiceList[$index]);
							}
						}
						$invoiceList = array_values($invoiceList);
						
						//remove invoice with credit note
						$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
						foreach ($invoiceList as $index=>$invoice){
							if($creditNoteDb->getDataByInvoice($invoice['bill_number'])){
								unset($invoiceList[$index]);
							}
						}
						$invoiceList = array_values($invoiceList);
						
					}
						
					if(isset($ses_discount_entry->invoice)){
					
						//set checked from data session
						foreach ($invoiceList as $index=>$invoice){
							
							if($invoice['id'] == $ses_discount_entry->invoice){
								$invoiceList[$index]['selected'] = true;
							}else{
								$invoiceList[$index]['selected'] = false;
							}
						}
					}
					
					$this->view->invoice_list = $invoiceList;
		    	}	
		    	
			}else
			if($step==2){
				
				if( !isset($ses_discount_entry->invoice )){
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'entry','step'=>1, 'msg'=>'Please Complete step 1 process'),'default',true));
				}else{
					
					//discount type
					$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
					$discountTypeList = $discountTypeDb->getData();
					$this->view->discount_type_list = $discountTypeList;
					
					//invoice
					$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					$invoice = $invoiceMainDb->getData($ses_discount_entry->invoice);
					$this->view->invoice = $invoice;
										
					if ($this->getRequest()->isPost()) {
						$formData = $this->getRequest()->getPost();
						
						//validation
						if( !isset($formData['dcnt_letter_number']) || $formData['dcnt_letter_number']==''){
							$this->view->noticeError = $this->view->translate('Please enter letter number');
						}else
						if( !isset($formData['discount_type_id']) || $formData['discount_type_id']==''){
							$this->view->noticeError = $this->view->translate('Please select discount type');
						}else
						if(  !isset($formData['discount_amount']) || $formData['discount_amount']=='' || $formData['discount_amount']==0 ){
							$this->view->noticeError = $this->view->translate('Please key-in discount amount');
						}else
						if(  is_int($formData['discount_amount']) ){
							$this->view->noticeError = $this->view->translate('Please key-in discount amount with positive numbers only');
						}else
						if($formData['discount_amount'] > $invoice['bill_balance']){
							$this->view->noticeError = $this->view->translate('Please key-in discount amount less than invoice balance');
						}else{
							$ses_discount_entry->discount = $formData;
							
							//redirect
							$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'entry','step'=>3),'default',true));
						}
						
						$this->view->discount = $formData;
						
					}else{
					
						if( isset($ses_discount_entry->discount) ){
							$this->view->discount = $ses_discount_entry->discount; 
						}					
					}
					
					
					
				}
				
			}else
			if($step==3){
				
				if( !isset($ses_discount_entry->invoice ) || !isset($ses_discount_entry->discount )){
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'entry','step'=>2, 'msg'=>'Please Complete step 2 process'),'default',true));
				}else{
					
					$this->view->txn_id = $ses_discount_entry->txn_id;
					
					if ($this->getRequest()->isPost()) {
						$formData = $this->getRequest()->getPost();
						
						exit;
					}else{
						//invoice
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
						$invoice = $invoiceMainDb->getData($ses_discount_entry->invoice);
						$this->view->invoice = $invoice;
						
						
						
						if( isset($ses_discount_entry->discount) ){
							$this->view->discount = $ses_discount_entry->discount;
							
							//discount type name
							$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
							$dscnt = $discountTypeDb->getData($ses_discount_entry->discount['discount_type_id']);
							$this->view->discount['discount_type_name'] = $dscnt['dt_discount'];
						}
						
						
					}
						
				}
				
			}else 
			if($step=='confirm'){
						
				
				//invoice
				$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
				$invoice = $invoiceMainDb->getData($ses_discount_entry->invoice);
				
				//discount type name
				$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
				$dscnt = $discountTypeDb->getData($ses_discount_entry->discount['discount_type_id']);
				
							
				$db = Zend_Db_Table::getDefaultAdapter();
				$db->beginTransaction();
					
				try{
					
					
					
					//insert discount
					$discountDb = new Studentfinance_Model_DbTable_Discount();
					$data = array(
						'dcnt_appl_id' => $txnData['at_appl_id'],
						'dcnt_txn_id' => $txnData['at_trans_id'],
						'dcnt_fomulir_id' => $txnData['at_pes_id'],
						'dcnt_type_id' => $ses_discount_entry->discount['discount_type_id'],
						'dcnt_amount' => $ses_discount_entry->discount['discount_amount'],
						'dcnt_invoice_id' => $ses_discount_entry->invoice,
						'dcnt_letter_number' => $ses_discount_entry->discount['dcnt_letter_number']
					);
					$discountDb->insert($data);
					
					
					//insert credit note
					$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
					$data = array(
						'cn_billing_no' => $invoice['bill_number'],
						'cn_fomulir' => $txnData['at_pes_id'],
						'appl_id' => $txnData['at_appl_id'],
						'cn_amount' => $ses_discount_entry->discount['discount_amount'],
						'cn_description' => $dscnt['dt_discount'],
						'cn_approver' => '-1'
					);
					$creditNoteDb->insert($data);
					
					//update invoice
					$data = array(
						'cn_amount' => $invoice['cn_amount'] + $ses_discount_entry->discount['discount_amount'],
						'bill_balance' => $invoice['bill_balance'] - $ses_discount_entry->discount['discount_amount']
					);
					
					$invoiceMainDb->update($data,'id = '.$invoice['id']);
					
					$db->commit();
					
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'detail', 'txn'=>$txnData['at_trans_id']),'default',true));
					
				}catch (Exception $e) {
					echo "Error in Discount entry. <br />";
					echo $e->getMessage();
					
					echo "<pre>";
					print_r($e->getTrace());
					echo "</pre>";
					
					$db->rollBack();
	    			
	    			exit;
				}
				
			}else{
			
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'entry','step'=>1),'default',true));
			}
		}else{
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'discount'),'default',true));
		}
    			
	}
	
	public function invoiceDetailAction(){
		
	} 
	
	public function setupAction(){
		//title
    	$this->view->title= $this->view->translate("Discount Set-up");
    	
    	$noticeError = $this->_getParam('error', null);
    	$this->view->noticeError = $noticeError;
    	
	}
	
	public function typeAction(){
		//$this->_helper->layout->disableLayout();
		
		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
		
		$form = new Studentfinance_Form_DiscountType();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				$data = array(
							'dt_discount' => $formData['dt_discount'],
							'dt_description' => $formData['dt_description'],
							'dt_discountitem' => $formData['dt_discountitem']
						);
				
				$discount_id = $discountTypeDb->insert($data);
				
				
				if($formData['FeeCodeGrid'])
				{
					$db = getDB();
					$check = count($formData['FeeCodeGrid']);
					for($i=0;$i<$check;$i++) 
					{
						$paramArray = array(
								'IdDiscountType'=>$id,
								'FeeCode'=> $formData['FeeCodeGrid'][$i],
								'CalculationMode'=> $formData['CalculationModeGrid'][$i],
								'Amount'=>$formData['AmountGrid'][$i],
						);

						$db->insert('discount_type_fee_info',$paramArray);
					}

				}

			}else{
				$error = "Unable to save";
			}
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'setup','error'=>$error),'default',true));
		}else{
			//paginator
			$data = $discountTypeDb->getPaginateData();
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->paginator = $paginator;
		}
		
		$this->view->form = $form;
	}

	public function addTypeAction()
	{
		//$this->_helper->layout->disableLayout();
		
		$this->view->title = $this->view->translate('Add Discount Type');

		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();


		$form = new Studentfinance_Form_DiscountType();
		
		$form->setAction($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'edit-type')));
		
		//Form
		$this->view->lobjSponsorSetupForm = new Studentfinance_Form_Sponsorsetup();
		$this->view->lobjSponsorSetupForm->Add->setAttrib('onclick','remarkValidation();');
		$this->view->lobjSponsorSetupForm->Clear->setAttrib('onclick','clearpageAdd();');

		//Fee Item
		$this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();
		$feeList = $this->lobjFeeItem->getData();
		foreach( $feeList as $item)
		{
			$this->view->lobjSponsorSetupForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name'].' - '.$item['fi_code']);
		}

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
								
				$data = array(
							'dt_discount' => $formData['dt_discount'],
							'dt_description' => $formData['dt_description']
						);

				$id = $discountTypeDb->insert($data);

				if($formData['FeeCodeGrid'])
				{
					$db = getDB();
					$check = count($formData['FeeCodeGrid']);
					for($i=0;$i<$check;$i++) 
					{
						$paramArray = array(
								'IdDiscountType'=>$id,
								'FeeCode'=> $formData['FeeCodeGrid'][$i],
								'CalculationMode'=> $formData['CalculationModeGrid'][$i],
								'Amount'=>$formData['AmountGrid'][$i],
						);

						$db->insert('discount_type_fee_info',$paramArray);
					}

				}
				
			}else{
				$error = "Unable to save";
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New Discount Type successfully added"));

			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'type','error'=>$error),'default',true));
		}else{
			
		}
		
		$this->view->form = $form;
		
	}
	
	public function editTypeAction(){
		
		$id = $this->_getParam('id', null);
		
		//$this->_helper->layout->disableLayout();
		
		$this->view->title = $this->view->translate('Edit Discount Type');

		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
		$data = $discountTypeDb->getData($id);


		$form = new Studentfinance_Form_DiscountType();
		
		$form->setAction($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'edit-type')));
		
		//Form
		$this->view->lobjSponsorSetupForm = new Studentfinance_Form_Sponsorsetup();
		$this->view->lobjSponsorSetupForm->Add->setAttrib('onclick','remarkValidation();');
		$this->view->lobjSponsorSetupForm->Clear->setAttrib('onclick','clearpageAdd();');

		//Fee Item
		$this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();
		$feeList = $this->lobjFeeItem->getData();
		foreach( $feeList as $item)
		{
			$this->view->lobjSponsorSetupForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name'].' - '.$item['fi_code']);
		}

		//get fee info
		$feeinfo = $discountTypeDb->getFeeInfo($data['dt_id']);
		$this->view->feeinfo = $feeinfo;

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
								
				$data = array(
							'dt_discount' => $formData['dt_discount'],
							'dt_description' => $formData['dt_description']
						);
				
				$discountTypeDb->update($data,'dt_id = '.$id);

				if($formData['FeeCodeGrid'])
				{
					$db = getDB();
					$check = count($formData['FeeCodeGrid']);
					for($i=0;$i<$check;$i++) 
					{
						$paramArray = array(
								'IdDiscountType'=>$id,
								'FeeCode'=> $formData['FeeCodeGrid'][$i],
								'CalculationMode'=> $formData['CalculationModeGrid'][$i],
								'Amount'=>$formData['AmountGrid'][$i],
						);

						$db->insert('discount_type_fee_info',$paramArray);
					}

				}
				
			}else{
				$error = "Unable to save";
			}
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			//redirect
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'type','error'=>$error),'default',true));
		}else{
			
			
			$form->populate($data);
		}
		
		$this->view->form = $form;
		
	}
	
	public function deleteTypeAction(){
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'setup'),'default',true));
		
	}
	
	public function deleteFeeAction()
	{
		$db = getDB();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('discount_type_fee_info', 'IdDiscountFeeInfo = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}
	
public function batchDiscountAction(){
	
	//set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
		//title
		$this->view->title= $this->view->translate("Discount Note by Batch");
		
		$ses_batch_invoice = new Zend_Session_Namespace('studentfinance_batch_discount');
		$ses_batch_invoice->setExpirationSeconds(900);
		
		$step = $this->_getParam('step', 1);
		$this->view->step = $step;
		
		if($step!=5 && $step!=6){
		  unset($ses_batch_invoice->student_list);
		}
		
		if($step==1){ //STEP 1
			
			//Zend_Session::namespaceUnset('studentfinance_batch_invoice');
			
			if ($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
				
				if($formData['semester_id']!=null){
					
					$ses_batch_invoice->semester_id = $formData['semester_id'];
				
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>2),'default',true));
				}
				
			}
			
			$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
			$this->view->semester_list = $semesterDb->fnGetSemestermasterList();
			
			if(isset($ses_batch_invoice->semester_id)){
				$this->view->semester_id = $ses_batch_invoice->semester_id;
			}
			
			
		}else
		if($step==2){ //STEP 2
			
			//step validation
			if(!isset($ses_batch_invoice->semester_id)){
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>1),'default',true));
			}
			
			if ($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
			
				if($formData['discounttype']!=null){
						
					$ses_batch_invoice->discounttype = $formData['discounttype'];
			
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>3),'default',true));
				}
			
			}
			
			//semester
			$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
			
			//discount type list
			$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
			$discountTypeList = $discountTypeDb->getData();
			
			$this->view->discounttypelist = $discountTypeList;
			
			if(isset($ses_batch_invoice->discounttype)){
				$this->view->discounttype = $ses_batch_invoice->discounttype;
			}

			
				
		}else
		if($step==3){ //STEP 3
			
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>1),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->discounttype)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>2),'default',true));
    		}
    		
    		if ($this->getRequest()->isPost()) {
    		  	
    		  $formData = $this->getRequest()->getPost();
    		  
    		  $ses_batch_invoice->intake = $formData['intake'];
    		  
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>4),'default',true));
    		  
    		}
    		
    		//get intake with not graduated student
    		$intakeDb = new App_Model_Record_DbTable_Intake();
    		
    		$intakeList = $intakeDb->getIntakeWithStudent(92);
    		$this->view->intake_list = $intakeList;
    		
    		if(isset($ses_batch_invoice->intake)){
    		  $this->view->selected_intake = $ses_batch_invoice->intake;
    		}
    		
    		
				
		}else
		if($step==4){ //STEP 4
		
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>1),'default',true));
    		}else
    		if(!isset($ses_batch_invoice->discounttype)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>2),'default',true));
    		}else
  		    if(!isset($ses_batch_invoice->intake)){
  		      $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>3),'default',true));
  		    }
    			
    		if ($this->getRequest()->isPost()) {
    		  	
    		  $formData = $this->getRequest()->getPost();
    		
    		  $ses_batch_invoice->fee_item = $formData['fi_id'];
    		
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>5),'default',true));
    		
    		}
    			
    		//discount fee item
    		
			$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
    		$feeItem = $discountTypeDb->getFeeInfo($ses_batch_invoice->discounttype);
    		$this->view->fee_item_list = $feeItem;
    			
    		
    		if(isset($ses_batch_invoice->fee_item)){
    		  $this->view->fee_item = $ses_batch_invoice->fee_item;
    		  
    		}
    		
				
		}else
		if($step==5){ //STEP 5
		
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>1),'default',true));
    		}else
    		if(!isset($ses_batch_invoice->discounttype)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>2),'default',true));
    		}else
    		if(!isset($ses_batch_invoice->intake)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>3),'default',true));
    		}else  
    		if(!isset($ses_batch_invoice->fee_item)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>4),'default',true));
    		}
    		
    		//get student in program which is not a new student
    		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
    		$student_list = $discountTypeDb->getStudentListByDiscountType($ses_batch_invoice->discounttype);
    		
    		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
    		$feeItem = $discountTypeDb->getData($ses_batch_invoice->discounttype);
			//var_dump($feeItem);
    		$this->view->fee_item = $feeItem;
    		
    		$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
    		
    		if ($this->getRequest()->isPost()) {
    		  	
    		  $formData = $this->getRequest()->getPost();
				//var_dump($formData); exit;
    		  
    		  //semester 
    		  $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		  $semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		  
    		/*  //program
    		  $programDb = new GeneralSetup_Model_DbTable_Program();
    		  $program = $programDb->fngetProgramData($ses_batch_invoice->program_id);*/
    		  
    		
    		  $student_list = array();
    		  foreach ($formData['id'] as $sid){
    		    //invoice
//    		    $invoice = $this->getInvoiceData($formData['sid'][$sid]);
    		    
//    		    $invoiceMain = $invoiceMainDB->getStudentInvoiceDataDiscount($formData['sid'][$sid],$ses_batch_invoice->semester_id,$ses_batch_invoice->fee_item,$ses_batch_invoice->discounttype);
    		    
    		   $student_disc_info = $discountTypeDb->getDataByStudent($ses_batch_invoice->discounttype,$formData['sid'][$sid]);
    		    
    		  	$disStartDate = $student_disc_info['dtt_start_date'];
    			$disEndDate = $student_disc_info['dtt_end_date'];
    			if($disEndDate != '0000-00-00'){
    				$disEndDate = $disEndDate;
    			}else{
    				$disEndDate = date('Y-m-d');
    			}
    			
    			$listInvoice = $invoiceMainDB->getStudentInvoiceDataDiscount($formData['sid'][$sid],$ses_batch_invoice->semester_id,$ses_batch_invoice->fee_item,$ses_batch_invoice->discounttype);
				  //var_dump($listInvoice);
    			$invoiceArray = array();
    			$invoiceArray = $listInvoice;
    			$m=0;
    			if($listInvoice){
	    			foreach($listInvoice as $inv){
	    				
	    				$invoiceArray[$m]['invdate'] =$inv['invoice_date'];
	    				$invoiceArray[$m]['d_start'] =$disStartDate;
	    				$invoiceArray[$m]['d_end'] =$disEndDate;
	    				$invdate = $inv['invoice_date'];
	    				if($invdate >= $disStartDate){
	    					if($invdate <= $disEndDate){
								$cnPaid = Studentfinance_Model_DbTable_PaymentAmount::getCreditNoteDtl($inv['id_detail']);

								$cnAmount = 0.00;
								if (isset($cnPaid[0]['cnpaid']) && $cnPaid[0]['cnpaid'] != null){
									$cnAmount =  str_replace(',', '', $cnPaid[0]['cnpaid']);
								}

								if ($cnAmount >= $inv['amount_dtl']){
									unset($invoiceArray[$m]);
								}
	    					}else{
	    						unset($invoiceArray[$m]);
	    					}
	    					
	    				}else{
	    					unset($invoiceArray[$m]);
	    				}
	    				$m++;
	    			}
    			}
    			if($invoiceArray){
    				$invoiceArray = array_values($invoiceArray);
    			}else{
    				$invoiceArray = null;
    			}
    		
    		    
    	    	 //student profile
        	    $studentProfileDb = new App_Model_Record_DbTable_StudentRegistration();
        	    $profile = $studentProfileDb->getStudentInfo($formData['sid'][$sid]);
        	    
                //intake detail
                $intakeDb = new App_Model_Record_DbTable_Intake();
                $intake = $intakeDb->getData($profile['IdIntake']);
        	    
        	    //student data
        	    $student_list[] = array(
        	      'student_id' => $formData['sid'][$sid],
        	      'fs_id' => $formData['fs_id'][$sid],
        	      'profile'=>$profile,  
        	      'intake'=>$intake,  
        	      'semester' => $semester,
        	      'invoice' => $invoiceArray 
        	    );
	        		    
    		  
	    		  $ses_batch_invoice->student_list = $student_list;
    		  }
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>6),'default',true)); 
    		}
    			
    		//semester
    		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		$this->view->semester = $semester;
    		
    		
    		//intake
    		$intakeDb = new App_Model_Record_DbTable_Intake();
    		$intake_list = null;
    		foreach ($ses_batch_invoice->intake as $intake){
    		  $intake_list[] = $intakeDb->getData($intake);
    		}
    		$this->view->intake = $intake_list;
    			
    		
    			
    		$db = Zend_Db_Table::getDefaultAdapter();
    			
    		for($i=0; $i<sizeof($student_list); $i++){
    			$disStartDate = $student_list[$i]['dtt_start_date'];
    			$disEndDate = $student_list[$i]['dtt_end_date'];
    			if($disEndDate != '0000-00-00'){
    				$disEndDate = $disEndDate;
    			}else{
    				$disEndDate = date('Y-m-d');
    			}
    			
    			$listInvoice = $invoiceMainDB->getStudentInvoiceDataDiscount($student_list[$i]['IdStudentRegistration'],$ses_batch_invoice->semester_id,$ses_batch_invoice->fee_item,$ses_batch_invoice->discounttype);
    			//var_dump($listInvoice);
				$invoiceArray = array();
    			$invoiceArray = $listInvoice;
    			$m=0;
    			if($listInvoice){
	    			foreach($listInvoice as $inv){

	    				$invoiceArray[$m]['invdate'] =$inv['invoice_date'];
	    				$invoiceArray[$m]['d_start'] =$disStartDate;
	    				$invoiceArray[$m]['d_end'] =$disEndDate;
	    				$invdate = $inv['invoice_date'];
	    				if($invdate >= $disStartDate){
	    					if($invdate <= $disEndDate){
								$cnPaid = Studentfinance_Model_DbTable_PaymentAmount::getCreditNoteDtl($inv['id_detail']);

								$cnAmount = 0.00;
								if (isset($cnPaid[0]['cnpaid']) && $cnPaid[0]['cnpaid'] != null){
									$cnAmount =  str_replace(',', '', $cnPaid[0]['cnpaid']);
								}

								if ($cnAmount >= $inv['amount_dtl']){
									unset($invoiceArray[$m]);
								}
	    					}else{
	    						unset($invoiceArray[$m]);
	    					}
	    					
	    				}else{
	    					unset($invoiceArray[$m]);
	    				}
	    				$m++;
	    			}
    			}
    			if($invoiceArray){
    				$invoiceArray = array_values($invoiceArray);
    			}
    		  if($invoiceArray){
    		  	$student_list[$i]['invoice'] = $invoiceArray;
    		  }else{
    		  	$student_list[$i]['invoice'] = null;
    		  }
    		
    		 
    		}
    		
    		$student_list = array_values($student_list);
    		$this->view->student_list = $student_list;
    		
    		if(isset($ses_batch_invoice->student_list)){
    		  $arr_std_id = array();
    		  foreach ($ses_batch_invoice->student_list as $std){
    		    $arr_std_id[] = $std['student_id'];
    		  }
    		  $this->view->student_selected = $arr_std_id;
    		  
    		}
    		
    		
    		
				
		}else
		if($step==6){// STEP 6
		
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>1),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->discounttype)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>2),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->intake)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>3),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->fee_item)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>4),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->student_list)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-discount', 'step'=>5),'default',true));
    		}
    		
    		//semester
    		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		
    		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
    		$feeItem = $discountTypeDb->getData($ses_batch_invoice->discounttype);
    		
    		if ($this->getRequest()->isPost()) {
    			
				$db = Zend_Db_Table::getDefaultAdapter();
					
					$arrayDiscount = array();
	    			$i=0;
	    			foreach($ses_batch_invoice->student_list as $data){
	    				$arrayDiscount[$i]['student_id'] = $data['student_id'];
	    				$arrayDiscount[$i]['description'] = 'Discount for '.$semester['SemesterMainName'].' ('.$feeItem['dt_description'].')';
	    				$m=0;
	    				foreach($data['invoice'] as $inv){
		    				$arrayDiscount[$i]['invoice'][$inv['currency_id']][$m]=$inv['id_detail'];
		    				$arrayDiscount[$i]['amount'][$inv['currency_id']][$m]=$inv['AmountDisc'];
		    				$m++;
	    				}
	    				$i++;
	    			}
    			
    		  		$status = $this->saveBatch($arrayDiscount,$ses_batch_invoice);
  
		
    		  
    		  if($status){
    		    Zend_Session::namespaceUnset('studentfinance_batch_invoice');
    		  }
    		  
    		  $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Batch Discount Saved');
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-list'),'default',true));
    		}
    		
    		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
    		$feeItem = $discountTypeDb->getData($ses_batch_invoice->discounttype);
    		$this->view->fee_item = $feeItem;
    		
    		//semester
    		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		$this->view->semester = $semester;
    		 
    		
    		//intake
    		$intakeDb = new App_Model_Record_DbTable_Intake();
    		$intake_list = null;
    		foreach ($ses_batch_invoice->intake as $intake){
    		  $intake_list[] = $intakeDb->getData($intake);
    		}
    		$this->view->intake = $intake_list;
    		
    		//student list
    		$this->view->student_list = $ses_batch_invoice->student_list;
    		
		}else{
		  
		}
		
	}
	
	public function batchListAction(){
		
		//title
		$this->view->title= $this->view->translate("Discount Note by Batch");
		
		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountNoteBatch();
		
		$dataStudent = $discountTypeDb->getData();
		$i=0;
		foreach($dataStudent as $data){
			$id = $data['id'] ;
			$discountTypeDb = new Studentfinance_Model_DbTable_Discount();
			$dataStudentnumber = $discountTypeDb->getlistStudentByStatus($id);
			$dataStudent[$i]['number']= $dataStudentnumber;
			$i++;
		}
		
		$this->view->paginator = $dataStudent;
		
	}
	
	public function batchListDetailAction(){
		
		//title
		$this->view->title= $this->view->translate("Discount Note by Batch - Detail");
		
		$id = $this->_getParam('id', null);
    	$this->view->id = $id;
		
		$discountTypeDb = new Studentfinance_Model_DbTable_Discount();
		$discountBatchDb = new Studentfinance_Model_DbTable_DiscountNoteBatch();
		$discountDetailDb = new Studentfinance_Model_DbTable_DiscountNoteDetail();
		
		$data = $discountBatchDb->getDataDetail($id);
		$dataStudent = $discountTypeDb->getlistStudent($id);
		
		$arrayStudent = array();
		$arrayStudent = $dataStudent;
		foreach($dataStudent as $key=>$dataStud){
			$studId = $dataStud['dcnt_IdStudentRegistration'];
			$invStud = $discountDetailDb->getStudentData($id,$studId);
			$arrayStudent[$key]['invoice'] = $invStud;
		}
		
		$this->view->data = $data;
		$this->view->liststudent = $arrayStudent;
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		$creator = $getUserIdentity->id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if ($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
				
				$action = $formData['approve'];
				$dcnt_id = $formData['dcnt_id'];
				
				$i=0;
				$n = count($dcnt_id);
				
				while($i < $n){
					
					$dId = $dcnt_id[$i];
					
					if($action == 'Approve'){
						$invoiceClass = new icampus_Function_Studentfinance_Invoice();
						$invoiceClass->discountNoteApproval($dId);
					}elseif($action == 'Cancel'){
						
						$dataCancel = array(
							'dcnt_status' => 'X',
							'dcnt_cancel_by' => $creator,
							'dcnt_cancel_date' => date('Y-m-d H:i:s')
						);
						$db->update('discount',$dataCancel,'dcnt_id = '.$dId);
					}
				
				
					$i++;
				}
				
				
				 $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Data has been updated');
    		  //redirect
				 $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'discount', 'action'=>'batch-list-detail','id'=>$id),'default',true));
		}
	}
	
	private function saveBatch($dataArray,$ses_batch_invoice){
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		$creator = $getUserIdentity->id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->beginTransaction();
		
		try{
			$btchNum = $this->getCurrentBatchNoSeq();
			
			$discBtachDB = new Studentfinance_Model_DbTable_DiscountNoteBatch();
			$dataBatch = array(
						'semester_id'		=> $ses_batch_invoice->semester_id,
						'dcnt_id'			=> $ses_batch_invoice->discounttype,
						'total_discount'		=> count($ses_batch_invoice->student_list),
						'batch_number'			=> $btchNum,
						'created_by'		=> $creator,
						'create_date'		=> date('Y-m-d H:i:s')
				);
				
			$btch_id = $discBtachDB->insert($dataBatch);
				
			foreach($dataArray as $data){
				$studentID = $data['student_id'];
				$description = $data['description'];
				
					
					foreach($data['invoice'] as $key=>$invData){
						
						$invoiceID = array_values($invData);
						$amount = array_values($data['amount'][$key]);
						
						$invoiceClass = new icampus_Function_Studentfinance_Invoice();
						$invoiceClass->generateDiscountNote($studentID,$invoiceID,$amount,$description,$ses_batch_invoice->discounttype,$btch_id);
					}
					
			}
			$db->commit();
		}catch(Exception $e){
				
				$db->rollBack();
				
				//save error message
			   	$sysErDb = new App_Model_General_DbTable_SystemError();
			   	$msg['se_IdStudentRegistration'] = $studentID;
			   	$msg['se_title'] = 'Error generate Discount by Batch';
			   	$msg['se_message'] = $e->getMessage();
			   	$msg['se_createdby'] = $creator;
			   	$msg['se_createddt'] = date("Y-m-d H:i:s");
			   	$sysErDb->addData($msg);
		   	
        		throw $e;
		}
	}

private function getCurrentBatchNoSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 13")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
			$db->update('sequence',array('seq'=>$seq),'year = '.date('Y').' and type = 13');
		}else{
			$db->insert('sequence',array('year'=>date('Y'),'type'=>13,'seq'=>1));
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		if($row2){
			$r_num = 'BDN'.date('Y').$row2['ReceiptSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	private function getCurrentAdjNoSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 14")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
			$db->update('sequence',array('seq'=>$seq),'year = '.date('Y').' and type = 14');
		}else{
			$db->insert('sequence',array('year'=>date('Y'),'type'=>14,'seq'=>1));
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		if($row2){
			$r_num = 'DADJ'.date('Y').$row2['ReceiptSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	private function revertInvoice($cnID){
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$discountDb = new Studentfinance_Model_DbTable_DiscountNoteDetail();
		
		$info = $discountDb->getDataDetail($cnID);
		
		$dn_amount = $info['dcnt_amount'];
		$dcntId = $info['dcnt_id'];
		
		$invIdDet = $info['dcnt_invoice_det_id'];
		$invId = $info['dcnt_invoice_id'];
	
		//update invoice detail
		$invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invDetail = $invoiceDetailDB->getData($invIdDet);
		
		$amount = $invDetail['balance']  + $dn_amount ;
		$amountcn = $invDetail['dn_amount'] - $dn_amount;
		$upd_data_inv = array(
			'balance'=> $amount,
			'dn_amount'=> $amountcn,
		);
	
		$invoiceDetailDB->update($upd_data_inv, array('id = ?' => $invIdDet) );
		
		$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
		$invMain = $invoiceMainDB->getData($invId);
		
		$amount = $invMain['bill_balance']  + $dn_amount ;
		$amountcn =  $invMain['dn_amount'] - $dn_amount;
		$upd_data_inv = array(
			'bill_balance'=> $amount,
			'dn_amount'=> $amountcn,
			'upd_by'=> $getUserIdentity->id,
			'upd_date'=> date('Y-m-d H:i:s')
		);
		
		$invoiceMainDB->update($upd_data_inv, array('id = ?' => $invId) );
		
		//insert discount_adjustment
		$discAdjustDB = new Studentfinance_Model_DbTable_DiscountAdjustment();
		
		$adjId = $this->getCurrentAdjNoSeq();
		
		$upd_data_inv = array(
			'AdjustmentCode'=> $adjId,
			'AdjustmentType'=> 1,
			'IdDiscount'=>$cnID,
			'Remarks' => 'Discount Cancellation',
			'cur_id'=>$info['dcnt_currency_id'],
			'amount'=>$dn_amount,
			'Status'=>'APPROVE',
			'EnterBy'=> $getUserIdentity->id,
			'EnterDate'=> date('Y-m-d H:i:s'),
			'UpdUser'=> $getUserIdentity->id,
			'UpdDate'=> date('Y-m-d H:i:s')
		);
		
		$idAdjust =  $discAdjustDB->insert($upd_data_inv);
		
		$newStatus = 'X';
			
		$upd_data = array(
			'adjustId'=> $idAdjust,
			'status'=> $newStatus,
			'status_by'=> $getUserIdentity->id,
			'status_date'=> date('Y-m-d H:i:s')
		);
		
		$discountDb->update($upd_data, array('id = ?' => $cnID) );
		
		$discDB = new Studentfinance_Model_DbTable_Discount();
		$dataDisc = $discDB->getData($dcntId);
		$amountAdj = $dataDisc['dcnt_amount_adj'] + $dn_amount;
		$amountAdjMain = $dataDisc['dcnt_amount'] - $dn_amount;
		$upd_data = array(
			'dcnt_amount_adj'=> $amountAdj,
//			'dcnt_amount'=> $amountAdjMain,
			'dcnt_update_by'=> $getUserIdentity->id,
			'dcnt_update_date'=> date('Y-m-d H:i:s')
		);
		
		$discDB->update($upd_data, array('dcnt_id = ?' => $dcntId) );
		
		
	}
	
}

?>