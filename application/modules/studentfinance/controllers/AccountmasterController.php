<?php
class Studentfinance_AccountmasterController extends Base_Base { //Controller for the User Module	
	private $lobjAccountmaster;
	private $lobjAccountmasterForm;
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();
	}
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjAccountmaster = new Studentfinance_Model_DbTable_Accountmaster();
		$this->lobjAccountmasterForm = new Studentfinance_Form_Accountmaster(); 
	}
	public function indexAction() { // action for search and view
		$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		
		$larrresult = $this->lobjAccountmaster->fngetAccountDetails (); //get user details
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->accountmasterpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->accountmasterpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->accountmasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjAccountmaster-> fngetaccountdeatilsSearch($this->_request->getPost ());  //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->accountmasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/accountmaster/index');
		}


	}

	public function newaccountmasterAction() { //Action for creating the new user
		$this->view->lobjAccountmasterForm = $this->lobjAccountmasterForm; 
		$auth = Zend_Auth::getInstance();	
		
		//for combobox IdGroup
		$larrGroupList	=	$this->lobjAccountmaster->fnGetIdGroupSelect();
		$this->view->lobjAccountmasterForm->IdGroup->addMultiOptions($larrGroupList);
		
		//for combobox BillingModule
		$larrBilling	=	$this->lobjAccountmaster->fnGetBillingModuleSelect();
		$this->view->lobjAccountmasterForm->BillingModule->addMultiOptions($larrBilling);
		
		//for combobox Period
		$larrBilling	=	$this->lobjAccountmaster->fnGetPeriodSelect();
		$this->view->lobjAccountmasterForm->Period->addMultiOptions($larrBilling);
    	
		$lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
		$initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);	
		
		if($initialConfig['AccountCodeType'] == 1 ){
			$this->view->lobjAccountmasterForm->PrefixCode->setAttrib('readonly','true');
			$this->view->lobjAccountmasterForm->PrefixCode->setValue('xxx-xxx-xxx');
		}else{
			$this->view->lobjAccountmasterForm->PrefixCode->setAttrib('required','true');
			$this->view->lobjAccountmasterForm->PrefixCode->setAttrib('maxlength','6');
		}
		//for Accountcode 
		// $larrAccountCode=  $this->lobjAccountmaster->fnGetAccountcodeSelect();
		//print_r($larrAccountCode);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );		
			if ($this->lobjAccountmasterForm->isValid ( $larrformData )) {
				$larrformData['AccountCreatedDt'] = date ( 'Y-m-d H:i:s' );
				$larrformData['AccountCreatedBy'] = $auth->getIdentity()->iduser;
				$larrformData['UpdDate'] = date ( 'Y-m-d H:i:s' );
				$larrformData['UpdUser'] = $auth->getIdentity()->iduser;
				if(!$larrformData['IdGroup']) $larrformData['IdGroup'] = 0;
				if(!$larrformData['BillingModule']) $larrformData['BillingModule'] = 0;
				$result = $this->lobjAccountmaster->fnInsert($larrformData); //instance for adding the lobjuserForm values to DB
				if($initialConfig['AccountCodeType'] == 1 )$codeGeneration = $this->lobjAccountmaster->fnGenerateCode($this->gobjsessionsis->idUniversity,$result,$larrformData['AccShortName']);
				$this->_redirect( $this->baseUrl . '/studentfinance/accountmaster/index');
			}
		}

	}

	public function accountmasterlistAction() { //Action for the updation and view of the user details
		$this->view->lobjAccountmasterForm = $this->lobjAccountmasterForm; 
		
		$lintidAccount = ( int ) $this->_getParam ( 'id' );
		$this->view->idAccount = $lintidAccount;
		
		
		//for combobox IdGroup
		$larrGroupList	=	$this->lobjAccountmaster->fnGetIdGroupSelect();
		$this->view->lobjAccountmasterForm->IdGroup->addMultiOptions($larrGroupList);
		
		//for combobox BillingModule
		$larrBilling	=	$this->lobjAccountmaster->fnGetBillingModuleSelect();
		$this->view->lobjAccountmasterForm->BillingModule->addMultiOptions($larrBilling);
		
		//for combobox Period
		$larrBilling	=	$this->lobjAccountmaster->fnGetPeriodSelect();
		$this->view->lobjAccountmasterForm->Period->addMultiOptions($larrBilling);
    	
		//for Accountcode 
		 //$larrAccountCode=  $this->lobjAccountmaster->fnGetAccountcodeSelect();
		
		$this->view->lobjAccountmasterForm->PrefixCode->setAttrib('readonly','true');
		
		$larrresult = $this->lobjAccountmaster->fnviewAccountmaster($lintidAccount); //getting user details based on userid
		$this->lobjAccountmasterForm->populate($larrresult);
		$auth = Zend_Auth::getInstance();
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjAccountmasterForm->isValid ( $larrformData )) {						
					$larrformData['UpdDate'] = date ( 'Y-m-d H:i:s' );
					$larrformData['UpdUser'] = $auth->getIdentity()->iduser;					
					$this->lobjAccountmaster->fnupdateaccountmaster($lintidAccount, $larrformData );
					//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
					$this->_redirect( $this->baseUrl . '/studentfinance/accountmaster/index');
				}
			}
		}
		$this->view->lobjAccountmasterForm = $this->lobjAccountmasterForm;
	}

	
}