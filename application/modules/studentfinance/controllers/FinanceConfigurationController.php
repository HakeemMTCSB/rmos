<?php 

class Studentfinance_FinanceConfigurationController extends Base_Base {

	private $_sis_session;
	
	public function init(){
		
		$this->_sis_session = new Zend_Session_Namespace('sis');
		
	}
	
	public function indexAction() {
		
			$this->view->title= $this->view->translate("Finance Configuration");
			
			$auth = Zend_Auth::getInstance();
			$finConfigDB = new Studentfinance_Model_DbTable_FinanceConfiguration();
				
			$data = $finConfigDB->getData();
			
			
			if ($this->getRequest()->isPost()) {
				
				$formData = $this->getRequest()->getPost();
								
				if(!isset($formData['os_balance'])){
					$formData['os_balance']=0;
				}
				
				if(!isset($formData['os_effective_date'])){
					$formData['os_effective_date']=NULL;
				}
				
				$formData['fin_updatedt']=date('Y-m-d H:i:s');  
				$formData['fin_updateby']=$auth->getIdentity()->iduser;  

				unset($formData['submit']);
				
				if(isset($formData['fin_id']) && $formData['fin_id']!=''){
					
					$finConfigDB->updateData($formData,$formData['fin_id']);
				}else{
					$finConfigDB->addData($formData);
				}
				
				$finConfigDB->addDataHistory($formData,$formData['fin_id']);
			
				$data = $finConfigDB->getData();
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			}
			
			$this->view->data = $data;
	}
	
	
}
?>