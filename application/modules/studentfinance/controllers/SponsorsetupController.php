<?php	
class Studentfinance_SponsorsetupController extends Base_Base { //Controller for the User Module

	private $lobjdeftype;
	private $_gobjlog;
	private $lobjPolicySetup;
	private $lobjconfig;
	private $lobjUser;
	private $lobjSubjectsetup;
	private $lobjcodegenObj;
	public $lobjform;


	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();

	}

	public function fnsetObj(){
			
		$this->lobjSponsorSetupForm = new Studentfinance_Form_Sponsorsetup();
		$this->lobjSponsorSetup = new Studentfinance_Model_DbTable_Sponsor();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
		$this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjSubjectsetup = new Application_Model_DbTable_Subjectsetup();
		$this->lobjcodegenObj = new Cms_CodeGeneration();

		$this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();
			
	}

	public function indexAction()
	{
		$this->view->title = 'Sponsor Setup';

		//Form
		$this->view->lobjform = $this->lobjform;

		$larrresult = $this->lobjSponsorSetup->fnSearchSponsorSetup( $post = NULL);

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->SponsorSetuppaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->SponsorSetuppaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->SponsorSetuppaginatorresult,$lintpage,$lintpagecount);
		}
		else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
			$larrformData = $this->_request->getPost ();

			$larrresult = $this->lobjSponsorSetup->fnSearchSponsorSetup($larrformData);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->SponsorSetuppaginatorresult = $larrresult;

		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/sponsorsetup/index');
		}


	}

	public function addAction()
	{
		$this->view->title = 'Add New Sponsor';

		//Form
		$this->view->lobjSponsorSetupForm = $this->lobjSponsorSetupForm;

		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
		$lIntIdUniversity = $this->gobjsessionsis->idUniversity;
		$larrconfig = $this->lobjSubjectsetup->getConfigDetail($lIntIdUniversity);

		//Render to View
		$this->view->lobjSponsorSetupForm->IdUniversity->setvalue ($lIntIdUniversity);
		$this->view->lobjSponsorSetupForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjSponsorSetupForm->UpdUser->setValue ($userId);

		//Sponsor Id
		$this->view->lobjSponsorSetupForm->SponsorId->setAttrib('readonly','true');
		$this->view->lobjSponsorSetupForm->SponsorId->setAttrib('required','false');
		$this->view->lobjSponsorSetupForm->SponsorId->setValue('xxx-xxx-xxx');

		$larrconfig[0]['SponsorIdFormat'] =  $this->lobjcodegenObj->buildFormat($larrconfig[0],'Sponsor');

		$larrformatkey = array('format'=>'SponsorIdFormat', 'prefix'=>'SponsorPrefix');
		$table = 'tbl_sponsor';
		$coloumn = 'SponsorCode';
		$lstrcoderesult = $this->lobjcodegenObj->studentfinanceIdGenration($lIntIdUniversity, $larrconfig[0],$table,$coloumn,$larrformatkey);
	
		//Country
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjSponsorSetupForm->Country->addMultiOptions($lobjcountry);

		//Fee Item
		$feeList = $this->lobjFeeItem->getData();
		foreach( $feeList as $item)
		{
			$this->view->lobjSponsorSetupForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name']);
		}


		//JS
		$this->view->lobjSponsorSetupForm->Country->setAttrib('OnChange','showstate();');
		//$this->view->lobjSponsorSetupForm->FeeCode->setAttrib('OnChange','showdesc();');
		$this->view->lobjSponsorSetupForm->State->setAttrib('OnChange','showcity();');
		$this->view->lobjSponsorSetupForm->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjSponsorSetupForm->Clear->setAttrib('OnClick','clearpageAdd();');
		$this->view->lobjSponsorSetupForm->Description->setAttrib('readonly','true');

		$this->view->lobjSponsorSetupForm->CoordinatorAdd->setAttrib('OnClick','validateCoordinator();');
		$this->view->lobjSponsorSetupForm->CoordinatorClear->setAttrib('OnClick','clearCoordinator();');
		


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost ();
			//var_dump(count($larrformData['coordinator_name'])); exit;
			$SponsorId = $lstrcoderesult;
				
			//$lstremail = $this->lobjSponsorSetup->fnCheckEmailId($larrformData); 
			//if($lstremail)
			//{	
				//$this->view->errMsg1 = '0';
				//$this->view->lobjSponsorSetupForm->populate($larrformData);
			//}
			//else{
				$this->lobjSponsorSetup->fnaddSponsorSetup($larrformData,$SponsorId);
				$this->_redirect( $this->baseUrl . '/studentfinance/sponsorsetup/');
			//}
			
		}
	}


	public function getdescAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjCommonModel = new App_Model_Common();
		$desc =  $this->_getParam ( 'idfeecode' );
		$larrdesc = $this->lobjSponsorSetup->fngetdesc($desc);
		echo $larrdesc;
	}


	public function editAction()
	{
		$this->view->title = 'Edit Sponsor';

		//Form
		$this->view->lobjSponsorSetupForm = $this->lobjSponsorSetupForm;

		$id = $this->_getParam ( 'id' );

		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
	
		//Render to View
		$this->view->lobjSponsorSetupForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjSponsorSetupForm->UpdUser->setValue ($userId);

		//Fee Item
		$feeList = $this->lobjFeeItem->getData();
		foreach( $feeList as $item)
		{
			$this->view->lobjSponsorSetupForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name']);
		}

		//Country
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjSponsorSetupForm->Country->addMultiOptions($lobjcountry);

		//JS
		$this->view->lobjSponsorSetupForm->Country->setAttrib('OnChange','showstate();');
		//$this->view->lobjSponsorSetupForm->FeeCode->setAttrib('OnChange','showdesc();');
		$this->view->lobjSponsorSetupForm->State->setAttrib('OnChange','showcity();');
		$this->view->lobjSponsorSetupForm->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjSponsorSetupForm->Clear->setAttrib('OnClick','clearpageAdd();');

		$this->view->lobjSponsorSetupForm->CoordinatorAdd->setAttrib('OnClick','validateCoordinator();');
		$this->view->lobjSponsorSetupForm->CoordinatorClear->setAttrib('OnClick','clearCoordinator();');


		$result = $this->lobjSponsorSetup->fngetsponsorsetupById($id);
		$this->view->result = $result;

		$paramArray = array('SponsorId' => $result[0]['SponsorCode'],
				'FirstName' => $result[0]['fName'],
				'LastName' => $result[0]['lName'],
				'Address1' => $result[0]['Add1'],
				'Address2' => $result[0]['Add2'],
				'Country' => $result[0]['Country'],
				'State' => $result[0]['State'],
				'City' => $result[0]['City'],
				'Postal' => $result[0]['zipCode'],
				'Phone' => $result[0]['CellPhone'],
				'Fax' => $result[0]['Fax'],
				'EmailAddress' => $result[0]['Email'],
				'UpdUser'=>$result[0]['UpdUser'],
				'UpdDate'=>$result[0]['UpdDate']);

		$this->lobjSponsorSetupForm->populate($paramArray);

		$this->view->lobjSponsorSetupForm->SponsorId->setAttrib('readonly','true');
		$this->view->lobjSponsorSetupForm->Description->setAttrib('readonly','true');
		$this->view->lobjSponsorSetupForm->EmailAddress->setAttrib('readonly','true');
		
		//get fee info
		$feeinfo = $this->lobjSponsorSetup->getFeeInfo($result[0]['idsponsor']);

		//get coordinators
		$coordinators = $this->lobjSponsorSetup->getCoordinator($result[0]['idsponsor']);

		//
		$this->view->feeinfo = $feeinfo;
		$this->view->coordinators = $coordinators;

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
                        
                        if (isset($larrformData['active']) && count($larrformData['active']) > 0){
                            foreach ($larrformData['active'] as $key=>$value){
                                
                                $dataActive = array(
                                    'c_active'=>$value
                                );
                                //update coordinator status
                                $this->lobjSponsorSetup->updateCoordinator($dataActive, $key);
                            }
                        }
                        
			$this->lobjSponsorSetup->fnupdateSponsorSetup($larrformData,$id);
			

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/studentfinance/sponsorsetup/edit/id/'.$id);
		}
	}

	public function deleteCoordinatorAction()
	{
		$db = getDB();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('tbl_sponsor_coordinator', 'c_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteFeeAction()
	{
		$db = getDB();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('tbl_sponsor_fee_info', 'IdSponsorFeeInfo = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

}