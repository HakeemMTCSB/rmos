<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 30/9/2016
 * Time: 11:13 AM
 */
class Studentfinance_AdvancePaymentUpdateController extends Zend_Controller_Action {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_AdvancePaymentUpdate();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Update Utilization Date');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $this->view->formData = $formData;

            $list = $this->model->getAdvancePaymentUtilized($formData);
            $this->view->list = $list;
        }
    }

    public function updateDateAction(){

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['utidate']) && count($formData['utidate']) > 0){
                foreach ($formData['utidate'] as $key => $date){
                    $data = array(
                        'advpydet_upddate' => date('Y-m-d', strtotime($date))
                    );
                    $this->model->updateAdvancePayment($data, $key);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => 'Date Updated'));
            $this->_redirect($this->baseUrl . '/studentfinance/advance-payment-update/');
        }

        $this->_redirect($this->baseUrl . '/studentfinance/advance-payment-update/');
        exit;
    }
}