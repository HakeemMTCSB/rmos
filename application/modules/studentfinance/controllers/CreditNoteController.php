<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_CreditNoteController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_CreditNote();
		$this->_DbObj = $db;
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() {
		
		//title
    	$this->view->title= $this->view->translate("Credit Note");
    	
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$p_data = $this->_DbObj->getPaginateListData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
	
	    	$this->view->paginator = $paginator;
			$this->view->formData = $formData;
			
			$this->view->type = $formData['type'];

		}
		
	}
	
	public function entryAction() {
		//title
    	$this->view->title= $this->view->translate("Credit Note");
    	
    	$msg = $this->_getParam('msg', null);
    	
    	$form = new Studentfinance_Form_CreditNote();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
				
				$data = array(
							'cn_billing_no' => $formData['cn_billing_no'],
							'cn_fomulir' => $formData['payee'],
							'appl_id' => $formData['appl_id'],
							'cn_amount' => $formData['cn_amount'],
							'cn_description' => $formData['cn_description'],
						);
				
				$creditNoteDb->insert($data);
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'credit-note', 'action'=>'entry', 'msg'=>'Credit Note created'),'default',true));
				
			}else{
				$form->populate($formData);
			}
    		
    	}
    	
    	if( $msg!=null ){
    		$this->view->noticeMessage = $msg;
    	}
    	
    	$this->view->form = $form;
    	    	
	}
	
	public function searchStudentAction(){
		$this->_helper->layout()->disableLayout();
		
		$name = $this->_getParam('name', null);
		$id = $this->_getParam('id', null);
		
		if ($this->getRequest()->isPost()) {
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
	        $ajaxContext->addActionContext('view', 'html');
	        $ajaxContext->initContext();
	            
		  	$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()
		                 ->from(array('ap'=>'applicant_profile'),array('appl_id','concat_ws(\' \',ap.appl_fname,ap.appl_mname,ap.appl_lname)name','appl_email'));
		                 //->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at.at_pes_id'));

		    if($name!=null && $name!=""){
		    	$select->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
		    }
		    
			if($id!=null){
		    	$select->where("ap.appl_id like '%".$id."%'");
		    }
		    //echo $select;
		    
	        $row = $db->fetchAll($select);
		  
			$ajaxContext->addActionContext('view', 'html')
	                    ->addActionContext('form', 'html')
	                    ->addActionContext('process', 'json')
	                    ->initContext();
	
			$json = Zend_Json::encode($row);
			
			echo $json;
			exit();
    
		}
	}
	
	public function searchBillAction(){
		$this->_helper->layout()->disableLayout();		 

		$txn_id = $this->_getParam('txn_id', null);
		
		if ($this->getRequest()->isPost()) {
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
	        $ajaxContext->addActionContext('view', 'html');
	        $ajaxContext->initContext();
	            
		  	$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()
		                 ->from(array('im'=>'invoice_main'))
		                 ->join(array('ap'=>'applicant_profile'),'ap.appl_id = im.appl_id', array('concat_ws(\' \',ap.appl_fname,ap.appl_mname,ap.appl_lname)name'))
		                 ->where('im.no_fomulir = ?',$txn_id)
		                 ->order('im.date_create');

		    //echo $select;
		    //exit;
		    
	        $row = $db->fetchAll($select);
		  
			$ajaxContext->addActionContext('view', 'html')
	                    ->addActionContext('form', 'html')
	                    ->addActionContext('process', 'json')
	                    ->initContext();
	
			$json = Zend_Json::encode($row);
			
			echo $json;
			exit();
    
		}
	}
	
	public function approveRejectAction(){
		//title
    	$this->view->title= $this->view->translate("Credit Note : Approve / Cancelation");	
	}
	
	public function approvalAction(){
		$this->_helper->layout()->disableLayout();
		
		//paginator
		$data = $this->_DbObj->getPaginateData(false);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function approvalDetailAction(){
		$this->view->title= $this->view->translate("Credit Note : Approval Detail");	
		
		$id = $this->_getParam('id', null);
		
		if($id==null){
			
		}else{
			
			//process form
			if ($this->getRequest()->isPost()) {
				$formData = $this->getRequest()->getPost();

				$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
				
				$auth = Zend_Auth::getInstance();
				
				//approve
				if($formData['result']==1){
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->beginTransaction();
					
					try {
						//get CN detail
						$credit_note = $creditNoteDb->getData($formData['cn_id']);
												
						//get invoice affected
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
						$invoice = $invoiceMainDb->getInvoiceData($credit_note['cn_billing_no'],true);
																		
						$bill_amount = $invoice['bill_amount'];
						$bill_paid = $invoice['bill_paid'];
						$bill_balance = $invoice['bill_balance'];
						$cn_amount = $invoice['cn_amount'];
						
						//update invoice
						if($bill_paid!=null && $bill_paid>0){
							$paid = $bill_paid + $credit_note['cn_amount'];
							
							if($paid > $bill_amount){
								$paid = $bill_amount;
								$balance = 0;
							}else{
								$balance = $bill_balance - $credit_note['cn_amount'];
							}
							
						}else{
							$paid = 0;
							$balance = 0;
						}
						
						$data = array(
									'cn_amount' => $cn_amount + $credit_note['cn_amount'],
									'bill_balance' => $balance
								);
								
						$invoiceMainDb->update($data, 'id = '.$invoice['id']);		
						
						
						//check for excess payment and create advance payment
						$excess_payment = $bill_paid - ($bill_amount - $credit_note['cn_amount']);
						
						if($excess_payment > 0){//create advance payment
							$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				       		
				       		$data = array(
				       			'advpy_appl_id' => $invoice['appl_id'],
				       			'advpy_acad_year_id' => $invoice['academic_year'],
				       			'advpy_sem_id' => $invoice['semester'],
				       			'advpy_prog_code' => $invoice['program_code'],
				       			'advpy_fomulir' => $invoice['no_fomulir'],
				       			'advpy_invoice_no' => $invoice['bill_number'],
				       			'advpy_invoice_id' => $invoice['id'],
				       			'advpy_payment_id' => null,
				       			'advpy_description' => 'Credit note for invoice no:'.$invoice['bill_number'],
				       			'advpy_amount' => $excess_payment,
				       			'advpy_total_paid' => 0,
				       			'advpy_total_balance' => $excess_payment,
				       			'advpy_status' => 'A'
				       		);
				       		$advancePaymentDb->insert($data);
						}
						
						echo "excess Payment = ".$excess_payment;
						
						
						//update cn
						$data = array(
									'cn_approver' => $auth->getIdentity()->iduser,
									'cn_approve_date' => date('Y-m-d H:i:s')
								);
	
						$creditNoteDb->update($data, 'cn_id = '.(int)$formData['cn_id']);
						
						$db->commit();
						
					}catch (Exception $e) {
						echo "Error in Approving Credit Note. <br />";
						echo $e->getMessage();
						
						echo "<pre>";
						print_r($e->getTrace());
						echo "</pre>";
						
						$db->rollBack();
		    			
		    			exit;
					}
				}else
				//cancel
				if($formData['result']==2){
					$data = array(
								'cn_cancel_by' => $auth->getIdentity()->iduser,
								'cn_cancel_date' => date('Y-m-d H:i:s')
							);
							
					$creditNoteDb->update($data, 'cn_id = '.(int)$formData['cn_id']);
				}
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'credit-note', 'action'=>'approve-reject', 'msg'=>'Credit Note Updated'),'default',true));
			}
			
			//cn data
			$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
			$this->view->data = $creditNoteDb->getData($id);
			
			//invoice data
			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoice = $invoiceMainDb->getInvoiceData($this->view->data['cn_billing_no']);
			
			//profile
			$profileDb = new App_Model_Application_DbTable_ApplicantProfile();
			$this->view->profile = $profileDb->getData($this->view->data['appl_id']);
			
			//transaction
			$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
			$this->view->transaction = $transactionDb->getTransactionDataByFomulir($this->view->data['cn_fomulir']);
			
			//program
			$programDb = new App_Model_Record_DbTable_Program();
			$this->view->program = $programDb->getProgrambyCode($invoice['program_code']);
						
		}
	}
	
	public function historyAction(){
		$this->_helper->layout()->disableLayout();
		
		//paginator
		$data = $this->_DbObj->getPaginateDataHistory(true,true);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function searchCnStudentAction(){
		$this->_helper->layout()->disableLayout();
		
		$name = $this->_getParam('name', null);
		$id = $this->_getParam('id', null);
		
		if ($this->getRequest()->isPost()) {
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
	        $ajaxContext->addActionContext('view', 'html');
	        $ajaxContext->initContext();
	            
		  	$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()
		                 ->from(array('ap'=>'applicant_profile'),array('appl_id','concat_ws(\' \',ap.appl_fname,ap.appl_mname,ap.appl_lname)name','appl_email'));
		                 //->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at.at_pes_id'));

		    if($name!=null){
		    	$select->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
		    }
		    
			if($id!=null){
		    	$select->where("ap.appl_id like %?%", $id);
		    }
		    //echo $select;
		    
	        $row = $db->fetchAll($select);
		  
			$ajaxContext->addActionContext('view', 'html')
	                    ->addActionContext('form', 'html')
	                    ->addActionContext('process', 'json')
	                    ->initContext();
	
			$json = Zend_Json::encode($row);
			
			echo $json;
			exit();
    
		}
	}
	
public function generateCreditNoteAction(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
			
//			echo "<pre>";
//			print_r($formData);
//			exit;
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$statustype = $formData['statustype'];
			
			$initial = 0;
			if($statustype == 3){
				$initial = 1;
			}
			
			$idSelect = $formData['id'];
			$n = count($idSelect);
			$i=0;
			while($i<$n){
				
				$id = $idSelect[$i];
				
				$arraySelected = explode(':',$id);
				
				$no = $arraySelected[0];
				$invoiceID = $arraySelected[1];
				$subjectID = $arraySelected[2];
				$itemID = $arraySelected[3];
				$studentID = $arraySelected[4];
				$percentage = $formData['refund'][$no];
				
				$invoiceClass = new icampus_Function_Studentfinance_Invoice();
				if($percentage != 0){
					$generated = $invoiceClass->generateCreditNote($studentID,$subjectID,$invoiceID,$itemID,$percentage,1,$initial);//$studentID,$subjectID,$invoiceID,$itemID=0,$percentageDefined=0,$commit=0
					
					if($generated){
						$select = $db->select()
								->from(array('a'=>'tbl_studentregsubjects_detail'))
								->where('a.student_id =?',$studentID)
								->where('a.subject_id =?',$subjectID)
								->where('a.item_id =?',$itemID);
		       				$results = $db->fetchRow($select);  
		       
		       				if($results){
								//update invoice_id as null
								$data_update = array('invoice_id' =>NULL);
//								$db->update('tbl_studentregsubjects_detail',$data_update, 'id = '.$results['id']);
		       				}
					}
	       				
				}
				
				$i++;
				
			}
			
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Credit Note Generated');
//			exit;
			$this->_redirect( $this->baseUrl . '/studentfinance/migrate/credit-not-generated');
			
		}
		
		
		
	}
	
	public function cancelAction(){
		
		$id = $this->_getParam('id', 0);
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		
				
		if($id != 0){
			
			$info = $this->_DbObj->getData($id);
			
	//		if($info['cn_status'] == 'A'){
	//			$newStatus = 'A';
	//		}else{
				$newStatus = 'X';
	//		}

			if($info['status'] != 'X'){
				$upd_data = array(
					'cn_status'=> $newStatus,
					'cn_cancel_by'=> $getUserIdentity->id,
					'cn_cancel_date'=> date('Y-m-d H:i:s')
				);
				
				$this->_DbObj->update($upd_data, array('cn_id = ?' => $id) );
				
				//revert invoice
				$invId = $info['cn_invoice_id'];
				$this->revertInvoice($id);
			}
				
			$this->_redirect( $this->baseUrl . '/studentfinance/credit-note/view/id/'.$id);
			
			
		}
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			$proforma = $formData['invoice_id'];
			$n=0;
			while($n < count($proforma)){
				$proforma_id = $proforma[$n];
				
				$info = $this->_DbObj->getData($proforma_id);
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
				
//				if($info['status'] == 'A'){
//					$newStatus = 'A';
//				}else{
					$newStatus = 'X';
//				}
			
				if($info['status'] != 'X'){
					$upd_data = array(
						'cn_status'=> $newStatus,
						'cn_cancel_by'=> $getUserIdentity->id,
						'cn_cancel_date'=> date('Y-m-d H:i:s')
					);
					
					$this->_DbObj->update($upd_data, array('cn_id = ?' => $proforma_id) );
					
					//revert invoice
					$this->revertInvoice($proforma_id);
				}
				$n++;
			}
		}
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Credit Note Cancelled');
		$this->_redirect( $this->baseUrl . '/studentfinance/credit-note');
		exit;
		
    }
	public function addAction(){
		
		$this->view->title = $this->view->translate('Add Credit Note');
		
		$payee_type_list = $this->lobjdeftype->fnGetDefinationsByLocale('Payee Type');
		
		$this->view->payee_type_list = $payee_type_list;
		
		//currency list
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$this->view->currencyList = $currencyDb->getList();
		
		
		
		if ( $this->getRequest()->isPost() )
			{
			$formData = $this->getRequest()->getPost();
			
			//echo "<pre>";
			//print_r($formData);
			//exit;

			$formData['invoice_id'] = explode('-', $formData['invoice_id']);

			$type = $formData['payee_type'];
			$txnId = $formData['payee_id'];
			$InvoiceID = $formData['invoice_id'][0];
			$SubID = $formData['invoice_id'][1];
			$calcType = $formData['calculation_type_id'];
			$dateCreated = $formData['invoice_date'];
			
			
			if($calcType == 0){//percentage
				$percentage = $formData['amount'];
				$amount = 0;
			}else{//amount
				$percentage= 0;
				$amount = $formData['amount'];
			}
			
			if($type == 645){
		    	$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
		    	$registration = $applicantProfileDb->getApplicantRegistrationDetail($txnId);
	    	}elseif($type == 646){
	    		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
		    	$registration = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
	    	}
    	
			
			$_DbObj = new Studentfinance_Model_DbTable_InvoiceDetail();
			$invoiceData = $_DbObj->getData($InvoiceID);
			
			$InvoiceMainID = $invoiceData['invoice_main_id'];
			
			$desc = '';
			if($formData['bill_description']==''){
				$desc = $invoiceData['fee_item_description'];
			}else{
				$desc = $formData['bill_description'];
			}
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$db->beginTransaction();
			try{
				$invoiceClass = new icampus_Function_Studentfinance_Invoice();
				$cn_id = $invoiceClass->generateCreditNoteEntryDetail($txnId,$InvoiceID,$percentage,$amount,0,$dateCreated,$SubID);
				$db->commit();
				
				if($cn_id){
					$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Credit Note Added');
					$this->_redirect( $this->baseUrl . '/studentfinance/credit-note/view/id/'.$cn_id);
				}else{
					$this->gobjsessionsis->flash = array('type'=>'error','message'=>'Credit Note Unsuccessful');
					$this->_redirect( $this->baseUrl . '/studentfinance/credit-note/add');
				}
				
			}catch (Exception $e){
        		$db->rollBack();
        		throw $e;
			}
		}
		
	}
	
public function searchPayeeAction(){
		
		$this->_helper->layout()->disableLayout();
		
		$type = $this->_getParam('rcp_payee_type', null);
		$id = $this->_getParam('payee_id', null);
		$invoice_no = $this->_getParam('payee_invoice_no', false);
		$proforma_invoice_no = $this->_getParam('payee_proforma_no', false);
		
		$sis_session = new Zend_Session_Namespace('sis');
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			 
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$result = null;
			
			if($formData['payee_id']!='' || $formData['payee_proforma_no']!='' || $formData['payee_invoice_no']!='' ){
			
			 
				if($formData['rcp_payee_type']==645){//applicant
					
					$select = $db->select()
								->from(array('ap'=>'applicant_profile'))
								->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array('at_pes_id', 'at_trans_id','fs_id'=>'at_fs_id'))
								->join(array('app'=>'applicant_program'), 'app.ap_at_trans_id = at.at_trans_id', array('ap_prog_id'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=app.ap_prog_id',array('program_name'=>'p.ProgramName','*'));
			
					 
					if($formData['payee_id']!=''){
						$select->where("at.at_trans_id = ?",$formData['payee_id']);
					}
				
					 
				}else
				if($formData['rcp_payee_type']==646){ //student
			
					$select = $db->select()
								->from(array('sp'=>'student_profile'))
								->join(array('sr'=>'tbl_studentregistration'), 'sr.sp_id = sp.id', array('sr.*'))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('program_name'=>'p.ProgramName','*'));
//								->where('sr.ProfileStatus = 92');
			
					if($formData['payee_id']!=''){
						$select->where("sr.IdStudentRegistration = ? ",$formData['payee_id']);
					}
					
			
				}
				$row = $db->fetchRow($select);
				
				
				//get default currency
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currencyData = $currencyDb->getDefaultCurrency();
				$defaultCurrency = $currencyData['cur_id'];
					
				if($row){
				
				
					if($type == 645){
						$result = array(
							'payee_id' => $row['at_pes_id'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['appl_id'],
							'trans_id' => $row['at_trans_id'],
							'fs_id' => $row['fs_id'],
							'program_id' => $row['ap_prog_id'],
							'scheme_id' => $row['IdScheme'],
						);
					}elseif($type == 646){
						$result = array(
							'payee_id' => $row['registrationId'],
							'payee_name' => preg_replace('!\s+!', ' ', $row['appl_fname']." ".$row['appl_mname']." ".$row['appl_lname']),
							'payee_type' => $type,
							'program_name' => $row['program_name'],
							'appl_id' => $row['IdStudentRegistration'],
							'trans_id' => $row['IdStudentRegistration'],
							'fs_id' => $row['fs_id'],
							'program_id' => $row['IdProgram'],
							'scheme_id' => $row['IdScheme'],
						);
					}
				
				
					//search invoice
					$select_invoice = $db->select()
					->from(array('i'=>'credit_note'),array('*','fee_item_description'=>'cn_description','amount'=>"cn_amount",'status'=>'cn_status'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.cn_cur_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					->where("cn_status = 'A'")
					;

					if($type == 645){
						$select_invoice->where('i.cn_trans_id = ?', $result['trans_id']);
					}elseif($type == 646){
						$select_invoice->where('i.cn_IdStudentRegistration= ?', $result['trans_id']);
					}
					
				
					$invoice = $db->fetchAll($select_invoice);
					
					$totalMyr=0;
					$totalUsd=0;
					$m=0;
					foreach($invoice as $inv){
						$result['invoice'][$m] = $inv;
						

						$result['invoice'][$m]['cur_def'] = $inv['cn_cur_id'];
						$result['invoice'][$m]['amount_default'] = $inv['amount'];
						
						$stsinv = '';
						if( $inv['cn_status'] == 'A'){
							$stsinv = 'Active';
						}elseif( $inv['cn_status'] == 'X'){
							$stsinv = 'Cancel';
						}elseif( $inv['cn_status'] == 'E'){
							$stsinv = 'Exempted';
						}
						
						$result['invoice'][$m]['status'] = $stsinv;
						
						$m++;
					}
				
				}
		
			}
			
//			echo "<pre>";
//			print_r($result);
		
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($result);
				
			echo $json;
			exit();
		
		}
	}
	
	public function viewAction()
	{
		$this->view->title = $this->view->translate('Credit Note Details');

		$id = $this->_getParam('id');

		$info = $this->_DbObj->getData($id);
		if ( empty($info) )
		{
			throw new Exception('Invalid ID');
		}

		$this->view->info = $info;
		$this->view->id = $id;
	}
	
	private function revertInvoice($cnID){
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$info = $this->_DbObj->getData($cnID);
		
		$cn_amount = $info['cn_amount_cn'];
		
		$invIdDet = $info['invoice_detail_id'];
		$invId = $info['invoice_main_id'];
	
		//update invoice detail
		$invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invDetail = $invoiceDetailDB->getData($invIdDet);
		
		$amount = $invDetail['balance']  + $cn_amount ;
		$amountcn = $cn_amount - $invDetail['cn_amount'];
		$upd_data_inv = array(
			'balance'=> $amount,
			'cn_amount'=> $amountcn,
		);
	
		$invoiceDetailDB->update($upd_data_inv, array('id = ?' => $invIdDet) );
		
		$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
		$invMain = $invoiceMainDB->getData($invId);
		
		$amount = $invMain['bill_balance']  + $cn_amount ;
		$amountcn =  $cn_amount - $invMain['cn_amount'];
		$upd_data_inv = array(
			'bill_balance'=> $amount,
			'cn_amount'=> $amountcn,
			'upd_by'=> $getUserIdentity->id,
			'upd_date'=> date('Y-m-d H:i:s')
		);
		
		$invoiceMainDB->update($upd_data_inv, array('id = ?' => $invId) );
	}
	
}

