<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_BatchSemesterInvoiceController extends Base_Base {

	private $_sis_session;

	public function init(){
		$this->_sis_session = new Zend_Session_Namespace('sis');
	}

	public function indexAction() {

	    $status = $this->_getParam('status', null);
	    
	    if($status){
    	    if($status==1){
    	      $this->view->noticeSuccess = "Batch Invoice Created";
    	    }else
            if($status==0){
              $this->view->noticeError = "Unable to create batch invoice";
    	    }
	    }
		//title
		$this->view->title= $this->view->translate("Semester Invoicing");
		
		$batchInvoiceDb = new Studentfinance_Model_DbTable_InvoiceBatch();
		$select = $batchInvoiceDb->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
		$select->setIntegrityCheck(false)
		      ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = invoice_batch.semester_id', array('semester_name'=>'s.SemesterMainDefaultLanguage'))
		      ->join(array('f'=>'tbl_collegemaster'),'f.IdCollege = invoice_batch.faculty_id', array('faculty_name'=>'f.ArabicName'))
		      ->join(array('p'=>'tbl_program'),'p.IdProgram = invoice_batch.program_id', array('program_name'=>'p.ArabicName', 'program_code'=>'p.ProgramCode'))
		      ->order('create_date desc');
		
		$batch_list = $batchInvoiceDb->fetchAll($select);
		$this->view->batch_list = $batch_list->toArray();
		 
	}
	
	public function newInvoiceAction(){
		//title
		$this->view->title= $this->view->translate("Semester Invoicing - Create New");
		
		$ses_batch_invoice = new Zend_Session_Namespace('studentfinance_batch_invoice');
		$ses_batch_invoice->setExpirationSeconds(900);
		
		$step = $this->_getParam('step', 1);
		$this->view->step = $step;
		
		if($step!=5 && $step!=6){
		  unset($ses_batch_invoice->student_list);
		}
		
		if($step==1){ //STEP 1
			
			//Zend_Session::namespaceUnset('studentfinance_batch_invoice');
			
			if ($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
				
				if($formData['semester_id']!=null){
					
					$ses_batch_invoice->semester_id = $formData['semester_id'];
				
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>2),'default',true));
				}
				
			}
			
			$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
			$this->view->semester_list = $semesterDb->fnGetSemestermasterList();
			
			if(isset($ses_batch_invoice->semester_id)){
				$this->view->semester_id = $ses_batch_invoice->semester_id;
			}
			
			
		}else
		if($step==2){ //STEP 2
			
			//step validation
			if(!isset($ses_batch_invoice->semester_id)){
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>1),'default',true));
			}
			
			if ($this->getRequest()->isPost()) {
					
				$formData = $this->getRequest()->getPost();
			
				if($formData['program_id']!=null){
						
					$ses_batch_invoice->program_id = $formData['program_id'];
			
					//redirect
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>3),'default',true));
				}
			
			}
			
			//semester
			$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
			
			
			//faculty
			$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
			$college_list = $collegeDb->getCollege();
			
			//program
			$programDb = new GeneralSetup_Model_DbTable_Program();
			foreach ($college_list as $index => $faculty){
				$programs = $programDb->getProgramDataByfacultyandscheme($faculty['IdCollege'],$semester['Scheme']);
				
				if($programs){
					$college_list[$index]['program_list'] = $programs;
				}
			}
			
			$this->view->faculty_list = $college_list;
			
			if(isset($ses_batch_invoice->program_id)){
				$this->view->program_id = $ses_batch_invoice->program_id;
			}

			
				
		}else
		if($step==3){ //STEP 3
			
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>1),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->program_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>2),'default',true));
    		}
    		
    		if ($this->getRequest()->isPost()) {
    		  	
    		  $formData = $this->getRequest()->getPost();
    		  
    		  $ses_batch_invoice->intake = $formData['intake'];
    		  
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>4),'default',true));
    		  
    		}
    		
    		//get intake with not graduated student
    		$intakeDb = new App_Model_Record_DbTable_Intake();
    		
    		$intakeList = $intakeDb->getIntakeWithStudent(92,$ses_batch_invoice->program_id);
    		$this->view->intake_list = $intakeList;
    		
    		if(isset($ses_batch_invoice->intake)){
    		  $this->view->selected_intake = $ses_batch_invoice->intake;
    		}
    		
    		
				
		}else
		if($step==4){ //STEP 4
		
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>1),'default',true));
    		}else
    		if(!isset($ses_batch_invoice->program_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>2),'default',true));
    		}else
  		    if(!isset($ses_batch_invoice->intake)){
  		      $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>3),'default',true));
  		    }
    			
    		if ($this->getRequest()->isPost()) {
    		  	
    		  $formData = $this->getRequest()->getPost();
    		
    		  $ses_batch_invoice->fee_item = $formData['fi_id'];
    		
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>5),'default',true));
    		
    		}
    			
    		//fee item
    		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
    		$feeItem = $feeItemDb->getActiveFeeItem();
    		$this->view->fee_item_list = $feeItem;
    			
    		if(isset($ses_batch_invoice->fee_item)){
    		  $this->view->fee_item = $ses_batch_invoice->fee_item;
    		}
    		
				
		}else
		if($step==5){ //STEP 5
		
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>1),'default',true));
    		}else
    		if(!isset($ses_batch_invoice->program_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>2),'default',true));
    		}else
    		if(!isset($ses_batch_invoice->intake)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>3),'default',true));
    		}else  
    		if(!isset($ses_batch_invoice->fee_item)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>4),'default',true));
    		}
    		
    		if ($this->getRequest()->isPost()) {
    		  	
    		  $formData = $this->getRequest()->getPost();
    		  
    		  //semester 
    		  $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		  $semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		  
    		  //academic year
    		  $academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    		  $academicYear = $academicYearDb->getData($semester['idacadyear']);
    		  
    		  //program
    		  $programDb = new GeneralSetup_Model_DbTable_Program();
    		  $program = $programDb->fngetProgramData($ses_batch_invoice->program_id);
    		  
    		  //faculty
    		  $collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();
    		  $faculty = $collegeDb->fetchRow('IdCollege ='.$program['IdCollege'])->toArray();
    		  
    		  $student_list = array();
    		  foreach ($formData['id'] as $sid){
    		    //invoice
    		    $invoice = $this->getInvoiceData($formData['sid'][$sid],$formData['fs_id'][$sid]);
    		    
    		    if($invoice['amount']>0){
    		    
        		    //registration detail
                    $studentRegistrationDb = new App_Model_Record_DbTable_StudentRegistration();
                    $registrationData = $studentRegistrationDb->getStudentInfo($formData['sid'][$sid]);
                    
                    //intake detail
                    $intakeDb = new App_Model_Record_DbTable_Intake();
                    $intake = $intakeDb->getData($registrationData['IdIntake']);
                    
        		    //student profile
        		    $studentProfileDb = new Records_Model_DbTable_Studentprofile();
        		    $profile = $studentProfileDb->fetchRow('appl_id ='.$registrationData['IdApplication'])->toArray();
        		    
        		    //student data
        		    $student_list[] = array(
        		      'student_id' => $formData['sid'][$sid],
        		      'fs_id' => $formData['fs_id'][$sid],
        		      'profile'=>$profile,  
        		      'registration'=>$registrationData,  
        		      'intake'=>$intake,  
        		      'semester' => $semester,
        		      'academic_year' => $academicYear,  
        		      'faculty' => $faculty,
        		      'program' => $program,
        		      'invoice' => $invoice 
        		    );
        		    
    		    }
    		  }
    		  
    		  $ses_batch_invoice->student_list = $student_list;
    		  
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>6),'default',true)); 
    		}
    			
    			
    		//semester
    		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		$this->view->semester = $semester;
    		
    		//program
    		$programDb = new GeneralSetup_Model_DbTable_Program();
    		$program = $programDb->fngetProgramData($ses_batch_invoice->program_id);
    		$this->view->program = $program;
    		
    		//intake
    		$intakeDb = new App_Model_Record_DbTable_Intake();
    		$intake_list = null;
    		foreach ($ses_batch_invoice->intake as $intake){
    		  $intake_list[] = $intakeDb->getData($intake);
    		}
    		$this->view->intake = $intake_list;
    			
    		//get student in program which is not a new student
    		$studentRegistrationDb = new App_Model_Record_DbTable_StudentRegistration();
    		$student_list = $studentRegistrationDb->getStudentLevelByProgram($ses_batch_invoice->program_id,$ses_batch_invoice->intake);
    		
    			
    		$feeStructure = new Studentfinance_Model_DbTable_FeeStructure();
    			
    		$db = Zend_Db_Table::getDefaultAdapter();
    			
    		for($i=0; $i<sizeof($student_list); $i++){
    		
    		  //get current semester level
    		  $sql = $db->select()
    		  ->from(array('sss' => 'tbl_studentsemesterstatus'), array(new Zend_Db_Expr('max(Level) as Level')))
    		  ->where('sss.IdStudentRegistration  = ?', $student_list[$i]['student_id']);
    		
    		  $result = $db->fetchRow($sql);
    		  
    		  
    		  if( $result['Level'] ){
    		    $student_list[$i]['current_level'] = $result['Level'];
    		  }else{
    		    //check if senior student then hardcode level
    		    $intake_year = substr($student_list[$i]['intake_name'], 0,4);
    		    $cur_sem_year = substr($semester['SemesterMainCode'], 0,4);
    		    
    		    if($intake_year<2013){
    		      $student_list[$i]['current_level']=0;
    		      
    		      while($intake_year<=$cur_sem_year){
    		        //check current gasal or genap for currencty
    		        if($intake_year == $cur_sem_year){
    		          
    		          if($semester['SemesterCountType']==1){
    		            $student_list[$i]['current_level'] += 1;
    		          }else{
    		            $student_list[$i]['current_level'] += 2;
    		          }
    		          
    		        }else{
    		          $student_list[$i]['current_level'] += 2;
    		        }
    		        
    		        $intake_year++;
    		      }
    		      
    		      //remove 1 because we will add 1 in view
    		      $student_list[$i]['current_level'] -= 1;
    		      
    		    }else{
    		      $student_list[$i]['current_level'] = 0;
    		      
    		      //unset($student_list[$i]);
    		    }
    		  }
    		
    		  //get fee structure
    		  if($student_list[$i]['nationality']!=96){
    		    $student_category = 315;
    		  }else{
    		    $student_category = 314;
    		  }
    		  
    		  
    		  $student_list[$i]['fee_structure'] = $feeStructure->getApplicantFeeStructure($student_list[$i]['intake_id'],$ses_batch_invoice->program_id,$student_category);
    		
    		}
    			
    		//$student_list = array_values($student_list);
    			
    		//clean new student from batch invoice
    		foreach ($student_list as $index=>$student){
    		  if($student['current_level']==0){
    		    unset($student_list[$index]);
    		  }
    		}
    		$student_list = array_values($student_list);
    		
    		
    		
    		$this->view->student_list = $student_list;
    		
    		if(isset($ses_batch_invoice->student_list)){
    		  $arr_std_id = array();
    		  foreach ($ses_batch_invoice->student_list as $std){
    		    $arr_std_id[] = $std['student_id'];
    		  }
    		  $this->view->student_selected = $arr_std_id;
    		  
    		}
    		
    		
    		
				
		}else
		if($step==6){// STEP 6
		
    		//step validation
    		if(!isset($ses_batch_invoice->semester_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>1),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->program_id)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>2),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->intake)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>3),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->fee_item)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>4),'default',true));
    		}else
    		  if(!isset($ses_batch_invoice->student_list)){
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'new-invoice', 'step'=>5),'default',true));
    		}
    		
    		
    		
    		if ($this->getRequest()->isPost()) {
    		  
    		  $status = $this->saveBatchInvoice();
    		  
    		  if($status){
    		    Zend_Session::namespaceUnset('studentfinance_batch_invoice');
    		  }
    		  
    		  //redirect
    		  $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'batch-semester-invoice', 'action'=>'index', 'status'=>$status),'default',true));
    		  exit;
    		}
    		
    		//semester
    		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
    		$semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
    		$this->view->semester = $semester;
    		 
    		//program
    		$programDb = new GeneralSetup_Model_DbTable_Program();
    		$program = $programDb->fngetProgramData($ses_batch_invoice->program_id);
    		$this->view->program = $program;
    		
    		//intake
    		$intakeDb = new App_Model_Record_DbTable_Intake();
    		$intake_list = null;
    		foreach ($ses_batch_invoice->intake as $intake){
    		  $intake_list[] = $intakeDb->getData($intake);
    		}
    		$this->view->intake = $intake_list;
    		
    		//student list
    		$this->view->student_list = $ses_batch_invoice->student_list;
    		
		}else{
		  
		}
		
	}
	
	public function previewInvoiceAction(){
	  
	  if( $this->getRequest()->isXmlHttpRequest() ){
	    $this->_helper->layout->disableLayout();
	  }
	  
	  //student_id
	  $idRegistration = $this->_getParam('id', null);
	  //fee structure
	  $feeStructureId = $this->_getParam('fid', null);
	  
	  $invoice = $this->getInvoiceData($idRegistration,$feeStructureId);
	  $this->view->invoice = $invoice;
	  
	}
	
	public function viewInvoiceAction(){
	  
	  if( $this->getRequest()->isXmlHttpRequest() ){
	    $this->_helper->layout->disableLayout();
	  }
	   
	  $batch_id = $this->_getParam('batch', null);
	  
	  //head data
	  $batchInvoiceDb = new Studentfinance_Model_DbTable_InvoiceBatch();
	  $select = $batchInvoiceDb->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
	  $select->setIntegrityCheck(false)
	  ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster = invoice_batch.semester_id', array('semester_name'=>'s.SemesterMainDefaultLanguage'))
	  ->join(array('f'=>'tbl_collegemaster'),'f.IdCollege = invoice_batch.faculty_id', array('faculty_name'=>'f.ArabicName'))
	  ->join(array('p'=>'tbl_program'),'p.IdProgram = invoice_batch.program_id', array('program_name'=>'p.ArabicName', 'program_code'=>'p.ProgramCode'))
	  ->where('id = ?',$batch_id)
	  ->order('create_date');
	  
	  $batch = $batchInvoiceDb->fetchRow($select);
	  $this->view->batch = $batch->toArray();
	  
	  //detail
	  $batchInvoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceBatchDetail();
	  
	  $select_id = $batchInvoiceDetailDb->select()->from($batchInvoiceDetailDb,array('invoice_id'));
	  $select_id->setIntegrityCheck(false)
	  ->where('invoice_batch_id = ?',$batch_id);
	  
	  //invoice
	  $invoiceDb = new Studentfinance_Model_DbTable_InvoiceMain();
	  $select_inv = $invoiceDb->select()->from($invoiceDb)->setIntegrityCheck(false)
	              ->join(array('sp'=>'student_profile'), 'sp.appl_id = invoice_main.appl_id', array('name'=>"concat_ws(' ',appl_fname,appl_mname,appl_lname)"))
	              ->where('invoice_main.id in ('.$select_id.')');
	  
	  $invoices = $invoiceDb->fetchAll($select_inv);
	  
	  $this->view->invoice = $invoices->toArray();
	  
	  
	}
	
	private function getInvoiceData($idRegistration, $feeStructureId){
	  
	  //data from configuration screen
	  $ses_batch_invoice = new Zend_Session_Namespace('studentfinance_batch_invoice');
	  
      if(
        !isset($ses_batch_invoice->semester_id) ||
        !isset($ses_batch_invoice->program_id) ||
        !isset($ses_batch_invoice->intake) ||
        !isset($ses_batch_invoice->fee_item)
        ){
        
        throw new Exception("No Invoicing Configuration");
        exit;
      }
	  
	  //registration detail
	  $studentRegistrationDb = new App_Model_Record_DbTable_StudentRegistration();
	  $registrationData = $studentRegistrationDb->getStudentInfo($idRegistration);
	   
	  //student profile
	  $studentProfileDb = new Records_Model_DbTable_Studentprofile();
	  $profile = $studentProfileDb->fnViewStudentAppnDetails($registrationData['IdApplication']);
	   
	  //fee structure detail
	  $feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
	  $fee_item = $feeStructureItemDb->getStructureData($feeStructureId);
	   
	  //get registered course in particular semester
	  $subjectRegisterDb = new Registration_Model_DbTable_Studentregsubjects();
	  $registered_subject = $subjectRegisterDb->getRegisteredSubject($idRegistration,$ses_batch_invoice->semester_id);
	  
	  //semester info
	  $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
	  $semester = $semesterDb->getData($ses_batch_invoice->semester_id);
	  
	  
	  //filter only selected fee item
	  foreach ($fee_item as $index=>$fee){
	    if( !in_array($fee['fi_id'],$ses_batch_invoice->fee_item) ){
	      unset($fee_item[$index]);
	    }
	  }
 
	  //get current semester level
	  $db = Zend_Db_Table::getDefaultAdapter();
	  $sql = $db->select()
	  ->from(array('sss' => 'tbl_studentsemesterstatus'), array(new Zend_Db_Expr('max(Level) as Level')))
	  ->where('sss.IdStudentRegistration  = ?', $idRegistration);
	   
	  $result = $db->fetchRow($sql);
	   
	  if( $result['Level'] ){
	    $student_sem = $result['Level']+1;
	  }else{
	  
	    //check if senior student then hardcode level
	    $intake_year = substr($registrationData['IntakeId'], 0,4);
	    $cur_sem_year = substr($semester['SemesterMainCode'], 0,4);
	  
	    if($intake_year<2013){
	      $student_sem=0;
	      while($intake_year<=$cur_sem_year){
	        
	        //check current gasal or genap for currencty
	        if($intake_year == $cur_sem_year){
	          if($semester['semesterCountType']==1){
	            $student_sem += 1;
	          }else{
	            $student_sem += 2;
	          }
	        }else{
	          $student_sem += 2;
	        }
	  
	        $intake_year++;
	      }
	    }else{
	      $student_sem = 1;
	    }
	  }
	   
	  //get fee item frequency type
	  $sem_fee_item = array();
	  foreach ($fee_item as $fs){
	     
	    //1st sem
	    if($student_sem==1 && $fs['fi_frequency_mode']== 302 ){
	      $sem_fee_item[] = $fs;
	    }
	     
	    //every sem
	    if($fs['fi_frequency_mode']== 303 || $fs['fi_frequency_mode']== 453){
	      $sem_fee_item[] = $fs;
	    }
	    
	    //every senior semester
	    if($student_sem>1 && $fs['fi_frequency_mode']== 304){
	      $sem_fee_item[] = $fs;
	    }
	     
	    //defined semester
	    if($fs['fi_frequency_mode']== 305){
	  
	      foreach ($fs['semester'] as $sem_defined){
	        if($sem_defined['fsis_semester'] == $student_sem+1){
	          $sem_fee_item[] = $fs;
	        }
	      }
	    }
	    
	    //every gasal regular
	    if($fs['fi_frequency_mode']== 460 && $semester['SemesterCountType']==1 && $semester['SemesterFunctionType']==0){
	      $sem_fee_item[] = $fs;
	    }
	    
	  }
	   
	  //get fee item calculation type
	  $invoice['amount']=0.00;
	  
	  foreach ($sem_fee_item as $item){
	     
	    //nilai tetap
	    if($item['fi_amount_calculation_type']==300){
	      $invoice['amount'] +=  $item['fsi_amount'];
	      $item['total_amount'] = $item['fsi_amount'];
	      $invoice['fee_item'][] = $item;
	    }else
	       
        //pendaraban SKS
        if($item['fi_amount_calculation_type']==299){
          $total_sks = 0;
          for($i=0; $i<sizeof($registered_subject); $i++){
            $total_sks +=$registered_subject[$i]['CreditHours'];
          }
        	
          if($total_sks!=0){
            $invoice['amount'] +=  $item['fsi_amount']*$total_sks;
            $item['total_amount'] = $item['fsi_amount']*$total_sks;
            $item['total_sks'] = $total_sks;
            $invoice['fee_item'][] = $item;
          }
	  
	    }else
	       
        //pendaraban jumlah subject
        if($item['fi_amount_calculation_type']==301){
          $total_subject = sizeof($registered_subject);
           
          if($total_subject!=0){
            $invoice['amount'] +=  $item['fsi_amount']*$total_subject;
            $item['total_amount'] = $item['fsi_amount']*$total_subject;
            $item['total_subject'] = $total_subject;
            $invoice['fee_item'][] = $item;
          }
        }else
        
        //registered subject
        if($item['fi_amount_calculation_type']==459){
          
          $item['total_amount'] = 0;
          
          for($i=0; $i<sizeof($registered_subject); $i++){
            
            if(isset($item['subject'])){
              foreach ($item['subject'] as $subject){
                
                if($subject['fsisub_subject_id'] = $registered_subject[$i]['IdSubject']){
                  $invoice['amount'] +=  $item['fsi_amount'];
                  $item['total_amount'] = $item['fsi_amount'];  
                }
                
              }
            }
            
            if($item['total_amount']>0){
              $invoice['fee_item'][] = $item;
            }
            
          }
           
        }
        
	  }
	  
	  return $invoice;
	}
	
	private function saveBatchInvoice(){
	  
	  //data from configuration screen
	  $ses_batch_invoice = new Zend_Session_Namespace('studentfinance_batch_invoice');
	   
	  if(
	      !isset($ses_batch_invoice->semester_id) ||
	      !isset($ses_batch_invoice->program_id) ||
	      !isset($ses_batch_invoice->intake) ||
	      !isset($ses_batch_invoice->fee_item) ||
	      !isset($ses_batch_invoice->student_list)
	  ){
	  
	    throw new Exception("No Invoicing Configuration");
	    exit;
	  }
	  
	  //semester
	  $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
	  $semester = $semesterDb->fnGetSemestermaster($ses_batch_invoice->semester_id);
	  
	  //academic year
	  $academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	  $academicYear = $academicYearDb->getData($semester['idacadyear']);
	  
	  //program
	  $programDb = new GeneralSetup_Model_DbTable_Program();
	  $program = $programDb->fngetProgramData($ses_batch_invoice->program_id);
	  
	  /*echo "<pre>";
	  print_r($ses_batch_invoice->student_list);
	  echo "</pre>";
	  exit;*/
	  
	  $db = Zend_Db_Table::getDefaultAdapter();
	  $db->beginTransaction();
	  
	  try{
    	  //insert invoice batch
    	  $inv_batch_data = array(
    	      'create_date' => date('Y-m-d H:i:s'),
    	      'semester_id' => $ses_batch_invoice->semester_id,
    	      'faculty_id' => $program['IdCollege'],
    	      'program_id' => $ses_batch_invoice->program_id,
    	      'intake_id' => implode(",",$ses_batch_invoice->intake),
    	      'total_invoice' => sizeof($ses_batch_invoice->student_list)
    	  );
    	  
    	  $invoiceBatchDb = new Studentfinance_Model_DbTable_InvoiceBatch();
    	  $batch_id = $invoiceBatchDb->insert($inv_batch_data);
    	  
    	  
    	  $invoiceDb = new Studentfinance_Model_DbTable_InvoiceMain();
    	  $invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
    	  $invoiceBatchDetailDb = new Studentfinance_Model_DbTable_InvoiceBatchDetail();
    	  $invoiceSpcDb = new Studentfinance_Model_DbTable_InvoiceSpc();
    	  
    	  foreach ($ses_batch_invoice->student_list as $student):
    	  
        	  /*
        	   * insert invoice_main
        	   */
        	  
        	  //get invoice no from sequence
    	      $seq_data = array(
    	          date('y',strtotime($academicYear['ay_start_date'])), 
    	          substr($student['intake']['IntakeId'],2,2), 
    	          $student['program']['ProgramCode'], 0
    	      );
    	      
        	  $db = Zend_Db_Table::getDefaultAdapter();
        	  $stmt = $db->prepare("SELECT invoice_seq(?,?,?,?) AS invoice_no");
        	  $stmt->execute($seq_data);
        	  $invoice_no = $stmt->fetch();
        	  
        	  $inv_data = array(
              	'bill_number' => $invoice_no['invoice_no'],
              	'appl_id' => $student['registration']['IdApplication'],
        	    'IdStudentRegistration' => $student['student_id'],
              	'academic_year' => $student['semester']['idacadyear'],
              	'semester' => $student['semester']['IdSemesterMaster'],
              	'bill_amount' => $student['invoice']['amount'],
              	'bill_paid' => 0.00,
              	'bill_balance' => $student['invoice']['amount'],
              	'bill_description' => 'Biaya Pendidikan Semester: '.$student['semester']['SemesterMainDefaultLanguage'],
              	'college_id' => $student['program']['IdCollege'],
              	'program_code' => $student['program']['ProgramCode'],
              	'creator' => '1',
              	'fs_id' => $student['fs_id'],
              	'status' => 'A',
              	'date_create' => date('Y-m-d H:i:s')
              );
        	  
        	  
        	  $invoice_id = $invoiceDb->insert($inv_data);
        	  
        	  //insert invoice detail
        	  foreach ($student['invoice']['fee_item'] as $fee_item){
            	  $inv_detail_data = array(
            	      'invoice_main_id' => $invoice_id,
            	      'fi_id' => $fee_item['fi_id'],
            	      'fee_item_description' => $fee_item['fi_name_bahasa'],
            	      'amount' => $fee_item['total_amount']
            	  );
            	  
            	  $invoiceDetailDb->insert($inv_detail_data);
        	  }
        	  
        	  //insert invoice batch detail
        	  $inv_batch_detaildata = array(
        	      'invoice_batch_id' => $batch_id,
        	      'invoice_id' => $invoice_id
        	  );
        	  
        	  $invoiceBatchDetailDb->insert($inv_batch_detaildata);
        	  
        	  
        	  $name = "";
        	  if($student['profile']['appl_fname']!=null && $student['profile']['appl_fname']!=""){
        	    $name .= $student['profile']['appl_fname']." ";
        	  }
        	  if($student['profile']['appl_mname']!=null && $student['profile']['appl_mname']!=""){
        	    $name .= $student['profile']['appl_mname']." ";
        	  }
        	  if($student['profile']['appl_lname']!=null && $student['profile']['appl_lname']!=""){
        	    $name .= $student['profile']['appl_lname']." ";
        	  }
        	  
        	  //insert invoice spc
        	  $invSpc_data = array(
        	      'invoice_id' => $invoice_id,
        	      'billing_no' => $invoice_no['invoice_no'],
        	      'payee_id' => $student['registration']['registrationId'],
        	      'appl_id' => $student['registration']['IdApplication'],
        	      'IdStudentRegistration' => $student['student_id'],
        	      'name' => $name,
        	      'address' => "Universitas Trisakti",
        	      'ref1' => $student['faculty']['CollegeCode']."-".$student['faculty']['ShortName'],
        	      'ref2' => '',
        	      'ref3' => $student['program']['ProgramCode']."-".$student['program']['ShortName'],
        	      'ref4' => substr($student['academic_year']['ay_code'], 0,4),
        	      'ref5' => $inv_data['bill_description'],
        	      'register_no' => $this->generateAlphanumeric(9),
        	      'due_date' => '',
        	      'create_date' =>date('Y-m-d H:i:s'),
        	      'amount_total' => $student['invoice']['amount']
        	  );
        	  
        	  foreach ($student['invoice']['fee_item'] as $key=>$item){
        	    	
        	    if($item['fi_id'] == 1 || $item['fi_id'] == 11 ){//SP & Registration
        	      $invSpc_data['amount1'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 2){//BPP-POKOK
        	      $invSpc_data['amount2'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 3){//BPP-SKS
        	      $invSpc_data['amount3'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 4){//PRAKTIKUM
        	      $invSpc_data['amount4'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 5){//LAIN-LAIN
        	      $invSpc_data['amount5'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 6){//DKMK
        	      $invSpc_data['amount6'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 7){//DKM
        	      $invSpc_data['amount7'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 8){//KKN
        	      $invSpc_data['amount8'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 9){//TUGAS AKHIR
        	      $invSpc_data['amount9'] = $item['total_amount'];
        	    }else
        	      if($item['fi_id'] == 10){//ICT
        	      $invSpc_data['amount10'] = $item['total_amount'];
        	    }
        	    
        	  }
        	  
        	  $invoiceSpcDb->insert($invSpc_data);
        	  
        	
    	  endforeach;
    	  
    	  $db->commit();
    	  
    	  return true;
	  
	  }catch (Exception $e){
        $db->rollBack();
        echo $e->getMessage();
        echo "<pre>";
        var_dump($e->getTrace());
        echo "</pre>";
        echo "<br />";
        
        throw $e;
        
        return false;
      } 
	  
	}
	
	public function getSpcAction(){
	  
	  $batch_id = $this->_getParam('batch', null);
	  
	  $spcInvoiceDb = new Studentfinance_Model_DbTable_InvoiceSpc();
	  $data_arr = $spcInvoiceDb->getSpcByInvoiceBatch($batch_id);
		
	  /*$output ='';
	   $output .="BILLING_NO,PAYEE_ID,BILL_FIRST_NAME,ADDRESS_1,BILL_REF_1,BILL_REF_2,BILL_REF_3,BILL_REF_4,BILL_REF_5,AMOUNT_TOTAL,AMOUNT_1,AMOUNT_2,AMOUNT_3,AMOUNT_4,AMOUNT_5,AMOUNT_6,AMOUNT_7,AMOUNT_8,AMOUNT_9,AMOUNT_10,AUTODEBET_ACC_D,REGISTER_NO,DUE_DATE"."\n";
	  	
	  if($data_arr){
	  foreach ($data_arr as $data){
	  $output .=$data['billing_no'].",".$data['payee_id'].",".$data['name'].",".$data['address'].",".$data['ref1'].",".$data['ref2'].",".$data['ref3'].",".$data['ref4'].",".$data['ref5'].",".$data['amount_total'].",".$data['amount1'].",".$data['amount2'].",".$data['amount3'].",".$data['amount4'].",".$data['amount5'].",".$data['amount6'].",".$data['amount7'].",".$data['amount8'].",".$data['amount9'].",".$data['amount10'].",,".$data['register_no'].",,"."\n";
	  }
	  }*/
	  	
	  $filename = "SPCUTS_".date("Ymd",strtotime($data_arr[0]['create_date']));
	
	  $file = APPLICATION_PATH . "/tmp/" . $filename . ".csv";
	
	  $realPath = realpath( $file );
	
	  if ( false === $realPath )
	  {
	    touch( $file );
	    chmod( $file, 0777 );
	  }
	
	  $file = realpath( $file );
	  $handle = fopen( $file, "w" );
	   
	  $finalData[] = array('BILLING_NO','PAYEE_ID','BILL_FIRST_NAME','ADDRESS_1','BILL_REF_1','BILL_REF_2','BILL_REF_3','BILL_REF_4','BILL_REF_5','AMOUNT_TOTAL','AMOUNT_1','AMOUNT_2','AMOUNT_3','AMOUNT_4','AMOUNT_5','AMOUNT_6','AMOUNT_7','AMOUNT_8','AMOUNT_9','AMOUNT_10','AUTODEBET_ACC_D','REGISTER_NO','DUE_DATE');
	   
	  $total_amount = 0;
	  if($data_arr){
	    foreach ( $data_arr AS $key=>$row )
	    {
	      $l = strlen($row["billing_no"]);
	       
	      $finalData[] = array(
	           
	          utf8_decode( chr(0).$row["billing_no"]) , // For chars with accents.
	          utf8_decode( $row["payee_id"] ),
	          utf8_decode( $row["name"] ),
	          utf8_decode( $row["address"] ),
	          utf8_decode( $row["ref1"] ),
	          utf8_decode( $row["ref2"] ),
	          utf8_decode( $row["ref3"] ),
	          utf8_decode( $row["ref4"] ),
	          utf8_decode( $row["ref5"] ),
	          utf8_decode( $row["amount_total"] ),
	          utf8_decode( $row["amount1"] ),
	          utf8_decode( $row["amount2"] ),
	          utf8_decode( $row["amount3"] ),
	          utf8_decode( $row["amount4"] ),
	          utf8_decode( $row["amount5"] ),
	          utf8_decode( $row["amount6"] ),
	          utf8_decode( $row["amount7"] ),
	          utf8_decode( $row["amount8"] ),
	          utf8_decode( $row["amount9"] ),
	          utf8_decode( $row["amount10"] ),
	          utf8_decode( ''),
	          utf8_decode( $row["register_no"] ),
	          utf8_decode( '' )
	      );
	
	      $total_amount = $total_amount + $row["amount_total"];
	      
	    }
	  }
	
	  if (PHP_EOL == "\r\n"){
	    $eol = "\n";
	  }else{
	    $eol = "\r\n";
	  }
	
	  foreach ( $finalData AS $finalRow )
	  {
	     
	    fwrite( $handle, implode(",", $finalRow).$eol);
	     
	  }
	
	  fclose( $handle );
	
	  $this->_helper->layout->disableLayout();
	  $this->_helper->viewRenderer->setNoRender();
	
	  $this->getResponse()->setRawHeader( "Content-Type: text/csv" )
	  ->setRawHeader( "Content-Disposition: attachment; filename=".$filename."_".sizeof($data_arr)."_".$total_amount.".csv" )
	  ->setRawHeader( "Content-Transfer-Encoding: binary" )
	  ->setRawHeader( "Expires: 0" )
	  ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
	  ->setRawHeader( "Pragma: public" )
	  ->setRawHeader( "Content-Length: " . filesize( $file ) )
	  ->sendResponse();
	
	  readfile( $file );
	  unlink($file);;
	}
	
	/**
	 *
	 * Generate Alphanumeric
	 * @param int $length
	 * @param unknown_type $alphanumeric
	 */
	private function generateAlphanumeric($length, $alphanumeric=true){
	
	  if($alphanumeric){
	    $character_array = array_merge(range('A', 'Z'), range(0, 9));
	  }else{
	    $character_array = array_merge(range(0, 9));
	  }
	
	
	  $string = "";
	  for($i = 0; $i < $length; $i++) {
	    $string .= $character_array[rand(0, (count($character_array) - 1))];
	  }
	   
	  return $string;
	}
}
?>