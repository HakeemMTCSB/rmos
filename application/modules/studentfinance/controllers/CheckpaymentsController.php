<?php
class Studentfinance_CheckpaymentsController extends Base_Base { //Controller for the User Module

	private $lobjCheckpaymentsForm;
	private $lobjCheckpayments;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();	
		
	}
	public function fnsetObj(){
		
		$this->lobjCheckpaymentsForm = new Studentfinance_Form_Checkpayments();
		$this->lobjCheckpayments = new Studentfinance_Model_DbTable_Checkpayments();
		
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		
		
		$lobjStudentNameList = $this->lobjCheckpayments->fnGetStudentNameList();
		$lobjform->field5->addMultiOptions($lobjStudentNameList);
		
		
		
		 if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->checkpaymentpaginatorresult);
		
		$larrresult = $this->lobjCheckpayments->fngetStudentApplicationDetails(); //get user details
		/*$lobjUniversityMasterList = $this->lobjCheckpayments->fnGetUniversityMasterList();
		$lobjform->field1->addMultiOptions($lobjUniversityMasterList);*/
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->checkpaymentpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->checkpaymentpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCheckpayments ->fnSearchStudentApplicationDetails( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->checkpaymentpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/checkpayments/index');
		}


	}

	public function newcheckpaymentsAction() { //Action for creating the new user
		
		$this->view->lobjCheckpaymentsForm = $this->lobjCheckpaymentsForm; 
		$IdApplication= ( int ) $this->_getParam ( 'id' );
		//echo "<pre>";print_r($IdApplication);die();
		$this->view->lobjCheckpaymentsForm->IdApplication->setValue ( $IdApplication );
		$this->view->lobjCheckpaymentsForm->IdApplication->setAttrib ('readonly','true');

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjCheckpaymentsForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjCheckpaymentsForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		$lobjStudentNameList = $this->lobjCheckpayments->fnGetStudentNameList();
		$this->lobjCheckpaymentsForm->IdApplication->addMultiOptions($lobjStudentNameList);

		$larrresult = $this->lobjCheckpayments->fnviewVerifyPaymentDetails($IdApplication);
		//echo "<pre>";print_r($larrresult);die();
		$this->view->larrresult=$larrresult;

		$lobjSemesterNameList = $this->lobjCheckpayments->fnGetSemesterNameList();
		$this->lobjCheckpaymentsForm->IdSemester->addMultiOptions($lobjSemesterNameList);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);die();
			
			unset ( $larrformData ['Save'] );
			if ($this->lobjCheckpaymentsForm->isValid ( $larrformData )) {
				$result = $this->lobjCheckpayments->fnSearchSem($larrformData['IdApplication'],$larrformData['IdSemester']);
				if($result){
					
					echo "<script>alert('Duplicate Semester not allowed');</script>";
					
				
					
					$this->view->alertError = 'Login failed. Either username or password is incorrect';
				}else{
				
					$this->lobjCheckpayments->fnaddVerfyPayments($larrformData);
				}
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Check Payments Add Id=' . $larrformData['IdApplication'],
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				$this->_redirect( $this->baseUrl . '/studentfinance/checkpayments/newcheckpayments/id/'.$larrformData['IdApplication']);
				
			}
		}

	}

	

	
}