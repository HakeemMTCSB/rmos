<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_FeeCategoryController extends Base_Base {
	
	public function indexAction(){
	
		$this->view->title = "Fee Category";
	
		$feeCategoryDb = new Studentfinance_Model_DbTable_FeeCategory();
	
		$this->view->fee_category_list = $feeCategoryDb->getData();
	
	}
	
	public function addFeeCategoryAction(){
	
		$this->view->title = "Fee Category - Add Fee Category";
	
		$form = new Studentfinance_Form_FeeCategory();
	
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
	
		if ($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
	
				$data = array(
						'fc_code' => $formData['fc_code'],
						'fc_desc' => $formData['fc_desc'],
						'fc_group' => $formData['fc_group'],
						'fc_desc_second_language' => $formData['fc_desc_second_language'],
						'fc_seq' => $formData['fc_seq'],
						'UpdDate' => date('Y-m-d H:i:s'),
						'UpdUser' => $getUserIdentity->id
				);
	
				$feeCategoryDb = new Studentfinance_Model_DbTable_FeeCategory();
	
				$feeCategoryDb->insert($data);
	
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new fee category');
	
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-category', 'action'=>'index'),'default',true));
	
			}else{
				$form->populate($formData);
			}
		}
	
		$this->view->form = $form;
	}
	
	public function editFeeCategoryAction(){
	
		$id = $this->_getParam('id',null);
	
		if($id==null){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-category', 'action'=>'index'),'default',true));
		}
	
		$this->view->title = "Fee Category - Edit Fee Category";
	
		$form = new Studentfinance_Form_FeeCategory();
	
		$feeCategoryDb = new Studentfinance_Model_DbTable_FeeCategory();
	
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
			
		if ($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
	
				$data = array(
						'fc_code' => $formData['fc_code'],
						'fc_desc' => $formData['fc_desc'],
						'fc_group' => $formData['fc_group'],
						'fc_desc_second_language' => $formData['fc_desc_second_language'],
						'UpdDate' => date('Y-m-d H:i:s'),
						'UpdUser' => $getUserIdentity->id
					,
				);
	
	
	
				$feeCategoryDb->update($data,array('fc_id=?'=>$id));
	
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update fee category');
	
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-category', 'action'=>'index'),'default',true));
	
			}else{
				$form->populate($formData);
			}
		}else{
			$data = $feeCategoryDb->fetchRow(array('fc_id=?'=>$id))->toArray();
			$form->populate($data);
		}
	
		$this->view->form = $form;
	}
}
?>