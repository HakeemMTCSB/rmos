<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/8/2016
 * Time: 11:13 AM
 */
class Studentfinance_AdvPymntReportController extends Zend_Controller_Action {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_AdvPymntReport();

        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function havingBalanceAction(){
        $this->view->title = $this->view->translate('Untilized Advance Payment');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Studentfinance_Form_AdvPymntHaveBalance(array('programId'=>$formData['program']));
            $form->populate($formData);
            $this->view->form = $form;

            $list = $this->model->getAdvPymntHavingBalance($formData);
            $this->view->list = $list;
        }else{
            $form = new Studentfinance_Form_AdvPymntHaveBalance();
            $this->view->form = $form;
        }
    }

    public function havingBalanceExportAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Studentfinance_Form_AdvPymntHaveBalance(array('programId'=>$formData['program']));
            $form->populate($formData);
            $this->view->form = $form;

            $list = $this->model->getAdvPymntHavingBalance($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'unutilized_advance_payment.xls';
    }

    public function unutilizedAction(){
        $this->view->title = $this->view->translate('Advance Payment');
        $pamodel = new Studentfinance_Model_DbTable_PaymentAmount();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Studentfinance_Form_AdvPymntHaveBalance(array('programId'=>$formData['program']));
            $form->populate($formData);
            $this->view->form = $form;

            $list = $this->model->getInvoice($formData);

            if ($list){
                foreach ($list as $key => $loop){
                    $balance = $pamodel->getBalanceMain($loop['mainId']);

                    if (str_replace(',', '', $balance['bill_balance'] < 0.00)){
                        $list[$key]['balance']=str_replace(',', '', $balance['bill_balance']);
                    }else{
                        unset($list[$key]);
                    }
                }
            }

            $this->view->list = $list;
        }else{
            $form = new Studentfinance_Form_AdvPymntHaveBalance();
            $this->view->form = $form;
        }
    }

    public function unutilizedExportAction(){
        $this->_helper->layout->disableLayout();
        $pamodel = new Studentfinance_Model_DbTable_PaymentAmount();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Studentfinance_Form_AdvPymntHaveBalance(array('programId'=>$formData['program']));
            $form->populate($formData);
            $this->view->form = $form;

            $list = $this->model->getInvoice($formData);

            if ($list){
                foreach ($list as $key => $loop){
                    $balance = $pamodel->getBalanceMain($loop['mainId']);

                    if (str_replace(',', '', $balance['bill_balance'] < 0.00)){
                        $list[$key]['balance']=str_replace(',', '', $balance['bill_balance']);
                    }else{
                        unset($list[$key]);
                    }
                }
            }

            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'advance_payment.xls';
    }

    public function getProgramSchemeAction(){
        $id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $programSchemeList = array();

        if ($id != null){
            $programSchemeList = $this->model->getProgramScheme($id);
        }

        $json = Zend_Json::encode($programSchemeList);
        echo $json;
        exit;
    }

    public function getSemesterAction(){
        $id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $semesterList = array();

        if ($id != null){
            $programInfo = $this->model->getProgramById($id);

            if (isset($programInfo['IdScheme'])){
                $semesterList = $this->model->getSemester($programInfo['IdScheme']);
            }
        }

        $json = Zend_Json::encode($semesterList);
        echo $json;
        exit;
    }

    public function advancePaymentGainLossAction(){
        $this->_helper->layout->disableLayout();

        $model = new Studentfinance_Model_DbTable_AdvancePaymentGainLoss();

        $list = $model->getAdvancePaymentList();

        if ($list){
            foreach ($list as $key => $loop){
                $exModel = new Studentfinance_Model_DbTable_CurrencyRate();

                if ($loop['dateinv1']=='' || $loop['dateinv1']==null) {
                    $exinvdate = $loop['datercp'];
                }else{
                    $exinvdate = $loop['dateinv1'];
                }

                $exInv = $exModel->getRateByDate(2, $exinvdate);
                $checkAdvAmount = $loop['advpydet_total_paid'] * $exInv['cr_exchange_rate'];

                $exInv2 = $exModel->getRateByDate(2, $loop['dateinv2']);
                $checkAdvAmount2 = $loop['advpydet_total_paid'] * $exInv2['cr_exchange_rate'];

                $checkAdvAmountGainLoss = $checkAdvAmount2 - $checkAdvAmount;

                $list[$key]['amountgainloss'] = $checkAdvAmountGainLoss;
                $list[$key]['exrate1'] = $exInv['cr_exchange_rate'];
                $list[$key]['exrate2'] = $exInv2['cr_exchange_rate'];
            }
        }

        $this->view->list = $list;

        $this->view->filename = date('Ymd').'_advance_payment_gsin_loss_report.xls';
    }
}