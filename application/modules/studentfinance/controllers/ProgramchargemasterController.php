<?php
class Studentfinance_ProgramchargemasterController extends Base_Base {
	private $lobjProgramChargemasterForm;
	private $lobjprogram;
	private $lobjChargemaster;
	private $lobjProgramChargemaster;
	
	public function init() { //initialization function
		$this->fnsetObj();
		 
	}
	public function fnsetObj(){
		
		$this->lobjProgramChargemasterForm = new Studentfinance_Form_Programchargemaster();
		$this->lobjProgramChargemaster = new Studentfinance_Model_DbTable_Programchargemaster();
		$this->lobjChargemaster = new Studentfinance_Model_DbTable_Chargemaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); 
		$this->view->lobjform = $lobjform; 
		$larrresult = $this->lobjprogram->fetchAll('Active = 1','ProgramName ASC');
		
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->programchargepaginatorresult);	
   	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->programchargepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->programchargepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjProgramChargemaster ->fnSearchPrograms( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programchargepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/programchargemaster/index');
		}
	}

	public function programchargemasterlistAction() { //Action for the updation and view of the  details
		$this->view->lobjProgramChargemasterForm = $this->lobjProgramChargemasterForm;
		$chargelist = $this->lobjChargemaster->fngetChargeslist();
		$this->view->lobjProgramChargemasterForm->IdCharges->addMultioptions($chargelist);
		$ldtsystemDate = date ( 'Y-m-d' );
		$this->view->lobjProgramChargemasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjProgramChargemasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$lintProgram = ( int ) $this->_getParam ( 'id' );
		$this->view->lintProgram = $lintProgram;
		$this->view->lobjProgramChargemasterForm->IdProgram->setValue ( $lintProgram );
		$this->view->programlist = $programlist = $this->lobjProgramChargemaster->fnprogramchargelist($lintProgram);
		if($this->_getParam('update')!= 'true') {
			$this->lobjProgramChargemaster->fninsertprogramcharge($programlist);
		}
		$this->view->tempprogramlist = $this->lobjProgramChargemaster->fnTempprogramchargelist($lintProgram);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
					$this->lobjProgramChargemaster->fnaddprogramcharges( $larrformData );
					$this->_redirect( $this->baseUrl . '/studentfinance/programchargemaster/index');
			}
		}
	}

	public function getchargesAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdCharges = $this->_getParam('IdCharges');
		$chargelist = $this->lobjChargemaster->fnGetCharges($IdCharges);
		echo Zend_Json_Encoder::encode($chargelist);
	}
	
	public function inserteducationdetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$InstitutionName = $this->_getParam('InstitutionName');
		$UniversityName = $this->_getParam('UniversityName');
		$MajorFiledOfStudy = $this->_getParam('MajorFiledOfStudy');
		$IdApplication = $this->_getParam('IdApplication');
		$StudyPlace = $this->_getParam('StudyPlace');
		$YearOfStudyFrom = $this->_getParam('YearOfStudyFrom');
		$YearOfStudyTo = $this->_getParam('YearOfStudyTo');
		$DegreeType = $this->_getParam('DegreeType');
		$GradeOrCGPA = $this->_getParam('GradeOrCGPA');
		$IdStudEduDtl = $this->_getParam('IdStudEduDtl');		
	    $upddate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $upduser = $auth->getIdentity()->iduser;
       	if($IdStudEduDtl) {
			$this->lobjstudenteducation->fnUpdateTempEditEducationdetails($InstitutionName,$UniversityName,$MajorFiledOfStudy,$StudyPlace,$YearOfStudyFrom,$YearOfStudyTo,$DegreeType,$GradeOrCGPA,$IdStudEduDtl,$upddate,$upduser);
	    } else {
			$this->lobjstudenteducation->fnInsertNewTempEducationDetails($InstitutionName,$UniversityName,$MajorFiledOfStudy,$StudyPlace,$YearOfStudyFrom,$YearOfStudyTo,$DegreeType,$GradeOrCGPA,$upddate,$upduser,$IdApplication);
	    }
	    echo "1";
	}
	
	public function deleteprogramchargedetailsAction(){
		 $this->_helper->layout->disableLayout();
		 $this->_helper->viewRenderer->setNoRender();
		 $IdTemp = $this->_getParam('IdTemp');
		 $this->lobjProgramChargemaster->fnUpdateTempprogramcharges($IdTemp);
			
		 echo "1";

		
	}
}