<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_RefundChequeController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_RefundCheque();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
		
		//title
    	$this->view->title= $this->view->translate("Refund Cheque List");
    	
    	$msg = $this->_getParam('msg', null);
    	if( $msg!=null ){
    		$this->view->noticeMessage = $msg;
    	} 
    	    	
    	if ($this->getRequest()->isPost()) {
    		
    	}
    	
    	//paginator
		$data = $this->_DbObj->getPaginateData();
		
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
    	
    	  	
	}
	
	public function detailAction(){
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Refund Cheque - Detail");
    	
    	//cheque data
    	$data = $this->_DbObj->getData($id);
    	$this->view->data = $data;
		
	}
	
	public function entryAction(){
		//title
    	$this->view->title= $this->view->translate("Refund Cheque - Entry");
    	
    	$form = new Studentfinance_Form_RefundCheque();
    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$data = array(
							'rchq_cheque_no' => $formData['rchq_cheque_no'],
							'rchq_bank_name' => $formData['rchq_bank_name'],
							'rchq_amount' => $formData['rchq_amount'],
							'rchq_issue_date' => date('Y-m-d', strtotime($formData['rchq_issue_date']))
						);
				
				$this->_DbObj->insert($data);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'refund-cheque', 'action'=>'index','msg'=>$this->view->translate('Berjaya disimpan')),'default',true));
				
			}else{
				$form->populate($formData);
			}
    		
    	}
    	
    	$this->view->form = $form;
    	
	}
	
	public function editAction(){
		
		$this->_helper->layout()->disableLayout();
		
		//title
    	$this->view->title= $this->view->translate("Refund Cheque - Edit");
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		$form = new Studentfinance_Form_RefundCheque();
		$form->setAttrib('action','/studentfinance/refund-cheque/edit/id/'.$id);
		$form->removeElement('cancel');
		
		$element = $form->getElement('rchq_issue_date');
		$element->setAttrib('class', 'hasDatepicker');
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
								
				$data = array(
							'rchq_cheque_no' => $formData['rchq_cheque_no'],
							'rchq_bank_name' => $formData['rchq_bank_name'],
							'rchq_amount' => $formData['rchq_amount'],
							'rchq_issue_date' => date('Y-m-d', strtotime($formData['rchq_issue_date']))
						);
				
				$this->_DbObj->update($data, 'rchq_id = '.$id);
				
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'refund-cheque', 'action'=>'detail','id'=>$id),'default',true));
			}else{
				$form->populate($formData);
			}
			
		}else{
			
			//cheque data
	    	$data = $this->_DbObj->getData($id);
	    	
	    	$data['rchq_issue_date'] = date('d-m-Y', strtotime($data['rchq_issue_date'])); 
	    	
	    	$form->populate($data);
		}
		
    	$this->view->form = $form;
    	
    	
	}
	
	public function deleteAction(){
		
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		
		$auth = Zend_Auth::getInstance();
		
		$data = array(
					'rchq_status' => "X",
					'rchq_cancel_by' => $auth->getIdentity()->iduser,
					'rchq_cancel_date' => date('Y-m-d H:i:s')
				);
		
		$this->_DbObj->update($data, 'rchq_id = '.$id);
		
		
		//redirect
		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'refund-cheque', 'action'=>'index'),'default',true));
		
		exit;
	}
	
}

