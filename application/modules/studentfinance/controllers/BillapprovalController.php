<?php
class Studentfinance_BillapprovalController extends Base_Base { //Controller for the User Module

	private $lobjvoucherForm;
	private $lobjvoucherModel;
	private $_gobjlog;
	
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
		 
	}
	
	public function fnsetObj()
	{
		$this->lobjvoucherModel = new Studentfinance_Model_DbTable_Billapprovalmodel();
		$this->lobjvoucherForm = new Studentfinance_Form_Billapproval();	
		$this->lobjform = new App_Form_Search ();
	}
	
   public function indexAction() {
   
		$this->view->lobjSearchForm = $this->lobjform; 			
		
		$this->view->lobjvoucherForm = $this->lobjvoucherForm;			
	
		$larrVoucherTypeList = $this->lobjvoucherModel->fnGetVoucherTypeList();		
		$this->view->lobjSearchForm->field1->addMultiOptions($larrVoucherTypeList);
		$this->view->lobjSearchForm->field7->setValue(0);
	   
		$larrVoucherList = $this->lobjvoucherModel->fnGetVoucherMasterDetails();	
			
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjvoucherForm->Upddate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjvoucherForm->Upduser->setValue( $auth->getIdentity()->iduser);	
		
		$lintpagecount = $this->gintPageCount;  // Records per page		
		$lobjPaginator = new App_Model_Definitiontype(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsams->paginator)) {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($this->gobjsessionsams->paginator,$lintpage,$lintpagecount);
		} else {
			$this->view->lobjPaginator = $lobjPaginator->fnPagination($larrVoucherList,$lintpage,$lintpagecount);
		}		
		//Disciplinary Action Search
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->view->lobjSearchForm->isValid ( $larrformData )) {	
				/*echo "<pre>";
				print_r($larrformData);
				exit;*/
				$larrSearchResult = $this->lobjvoucherModel->fnSearchVoucherMasterDetails( $larrformData );			
				/*echo "<pre>";
				print_r($larrSearchResult);
				exit;*/
				
				$this->view->lobjPaginator = $lobjPaginator->fnPagination($larrSearchResult,$lintpage,$lintpagecount);
		    	$this->gobjsessionsams->paginator = $larrSearchResult;	    		
			}
		}		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {			
			$larrformData = $this->_request->getPost (); 		
					
						$count=count($larrformData['Idvoucher']);
						
				for($bill=0;$bill<$count;$bill++){
					//echo $larrformData['Idvoucher'][$bill];
					//echo $larrformData['billapproval'][$larrformData['Idvoucher'][$bill]];exit;
					if(!$larrformData['billapproval'][$larrformData['Idvoucher'][$bill]])$larrformData['billapproval'][$larrformData['Idvoucher'][$bill]]=0;
					$this->lobjvoucherModel->fnupdateVoucherBillaction($larrformData['Idvoucher'][$bill],$larrformData['billapproval'][$larrformData['Idvoucher'][$bill]]);	
				}	

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Bill Approval Add ',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
					$this->_redirect( $this->baseUrl . '/studentfinance/billapproval/index');
			}
		//Disciplinary Action Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/billapproval/index');			
		}
	
	}
	
	public function newbillapprovalAction() 
	{ //Action for creating the new user
		
		$this->view->lobjvoucherForm = $this->$lobjvoucherForm;
	}
	
    

	public function editbillapprovalAction() 
	{ //Action for creating the new user
		
		$this->view->lobjvoucherForm = $this->$lobjvoucherForm;
	}	 
}