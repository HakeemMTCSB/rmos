<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_FeeDiscountController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_FeeStructurePlan();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Rank Discount Set-up");
    	
    	$this->view->searchForm = new Studentfinance_Form_FeeStructure();
    	
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//paginator
			$data = $feeStructureDb->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->paginator = $paginator;
		
    	}
    	    	
	}
	
	public function localAction(){
		
		$this->_helper->layout->disableLayout();
		
		$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
		
		//paginator
		$data = $feeStructureDb->getPaginateDataByCategory(314);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function foreignAction(){
		
		$this->_helper->layout->disableLayout();
		
		$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
		
		//paginator
		$data = $feeStructureDb->getPaginateDataByCategory(315);
		
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function detailAction(){
    	$id = $this->_getParam('id', 0);
    	$this->view->fee_structure_id = $id;
    	
    	//title
    	$this->view->title= $this->view->translate("Rank Discount Set-up")." - ".$this->view->translate("Detail");
    	
    	//fee-structure data
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	$this->view->fee_structure = $feeStructureDb->getData($id);
    	
    	//fee structure item
    	$feeStructureItemDb = new Studentfinance_Model_DbTable_FeeStructureItem();
    	$this->view->fee_structure_item = $feeStructureItemDb->getStructureData($id);
    	
    	//rank
    	$this->view->rankList = array(
    								array('name'=>'Peringkat 1','id' => 1),
							    	array('name'=>'Peringkat 2','id' => 2),
							    	array('name'=>'Peringkat 3','id' => 3),
    							);
    	
    }
	
	public function addAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Fee Structure Set-up")." - ".$this->view->translate("Add");
    	    	
    	$form = new Studentfinance_Form_FeeStructure();
    	
    	$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true)."'; return false;";
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$code = $this->_DbObj->addData($formData);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-structure', 'action'=>'index'),'default',true));	
			}else{
				$form->populate($formData);
			}
        	
        }
    	
        $this->view->form = $form;
    }
    
	public function editAction(){
		$code = $this->_getParam('code', null);
		
		//title
    	$this->view->title= $this->view->translate("Discipline Set-up")." - ".$this->view->translate("Edit");
    	
    	$form = new GeneralSetup_Form_SchoolDiscipline();
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
	    	if ($form->isValid($formData)) {
				
				$this->_DbObj->updateData($formData,$code);
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true)); 
			}else{
				$form->populate($formData);	
			}
    	}else{
    		if($code!=null){
    			
    			$form->populate($this->_DbObj->getData($code));
    		}
    	}
    	
    	$this->view->form = $form;
    }
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	
    	if($id!=0){
    		$this->_DbObj->deleteData($id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true));
    	
    }
    
    public function rankAction(){
    	
    	$fee_structure_id = $this->_getParam('fee_structure_id', 0);
    	$this->view->fee_structure_id = $fee_structure_id;
    	
    	$rank_id = $this->_getParam('id', 0);
    	$this->view->rank_id = $rank_id;
    	
    	$this->view->title= $this->view->translate("Discount Set-up")." - ".$this->view->translate("Rank");
    	
    	
    	//fee structure data
    	$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
    	$this->view->fee_structure = $feeStructureDb->getData($fee_structure_id);
    	
    	//fee structure item
    	$feeStructureItem = new Studentfinance_Model_DbTable_FeeStructureItem();
    	$this->view->feeStructureItemList = $feeStructureItem->getStructureData($fee_structure_id); 
    	
    	//fee structure plan detail
    	$discountDetail = array();
    	$discountRankDb = new Studentfinance_Model_DbTable_FeeStructureDiscountRank();
    	
    	$this->view->discount = $discountRankDb->getPlanData($fee_structure_id,$rank_id);
    	
    	/*echo "<pre>";
    	print_r($this->view->discount);
    	echo "</pre>";*/
    	
    	/*for($i=0; ($i+1)<=(int)$paymentPlanData['fsp_bil_installment']; $i++){
    		$paymentPlanDetail[$i] = $fsPlanDetailDb->getPlanData($paymentPlanData['fsp_structure_id'], $id, ($i+1));
    	}
    	$this->view->paymentPlanDetail = $paymentPlanDetail;*/
    	
    }
    
	public function ajaxSaveFeeDiscountDetailAction(){
		$result = array('status'=>false);
		
		$fdr_id = $this->_getParam('fdr_id', 0);
		$rank = $this->_getParam('fdr_rank', 0);
		$fdr_item_id = $this->_getParam('fdr_item_id', 0);
    	
    	
    	$amount = $this->_getParam('fdr_amount', null);
    	$percentage = $this->_getParam('fdr_percentage', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
        $fdrDb = new Studentfinance_Model_DbTable_FeeStructureDiscountRank();
        if($fdr_id!="" && $fdr_id!=0){
        	//update
        	$data = array(
        				'fdr_percentage' => $percentage,
        				'fdr_amount' => $amount
        			);
        	
        	$result['id'] = 0;
        	if( $fdrDb->updateData($data, $fdr_id) ){
        		$result['status'] = true;
        	}
        	
        }else{
        	//add
        	$data = array(
        				'fdr_rank' => $rank,
        				'fdr_item_id' => $fdr_item_id,
        				'fdr_percentage' => $percentage,
        				'fdr_amount' => $amount
        			);
        			
        	$result['id'] = $fdrDb->addData($data);
        	$result['status'] = true;
        }
	  	
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode( $result );
		
		echo $json;
		exit();
    }
}

