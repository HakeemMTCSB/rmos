<?php
/**
 *  @author suliana 
 *  @date Feb 25, 2014
 */
 

ini_set('memory_limit', '-1');

class Studentfinance_ReportController extends Base_Base {

  public function studentSemesterPaymentAction(){
    
    $this->view->title = "Student Semester Payment Report";
    
    $this->view->paid_status = 1;
    
    if ($this->getRequest()->isPost()) {
        $formData = $this->getRequest()->getPost();
        
        $academic_year = $formData['academic_year_id'];
        $this->view->academic_year = $academic_year;
        
        $semester = $formData['semester_id'];
        $this->view->semester = $semester;
        
        $faculty = $formData['faculty_id'];
        $this->view->faculty = $faculty;
        
        $program = $formData['program_id'];
        $this->view->program = $program;
        
        $paid_status = $formData['paid_status'];
        $this->view->paid_status = $paid_status;
        
        //get report status
        $studentSemesterStatusDb = new Studentfinance_Model_DbTable_StudentSemesterPayment();
        
        $where = array(
            'semester_id = ?' => $semester,
            'faculty_id = ?' => $faculty,
            'program_id = ?' => $program,
            'paid_status =?' => $paid_status
        );
        
        $report_list = $studentSemesterStatusDb->fetchAll($where);
        
        if($report_list){
          $report_list = $report_list->toArray();
        }else{
          $report_list = array();
        }
        
        //check if there is not complete report
        if(sizeof($report_list)>0){
          $running_report = false;
          
          foreach ($report_list as $report){
            if($report['total_record']!=$report['total_processed']){
              $running_report = true;
              break;
            }
          }
          
          $this->view->running_report = $running_report;
        }
        
        $this->view->report_list = $report_list;
        
    }
    
    //academic year
    $academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    $academicYearList = $academicYearDb->fetchAll(null,'ay_start_date desc');
    $this->view->academic_year_list = $academicYearList;
    
    //semester
    $semesterDb = new Records_Model_DbTable_SemesterMaster();
    $order = "SUBSTRING(SemesterMainCode, 1,4) DESC, SemesterCountType DESC, SemesterFunctionType DESC";
    
    $semesterList = $semesterDb->fetchAll(null,$order);
    $this->view->semester_list = $semesterList;
    
    //faculty
    $facultyDb = new App_Model_General_DbTable_Collegemaster();
    $facultyList = $facultyDb->getFaculty();
    $this->view->faculty_list = $facultyList;
    
    //program
    $programDb = new App_Model_Record_DbTable_Program();
    $programList = $programDb->fetchAll(null,'ProgramCode');
    $this->view->program_list = $programList;
    
    
  }
  
  public function studentSemesterPaymentPdfAction(){
    
    if ($this->getRequest()->isPost()) {
      
      $formData = $this->getRequest()->getPost();
      
      //fee item list
      $feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
      $feeItemList = $feeItemDb->getActiveFeeItem();
      
      //report info
      $studentSemesterPaymentDb = new Studentfinance_Model_DbTable_StudentSemesterPayment();
      
      $report = $studentSemesterPaymentDb->getData($formData['rid']);
      
      
      //report detail
      $studentSemesterPaymentDetailDb = new Studentfinance_Model_DbTable_StudentSemesterPaymentDetail();
      
      $where = array('rpt_student_semester_payment_id=?'=>$report['id']);
      $report_detail = $studentSemesterPaymentDetailDb->fetchAll($where);
      $report_detail = $report_detail->toArray();
      
      
      /*
       * Generate CSV File
       * 
       */
      
      $filename = "STUDSEMPAY_".$report['faculty_code']."_".date("Ymd",strtotime($report['date_create']));
      
      $file = APPLICATION_PATH . "/tmp/" . $filename . ".csv";
      
      $realPath = realpath( $file );
      
      if ( false === $realPath )
      {
        touch( $file );
        chmod( $file, 0777 );
      }
      
      $file = realpath( $file );
      $handle = fopen( $file, "w" );
      
      
      $finalData[] = array('Laporan Keuangan');
      $finalData[] = array('');
      $finalData[] = array('Fakultas:',$report['faculty_name']);
      $finalData[] = array('Program:',$report['program_name']);
      $finalData[] = array('Period:',$report['semester_name']);
      $finalData[] = array('Status:',$report['paid_status']==1?'Paid':'Not Paid');
      $finalData[] = array('');
      
      
      $header[] = 'NO';
      $header[] = 'NIM';
      $header[] = 'NAME';
      
      foreach ($feeItemList as $feeitem){
        $header[] = strtoupper($feeitem['fi_name_short']);
      }
      
      $header[] = 'TOTAL';
      
      $header[] = '';
      $header[] = '';
      $header[] = 'TOTAL INVOICE AMOUNT';
      $header[] = 'TOTAL INVOICE PAID';
      $header[] = 'TOTAL INVOICE BALANCE';
       
      $finalData[] = $header;
       
      $total_amount = 0;
      $total_i1 = 0;
      $total_i2 = 0;
      $total_i3 = 0;
      $total_i4 = 0;
      $total_i5 = 0;
      $total_i6 = 0;
      $total_i7 = 0;
      $total_i8 = 0;
      $total_i9 = 0;
      $total_i10 = 0;
      $total_i11 = 0;
      
      if($report_detail){
        $i=1;
        foreach ( $report_detail AS $key=>$row )
        {
           
          $total = $row["item_1"]+$row["item_2"]+$row["item_3"]+$row["item_4"]+$row["item_5"]+$row["item_6"]+$row["item_7"]+$row["item_8"]+$row["item_9"]+$row["item_10"]+$row["item_11"];
          $total_amount += $total;
          
          $total_i1 += $row["item_1"];
          $total_i2 += $row["item_2"];
          $total_i3 += $row["item_3"];
          $total_i4 += $row["item_4"];
          $total_i5 += $row["item_5"];
          $total_i6 += $row["item_6"];
          $total_i7 += $row["item_7"];
          $total_i8 += $row["item_8"];
          $total_i9 += $row["item_9"];
          $total_i10 += $row["item_10"];
          $total_i11 += $row["item_11"];
          
          $finalData[] = array(
               
              $i , 
              "'".$row["nim"]."'" ,
              $row["fullname"] ,
              $row["item_1"] ,
              $row["item_2"] ,
              $row["item_3"] ,
              $row["item_4"] ,
              $row["item_5"] ,
              $row["item_6"] ,
              $row["item_7"] ,
              $row["item_8"] ,
              $row["item_9"] ,
              $row["item_10"] ,
              $row["item_11"] ,
              $total,
              '',
              '',
              $row["invoice_total_amount"],
              $row["invoice_total_paid"],
              $row["invoice_total_balance"]
          );
      
          $i++;
        }
      }
      
      $finalData[] = array('','','TOTAL',
          $total_i1,
          $total_i2,
          $total_i3,
          $total_i4,
          $total_i5,
          $total_i6,
          $total_i7,
          $total_i8,
          $total_i9,
          $total_i10,
          $total_i11,
          $total_amount
          );
      
      
      
      
      //Write to file
      
      if (PHP_EOL == "\r\n"){
        $eol = "\n";
      }else{
        $eol = "\r\n";
      }
      
      foreach ( $finalData AS $finalRow )
      {
         
        fwrite( $handle, implode(",", $finalRow).$eol);
         
      }
       
      fclose( $handle );
      
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      
      $this->getResponse()->setRawHeader( "Content-Type: application/csv" )
      ->setRawHeader( "Content-Disposition: attachment; filename=".$filename.".csv" )
      ->setRawHeader( "Content-Transfer-Encoding: binary" )
      ->setRawHeader( "Expires: 0" )
      ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
      ->setRawHeader( "Pragma: public" )
      ->setRawHeader( "Content-Length: " . filesize( $file ) )
      ->sendResponse();
      
      readfile( $file );
      unlink($file);
      exit;
    }
    
  }
  
  public function generateReportAction(){
    
    $result['status'] = 0;
    
    if ($this->getRequest()->isPost()) {
      $formData = $this->getRequest()->getPost();
      
      $result['data'] = $formData;
      
      //get student
      $db = Zend_Db_Table::getDefaultAdapter();
      
      if($formData['paid_status']==1){
        
        $select = $db->select()
        ->from(array('sr'=>'tbl_studentregistration'))
        ->join(array('im'=>'invoice_main'), 'im.IdStudentRegistration = sr.IdStudentRegistration and im.semester = '.$formData['semester_id'], array())
        ->join(array('pm'=>'payment_main'), 'pm.billing_no = im.bill_number')
        ->where('sr.IdProgram = ?',$formData['program_id']);
       
        $row = $db->fetchAll($select);
      
      }else{
        
        $select = $db->select()
        ->from(array('sr'=>'tbl_studentregistration'))
        ->join(array('im'=>'invoice_main'), 'im.IdStudentRegistration = sr.IdStudentRegistration and im.semester = '.$formData['semester_id'], array())
        ->joinLeft(array('pm'=>'payment_main'), 'pm.billing_no = im.bill_number')
        ->where('sr.IdProgram = ?',$formData['program_id'])
        ->where('pm.id is null');
        
        $row = $db->fetchAll($select);
        
      }
      
      if(!$row){
        $result['status'] = 0;
        $result['msg'] = "Cannot generate report. No student record found with status: " . ($formData['paid_status']==1?"Paid":"Not Paid");
      }else{
      
        $result['student'] = $row;
        
        //update tbl student_semester_payment
        $data = array(
            'semester_id' => $formData['semester_id'],
            'faculty_id' => $formData['faculty_id'],
            'program_id' => $formData['program_id'],
            'paid_status' => $formData['paid_status'],
            'total_record' => sizeof($row),
            'date_create' => date('Y-m-d H:i:s')
        );
        
        $studentSemesterPaymentDb = new Studentfinance_Model_DbTable_StudentSemesterPayment();
        $report_id = $studentSemesterPaymentDb->insert($data);
        
        //run command line
        $command = "php ".APPLICATION_PATH."/../cronjob/report_finance.php ".$report_id;
        
        $this->execInBackground($command);
        
        
        $result['status'] = 1;
      }
    }
    

    
    $this->_helper->json($result);
    exit;
  }
  
  private function execInBackground($cmd) {
    if (substr(php_uname(), 0, 7) == "Windows"){
      pclose(popen("start /B ". $cmd, "r"));
    }
    else {
      //exec($cmd . " > /dev/null 2>/dev/null &");
      exec($cmd . " > /dev/null &");
      //exec($cmd . " >> ".APPLICATION_PATH."/../cronjob/report.txt");
    }
  }
  
	public function collectionAction(){
        $this->view->title = $this->view->translate('Collection Report');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //account code
        $accountCodeDB = new Studentfinance_Model_DbTable_AccountCode();
        $this->view->accountcodeData = $accountCodeDB->getAccountCode();
        
        //payment group
        $lobjdeftype = new App_Model_Definitiontype();
		$this->view->payment_group = $lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
//			 echo "<pre>";
//			 print_r($formData);

//exit;
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->program = $formData['program'];
			 $this->view->IdProgramScheme = $formData['IdProgramScheme'];
			 $this->view->accountcode = $formData['accountcode'];
			 $this->view->paymentmode = $formData['paymentmode'];
				 
			$receiptDB = new Studentfinance_Model_DbTable_Receipt();		
		    $list = $receiptDB->getSearchDataReport($formData);
		    
		    
         	$reportArray = array();
		    if($list){
			    
			    $i=0;
			    $reportArray = $list;
			    foreach($list as $data){
			    	
				    if($data['scholarship']){ //scholarship
						$reportArray[$i]['funding'] = $data['scholarship'];
			    	}elseif($data['sponsorship']){ //scholarship
						$reportArray[$i]['funding'] = $data['sponsorship'];
			    	}else{
			    		$reportArray[$i]['funding'] = 'Self Funding';
			    	}
			    	
			    	
			    	if(isset($data['EnterDate'])){
			    		$monthRcp = date('m',strtotime($data['rcp_receive_date']));
			    		$monthAdj = date('m',strtotime($data['EnterDate']));
			    		if($monthRcp != $monthAdj){
			    			$reportArray[$i]['AdjustmentDate'] = $data['EnterDate'];
			    		}else{
			    			$reportArray[$i]['sama'] = 'sama';
			    			unset($reportArray[$i]);
			    		}
			    		
			    	}
			    	
			    	if(($data['rcp_status'] == 'CANCEL') && !isset($data['EnterDate'])){
			    		unset($reportArray[$i]);
			    	}
			    
			    	$i++;
			    }
		    }
		    
			$this->view->list = $reportArray;
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($reportArray));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$form->populate($formData);
			$this->view->paginator = $paginator;
				
//				echo '<pre>';
//        		print_r($reportArray);
        		//exit;
         }
            
        
       
    }
    
    public function printCollectionReportAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 	
			$receiptDB = new Studentfinance_Model_DbTable_Receipt();		
			$list = $receiptDB->getSearchDataReport($formData);
			
			$reportArray = array();
		    if($list){
			    
			    $i=0;
			    $reportArray = $list;
			    foreach($list as $data){
			    	
			    	if($data['scholarship']){ //scholarship
						$reportArray[$i]['funding'] = $data['scholarship'];
			    	}elseif($data['sponsorship']){ //scholarship
						$reportArray[$i]['funding'] = $data['sponsorship'];
			    	}else{
			    		$reportArray[$i]['funding'] = 'Self Funding';
			    	}
			    	
			    	
			    	if(isset($data['EnterDate'])){
			    		$monthRcp = date('m',strtotime($data['rcp_receive_date']));
			    		$monthAdj = date('m',strtotime($data['EnterDate']));
			    		if($monthRcp != $monthAdj){
			    			$reportArray[$i]['AdjustmentDate'] = $data['EnterDate'];
			    		}else{
			    			$reportArray[$i]['sama'] = 'sama';
			    			unset($reportArray[$i]);
			    		}
			    		
			    	}
			    	
			    	if(($data['rcp_status'] == 'CANCEL') && !isset($data['EnterDate'])){
			    		unset($reportArray[$i]);
			    	}
			    
			    	$i++;
	    		}
		    }
		    
			    
		    $this->view->paginator = $reportArray;	
        }
        $this->view->filename = date('Ymd').'_collectionreport.xls';
    }

    public function ageingAction(){
        $this->view->title = $this->view->translate('Ageing Report');


//        $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Under Maintenance');

        $session = Zend_Registry::get('sis');

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        //clear the session
        if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
            unset($session->result);
        }

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        $db = Zend_Db_Table::getDefaultAdapter();

        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();
            $formData['semester']=null;

//			  $this->gobjsessionsis->flash = array('type'=>'error','message'=>'Disabled at this moment');



//			 echo "<pre>";
//			 print_r($formData);

            $this->view->date_from = $formData['date_from'];
            $this->view->date_to = $formData['date_to'];
            $this->view->program = $formData['program'];
            $this->view->semester = isset($formData['semester_id'])?$formData['semester_id']:0;

            $receiptDB = new Studentfinance_Model_DbTable_InvoiceMain();
            $list = $receiptDB->getOutstandingInvoiceReport($formData);
            //var_dump($list); //exit;

           /* echo "<pre>";
            print_r($list[0]);
            exit;*/

            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

            $reportArray = array();
            $i=0;
            foreach($list as $data){

                $studentName = '';
                if($data['appl_fullname']){
                    $studentName = $data['appl_fullname'];
                }else{
                    $studentName = $data['student_fullname'];
                }

                $reportArray[$i]['invoice_no'] = $data['invoice_no'];
                $reportArray[$i]['invoice_date'] = $data['invoice_date'];
                $reportArray[$i]['registrationId'] = isset($data['registrationId'])?$data['registrationId']:$data['at_pes_id'];
                $reportArray[$i]['student_fullname'] = $studentName;
                $reportArray[$i]['profileStatus'] = $data['profileStatus'];
                $reportArray[$i]['SemesterMainName'] = $data['SemesterMainName'];
                $reportArray[$i]['applicant_program'] = $data['applicant_program'];
                $reportArray[$i]['mop'] = $data['mop'];
                $reportArray[$i]['mos'] = $data['mos'];
                $reportArray[$i]['pt'] = $data['pt'];
                $reportArray[$i]['fi_name'] = $data['fi_name'];
                $reportArray[$i]['appl_email'] = $data['appl_email'];
                $reportArray[$i]['amount'] = $data['amount'];
                $reportArray[$i]['dt_discount'] = $data['dt_discount'];
                $reportArray[$i]['currency_id'] = $data['currency_id'];
                $reportArray[$i]['SubCode'] = $data['SubCode'];
                $reportArray[$i]['MigrateCode'] = $data['MigrateCode'];
                $reportArray[$i]['bill_description'] = $data['bill_description'];

//	    		$newbalance = 0;
                //get receipt_invoice
                $receiptInvDB = new Studentfinance_Model_DbTable_ReceiptInvoice();
                $invMain =  $data['invMainID'];
                $invDetail =  $data['idDetail'];
                $reportArray[$i]['invMainID'] = $invMain;
                $reportArray[$i]['idDetail'] = $invDetail;

                /*$searchRecInv = array(
                    'date_from'=>$formData['date_from'],
                    'date_to'=>$formData['date_to'],
                    'invoice_id'=>$invMain,
                    'invoice_detail_id'=>$invDetail,
                    'currency_id'=>$data['currency_id'],
                    'invoice_date'=>$data['invoice_date']
                );*/

//		    	echo "<pre>";
//		    	print_r($reportArray);

                /*
                 * FORMULA
                 * [payment] - [advance_payment_utilize] - [discount] - [CN] + [refund] = [outstanding_amount]
                 *
                 * checking balance from
                 * 1) receipt_invoice
                 * 2) advance payment utilization
                 * 3) discount
                 * 4) CN drop by asad
                 */

                //get outstanding invoice
                /*$balancePaid = $receiptInvDB->getOutstandingInv($searchRecInv);

                $reportArray[$i]['balancePaid'] = $balancePaid;*/

                //get cn amount based on date

                /*$searchCN = array(
                    'date_from'=>$formData['date_from'],
                    'date_to'=>$formData['date_to'],
                    'invoice_id'=>$invMain,
                    'invoice_detail_id'=>$invDetail
                );

                $creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
                $cnAmount = $creditNoteDB->getSearchDataCN($searchCN);

                $cnAmount['cnamount'] = isset($cnAmount['cnamount'])?$cnAmount['cnamount']:0;
                $newbalance = $data['amount'] - $cnAmount['cnamount'];
                if($balancePaid){
                    $newbalance = $newbalance - $balancePaid;
                }*/

                //check sponsor payment
                /*$searchSPM = array(
                    'date_from'=>$formData['date_from'],
                    'date_to'=>$formData['date_to'],
                    'invoice_id'=>$invMain,
                    'invoice_detail_id'=>$invDetail
                );
                $sponsorPayment = $receiptDB->getSponsorPayment($searchSPM);*/

                //$newbalance = $newbalance - $sponsorPayment[0]['sponsorpaid'];

                $balModel = new Studentfinance_Model_DbTable_PaymentAmount();

                $balance2 = $balModel->getBalanceMain($invMain, $formData['date_from'], $formData['date_to']);
                $newbalance2 = str_replace(',', '', $balance2['bill_balance']);

                $newbalance = 0.00;

                if ($newbalance2 > 0) {
                    $balance = $balModel->getBalanceDtl($invDetail, $formData['date_from'], $formData['date_to']);
                    $newbalance = str_replace(',', '', $balance['bill_balance']);
                }

                $reportArray[$i]['balance'] = $newbalance;

                $amountCur = $newbalance;
                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                $exrate = $curRateDB->getRateByDate($data['currency_id'], $data['invoice_date']);

                if (!$exrate){
                    $exrate['cr_exchange_rate'] = $data['cr_exchange_rate'];
                }

                $reportArray[$i]['amount_convert'] = ($amountCur*$exrate['cr_exchange_rate']);

                //funding method - get latest funding method
                $fundName = array();
                if($data['IdStudentRegistration']) {
                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                    $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($data['IdStudentRegistration'], $data['invoice_date']);


                    if (isset($taggingFinancialAid['Sponsorship'])) {
                        $fundName[] = $taggingFinancialAid['Sponsorship'];
                    }

                    if (isset($taggingFinancialAid['Scholarship'])) {
                        $fundName[] = $taggingFinancialAid['Scholarship'];
                    }
                }

                /*if($taggingFinancialAid) {
                    $reportArray[$i]['funding'] = isset($taggingFinancialAid['Sponsorship']) ? $taggingFinancialAid['Sponsorship'] : $taggingFinancialAid['Scholarship'];
                }else {
                    $reportArray[$i]['funding'] = 'Self Funding';
                }*/

                if($fundName){
                    $reportArray[$i]['funding'] = $fundName;
                }else {
                    $reportArray[$i]['funding'] = 'Self Funding';
                }



                /*if($data['scholarship']){ //scholarship
                    $reportArray[$i]['funding'] = $data['scholarship'];
                }elseif($data['sponsorship']){ //scholarship
                    $reportArray[$i]['funding'] = $data['sponsorship'];
                }else{
                    $reportArray[$i]['funding'] = 'Self Funding';
                }*/

                $days = (strtotime( $formData['date_to']) - strtotime($data['invoice_date'])) / (60 * 60 * 24);
                $reportArray[$i]['days'] = $days;

                if($newbalance <= 0){
                    unset($reportArray[$i]);
                }

                $i++;
            }


            /* $this->view->list = $reportArray;

             $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($reportArray));
             $paginator->setItemCountPerPage(5000);
             $paginator->setCurrentPageNumber($this->_getParam('page',1));
             $this->view->paginator = $paginator;*/

//				echo '<pre>';
//        		print_r($reportArray);
            //exit;
        }else{
            $reportArray = array();
        }

        $this->view->list = $reportArray;

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($reportArray));
        $paginator->setItemCountPerPage(5000);
        $paginator->setCurrentPageNumber($this->_getParam('page',1));
        $this->view->paginator = $paginator;



    }
    
	public function getSemesterAction()
	{	
    	$idProgram = $this->_getParam('idProgram', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
					->from(array('a'=>'tbl_program'))
					->join(array('b' => 'tbl_semestermaster'), 'b.IdScheme = a.IdScheme')
					//->where("b.SemesterMainName NOT IN ('Exemption','Credit Transfer','Audit','Part 3')")
                                        ->where('b.display = ?', 1)
					->order('b.SemesterMainStartDate desc');
					if($idProgram != 'ALL'){
						$select->where('a.IdProgram = ?',$idProgram);
					}else{
						$select->group('b.SemesterMainCode');
					}
       	$result = $db->fetchAll($select);
         
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }

    public function printAgeingReportAction(){

        $session = Zend_Registry::get('sis');

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        $this->_helper->layout->disableLayout();

        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();
            $formData['semester']=null;
            $this->view->date_to = $formData['date_to'];

            $receiptDB = new Studentfinance_Model_DbTable_InvoiceMain();
            $list = $receiptDB->getOutstandingInvoiceReport($formData);

            $currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

            $reportArray = array();
            $i=0;
            foreach($list as $data){
                $studentName = '';
                if($data['appl_fullname']){
                    $studentName = $data['appl_fullname'];
                }else{
                    $studentName = $data['student_fullname'];
                }

                $reportArray[$i]['invoice_no'] = $data['invoice_no'];
                $reportArray[$i]['invoice_date'] = $data['invoice_date'];
                $reportArray[$i]['registrationId'] = isset($data['registrationId'])?$data['registrationId']:$data['at_pes_id'];
                $reportArray[$i]['student_fullname'] = $studentName;
                $reportArray[$i]['profileStatus'] = $data['profileStatus'];
                $reportArray[$i]['SemesterMainName'] = $data['SemesterMainName'];
                $reportArray[$i]['applicant_program'] = $data['applicant_program'];
                $reportArray[$i]['mop'] = $data['mop'];
                $reportArray[$i]['mos'] = $data['mos'];
                $reportArray[$i]['pt'] = $data['pt'];
                $reportArray[$i]['fi_name'] = $data['fi_name'];
                $reportArray[$i]['appl_email'] = $data['appl_email'];
                $reportArray[$i]['amount'] = $data['amount'];
                $reportArray[$i]['dt_discount'] = $data['dt_discount'];
                $reportArray[$i]['currency_id'] = $data['currency_id'];
                $reportArray[$i]['SubCode'] = $data['SubCode'];
                $reportArray[$i]['MigrateCode'] = $data['MigrateCode'];
                $reportArray[$i]['bill_description'] = $data['bill_description'];

//	    		$newbalance = 0;
                //get receipt_invoice
                $receiptInvDB = new Studentfinance_Model_DbTable_ReceiptInvoice();
                $invMain =  $data['invMainID'];
                $invDetail =  $data['idDetail'];
                $reportArray[$i]['invMainID'] = $invMain;
                $reportArray[$i]['idDetail'] = $invDetail;

                /*$searchRecInv = array(
                    'date_from'=>$formData['date_from'],
                    'date_to'=>$formData['date_to'],
                    'invoice_id'=>$invMain,
                    'invoice_detail_id'=>$invDetail,
                    'currency_id'=>$data['currency_id'],
                    'invoice_date'=>$data['invoice_date']
                );*/

//		    	echo "<pre>";
//		    	print_r($reportArray);

                /*
                 * FORMULA
                 * [payment] - [advance_payment_utilize] - [discount] - [CN] + [refund] = [outstanding_amount]
                 *
                 * checking balance from
                 * 1) receipt_invoice
                 * 2) advance payment utilization
                 * 3) discount
                 * 4) CN drop by asad
                 */

                //get outstanding invoice
                /*$balancePaid = $receiptInvDB->getOutstandingInv($searchRecInv);

                $reportArray[$i]['balancePaid'] = $balancePaid;*/

                //get cn amount based on date

                /*$searchCN = array(
                    'date_from'=>$formData['date_from'],
                    'date_to'=>$formData['date_to'],
                    'invoice_id'=>$invMain,
                    'invoice_detail_id'=>$invDetail
                );

                $creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
                $cnAmount = $creditNoteDB->getSearchDataCN($searchCN);

                $cnAmount['cnamount'] = isset($cnAmount['cnamount'])?$cnAmount['cnamount']:0;
                $newbalance = $data['amount'] - $cnAmount['cnamount'];
                if($balancePaid){
                    $newbalance = $newbalance - $balancePaid;
                }

                $reportArray[$i]['balance'] = $newbalance;

                $amountCur = $newbalance;
                $amountCur = $amountCur * $data['cr_exchange_rate'];
                $reportArray[$i]['amount_convert'] = $amountCur;*/

                $balModel = new Studentfinance_Model_DbTable_PaymentAmount();

                //$balance2 = $balModel->getBalanceMain($invMain, $formData['date_from'], $formData['date_to']);
                //$newbalance2 = str_replace(',', '', $balance2['bill_balance']);

                //$newbalance = 0.00;

                //if ($newbalance2 > 0) {
                    $balance = $balModel->getBalanceDtl($invDetail, $formData['date_from'], $formData['date_to']);
                    $newbalance = str_replace(',', '', $balance['bill_balance']);
                //}

                $reportArray[$i]['balance'] = $newbalance;

                $amountCur = $newbalance;
                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                $exrate = $curRateDB->getRateByDate($data['currency_id'], $data['invoice_date']);

                if (!$exrate){
                    $exrate['cr_exchange_rate'] = $data['cr_exchange_rate'];
                }

                $reportArray[$i]['amount_convert'] = ($amountCur*$exrate['cr_exchange_rate']);

                //funding method - get latest funding method
                $fundName = array();
                if($data['IdStudentRegistration']) {
                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                    $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($data['IdStudentRegistration'], $data['invoice_date']);


                    if (isset($taggingFinancialAid['Sponsorship'])) {
                        $fundName[] = $taggingFinancialAid['Sponsorship'];
                    }

                    if (isset($taggingFinancialAid['Scholarship'])) {
                        $fundName[] = $taggingFinancialAid['Scholarship'];
                    }
                }

                /*if($taggingFinancialAid) {
                    $reportArray[$i]['funding'] = isset($taggingFinancialAid['Sponsorship']) ? $taggingFinancialAid['Sponsorship'] : $taggingFinancialAid['Scholarship'];
                }else {
                    $reportArray[$i]['funding'] = 'Self Funding';
                }*/

                if($fundName){
                    $reportArray[$i]['funding'] = $fundName;
                }else {
                    $reportArray[$i]['funding'] = 'Self Funding';
                }



                /*if($data['scholarship']){ //scholarship
                    $reportArray[$i]['funding'] = $data['scholarship'];
                }elseif($data['sponsorship']){ //scholarship
                    $reportArray[$i]['funding'] = $data['sponsorship'];
                }else{
                    $reportArray[$i]['funding'] = 'Self Funding';
                }*/

                $days = (strtotime( $formData['date_to']) - strtotime($data['invoice_date'])) / (60 * 60 * 24);
                $reportArray[$i]['days'] = $days;

                if($newbalance <= 0){
                    unset($reportArray[$i]);
                }

                $i++;
            }


            $this->view->paginator = $reportArray;
        }
        $this->view->filename = date('Ymd').'_ageingreport.xls';
    }
    
    
	public function ageingSummaryAction(){
        $this->view->title = $this->view->translate('Ageing Summary');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        $arrayDay = array();
	        $m=0;
	        for($k=0;$k<=360;$k++){
				$arrayDay[$m]['min'] = $k;
				
				if($m == 0){
					$k = $k +30;
				}else{
					$k = $k +29;
				}
			 	
			 	$arrayDay[$m]['max']=$k;
			 	
			 	$m++;
			 	
			 }
			 $arrayDay[$m]['min']='361';
			 $arrayDay[$m]['max']='10000';
			 $this->view->range = $arrayDay;
        
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 $formData['semester_id']=null;
			 
          	$this->gobjsessionsis->flash = array('type'=>'error','message'=>'Disabled at this moment');
         }
			 
			/* $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->program = $formData['program'];
			 $this->view->semester = $formData['semester'];
			 
         	
			 
			 $programData = $programDb->getProgramArray($formData['program']);
			 
			 $newArray = array();
			 $i=0;
			 foreach($programData as $prog){
			 	
			 	$feeDB = new Studentfinance_Model_DbTable_FeeItem();		
			    $listFee = $feeDB->getListByProgram($prog['IdProgram']);
		
			    $newArray[$i]['programname'] = $prog['ProgramName'];
			    $newArray[$i]['ProgramCode'] = $prog['ProgramCode'];
			    
			    $m=0;
			    if($listFee){
			    	$sumArray = array();
				    foreach($listFee as $fee){
				   		$newArray[$i]['fee'][$m]['fi_name'] = $fee['fi_name'];

				   		$n=0;
				   		
				   		$grandtotal = 0;
				   		
					    foreach($arrayDay as $key=>$ady){
					    	$min = $ady['min'];
					    	$max = $ady['max'];
					    	$newArray[$i]['fee'][$m]['range_days'][$n]['min'] = $min;
					    	$newArray[$i]['fee'][$m]['range_days'][$n]['max'] = $max;
					    	
					    	$formData['min'] = $min;
					    	$formData['max'] = $max;
					    	$formData['fi_id'] = $fee['fi_id'];
					    	$formData['program_id'] = $fee['IdProgram'];
					    	
					    	//get total amount by range_days
					    	$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();	
					    	$list = $invoiceMainDB->getOutstandingAmountByDays($formData);
					    	$newArray[$i]['fee'][$m]['range_days'][$n]['totalamount'] = $list;
					    	
					    	$grandtotal += $list;
					    	
							$sumArray[$n] = !isset($sumArray[$n]) ? 0 : $sumArray[$n];
					    	$sumArray[$n] += $list;
					    	
					    	$n++;
					    	
					    }
					    
				    	$newArray[$i]['fee'][$m]['grandtotal'] = $grandtotal;
						
						
				   		$m++;
				    }
				  $newArray[$i]['totalamountprogram']= $sumArray;
				  $newArray[$i]['grandtotalamountprogram']= $sumArray; 
				    
			    }else{
			    	$newArray[$i]['fee'] = null;
			    }
			    
			    $i++;
//			    	 echo "<pre>";
//		 print_r($newArray);
//			    exit;
		    
			 }
			 

		    $this->view->list = $newArray;
		    
			
         }   */         
        
       
    }
    
    
	public function printAgeingSummaryReportAction(){
		
		 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 $programDb = new Registration_Model_DbTable_Program();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
			$formData['semester']=null;
			
     		$arrayDay = array();
	        $m=0;
	        for($k=0;$k<=360;$k++){
				$arrayDay[$m]['min'] = $k;
				
				if($m == 0){
					$k = $k +30;
				}else{
					$k = $k +29;
				}
			 	
			 	$arrayDay[$m]['max']=$k;
			 	
			 	$m++;
			 	
			 }
 			 $arrayDay[$m]['min']='361';
			 $arrayDay[$m]['max']='10000';
			 $this->view->range = $arrayDay;
			 
			 $programData = $programDb->getProgramArray($formData['program']);
			 
			 $newArray = array();
			 $i=0;
     	foreach($programData as $prog){
			 	
			 	$feeDB = new Studentfinance_Model_DbTable_FeeItem();		
			    $listFee = $feeDB->getListByProgram($prog['IdProgram']);
		
			    $newArray[$i]['programname'] = $prog['ProgramName'];
			    $newArray[$i]['ProgramCode'] = $prog['ProgramCode'];
			    
			    $m=0;
			    if($listFee){
			    	$sumArray = array();
				    foreach($listFee as $fee){
				   		$newArray[$i]['fee'][$m]['fi_name'] = $fee['fi_name'];

				   		$n=0;
				   		
				   		$grandtotal = 0;
				   		
					    foreach($arrayDay as $key=>$ady){
					    	$min = $ady['min'];
					    	$max = $ady['max'];
					    	$newArray[$i]['fee'][$m]['range_days'][$n]['min'] = $min;
					    	$newArray[$i]['fee'][$m]['range_days'][$n]['max'] = $max;
					    	
					    	$formData['min'] = $min;
					    	$formData['max'] = $max;
					    	$formData['fi_id'] = $fee['fi_id'];
					    	$formData['program_id'] = $fee['IdProgram'];
					    	
					    	//get total amount by range_days
					    	$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();	
					    	$list = $invoiceMainDB->getOutstandingAmountByDays($formData);
					    	$newArray[$i]['fee'][$m]['range_days'][$n]['totalamount'] = $list;
					    	
					    	$grandtotal += $list;
					    	
							$sumArray[$n] = !isset($sumArray[$n]) ? 0 : $sumArray[$n];
					    	$sumArray[$n] += $list;
					    	
					    	$n++;
					    	
					    }
					    
				    	$newArray[$i]['fee'][$m]['grandtotal'] = $grandtotal;
						
						
				   		$m++;
				    }
				  $newArray[$i]['totalamountprogram']= $sumArray;
				  $newArray[$i]['grandtotalamountprogram']= $sumArray; 
				    
			    }else{
			    	$newArray[$i]['fee'] = null;
			    }
			    
			    $i++;
		    
			 }
		    
		    $this->view->list = $newArray;	
        }
        
        $this->view->filename = date('Ymd').'_ageingsummaryreport.xls';
    }
    
	public function billingAction(){
        $this->view->title = $this->view->translate('Billing Report');
        
        $session = Zend_Registry::get('sis');
        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $formData['semester']=null;

        $cmodel = new Studentfinance_Model_DbTable_Correction();

        $slist = $cmodel->getInvoiceWrongSemesterStudent();

        if ($slist){
            foreach ($slist as $sloop){
                $sdata = array(
                    'semester'=>$sloop['SemesterProgram']
                );
                $cmodel->updateInvoice($sdata, $sloop['id']);
            }
        }

        $alist = $cmodel->getInvoiceWrongSemesterApplicant();

        if ($alist){
            foreach ($alist as $aloop){
                $adata = array(
                    'semester'=>$aloop['SemesterProgram']
                );
                $cmodel->updateInvoice($adata, $aloop['id']);
            }
        }
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();	
        $this->view->intakeData = $intakeDb->fngetallIntakelist();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();

             //var_dump($formData); exit;
			 
			 $formData['semester_id']=null;
			 $formData['intake_id']=null;
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->program = $formData['program'];
			 $this->view->semester = isset($formData['semester'])?$formData['semester']:0;
			 $this->view->intake = isset($formData['intake'])?$formData['intake']:0;
				 
			$receiptDB = new Studentfinance_Model_DbTable_InvoiceMain();		
		    $list = $receiptDB->getInvoiceReport($formData);

		    $reportArray = array();
		    if($list){
			    
			    $i=0;
			    $reportArray = $list;
			    foreach($list as $data){
			    	$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();

			    	if($data['IdStudentRegistration']){
			    						$taggingFinancialAid=$studentRegistrationDb->getFinancialAidTagging($data['IdStudentRegistration']);
    					$reportArray[$i]['scholarship'] = isset($taggingFinancialAid['Scholarship'])?$taggingFinancialAid['Scholarship']:'';
	    				$reportArray[$i]['sponsorship'] = isset($taggingFinancialAid['Sponsorship'])?$taggingFinancialAid['Sponsorship']:'';
	    				
			    	}
    				
    				
			    	
			    	//get course status
			    	$statusCourse= null;
			    	
			    	if($data['statusInv'] == 'A'){
			    	
				    	if($data['subject_id'] && $data['IdStudentRegistration']){
					    	$regSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
					    	$courseStatusData = $regSubjectDB->checkCourseStatus($data['IdStudentRegistration'],$data['subject_id'],$data['semester']);
					    	
					    	if(isset($courseStatusData)){
						    	$statusCourse= $this->courseStatus($courseStatusData['Active']);
					    	}
				    	}else{
				    		$statusCourse = 'Active';
				    	}
			    	}elseif($data['statusInv'] == 'W'){
			    		$statusCourse = 'Waived';
			    	}
			    	
			   		$reportArray[$i]['courseStatus'] = $statusCourse;
			    	
			    	
		        	if($reportArray[$i]['scholarship']){ //scholarship
						$reportArray[$i]['funding'] = $reportArray[$i]['scholarship'];
			    	}elseif($data['sponsorship']){ //scholarship
						$reportArray[$i]['funding'] = $reportArray[$i]['sponsorship'];
			    	}else{
			    		$reportArray[$i]['funding'] = 'Self Funding';
			    	}
			    	
			    	//discount
			    	
			    	/*$date = new DateTime('+1 day');
			 		$currentDate = $date->format('Y-m-d');

			 		$invdate =$data['invoice_date'];
			   	 	$disStartDate =$data['dtt_start_date'];
    				$disEndDate =$data['dtt_end_date'];
    				if($invdate >= $disStartDate){
    					if($invdate <= $disEndDate){
    						$reportArray[$i]['discountName']=$data['dt_discount'];
    					}
    				}*/

                    $exrate = $curRateDB->getRateByDate($data['curidmain'], $data['invoice_date']);

                    if ($exrate || isset($exrate['cr_exchange_rate'])){
                        $data['cr_exchange_rate'] = $exrate['cr_exchange_rate'];
                    }

                    $reportArray[$i]['amount'] = $amountCur = str_replace('-', '', $data['amount']);
		    		$amountCur = $amountCur * $data['cr_exchange_rate'];
		    		$reportArray[$i]['amount_convert'] = $amountCur;
		    		
		    		$invMain =  $data['invMainID'];
		    		$invDetail =  $data['idDetail'];
		    		
		    		$searchCN = array(
				    	'date_from'=>$formData['date_from'],
				    	'date_to'=>$formData['date_to'],
				    	'invoice_id'=>$invMain,
				    	'invoice_detail_id'=>$invDetail
			    	);
			    	
			    	$creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
			    	$cnAmount = $creditNoteDB->getSearchDataCN($searchCN);
		    	
//		    		$amountCn = $data['cn_amount'];
		    		$amountCn = isset($cnAmount['cnamount']) ? $cnAmount['cnamount']:0;
		    		if($data['cn_cur_id'] == 2){
		    			$amountCn = $amountCn * $data['cr_exchange_rate'];
		    		}
		    		$reportArray[$i]['cn_amount'] = $amountCn;
		    		
		    		$discountNoteDB = new Studentfinance_Model_DbTable_Discount();
			    	$dnAmount = $discountNoteDB->getSearchDataDN($searchCN);

//		    		$amountDn = $data['dcnt_amount'];
		    		$amountDn = isset($dnAmount['dnamount'])?$dnAmount['dnamount']:0;
		    		
		    		if($data['cur_id'] == 2){
		    			$amountDn = $amountDn * $data['cr_exchange_rate'];
		    		}
		    		$reportArray[$i]['dn_amount'] = $amountDn;
		    		
		    		$discountNoteAdjustDB = new Studentfinance_Model_DbTable_DiscountAdjustment();
			    	$dnaAmount = $discountNoteAdjustDB->getDiscountData($searchCN);
			    	
			    	$amountDna = isset($dnaAmount['dnaamount'])?$dnaAmount['dnaamount']:0;
		    		
			    	if($data['cur_id'] == 2){
		    			$amountDna = $amountDna * $data['cr_exchange_rate'];
		    		}
		    		$reportArray[$i]['dn_amount_adjust'] = $amountDna;
		    		
		    		$nettAdjustAmount = $amountCur - $amountCn;
		    		$reportArray[$i]['nettadjust'] = $nettAdjustAmount;
		    		
		    		$nettAmount = $amountCur - $amountCn - $amountDn + $amountDna;
		    		$reportArray[$i]['nett'] = $nettAmount;
		    		
		    		if($amountDn){
		    			$reportArray[$i]['discountName'] = isset($taggingFinancialAid['Discount'])?$taggingFinancialAid['Discount']:'';
		    		}
		    		
		    		$cnType = '';
		    		
		    		if($amountCn){
                        $checkAwvr = $receiptDB->checkAbsentWithValidReason($invMain);

                        if ($checkAwvr){
                            $cnType = 'Absent with valid reason';
                        }else{
                            if ($data['subject_id']) {
                                if ($data['cn_type'] != 0) {
                                    $cnType = 'Withdrawal ' . $data['type_amount'] . '%';
                                } else {
                                    if ($amountCur == $amountCn) {
                                        $cnType = 'Withdrawal 100%';
                                    } else {
                                        if ($amountCn != 0) {
                                            $percent = number_format(($amountCn / $amountCur) * 100, 0);
                                            $cnType = 'Withdrawal ' . $percent . '%';
                                        }
                                    }
                                }
                            } else {
                                $cnType = 'Withdrawal';
                            }
                        }
		    		}
                	
                	$reportArray[$i]['cnType'] = $cnType;
			    	
			    	$i++;
			    }
		    }
		    
		    //echo '<pre>';
        	//	print_r($reportArray); exit;
		    $this->view->list = $reportArray;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($reportArray));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;
				
				//echo '<pre>';
        		//print_r($student_list);
        		//exit;
         }
            
        
       
    }
    
public function printBillingReportAction(){
	
	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
            //dd($formData); //exit;
			//$formData['intake']=null;
			//$formData['semester']=null;

            $formData['semester_id']=null;
            $formData['intake_id']=null;
     		$receiptDB = new Studentfinance_Model_DbTable_InvoiceMain();		
		    $list = $receiptDB->getInvoiceReport($formData);
            $reportArray = array();
            //dd($list); exit;
            if($list){

                $i=0;
                $reportArray = $list;
                foreach($list as $data){
                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();

                    if($data['IdStudentRegistration']){
                        $taggingFinancialAid=$studentRegistrationDb->getFinancialAidTagging($data['IdStudentRegistration']);
                        $reportArray[$i]['scholarship'] = isset($taggingFinancialAid['Scholarship'])?$taggingFinancialAid['Scholarship']:'';
                        $reportArray[$i]['sponsorship'] = isset($taggingFinancialAid['Sponsorship'])?$taggingFinancialAid['Sponsorship']:'';

                    }



                    //get course status
                    $statusCourse= null;

                    if($data['statusInv'] == 'A'){

                        if($data['subject_id'] && $data['IdStudentRegistration']){
                            $regSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
                            $courseStatusData = $regSubjectDB->checkCourseStatus($data['IdStudentRegistration'],$data['subject_id'],$data['semester']);

                            if(isset($courseStatusData)){
                                $statusCourse= $this->courseStatus($courseStatusData['Active']);
                            }
                        }else{
                            $statusCourse = 'Active';
                        }
                    }elseif($data['statusInv'] == 'W'){
                        $statusCourse = 'Waived';
                    }

                    $reportArray[$i]['courseStatus'] = $statusCourse;


                    if($reportArray[$i]['scholarship']){ //scholarship
                        $reportArray[$i]['funding'] = $reportArray[$i]['scholarship'];
                    }elseif($data['sponsorship']){ //scholarship
                        $reportArray[$i]['funding'] = $reportArray[$i]['sponsorship'];
                    }else{
                        $reportArray[$i]['funding'] = 'Self Funding';
                    }

                    //discount

                    /*$date = new DateTime('+1 day');
                     $currentDate = $date->format('Y-m-d');

                     $invdate =$data['invoice_date'];
                        $disStartDate =$data['dtt_start_date'];
                    $disEndDate =$data['dtt_end_date'];
                    if($invdate >= $disStartDate){
                        if($invdate <= $disEndDate){
                            $reportArray[$i]['discountName']=$data['dt_discount'];
                        }
                    }*/

                    $exrate = $curRateDB->getRateByDate($data['curidmain'], $data['invoice_date']);

                    if ($exrate || isset($exrate['cr_exchange_rate'])){
                        $data['cr_exchange_rate'] = $exrate['cr_exchange_rate'];
                    }

                    $reportArray[$i]['amount'] = $amountCur = str_replace('-', '', $data['amount']);
                    $amountCur = $amountCur * $data['cr_exchange_rate'];
                    $reportArray[$i]['amount_convert'] = $amountCur;

                    $invMain =  $data['invMainID'];
                    $invDetail =  $data['idDetail'];

                    $searchCN = array(
                        'date_from'=>$formData['date_from'],
                        'date_to'=>$formData['date_to'],
                        'invoice_id'=>$invMain,
                        'invoice_detail_id'=>$invDetail
                    );

                    $creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
                    $cnAmount = $creditNoteDB->getSearchDataCN($searchCN);

//		    		$amountCn = $data['cn_amount'];
                    $amountCn = isset($cnAmount['cnamount']) ? $cnAmount['cnamount']:0;
                    if($data['cn_cur_id'] == 2){
                        $amountCn = $amountCn * $data['cr_exchange_rate'];
                    }
                    $reportArray[$i]['cn_amount'] = $amountCn;

                    $discountNoteDB = new Studentfinance_Model_DbTable_Discount();
                    $dnAmount = $discountNoteDB->getSearchDataDN($searchCN);

//		    		$amountDn = $data['dcnt_amount'];
                    $amountDn = isset($dnAmount['dnamount'])?$dnAmount['dnamount']:0;

                    if($data['cur_id'] == 2){
                        $amountDn = $amountDn * $data['cr_exchange_rate'];
                    }
                    $reportArray[$i]['dn_amount'] = $amountDn;

                    $discountNoteAdjustDB = new Studentfinance_Model_DbTable_DiscountAdjustment();
                    $dnaAmount = $discountNoteAdjustDB->getDiscountData($searchCN);

                    $amountDna = isset($dnaAmount['dnaamount'])?$dnaAmount['dnaamount']:0;

                    if($data['cur_id'] == 2){
                        $amountDna = $amountDna * $data['cr_exchange_rate'];
                    }
                    $reportArray[$i]['dn_amount_adjust'] = $amountDna;

                    $nettAdjustAmount = $amountCur - $amountCn;
                    $reportArray[$i]['nettadjust'] = $nettAdjustAmount;

                    $nettAmount = $amountCur - $amountCn - $amountDn + $amountDna;
                    $reportArray[$i]['nett'] = $nettAmount;

                    if($amountDn){
                        $reportArray[$i]['discountName'] = isset($taggingFinancialAid['Discount'])?$taggingFinancialAid['Discount']:$dnAmount['dcnt_description'];
                    }

                    $cnType = '';

                    if($amountCn){
                        $checkAwvr = $receiptDB->checkAbsentWithValidReason($invMain);

                        if ($checkAwvr){
                            $cnType = 'Absent with valid reason';
                        }else{
                            if ($data['subject_id']) {
                                if ($data['cn_type'] != 0) {
                                    $cnType = 'Withdrawal ' . $data['type_amount'] . '%';
                                } else {
                                    if ($amountCur == $amountCn) {
                                        $cnType = 'Withdrawal 100%';
                                    } else {
                                        if ($amountCn != 0) {
                                            $percent = number_format(($amountCn / $amountCur) * 100, 0);
                                            $cnType = 'Withdrawal ' . $percent . '%';
                                        }
                                    }
                                }
                            } else {
                                $cnType = 'Withdrawal';
                            }
                        }
                    }

                    $reportArray[$i]['cnType'] = $cnType;

                    $i++;
                }
            }
			    
		    $this->view->paginator = $reportArray;	
        }
        $this->view->filename = date('Ymd').'_billingreport.xls';
    }
    
	public function getIntakeAction()
	{	
    	$idProgram = $this->_getParam('idProgram', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
					->from(array('a'=>'tbl_intake_program'))
					->join(array('b' => 'tbl_intake'), 'b.IdIntake = a.IdIntake');
					if($idProgram != 'ALL'){
						$select->where('a.IdProgram = ?',$idProgram);
					}
				$select->group('a.IdProgram');
       	$result = $db->fetchAll($select);
         
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
    
    
	public static function courseStatus($val)
	{
		switch($val) 
		{
			case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
			case 6:	$newval = 'Replace';		break;
			case 9: $newval = 'Pre-Repeat';		break;
			case 10:$newval = 'Pre-Replace';	break;
		}

		return $newval;
	}
        
    public function creditNoteListingAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = $this->view->translate('Credit Note Listing');
        
        $model = new Studentfinance_Model_DbTable_CreditNoteListing();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['search'])){
                $form = new Studentfinance_Form_CreditNoteListing(array('programid'=>$formData['program']));
                $form->populate($formData);
                $this->view->form = $form;
                
                $cnList =$model->getCreditNoteListing($formData);
                $this->view->cnList = $cnList;
                //var_dump($cnList); //exit;
            }else{
                $form = new Studentfinance_Form_CreditNoteListing();
                $this->view->form = $form; 
            }
        }else{
            $form = new Studentfinance_Form_CreditNoteListing();
            $this->view->form = $form;
        }
    }
    
    public function printCreditNoteListingAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        //$this->view->role = $this->auth->getIdentity()->IdRole;
        
        $model = new Studentfinance_Model_DbTable_CreditNoteListing();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $cnList =$model->getCreditNoteListing($formData);
            $this->view->cnList = $cnList;
        }
        
        $this->view->filename = date('Ymd').'_credit_note_list.xls';
    }
    
	public function creditNoteUtilizeReportAction(){
        $this->view->title = $this->view->translate('Credit Note Utilization Report');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->program = $formData['program'];
			 $this->view->IdProgramScheme = $formData['IdProgramScheme'];
				 
			$creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();		
		    $list = $creditNoteDB->getSearchDataReport($formData);
		    $this->view->list = $list;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$form->populate($formData);
			$this->view->paginator = $paginator;
				
    	}
	}
    
	public function printCnUtilizeReportAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 	
			$creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();		
		    $list = $creditNoteDB->getSearchDataReport($formData);
			    
		    $this->view->paginator = $list;	
        }
        $this->view->filename = date('Ymd').'_CN_utilizationreport.xls';
    }
    
	public function discountAction(){
        $this->view->title = $this->view->translate('Discount Report');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
       //discount type list
		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
		$this->view->discounttypelist = $discountTypeDb->getData();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
//			 echo "<pre>";
//			 print_r($formData);

//exit;
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->discounttype = $formData['discounttype'];
				 
		    $list = $discountTypeDb->getSearchDataReport($formData);
		    $this->view->list = $list;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;
				
         }
    }
    
 	public function printDiscountReportAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 	
			$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();	
			 $list = $discountTypeDb->getSearchDataReport($formData);
			    
		    $this->view->paginator = $list;	
        }
        $this->view->filename = date('Ymd').'_discountreport.xls';
    }
    
	public function refundAction(){
        $this->view->title = $this->view->translate('Refund Report');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
//			 echo "<pre>";
//			 print_r($formData);

//exit;
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 
			$refundDb = new Studentfinance_Model_DbTable_Refund();	 
		    $list = $refundDb->getSearchDataReport($formData);
		    $this->view->list = $list;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;
				
         }
    }
    
	public function printRefundReportAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 	
			$refundDb = new Studentfinance_Model_DbTable_Refund();	 
		    $list = $refundDb->getSearchDataReport($formData);
			    
		    $this->view->paginator = $list;	
        }
        $this->view->filename = date('Ymd').'_refundreport.xls';
    }
    
	public function discountDetailAction(){
        $this->view->title = $this->view->translate('Discount Detail Report');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
        
       //discount type list
		$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();
		$this->view->discounttypelist = $discountTypeDb->getData();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
//			 echo "<pre>";
//			 print_r($formData);

//exit;
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->discounttype = $formData['discounttype'];
				 
		    $list = $discountTypeDb->getSearchDataDetailReport($formData);
		    $this->view->list = $list;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;
				
         }
    }
    
 	public function printDiscountDetailReportAction(){
    	 $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    	
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 	
			$discountTypeDb = new Studentfinance_Model_DbTable_DiscountType();	
			 $list = $discountTypeDb->getSearchDataReport($formData);
			    
		    $this->view->paginator = $list;	
        }
        $this->view->filename = date('Ymd').'_discountreport.xls';
    }
    
	public function invoiceOutstandingListAction(){
        $this->view->title = $this->view->translate('Invoice Outstanding & -ve Balance');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $formData['semester']=null;
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    
        
        $semesterDb = new GeneralSetup_Model_DbTable_Semester();
		$this->view->semesterData  = $semesterDb->getListSemester();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->semester = $formData['semester'];
				 
			$receiptDB = new Studentfinance_Model_DbTable_InvoiceMain();		
		    $list = $receiptDB->getOutstandingInvoiceReport($formData);
		    $reportArray = array();
		    if($list){
			    
			    $i=0;
			    $reportArray = $list;
			    foreach($list as $data){
			    	
			    	//get course status
			    	$statusCourse= null;
			    	
			    	if($data['statusInv'] == 'A'){
			    	
				    	if($data['subject_id'] && $data['IdStudentRegistration']){
					    	$regSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
					    	$courseStatusData = $regSubjectDB->checkCourseStatus($data['IdStudentRegistration'],$data['subject_id'],$data['semester']);
					    	
					    	if(isset($courseStatusData)){
						    	$statusCourse= $this->courseStatus($courseStatusData['Active']);
					    	}
				    	}
			    	}elseif($data['statusInv'] == 'W'){
			    		$statusCourse = 'Waived';
			    	}
			    	
			   		$reportArray[$i]['courseStatus'] = $statusCourse;
			    	
			    	
		        	if($data['scholarship']){ //scholarship
						$reportArray[$i]['funding'] = $data['scholarship'];
			    	}elseif($data['sponsorship']){ //scholarship
						$reportArray[$i]['funding'] = $data['sponsorship'];
			    	}else{
			    		$reportArray[$i]['funding'] = 'Self Funding';
			    	}
			    	
			    	
		    		$amountCur = $data['amount'];
		    		$amountCur = $amountCur * $data['cr_exchange_rate'];
		    		$reportArray[$i]['amount_convert'] = $amountCur;
		    		
		    		$amountCn = $data['cn_amount'];
		    		if($data['cn_cur_id'] == 2){
		    			$amountCn = $amountCn * $data['cr_exchange_rate'];
		    		}
		    		$reportArray[$i]['cn_amount'] = $amountCn;
		    		
		    		$cnType = '';
		    		
		    		if($amountCn){
		    			if($data['subject_id']){
		                	if($data['cn_type'] != 0){
		                		$cnType =  'Withdrawal '.$data['type_amount'].'%';
		                	}else{
			                	 if ($amountCur==$amountCn){
			                        $cnType = 'Withdrawal 100%';
			                    }else{
			                    	if($amountCn != 0){
				                        $percent = number_format(($amountCn/$amountCur) * 100, 0);
				                        $cnType =  'Withdrawal '.$percent.'%';
			                    	}
			                    }
		                	}
		    			}else{
		    				$cnType =  'Withdrawal';
		    			}
		    		}
                	
                	$reportArray[$i]['cnType'] = $cnType;
			    	
			    	$i++;
			    }
		    }
		    
//		    echo '<pre>';
//        		print_r($reportArray);
		    $this->view->list = $reportArray;
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($reportArray));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;
				
				//echo '<pre>';
        		//print_r($student_list);
        		//exit;
         }
       
    }
    
	public function listStudentSponsorAction(){
        $this->view->title = $this->view->translate('List of Sponsorship/Scholarship Students');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $formData['semester']=null;
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
        //intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();	
        $this->view->intakeData = $intakeDb->fngetallIntakelist();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			 $this->view->typeinfo = $formData['typeinfo'];
			 $this->view->sponsortype = $formData['sponsortype'];
			 $this->view->name = $formData['name'];
			 $this->view->id = $formData['id'];
			 
			 $typeinfo = $formData['typeinfo'];
			 $sponsortype =  $formData['sponsortype'];
			 $name = $formData['name'];
			 $id = $formData['id'];
			 
    		$sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
    		$scholarDb = new Studentfinance_Model_DbTable_Scholarship();
    		$scholarTaggingDB = new Studentfinance_Model_DbTable_ScholarshipStudentTag();
    		
    		if($typeinfo == 1){
	    		$feeItem = $sponsorDb->getFeeInfo($sponsortype);
	    		$student_list = $sponsorDb->fngettagstudentById($sponsortype,0,$id,$name);
	    		$infodetail = $sponsorDb->fngetsponsorbyid($sponsortype);
    		}elseif($typeinfo == 2){
	    		$feeItem = $scholarDb->getFeeInfo($sponsortype);
    			$student_list = $scholarTaggingDB->getStudentListByType($sponsortype,0,$id,$name);
    			$infodetail = $scholarDb->getScholarshipById($sponsortype);
    		}
    		
    		if($student_list){
	    		$reportArray = array();
	    		$reportArray = $student_list;
	    		foreach($student_list as $key=>$data){
	    			$trans_id = $data['IdStudentRegistration'];
	    			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
		    		$profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
		    		$reportArray[$key]['profile'] = $profile;
	    		}
    		}else{
    			$reportArray = null;
    		}
    		
    		$this->view->fee_item = $feeItem;
		    
//		    echo '<pre>';
//        		print_r($reportArray);
		    $this->view->paginator = $reportArray;
			
			/*$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($reportArray));				
			$paginator->setItemCountPerPage(5000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			$this->view->paginator = $paginator;*/
				
				//echo '<pre>';
        		//print_r($student_list);
        		//exit;
         }
            
        
       
    }

    public function listStudentSponsorPrintAction(){
        $this->_helper->layout->disableLayout();

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $formData['semester']=null;

        if($this->getRequest()->isPost()){

            $formData = $this->getRequest()->getPost();

            $typeinfo = $formData['typeinfo'];
            $sponsortype =  $formData['sponsortype'];
            $name = $formData['name'];
            $id = $formData['id'];

            $sponsorDb = new Studentfinance_Model_DbTable_Sponsor();
            $scholarDb = new Studentfinance_Model_DbTable_Scholarship();
            $scholarTaggingDB = new Studentfinance_Model_DbTable_ScholarshipStudentTag();

            if($typeinfo == 1){
                $feeItem = $sponsorDb->getFeeInfo($sponsortype);
                $student_list = $sponsorDb->fngettagstudentById($sponsortype,0,$id,$name);
                $infodetail = $sponsorDb->fngetsponsorbyid($sponsortype);
            }elseif($typeinfo == 2){
                $feeItem = $scholarDb->getFeeInfo($sponsortype);
                $student_list = $scholarTaggingDB->getStudentListByType($sponsortype,0,$id,$name);
                $infodetail = $scholarDb->getScholarshipById($sponsortype);
            }

            if($student_list){
                $reportArray = array();
                $reportArray = $student_list;
                foreach($student_list as $key=>$data){
                    $trans_id = $data['IdStudentRegistration'];
                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                    $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
                    $reportArray[$key]['profile'] = $profile;
                }
            }else{
                $reportArray = null;
            }
        }

        $this->view->paginator = $reportArray;

        $this->view->filename = date('Ymd').'_list_sponsor_student_report.xls';
    }
    
	public function getProgramBySchemeAction()
	{	
    	$idScheme = $this->_getParam('idScheme', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
					->from(array('a'=>'tbl_program'));
					if($idScheme != 'ALL'){
						$select->where('a.IdScheme = ?',$idScheme);
					}
       	$result = $db->fetchAll($select);
         
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
	public function getSemesterBySchemeAction()
	{	
    	$idScheme = $this->_getParam('idScheme', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
					->from(array('b'=>'tbl_semestermaster'))
                    ->where('b.display = ?', 1)
					->order('b.SemesterMainStartDate desc');
					if($idScheme != 'ALL'){
						$select->where('b.IdScheme = ?',$idScheme);
					}else{
						$select->group('b.SemesterMainCode');
					}
       	$result = $db->fetchAll($select);
         
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
	public function barredListingAction(){
        $this->view->title = $this->view->translate('Barred Listing Report');
        
    	$FinOsTempDb = new Studentfinance_Model_DbTable_FinanceOutstandingTemp();

        $form = new Studentfinance_Form_BarredListing();
        $this->view->form = $form;
	    
	    //release type 
	    $model = new Application_Model_DbTable_Barringrelease();

		if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            if(isset($formData['search'])){
                $form->populate($formData);
                $list = $FinOsTempDb->getData($formData);
            }else{
                $list = $FinOsTempDb->getData();
            }
	    }else{
	    	 
	   	 	$list = $FinOsTempDb->getData();
	    }

        $defModel = new App_Model_General_DbTable_Definationms();

        if (isset($formData['type']) && $formData['type'] != ''){
            $typeList[0] = $defModel->getData($formData['type']);
        }else{
            $typeList = $model->getBarringReleaseType();
        }

        $this->view->type = $typeList;
	    
	    if($list){
	    	$dataArray = array();
	    	$dataArray = $list;
		    foreach($list as $key=>$data){
		    	
		    	foreach($typeList as $m=>$type){
		    		
		    		//get current semester
					$profile = array('ap_prog_scheme'=>$data['IdScheme'],'branch_id'=>$data['IdBranch']);
					$semesterDB = new Registration_Model_DbTable_Semester();

                    if (isset($formData['semester']) && $formData['semester'] != ''){
                        $semester = $semesterDB->getData($formData['semester']);
                    }else {
                        $semester = $semesterDB->getApplicantCurrentSemester($profile);
                    }

                    $dataArray[$key]['semester'] = $semester['IdSemesterMaster'];
                    $dataArray[$key]['semesterName'] = $semester['SemesterMainCode'];

		    		//check release
			    	$barringDB = new Application_Model_DbTable_Barringrelease();
			    	$checkRelease = $barringDB->checkReleaseStudent($data['IdStudentRegistration'],$type['idDefinition'],$semester['IdSemesterMaster']);

			    	$release = 0;
			    	if($checkRelease){
			    		$release = 1;
			    	}
			    	$dataArray[$key]['release'][$m] = $release;
		    	}
		    }
	    }

	    $this->view->list = $dataArray;
		$this->view->paginator = $list;
				
    }
    
public function withdrawalReportAction(){
        $this->view->title = $this->view->translate('Withdrawal Report');
        
        $session = Zend_Registry::get('sis');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
        
        //program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
        
         //scheme
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->view->schemeData = $schemeDb->fngetSchemes();
        
        //semester
        $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
        $this->view->semesterData = $semesterDb->fnGetSemestermasterList();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 echo "<pre>";
			 print_r($formData);

//exit;
			 
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->scheme = $formData['scheme'];
			 $this->view->semester = $formData['semester'];
				 
			$reportDB = new Studentfinance_Model_DbTable_Report();		
			
			$ItemList = $reportDB->getListRegistrationItem($formData['scheme'],$formData['semester']);
			
		    $list = $reportDB->getWithdrawList($formData,1);
		    
		    
		    $reportArray = array();
		    if($list){
		    	
			    $reportArray = $list;
			    $i=0;
			    foreach($list as $data){
			    	
			    	foreach($ItemList as $item){
			    		$reportArray[$i]['item'][$item['item_id']]['item_name'] = $item['item_name'];
			    		
			    		$studentId = $data['IdStudentRegistration'];
			    		$ItemDetail = $reportDB->getWithdrawDetail($data['id'],$studentId,$formData['semester'],$data['subject_id'],$item['item_id'],3,$data['initial']);
			    		$reportArray[$i]['item'][$item['item_id']]['invoice_id'] = $ItemDetail['invoice_id'];
			    		$reportArray[$i]['item'][$item['item_id']]['inv_amount'] = $ItemDetail['amount'];
			    		
			    		
			    		$refund = '';
			    		
			    		$cn_amount = 0;
			    		$percentageWithdraw = 0;
			    		if($ItemDetail['invoice_id']){
			    			
			    			if($ItemDetail['refund'] == NULL || $ItemDetail['refund'] == 0){
				    			$refund = 'No Refund';
				    		}else{
				    		
				    			$creditNoteWithdraw = $reportDB->getCreditNote($studentId,$ItemDetail['invoice_id'],$data['subject_id'],$item['item_id'],$data['initial']);
				    			$cn_amount = isset($creditNoteWithdraw['amount'])?$creditNoteWithdraw['amount']:0;
				    			
				    			$percentageWithdraw = ($cn_amount/$ItemDetail['amount']) *100;
				    			$percentageWithdraw2 = $ItemDetail['refund'];
				    			$reportArray[$i]['item'][$item['item_id']]['withdraw_status2'] = $percentageWithdraw2.'%';
				    			$reportArray[$i]['item'][$item['item_id']]['withdraw_status'] = $percentageWithdraw.'%';
				    		}	
			    		}
			    		
			    		$reportArray[$i]['item'][$item['item_id']]['cn_amount'] = $cn_amount;
			    		
			    		$reportArray[$i]['item'][$item['item_id']]['refund'] = $refund;
			    		
			    		
			    	}
			    	
			    	
			    	
			    	
			    	$i++;
			    }
		    }
		    
		   echo '<pre>';
        		print_r($reportArray);
		    
		    /*
         	$reportArray = array();
		    if($list){
			    
			    $i=0;
			    $reportArray = $list;
			    foreach($list as $data){
			    	
				    if($data['scholarship']){ //scholarship
						$reportArray[$i]['funding'] = $data['scholarship'];
			    	}elseif($data['sponsorship']){ //scholarship
						$reportArray[$i]['funding'] = $data['sponsorship'];
			    	}else{
			    		$reportArray[$i]['funding'] = 'Self Funding';
			    	}
			    	
			    	/*if(!isset($data['rcp_inv_invoice_id'])){
			    		unset($reportArray[$i]);
			    	}
			    
			    	$i++;
			    }
		    }
		    */
			$this->view->list = $reportArray;
			$this->view->itemlist = $ItemList;
			
				
//				echo '<pre>';
//        		print_r($reportArray);
        		//exit;
         }
            
        
       
    }
    
	public function getSemesterByProgramAction()
	{	
    	$idProgram = $this->_getParam('idProgram', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
					->from(array('b'=>'tbl_program'))
					->join(array('a'=>'tbl_semestermaster'), 'a.IdScheme = b.IdScheme')
                    ->where('b.IdProgram = ?', $idProgram);
					
       	$result = $db->fetchAll($select);
         
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }

    public function sponsorPaymentReportAction(){
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->view->title = $this->view->translate('Sponsor Payment Report');

        $model = new Studentfinance_Model_DbTable_SponsorPaymentReport();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $form = new Studentfinance_Form_SponsorPaymentSearch(array('programid'=>$formData['Program'], 'stypeid'=>$formData['stype']));
                $this->view->form = $form;

                $list = $model->getSponsorPayment($formData);
                //dd($list); exit;
                $this->view->list = $list;
                $this->view->formData = $formData;

                $form->populate($formData);
            }else{
                $form = new Studentfinance_Form_SponsorPaymentSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Studentfinance_Form_SponsorPaymentSearch();
            $this->view->form = $form;
        }
    }

    public function sponsorPaymentReportPrintAction(){
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $model = new Studentfinance_Model_DbTable_SponsorPaymentReport();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $model->getSponsorPayment($formData);
            $this->view->list = $list;
            $this->view->formData = $formData;
        }

        $this->view->filename = date('Ymd').'_sponsor_payment_report.xls';
    }

    public function sponsorAdvanceReportAction(){
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->view->title = $this->view->translate('Sponsor Payment Advance Payment Report');

        $model = new Studentfinance_Model_DbTable_SponsorPaymentReport();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])) {
                $form = new Studentfinance_Form_SponsorPaymentSearch(array('programid'=>$formData['Program'], 'stypeid'=>$formData['stype']));
                $this->view->form = $form;

                $list = $model->getSponsorAdvancePayment($formData, 0);
                //dd($list); exit;
                $this->view->list = $list;
                $this->view->formData = $formData;

                $form->populate($formData);
            }else{
                $form = new Studentfinance_Form_SponsorPaymentSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Studentfinance_Form_SponsorPaymentSearch();
            $this->view->form = $form;
        }
    }

    public function sponsorAdvanceReportPrintAction(){
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $model = new Studentfinance_Model_DbTable_SponsorPaymentReport();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $model->getSponsorAdvancePayment($formData, 0);
            $this->view->list = $list;
            $this->view->formData = $formData;
        }

        $this->view->filename = date('Ymd').'_sponsor_advance_payment_report.xls';
    }
}
?>