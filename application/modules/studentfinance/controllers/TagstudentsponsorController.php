<?php
class Studentfinance_TagstudentsponsorController extends Base_Base { //Controller for the User Module

	private $lobjdeftype;
	private $_gobjlog;
	private $lobjPolicySetup;
	public $lobjform;


	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();

	}

	public function fnsetObj(){
			
		$this->lobjSponsorSetupForm = new Studentfinance_Form_Sponsorsetup();
		$this->lobjSponsorSetup = new Studentfinance_Model_DbTable_Sponsor();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
		$this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();
			
	}

	public function indexAction()
	{
		$this->view->title = 'Student Sponsor Tagging';

		//Form
		$this->view->lobjform = $this->lobjform;

		$larrresult = $this->lobjSponsorSetup->fnSearchTagStudentSponsor( $post = NULL);
		//echo "<pre>";
		//print_r($larrresult);
		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->TagStudentpaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		//Sponsor
		$larrsponsor = $this->lobjSponsorSetup->fngetsponsor();
		foreach($larrsponsor as $larrvalues) {
			$this->view->lobjform->field5->addMultiOption($larrvalues['idsponsor'],$larrvalues['fName'].' '.$larrvalues['lName']);
		}

		if(isset($this->gobjsessionsis->TagStudentpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->TagStudentpaginatorresult,$lintpage,$lintpagecount);
		}
		else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
			$larrformData = $this->_request->getPost ();

			$larrresult = $this->lobjSponsorSetup->fnSearchTagStudentSponsor($larrformData);
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->TagStudentpaginatorresult = $larrresult;

		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/studentfinance/tagstudentsponsor/');
		}


	}

	public function addAction()
	{
		$this->view->title = 'Student Sponsor Tagging';

		//Form
		$this->view->lobjSponsorSetupForm = $this->lobjSponsorSetupForm;
		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
		$IdUniversity = $this->gobjsessionsis->idUniversity;

		//Render to View
		$this->view->lobjSponsorSetupForm->IdUniversity->setvalue ($IdUniversity);
		$this->view->lobjSponsorSetupForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjSponsorSetupForm->UpdUser->setValue ($userId);

		//Sponsor
		$larrsponsor = $this->lobjSponsorSetup->fngetsponsor();
		foreach($larrsponsor as $larrvalues) {
			$this->view->lobjSponsorSetupForm->Sponsor->addMultiOption($larrvalues['idsponsor'],$larrvalues['fName'].' '.$larrvalues['lName']);
		}

		//JS
		$this->view->lobjSponsorSetupForm->Sponsor->setAttrib('OnChange','showsponsorid();');
		$this->view->lobjSponsorSetupForm->SearchButton->setAttrib('OnClick','showstudentname();');
		$this->view->lobjSponsorSetupForm->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjSponsorSetupForm->Clear->setAttrib('OnClick','clearpageAdd();');
		//$this->view->lobjSponsorSetupForm->Save->setAttrib('OnClick','checkstudentid();');

		$this->view->lobjSponsorSetupForm->SponsorId->setAttrib('readonly','true');
		$this->view->lobjSponsorSetupForm->StudentName->setAttrib('readonly','true');

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			$this->lobjSponsorSetup->fnaddTagStudentSponsorSetup($larrformData);
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Add tag student sponsor' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			$this->_redirect( $this->baseUrl . '/studentfinance/tagstudentsponsor/');
		}
	}


	public function getsponsornameAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$sponsorid =  $this->_getParam ( 'sponsorid' );
		$larrsponsorid = $this->lobjSponsorSetup->fngetsponsorid($sponsorid);
		echo $larrsponsorid;
	}

	public function getstudentnameAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lstrstudentname=  $this->_getParam ( 'studentid' );
		$larrstudentname = $this->lobjSponsorSetup->fngetstudentname($lstrstudentname);
		echo Zend_Json_Encoder::encode($larrstudentname);
	}

	public function editAction()
	{
		$this->view->title = 'Edit Student Sponsor Tagging';

		//Form
		$this->view->lobjSponsorSetupForm = $this->lobjSponsorSetupForm;

		$id = $this->_getParam ( 'id' );
		$lstrsponsorid = $this->lobjSponsorSetup->fngetsponsorid($id);
		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
                //var_dump($auth->getIdentity()); exit;
                $this->view->IdRole = $auth->getIdentity()->IdRole;
                
		$UpdDate = date('Y-m-d H:i:s');
		$lIntIdUniversity = $this->gobjsessionsis->idUniversity;

		//Sponsor
		$larrsponsor = $this->lobjSponsorSetup->fngetsponsor();
		foreach($larrsponsor as $larrvalues) {
			$this->view->lobjSponsorSetupForm->Sponsor->addMultiOption($larrvalues['idsponsor'],$larrvalues['fName'].' '.$larrvalues['lName']);
		}

		//Fee Item
		$feeList = $this->lobjFeeItem->getData();
		$this->lobjSponsorSetupForm->FeeItem->addMultiOption('',$this->view->translate('None'));
		foreach( $feeList as $item)
		{
			$this->lobjSponsorSetupForm->FeeItem->addMultiOption($item['fi_id'],$item['fi_name']);
		}

		//Render to View
		$this->view->lobjSponsorSetupForm->IdUniversity->setvalue ($lIntIdUniversity);
		$this->view->lobjSponsorSetupForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjSponsorSetupForm->UpdUser->setValue ($userId);

		//JS
		$this->view->lobjSponsorSetupForm->Sponsor->setAttrib('OnChange','showsponsorid();');
		$this->view->lobjSponsorSetupForm->SearchButton->setAttrib('OnClick','showstudentname();');
		$this->view->lobjSponsorSetupForm->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjSponsorSetupForm->Clear->setAttrib('OnClick','clearpageAdd();');


		$result = $this->lobjSponsorSetup->fngettagstudentById($id);
		$this->view->result = $result;

		$this->lobjSponsorSetupForm->populate($result);

		$this->view->lobjSponsorSetupForm->Sponsor->setValue($id);
		$this->view->lobjSponsorSetupForm->Sponsor->setAttrib('readonly','true');

		$this->view->lobjSponsorSetupForm->SponsorId->setValue($lstrsponsorid);
		$this->view->lobjSponsorSetupForm->SponsorId->setAttrib('readonly','true');

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
                        //var_dump($larrformData); exit;
                        
                        if (isset($larrformData['deleteColVal']) && count($larrformData['deleteColVal']) > 0){
                            foreach ($larrformData['deleteColVal'] as $delLoop){
                                $this->lobjSponsorSetup->deleteStudentTag($delLoop);
                            }
                        }
                        
			$this->lobjSponsorSetup->fnupdateTagStudentSponsorSetup($larrformData,$id);
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Edit tag student sponsor' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			$this->_redirect( $this->baseUrl . '/studentfinance/tagstudentsponsor/');
		}
	}
	
	public function viewstudentAction()
	{
		$this->view->title = 'Edit Student Sponsor Tagging Detail';

		$id = $this->_getParam('id');
		$info = $this->lobjSponsorSetup->getTagStudent($id);
		
		$data = array(
						'StudAgreementNo'	=> $info['AggrementNo'],
						'StudStartDate'		=> $info['StartDate'],
						'StudEndDate'		=> $info['EndDate'],
						'StudAmount'		=> $info['Amount'],
						'StudFeeItem'		=> $info['FeeItem']
					);

		//Fee Item
		$feeList = $this->lobjFeeItem->getData();
		$this->lobjSponsorSetupForm->StudFeeItem->addMultiOption('',$this->view->translate('None'));
		foreach( $feeList as $item)
		{
			$this->lobjSponsorSetupForm->StudFeeItem->addMultiOption($item['fi_id'],$item['fi_name']);
		}
                
                if ($data['StudEndDate']=='0000-00-00'){
                    unset($data['StudEndDate']);
                }
                
		//populate
		$this->lobjSponsorSetupForm->populate($data);



		$this->view->sponsor_id = $info['Sponsor'];

		$this->view->lobjSponsorSetupForm = $this->lobjSponsorSetupForm;

		if ($this->_request->isPost () && $this->_request->getPost ( 'Submit' )) 
		{	
			$formData = $this->_request->getPost();
			
			$data = array(
						'AggrementNo'	=> $formData['StudAggrementNo'],
						'StartDate'		=> $formData['StudStartDate'],
						'EndDate'		=> $formData['StudEndDate'],
						'Amount'		=> $formData['StudAmount'],
						'FeeItem'		=> $formData['StudFeeItem']
					);
			
			
			$this->lobjSponsorSetup->updateTagStudent($data, $id);

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/studentfinance/tagstudentsponsor/edit/id/'.$info['Sponsor']);
		}
			
	}
}