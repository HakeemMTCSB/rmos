<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_FeeItemController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_FeeItem();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Fee Set-up");
    	
    	$this->view->searchForm = new Studentfinance_Form_FeeItem();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//paginator
			$data = $this->_DbObj->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->paginator = $paginator;
		
    	}else{
    		//paginator
			$data = $this->_DbObj->getPaginateData();
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->paginator = $paginator;	
    	}
    	    	
	}
	
	public function addAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Fee Set-up")." - ".$this->view->translate("Add");
    	    	
    	$form = new Studentfinance_Form_FeeItem();
    	
    	$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true)."'; return false;";
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$codeId = $this->_DbObj->addData($formData);
				$this->_DbObj->addHistory($formData,$codeId);
				
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new fee');
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true));	
			}else{
				$form->populate($formData);
			}
        	
        }
    	
        $this->view->form = $form;
    }
    
	public function editAction(){
		$id = $this->_getParam('id', null);
		
		if($id==null){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true));
		}
		
		//title
    	$this->view->title= $this->view->translate("Fee Set-up")." - ".$this->view->translate("Edit");
    	
    	$form = new Studentfinance_Form_FeeItem();
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
	    	if ($form->isValid($formData)) {
				
				$this->_DbObj->updateData($formData,$id);
				$this->_DbObj->addHistory($formData,$id);
				
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update');
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true)); 
			}else{
				$form->populate($formData);	
			}
    	}else{
    		if($id!=null){
    			
    			$form->populate($this->_DbObj->getData($id));
    		}
    	}
    	
    	$this->view->form = $form;
    }
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	
    	if($id!=0){
    		$this->_DbObj->deleteData($id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true));
    	
    }
    
    public function countryAmountAction(){
    	$id = $this->_getParam('id', 0);
    	
    	if($id == 0){
    		$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-item', 'action'=>'index'),'default',true));
    	}
    	
    	$this->view->title = "Fee Set-up - Amount by Country";
    	
    	$fee_item =  $this->_DbObj->getData($id);
    	$this->view->fee_item = $fee_item;
    	
    	//currency
    	$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$this->view->currency = $currencyDb->fetchAll()->toArray();
    	
    	//fee by country
    	$feeItemCountryDb = new Studentfinance_Model_DbTable_FeeItemCountry();
    	
    	$fees = $feeItemCountryDb->getCountryFeeItem(false,null,$id);
    	$this->view->fees = $fees;
    	
    }
    
    public function changeCountryAmountAction(){
    	
    	$id = $this->_getParam('cid', 0);
    	$fid = $this->_getParam('fid', 0);
    	$this->view->fid = $fid;
    	
    	$this->_helper->layout->disableLayout();
    	$feeItemCountryDb = new Studentfinance_Model_DbTable_FeeItemCountry();
    	
    	//currency
    	$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$currency = $currencyDb->fetchAll()->toArray();
    	$this->view->currency = $currency;
    	
    	if ($this->getRequest()->isPost()) {
    			
    		$formData = $this->getRequest()->getPost();
    		
    		
    		$result = array('status'=>0);
    		
    		foreach ($currency as $cur){
    			
    			//check if already having data
    			$where = array(
	    			'fic_fi_id = ? ' => $formData['fic_fi_id'],
	    			'fic_idCountry = ?' => $formData['fic_idCountry'],
    				'fic_cur_id = ?' => $cur['cur_id']	
    				
	    		);
    			
    			$cur_c_item = $feeItemCountryDb->fetchRow($where);
    			
    			if($cur_c_item==true){
    				
    				$data = array(
    					'fic_amount' => $formData['cur_'.$cur['cur_id']],
    					'fic_year' => $formData['fic_year']
    				);
    				
    				$feeItemCountryDb->update($data, 'fic_id = '.$cur_c_item['fic_id']);
    				
    				$result['status'] = 1;
    			}else{
    				
    				$data = array(
    						'fic_fi_id' => $formData['fic_fi_id'],
    						'fic_idCountry' => $formData['fic_idCountry'],
    						'fic_cur_id' => $cur['cur_id'],
    						'fic_amount' => $formData['cur_'.$cur['cur_id']],
    						'fic_year' => $formData['fic_year']
    				);
    				
    				$feeItemCountryDb->insert($data);
    				
    				$result['status'] = 1;
    				
    			}
    		}
    		
    		$json = Zend_Json::encode($result);
    		echo $json;
    		exit;
    	}
    	

    	$fees = $feeItemCountryDb->getCountryFeeItem(false, $id, $fid);
    	$this->view->fees = $fees;

    	
    }
    
	public function stateAmountAction(){
    	$id = $this->_getParam('idCountry', 0);
    	$fid = $this->_getParam('id', 0);
    	
    	$this->view->title = "Fee Set-up - Amount by State";
    	
    	$fee_item =  $this->_DbObj->getData($fid);
    	$this->view->fee_item = $fee_item;
    	
    	$this->view->idCountry = $id;
    	$this->view->fid = $fid;
    	
    	//currency
    	$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$this->view->currency = $currencyDb->fetchAll()->toArray();
    	
    	//fee by state
    	$feeItemStateDb = new Studentfinance_Model_DbTable_FeeItemState();
    	
    	$fees = $feeItemStateDb->getCountryStateFeeItem(false,$id,$fid);
    	$this->view->fees = $fees;

    }
    
public function changeStateAmountAction(){
    	
    	$id = $this->_getParam('cid', 0);
    	$fid = $this->_getParam('fid', 0);
    	$sid = $this->_getParam('sid', 0);
    	$this->view->fid = $fid;
    	$this->view->cid = $id;
    	$this->view->sid = $sid;
    	
    	$this->_helper->layout->disableLayout();
    	$feeItemStateDb = new Studentfinance_Model_DbTable_FeeItemState();
    	
    	//currency
    	$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$currency = $currencyDb->fetchAll()->toArray();
    	$this->view->currency = $currency;
    	
    	if ($this->getRequest()->isPost()) {
    			
    		$formData = $this->getRequest()->getPost();
    		
    		
    		$result = array('status'=>0);
    		
    		foreach ($currency as $cur){
    			
    			//check if already having data
    			$where = array(
	    			'fic_fi_id = ? ' => $formData['fic_fi_id'],
	    			'fic_idCountry = ?' => $formData['fic_idCountry'],
    				'fic_idState = ?' => $formData['fic_idState'],
    				'fic_cur_id = ?' => $cur['cur_id']	
	    		);
    			
    			$cur_c_item = $feeItemStateDb->fetchRow($where);
    			
    			if($cur_c_item==true){
    				
    				
    				$data = array(
    					'fic_amount' => $formData['cur_'.$cur['cur_id']]
    				);
    				
    				$feeItemStateDb->update($data, 'fis_id = '.$cur_c_item['fis_id']);
    				
    				$result['status'] = 1;
    			}else{
    				
    				$data = array(
    						'fic_fi_id' => $formData['fic_fi_id'],
    						'fic_idCountry' => $formData['fic_idCountry'],
    						'fic_idState' => $formData['fic_idState'],
    						'fic_cur_id' => $cur['cur_id'],
    						'fic_amount' => $formData['cur_'.$cur['cur_id']]
    				);
    				
    				$feeItemStateDb->insert($data);
    				
    				$result['status'] = 1;
    				
    			}
    		}
    		
    		$json = Zend_Json::encode($result);
    		echo $json;
    		exit;
    	}
    	

    	$fees = $feeItemStateDb->getCountryStateFeeItem(false, $id, $fid, $sid);
    	$this->view->fees = $fees;

    	
    }
    
}

