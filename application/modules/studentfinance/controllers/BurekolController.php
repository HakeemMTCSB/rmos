<?php
/**
 *  @author alif 
 *  @date Aug 25, 2013
 */
 
class Studentfinance_BurekolController extends Base_Base {

	private $_DbObj;
	
	public function init(){
		//$db = new Studentfinance_Model_DbTable_Burekol();
		//$this->_DbObj = $db;
	}
	
	public function indexAction() {
		$verified = $this->_getParam('verified', 0);
		$this->view->verified = $verified;
		
		$intake_id = $this->_getParam('intake', null);
		
		$faculty_id = $this->_getParam('faculty', 0);
		$this->view->faculty_id = $faculty_id;
		
		
		//title
		$this->view->title= $this->view->translate("Buka Rekod Kolektif");
		 
		$msg = $this->_getParam('msg', null);
		if( $msg!=null ){
			$this->view->noticeMessage = $msg;
		}
		
		//Intake
		$intakeDB = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDB->getData();
		$this->view->intakeList = $intakeList;
		 
		if($intake_id){
			$this->view->intake = $intake_id;
		}else{
			$current_intake = $intakeDB->getCurrentIntake();
			
			if( $current_intake ){
				$this->view->intake = $current_intake['IdIntake'];
			}else{
				 $prev_intake = $intakeDB->getPreviousIntake();
				 $this->view->intake = $prev_intake['IdIntake'];
			}
			
		}
				
		//Faculty
		$collegeDb = new App_Model_General_DbTable_Collegemaster();
		$facultyList = $collegeDb->getFaculty();
		$this->view->facultyList = $facultyList;
		
		/*Registration data*/
		$session = new Zend_Session_Namespace('sis');
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))
		->joinLeft(array('sp'=>'student_profile'),'sp.appl_id=sa.IdApplication',array('burekol_verify_date','appl_fname','appl_mname','appl_lname'))
		->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=sa.Status', array('deftn.DefinitionCode'))
		->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('prg.ArabicName','ProgramName'))
		->joinLeft(array('fac' => 'tbl_collegemaster'), 'fac.IdCollege = prg.IdCollege', array('faculty_name'=>'ShortName'))
		->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sa.IdIntake', array('intk.IntakeId'))
		->where("sa.OldIdStudentRegistration IS NULL")
		->where("sa.IdIntake = ?",$this->view->intake)
		->order("sa.registrationId ASC");
		
		if($faculty_id!=0){
			$lstrSelect->where('prg.IdCollege = ? ', $faculty_id);
		}
		
		if($verified==1){
			$lstrSelect->where('sp.burekol_verify_date is not null');
		}else
		if($verified==2){
			$lstrSelect->where('sp.burekol_verify_date is null');
		}
		
		$lstrSelect->group('sp.appl_id');
		
		$result = $db->fetchAll($lstrSelect);
		$this->view->studentList = $result;
		
	}
	
	public function exportExcelAction(){
		
		$verified = $this->_getParam('verified', 0);
		$intake_id = $this->_getParam('intake', null);
		$faculty_id = $this->_getParam('faculty', 0);
				
		$intakeDB = new App_Model_Record_DbTable_Intake();
		$intake = $intakeDB->getData($intake_id);
		
		$familyDb = new App_Model_Application_DbTable_ApplicantFamily();
		
		/*Registration data*/
		$session = new Zend_Session_Namespace('sis');
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))
		->joinLeft(array('p'=>'student_profile'),'p.appl_id=sa.IdApplication')
		->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=sa.Status', array('deftn.DefinitionCode'))
		->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram')
		->joinLeft(array('col' => 'tbl_collegemaster'), 'col.IdCollege=prg.IdCollege',array('collegeName'=>'col.ArabicName','collegeShortcode'=>'col.ShortName'))
		->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sa.IdIntake', array('intk.IntakeId'))
		->where("sa.OldIdStudentRegistration IS NULL")
		->where("sa.IdIntake = ?",$intake_id)
		->order("sa.registrationId ASC");
		
		if($faculty_id!=0){
			$lstrSelect->where('col.IdCollege = ? ', $faculty_id);
		}
		
		if($verified==1){
			$lstrSelect->where('p.burekol_verify_date is not null');
		}else
		if($verified==2){
			$lstrSelect->where('p.burekol_verify_date is null');
		}
		
		$lstrSelect->group('p.appl_id');
				
		$data_arr = $db->fetchAll($lstrSelect);
				
		$filename = "BUREKOL_".date('dmyhis');

		$file = APPLICATION_PATH . "/tmp/" . $filename . ".csv";
 
	        $realPath = realpath( $file );
	 
	        if ( false === $realPath )
	        {
	            touch( $file );
	            chmod( $file, 0777 );
	        }
	 
	        $file = realpath( $file );
	        $handle = fopen( $file, "w" );
	        
	        $finalData[] = array(
	        			'NAMA_INSTITUSI',
	        			'NO_INDUK',
	        			'KODE_PANGGILAN',
	        			'NAMA_DEPAN',
	        			'NAMA_TENGAH',
	        			'NAMA_BELAKANG',
	        			'JENIS_NASABAH',
	        			'MEMILIKI_NPWP',
	        			'NO_NPWP',
	        			'WARGA_NEGARA',
	        			'BAHASA',
	        			'DOMISILI',
	        			'KODE_AGAMA',
	        			'TANGGAL_LAHIR',
	        			'TEMPAT_LAHIR',
	        			'JENIS_KELAMIN',
	        			'STATUS_PERKAWINAN',
	        			'NAMA_GADIS',
	        			'JENIS_PEKERJAAN',
	        			'JENIS_PENDIDIKAN',
	        			'JENIS_IDENTITAS',
	        			'NO_IDENTITAS',
	        			'LOKASI_PENERBITAN_ID',
	        			'TANGGAL_EXPIRE_ID',
	        			'ALAMAT_JALAN',
	        			'ALAMAT_RT_PERUM',
	        			'ALAMAT_KELURAHAN',
	        			'ALAMAT_KECAMATAN',
	        			'KODE_POS',
	        			'NO_TLP_RMH',
	        			'NO_TLP_KNTR',
	        			'NO_TLP_HP',
	        			'NO_FAX',
	        			'ALAMAT_EMAIL',
	        			'ALAMAT_JALAN_TERKINI',
	        			'ALAMAT_RT_PERUM_TERKINI',
	        			'ALAMAT_KELURAHAN_TERKINI',
	        			'ALAMAT_KECAMATAN_TERKINI',
	        			'KODE_POS_TERKINI',
	        			'NO_TLPN_RMH_TERKINI',
	        			'NO_TLPN_HP_TERKINI',
	        			'LOKASI_BI_TERKINI',
	        			'TANGGAL_BERLAKU',
	        			'TANGGAL_JATUH_TEMPO',
	        			'ALASAN',
	        			'SUMBER_DANA',
	        			'NAMA_ALIAS',
	        			'ALMT_PEMBERI_KERJA_1',
	        			'ALMT_PEMBERI_KERJA_2',
	        			'DESKRIPSI_PEKERJAAN',
	        			'PENGHASILAN',
	        			'FREK_PENGHASILAN',
	        			'CIF',
	                    'NAMA_KARTU'
	        			);
	        
	        
	        if($data_arr){
		        foreach ( $data_arr AS $index=>$row )
		        {
		        	
		        	//parent (mother)
		        	$mother_data = $familyDb->getData($row['appl_id'],'21');
		        	
		            $finalData[] = array(
		            	
		            	'UNIVERSITASTRISAKTI',
		                $row['registrationId'],
		                '',
	            		$row['appl_fname'],
	            		$row['appl_mname'],
	            		$row['appl_lname'],
	            		"01",
	            		'N',
	            		'',
	            		'ID',
	            		'4',
	            		'ID',
	            		$row['appl_religion'],
	            		date('d/m/Y',strtotime($row['appl_dob'])),
	            		isset($row['appl_birth_place'])?$row['appl_birth_place']:null,
	            		isset($row['appl_gender'])?$row['appl_gender']:null,
	            		'L',
	            		isset($mother_data) && isset($mother_data['af_name'])?$mother_data['af_name']:"",
	            		"08",
	            		'3',
	            		'1',
	            		$row['registrationId'],
	            		'JAKARTABARAT',
	            		'30/09/2017',
	            		isset($row['appl_address1'])?str_replace(","," ",$row['appl_address1']):"",
	            		isset($row['appl_address_rt'])?$row['appl_address_rt']:"",
	            		isset($row['appl_kelurahan'])?$row['appl_kelurahan']:"",
	            		$row['appl_kecamatan'],
	            		$row['appl_postcode'],
	            		$row['appl_phone_home'],
	            		'',
	            		$row['appl_phone_mobile'],
	            		'',
	            		$row['appl_email'],
	            		isset($row['appl_caddress1'])?str_replace(","," ",$row['appl_caddress1']):"",
	            		$row['appl_caddress_rt'],
	            		$row['appl_ckelurahan'],
	            		$row['appl_ckecamatan'],
	            		$row['appl_cpostcode'],
	            		$row['appl_phone_home'],
	            		$row['appl_phone_mobile'],
	            		'0393',
	            		date('d/m/Y'),
	            		'30/9/2017',
	            		'7',
	            		'4',
	            		'',
	            		'UNIVERSITASTRISAKTI',
	            		$row['collegeShortcode'].$row['ArabicName'],
	            		'MAHASISWA USAKTI '.$row['collegeShortcode'],
	            		'2JTSD5JT',
	            		'M',
	            		"",
		                $row['appl_name_kartu']
		            );
		            
		            /*NULL FILTER*/
		            if($finalData[$index][14]==null || $finalData[$index][14]=="" ){
		            	$finalData[$index][14] = "JAKARTA";
		            }
		            
		            if($finalData[$index][15]==null || $finalData[$index][15]=="" ){
		            	$finalData[$index][15] = "M";
		            }else
		            if($finalData[$index][15]==1){
		            	$finalData[$index][15] = "F";
		            	$finalData[$index][2] = "04";
		            }if($finalData[$index][15]==2){
		            	$finalData[$index][15] = "M";
		            	$finalData[$index][2] = "03";
		            }
		            
		        }
	        }
	        
	 
			if (PHP_EOL == "\r\n"){
			    $eol = "\n";
			}else{
			    $eol = "\r\n";
			}

	        foreach ( $finalData AS $finalRow )
	        {
	            //remove comma
	            foreach($finalRow as $index=>$data){
	            	$finalRow[$index] = str_replace(","," ",$data);
	            }
	            
	            fwrite( $handle, implode(",", $finalRow).$eol);
	            
	        }
	        
	       fclose( $handle );
	 
	        $this->_helper->layout->disableLayout();
	        $this->_helper->viewRenderer->setNoRender();
	 
	        $this->getResponse()->setRawHeader( "Content-Type: application/csv" )
	            ->setRawHeader( "Content-Disposition: attachment; filename=".$filename.".csv" )
	            ->setRawHeader( "Content-Transfer-Encoding: binary" )
	            ->setRawHeader( "Expires: 0" )
	            ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
	            ->setRawHeader( "Pragma: public" )
	            ->setRawHeader( "Content-Length: " . filesize( $file ) )
	            ->sendResponse();
	 
	        readfile( $file ); 
	        unlink($file);
	        
    	exit();
	}
	
	public function importAction(){
		
		set_time_limit(0);
		ini_set('memory_limit','1024M');
		
		//title
		$this->view->title= $this->view->translate("Burekol : Import");
		
		$form = new Studentfinance_Form_UploadBurekol();
		$this->view->form = $form;
		
		//lookup table list
		$sisSetupDetailDb = new App_Model_General_DbTable_SisSetupDetail();
		$religionList = $sisSetupDetailDb->getDataList("RELIGION");
		$this->view->religionList = $religionList;
		
		$maritalList = $sisSetupDetailDb->getDataList("MARITAL");
		$this->view->maritalList = $maritalList;
		
		$cityDb = new App_Model_General_DbTable_City();
		$cityList = $cityDb->getData();
		$this->view->cityList = $cityList;
		
		$stateDb = new App_Model_General_DbTable_State();
		$stateList = $stateDb->getData();
		$this->view->stateList = $stateList;
		
		$countryDb = new App_Model_General_DbTable_Country();
		$countryList = $countryDb->getData();
		$this->view->countryList = $countryList;
		
		
		if ($this->_request->isPost()) {
			
			$formData = $this->_request->getPost();
			
			$header_skip = $formData['remove_header']==1?true:false;
			$this->view->remove_header = $header_skip;
			
		 	// success - do something with the uploaded file
            $uploadedData = $form->getValues();
            $fullFilePath = $form->file->getFileName();
            
            $data_mapped = $this->extractDataFromCsv($fullFilePath,$header_skip,false);
            
            /*echo "<pre>";
            print_r($data_mapped);
            echo "</pre>";*/
            //exit;
           
            $this->view->data = $data_mapped;
		}
		
		
		
	}
	
	public function saveImportAction(){
		
		$auth = Zend_Auth::getInstance();
		
		if ($this->_request->isPost()) {
				
			$formData = $this->_request->getPost();
			
			
			$header_skip = isset($formData['remove_header']) && $formData['remove_header']==1?true:false;
			$preview = isset($formData['preview']) && $formData['preview']==0?false:true;
			
			if(!$preview){// data clean (direct import)

				$form = new Studentfinance_Form_UploadBurekol();
				
				// success - do something with the uploaded file
				$uploadedData = $form->getValues();
				$fullFilePath = $form->file->getFileName();
				
				$data_mapped = $this->extractDataFromCsv($fullFilePath,$header_skip);
				$data = $data_mapped;
				
				foreach ($data as $index=>$data_profile){
					$data[$index]['burekol_verify_by'] = $auth->getIdentity()->id;
					$data[$index]['burekol_verify_date'] = date('Y-m-d H:i:s');
				}
				
			}else{// data from form
				echo "<pre>";
				print_r($formData);
				echo "</pre>";
				exit;
				
				$data = array(
						'appl_fname' => $formData['appl_fname'],
						'appl_mname' => $formData['appl_mname'],
						'appl_lname' => $formData['appl_lname'],
						'appl_name_kartu' => $formData['appl_name_kartu'],
						'appl_dob' => $formData['appl_dob'],
						'appl_birth_place' => $formData['appl_birth_place'],
						'appl_gender' => $formData['appl_gender'],
						'appl_religion' => $formData['appl_religion'],
						'appl_marital_status' => $formData['appl_marital_status'],
						'appl_address_rw' => $formData['appl_address_rw'],
						'appl_address_rt' => $formData['appl_address_rt'],
						'appl_address1' => $formData['appl_address1'],
						'appl_province' => $formData['appl_province'],
						'appl_state' => $formData['appl_state'],
						'appl_city' => $formData['appl_city'],
						'appl_kecamatan' => $formData['appl_kecamatan'],
						'appl_kelurahan' => $formData['appl_kelurahan'],
						'appl_postcode' => $formData['appl_postcode'],
						'appl_caddress_rw' => $formData['appl_caddress_rw'],
						'appl_caddress_rt' => $formData['appl_caddress_rt'],
						'appl_caddress1' => $formData['appl_caddress1'],
						'appl_cprovince' => $formData['appl_cprovince'],
						'appl_cstate' => $formData['appl_cstate'],
						'appl_ccity' => $formData['appl_ccity'],
						'appl_ckecamatan' => $formData['appl_ckecamatan'],
						'appl_ckelurahan' => $formData['appl_ckelurahan'],
						'appl_cpostcode' => $formData['appl_cpostcode'],
						'appl_phone_home' => $formData['appl_phone_home'],
						'appl_phone_mobile' => $formData['appl_phone_mobile'],
						'appl_email' => $formData['appl_email'],
						'burekol_verify_by' => $auth->getIdentity()->id,
						'burekol_verify_date' =>date('Y-m-d H:i:s')
				);
			}
			
			
			/*
			 * update database
			 */
			
			$studentProfileDb = new Records_Model_DbTable_Studentprofile();
			$familyDb = new App_Model_Application_DbTable_ApplicantFamily();
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$update_count = 0;
			$fail_count = 0;
			
			foreach($data as $student){
				
				//get profile id
				$sql = $db->select()
						->from(array('a' => 'tbl_studentregistration'), array('a.*'))
						->join(array('b' => 'student_profile'), 'a.IdApplication = b.appl_id')
						->where('a.registrationId = ?', $student['nim']);
				
				$student_profile = $db->fetchRow($sql);
				
				if($student_profile){
		
					try {
						
						
						//update / insert mother's name
						$mother = $familyDb->getData($student_profile['IdApplication'], 21);
						
						if(isset($student['af_name']) && $student['af_name']!=null){
							if($mother){
								$data = array(
										'af_name' => $student['af_name']
								);
							
								$familyDb->update($data,'af_appl_id = '.$student_profile['IdApplication'].' and af_relation_type = 21');
							}else{
								$data = array(
										'af_name' => $student['af_name'],
										'af_appl_id' => $student_profile['IdApplication'],
										'af_relation_type' => 21,
										'af_address1' => $student['appl_address1'],
										'af_address2' => '',
										//'af_city' => $student['appl_city'],
										'af_postcode' => $student['appl_postcode'],
										//'af_state' => $student['appl_state'],
										'af_phone' => '',
										'af_email' => '',
										'af_job' => ''
								);
							
								$familyDb->insert($data);
							}
						}
						
						//remove nim & mother's name
						unset($student['nim']);
						unset($student['af_name']);
						
						//update
						$studentProfileDb->update($student,'appl_id = '.$student_profile['IdApplication']);
						
						
						$update_count++;
					} catch (Exception $e) {
						echo $student_profile['IdApplication']; 
						echo $e->getMessage();
						
						$fail_count++;
						exit;
					}
					
				}else{
					echo  "error: ".$student['nim']."<br />";
					$fail_count++;
				}
			}

			echo "<br />Total Data : ".count($data);
			echo "<br />Burekol Data updated : ".$update_count;
			echo "<br />Burekol Data Failed : ".$fail_count;
			//$this->_redirect( $this->baseUrl . '/registration/registrationhistory/studenthistorydetails/id/'.$formData['profile_id'].'/#tab-5');
			
		}
		
		exit;
	}
	
	private function extractDataFromCsv($fullFilePath,$header_skip, $direct=true){
		
		$sisSetupDetailDb = new App_Model_General_DbTable_SisSetupDetail();
		$religionList = $sisSetupDetailDb->getDataList("RELIGION");
		
		$arr_religion = array();
		foreach ($religionList as $religion){
			$arr_religion[$religion['ssd_seq']] = $religion['ssd_id'];
		}
		
		$maritalList = $sisSetupDetailDb->getDataList("MARITAL");
		
		$arr_marital = array();
		foreach ($maritalList as $marital){
			$arr_marital[$marital['ssd_seq']] = $marital['ssd_id'];
		}
			
		$data = array();
		$data_mapped = array();
		if (($handle = fopen($fullFilePath, 'r')) !== FALSE)
		{
			$i=0;
			$row_count = 0;
			while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
			{
				$data = $row;
		
				if($header_skip && $row_count==0){
					// do nothing
				}else{
		
					/*
					 * Data Mapping
					*/
					$data_mapped[$i]["nim"] = $data[1];
					$data_mapped[$i]["appl_fname"] = $data[4];
					$data_mapped[$i]["appl_mname"] = $data[5];
					$data_mapped[$i]["appl_lname"] = $data[6];
					$data_mapped[$i]["appl_name_kartu"] = $data[7];
					//$data_mapped[$i]["appl_nationality"] = $data[11];
					$data_mapped[$i]["appl_nationality"] = 1;
					$data_mapped[$i]["appl_religion"] = $data[14];
					$data_mapped[$i]["appl_dob"] = $data[15];
					$data_mapped[$i]["appl_birth_place"] = $data[16];
					$data_mapped[$i]["appl_gender"] = $data[17];
					$data_mapped[$i]["appl_marital_status"] = $data[18];
					 
					$data_mapped[$i]["af_name"] = $data[19];
					 
					$data_mapped[$i]["appl_address_rw"] = $data[29];
					$data_mapped[$i]["appl_address_rt"] = $data[28];
					$data_mapped[$i]["appl_address1"] = $data[26];
					$data_mapped[$i]["appl_province"] = 96;
					$data_mapped[$i]["appl_state"] = null;
					$data_mapped[$i]["appl_city"] = null;
					$data_mapped[$i]["appl_kecamatan"] = $data[31];
					$data_mapped[$i]["appl_kelurahan"] = $data[30];
					$data_mapped[$i]["appl_postcode"] = $data[33];
					 
					$data_mapped[$i]["appl_caddress_rw"] = $data[29];
					$data_mapped[$i]["appl_caddress_rt"] = $data[28];
					$data_mapped[$i]["appl_caddress1"] = $data[26];
					$data_mapped[$i]["appl_cprovince"] = 96;
					$data_mapped[$i]["appl_cstate"] = null;
					$data_mapped[$i]["appl_ccity"] = null;
					$data_mapped[$i]["appl_ckecamatan"] = $data[31];
					$data_mapped[$i]["appl_ckelurahan"] = $data[30];
					$data_mapped[$i]["appl_cpostcode"] = $data[33];
					 
					$data_mapped[$i]["appl_phone_home"] = $data[34];
					$data_mapped[$i]["appl_phone_mobile"] = $data[36];
					$data_mapped[$i]["appl_email"] = $data[38];
					 
					 
					/*
					 * Data Cleaning
					*/
					if( $data_mapped[$i]["appl_gender"]=='M' ){
						$data_mapped[$i]["appl_gender"] = 1;
					}else
					if( $data_mapped[$i]["appl_gender"]=='F' ){
						$data_mapped[$i]["appl_gender"] = 2;
					}else{
						$data_mapped[$i]["appl_gender"] = 1;
					}
					 
					$data_mapped[$i]["appl_dob"] = date('Y-m-d',strtotime($data_mapped[$i]["appl_dob"]));

					
					if($direct){
						if( isset($data_mapped[$i]["appl_religion"]) && $data_mapped[$i]["appl_religion"]!=null && $data_mapped[$i]["appl_religion"]!=""){
							$data_mapped[$i]["appl_religion"] = $arr_religion[trim($data_mapped[$i]["appl_religion"])];
						}else{
							$data_mapped[$i]["appl_religion"] = 1;
						}
						
						if( isset($data_mapped[$i]["appl_marital_status"]) && $data_mapped[$i]["appl_marital_status"]!=null && $data_mapped[$i]["appl_marital_status"]!=""){
							$data_mapped[$i]["appl_marital_status"] = $arr_marital[trim($data_mapped[$i]["appl_marital_status"])];
						}else{
							$data_mapped[$i]["appl_marital_status"] = 1;
						}
					}
					
					$i++;
				}
		
				$row_count++;
			}
			fclose($handle);
		}
		
		return $data_mapped;
	}
	
}
 ?>