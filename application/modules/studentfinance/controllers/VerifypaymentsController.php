<?php

/**
 * PlacementtestmarksController
 * 
 * @author
 * @version 
 */


class Studentfinance_VerifypaymentsController extends Base_Base {
	/**
	 * The default action - show the home page
	 */
	
	public function init()
	{
		$this->fnsetObj();
			 
	}
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjplacementtestmodel = new Application_Model_DbTable_Placementtest();
		$this->lobjplacementtestmarksbulkmodel = new Registration_Model_DbTable_Verifypayments();
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
	}	
	
	public function indexAction() 
	{
		$this->view->lobjform = $this->lobjform; 
		$this->view->IdPrgm = 0;
		// TODO Auto-generated PlacementtestmarksController::indexAction() default action
		$larrresult = array();//$this->lobjstudentapplication->fetchAll('Active = 1', "FName ASC");
		
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->placementmarkspaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();	
		$this->view->lobjform->field5->addMultiOptions($larProgramNameCombo);
		$this->view->lobjform->field5->setAttrib('required',"true");
		
		$larBranchNameCombo = $this->lobjplacementtestmarksmodel->fngetBranchCombo();
		$this->view->lobjform->field8->addMultiOption("","Select");
		$this->view->lobjform->field8->addMultiOptions($larBranchNameCombo);
		//$this->view->lobjform->field8->setAttrib('required',"true");	
		
		
		if(isset($this->gobjsessionsis->placementmarkspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->placementmarkspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjplacementtestmarksbulkmodel->fnSearchEducationlist( $this->lobjform->getValues ());
			
				
				if(count($larrresult) > 0 ) {
					$ArraIdApplications['IdApplication'][0] =  0;
					$ArraIdApplication[0]['IdApplication'] =  0;
					$ArraIdApplication[0]['FName'] =  0;
					$ArraIdApplication[0]['PermAddressDetails'] =  0;
					$ArraIdApplication[0]['ICNumber'] =  0;
					$ArraIdApplication[0]['ProgramName'] =  0;
					$ArraIdApplication[0]['PermCity'] =  0;
					$ArraIdCheckLists['IdCheckList'][0] =  0;
					$ArraIdCheckList[0]['IdCheckList'] =  0;
					$ArraIdCheckList[0]['CheckListName'] =  0;
					$cnts  = 0;
					$cnts1 = 0;
					for($i=0;$i<count($larrresult);$i++){
					
						if(!in_array($larrresult[$i]['IdApplication'],$ArraIdApplications['IdApplication'])){
							$ArraIdApplications['IdApplication'][$cnts] = $larrresult[$i]['IdApplication'];
							$ArraIdApplication[$cnts]['IdApplication'] = $larrresult[$i]['IdApplication'];
							$ArraIdApplication[$cnts]['FName'] = $larrresult[$i]['FName'];
							$ArraIdApplication[$cnts]['PermAddressDetails'] = $larrresult[$i]['PermAddressDetails'];
							$ArraIdApplication[$cnts]['IdProgram'] = $larrresult[$i]['IDCourse'];
							$ArraIdApplication[$cnts]['ICNumber'] = $larrresult[$i]['ICNumber'];
							$ArraIdApplication[$cnts]['ProgramName'] = $larrresult[$i]['ProgramName'];
							$ArraIdApplication[$cnts]['PermCity'] = $larrresult[$i]['PermCity'];
							$ArraIdApplication[$cnts]['CityName'] = $larrresult[$i]['CityName'];
							if($larrresult[$i]['PaymentRecived'])$ArraIdApplication[$cnts]['PaymentRecived'] = $larrresult[$i]['PaymentRecived'];
							else $ArraIdApplication[$cnts]['PaymentRecived'] = 0;
							if($larrresult[$i]['FullPayment'])$ArraIdApplication[$cnts]['FullPayment'] = $larrresult[$i]['FullPayment'];
							else $ArraIdApplication[$cnts]['FullPayment'] = 0;
							if($larrresult[$i]['OtherDocumentVarified'])$ArraIdApplication[$cnts]['OtherDocumentVarified'] = $larrresult[$i]['OtherDocumentVarified'];
							else $ArraIdApplication[$cnts]['OtherDocumentVarified'] = 0;
							if($larrresult[$i]['Comments'])$ArraIdApplication[$cnts]['Comments'] = $larrresult[$i]['Comments'];
							else $ArraIdApplication[$cnts]['Comments'] = "";
							$cnts++;						
						}					
					}					
					$larrresult	 = $ArraIdApplication;						
				}
				$lintpagecount= 1000;
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);	
				$this->gobjsessionsis->placementmarkspaginatorresult = $larrresult;
			}
		}
	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();	
			//echo "<pre>";print_r($larrformData);	exit;	
			if($larrformData['documentsvarified'])$array1 = $larrformData['documentsvarified'];  else $array1 = array();
			if($larrformData['recieved'])		  $array2 = $larrformData['recieved'];			 else $array2 = array();
			if($larrformData['fullPayment'])	  $array3 = $larrformData['fullPayment'];		 else $array3 = array();
			$result = array_diff($array2, $array1);
			$array1 = array_merge($array1,$result);
			$result = array_diff($array3, $array1);
			$array1 = array_merge($array1,$result);				
			if($array1)$this->lobjplacementtestmarksbulkmodel->fndeleteOldMarks($array1);
			for($ss=0;$ss<count($array1);$ss++){
				$StudentsIds  = $array1[$ss];
				$formdata['IdApplication'][$ss] = $StudentsIds;
				$formdata['PaymentRecived'][$ss] = 0;	
				$formdata['OtherDocumentVarified'][$ss] = 0;
				$formdata['FullPayment'][$ss] = 0;		
				$formdata['Comments'][$ss] = "'".$larrformData['comments'][$StudentsIds]."'";			
				 if(in_array($StudentsIds,$larrformData['documentsvarified'])){
				 	$formdata['OtherDocumentVarified'][$ss] = 1;				 	
				 }
			 	if(in_array($StudentsIds,$larrformData['recieved'])){
				 	$formdata['PaymentRecived'][$ss] = 1;				 	
				 }
			 	if(in_array($StudentsIds,$larrformData['fullPayment'])){
				 	$formdata['FullPayment'][$ss] = 1;				 	
				 }
			}
			$auth = Zend_Auth::getInstance();	
			$formdata['UpdUser']    =  $auth->getIdentity()->iduser;
			$formdata['UpdDate']    =  date ( 'Y-m-d H:i:s' );
			if($array1) $this->lobjplacementtestmarksbulkmodel->fninsertNewMarks($formdata);					
			$this->_redirect( $this->view->baseUrl . '/studentfinance/verifypayments/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'placementtestmarks', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/studentfinance/verifypayments/index');
		}
		
	}

}
