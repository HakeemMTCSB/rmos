<?php
class Studentfinance_StudentreleaseController extends Base_Base {
	private $lobjsearchform;
	private $lobjdefinition;
	private $lobjintake;
	private $lobjstudentrelease;
	private $lobjstudentreleaseform;

	public function init() {
		$this->_gobjlog = Zend_Registry :: get('log'); //instantiate log object
		$this->view->translate = Zend_Registry :: get('Zend_Translate');
		Zend_Form :: setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}

	public function fnsetObj() {
		$this->lobjsearchform = new App_Form_Search();
		$this->lobjdefinition = new App_Model_Definitiontype();
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$this->lobjstudentrelease = new Studentfinance_Model_DbTable_Studentrelease();
		$this->lobjstudentreleaseform = new Studentfinance_Form_Studentrelease();
	}

	public function indexAction() {
		$this->view->lobjform = $this->lobjsearchform;
		$this->view->lobjform->field5->addMultiOptions($this->lobjdefinition->fnGetDefinationMs("Release Type"));
		$this->view->lobjform->field8->addMultiOptions($this->lobjintake->fngetallIntake());
		$larrresult = $this->lobjstudentrelease->fngetAllStudentRelease();
		//echo "<pre>";print_r($larrresult);
		if (!$this->_getParam('search'))
			unset ($this->gobjsessionstudent->studentreleasepaginatorresult);
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page', 1); // Paginator instance

		if (isset ($this->gobjsessionstudent->studentreleasepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->studentreleasepaginatorresult, $lintpage, $lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
		}

		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			unset ($larrformData['Search']);
			if ($this->lobjsearchform->isValid($larrformData)) {
				$larrresult = $this->lobjstudentrelease->fnsearchStudentRelease($larrformData);
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
				$this->gobjsessionsis->gobjsessionstudent = $larrresult;
			}
		}
	}

	public function addstudentreleaseAction() {
		$this->view->lobjstudentreleaseform = $this->lobjstudentreleaseform;
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjstudentreleaseform->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth :: getInstance();
		$this->view->lobjstudentreleaseform->UpdUser->setValue($auth->getIdentity()->iduser);
		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost();
			unset($larrformData['Save']);
			$this->lobjstudentrelease->fninsertstudentrelease($larrformData);
			$priority = Zend_Log :: INFO;
			$larrlog = array (
			'user_id' => $auth->getIdentity()->iduser, 'level' => $priority, 'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']), 'time' => date('Y-m-d H:i:s'), 'message' => 'New Student Release Add', 'Description' => Zend_Log :: DEBUG, 'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
			$this->_gobjlog->write($larrlog);
			$this->_redirect( $this->baseUrl . '/studentfinance/studentrelease/index	');
		}
	}

	public function editstudentreleaseAction() {
		$IdStudentRelease = $this->_getParam('id', 0);
		$result = $this->lobjstudentrelease->fetchAll('IdStudentRelease ='.$IdStudentRelease);
		$result = $result->toArray();
               
		if(isset($result[0])) {
			$this->lobjstudentreleaseform->populate($result[0]);
		}
		$this->view->lobjstudentreleaseform = $this->lobjstudentreleaseform;
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjstudentreleaseform->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth :: getInstance();
		$this->view->lobjstudentreleaseform->UpdUser->setValue($auth->getIdentity()->iduser);
		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost();
			unset($larrformData['Save']);
                        unset($larrformData['IdStudent']);
			$IdStudentRelease = $this->_getParam('IdStudentRelease', 0);
			$this->lobjstudentrelease->fnupdatestudentrelease($larrformData,$IdStudentRelease);
			$priority = Zend_Log :: INFO;
			$larrlog = array (
			'user_id' => $auth->getIdentity()->iduser, 'level' => $priority, 'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']), 'time' => date('Y-m-d H:i:s'), 'message' => 'Edit Student Release ID: '.$IdStudentRelease, 'Description' => Zend_Log :: DEBUG, 'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
			$this->_gobjlog->write($larrlog);
			$this->_redirect( $this->baseUrl . '/studentfinance/studentrelease/index');
		}
	}

	public function getstudentapplicantlistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdCategory = $this->_getParam('IdCategory');
		if ($IdCategory == "1") {
			$result = $this->lobjstudentrelease->fngetstudentlist();
			echo Zend_Json_Encoder :: encode($result);
		}
		elseif ($IdCategory == "2") {
			$result = $this->lobjstudentrelease->fngetapplicantlist();
			echo Zend_Json_Encoder :: encode($result);
		}
		exit;
	}
        
        
        public function delstudentreleaseAction() {
                $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('id');
                $result = $this->lobjstudentrelease->fndeleterelease($id);
                if($result) {  echo '1'; die; } 
        }
        
        
}