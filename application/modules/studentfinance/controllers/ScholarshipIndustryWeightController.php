<?php

class Studentfinance_ScholarshipIndustryWeightController extends Zend_Controller_Action
{

    public function indexAction() {
        $IndustryWeight = new Studentfinance_Model_DbTable_ScholarshipIndustryWeight();
        $this->view->industryweights = $IndustryWeight->fetchAll();
    }

    public function addAction() {
        $IndustryWeight = new Studentfinance_Model_DbTable_ScholarshipIndustryWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('industryweight');
            $IndustryWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-industry-weight/index');
        }
    }

    public function editAction() {
        $IndustryWeight = new Studentfinance_Model_DbTable_ScholarshipIndustryWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('industryweight');
            $IndustryWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-industry-weight/index');
        }

        $id =$this->_getParam('id');
        $industryweight = $IndustryWeight->find($id);
        if($industryweight->count() > 0) {
            $this->view->industryweight = $industryweight->current();
        } else {
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-industry-weight/index');
        }
    }

    public function deleteAction() {
        $IndustryWeight = new Studentfinance_Model_DbTable_ScholarshipIndustryWeight();
        $id =$this->_getParam('id');
        $industryweight = $IndustryWeight->find($id);
        $industryweight->current()->delete();

        $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record deleted"));
        $this->redirect( $this->baseUrl . '/studentfinance/scholarship-industry-weight/index');
    }

}