<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 5/4/2016
 * Time: 11:37 AM
 */
class Studentfinance_BillingSummaryController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_BillingSummary();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('GL - Billing Report');

        //intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $this->view->intakeData = $intakeDb->fngetallIntakelist();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData);
            $this->view->semester = isset($formData['semester'])?$formData['semester']:0;
            $this->view->intake = isset($formData['intake'])?$formData['intake']:0;
            $form = new Studentfinance_Form_BillingVsCoursereg(array('programid'=>$formData['program']));
            $this->view->form = $form;
            $form->populate($formData);

            $list = $this->model->getAccountCode($formData);

            //var_dump($list); exit;
            if ($list){
                foreach ($list as $key => $loop){
                    if ($loop['ac_id'] != null) {
                        $invoice = $this->model->getInvoice($formData, $loop['ac_id']);
                    }else{
                        $invoice = $this->model->getInvoice2($formData);
                    }

                    $totalamount = 0.00;
                    $totalcnamount = 0.00;
                    $totaldnamount = 0.00;
                    $totaldnaamount = 0.00;
                    $totalnettAdjustAmount = 0.00;

                    if ($invoice){
                        foreach ($invoice as $invoiceLoop){
                            $amountCur = $invoiceLoop['amount'];
                            $amountCur = $amountCur * $invoiceLoop['cr_exchange_rate'];

                            $totalamount = $totalamount + $amountCur;

                            $invMain =  $invoiceLoop['invMainID'];
                            $invDetail =  $invoiceLoop['idDetail'];

                            $searchCN = array(
                                'date_from'=>$formData['datefrom'],
                                'date_to'=>$formData['dateto'],
                                'invoice_id'=>$invMain,
                                'invoice_detail_id'=>$invDetail
                            );

                            $creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
                            $cnAmount = $creditNoteDB->getSearchDataCN($searchCN);

                            $amountCn = isset($cnAmount['cnamount']) ? $cnAmount['cnamount']:0;
                            if($invoiceLoop['cn_cur_id'] == 2){
                                $amountCn = $amountCn * $invoiceLoop['cr_exchange_rate'];
                            }
                            $totalcnamount = $totalcnamount + $amountCn;

                            $discountNoteDB = new Studentfinance_Model_DbTable_Discount();
                            $dnAmount = $discountNoteDB->getSearchDataDN($searchCN);

                            $amountDn = isset($dnAmount['dnamount'])?$dnAmount['dnamount']:0;
                            if($invoiceLoop['cur_id'] == 2){
                                $amountDn = $amountDn * $invoiceLoop['cr_exchange_rate'];
                            }
                            $totaldnamount = $totaldnamount + $amountDn;

                            $discountNoteAdjustDB = new Studentfinance_Model_DbTable_DiscountAdjustment();
                            $dnaAmount = $discountNoteAdjustDB->getDiscountData($searchCN);

                            $amountDna = isset($dnaAmount['dnaamount'])?$dnaAmount['dnaamount']:0;
                            if($invoiceLoop['cur_id'] == 2){
                                $amountDna = $amountDna * $invoiceLoop['cr_exchange_rate'];
                            }
                            $totaldnaamount = $totaldnaamount + $amountDna;

                            $nettAdjustAmount = $amountCur - $amountCn;
                            $totalnettAdjustAmount = $totalnettAdjustAmount + $nettAdjustAmount;
                        }
                    }

                    $nettAmount = $totalamount - $totalcnamount - $totaldnamount + $totaldnaamount;

                    $list[$key]['current_billing'] = $totalamount;
                    $list[$key]['adjustment'] = $totalcnamount;
                    $list[$key]['billing_after_adjustment'] = $totalnettAdjustAmount;
                    $list[$key]['discount'] = $totaldnamount;
                    $list[$key]['discount_adjustment'] = $totaldnaamount;
                    $list[$key]['billing_after_adjustment_discount'] = $totalnettAdjustAmount - $totaldnamount + $totaldnaamount;
                    $list[$key]['net_discount'] = $totaldnamount - $totaldnaamount;
                    $list[$key]['net_amount'] = $nettAmount;
                }
            }

            $this->view->list = $list;
        }else{
            $date = date('d-m-Y');
            $populate = array(
                'datefrom'=>$date,
                'dateto'=>$date
            );
            $form = new Studentfinance_Form_BillingVsCoursereg();
            $this->view->form = $form;
            $form->populate($populate);
        }
    }

    public function printAction(){
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['semester'] = $formData['semester_id'];
            $formData['intake'] = $formData['intake_id'];
            $list = $this->model->getAccountCode($formData);

            if ($list){
                foreach ($list as $key => $loop){
                    if ($loop['ac_id'] != null) {
                        $invoice = $this->model->getInvoice($formData, $loop['ac_id']);
                    }else{
                        $invoice = $this->model->getInvoice2($formData);
                    }

                    $totalamount = 0.00;
                    $totalcnamount = 0.00;
                    $totaldnamount = 0.00;
                    $totaldnaamount = 0.00;
                    $totalnettAdjustAmount = 0.00;

                    if ($invoice){
                        foreach ($invoice as $invoiceLoop){
                            $amountCur = $invoiceLoop['amount'];
                            $amountCur = $amountCur * $invoiceLoop['cr_exchange_rate'];

                            $totalamount = $totalamount + $amountCur;

                            $invMain =  $invoiceLoop['invMainID'];
                            $invDetail =  $invoiceLoop['idDetail'];

                            $searchCN = array(
                                'date_from'=>$formData['datefrom'],
                                'date_to'=>$formData['dateto'],
                                'invoice_id'=>$invMain,
                                'invoice_detail_id'=>$invDetail
                            );

                            $creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
                            $cnAmount = $creditNoteDB->getSearchDataCN($searchCN);

                            $amountCn = isset($cnAmount['cnamount']) ? $cnAmount['cnamount']:0;
                            if($invoiceLoop['cn_cur_id'] == 2){
                                $amountCn = $amountCn * $invoiceLoop['cr_exchange_rate'];
                            }
                            $totalcnamount = $totalcnamount + $amountCn;

                            $discountNoteDB = new Studentfinance_Model_DbTable_Discount();
                            $dnAmount = $discountNoteDB->getSearchDataDN($searchCN);

                            $amountDn = isset($dnAmount['dnamount'])?$dnAmount['dnamount']:0;
                            if($invoiceLoop['cur_id'] == 2){
                                $amountDn = $amountDn * $invoiceLoop['cr_exchange_rate'];
                            }
                            $totaldnamount = $totaldnamount + $amountDn;

                            $discountNoteAdjustDB = new Studentfinance_Model_DbTable_DiscountAdjustment();
                            $dnaAmount = $discountNoteAdjustDB->getDiscountData($searchCN);

                            $amountDna = isset($dnaAmount['dnaamount'])?$dnaAmount['dnaamount']:0;
                            if($invoiceLoop['cur_id'] == 2){
                                $amountDna = $amountDna * $invoiceLoop['cr_exchange_rate'];
                            }
                            $totaldnaamount = $totaldnaamount + $amountDna;

                            $nettAdjustAmount = $amountCur - $amountCn;
                            $totalnettAdjustAmount = $totalnettAdjustAmount + $nettAdjustAmount;
                        }
                    }

                    $nettAmount = $totalamount - $totalcnamount - $totaldnamount + $totaldnaamount;

                    $list[$key]['current_billing'] = $totalamount;
                    $list[$key]['adjustment'] = $totalcnamount;
                    $list[$key]['billing_after_adjustment'] = $totalnettAdjustAmount;
                    $list[$key]['discount'] = $totaldnamount;
                    $list[$key]['discount_adjustment'] = $totaldnaamount;
                    $list[$key]['billing_after_adjustment_discount'] = $totalnettAdjustAmount - $totaldnamount + $totaldnaamount;
                    $list[$key]['net_discount'] = $totaldnamount - $totaldnaamount;
                    $list[$key]['net_amount'] = $nettAmount;
                }
            }

            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_billing_summary.xls';
    }
}