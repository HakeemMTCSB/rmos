<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_AccountCodeController extends Base_Base {

	public function indexAction(){

		$this->view->title = "Account Code";
		
		$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		
		$this->view->account_code_list = $accountCodeDb->getAccountCode();

	}
	
	public function addAccountCodeAction(){
		
		$this->view->title = "Add Account Code";
		
		$form = new Studentfinance_Form_AccountCode();
		
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
		
			if ($form->isValid($formData)) {
		
				$data = array(
						'ac_code' => $formData['ac_code'],
						'ac_desc' => $formData['ac_desc'],
						'ac_acc_type' => $formData['ac_acc_type'],
						'ac_status' => $formData['ac_status'],
				);
		
				$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		
				$accountCodeDb->insert($data);
		
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new account code');
		
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'account-code', 'action'=>'index'),'default',true));
		
			}else{
				$form->populate($formData);
			}
		}
		
		$this->view->form = $form;
	}
	
	public function editAccountCodeAction(){
		
		$id = $this->_getParam('id',null);
		
		if($id==null){
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'account-code', 'action'=>'index'),'default',true));
		}
	
		$this->view->title = "Edit Account Code";
	
		$form = new Studentfinance_Form_AccountCode();
		
		$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
	
		if ($this->getRequest()->isPost()) {
	
			$formData = $this->getRequest()->getPost();
	
			if ($form->isValid($formData)) {
	
				$data = array(
						'ac_code' => $formData['ac_code'],
						'ac_desc' => $formData['ac_desc'],
						'ac_acc_type' => $formData['ac_acc_type'],
						'ac_status' => $formData['ac_status'],
				);
	
				
	
				$accountCodeDb->update($data,array('ac_id=?'=>$id));
	
				$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update account code');
	
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'account-code', 'action'=>'index'),'default',true));
	
			}else{
				$form->populate($formData);
			}
		}else{
			$data = $accountCodeDb->fetchRow(array('ac_id=?'=>$id))->toArray();
			$form->populate($data);
		}
	
		$this->view->form = $form;
	}
}