<?php

class Studentfinance_ErpIntegrationController extends Base_Base {
	
	public function init(){
		$this->lobjdeftype = new App_Model_Definitiontype();
		
        require_once('../library/PHPExcel/Classes/PHPExcel.php');
	}
	
	public function indexAction(){
        $this->view->title = $this->view->translate('ERP Integration');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 $type = $formData['type'];
			 $dateFrom = $formData['date_from'];
			 $dateTo = $formData['date_to'];
			 $this->view->date_from = $formData['date_from'];
			 $this->view->date_to = $formData['date_to'];
			 $this->view->type = $formData['type'];
			 
			 /*
			  * 1 = invoice
			  * 2 = receipt
			  * 3 = credit note
			  * 4 = refund
			  * 5 = discount
			  */
			
			 $db = Zend_Db_Table::getDefaultAdapter();
			 
			 $auth = Zend_Auth::getInstance();
			 $getUserIdentity = $auth->getIdentity();
			 $creator = $getUserIdentity->id;
			 
			 //insert
			 $dataStack = array(
				'type'=>$type,
				'date_from'=>$dateFrom,
				'date_to'=>$dateTo,
				'created_date'=>date('Y-m-d H:i:s'),
				'created_by'=>$creator
			
			);
			$db->insert('finance_erp',$dataStack);
			$id = $db->lastInsertId();
			 
			 if($type == 2){
			 	
			 	//bank
				 $select_payment_1 = $db->select()
								->from(
									array('pm'=>'receipt'),array(
											'id'=>'pm.rcp_id',
											'record_date'=>'pm.rcp_receive_date',
											'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
											'applicant_name' => new Zend_Db_Expr ('""'),
											'student_name' => new Zend_Db_Expr ('""'),
											'accounttype' => new Zend_Db_Expr ('"Bank"'),
											'txn_type' => new Zend_Db_Expr ('"Payment"'),
											'debit' => 'rcp_amount',
											'credit' => new Zend_Db_Expr ('"0.00"'),
											'reference_num' => 'rcp_no',
											'invoice_no' => 'rcp_no',
											'invoice_desc' => new Zend_Db_Expr ('""'),
											'subject' =>  new Zend_Db_Expr ('""'),
//											'migrate_desc' => 'pm.rcp_description',
//											'migrate_code' => 'MigrateCode',		
											'exchange_rate' =>  new Zend_Db_Expr ('"x"'),
											'accountcode' =>  "CONCAT_WS(' ',ac.ac_code,ac.ac_desc)",																	
										)
								)
								
								->join(array('p'=>'payment'),'p.p_rcp_id = pm.rcp_id', array())	
								->join(array('c'=>'tbl_currency'),'c.cur_id=pm.rcp_cur_id', array('cur_code','cur_id','cur_desc'))	
								->join(array('cr'=>'tbl_currency_rate'),'pm.rcp_receive_date <= DATE(cr.cr_effective_date) AND pm.rcp_receive_date <= DATE(cr.cr_effective_date) ', array())	
								->join(array('ac'=>'tbl_account_code'),'ac.ac_id = pm.rcp_account_code', array())	
								->where("pm.rcp_status = 'APPROVE'");
								
					if($dateFrom!=''){
						$select_payment_1->where("DATE(pm.rcp_receive_date) >= ?",$dateFrom);
					}
				
					if($dateTo!=''){
						$select_payment_1->where("DATE(pm.rcp_receive_date) <= ?",$dateTo);
					}
								

					//customer
					$select_payment_2 = $db->select()
								->from(
									array('pm'=>'receipt'),array(
											'id'=>'pm.rcp_id',
											'record_date'=>'pm.rcp_receive_date',
											'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
											'applicant_name' => "CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)",
											'student_name' => "CONCAT_WS(' ',sa.appl_fname,sa.appl_mname,sa.appl_lname)",
											'accounttype' => new Zend_Db_Expr ('"Customer"'),
											'txn_type' => new Zend_Db_Expr ('"Payment"'),
											'debit' => new Zend_Db_Expr ('"0.00"'),
											'credit' => 'rcv.rcp_inv_amount',
//											'credit' => "
//											IF(pm.rcp_cur_id = 2,".new Zend_Db_Expr ('rcv.rcp_inv_amount * cr.cr_exchange_rate').",".new Zend_Db_Expr ('rcv.rcp_inv_amount').")",
											'reference_num' => 'rcp_no',
											'invoice_no' => 'im.bill_number',
											'invoice_desc' => 'fi.fi_name',
											'subject' =>  new Zend_Db_Expr ('""'),
//											'migrate_desc' => 'pm.rcp_description',
//											'migrate_code' => 'MigrateCode',		
											'exchange_rate' =>  'im.exchange_rate',
											'accountcode' =>  "CONCAT_WS(' ',ac.ac_code,ac.ac_desc)",
										)
								)
								->joinLeft(array('rcv'=>'receipt_invoice'),'rcv.rcp_inv_rcp_id = pm.rcp_id', array())	
								->join(array('im'=>'invoice_main'),'im.id = rcv.rcp_inv_invoice_id', array())	
								->join(array('ivd'=>'invoice_detail'),'ivd.id = rcv.rcp_inv_invoice_dtl_id and ivd.invoice_main_id = im.id', array())	
								->joinLeft(array('fi'=>'fee_item'),'fi.fi_id = ivd.fi_id', array())
								->join(array('p'=>'payment'),'p.p_rcp_id = pm.rcp_id', array())	
								->join(array('c'=>'tbl_currency'),'c.cur_id=pm.rcp_cur_id', array('cur_code','cur_id','cur_desc'))	
								->join(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id', array())	
								->join(array('cr'=>'tbl_currency_rate'),'cr.cr_id = im.exchange_rate', array())
								->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = pm.rcp_trans_id',array())
								->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = at.at_appl_id',array())
						->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
						->joinLeft(array('str'=>'tbl_studentregistration'), 'str.IdStudentRegistration = pm.rcp_idStudentRegistration',array())
						->joinLeft(array('sa'=>'student_profile'), 'sa.id = str.sp_id',array())
								->where("pm.rcp_status = 'APPROVE'");
			
			 		if($dateFrom!=''){
						$select_payment_2->where("DATE(pm.rcp_receive_date) >= ?",$dateFrom);
					}
				
					if($dateTo!=''){
						$select_payment_2->where("DATE(pm.rcp_receive_date) <= ?",$dateTo);
					}
         	}
         	
         	//get array payment				
			$select = $db->select()
					    ->union(array($select_payment_1,$select_payment_2),  Zend_Db_Select::SQL_UNION_ALL)
//					    ->order("record_date asc")
					    ->order("txn_type asc")
					    ->order("accounttype asc")
					   
					   ;
			$results = $db->fetchAll($select);
			
			$amountCurr = array();
			$curencyArray = array();
			$balance = 0;
			$i = 0;
			
        	 if($results){
				foreach ( $results as $row )
				{
//					$curencyArray[$row['cur_id']] = $row['cur_code'];
					
					$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
					
					if($row['exchange_rate']){
						if($row['exchange_rate']== 'x'){
							$dataCur = $curRateDB->getRateByDate(2,$row['record_date']);//usd
						}else{
							$dataCur = $curRateDB->getData($row['exchange_rate']);//usd
						}
					}else{
						$dataCur = $curRateDB->getRateByDate(2,$row['record_date']);
					}
						
					if($row['cur_id'] == 2){
						$amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'],2);
						$amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'],2);
					}else{
						$amountDebit = $row['debit'];
						$amountCredit = $row['credit'];
					}
		
					/*if( $row['txn_type']=="Invoice" ){
						$balance = $balance + $amountDebit;
					}else
					if( $row['txn_type']=="Payment" ){
			
						$balance = $balance - $amountCredit;
						
					}else
					if( $row['txn_type']=="Credit Note" ){
						$balance = $balance - $amountCredit;
					}else
					if( $row['txn_type']=="Discount" ){
						$balance = $balance - $amountCredit;
					}else
					if( $row['txn_type']=="Debit Note" ){
						$balance = $balance + $row['debit'];
					}else
					if( $row['txn_type']=="Refund" ){
						$balance = $balance + $amountDebit;
					}*/
					
					$desc = '';
					$studentName = '';
					if($row['accounttype'] == 'Bank'){
						$desc = $row['description'];
					}elseif($row['accounttype'] == 'Customer'){
						$studentName = ($row['applicant_name']) ? $row['applicant_name'] : $row['student_name'];
						$desc = $studentName.' : '.$row['description'].' : '.$row['invoice_desc'];
					}
					
					$amountCurr[$i]['txn_type'] = $row['txn_type'];
					$amountCurr[$i]['account_type'] = $row['accounttype'];
					$amountCurr[$i]['record_date'] = $row['record_date'];
					$amountCurr[$i]['reference_num'] = $row['reference_num'];
		         	$amountCurr[$i]['business_unit'] = 1;
		         	$amountCurr[$i]['cost_center'] = 3;
		         	$amountCurr[$i]['department'] = NULL;
		         	$amountCurr[$i]['main_account'] = $row['accountcode'];
		         	$amountCurr[$i]['description'] = $desc;
		         	$amountCurr[$i]['debit'] = $row['debit'];
		         	$amountCurr[$i]['credit'] = $row['credit'];
		         	$amountCurr[$i]['cur_id'] = $row['cur_id'];
					$amountCurr[$i]['invoice_no'] = $row['invoice_no'];
					$amountCurr[$i]['debit_myr'] = $amountDebit;
					$amountCurr[$i]['credit_myr'] = $amountCredit;
//					$amountCurr[$i]['balance_myr'] = number_format($balance, 2, '.', ',');
					
					
	         	
		         	$i++;
				}
			}else{
				$amountCurr = null;
			}
			
//			echo "<pre>";
//			print_r($amountCurr);

			$dirName = $this->createUploadFile();
			$fileExcel = $this->generateExcel($amountCurr,$dirName,$id);
			
			$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Upload successful');
			
         }
         
        
         
       
    }
    
	public function generateExcel($arrayStack,$dirName,$id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("SMS")
									 ->setTitle("ERP Integration")
									 ->setSubject("ERP Integration");
		
		// Add some data
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'AccountType')
		                              ->setCellValue('B1', 'Business Unit')
		                              ->setCellValue('C1', 'Cost Center')
		                              ->setCellValue('D1', 'Department')
		                              ->setCellValue('E1', 'Main account')
		                              ->setCellValue('F1', 'Description')
		                              ->setCellValue('G1', 'Debit')
		                              ->setCellValue('H1', 'Credit')
		                              ->setCellValue('I1', 'Currency')
		                              ->setCellValue('J1', 'OffsetLedger')
		                              ->setCellValue('K1', 'OffsetBU')
		                              ->setCellValue('L1', 'OffsetCC')
		                              ->setCellValue('M1', 'OffsetDept')
		                              ->setCellValue('N1', 'Offset Main Account')
		                              ->setCellValue('O1', 'PostingProfile')
		                              ->setCellValue('P1', 'Documentnum')
		                              ->setCellValue('Q1', 'Documentdate');
		                              
		$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
		                              
		//lets manipulate the data
		if($arrayStack){
			$noExcel = 2;
			foreach ($arrayStack as $entry):
			
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$noExcel, $entry['account_type']);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$noExcel, $entry['business_unit']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$noExcel, $entry['cost_center']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$noExcel, $entry['department']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$noExcel, $entry['main_account']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$noExcel, $entry['description']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$noExcel, $entry['debit_myr']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$noExcel, $entry['credit_myr']);
				
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$noExcel, $entry['reference_num']);
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$noExcel, date('d-m-Y', strtotime($entry['record_date']) ));

				$noExcel++;	
			endforeach;
			
		}                              
		
		//sheet name
		$objPHPExcel->getActiveSheet()->setTitle('ERP - Student Finance');
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		//filename
		$fileName = date('Ymd').'_finance.xlsx';
		$pathName =  date('Ymd').'_finance.csv';
		//save into folder
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$saveName = $dirName.'/'.$fileName;
		$saveName2 = $dirName.'/'.$pathName;
		
//		$objWriter->save($fileName);
		$checkFile = file_exists($saveName2);
		if($checkFile){
			unlink($saveName2);
		}
		
		$objWriter->save(str_replace('.xlsx', '.csv',$saveName));
		
		//update filepath
		
		$dataUpdate = array(
			'filepath' => $pathName, 
			'path' => $saveName, 
			'filename' => $pathName, 
		);
		
		$db->update('finance_erp', $dataUpdate, 'id ='.  $id );
		
		
	}
	
	public function createUploadFile()
	{
		
        $dir = ERP_URL."/GL";
            
        if ( !is_dir($dir) ){
            if ( mkdir_p($dir) === false )
                {
                   echo 'Cannot create attachment folder ('.$dir.')';
                }
        }
        
        return $dir;
			
	}
	
	public function checkFileAction()
	{
		
		$dir    = ERP_URL.'/GL/';
		$files1 = scandir($dir);
        echo "<pre>";
        print_r($files1);
        
        foreach($files1 as $file){
        	
        	readfile($dir.$file);
        	
        }
        
        exit;
		
	}
    
}