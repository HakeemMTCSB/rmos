<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_AdvancePaymentController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_AdvancePayment();
		$this->_DbObj = $db;
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() {
		
		//title
    	$this->view->title= $this->view->translate("Advance Payment");
    	
    	$msg = $this->_getParam('msg', null);
    	    	
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			if($formData['type'] == 645){
				$type = 1;
			}else{
				$type = 2;
			}
			
			$this->view->type = $type;
			$this->view->search = $formData;
			
			$advPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
			
			$list = $advPaymentDb->getSearchData($formData);
			$this->view->receipt_list = $list;
		}
    	
    	if( $msg!=null ){
    		$this->view->noticeMessage = $msg;
    	}   	
	}
	
	public function detailAction(){
		
		$id = $this->_getParam('id', null);
                $type = $this->_getParam('type', null);
		$this->view->id = $id;
                $this->view->type = $type;
		
		//title
    	$this->view->title= $this->view->translate("Advance Payment : Detail");
    	
	//profile
    	if($type == 1){
	    	$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
	    	$profile = $applicantProfileDb->getApplicantRegistrationDetail($id);
    	}elseif($type == 2){
    		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
	    	$profile = $studentRegistrationDb->getTheStudentRegistrationDetail($id);
    	}
    	
    	$this->view->profile = $profile;
	}
	
	public function applicantDetailAction(){
		$this->_helper->layout()->disableLayout();
		
		$id = $this->_getParam('id', null);
		$type = $this->_getParam('type', null);
                
                $this->view->id = $id;
                $this->view->type = $type;
                
		$this->view->data = $this->_DbObj->getApplicantAvdPaymentAll($id,$type);
	}
	
	public function advancePaymentDetailAction(){
		if($this->_request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();
		}

		$id = $this->_getParam('id', null);
                $type = $this->_getParam('type', null);
                $idapp = $this->_getParam('idapp', null);
                
                $this->view->id = $idapp;
                $this->view->type = $type;
                $this->view->idAdv = $id;
		
		$this->view->title= $this->view->translate("Advance Payment : Detail");
		
		//data
		$this->view->data = $this->_DbObj->getData($id);
				
		//applicant info
		if($type == 1){
	    	$applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
	    	$profile = $applicantProfileDb->getApplicantRegistrationDetail($idapp);
    	}elseif($type == 2){
    		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
	    	$profile = $studentRegistrationDb->getTheStudentRegistrationDetail($idapp);
    	}
    	
    	$this->view->profile = $profile;
		
		//data detail
		$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
		$this->view->data_detail = $advancePaymentDetailDb->getDataDetail($id);	
		
	}
	
	public function searchStudentAction(){
		$this->_helper->layout()->disableLayout();
		
		$name = $this->_getParam('name', null);
		$id = $this->_getParam('id', null);
		
		if ($this->getRequest()->isPost()) {
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
	        $ajaxContext->addActionContext('view', 'html');
	        $ajaxContext->initContext();
	            
		  	$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()
		                 ->from(array('ap'=>'applicant_profile'),array('distinct(appl_id)'))
		                 ->join(array('at'=>'applicant_transaction'), 'at.at_appl_id = ap.appl_id', array());

		    if($name!=null){
		    	$select->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$name.'%');
		    }
		    
			if($id!=null){
		    	$select->where("at.at_pes_id like '%".$id."%'");
		    }
		    //echo $select;

		    $select_clear = $db->select()
		    				->from(array('ap'=>'applicant_profile'),array('appl_id','concat_ws(\' \',ap.appl_fname,ap.appl_mname,ap.appl_lname)name','appl_email'))
		    				->where('ap.appl_id in ('.$select.')');
		    
		    //echo $select_clear;
		    //exit;
		    				
	        $row = $db->fetchAll($select_clear);
		  
			$ajaxContext->addActionContext('view', 'html')
	                    ->addActionContext('form', 'html')
	                    ->addActionContext('process', 'json')
	                    ->initContext();
	
			$json = Zend_Json::encode($row);
			
			echo $json;
			exit();
    
		}
	}
	
public function addAction() {

	//set unlimited
	set_time_limit(0);
	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', 0);

		//title
    	$this->view->title= $this->view->translate("Generate Advance Payment");
    	
    	$msg = $this->_getParam('msg', null);
    	
    	//program
        $programDb = new Registration_Model_DbTable_Program();	
        $this->view->programData = $programDb->getData();
    	    	
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			if($formData['type'] == 645){
				$type = 1;
			}else{
				$type = 2;
			}
			
			$advPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
			
			if($formData['typeoftransfer'] == 1){ //deposit
				$list = $advPaymentDb->getDepositFee($formData);

				if ($list){
					foreach ($list as $key => $loop){
						$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
						$balance = $paModel->getBalanceDtl($loop['idDetail']);
						//dd($balance);
						if (str_replace(',', '', $balance['bill_balance']) > 0){
							unset($list[$key]);
						}else{
							$list[$key]['bill_balance']=str_replace(',', '', $balance['bill_balance']);
						}
					}
				}

				//var_dump($list);
				$this->view->deposit_list = $list;
			}elseif($formData['typeoftransfer'] == 2){//-ve
				$list = $advPaymentDb->getNegatifInvoiceBalance($formData);
				//var_dump($list); exit;

				if ($list){
					foreach ($list as $key => $loop){
						$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
						$balance = $paModel->getBalanceMain($loop['id']);
						//dd($balance);
						if (str_replace(',', '', $balance['bill_balance']) >= 0){
							unset($list[$key]);
							//var_dump($balance);
						}else{
							$list[$key]['bill_balance']=str_replace(',', '', $balance['bill_balance']);
						}
					}
				}

				$this->view->invoice_list = $list;
			}
			
			$this->view->type = $type;
			$this->view->search = $formData;
			
			$this->view->receipt_list = $list;
		}
    	
    	if( $msg!=null ){
    		$this->view->noticeMessage = $msg;
    	}   	
	}
	
	public function generateAction(){
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			$proforma = $formData['invoice_id'];
			$n=0;
			while($n < count($proforma)){
				$proforma_id = $proforma[$n];
				$invoiceDB = new Studentfinance_Model_DbTable_InvoiceMain();
				$info = $invoiceDB->getData($proforma_id);
				
			
				//$balance = str_replace('-', '', $info['bill_balance']);
				$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
				$balanceInfo = $paModel->getBalanceMain($proforma_id);
				$balance = str_replace('-', '', str_replace(',', '', $balanceInfo['bill_balance']));
				$total_advance_payment_amount = $balance;
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
				
				if($info){
					
					$db->beginTransaction();
			
					try {
				
			
						$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
						$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
						
						$bill_no =  $this->getBillSeq(8, date('Y'));
						
						$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
						$rate = $currencyRateDb->getCurrentExchangeRate($info['currency_id']);
						
						$advance_payment_data = array(
							'advpy_fomulir' => $bill_no,
							'advpy_appl_id' => $info['appl_id'],
							'advpy_trans_id' => $info['trans_id'],
							'advpy_idStudentRegistration' => $info['IdStudentRegistration'],
							'advpy_sem_id'=>$info['semester'],
							'advpy_invoice_no' => $info['bill_number'],
							'advpy_invoice_id' => $proforma_id,
							'advpy_description' => 'Advance payment from invoice no:'.$info['bill_number'],
							'advpy_cur_id' => $info['currency_id'],
							'advpy_amount' => $total_advance_payment_amount,
							'advpy_total_paid' => 0.00,
							'advpy_exchange_rate' => $rate['cr_id'],
							'advpy_total_balance' => $total_advance_payment_amount,
							'advpy_status' => 'A',
							'advpy_date' => date('Y-m-d'),
							'ref_flag' => 1

						);
					
						
						$advPayID = $advancePaymentDb->insert($advance_payment_data);
						
						$advance_payment_det_data = array(
							'advpydet_advpy_id' => $advPayID,
							'advpydet_total_paid' => 0.00
						);
						
						$advancePaymentDetailDb->insert($advance_payment_det_data);
						
						//update invoice
						$data_update = array('bill_balance' =>0);
						$db->update('invoice_main',$data_update, 'id = '.$proforma_id);
						
						$data_update2 = array('balance'=>0);
						$db->update('invoice_detail',$data_update2, 'invoice_main_id = '.$proforma_id);
						
						$db->commit();
						
					} catch (Exception $e) {
						$db->rollBack();
						echo $e->getMessage();
						
						$sysErDb = new App_Model_General_DbTable_SystemError();
					   	$msg['se_IdStudentRegistration'] = $info['IdStudentRegistration'];
					   	$msg['se_txn_id'] = $info['trans_id'];
					   	$msg['se_title'] = 'Error generate advance payment';
					   	$msg['se_message'] = $e->getMessage();
					   	$msg['se_createdby'] = $auth->getIdentity()->iduser;
					   	$msg['se_createddt'] = date("Y-m-d H:i:s");
					   	$sysErDb->addData($msg);
						exit;
					}
				}
				
						
				$n++;
			}
		}
		
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Advance Payment Generated');
		$this->_redirect( $this->baseUrl . '/studentfinance/advance-payment/add');
		exit;
    }
    
public function generateDepositAction(){
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			$proforma = $formData['invoice_id'];
			$n=0;
			while($n < count($proforma)){
				$proforma_id = $proforma[$n];
				$invoiceDB = new Studentfinance_Model_DbTable_InvoiceMain();
				$info = $invoiceDB->getData($proforma_id);
				
				$balance = $info['bill_amount'];
				//$paModel = new Studentfinance_Model_DbTable_PaymentAmount();
				//$balanceInfo = $paModel->getBalanceMain($proforma_id);
				//$balance = str_replace('-', '', str_replace(',', '', $balanceInfo['bill_balance']));
				$total_advance_payment_amount = $balance;
				
				$auth = Zend_Auth::getInstance();
				$getUserIdentity = $auth->getIdentity();
				
				if($info){
					
					$db->beginTransaction();
			
					try {
				
						$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
						$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
						
						$bill_no =  $this->getBillSeq(8, date('Y'));
						
						$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
						$rate = $currencyRateDb->getCurrentExchangeRate($info['currency_id']);
						
						$advance_payment_data = array(
							'advpy_fomulir' => $bill_no,
							'advpy_appl_id' => $info['appl_id'],
							'advpy_trans_id' => $info['trans_id'],
							'advpy_idStudentRegistration' => $info['IdStudentRegistration'],
							'advpy_sem_id'=>$info['semester'],
							'advpy_invoice_no' => $info['bill_number'],
							'advpy_invoice_id' => $proforma_id,
							'advpy_description' => 'Advance payment from invoice no:'.$info['bill_number'],
							'advpy_cur_id' => $info['currency_id'],
							'advpy_amount' => $total_advance_payment_amount,
							'advpy_total_paid' => 0.00,
							'advpy_exchange_rate' => $rate['cr_id'],
							'advpy_total_balance' => $total_advance_payment_amount,
							'advpy_status' => 'A',
							'advpy_date' => date('Y-m-d'),
							
						);
					
						$advPayID = $advancePaymentDb->insert($advance_payment_data);
						
						$advance_payment_det_data = array(
							'advpydet_advpy_id' => $advPayID,
							'advpydet_total_paid' => 0.00
						);
						
						$advancePaymentDetailDb->insert($advance_payment_det_data);
						
						//update invoice
						$data_update = array('adv_transfer_id' =>$advPayID);
						$db->update('invoice_main',$data_update, 'id = '.$proforma_id);
						
					
						$db->commit();
						
					} catch (Exception $e) {
						$db->rollBack();
						echo $e->getMessage();
						
						$sysErDb = new App_Model_General_DbTable_SystemError();
					   	$msg['se_IdStudentRegistration'] = $info['IdStudentRegistration'];
					   	$msg['se_txn_id'] = $info['trans_id'];
					   	$msg['se_title'] = 'Error generate advance payment';
					   	$msg['se_message'] = $e->getMessage();
					   	$msg['se_createdby'] = $auth->getIdentity()->iduser;
					   	$msg['se_createddt'] = date("Y-m-d H:i:s");
					   	$sysErDb->addData($msg);
						exit;
					}
				}
				
						
				$n++;
			}
		}

		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Advance Payment Generated');
		$this->_redirect( $this->baseUrl . '/studentfinance/advance-payment/add');
		
    }
    
    /*
	 * Get Bill no from mysql function
	 */
	private function getBillSeq($type, $year){
		
		$seq_data = array(
				$type,
				$year,
				0,
				0, 
				0
		);
		 
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
		
		return $seq['proforma_invoice_no'];
	}
	
	public function ajaxGetAdvancePaymentAction(){
    
    	$id = $this->_getParam('id', 0);
    	$type = $this->_getParam('type', 0);
    	
    	if($id==0){
    		echo "error";
    		exit;
    	}
    	 
    	//if ($this->getRequest()->isXmlHttpRequest()) {
    	$this->_helper->layout->disableLayout();
    	//}
    
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('view', 'html');
    	$ajaxContext->initContext();
    
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select_invoice = $db->select()
					->from(array('i'=>'advance_payment'),array('*'))
					->join(array('c'=>'tbl_currency'),'c.cur_id = i.advpy_cur_id', array('cur_code','cur_symbol_prefix','cur_symbol_suffix'))
					
					->where("i.advpy_status = 'A'")
					->where("i.advpy_total_balance > 0")
					->order('i.advpy_date desc');
					
		if($type == 645){
			$select_invoice->where('i.advpy_trans_id = ?',$id);
		}elseif($type == 646){
			$select_invoice->where('i.advpy_idStudentRegistration = ?',$id);
		}
		$invoice = $db->fetchAll($select_invoice);
    
    	$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		echo $json = Zend_Json::encode($invoice);
		
		$this->view->json = $json;
		exit;
    }
    
    
	public function cancelAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
			$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
			
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			$advID = $formData['advID'];
			
			$proforma = isset($formData['invoice_id'])?$formData['invoice_id']:0;
			
			if($proforma){
				$n=0;
				while($n < count($proforma)){
					$proforma_id = $proforma[$n];
					
					$info = $advancePaymentDetailDb->getDataDetailUtilized($proforma_id);
					
					$auth = Zend_Auth::getInstance();
					$getUserIdentity = $auth->getIdentity();
					
					if(isset($info['advpy_idStudentRegistration'])){
						$idApp = $info['advpy_idStudentRegistration'];
						$type = 2;
					}else{
						$idApp = $info['advpy_trans_id'];
						$type = 1;
					}
					
					//revert invoice
					$this->revertInvoice($proforma_id);
					$n++;
				}
			}
			
			//cancel main status
			//id no data to be utilized yet
			/*$advMain = $advancePaymentDb->getData($advID);
			
			$advDetail = $advancePaymentDetailDb->getDataDetail($advID);
			$advDetailActive = $advancePaymentDetailDb->getDataDetailActive($advID);
			
			if(!$advDetail){
				
				$upd_data = array(
					'advpy_status'=> 'X',
					'advpy_cancel_by'=> $getUserIdentity->id,
					'advpy_cancel_date'=> date('Y-m-d H:i:s')
				);
				
				$advancePaymentDb->update($upd_data, array('advpy_id = ?' => $advID) );
				
			}*/
			
			
		}
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Advance Payment has been Removed');
		$this->_redirect( $this->baseUrl . '/studentfinance/advance-payment/advance-payment-detail/id/'.$info['advpydet_advpy_id'].'/idapp/'.$idApp.'/type/'.$type);
		exit;
    }
    
    
	private function revertInvoice($cnID){
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
		
		$info = $advancePaymentDetailDb->getDataDetailUtilized($cnID);
		
		$adv_amount = $info['advpydet_total_paid'];
		$advId = $info['advpydet_advpy_id'];
		
		$invIdDet = $info['advpydet_inv_det_id'];
		$invId = $info['advpydet_inv_id'];
	
		//update invoice detail
		$invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invDetail = $invoiceDetailDB->getData($invIdDet);
		
		$balance = $invDetail['balance'];
		$paid = $invDetail['paid'];
		
		if($info['advpy_cur_id'] ==1 && $invDetail['cur_id'] == 2){
			$balance = $invDetail['balance'] * $invDetail['cr_exchange_rate'];
			$paid = $invDetail['paid'] * $invDetail['cr_exchange_rate'];
		}else if($info['advpy_cur_id'] == 2 && $invDetail['cur_id'] == 1){
			$balance = $invDetail['balance'] / $invDetail['cr_exchange_rate'];
			$paid = $invDetail['paid'] / $invDetail['cr_exchange_rate'];
		}
			
		$amount = $balance  + $adv_amount ;
		$paidAmount = $paid - $adv_amount;
		
		$upd_data_inv = array(
			'balance'=> $amount,
			'paid'=> $paidAmount,
		);
	
		$invoiceDetailDB->update($upd_data_inv, array('id = ?' => $invIdDet) );
		
		$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
		$invMain = $invoiceMainDB->getData($invId);
		
		$balance = $invMain['bill_balance'];
		$paid = $invMain['bill_paid'];
		
		if($info['advpy_cur_id'] ==1 && $invDetail['cur_id'] == 2){
			$balance = $invMain['bill_balance'] * $invMain['cr_exchange_rate'];
			$paid = $invMain['bill_paid'] * $invMain['cr_exchange_rate'];
		}else if($info['advpy_cur_id'] == 2 && $invDetail['cur_id'] == 1){
			$balance = $invMain['bill_balance'] / $invMain['cr_exchange_rate'];
			$paid = $invMain['bill_paid'] / $invMain['cr_exchange_rate'];
		}
		
		$amount = $balance + $adv_amount ;
		$paidAmt =  $paid - $adv_amount;
		$upd_data_inv = array(
			'bill_balance'=> $amount,
			'bill_paid'=> $paidAmt,
			'upd_by'=> $getUserIdentity->id,
			'upd_date'=> date('Y-m-d H:i:s')
		);
		
		$invoiceMainDB->update($upd_data_inv, array('id = ?' => $invId) );
		
		//insert discount_adjustment
		$advAdjustDB = new Studentfinance_Model_DbTable_AdvancePaymentAdjustment();
		
		$adjId = $this->getCurrentAdjNoSeq();
		
		$upd_data_inv = array(
			'AdjustmentCode'=> $adjId,
			'AdjustmentType'=> 1,
			'IdAdv_Det'=>$cnID,
			'Remarks' => 'Advance Payment Cancellation',
			'cur_id'=>$info['advpy_cur_id'],
			'amount'=>$adv_amount,
			'Status'=>'APPROVE',
			'EnterBy'=> $getUserIdentity->id,
			'EnterDate'=> date('Y-m-d H:i:s'),
			'UpdUser'=> $getUserIdentity->id,
			'UpdDate'=> date('Y-m-d H:i:s')
		);
		
		$idAdjust =  $advAdjustDB->insert($upd_data_inv);
		
		$newStatus = 'X';
			
		$upd_data = array(
			'adjustId'=> $idAdjust,
			'advpydet_status'=> $newStatus,
			'status_by'=> $getUserIdentity->id,
			'status_date'=> date('Y-m-d H:i:s')
		);
		
		$advancePaymentDetailDb->update($upd_data, array('advpydet_id = ?' => $cnID) );
		
		$advDB = new Studentfinance_Model_DbTable_AdvancePayment();
		$dataDisc = $advDB->getData($advId);
		
		$amountAdPaid = $dataDisc['advpy_total_paid'] - $adv_amount;
		$amountAdjBal = $dataDisc['advpy_total_balance'] + $adv_amount;
		$upd_data = array(
			'advpy_total_paid'=> $amountAdPaid,
			'advpy_total_balance'=> $amountAdjBal,
			'advpy_upd_by'=> $getUserIdentity->id,
			'advpy_upd_date'=> date('Y-m-d H:i:s')
		);
		
		$advDB->update($upd_data, array('advpy_id = ?' => $advId) );
		
		
	}
	
	private function getCurrentAdjNoSeq(){
	
		$seq = 1;	
		$r_num = "";
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('s'=>'sequence'))
						->where("s.type = 15")
						->where("s.year = ?", date('Y'));
		
		$row = $db->fetchRow($selectData);
		
		if($row){
			$seq = $row['seq']+1;
			$db->update('sequence',array('seq'=>$seq),'year = '.date('Y').' and type = 15');
		}else{
			$db->insert('sequence',array('year'=>date('Y'),'type'=>15,'seq'=>1));
		}
		
		//read initial config
		$selectData2 = $db->select()
						->from(array('i'=>'tbl_config'))
						->where("i.idUniversity = 1")
						->order("i.UpdDate ASC");
		
		$row2 = $db->fetchRow($selectData2);
		
		if($row2){
			$r_num = 'ADJV'.date('Y').$row2['ReceiptSeparator'].str_pad($seq, 5, "0", STR_PAD_LEFT);
		}

		return $r_num;
	}
	
	public function deleteAction(){
		
		if ($this->_request->isPost ()) {
			
			$formData = $this->getRequest()->getPost();
			
			$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
			$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
			
			$auth = Zend_Auth::getInstance();
			$getUserIdentity = $auth->getIdentity();
			
			$advID = $formData['advID'];
			
			$advMain = $advancePaymentDb->getData($advID);
			
			$advDetail = $advancePaymentDetailDb->getDataDetail($advID);
//			$advDetailActive = $advancePaymentDetailDb->getDataDetailActive($advID);
			
			
			echo "<pre>";
			print_r($advDetail);
			if($advMain){
				$idApp = $advMain['advpy_idStudentRegistration'];
				$type = 2;
			}else{
				$idApp = $advMain['advpy_trans_id'];
				$type = 1;
			}
					
			if($advDetail){
				$n=0;
				foreach($advDetail as $data){
					$proforma_id = $data['advpydet_id'];
					
					$info = $advancePaymentDetailDb->getDataDetailUtilized($proforma_id);
					
					$auth = Zend_Auth::getInstance();
					$getUserIdentity = $auth->getIdentity();
					
					
					
					//revert invoice
					$this->revertInvoice($proforma_id);
					$n++;
				}
			}
			
			//cancel main status
			//id no data to be utilized yet
			
//			if(!$advDetail){
				
				$upd_data = array(
					'advpy_status'=> 'X',
					'advpy_cancel_by'=> $getUserIdentity->id,
					'advpy_cancel_date'=> date('Y-m-d H:i:s')
				);
				
				$advancePaymentDb->update($upd_data, array('advpy_id = ?' => $advID) );
				
//			}
			
			
		}
		$this->gobjsessionsis->flash = array('type'=>'success','message'=>'Advance Payment has been Cancelled');
		$this->_redirect( $this->baseUrl . '/studentfinance/advance-payment/advance-payment-detail/id/'.$advMain['advpy_id'].'/idapp/'.$idApp.'/type/'.$type);
		exit;
    }
    
}