<?php

/*
 * Scholarship controller
 *
 * @on 13/6/2014
 * @authur izham
 */

class Studentfinance_FinancialAidController extends Base_Base
{


    public function init(){
        //$this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->loadObj();
    }

    public function loadObj() {

        $this->auth = Zend_Auth::getInstance();

        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->gobjsessionsis = Zend_Registry::get('sis');

        $this->scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $this->intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->scholarshipTagModel = new Studentfinance_Model_DbTable_ScholarshipTagging();
        $this->SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();

        $this->SponsorModel = new Studentfinance_Model_DbTable_Sponsor();
        $this->SponsorApplicationForm = new Studentfinance_Form_SponsorApplication();

        $this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();

        $this->locales = $this->view->locales = array(
            'en_US'	=> 'English',
            'ms_MY'	=> 'Malay'
        );
    }

    public function indexAction(){
        $this->view->title = $this->view->translate("Financial Aids");
        $FinancialAid  = new Studentfinance_Model_DbTable_FinancialAid();
        $financial_aids = $FinancialAid->fetchAll();

        $this->view->financial_aids = $financial_aids;
    }

    public function addAction()
    {

        $this->view->title = $this->view->translate("Add Scholarship");

        //Fee Item
        $this->view->fees = $this->lobjFeeItem->getData();



        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('financialaid');

            $data = array(
                'code' => $post_data['code'],
                'name' => $post_data['name'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->getIdentity()->id
            );

            $FinancialAid = new Studentfinance_Model_DbTable_FinancialAid();
            $id = $FinancialAid->insert($data);

            $this->redirect('/studentfinance/financial-aid/edit/id/' . $id);

        }

    }

    public function editAction()
    {
        $FinancialAid = new Studentfinance_Model_DbTable_FinancialAid();
        $id = $this->_getParam('id',null);
        $this->view->title = $this->view->translate("Edit Scholarship");
        $FinancialAidCourse = new Studentfinance_Model_DbTable_FinancialAidCourse();

        $financialaids = $FinancialAid->find($id);
        $financialaid = $financialaids->current();
        $this->view->financialaid = $financialaid;

        //Fee Item
        $feeList = $this->lobjFeeItem->getData();
        $this->view->fee_items = array();
        foreach( $feeList as $item)
        {
            $this->view->fee_items[$item['fi_id']] = $item['fi_name'];
        }



        $FinancialCoverage = new Studentfinance_Model_DbTable_FinancialAidCoverage();
        $financial_cov_sql = $FinancialCoverage->select()->where('financial_aid_id',$id);
        $this->view->financialcoverages = $FinancialCoverage->fetchAll($financial_cov_sql);
        //var_dump($this->view->financialcoverages);
        $Semester = new Registration_Model_DbTable_Semester();
        $this->view->semesters = $Semester->fetchAll('display = 1', 'SemesterMainStartDate DESC');

        $financial_aid_courses_sql = $FinancialAidCourse->select()->where('financial_aid_id = ?', $id);
        $financial_aid_courses = $FinancialAidCourse->fetchAll($financial_aid_courses_sql);
        $aid_courses = array();
        foreach($financial_aid_courses as $financial_course) {
            $aid_courses[$financial_course->semester_id] = $financial_course->total_course;
        }
        $this->view->aid_courses = $aid_courses;

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (!empty($formData['financialaid'])) {
				

                $db = getDB();
                $data = array(
                    'code' => $formData['financialaid']['code'],
                    'name' => $formData['financialaid']['name']
                );

                $db->update('tbl_financial_aid', $data, array("id = ?" =>$id));

            } else if (!empty($formData['feeinfo'])) {
                // Fee Info
                if(!empty($formData['feeinfo'])) {
                    $check = count($formData['feeinfo']);
                    $feeinfo = $formData['feeinfo'];

                    $fee_data = array(
                        'financial_aid_id'	=> $id,
                        'fee_code'			=> $feeinfo['fee_code'],
                        'calculation_mode'	=> $feeinfo['calculation_mode'],
                        'amount'			=> $feeinfo['amount']
                    );

                    $FinancialCoverage->insert($fee_data);
                }
            } else if (!empty($formData['financial_aid_course'])) {
                // Fee Info

                if(!empty($formData['financial_aid_course'])) {
                    $FinancialAidCourse->delete('financial_aid_id = ' . $id);
                   foreach($formData['financial_aid_course'] as $semester_id => $fa_course) {
                       if(empty($fa_course)) { continue; }
                       $fcdata['financial_aid_id'] = $id;
                       $fcdata['semester_id'] = $semester_id;
                       $fcdata['total_course'] = $fa_course;

                       $FinancialAidCourse->insert($fcdata);
                   }

                }
            }

            //redirect
            $this->redirect('/studentfinance/financial-aid/edit/id/' . $id);
        }

    }

    public function deletefeeAction() {
        $id = $this->_getParam('id');
        $Feecoverage = new Studentfinance_Model_DbTable_FinancialAidCoverage();
        $Feecoverage->deleteFee($id);
        $this->redirect('/studentfinance/financial-aid/edit/id/1');
    }

    /*public function deleteFeeAction()
    {
        $db = getDB();

        $id = $this->_getParam('id');

        if ( $id != '' )
        {
            $db->delete('tbl_scholarship_fee_info', 'IdScholarshipFeeInfo = '.(int) $id);

            $data = array('msg' => '');

            echo Zend_Json::encode($data);
            exit;
        }
        else
        {
            $data = array('msg' => 'Invalid ID');

            echo Zend_Json::encode($data);
            exit;
        }
    }*/

    public function deleteAction(){
        $id = $this->_getParam('id',null);
        $FinancialAid = new Studentfinance_Model_DbTable_FinancialAid();

        if ($id){
            $FinancialAid->delete("id = ".$id);
        }

        $this->view->backURL = $this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true);
        //redirect
        $this->redirect('/studentfinance/financial-aid/index');
    }

}