<?php

class Studentfinance_StudentDiscountTaggingController extends Base_Base
{
	private $lobjdeftype;
	private $_gobjlog;
	private $lobjPolicySetup;
	public $lobjform;


	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();

	}

	public function fnsetObj(){
			
		$this->lobjDiscountSetupForm = new Studentfinance_Form_DiscountSetup();
		//$this->lobjSponsorSetup = new Studentfinance_Model_DbTable_Sponsor();
		$this->lobjDiscountSetup = new Studentfinance_Model_DbTable_DiscountType();
		$this->lobjDiscountTag = new Studentfinance_Model_DbTable_DiscountTypeTag();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
		$this->lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();
			
	}

	public function indexAction()
	{
		$this->view->title = 'Student Discount Tagging';


	}

	public function tagAction()
	{
		$this->view->title = 'Student Discount Tagging';

		//Form
		$this->view->lobjDiscountSetupForm = $this->lobjDiscountSetupForm;

		$id = $this->_getParam ( 'id' );
		
		$discount = $this->lobjDiscountSetup->getData($id);
		$this->view->discount = $discount;
		
		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$UpdDate = date('Y-m-d H:i:s');
		$lIntIdUniversity = $this->gobjsessionsis->idUniversity;

	
	
		//Render to View
		$this->view->lobjDiscountSetupForm->IdUniversity->setvalue ($lIntIdUniversity);
		$this->view->lobjDiscountSetupForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjDiscountSetupForm->UpdUser->setValue ($userId);
		$this->view->lobjDiscountSetupForm->dt_id->setValue ($id);

		//JS
		
		$this->view->lobjDiscountSetupForm->SearchButton->setAttrib('OnClick','showstudentname();');
		$this->view->lobjDiscountSetupForm->Add->setAttrib('OnClick','remarkValidation();');
		$this->view->lobjDiscountSetupForm->Clear->setAttrib('OnClick','clearpageAdd();');


		$result = $this->lobjDiscountTag->getDiscountTag($id);
		$this->view->result = $result;

		$this->lobjDiscountSetupForm->populate($result);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
			
			$this->lobjDiscountTag->fnaddTagStudentDiscount($larrformData,$id);
			
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Add tag student discount' ,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			$this->_redirect( $this->baseUrl . '/studentfinance/student-discount-tagging/tag/id/'.$id);
		}
	}

	
	public function getstudentnameAction(){
		 $this->_helper->layout->disableLayout();
	   
	 	 $ajaxContext = $this->_helper->getHelper('AjaxContext');
	  	 $ajaxContext->addActionContext('view', 'html');
	 	 $ajaxContext->initContext();
	  
		 $lstrstudentname=  $this->_getParam ( 'studentid' );
		 $larrstudentname = $this->lobjDiscountSetup->fngetstudentname($lstrstudentname);
		
		 $ajaxContext->addActionContext('view', 'html')
					  ->addActionContext('form', 'html')
					  ->addActionContext('process', 'json')
					  ->initContext();
		   
		  $json = Zend_Json::encode($larrstudentname);
		   
		  echo $json;
		  exit();
	}
	
	
	public function editTaggingAction()
	{
		$this->view->title = 'Edit Student Discount Tagging';

		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		
		$id = $this->_getParam('id');
		$this->view->dtt_id = $id;
		
		$info = $this->lobjDiscountTag->getData($id);		
		$this->view->info = $info;
				
		$data = array(
						'StartDate'		=> $info['dtt_start_date'],
						'EndDate'		=> $info['dtt_end_date']
					);

		$this->view->lobjDiscountSetupForm = $this->lobjDiscountSetupForm;
					
		$this->view->lobjDiscountSetupForm->UpdDate->setValue (date('Y-m-d H:i:s'));
		$this->view->lobjDiscountSetupForm->UpdUser->setValue ($userId);
		
		
		//populate
		$this->lobjDiscountSetupForm->populate($data);

		
		if ($this->_request->isPost ()) 
		{	
			$formData = $this->_request->getPost();
			
			$data = array(
						'dtt_start_date'=> $formData['StartDate'],
						'dtt_end_date'	=> $formData['EndDate'],
						'dtt_modifyby'  => $formData['UpdUser'],
						'dtt_modifydt'  => $formData['UpdDate']
					);
			
			
			$this->lobjDiscountTag->updateTagStudent($data, $id);

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			$this->_redirect( $this->baseUrl . '/studentfinance/student-discount-tagging/tag/id/'.$info['dt_id']);
		}
			
	}
	
	public function deleteTaggingAction()
	{
		$dtt_id = $this->_getParam('dtt_id');
		$id = $this->_getParam('id');
		
		$where = "dtt_id = '".$dtt_id."'";
		$this->lobjDiscountTag->deleteData($where);
		
		$this->_helper->flashMessenger->addMessage(array('success' => "Information deleted"));

		$this->_redirect( $this->baseUrl . '/studentfinance/student-discount-tagging/tag/id/'.$id);
	}
}