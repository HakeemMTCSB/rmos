<?php

class Studentfinance_TagstudentscholarshipController extends Base_Base
{
    private $_gobjlog;
    public $lobjform;


    public function init()
    { //initialization function
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object

    }

    public function indexAction()
    {
        $this->view->title = 'Student Scholarship Tagging';

        //Form
        $this->view->lobjform = $this->lobjform;
        $ScholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $pagecount = $this->gintPageCount;
        $Paginator = new App_Model_Common(); // Definitiontype model
        $page = $this->_getParam('page', 1); // Paginator instance

        $clearform = $this->_request->getPost('Clear');
        if ($this->_request->isPost()) {
            if ($this->_request->getPost('Search')) {
                $post_data = $this->_request->getPost();

                $financialaids = $ScholarshipModel->getScholarshipList($post_data['search']);

            } else if (!empty($clearform)) {
                $this->redirect($this->baseUrl . '/studentfinance/tagstudentscholarship/');
            }
        } else {
            $financialaids = $ScholarshipModel->getScholarshipList();
        }

        $this->view->financialaids = $Paginator->fnPagination($financialaids, $page, $pagecount);

    }


    public function editAction()
    {
        $this->view->title = 'Edit Student Scholarship Tagging';
        $auth = Zend_Auth::getInstance();
        $ScholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $StudentTag = new Studentfinance_Model_DbTable_ScholarshipStudentTag();
        $Semester = new GeneralSetup_Model_DbTable_Semestermaster();

        $id =(int) $this->_getParam('id');

        $app_data = $this->_request->getPost('application');
        $search_data = $this->_request->getPost('search');

        if ( $this->_request->isPost() && (!empty( $app_data )) ) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); 
            //print_r($formData);
            
          
           
            if( empty($app_data['sa_cust_id'])){

                $this->_helper->flashMessenger->addMessage(array('error' => "Student must be selected."));

            } else {

            	$app_data['sa_cust_type'] = 1; //STudent
                $app_data['sa_status'] = 2; //automatically added as accepted
                
                if (isset($formData['from'])){
                    $app_data['sa_start_date'] = $formData['from']!='' ? date('Y-m-d', strtotime($formData['from'])):null;
                }
                
                if (isset($formData['to'])){
                    $app_data['sa_end_date'] = $formData['to']!='' ? date('Y-m-d', strtotime($formData['to'])):null;
                }

                //first search for existing
                $cur_rec = $StudentTag->getexisting($app_data['sa_cust_id'], $app_data['sa_scholarship_type']);
                if(!empty($cur_rec)) {
                	//echo 'edit';
                	//echo '<pre>';
                	//print_r($app_data);
                	//echo $cur_rec->sa_id;
                    $StudentTag->updateData($app_data, $cur_rec->sa_id);
                } else {
                	//echo 'add';
                	$app_data['sa_createddt'] = date('Y-m-d H:i:s');
            		$app_data['sa_createdby'] = $auth->getIdentity()->iduser;              
                    $StudentTag->insert($app_data);
                }
            }
			
            // Write Logs
            $priority = Zend_Log::INFO;
            $larrlog = array('user_id' => $auth->getIdentity()->iduser,
                'level' => $priority,
                'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                'time' => date('Y-m-d H:i:s'),
                'message' => 'Edit tag student scholarship',
                'Description' => Zend_Log::DEBUG,
                'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
            $this->_gobjlog->write($larrlog); //insert to tbl_log
            $this->redirect($this->baseUrl . '/studentfinance/tagstudentscholarship/edit/id/' . $id);

        } else {
            if (!empty($search_data)) {

                $scholarship_appsquery = $StudentTag->select();
                if(!empty($search_data['studentregistration_id'])) {
                    $studentregistration = new Registration_Model_DbTable_Studentregistration();
                    $studentregistrationquery = $studentregistration->select()
                        ->where('registrationId = ?', $search_data['studentregistration_id'])
                        ;
                    $student = $studentregistration->fetchRow($studentregistrationquery);
                    if(!empty($student)) {
                        $sa_cust_id = $student->IdStudentRegistration;
                        $scholarship_appsquery->where('sa_cust_id = ?', $sa_cust_id);
                        //dd($sa_cust_id);
                    } else {

                    }
                }

                if(!empty($search_data['sa_semester_id'])) {
                    $scholarship_appsquery->where('sa_semester_id = ?', $search_data['sa_semester_id']);
                }

                $scholarship_apps = $StudentTag->fetchAll($scholarship_appsquery);
            } else {

                $scholarship_apps = $StudentTag->fetchAll( 'sa_scholarship_type = '. $id );


            }

            $students = array();
            foreach($scholarship_apps as $scholarship_app) {
                if( $scholarship_app->sa_status != 2 ) { continue; }//skips that has not been accepted

                $student = $scholarship_app->findParentRow('Registration_Model_DbTable_Studentregistration');
                $student_profile = $student->findParentRow('Records_Model_DbTable_Studentprofile');
                $semester = $scholarship_app->findParentRow('GeneralSetup_Model_DbTable_Semestermaster');
                $students[] = array(
                    'ScholarshipApplication' => $scholarship_app,
                    'StudentRegistration' => $student,
                    'StudentProfile' => $student_profile,
                    'Semester' => $semester
                );
            }
        }

        $this->view->scholarship = $ScholarshipModel->find($id)->current();


        $this->view->semesters = $Semester->fetchAll();
        $this->view->students = $students;
        //var_dump($students[0]['ScholarshipApplication']); exit;
    }

    function disableAction() {

        $StudentTag = new Studentfinance_Model_DbTable_ScholarshipStudentTag();
        
        $id = $this->_getParam('id');
        
        $scholarship = $StudentTag->find($id)->current();
  
       // $update['sa_id'] = (int) $scholarship->sa_id;
        $update['sa_status'] = 7; 
        
        //$StudentTag->updateData($update,$scholarship->sa_id);
        $StudentTag->deleteData($scholarship->sa_id);
      
        //$StudentTag->update($update, 'sa_id = '. $update['sa_id']);

        $this->redirect('/studentfinance/tagstudentscholarship/edit/id/' . $scholarship->sa_scholarship_type);
    }

    public function viewStudentAction(){
        $this->view->title = $this->view->translate('Edit Student Sponsor Tagging Detail');
        
        $id = $this->_getParam('id');
        
        $StudentTag = new Studentfinance_Model_DbTable_ScholarshipStudentTag();
        $studentInfo = $StudentTag->getSponsorStudent($id);
        $this->view->studentInfo = $studentInfo;
        
        if ($this->_request->isPost()){
            $post_data = $this->_request->getPost();
            
            $data = array(
                'sa_start_date'=>$post_data['from']=='' ? null:date('Y-m-d', strtotime($post_data['from'])),
                'sa_end_date'=>$post_data['to']=='' ? null:date('Y-m-d', strtotime($post_data['to']))
            );
            $StudentTag->updateStudent($data, $id);
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/studentfinance/tagstudentscholarship/view-student/id/'.$id);
        }
        //exit;
    }
}