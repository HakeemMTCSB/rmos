<?php

/**
 * @author Muhamad Alif
 * @version 1.0
 */
class Studentfinance_AccountApplicantController extends Base_Base
{

    private $_DbObj;
    private $_sis_session;

    public function init()
    {
        $db = new Application_Model_DbTable_Applicant();
        $this->_DbObj = $db;

        $this->_sis_session = new Zend_Session_Namespace('sis');
        $resultsStatementDB = new Studentfinance_Model_DbTable_AccountStatement();
    }

    public function indexAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        //title
        $this->view->title = $this->view->translate("Statement of Account");

        $resultsStatementDB = new Studentfinance_Model_DbTable_AccountStatement();

        //program
        $programDb = new Registration_Model_DbTable_Program();
        $this->view->programData = $programDb->getData();

        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $this->view->intakeData = $intakeDb->fngetallIntakelist();

        $statusDb = new App_Model_Definitiontype();
        $this->view->statusData = $statusDb->getDefinationByType(20);


        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creatorRole = $getUserIdentity->IdRole;
        $this->view->user = $creatorRole;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $dateFrom = $formData['date_from'];
            $dateTo = $formData['date_to'];
            $p_data = $resultsStatementDB->getPaginateData($formData);

            $p_List = new Zend_Paginator_Adapter_DbSelect($p_data);
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
            $paginator->setItemCountPerPage(5000);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

            if ($paginator) {
                $i = 0;
                $balanceArr = array();
                foreach ($paginator as $p_Loop) {
                    $balance = $this->balance($p_Loop['transID'], $formData['type'], $dateFrom, $dateTo);
                    $balanceArr[$i]['balance'] = $balance;
                    $i++;
                }
                $this->view->balanceArr = $balanceArr;
            }
            $this->view->paginator = $paginator;
            $this->view->formData = $formData;

        }

    }

    public function listAction()
    {

        //title
        $this->view->title = $this->view->translate("Download Log");

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creatorRole = $getUserIdentity->IdRole;
        $this->view->user = $creatorRole;


        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'finance_service'))
            ->joinLeft(array('b' => 'tbl_program'), "b.IdProgram = a.program_id", array('ProgramCode'))
            ->joinLeft(array('c' => 'tbl_intake'), "c.IdIntake = a.intake_id", array("IntakeDesc"))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array('IdRole'))
            ->joinLeft(array('ts' => 'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('ts.FullName'))
            ->order('created_date desc');
        $results = $db->fetchAll($select);

        foreach ($results as $key => $data) {
            $type = $data['type'];
            $student_Array = explode(',', $data['student_id']);
            $trans_id = $student_Array[0];
            if ($type == 1) {
                $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
                $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            } elseif ($type == 2) {
                $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
            }

            $results[$key]['profile'] = $profile;
        }

        $this->view->result = $results;

    }

    public function ledgerAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

//		$invoiceClass = new icampus_Function_Studentfinance_Invoice();
//		$invoiceClass->generateCreditNote(4260,112,37939,0,50,0,1);
//		exit;

        $trans_id = $this->_getParam('id', null);
        $this->view->appl_id = $trans_id;

        $type = $this->_getParam('type', 1);
        $this->view->type = $type;

        $invoiceDB = new Studentfinance_Model_DbTable_InvoiceMain();

        $idStudentRegistration = $this->_getParam('id_reg', null);
        $this->view->idStudentRegistration = $idStudentRegistration;

        //title
        $this->view->title = $this->view->translate("Statement of Account");

        //profile
        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
            $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($trans_id);
            $profile = array_merge($profile, $taggingFinancialAid);
        }

        $this->view->profile = $profile;

        $idGroup = 0;
        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $idGroup = $formData['group'];

        }

        $this->view->groupid = $idGroup;

//    	echo "<pre>";
//    	print_r($profile);
//    	exit;

        //account
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'description' => 'GROUP_CONCAT(fc.fi_name)',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'bill_number',
                    'invoice_no' => 'bill_number',
                    'receipt_no' => new Zend_Db_Expr ('"-"'),
                    'subject' => 'GROUP_CONCAT(distinct s.SubCode)',
                    'fc_seq' => 'ivd.fi_id',
                    'migrate_desc' => 'bill_description',
                    'migrate_code' => 'MigrateCode',
                    //'exchange_rate' => 'im.exchange_rate',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('cn'=>'credit_note'), 'cn.cn_invoice_id = im.id AND cn.cn_status = "A"', array())
            //->where('im.currency_id= ?',$currency)						  
            ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id AND cnn.cn_status="A") OR im.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")')
            //->where('im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id)')
            ->where("im.status NOT IN ('X', 'E','0')")//display ALL status except X,0=entry (24/27/07/2015)
            ->group('im.id');
        if ($type == 1) {
            $select_invoice->where('im.trans_id = ?', $trans_id);
        } else {
            $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
        }
        //echo $select_invoice.'<br/>'; exit;


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => "rcp_amount",
                    'document' => 'rcp_gainloss_soa',
                    'invoice_no' => 'rcp_no',
                    'receipt_no' => 'rcp_no',
                    'subject' => 'rcp_gainloss',//gain/loss
                    'fc_seq' => 'rcp_gainloss_amt',//gain/loss - myr
                    'migrate_desc' => 'pm.rcp_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('a' => 'tbl_receivable_adjustment'), "a.IdReceipt = pm.rcp_id and a.Status = 'APPROVE'", array())
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("pm.rcp_status = 'APPROVE' OR (pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0 AND MONTH(pm.rcp_receive_date) != MONTH(a.EnterDate))");
//							->orWhere("pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0");
        if ($type == 1) {
            $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
        } else {
            $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
        }

        $select_credit = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn.cn_id',
                    'record_date' => 'cn.cn_create_date',
                    'description' => 'cn.cn_description',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'document' => 'cn_billing_no',
                    'invoice_no' => 'cn_billing_no',
                    'receipt_no' => 'cn_billing_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'cn.cn_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(i.exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
            ->joinLeft(array('j'=>'invoice_main'), 'cn.cn_invoice_id = j.id', array())
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("cn.cn_status = 'A'")
            ->where("IFNULL(j.status, 'A') NOT IN ('X', 'E','0')")
            //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A")');
            ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(j.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(j.invoice_date), 0))) OR i.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")');
        if ($type == 1) {
            $select_credit->where('cn.cn_trans_id = ?', $trans_id);
        } else {
            $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
        }

        $select_discount = $db->select()
            ->from(
                array('a' => 'discount_detail'), array(
                    'id' => 'dn.dcnt_id',
                    'record_date' => 'dn.dcnt_approve_date',
                    'description' => 'dn.dcnt_description',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'SUM(a.dcnt_amount)',
                    'document' => 'dn.dcnt_fomulir_id',
                    'invoice_no' => 'dn.dcnt_fomulir_id',
                    'receipt_no' => 'dn.dcnt_fomulir_id',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dn.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
//							->joinLeft(array('adj'=>'discount_adjustment'),"adj.IdDiscount = a.id and adj.Status= 'APPROVE' AND (MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date))", array())	
            //->where('pm.rcp_cur_id= ?',$currency)
            //->where("a.status = ?", 'A')
            ->where("MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date)")
            ->group('dn.dcnt_id');

        if ($type == 1) {
            $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        //refund
        $select_refund = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'rfd_fomulir',
                    'invoice_no' => 'rfd_fomulir',
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rn.rfd_date) != MONTH(rn.rfd_cancel_date)")
            ->where("rn.rfd_status IN  ('A','X')");

        if ($type == 1) {
            $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
        }

        //advance payment from invoice deposit only
        $select_advance = $db->select()
            ->from(
                array('adv' => 'advance_payment'), array(
                    'id' => 'advpy_id',
                    'record_date' => 'advpy_date',
                    'description' => "CONCAT(advpy_description,' - ',fi.fi_name)",
                    'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'advpy_amount',
                    'document' => 'advpy_fomulir',
                    'invoice_no' => 'advpy_fomulir',
                    'receipt_no' => 'advpy_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'advpy_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(adv.advpy_exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=adv.advpy_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('i' => 'invoice_main'), 'i.id=adv.advpy_invoice_id', array())
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = i.id', array())
            ->join(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
            ->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
            ->where("i.status = 'A'")
            ->where("adv.advpy_status = 'A'");

        if ($type == 1) {
            $select_advance->where('adv.advpy_trans_id = ?', $trans_id);
        } else {
            $select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);
        }

        //refund cancel
        $select_refund_cancel = $db->select()
            ->from(
                array('rnc' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_cancel_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund Adjustment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'rfd_amount',
                    'document' => 'rfd_fomulir',
                    'invoice_no' => "CONCAT(rfd_fomulir,'-',rfd_id)",
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rnc.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rnc.rfd_date) != MONTH(rnc.rfd_cancel_date)")
            ->where("rnc.rfd_status = 'X'");

        if ($type == 1) {
            $select_refund_cancel->where('rnc.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund_cancel->where('rnc.rfd_IdStudentRegistration = ?', $trans_id);
        }

        //discount adjustment
        $select_discount_cancel = $db->select()
            ->from(
                array('dsn' => 'discount_adjustment'), array(
                    'id' => 'dsn.id',
                    'record_date' => 'dsn.EnterDate',
                    'description' => "CONCAT(dsnt.dcnt_description,' (',dsnta.dcnt_fomulir_id,')')",
                    'txn_type' => new Zend_Db_Expr ('"Discount Adjustment"'),
                    'debit' => 'dsn.amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => "AdjustmentCode",
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dsnt.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dsn.cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('dsnt' => 'discount_detail'), 'dsnt.id=dsn.IdDiscount', array())
            ->join(array('dsnta' => 'discount'), 'dsnta.dcnt_id=dsnt.dcnt_id', array())
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = dsnt.dcnt_invoice_id', array())
            ->where("dsn.Status = 'APPROVE'")
            ->where("MONTH(dsnta.dcnt_approve_date) != MONTH(dsnta.dcnt_cancel_date)")
            ->where('dsn.AdjustmentType = 1'); //cancel

        if ($type == 1) {
            $select_discount_cancel->where('dsnta.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount_cancel->where('dsnta.dcnt_IdStudentRegistration = ?', $trans_id);
        }


        //payment adjustment - cancel approved receipt
        //checking not same month with receipt only

        $select_receipt_adj = $db->select()
            ->from(
                array('a' => 'tbl_receivable_adjustment'), array(
                    'id' => 'a.IdReceivableAdjustment',
                    'record_date' => 'a.EnterDate',
                    'description' => 'a.Remarks',
                    'txn_type' => new Zend_Db_Expr ('"Payment Adjustment"'),
                    'debit' => 'b.rcp_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => 'AdjustmentCode',
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'a.Remarks',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'receipt'), 'b.rcp_id = a.IdReceipt', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("a.Status = 'APPROVE'")
            ->where("MONTH(b.rcp_receive_date) != MONTH(a.EnterDate)");

        if ($type == 1) {
            $select_receipt_adj->where('b.rcp_trans_id = ?', $trans_id);
        } else {
            $select_receipt_adj->where('b.rcp_IdStudentRegistration = ?', $trans_id);
        }

        //currency adjustment
        $select_currency_adjustment = $db->select()
            ->from(
                array('a' => 'advance_payment_detail'), array(
                    'id' => 'a.advpydet_id',
                    'record_date' => 'a.advpydet_upddate',
                    'description' => new Zend_Db_Expr ('"Currency Adjustment"'),
                    'txn_type' => new Zend_Db_Expr ('"Currency Adjustment"'),
                    'debit' => 'a.gain',
                    'credit' => 'a.loss',
                    'document' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'invoice_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'receipt_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'b.advpy_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.advpy_cur_id', array('cur_code', 'cur_id'=>new Zend_Db_Expr ('"1"'), 'cur_desc'))
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.gain != 0.00 OR a.loss != 0.00');

        if ($type == 1) {
            $select_currency_adjustment->where('b.advpy_trans_id = ?', $trans_id);
        } else {
            $select_currency_adjustment->where('b.advpy_idStudentRegistration = ?', $trans_id);
        }

        //sponsor payment
        if ($type == 2) {
            $select_receipt_sponsor = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'b.rcp_receive_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
                        'debit' => new Zend_Db_Expr ('"0.00"'),
                        'credit' => 'a.rcp_inv_amount',
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'b.rcp_no',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))');
                //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array());
                //->where("b.rcp_status = 'APPROVE'");


            $select_receipt_sponsor->where('a.IdStudentRegistration = ?', $trans_id);

            $select_sponsor_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'IFNULL(e.sp_cancel_date, b.cancel_date)',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'e.sp_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", e.id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                ->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                //->where("e.sp_status = ?", 0)
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                ->where("e.sp_status = 0 OR b.rcp_status = 'CANCEL'");
                //->where("b.rcp_status = 'APPROVE'");

            $select_sponsor_adjustment->where('a.IdStudentRegistration = ?', $trans_id);

            $select_sponsor_adv_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'f.advpy_cancel_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'f.advpy_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", f.advpy_id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('f'=>'advance_payment'), 'a.rcp_inv_rcp_id = f.advpy_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                ->where("f.advpy_status = ?", 'X');

                $select_sponsor_adv_adjustment->where('a.IdStudentRegistration = ?', $trans_id);
                $select_sponsor_adv_adjustment->where('f.advpy_idStudentRegistration = ?', $trans_id);
        }

        //get array payment

        if ($type == 1) {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_currency_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");

        } else {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_currency_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");
        }


        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        //process data
        $results2 = array();
        $amountCurr = array();
        $curencyArray = array();
        $balance = 0;

        //dd($results); exit;

        if ($results) {
            foreach ($results as $resultkey => $row) {
                $spmCurAdjStatus = false;
                $sponsorCurAdj = 0.00;

                $curencyArray[$row['cur_id']] = $row['cur_code'];

                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                if ($row['txn_type'] == "Sponsor Payment" || $row['txn_type'] == "Sponsor Payment Adjustment") {
                    $selectInvoiceSpm = $db->select()
                        ->from(array('a'=>'sponsor_invoice_receipt_invoice'), array())
                        ->join(array('b'=>'sponsor_invoice_detail'), 'a.rcp_inv_sp_id = b.sp_id', array('b.sp_amount'))
                        ->joinLeft(array('c'=>'invoice_main'), 'b.sp_invoice_id = c.id', array('c.invoice_date'))
                        ->join(array('d'=>'sponsor_invoice_receipt'), 'a.rcp_inv_rcp_id = d.rcp_id', array())
                        ->where('a.rcp_inv_student = ?', $row['id'])
                        ->group('b.id');

                    if ($row['txn_type'] == "Sponsor Payment Adjustment") {
                        $selectInvoiceSpm->where("b.sp_status = 0 OR d.rcp_status = 'CANCEL'");
                    }

                    $listInvoiceSpm = $db->fetchAll($selectInvoiceSpm);

                    $amountDebitSponsor = 0.00;
                    $amountCreditSponsor = 0.00;

                    if ($listInvoiceSpm){
                        foreach ($listInvoiceSpm as $invoiceSpm){
                            if ($row['cur_id'] == 2) {
                                $dataCur = $curRateDB->getRateByDate(2, $invoiceSpm['invoice_date']);
                                //$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);

                                if ($row['txn_type'] == "Sponsor Payment") {
                                    $amountCreditSponsor = round($amountCreditSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                }else{
                                    $amountDebitSponsor = round($amountDebitSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                }
                            } else {
                                if ($row['txn_type'] == "Sponsor Payment") {
                                    $amountCreditSponsor = $amountCreditSponsor + $invoiceSpm['sp_amount'];
                                }else{
                                    $amountDebitSponsor = $amountDebitSponsor + $invoiceSpm['sp_amount'];
                                }
                            }
                        }
                    }
                } //else {
                    if ($row['exchange_rate']) {
                        if ($row['exchange_rate'] == 'x') {
                            $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
//							$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                        } else {
                            $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                        }
                    } else {
                        $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);
//						$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                    }
                //}

//					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
//					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
//					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];
                //if ($row['txn_type'] != "Sponsor Payment" && $row['txn_type'] != "Sponsor Payment Adjustment") {
                    if ($row['cur_id'] == 2) {
                        $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                        $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                    } else {
                        $amountDebit = $row['debit'];
                        $amountCredit = $row['credit'];
                    }
                //}
//					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;

                if ($row['txn_type'] == "Invoice") {
                    $balance = $balance + $amountDebit;
                } else
                    if ($row['txn_type'] == "Payment") {

                        //cek invoice value and receipt value
                        $getReceiptInvoice = $curRateDB->getReceiptInvoice($row['id']);

                        $invoiceValue = 0;

                        if ($getReceiptInvoice){
                            foreach ($getReceiptInvoice as $getReceiptInvoiceLoop){
                                if ($getReceiptInvoiceLoop['rcp_inv_cur_id'] != 1) {
                                    $getReceiptInvoiceRate = $curRateDB->getRateByDate($getReceiptInvoiceLoop['rcp_inv_cur_id'], $getReceiptInvoiceLoop['invoice_date']);
                                    $invoiceValue2 = round($getReceiptInvoiceLoop['rcp_inv_amount'] * $getReceiptInvoiceRate['cr_exchange_rate'], 2);
                                }else{
                                    $invoiceValue2 = $getReceiptInvoiceLoop['rcp_inv_amount'];
                                }

                                $invoiceValue = ($invoiceValue+$invoiceValue2);
                            }
                        }

                        if ($row['cur_id'] != 1) {
                            $getInvoiceRate = $curRateDB->getRateByDate($row['cur_id'], $row['record_date']);
                            $receiptValue = round($row['credit'] * $getInvoiceRate['cr_exchange_rate'], 2);
                        }else{
                            $receiptValue = $row['credit'];
                        }

                        $checkGainLoss = $receiptValue-$invoiceValue;
                        $balance = $balance - $amountCredit;

                        ///hide on 05/08/2015 - do nothing for no
                        //unhide 12/08/2015
                        if ($checkGainLoss > 0) {
                            if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                                if ($row['subject'] == 'gain') {
                                    $balance = $balance + $row['fc_seq'];
                                    $amountCredit = $amountCredit - $row['fc_seq'];
                                } elseif ($row['subject'] == 'loss') {//loss
                                    $balance = $balance - $row['fc_seq'];
                                    $amountCredit = $amountCredit + $row['fc_seq'];
                                }
                            }
                        }
                    } else
                        if ($row['txn_type'] == "Credit Note") {
                            $balance = $balance - $amountCredit;
                        } else
                            if ($row['txn_type'] == "Discount") {
                                $balance = $balance - $amountCredit;
                            } else
                                if ($row['txn_type'] == "Debit Note") {
                                    $balance = $balance + $row['debit'];
                                } else
                                    if ($row['txn_type'] == "Refund") {
                                        $balance = $balance + $amountDebit;
                                    } else
                                        if ($row['txn_type'] == "Advance Payment") {

                                            $balance = $balance - $amountCredit;

                                        } else
                                            if ($row['txn_type'] == "Refund Adjustment") {

                                                $balance = $balance - $amountCredit;

                                            } else
                                                if ($row['txn_type'] == "Discount Adjustment") {

                                                    $balance = $balance + $amountDebit;

                                                } else
                                                    if ($row['txn_type'] == "Payment Adjustment") {

                                                        $balance = $balance + $amountDebit;

                                                    } else
                                                        if ($row['txn_type'] == "Sponsor Payment") {
                                                            if (strtotime($row['record_date']) <= strtotime('2016-12-31')){
                                                                $balance = $balance - $amountCredit;

                                                                $sponsorCurAdj = ($amountCreditSponsor - $amountCredit);

                                                                if ($sponsorCurAdj != 0.00){
                                                                    $spmCurAdjStatus = true;
                                                                }
                                                            }else{
                                                                $balance = $balance - $amountCreditSponsor;
                                                                $row['credit'] = $amountCredit = $amountCreditSponsor;
                                                                $row['cur_code'] = 'MYR';
                                                                $row['cur_id'] = 1;
                                                                $row['cur_desc'] = 'Malaysia Ringgit';
                                                            }
                                                        }else if ($row['txn_type'] == "Sponsor Payment Adjustment"){
                                                            if (strtotime($row['record_date']) <= strtotime('2016-12-31')) {
                                                                $balance = $balance + $amountDebit;

                                                                $sponsorCurAdj = ($amountDebitSponsor - $amountDebit);

                                                                if ($sponsorCurAdj != 0.00) {
                                                                    $spmCurAdjStatus = true;
                                                                }
                                                            }else{
                                                                $balance = $balance + $amountDebitSponsor;
                                                                $row['debit'] = $amountDebit = $amountDebitSponsor;
                                                                $row['cur_code'] = 'MYR';
                                                                $row['cur_id'] = 1;
                                                                $row['cur_desc'] = 'Malaysia Ringgit';
                                                            }
                                                        }else if ($row['txn_type'] == "Currency Adjustment"){
                                                            $balance = $balance - $amountCredit;
                                                            $balance = $balance + $amountDebit;
                                                        }

                $results2[] = $row;

                $amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
                $amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
                $amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');

                if ($spmCurAdjStatus == true){
                    if ($row['txn_type'] == "Sponsor Payment") {
                        $balance = $balance - ($sponsorCurAdj);
                    }else{
                        $balance = $balance + ($sponsorCurAdj);
                    }

                    $results2[] = array(
                        'id' => $row['id'],
                        'record_date' => '2017-04-26',
                        'description' => $row['description'],
                        'txn_type' => 'Sponsor Payment Currency Adjustment',
                        'debit'=> str_replace('-', '', number_format(($sponsorCurAdj < 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
                        'credit' => str_replace('-', '', number_format(($sponsorCurAdj > 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
                        'document' => $row['document'].'-CURADV',
                        'invoice_no' => $row['invoice_no'].'-CURADV',
                        'receipt_no' => $row['receipt_no'].'-CURADV',
                        'subject' => $row['subject'],
                        'fc_seq' => $row['fc_seq'],
                        'migrate_desc' => $row['migrate_desc'].' Currency Adjustment',
                        'migrate_code' => $row['migrate_code'],
                        'exchange_rate' => $row['exchange_rate'],
                        'cur_code' => 'MYR',
                        'cur_id' => 1,
                        'cur_desc' => 'Malaysia Ringgit'
                    );

                    if ($sponsorCurAdj > 0.00){
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = 0.00;
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = str_replace('-', '', $sponsorCurAdj);
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
                    }else{
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = str_replace('-', '', $sponsorCurAdj);
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = 0.00;
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
                    }
                }
            }
        }

        /*if (count($results2) > 0) {
            $this->aasort($results2, 'record_date');
        }*/
        $this->view->account = $results2;
        //dd($results2);
        //dd($amountCurr);

//		    $curAvailable = array_unique($curencyArray);
        /*if($trans_id == 3258){
		    echo "<pre>";
			print_r($amountCurr);
		}*/
//			exit;
//echo "<pre>";
//			print_r($results);
        $curAvailable = array('2' => 'USD', '1' => 'MYR');

        $this->view->availableCurrency = $curAvailable;
        $this->view->amountCurr = $amountCurr;
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }

    public function invoiceAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $appl_id = $this->_getParam('id', null);
        $this->view->appl_id = $appl_id;

        $type = $this->_getParam('type', null);
        $this->view->type = $type;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(
                array('im' => 'invoice_main'),
                array(
                    'id' => 'im.id',
                    'billing' => 'im.bill_number',
                    'payer' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'description' => 'im.bill_description',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'bill_number',
                    'paid' => 'bill_paid',
                    'balance' => 'bill_balance',
                    'cn' => 'im.cn_amount',
                    'currency' => 'c.cur_symbol_prefix',
                    'migratecode' => 'im.MigrateCode',
                    'not_include' => 'im.not_include',
                    'status_inv' => 'im.status'
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc', 'cur_symbol_prefix'))
            ->joinLeft(array('cn'=>'credit_note'), 'cn.cn_invoice_id = im.id', array())
            //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id AND cnn.cn_status="A")')
            ->where("im.status NOT IN ('X','0')")//display ALL status except X,0=entry (24/27/07/2015)
            ->order('im.invoice_date')
            ->order('im.id')
            ->group('im.id');

        if ($type == 1) {
            $select->where('im.trans_id = ?', $appl_id);
        } elseif ($type == 2) {
            $select->where('im.IdStudentRegistration = ?', $appl_id);
        }


        $row = $db->fetchAll($select);

        //calculate invoice paid
        if ($row) {
            $paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
            $creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
            $receiptInvoiceDB = new Studentfinance_Model_DbTable_ReceiptInvoice();
            $invoiceSubjectDB = new Studentfinance_Model_DbTable_InvoiceSubject();
            $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();


            foreach ($row as $index => $invoice) {
//				$payment_record = $paymentMainDb->getAllInvoicePaymentRecord($invoice['billing']);
                $payment_record = $receiptInvoiceDB->getAllInvoicePaymentRecord($invoice['id'], 1);
                $row[$index]['payment'] = $payment_record;

                //get invoice details
                $invoiceDetailData = $invoiceDetailDB->getInvoiceDetail($invoice['id']);
                $row[$index]['invoice_detail'] = $invoiceDetailData;

                $m = 0;
                foreach ($invoiceDetailData as $det) {
                    //get invoice subject
                    $row[$index]['invoice_detail'][$m]['invoice_subject'] = $invoiceSubjectDB->getData($invoice['id'], $det['id']);
                    $m++;
                }


                $creditNote = $creditNoteDb->getAllDataByInvoice($invoice['id']);
                if ($creditNote) {
                    $row[$index]['credit_note'] = $creditNote;
                }

                /*//calculate if payment made and paid is null or 0.00 and no credit note
				if( ($invoice['paid']==null || $invoice['paid']==0.00) && $payment_record && $creditNote==null){
					
					//initialy set balance to invoice amount
					$row[$index]['balance'] = $row[$index]['debit'];
						
					//loop all payment record
					foreach ($payment_record as $pmt_record){
						
						//check if payment having excess amount
						if($pmt_record['rcp_amount'] > $row[$index]['debit']){
							
							$amount_paid = $row[$index]['debit'];
							
							$row[$index]['paid'] = $amount_paid;
							$row[$index]['balance'] = $row[$index]['balance'] - $amount_paid;
						}else{
							
							$amount_paid = $pmt_record['rcp_amount'];
							
							$row[$index]['paid'] = $row[$index]['paid'] + $amount_paid;
							$row[$index]['balance'] = $row[$index]['balance'] - $amount_paid;
						}
					}
					
					//mark update
					$row[$index]['update'] = 1;
					$row[$index]['update_record'] = "paid";
				}else
				//do correction if paid is null and balance is null
				if( $invoice['paid']==null && $invoice['balance']==null ){
					$row[$index]['paid'] = 0;
					$row[$index]['balance'] = $row[$index]['debit'];
					
					//mark update
					$row[$index]['update'] = 1;
					$row[$index]['update_record'] = "init";
				}else{
					//mark do not update
					$row[$index]['update'] = 0;
					$row[$index]['update_record'] = "nan";
				}*/

                //check for credit note
                /*if( $creditNote && ( $invoice['cn']==0.00 || $invoice['cn']==null || $invoice['cn']=="" ) ){

					$sum_cn = 0;
					foreach ($creditNote as $cn){
						if( $cn['cn_cancel_date']==null && isset($cn['cn_approve_date']) ){
							$sum_cn += $cn['cn_amount'];
						}
					}
					$row[$index]['cn'] = $sum_cn;
					$row[$index]['balance'] = $row[$index]['balance'] - $sum_cn;
					
					/*if($row[$index]['balance']<0){
						$row[$index]['balance'] = 0.00;
					}
					
					$row[$index]['update'] = $row[$index]['update'] || 1;
					$row[$index]['update_record'] .= " CN";
					
				}else{
					if(!isset($row[$index]['cn'])){
						$row[$index]['cn'] = 0.00;
						$row[$index]['update'] = $row[$index]['update'] || 1;
						$row[$index]['update_record'] .= " CN";
					}else{
						$row[$index]['update'] = $row[$index]['update'] || 0;
						$row[$index]['update_record'] .= " XCN";
					}
					
				}*/

            }
        }


        //process update invoice's balance and paid
        /*$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		foreach ($row as $invoice){
			if($invoice['update']==1){
				$data = array(
					'bill_paid' => $invoice['paid'],
					'bill_balance' => $invoice['balance'],
					'cn_amount' => $invoice['cn']
				);
				 
				//2015-09-02 - comment update juta2
//				$invoiceMainDb->update($data,'id = '.$invoice['id']);
			}
		}*/

        if (!$row) {
            $row = null;
        }


        //get payment, advance payment detail and cn info
        $advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

        if ($row) {
            foreach ($row as $index => $invoice) {
                $row[$index]['payment_record'] = $receiptInvoiceDB->getAllInvoicePaymentRecord($invoice['id'], 1);
                $row[$index]['advance_payment_detail'] = $advancePaymentDetailDb->getAllBillPayment($invoice['id']);
                $row[$index]['cn_info'] = $creditNoteDb->getAllDataByInvoice($invoice['id']);

            }
        }

		//echo "<pre>";
		//print_r($row); exit;

        $this->view->invoice = $row;
    }

    public function paymentAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $appl_id = $this->_getParam('id', null);
        $this->view->appl_id = $appl_id;
        $type = $this->_getParam('type', null);
        $this->view->type = $type;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'billing_no' => 'pm.rcp_no',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => 'pm.rcp_description',
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'pm.rcp_amount',
                    'currency' => 'c.cur_symbol_prefix'
                )
            )
            ->joinLeft(array('rci' => 'receipt_invoice'), 'rci.rcp_inv_rcp_id=pm.rcp_id', array(''))
            ->joinLeft(array('iv' => 'invoice_main'), 'rci.rcp_inv_invoice_id=iv.id and iv.IdStudentRegistration = pm.rcp_idStudentRegistration', array('iv.bill_number', 'GROUP_CONCAT(iv.bill_number) as invoice_no'))
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc', 'cur_symbol_prefix'))
            ->where("pm.rcp_status = 'APPROVE'");

        if ($type == 1) {
            $select->where('pm.rcp_trans_id = ?', $appl_id);
        } elseif ($type == 2) {
            $select->where('pm.rcp_idStudentRegistration = ?', $appl_id);
        }
        $select->group('pm.rcp_id');
        $row = $db->fetchAll($select);

        if (!$row) {
            $row = null;
        }

        $this->view->payment = $row;
    }

    public function creditNoteAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $appl_id = $this->_getParam('id', null);
        $this->view->appl_id = $appl_id;
        $type = $this->_getParam('type', null);
        $this->view->type = $type;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn_id',
                    'billing_no' => 'cn_billing_no',
                    'record_date' => 'cn_create_date',
                    'description' => 'cn_description',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'currency' => 'c.cur_symbol_prefix'
                )
            )
            ->joinLeft(array('i' => 'invoice_main'), 'i.id=cn.cn_invoice_id', array('invoice_no' => 'i.bill_number'))
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc', 'cur_symbol_prefix'))
            ->where("cn.cn_status = 'A'")
            //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(i.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0)) OR i.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=i.id AND cnn.cn_status="A")')
            ->order("cn_create_date asc");

        if ($type == 1) {
            $select->where('cn.cn_trans_id = ?', $appl_id);
        } elseif ($type == 2) {
            $select->where('cn.cn_IdStudentRegistration = ?', $appl_id);
        }
        $select->group('cn.cn_id');
        $row = $db->fetchAll($select);

        if (!$row) {
            $row = null;
        }

        $this->view->payment = $row;
    }

    public function discountNoteAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $appl_id = $this->_getParam('id', null);
        $this->view->appl_id = $appl_id;
        $type = $this->_getParam('type', null);
        $this->view->type = $type;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(
                array('dn' => 'discount'), array(
                    'id' => 'dcnt_id',
                    'billing_no' => 'dcnt_fomulir_id',
                    'record_date' => 'dcnt_approve_date',
                    'description' => 'dcnt_description',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'dcnt_amount',
                    'currency' => 'c.cur_symbol_prefix'
                )
            )
//						->joinLeft(array('i'=>'discount_detail'),'i.dcnt_id=dn.dcnt_id', array('dcnt_invoice_id'))
//							->joinLeft(array('iv'=>'invoice_main'),'iv.id = i.dcnt_invoice_id',array('iv.bill_number','GROUP_CONCAT(iv.bill_number) as invoice_no'))

            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc', 'cur_symbol_prefix'))
            ->where("dn.dcnt_status = 'A'");

        if ($type == 1) {
            $select->where('dn.dcnt_txn_id = ?', $appl_id);
        } elseif ($type == 2) {
            $select->where('dn.dcnt_IdStudentRegistration = ?', $appl_id);
        }
//			$select->group('dn.dcnt_id')		;
//			$select->group('iv.id')		;
        $select->group('dn.dcnt_id');
        $row = $db->fetchAll($select);
        $m = 0;
        foreach ($row as $data) {
            $selectData2 = $db->select()
                ->from(array('a' => 'discount_detail'), array())
                ->joinLeft(array('i' => 'invoice_main'), 'a.dcnt_invoice_id=i.id', array('bill_number'))
                ->where('a.dcnt_id = ?', $data['id'])
                ->group('i.id');
            $row2 = $db->fetchAll($selectData2);
            $row[$m]['invoice_no'] = $row2;
            $m++;

        }

        if (!$row) {
            $row = null;
        }

        $this->view->payment = $row;
    }

    public function printAction()
    {

        if ($this->_request->isPost()) {

            $formData = $this->getRequest()->getPost();

            $type = $formData['type'];
            $chk = $formData['download'];
            $n = count($chk);

            $i = 0;
            while ($i < $n) {

                $trans_id = $chk[$i];

                if ($type == 1) {
                    $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
                    $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
                    $appl_id = $profile['appl_id'];
                } elseif ($type == 2) {
                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                    $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
                }

                $fieldValues['[Name]'] = $profile['name'];
                $fieldValues['[ID]'] = $profile['at_pes_id'];
                $fieldValues['[Fee Structure]'] = $profile['fs_name'];
                $fieldValues['[Intake]'] = $profile['IntakeDesc'];
                $fieldValues['[Sponsorship]'] = $profile['typeScholarshipName'];
                $fieldValues['[ID]'] = $profile['at_pes_id'];
                $fieldValues['[ID]'] = $profile['at_pes_id'];
                $fieldValues['[ID]'] = $profile['at_pes_id'];


                //account
                $db = Zend_Db_Table::getDefaultAdapter();

                $select_invoice = $db->select()
                    ->from(array('im' => 'invoice_main'), array(
                            'id' => 'im.id',
                            'record_date' => 'im.invoice_date',
                            'description' => 'fc.fi_name',
                            'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                            'debit' => 'bill_amount',
                            'credit' => new Zend_Db_Expr ('"0.00"'),
                            'document' => 'bill_number',
                            'invoice_no' => 'bill_number',
                            'receipt_no' => new Zend_Db_Expr ('"-"'),
                            'subject' => 's.SubCode',
                            'fc_seq' => 'im.id',
                            'migrate_desc' => 'bill_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => 'im.exchange_rate',
                        )
                    )
                    ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
                    ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
                    ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
                    ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
                    //->where('im.currency_id= ?',$currency)						  

                    ->where("im.status NOT IN ('X','0')")//display ALL status except X,0=entry (24/27/07/2015)
                    ->group('im.id');
                if ($type == 1) {
                    $select_invoice->where('im.trans_id = ?', $trans_id);
                } else {
                    $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
                }


                $select_payment = $db->select()
                    ->from(
                        array('pm' => 'receipt'), array(
                            'id' => 'pm.rcp_id',
                            'record_date' => 'pm.rcp_receive_date',
                            'description' => 'pm.rcp_description',
                            'txn_type' => new Zend_Db_Expr ('"Payment"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => 'rcp_amount',
                            'document' => 'rcp_gainloss_soa',
                            'invoice_no' => 'rcp_no',
                            'receipt_no' => 'rcp_no',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'pm.rcp_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => new Zend_Db_Expr ('""'),
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    //->where('pm.rcp_cur_id= ?',$currency)		
                    ->where("pm.rcp_status = 'APPROVE'");
                if ($type == 1) {
                    $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
                } else {
                    $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
                }

                $select_credit = $db->select()
                    ->from(
                        array('cn' => 'credit_note'), array(
                            'id' => 'cn.cn_id',
                            'record_date' => 'cn.cn_create_date',
                            'description' => 'cn.cn_description',
                            'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => 'cn_amount',
                            'document' => 'cn_billing_no',
                            'invoice_no' => 'cn_billing_no',
                            'receipt_no' => 'cn_billing_no',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'cn.cn_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => 'i.exchange_rate',
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
                    //->where('pm.rcp_cur_id= ?',$currency)		
                    ->where("cn.cn_status = 'A'");
                if ($type == 1) {
                    $select_credit->where('cn.cn_trans_id = ?', $trans_id);
                } else {
                    $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
                }

                //get array payment				
                $select = $db->select()
                    ->union(array($select_invoice, $select_payment, $select_credit), Zend_Db_Select::SQL_UNION_ALL)
                    ->order("record_date asc")
                    ->order("txn_type asc");

                $results = $db->fetchAll($select);


                if (!$results) {
                    $results = null;
                }


                //process data
                $amountCurr = array();
                $curencyArray = array();
                $balance = 0;

                if ($results) {
                    foreach ($results as $row) {
                        $curencyArray[$row['cur_id']] = $row['cur_code'];

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();


                        if ($row['exchange_rate']) {
                            $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                        } else {
                            $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
//								$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                        }

                        //					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
                        //					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
                        //					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];
                        if ($row['cur_id'] == 2) {
                            $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                            $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                        } else {
                            $amountDebit = $row['debit'];
                            $amountCredit = $row['credit'];
                        }
                        //					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;

                        if ($row['txn_type'] == "Invoice") {
                            $balance = $balance + $amountDebit;
                        } else
                            if ($row['txn_type'] == "Payment") {

                                $balance = $balance - $amountCredit;

                            } else
                                if ($row['txn_type'] == "Credit Note") {
                                    $balance = $balance - $amountCredit;
                                } else
                                    if ($row['txn_type'] == "Debit Note") {
                                        $balance = $balance + $row['debit'];
                                    } else
                                        if ($row['txn_type'] == "Refund") {
                                            $balance = $balance + $row['debit'];
                                        }


                        $amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
                        $amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
                        $amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');

                    }
                }


                $curAvailable = array('2' => 'USD', '1' => 'MYR');


                //table account statement
                $availableCur = count($curAvailable);
                if ($availableCur == 1) {
                    $colspan = "";
                } else {
                    $colspan = "colspan = $availableCur";
                }

                $tableAccount = '<table id="tfhover" class="tftable" border="1">
				<thead>
				<tr>
					<th width="3%"  rowspan="2"></th>
					<th width="12%" rowspan="2">Date</th>
					<th width="10%" rowspan="2">Transaction Type</th>
					<th width="10%" rowspan="2">Reference No</th>				
					<th width="20%" rowspan="2">Description</th>
					<th width="10%" ' . $colspan . '>Debit</th>
					<th width="5%">Credit</th>
					<th width="5%">Balance</th>
				</tr><tr>';

                for ($a = 1; $a <= 1; $a++) {
                    foreach ($curAvailable as $key => $cur) {
                        $tableAccount .= '<th width="5%">' . $cur . '</th>';
                    }
                }
                $tableAccount .= '<th width="5%">MYR</th>
			<th width="5%">MYR</th>
			</tr>
			</thead>
			<tbody>';

                $i = 1;

                if ($results) {

                    $balance = 0;
                    $totalDebit = 0;
                    foreach ($results as $entry):

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                        $arrayDebit = array();
                        if ($entry['cur_id'] == 2) {
                            $dataCur = $curRateDB->getData($entry['exchange_rate']);
                            $amountDebit = number_format($entry['debit'] * $dataCur['cr_exchange_rate'], 2);
                            $arrayDebit['USD'] = $entry['debit'];
                            $arrayDebit['MYR'] = $amountDebit;
                            $totalDebit = $amountDebit;


                        }

                        if ($entry['cur_id'] == 1) {
                            $dataCur = $curRateDB->getCurrentExchangeRate(2);
                            $amountDebit = number_format($entry['debit'] / $dataCur['cr_exchange_rate'], 2);
                            $arrayDebit['MYR'] = $entry['debit'];
                            $arrayDebit['USD'] = $amountDebit;
                            $totalDebit = $entry['debit'];

                        }


                        if ($entry['txn_type'] == "Invoice") {

                            $balance = $balance + $totalDebit;

                        } elseif ($entry['txn_type'] == "Proforma") {

                            $balance = $balance + $totalDebit;

                        } else
                            if ($entry['txn_type'] == "Payment") {

                                if ($entry['cur_id'] == 2) {
                                    $amountCredit = round($entry['credit'] * $dataCur['cr_exchange_rate'], 2);
                                }

                                if ($entry['cur_id'] == 1) {
                                    $amountCredit = $entry['credit'];
                                }

                                $balance = $balance - $amountCredit;
                            } else
                                if ($entry['txn_type'] == "Credit Note") {
                                    $balance = $balance - $entry['credit'];
                                } else
                                    if ($entry['txn_type'] == "Debit Note") {
                                        $balance = $balance + $entry['debit'];
                                    } else
                                        if ($entry['txn_type'] == "Refund") {
                                            $balance = $balance + $entry['debit'];
                                        }

                        $dateCreate = date("d-m-Y", strtotime($entry['record_date']));
                        $tableAccount .= '<tr>
				<td align="center">' . $i . '</td>
				<td align="center">' . $dateCreate . '</td>
				<td>' . $entry['txn_type'] . '</td>
				<td>' . $entry['invoice_no'] . '</td>';

                        if ($entry['migrate_code'] != '') {
                            $description = $entry['migrate_desc'];
                        } else {
                            $description = ($entry['subject']) ? $entry['subject'] . ' - ' : '';
                            $description .= $entry['description'];
                        }

                        $tableAccount .= '<td>' . $description . '</td>';

                        //debit
                        $color = '#000';
                        if ($entry['txn_type'] == 'Invoice' || $entry['txn_type'] == 'Proforma') {
                            if ($curAvailable) {
                                foreach ($curAvailable as $key => $cur) {

                                    foreach ($arrayDebit as $keyD => $dbt) {

                                        if ($key == $entry['cur_id']) {
                                            $color = 'green';
                                        } else {
                                            $color = '#000';
                                        }

                                        if ($cur == $keyD) {
                                            $tableAccount .= '<td align="right"><font color="' . $color . '">' . $dbt . '</font></td>';
                                        }

                                    }

                                }
                            }
                        } else {
                            $tableAccount .= '<td align="right">0.00</td>';
                            $tableAccount .= '<td align="right">0.00</td>';
                        }

                        if ($amountCurr[$entry['invoice_no']]['MYR']['credit']) {
                            $credit = '0.00';
                        } else {
                            $credit = number_format($amountCurr[$entry['invoice_no']]['MYR']['credit'], 2);
                        }

                        if ($amountCurr[$entry['invoice_no']]['MYR']['balance']) {
                            $balance = '0.00';
                        } else {
                            $balance = number_format($amountCurr[$entry['invoice_no']]['MYR']['balance'], 2);
                        }

                        $tableAccount .= '<td align="right">' . $credit . '</td>
			
						<td align="right">' . $balance . '</td>
			
			</tr>';
                        $i++;
                    endforeach;
                } else {

                    $tableAccount .= '<tr><td colspan="9">There is no record available</td></tr>';
                }
                $tableAccount .= '</tbody></table>';

                require_once '../library/dompdf/dompdf_config.inc.php';
                $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
                $autoloader->pushAutoloader('DOMPDF_autoload');

                $fieldValues['[SOA]'] = $tableAccount;
                $templateDb = new Communication_Model_DbTable_Template();
                $template = $templateDb->getTemplateContent(94, $this->view->locale);

                $html = $template['tpl_content'];

                $templateCMS = new Cms_TemplateTags();

                if ($type == 645) {
                    $html = $templateCMS->parseContent($trans_id, $template['tpl_id'], $template['tpl_content']);
                } elseif ($type == 646) {
                    $html = $templateCMS->parseTag($trans_id, $template['tpl_id'], $template['tpl_content']);
                }


                //replace variable
                foreach ($fieldValues as $key => $value) {
                    $html = str_replace($key, $value, $html);
                }

//				echo $html;
//				exit;
                $option = array(
                    'content' => $html,
                    'save' => false,
                    'file_name' => 'invoice',
                    'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}',

                    'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 180; 
					  $img_h = 59;
                      $pdf->image("images/logo_text_high.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
                    'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
                );

                $pdf = generatePdf($option);


                $i++;
            }

            exit;

        }
    }

    public function runScriptAction()
    {

        $invoiceClass = new icampus_Function_Studentfinance_Invoice();

        /*
		 * script to update fee structure id for applicant 
		 */

        /*
    	$invoiceClass->updateFeeStructureApplicant();
    	exit;*/


        /* TODO / Not tested yet
		 * script to update fee structure id for student 
		 */

        /*$invoiceClass->updateFeeStructureStudent();
    	exit;*/

        /*
		 * Script to calculate invoice amount for course registration
		 * Parameter in: Student ID, Semester, Subject Code, Registration Item)
		 * Parameter out : Amount
		 *  1.	Cek  fee structure id (if ada proceed no 2, else assign fee structure)
			2.	Cara calculation : Cek  dekat Fee � Detail tab, 
				a.	Registration item field
				b.	Amount
				c.	Currency
				d.	Calculation type  eg, by credit hour ke, per subject ke
		 * 
		 */

        $studentID = 3970;
        $courseID = 106;
        $registrationItem = 5;

        $invoiceClass->calculateInvoiceAmountByCourse($studentID, $courseID, $registrationItem);

    }

    public function printInvoiceAction()
    {
        $this->view->title = 'Invoice';

//		$student = $this->studentinfo;

        $invoiceID = $this->_getParam('id', null);
        $type = $this->_getParam('type', null);

        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceDetail();
        $invoice = $invoiceMainDB->getDetail($invoiceID);

        if ($invoice[0]['IdStudentRegistration']) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $student = $studentRegistrationDb->getTheStudentRegistrationDetail($invoice[0]['IdStudentRegistration']);
            $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($invoice[0]['IdStudentRegistration']);
        } else {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $student = $applicantProfileDb->getApplicantRegistrationDetail($invoice[0]['trans_id']);
        }


        if ($type == null) {
            $address = trim($student["appl_caddress1"]);
            if ($student["appl_caddress2"]) {
                $address .= '<br/>' . $student["appl_caddress2"] . ' ';
            }
            if ($student["appl_caddress3"]) {
                $address .= '<br/>' . $student["appl_caddress3"] . ' ';
            }
            if ($student["appl_cpostcode"]) {
                $address .= '<br/>' . $student["appl_cpostcode"] . ' ';
            }
            if ($student["appl_ccity"] != 99) {
                $address .= '<br />' . $student["CCityName"] . ' ';
            } else {
                $address .= '<br/ >' . $student["appl_ccity_others"] . ' ';
            }
            if ($student["appl_cstate"] != 99) {
                $address .= $student["CStateName"] . ' ';
            } else {
                $address .= $student["appl_cstate_others"];
            }
            $address .= '<br/>' . $student["CCountryName"];
        }else{
            $sponsormodel = new Studentfinance_Model_DbTable_Sponsor();
            $sponsorInfo = $sponsormodel->getSponsorInfo($taggingFinancialAid['Sponsorshipid']);

            $address = $sponsorInfo['Add1']!= '' ? $sponsorInfo['Add1']:'';
            $address .= $sponsorInfo['Add2']!= '' ? '<br />'.$sponsorInfo['Add2']:'';
            $address .= $sponsorInfo['zipCode']!= '' ? '<br />'.$sponsorInfo['zipCode']:'';
            $address .= $sponsorInfo['CityName']!= '' && $sponsorInfo['CityName']!=null ? '<br />'.$sponsorInfo['CityName']:'';
            $address .= $sponsorInfo['StateName']!= '' && $sponsorInfo['StateName']!=null ? '<br />'.$sponsorInfo['StateName']:'';
            $address .= $sponsorInfo['CountryName']!= '' && $sponsorInfo['CountryName']!=null ? '<br />'.$sponsorInfo['CountryName']:'';
        }

        $fieldValues = array(
            '[Name]' => $type == null ? strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"]):strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"].'<br />'.$sponsorInfo['fName']),
            '[Address]' => strtoupper($address),
            '[Date]' => date('d F Y', strtotime($invoice[0]["invoice_date"])),
            '[ID]' => isset($student["registrationId"]) ? strtoupper($student["registrationId"]):strtoupper($student["at_pes_id"]),
            '[NRIC]' => strtoupper($student["appl_idnumber"]),
            '[Amount in Text]' => '',
            '[Ref No]' => strtoupper($invoice[0]["bill_number"]),
            '[Programme]' => strtoupper($student["ProgramName"]),
            '[Mode of Study]' => strtoupper($student["StudyMode"]),
            '[Semester]' => strtoupper($invoice[0]["SemesterMainName"]),
            '[DatePrint]' => date('d F Y'),
//			 '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
        );


        $currency = new Zend_Currency(array('currency' => $invoice[0]['currency_id']));

        $totalInvoice = 0;
        $totalNet = 0;
        $totalDisc = 0;
        $totalCn = 0;

        $processing_fee = "<table id='tfhover' class='tftable' border='1'>";
        $processing_fee .= "<thead>";
        $processing_fee .= "<tr>";
        $processing_fee .= "<th>Item Code</th>";
        $processing_fee .= "<th>Description</th>";
        $processing_fee .= "<th>Type</th>";
        $processing_fee .= "<th>Amount (" . $invoice[0]['cur_code'] . ")</th>";
        $processing_fee .= "<th>Discount Amount</th>";
        $processing_fee .= "<th>Credit Note Amount</th>";
        $processing_fee .= "<th>Nett Amount</th>";
        $processing_fee .= "</tr>";
        $processing_fee .= "</thead>";
        $processing_fee .= "<tbody>";

        if (count($invoice) > 0) {
            $a = 0;

            foreach ($invoice as $proformaInfoLoop) {
                $desc = '';
                if ($proformaInfoLoop['IdSubject']) {
                    $desc .= $proformaInfoLoop['SubCode'] . ' ';
                    $desc .= $proformaInfoLoop['fi_name'];
//                        		$desc .= ' - '.$proformaInfoLoop['item_name'].' ';
                    $amount = $proformaInfoLoop['amount_subject'];
                    //$amount = $proformaInfoLoop['amount'];
                    $discamount = $proformaInfoLoop['dcnt_amount'];
                } elseif ($proformaInfoLoop["MigrateCode"] != "" || $proformaInfoLoop['bill_description']!='') {
                    $desc = $proformaInfoLoop['bill_description'];
                    $amount = $proformaInfoLoop['amount'];
                    $discamount = $proformaInfoLoop['dcnt_amount'];
                } else {
                    $desc .= $proformaInfoLoop['fi_name'];
                    $amount = $proformaInfoLoop['amount'];
                    $discamount = $proformaInfoLoop['dcnt_amount'];
                }

                $cnPaid = Studentfinance_Model_DbTable_PaymentAmount::getCreditNoteDtl($proformaInfoLoop['id']);

                $cnAmount = 0.00;
                if (isset($cnPaid[0]['cnpaid']) && $cnPaid[0]['cnpaid'] != null){
                    $cnAmount =  str_replace(',', '', $cnPaid[0]['cnpaid']);
                }

                $nettamount = $amount - $discamount - $cnAmount;
                $discamount = $discamount ? $discamount : '0.00';

                $processing_fee .= "<tr>";
                $processing_fee .= "<td>" . $student["ProgramCode"] . "</td>";
                $processing_fee .= "<td>" . $student["ProgramName"] . "</td>";
                $processing_fee .= "<td>" . $desc . "</td>";
                $processing_fee .= "<td align='right'>" . $amount . "</td>";
                $processing_fee .= "<td align='right'>" . $discamount . "</td>";
                $processing_fee .= "<td align='right'>" . number_format($cnAmount, 2) . "</td>";
                $processing_fee .= "<td align='right'>" . number_format($nettamount, 2) . "</td>";
                $processing_fee .= "</tr>";

                $totalInvoice = $totalInvoice + $amount;
                $totalNet = $totalNet + $nettamount;
                $totalDisc = $totalDisc + $discamount;
                $totalCn = $totalCn + $cnAmount;

                $a++;
            }
        }
        $processing_fee .= "<tr><td colspan='3' align='right'>Total : </td>";
        $processing_fee .= "<td align='right'>" . sprintf("%.2f", $totalInvoice) . "</td>";
        $processing_fee .= "<td align='right'>" . sprintf("%.2f", $totalDisc) . "</td>";
        $processing_fee .= "<td align='right'>" . sprintf("%.2f", $totalCn) . "</td>";
        $processing_fee .= "<td align='right'>" . sprintf("%.2f", $totalNet) . "</td>";
        $processing_fee .= "</tr>";
        $processing_fee .= "</tbody>";
        $processing_fee .= "</table>";
        //echo $processing_fee;
        //exit;
        $fieldValues['[Invoice]'] = $processing_fee;
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $templateDb = new Communication_Model_DbTable_Template();
        $template = $templateDb->getTemplateContent(83, 'en_US');

        $html = $template['tpl_content'];

        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

//		echo $html;exit;
        $option = array(
            'content' => $html,
            'save' => false,
            'file_name' => 'invoice',
            'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',

            'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 180; 
					  $img_h = 59;
                      $pdf->image("images/logo_text_high.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
            'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
        );

        $pdf = generatePdf($option);

        exit;
    }


    public function printReceiptAction()
    {
        $this->view->title = 'Receipt';

//		$student = $this->studentinfo;

        $receiptID = $this->_getParam('id', null);
        $type = $this->_getParam('type', null);

        $receiptDB = new Studentfinance_Model_DbTable_Receipt();
        $receipt = $receiptDB->getData($receiptID);

        $receipt = $receipt[0];

        if ($receipt['rcp_idStudentRegistration']) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $student = $studentRegistrationDb->getTheStudentRegistrationDetail($receipt['rcp_idStudentRegistration']);
            $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($receipt['rcp_idStudentRegistration']);
        } else {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $student = $applicantProfileDb->getApplicantRegistrationDetail($receipt['rcp_trans_id']);
        }


        if ($type==null) {
            $address = trim($student["appl_caddress1"]);
            if ($student["appl_caddress2"]) {
                $address .= '<br/>' . $student["appl_caddress2"] . ' ';
            }
            if ($student["appl_cpostcode"]) {
                $address .= '<br/>' . $student["appl_cpostcode"] . ' ';
            }
            if ($student["appl_ccity"] != 99) {
                $address .= '<br />' . $student["CCityName"] . ' ';
            } else {
                $address .= '<br/ >' . $student["appl_ccity_others"] . ' ';
            }
            if ($student["appl_cstate"] != 99) {
                $address .= $student["CStateName"] . ' ';
            } else {
                $address .= $student["appl_cstate_others"];
            }
            $address .= '<br/>' . $student["CCountryName"];
        }else{
            $sponsormodel = new Studentfinance_Model_DbTable_Sponsor();
            $sponsorInfo = $sponsormodel->getSponsorInfo($taggingFinancialAid['Sponsorshipid']);

            $address = $sponsorInfo['Add1']!= '' ? $sponsorInfo['Add1']:'';
            $address .= $sponsorInfo['Add2']!= '' ? '<br />'.$sponsorInfo['Add2']:'';
            $address .= $sponsorInfo['zipCode']!= '' ? '<br />'.$sponsorInfo['zipCode']:'';
            $address .= $sponsorInfo['CityName']!= '' && $sponsorInfo['CityName']!=null ? '<br />'.$sponsorInfo['CityName']:'';
            $address .= $sponsorInfo['StateName']!= '' && $sponsorInfo['StateName']!=null ? '<br />'.$sponsorInfo['StateName']:'';
            $address .= $sponsorInfo['CountryName']!= '' && $sponsorInfo['CountryName']!=null ? '<br />'.$sponsorInfo['CountryName']:'';
        }


        $fieldValues = array(
            '[Applicant Name]' => $type==null ? strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"]):strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"].'<br />'.$sponsorInfo['fName']),
            '[Applicant Address]' => strtoupper($address),
            '[Date]' => date('d F Y', strtotime($receipt["rcp_receive_date"])),
            '[Applicant ID]' => isset($student["registrationId"]) ? strtoupper($student["registrationId"]):strtoupper($student["at_pes_id"]),
            '[DatePrint]' => date('d F Y'),

//			 '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
        );

        $currency = new Zend_Currency(array('currency' => $receipt['cur_code']));

        //invoice
        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
        $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($receiptID);

        $inv = array();
        $total_payment = 0.00;

        if ($invoice_list) {

            foreach ($invoice_list as $invoice) {
                $iv = array(
                    'invoice_no' => $invoice['bill_number'],
                    'amount' => 0,
                    'currency' => $invoice['cur_symbol_prefix']
                );

                foreach ($invoice['receipt_invoice'] as $ri) {
                    $iv['amount'] += $ri['rcp_inv_amount'];
                    $total_payment += $ri['rcp_inv_amount'];

                    $feeID = $ri['fi_id'];
                    $receipt_no = $ri['rcp_inv_rcp_no'];
                    $bill_paid = $ri['rcp_inv_amount'];
                    $currencyPaid = $ri['cur_symbol_prefix'];
                }

                $inv[] = $iv;
            }
        }

        //non-invoice
        $nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
        $non_invoice_list = $nonInvoiceDb->getNonInvoiceFromReceipt($receiptID);

        if ($non_invoice_list) {

            foreach ($non_invoice_list as $non_invoice) {
                $iv = array(
                    'invoice_no' => $non_invoice['fi_name'],
                    'amount' => $non_invoice['ni_amount'],
                    'currency' => $non_invoice['cur_symbol_prefix']
                );

                $total_payment += $non_invoice['ni_amount'];

                $inv[] = $iv;
            }
        }


        $fieldValues['[Receipt No]'] = $receipt['rcp_no'];

        $fieldValues['[Data Loop]'] = '<table width="100%" cellpadding="2" cellspacing="0" style="border:1px solid #000000;"><tr><th width="50%" style="border:1px solid #000000;">Invoice No.</th><th style="border:1px solid #000000;">Amount Paid</th></tr>';
        for ($i = 0; $i < sizeof($inv); $i++) {
            $invoice = $inv[$i];

            $fieldValues['[Data Loop]'] .= '<tr>';
            $fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">' . $invoice['invoice_no'] . '</td>';
            $fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">' . $currency->toCurrency($invoice['amount'], array('display' => false)) . '&nbsp;</td>';
            $fieldValues['[Data Loop]'] .= '</tr>';
        }

        //Excess Payment
        if ($receipt['rcp_adv_payment_amt'] > 0) {
            $fieldValues['[Data Loop]'] .= '<tr>';
            $fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">Excess Payment</td>';
            $fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">' . $currency->toCurrency($receipt['rcp_adv_payment_amt'], array('display' => false)) . '&nbsp;</td>';
            $fieldValues['[Data Loop]'] .= '</tr>';

            $total_payment = $total_payment + $receipt['rcp_adv_payment_amt'];
        }

        //
        $fieldValues['[Data Loop]'] .= '<tr><td style="border:1px solid #000000;"><b>Total</b></td><td align="right" style="border:1px solid #000000;"><b>' . $currency->toCurrency($total_payment, array('currency' => $receipt['cur_code'])) . '</b>&nbsp;</td></tr>';
        $fieldValues['[Data Loop]'] .= '</table>';

        //number to words
        $total_amount_in_text = convert_number_to_words($total_payment);


        //invoice & non-invoice data
        $fieldValues['[Amount in Text]'] = $receipt['cur_symbol_prefix'] . ": " . $total_amount_in_text . " Only";


        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $templateDb = new Communication_Model_DbTable_Template();
        $template = $templateDb->getTemplateContent(22, 'en_US');

        $html = $template['tpl_content'];

        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

//		echo $html;exit;
        $option = array(
            'content' => $html,
            'save' => false,
            'file_name' => 'receipt',
            'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',

            'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 180; 
					  $img_h = 59;
                      $pdf->image("images/logo_text_high.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
            'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
        );

        $pdf = generatePdf($option);

        exit;
    }

    public function downloadCronJobAction()
    {
        $this->_helper->layout->disableLayout();

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        if ($this->_request->isPost()) {
            $formData = $this->getRequest()->getPost();

            $type = $formData['type'];
            $dateFrom = $formData['date_from'];
            $dateTo = $formData['date_to'];
            $chk = $formData['download'];
            $n = count($chk);

            $arrayStack = array();
            $i = 0;
            while ($i < $n) {
                $trans_id = $chk[$i];
                $arrayStack[] = $trans_id;
                $i++;
            }
            $dataCron = array(
                'name' => 'Download SOA Detail',
                'action' => 'reportSoaDetail',
                'created_date' => date('Y-m-d H:i:s'),

            );

            //temporary disable on 28-03-2015
//			$db->insert('cron_service',$dataCron);
//			$idCron = $db->lastInsertId();

            $dataStack = array(
                'id_cron' => $idCron,
                'action' => 'Download SOA Detail',
                'student_id' => implode(',', array_values($arrayStack)),
                'type' => $type,
                'date_from' => $dateFrom,
                'date_to' => $dateTo,
                'program_id' => $formData['program'],
                'intake_id' => $formData['IdIntake'],
                'profile_status_id' => $formData['status'],
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $creator

            );
//			$db->insert('finance_service',$dataStack);

//			$this->_helper->flashMessenger->addMessage(array('success'=>$this->view->translate("Downloading report in progress."))); 
            //redirect
            $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'account-applicant', 'action' => 'index'), 'default', true));
        }
    }

    public function downloadAction()
    {
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
//      echo ini_get('max_input_vars');
//    	exit;
        $this->_helper->layout->disableLayout();

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        if ($this->_request->isPost()) {

            $formData = $this->getRequest()->getPost();

            $type = $formData['type'];
            $dateFrom = $formData['date_from'];
            $dateTo = $formData['date_to'];
            $chk = $formData['download'];
            $n = count($chk);

            $arrayStack = array();
            $arrayStackNew = array();
            $i = 0;
            while ($i < $n) {

                $trans_id = $chk[$i];
                $arrayStackNew[] = $trans_id;

                //title
                $this->view->title = $this->view->translate("Statement of Account");

                //profile
                if ($type == 1) {
                    $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
                    $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
                    $appl_id = $profile['appl_id'];
                } elseif ($type == 2) {
                    $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
                    $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
                }


                $this->view->profile = $profile;

//		    	echo "<pre>";
//		    	print_r($profile);
//		    	exit;
                $arrayStack[$i]['profile'] = $profile;
                //account
                $db = Zend_Db_Table::getDefaultAdapter();

                $select_invoice = $db->select()
                    ->from(array('im' => 'invoice_main'), array(
                            'id' => 'im.id',
                            'record_date' => 'im.invoice_date',
                            'description' => 'GROUP_CONCAT(fc.fi_name)',
                            'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                            'debit' => 'bill_amount',
                            'credit' => new Zend_Db_Expr ('"0.00"'),
                            'document' => 'bill_number',
                            'invoice_no' => 'bill_number',
                            'receipt_no' => new Zend_Db_Expr ('"-"'),
                            'subject' => 'GROUP_CONCAT(distinct s.SubCode)',
                            'fc_seq' => 'ivd.fi_id',
                            'migrate_desc' => 'bill_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => new Zend_Db_Expr ('"x"'),
                        )
                    )
                    ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
                    ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
                    ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
                    ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->joinLeft(array('cn'=>'credit_note'), 'cn.cn_invoice_id = im.id AND cn.cn_status = "A"', array())
                    //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id AND cnn.cn_status="A")')
                    ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id AND cnn.cn_status="A") OR im.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")')
                    //->where('im.currency_id= ?',$currency)

                    ->where("im.status NOT IN ('X', 'E', '0')")//display ALL status except X,0=entry (24/27/07/2015)
                    ->group('im.id');
                if ($type == 1) {
                    $select_invoice->where('im.trans_id = ?', $trans_id);
                } else {
                    $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
                }



                if ($dateFrom != '') {
                    $select_invoice->where("DATE(im.invoice_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_invoice->where("DATE(im.invoice_date) <= ?", $dateTo);
                }

                $select_payment = $db->select()
                    ->from(
                        array('pm' => 'receipt'), array(
                            'id' => 'pm.rcp_id',
                            'record_date' => 'pm.rcp_receive_date',
                            'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
                            'txn_type' => new Zend_Db_Expr ('"Payment"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => "rcp_amount",
                            'document' => 'rcp_gainloss_soa',
                            'invoice_no' => 'rcp_no',
                            'receipt_no' => 'rcp_no',
                            'subject' => 'rcp_gainloss',//gain/loss
                            'fc_seq' => 'rcp_gainloss_amt',//gain/loss - myr
                            'migrate_desc' => 'pm.rcp_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => new Zend_Db_Expr ('"x"'),
                        )
                    )
                    ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->joinLeft(array('a' => 'tbl_receivable_adjustment'), "a.IdReceipt = pm.rcp_id and a.Status = 'APPROVE'", array())
                    //->where('pm.rcp_cur_id= ?',$currency)		
                    ->where("pm.rcp_status = 'APPROVE' OR (pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0 AND MONTH(pm.rcp_receive_date) != MONTH(a.EnterDate))");
//							->orWhere("pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0");
                if ($type == 1) {
                    $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
                } else {
                    $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_payment->where("DATE(pm.rcp_receive_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_payment->where("DATE(pm.rcp_receive_date) <= ?", $dateTo);
                }

                $select_credit = $db->select()
                    ->from(
                        array('cn' => 'credit_note'), array(
                            'id' => 'cn.cn_id',
                            'record_date' => 'cn.cn_create_date',
                            'description' => 'cn.cn_description',
                            'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => 'cn_amount',
                            'document' => 'cn_billing_no',
                            'invoice_no' => 'cn_billing_no',
                            'receipt_no' => 'cn_billing_no',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'cn.cn_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => "IFNULL(i.exchange_rate,'x')",
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
                    ->joinLeft(array('j'=>'invoice_main'), 'cn.cn_invoice_id = j.id', array())
                    //->where('pm.rcp_cur_id= ?',$currency)
                    ->where("IFNULL(j.status, 'A') NOT IN ('X', 'E','0')")
                    ->where("cn.cn_status = 'A'")
                    //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(j.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(j.invoice_date), 0)))');
                ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(j.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(j.invoice_date), 0))) OR i.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")');

                if ($type == 1) {
                    $select_credit->where('cn.cn_trans_id = ?', $trans_id);
                } else {
                    $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_credit->where("DATE(cn.cn_create_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_credit->where("DATE(cn.cn_create_date) <= ?", $dateTo);
                }


                $select_discount = $db->select()
                    ->from(
                        array('a' => 'discount_detail'), array(
                            'id' => 'dn.dcnt_id',
                            'record_date' => 'dn.dcnt_approve_date',
                            'description' => 'dn.dcnt_description',
                            'txn_type' => new Zend_Db_Expr ('"Discount"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => 'SUM(a.dcnt_amount)',
                            'document' => 'dn.dcnt_fomulir_id',
                            'invoice_no' => 'dn.dcnt_fomulir_id',
                            'receipt_no' => 'dn.dcnt_fomulir_id',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'dn.dcnt_description',
                            'migrate_code' => new Zend_Db_Expr ('""'),
                            'exchange_rate' => 'i.exchange_rate',
                        )
                    )
                    ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
//							->joinLeft(array('adj'=>'discount_adjustment'),"adj.IdDiscount = a.id and adj.Status= 'APPROVE' AND (MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date))", array())	
                    //->where('pm.rcp_cur_id= ?',$currency)		
                    ->where("MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date)")
                    ->group('dn.dcnt_id');

                if ($type == 1) {
                    $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
                } else {
                    $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_discount->where("DATE(dn.dcnt_approve_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_discount->where("DATE(dn.dcnt_approve_date) <= ?", $dateTo);
                }


                //refund
                $select_refund = $db->select()
                    ->from(
                        array('rn' => 'refund'), array(
                            'id' => 'rfd_id',
                            'record_date' => 'rfd_date',
                            'description' => 'rfd_desc',
                            'txn_type' => new Zend_Db_Expr ('"Refund"'),
                            'debit' => 'rfd_amount',
                            'credit' => new Zend_Db_Expr ('"0.00"'),
                            'document' => 'rfd_fomulir',
                            'invoice_no' => 'rfd_fomulir',
                            'receipt_no' => 'rfd_fomulir',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'rfd_desc',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => new Zend_Db_Expr ('"x"'),
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->where("MONTH(rn.rfd_date) != MONTH(rn.rfd_cancel_date)")
                    ->where("rn.rfd_status IN  ('A','X')");

                if ($type == 1) {
                    $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
                } else {
                    $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_refund->where("DATE(rn.rfd_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_refund->where("DATE(rn.rfd_date) <= ?", $dateTo);
                }

                //advance payment from invoice deposit only
                $select_advance = $db->select()
                    ->from(
                        array('adv' => 'advance_payment'), array(
                            'id' => 'advpy_id',
                            'record_date' => 'advpy_date',
                            'description' => "CONCAT(advpy_description,' - ',fi.fi_name)",
                            'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => 'advpy_amount',
                            'document' => 'advpy_fomulir',
                            'invoice_no' => 'advpy_fomulir',
                            'receipt_no' => 'advpy_fomulir',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'advpy_description',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => "IFNULL(adv.advpy_exchange_rate,'x')",
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=adv.advpy_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->join(array('i' => 'invoice_main'), 'i.id=adv.advpy_invoice_id AND i.adv_transfer_id = adv.advpy_id', array())
                    ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = i.id', array())
                    ->join(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
                    ->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
                    ->where("i.status = 'A'")
                    ->where("adv.advpy_status = 'A'");

                if ($type == 1) {
                    $select_advance->where('adv.advpy_trans_id = ?', $trans_id);
                } else {
                    $select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_advance->where("DATE(adv.advpy_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_advance->where("DATE(adv.advpy_date) <= ?", $dateTo);
                }

                //refund cancel
                $select_refund_cancel = $db->select()
                    ->from(
                        array('rnc' => 'refund'), array(
                            'id' => 'rfd_id',
                            'record_date' => 'rfd_cancel_date',
                            'description' => 'rfd_desc',
                            'txn_type' => new Zend_Db_Expr ('"Refund Adjustment"'),
                            'debit' => new Zend_Db_Expr ('"0.00"'),
                            'credit' => 'rfd_amount',
                            'document' => 'rfd_fomulir',
                            'invoice_no' => "CONCAT(rfd_fomulir,'-',rfd_id)",
                            'receipt_no' => 'rfd_fomulir',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'rfd_desc',
                            'migrate_code' => 'MigrateCode',
                            'exchange_rate' => new Zend_Db_Expr ('"x"'),
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=rnc.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->where("MONTH(rnc.rfd_date) != MONTH(rnc.rfd_cancel_date)")
                    ->where("rnc.rfd_status = 'X'");

                if ($type == 1) {
                    $select_refund_cancel->where('rnc.rfd_trans_id = ?', $trans_id);
                } else {
                    $select_refund_cancel->where('rnc.rfd_IdStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_refund_cancel->where("DATE(rnc.rfd_cancel_date) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_refund_cancel->where("DATE(rnc.rfd_cancel_date) <= ?", $dateTo);
                }


                //discount adjustment
                $select_discount_cancel = $db->select()
                    ->from(
                        array('dsn' => 'discount_adjustment'), array(
                            'id' => 'dsn.id',
                            'record_date' => 'dsn.EnterDate',
                            'description' => "CONCAT(dsnt.dcnt_description,' (',dsnta.dcnt_fomulir_id,')')",
                            'txn_type' => new Zend_Db_Expr ('"Discount Adjustment"'),
                            'debit' => 'dsn.amount',
                            'credit' => new Zend_Db_Expr ('"0.00"'),
                            'document' => 'AdjustmentCode',
                            'invoice_no' => "AdjustmentCode",
                            'receipt_no' => 'AdjustmentCode',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'dsnt.dcnt_description',
                            'migrate_code' => new Zend_Db_Expr ('""'),
                            'exchange_rate' => 'i.exchange_rate',
                        )
                    )
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=dsn.cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->join(array('dsnt' => 'discount_detail'), 'dsnt.id=dsn.IdDiscount', array())
                    ->join(array('dsnta' => 'discount'), 'dsnta.dcnt_id=dsnt.dcnt_id', array())
                    ->joinLeft(array('i' => 'invoice_main'), 'i.id = dsnt.dcnt_invoice_id', array())
                    ->where("dsn.Status = 'APPROVE'")
                    ->where("MONTH(dsnta.dcnt_approve_date) != MONTH(dsnta.dcnt_cancel_date)")
                    ->where('dsn.AdjustmentType = 1'); //cancel

                if ($type == 1) {
                    $select_discount_cancel->where('dsnta.dcnt_txn_id = ?', $trans_id);
                } else {
                    $select_discount_cancel->where('dsnta.dcnt_IdStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_discount_cancel->where("DATE(dsn.EnterDate) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_discount_cancel->where("DATE(dsn.EnterDate) <= ?", $dateTo);
                }


                //payment adjustment - cancel approved receipt
                //checking not same month with receipt only

                $select_receipt_adj = $db->select()
                    ->from(
                        array('a' => 'tbl_receivable_adjustment'), array(
                            'id' => 'a.IdReceivableAdjustment',
                            'record_date' => 'a.EnterDate',
                            'description' => 'a.Remarks',
                            'txn_type' => new Zend_Db_Expr ('"Payment Adjustment"'),
                            'debit' => 'b.rcp_amount',
                            'credit' => new Zend_Db_Expr ('"0.00"'),
                            'document' => 'AdjustmentCode',
                            'invoice_no' => 'AdjustmentCode',
                            'receipt_no' => 'AdjustmentCode',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'a.Remarks',
                            'migrate_code' => new Zend_Db_Expr ('""'),
                            'exchange_rate' => new Zend_Db_Expr ('"x"'),
                        )
                    )
                    ->join(array('b' => 'receipt'), 'b.rcp_id = a.IdReceipt', array())
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                    ->where("a.Status = 'APPROVE'")
                    ->where("MONTH(b.rcp_receive_date) != MONTH(a.EnterDate)");

                if ($type == 1) {
                    $select_receipt_adj->where('b.rcp_trans_id = ?', $trans_id);
                } else {
                    $select_receipt_adj->where('b.rcp_IdStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_receipt_adj->where("DATE(a.EnterDate) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_receipt_adj->where("DATE(a.EnterDate) <= ?", $dateTo);
                }

                //currency adjustment
                $select_currency_adjustment = $db->select()
                    ->from(
                        array('a' => 'advance_payment_detail'), array(
                            'id' => 'a.advpydet_id',
                            'record_date' => 'a.advpydet_upddate',
                            'description' => new Zend_Db_Expr ('"Currency Adjustment"'),
                            'txn_type' => new Zend_Db_Expr ('"Currency Adjustment"'),
                            'debit' => 'a.gain',
                            'credit' => 'a.loss',
                            'document' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                            'invoice_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                            'receipt_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                            'subject' => new Zend_Db_Expr ('""'),
                            'fc_seq' => 'c.cur_id',
                            'migrate_desc' => 'b.advpy_description',
                            'migrate_code' => new Zend_Db_Expr ('""'),
                            'exchange_rate' => new Zend_Db_Expr ('"x"'),
                        )
                    )
                    ->join(array('b' => 'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id', array())
                    ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.advpy_cur_id', array('cur_code', 'cur_id'=>new Zend_Db_Expr ('"1"'), 'cur_desc'))
                    ->where('a.advpydet_status = ?', 'A')
                    ->where('b.advpy_status = ?', 'A')
                    ->where('a.gain != 0.00 OR a.loss != 0.00');

                if ($type == 1) {
                    $select_currency_adjustment->where('b.advpy_trans_id = ?', $trans_id);
                } else {
                    $select_currency_adjustment->where('b.advpy_idStudentRegistration = ?', $trans_id);
                }

                if ($dateFrom != '') {
                    $select_currency_adjustment->where("DATE(a.advpydet_upddate) >= ?", $dateFrom);
                }

                if ($dateTo != '') {
                    $select_currency_adjustment->where("DATE(a.advpydet_upddate) <= ?", $dateTo);
                }


                //sponsor payment
                if ($type == 2) {
                    $select_receipt_sponsor = $db->select()
                        ->from(
                            array('a' => 'sponsor_invoice_receipt_student'), array(
                                'id' => 'a.rcp_inv_id',
                                'record_date' => 'b.rcp_receive_date',
                                'description' => 'b.rcp_description',
                                'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
                                'debit' => new Zend_Db_Expr ('"0.00"'),
                                'credit' => 'a.rcp_inv_amount',
                                'document' => 'b.rcp_no',
                                'invoice_no' => 'b.rcp_no',
                                'receipt_no' => 'b.rcp_no',
                                'subject' => new Zend_Db_Expr ('""'),
                                'fc_seq' => 'c.cur_id',
                                'migrate_desc' => 'b.rcp_description',
                                'migrate_code' => new Zend_Db_Expr ('""'),
                                'exchange_rate' => new Zend_Db_Expr ('"x"'),
                            )
                        )
                        ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                        ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                        ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))');
                        //->where("b.rcp_status = 'APPROVE'");


                    $select_receipt_sponsor->where('a.IdStudentRegistration = ?', $trans_id);
                    $select_receipt_sponsor->where("DATE(b.rcp_receive_date) >= ?", $dateFrom);

                    $select_sponsor_adjustment = $db->select()
                        ->from(
                            array('a' => 'sponsor_invoice_receipt_student'), array(
                                'id' => 'a.rcp_inv_id',
                                'record_date' => 'IFNULL(e.sp_cancel_date, b.cancel_date)',
                                'description' => 'b.rcp_description',
                                'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                                'debit' => 'e.sp_amount',
                                'credit' => new Zend_Db_Expr ('"0.00"'),
                                'document' => 'b.rcp_no',
                                'invoice_no' => 'concat(b.rcp_no, "-", e.id)',
                                'receipt_no' => 'b.rcp_no',
                                'subject' => new Zend_Db_Expr ('""'),
                                'fc_seq' => 'c.cur_id',
                                'migrate_desc' => 'b.rcp_description',
                                'migrate_code' => new Zend_Db_Expr ('""'),
                                'exchange_rate' => new Zend_Db_Expr ('"x"'),
                            )
                        )
                        ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                        ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                        ->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                        ->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                        //->where("e.sp_status = ?", 0)
                        ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                        ->where("e.sp_status = 0 OR b.rcp_status = 'CANCEL'");
                    //->where("b.rcp_status = 'APPROVE'");

                    $select_sponsor_adjustment->where('a.IdStudentRegistration = ?', $trans_id);

                    $select_sponsor_adv_adjustment = $db->select()
                        ->from(
                            array('a' => 'sponsor_invoice_receipt_student'), array(
                                'id' => 'a.rcp_inv_id',
                                'record_date' => 'f.advpy_cancel_date',
                                'description' => 'b.rcp_description',
                                'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                                'debit' => 'f.advpy_amount',
                                'credit' => new Zend_Db_Expr ('"0.00"'),
                                'document' => 'b.rcp_no',
                                'invoice_no' => 'concat(b.rcp_no, "-", f.advpy_id)',
                                'receipt_no' => 'b.rcp_no',
                                'subject' => new Zend_Db_Expr ('""'),
                                'fc_seq' => 'c.cur_id',
                                'migrate_desc' => 'b.rcp_description',
                                'migrate_code' => new Zend_Db_Expr ('""'),
                                'exchange_rate' => new Zend_Db_Expr ('"x"'),
                            )
                        )
                        ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                        ->join(array('f'=>'advance_payment'), 'a.rcp_inv_rcp_id = f.advpy_rcp_id', array())
                        ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                        //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                        //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                        ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                        ->where("f.advpy_status = ?", 'X');

                    $select_sponsor_adv_adjustment->where('a.IdStudentRegistration = ?', $trans_id);
                    $select_sponsor_adv_adjustment->where('f.advpy_idStudentRegistration = ?', $trans_id);
                }

                //get array payment				
                if ($type == 1) {
                    $select = $db->select()
                        //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_currency_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                        ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj), Zend_Db_Select::SQL_UNION_ALL)
                        ->order("record_date asc")
                        ->order("txn_type asc");

                } else {
                    $select = $db->select()
                        //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_currency_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                        ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                        ->order("record_date asc")
                        ->order("txn_type asc");
                }

                $results = $db->fetchAll($select);


                if (!$results) {
                    $results = null;
                }

                $arrayStack[$i]['soa'] = $results;


                //process data
                $amountCurr = array();
                $curencyArray = array();
                $balance = 0;

                if ($results) {
                    foreach ($results as $row) {
                        $spmCurAdjStatus = false;
                        $sponsorCurAdj = 0.00;

                        $curencyArray[$row['cur_id']] = $row['cur_code'];

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                        if ($row['txn_type'] == "Sponsor Payment" || $row['txn_type'] == "Sponsor Payment Adjustment") {
                            $selectInvoiceSpm = $db->select()
                                ->from(array('a'=>'sponsor_invoice_receipt_invoice'), array())
                                ->join(array('b'=>'sponsor_invoice_detail'), 'a.rcp_inv_sp_id = b.sp_id', array('b.sp_amount'))
                                ->joinLeft(array('c'=>'invoice_main'), 'b.sp_invoice_id = c.id', array('c.invoice_date'))
                                ->join(array('d'=>'sponsor_invoice_receipt'), 'a.rcp_inv_rcp_id = d.rcp_id', array())
                                ->where('a.rcp_inv_student = ?', $row['id'])
                                ->group('b.id');

                            if ($row['txn_type'] == "Sponsor Payment Adjustment") {
                                $selectInvoiceSpm->where("b.sp_status = 0 OR d.rcp_status = 'CANCEL'");
                            }

                            $listInvoiceSpm = $db->fetchAll($selectInvoiceSpm);

                            $amountDebitSponsor = 0.00;
                            $amountCreditSponsor = 0.00;

                            if ($listInvoiceSpm){
                                foreach ($listInvoiceSpm as $invoiceSpm){
                                    if ($row['cur_id'] == 2) {
                                        $dataCur = $curRateDB->getRateByDate(2, $invoiceSpm['invoice_date']);
                                        //$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);

                                        if ($row['txn_type'] == "Sponsor Payment") {
                                            $amountCreditSponsor = round($amountCreditSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                        }else{
                                            $amountDebitSponsor = round($amountDebitSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                        }
                                    } else {
                                        if ($row['txn_type'] == "Sponsor Payment") {
                                            $amountCreditSponsor = $amountCreditSponsor + $invoiceSpm['sp_amount'];
                                        }else{
                                            $amountDebitSponsor = $amountDebitSponsor + $invoiceSpm['sp_amount'];
                                        }
                                    }
                                }
                            }
                        }

                        if ($row['exchange_rate']) {
                            if ($row['exchange_rate'] == 'x') {
                                $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
                                //							$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                            } else {
                                $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                            }
                        } else {
                            $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);
                            //						$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                        }

                        //					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
                        //					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
                        //					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];
                        if ($row['cur_id'] == 2) {
                            $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                            $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                        } else {
                            $amountDebit = $row['debit'];
                            $amountCredit = $row['credit'];
                        }
                        //					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;

                        if ($row['txn_type'] == "Invoice") {
                            $balance = $balance + $amountDebit;
                        } else
                            if ($row['txn_type'] == "Payment") {

                                $balance = $balance - $amountCredit;

                                ///hide on 05/08/2015 - do nothing for no
                                //unhide 12/08/2015

                                if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                                    if ($row['subject'] == 'gain') {
                                        $balance = $balance + $row['fc_seq'];
                                        $amountCredit = $amountCredit - $row['fc_seq'];
                                    } elseif ($row['subject'] == 'loss') {//loss
                                        $balance = $balance - $row['fc_seq'];
                                        $amountCredit = $amountCredit + $row['fc_seq'];
                                    }
                                }

                            } else
                                if ($row['txn_type'] == "Credit Note") {
                                    $balance = $balance - $amountCredit;
                                } else
                                    if ($row['txn_type'] == "Discount") {
                                        $balance = $balance - $amountCredit;
                                    } else
                                        if ($row['txn_type'] == "Debit Note") {
                                            $balance = $balance + $row['debit'];
                                        } else
                                            if ($row['txn_type'] == "Refund") {
                                                $balance = $balance + $amountDebit;
                                            } else
                                                if ($row['txn_type'] == "Advance Payment") {

                                                    $balance = $balance - $amountCredit;

                                                } else
                                                    if ($row['txn_type'] == "Refund Adjustment") {

                                                        $balance = $balance - $amountCredit;

                                                    } else
                                                        if ($row['txn_type'] == "Discount Adjustment") {

                                                            $balance = $balance + $amountDebit;

                                                        } else
                                                            if ($row['txn_type'] == "Payment Adjustment") {

                                                                $balance = $balance + $amountDebit;

                                                            } else
                                                                if ($row['txn_type'] == "Sponsor Payment") {

                                                                    //$balance = $balance - $amountCredit;

                                                                    if (strtotime($row['record_date']) <= strtotime('2016-12-31')){
                                                                        $balance = $balance - $amountCredit;

                                                                        $sponsorCurAdj = ($amountCreditSponsor - $amountCredit);

                                                                        if ($sponsorCurAdj != 0.00){
                                                                            $spmCurAdjStatus = true;
                                                                        }
                                                                    }else{
                                                                        $balance = $balance - $amountCreditSponsor;
                                                                        $row['credit'] = $amountCredit = $amountCreditSponsor;
                                                                        $row['cur_code'] = 'MYR';
                                                                        $row['cur_id'] = 1;
                                                                        $row['cur_desc'] = 'Malaysia Ringgit';
                                                                    }

                                                                }else if ($row['txn_type'] == "Sponsor Payment Adjustment"){
                                                                    if (strtotime($row['record_date']) <= strtotime('2016-12-31')) {
                                                                        $balance = $balance + $amountDebit;

                                                                        $sponsorCurAdj = ($amountDebitSponsor - $amountDebit);

                                                                        if ($sponsorCurAdj != 0.00) {
                                                                            $spmCurAdjStatus = true;
                                                                        }
                                                                    }else{
                                                                        $balance = $balance + $amountDebitSponsor;
                                                                        $row['debit'] = $amountDebit = $amountDebitSponsor;
                                                                        $row['cur_code'] = 'MYR';
                                                                        $row['cur_id'] = 1;
                                                                        $row['cur_desc'] = 'Malaysia Ringgit';
                                                                    }
                                                                }


                        $results2[] = $row;

                        $amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
                        $amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
                        $amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');

                        if ($spmCurAdjStatus == true){
                            if ($row['txn_type'] == "Sponsor Payment") {
                                $balance = $balance - ($sponsorCurAdj);
                            }else{
                                $balance = $balance + ($sponsorCurAdj);
                            }

                            $results2[] = array(
                                'id' => $row['id'],
                                'record_date' => '2017-04-26',
                                'description' => $row['description'],
                                'txn_type' => 'Sponsor Payment Currency Adjustment',
                                'debit'=> str_replace('-', '', number_format(($sponsorCurAdj < 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
                                'credit' => str_replace('-', '', number_format(($sponsorCurAdj > 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
                                'document' => $row['document'].'-CURADV',
                                'invoice_no' => $row['invoice_no'].'-CURADV',
                                'receipt_no' => $row['receipt_no'].'-CURADV',
                                'subject' => $row['subject'],
                                'fc_seq' => $row['fc_seq'],
                                'migrate_desc' => $row['migrate_desc'].' Currency Adjustment',
                                'migrate_code' => $row['migrate_code'],
                                'exchange_rate' => $row['exchange_rate'],
                                'cur_code' => 'MYR',
                                'cur_id' => 1,
                                'cur_desc' => 'Malaysia Ringgit'
                            );

                            if ($sponsorCurAdj > 0.00){
                                $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = 0.00;
                                $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = str_replace('-', '', $sponsorCurAdj);
                                $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
                            }else{
                                $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = str_replace('-', '', $sponsorCurAdj);
                                $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = 0.00;
                                $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
                            }
                        }

                    }
                }


                //		    $curAvailable = array_unique($curencyArray);
                //		    echo "<pre>";
                //			print_r($amountCurr);

                $curAvailable = array('2' => 'USD', '1' => 'MYR');
                $arrayStack[$i]['amountCur'] = $amountCurr;
                $this->view->availableCurrency = $curAvailable;
                $this->view->amountCurr = $amountCurr;

                $i++;
            }

            $dataStack = array(
                'id_cron' => 0,
                'action' => 'Download SOA Detail',
                'student_id' => implode(',', array_values($arrayStackNew)),
                'type' => $type,
                'date_from' => $dateFrom,
                'date_to' => $dateTo,
                'program_id' => $formData['program'],
                'intake_id' => $formData['IdIntake'],
                'profile_status_id' => $formData['status'],
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $creator

            );
            $db->insert('finance_service', $dataStack);

//			echo "<pre>";
//					print_r($arrayStack);
//					exit;
            //var_dump($arrayStack[0]['amountCur']);
            //exit;
            $this->view->account = $arrayStack;


        }

        $this->view->filename = date('Ymd') . '_statementofaccount.xls';
    }

    private function balance($trans_id, $type, $dateFrom, $dateTo)
    {

        $idStudentRegistration = $this->_getParam('id_reg', null);
        $this->view->idStudentRegistration = $idStudentRegistration;

        //title
        $this->view->title = $this->view->translate("Statement of Account");

        //profile
        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
        }


        $this->view->profile = $profile;

        //account
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'description' => 'GROUP_CONCAT(fc.fi_name)',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'bill_number',
                    'invoice_no' => 'bill_number',
                    'receipt_no' => new Zend_Db_Expr ('"-"'),
                    'subject' => 'GROUP_CONCAT(distinct s.SubCode)',
                    'fc_seq' => 'ivd.fi_id',
                    'migrate_desc' => 'bill_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => 'im.exchange_rate',
                )
            )

        ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
        ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
        ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
        ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
        ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
        ->joinLeft(array('cn'=>'credit_note'), 'cn.cn_invoice_id = im.id AND cn.cn_status = "A"', array())
        //->where('im.currency_id= ?',$currency)
        //->where('IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)')
        //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id) OR im.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")')
            ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id AND cnn.cn_status="A") OR im.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")')
            ->where("im.status NOT IN ('X', 'E','0')")//display ALL status except X,0=entry (24/27/07/2015)
        ->group('im.id');
        if ($type == 1) {
            $select_invoice->where('im.trans_id = ?', $trans_id);
        } else {
            $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_invoice->where("DATE(im.invoice_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_invoice->where("DATE(im.invoice_date) <= ?", $dateTo);
        }


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => "rcp_amount",
                    'document' => 'rcp_gainloss_soa',
                    'invoice_no' => 'rcp_no',
                    'receipt_no' => 'rcp_no',
                    'subject' => 'rcp_gainloss',//gain/loss
                    'fc_seq' => 'rcp_gainloss_amt',//gain/loss - myr
                    'migrate_desc' => 'pm.rcp_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('a' => 'tbl_receivable_adjustment'), "a.IdReceipt = pm.rcp_id and a.Status = 'APPROVE'", array())
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("pm.rcp_status = 'APPROVE' OR (pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0 AND MONTH(pm.rcp_receive_date) != MONTH(a.EnterDate))");
//							->orWhere("pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0");
        if ($type == 1) {
            $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
        } else {
            $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_payment->where("DATE(pm.rcp_receive_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_payment->where("DATE(pm.rcp_receive_date) <= ?", $dateTo);
        }

        $select_credit = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn.cn_id',
                    'record_date' => 'cn.cn_create_date',
                    'description' => 'cn.cn_description',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'document' => 'cn_billing_no',
                    'invoice_no' => 'cn_billing_no',
                    'receipt_no' => 'cn_billing_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'cn.cn_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(i.exchange_rate,'x')",
                )
            )
        ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
        ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
        ->joinLeft(array('j'=>'invoice_main'), 'cn.cn_invoice_id = j.id', array())
        //->where('pm.rcp_cur_id= ?',$currency)
        ->where("cn.cn_status = 'A'")
        ->where("IFNULL(j.status, 'A') NOT IN ('X', 'E','0')")
        //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A")');
        //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(j.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(j.invoice_date), 0))) OR i.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")');
        ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(j.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(j.invoice_date), 0)) OR j.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=j.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(j.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(j.invoice_date), 0))) OR i.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")');

        if ($type == 1) {
            $select_credit->where('cn.cn_trans_id = ?', $trans_id);
        } else {
            $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_credit->where("DATE(cn.cn_create_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_credit->where("DATE(cn.cn_create_date) <= ?", $dateTo);
        }

        $select_discount = $db->select()
            ->from(
                array('a' => 'discount_detail'), array(
                    'id' => 'dn.dcnt_id',
                    'record_date' => 'dn.dcnt_approve_date',
                    'description' => 'dn.dcnt_description',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'SUM(a.dcnt_amount)',
                    'document' => 'dn.dcnt_fomulir_id',
                    'invoice_no' => 'dn.dcnt_fomulir_id',
                    'receipt_no' => 'dn.dcnt_fomulir_id',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dn.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
//							->joinLeft(array('adj'=>'discount_adjustment'),"adj.IdDiscount = a.id and adj.Status= 'APPROVE' AND (MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date))", array())	
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date)")
            ->group('dn.dcnt_id');

        if ($type == 1) {
            $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_discount->where("DATE(dn.dcnt_approve_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_discount->where("DATE(dn.dcnt_approve_date) <= ?", $dateTo);
        }

        //refund
        $select_refund = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'rfd_fomulir',
                    'invoice_no' => 'rfd_fomulir',
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rn.rfd_date) != MONTH(rn.rfd_cancel_date)")
            ->where("rn.rfd_status IN  ('A','X')");

        if ($type == 1) {
            $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_refund->where("DATE(rfd_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_refund->where("DATE(rfd_date) <= ?", $dateTo);
        }

        //advance payment from invoice deposit only
        $select_advance = $db->select()
            ->from(
                array('adv' => 'advance_payment'), array(
                    'id' => 'advpy_id',
                    'record_date' => 'advpy_date',
                    'description' => "CONCAT(advpy_description,' - ',fi.fi_name)",
                    'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'advpy_amount',
                    'document' => 'advpy_fomulir',
                    'invoice_no' => 'advpy_fomulir',
                    'receipt_no' => 'advpy_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'advpy_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(adv.advpy_exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=adv.advpy_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('i' => 'invoice_main'), 'i.id=adv.advpy_invoice_id AND i.adv_transfer_id = adv.advpy_id', array())
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = i.id', array())
            ->join(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
            ->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
            ->where("i.status = 'A'")
            ->where("adv.advpy_status = 'A'");

        if ($type == 1) {
            $select_advance->where('adv.advpy_trans_id = ?', $trans_id);
        } else {
            $select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_advance->where("DATE(advpy_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_advance->where("DATE(advpy_date) <= ?", $dateTo);
        }


        //refund cancel
        $select_refund_cancel = $db->select()
            ->from(
                array('rnc' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_cancel_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund Adjustment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'rfd_amount',
                    'document' => 'rfd_fomulir',
                    'invoice_no' => "CONCAT(rfd_fomulir,'-',rfd_id)",
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rnc.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rnc.rfd_date) != MONTH(rnc.rfd_cancel_date)")
            ->where("rnc.rfd_status = 'X'");

        if ($type == 1) {
            $select_refund_cancel->where('rnc.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund_cancel->where('rnc.rfd_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_refund_cancel->where("DATE(rfd_cancel_date) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_refund_cancel->where("DATE(rfd_cancel_date) <= ?", $dateTo);
        }

        //discount adjustment
        $select_discount_cancel = $db->select()
            ->from(
                array('dsn' => 'discount_adjustment'), array(
                    'id' => 'dsn.id',
                    'record_date' => 'dsn.EnterDate',
                    'description' => "CONCAT(dsnt.dcnt_description,' (',dsnta.dcnt_fomulir_id,')')",
                    'txn_type' => new Zend_Db_Expr ('"Discount Adjustment"'),
                    'debit' => 'dsn.amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => "AdjustmentCode",
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dsnt.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dsn.cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('dsnt' => 'discount_detail'), 'dsnt.id=dsn.IdDiscount', array())
            ->join(array('dsnta' => 'discount'), 'dsnta.dcnt_id=dsnt.dcnt_id', array())
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = dsnt.dcnt_invoice_id', array())
            ->where("dsn.Status = 'APPROVE'")
            ->where("MONTH(dsnta.dcnt_approve_date) != MONTH(dsnta.dcnt_cancel_date)")
            ->where('dsn.AdjustmentType = 1'); //cancel

        if ($type == 1) {
            $select_discount_cancel->where('dsnta.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount_cancel->where('dsnta.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_discount_cancel->where("DATE(dsn.EnterDate) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_discount_cancel->where("DATE(dsn.EnterDate) <= ?", $dateTo);
        }


        //payment adjustment - cancel approved receipt
        //checking not same month with receipt only

        $select_receipt_adj = $db->select()
            ->from(
                array('a' => 'tbl_receivable_adjustment'), array(
                    'id' => 'a.IdReceivableAdjustment',
                    'record_date' => 'a.EnterDate',
                    'description' => 'a.Remarks',
                    'txn_type' => new Zend_Db_Expr ('"Payment Adjustment"'),
                    'debit' => 'b.rcp_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => 'AdjustmentCode',
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'a.Remarks',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'receipt'), 'b.rcp_id = a.IdReceipt', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("a.Status = 'APPROVE'")
            ->where("MONTH(b.rcp_receive_date) != MONTH(a.EnterDate)");

        if ($type == 1) {
            $select_receipt_adj->where('b.rcp_trans_id = ?', $trans_id);
        } else {
            $select_receipt_adj->where('b.rcp_IdStudentRegistration = ?', $trans_id);
        }

        if ($dateFrom != '') {
            $select_receipt_adj->where("DATE(a.EnterDate) >= ?", $dateFrom);
        }

        if ($dateTo != '') {
            $select_receipt_adj->where("DATE(a.EnterDate) <= ?", $dateTo);
        }

        //currency adjustment
        $select_currency_adjustment = $db->select()
            ->from(
                array('a' => 'advance_payment_detail'), array(
                    'id' => 'a.advpydet_id',
                    'record_date' => 'a.advpydet_upddate',
                    'description' => new Zend_Db_Expr ('"Currency Adjustment"'),
                    'txn_type' => new Zend_Db_Expr ('"Currency Adjustment"'),
                    'debit' => 'a.gain',
                    'credit' => 'a.loss',
                    'document' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'invoice_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'receipt_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'b.advpy_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.advpy_cur_id', array('cur_code', 'cur_id'=>new Zend_Db_Expr ('"1"'), 'cur_desc'))
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.gain != 0.00 OR a.loss != 0.00');

        if ($type == 1) {
            $select_currency_adjustment->where('b.advpy_trans_id = ?', $trans_id);
        } else {
            $select_currency_adjustment->where('b.advpy_idStudentRegistration = ?', $trans_id);
        }

        //sponsor payment
        if ($type == 2) {
            $select_receipt_sponsor = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'b.rcp_receive_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
                        'debit' => new Zend_Db_Expr ('"0.00"'),
                        'credit' => 'a.rcp_inv_amount',
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'b.rcp_no',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))');
            //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
            //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array());
            //->where("b.rcp_status = 'APPROVE'");


            $select_receipt_sponsor->where('a.IdStudentRegistration = ?', $trans_id);

            $select_sponsor_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'IFNULL(e.sp_cancel_date, b.cancel_date)',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'e.sp_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", e.id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                ->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                //->where("e.sp_status = ?", 0)
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                ->where("e.sp_status = 0 OR b.rcp_status = 'CANCEL'");
            //->where("b.rcp_status = 'APPROVE'");

            $select_sponsor_adjustment->where('a.IdStudentRegistration = ?', $trans_id);

            if ($dateFrom != '') {
                //$select_sponsor_adjustment->where("DATE(e.sp_cancel_date) >= ?", $dateFrom);
            }

            if ($dateTo != '') {
                //$select_sponsor_adjustment->where("DATE(e.sp_cancel_date) <= ?", $dateTo);
            }

            $select_sponsor_adv_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'f.advpy_cancel_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'f.advpy_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", f.advpy_id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('f'=>'advance_payment'), 'a.rcp_inv_rcp_id = f.advpy_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                ->where("f.advpy_status = ?", 'X');

            $select_sponsor_adv_adjustment->where('a.IdStudentRegistration = ?', $trans_id);
            $select_sponsor_adv_adjustment->where('f.advpy_idStudentRegistration = ?', $trans_id);
        }

        //get array payment	

        if ($type == 1) {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_currency_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");

        } else {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_currency_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");
        }

        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        $this->view->account = $results;

        //process data
        $amountCurr = array();
        $curencyArray = array();
        $balance = 0;

        if ($results) {
            foreach ($results as $row) {
                $curencyArray[$row['cur_id']] = $row['cur_code'];

                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                if ($row['txn_type'] == "Sponsor Payment" || $row['txn_type'] == "Sponsor Payment Adjustment") {
                    $selectInvoiceSpm = $db->select()
                        ->from(array('a'=>'sponsor_invoice_receipt_invoice'), array())
                        ->join(array('b'=>'sponsor_invoice_detail'), 'a.rcp_inv_sp_id = b.sp_id', array('b.sp_amount'))
                        ->joinLeft(array('c'=>'invoice_main'), 'b.sp_invoice_id = c.id', array('c.invoice_date'))
                        ->join(array('d'=>'sponsor_invoice_receipt'), 'a.rcp_inv_rcp_id = d.rcp_id', array())
                        ->where('a.rcp_inv_student = ?', $row['id'])
                        ->group('b.id');

                    if ($row['txn_type'] == "Sponsor Payment Adjustment") {
                        $selectInvoiceSpm->where("b.sp_status = 0 OR d.rcp_status = 'CANCEL'");
                    }

                    $listInvoiceSpm = $db->fetchAll($selectInvoiceSpm);

                    $amountDebit = 0.00;
                    $amountCredit = 0.00;

                    if ($listInvoiceSpm){
                        foreach ($listInvoiceSpm as $invoiceSpm){
                            if ($row['cur_id'] == 2) {
                                $dataCur = $curRateDB->getRateByDate(2, $invoiceSpm['invoice_date']);
                                //$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);

                                if ($row['txn_type'] == "Sponsor Payment") {
                                    $amountCredit = round($amountCredit + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                }else{
                                    $amountDebit = round($amountDebit + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                }
                            } else {
                                if ($row['txn_type'] == "Sponsor Payment") {
                                    $amountCredit = $amountCredit + $invoiceSpm['sp_amount'];
                                }else{
                                    $amountDebit = $amountDebit + $invoiceSpm['sp_amount'];
                                }
                            }
                        }
                    }
                } else {
                    if ($row['exchange_rate']) {
                        if ($row['exchange_rate'] == 'x') {
                            $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
//							$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                        } else {
                            $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                        }
                    } else {
                        $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);
//						$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                    }
                }

//					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
//					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
//					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];
                //if ($row['txn_type'] != "Sponsor Payment" && $row['txn_type'] != "Sponsor Payment Adjustment") {
                    if ($row['cur_id'] == 2) {
                        $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                        $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                    } else {
                        $amountDebit = $row['debit'];
                        $amountCredit = $row['credit'];
                    }
                //}
//					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;

                if ($row['txn_type'] == "Invoice") {
                    $balance = $balance + $amountDebit;
                } else
                    if ($row['txn_type'] == "Payment") {

                        /*$balance = $balance - $amountCredit;

                        ///hide on 05/08/2015 - do nothing for no
                        //unhide 12/08/2015

                        if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                            if ($row['subject'] == 'gain') {
                                $balance = $balance + $row['fc_seq'];
                                $amountCredit = $amountCredit - $row['fc_seq'];
                            } elseif ($row['subject'] == 'loss') {//loss
                                $balance = $balance - $row['fc_seq'];
                                $amountCredit = $amountCredit + $row['fc_seq'];
                            }
                        }*/

                        //cek invoice value and receipt value
                        $getReceiptInvoice = $curRateDB->getReceiptInvoice($row['id']);

                        $invoiceValue = 0;

                        if ($getReceiptInvoice){
                            foreach ($getReceiptInvoice as $getReceiptInvoiceLoop){
                                if ($getReceiptInvoiceLoop['rcp_inv_cur_id'] != 1) {
                                    $getReceiptInvoiceRate = $curRateDB->getRateByDate($getReceiptInvoiceLoop['rcp_inv_cur_id'], $getReceiptInvoiceLoop['invoice_date']);
                                    $invoiceValue2 = round($getReceiptInvoiceLoop['rcp_inv_amount'] * $getReceiptInvoiceRate['cr_exchange_rate'], 2);
                                }else{
                                    $invoiceValue2 = $getReceiptInvoiceLoop['rcp_inv_amount'];
                                }

                                $invoiceValue = ($invoiceValue+$invoiceValue2);
                            }
                        }

                        if ($row['cur_id'] != 1) {
                            $getInvoiceRate = $curRateDB->getRateByDate($row['cur_id'], $row['record_date']);
                            $receiptValue = round($row['credit'] * $getInvoiceRate['cr_exchange_rate'], 2);
                        }else{
                            $receiptValue = $row['credit'];
                        }

                        $checkGainLoss = $receiptValue-$invoiceValue;
                        $balance = $balance - $amountCredit;

                        ///hide on 05/08/2015 - do nothing for no
                        //unhide 12/08/2015
                        if ($checkGainLoss > 0) {
                            if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                                if ($row['subject'] == 'gain') {
                                    $balance = $balance + $row['fc_seq'];
                                    $amountCredit = $amountCredit - $row['fc_seq'];
                                } elseif ($row['subject'] == 'loss') {//loss
                                    $balance = $balance - $row['fc_seq'];
                                    $amountCredit = $amountCredit + $row['fc_seq'];
                                }
                            }
                        }

                    } else
                        /*if ($row['txn_type'] == "Credit Note") {
                            $balance = $balance - $amountCredit;
                        } else
                            if ($row['txn_type'] == "Discount") {
                                $balance = $balance - $amountCredit;
                            } else
                                if ($row['txn_type'] == "Debit Note") {
                                    $balance = $balance + $row['debit'];
                                } else
                                    if ($row['txn_type'] == "Refund") {
                                        $balance = $balance + $amountDebit;
                                    } else
                                        if ($row['txn_type'] == "Advance Payment") {

                                            $balance = $balance - $amountCredit;

                                        } else
                                            if ($row['txn_type'] == "Refund Adjustment") {

                                                $balance = $balance - $amountCredit;

                                            } else
                                                if ($row['txn_type'] == "Discount Adjustment") {

                                                    $balance = $balance + $amountDebit;

                                                } else
                                                    if ($row['txn_type'] == "Payment Adjustment") {

                                                        $balance = $balance + $amountDebit;

                                                    } else
                                                        if ($row['txn_type'] == "Sponsor Payment") {

                                                            $balance = $balance - $amountCredit;

                                                        } else
                                                            if ($row['txn_type'] == "Sponsor Payment Adjustment") {

                                                                $balance = $balance + $amountDebit;

                                                            }else if ($row['txn_type'] == "Currency Adjustment"){
                                                                $balance = $balance + $amountCredit;
                                                                $balance = $balance - $amountDebit;
                                                            }*/
                        if ($row['txn_type'] == "Credit Note") {
                            $balance = $balance - $amountCredit;
                        } else
                            if ($row['txn_type'] == "Discount") {
                                $balance = $balance - $amountCredit;
                            } else
                                if ($row['txn_type'] == "Debit Note") {
                                    $balance = $balance + $row['debit'];
                                } else
                                    if ($row['txn_type'] == "Refund") {
                                        $balance = $balance + $amountDebit;
                                    } else
                                        if ($row['txn_type'] == "Advance Payment") {

                                            $balance = $balance - $amountCredit;

                                        } else
                                            if ($row['txn_type'] == "Refund Adjustment") {

                                                $balance = $balance - $amountCredit;

                                            } else
                                                if ($row['txn_type'] == "Discount Adjustment") {

                                                    $balance = $balance + $amountDebit;

                                                } else
                                                    if ($row['txn_type'] == "Payment Adjustment") {

                                                        $balance = $balance + $amountDebit;

                                                    } else
                                                        if ($row['txn_type'] == "Sponsor Payment") {
                                                            $balance = $balance - $amountCredit;

                                                        }else if ($row['txn_type'] == "Sponsor Payment Adjustment"){
                                                            $balance = $balance + $amountDebit;
                                                        }else if ($row['txn_type'] == "Currency Adjustment"){
                                                            $balance = $balance - $amountCredit;
                                                            $balance = $balance + $amountDebit;
                                                        }

                $amountCurr['cur_id'] = $row['cur_id'];
                $amountCurr['debit'] = $amountDebit;
                $amountCurr['credit'] = $amountCredit;
                $amountCurr['balance'] = number_format($balance, 2, '.', ',');


            }

        }
//echo "<pre>";
//			print_r($results);
        /*$curAvailable = array('2'=>'USD','1'=>'MYR');

            $this->view->availableCurrency= $curAvailable;*/
        return $amountCurr;
    }

    public function courseRegistrationAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $trans_id = $this->_getParam('id', null);
        $this->view->appl_id = $trans_id;
        $type = $this->_getParam('type', null);
        $this->view->type = $type;

        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);


            //get landscape info
            $landscapeId = $profile["IdLandscape"];
            $landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
            $landscape = $landscapeDB->getLandscapeDetails($landscapeId);
            $this->view->landscape = $landscape;

            //get total registered semester for course registration
            $studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
            $semester = $studentSemesterDB->getRegisteredSemester($trans_id);
            $this->view->semester = $semester;
        }
    }

    public function printCreditNoteAction()
    {
        $this->view->title = 'Credit Note';

//		$student = $this->studentinfo;

        $invoiceID = $this->_getParam('id', null);
        $type = $this->_getParam('type', null);

        $cnDB = new Studentfinance_Model_DbTable_CreditNote();
        $invoice = $cnDB->getData($invoiceID);

        if ($invoice['cn_IdStudentRegistration']) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $student = $studentRegistrationDb->getTheStudentRegistrationDetail($invoice['cn_IdStudentRegistration']);
            $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($invoice['cn_IdStudentRegistration']);
        } else {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $student = $applicantProfileDb->getApplicantRegistrationDetail($invoice['cn_trans_id']);
        }


        if ($type == null) {
            $address = trim($student["appl_caddress1"]);
            if ($student["appl_caddress2"]) {
                $address .= '<br/>' . $student["appl_caddress2"] . ' ';
            }
            if ($student["appl_cpostcode"]) {
                $address .= '<br/>' . $student["appl_cpostcode"] . ' ';
            }
            if ($student["appl_ccity"] != 99) {
                $address .= '<br />' . $student["CCityName"] . ' ';
            } else {
                $address .= '<br/ >' . $student["appl_ccity_others"] . ' ';
            }
            if ($student["appl_cstate"] != 99) {
                $address .= $student["CStateName"] . ' ';
            } else {
                $address .= $student["appl_cstate_others"];
            }
            $address .= '<br/>' . $student["CCountryName"];
        }else{
            $sponsormodel = new Studentfinance_Model_DbTable_Sponsor();
            $sponsorInfo = $sponsormodel->getSponsorInfo($taggingFinancialAid['Sponsorshipid']);

            $address = $sponsorInfo['Add1']!= '' ? $sponsorInfo['Add1']:'';
            $address .= $sponsorInfo['Add2']!= '' ? '<br />'.$sponsorInfo['Add2']:'';
            $address .= $sponsorInfo['zipCode']!= '' ? '<br />'.$sponsorInfo['zipCode']:'';
            $address .= $sponsorInfo['CityName']!= '' && $sponsorInfo['CityName']!=null ? '<br />'.$sponsorInfo['CityName']:'';
            $address .= $sponsorInfo['StateName']!= '' && $sponsorInfo['StateName']!=null ? '<br />'.$sponsorInfo['StateName']:'';
            $address .= $sponsorInfo['CountryName']!= '' && $sponsorInfo['CountryName']!=null ? '<br />'.$sponsorInfo['CountryName']:'';
        }


        $fieldValues = array(
            '[Name]' => $type == null ? strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"]):strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"].'<br />'.$sponsorInfo['fName']),
            '[Address]' => strtoupper($address),
            '[Date]' => date('d F Y', strtotime($invoice["cn_create_date"])),
            '[ID]' => isset($student["registrationId"]) ? strtoupper($student["registrationId"]):strtoupper($student["at_pes_id"]),
            '[NRIC]' => strtoupper($student["appl_idnumber"]),
            '[Amount in Text]' => '',
            '[Ref No]' => strtoupper($invoice["cn_billing_no"]),
            '[Programme]' => strtoupper($student["ProgramName"]),
            '[Mode of Study]' => strtoupper($student["StudyMode"]),
            '[DatePrint]' => date('d F Y'),
//			'[Semester]' => strtoupper( $invoice[0]["SemesterMainName"] ),
//			 '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
        );


        $currency = new Zend_Currency(array('currency' => $invoice['cur_id']));

        $totalInvoice = 0;

        $processing_fee = "<table id='tfhover' class='tftable' border='1'>";
        $processing_fee .= "<thead>";
        $processing_fee .= "<tr>";
        $processing_fee .= "<th>Item Code</th>";
        $processing_fee .= "<th>Description</th>";
        $processing_fee .= "<th>Type</th>";
        $processing_fee .= "<th>Amount (" . $invoice['cur_code'] . ")</th>";
        $processing_fee .= "</tr>";
        $processing_fee .= "</thead>";
        $processing_fee .= "<tbody>";

        if (count($invoice) > 0) {
            $a = 0;

            $desc = $invoice['cn_description'] . ' ';
            $amount = $invoice['cn_amount_cn'];

            $processing_fee .= "<tr>";
            $processing_fee .= "<td>" . $student["ProgramCode"] . "</td>";
            $processing_fee .= "<td>" . $student["ProgramName"] . "</td>";
            $processing_fee .= "<td>" . $desc . "</td>";
            $processing_fee .= "<td align='right'>" . $amount . "</td>";
            $processing_fee .= "</tr>";

        }
        $processing_fee .= "<tr><td colspan='3' align='right'>Total : </td>";
        $processing_fee .= "<td align='right'>" . sprintf("%.2f", $amount) . "</td>";
        $processing_fee .= "</tr>";
        $processing_fee .= "</tbody>";
        $processing_fee .= "</table>";

        $fieldValues['[Credit Note]'] = $processing_fee;
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $templateDb = new Communication_Model_DbTable_Template();
        $template = $templateDb->getTemplateContent(96, 'en_US');

        $html = $template['tpl_content'];

        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

//		echo $html;exit;
        $option = array(
            'content' => $html,
            'save' => false,
            'file_name' => 'creditnote',
            'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',

            'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 180; 
					  $img_h = 59;
                      $pdf->image("images/logo_text_high.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
            'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
        );

        $pdf = generatePdf($option);

        exit;
    }

    public function downloadBalanceAction()
    {

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $resultsStatementDB = new Studentfinance_Model_DbTable_AccountStatement();
        if ($this->_request->isPost()) {

            $formData = $this->getRequest()->getPost();
            //var_dump($formData); //exit;
            $dateFrom = $formData['date_from'];
            $dateTo = $formData['date_to'];

            $p_data = $resultsStatementDB->getPaginateDataDownload($formData);

            $p_List = new Zend_Paginator_Adapter_DbSelect($p_data);
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
            $paginator->setItemCountPerPage(5000);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

            if ($paginator) {
                $i = 0;
                $balanceArr = array();
                foreach ($paginator as $p_Loop) {
                    $balance = $this->balance($p_Loop['transID'], $formData['type'], $dateFrom, $dateTo);
                    $balanceArr[$i]['balance'] = $balance;
                    $i++;
                }
                $this->view->balanceArr = $balanceArr;
            }
            $this->view->paginator = $paginator;


        }

        $this->view->filename = date('Ymd') . '_statementofaccountbalance.xls';
    }

    public function refundAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $appl_id = $this->_getParam('id', null);
        $this->view->appl_id = $appl_id;
        $type = $this->_getParam('type', null);
        $this->view->type = $type;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'billing_no' => 'rfd_fomulir',
                    'record_date' => 'rfd_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'currency' => 'c.cur_symbol_prefix'
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc', 'cur_symbol_prefix'))
            ->where("rn.rfd_status = 'A'");

        if ($type == 1) {
            $select->where('rn.rfd_trans_id = ?', $appl_id);
        } elseif ($type == 2) {
            $select->where('rn.rfd_IdStudentRegistration = ?', $appl_id);
        }
        $row = $db->fetchAll($select);
        $m = 0;
        foreach ($row as $data) {
            $selectData2 = $db->select()
                ->from(array('a' => 'refund_detail'), array())
                ->joinLeft(array('i' => 'advance_payment_detail'), 'a.rfdd_advpydet_id=i.advpydet_id and i.advpydet_advpy_id = a.rfdd_adv_id', array())
                ->joinLeft(array('id' => 'advance_payment'), 'i.advpydet_advpy_id = id.advpy_id', array('advpy_fomulir'))
                ->where('a.rfdd_refund_id = ?', $data['id']);
            $row2 = $db->fetchAll($selectData2);
            $row[$m]['invoice_no'] = $row2;
            $m++;

        }

        if (!$row) {
            $row = null;
        }

        $this->view->payment = $row;
    }

    public function soaSummaryDoubleAction()
    {

        $this->_helper->layout->disableLayout();
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectDetail = 'select count(*),registrationId from tbl_studentregistration group by registrationId having count(*) > 1';;
        $resultsDetail = $db->fetchAll($selectDetail);

        $arrayNew = array();
        $m = 0;
        foreach ($resultsDetail as $data) {

            $selectData2 = $db->select()
                ->from(array('a' => 'tbl_studentregistration'), array('IdStudentRegistration', 'registrationId'))
                ->joinLeft(array('StudentProfile' => 'student_profile'), "a.sp_id = StudentProfile.id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name"))
                ->joinLeft(array('p' => 'tbl_program'), 'a.IdProgram = p.IdProgram', array('ProgramCode'))
                ->joinLeft(array('i' => 'tbl_intake'), 'a.IdIntake = i.IdIntake', array('IntakeDesc'))
                ->where('a.registrationId = ?', $data['registrationId']);
            $row2 = $db->fetchAll($selectData2);

            foreach ($row2 as $rw) {
                $arrayNew[$m] = $rw;

                $balance = $this->balance($rw['IdStudentRegistration'], 2, '', date('Y-m-d'));
                $arrayNew[$m]['balance'] = $balance['balance'];

                $m++;
            }
        }

        $this->view->paginator = $arrayNew;


    }

    public function ledgerGroupAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $trans_id = $this->_getParam('id', null);
        $this->view->appl_id = $trans_id;

        $type = $this->_getParam('type', 1);
        $this->view->type = $type;

        $idStudentRegistration = $this->_getParam('id_reg', null);
        $this->view->idStudentRegistration = $idStudentRegistration;

        //title
        $this->view->title = $this->view->translate("Statement of Account");

        $lobjdeftype = new App_Model_Definitiontype();
        $this->view->group = $lobjdeftype->fnGetDefinationsByLocale('Fee Group');

        //profile
        if ($type == 1) {
            $applicantProfileDb = new Registration_Model_DbTable_ApplicantTransaction();
            $profile = $applicantProfileDb->getApplicantRegistrationDetail($trans_id);
            $appl_id = $profile['appl_id'];
        } elseif ($type == 2) {
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $profile = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
        }


        $this->view->profile = $profile;

//    	echo "<pre>";
//    	print_r($profile);
//    	exit;

        $idGroup = $this->_getParam('groupid', null);
        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $idGroup = $formData['group'];

        }

        $this->view->groupid = $idGroup;
        //account
        $db = Zend_Db_Table::getDefaultAdapter();

        $select_invoice = $db->select()
            ->from(array('im' => 'invoice_main'), array(
                    'id' => 'im.id',
                    'record_date' => 'im.invoice_date',
                    'description' => 'GROUP_CONCAT(fc.fi_name)',
                    'txn_type' => new Zend_Db_Expr ('"Invoice"'),
                    'debit' => 'bill_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'bill_number',
                    'invoice_no' => 'bill_number',
                    'receipt_no' => new Zend_Db_Expr ('"-"'),
                    'subject' => 's.SubCode',
                    'fc_seq' => 'ivd.fi_id',
                    'migrate_desc' => 'bill_description',
                    'migrate_code' => 'MigrateCode',
                    //'exchange_rate' => 'im.exchange_rate',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id=im.id', array())
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array())
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('cn'=>'credit_note'), 'cn.cn_invoice_id = im.id AND cn.cn_status = "A"', array())
            //->where('im.currency_id= ?',$currency)						  
            //->where('IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)')
            ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id) OR im.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")')
            ->where("im.status NOT IN ('X', 'E','0')")//display ALL status except X,0=entry (24/27/07/2015)
            ->group('im.id');
        if ($type == 1) {
            $select_invoice->where('im.trans_id = ?', $trans_id);
        } else {
            $select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
        }


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id' => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
                    'txn_type' => new Zend_Db_Expr ('"Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => "rcp_amount",
                    'document' => 'rcp_gainloss_soa',
                    'invoice_no' => 'rcp_no',
                    'receipt_no' => 'rcp_no',
                    'subject' => 'rcp_gainloss',//gain/loss
                    'fc_seq' => 'rcp_gainloss_amt',//gain/loss - myr
                    'migrate_desc' => 'pm.rcp_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('p' => 'payment'), 'p.p_rcp_id = pm.rcp_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('a' => 'tbl_receivable_adjustment'), "a.IdReceipt = pm.rcp_id and a.Status = 'APPROVE'", array())
            //->where('pm.rcp_cur_id= ?',$currency)		
            ->where("pm.rcp_status = 'APPROVE' OR (pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0 AND MONTH(pm.rcp_receive_date) != MONTH(a.EnterDate))");
//							->orWhere("pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0");
        if ($type == 1) {
            $select_payment->where('pm.rcp_trans_id = ?', $trans_id);
        } else {
            $select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
        }

        $select_credit = $db->select()
            ->from(
                array('cn' => 'credit_note'), array(
                    'id' => 'cn.cn_id',
                    'record_date' => 'cn.cn_create_date',
                    'description' => 'cn.cn_description',
                    'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'cn_amount',
                    'document' => 'cn_billing_no',
                    'invoice_no' => 'cn_billing_no',
                    'receipt_no' => 'cn_billing_no',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'cn.cn_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(i.exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=cn.cn_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = cn.cn_invoice_id', array())
            //->joinLeft(array('j'=>'invoice_main'), 'cn.cn_invoice_id = j.id', array())
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("cn.cn_status = 'A'")
            ->where("IFNULL(i.status, 'A') NOT IN ('X', 'E','0')")
            //->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(i.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0)) OR i.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=i.id AND cnn.cn_id = cn.cn_id AND cnn.cn_status="A")');
            ->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(i.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0)) OR i.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=i.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(i.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(i.invoice_date), 0))) OR i.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")');
            //->where('IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0) OR i.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=i.id)');
        if ($type == 1) {
            $select_credit->where('cn.cn_trans_id = ?', $trans_id);
        } else {
            $select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
        }

        $select_discount = $db->select()
            ->from(
                array('a' => 'discount_detail'), array(
                    'id' => 'dn.dcnt_id',
                    'record_date' => 'dn.dcnt_approve_date',
                    'description' => 'dn.dcnt_description',
                    'txn_type' => new Zend_Db_Expr ('"Discount"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'SUM(a.dcnt_amount)',
                    'document' => 'dn.dcnt_fomulir_id',
                    'invoice_no' => 'dn.dcnt_fomulir_id',
                    'receipt_no' => 'dn.dcnt_fomulir_id',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dn.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('dn' => 'discount'), 'dn.dcnt_id = a.dcnt_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dn.dcnt_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = a.dcnt_invoice_id', array())
//							->joinLeft(array('adj'=>'discount_adjustment'),"adj.IdDiscount = a.id and adj.Status= 'APPROVE' AND (MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date))", array())	
            //->where('pm.rcp_cur_id= ?',$currency)
            //->where("a.status = ?", 'A')
            ->where("MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date)")
            ->group('dn.dcnt_id');

        if ($type == 1) {
            $select_discount->where('dn.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id);
        }

        //refund
        $select_refund = $db->select()
            ->from(
                array('rn' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund"'),
                    'debit' => 'rfd_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'rfd_fomulir',
                    'invoice_no' => 'rfd_fomulir',
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rn.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rn.rfd_date) != MONTH(rn.rfd_cancel_date)")
            ->where("rn.rfd_status IN  ('A','X')");

        if ($type == 1) {
            $select_refund->where('rn.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);
        }

        //advance payment from invoice deposit only
        $select_advance = $db->select()
            ->from(
                array('adv' => 'advance_payment'), array(
                    'id' => 'advpy_id',
                    'record_date' => 'advpy_date',
                    'description' => "CONCAT(advpy_description,' - ',fi.fi_name)",
                    'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'advpy_amount',
                    'document' => 'advpy_fomulir',
                    'invoice_no' => 'advpy_fomulir',
                    'receipt_no' => 'advpy_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'advpy_description',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => "IFNULL(adv.advpy_exchange_rate,'x')",
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=adv.advpy_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('i' => 'invoice_main'), 'i.id=adv.advpy_invoice_id', array())
            ->join(array('ivd' => 'invoice_detail'), 'ivd.invoice_main_id = i.id', array())
            ->join(array('fi' => 'fee_item'), 'fi.fi_id = ivd.fi_id', array())
            ->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
            ->where("i.status = 'A'")
            ->where("adv.advpy_status = 'A'");

        if ($type == 1) {
            $select_advance->where('adv.advpy_trans_id = ?', $trans_id);
        } else {
            $select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);
        }


        //refund cancel
        $select_refund_cancel = $db->select()
            ->from(
                array('rnc' => 'refund'), array(
                    'id' => 'rfd_id',
                    'record_date' => 'rfd_cancel_date',
                    'description' => 'rfd_desc',
                    'txn_type' => new Zend_Db_Expr ('"Refund Adjustment"'),
                    'debit' => new Zend_Db_Expr ('"0.00"'),
                    'credit' => 'rfd_amount',
                    'document' => 'rfd_fomulir',
                    'invoice_no' => "CONCAT(rfd_fomulir,'-',rfd_id)",
                    'receipt_no' => 'rfd_fomulir',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'rfd_desc',
                    'migrate_code' => 'MigrateCode',
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=rnc.rfd_currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("MONTH(rnc.rfd_date) != MONTH(rnc.rfd_cancel_date)")
            ->where("rnc.rfd_status = 'X'");

        if ($type == 1) {
            $select_refund_cancel->where('rnc.rfd_trans_id = ?', $trans_id);
        } else {
            $select_refund_cancel->where('rnc.rfd_IdStudentRegistration = ?', $trans_id);
        }

        //discount adjustment
        $select_discount_cancel = $db->select()
            ->from(
                array('dsn' => 'discount_adjustment'), array(
                    'id' => 'dsn.id',
                    'record_date' => 'dsn.EnterDate',
                    'description' => "CONCAT(dsnt.dcnt_description,' (',dsnta.dcnt_fomulir_id,')')",
                    'txn_type' => new Zend_Db_Expr ('"Discount Adjustment"'),
                    'debit' => 'dsn.amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => "AdjustmentCode",
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'dsnt.dcnt_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => 'i.exchange_rate',
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=dsn.cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->join(array('dsnt' => 'discount_detail'), 'dsnt.id=dsn.IdDiscount', array())
            ->join(array('dsnta' => 'discount'), 'dsnta.dcnt_id=dsnt.dcnt_id', array())
            ->joinLeft(array('i' => 'invoice_main'), 'i.id = dsnt.dcnt_invoice_id', array())
            ->where("dsn.Status = 'APPROVE'")
            ->where("MONTH(dsnta.dcnt_approve_date) != MONTH(dsnta.dcnt_cancel_date)")
            ->where('dsn.AdjustmentType = 1'); //cancel

        if ($type == 1) {
            $select_discount_cancel->where('dsnta.dcnt_txn_id = ?', $trans_id);
        } else {
            $select_discount_cancel->where('dsnta.dcnt_IdStudentRegistration = ?', $trans_id);
        }


        //payment adjustment - cancel approved receipt
        //checking not same month with receipt only

        $select_receipt_adj = $db->select()
            ->from(
                array('a' => 'tbl_receivable_adjustment'), array(
                    'id' => 'a.IdReceivableAdjustment',
                    'record_date' => 'a.EnterDate',
                    'description' => 'a.Remarks',
                    'txn_type' => new Zend_Db_Expr ('"Payment Adjustment"'),
                    'debit' => 'b.rcp_amount',
                    'credit' => new Zend_Db_Expr ('"0.00"'),
                    'document' => 'AdjustmentCode',
                    'invoice_no' => 'AdjustmentCode',
                    'receipt_no' => 'AdjustmentCode',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'a.Remarks',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'receipt'), 'b.rcp_id = a.IdReceipt', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->where("a.Status = 'APPROVE'")
            ->where("MONTH(b.rcp_receive_date) != MONTH(a.EnterDate)");

        if ($type == 1) {
            $select_receipt_adj->where('b.rcp_trans_id = ?', $trans_id);
        } else {
            $select_receipt_adj->where('b.rcp_IdStudentRegistration = ?', $trans_id);
        }

        //currency adjustment
        $select_currency_adjustment = $db->select()
            ->from(
                array('a' => 'advance_payment_detail'), array(
                    'id' => 'a.advpydet_id',
                    'record_date' => 'a.advpydet_upddate',
                    'description' => new Zend_Db_Expr ('"Currency Adjustment"'),
                    'txn_type' => new Zend_Db_Expr ('"Currency Adjustment"'),
                    'debit' => 'a.gain',
                    'credit' => 'a.loss',
                    'document' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'invoice_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'receipt_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
                    'subject' => new Zend_Db_Expr ('""'),
                    'fc_seq' => 'c.cur_id',
                    'migrate_desc' => 'b.advpy_description',
                    'migrate_code' => new Zend_Db_Expr ('""'),
                    'exchange_rate' => new Zend_Db_Expr ('"x"'),
                )
            )
            ->join(array('b' => 'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=b.advpy_cur_id', array('cur_code', 'cur_id'=>new Zend_Db_Expr ('"1"'), 'cur_desc'))
            ->where('a.advpydet_status = ?', 'A')
            ->where('b.advpy_status = ?', 'A')
            ->where('a.gain != 0.00 OR a.loss != 0.00');

        if ($type == 1) {
            $select_currency_adjustment->where('b.advpy_trans_id = ?', $trans_id);
        } else {
            $select_currency_adjustment->where('b.advpy_idStudentRegistration = ?', $trans_id);
        }

        //sponsor payment
        if ($type == 2) {
            $select_receipt_sponsor = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'b.rcp_receive_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
                        'debit' => new Zend_Db_Expr ('"0.00"'),
                        'credit' => 'a.rcp_inv_amount',
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'b.rcp_no',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))');
            //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
            //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array());
            //->where("b.rcp_status = 'APPROVE'");


            $select_receipt_sponsor->where('a.IdStudentRegistration = ?', $trans_id);

            $select_sponsor_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'IFNULL(e.sp_cancel_date, b.cancel_date)',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'e.sp_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", e.id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                ->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                ->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                //->where("e.sp_status = ?", 0)
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                ->where("e.sp_status = 0 OR b.rcp_status = 'CANCEL'");
            //->where("b.rcp_status = 'APPROVE'");

            $select_sponsor_adjustment->where('a.IdStudentRegistration = ?', $trans_id);

            $select_sponsor_adv_adjustment = $db->select()
                ->from(
                    array('a' => 'sponsor_invoice_receipt_student'), array(
                        'id' => 'a.rcp_inv_id',
                        'record_date' => 'f.advpy_cancel_date',
                        'description' => 'b.rcp_description',
                        'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
                        'debit' => 'f.advpy_amount',
                        'credit' => new Zend_Db_Expr ('"0.00"'),
                        'document' => 'b.rcp_no',
                        'invoice_no' => 'concat(b.rcp_no, "-", f.advpy_id)',
                        'receipt_no' => 'b.rcp_no',
                        'subject' => new Zend_Db_Expr ('""'),
                        'fc_seq' => 'c.cur_id',
                        'migrate_desc' => 'b.rcp_description',
                        'migrate_code' => new Zend_Db_Expr ('""'),
                        'exchange_rate' => new Zend_Db_Expr ('"x"'),
                    )
                )
                ->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
                ->join(array('f'=>'advance_payment'), 'a.rcp_inv_rcp_id = f.advpy_rcp_id', array())
                ->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
                //->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
                //->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
                ->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
                ->where("f.advpy_status = ?", 'X');

            $select_sponsor_adv_adjustment->where('a.IdStudentRegistration = ?', $trans_id);
            $select_sponsor_adv_adjustment->where('f.advpy_idStudentRegistration = ?', $trans_id);
        }

        //get array payment	

        if ($type == 1) {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_currency_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");

        } else {
            $select = $db->select()
                ->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_currency_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                //->union(array($select_invoice, $select_payment, $select_credit, $select_discount, $select_refund, $select_advance, $select_refund_cancel, $select_discount_cancel, $select_receipt_adj, $select_receipt_sponsor, $select_sponsor_adjustment, $select_sponsor_adv_adjustment), Zend_Db_Select::SQL_UNION_ALL)
                ->order("record_date asc")
                ->order("txn_type asc");
        }

        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        //process data
        $results2 = array();
        $amountCurr = array();
        $curencyArray = array();
        $balance = 0;

        if ($results) {
            foreach ($results as $row) {
                $spmCurAdjStatus = false;
                $curencyArray[$row['cur_id']] = $row['cur_code'];

                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                if ($row['txn_type'] == "Sponsor Payment" || $row['txn_type'] == "Sponsor Payment Adjustment") {
                    $selectInvoiceSpm = $db->select()
                        ->from(array('a'=>'sponsor_invoice_receipt_invoice'), array())
                        ->join(array('b'=>'sponsor_invoice_detail'), 'a.rcp_inv_sp_id = b.sp_id', array('b.sp_amount'))
                        ->joinLeft(array('c'=>'invoice_main'), 'b.sp_invoice_id = c.id', array('c.invoice_date'))
                        ->join(array('d'=>'sponsor_invoice_receipt'), 'a.rcp_inv_rcp_id = d.rcp_id', array())
                        ->where('a.rcp_inv_student = ?', $row['id'])
                        ->group('b.id');

                    if ($row['txn_type'] == "Sponsor Payment Adjustment") {
                        $selectInvoiceSpm->where("b.sp_status = 0 OR d.rcp_status = 'CANCEL'");
                    }

                    $listInvoiceSpm = $db->fetchAll($selectInvoiceSpm);

                    $amountDebitSponsor = 0.00;
                    $amountCreditSponsor = 0.00;

                    if ($listInvoiceSpm){
                        foreach ($listInvoiceSpm as $invoiceSpm){
                            if ($row['cur_id'] == 2) {
                                $dataCur = $curRateDB->getRateByDate(2, $invoiceSpm['invoice_date']);
                                //$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);

                                if ($row['txn_type'] == "Sponsor Payment") {
                                    $amountCreditSponsor = round($amountCreditSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                }else{
                                    $amountDebitSponsor = round($amountDebitSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
                                }
                            } else {
                                if ($row['txn_type'] == "Sponsor Payment") {
                                    $amountCreditSponsor = $amountCreditSponsor + $invoiceSpm['sp_amount'];
                                }else{
                                    $amountDebitSponsor = $amountDebitSponsor + $invoiceSpm['sp_amount'];
                                }
                            }
                        }
                    }
                } //else {
                    if ($row['exchange_rate']) {
                        if ($row['exchange_rate'] == 'x') {
                            $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
                            //							$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                        } else {
                            $dataCur = $curRateDB->getData($row['exchange_rate']);//usd
                        }
                    } else {
                        $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);
                        //						$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                    }
                //}

                //					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
                //					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
                //					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];

                //if ($row['txn_type'] != "Sponsor Payment" && $row['txn_type'] != "Sponsor Payment Adjustment") {
                    if ($row['cur_id'] == 2) {
                        $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                        $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                    } else {
                        $amountDebit = $row['debit'];
                        $amountCredit = $row['credit'];
                    }
                //}
                //					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;

                if ($row['txn_type'] == "Invoice") {
                    $balance = $balance + $amountDebit;
                } elseif ($row['txn_type'] == "Payment") {

                    /*$balance = $balance - $amountCredit;

                    ///hide on 05/08/2015 - do nothing for no
                    //unhide 12/08/2015

                    if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                        if ($row['subject'] == 'gain') {
                            $balance = $balance + $row['fc_seq'];
                            $amountCredit = $amountCredit - $row['fc_seq'];
                        } elseif ($row['subject'] == 'loss') {//loss
                            $balance = $balance - $row['fc_seq'];
                            $amountCredit = $amountCredit + $row['fc_seq'];
                        }
                    }*/

                    //cek invoice value and receipt value
                    $getReceiptInvoice = $curRateDB->getReceiptInvoice($row['id']);

                    $invoiceValue = 0;

                    if ($getReceiptInvoice){
                        foreach ($getReceiptInvoice as $getReceiptInvoiceLoop){
                            if ($getReceiptInvoiceLoop['rcp_inv_cur_id'] != 1) {
                                $getReceiptInvoiceRate = $curRateDB->getRateByDate($getReceiptInvoiceLoop['rcp_inv_cur_id'], $getReceiptInvoiceLoop['invoice_date']);
                                $invoiceValue2 = round($getReceiptInvoiceLoop['rcp_inv_amount'] * $getReceiptInvoiceRate['cr_exchange_rate'], 2);
                            }else{
                                $invoiceValue2 = $getReceiptInvoiceLoop['rcp_inv_amount'];
                            }

                            $invoiceValue = ($invoiceValue+$invoiceValue2);
                        }
                    }

                    if ($row['cur_id'] != 1) {
                        $getInvoiceRate = $curRateDB->getRateByDate($row['cur_id'], $row['record_date']);
                        $receiptValue = round($row['credit'] * $getInvoiceRate['cr_exchange_rate'], 2);
                    }else{
                        $receiptValue = $row['credit'];
                    }

                    $checkGainLoss = $receiptValue-$invoiceValue;
                    $balance = $balance - $amountCredit;

                    ///hide on 05/08/2015 - do nothing for no
                    //unhide 12/08/2015
                    if ($checkGainLoss > 0) {
                        if ($row['document'] == 0) { // 02/10/2015 - to cater gain/loss at bank charges

                            if ($row['subject'] == 'gain') {
                                $balance = $balance + $row['fc_seq'];
                                $amountCredit = $amountCredit - $row['fc_seq'];
                            } elseif ($row['subject'] == 'loss') {//loss
                                $balance = $balance - $row['fc_seq'];
                                $amountCredit = $amountCredit + $row['fc_seq'];
                            }
                        }
                    }

                } elseif ($row['txn_type'] == "Credit Note") {
                    $balance = $balance - $amountCredit;
                } elseif ($row['txn_type'] == "Discount") {
                    $balance = $balance - $amountCredit;
                } elseif ($row['txn_type'] == "Debit Note") {
                    $balance = $balance + $row['debit'];
                } elseif ($row['txn_type'] == "Refund") {
                    $balance = $balance + $amountDebit;
                } elseif ($row['txn_type'] == "Advance Payment") {

                    $balance = $balance - $amountCredit;

                } elseif ($row['txn_type'] == "Refund Adjustment") {

                    $balance = $balance - $amountCredit;

                } elseif ($row['txn_type'] == "Discount Adjustment") {

                    $balance = $balance + $amountDebit;

                } elseif ($row['txn_type'] == "Payment Adjustment") {

                    $balance = $balance + $amountDebit;

                } else if ($row['txn_type'] == "Sponsor Payment") {
                    if (strtotime($row['record_date']) <= strtotime('2016-12-31')){
                        $balance = $balance - $amountCredit;

                        $sponsorCurAdj = ($amountCreditSponsor - $amountCredit);

                        if ($sponsorCurAdj != 0.00){
                            $spmCurAdjStatus = true;
                        }
                    }else{
                        $balance = $balance - $amountCreditSponsor;
                        $row['credit'] = $amountCredit = $amountCreditSponsor;
                        $row['cur_code'] = 'MYR';
                        $row['cur_id'] = 1;
                        $row['cur_desc'] = 'Malaysia Ringgit';
                    }
                }else if ($row['txn_type'] == "Sponsor Payment Adjustment"){
                    if (strtotime($row['record_date']) <= strtotime('2016-12-31')) {
                        $balance = $balance + $amountDebit;

                        $sponsorCurAdj = ($amountDebitSponsor - $amountDebit);

                        if ($sponsorCurAdj != 0.00) {
                            $spmCurAdjStatus = true;
                        }
                    }else{
                        $balance = $balance + $amountDebitSponsor;
                        $row['debit'] = $amountDebit = $amountDebitSponsor;
                        $row['cur_code'] = 'MYR';
                        $row['cur_id'] = 1;
                        $row['cur_desc'] = 'Malaysia Ringgit';
                    }
                }else if ($row['txn_type'] == "Currency Adjustment"){
                    $balance = $balance - $amountCredit;
                    $balance = $balance + $amountDebit;
                }

                $results2[] = $row;

                $amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
                $amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
                $amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');

                if ($spmCurAdjStatus == true){
                    if ($row['txn_type'] == "Sponsor Payment") {
                        $balance = $balance - ($sponsorCurAdj);
                    }else{
                        $balance = $balance + ($sponsorCurAdj);
                    }

                    $results2[] = array(
                        'id' => $row['id'],
                        'record_date' => '2017-04-24',
                        'description' => $row['description'],
                        'txn_type' => 'Sponsor Payment Currency Adjustment',
                        'debit'=> str_replace('-', '', number_format(($sponsorCurAdj < 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
                        'credit' => str_replace('-', '', number_format(($sponsorCurAdj > 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
                        'document' => $row['document'].'-CURADV',
                        'invoice_no' => $row['invoice_no'].'-CURADV',
                        'receipt_no' => $row['receipt_no'].'-CURADV',
                        'subject' => $row['subject'],
                        'fc_seq' => $row['fc_seq'],
                        'migrate_desc' => $row['migrate_desc'].' Currency Adjustment',
                        'migrate_code' => $row['migrate_code'],
                        'exchange_rate' => $row['exchange_rate'],
                        'cur_code' => 'MYR',
                        'cur_id' => 1,
                        'cur_desc' => 'Malaysia Ringgit'
                    );

                    if ($sponsorCurAdj > 0.00){
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = 0.00;
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = str_replace('-', '', $sponsorCurAdj);
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
                    }else{
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = str_replace('-', '', $sponsorCurAdj);
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = 0.00;
                        $amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
                    }
                }

            }
        }

        /*if (count($results2) > 0) {
            $this->aasort($results2, 'record_date');
        }*/
        $this->view->account = $results2;

        $curAvailable = array('2' => 'USD', '1' => 'MYR');

        $this->view->availableCurrency = $curAvailable;
        $this->view->amountCurr = $amountCurr;

        //dd($results2);
        //dd($amountCurr);
    }

    public function printSponsorPaymentAction(){
        $id = $this->_getParam('id', null);
        $type = $this->_getParam('type', null);

        $model = new Studentfinance_Model_DbTable_InvoiceMain();
        $invStudent = $model->getSponsorPaymentStudent($id);
        $sponsorinvmain = $model->getSponsorPaymentReceipt($invStudent['rcp_inv_rcp_id']);

        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $student = $studentRegistrationDb->getTheStudentRegistrationDetail($invStudent['IdStudentRegistration']);
        $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($invStudent['IdStudentRegistration']);

        $currency = new Zend_Currency(array('currency' => $sponsorinvmain['rcp_cur_id']));

        if ($type==null) {
            $address = trim($student["appl_caddress1"]);
            if ($student["appl_caddress2"]) {
                $address .= '<br/>' . $student["appl_caddress2"] . ' ';
            }
            if ($student["appl_cpostcode"]) {
                $address .= '<br/>' . $student["appl_cpostcode"] . ' ';
            }
            if ($student["appl_ccity"] != 99) {
                $address .= '<br />' . $student["CCityName"] . ' ';
            } else {
                $address .= '<br/ >' . $student["appl_ccity_others"] . ' ';
            }
            if ($student["appl_cstate"] != 99) {
                $address .= $student["CStateName"] . ' ';
            } else {
                $address .= $student["appl_cstate_others"];
            }
            $address .= '<br/>' . $student["CCountryName"];
        }else{
            $sponsormodel = new Studentfinance_Model_DbTable_Sponsor();
            $sponsorInfo = $sponsormodel->getSponsorInfo($taggingFinancialAid['Sponsorshipid']);

            $address = $sponsorInfo['Add1']!= '' ? $sponsorInfo['Add1']:'';
            $address .= $sponsorInfo['Add2']!= '' ? '<br />'.$sponsorInfo['Add2']:'';
            $address .= $sponsorInfo['zipCode']!= '' ? '<br />'.$sponsorInfo['zipCode']:'';
            $address .= $sponsorInfo['CityName']!= '' && $sponsorInfo['CityName']!=null ? '<br />'.$sponsorInfo['CityName']:'';
            $address .= $sponsorInfo['StateName']!= '' && $sponsorInfo['StateName']!=null ? '<br />'.$sponsorInfo['StateName']:'';
            $address .= $sponsorInfo['CountryName']!= '' && $sponsorInfo['CountryName']!=null ? '<br />'.$sponsorInfo['CountryName']:'';
        }


        $fieldValues = array(
            '[Applicant Name]' => $type==null ? strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"]):strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"].'<br />'.$sponsorInfo['fName']),
            '[Applicant Address]' => strtoupper($address),
            '[Date]' => date('d F Y', strtotime($sponsorinvmain["rcp_receive_date"])),
            '[Applicant ID]' => isset($student["registrationId"]) ? strtoupper($student["registrationId"]):strtoupper($student["at_pes_id"]),
            '[DatePrint]' => date('d F Y'),
        );

        $invoiceList = $model->getSponsorPaymentInvoice($id);

        $fieldValues['[Receipt No]'] = $sponsorinvmain['rcp_no'];

        $fieldValues['[Data Loop]'] = '<table width="100%" cellpadding="2" cellspacing="0" style="border:1px solid #000000;"><tr><th width="50%" style="border:1px solid #000000;">Invoice No.</th><th style="border:1px solid #000000;">Amount Paid</th></tr>';

        $total_payment = 0;
        if ($invoiceList) {
            foreach ($invoiceList as $invoiceLoop) {
                $fieldValues['[Data Loop]'] .= '<tr>';
                $fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">' . $invoiceLoop['bill_number'] . '</td>';
                $fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">' . $currency->toCurrency($invoiceLoop['sp_paid'], array('display' => false)) . '&nbsp;</td>';
                $fieldValues['[Data Loop]'] .= '</tr>';

                $total_payment = $total_payment + $invoiceLoop['sp_paid'];
            }
        }

        //
        $fieldValues['[Data Loop]'] .= '<tr><td style="border:1px solid #000000;"><b>Total</b></td><td align="right" style="border:1px solid #000000;"><b>' . $currency->toCurrency($total_payment, array('currency' => $sponsorinvmain['rcp_cur_id'])) . '</b>&nbsp;</td></tr>';
        $fieldValues['[Data Loop]'] .= '</table>';

        //number to words
        $total_amount_in_text = convert_number_to_words($total_payment);


        //invoice & non-invoice data
        $fieldValues['[Amount in Text]'] = $sponsorinvmain['cur_symbol_prefix'] . ": " . $total_amount_in_text . " Only";

        //var_dump($fieldValues); exit;

        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $templateDb = new Communication_Model_DbTable_Template();
        $template = $templateDb->getTemplateContent(214, 'en_US');

        $html = $template['tpl_content'];

        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

//		echo $html;exit;
        $option = array(
            'content' => $html,
            'save' => false,
            'file_name' => 'receipt',
            'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',

            'header' => '<script type="text/php">
					if ( isset($pdf) ) {

					  $header = $pdf->open_object();

					  $w = $pdf->get_width();
					  $h = $pdf->get_height();

					  $color = array(0,0,0);

					  $img_w = 180;
					  $img_h = 59;
                      $pdf->image("images/logo_text_high.jpg",  $w-($img_w+35), 20, $img_w, $img_h);

					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);

			          $pdf->close_object();

					  $pdf->add_object($header, "all");
					}
					</script>',
            'footer' => '<script type="text/php">
					if ( isset($pdf) ) {

			          $footer = $pdf->open_object();

					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;

					  $w = $pdf->get_width();
					  $h = $pdf->get_height();


					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);

					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;

					  $pdf->page_text($x, $y, $text, $font, $size, $color);

			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;

					  $pdf->page_text($x, $y, $text, $font, $size, $color);

					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);

					  $pdf->close_object();

					  $pdf->add_object($footer, "all");

					}
					</script>'
        );

        $pdf = generatePdf($option);

        exit();
    }
}