<?php	
class Studentfinance_StudentactivitysetupController extends Base_Base { //Controller for the User Module

	private $lobjdeftype;
	private $_gobjlog;


	public function init() 
	{
		//initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
	}

	public function fnsetObj()
	{		
		$this->lobjFeeStudentActivityForm = new Studentfinance_Form_Studentactivity();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjFeeCategory = new Studentfinance_Model_DbTable_Fee();
		//$this->lobjFeeSetupForm = new_Studentfinance_Form_Feesetup();
			
	}

	public function indexAction()
	{
		//Form
		$this->view->lobjFeeStudentActivityForm = $this->lobjFeeStudentActivityForm;
 
		$this->view->title = 'Student Activity Setup';
		//For user Id and university
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->id;
		$IdUniversity = 1;
		$UpdDate = date('Y-m-d H:i:s');


		//Render to View
		$this->view->lobjFeeStudentActivityForm->IdUniversity->setvalue ($IdUniversity);
		$this->view->lobjFeeStudentActivityForm->UpdDate->setValue ($UpdDate);
		$this->view->lobjFeeStudentActivityForm->UpdUser->setValue ($userId);

		//For Chargeable
		$this->view->lobjFeeStudentActivityForm->ChangeProgramChargeable->addMultiOptions(array('1'=>'Yes', '0'=>'No') );
		$this->view->lobjFeeStudentActivityForm->ChangeStatusChargeable->addMultiOptions(array('1'=>'Yes', '0'=>'No') );
		$this->view->lobjFeeStudentActivityForm->CreditTransferChargeable->addMultiOptions(array('1'=>'Yes', '0'=>'No') );
		$this->view->lobjFeeStudentActivityForm->ResitExamChargeable->addMultiOptions(array('1'=>'Yes', '0'=>'No') );
		$this->view->lobjFeeStudentActivityForm->PaperReviewChargeable->addMultiOptions(array('1'=>'Yes', '0'=>'No') );
		$this->view->lobjFeeStudentActivityForm->ChangeStatusQuitChargeable->addMultiOptions(array('1'=>'Yes', '0'=>'No') );

		//For Trigger
		$DefmodelObj = new App_Model_Definitiontype();
		$raceList = $DefmodelObj->fnGetDefinationMs('Trigger');

		foreach($raceList as $larrvalues) 
		{
			$this->view->lobjFeeStudentActivityForm->ChangeProgramTrigger->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		
		foreach($raceList as $larrvalues) 
		{
			$this->view->lobjFeeStudentActivityForm->ChangeStatusTrigger->addMultiOption($larrvalues['key'],$larrvalues['value']);
			$this->view->lobjFeeStudentActivityForm->ChangeStatusQuitTrigger->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		
		foreach($raceList as $larrvalues) 
		{
			$this->view->lobjFeeStudentActivityForm->CreditTransferTrigger->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		
		foreach($raceList as $larrvalues) 
		{
			$this->view->lobjFeeStudentActivityForm->ResitExamTrigger->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		
		foreach($raceList as $larrvalues) 
		{
			$this->view->lobjFeeStudentActivityForm->PaperReviewTrigger->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}

		//For fee setup
		$feecategorycode = $this->lobjFeeCategory->getfeesetup();

		foreach($feecategorycode as $larrvalues) 
		{
			if($larrvalues['fi_name'])
			{
				//$result= $larrvalues['Description'].'-'.$larrvalues['FeeCode'];
				$result = $larrvalues['fi_code'].'-'.$larrvalues['fi_name'];
			}
			else
			{
				$result = $larrvalues['fi_code'];
			}

			$this->view->lobjFeeStudentActivityForm->ChangeProgramFeeSetup->addMultiOption($larrvalues['fi_id'],$result);
			$this->view->lobjFeeStudentActivityForm->ChangeStatusFeeSetup->addMultiOption($larrvalues['fi_id'],$result);
			$this->view->lobjFeeStudentActivityForm->CreditTransferFeeSetup->addMultiOption($larrvalues['fi_id'],$result);
			$this->view->lobjFeeStudentActivityForm->ResitExamFeeSetup->addMultiOption($larrvalues['fi_id'],$result);
			$this->view->lobjFeeStudentActivityForm->PaperReviewFeeSetup->addMultiOption($larrvalues['fi_id'],$result);
			
			$this->view->lobjFeeStudentActivityForm->ChangeStatusQuitFeeSetup->addMultiOption($larrvalues['fi_id'],$result);
		}
		
		$getDetauilsConfig = $this->lobjFeeCategory->getstudentactivityDetails($IdUniversity);
		$this->view->result = $getDetauilsConfig;

		if ( count($getDetauilsConfig) > 0 ) 
		{
			$this->view->lobjFeeStudentActivityForm->populate($getDetauilsConfig[0]);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost ();

			$errMsgInsert = array();

			$errMsgInsert = $this->lobjFeeCategory->getstudentactivityDetails($IdUniversity);
			if(empty($errMsgInsert))
			{
				$this->lobjFeeCategory->addStudentActivitySetup($larrformData);
			}
			else
			{
				$this->lobjFeeCategory->updatestudentactivityDetails($larrformData,$IdUniversity);
			}
			
			$this->_redirect( $this->baseUrl . '/studentfinance/studentactivitysetup');
		}
	}
}