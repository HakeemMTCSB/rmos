<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_ScholarshipReportController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Studentfinance_Model_DbTable_ScholarshipReport();
        $this->SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();
        $this->SponsorModel = new Studentfinance_Model_DbTable_Sponsor();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Course Selection Application');
        
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            //var_dump($post_data); exit;
            if (isset($post_data['search'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm(array('programid'=>$post_data['Program']));
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed(1, $post_data);
                //var_dump($sponsor_applications);
                $searchForm->populate($post_data);

                $this->view->applications = $sponsor_applications;
                $this->view->cust_type = $this->SponsorApplication->cust_types;
                $this->view->status = $this->SponsorApplication->status;
            }else{
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
            }
        }else{
            $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
            $this->view->searchForm = $searchForm;
        }
    }
    
    public function exportApplicationAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            $sponsor_applications = $this->SponsorApplication->fetch_to_be_processed2(1, $post_data);
            
            $this->view->sponsor_application = $sponsor_applications;
        }
        
        $this->view->status = $this->SponsorApplication->status;
        $this->view->filename = date('Ymd').'_selection_course_application.xls';
    }
    
    public function reapplicationAction(){
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            
            if (isset($post_data['search'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm(array('programid'=>$post_data['Program']));
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorModel->viewReAppList($post_data);
                $searchForm->populate($post_data);
                
                $this->view->applications = $sponsor_applications;
                $this->view->cust_type = $this->SponsorApplication->cust_types;
                $this->view->status = $this->SponsorApplication->status;
            }else{
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
                
                $sponsor_applications = $this->SponsorModel->viewReAppList();
            }
        }else{
            $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
            $this->view->searchForm = $searchForm;

            $sponsor_applications = $this->SponsorModel->viewReAppList();
        }
    }
    
    public function exportReapplicationAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            $sponsor_applications = $this->SponsorModel->viewReAppList($post_data);
            
            $this->view->sponsor_application = $sponsor_applications;
        }
        
        $this->view->status = $this->SponsorApplication->status;
        $this->view->filename = date('Ymd').'_selection_course_reapplication.xls';
    }

    public function financialAidReportAction(){
        $this->view->title = $this->view->translate('Financial Aid Report');

        $model = new Studentfinance_Model_DbTable_ScholarshipReport();

        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            //var_dump($post_data); exit;
            if (isset($post_data['search'])){
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm(array('programid'=>$post_data['Program']));
                $this->view->searchForm = $searchForm;

                $sponsor_applications = $model->getFinancialAidList($post_data);
                //var_dump($sponsor_applications);
                $searchForm->populate($post_data);

                $this->view->applications = $sponsor_applications;
                $this->view->cust_type = $this->SponsorApplication->cust_types;
                $this->view->status = $this->SponsorApplication->status;
            }else{
                $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
                $this->view->searchForm = $searchForm;
            }
        }else{
            $searchForm = new Studentfinance_Form_SponsorshipApplicationSearchForm();
            $this->view->searchForm = $searchForm;
        }
    }

    public function exportFinancialAidReportAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();

        $model = new Studentfinance_Model_DbTable_ScholarshipReport();

        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            $sponsor_applications = $model->getFinancialAidList($post_data);
            $this->view->applications = $sponsor_applications;
            $this->view->cust_type = $this->SponsorApplication->cust_types;
            $this->view->status = $this->SponsorApplication->status;
        }

        $this->view->filename = date('Ymd').'_financial_aid_report.xls';
    }
}