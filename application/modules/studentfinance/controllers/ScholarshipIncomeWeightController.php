<?php

class Studentfinance_ScholarshipIncomeWeightController extends Zend_Controller_Action
{

    public function indexAction() {
        $IncomeWeight = new Studentfinance_Model_DbTable_ScholarshipIncomeWeight();
        $this->view->incomeweights = $IncomeWeight->fetchAll();
    }

    public function addAction() {
        $IncomeWeight = new Studentfinance_Model_DbTable_ScholarshipIncomeWeight();
        $currency = $IncomeWeight->getCurrency();
        $this->view->currency = $currency;
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('incomeweight');
            $IncomeWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-income-weight/index');
        }
    }

    public function editAction() {
        $IncomeWeight = new Studentfinance_Model_DbTable_ScholarshipIncomeWeight();
        $currency = $IncomeWeight->getCurrency();
        $this->view->currency = $currency;
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('incomeweight');
            $IncomeWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-income-weight/index');
        }

        $id =$this->_getParam('id');
        $incomeweight = $IncomeWeight->find($id);
        if($incomeweight->count() > 0) {
            $this->view->incomeweight = $incomeweight->current();
        } else {
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-income-weight/index');
        }
    }

    public function deleteAction() {
        $IncomeWeight = new Studentfinance_Model_DbTable_ScholarshipIncomeWeight();
        $id =$this->_getParam('id');
        $incomeweight = $IncomeWeight->find($id);
        $incomeweight->current()->delete();
        $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record deleted"));
        $this->redirect( $this->baseUrl . '/studentfinance/scholarship-income-weight/index');
    }

}