<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Studentfinance_FeeQuitController extends Base_Base {
	
	private $_DbObj;
	
	public function init(){
		$db = new Studentfinance_Model_DbTable_FeeQuit();
		$this->_DbObj = $db;
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Quit Charges - Intake List");
    	
    	
		//paginator
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$data = $intakeDb->getPaginateData();
			
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
		$this->view->paginator = $paginator;	
    	
    	   	
	}
	
	public function chargesAction(){
		
    	
    	$intake_id = $this->_getParam('intake', 0);
    	$this->view->intake_id = $intake_id;
    	
    	$intakeDb = new App_Model_Record_DbTable_Intake();
    	$this->view->intake_data = $intakeDb->getData($intake_id);
    	    	
    	//title
    	$this->view->title= $this->view->translate("Quit Charges")." (".$this->view->intake_data['IntakeDefaultLanguage'].") - Cut-off Date";
    	
    	$this->view->data = $this->_DbObj->getDataIntake($intake_id);
    	
	}
	
	public function chargesAddAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Quit Charges")." - ".$this->view->translate("Add");
    	    	
    	$intake_id = $this->_getParam('intake', null);
    	$this->view->intake_id = $intake_id;
    	
    	$form = new Studentfinance_Form_FeeQuit(array('intake'=>$intake_id));
    	
    	$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'studentfinance','controller'=>'fee-quit', 'action'=>'charges', 'intake'=>$intake_id),'default',true)."'; return false;";
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$data = array(
							'intake_id' => $formData['intake_id'],
							'last_effective_date' => date('Y-m-d', strtotime($formData['last_effective_date'])),
						);
				
				$this->_DbObj->insert($data);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-quit', 'action'=>'charges', 'intake'=>$formData['intake_id']),'default',true));	
			}else{
				$form->populate($formData);
			}
        	
        }
    	
        $this->view->form = $form;
    }
    
	public function chargesEditAction(){
		$intake_id = $this->_getParam('intake', null);
		$id = $this->_getParam('id', null);
		
		//title
    	$this->view->title= $this->view->translate("Quit Charges")." - ".$this->view->translate("Add");
    	
    	$form = new Studentfinance_Form_FeeQuit(array('intake'=>$intake_id));
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
	    	if ($form->isValid($formData)) {
				
	    		$data = array(
							'intake_id' => $formData['intake_id'],
							'last_effective_date' => date('Y-m-d', strtotime($formData['last_effective_date'])),
						);
				$this->_DbObj->update($data,"id = ".$id);
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-quit', 'action'=>'charges', 'intake'=>$formData['intake_id']),'default',true)); 
			}else{
				$form->populate($formData);	
			}
    	}else{
    		if($id!=null){
    			
    			$form->populate($this->_DbObj->getData($id));
    		}
    	}
    	
    	$this->view->form = $form;
    }
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	$intake_id = $this->_getParam('intake', null);
    	
    	if($id!=0){
    		$this->_DbObj->deleteData($id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-quit', 'action'=>'charges', 'intake'=>$intake_id),'default',true));
    	
    }
    
    public function chargesFacultyAction(){
    	//head
    	$id = $this->_getParam('id', 0);
    	$this->view->head = $this->_DbObj->getData($id);
    	
    	//intake
    	$intake_id = $this->_getParam('intake', null);
    	$this->view->intake_id = $intake_id;
    	$intakeDb = new App_Model_Record_DbTable_Intake();
    	$this->view->intake_data = $intakeDb->getData($intake_id);
    	    	
    	//title
    	$this->view->title= $this->view->translate("Quit Charges")." (".$this->view->intake_data['IntakeDefaultLanguage'].") - Faculty Charges";
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$selectData = $db->select()
					->from(array('f'=> 'tbl_collegemaster'))
					->joinLeft(array('fqp'=>'fee_quit_faculty'),'fqp.college_id = f.IdCollege and fqp.fq_id = '.$id);
			
		$row = $db->fetchAll($selectData);
		
		$this->view->data = $row;
    }
    
	public function chargesFacultyEditAction(){
		$id = $this->_getParam('id', null);
		$this->view->id = $id;
		$intake_id = $this->_getParam('intake', null);
		$this->view->intake_id = $intake_id;
		$faculty_id = $this->_getParam('faculty_id', null);
		$this->view->faculty_id = $faculty_id;
		
		//title
    	$this->view->title= $this->view->translate("Quit Charges")." - ".$this->view->translate("Faculty Charges");
    	
    	$form = new Studentfinance_Form_FeeQuitFaculty(array('feequit'=>$id,'college'=>$faculty_id, 'intake'=>$intake_id));
    	$feeQuitFacultyDb = new Studentfinance_Model_DbTable_FeeQuitFaculty();
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
	    	if ($form->isValid($formData)) {
	    		
					    		
				$data = array(
							'fq_id' => $formData['fq_id'],
	    					'college_id' => $formData['college_id'],
	    					'amount' => $formData['amount'],
						);

				$id = $feeQuitFacultyDb->getDataCollege($formData['fq_id'], $formData['college_id']);		
	    		if($id==null){
	    			$feeQuitFacultyDb->insert($data);
	    		}else{
	    			$feeQuitFacultyDb->update($data,'id = '.$id['id']);
	    		}
	    		
				
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'fee-quit', 'action'=>'charges-faculty', 'intake'=>$intake_id, 'id'=>$formData['fq_id'] ),'default',true)); 
			}else{
				$form->populate($formData);	
			}
    	}else{
    		if($faculty_id!=null){
    			
    			$data = $feeQuitFacultyDb->getDataCollege($id,$faculty_id);
    			if($data){
    				$form->populate($data);
    			}
    		}
    	}
    	
    	$this->view->form = $form;
    }
}

