<?php

class Studentfinance_ScholarshipAcademicWeightController extends Zend_Controller_Action
{

    public function indexAction() {
        $AcademicWeight = new Studentfinance_Model_DbTable_ScholarshipAcademicWeight();
        $this->view->academicweights = $AcademicWeight->fetchAll();
    }

    public function addAction() {
        $AcademicWeight = new Studentfinance_Model_DbTable_ScholarshipAcademicWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('academicweight');
            $AcademicWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-academic-weight/index');
        }
    }

    public function editAction() {
        $AcademicWeight = new Studentfinance_Model_DbTable_ScholarshipAcademicWeight();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost('academicweight');
            $AcademicWeight->save($post_data);
            $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record updated"));
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-academic-weight/index');
        }

        $id =$this->_getParam('id');
        $academicweight = $AcademicWeight->find($id);
        if($academicweight->count() > 0) {
            $this->view->academicweight = $academicweight->current();
        } else {
            $this->redirect( $this->baseUrl . '/studentfinance/scholarship-academic-weight/index');
        }
    }

    public function deleteAction() {
        $AcademicWeight = new Studentfinance_Model_DbTable_ScholarshipAcademicWeight();
        $id =$this->_getParam('id');
        $academicweight = $AcademicWeight->find($id);
        $academicweight->current()->delete();
        $this->_helper->flashMessenger->addMessage(array('success' => "Weightage record deleted"));
        $this->redirect( $this->baseUrl . '/studentfinance/scholarship-academic-weight/index');
    }

}