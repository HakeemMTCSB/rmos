<?php
class Studentfinance_SponsorinvoiceoldController extends Base_Base { //Controller for the User Module

	private $lobjSponsorInvoiceForm;
	private $lobjSponsorInvoiceModel;
	
	public function init() {
		$this->fnsetObj();
		
	}
	
	public function fnsetObj(){
		$this->lobjSponsorInvoiceModel = new Studentfinance_Model_DbTable_Sponsorinvoice();
		$this->lobjSponsorInvoiceForm = new Studentfinance_Form_Sponsorinvoice();		
	}
	
   public function indexAction() {
   
		$this->view->lobjform = $this->lobjform; 
		$larrresult = $this->lobjSponsorInvoiceModel->fngetStudentApplicationDetails();
		$studentdropdown = $this->lobjSponsorInvoiceModel->fngetStudentDropdown();
		$this->view->lobjform->field5->addMultiOptions($studentdropdown);
	
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->sponsorinvoicepaginatorresult);	
   	    	
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->sponsorinvoicepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->sponsorinvoicepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjSponsorInvoiceModel->fnSearchStudentApplication($this->lobjform->getValues()); //searching the values for the user
				//print_r($larrresult);
				//die();
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->sponsorinvoicepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {

			$this->_redirect( $this->baseUrl . '/studentfinance/sponsorinvoice/index');
		}
		
	}

	public function newsponsorinvoiceAction() { //Action for creating the new user
		$this->view->lobjSponsorInvoiceForm = $this->lobjSponsorInvoiceForm;
		$lintidapplicant =$this->_getParam('idapplicant');
		$lintIdStudentRegistration =$this->_getParam('IdStudentRegistration');
		$lintIdSemester =$this->_getParam('IdSemester');
		$lintidsponsor =$this->_getParam('idsponsor');
		$larrresultstudentdetails = $this->lobjSponsorInvoiceModel->fngetStudentDetails($lintIdStudentRegistration);
		$this->view->lobjSponsorInvoiceForm->Studentid->setValue($lintidapplicant);
		$this->view->lobjSponsorInvoiceForm->Sponsorid->setValue($lintidsponsor);
		//////////////////fecthing name and amount of prog//////////////
		$this->view->lobjSponsorInvoiceForm->StudentName->setValue($larrresultstudentdetails['FName'].' '.$larrresultstudentdetails['MName'].' '.$larrresultstudentdetails['LName']);
		$this->view->lobjSponsorInvoiceForm->ProgramName->setValue($larrresultstudentdetails['ProgramName']);
		$this->view->lobjSponsorInvoiceForm->InvoiceDate->setValue(date('Y-m-d')); 		
		$this->view->lobjSponsorInvoiceForm->SponsorName->setValue($larrresultstudentdetails['SponsorName']);
		$lintprogramid = $larrresultstudentdetails['IdProgram'];
//		$larrprogramdetails = $this->lobjSponsorInvoiceModel->fngetProgramDetails($lintprogramid);
		/*echo "<pre/>";
		print_r($larrresultstudentdetails);
		die();
		$this->view->programdetails = $larrprogramdetails;
		
	    $sum=0;
	    foreach($larrprogramdetails as $larrprogramcharges)
	    {
	    	$sum = $sum+($larrprogramcharges['Charges']);
	    }*/
	    
		//////////////////////End /////////////////////////////////////////////////
	
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjSponsorInvoiceForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjSponsorInvoiceForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$this->view->lobjSponsorInvoiceForm->MonthYear->setValue(date('M Y')); 	
		////////////////////////////////////Fetching all amount based on the regisatration id/////////////
		
		$larrresultsubjectcharges = $this->lobjSponsorInvoiceModel->fnGetSubjectCharges($lintIdStudentRegistration);
		$this->view->subjectcharges = $larrresultsubjectcharges;
		$this->view->lobjSponsorInvoiceForm->AcademicPeriod->setValue($lintIdSemester);
		$product=0;
	    foreach($larrresultsubjectcharges as $Totalamount)
	    {
	    	$product = $product+($Totalamount['CreditHours']*$Totalamount['AmtPerHour']);
	    }
	    $totalinvoiceamount = $sum+$product;
	    $this->view->lobjSponsorInvoiceForm->InvoiceAmount->setValue($totalinvoiceamount); 
		/////////////////////////////////////////////////////////////////
		

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post
			/*echo "<pre/>";
			print_r($larrformData);
			die();*/
				$result = $this->lobjSponsorInvoiceModel->fnAddInvoiceMaster($larrformData); //instance for adding the lobjuserForm values to DB
				//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/studentfinance/invoice/index');
			
		}

	}

	
}