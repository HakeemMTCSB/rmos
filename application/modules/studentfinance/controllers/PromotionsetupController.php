<?php
class Studentfinance_PromotionsetupController extends Base_Base { //Controller for the User Module

    private $_gobjlog;
    private $lobjconfig;
    private $lobjUser;
    private $lobjSubjectsetup;
    public $lobjform;


    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        $this->fnsetObj();

    }

    public function fnsetObj(){

        $this->lobjPromotionSetupForm = new Studentfinance_Form_Promotionsetup();
        $this->lobjPromotionSetup = new Studentfinance_Model_DbTable_Promotion();
        $this->lobjPromotionFeeSetup = new Studentfinance_Model_DbTable_PromotionFee();
        $this->lobjPromotionProgramSetup = new Studentfinance_Model_DbTable_PromotionProgram();
        $this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
        $this->lobjUser = new GeneralSetup_Model_DbTable_User();
        $this->lobjFeeItem = new Studentfinance_Model_DbTable_PromotionFee();
        $this->lobjProgram = new Studentfinance_Model_DbTable_PromotionProgram();

    }

    public function indexAction()
    {
        $this->view->title = 'Promotion Setup';

        //Form
        $this->view->lobjform = $this->lobjform;

        $larrresult = $this->lobjPromotionSetup->fnSearchPromotionSetup( $post = NULL);

        if(!$this->_getParam('search'))
            unset($this->gobjsessionsis->PromotionSetuppaginatorresult);

        //$lintpagecount = $this->gintPageCount;
        $lintpagecount = 50;
        $lobjPaginator = new App_Model_Common(); // Definitiontype model
        $lintpage = $this->_getParam('page',1); // Paginator instance

        if(isset($this->gobjsessionsis->PromotionSetuppaginatorresult)) {
            $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->PromotionSetuppaginatorresult,$lintpage,$lintpagecount);


        }
        else {
            $this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);

        }

        if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
            $larrformData = $this->_request->getPost ();

            $larrresult = $this->lobjPromotionSetup->fnSearchPromotionSetup($larrformData);
            $this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
            $this->gobjsessionsis->PromotionSetuppaginatorresult = $larrresult;



        }

        $totalRecord = count($larrresult);
        $this->view->totalRecord =$totalRecord;

        if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
            $this->_redirect( $this->baseUrl . '/studentfinance/promotionsetup/index');
        }


    }

    public function addAction()
    {
        $this->view->title = 'Add New Promotion Setup';

        //Form
        $this->view->lobjPromotionSetupForm = $this->lobjPromotionSetupForm;

        //For user Id and university
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $UpdDate = date('Y-m-d H:i:s');
        $lIntIdUniversity = $this->gobjsessionsis->idUniversity;
       // $larrconfig = $this->lobjSubjectsetup->getConfigDetail($lIntIdUniversity);

        //Render to View
        $this->view->lobjPromotionSetupForm->IdUniversity->setvalue ($lIntIdUniversity);
        $this->view->lobjPromotionSetupForm->UpdDate->setValue ($UpdDate);
        $this->view->lobjPromotionSetupForm->UpdUser->setValue ($userId);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->lobjPromotionSetupForm->isValid($formData)) {
                $auth = Zend_Auth::getInstance();

                $flag = false;

                //add promotion
                $promo['promotion_code']=$formData['Promotion_Code'];
                $promo['promotion_description']=$formData['Promotion_description'];
                $promo['awardid']=$formData['Awardid'];
               /* $promo['sem_start'] = date('Y-m-d',strtotime($formData['sem_start']));
                $promo['sem_end'] = date('Y-m-d',strtotime($formData['sem_end']));
                $promo['reg_start'] = date('Y-m-d',strtotime($formData['reg_start']));
                $promo['reg_end'] = date('Y-m-d',strtotime($formData['reg_end']));*/
                $promo['sem_start']= $formData['sem_start'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['sem_start']));
                $promo['sem_end']= $formData['sem_end'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['sem_end']));
                $promo['reg_start']= $formData['reg_start'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['reg_start']));
                $promo['reg_end']= $formData['reg_end'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['reg_end']));
                $promo['min_payment']=$formData['min_payment'];
                $promo['total_sem']=$formData['total_sem'];
                $promo['adjustment_type']=$formData['adjustment_type'];
                $promo['exclusive']=$formData['exclusive'];
                $promo['active']=$formData['active'];
                $promo['createdby'] = $auth->getIdentity()->iduser;
                $promo['createddt'] = date('Y-m-d H:i:s');

                    //check duplicate data
                    $duplicate_promo = $this->lobjPromotionSetup->checkDuplicate($formData['Promotion_Code']);

                    if(!$duplicate_promo){

                        //add promo
                        $lastId = $this->lobjPromotionSetup->addData($promo);

                        //add fee info

                        for($i = 0; $i < count($formData['feecategoryinfo']); $i++) {
                            $feeinfo = array();

                            //$result = $this->lobjPromotionSetup->checkDuplicateFee($formData['fee_category_info'][$i],  $lastId);

                           // if (!$result) {
                            $feeinfo['promotion_id'] = $lastId;
                            $feeinfo['fee_category'] = $formData['feecategoryinfo'][$i];
                            $feeinfo['calc_type'] = $formData['calctypeinfo'][$i];
                            $feeinfo['semester'] = $formData['seminfo'][$i];
                            $feeinfo['value'] = $formData['valueinfo'][$i];
                            $feeinfo['createdby'] = $auth->getIdentity()->iduser;
                            $feeinfo['createddt'] = date('Y-m-d H:i:s');


                            $this->lobjFeeItem->addData($feeinfo);
                           // }
                        }

                        for($i = 0; $i < count($formData['programdesc']); $i++) {
                            $program = array();

                            //$result = $this->lobjPromotionSetup->checkDuplicateFee($formData['fee_category_info'][$i],  $lastId);

                            // if (!$result) {
                            $program['promotion_id'] = $lastId;
                            $program['id_program'] = $formData['program'][$i];
                            $program['createdby'] = $auth->getIdentity()->iduser;
                            $program['createddt'] = date('Y-m-d H:i:s');


                            $this->lobjProgram->addData($program);
                            // }
                        }

                        $flag = true;
                    }


                if($flag==true){
                    $this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
                }else{
                    $this->_helper->flashMessenger->addMessage(array('error' => "Duplicate entry of promotion code"));
                }

                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'promotionsetup', 'action'=>'index'),'default',true));
            }
        }

    }





    public function editAction()
    {
        $this->view->title = 'Edit Promotion';

        //Form
        $this->view->lobjPromotionSetupForm = $this->lobjPromotionSetupForm;

        $id = $this->_getParam ( 'id' );
        $this->view->promo_id = $id;

        //For user Id and university
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $UpdDate = date('Y-m-d H:i:s');

        //Render to View
        $this->view->lobjPromotionSetupForm->UpdDate->setValue ($UpdDate);
        $this->view->lobjPromotionSetupForm->UpdUser->setValue ($userId);

        //Fee Item
        /*$feeList = $this->lobjFeeItem->getData();
        foreach( $feeList as $item)
        {
            $this->view->lobjPromotionSetupForm->FeeCode->addMultiOption($item['fi_id'],$item['fi_name']);
        }*/

        //Country
      //  $lobjcountry = $this->lobjUser->fnGetCountryList();
       // $this->lobjPromotionSetupForm->Country->addMultiOptions($lobjcountry);



        $result = $this->lobjPromotionSetup->fngetpromotionsetupById($id);
        $this->view->result = $result;

        $paramArray = array('Promotion_Code' => $result[0]['promotion_code'],
            'Promotion_description' => $result[0]['promotion_description'],
            'Awardid' => $result[0]['awardid'],
            'sem_start' => $result[0]['sem_start'],
            'sem_end' => $result[0]['sem_end'],
            'reg_start' => $result[0]['reg_start'],
            'reg_end' => $result[0]['reg_end'],
            'min_payment' => $result[0]['min_payment'],
            'total_sem' => $result[0]['total_sem'],
            'adjustment_type' => $result[0]['adjustment_type'],
            'exclusive' => $result[0]['exclusive'],
            'active' => $result[0]['active']);


        $this->lobjPromotionSetupForm->populate($paramArray);

        $this->view->lobjPromotionSetupForm->Promotion_Code->setAttrib('readonly','true');

        //get fee info
        $feeinfo = $this->lobjPromotionFeeSetup->getFeeInfo($result[0]['promotion_id']);

        //get program
        $program = $this->lobjPromotionProgramSetup->getProgram($result[0]['promotion_id']);

        //
        $this->view->feeinfo = $feeinfo;
        $this->view->program = $program;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->lobjPromotionSetupForm->isValid($formData)) {
                $auth = Zend_Auth::getInstance();

                $flag = false;

                //add promotion
                $promo['promotion_description']=$formData['Promotion_description'];
                $promo['awardid']=$formData['Awardid'];
               /* $promo['sem_start'] = date('Y-m-d',strtotime($formData['sem_start']));
                $promo['sem_end'] = date('Y-m-d',strtotime($formData['sem_end']));
                $promo['reg_start'] = date('Y-m-d',strtotime($formData['reg_start']));
                $promo['reg_end'] = date('Y-m-d',strtotime($formData['reg_end']));*/
                $promo['sem_start']= $formData['sem_start'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['sem_start']));
                $promo['sem_end']= $formData['sem_end'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['sem_end']));
                $promo['reg_start']= $formData['reg_start'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['reg_start']));
                $promo['reg_end']= $formData['reg_end'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['reg_end']));
                $promo['min_payment']=$formData['min_payment'];
                $promo['total_sem']=$formData['total_sem'];
                $promo['adjustment_type']=$formData['adjustment_type'];
                $promo['exclusive']=$formData['exclusive'];
                $promo['active']=$formData['active'];

                $this->lobjPromotionSetup->updatePromotion($promo,$id);


                //check duplicate data
               // $duplicate_promo = $this->lobjPromotionSetup->checkDuplicate($formData['Promotion_Code']);

               // if(!$duplicate_promo){

                    //add promo
                   // $lastId = $this->lobjPromotionSetup->addData($promo);

                    //add fee info

                    for($i = 0; $i < count($formData['feecategoryinfo']); $i++) {
                        $feeinfo = array();

                        //$result = $this->lobjPromotionSetup->checkDuplicateFee($formData['fee_category_info'][$i],  $lastId);

                        // if (!$result) {
                        $feeinfo['promotion_id'] = $id;
                        $feeinfo['fee_category'] = $formData['feecategoryinfo'][$i];
                        $feeinfo['calc_type'] = $formData['calctypeinfo'][$i];
                        $feeinfo['semester'] = $formData['seminfo'][$i];
                        $feeinfo['value'] = $formData['valueinfo'][$i];
                        $feeinfo['createdby'] = $auth->getIdentity()->iduser;
                        $feeinfo['createddt'] = date('Y-m-d H:i:s');


                        $this->lobjFeeItem->addData($feeinfo);
                        // }
                    }

                    for($i = 0; $i < count($formData['programdesc']); $i++) {
                        $program = array();

                        //$result = $this->lobjPromotionSetup->checkDuplicateFee($formData['fee_category_info'][$i],  $lastId);

                        // if (!$result) {
                        $program['promotion_id'] = $id;
                        $program['id_program'] = $formData['program'][$i];
                        $program['createdby'] = $auth->getIdentity()->iduser;
                        $program['createddt'] = date('Y-m-d H:i:s');


                        $this->lobjProgram->addData($program);
                        // }
                    }

                    $flag = true;
               // }


                if($flag==true){
                    $this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
                }else{
                    $this->_helper->flashMessenger->addMessage(array('error' => "Duplicate entry of promotion code"));
                }

                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'promotionsetup', 'action'=>'index'),'default',true));
            }
        }

   /*     if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
            $larrformData = $this->_request->getPost ();

            if (isset($larrformData['active']) && count($larrformData['active']) > 0){
                foreach ($larrformData['active'] as $key=>$value){

                    $dataActive = array(
                        'c_active'=>$value
                    );
                    //update coordinator status
                    $this->lobjSponsorSetup->updateCoordinator($dataActive, $key);
                }
            }

            $this->lobjSponsorSetup->fnupdateSponsorSetup($larrformData,$id);


            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect( $this->baseUrl . '/studentfinance/sponsorsetup/edit/id/'.$id);
        }*/
    }


   /* public function deleteFeeAction()
    {
        $db = getDB();

        $id = $this->_getParam('id');
echo "masuk:".$id;exit;
        if ( $id != '' )
        {
            $db->delete('tbl_promotion_fee_detail', 'id = '.(int) $id);

            $data = array('msg' => '');

            echo Zend_Json::encode($data);
            exit;
        }
        else
        {
            $data = array('msg' => 'Invalid ID');

            echo Zend_Json::encode($data);
            exit;
        }
    }*/

    public function deleteFeeAction(){

        $id = $this->_getParam('id');
        $promo_id = $this->_getParam('promo_id');

        $this->lobjFeeItem->deleteData($id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Selected Fee deleted."));

        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'promotionsetup', 'action'=>'edit','id'=>$promo_id),'default',true));
    }

    public function deleteProgramAction(){

        $id = $this->_getParam('id');
        $promo_id = $this->_getParam('promo_id');

        $this->lobjProgram->deleteData($id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Selected Program deleted."));

        $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'promotionsetup', 'action'=>'edit','id'=>$promo_id),'default',true));
    }

    /*public function deleteProgramAction()
    {
        $db = getDB();

        $id = $this->_getParam('id');

        if ( $id != '' )
        {
            $db->delete('tbl_promotion_program', 'id = '.(int) $id);

            $data = array('msg' => '');

            echo Zend_Json::encode($data);
            exit;
        }
        else
        {
            $data = array('msg' => 'Invalid ID');

            echo Zend_Json::encode($data);
            exit;
        }
    }*/


    public function getcodeAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $idprog =  $this->_getParam ( 'idprogram' );

        $larrdesc = $this->lobjProgram->fngetcode($idprog);

        echo $larrdesc;
    }

}