<?php
class Studentfinance_Form_Accountgroup extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	        
    	$UpdDate = 	new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
     
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $IdAccountGroup = new Zend_Form_Element_Hidden('IdAccountGroup');
        $IdAccountGroup->removeDecorator("DtDdWrapper");
        $IdAccountGroup->removeDecorator("Label");
        $IdAccountGroup->removeDecorator('HtmlTag');      

        $GroupName 	 = new Zend_Form_Element_Text('GroupName');
        $GroupName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $GroupName->removeDecorator("DtDdWrapper");
        $GroupName->removeDecorator("Label");
        $GroupName->removeDecorator('HtmlTag');
        $GroupName->setAttrib('required',"true"); 
        $GroupName->setAttrib('propercase',"true");   
        
        $GroupCode 	 = new Zend_Form_Element_Text('GroupCode');
        $GroupCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $GroupCode->removeDecorator("DtDdWrapper");
        $GroupCode->removeDecorator("Label");
        $GroupCode->removeDecorator('HtmlTag');
        $GroupCode->setAttrib('required',"true");   
         
        $IdParent = new Zend_Dojo_Form_Element_FilteringSelect('IdParent');   
        $IdParent->	addMultiOption('0', 'Select');
        $IdParent->setAttrib('required',"false")       		        
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label") 				
				->removeDecorator('HtmlTag')		
				->setRegisterInArrayValidator(false)				
        		-> setAttrib('dojoType',"dijit.form.FilteringSelect");

        $Description = new Zend_Form_Element_Text('Description');
        $Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');
        $Description->setAttrib('required',"true"); 
        
        $Active = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag')
        	->	setValue("1");		

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->setAttrib('class','NormalBtn');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('id', 'submitbutton');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        
        
         
   		$this->addElements(array($UpdDate,
   								 $UpdUser,
   								 $IdAccountGroup,
   								 $GroupName,
   								 $GroupCode,
   								 $IdParent,
   								 $Description,
   								 $Active,
   								 $Save));
    }
}