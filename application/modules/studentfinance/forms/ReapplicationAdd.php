<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_Form_ReapplicationAdd extends Zend_Dojo_Form {
    
    public function init(){
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');

        $scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();

        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
        $programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        $programscheme->removeDecorator("Label");

        $programschemelist = $scholarshipModel->getProgSchemeBasedOnProg($programid);

        if ($programschemelist){
            foreach ($programschemelist as $programschemeloop){
                $programscheme->addMultiOption($programschemeloop['IdProgramScheme'], $programschemeloop['mop'].' '.$programschemeloop['mos'].' '.$programschemeloop['pt']);
            }
        }

        //student category
        $studentcategory = new Zend_Form_Element_Select('studentcategory');
        $studentcategory->removeDecorator("DtDdWrapper");
        $studentcategory->setAttrib('class', 'select');
        $studentcategory->removeDecorator("Label");

        $studentcategorylist = $scholarshipModel->getDefinationByType(95);

        if ($studentcategorylist){
            foreach ($studentcategorylist as $studentcategoryloop){
                $studentcategory->addMultiOption($studentcategoryloop['idDefinition'], $studentcategoryloop['DefinitionDesc']);
            }
        }

        //type
        $stype = new Zend_Form_Element_Select('stype');
	$stype->removeDecorator("DtDdWrapper");
        $stype->setAttrib('class', 'select');
	$stype->removeDecorator("Label");
        $stype->setAttrib('onchange', 'getScholarship(this.value);');
        
        $stype->addMultiOption('1', 'Scholarship');
        $stype->addMultiOption('2', 'Sponsorship');
        $stype->addMultiOption('3', 'Financial Aid');
        
        //scholarship
        $ScholarshipType = new Zend_Form_Element_Select('ScholarshipType');
	$ScholarshipType->removeDecorator("DtDdWrapper");
        $ScholarshipType->setAttrib('class', 'select');
	$ScholarshipType->removeDecorator("Label");
        //$ScholarshipType->setAttrib('onchange', 'getScholarship(this.value);');
        
        $scholarshipList = $scholarshipModel->getScholarshipList();
        foreach ( $scholarshipList as $row ){
            $ScholarshipType->addMultiOption($row['sch_Id'], $row['sch_name']);
        }

        $this->addElements(array(
            $programscheme,
            $studentcategory,
            $stype,
            $ScholarshipType
        ));
    }
}
?>