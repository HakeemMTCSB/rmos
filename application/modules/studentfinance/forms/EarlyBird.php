<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/8/2016
 * Time: 10:40 AM
 */
class Studentfinance_Form_EarlyBird extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $model = new Studentfinance_Model_DbTable_EarlyBird();

        //semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('onchange', 'getProgram(this.value); getSemester(this.value);');
        $semester->setAttrib('required', true);
        $semester->removeDecorator("Label");

        $semester->addMultiOption('', '-- Select --');

        $semList = $model->getSemester();

        if ($semList){
            foreach ($semList as $semLoop) {
                $semester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName'].' - '.$semLoop['SemesterMainCode']);
            }
        }

        //discount type
        $discounttype = new Zend_Form_Element_Select('discounttype');
        $discounttype->removeDecorator("DtDdWrapper");
        $discounttype->setAttrib('class', 'select');
        $discounttype->setAttrib('required', true);
        $discounttype->removeDecorator("Label");

        $discounttype->addMultiOption('', '-- Select --');

        $disList = $model->discountType();

        if ($disList){
            foreach ($disList as $disLoop) {
                $discounttype->addMultiOption($disLoop['dt_id'], $disLoop['dt_discount']);
            }
        }

        //Payment Date
        $paymentdate = new Zend_Form_Element_Text('paymentdate');
        $paymentdate->setAttrib('class', 'input-txt')
            ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //name
        $eb_name = new Zend_Form_Element_Text('eb_name');
        $eb_name->setAttrib('class', 'input-txt')
            ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //code
        $eb_code = new Zend_Form_Element_Text('eb_code');
        $eb_code->setAttrib('class', 'input-txt')
            ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $semester,
            $paymentdate,
            $discounttype,
            $eb_name,
            $eb_code
        ));
    }
}