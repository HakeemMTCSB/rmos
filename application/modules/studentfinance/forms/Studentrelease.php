<?php
class Studentfinance_Form_Studentrelease extends Zend_Dojo_Form {
	/**
	 *
	 */
	public function init() {

		$lobjdefinition = new App_Model_Definitiontype();
		$lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$IdStudentRelease = new Zend_Form_Element_Hidden('IdStudentRelease');
		$IdStudentRelease->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Category = new Zend_Dojo_Form_Element_FilteringSelect('IdCategory');
		$Category->removeDecorator("DtDdWrapper");
		$Category->setAttrib('required', "true");
		$Category->removeDecorator("Label");
		$Category->removeDecorator('HtmlTag');
		$Category->setAttrib('OnChange', 'fnGetList');
		$Category->setRegisterInArrayValidator(false);
		$Category->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$options = array (
			"1" => "Student",
			"2" => "Applicant"
		);
		$Category->addMultiOptions($options);

		$List = new Zend_Dojo_Form_Element_FilteringSelect('IdStudent');
		$List->removeDecorator("DtDdWrapper");
		$List->setAttrib('required', "true");
		$List->removeDecorator("Label");
		$List->removeDecorator('HtmlTag');
		$List->setRegisterInArrayValidator(false);
		$List->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$ReleaseType = new Zend_Dojo_Form_Element_FilteringSelect('IdReleaseType');
		$ReleaseType->removeDecorator("DtDdWrapper");
		$ReleaseType->setAttrib('required', "true");
		$ReleaseType->removeDecorator("Label");
		$ReleaseType->removeDecorator('HtmlTag');
		$ReleaseType->setRegisterInArrayValidator(false);
		$ReleaseType->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$ReleaseType->addMultiOptions($lobjdefinition->fnGetDefinationMs("Release Type"));

		$IdIntake = new Zend_Dojo_Form_Element_FilteringSelect('IdIntake');
		$IdIntake->removeDecorator("DtDdWrapper");
		$IdIntake->setAttrib('required', "true");
		$IdIntake->removeDecorator("Label");
		$IdIntake->removeDecorator('HtmlTag');
		$IdIntake->setRegisterInArrayValidator(false);
		$IdIntake->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$IdIntake->addMultiOptions($lobjintake->fngetallIntake());

		$Reason = new Zend_Form_Element_Textarea('Reason');
		$Reason->removeDecorator("DtDdWrapper");
		$Reason->removeDecorator("Label");
		$Reason->removeDecorator('HtmlTag');
		$Reason->setAttrib("rows", "5");
		$Reason->setAttrib("cols", "30");

		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype = "dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag');

		$this->addElements(array (
			$IdStudentRelease,
			$Category,
			$IdIntake,
			$List,
			$ReleaseType,
			$Reason,
			$UpdDate,
			$UpdUser,
			$Save
		));
	}
}