<?php
class Studentfinance_Form_SponsorApplication extends Zend_Dojo_Form { //Formclass for the Programmaster  module
public $cust_types = array(
        1 => 'Student',
        2 => 'Applicant',
        3 => 'Admin'
    );

    public $status = array(
        1 => 'new',
        2 => 'approved',
        3 => 'rejected'
    );
	
    
    public function init() {
        $gstrtranslate =Zend_Registry::get( 'Zend_Translate' );
        $SponsorApplication =  new Studentfinance_Model_DbTable_SponsorApplication();

        $sa_id  = new Zend_Form_Element_Hidden( 'sa_id' );
        $sa_id->removeDecorator( "DtDdWrapper" );
        $sa_id->setAttrib( 'dojoType', "dijit.form.TextBox" );
        $sa_id->removeDecorator( "Label" );
        $sa_id->removeDecorator( 'HtmlTag' );


        $ApplicantFinancial = new Studentfinance_Model_DbTable_ApplicantFinancial();
        $applicant_financials = $ApplicantFinancial->get_list();
        $sa_af_id = new Zend_Dojo_Form_Element_FilteringSelect( 'sa_af_id' );
        $sa_af_id->removeDecorator( "DtDdWrapper" );
        $sa_af_id->setAttrib( 'required', "true" ) ;
        $sa_af_id->removeDecorator( "Label" );
        $sa_af_id->removeDecorator( 'HtmlTag' );
        $sa_af_id->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        $sa_af_id->addMultiOptions( $applicant_financials );


        $sa_cust_type = new Zend_Dojo_Form_Element_FilteringSelect( 'sa_cust_type' );
        $sa_cust_type->removeDecorator( "DtDdWrapper" );
        $sa_cust_type->setAttrib( 'required', "true" ) ;
        $sa_cust_type->removeDecorator( "Label" );
        $sa_cust_type->removeDecorator( 'HtmlTag' );
        $sa_cust_type->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        $sa_cust_type->addMultiOptions( $SponsorApplication->cust_types);

        $sa_status = new Zend_Dojo_Form_Element_FilteringSelect( 'sa_status' );
        $sa_status->removeDecorator( "DtDdWrapper" );
        $sa_status->setAttrib( 'required', "true" ) ;
        $sa_status->removeDecorator( "Label" );
        $sa_status->removeDecorator( 'HtmlTag' );
        $sa_status->setAttrib( 'dojoType', "dijit.form.FilteringSelect" );
        $sa_status->addMultiOptions( $SponsorApplication->status );

        $this->addElement('submit', 'save', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));


        $this->addElements( array( 
        		$sa_id, $sa_af_id,
        		$sa_cust_type, $sa_status
            ) );

    }
}