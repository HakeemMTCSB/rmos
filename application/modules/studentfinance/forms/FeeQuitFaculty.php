<?php

class Studentfinance_Form_FeeQuitFaculty extends Zend_Form
{
	protected $feequit;
	protected $college;
	protected $intake;
	
	public function setIntake($intake){
		$this->intake = $intake;
	}
	
	public function setFeequit($feequit){
		$this->feequit = $feequit;
	}
	
	public function setCollege($college){
		$this->college = $college;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_quit_charges');
		
		//head id
		$element = new Zend_Form_Element_Hidden('fq_id');
		$element->setValue($this->feequit);
		$this->addElement($element);
		
		//college id
		$element = new Zend_Form_Element_Hidden('college_id');
		$element->setValue($this->college);
		$this->addElement($element);
		
		//amount
		$this->addElement('text','amount', 
			array(
				'label'=>'Amount'
			)
		);
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-quit','action'=>'charges-faculty', 'intake'=>$this->intake, 'id'=>$this->feequit),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
	
	private function getIntake($intake_id){
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeData = $intakeDb->getData($intake_id);
		
		return $intakeData;
	}
}
?>