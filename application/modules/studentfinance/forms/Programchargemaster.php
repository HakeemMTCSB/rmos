<?php
class Studentfinance_Form_Programchargemaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdProgramCharges  = new Zend_Form_Element_Hidden('IdProgramCharges');
        $IdProgramCharges->removeDecorator("DtDdWrapper");
        $IdProgramCharges->removeDecorator("Label");
        $IdProgramCharges->removeDecorator('HtmlTag');
        
        $IdProgram  = new Zend_Form_Element_Hidden('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        
                		
        $IdCharges = new Zend_Dojo_Form_Element_FilteringSelect('IdCharges');
        $IdCharges->removeDecorator("DtDdWrapper");
        $IdCharges->setAttrib('required',"true") ;
        $IdCharges->removeDecorator("Label");
        $IdCharges->removeDecorator('HtmlTag')
        ->setAttrib('OnChange', "fnGetChargelist(this)");
        $IdCharges->setRegisterInArrayValidator(false);
		$IdCharges->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		$Charges = new Zend_Form_Element_Text('Charges');
		$Charges->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Charges->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','255')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('OnClick', 'addChargeDetails()')
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
		$Add->dojotype="dijit.form.Button";
		$Add->label = $gstrtranslate->_("Add");
         				
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdProgramCharges,
        						  $IdProgram,
        						  $IdCharges,
        						  $Charges,
        						  $UpdDate,
        						  $UpdUser,
        						  $Save,
        						  $Clear,
        						  $Add
                                 ));
    }
}