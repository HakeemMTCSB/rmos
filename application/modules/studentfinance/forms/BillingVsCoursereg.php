<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 28/3/2016
 * Time: 9:03 AM
 */
class Studentfinance_Form_BillingVsCoursereg extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');

        $model = new Studentfinance_Model_DbTable_BillingVsCoursereg();

        //program
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getIntake(this); getSemester(this);');
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programList = $model->programList();

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }

        $program->addMultiOption('ALL', 'All');

        //intake
        $intake = new Zend_Form_Element_Select('intake');
        $intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
        $intake->setAttrib('multiple', true);
        $intake->removeDecorator("Label");

        $intakeList = $model->getIntake();

        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }

        //semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('multiple', true);
        $semester->removeDecorator("Label");

        if ($programid){
            $programInfo = $model->programById($programid);

            if ($programInfo){
                $semesterList = $model->getSemester($programInfo['IdScheme']);

                if ($semesterList){
                    foreach ($semesterList as $semesterLoop){
                        $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
                    }
                }
            }
        }

        //date from
        $datefrom = new Zend_Form_Element_Text('datefrom');
        $datefrom->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //date to
        $dateto = new Zend_Form_Element_Text('dateto');
        $dateto->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $program,
            $intake,
            $semester,
            $datefrom,
            $dateto
        ));
    }
}