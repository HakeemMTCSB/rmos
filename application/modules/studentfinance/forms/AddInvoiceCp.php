<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/3/2016
 * Time: 11:29 AM
 */
class Studentfinance_Form_AddInvoiceCp extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        //load model
        $model = new Studentfinance_Model_DbTable_InvoiceCp();

        //branch
        $branch = new Zend_Form_Element_Select('branch');
        $branch->removeDecorator("DtDdWrapper");
        $branch->setAttrib('class', 'select');
        $branch->setAttrib('required', true);
        $branch->removeDecorator("Label");

        $branch->addMultiOption('', '-- Select --');

        $branchList = $model->getBranch();

        if ($branchList){
            foreach ($branchList as $branchLoop){
                $branch->addMultiOption($branchLoop['IdBranch'], $branchLoop['BranchName']);
            }
        }

        //Invoice Date
        $invoice_date = new Zend_Form_Element_Text('invoice_date');
        $invoice_date->setAttrib('class', 'input-txt')
            ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //semester
        $invoice_semester = new Zend_Form_Element_Select('invoice_semester');
        $invoice_semester->removeDecorator("DtDdWrapper");
        $invoice_semester->setAttrib('class', 'select');
        $invoice_semester->setAttrib('required', true);
        $invoice_semester->removeDecorator("Label");

        $invoice_semester->addMultiOption('', '-- Select --');

        $semesterList = $model->getSemester();

        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $invoice_semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
            }
        }

        //type
        $invoice_type = new Zend_Form_Element_Select('invoice_type');
        $invoice_type->removeDecorator("DtDdWrapper");
        $invoice_type->setAttrib('class', 'select');
        $invoice_type->setAttrib('required', true);
        $invoice_type->removeDecorator("Label");
        $invoice_type->setAttrib('onchange', 'defineType(this.value);');

        $invoice_type->addMultiOption(1, 'Student');
        $invoice_type->addMultiOption(2, 'Non-Student');

        //main description
        $invoice_main_desc = new Zend_Form_Element_Textarea('invoice_main_desc');
        $invoice_main_desc->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //fee item
        $fee_item = new Zend_Form_Element_Select('fee_item');
        $fee_item->removeDecorator("DtDdWrapper");
        $fee_item->setAttrib('class', 'select');
        $fee_item->removeDecorator("Label");

        $fee_item->addMultiOption('', '-- Select --');

        $itemList = $model->getItem('CP');

        if ($itemList){
            foreach ($itemList as $itemLoop){
                $fee_item->addMultiOption($itemLoop['fi_id'], $itemLoop['fi_name'].' ('.$itemLoop['fi_code'].')');
            }
        }

        //currency
        $currency = new Zend_Form_Element_Select('currency');
        $currency->removeDecorator("DtDdWrapper");
        $currency->setAttrib('class', 'select');
        $currency->setAttrib('required', true);
        $currency->removeDecorator("Label");

        $currency->addMultiOption('', '-- Select --');

        $currencyDb = new Studentfinance_Model_DbTable_Currency();
        $currencyList = $currencyDb->getList();

        if ($currencyList){
            foreach ($currencyList as $currencyLoop){
                $currency->addMultiOption($currencyLoop['cur_id'], $currencyLoop['cur_code']);
            }
        }

        //amount
        $amount = new Zend_Form_Element_Text('amount');
        $amount->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        //detail description
        $invoice_details_desc = new Zend_Form_Element_Textarea('invoice_details_desc');
        $invoice_details_desc->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $branch,
            $invoice_date,
            $invoice_semester,
            $invoice_type,
            $invoice_main_desc,
            $fee_item,
            $currency,
            $amount,
            $invoice_details_desc
        ));
    }
}