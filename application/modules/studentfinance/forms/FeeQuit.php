<?php

class Studentfinance_Form_FeeQuit extends Zend_Form
{
	protected $intake;
	
	public function setIntake($intake){
		$this->intake = $intake;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_quit_charges');
		
		//intake
		$intake_id = new Zend_Form_Element_Hidden('intake_id');
		$intake_id->setValue($this->intake);
		$this->addElement($intake_id);
		
		$this->addElement('text','intake_lbl', 
			array(
				'label'=>'Intake',
				'disabled'=>'disabled',
			)
		);
		
		$intake_data = $this->getIntake($this->intake);
		
		$this->intake_lbl->setValue($intake_data['IntakeDefaultLanguage']);
		
		
		
		$element = new ZendX_JQuery_Form_Element_DatePicker('last_effective_date',
            array(
            	'jQueryParams' => array('dateFormat' => 'dd-mm-yy'),
            	'label'=>'Last Effective Date',
            	'required'=>'true'
            )
        );
		$this->addElement($element);
		
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'general-setup', 'controller'=>'highschool-discipline','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
	
	private function getIntake($intake_id){
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeData = $intakeDb->getData($intake_id);
		
		return $intakeData;
	}
}
?>