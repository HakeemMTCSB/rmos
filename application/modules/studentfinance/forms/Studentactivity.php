<?php

class Studentfinance_Form_Studentactivity extends Zend_Dojo_Form
{

	public function init()
	{

		$gstrtranslate =Zend_Registry::get('Zend_Translate');


		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$IdUniversity = new Zend_Form_Element_Hidden('IdUniversity');
		$IdUniversity->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$ChangeProgramFeeSetup = new Zend_Form_Element_Select('ChangeProgramFeeSetup');
		$ChangeProgramFeeSetup->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeProgramFeeSetup->removeDecorator("DtDdWrapper");
		$ChangeProgramFeeSetup->setAttrib('required',"true");
		$ChangeProgramFeeSetup->removeDecorator("Label");
		$ChangeProgramFeeSetup->removeDecorator('HtmlTag');
		$ChangeProgramFeeSetup->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ChangeStatusFeeSetup = new Zend_Form_Element_Select('ChangeStatusFeeSetup');
		$ChangeStatusFeeSetup->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeStatusFeeSetup->removeDecorator("DtDdWrapper");
		$ChangeStatusFeeSetup->setAttrib('required',"true");
		$ChangeStatusFeeSetup->removeDecorator("Label");
		$ChangeStatusFeeSetup->removeDecorator('HtmlTag');
		$ChangeStatusFeeSetup->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$CreditTransferFeeSetup = new Zend_Form_Element_Select('CreditTransferFeeSetup');
		$CreditTransferFeeSetup->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CreditTransferFeeSetup->removeDecorator("DtDdWrapper");
		$CreditTransferFeeSetup->setAttrib('required',"true");
		$CreditTransferFeeSetup->removeDecorator("Label");
		$CreditTransferFeeSetup->removeDecorator('HtmlTag');
		$CreditTransferFeeSetup->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ResitExamFeeSetup = new Zend_Form_Element_Select('ResitExamFeeSetup');
		$ResitExamFeeSetup->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ResitExamFeeSetup->removeDecorator("DtDdWrapper");
		$ResitExamFeeSetup->setAttrib('required',"true");
		$ResitExamFeeSetup->removeDecorator("Label");
		$ResitExamFeeSetup->removeDecorator('HtmlTag');
		$ResitExamFeeSetup->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$PaperReviewFeeSetup = new Zend_Form_Element_Select('PaperReviewFeeSetup');
		$PaperReviewFeeSetup->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PaperReviewFeeSetup->removeDecorator("DtDdWrapper");
		$PaperReviewFeeSetup->setAttrib('required',"true");
		$PaperReviewFeeSetup->removeDecorator("Label");
		$PaperReviewFeeSetup->removeDecorator('HtmlTag');
		$PaperReviewFeeSetup->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ChangeProgramChargeable = new Zend_Form_Element_Select('ChangeProgramChargeable');
		$ChangeProgramChargeable->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeProgramChargeable->removeDecorator("DtDdWrapper");
		$ChangeProgramChargeable->setAttrib('required',"true");
		$ChangeProgramChargeable->removeDecorator("Label");
		$ChangeProgramChargeable->removeDecorator('HtmlTag');
		$ChangeProgramChargeable->setAttrib('dojoType',"dijit.form.FilteringSelect");



		$ChangeStatusChargeable = new Zend_Form_Element_Select('ChangeStatusChargeable');
		$ChangeStatusChargeable->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeStatusChargeable->removeDecorator("DtDdWrapper");
		$ChangeStatusChargeable->setAttrib('required',"true");
		$ChangeStatusChargeable->removeDecorator("Label");
		$ChangeStatusChargeable->removeDecorator('HtmlTag');
		$ChangeStatusChargeable->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$CreditTransferChargeable = new Zend_Form_Element_Select('CreditTransferChargeable');
		$CreditTransferChargeable->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CreditTransferChargeable->removeDecorator("DtDdWrapper");
		$CreditTransferChargeable->setAttrib('required',"true");
		$CreditTransferChargeable->removeDecorator("Label");
		$CreditTransferChargeable->removeDecorator('HtmlTag');
		$CreditTransferChargeable->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ResitExamChargeable = new Zend_Form_Element_Select('ResitExamChargeable');
		$ResitExamChargeable->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ResitExamChargeable->removeDecorator("DtDdWrapper");
		$ResitExamChargeable->setAttrib('required',"true");
		$ResitExamChargeable->removeDecorator("Label");
		$ResitExamChargeable->removeDecorator('HtmlTag');
		$ResitExamChargeable->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$PaperReviewChargeable = new Zend_Form_Element_Select('PaperReviewChargeable');
		$PaperReviewChargeable->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PaperReviewChargeable->removeDecorator("DtDdWrapper");
		$PaperReviewChargeable->setAttrib('required',"true");
		$PaperReviewChargeable->removeDecorator("Label");
		$PaperReviewChargeable->removeDecorator('HtmlTag');
		$PaperReviewChargeable->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ChangeProgramTrigger = new Zend_Form_Element_Select('ChangeProgramTrigger');
		$ChangeProgramTrigger->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeProgramTrigger->removeDecorator("DtDdWrapper");
		$ChangeProgramTrigger->setAttrib('required',"true");
		$ChangeProgramTrigger->removeDecorator("Label");
		$ChangeProgramTrigger->removeDecorator('HtmlTag');
		$ChangeProgramTrigger->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ChangeStatusTrigger = new Zend_Form_Element_Select('ChangeStatusTrigger');
		$ChangeStatusTrigger->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeStatusTrigger->removeDecorator("DtDdWrapper");
		$ChangeStatusTrigger->setAttrib('required',"true");
		$ChangeStatusTrigger->removeDecorator("Label");
		$ChangeStatusTrigger->removeDecorator('HtmlTag');
		$ChangeStatusTrigger->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$CreditTransferTrigger = new Zend_Form_Element_Select('CreditTransferTrigger');
		$CreditTransferTrigger->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CreditTransferTrigger->removeDecorator("DtDdWrapper");
		$CreditTransferTrigger->setAttrib('required',"true");
		$CreditTransferTrigger->removeDecorator("Label");
		$CreditTransferTrigger->removeDecorator('HtmlTag');
		$CreditTransferTrigger->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ResitExamTrigger = new Zend_Form_Element_Select('ResitExamTrigger');
		$ResitExamTrigger->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ResitExamTrigger->removeDecorator("DtDdWrapper");
		$ResitExamTrigger->setAttrib('required',"true");
		$ResitExamTrigger->removeDecorator("Label");
		$ResitExamTrigger->removeDecorator('HtmlTag');
		$ResitExamTrigger->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$PaperReviewTrigger = new Zend_Form_Element_Select('PaperReviewTrigger');
		$PaperReviewTrigger->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PaperReviewTrigger->removeDecorator("DtDdWrapper");
		$PaperReviewTrigger->setAttrib('required',"true");
		$PaperReviewTrigger->removeDecorator("Label");
		$PaperReviewTrigger->removeDecorator('HtmlTag');
		$PaperReviewTrigger->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$Search = new Zend_Form_Element_Submit('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag')
		->class = "NormalBtn";
		
		
		$ChangeStatusQuitFeeSetup = new Zend_Form_Element_Select('ChangeStatusQuitFeeSetup');
		$ChangeStatusQuitFeeSetup->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeStatusQuitFeeSetup->removeDecorator("DtDdWrapper");
		$ChangeStatusQuitFeeSetup->setAttrib('required',"true");
		$ChangeStatusQuitFeeSetup->removeDecorator("Label");
		$ChangeStatusQuitFeeSetup->removeDecorator('HtmlTag');
		$ChangeStatusQuitFeeSetup->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$ChangeStatusQuitChargeable = new Zend_Form_Element_Select('ChangeStatusQuitChargeable');
		$ChangeStatusQuitChargeable->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeStatusQuitChargeable->removeDecorator("DtDdWrapper");
		$ChangeStatusQuitChargeable->setAttrib('required',"true");
		$ChangeStatusQuitChargeable->removeDecorator("Label");
		$ChangeStatusQuitChargeable->removeDecorator('HtmlTag');
		$ChangeStatusQuitChargeable->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$ChangeStatusQuitTrigger = new Zend_Form_Element_Select('ChangeStatusQuitTrigger');
		$ChangeStatusQuitTrigger->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChangeStatusQuitTrigger->removeDecorator("DtDdWrapper");
		$ChangeStatusQuitTrigger->setAttrib('required',"true");
		$ChangeStatusQuitTrigger->removeDecorator("Label");
		$ChangeStatusQuitTrigger->removeDecorator('HtmlTag');
		$ChangeStatusQuitTrigger->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$this->addElements(array($UpdUser,$UpdDate,$IdUniversity,$ChangeProgramFeeSetup,$ChangeStatusFeeSetup,
				$CreditTransferFeeSetup,$ResitExamFeeSetup,$PaperReviewFeeSetup,
				$ChangeProgramChargeable,$ChangeStatusChargeable,$CreditTransferChargeable,
				$ResitExamChargeable,$PaperReviewChargeable,$ChangeProgramTrigger,
				$ChangeStatusTrigger,$CreditTransferTrigger,$ResitExamTrigger,
				$PaperReviewTrigger,
				$Save,$Add,$Clear,$Search,
				$ChangeStatusQuitFeeSetup,$ChangeStatusQuitChargeable,$ChangeStatusQuitTrigger
				));

	}

}