<?php

class Studentfinance_Form_CreditcardTerminal extends Zend_Form
{
	protected $_locale;
	
	public function init()
	{
		
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
		
		$this->setMethod('post');
		$this->setAttrib('id','form_sd');
		
		$this->addElement('text','terminal_id', 
			array(
				'label'=>'Terminal ID',
				'required'=>'true',
			    'maxlength'=>'200'
			)
		);
		
		
		//branch
		$this->addElement('select','branch_id',
				array(
						'label'=>'Branch',
						'required'=>'true'
				)
		);
		
		
		$branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$listData = $branchDb->fetchAll()->toArray();
		$this->branch_id->addMultiOption(null,"Please Select");
		
		if($this->_locale == 'en_US'){
			foreach ($listData as $list){
				$this->branch_id->addMultiOption($list['IdBranch'],$list['BranchName']);
			}
		}else{
			foreach ($listData as $list){
				$this->branch_id->addMultiOption($list['IdBranch'],$list['Arabic']);
			}
		}
		
		//default
		$this->addElement('checkbox','default',
				array(
						'label'=>'Default?',
				)
		);
		
		//active
		$this->addElement('checkbox','active',
				array(
						'label'=>'Active?',
				)
		);
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'receivable','action'=>'payment-mode'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>