<?php

class Studentfinance_Form_RefundCheque extends Zend_Form
{
		
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_refund_cheque');
		
		//cheque number
		$this->addElement('text','rchq_cheque_no', 
			array(
				'label'=>'Cheque Number',
				'required'=>'true',
			)
		);
		
		//bank
		$this->addElement('select','rchq_bank_name', array(
			'label'=>'Bank',
			'required'=>'true'	
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$this->rchq_bank_name->addMultiOption(null,'Please Select');
		foreach ($definationDb->getDataByType(88) as $list){
			$this->rchq_bank_name->addMultiOption($list['idDefinition'],$list['BahasaIndonesia']);
		}
		
		$this->addElement('text','rchq_amount', 
			array(
				'label'=>'Amount',
				'required'=>'true'
			)
		);
		
		$this->rchq_amount->addValidator('Regex',false, 
			array(
				'pattern' =>'/^\$?[0-9]+(,[0-9]{3})*(.[0-9]{2})?$/',
				'messages'=>array(
                               'regexNotMatch'=>'Masukan nilai cek seperti 888.88'
                           )
			)
		);
		
		//issue date
		$element = new ZendX_JQuery_Form_Element_DatePicker('rchq_issue_date',
            array(
            	'jQueryParams' => array('dateFormat' => 'dd-mm-yy'),
            	'label'=>'Cheque Issue Date',
            	'required'=>'true'
            )
        );
		$this->addElement($element);
		
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'refund-cheque','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
	
}
?>