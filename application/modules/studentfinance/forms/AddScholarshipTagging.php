<?php

class Studentfinance_Form_AddScholarshipTagging extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('sct_Id', 'add_scholarship_tagging_form');

        $this->addElement('text', 'IntakeDesc', array(
            'label' => 'Intake',
            'disable' => 'true'
        ));

        $selectScholarBtn = new Zend_Form_Element_Button('Select Scholarship');
        $selectScholarBtn->setLabel('Select Scholarship')
            ->setAttrib('onClick', 'selectScholarship()')
            ->setAttrib('class', 'button')
            ->setAttrib('style', 'margin-top:-1px;')
            //->setRequired(true)
            ->setDecorators(array(
                'ViewHelper',
                array('Description', array('escape' => false, 'tag' => false)),
                array('HtmlTag', array('tag' => 'dd')),
                array('Label', array('tag' => 'dt')),
                'Errors',
            ));
        $this->addElement($selectScholarBtn);

        $scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $scholarship_list = $scholarshipModel->getScholarshipList();

        $scholarship = new Zend_Form_Element_MultiCheckbox('sch_Id');

        if (count($scholarship_list) > 0) {
            foreach ($scholarship_list as $list) {
                $scholarship->addMultiOption($list['sch_Id'], ' ' . $list['sch_name']);
                //->setRequired(true);
            }
            $this->addElement($scholarship);
        }


        $this->addElement('select', 'sct_program', array(
            'label' => 'Program',
            'required' => 'true',
            'id' => 'program',
            'onChange' => 'get_programscheme()'
        ));

        $this->sct_program->addMultiOption(null, "-- All --");

        $get_program_list = new Application_Model_DbTable_ProgramScheme();
        $program_list = $get_program_list->fnGetProgramList();

        if (count($program_list) > 0) {
            foreach ($program_list as $list) {
                $this->sct_program->addMultiOption($list['IdProgram'], $list['ProgramName']);
            }
        }

        $this->addElement('select', 'sct_programscheme', array(
            'label' => 'Program Scheme',
            'required' => 'true',
            'id' => 'programscheme',
            'registerInArrayValidator' => false
        ));

        $this->sct_programscheme->addMultiOption(null, "-- All --");

        $this->addElement('select', 'sct_studentcategory', array(
            'label' => 'Student Category',
            'required' => 'true',
            'id' => 'program',
            //'onChange'=>'get_programscheme()'
        ));

        $this->sct_studentcategory->addMultiOption(null, "-- All --");

        $definationDB = new App_Model_General_DbTable_Definationms();
        $category = $definationDB->getDataByType(95);

        if (count($category) > 0) {
            foreach ($category as $list) {
                $this->sct_studentcategory->addMultiOption($list['idDefinition'], $list['DefinitionDesc']);
            }
        }

        $StartDate = new Zend_Form_Element_Text('sct_startDate');
        $StartDate->setAttrib('dojoType', "dijit.form.DateTextBox");
        $StartDate->setAttrib('onChange', "dijit.byId('sct_endDate').constraints.min = arguments[0];");
        $StartDate->setAttrib('required', "true");
        $StartDate->removeDecorator("DtDdWrapper");
        $StartDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $StartDate->setLabel('Start Date');
        $StartDate->setRequired(true);

        $EndDate = new Zend_Form_Element_Text('sct_endDate');
        $EndDate->setAttrib('dojoType', "dijit.form.DateTextBox");
        $EndDate->setAttrib('onChange', "dijit.byId('sct_startDate').constraints.max = arguments[0];");
        $EndDate->setAttrib('required', "true");
        $EndDate->removeDecorator("DtDdWrapper");
        $EndDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $EndDate->setLabel('End Date');
        $EndDate->setRequired(true);
        
        $StartDateAccept = new Zend_Form_Element_Text('sct_startDateAccept');
        $StartDateAccept->setAttrib('dojoType', "dijit.form.DateTextBox");
        $StartDateAccept->setAttrib('onChange', "dijit.byId('sct_endDateAccept').constraints.min = arguments[0];");
        $StartDateAccept->setAttrib('required', "true");
        $StartDateAccept->removeDecorator("DtDdWrapper");
        $StartDateAccept->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $StartDateAccept->setLabel('Accepted Offer Start Date');
        $StartDateAccept->setRequired(true);

        $EndDateAccept = new Zend_Form_Element_Text('sct_endDateAccept');
        $EndDateAccept->setAttrib('dojoType', "dijit.form.DateTextBox");
        $EndDateAccept->setAttrib('onChange', "dijit.byId('sct_startDateAccept').constraints.max = arguments[0];");
        $EndDateAccept->setAttrib('required', "true");
        $EndDateAccept->removeDecorator("DtDdWrapper");
        $EndDateAccept->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $EndDateAccept->setLabel('Accepted Offer End Date');
        $EndDateAccept->setRequired(true);

        $this->addElements(array($StartDate, $EndDate, $StartDateAccept, $EndDateAccept));

        $this->addElement('textarea', 'sct_desc', array(
            'label' => 'Desciption'
        ));

        $this->addElement('radio', 'sct_status', array(
            'label' => 'Status'));

        $this->sct_status->addMultiOptions(array(
            '1' => 'Active',
            '0' => 'No'
        ))->setSeparator('&nbsp;&nbsp;')->setValue("1");

        //button
        $this->addElement('submit', 'save', array(
            'label' => 'Save',
            'decorators' => array('ViewHelper'),
            //'onClick'=>'submitSch()'
        ));

        $this->addElement('submit', 'cancel', array(
            'label' => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick' => "window.location ='" . $this->getView()->url(array('module' => 'studentfinance', 'controller' => 'scholarship', 'action' => 'scholarship'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}

?>