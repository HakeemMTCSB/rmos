<?php
class Studentfinance_Form_Bulkinvoice extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

    	
    	$IdInvoice = new Zend_Form_Element_Hidden('IdInvoice');
        $IdInvoice->removeDecorator("DtDdWrapper");
        $IdInvoice->removeDecorator("Label");
        $IdInvoice->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
		$Studentid = new Zend_Form_Element_Hidden('Studentid');
        $Studentid->removeDecorator("DtDdWrapper");
        $Studentid->removeDecorator("Label");
        $Studentid->removeDecorator('HtmlTag');        
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   		
        $InvoiceCode = new Zend_Form_Element_Text('InvoiceCode');
		$InvoiceCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $InvoiceCode->removeDecorator("DtDdWrapper")
                 ->setAttrib('readonly',true)
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag'); 
		
		$StudentName = new Zend_Form_Element_Text('StudentName');
		$StudentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $StudentName->removeDecorator("DtDdWrapper")
        			->setAttrib('readonly',true)
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');

		$ProgramName = new Zend_Form_Element_Text('ProgramName');
		$ProgramName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ProgramName->removeDecorator("DtDdWrapper")
                 ->setAttrib('readonly',true)
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        		
        		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
 
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$InvoiceDate = new Zend_Dojo_Form_Element_DateTextBox('InvoiceDate');
        $InvoiceDate 	-> setAttrib('dojoType',"dijit.form.DateTextBox")
						-> setAttrib('title',"dd-mm-yyyy")
						-> setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						-> setAttrib('required',"true")		
						-> removeDecorator("Label")
						-> removeDecorator("DtDdWrapper")
						-> removeDecorator('HtmlTag');
	
		$InvoiceAmount = new Zend_Form_Element_Text('InvoiceAmount');
		$InvoiceAmount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $InvoiceAmount->removeDecorator("DtDdWrapper")
        			  ->setAttrib('readonly',true)
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');	
        		    
        $MonthYear = new Zend_Form_Element_Text('MonthYear');
		$MonthYear->setAttrib('dojoType',"dijit.form.TextBox");
        $MonthYear->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $Naration = new Zend_Form_Element_Text('Naration');
		$Naration->setAttrib('dojoType',"dijit.form.TextBox");
        $Naration->removeDecorator("DtDdWrapper")
                 ->setAttrib('readonly',true)
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		        		
        $AcademicPeriod = new Zend_Form_Element_Hidden('AcademicPeriod');
		$AcademicPeriod->setAttrib('dojoType',"dijit.form.TextBox");
        $AcademicPeriod->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        		
        		
        		
        //form elements
        $this->addElements(array($IdInvoice,
        						  $UpdDate,
        						  $UpdUser,
        						  $StudentName,
        						  $ProgramName,
        						  $Save,
        						  $Add,
        						  $InvoiceCode,
        						  $InvoiceDate,
        						  $InvoiceAmount,
        						  $Studentid,
        						  $MonthYear,
        						  $Naration,
        						  $AcademicPeriod
                                 ));

    }
}