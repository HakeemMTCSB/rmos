<?php

class Studentfinance_Form_FeeStructureProgramItem extends Zend_Form
{	
	
	protected $programid;
	protected $scheme;
	protected $category;
	
	public function setProgramid($programid){
		$this->programid = $programid;
	}
	
	public function setScheme($scheme){
		$this->scheme = $scheme;
	}
	
	public function setCategory($category){
		$this->category = $category;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_fee_program_item');
		$this->setAttrib('onsubmit', 'return checkFormProgramItem()');
		$this->setAction('/studentfinance/fee-structure/add-program-item');
		
		//fee struc id
		$this->addElement('hidden','fspi_fs_id', 
			array(
				'required'=>'true',
			)
		);
				
		
		//program
		$this->addElement('hidden','fspi_program_id', 
			array(
				'required'=>'true',
			)
		);

		//type
		$this->addElement('hidden','fspi_type', 
			array(
				'required'=>'true',
			)
		);
		
		
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$feeItemList = $feeItemDb->getActiveFeeItem();
		
		//fee item
		$this->addElement('select','fspi_fee_id', array(
			'label'=>'Fee Item',
			'required'=>'true'	
		));
		$this->fspi_fee_id->addMultiOption(null,'Please Select');
		foreach ($feeItemList as $feeitem )
		{
			$this->fspi_fee_id->addMultiOption($feeitem['fi_id'], $feeitem['fi_name'].' ('.$feeitem['freqMode'].' '.$feeitem['calType'].')');
		}
		
		//currency_id
		$this->addElement('select','fspi_currency_id', array(
				'label' => 'Currency',
				'required' => 'true'
		));

		$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$currencyData = $currencyDb->fetchAll('cur_status = 1')->toArray();
		foreach ( $currencyData as $currency )
		{
			$this->fspi_currency_id->addMultiOption($currency['cur_id'], $currency['cur_code']);
		}

		//amount
		$this->addElement('text','fspi_amount', 
			array(
				'label'=>'Amount',
				'class' => 'input-txt'
			)
		);
		
		//document type
		$this->addElement('select','fspi_document_type', array(
			'label'=>'Document Type',
			'required'=>false
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$docTypeFinance = $definationDb->getDataByTypeDocumentType($this->scheme,$this->category);
		
		$this->fspi_document_type->addMultiOption(null,'Please Select');
		foreach ($docTypeFinance as $list){
			$this->fspi_document_type->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}
		$this->fspi_document_type->addMultiOption(912,'No Objection Certificate (NOC) Fee');


	
		//button
		$this->addElement('submit', 'save_programitem', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"$('#dialog-add-application').dialog('close'); return false;"
        ));
        
        $this->addDisplayGroup(array('save_programitem','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>