<?php
class Studentfinance_Form_Billstructure extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {    	
    	$UpdDate 		= 	new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate		->	removeDecorator("DtDdWrapper");
        $UpdDate		->	removeDecorator("Label");
        $UpdDate		->	removeDecorator('HtmlTag');
     
        $UpdUser 		= 	new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser		->	removeDecorator("DtDdWrapper");
        $UpdUser		->	removeDecorator("Label");
        $UpdUser		->	removeDecorator('HtmlTag');						

        $BillMasterCode	= 	new Zend_Form_Element_Text('BillMasterCode');
        $BillMasterCode ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $BillMasterCode	->	removeDecorator("DtDdWrapper");
        $BillMasterCode	->	removeDecorator("Label");
        $BillMasterCode	->	removeDecorator('HtmlTag');       	
        $BillMasterCode	->	setAttrib('class', 'txt_put'); 
        
		$IdIntakeFrom = new Zend_Dojo_Form_Element_FilteringSelect('IdIntakeFrom');	       
        $IdIntakeFrom->removeDecorator("DtDdWrapper");
        $IdIntakeFrom->removeDecorator("Label");
        $IdIntakeFrom->removeDecorator('HtmlTag');
        $IdIntakeFrom->setRegisterInArrayValidator(false);
		$IdIntakeFrom->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
        //$IdGroup->setAttrib('style','width:150px;'); 
        /*  $ParentCollege->setRequired(true)
        				  ->addValidators(array(array('NotEmpty'))); */     
        // $IdGroup->setAttrib('maxlength','50');
        // $ParentCollege->setAttrib('required',true);							

		$IdIntakeTo = new Zend_Dojo_Form_Element_FilteringSelect('IdIntakeTo');	         
        $IdIntakeTo->removeDecorator("DtDdWrapper");
        $IdIntakeTo->removeDecorator("Label");
        $IdIntakeTo->removeDecorator('HtmlTag');
        $IdIntakeTo->setRegisterInArrayValidator(false);
		$IdIntakeTo->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdCategory = new Zend_Dojo_Form_Element_FilteringSelect('IdCategory');	    
        $IdCategory->removeDecorator("DtDdWrapper");
        $IdCategory->removeDecorator("Label");
        $IdCategory->removeDecorator('HtmlTag');
        $IdCategory->setRegisterInArrayValidator(false);
		$IdCategory->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$effectiveDate = new Zend_Dojo_Form_Element_DateTextBox('effectiveDate');
        $effectiveDate 	-> setAttrib('dojoType',"dijit.form.DateTextBox")
						-> setAttrib('title',"dd-mm-yyyy")
						-> setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						-> setAttrib('required',"true")		
						-> removeDecorator("Label")
						-> removeDecorator("DtDdWrapper")
						-> removeDecorator('HtmlTag');		
		
		$Description = new Zend_Form_Element_Text('Description');
        $Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');		
		
		$idProgram = new Zend_Dojo_Form_Element_FilteringSelect('idProgram');       
        $idProgram->removeDecorator("DtDdWrapper");
        $idProgram->removeDecorator("Label");
        $idProgram->removeDecorator('HtmlTag');
        $idProgram->setRegisterInArrayValidator(false);
		$idProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$idVoucherType = new Zend_Dojo_Form_Element_FilteringSelect('idVoucherType');       
        $idVoucherType->removeDecorator("DtDdWrapper");
        $idVoucherType->removeDecorator("Label");
        $idVoucherType->removeDecorator('HtmlTag');
        $idVoucherType->setRegisterInArrayValidator(false);
		$idVoucherType->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$itemDescription = new Zend_Form_Element_Text('itemDescription');
        $itemDescription-> setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $itemDescription->removeDecorator("DtDdWrapper");
        $itemDescription->removeDecorator("Label");
        $itemDescription->removeDecorator('HtmlTag');
        $itemDescription->setAttrib('required',"true"); 
		
		
		$idCalculationMethod = new Zend_Dojo_Form_Element_FilteringSelect('idCalculationMethod');	        
        $idCalculationMethod->removeDecorator("DtDdWrapper");
        $idCalculationMethod->removeDecorator("Label");
        $idCalculationMethod->removeDecorator('HtmlTag');
        $idCalculationMethod->setRegisterInArrayValidator(false);
		$idCalculationMethod->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Amount	= 	new Zend_Form_Element_Text('Amount');
        $Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount->setAttrib('required',true);
        $Amount->removeDecorator("DtDdWrapper");
        $Amount->removeDecorator("Label");
        $Amount->removeDecorator('HtmlTag');
		
		$idDebitAccount = new Zend_Dojo_Form_Element_FilteringSelect('idDebitAccount');	       
        $idDebitAccount->removeDecorator("DtDdWrapper");
        $idDebitAccount->removeDecorator("Label");
        $idDebitAccount->removeDecorator('HtmlTag');
        $idDebitAccount->setRegisterInArrayValidator(false);
		$idDebitAccount->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$idCreditAccount = new Zend_Dojo_Form_Element_FilteringSelect('idCreditAccount');	        
        $idCreditAccount->removeDecorator("DtDdWrapper");
        $idCreditAccount->removeDecorator("Label");
        $idCreditAccount->removeDecorator('HtmlTag');
        $idCreditAccount->setRegisterInArrayValidator(false);
		$idCreditAccount->setAttrib('dojoType',"dijit.form.FilteringSelect");							
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');         
        $Save 	= 	new Zend_Form_Element_Submit('Save');
        $Save	->	setAttrib('class','NormalBtn');
        $Save	->	dojotype="dijit.form.Button";
        $Save	->	label 	= $gstrtranslate->_("Save");
        $Save	->	setAttrib('id', 'submitbutton');
        $Save	->	removeDecorator("DtDdWrapper");
        $Save	->	removeDecorator("Label");
        $Save	->	removeDecorator('HtmlTag');        
        
         
   		$this->addElements(array($UpdDate,$UpdUser,$BillMasterCode,$IdIntakeFrom,$IdIntakeTo,$IdCategory,$effectiveDate,$Description,$idProgram,
   		$idVoucherType,$itemDescription,$idCalculationMethod,$Amount,$idDebitAccount,$idCreditAccount,$Save));
    }
}