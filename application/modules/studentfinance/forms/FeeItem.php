<?php

class Studentfinance_Form_FeeItem extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_sd');
		
		$this->addElement('text','fi_name', 
			array(
				'label'=>'Fee Name',
				'required'=>'true',
			    'maxlength'=>'200',
				'class'=>'input-txt'
			)
		);
		
		$this->addElement('text','fi_name_bahasa', 
			array(
				'label'=>'Fee Name (2nd Language)',
			    'maxlength'=>'200',
				'class'=>'input-txt'
			)
		);
		
		$this->addElement('text','fi_code',
		    array(
		        'label'=>'Code',
		        'maxlength'=>'25',
				'class'=>'input-txt'
		    )
		);
		
		//fee category
		$this->addElement('select','fi_fc_id',
				array(
						'label'=>'Fee Category',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		$feeCategoryDb = new Studentfinance_Model_DbTable_FeeCategory();
		$listData = $feeCategoryDb->fetchAll();
		$this->fi_fc_id->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fi_fc_id->addMultiOption($list['fc_id'],$list['fc_code']." (".$list['fc_desc'].")");
		}
		
		//calculation type
		$this->addElement('select','fi_amount_calculation_type', 
			array(
				'label'=>'Amount Calculation Type',
				'required'=>'true'
			)
		);
		$defDb = new App_Model_Definitiontype();
		$listData = $defDb->fnGetDefinations('Fee Item Calculation Type');
		$this->fi_amount_calculation_type->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fi_amount_calculation_type->addMultiOption($list['idDefinition'],$list['Description']);
		}

		//frequency mode	
		$this->addElement('select','fi_frequency_mode', 
			array(
				'label'=>'Frequency Mode',
				'required'=>'true'
			)
		);
		$defDb = new App_Model_Definitiontype();
		$listData = $defDb->fnGetDefinations('Fee Item Frequency Mode');
		$this->fi_frequency_mode->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fi_frequency_mode->addMultiOption($list['idDefinition'],$list['Description']);
		}
		
		
		//account code
		$this->addElement('select','fi_ac_id',
				array(
						'label'=>'Account Code',
						'required'=>'true'
				)
		);
		
		$accCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		//$listData = $accCodeDb->getAccountCode(1, array(610,613,821));
		$listData = $accCodeDb->getAccountCode(1);
		$this->fi_ac_id->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fi_ac_id->addMultiOption($list['ac_id'],$list['ac_code']." (".$list['ac_desc'].")");
		}
		
		//status
		$this->addElement('checkbox','fi_refundable',
				array(
						'label'=>'Refundable?',
				)
		);
		
		//non-invoice
		//status
		$this->addElement('checkbox','fi_non_invoice',
				array(
						'label'=>'Non-Invoice',
				)
		);
		
		//status
		$this->addElement('checkbox','fi_active',
				array(
						'label'=>'Active?',
				)
		);

		$this->fi_active->setValue(1);
		
		//gst
		$this->addElement('checkbox','fi_gst',
				array(
						'label'=>'GST',
				)
		);
		
		$this->addElement('text','fi_gst_tax', 
			array(
				'label'=>'GST Tax',
			    'maxlength'=>'200',
				'class'=>'input-txt'
			)
		);
		
		//efective date
		$this->addElement('text','fi_effective_date',
				array(
						'label'=>'Effective Date',
						'class'=>'datepicker input-txt',
						'placeholder' => 'YYYY-MM-DD'
				)
		);
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-item','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>