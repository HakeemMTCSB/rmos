<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Studentfinance_Form_CreditNoteListing extends Zend_Dojo_Form{
	
    public function init(){
        
        //get attr
        $programId = $this->getAttrib('programid');
        
        //model
        $model = new Studentfinance_Model_DbTable_CreditNoteListing();
        
        //date from
        $date_from = new Zend_Form_Element_Text('date_from');
        $date_from->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //date_to
        $date_to = new Zend_Form_Element_Text('date_to');
        $date_to->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //program
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
	$program->removeDecorator("Label");
        $program->setAttrib('onchange', 'getProgramScheme(this.value)');
        
        $programList = $model->getProgramList();
        
        $program->addMultiOption('', '--All--');
        
        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }
        
        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
	$programscheme->removeDecorator("Label");
        
        $programscheme->addMultiOption('', '--All--');
        
        if ($programId){
            $programSchemeList = $model->getProgramSchemeList($programId);
            
            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }
        
        //account code
        $account_code = new Zend_Form_Element_Select('account_code');
	$account_code->removeDecorator("DtDdWrapper");
        $account_code->setAttrib('class', 'select');
	$account_code->removeDecorator("Label");
        
        $account_code->addMultiOption('', '--All-');
        
        $accountCodeList = $model->getAccountCode();
        
        if ($accountCodeList){
            foreach ($accountCodeList as $accountCodeLoop){
                $account_code->addMultiOption($accountCodeLoop['ac_id'], $accountCodeLoop['ac_code'].' '.$accountCodeLoop['ac_desc']);
            }
        }
        
        //payment mode
        $payment_mode = new Zend_Form_Element_Select('payment_mode');
	$payment_mode->removeDecorator("DtDdWrapper");
        $payment_mode->setAttrib('class', 'select');
	$payment_mode->removeDecorator("Label");
        
        $payment_mode->addMultiOption('', '--All-');
        
        //form elements
        $this->addElements(array(
            $date_from,
            $date_to,
            $program,
            $programscheme,
            $account_code,
            $payment_mode
        ));
    }
}
?>

