<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Form_FeeCategory extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_fee_category');

		$this->addElement('text','fc_code',
				array(
						'label'=>'Fee Category Code',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		$this->addElement('text','fc_desc',
				array(
						'label'=>'Description',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		$this->addElement('text','fc_desc_second_language',
				array(
						'label'=>'Description (Second Language)',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		//fee category
		$this->addElement('select','fc_group',
				array(
						'label'=>'Fee Group',
						'required'=>false,
						'class'=>'input-txt'
				)
		);
		
		$lobjdeftype = new App_Model_Definitiontype();
		$fee_group = $lobjdeftype->fnGetDefinationsByLocale('Fee Group');
		
		$this->fc_group->addMultiOption(null,"Please Select");
		
		foreach ($fee_group as $list){
			$this->fc_group->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}
		
		$this->addElement('text','fc_seq',
				array(
						'label'=>'Sequence',
						'required'=>'true',
						'class'=>'input-txt small'
				)
		);
		
		$this->fc_seq->addValidator('Digits');



		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-category','action'=>'index'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>