<?php
class Studentfinance_Form_Billapproval extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$idvoucher = new Zend_Form_Element_Hidden('idvoucher');
        $idvoucher->removeDecorator("DtDdWrapper");
        $idvoucher->removeDecorator("Label");
        $idvoucher->removeDecorator('HtmlTag');
        
        $Voucherdate  = new Zend_Dojo_Form_Element_DateTextBox('Voucherdate');
        $Voucherdate->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Voucherdate->setAttrib('title',"dd-mm-yyyy");
	    $Voucherdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    $Voucherdate->setAttrib('required',"true");		
	    $Voucherdate->removeDecorator("Label");
	    $Voucherdate->removeDecorator("DtDdWrapper");
	    $Voucherdate->removeDecorator('HtmlTag');	

		$Vouchertype = new Zend_Form_Element_Text('Vouchertype');
		$Vouchertype->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Vouchertype->setAttrib('required',"true")	;
        $Vouchertype->removeDecorator("DtDdWrapper");
        $Vouchertype->removeDecorator("Label");	
        $Vouchertype->removeDecorator('HtmlTag');
        		
        $Vouchernumber  = new Zend_Form_Element_Text('Vouchernumber');
		$Vouchernumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Vouchernumber->removeDecorator("Label");
        $Vouchernumber->setAttrib('required',"true");		
        $Vouchernumber->removeDecorator('HtmlTag');
    	
		$idstudent = new Zend_Form_Element_Text('idstudent');
		$idstudent->setAttrib('dojoType',"dijit.form.TextBox");
        $idstudent->removeDecorator("DtDdWrapper");
        $idstudent->removeDecorator("Label");
        $idstudent->removeDecorator('HtmlTag');
        		
       	$approved = new Zend_Form_Element_Checkbox('approved');
      	$approved->setAttrib('dojoType',"dijit.form.CheckBox");
       	$approved->removeDecorator("DtDdWrapper");
       	$approved->removeDecorator("Label");
       	$approved->removeDecorator('HtmlTag')
        	     ->setValue("1");

        $Fromdate  = new Zend_Dojo_Form_Element_DateTextBox('Fromdate');
        $Fromdate->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Fromdate->setAttrib('title',"dd-mm-yyyy");
	    $Fromdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    $Fromdate->setAttrib('required',"true");		
	    $Fromdate->removeDecorator("Label");
	    $Fromdate->removeDecorator("DtDdWrapper");
	    $Fromdate->removeDecorator('HtmlTag');

	    $Todate  = new Zend_Dojo_Form_Element_DateTextBox('Todate');
        $Todate->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Todate->setAttrib('title',"dd-mm-yyyy");
	    $Todate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    $Todate->setAttrib('required',"true");		
	    $Todate->removeDecorator("Label");
	    $Todate->removeDecorator("DtDdWrapper");
	    $Todate->removeDecorator('HtmlTag');
        	  	
        $Upddate = new Zend_Form_Element_Hidden('Upddate');
        $Upddate->removeDecorator("DtDdWrapper");
        $Upddate->removeDecorator("Label");
        $Upddate->removeDecorator('HtmlTag');
        
        $Upduser  = new Zend_Form_Element_Hidden('Upduser');
        $Upduser->removeDecorator("DtDdWrapper");
        $Upduser->removeDecorator("Label");
        $Upduser->removeDecorator('HtmlTag');
        
        
        // voucherdetails form elements
        
   		$idvoucherdetails = new Zend_Form_Element_Hidden('idvoucherdetails');
        $idvoucherdetails->removeDecorator("DtDdWrapper");
        $idvoucherdetails->removeDecorator("Label");
        $idvoucherdetails->removeDecorator('HtmlTag');
        		    
       	$amount = new Zend_Form_Element_Text('amount');
		$amount->setAttrib('dojoType',"dijit.form.TextBox");
        $amount->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $description->removeDecorator("DtDdWrapper")
                    ->setAttrib('readonly',true)
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag'); 
    		
        // common to both
        		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
 
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
		    ->setAttrib('onClick','addAccountDetails()')
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
				    
        //form elements
        $this->addElements(array( $idvoucher,
        						  $Voucherdate,
        						  $Vouchertype,
        						  $Vouchernumber,
        						  $idstudent,
        						  $approved,
        						  $Fromdate,
        						  $Todate,
        						  $Upddate,
        						  $Upduser,
        						  $idvoucherdetails,
        						  $amount,
        						  $description,
        						  $Save,
        						  $Add
                                 ));

    }
}