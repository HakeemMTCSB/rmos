<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Form_AccountCode extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_account_code');

		$this->addElement('text','ac_code',
				array(
						'label'=>'Account Code',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		$this->addElement('text','ac_desc',
				array(
						'label'=>'Description',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		$this->addElement('select','ac_acc_type',
				array(
						'label'=>'Account Type',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		$defType = new App_Model_Definitiontype();
		$listData = $defType->fnGetDefinationMs('Account Type');
		
		$this->ac_acc_type->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->ac_acc_type->addMultiOption($list['key'],$list['value']);
		}

		$this->addElement('checkbox','ac_status',
				array(
						'label'=>'Active',
				)
		);



		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'account-code','action'=>'index'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>