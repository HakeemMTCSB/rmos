<?php
class Studentfinance_Form_Invoiceentry extends Zend_Dojo_Form { //Formclass for the user module
	public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$lobjinvoiceentryModel = new Studentfinance_Model_DbTable_Invoiceentry();
		$larrdebitList = $lobjinvoiceentryModel->fngetdebitList();

		$larrcreditList = $lobjinvoiceentryModel->fngetcreditList();

		$lobjInvoiceFeeStructureModel = new Studentfinance_Model_DbTable_AccountCode();
		$lobjgetcostctrl = $lobjInvoiceFeeStructureModel->getAccountCode();

		$lobjsubjectsofferedmodel = new GeneralSetup_Model_DbTable_Subjectsoffered();
		$larrbranchset = $lobjsubjectsofferedmodel->fngetAllBranchset(1);

		$lobjCurrency = new Hostel_Model_DbTable_Hostelconfig();
		$default = '';
		$currencyList = $lobjCurrency->fnGetCurrency($default);
		$finalList = array();
		$i = 0;
		foreach($currencyList as $currency){
			$finalList[$i]['key'] = $currency['cur_id'];
			$finalList[$i]['value'] = $currency['cur_code'];
			$i++;
		}
		$lobjFeeCode = new Studentfinance_Model_DbTable_Fee();
		$FeeCodeList = $lobjFeeCode->fngetfeesetuplist();

		$IdInvoiceEntry = new Zend_Form_Element_Hidden('IdInvoiceEntry');
		$IdInvoiceEntry->removeDecorator("DtDdWrapper");
		$IdInvoiceEntry->removeDecorator("Label");
		$IdInvoiceEntry->removeDecorator('HtmlTag');



		$InvoiceNumber = new Zend_Form_Element_Text('InvoiceNumber');
		$InvoiceNumber->removeDecorator("DtDdWrapper");
		$InvoiceNumber->removeDecorator("Label");
		$InvoiceNumber->removeDecorator('HtmlTag');

		$IdStudent 	 = new Zend_Dojo_Form_Element_FilteringSelect('IdStudent');
		$IdStudent->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$IdStudent->setAttrib('onchange',"filldetails(this)");
		$IdStudent->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$FeeCode 	= new Zend_Dojo_Form_Element_FilteringSelect('FeeCode');
		$FeeCode->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$FeeCode->removeDecorator("DtDdWrapper")
		->setAttrib('required','false')
		->setAttrib('onchange',"getRefundableStatus(this.value)")
		->addMultioptions($FeeCodeList)
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$IdBranch	= new Zend_Dojo_Form_Element_FilteringSelect('IdBranch');
		$IdBranch->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$IdBranch->removeDecorator("DtDdWrapper")
		->setAttrib('required','true')
		->addMultioptions($larrbranchset)
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Currency	= new Zend_Dojo_Form_Element_FilteringSelect('Currency');
		$Currency->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$Currency->removeDecorator("DtDdWrapper")
		->setAttrib('required','true')
		->addMultioptions($finalList)
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Debit 	= new Zend_Dojo_Form_Element_FilteringSelect('Debit');
		$Debit->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$Debit->removeDecorator("DtDdWrapper")
		->setAttrib('required','false')
		->addMultioptions($larrdebitList)
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Credit = new Zend_Dojo_Form_Element_FilteringSelect('Credit');
		$Credit->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$Credit->removeDecorator("DtDdWrapper")
		->setAttrib('required','false')
		->addMultioptions($larrcreditList)
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$CostCtr = new Zend_Dojo_Form_Element_FilteringSelect('CostCtr');
		$CostCtr->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$CostCtr->removeDecorator("DtDdWrapper")
		->setAttrib('required','false')
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		foreach($lobjgetcostctrl as $larrvalues) {
        	$CostCtr->addMultiOption($larrvalues['ac_id'],$larrvalues['ac_desc']);
        }

		$Amount = new Zend_Form_Element_Text('Amount');
		$Amount->removeDecorator("DtDdWrapper");
		$Amount->setAttrib('required',"false") ;
		$Amount->setAttrib('constraints', '{min:1,max:9999,pattern:"#.##"}');
		$Amount->setAttrib('invalidMessage', 'Only numbers greater than/equal to 1 upto two decimal places is allowed');
		$Amount->removeDecorator("Label");
		$Amount->removeDecorator('HtmlTag');
		$Amount->setAttrib('dojoType',"dijit.form.NumberTextBox");


		$Description = new Zend_Dojo_Form_Element_Textarea('Description');
		$Description->removeDecorator("DtDdWrapper");
		$Description->setAttrib('required',"true") ;
		$Description->removeDecorator("Label");
		$Description->removeDecorator('HtmlTag');
		$Description->setAttrib("rows", "5");
		$Description->setAttrib("cols", "30")->setAttrib('dojoType',"dijit.form.SimpleTextarea");

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->setAttrib('class', 'NormalBtn');
		$Save->dojotype = "dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('id', 'submitbutton');
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag');

		$Approve = new Zend_Form_Element_Submit('Approve');
		$Approve->setAttrib('class', 'NormalBtn');
		$Approve->dojotype = "dijit.form.Button";
		$Approve->label = $gstrtranslate->_("Approve");
		$Approve->removeDecorator("DtDdWrapper");
		$Approve->removeDecorator("Label");
		$Approve->removeDecorator('HtmlTag');

		$Disapprove = new Zend_Form_Element_Submit('Disapprove');
		$Disapprove->dojotype = "dijit.form.Button";
		$Disapprove->label = $gstrtranslate->_("Disapprove");
		$Disapprove->removeDecorator("DtDdWrapper");
		$Disapprove->removeDecorator("Label");
		$Disapprove->removeDecorator('HtmlTag');

		$InvoiceId = new Zend_Form_Element_Text('InvoiceId');
		$InvoiceId->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('required',"false")
		->setAttrib('maxlength','50')
		->setAttrib('class', 'txt_put')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		//form elements
		$this->addElements(array($IdStudent,
								 $IdInvoiceEntry,
								 $InvoiceNumber,
								 $Description,
								 $Save,
								 $FeeCode,
								 $Debit,
								 $Credit,
								 $Amount,
								 $CostCtr,
								 $IdBranch,
								 $Currency,
								 $Approve,
								 $Disapprove,
								 $InvoiceId
		));
	}
}