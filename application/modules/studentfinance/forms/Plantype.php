<?php
class Studentfinance_Form_Plantype extends Zend_Dojo_Form { //Formclass for the plantype module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdPlantype  = new Zend_Form_Element_Hidden('IdPlantype ');
        $IdPlantype ->removeDecorator("DtDdWrapper");
        $IdPlantype ->removeDecorator("Label");
        $IdPlantype ->removeDecorator('HtmlTag');
        
        
		$Planname = new Zend_Form_Element_Text('Planname');
		$Planname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Planname->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','255')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       $Active = new Zend_Form_Element_Checkbox('Active');
       $Active->setAttrib('dojoType',"dijit.form.CheckBox");
       $Active->setvalue('1');
       $Active->removeDecorator("DtDdWrapper");
       $Active->removeDecorator("Label");
       $Active->removeDecorator('HtmlTag');
        
        $Description = new Zend_Form_Element_Text('Description');	
        $Description	->setAttrib('maxlength','250')
        				->setAttrib('dojoType',"dijit.form.Textarea")
        				->setAttrib('style','width:auto;')
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');	

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
        		
        		
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
		
       
        //form elements
        $this->addElements(array($IdPlantype,
        						$Planname,   
                                $Active ,
                                $Description,
                                $Save,
                                $UpdDate,
                                $UpdUser
                                    
                                 ));
    }
}