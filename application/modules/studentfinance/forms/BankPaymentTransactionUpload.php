<?php
class Studentfinance_Form_BankPaymentTransactionUpload extends Zend_Form {
	
	public function init()
	{
        //parent::__construct($options);

        $this->setName('upload');
        $this->setAttrib('enctype', 'multipart/form-data');
		$this->setAttrib('action', $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'payment-bank','action'=>'upload-transaction'),'default',true) );
          
        $file = new Zend_Form_Element_File('file');
        $file->setLabel('File')
            ->setDestination(APPLICATION_PATH  . '/tmp')
            ->setRequired(true);
		$this->addElement($file);
    }
}