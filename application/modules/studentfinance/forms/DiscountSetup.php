<?php

class Studentfinance_Form_Discountsetup extends Zend_Dojo_Form
{
	
	
	private $lobjPolicySetup;
	public function init()
	{

		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		$this->lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
	

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$IdUniversity = new Zend_Form_Element_Hidden('IdUniversity');
		$IdUniversity->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		$dt_id = new Zend_Form_Element_Hidden('dt_id');
		$dt_id->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$StudentId = new Zend_Form_Element_Text('StudentId');
		$StudentId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudentId->removeDecorator("DtDdWrapper");
		$StudentId->removeDecorator("Label");
		$StudentId->removeDecorator('HtmlTag');
		$StudentId->setAttrib('required',"false");

		$StudentName = new Zend_Form_Element_Text('idstudentname');
		$StudentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudentName->removeDecorator("DtDdWrapper");
		$StudentName->removeDecorator("Label");
		$StudentName->removeDecorator('HtmlTag');
		$StudentName->setAttrib('required',"true");
		
	

		$dateformat = "{datePattern:'yyyy-MM-dd'}";
		$StartDate  = new Zend_Form_Element_Text('StartDate');
		$StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$StartDate->setAttrib('onChange',"dijit.byId('EndDate').constraints.min = arguments[0];") ;
		$StartDate->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
		$StartDate->removeDecorator("DtDdWrapper");
		$StartDate->removeDecorator("Label");
		$StartDate->removeDecorator('HtmlTag');

		$EndDate  = new Zend_Form_Element_Text('EndDate');
		$EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$EndDate->setAttrib('onChange',"dijit.byId('StartDate').constraints.max = arguments[0];") ;
		$EndDate->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
		$EndDate->removeDecorator("DtDdWrapper");
		$EndDate->removeDecorator("Label");
		$EndDate->removeDecorator('HtmlTag');


		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$AddByFeeInfo = new Zend_Form_Element_Button('$AddByFeeInfo');
		$AddByFeeInfo->label = $gstrtranslate->_("Add");
		$AddByFeeInfo->dojotype="dijit.form.Button";
		$AddByFeeInfo->removeDecorator("DtDdWrapper");
		$AddByFeeInfo->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Clear = new Zend_Form_Element_Button('Clear');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ClearByFeeInfo = new Zend_Form_Element_Button('$ClearByFeeInfo');
		$ClearByFeeInfo->dojotype="dijit.form.Button";
		$ClearByFeeInfo->label = $gstrtranslate->_("Clear");
		$ClearByFeeInfo->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$Search = new Zend_Form_Element_Submit('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag')
		->class = "NormalBtn";
			
		$SearchButton = new Zend_Form_Element_Button('SearchButton');
		$SearchButton->dojotype="dijit.form.Button";
		$SearchButton->label = $gstrtranslate->_("Search");
		$SearchButton->removeDecorator("DtDdWrapper");
		$SearchButton->removeDecorator("Label");
		$SearchButton->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$this->addElements(array($UpdUser,$UpdDate,$IdUniversity,$dt_id,
				$Save,$Add,$Clear,$Search,
				$StudentId,$StudentName,
				$StartDate,$EndDate,$ClearByFeeInfo,$AddByFeeInfo,$SearchButton	
			));


	}
}