<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Form_CurrencyRate extends Zend_Form
{
	
	protected $currency;
	
	public function setCurrency($currency){
		$this->currency = $currency;
	}

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_currency_rate');

		$this->addElement('hidden','cr_cur_id');
		$this->cr_cur_id->setValue($this->currency);
		
		
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$cur_data =  $currencyDb->fetchRow(array('cur_id = ?'=>$this->currency))->toArray();
				
		$this->addElement('text','cur_name',
				array(
						'label'=>'Currency Name',
						'disabled'=>'disabled',
						'class' => 'input-txt'
				)
		);
		$this->cur_name->setValue($cur_data['cur_desc']);
		
		$this->addElement('text','cur_code',
				array(
						'label'=>'Currency Code',
						'disabled'=>'disabled',
						'class' => 'input-txt'
				)
		);
		$this->cur_code->setValue($cur_data['cur_code']);
		
		$this->addElement('text','cr_exchange_rate',
				array(
						'label'=>'Exchange Rate',
						'required'=>'true',
						'class' => 'input-txt'
				)
		);
		
		
		$this->addElement('text','cr_min_rate',
				array(
						'label'=>'Min Rate',
						'required'=>'true',
						'class' => 'input-txt'
				)
		);
		
		$this->addElement('text','cr_max_rate',
				array(
						'label'=>'Max Rate',
						'required'=>'true',
						'class' => 'input-txt'
				)
		);
		
		$this->addElement('text','cr_effective_date',
				array(
						'label'=>'Effective Date',
						'required'=>'true',
						'class'=>'datepicker',
						'placeholder'=>'YYYY-MM-DD',
						'class' => 'input-txt'
				)
		);
		
		


		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Save',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'currency','action'=>'rate-setup-detail')) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>