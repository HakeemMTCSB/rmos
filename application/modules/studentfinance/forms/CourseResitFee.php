<?php

class Studentfinance_Form_CourseResitFee extends Zend_Form
{
	protected $_cid;
	protected $_pid;
	
	public function setCid($cid){
		$this->_cid = $cid;
	}
	
	public function setPid($pid){
		$this->_pid = $pid;
	}
	
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_course_resit_fee');
		
		$this->addElement('text','fee', 
			array(
				'label'=>'Resit Fee'
			)
		);
		$this->fee->setRequired(true)
					->addValidator('Regex',false, array('pattern' =>'/^\$?[0-9]+(,[0-9]{3})*(.[0-9]{2})?$/'));
		
	
		$this->addElement('text','effective_date',
			array(
				'label'=>'Effective Date',
				'class'=>'datepicker',
				'required'=>true,
			)
		);
		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-resit','action'=>'course-resit-fee'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        
        $this->addElement('hidden','program_id',array('required'=>true));
        $this->program_id->setValue($this->_pid)
        ->removeDecorator('label')
        ->removeDecorator('HtmlTag'); 
        
        $this->addElement('hidden','course_id',array('required'=>true));
        $this->course_id->setValue($this->_cid)
        ->removeDecorator('label')
        ->removeDecorator('HtmlTag');
        
		
	}
}
?>