<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/3/2016
 * Time: 10:32 AM
 */
class Studentfinance_Form_SponsorTaggingReport extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $model = new Studentfinance_Model_DbTable_SponsorTaggingReport();

        //program
        $sponsorship = new Zend_Form_Element_Select('sponsorship');
        $sponsorship->removeDecorator("DtDdWrapper");
        $sponsorship->setAttrib('class', 'select');
        $sponsorship->setAttrib('required', '');
        $sponsorship->setAttrib('multiple', true);
        $sponsorship->removeDecorator("Label");

        $sponsorshipList = $model->getSponsorshipList();

        if ($sponsorshipList){
            foreach ($sponsorshipList as $sponsorshipLoop){
                $sponsorship->addMultiOption($sponsorshipLoop['idsponsor'], $sponsorshipLoop['fName'].' - '.$sponsorshipLoop['SponsorCode']);
            }
        }

        //program
        $scholarship = new Zend_Form_Element_Select('scholarship');
        $scholarship->removeDecorator("DtDdWrapper");
        $scholarship->setAttrib('class', 'select');
        $scholarship->setAttrib('required', '');
        $scholarship->setAttrib('multiple', true);
        $scholarship->removeDecorator("Label");

        $scholarshipList = $model->getScholarshipList();

        if ($scholarshipList){
            foreach ($scholarshipList as $scholarshipLoop){
                $scholarship->addMultiOption($scholarshipLoop['sch_Id'], $scholarshipLoop['sch_name'].' - '.$scholarshipLoop['sch_code']);
            }
        }

        $this->addElements(array(
            $sponsorship,
            $scholarship
        ));
    }
}