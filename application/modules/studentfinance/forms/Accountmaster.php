<?php
class Studentfinance_Form_Accountmaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$UpdDate 		= 	new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate		->	removeDecorator("DtDdWrapper");
        $UpdDate		->	removeDecorator("Label");
        $UpdDate		->	removeDecorator('HtmlTag');
     
        $UpdUser 		= 	new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser		->	removeDecorator("DtDdWrapper");
        $UpdUser		->	removeDecorator("Label");
        $UpdUser		->	removeDecorator('HtmlTag');
        
        $idConfig 		= 	new Zend_Form_Element_Hidden('idConfig');
        $idConfig		->	removeDecorator("DtDdWrapper");
        $idConfig		->	removeDecorator("Label");
        $idConfig		->	removeDecorator('HtmlTag');                    
         
/*        $IdGroup	 	= 	new Zend_Form_Element_Select('IdGroup');
       	$IdGroup		->	addMultiOptions('', 'Select');
        $IdGroup		->	setAttrib('class', 'txt_put MakeEditable')
						->	setAttrib('style','width:150px;')        		        
						->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 				
						->	removeDecorator('HtmlTag')
						->  setAttrib('dojoType',"dijit.form.FilteringSelect");*/
						
						
		$IdGroup = new Zend_Dojo_Form_Element_FilteringSelect('IdGroup');	
       $IdGroup->addMultiOption('','');  
      /*  $ParentCollege->setRequired(true)
           			  ->addValidators(array(array('NotEmpty'))); */     
       // $IdGroup->setAttrib('maxlength','50');
       // $ParentCollege->setAttrib('required',true);
        $IdGroup->removeDecorator("DtDdWrapper");
        $IdGroup->removeDecorator("Label");
        $IdGroup->removeDecorator('HtmlTag');
        $IdGroup->setRegisterInArrayValidator(false);
		$IdGroup->setAttrib('dojoType',"dijit.form.FilteringSelect");
        //$IdGroup->setAttrib('style','width:150px;'); 							
						
						
        $AccountName	=   new Zend_Form_Element_Text('AccountName',array('regExp'=>"[A-Za-z -]+",'invalidMessage'=>"Alphabets Only"));
        $AccountName    ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccountName	->	removeDecorator("DtDdWrapper");
        $AccountName	->	removeDecorator("Label");
        $AccountName	->	removeDecorator('HtmlTag');
        $AccountName	->	setAttrib('required',"true");         
        
        $AccShortName	= 	new Zend_Form_Element_Text('AccShortName');
        $AccShortName   ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccShortName	->	removeDecorator("DtDdWrapper");
        $AccShortName	->	removeDecorator("Label");
        $AccShortName	->	removeDecorator('HtmlTag');
        $AccShortName	->  setAttrib('required',"true"); 
        				 
        $PrefixCode		= 	new Zend_Form_Element_Text('PrefixCode');
        $PrefixCode     ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $PrefixCode		->	removeDecorator("DtDdWrapper");
        $PrefixCode		->	removeDecorator("Label");
        $PrefixCode		->	removeDecorator('HtmlTag');
       		//->	setAttrib('style','width:155px;')
        $PrefixCode		->	setAttrib('class', 'txt_put'); 

        $Description	= 	new Zend_Form_Element_Text('Description');
        $Description    ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Description	->	removeDecorator("DtDdWrapper");
        $Description	->	removeDecorator("Label");
        $Description	->	removeDecorator('HtmlTag');
       // $Description	->	setAttrib('class', 'txt_put'); 	
        
        $AccountCode	= 	new Zend_Form_Element_Text('AccountCode');
        $AccountCode    ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccountCode	->setAttrib('required',true);
        $AccountCode    ->	removeDecorator("DtDdWrapper");
        $AccountCode	->	removeDecorator("Label");
        $AccountCode	->	removeDecorator('HtmlTag');
        //$AccountCode	->	setAttrib('class','txt_put'); 	
        				
        $Period	 		= 	new Zend_Dojo_Form_Element_FilteringSelect('Period');   
        $Period			->	setAttrib('required',"true")       		        
						->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 				
						->	removeDecorator('HtmlTag')		
						-> setRegisterInArrayValidator(false)				
        				->  setAttrib('dojoType',"dijit.form.FilteringSelect");
        $AccountType 	= 	new Zend_Form_Element_Radio('AccountType');
        $AccountType    ->  setAttrib('dojoType',"dijit.form.RadioButton");
        //$AccountType	->	setLabel(�Account Type�);.RadioButton
        //$Active		->	setAttrib('onClick','ToggleSelectBoxs()');
        $AccountType	->	removeDecorator("DtDdWrapper")
        				->	addMultiOptions(array( "0" => "Debit" ,
													   "1" => "Credit"
													));
        $AccountType	->	removeDecorator("Label");
        $AccountType	->	removeDecorator('HtmlTag')
        				->	setValue("0");;			
        				
        				
        $BillingModule	= 	new Zend_Form_Element_Select('BillingModule');
       	$BillingModule	->	addMultiOption('', 'Select');
        $BillingModule	->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 
						-> setRegisterInArrayValidator(false)					
						->	removeDecorator('HtmlTag')	;
                        //->  setAttrib('dojoType',"dijit.form.FilteringSelect");
                        
		$Revenue 		= 	new Zend_Form_Element_Checkbox('Revenue');
		$Revenue        ->  setAttrib('dojoType',"dijit.form.CheckBox");
        //$Active		->	setAttrib('onClick','ToggleSelectBoxs()');
        $Revenue		->	removeDecorator("DtDdWrapper");
        $Revenue		->	removeDecorator("Label");
        $Revenue		->	removeDecorator('HtmlTag');
        
        
        $duringRegistration 		= 	new Zend_Form_Element_Checkbox('duringRegistration');
        //$Active		->	setAttrib('onClick','ToggleSelectBoxs()');.CheckBox
        $duringRegistration     ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $duringRegistration		->	removeDecorator("DtDdWrapper");
        $duringRegistration		->	removeDecorator("Label");
        $duringRegistration		->	removeDecorator('HtmlTag');
        
           $AdvancePayment 		= 	new Zend_Form_Element_Checkbox('AdvancePayment');
             $AdvancePayment ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $AdvancePayment		->	removeDecorator("DtDdWrapper");
        $AdvancePayment		->	removeDecorator("Label");
        $AdvancePayment		->	removeDecorator('HtmlTag');

        $Active 		= 	new Zend_Form_Element_Checkbox('Active');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $Active         ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $Active			->	removeDecorator("DtDdWrapper");
        $Active			->	removeDecorator("Label");
        $Active			->	removeDecorator('HtmlTag')
        				->	setValue("1");		
        						
       $Refund 		    = 	new Zend_Form_Element_Checkbox('Refund');
       $Refund         ->   setAttrib('dojoType',"dijit.form.CheckBox");
       $Refund		   ->	removeDecorator("DtDdWrapper");
       $Refund		   ->	removeDecorator("Label");
       $Refund		   ->	removeDecorator('HtmlTag');
        
       $Deposit 		= 	new Zend_Form_Element_Checkbox('Deposit');
       $Deposit        ->   setAttrib('dojoType',"dijit.form.CheckBox");
       $Deposit		   ->	removeDecorator("DtDdWrapper");
       $Deposit	       ->	removeDecorator("Label");
       $Deposit		   ->	removeDecorator('HtmlTag');
    
       $duringProcessing	= 	new Zend_Form_Element_Checkbox('duringProcessing');
       $duringProcessing   ->   setAttrib('dojoType',"dijit.form.CheckBox");
       $duringProcessing   ->	removeDecorator("DtDdWrapper");
       $duringProcessing   ->	removeDecorator("Label");
       $duringProcessing   ->	removeDecorator('HtmlTag');

        $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        
        $Save 	= 	new Zend_Form_Element_Submit('Save');
        $Save	->	setAttrib('class','NormalBtn');
        $Save	->dojotype="dijit.form.Button";
        $Save	->label = $gstrtranslate->_("Save");
        $Save	->	setAttrib('id', 'submitbutton');
        $Save	->	removeDecorator("DtDdWrapper");
        $Save	->	removeDecorator("Label");
        $Save	->	removeDecorator('HtmlTag');
        
        
         
   		$this->addElements(array($UpdDate,$UpdUser,$IdGroup,$AccountName,$AccShortName,$PrefixCode,$Description,$AccountCode,$duringRegistration,$AdvancePayment,$Period,$AccountType,$BillingModule,$Revenue,$Active,$Refund,$Deposit,$duringProcessing,$Save));
    }
}