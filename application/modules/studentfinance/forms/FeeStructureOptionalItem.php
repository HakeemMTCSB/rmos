<?php

class Studentfinance_Form_FeeStructureOptionalItem extends Zend_Form
{	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_fee_optional_item');
		$this->setAttrib('onsubmit', 'return checkFormOptionalItem()');
		$this->setAction('/studentfinance/fee-structure/edit-optional-item');
		
		//fee struc id
		$this->addElement('hidden','fsoi_structure_id', 
			array(
				'required'=>'true',
			)
		);
		
		//detail id
		$this->addElement('hidden','fsoi_detail_id', 
			array(
				'required'=>'true',
			)
		);
		

		//registration item
		$this->addElement('select','fsoi_item_id', array(
			'label'=>'Registration Item',
			'required'=>'true'	
		));
		$this->fsoi_item_id->addMultiOption(null,'Please Select');
		
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$feeItemList = $feeItemDb->getActiveFeeItem();
		
		//fee item
		$this->addElement('select','fsoi_fee_id', array(
			'label'=>'Fee Item',
			'required'=>'true'	
		));
		$this->fsoi_fee_id->addMultiOption(null,'Please Select');
		foreach ($feeItemList as $feeitem )
		{
			$this->fsoi_fee_id->addMultiOption($feeitem['fi_id'], $feeitem['fi_name'].' ('.$feeitem['freqMode'].' '.$feeitem['calType'].')');
		}
		
		//currency_id
		$this->addElement('select','fsoi_currency_id', array(
				'label' => 'Currency',
				'required' => 'true'
		));

		$currencyDb = new Studentfinance_Model_DbTable_Currency();
    	$currencyData = $currencyDb->fetchAll('cur_status = 1')->toArray();
		foreach ( $currencyData as $currency )
		{
			$this->fsoi_currency_id->addMultiOption($currency['cur_id'], $currency['cur_code']);
		}

		//amount
		$this->addElement('text','fsoi_amount', 
			array(
				'label'=>'Amount',
				'class' => 'input-txt'
			)
		);

		
	
		//button
		$this->addElement('submit', 'save_optionalitem', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"showform(); return false;"
        ));
        
        $this->addDisplayGroup(array('save_optionalitem','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>