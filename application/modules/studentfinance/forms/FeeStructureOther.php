<?php 

class Studentfinance_Form_FeeStructureOther extends Zend_Form
{
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		
		//Program
		$this->addElement('select','fso_program_id', array(
			'label'=>$this->getView()->translate('Programme Name'),
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->fso_program_id->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
				$this->fso_program_id->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}
		
		//activity
		$this->addElement('select','fso_activity_id', array(
			'label'=>$this->getView()->translate('Activity'),
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$this->fso_activity_id->addMultiOption(null,'Please Select');
		foreach ($definationDb->getByCode('Finance Activity') as $list){
			$this->fso_activity_id->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}

		//trigger
		$this->addElement('select','fso_trigger', array(
			'label'=>$this->getView()->translate('Trigger'),
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$this->fso_trigger->addMultiOption(null,'Please Select');
		foreach ($definationDb->getByCode('Trigger') as $list){
			$this->fso_trigger->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}

		//Fee code
		$this->addElement('select','fso_fee_id', array(
			'label'=>$this->getView()->translate('Fee Code'),
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$feeItemDb = new Studentfinance_Model_DbTable_FeeItem();
		$feeItemList = $feeItemDb->getActiveFeeItem();
		
		$this->fso_fee_id->addMultiOption(null,"-- Please Select --");		
		foreach ($feeItemList as $feeitem )
		{
			$this->fso_fee_id->addMultiOption($feeitem['fi_id'], $feeitem['fi_name'].' ('.$feeitem['freqMode'].' '.$feeitem['calType'].')');
		}
		
		//proforma
		$this->addElement('checkbox','fso_proforma', array(
			'label'=>$this->getView()->translate('Proforma?')
		));
		
		//status
		$this->addElement('checkbox','fso_status', array(
			'label'=>$this->getView()->translate('Active?')
		));
		
		
		
		
		//button
		$this->addElement('submit', 'Submit', array(
          'label'=>$this->getView()->translate('Submit'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('reset', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Submit','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>