<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/8/2016
 * Time: 2:11 PM
 */
class Studentfinance_Form_AdvPymntHaveBalance extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');
        $programId = $this->getAttrib('programId');

        $model = new Studentfinance_Model_DbTable_AdvPymntReport();

        //program
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getProgramScheme(this.value); getSemester(this.value);');
        $program->setAttrib('required', true);
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programList = $model->getProgram();

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }

        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
        $programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        //$programscheme->setAttrib('required', true);
        $programscheme->removeDecorator("Label");

        $programscheme->addMultiOption('', '-- Select --');

        //semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('required', true);
        $semester->removeDecorator("Label");

        $semester->addMultiOption('', '-- Select --');

        if ($programId){
            $programschemeList = $model->getProgramScheme($programId);

            if ($programschemeList){
                foreach ($programschemeList as $programschemeLoop){
                    $programscheme->addMultiOption($programschemeLoop['IdProgramScheme'], $programschemeLoop['mop'].' '.$programschemeLoop['mos'].' '.$programschemeLoop['pt']);
                }
            }

            $programInfo = $model->getProgramById($programId);

            if (isset($programInfo['IdScheme']) && $programInfo['IdScheme'] != null) {
                $semesterList = $model->getSemester($programInfo['IdScheme']);

                if ($semesterList) {
                    foreach ($semesterList as $semesterLoop){
                        $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
                    }
                }
            }
        }

        //Student Name
        $name = new Zend_Form_Element_Text('name');
        $name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //Student Id
        $studentid = new Zend_Form_Element_Text('studentid');
        $studentid->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //advance payment no
        $advpymntno = new Zend_Form_Element_Text('advpymntno');
        $advpymntno->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //type
        $type = new Zend_Form_Element_Radio('type');
        $type->addMultiOptions(array (
            '0' => ' Applicant',
            '1' => ' Student'
        ));
        $type->setvalue('1')->setSeparator('<br />');
        $type->removeDecorator("DtDdWrapper");
        $type->removeDecorator("Label");
        $type->removeDecorator('HtmlTag');

        $this->addElements(array(
            $program,
            $programscheme,
            $semester,
            $name,
            $studentid,
            $advpymntno,
            $type
        ));
    }
}