<?php
class Studentfinance_Form_Termsandconditions extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {      				

        $Termname	= 	new Zend_Form_Element_Text('Termname');
        $Termname ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Termname	->	removeDecorator("DtDdWrapper");
        $Termname-> setAttrib('required',"true")	;
        $Termname	->	removeDecorator("Label");
        $Termname	->	removeDecorator('HtmlTag');       	
        $Termname	->	setAttrib('class', 'txt_put'); 
		
		$Description = new Zend_Form_Element_Text('Description');
        $Description->setAttrib('dojoType',"dijit.form.SimpleTextarea");
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');

        $Default  = new Zend_Form_Element_Checkbox('Default');
        $Default->setAttrib('dojoType',"dijit.form.CheckBox");
        $Default->setvalue('1');
        $Default->removeDecorator("DtDdWrapper");
        $Default->removeDecorator("Label");
        $Default->removeDecorator('HtmlTag');
		
        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

        $gstrtranslate =Zend_Registry::get('Zend_Translate');         
        $Save 	= 	new Zend_Form_Element_Submit('Save');
        $Save	->	setAttrib('class','NormalBtn');
        $Save	->	dojotype="dijit.form.Button";
        $Save	->	label 	= $gstrtranslate->_("Save");
        $Save	->	setAttrib('id', 'submitbutton');
        $Save	->	removeDecorator("DtDdWrapper");
        $Save	->	removeDecorator("Label");
        $Save	->	removeDecorator('HtmlTag');           
         
   		$this->addElements(array($Termname,$Description,$Default,$Active,$Save));
    }
}