<?php

class Studentfinance_Form_Promotionsetup extends Zend_Dojo_Form
{
	private $lobjPolicySetup;
	public function init()
	{

		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		$this->lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
		//$this->lobjSponsorSetup = new Studentfinance_Model_DbTable_Sponsor();


		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$IdUniversity = new Zend_Form_Element_Hidden('IdUniversity');
		$IdUniversity->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$Promotion_Code = new Zend_Form_Element_Text('Promotion_Code');
        $Promotion_Code->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Promotion_Code->removeDecorator("DtDdWrapper");
        $Promotion_Code->removeDecorator("Label");
        $Promotion_Code->removeDecorator('HtmlTag');
        $Promotion_Code->setAttrib('required',"true");


		$Promotion_description = new Zend_Form_Element_Text('Promotion_description');
        $Promotion_description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Promotion_description->removeDecorator("DtDdWrapper");
        $Promotion_description->setAttrib('required',"true");
        $Promotion_description->removeDecorator("Label");
        $Promotion_description->removeDecorator('HtmlTag');

        $Awardid = new Zend_Form_Element_Select('Awardid');
        $Awardid->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Awardid->removeDecorator("DtDdWrapper");
        $Awardid->setAttrib('required',"true");
        $Awardid->removeDecorator("Label");
        $Awardid->removeDecorator('HtmlTag');
        $Awardid->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $awardDb = new GeneralSetup_Model_DbTable_Awardlevel();
        $award = $awardDb->getLevelByProgramType('');
        foreach($award as $aw){
            $Awardid->addMultiOption($aw['key'],$aw['value']);
        }

        $sem_start  = new Zend_Form_Element_Text('sem_start');
        //$sem_start->setAttrib('required',"false");
        $sem_start->removeDecorator("DtDdWrapper");
        $sem_start->setAttrib('class', 'dojoinput datepicker');
        $sem_start->removeDecorator("Label");
        $sem_start->removeDecorator('HtmlTag');

        $sem_end  = new Zend_Form_Element_Text('sem_end');
        //$sem_end->setAttrib('required',"false");
        $sem_end->removeDecorator("DtDdWrapper");
        $sem_end->setAttrib('class', 'dojoinput datepicker');
        $sem_end->removeDecorator("Label");
        $sem_end->removeDecorator('HtmlTag');

 /*       $dateformat = "{datePattern:'dd-MM-yyyy'}";
        $sem_start  = new Zend_Form_Element_Text('sem_start');
        $sem_start->setAttrib('dojoType',"dijit.form.DateTextBox");
        $sem_start->setAttrib('onChange',"dijit.byId('sem_end').constraints.min = arguments[0];") ;
        $sem_start->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
        $sem_start->removeDecorator("DtDdWrapper");
        $sem_start->removeDecorator("Label");
        $sem_start->removeDecorator('HtmlTag');


        $sem_end  = new Zend_Form_Element_Text('sem_end');
        $sem_end->setAttrib('dojoType',"dijit.form.DateTextBox");
        $sem_end->setAttrib('onChange',"dijit.byId('sem_start').constraints.max = arguments[0];") ;
        $sem_end->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
        $sem_end->removeDecorator("DtDdWrapper");
        $sem_end->removeDecorator("Label");
        $sem_end->removeDecorator('HtmlTag');*/

        $reg_start  = new Zend_Form_Element_Text('reg_start');
        //$reg_start->setAttrib('required',"false");
        $reg_start->removeDecorator("DtDdWrapper");
        $reg_start->setAttrib('class', 'dojoinput datepicker');
        $reg_start->removeDecorator("Label");
        $reg_start->removeDecorator('HtmlTag');

        $reg_end  = new Zend_Form_Element_Text('reg_end');
        //$reg_end->setAttrib('required',"false");
        $reg_end->removeDecorator("DtDdWrapper");
        $reg_end->setAttrib('class', 'dojoinput datepicker');
        $reg_end->removeDecorator("Label");
        $reg_end->removeDecorator('HtmlTag');

        $min_payment = new Zend_Form_Element_Text('min_payment');
        $min_payment->setAttrib('dojoType',"dijit.form.NumberTextBox")
            ->setAttrib('invalidMessage', 'Only digits')
            ->setAttrib('constraints',"{min:0,pattern:'#'}")
            ->setAttrib('required',"false")
            ->setAttrib('class', 'txt_put')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $total_sem  = new Zend_Form_Element_Text('total_sem');
        $total_sem ->setAttrib('dojoType',"dijit.form.NumberTextBox")
            ->setAttrib('invalidMessage', 'Only digits')
            ->setAttrib('constraints',"{min:0,pattern:'#'}")
            ->setAttrib('required',"false")
            ->setAttrib('class', 'txt_put')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $adjustment_type= new Zend_Form_Element_Select('adjustment_type');
        $adjustment_type->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $adjustment_type->removeDecorator("DtDdWrapper");
        $adjustment_type->setAttrib('required',"false");
        $adjustment_type->removeDecorator("Label");
        $adjustment_type->removeDecorator('HtmlTag');
        $adjustment_type->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $registerDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $type = $registerDb->getDataByCodeType('adjustment-type');
        foreach($type as $data){
            $adjustment_type->addMultiOption($data['key'],$data['value']);
        }

        $exclusive =  new Zend_Form_Element_Checkbox('exclusive');
        $exclusive -> setAttrib('dojoType',"dijit.form.CheckBox");
        $exclusive -> removeDecorator("DtDdWrapper");
        $exclusive	-> removeDecorator("Label");
        $exclusive->setCheckedValue(1);
        $exclusive->setUncheckedValue(0);
        $exclusive	-> removeDecorator('HtmlTag');

        $active =  new Zend_Form_Element_Checkbox('active');
        $active -> setAttrib('dojoType',"dijit.form.CheckBox");
        $active -> removeDecorator("DtDdWrapper");
        $active	-> removeDecorator("Label");
        $active->setCheckedValue(1);
        $active->setUncheckedValue(0);
        $active	-> removeDecorator('HtmlTag');

        $createddt = new Zend_Form_Element_Hidden('createddt');
        $createddt->removeDecorator("DtDdWrapper");
        $createddt->removeDecorator("Label");
        $createddt->removeDecorator('HtmlTag');

        $createdby  = new Zend_Form_Element_Hidden('createdby');
        $createdby->removeDecorator("DtDdWrapper");
        $createdby->removeDecorator("Label");
        $createdby->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
            ->class = "NormalBtn";


        $Add = new Zend_Form_Element_Button('Add');
        $Add->label = $gstrtranslate->_("Add");
        $Add->dojotype="dijit.form.Button";
        $Add->removeDecorator("DtDdWrapper");
        $Add->removeDecorator('HtmlTag')
            ->class = "NormalBtn";


        $Clear = new Zend_Form_Element_Button('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
        $Clear->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $fee_category_info = new Zend_Form_Element_Text('fee_category_info');
        $fee_category_info->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $fee_category_info->removeDecorator("DtDdWrapper");
        $fee_category_info->removeDecorator("Label");
        $fee_category_info->removeDecorator('HtmlTag');
        $fee_category_info->setAttrib('required',"false");

        $calc_type_info= new Zend_Form_Element_Select('calc_type_info');
        $calc_type_info->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $calc_type_info->removeDecorator("DtDdWrapper");
        $calc_type_info->setAttrib('required',"false");
        $calc_type_info->removeDecorator("Label");
        $calc_type_info->removeDecorator('HtmlTag');
        $calc_type_info->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $registerDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $type = $registerDb->getDataByCodeType('calculation-type');
        foreach($type as $data){
            $calc_type_info->addMultiOption($data['key'],$data['value']);
        }

        $sem_info  = new Zend_Form_Element_Text('sem_info');
        $sem_info ->setAttrib('dojoType',"dijit.form.NumberTextBox")
            ->setAttrib('invalidMessage', 'Only digits')
            ->setAttrib('constraints',"{min:0,pattern:'#'}")
            ->setAttrib('required',"false")
            ->setAttrib('class', 'txt_put')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $value_info  = new Zend_Form_Element_Text('value_info');
        $value_info ->setAttrib('dojoType',"dijit.form.NumberTextBox")
            ->setAttrib('invalidMessage', 'Only digits')
            ->setAttrib('constraints',"{min:0,pattern:'#'}")
            ->setAttrib('required',"false")
            ->setAttrib('class', 'txt_put')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $program= new Zend_Form_Element_Select('program');
        $program->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('required',"false");
        $program->removeDecorator("Label");
        $program->removeDecorator('HtmlTag');
        $program->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $progDb = new GeneralSetup_Model_DbTable_Program();
        $pg = $progDb->fnGetProgramList();//fngetProgramDetails(programid);
        foreach($pg as $data){
            $program->addMultiOption($data['key'],$data['value']);
        }




        $this->addElements(array($UpdUser,$UpdDate,$IdUniversity,
            $Promotion_Code,
            $Promotion_description,
            $Awardid,
            $sem_start,
            $sem_end,
            $reg_start,
            $reg_end,
            $min_payment,
            $total_sem,
            $adjustment_type,
            $exclusive,
            $active,
            $Save,
            $fee_category_info,
            $calc_type_info,
            $sem_info,
            $value_info,
            $Add,
            $Clear,
            $program

			));


	}
}