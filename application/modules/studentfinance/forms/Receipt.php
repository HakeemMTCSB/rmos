<?php
class Studentfinance_Form_Receipt extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdInvoice = new Zend_Form_Element_Hidden('IdInvoice');
        $IdInvoice->removeDecorator("DtDdWrapper");
        $IdInvoice->removeDecorator("Label");
        $IdInvoice->removeDecorator('HtmlTag');
    	
	 	$StudentId = new Zend_Dojo_Form_Element_FilteringSelect('StudentId');
     	$StudentId->setAttrib('dojoType',"dijit.form.FilteringSelect");
     	$StudentId ->setAttrib('OnChange', 'fnGetInvoiceNumber(this.value)');
     	$StudentId->setAttrib('required',"false	")	;
	 	$StudentId->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
					
		$ItemName = new Zend_Dojo_Form_Element_FilteringSelect('ItemName');
		$ItemName->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $ItemName->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
						
		$IdProgram  = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
    	$IdProgram ->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$IdProgram->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
					
		$IdStudent = new Zend_Form_Element_Text('IdStudent');
        $IdStudent->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $IdStudent->setAttrib('required',"false");
        $IdStudent->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
								
	    $IdCollege = new Zend_Dojo_Form_Element_FilteringSelect('IdCollege');
		$IdCollege ->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$IdCollege->removeDecorator("DtDdWrapper")
	        	        ->removeDecorator("Label")
	        		    ->removeDecorator('HtmlTag');
        		    
       $InvoiceDt  = new Zend_Dojo_Form_Element_DateTextBox('InvoiceDt');
       $InvoiceDt->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
		
	  	$RecieptDate = new Zend_Dojo_Form_Element_DateTextBox('RecieptDate');
      	$RecieptDate->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
       
		
		$SemesterId = new Zend_Dojo_Form_Element_FilteringSelect('SemesterId');
	    $SemesterId->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $SemesterId->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');

		$MonthYear = new Zend_Form_Element_Text('MonthYear');
		$MonthYear->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		          ->setAttrib('required',"true")	;
        $MonthYear->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label")	
        		->removeDecorator('HtmlTag');
        		
        $AcdmcPeriod  = new Zend_Form_Element_Text('AcdmcPeriod',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$AcdmcPeriod->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AcdmcPeriod->removeDecorator("Label")
        			->setAttrib('required',"true")		
        		->removeDecorator('HtmlTag');
    	
		$Naration = new Zend_Form_Element_Text('Naration');
		$Naration->setAttrib('dojoType',"dijit.form.TextBox");
        $Naration->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       $Active = new Zend_Form_Element_Checkbox('Active');
       $Active->setAttrib('dojoType',"dijit.form.CheckBox");
       $Active->removeDecorator("DtDdWrapper");
       $Active->removeDecorator("Label");
       $Active->removeDecorator('HtmlTag')
        	  ->setValue("1");		
        	  
       $Description = new Zend_Form_Element_Text('Description');	
       $Description		->setAttrib('maxlength','250')
        				->setAttrib('dojoType',"dijit.form.Textarea")
        				->setAttrib('style','width:auto;')
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');	
        	  	
        $idAccount 	 = new Zend_Dojo_Form_Element_FilteringSelect('idAccount');
        $idAccount->setAttrib('OnChange', 'fnGetAmount(this.value)');
		$idAccount->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $idAccount->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
        		    
       	$Amount = new Zend_Form_Element_Text('Amount');
       	$Amount->setAttrib('readonly',true);
		$Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag')
        		->setAttrib('required',"true");
        		    
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        
        
        $idReciept = new Zend_Form_Element_Hidden('idReciept');
        $idReciept->removeDecorator("DtDdWrapper");
        $idReciept->removeDecorator("Label");
        $idReciept->removeDecorator('HtmlTag');
        
		$Studentid = new Zend_Form_Element_Hidden('Studentid');
        $Studentid->removeDecorator("DtDdWrapper");
        $Studentid->removeDecorator("Label");
        $Studentid->removeDecorator('HtmlTag');        
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   		$InvoiceId  = new Zend_Dojo_Form_Element_FilteringSelect('InvoiceId');
   		$InvoiceId->setAttrib('required',"false");
      	$InvoiceId ->setAttrib('OnChange', 'fnGetInvoicedetails(this.value)');
   		$InvoiceId ->setAttrib('dojoType',"dijit.form.FilteringSelect");
	 	$InvoiceId->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
    		
        		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
 
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
		    ->setAttrib('onClick','addRecieptDetails()')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$RecieptNumber = new Zend_Form_Element_Text('RecieptNumber');
        $RecieptNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");	   
        $RecieptNumber->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
        		    
        $Amount = new Zend_Form_Element_Text('Amount');
        $Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Amount->setAttrib('required',"false");
        $Amount->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
				    
       	$ModeOfPayment = new Zend_Dojo_Form_Element_FilteringSelect('ModeOfPayment');
		$ModeOfPayment->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$ModeOfPayment->removeDecorator("Label")
        		      ->removeDecorator('HtmlTag');
        		      
       $ChequeDate  = new Zend_Dojo_Form_Element_DateTextBox('ChequeDate');
       $ChequeDate->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"false")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
	   $ChequeNumber  = new Zend_Form_Element_Text('ChequeNumber');
       $ChequeNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"false")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
									
        //form elements
        $this->addElements(array($IdInvoice,
                                  $UpdDate,
        						  $UpdUser,
        						  $IdStudent,
        						  $InvoiceDt,
        						  $Save,
        						  $Add,
        						  $Active,
        						  $Description,
        						  $InvoiceId,
        						  $Studentid,
        						  $MonthYear,
        						  $Naration,
        						  $AcdmcPeriod,
        						  $idAccount,
        						  $Amount,
        						  $SemesterId,
								  $RecieptDate,
								  $IdProgram,
								  $IdCollege,
								  $RecieptNumber,
								  $Amount,
								  $StudentId,
								  $ItemName,
								  $ModeOfPayment,
								  $ChequeDate,$idReciept,
								  $ChequeNumber
                                 ));

    }
}