<?php
/**
 * @author Suliana <suliana@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Form_FeeConvo extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_fee_convo');
		
		$this->addElement('select','fv_fi_id',
				array(
						'label'=>'Fee Item',
						'required'=>true,
						'class'=>'input-txt'
				)
		);
		
		$FeeDB = new Studentfinance_Model_DbTable_FeeItem();
		$fee_item = $FeeDB->getActiveFeeByCategory(25); //convocation
		
		$this->fv_fi_id->addMultiOption(null,"Please Select");
		
		if($fee_item){
			foreach ($fee_item as $list){
				$this->fv_fi_id->addMultiOption($list['fi_id'],$list['fi_name']." (".$list['fi_code'].")");
			}
		}
		
		$this->addElement('select','fv_currency_id',
				array(
						'label'=>'Currency',
						'required'=>true,
						'class'=>'input-txt'
				)
		);
		
		$currencyDB = new Studentfinance_Model_DbTable_Currency();
		$currencyData = $currencyDB->getList(); 
		
		$this->fv_currency_id->addMultiOption(null,"Please Select");
		
		if($currencyData){
			foreach ($currencyData as $list){
				$this->fv_currency_id->addMultiOption($list['cur_id'],$list['cur_desc']." (".$list['cur_code'].")");
			}
		}

		$this->addElement('text','fv_amount',
				array(
						'label'=>'Fee Amount',
						'required'=>true,
						'class'=>'input-txt'
				)
		);
		

		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-category','action'=>'index'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>