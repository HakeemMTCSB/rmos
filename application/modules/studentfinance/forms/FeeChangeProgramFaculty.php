<?php

class Studentfinance_Form_FeeChangeProgramFaculty extends Zend_Form
{
	protected $feechangeprogram;
	protected $college;
	protected $intake;
	
	public function setIntake($intake){
		$this->intake = $intake;
	}
	
	public function setFeechangeprogram($feechangeprogram){
		$this->feechangeprogram = $feechangeprogram;
	}
	
	public function setCollege($college){
		$this->college = $college;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_change_program_charges');
		
		//head id
		$element = new Zend_Form_Element_Hidden('fcp_id');
		$element->setValue($this->feechangeprogram);
		$this->addElement($element);
		
		//college id
		$element = new Zend_Form_Element_Hidden('college_id');
		$element->setValue($this->college);
		$this->addElement($element);
		
		//amount
		$this->addElement('text','amount', 
			array(
				'label'=>'Amount'
			)
		);
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-change-program','action'=>'charges-faculty', 'intake'=>$this->intake, 'id'=>$this->feechangeprogram),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
	
	private function getIntake($intake_id){
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeData = $intakeDb->getData($intake_id);
		
		return $intakeData;
	}
}
?>