<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 27/10/2015
 * Time: 3:23 PM
 */
class Studentfinance_Form_SponsorPaymentSearch extends Zend_Dojo_Form {

    public function init() {
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');
        $stypeid = $this->getAttrib('stypeid');

        //load model
        $model = new Registration_Model_DbTable_Studentregistration();
        $definationDB = new App_Model_General_DbTable_Definationms();
        $model2 = new Records_Model_DbTable_Report();

        //date from
        $DateFrom = new Zend_Form_Element_Text('DateFrom');
        $DateFrom->setAttrib('class', 'input-txt')
            ->setAttrib("required", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //date to
        $DateTo = new Zend_Form_Element_Text('DateTo');
        $DateTo->setAttrib('class', 'input-txt')
            ->setAttrib("required", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //program
        $Program = new Zend_Form_Element_Select('Program');
        $Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getProgramScheme(this.value);');

        $Program->addMultiOption('', '-- Select --');

        $ProgramList = $model->getProgram();

        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $Program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }

        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
        $ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
        $ProgramScheme->removeDecorator("Label");

        $ProgramScheme->addMultiOption('', '-- Select --');

        if ($programid){
            $programSchemeList = $model2->getProgramScheme($programid);

            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $ProgramScheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }

        //type
        $stype = new Zend_Form_Element_Select('stype');
        $stype->removeDecorator("DtDdWrapper");
        $stype->setAttrib('class', 'select');
        $stype->removeDecorator("Label");
        $stype->setAttrib('onchange', 'getScholarship(this.value);');

        $stype->addMultiOption('', '-- Select --');
        $stype->addMultiOption('1', 'Scholarship');
        $stype->addMultiOption('2', 'Sponsorship');
        $stype->addMultiOption('3', 'Financial Aid');

        //scholarsip/sponsorship
        $Scholarship = new Zend_Form_Element_Select('Scholarship');
        $Scholarship->removeDecorator("DtDdWrapper");
        $Scholarship->setAttrib('class', 'select');
        $Scholarship->removeDecorator("Label");

        $Scholarship->addMultiOption('', '-- Select --');

        if ($stypeid){
            $scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();

            switch ($stypeid){
                case 1: //scholarship
                    $list = $scholarshipModel->getScholarship();
                    break;
                case 2: //sponsorship
                    $list = $scholarshipModel->getSponsorship();
                    break;
                case 3: //financial aid
                    $list = $scholarshipModel->getFinancialAid();
                    break;
            }

            if ($list){
                foreach ($list as $loop){
                    $Scholarship->addMultiOption($loop['id'], $loop['name']);
                }
            }
        }

        //payment mode
        $PaymentMode = new Zend_Form_Element_Select('PaymentMode');
        $PaymentMode->removeDecorator("DtDdWrapper");
        $PaymentMode->setAttrib('class', 'select');
        $PaymentMode->removeDecorator("Label");

        $PaymentMode->addMultiOption('', '-- Select --');

        //payment group
        $lobjdeftype = new App_Model_Definitiontype();
        $payment_mode = $lobjdeftype->fnGetDefinationsByLocale('Payment Mode Grouping');

        if ($payment_mode){
            foreach ($payment_mode as $payment_modeLoop){
                $PaymentMode->addMultiOption($payment_modeLoop['idDefinition'], $payment_modeLoop['DefinitionDesc']);
            }
        }

        $this->addElements(array(
            $DateFrom,
            $DateTo,
            $Program,
            $ProgramScheme,
            $stype,
            $Scholarship,
            $PaymentMode
        ));
    }
}