<?php

class Studentfinance_Form_DiscountType extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_dt');
		$this->setAttrib('action','/studentfinance/discount/type');
		
		$this->addElement('text','dt_discount', 
			array(
				'label'=>'Discount',
				'required'=>'true',
				'class'=>'input-txt'
			)
		);

		$this->dt_discount->removeDecorator("DtDdWrapper")
					      ->removeDecorator("Label")
						  ->removeDecorator('HtmlTag');
		
		$this->addElement('textarea','dt_description', 
			array(
				'label'=>'Description'
			)
		);

		$this->dt_description->removeDecorator("DtDdWrapper")
							 ->removeDecorator("Label")
							 ->removeDecorator('HtmlTag');
		
		/*$this->addElement('select','dt_discountitem', 
			array(
				'label'=>'Discounted Item'
			)
		);
		
		$lobjFeeItem = new Studentfinance_Model_DbTable_FeeItem();

		//Fee Item
		$feeList = $lobjFeeItem->getData();
		foreach( $feeList as $item)
		{
			$this->dt_discountitem->addMultiOption($item['fi_id'],$item['fi_name']);
		}*/

		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'discount','action'=>'setup'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>