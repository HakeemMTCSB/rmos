<?php

class Studentfinance_Form_ApplicationFormSetup extends Zend_Form
{
	protected $_locale;
	
	public function init()
	{
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
		
		$this->setMethod('post');
		$this->setAttrib('id','form_fs');
		
		//intake start
		$this->addElement('select','semester_id', 
			array(
				'label'=>'Semester',
				'required'=>'true'
			)
		);
		$semDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$listData = $semDb->getData();
		$this->semester_id->addMultiOption(null,"Please Select");
		 
		if($this->_locale == 'en_US'){
			foreach ($listData as $list){
				$this->semester_id->addMultiOption($list['IdSemesterMaster'],$list['SemesterMainName']);
			}
		}else{
			foreach ($listData as $list){
				$this->semester_id->addMultiOption($list['IdSemesterMaster'],$list['SemesterMainDefaultLanguage']);
			}
		}
		
		//start date
		$this->addElement('text','start_date',
				array(
						'label'=>'Start Date',
						'required'=>'true',
						'class'=>'datepicker input-txt',
						'placeholder' => 'YYYY-MM-DD'
				)
		);

		//start date
		$this->addElement('text','end_date',
				array(
						'label'=>'End Date',
						'required'=>'true',
						'class'=>'datepicker input-txt',
						'placeholder' => 'YYYY-MM-DD'
				)
		);
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'applicationformsetup','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>