<?php

class Studentfinance_Form_Receivableadjustment extends Zend_Dojo_Form
{

	public function init()
	{
		$DefmodelObj = new App_Model_Definitiontype();
		$lobjReceivableAdjustment = new Studentfinance_Model_DbTable_Receivableadjustment();
		$lobjFeeCategory = new Studentfinance_Model_DbTable_Fee();

		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$EnterBy = new Zend_Form_Element_Hidden('EnterBy');
		$EnterBy->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$EnterDate = new Zend_Form_Element_Hidden('EnterDate');
		$EnterDate->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');




		$IdUniversity = new Zend_Form_Element_Hidden('IdUniversity');
		$IdUniversity->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$AdjustmentNumber = new Zend_Form_Element_Text('AdjustmentNumber');
		$AdjustmentNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$AdjustmentNumber->removeDecorator("DtDdWrapper");
		$AdjustmentNumber->removeDecorator("Label");
		$AdjustmentNumber->removeDecorator('HtmlTag');
		$AdjustmentNumber->setAttrib('required',"true");
		//$AdjustmentNumber->setAttrib('propercase',"true");


		$AdjustmentType = new Zend_Form_Element_Select('AdjustmentType');
		$AdjustmentType->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$AdjustmentType->removeDecorator("DtDdWrapper");
		$AdjustmentType->setAttrib('required',"true");
		$AdjustmentType->removeDecorator("Label");
		$AdjustmentType->removeDecorator('HtmlTag');
		$AdjustmentType->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$larrAdjustmenttype = $DefmodelObj->fnGetDefinationMs('Adjustment Type');
		foreach($larrAdjustmenttype as $larrvalues) {
			$AdjustmentType->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		$AdjustmentType->setAttrib('onChange', "showbelow(this)");
		
		$month = date("m"); // Month value
		$day = date("d"); //today's date
		$year = date("Y"); // Year value
		$yesterdaydate = date('Y-m-d', mktime(0, 0, 0, $month, ($day), $year));
		$DocumentDate = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
		$DocumentDate = new Zend_Dojo_Form_Element_DateTextBox('DocumentDate');
		$DocumentDate->setAttrib('required', "true")
		->setAttrib('dojoType', "dijit.form.DateTextBox")
		->setAttrib('title', "dd-mm-yyyy")
		//->setAttrib('constraints', "$BankSlipDate")
		->setAttrib('constraints',"{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}")
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ReceiptNumber = new Zend_Form_Element_Text('ReceiptNumber');
		$ReceiptNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ReceiptNumber->removeDecorator("DtDdWrapper");
		$ReceiptNumber->removeDecorator("Label");
		$ReceiptNumber->removeDecorator('HtmlTag');
		$ReceiptNumber->setAttrib('required',"true");
		//$ReceiptNumber->setAttrib('propercase',"true");

		$Remarks = new Zend_Form_Element_Text('Remarks');
		$Remarks->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Remarks->removeDecorator("DtDdWrapper");
		$Remarks->removeDecorator("Label");
		$Remarks->removeDecorator('HtmlTag');
		$Remarks->setAttrib('required',"true");
		$Remarks->setAttrib('propercase',"true");


		$Active = new Zend_Form_Element_Checkbox('Active');
		$Active->setAttrib('dojoType',"dijit.form.CheckBox");
		$Active->removeDecorator("DtDdWrapper");
		$Active->removeDecorator("Label");
		$Active->removeDecorator('HtmlTag')
		->	setValue("1");

		$IdPaymentGroup = new Zend_Dojo_Form_Element_FilteringSelect('IdPaymentGroup');
		$IdPaymentGroup->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$larrIdPaymentGroup = $DefmodelObj->fnGetDefinationMs('Payment Mode Grouping');
		foreach($larrIdPaymentGroup as $larrvalues) {
			$IdPaymentGroup->addMultiOption($larrvalues['key'],$larrvalues['value']);
		}
		//->setAttrib('onChange', "fngetpaymentmode(this)");

		$PaymentMode = new Zend_Dojo_Form_Element_FilteringSelect('PaymentMode');
		$PaymentMode->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$PaymentDocNo = new Zend_Form_Element_Text('PaymentDocNo');
		$PaymentDocNo->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$PaymentDocNo->removeDecorator("DtDdWrapper");
		$PaymentDocNo->removeDecorator("Label");
		$PaymentDocNo->removeDecorator('HtmlTag');
		$PaymentDocNo->setAttrib('required', "false");
		$PaymentDocNo->setAttrib('propercase', "true");

		$PayDocBank = new Zend_Dojo_Form_Element_FilteringSelect('PayDocBank');
		$PayDocBank->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$larrPayDocBank = $lobjReceivableAdjustment->fngetbanktype();
		foreach($larrPayDocBank as $larrvalues) {
			$PayDocBank->addMultiOption($larrvalues['AUTOINC'],$larrvalues['BANKCODE']);
		}

		$PayDocBankBranch = new Zend_Form_Element_Text('PayDocBankBranch');
		$PayDocBankBranch->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$PayDocBankBranch->removeDecorator("DtDdWrapper");
		$PayDocBankBranch->removeDecorator("Label");
		$PayDocBankBranch->removeDecorator('HtmlTag');
		$PayDocBankBranch->setAttrib('required', "false");
		$PayDocBankBranch->setAttrib('propercase', "false");

		$TerminalId = new Zend_Dojo_Form_Element_FilteringSelect('TerminalId');
		$TerminalId->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$larrTerminalId = $lobjReceivableAdjustment->fngetterminalid();
		foreach($larrTerminalId as $larrvalues) {
			$TerminalId->addMultiOption($larrvalues['IdTerminal'],$larrvalues['TerminalID']);
		}


		$BankCardNo = new Zend_Form_Element_Text('BankCardNo');
		$BankCardNo = new Zend_Form_Element_Text('BankCardNo', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$BankCardNo->setAttrib('constraints', '{min:1,max:9999}');
		$BankCardNo->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$BankCardNo->removeDecorator("DtDdWrapper");
		$BankCardNo->removeDecorator("Label");
		$BankCardNo->setAttrib('maxlength','16');
		$BankCardNo->setAttrib('minlength','16');
		$BankCardNo->removeDecorator('HtmlTag');
		$BankCardNo->setAttrib('required', "false");
		$BankCardNo->setAttrib('propercase', "true");

		$BankCardType = new Zend_Dojo_Form_Element_FilteringSelect('BankCardType');
		$BankCardType->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$larrBankCard = $lobjReceivableAdjustment->fngetbankcard();
		foreach($larrBankCard as $larrvalues) {
			$BankCardType->addMultiOption($larrvalues['IdBankCard'],$larrvalues['BankCardType']);
		}


		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";
		
		$AddPaymentInfo = new Zend_Form_Element_Button('AddPaymentInfo');
		$AddPaymentInfo->label = $gstrtranslate->_("Add");
		$AddPaymentInfo->dojotype="dijit.form.Button";
		$AddPaymentInfo->removeDecorator("DtDdWrapper");
		$AddPaymentInfo->removeDecorator('HtmlTag')
		->setAttrib('onclick', 'addPaymentInfodetails()')
		->class = "NormalBtn";

		$Clear = new Zend_Form_Element_Button('Clear');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
		
		$ClearPaymentInfo = new Zend_Form_Element_Button('ClearPaymentInfo');
		$ClearPaymentInfo->dojotype="dijit.form.Button";
		$ClearPaymentInfo->label = $gstrtranslate->_("Clear");
		$ClearPaymentInfo->setAttrib('class', 'NormalBtn')
		->setAttrib('onclick', 'clearPaymentInfo()')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
		
		
		$Search = new Zend_Form_Element_Button('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$Back = new Zend_Form_Element_Button('Back');
		$Back->label = $gstrtranslate->_("Back");
		$Back->dojotype="dijit.form.Button";
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$lobjfeeModel = new Studentfinance_Model_DbTable_Fee();
		$larrfeecodeList = $lobjfeeModel->fngetfeesetuplist();
		$FeeCode = new Zend_Dojo_Form_Element_FilteringSelect('FeeCode');
		$FeeCode->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect")
		->setAttrib('onchange', "getfeecodedet(this)");
		$FeeCode->addMultioptions($larrfeecodeList);

		$NonInvoiceDescription = new Zend_Form_Element_Textarea('NonInvoiceDescription');
		$NonInvoiceDescription->setAttrib('cols', '30')
		->setAttrib('rows','3')
		->setAttrib('style','width = 10%;')
		->setAttrib('maxlength','250')
		->setAttrib('dojoType',"dijit.form.SimpleTextarea")
		->setAttrib('style','margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		$NonInvoiceDescriptiondefault = new Zend_Form_Element_Textarea('NonInvoiceDescriptiondefault');
		$NonInvoiceDescriptiondefault->setAttrib('cols', '30')
		->setAttrib('rows','3')
		->setAttrib('style','width = 10%;')
		->setAttrib('maxlength','250')
		->setAttrib('dojoType',"dijit.form.SimpleTextarea")
		->setAttrib('style','margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');




		$ChargeCode = new Zend_Dojo_Form_Element_FilteringSelect('ChargeCode');
		$ChargeCode->setAttrib('required', "false")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setRegisterInArrayValidator(false)
		->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$larrcode = $lobjFeeCategory->fngetcode();
//		foreach($larrcode as $larrvalues) {
//			$ChargeCode->addMultiOption($larrvalues['AUTOINC'],$larrvalues['CMPYCODE'].' '.$larrvalues['ACCOUNTCODE']);
//		}


		$Amount = new Zend_Form_Element_Text('Amount');
		$Amount = new Zend_Form_Element_Text('Amount', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$Amount->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Amount->removeDecorator("DtDdWrapper");
		$Amount->removeDecorator("Label");
		$Amount->removeDecorator('HtmlTag');
		$Amount->setAttrib('required', "false");
		$Amount->setAttrib('propercase', "true");
			
		$this->addElements(array($UpdUser,$UpdDate,$IdUniversity,$AdjustmentNumber,
				$AdjustmentType,$DocumentDate,$ReceiptNumber,
				$Active,$Remarks,$Back,$EnterBy,$EnterDate,
				$Save,$Add,$Clear,$Search,$IdPaymentGroup,$TerminalId,$BankCardNo,$BankCardType,
				$PaymentMode,$PaymentDocNo,$PayDocBank,$PayDocBankBranch,
				$Amount,$ChargeCode,$NonInvoiceDescription,$FeeCode,$NonInvoiceDescriptiondefault,
				$ClearPaymentInfo,$AddPaymentInfo));


	}
}