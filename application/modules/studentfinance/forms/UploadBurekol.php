<?php
class Studentfinance_Form_UploadBurekol extends Zend_Form {
		
	public function init()
	{
        //parent::__construct($options);
        
        $this->setName('upload');
        $this->setAttrib('enctype', 'multipart/form-data');
		$this->setAttrib('action', $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'burekol','action'=>'import'),'default',true) );
		$this->setAttrib('method', "POST" );
          
        $file = new Zend_Form_Element_File('file');
        $file->setLabel('File')
            ->setDestination(APPLICATION_PATH  . '/tmp')
            ->setRequired(true);
		$this->addElement($file);
		
		//ApplicantID

		$this->addElement('hidden', 'header', array(
			'description' => 'CSV Configuration Option',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$checkbox_element = new Zend_Form_Element_Checkbox("preview", array("checked" => "checked"));
        $checkbox_element->setLabel('Preview data before save')
               ->setRequired(true);
        
		$this->addElement($checkbox_element);

		$format = new Zend_Form_Element_Checkbox('remove_header', array("checked" => "checked"));
		$format->setLabel('Remove Header')
		->setRequired(true);
		
		$this->addElement($format);
	
	}
		
}