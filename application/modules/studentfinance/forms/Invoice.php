<?php
class Studentfinance_Form_Invoice extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdInvoice = new Zend_Form_Element_Hidden('IdInvoice');
        $IdInvoice->removeDecorator("DtDdWrapper");
        $IdInvoice->removeDecorator("Label");
        $IdInvoice->removeDecorator('HtmlTag');
        
        $IdProgram = new Zend_Form_Element_Hidden('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        
    	$IdStudent 	 = new Zend_Dojo_Form_Element_FilteringSelect('IdStudent');
    	$IdStudent->setAttrib('OnChange', 'fnGetProgram(this.value)');
		$IdStudent->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdStudent->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
					
		$Program = new Zend_Form_Element_Text('Program');
		$Program->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Program->setAttrib('required',"false");
		$Program->setAttrib('readonly',true);
     	$Program->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
					
		$Faculty = new Zend_Form_Element_Text('Faculty');
		$Faculty->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		          ->setAttrib('required',"false");
		$Faculty->setAttrib('readonly',true);
	    $Faculty->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label")	
        		->removeDecorator('HtmlTag');
        		    
       $InvoiceDt  = new Zend_Dojo_Form_Element_DateTextBox('InvoiceDt');
       $InvoiceDt->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
       
        $ApplicantDate  = new Zend_Form_Element_Text('ApplicantDate');
        $ApplicantDate->setAttrib('readonly',true);
        $ApplicantDate->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('required',"false")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');		   
						
						
		$semester = new Zend_Dojo_Form_Element_FilteringSelect('semester');
		$semester->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $semester->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');

		$MonthYear = new Zend_Form_Element_Text('MonthYear');
		$MonthYear->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		          ->setAttrib('required',"true")	;
        $MonthYear->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label")	
        		->removeDecorator('HtmlTag');
        		
        $AcdmcPeriod  = new Zend_Form_Element_Text('AcdmcPeriod',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$AcdmcPeriod->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AcdmcPeriod->removeDecorator("Label")
        			->setAttrib('required',"true")		
        		->removeDecorator('HtmlTag');
    	
		$Naration = new Zend_Form_Element_Text('Naration');
		$Naration->setAttrib('dojoType',"dijit.form.TextBox");
        $Naration->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       $Active = new Zend_Form_Element_Checkbox('Active');
       $Active->setAttrib('dojoType',"dijit.form.CheckBox");
       $Active->removeDecorator("DtDdWrapper");
       $Active->removeDecorator("Label");
       $Active->removeDecorator('HtmlTag')
        	  ->setValue("1");		
        	  
       $Description = new Zend_Form_Element_Text('Description');	
       $Description		->setAttrib('maxlength','250')
        				->setAttrib('dojoType',"dijit.form.Textarea")
        				->setAttrib('style','width:auto;')
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');	
        	  	
        $idAccount 	 = new Zend_Dojo_Form_Element_FilteringSelect('idAccount');
        $idAccount->setAttrib('OnChange', 'fnGetAmount(this.value)');
		$idAccount->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $idAccount->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
        		    
       	$Amount = new Zend_Form_Element_Text('Amount');
       	//$Amount->setAttrib('readonly',true);
		$Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag')
        		->setAttrib('required',"true");
        		    
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
		$Studentid = new Zend_Form_Element_Hidden('Studentid');
        $Studentid->removeDecorator("DtDdWrapper");
        $Studentid->removeDecorator("Label");
        $Studentid->removeDecorator('HtmlTag');        
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   		
        $InvoiceCode = new Zend_Form_Element_Text('InvoiceCode');
		$InvoiceCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $InvoiceCode->removeDecorator("DtDdWrapper")
                ->setAttrib('readonly',true)
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag'); 
    		
        		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
 
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
		    ->setAttrib('onClick','addAccountDetails()')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$termCondition = new Zend_Form_Element_MultiCheckbox('termCondition');
        $termCondition->setAttrib('required',"true");
        $termCondition->removeDecorator("DtDdWrapper"); 
        $termCondition->removeDecorator("Label");
        $termCondition->removeDecorator('HtmlTag');
		$termCondition->setAttrib('dojoType',"dijit.form.CheckBox");
	    $termCondition->class = "IdCollege";
				    
        //form elements
        $this->addElements(array($IdInvoice,
                                 $IdProgram,
        						  $UpdDate,
        						  $UpdUser,
        						  $IdStudent,
        						  $InvoiceDt,
        						  $Save,
        						  $Add,
        						  $Active,
        						  $Description,
        						  $InvoiceCode,
        						  $Studentid,
        						  $MonthYear,
        						  $Naration,
        						  $AcdmcPeriod,
        						  $idAccount,
        						  $Amount,
        						  $semester,
								  $ApplicantDate,
								  $Program,
								  $Faculty,
								  $termCondition
                                 ));

    }
}