<?php
class Studentfinance_Form_Checkpayments extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$joiningdate = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
    	
    	$IdVerifyPayments = new Zend_Form_Element_Hidden('IdVerifyPayments');
        $IdVerifyPayments->removeDecorator("DtDdWrapper");
        $IdVerifyPayments->removeDecorator("Label");
        $IdVerifyPayments->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
		
        
        $IdApplication = new Zend_Dojo_Form_Element_FilteringSelect('IdApplication');
   		$IdApplication->removeDecorator("DtDdWrapper");
        $IdApplication->setAttrib('required',"true") ;
        $IdApplication->removeDecorator("Label");
        $IdApplication->removeDecorator('HtmlTag');
        $IdApplication->setRegisterInArrayValidator(false);
		$IdApplication->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdSemester = new Zend_Dojo_Form_Element_FilteringSelect('IdSemester');
   		$IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->setAttrib('required',"true") ;
        $IdSemester->removeDecorator("Label");
        $IdSemester->removeDecorator('HtmlTag');
        $IdSemester->setRegisterInArrayValidator(false);
		$IdSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$VerifyPayment  = new Zend_Form_Element_Checkbox('VerifyPayment');
        $VerifyPayment->setAttrib('dojoType',"dijit.form.CheckBox");
        //$Active->setvalue('1');
        $VerifyPayment->removeDecorator("DtDdWrapper");
        $VerifyPayment->removeDecorator("Label");
        $VerifyPayment->removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         				
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
						
		$this->addElements(array($IdVerifyPayments,
								  $IdApplication,
        						  $UpdDate,
        						  $UpdUser,
        						  $IdSemester,
        						  $Save,
        						  $VerifyPayment,
        						  $Clear
                                 ));

    }
}