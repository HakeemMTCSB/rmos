<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/3/2016
 * Time: 3:06 PM
 */
class Studentfinance_Form_BarredListing extends Zend_Dojo_Form {

    public function init(){

        //semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->removeDecorator("Label");

        $semester->addMultiOption('', '-- Select --');

        $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
        $semesterList = $semesterDB->fnGetSemesterList();

        if ($semesterList) {
            foreach ($semesterList as $semesterLoop) {
                $semester->addMultiOption($semesterLoop["key"], $semesterLoop["SemesterMainCode"] . ' - ' . $semesterLoop["value"]);
            }
        }

        //programme
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programDb = new Registration_Model_DbTable_Program();
        $programList = $programDb->getData();

        if ($programList) {
            foreach ($programList as $programList) {
                $program->addMultiOption($programList["IdProgram"], $programList["ProgramCode"] . ' - ' . $programList["ProgramName"]);
            }
        }

        //release type
        $type = new Zend_Form_Element_Select('type');
        $type->removeDecorator("DtDdWrapper");
        $type->setAttrib('class', 'select');
        $type->removeDecorator("Label");

        $type->addMultiOption('', '-- Select --');

        $defModel = new App_Model_General_DbTable_Definationms();
        $typeList = $defModel->getDataByType(165);

        if ($typeList){
            foreach ($typeList as $typeLoop){
                $type->addMultiOption($typeLoop['idDefinition'], $typeLoop['DefinitionDesc']);
            }
        }

        //user
        $role = new Zend_Form_Element_Select('role');
        $role->removeDecorator("DtDdWrapper");
        $role->setAttrib('class', 'select');
        $role->removeDecorator("Label");

        $role->addMultiOption('', '-- Select --');

        $fotModel = new Studentfinance_Model_DbTable_FinanceOutstandingTemp();
        $roleList = $fotModel->getRoleBarRoleList();

        if ($roleList){
            foreach ($roleList as $roleList){
                $role->addMultiOption($roleList['brole_role'], $roleList['DefinitionDesc']);
            }
        }

        //id
        $id = new Zend_Form_Element_Text('id');
        $id->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //form elements
        $this->addElements(array(
            $semester,
            $program,
            $type,
            $role,
            $id
        ));
    }
}