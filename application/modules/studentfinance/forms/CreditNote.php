<?php

class Studentfinance_Form_CreditNote extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_sd');
		
		$this->addElement('text','applicant', 
			array(
				'label'=>'Applicant / Student Name',
				'disabled'=>'disabled'
			)
		);
		
		$this->addElement('text','payee_label', 
			array(
				'label'=>'Applicant / Student ID',
				'disabled'=>'disabled'
			)
		);
		
		$this->addElement('hidden','payee'
		);
		
		$this->addElement('hidden','appl_id'
		);
		
		$this->addElement('text','cn_billing_no_label', 
			array(
				'label'=>'Billing No',
				'disabled'=>'disabled'
			)
		);
		
		$cn_billing_no = new Zend_Form_Element_Hidden('cn_billing_no');
		$this->addElement($cn_billing_no);
		
		$this->addElement('text','cn_amount', 
			array(
				'label'=>'Amount',
				'required'=>'true'
			)
		);
		
		$this->addElement('textarea','cn_description', 
			array(
				'label'=>'Description',
				'required'=>'true'
			)
		);
		
		
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'credit-note','action'=>'entry'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>