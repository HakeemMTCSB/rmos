<?php
class Studentfinance_Form_Itemgroup extends Zend_Dojo_Form { //Formclass for the ItemGroup module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	        
    	$UpdDate = 	new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
     
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $IdItem = new Zend_Form_Element_Hidden('IdItem');
        $IdItem->removeDecorator("DtDdWrapper");
        $IdItem->removeDecorator("Label");
        $IdItem->removeDecorator('HtmlTag');      

        $ItemCode = new Zend_Form_Element_Text('ItemCode');
        $ItemCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ItemCode->removeDecorator("DtDdWrapper");
        $ItemCode->removeDecorator("Label");
        $ItemCode->removeDecorator('HtmlTag');
        $ItemCode->setAttrib('required',"true"); 
        $ItemCode->setAttrib('propercase',"true");   
        
        $EnglishDescription = new Zend_Form_Element_Text('EnglishDescription');
        $EnglishDescription->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $EnglishDescription->removeDecorator("DtDdWrapper");
        $EnglishDescription->removeDecorator("Label");
        $EnglishDescription->removeDecorator('HtmlTag');
        $EnglishDescription->setAttrib('required',"true"); 
        
        $BahasaDescription = new Zend_Form_Element_Text('BahasaDescription');
        $BahasaDescription->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $BahasaDescription->removeDecorator("DtDdWrapper");
        $BahasaDescription->removeDecorator("Label");
        $BahasaDescription->removeDecorator('HtmlTag');
        $BahasaDescription->setAttrib('required',"true");  
        
        $Alias = new Zend_Form_Element_Text('Alias');
        $Alias->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Alias->removeDecorator("DtDdWrapper");
        $Alias->removeDecorator("Label");
        $Alias->removeDecorator('HtmlTag');
        $Alias->setAttrib('required',"true");
        
        $ShortName = new Zend_Form_Element_Text('ShortName');
        $ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ShortName->removeDecorator("DtDdWrapper");
        $ShortName->removeDecorator("Label");
        $ShortName->removeDecorator('HtmlTag');
        $ShortName->setAttrib('required',"true"); 
        $ShortName->setAttrib('propercase',"true");   
         
        $IdBank = new Zend_Dojo_Form_Element_FilteringSelect('IdBank');   
        $IdBank->setAttrib('required','false')       		        
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label") 				
				->removeDecorator('HtmlTag')		
				->setRegisterInArrayValidator(false)				
        		-> setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdBank->setAttrib('OnChange', 'fnGetAccountType(this)');
        		
        $IdAccount = new Zend_Dojo_Form_Element_FilteringSelect('IdAccount');   
        $IdAccount->setAttrib('required','false')       		        
			->removeDecorator("DtDdWrapper")
				->removeDecorator("Label") 				
				->removeDecorator('HtmlTag')		
				->setRegisterInArrayValidator(false)				
        		-> setAttrib('dojoType',"dijit.form.FilteringSelect");
        		
        $AccountGroup = new Zend_Dojo_Form_Element_FilteringSelect('IdAccountGroup');   
        $AccountGroup->setAttrib('required',"true")       		        
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label") 				
				->removeDecorator('HtmlTag')		
				->setRegisterInArrayValidator(false)				
        		-> setAttrib('dojoType',"dijit.form.FilteringSelect");
        //$AccountGroup->setAttrib('OnChange', 'fnGetAccountGroup(this)');

        $Active = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag')
        		->setValue("1");		

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->setAttrib('class','NormalBtn');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('id', 'submitbutton');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        
        $Clear = new Zend_Form_Element_Button('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Search = new Zend_Form_Element_Button('Search');
		$Search->dojotype="dijit.form.Button";
       	$Search->label = $gstrtranslate->_("Search");
		$Search->setAttrib('class', 'NormalBtn')				
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Back = new Zend_Form_Element_Button('Back');
		$Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Back");
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
         
   		$this->addElements(array($UpdDate,
   								 $UpdUser,
   								 $IdItem,
   								 $ShortName,
   								 $ItemCode,
   								 $EnglishDescription,
   								 $BahasaDescription,
   								 $Alias,
   								 $IdBank,
   								 $IdAccount,
   								 $AccountGroup,
   								 $Active,
   								 $Save,
   								 $Clear,
   								 $Search,
   								 $Add,
   								 $Back));
    }
}