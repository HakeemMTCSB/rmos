<?php 
class Studentfinance_Form_AddScholarship extends Zend_Dojo_Form{
	
    public function init()
    {
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

        $this->setMethod('post');
		$this->setAttrib('sch_Id', 'add_scholarship_form');
		
		$sch_code = new Zend_Form_Element_Text('sch_code');
		$sch_code->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$sch_code->removeDecorator("DtDdWrapper");
		$sch_code->removeDecorator("Label");
		$sch_code->removeDecorator('HtmlTag');
		$sch_code->setAttrib('required',"true")->setAttrib("propercase", "true");

		$sch_name = new Zend_Form_Element_Text('sch_name');
		$sch_name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$sch_name->removeDecorator("DtDdWrapper");
		$sch_name->removeDecorator("Label");
		$sch_name->removeDecorator('HtmlTag');
		$sch_name->setAttrib('required',"true")->setAttrib("propercase", "true");

		$sch_malayName = new Zend_Form_Element_Text('sch_malayName');
		$sch_malayName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$sch_malayName->removeDecorator("DtDdWrapper");
		$sch_malayName->removeDecorator("Label");
		$sch_malayName->removeDecorator('HtmlTag');
		$sch_malayName->setAttrib('required',"true")->setAttrib("propercase", "true");

		$sch_desc = new Zend_Form_Element_Textarea('sch_desc');
		$sch_desc->setAttrib('dojoType',"dijit.form.SimpleTextarea");
		$sch_desc->setAttrib('class','textarea');
		$sch_desc->removeDecorator("DtDdWrapper");
		$sch_desc->removeDecorator("Label");
		$sch_desc->removeDecorator('HtmlTag');

		$sch_frequency = new Zend_Form_Element_Text('sch_frequency');
		$sch_frequency->setAttrib('dojoType',"dijit.form.NumberTextBox")
		->setAttrib('invalidMessage', 'Only digits')
		->setAttrib('maxlength','50')
		->setAttrib('constraints',"{min:0,max:100,pattern:'#'}")
		->setAttrib('required',"false")
		->setAttrib('class', 'txt_put')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$sch_amount = new Zend_Form_Element_Text('sch_amount');
		$sch_amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$sch_amount->removeDecorator("DtDdWrapper");
		$sch_amount->removeDecorator("Label");
		$sch_amount->removeDecorator('HtmlTag');
		$sch_amount->setAttrib('required',"true");

        
		$save = new Zend_Form_Element_Submit('save');
		$save->label = $gstrtranslate->_("Save");
		$save->dojotype="dijit.form.Button";
		$save->removeDecorator("DtDdWrapper");
		$save->removeDecorator('HtmlTag');

		


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$FeeCode = new Zend_Form_Element_Select('FeeCode');
		$FeeCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$FeeCode->removeDecorator("DtDdWrapper");
		$FeeCode->setAttrib('required',"false");
		$FeeCode->removeDecorator("Label");
		$FeeCode->removeDecorator('HtmlTag');
		$FeeCode->setAttrib('dojoType',"dijit.form.FilteringSelect");

		//Fee Code
		$lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
		$larrfeecode = $lobjPolicySetup->getfeecode();
		foreach($larrfeecode as $larrvalues) {
			$FeeCode->addMultiOption($larrvalues['IdFeeSetup'],$larrvalues['FeeCode']);
		}
		
		$CalculationMode = new Zend_Form_Element_Select('CalculationMode');
		$CalculationMode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CalculationMode->removeDecorator("DtDdWrapper");
		$CalculationMode->setAttrib('required',"false");
		$CalculationMode->removeDecorator("Label");
		$CalculationMode->removeDecorator('HtmlTag');
		$CalculationMode->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$CalculationMode->addMultiOptions(array('1'=>'Amount','2'=>'Percentage'));

		$FreqMode = new Zend_Form_Element_Select('FreqMode');
		$FreqMode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$FreqMode->removeDecorator("DtDdWrapper");
		$FreqMode->setAttrib('required',"false");
		$FreqMode->removeDecorator("Label");
		$FreqMode->removeDecorator('HtmlTag');
		$FreqMode->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$FreqMode->addMultiOptions(array('1'=>'One Time','2'=>'Semester'));

		$MaxRepeat = new Zend_Form_Element_Text('MaxRepeat');
		$MaxRepeat->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$MaxRepeat->removeDecorator("DtDdWrapper");
		$MaxRepeat->removeDecorator("Label");
		$MaxRepeat->removeDecorator('HtmlTag');

		$Repeat =  new Zend_Form_Element_Checkbox('Repeat');
		$Repeat->setAttrib('dojoType',"dijit.form.CheckBox");
		$Repeat->removeDecorator("DtDdWrapper");
		$Repeat->removeDecorator("Label");
                $Repeat->setCheckedValue(1);  
                $Repeat->setUncheckedValue(0);
		$Repeat->removeDecorator('HtmlTag');

		$Amount = new Zend_Form_Element_Text('Amount');
		$Amount->setAttrib('dojoType',"dijit.form.NumberTextBox")
		->setAttrib('invalidMessage', 'Only digits')
		->setAttrib('maxlength','50')
		->setAttrib('constraints',"{min:0,max:20000,pattern:'#'}")
		->setAttrib('required',"false")
		->setAttrib('class', 'txt_put')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$this->addElements(array(
							$sch_code, $sch_name, $sch_malayName, $sch_desc, $sch_amount,  $save,
							$Add, $FeeCode, $CalculationMode, $Amount,
							$FreqMode, $MaxRepeat, $Repeat
						));

	}
}
?>