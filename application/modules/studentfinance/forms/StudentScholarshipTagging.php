<?php 
class Studentfinance_Form_StudentScholarshipTagging extends Zend_Form
{

	public function init()
	{
		$this->setAttrib('onsubmit','return checkForm()');
		$this->setAttrib('id','form_scholarship');
                $s_type = $this->getAttrib('s_type');
                
                //type
                $stype = new Zend_Form_Element_Select('stype');
                $stype->removeDecorator("DtDdWrapper");
                $stype->setAttrib('class', 'select');
                $stype->removeDecorator("Label");
                $stype->setAttrib('onchange', 'getScholarship(this.value);');

                $stype->addMultiOption('1', 'Scholarship');
                $stype->addMultiOption('2', 'Sponsorship');
                $stype->addMultiOption('3', 'Financial Aid');

		//Scholarship Type
		$this->addElement('select','scholarship_type', array(
				'label'=>'Scholarship Type',
				'required'=>true,
				'class'=>'reqfield select'
		));
		
		$scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
		$scholarshipList = $scholarshipModel->getScholarshipList();
		$this->scholarship_type->addMultiOption(null,"-- Select --");
                
                if ($s_type){
                    switch ($s_type){
                        case 1: //scholarship
                            $list = $scholarshipModel->getScholarship();
                            break;
                        case 2: //sponsorship
                            $list = $scholarshipModel->getSponsorship();
                            break;
                        case 3: //financial aid
                            $list = $scholarshipModel->getFinancialAid();
                            break;
                    }
                    
                    if ($list){
                        foreach ($list as $loop){
                            $this->scholarship_type->addMultiOption($loop['id'], $loop['name']);
                        }
                    }
                }else{
                    foreach ( $scholarshipList as $srow )
                    {
                            $this->scholarship_type->addMultiOption($srow['sch_Id'], $srow['sch_name']);
                    }
                }

		$this->scholarship_type->removeDecorator("DtDdWrapper");
		$this->scholarship_type->removeDecorator("Label");
		$this->scholarship_type->removeDecorator('HtmlTag');

		//Program   
		$this->addElement('select','program', array(
				'label'=>'Programme',
				'class'=>'select'
		));
					
		$this->program->addMultiOption(null,"-- Select --");		
		
		$get_program_list = new Application_Model_DbTable_ProgramScheme();
		$program_list = $get_program_list->fnGetProgramList();
			
		if(count($program_list)>0)
		{
			foreach ($program_list as $list){		
				$this->program->addMultiOption($list['IdProgram'],$list['ProgramName']);
			}
		}

		$this->program->removeDecorator("DtDdWrapper");
		$this->program->removeDecorator("Label");
		$this->program->removeDecorator('HtmlTag');

		//Application Type
		$application_type = new Zend_Form_Element_Radio('application_type');
		$application_type->addMultiOptions(array(
			0 => 'New Application',
			1 => 'Re-Application'
		  ))->setSeparator(' ')
		  ->setLabel('Application Type')
		  ->setValue(0)
		  ->setRequired(true);


		$StartDate  = new Zend_Form_Element_Text('startDate');
		$StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$StartDate->setAttrib('onChange',"dijit.byId('endDate').constraints.min = arguments[0];") ;
		$StartDate->setAttrib('required',"true");
		$StartDate->removeDecorator("DtDdWrapper");
		$StartDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$StartDate->setLabel('Start Date');
		$StartDate->setRequired(true);
		
		$EndDate  = new Zend_Form_Element_Text('endDate');
		$EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$EndDate->setAttrib('onChange',"dijit.byId('startDate').constraints.max = arguments[0];") ;
		$EndDate->setAttrib('required',"true");
		$EndDate->removeDecorator("DtDdWrapper");
		$EndDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$EndDate->setLabel('End Date');
		$EndDate->setRequired(true);
                
                $CourseStartDate  = new Zend_Form_Element_Text('course_start_date');
		$CourseStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$CourseStartDate->setAttrib('onChange',"dijit.byId('course_end_date').constraints.min = arguments[0];") ;
		$CourseStartDate->setAttrib('required',"true");
		$CourseStartDate->removeDecorator("DtDdWrapper");
		$CourseStartDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$CourseStartDate->setLabel('Course Selection Start Date');
		$CourseStartDate->setRequired(true);
                
                $CourseEndDate  = new Zend_Form_Element_Text('course_end_date');
		$CourseEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$CourseEndDate->setAttrib('onChange',"dijit.byId('course_start_date').constraints.max = arguments[0];") ;
		$CourseEndDate->setAttrib('required',"true");
		$CourseEndDate->removeDecorator("DtDdWrapper");
		$CourseEndDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$CourseEndDate->setLabel('Course Selection End Date');
		$CourseEndDate->setRequired(true);
		
		$this->addElements(array($stype, $application_type, $StartDate, $EndDate, $CourseStartDate, $CourseEndDate));

		$this->application_type->removeDecorator("DtDdWrapper");
		$this->application_type->removeDecorator("Label");
		$this->application_type->removeDecorator('HtmlTag');

		$this->startDate->removeDecorator("DtDdWrapper");
		$this->startDate->removeDecorator("Label");
		$this->startDate->removeDecorator('HtmlTag');

		
		$this->endDate->removeDecorator("DtDdWrapper");
		$this->endDate->removeDecorator("Label");
		$this->endDate->removeDecorator('HtmlTag');
                
                $this->course_start_date->removeDecorator("DtDdWrapper");
		$this->course_start_date->removeDecorator("Label");
		$this->course_start_date->removeDecorator('HtmlTag');
                
                $this->course_end_date->removeDecorator("DtDdWrapper");
		$this->course_end_date->removeDecorator("Label");
		$this->course_end_date->removeDecorator('HtmlTag');


		//button
		$this->addElement('submit', 'save', array(
			  'label'=>'Save',
			  'decorators'=>array('ViewHelper'),
			  //'onClick'=>'submitSch()'
			));
			
			$this->addElement('submit', 'cancel', array(
			  'label'=>'Cancel',
			  'decorators'=>array('ViewHelper'),
			  'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'publishingsetup'),'default',true) . "#tab-2'; return false;"
			));
			
			$this->addDisplayGroup(array('save','cancel'),'buttons', array(
			  'decorators'=>array(
				'FormElements',
				array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
				'DtDdWrapper'
			  )
		));
		
    }
}
?>