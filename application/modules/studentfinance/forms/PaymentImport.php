<?php

class Studentfinance_Form_PaymentImport extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_pi');
		
		
		$element = new ZendX_JQuery_Form_Element_DatePicker('payment_date',
			array(
				'jQueryParams' => array('dateFormat' => 'dd-mm-yy'),
				'label'=>'Payment Date',
				'required'=>'true'
			)
		);
		$this->addElement($element);
		
		$this->addElement('text','amount', 
			array(
				'label'=>'Amount',
				'required'=>'true'
			)
		);
		
		$this->amount->addValidator('Regex',false, array('pattern' =>'/^\$?[0-9]+(,[0-9]{3})*(.[0-9]{2})?$/','messages'=>'Please fill in the decimal amount only (eg: 8.88)'));
		
		$this->addElement('select', 'payment_mode',
			array(
				'label'=>'Payment Mode',
				'required'=>'true'
			)
		);
		
		$this->payment_mode->addMultiOptions(array(
				"SPC-BNI" => 'SPC-BNI',
				"SLIP-BNI" => 'SLIP-BNI',
				"ATM-BNI" => 'ATM-BNI'
				
		));
		
		$this->addElement('text','bank_reference', 
			array(
				'label'=>'Bank Reference',
				'required'=>'true'
			)
		);
		
		$this->addElement('textarea','payment_description', 
			array(
				'label'=>'Description',
				'required'=>'true'
			)
		);
		
		
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'credit-note','action'=>'entry'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>