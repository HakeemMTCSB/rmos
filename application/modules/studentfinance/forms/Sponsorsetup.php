<?php

class Studentfinance_Form_Sponsorsetup extends Zend_Dojo_Form
{
	private $lobjPolicySetup;
	public function init()
	{

		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		$this->lobjPolicySetup = new Studentfinance_Model_DbTable_Policysetup();
		//$this->lobjSponsorSetup = new Studentfinance_Model_DbTable_Sponsor();


		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$IdUniversity = new Zend_Form_Element_Hidden('IdUniversity');
		$IdUniversity->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$SponsorId = new Zend_Form_Element_Text('SponsorId');
		$SponsorId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$SponsorId->removeDecorator("DtDdWrapper");
		$SponsorId->removeDecorator("Label");
		$SponsorId->removeDecorator('HtmlTag');
		$SponsorId->setAttrib('required',"true");


		$Sponsor = new Zend_Form_Element_Select('Sponsor');
		$Sponsor->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Sponsor->removeDecorator("DtDdWrapper");
		$Sponsor->setAttrib('required',"true");
		$Sponsor->removeDecorator("Label");
		$Sponsor->removeDecorator('HtmlTag');
		$Sponsor->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$StudentId = new Zend_Form_Element_Text('StudentId');
		$StudentId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudentId->removeDecorator("DtDdWrapper");
		$StudentId->removeDecorator("Label");
		$StudentId->removeDecorator('HtmlTag');
		$StudentId->setAttrib('required',"false");

		$StudentName = new Zend_Form_Element_Text('idstudentname');
		$StudentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudentName->removeDecorator("DtDdWrapper");
		$StudentName->removeDecorator("Label");
		$StudentName->removeDecorator('HtmlTag');
		$StudentName->setAttrib('required',"true");
		
		$ApplicantId = new Zend_Form_Element_Hidden('ApplicantId');
		$ApplicantId->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$ApplicantName = new Zend_Form_Element_Text('idapplicantname');
		$ApplicantName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ApplicantName->removeDecorator("DtDdWrapper");
		$ApplicantName->removeDecorator("Label");
		$ApplicantName->removeDecorator('HtmlTag');
		$ApplicantName->setAttrib('required',"true");

		$Name = new Zend_Form_Element_Text('Name');
		$Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Name->removeDecorator("DtDdWrapper");
		$Name->removeDecorator("Label");
		$Name->removeDecorator('HtmlTag');
		$Name->setAttrib('required',"true")->setAttrib("propercase", "true");

		$FirstName = new Zend_Form_Element_Text('FirstName');
		$FirstName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$FirstName->removeDecorator("DtDdWrapper");
		$FirstName->removeDecorator("Label");
		$FirstName->removeDecorator('HtmlTag');
		$FirstName->setAttrib('required',"true")->setAttrib("propercase", "true");


		$LastName = new Zend_Form_Element_Text('LastName');
		$LastName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$LastName->removeDecorator("DtDdWrapper");
		$LastName->removeDecorator("Label");
		$LastName->removeDecorator('HtmlTag');
		$LastName->setAttrib('required',"false")->setAttrib("propercase", "true");

		$Address1 = new Zend_Form_Element_Text('Address1');
		$Address1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Address1->removeDecorator("DtDdWrapper");
		$Address1->removeDecorator("Label");
		$Address1->removeDecorator('HtmlTag');
		$Address1->setAttrib('required',"false")->setAttrib("propercase", "true");

		$Address2 = new Zend_Form_Element_Text('Address2');
		$Address2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Address2->removeDecorator("DtDdWrapper");
		$Address2->removeDecorator("Label");
		$Address2->removeDecorator('HtmlTag');
		$Address2->setAttrib('required',"false")->setAttrib("propercase", "true");


		$Country = new Zend_Form_Element_Select('Country');
		$Country->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Country->removeDecorator("DtDdWrapper");
		$Country->setAttrib('required',"false");
		$Country->removeDecorator("Label");
		$Country->removeDecorator('HtmlTag');
		$Country->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$State = new Zend_Form_Element_Select('State');
		$State->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$State->removeDecorator("DtDdWrapper");
		$State->setAttrib('required',"false");
		$State->removeDecorator("Label");
		$State->removeDecorator('HtmlTag');
		$State->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$City = new Zend_Form_Element_Select('City');
		$City->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$City->removeDecorator("DtDdWrapper");
		$City->setAttrib('required',"false");
		$City->removeDecorator("Label");
		$City->removeDecorator('HtmlTag');
		$City->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$Postal = new Zend_Form_Element_Text('Postal', array (
				'regExp' => "[0-9]+",
				'invalidMessage' => "Only digits"
		));
		$Postal->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		//$Postal->setAttrib('invalidMessage', 'Only digits');
		$Postal->setAttrib('maxlength', '20');
		//$Postal->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Postal->removeDecorator("DtDdWrapper");
		$Postal->removeDecorator("Label");
		$Postal->removeDecorator('HtmlTag');
		
		$Phone = new Zend_Form_Element_Text('Phone', array ('regExp'=>'[\+0-9]+'));
		$Phone->setAttrib('maxlength', '20')
			->removeDecorator("DtDdWrapper")
			->setAttrib('dojoType', "dijit.form.ValidationTextBox")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag');

		$Fax = new Zend_Form_Element_Text('Fax', array ('regExp'=>'[\+0-9]+'));
		$Fax->setAttrib('maxlength', '20')
			->removeDecorator("DtDdWrapper")
			->setAttrib('dojoType', "dijit.form.ValidationTextBox")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag');

		$EmailAddress = new Zend_Form_Element_Text('EmailAddress', array (
				'regExp' => "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",
				'invalidMessage' => "Not a valid email"
		));
		$EmailAddress->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$EmailAddress->setAttrib('maxlength', '50')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');


		$FeeCode = new Zend_Form_Element_Select('FeeCode');
		$FeeCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$FeeCode->removeDecorator("DtDdWrapper");
		$FeeCode->setAttrib('required',"false");
		$FeeCode->removeDecorator("Label");
		$FeeCode->removeDecorator('HtmlTag');
		$FeeCode->setAttrib('dojoType',"dijit.form.FilteringSelect");

		//Fee Code
		$larrfeecode = $this->lobjPolicySetup->getfeecode();
		foreach($larrfeecode as $larrvalues) {
			$FeeCode->addMultiOption($larrvalues['IdFeeSetup'],$larrvalues['FeeCode']);
		}

		$Description = new Zend_Form_Element_Text('Description');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Description->removeDecorator("DtDdWrapper");
		$Description->removeDecorator("Label");
		$Description->removeDecorator('HtmlTag');
		$Description->setAttrib('required',"false");

		$CalculationMode = new Zend_Form_Element_Select('CalculationMode');
		$CalculationMode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CalculationMode->removeDecorator("DtDdWrapper");
		$CalculationMode->setAttrib('required',"false");
		$CalculationMode->removeDecorator("Label");
		$CalculationMode->removeDecorator('HtmlTag');
		$CalculationMode->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$CalculationMode->addMultiOptions(array('1'=>'Amount',
				'2'=>'Percentage'));


		$FreqMode = new Zend_Form_Element_Select('FreqMode');
		$FreqMode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$FreqMode->removeDecorator("DtDdWrapper");
		$FreqMode->setAttrib('required',"false");
		$FreqMode->removeDecorator("Label");
		$FreqMode->removeDecorator('HtmlTag');
		$FreqMode->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$FreqMode->addMultiOptions(array('1'=>'One Time','2'=>'Semester'));

		$MaxRepeat = new Zend_Form_Element_Text('MaxRepeat');
		$MaxRepeat->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$MaxRepeat->removeDecorator("DtDdWrapper");
		$MaxRepeat->removeDecorator("Label");
		$MaxRepeat->removeDecorator('HtmlTag');

		$Repeat =  new Zend_Form_Element_Checkbox('Repeat');
		$Repeat -> setAttrib('dojoType',"dijit.form.CheckBox");
		$Repeat -> removeDecorator("DtDdWrapper");
		$Repeat	-> removeDecorator("Label");
                $Repeat->setCheckedValue(1);  
                $Repeat->setUncheckedValue(0);
		$Repeat	-> removeDecorator('HtmlTag');


		$AggrementStatus = new Zend_Form_Element_Select('AggrementStatus');
		$AggrementStatus->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$AggrementStatus->removeDecorator("DtDdWrapper");
		$AggrementStatus->setAttrib('required',"false");
		$AggrementStatus->removeDecorator("Label");
		$AggrementStatus->removeDecorator('HtmlTag');
		$AggrementStatus->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$AggrementStatus->addMultiOptions(array('1'=>'With Agree',
				'2'=>'Without Agree'));


		$AggrementNo = new Zend_Form_Element_Text('AggrementNo');
		$AggrementNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$AggrementNo->removeDecorator("DtDdWrapper");
		$AggrementNo->removeDecorator("Label");
		$AggrementNo->removeDecorator('HtmlTag');
		$AggrementNo->setAttrib('required',"false");

		$dateformat = "{datePattern:'dd-MM-yyyy'}";
		$StartDate  = new Zend_Form_Element_Text('StartDate');
		$StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$StartDate->setAttrib('onChange',"dijit.byId('EndDate').constraints.min = arguments[0];") ;
		$StartDate->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
		$StartDate->removeDecorator("DtDdWrapper");
		$StartDate->removeDecorator("Label");
		$StartDate->removeDecorator('HtmlTag');


		$EndDate  = new Zend_Form_Element_Text('EndDate');
		$EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$EndDate->setAttrib('onChange',"dijit.byId('StartDate').constraints.max = arguments[0];") ;
		$EndDate->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
		$EndDate->removeDecorator("DtDdWrapper");
		$EndDate->removeDecorator("Label");
		$EndDate->removeDecorator('HtmlTag');

		$Amount = new Zend_Form_Element_Text('Amount');
		$Amount->setAttrib('dojoType',"dijit.form.NumberTextBox")
		->setAttrib('invalidMessage', 'Only digits')
		->setAttrib('maxlength','50')
		->setAttrib('constraints',"{min:0,pattern:'#'}")
		->setAttrib('required',"false")
		->setAttrib('class', 'txt_put')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$AmountByProgram = new Zend_Form_Element_Text('AmountByProgram');
		$AmountByProgram->setAttrib('dojoType',"dijit.form.NumberTextBox")
		->setAttrib('invalidMessage', 'Only digits')
		->setAttrib('maxlength','50')
		->setAttrib('constraints',"{min:0,max:10000,pattern:'#'}")
		->setAttrib('required',"false")
		->setAttrib('class', 'txt_put')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Add = new Zend_Form_Element_Button('Add');
		$Add->label = $gstrtranslate->_("Add");
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$AddByFeeInfo = new Zend_Form_Element_Button('$AddByFeeInfo');
		$AddByFeeInfo->label = $gstrtranslate->_("Add");
		$AddByFeeInfo->dojotype="dijit.form.Button";
		$AddByFeeInfo->removeDecorator("DtDdWrapper");
		$AddByFeeInfo->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$Clear = new Zend_Form_Element_Button('Clear');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$ClearByFeeInfo = new Zend_Form_Element_Button('$ClearByFeeInfo');
		$ClearByFeeInfo->dojotype="dijit.form.Button";
		$ClearByFeeInfo->label = $gstrtranslate->_("Clear");
		$ClearByFeeInfo->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$Search = new Zend_Form_Element_Submit('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag')
		->class = "NormalBtn";
			
		$SearchButton = new Zend_Form_Element_Button('SearchButton');
		$SearchButton->dojotype="dijit.form.Button";
		$SearchButton->label = $gstrtranslate->_("Search");
		$SearchButton->removeDecorator("DtDdWrapper");
		$SearchButton->removeDecorator("Label");
		$SearchButton->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$FeeItem = new Zend_Form_Element_Select('FeeItem');
		$FeeItem->removeDecorator("DtDdWrapper");
		$FeeItem->removeDecorator("Label");
		$FeeItem->removeDecorator('HtmlTag');
		$FeeItem->setAttrib('dojoType',"dijit.form.FilteringSelect");
		

		$CoordinatorName = new Zend_Form_Element_Text('CoordinatorName');
		$CoordinatorName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CoordinatorName->removeDecorator("DtDdWrapper");
		$CoordinatorName->removeDecorator("Label");
		$CoordinatorName->removeDecorator('HtmlTag');
		$CoordinatorName->setAttrib("propercase", "true");

		$CoordinatorEmail = new Zend_Form_Element_Text('CoordinatorEmail', array (
				'regExp' => "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",
				'invalidMessage' => "Not a valid email"
		));
		$CoordinatorEmail->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$CoordinatorEmail->setAttrib('maxlength', '60')->removeDecorator("DtDdWrapper")
			->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CoordinatorAdd = new Zend_Form_Element_Button('CoordinatorAdd');
		$CoordinatorAdd->label = $gstrtranslate->_("Add");
		$CoordinatorAdd->dojotype="dijit.form.Button";
		$CoordinatorAdd->removeDecorator("DtDdWrapper");
		$CoordinatorAdd->removeDecorator('HtmlTag');

		$CoordinatorPhone = new Zend_Form_Element_Text('CoordinatorPhone');
		$CoordinatorPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CoordinatorPhone->removeDecorator("DtDdWrapper");
		$CoordinatorPhone->removeDecorator("Label");
		$CoordinatorPhone->removeDecorator('HtmlTag');

		$CoordinatorFax = new Zend_Form_Element_Text('CoordinatorFax');
		$CoordinatorFax->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CoordinatorFax->removeDecorator("DtDdWrapper");
		$CoordinatorFax->removeDecorator("Label");
		$CoordinatorFax->removeDecorator('HtmlTag');
		
		$CoordinatorMobile = new Zend_Form_Element_Text('CoordinatorMobile');
		$CoordinatorMobile->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CoordinatorMobile->removeDecorator("DtDdWrapper");
		$CoordinatorMobile->removeDecorator("Label");
		$CoordinatorMobile->removeDecorator('HtmlTag');

		$CoordinatorClear = new Zend_Form_Element_Button('CoordinatorClear');
		$CoordinatorClear->dojotype="dijit.form.Button";
		$CoordinatorClear->label = $gstrtranslate->_("Clear");
		$CoordinatorClear->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		//Stud
		$StudAggrementNo = new Zend_Form_Element_Text('StudAggrementNo');
		$StudAggrementNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudAggrementNo->removeDecorator("DtDdWrapper");
		$StudAggrementNo->removeDecorator("Label");
		$StudAggrementNo->removeDecorator('HtmlTag');
		$StudAggrementNo->setAttrib('required',"false");

		$StudStartDate  = new Zend_Form_Element_Text('StudStartDate');
		$StudStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$StudStartDate->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
		$StudStartDate->removeDecorator("DtDdWrapper");
		$StudStartDate->removeDecorator("Label");
		$StudStartDate->removeDecorator('HtmlTag');


		$StudEndDate  = new Zend_Form_Element_Text('StudEndDate');
		$StudEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$StudEndDate->setAttrib('required',"false")->setAttrib('constraints', "$dateformat");
		$StudEndDate->removeDecorator("DtDdWrapper");
		$StudEndDate->removeDecorator("Label");
		$StudEndDate->removeDecorator('HtmlTag');

		$StudAmount = new Zend_Form_Element_Text('StudAmount');
		$StudAmount->setAttrib('dojoType',"dijit.form.NumberTextBox")
		->setAttrib('invalidMessage', 'Only digits')
		->setAttrib('maxlength','50')
		->setAttrib('constraints',"{min:0,pattern:'#'}")
		->setAttrib('required',"false")
		->setAttrib('class', 'txt_put')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		$StudFeeItem = new Zend_Form_Element_Select('StudFeeItem');
		$StudFeeItem->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudFeeItem->removeDecorator("DtDdWrapper");
		$StudFeeItem->removeDecorator("Label");
		$StudFeeItem->removeDecorator('HtmlTag');
		$StudFeeItem->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ScholarshipType = new Zend_Form_Element_Select('ScholarshipType');
		$ScholarshipType->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ScholarshipType->removeDecorator("DtDdWrapper");
		$ScholarshipType->removeDecorator("Label");
		$ScholarshipType->removeDecorator('HtmlTag');
		$ScholarshipType->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
		$scholarshipList = $scholarshipModel->getScholarshipList();
		foreach ( $scholarshipList as $row )
		{
			$ScholarshipType->addMultiOption($row['sch_Id'], $row['sch_name']);
		}

                //active leturer
                $active =  new Zend_Form_Element_Checkbox('active');
		$active -> setAttrib('dojoType',"dijit.form.CheckBox");
		$active -> removeDecorator("DtDdWrapper");
		$active	-> removeDecorator("Label");
                $active->setCheckedValue(1);  
                $active->setUncheckedValue(0);
		$active	-> removeDecorator('HtmlTag');

		$this->addElements(array($UpdUser,$UpdDate,$IdUniversity,$SponsorId,$Name,$Country,
				$State,$City,$Postal,$Phone,
				$Fax,$EmailAddress,
				$FeeCode,$Description,$Address1,$Address2,$CalculationMode,$Amount,
				$Save,$Add,$Clear,$Search,$FirstName,$LastName,$AggrementStatus,
				$Sponsor,$StudentId,$StudentName,$AggrementNo,$AmountByProgram,
				$StartDate,$EndDate,$ClearByFeeInfo,$AddByFeeInfo,$SearchButton,$FeeItem,
				
			$CoordinatorName,
			$CoordinatorEmail,
			$CoordinatorPhone,
			$CoordinatorMobile,
			$CoordinatorFax,
			$CoordinatorClear,
			$CoordinatorAdd,

			$StudAggrementNo,
			$StudStartDate,
			$StudEndDate,
			$StudAmount,
			$StudFeeItem,

			$ScholarshipType,
			$ApplicantName,
			$ApplicantId,

			$FreqMode, $MaxRepeat, $Repeat, $active

			));


	}
}