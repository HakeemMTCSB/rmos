<?php

class Studentfinance_Form_FeeStructure extends Zend_Form
{
	protected $_locale;
	
	protected $programid;
	
	public function setProgramid($programid){
		$this->programid = $programid;
	}
	
	
	public function init()
	{
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
		
		$this->setMethod('post');
		$this->setAttrib('id','form_fs');
		
		$this->addElement('hidden','fsp_program_id');
		$this->fsp_program_id->setValue($this->programid);
		
		$programDb = new GeneralSetup_Model_DbTable_Program();
		$prog_data =  $programDb->fetchRow(array('IdProgram = ?'=>$this->programid))->toArray();
				
		$this->addElement('text','fsp_program_name',
				array(
						'label'=>'Programme Name',
						'disabled'=>'disabled',
						'class' => 'input-txt'
				)
		);
		$this->fsp_program_name->setValue($prog_data['ProgramName']);
		
		//scheme
		$this->addElement('select','fsp_idProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),	
		    'required'=>true
		));
		
		$programSchemeDb = new GeneralSetup_Model_DbTable_Programscheme();
    	$programScheme = $programSchemeDb->getProgSchemeByProgram($this->programid);
		
		$this->fsp_idProgramScheme->addMultiOption(null,"-- Please Select --");		
		foreach($programScheme as $ps){
			$this->fsp_idProgramScheme->addMultiOption($ps["IdProgramScheme"],$ps["ProgramMode"].', '.$ps["StudyMode"].', '.$ps["ProgramType"]);
		}
		
		$this->addElement('text','fs_name', 
			array(
				'label'=>'Structure Name',
				'class'=>'input-txt',
				'required'=>'true',
			)
		);
		
		$this->addElement('textarea','fs_description', 
			array(
				'label'=>'Description'
			)
		);
		
		//intake start
		$this->addElement('select','fs_intake_start', 
			array(
				'label'=>'Intake',
				'required'=>'true'
			)
		);
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$listData = $intakeDb->fngetIntakeList();
		$this->fs_intake_start->addMultiOption(null,"Please Select");
		
		if($this->_locale == 'en_US'){
			foreach ($listData as $list){
				$this->fs_intake_start->addMultiOption($list['IdIntake'],$list['IntakeDesc']);
			}
		}else{
			foreach ($listData as $list){
				$this->fs_intake_start->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
			}
		}

		//Semester
		$this->addElement('select','fs_semester_start', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->fs_semester_start->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->fs_semester_start->addMultiOption($semester["key"],$semester["value"]);
		}
		
		//intake end
		/*$this->addElement('select','fs_intake_end', 
			array(
				'label'=>'End Intake'
			)
		);
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$listData = $intakeDb->fngetIntakeList();
		$this->fs_intake_end->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fs_intake_end->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}*/
		
		//efective date
		$this->addElement('text','fs_effective_date',
				array(
						'label'=>'Effective Date',
						'required'=>'true',
						'class'=>'datepicker input-txt',
						'placeholder' => 'YYYY-MM-DD'
				)
		);

		//category	
		$this->addElement('select','fs_student_category', 
			array(
				'label'=>'Student Category',
				'required'=>'true'
			)
		);
		$defDb = new App_Model_Definitiontype();
		$listData = $defDb->fnGetDefinations('Student Category');
		$this->fs_student_category->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fs_student_category->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}
		
		
		
		//account code
		$this->addElement('select','fs_ac_id',
				array(
						'label'=>'Debtor Account Code',
						'required'=>'true'
				)
		);
		$accountCodeDb = new Studentfinance_Model_DbTable_AccountCode();
		$listData = $accountCodeDb->getAccountCode(1,609);
		$this->fs_ac_id->addMultiOption(null,"Please Select");
		
		if($listData){
			foreach ($listData as $list){
				$this->fs_ac_id->addMultiOption($list['ac_id'],$list['ac_code'] ." (".$list['ac_desc'].")");
			}
		}
		
		//pre payment account code	
		$this->addElement('select','fs_prepayment_ac', 
			array(
				'label'=>'Pre Payment Account Code',
				'required'=>'true'
			)
		);
		$listData = $accountCodeDb->getAccountCode(1,609);
		$this->fs_prepayment_ac->addMultiOption(null,"Please Select");
		
		if($listData){
			foreach ($listData as $list){
				$this->fs_prepayment_ac->addMultiOption($list['ac_id'],$list['ac_code'] ." (".$list['ac_desc'].")");
			}
		}
		
		//currency
		$this->addElement('select','fs_cur_id',
				array(
						'label'=>'Currency of Estimated total fees',
						'required'=>'true'
				)
		);
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$listData = $currencyDb->fetchAll('cur_status = 1')->toArray();
		$this->fs_cur_id->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->fs_cur_id->addMultiOption($list['cur_id'],$list['cur_desc']);
		}
		
		//estimated total fee
		$this->addElement('text','fs_estimated_total_fee',
				array(
						'label'=>'Estimated Total Fees',
						'required'=>'true',
						'class'=>'input-txt'
				)
		);
		
		//first semester credit hour
		$this->addElement('text','fsp_first_sem_sks',
				array(
						'label'=>'First semester credit hour/ Total Subject',
						'required'=>false,
						'class'=>'input-txt'
				)
		);
		

		//cp
		$this->addElement('select','fs_cp',
				array(
						'label'=>'Collaborative Partner',
						'required'=>false
				)
		);
		
		$Branchofficevenue = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$branches = $Branchofficevenue->fngetBranchDetails(1);
		$this->fs_cp->addMultiOption(null, '-- Please Select --');
		foreach ( $branches as $branch )
		{
			$this->fs_cp->addMultiOption($branch['IdBranch'], $branch['BranchName']);
		}
		
		
		$this->addElement('select','fs_status', array(
			'label'=>'Status',
		'required'=>true));
			$this->fs_status->addMultiOption(1,'Active');
			$this->fs_status->addMultiOption(0,'Draft');
			$this->fs_status->addMultiOption(2,'Inactive');
			
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'fee-structure','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>