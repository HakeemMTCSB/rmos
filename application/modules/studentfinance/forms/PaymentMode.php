<?php

class Studentfinance_Form_PaymentMode extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_sd');
		
		$this->addElement('text','payment_mode', 
			array(
				'label'=>'Payment Mode',
				'required'=>'true',
			    'maxlength'=>'250'
			)
		);
		
		$this->addElement('text','description', 
			array(
				'label'=>'Description',
			    'maxlength'=>'250',
				'required'=>'true',
			)
		);
		
		$this->addElement('text','description_second_language',
				array(
						'label'=>'Description (Second language)',
						'maxlength'=>'250',
						'required'=>'true',
				)
		);
		
		
		//payment group
		$this->addElement('select','payment_group',
				array(
						'label'=>'Payment Group',
						'required'=>'true'
				)
		);
		
		
		$defDb = new App_Model_Definitiontype();
		$listData = $defDb->fnGetDefinations('Payment Mode Grouping');
		$this->payment_group->addMultiOption(null,"Please Select");
		
		foreach ($listData as $list){
			$this->payment_group->addMultiOption($list['idDefinition'],$list['Description']);
		}
		
		
		//status
		$this->addElement('checkbox','active',
				array(
						'label'=>'Active?',
				)
		);
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'general-setup', 'controller'=>'highschool-discipline','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>