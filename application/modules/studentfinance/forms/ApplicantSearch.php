<?php
class Studentfinance_Form_ApplicantSearch extends Zend_Form
{
		
	public function init()
	{
	

		$this->setMethod('post');
		
		$hiddenControl = new Zend_Form_Element_Hidden('search');
		$hiddenControl->setValue('1');
		$this->addElement($hiddenControl);

		$format = new Zend_Form_Element_Text('no_formulir');
        $format->setLabel('Nomor Pendaftar');
		$this->addElement($format);	

		$format = new Zend_Form_Element_Text('fullname');
        $format->setLabel($this->getView()->translate('Nama'));
		$this->addElement($format);			
		
		$format = new Zend_Form_Element_Text('email');
        $format->setLabel($this->getView()->translate('Email'))
        		->addFilter('StringToLower')
        		->addValidator('EmailAddress');
		$this->addElement($format);	

			
		$field2 = new Zend_Form_Element_Select('category');
		$field2->removeDecorator("DtDdWrapper");
		//$field2->setAttrib('required',"false");
        $field2->setAttrib('id', 'category');
        $field2->setAttrib('class', 'span-7');
		$field2->removeDecorator("Label");
        
        $field2->addMultiOption('', '--All--');
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        $IdProgram = 2;
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $field2->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }
        

				
		//button
		$this->addElement('submit', 'search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addDisplayGroup(array('search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
}
?>