<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 3/8/2016
 * Time: 11:18 AM
 */
class Studentfinance_Form_FeeStructureReport extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');
        $programId = $this->getAttrib('programId');

        $model = new Studentfinance_Model_DbTable_FeeStructureReport();

        //program
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getProgramScheme(this.value);');
        //$program->setAttrib('required', true);
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programList = $model->getProgram();

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramCode'].' - '.$programLoop['ProgramName']);
            }
        }

        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
        $programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        //$programscheme->setAttrib('required', true);
        $programscheme->removeDecorator("Label");

        $programscheme->addMultiOption('', '-- Select --');

        if ($programId){
            $programschemeList = $model->getProgramScheme($programId);

            if ($programschemeList){
                foreach ($programschemeList as $programschemeLoop){
                    $programscheme->addMultiOption($programschemeLoop['IdProgramScheme'], $programschemeLoop['mop'].' '.$programschemeLoop['mos'].' '.$programschemeLoop['pt']);
                }
            }
        }

        //intake
        $intake = new Zend_Form_Element_Select('intake');
        $intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
        //$intake->setAttrib('required', true);
        $intake->removeDecorator("Label");

        $intake->addMultiOption('', '-- Select --');

        $intakeList = $model->getIntake();

        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }

        //type
        $type = new Zend_Form_Element_Select('type');
        $type->removeDecorator("DtDdWrapper");
        $type->setAttrib('class', 'select');
        //$intake->setAttrib('required', true);
        $type->removeDecorator("Label");

        $type->addMultiOption('', '-- Select --');

        $typeList = $model->getDefination(95);

        if ($typeList){
            foreach ($typeList as $typeLoop){
                $type->addMultiOption($typeLoop['idDefinition'], $typeLoop['DefinitionDesc']);
            }
        }

        $this->addElements(array(
            $program,
            $programscheme,
            $intake,
            $type
        ));
    }
}