<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_Form_SponsorshipApplicationSearchForm extends Zend_Form {
    
    public function init(){
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');
        
        $model = new Studentfinance_Model_DbTable_Scholarship();
        $defModel = new App_Model_General_DbTable_Definationms();
        
        //program
        $Program = new Zend_Form_Element_Select('Program');
	$Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        //$Program->setAttrib('required', '');
	$Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getProgramScheme(this.value); getSemester(this.value);');
        
        $Program->addMultiOption('', '-- All --');
        
        $programList = $model->getProgramList();
        
        if ($programList){
            foreach ($programList as $programLoop){
                $Program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }
        
        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
	$ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
	$ProgramScheme->removeDecorator("Label");
        //$ProgramScheme->setAttrib('onchange', 'getProgramScheme(this.value);');
        
        $ProgramScheme->addMultiOption('', '-- All --');
        
        if ($programid && $programid!=''){
            $progSchemeList = $model->getProgSchemeBasedOnProg($programid);
            
            $i = 0;
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $progSchemeList[$i]['mos_name'] = $mod_of_study['DefinitionDesc'];
                $progSchemeList[$i]['mop_name'] = $mod_of_program['DefinitionDesc'];
                $progSchemeList[$i]['programType'] = $programType['DefinitionDesc'];

                $i++;
            }
            
            if ($progSchemeList){
                foreach ($progSchemeList as $progSchemeLoop){
                    $ProgramScheme->addMultiOption($progSchemeLoop['IdProgramScheme'], $progSchemeLoop['programType'].' '.$progSchemeLoop['mop_name'].' '.$progSchemeLoop['mos_name']);
                }
            }
        }
        
        //semester apply
        $SemesterApplied = new Zend_Form_Element_Select('SemesterApplied');
	$SemesterApplied->removeDecorator("DtDdWrapper");
        $SemesterApplied->setAttrib('class', 'select');
	$SemesterApplied->removeDecorator("Label");
        
        $SemesterApplied->addMultiOption('', '-- All --');
        
        if ($programid && $programid!=''){
            $programInfo = $model->getProgramById($programid);
            
            $semesterList = $model->getSemester($programInfo['IdScheme']);
            if ($semesterList){
                foreach ($semesterList as $semesterLoop){
                    $SemesterApplied->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
                }
            }
        }else{
            $semesterList = $model->getSemesterAll();
            if ($semesterList){
                foreach ($semesterList as $semesterLoop){
                    $SemesterApplied->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
                }
            }
        }
        
        //student name
        $StudentName = new Zend_Form_Element_Text('StudentName');
	$StudentName->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //student id
        $StudentID = new Zend_Form_Element_Text('StudentID');
	$StudentID->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //semester apply
        $Status = new Zend_Form_Element_Select('Status');
	$Status->removeDecorator("DtDdWrapper");
        $Status->setAttrib('class', 'select');
	$Status->removeDecorator("Label");
        
        $Status->addMultiOption('', '-- All --');
        $Status->addMultiOption(1, 'APPLIED');
        $Status->addMultiOption(2, 'OFFERED');
        $Status->addMultiOption(3, 'REJECTED');
        $Status->addMultiOption(4, 'COMPLETED');
        $Status->addMultiOption(5, 'INCOMPLETE');
        $Status->addMultiOption(6, 'CANCEL');
        
        $this->addElements(array(
            $Program,
            $ProgramScheme,
            $SemesterApplied,
            $StudentName,
            $StudentID,
            $Status
        ));
    }
}
?>