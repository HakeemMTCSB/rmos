<?php

class Studentfinance_Form_FeeItemAccount extends Zend_Form
{
	
	protected $_facultyid;
	protected $_programid;
	protected $_feeitemid;
	
	public function setFacultyid($value){
		$this->_facultyid = $value;
	}
	
	public function setProgramid($value){
		$this->_programid = $value;
	}
	
	public function setFeeitemid($value){
		$this->_feeitemid = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_fee_item_account');
		
		//fee item id
		$this->addElement('hidden','fiacc_fee_item', 
			array(
				'required'=>'true',
			)
		);
		$this->fiacc_fee_item->setValue($this->_feeitemid);
		
		//faculty
		$this->addElement('hidden','fiacc_faculty_id', 
			array(
				'required'=>'true',
			)
		);
		$this->fiacc_faculty_id->setValue($this->_facultyid);
		
		
		//program
		$this->addElement('hidden','fiacc_program_id', 
			array(
				'required'=>'true',
			)
		);
		$this->fiacc_program_id->setValue($this->_programid);
		
		//acc no
		$this->addElement('text','fiacc_account', 
			array(
				'label'=>'Account No.',
				'required'=>'true',
			)
		);
		
		//bank
		$this->addElement('select','fiacc_bank', array(
			'label'=>'Bank',
			'required'=>'true'	
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$this->fiacc_bank->addMultiOption(null,'Please Select');
		foreach ($definationDb->getDataByType(88) as $list){
			$this->fiacc_bank->addMultiOption($list['idDefinition'],$list['BahasaIndonesia']);
		}
		
		
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'general-setup', 'controller'=>'highschool-discipline','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>