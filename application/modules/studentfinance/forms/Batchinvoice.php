<?php
class Studentfinance_Form_Batchinvoice extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 	
       
        
    	$StudentName = new Zend_Dojo_Form_Element_FilteringSelect('StudentName');
    	$StudentName->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $StudentName->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
					
		$StudentId = new Zend_Form_Element_Text('StudentId');
		$StudentId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$StudentId->setAttrib('required',"false");
		$StudentId->setAttrib('readonly',true);
     	$StudentId->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
        		    
        $Program = new Zend_Dojo_Form_Element_FilteringSelect('Program');
    	$Program->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $Program->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
        		    
        $Faculty = new Zend_Dojo_Form_Element_FilteringSelect('Faculty');
    	$Faculty->setAttrib('dojoType',"dijit.form.FilteringSelect")
    			->setAttrib('OnChange', 'fnGetProgram(this.value)');
        $Faculty->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');  
        		    
        $Faculty1 = new Zend_Dojo_Form_Element_FilteringSelect('Faculty1');
    	$Faculty1->setAttrib('dojoType',"dijit.form.FilteringSelect")
    			->setAttrib('OnChange', 'fnGetProgram(this.value)');
        $Faculty1->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');  
        		    
        $Semester = new Zend_Dojo_Form_Element_FilteringSelect('Semester');
    	$Semester->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $Semester->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag'); 
        		     
        $Mode = new Zend_Dojo_Form_Element_FilteringSelect('Mode');
    	$Mode->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $Mode->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
					
        $idAccount 	 = new Zend_Dojo_Form_Element_FilteringSelect('idAccount');
        $idAccount->setAttrib('OnChange', 'fnGetAmount(this.value)');
		$idAccount->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $idAccount->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');
        		    
        		    
		$Amount = new Zend_Form_Element_Text('Amount',array('regExp'=>'[\0-9]+'));       
		$Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag')
        		->setAttrib('required',"true");
        		
        $Description = new Zend_Form_Element_Text('Description');       
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Description->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->setAttrib('class','NormalBtn');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('id', 'submitbutton');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
 
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
		    ->setAttrib('onClick','addBatchInvoiceDetails()')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				    
        //form elements
        $this->addElements(array($StudentName,$StudentId,$Program,$Faculty,$Semester,$Mode,$Search,$Add,$Save,$idAccount,$Amount,$Description,$Faculty1
                                 ));

    }
}