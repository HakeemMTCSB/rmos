<?php
class Studentfinance_Form_Chargemaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$joiningdate = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
    	
    	$IdCharges = new Zend_Form_Element_Hidden('IdCharges');
        $IdCharges->removeDecorator("DtDdWrapper");
        $IdCharges->removeDecorator("Label");
        $IdCharges->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
		
		$ChargeName = new Zend_Form_Element_Text('ChargeName');
		$ChargeName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ChargeName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','255')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        				
		$effectiveDate = new Zend_Form_Element_Text('effectiveDate');
		$effectiveDate->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$effectiveDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $effectiveDate->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','255')      
        		  ->setAttrib('constraints', "$joiningdate")	  
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        		
        $ChargeType = new Zend_Dojo_Form_Element_FilteringSelect('ChargeType');
   		//$ChargeType->addMultiOption('','Select');
        $ChargeType->removeDecorator("DtDdWrapper");
        $ChargeType->setAttrib('required',"true") ;
        $ChargeType->removeDecorator("Label");
        $ChargeType->removeDecorator('HtmlTag');
        $ChargeType->setRegisterInArrayValidator(false);
		$ChargeType->setAttrib('dojoType',"dijit.form.FilteringSelect");	

		$IdAccountMaster = new Zend_Dojo_Form_Element_FilteringSelect('IdAccountMaster');
   		//$ChargeType->addMultiOption('','Select');
        $IdAccountMaster->removeDecorator("DtDdWrapper");
        $IdAccountMaster->setAttrib('required',"true") ;
        $IdAccountMaster->removeDecorator("Label");
        $IdAccountMaster->removeDecorator('HtmlTag');
        $IdAccountMaster->setRegisterInArrayValidator(false);
		$IdAccountMaster->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdProgram = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
   		//$ChargeType->addMultiOption('','Select');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->setAttrib('required',"true") ;
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        $IdProgram->setRegisterInArrayValidator(false);
		$IdProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
		$Rate = new Zend_Form_Element_Text('Rate',array('regExp'=>"[0-9]*\.[0-9]+|[0-9]+",'invalidMessage'=>"Digits Only"));
		$Rate->setAttrib('required',"true");
		$Rate//->setAttrib('maxlength','50')
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")				
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');
						
		$Payment  = new Zend_Form_Element_Radio('Payment');
		$Payment->setAttrib('dojoType',"dijit.form.RadioButton");
        $Payment->addMultiOptions(array('0' => 'During Application','1' => 'During Registration'))
        			->setvalue('0')
        			->setSeparator('&nbsp;')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->setAttrib('onclick', 'fnToggleCollegeDetails(this.value)');	
		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         				
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
						
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');	
	
        //form elements
        $this->addElements(array($IdAccountMaster,
        						  $UpdDate,
        						  $UpdUser,
        						  $ChargeName,
        						  $ChargeType,
        						  $Rate,
        						  $Payment,
        						  $Save,
        						  $effectiveDate,
        						  $Clear,$IdProgram,
        						  $Add,$Active,$IdCharges
                                 ));

    }
}