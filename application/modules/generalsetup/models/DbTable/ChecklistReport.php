<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/8/2016
 * Time: 10:34 AM
 */
class GeneralSetup_Model_DbTable_ChecklistReport extends Zend_Db_Table_Abstract{

    public function getSemester(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.display = ?', 1)
            ->order('SemesterMainStartDate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemesterById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'))
            ->where('a.IdSemesterMaster = ?',$id)
            ->order('SemesterMainStartDate DESC');

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getExamSchedule($semid, $subid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_schedule'))
            ->joinLeft(array('b'=>'tbl_exam_center'), 'a.es_exam_center = ec_id', array('b.ec_name'))
            ->where('a.es_semester = ?', $semid)
            ->where('a.es_course = ?', $subid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCalendar($semid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_activity'))
            ->joinLeft(array('b'=>'tbl_activity_calender'), 'a.idActivity = b.IdActivity')
            ->where('b.IdSemesterMain = ?', $semid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgram($scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->where('a.IdScheme = ?', $scheme)
            ->order('a.seq_no');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramScheme($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getRegistrationItem($semid, $progid, $schemeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_registration_item'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.item_id = b.idDefinition', array('item_name'=>'b.DefinitionDesc'))
            ->where('a.semester_id = ?', $semid)
            ->where('a.program_id = ?', $progid)
            ->where('a.program_scheme_id = ?', $schemeid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getActivity($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('er'=>'tbl_registration_item'),array())
            ->joinLeft(array('era'=>'tbl_registration_item_activity'), 'era.ria_ri_id = er.ri_id', array('*'))
            ->joinLeft(array('tc'=>'tbl_activity_calender'), 'tc.IdActivity = era.ria_activity_id and er.semester_id = tc.IdSemesterMain', array('*'))
            ->joinLeft(array('tca'=>'tbl_activity'), 'tca.idActivity = tc.IdActivity', array('*'))
            ->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=er.semester_id', array('s.SemesterMainCode', 's.SemesterMainName'))
            ->joinLeft(array('p'=>'tbl_program'),'er.program_id=p.IdProgram', array('p.ProgramName as program_name'))
            ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=er.program_scheme_id')
            ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc'))
            ->where('era.ria_ri_id = ?', $id)
            ->group('era.ria_id');

        $result = $db->fetchAll($select);
        return $result;
    }
}