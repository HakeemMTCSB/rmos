<?php 
class GeneralSetup_Model_DbTable_Barringsetup extends Zend_Db_Table_Abstract{
    
    public function getBarringSetup(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'))
            ->joinLeft(array('b'=>'tbl_barringsetup'), 'a.idDefinition = b.tbs_type')
            ->where('a.idDefType = ?', 165)
            ->order('defOrder');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getRoleList($type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'))
            ->joinLeft(array('b'=>'tbl_barringrole'), 'a.idDefinition = b.brole_role AND b.brole_type = '.$type)
            ->where('a.idDefType = ?', 1)
            ->order('a.defOrder')
            ->order('a.DefinitionDesc');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkBarringSetup($type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_barringsetup'))
            ->where('a.tbs_type = ?', $type);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertBarringSetup($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_barringsetup', $bind);
        $id = $db->lastInsertId('tbl_barringsetup', 'tbs_id');
        return $id;
    }
    
    public function updateBarringSetup($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_barringsetup', $bind, 'tbs_id = '.$id);
        return $update;
    }
    
    public function deleteRole($type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_barringrole', 'brole_type = '.$type);
        return $delete;
    }
    
    public function insertRole($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_barringrole', $bind);
        $id = $db->lastInsertId('tbl_barringrole', 'brole_id');
        return $id;
    }
    
    public function getRole($type){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_barringrole'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.brole_role = b.idDefinition', array('roleName'=>'b.DefinitionDesc'))
            ->where('a.brole_type = ?', $type);
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>