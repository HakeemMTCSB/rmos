<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class GeneralSetup_Model_DbTable_CourseOptionalItem extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_course_optional_item';
	protected $_primary = "coi_id";
	protected $_locale;
	
	public function init()
	{
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
	
	
}