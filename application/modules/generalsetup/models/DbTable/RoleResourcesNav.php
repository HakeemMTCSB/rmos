<?php
class GeneralSetup_Model_DbTable_RoleResourcesNav extends Zend_Db_Table { 
		
	protected $_name = 'tbl_role_resources_nav';
	protected $_primary = "rrn_id";
		
		
	public function addData($data)
    {    	     
        $id = $this->insert($data);  
        if(!$id){
			   	throw new exception('Fail to add');
		}
			   
        
    }
    
	public function deleteData($role_id){
		if($role_id != 0){
			$this->delete('rrn_role_id = '. (int)$role_id);
		}
	}
	
	public function checkRoleNav($role_id,$menu_id,$title_id,$screen_id,$i){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('rrn'=>$this->_name))					
					->where("rrn.rrn_role_id  = ?",$role_id)	
					->where("rrn.rrn_menu_id = ?",$menu_id)
					->where("rrn.rrn_title_id = ?",$title_id)
					->where("rrn.rrn_screen_id = ?",$screen_id)
					->where("rrn.rrn_res_group = ?",$i);
								
		$row = $db->fetchRow($select);
		
		if($row){
			return $row;
		}else{
			return null;
		}
		
	}
	
	public function migrate(){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$parent_id=1;
		$old_parent_id = 0 ;
		
		for($z=1; $z<=10; $z++){
			
				$select = $db->select()
						->from(array('nm'=>'tbl_nav_sidebar'))
						->where('nm.nav_menu_id = ?',$z)
						->order('nm.seq_order ASC');
									
				$rows = $db->fetchAll($select);
												
				$child_seq=0;
				$parent_seq=0;	
				
				foreach($rows as $row){
									
					if($old_parent_id !=$parent_id){
						$child_seq=0;
					}
					
					$data['sm_tm_id']= $row['nav_menu_id'];
					$data['sm_name']= $row['name'];
					$data['sm_desc']= $row['name'];
					$data['sm_module']= $row['module'];
					$data['sm_controller']= $row['controller'];
					$data['sm_action']= $row['action'];
					$data['sm_visibility']= $row['visible'];
					
					if($row['title_head']==0){ //child		
		
						$old_parent_id = $parent_id; 
						$data['sm_parentid']= $parent_id;
						$child_seq=$child_seq+1;
						$data['sm_seq_order']= $child_seq;
						$db->insert('tbl_sidebar_menu',$data);
						
					}else{ //parent	
												
						$data['sm_parentid']= 0;
						$parent_seq = $parent_seq+1;
						$data['sm_seq_order']= $parent_seq;				
						
						$db->insert('tbl_sidebar_menu',$data);
						$old_parent_id = $parent_id; 
						$parent_id = $db->lastInsertId('tbl_sidebar_menu');
						
					}
					
				}//END FOREACH
		}//END FOR
		
			
	}
}
