<?php

class GeneralSetup_Model_DbTable_ProgramMajorIntake extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_programmajor_intake';
    protected $_primary   = 'IDMajorIntake';

    public function getData($idMajor){

        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $db->select()
            ->from(array('m'=>$this->_name))
            ->where("m.IdMajor =?", $idMajor);

        $result = $db->fetchAll($lstrSelect);

        if($result){
            return $result;
        }else{
            return null;
        }
    }


    public function getInfo($IDMajorIntake){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('pm'=>'tbl_programmajor_intake'))
            ->joinLeft(array('b' => 'tbl_intake'), 'pm.idStartIntake=b.IdIntake', array('StartIntakeId' => 'b.IntakeId' ))
            ->joinLeft(array('c' => 'tbl_intake'), 'pm.idEndIntake=c.IdIntake', array('EndIntakeId' => 'c.IntakeId' ))
            ->where('pm.IDMajorIntake = ?', $IDMajorIntake);
        $row = $db->fetchRow($select);
        return $row;
    }

    public function addData($data){
        $this->insert($data);
    }

    public function updateData($data,$id){
        $this->update($data, $this->_primary .' = '. (int)$id);
    }

    public function deleteAllData($id){
        $this->delete("IdMajor = '".$id."'");
    }

    public function deleteData($id){
        $this->delete("IDMajorIntake = '".$id."'");
    }

    public function getMajorIntakeList($idMajor){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("r"=>$this->_name))
            ->joinLeft(array('b' => 'tbl_intake'), 'r.idStartIntake=b.IdIntake', array('StartIntakeId' => 'b.IntakeId' ))
            ->joinLeft(array('c' => 'tbl_intake'), 'r.idEndIntake=c.IdIntake', array('EndIntakeId' => 'c.IntakeId' ))
            ->where('r.IdMajor  =?',$idMajor);
        $row = $db->fetchAll($select);
        //dd($row);
        return $row;
    }

}
?>