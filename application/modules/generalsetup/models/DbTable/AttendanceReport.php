<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Model_DbTable_AttendanceReport extends Zend_Db_Table_Abstract {
    
    public function getSemesterList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.display = ?', 1)
            ->order('a.SemesterMainStartDate DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemesterById($semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.	IdSemesterMaster = ?', $semesterid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgramByScheme($schemeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('IdScheme = ?', $schemeid);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('a.seq_no ASC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSubjectFromLandscape($programId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscapesubject'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject=b.IdSubject')
            ->where('a.IdProgram = ?', $programId)
            ->group('b.IdSubject');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCourseSection($programid, $semesterid, $subjectid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_program'), array('value'=>'*'))
            ->join(array('b'=>'tbl_course_tagging_group'), 'a.group_id=b.IdCourseTaggingGroup')
            ->where('a.program_id = ?', $programid)
            ->where('b.IdSemester = ?', $semesterid)
            ->where('b.IdSubject = ?', $subjectid)
            ->group('a.group_id');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getLecturer($groupid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->join(array('b'=>'course_group_schedule'), 'a.IdCourseTaggingGroup=b.idGroup')
            ->joinLeft(array('c'=>'tbl_staffmaster'), 'b.IdLecturer=c.IdStaff')
            ->where('a.IdCourseTaggingGroup = ?', $groupid)
            ->group('b.IdLecturer');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getRegistrationItem($groupid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->join(array('b'=>'course_group_schedule'), 'a.IdCourseTaggingGroup=b.idGroup')
            ->joinLeft(array('c'=>'tbl_definationms'), 'b.idClassType=c.idDefinition')
            ->where('a.IdCourseTaggingGroup = ?', $groupid)
            ->group('b.idClassType');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer=b.IdStaff', array('lecturername'=>'b.FullName', 'FullName'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.Idsemester=c.IdSemesterMaster', array('semestername'=>'concat(c.SemesterMainName, " - ", c.SemesterMainCode)', 'semester'=>'concat(c.SemesterMainName, " - ", c.SemesterMainCode)'))
            ->joinLeft(array('d'=>'tbl_subjectmaster'), 'a.IdSubject=d.IdSubject', array('subjectname'=>'concat(d.SubCode, " - ", d.SubjectName)'))
            ->joinLeft(array('e'=>'course_group_schedule'), 'a.IdCourseTaggingGroup = e.idGroup')
            ->joinLeft(array('f'=>'tbl_definationms'), 'e.sc_venue=f.idDefinition', array('venue'=>'f.DefinitionDesc'))
            ->where('a.IdCourseTaggingGroup = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getGroupStudent($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration=b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id=c.id', array('studentName'=>'concat(c.appl_fname, " ", c.appl_lname)', 'appl_fname','appl_mname','appl_lname'))
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram', array('d.ProgramCode', 'd.*'))
            ->where('a.IdCourseTaggingGroup = ?', $id)
            ->where('IFNULL(a.exam_status,"Z") NOT IN ("EX","CT")')
            //->where('b.IdProgram = ?', $programid)
            ->where('a.Active <> ?', 3)
            ->order('c.appl_fname ASC')
            ->group('a.IdStudentRegistration');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupSchedule($id, $formData = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->where('a.idGroup = ?', $id)
            ->where('a.sc_status = ?', 1)
            ->order('a.sc_date');
        
        if ($formData != false){
            if (isset($formData['lecturer']) && $formData['lecturer']!=''){
                $select->where('a.IdLecturer = ?', $formData['lecturer']);
            }
            
            if (isset($formData['item']) && $formData['item']!=''){
                $select->where('a.idClassType = ?', $formData['item']);
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getGroupScheduleByItem($id, $item){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.idGroup = ?', $id)
            ->where('a.idClassType = ?', $item)
            ->where('a.sc_status = ?', 1)
            ->order('a.sc_date');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getAttendanceDate($groupId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ctd_status=b.idDefinition')
            ->where('a.ctd_groupid = ?', $groupId)
            //->where('a.ctd_class_status = ?', 1)
            ->group('a.ctd_scdate');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getPresentAttendanceDate($groupId, $studentId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ctd_status=b.idDefinition')
            ->where('a.ctd_groupid = ?', $groupId)
            ->where('a.ctd_studentid = ?', $studentId)
            ->where('a.ctd_class_status = ?', 1)
            ->where('a.ctd_status = 395 OR a.ctd_status = 396')
            ->group('a.ctd_scdate');
        
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getPresentAttendanceDateByItem($groupId, $studentId, $item){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ctd_status=b.idDefinition')
            ->where('a.ctd_groupid = ?', $groupId)
            ->where('a.ctd_studentid = ?', $studentId)
            ->where('a.ctd_item_type = ?', $item)
            ->where('a.ctd_status = 395 OR a.ctd_status = 396')
            ->where('a.ctd_class_status = ?', 1)
            ->group('a.ctd_scdate')
            ->order('a.ctd_upddate DESC');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getReportAtttendanceReport($data = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->join(array('d'=>'tbl_subjectmaster'), 'a.IdSubject = d.IdSubject')
            ->join(array('e'=>'tbl_course_tagging_group'), 'a.IdCourseTaggingGroup = e.IdCourseTaggingGroup')
            ->join(array('f'=>'tbl_semestermaster'), 'a.IdSemesterMain=f.IdSemesterMaster')
            ->order('f.SemesterMainStartDate ASC');
        
        if ($data != false){
            if (isset($data['semester']) && $data['semester'] != ''){
                $select->where('a.IdSemesterMain = ?', $data['semester']);
            }
            if (isset($data['program']) && $data['program'] != ''){
                $select->where('b.IdProgram = ?', $data['program']);
            }
            if (isset($data['programscheme']) && $data['programscheme'] != ''){
                $select->where('b.IdProgramScheme = ?', $data['programscheme']);
            }
            if (isset($data['course']) && $data['course'] != ''){
                $select->where('a.IdSubject = ?', $data['course']);
            }
            if (isset($data['studentid']) && $data['studentid'] != ''){
                $select->where('b.registrationId like "%'.$data['studentid'].'%"');
            }
            if (isset($data['studentname']) && $data['studentname'] != ''){
                $select->where('concat(c.appl_fname, " ", c.appl_lname) like "%'.$data['studentname'].'%"');
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
}