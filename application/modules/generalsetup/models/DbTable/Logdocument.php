<?php
class GeneralSetup_Model_DbTable_Logdocument extends Zend_Db_Table
{

    protected $_name = 'tbl_log_document';
    protected $_primary = "doc_id";

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public function addData($data)
    {
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $db->lastInsertId();
        return $id;
    }

    public function getDocumentList($search = array(), $type = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('a' => $this->_name));

        if (isset($search['doc_desc']) && !empty($search['doc_desc'])) {
            $selectData = $selectData->where("a.doc_desc  like '%' ? '%'", $search['doc_desc']);
        }

        if (isset($search['doc_desc_malay']) && !empty($search['doc_desc_malay'])) {
            $selectData = $selectData->where("a.doc_desc_malay  like '%' ? '%'", $search['doc_desc_malay']);
        }

        if ($type == 'sql') {
            return $selectData;
        } elseif ($type == 'count') {

        } else {
            return $result = $db->fetchAll($selectData);
        }

    }

}

?>