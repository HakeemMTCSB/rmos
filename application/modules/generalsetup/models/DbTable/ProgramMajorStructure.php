<?php

class GeneralSetup_Model_DbTable_ProgramMajorStructure extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_programmajor_struc';
    protected $_primary   = 'IDMajorStruc';

    public function getData($idMajor){

        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $db->select()
            ->from(array('m'=>$this->_name))
            ->where("m.IdMajor =?", $idMajor);

        $result = $db->fetchAll($lstrSelect);

        if($result){
            return $result;
        }else{
            return null;
        }
    }

    public function getInfo($IDMajorStruc){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('pm'=>'tbl_programmajor_struc'))
            ->joinLeft(array('b' => 'registry_values'), 'pm.idStruc = b.id', array('StrucDesc' => 'b.name' ))
            ->where('pm.IDMajorStruc = ?', $IDMajorStruc);
        $row = $db->fetchRow($select);
        return $row;
    }

    public function addData($data){
        $this->insert($data);
    }

    public function updateData($data,$id){
        $this->update($data, $this->_primary .' = '. (int)$id);
    }

    public function deleteAllData($id){
        $this->delete("IdMajor = '".$id."'");
    }

    public function deleteData($id){
        $this->delete("IDMajorStruc = '".$id."'");
    }

    public function getMajorStructureList($idMajor){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("r"=>$this->_name))
            ->joinLeft(array('b' => 'registry_values'), 'r.idStruc = b.id', array('StrucDesc' => 'b.name' ))
            //->joinLeft(array('c' => 'tbl_intake'), 'r.idEndIntake=c.IdIntake', array('EndIntakeId' => 'c.IntakeId' ))
            ->where('r.IdMajor  =?',$idMajor);

        $row = $db->fetchAll($select);
        return $row;
    }

}
?>