<?php

class GeneralSetup_Model_DbTable_Batch extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_batch';
    protected $_primary   = 'IDBatch';

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getData($idBatch){

        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $db->select()
            ->from(array('m'=>$this->_name))
            ->where("m.IDBatch =?", $idBatch);

        $result = $db->fetchAll($lstrSelect);

        if($result){
            return $result;
        }else{
            return null;
        }
    }

    public function getInfo(){//function to display all schemesetup details in list

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_batch"),array("a.*"))
            ->where("a.BatchStatus = 'ACTIVE' ")
            ->order("a.BatchCode");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function addData($data){
        $this->insert($data);
    }

    public function updateData($data,$id){
        $this->update($data, $this->_primary .' = '. (int)$id);
    }

    public function deleteAllData($id){
        $this->delete("IdMajor = '".$id."'");
    }

    public function deleteData($id){
        $this->delete("IDBatch = '".$id."'");
    }

    public function getBatchData($post = array(), $returnType = null, $id = null)
    {

        $select = $this->db->select()
            ->from(array('a' => $this->_name))
            ->order('a.BatchCode ASC');

        if (isset($post['BatchCode']) && $post['BatchCode'] != '') {
            //$select->where('a.BatchCode like '%'?'%'', $post['BatchCode']);
            $select->where("a.BatchCode like '%" .$post['BatchCode']."%'");
        }

        if (isset($post['BatchStatus']) && $post['BatchStatus'] != '') {
            $select->where('a.BatchStatus = ?', $post['BatchStatus']);
        }

        if ($returnType == 'sql') {
            return $select;

        }elseif($id){
            $select->where('a.id = ?', $id);
            return $result = $this->db->fetchRow($select);
        } else {
            return $result = $this->db->fetchAll($select);
        }

        echo $select;

    }

    public function getBatchInfo($id){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('pm'=>'tbl_batch'))
            ->where('pm.IDBatch = ?', $id);
        $row = $db->fetchRow($select);
        return $row;
    }


}
?>