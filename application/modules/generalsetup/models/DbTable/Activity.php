<?php

class GeneralSetup_Model_DbTable_Activity extends Zend_Db_Table
{
    protected $_name = 'tbl_activity'; // table name
    private $lobjDbAdpt;
    protected $addDropId = '18';

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->_sis_session = new Zend_Session_Namespace('sis');
    }

    public function fnaddActivity($larrformData)
    {
        $this->insert($larrformData);
    }

    public function fngetActivityDetails($semesterId = 0, $groupBy = 0)
    { //Function to get the Activity details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_activity"), array("a.*"))
            ->joinLeft(array('d' => 'registry_values'), 'd.id=a.ActivityType', array('ActivityTypeName' => 'name'))
            ->order('a.ActivityName asc');

        if ($semesterId) {
            $lstrSelect->join(array('ac' => 'tbl_activity_calender'), 'ac.IdActivity = a.idActivity', array('ac.StartDate', 'ac.EndDate', 'IdSemesterCalendar' => 'ac.id'))
                ->joinLeft(array('e' => 'registry_values'), 'ac.group_type_id = e.id', array('StudentGroupName' => 'e.name','StudentGroupCode' => 'e.code'))
                ->where('ac.IdSemesterMain = ? ', $semesterId)
                ->order('ac.StartDate desc');
        }

        if ($groupBy) {
            $lstrSelect->group('ac.IdActivity');
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnupdateActivity($formData, $lintIdActivity)
    { //Function for updating the Activity details
        unset ($formData ['Save']);
        $where = 'idActivity = ' . $lintIdActivity;
        $this->update($formData, $where);
    }

    public function fnDeleteActivity($idActivity)
    {  // function to delete activity details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = 'tbl_activity';
        $where = $db->quoteInto('idActivity = ?', $idActivity);
        $db->delete('tbl_activity', $where);
    }

    public function fnSearchActivity($post = array())
    { //Function for searching the Activity details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_activity"), array("a.*"))
            ->joinLeft(array('d' => 'registry_values'), 'd.id=a.ActivityType', array('ActivityTypeName' => 'name'))
            ->where('a.ActivityName like "%" ? "%"', $post['field3'])
            ->order("a.ActivityName");

        if (isset($post['field5']) && $post['field5'] != '') {
            $lstrSelect->where('a.ActivityType = ?', $post['field5']);
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }

    public function checkColor($color = '', $id = '')
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $db->select()
            ->from(array("a" => "tbl_activity"), array("a.*"))
            ->where('a.ActivityColorCode = ?', $color)
            ->order("a.ActivityName");

        if ($id != '') {
            $lstrSelect->where('a.idActivity != ?', $id);
        }

        $larrResult = $db->fetchRow($lstrSelect);

        return !empty($larrResult) ? false : true;
    }


    public function fngetActivityList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => $this->_name), array("key" => "a.idActivity", "value" => "IFNULL(a.ActivityName,'')"));
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    // Function to add calender entry for semester
    public function fnaddCalenderActivity($formData)
    {

        $auth = Zend_Auth::getInstance();

        $idSem = explode('_', $formData['IdSemester']);
        if ($idSem['1'] == 'detail') {
            $IdSemester = $idSem['0'];
            $IdSemesterMain = NULL;
        } else {
            $IdSemesterMain = $idSem['0'];
            $IdSemester = NULL;
        }
        $activityMappingtable = new Zend_Db_Table('tbl_activity_calender');
        $data = array(
            'IdSemesterMain' => $IdSemesterMain,
            'type'           => 1,
            'IdActivity'     => $formData['IdActivity'],
            'StartDate'      => $formData['StartDate'],
            'EndDate'        => $formData['EndDate'],
            'group_type_id'  => $formData['group_type_id'],
            'createdby'      => $auth->getIdentity()->iduser,
            'createddt'      => date('Y-m-d H:i:s')
        );

        if ($activityMappingtable->insert($data)) {
            return true;
        }
        return false;
    }

    public function fnupdateCalenderActivity($formData, $idCalendar)
    {
        $auth = Zend_Auth::getInstance();

        unset($formData['id']);
        $data = array(
            'IdActivity'    => $formData['IdActivity'],
            'StartDate'     => $formData['StartDate'],
            'EndDate'       => $formData['EndDate'],
            'group_type_id' => $formData['group_type_id'],
            'createdby'     => $auth->getIdentity()->iduser,
            'createddt'     => date('Y-m-d H:i:s')
        );

        $where = 'id = ' . $idCalendar;
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update('tbl_activity_calender', $data, $where);

    }


    // Function to get calender and semester mapping entries according to semester id
    public function getcalenderMappingDetail($semId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.IdSemester = ?', $semId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    // Function to get the calender detatil according to id
    public function getcalenderMappingDetailById($Id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.id = ?', $Id);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    // Function to get calender and semester mapping entries according to semester id
    public function getActivityDetailById($Id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity'))->where('a.idActivity = ?', $Id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    //Function to check semester mapping exist in activity semester mapping table
    public function checkcalenderexist($semId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.IdSemester = ?', $semId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        if (!empty($larrResult)) {
            return $larrResult;
        }
        return false;
    }


    //Function to check semester mapping exist in activity semester mapping table
    public function checkcalenderexistmain($semId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_activity_calender'))->where('a.IdSemesterMain = ?', $semId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        if (!empty($larrResult)) {
            return $larrResult;
        }
        return false;
    }


    public function getcalenderArray($calender, $activityDetail)
    {
        $tem = array();
        $tem['activityid'] = $calender['IdActivity'];
        $tem['activityname'] = $activityDetail['ActivityName'];
        $tem['colorcode'] = $activityDetail['ActivityColorCode'];
        $tem['calenderId'] = $calender['id'];
        $tem['calenderstartdate'] = $calender['StartDate'];
        $tem['calenderenddate'] = $calender['EndDate'];
        return $tem;
    }

    public function fnDeletecalender($id)
    {  // function to delete activity details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = 'tbl_activity_calender';
        $where = $db->quoteInto('id = ?', $id);
        $db->delete('tbl_activity_calender', $where);
    }

    public function fnupdatecalender($formData)
    { //Function for updating the Activity details
        unset($formData['Save']);
        $calenderId = $formData['id'];
        unset($formData['id']);
        $where = 'id = ' . $calenderId;
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update('tbl_activity_calender', $formData, $where);
    }


    public function getcalenderDetails($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_activity_calender'))
            ->join(array("d" => "tbl_semestermaster"), 'a.IdSemesterMain = d.IdSemesterMaster', array('d.SemesterMainCode as SemesterCodeMain'))
            ->join(array("c" => "tbl_activity"), 'a.IdActivity = c.idActivity')
            ->where('a.id = ?', $id);
        $result = $db->fetchRow($select);
        return $result;
    }

    /*
    * GET activity for add and drop subject
    */
    public function getaddDrop($id, $programId = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from('tbl_activity_calender')
            ->where('IdSemesterMain = ?', (int)$id);
        // ->where('IdActivity = ?', (int)$this->addDropId);
        if ($programId != 0) {
            $select->where('IdProgram = ?', (int)$programId);
        } else {
            $select->where('IdProgram IS NULL');
        }
        //echo $select;
        $result = $db->fetchAll($select);

        return $result;

    }

    public function fnainsertCalenderActivity($formData)
    {

        $newCalender = new Zend_Db_Table('tbl_activity_calender');
        $data = array(
            'IdSemesterMain' => $formData['IdSemesterMaster'],
            'IdActivity'     => $formData['IdActivity'],
            'type'           => 1,
            'StartDate'      => $formData['StartDate'],
            'EndDate'        => $formData['EndDate']
        );

        if (isset($formData['IdProgram'])) {
            $data['IdProgram'] = $formData['IdProgram'];
        }

        $newCalender->insert($data);
    }

    public function fngetActivityActive()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('a' => $this->_name), array("key" => "a.idActivity", "value" => "IFNULL(a.ActivityName,'')"))
            ->where('a.status = 1');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function getEventList($start_date = null, $end_date = null, $activity_type = null, $order = null, $semesterId = 0)
    {
        $auth = Zend_Auth::getInstance();
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'))
            ->join(array('a' => 'tbl_activity'), 'a.idActivity=ac.IdActivity', array('ActivityName', 'ActivityColorCode'))
            ->joinLeft(array("d" => "tbl_semestermaster"), 'ac.IdSemesterMain = d.IdSemesterMaster', array('d.SemesterMainCode as SemesterCodeMain'));

        if ($start_date != '' && $end_date != '') {
            $lstrSelect->where("ac.StartDate >= ?", $start_date)
                ->where("ac.EndDate <= ?", $end_date);
        }

        if ($semesterId) {
            $lstrSelect->where('ac.IdSemesterMain = ?', $semesterId);
        }

        if (isset($activity_type) && $activity_type != 0) {
            $lstrSelect->where('a.ActivityType = ?', $activity_type);
        }

        if (!in_array($this->_sis_session->IdRole, array(1, 455))) {
            $lstrSelect->where('ac.createdby = ?', $auth->getIdentity()->iduser);
        }

        if ($order != '') {
            $lstrSelect->order($order);
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }


    public function searchEventList($formData = null)
    {
        $auth = Zend_Auth::getInstance();
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'))
            ->join(array('a' => 'tbl_activity'), 'a.idActivity=ac.IdActivity', array('ActivityName', 'ActivityColorCode'))
            ->joinLeft(array("d" => "tbl_semestermaster"), 'ac.IdSemesterMain = d.IdSemesterMaster', array('d.SemesterMainCode as SemesterCodeMain'))
            ->order('ac.StartDate DESC')
            ->limit(100);

        if (isset($formData)) {

            if (isset($formData['IdSemester']) && $formData['IdSemester'] != '') {
                $lstrSelect->where('ac.IdSemesterMain = ?', $formData['IdSemester']);
            }

            if (isset($formData['IdActivity']) && $formData['IdActivity'] != '') {
                $lstrSelect->where('ac.IdActivity = ?', $formData['IdActivity']);
            }

            if (isset($formData['date_from']) && $formData['date_from'] != '') {
                $lstrSelect->where('DATE(ac.StartDate) >= ?', date('Y-m-d', strtotime($formData['date_from'])));
            }

            if (isset($formData['date_to']) && $formData['date_to'] != '') {
                $lstrSelect->where('DATE(ac.EndDate) <= ?', date('Y-m-d', strtotime($formData['date_to'])));
            }
        }

        if (!in_array($this->_sis_session->IdRole, array(1, 455))) {
            $lstrSelect->where('ac.createdby = ?', $auth->getIdentity()->iduser);
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }

    public function checkExistActivity($idActivity)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array('ac' => 'tbl_activity_calender'))
            ->where('ac.IdActivity = ?', $idActivity);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
}