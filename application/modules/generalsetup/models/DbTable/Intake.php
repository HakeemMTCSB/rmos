<?php

class GeneralSetup_Model_DbTable_Intake extends Zend_Db_Table
{
    protected $_name = "tbl_intake";
    private $lobjDbAdpt;
    protected $_locale;

    /**
     *
     * @see Zend_Db_Table_Abstract::init()
     */
    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        $registry = Zend_Registry:: getInstance();
        $this->_locale = $registry->get('Zend_Locale');
    }

    /**
     *
     * Method to get all the branch list
     */
    public function fngetBranchList()
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_branchofficevenue"), array("key" => "a.IdBranch", "value" => "a.BranchName"))
            ->where("a.Active = 1")
            //->where("a.IdType = 1")
            ->order("a.BranchName");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnaddIntake($formData)
    {

        //asd($formData);
        $appSD = $formData['ApplicationStartDate'];
        $appED = $formData['ApplicationEndDate'];

        //$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelectintake = $this->lobjDbAdpt->query("SELECT * FROM tbl_intake WHERE 	IntakeId = '" . $formData['IntakeId'] . "'");
        $row_intake = $lstrSelectintake->fetch();

        //$lstrSelect = $this->lobjDbAdpt->query("SELECT * FROM tbl_intake WHERE (  ( '".$appSD."' BETWEEN ApplicationStartDate AND ApplicationEndDate ) OR ( '".$appED."' BETWEEN ApplicationStartDate AND ApplicationEndDate ) ) ");
        //$rows = $lstrSelect->fetchAll();

        //echo "SELECT * FROM tbl_intake WHERE (  ( '".$appSD."' BETWEEN ApplicationStartDate AND ApplicationEndDate ) OR ( '".$appED."' BETWEEN ApplicationStartDate AND ApplicationEndDate ) ) ";
        //echo "</br>";
        //echo "SELECT * FROM tbl_intake WHERE (  ( ApplicationStartDate BETWEEN '".$appSD."' AND '".$appED."' ) OR ( ApplicationEndDate BETWEEN '".$appSD."' AND '".$appED."' ) ) ";
        //die;

        //$lstrSelect2 = $this->lobjDbAdpt->query("SELECT * FROM tbl_intake WHERE (  ( ApplicationStartDate BETWEEN '".$appSD."' AND '".$appED."' ) OR ( ApplicationEndDate BETWEEN '".$appSD."' AND '".$appED."' ) ) ");
        //$rows2 = $lstrSelect2->fetchAll();

        if (!$row_intake) {
            $data['IntakeId'] = $formData['IntakeId'];
            $data['IntakeDesc'] = $formData['IntakeDesc'];
            $data['IntakeDescMalay'] = $formData['IntakeDescMalay'];
            $data['UpdUser'] = $formData['UpdUser'];
            $data['UpdDate'] = $formData['UpdDate'];
            $data['sem_year'] = $formData['sem_year'];
            $data['special_intake'] = $formData['special_intake'];
            if ($formData['ApplicationStartDate'] == "") {
                $data['ApplicationStartDate'] = '0000-00-00';
            } else {
                $data['ApplicationStartDate'] = $formData['ApplicationStartDate'];
            }

            if ($formData['ApplicationEndDate'] == "") {
                $data['ApplicationEndDate'] = '0000-00-00';
            } else {
                $data['ApplicationEndDate'] = $formData['ApplicationEndDate'];
            }
            $data['apply_online'] = 0;
            $this->insert($data);
            $insertId = $this->lobjDbAdpt->lastInsertId('tbl_intake', 'IdIntake');

            return $insertId;

        } else {
            return 'duplicate';
        }
    }


    public function fnupdateIntake($formData, $IdIntake)
    {
        $data['IntakeDesc'] = $formData['IntakeDesc'];
        $data['IntakeDescMalay'] = $formData['IntakeDescMalay'];
        $data['sem_seq'] = $formData['sem_seq'];
        $data['IntakeId'] = $formData['IntakeId'];
        $data['sem_year'] = $formData['sem_year'];
        $data['special_intake'] = $formData['special_intake'];
        if ($formData['ApplicationStartDate'] == "") {
            $data['ApplicationStartDate'] = '0000-00-00';
        } else {
            $data['ApplicationStartDate'] = $formData['ApplicationStartDate'];
        }

        if ($formData['ApplicationEndDate'] == "") {
            $data['ApplicationEndDate'] = '0000-00-00';
        } else {
            $data['ApplicationEndDate'] = $formData['ApplicationEndDate'];
        }

        $where = 'IdIntake = ' . $IdIntake;
        $this->update($data, $where);
    }

    public function fngetIntakeList()
    { //Function to get the Activity details
        $lstrSelect = $this->lobjDbAdpt->select()->from($this->_name)
            ->order("ApplicationStartDate desc");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnSearchIntake($post = array())
    { //Function for searching the Activity details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_intake"), array("a.*"))
            ->order("sem_year DESC  ,
											   CASE `sem_seq`
											        WHEN 0
											            THEN 'JAN'
											        WHEN 2
											            THEN 'JUN'
											        WHEN 3
											            THEN 'SEP'
											    END ");

        if (isset($post['field3']) && $post['field3'] != '') {
            $lstrSelect->where('a.IntakeId like "%" ? "%"', $post['field3']);
        }

        if (isset($post['field28']) && $post['field28'] != '') {
            $lstrSelect->where('a.IntakeDescMalay like "%" ? "%"', $post['field28']);
        }

        if (isset($post['field4']) && $post['field4'] != '') {
            $lstrSelect->where('a.IntakeDesc like "%" ? "%"', $post['field4']);
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetallIntake()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => $this->_name), array("key" => "a.IdIntake", "value" => "a.IntakeDesc"));

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetallIntakelist()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => $this->_name), array("key" => "a.IdIntake", "name" => "a.IntakeId", 'IntakeDesc'))
            ->order('a.sem_year desc');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetIntakeDetails($IdIntake = "")
    { //Function to get the user details
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => $this->_name), array("$IdIntake"))
            ->where("IdIntake = $IdIntake");
        $result = $this->fetchAll($select);
        return $result->toArray();
    }

    public function fngetIntakes()
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_intake"), array("key" => "a.IdIntake", "value" => "a.IntakeDesc"));
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnfetchIntakeCode($lintintakeid)
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_intake"), array("a.IntakeId"))
            ->where("IdIntake = ?", $lintintakeid);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnAddperiod($intakeid, $sdate, $edate)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $ux_sdate = strtotime($sdate);
        $ux_edate = strtotime($edate);

        $montharray = array(
            1 => "Januari",
            2 => "Februari",
            3 => "Maret",
            4 => "April",
            5 => "Mei",
            6 => "Juni",
            7 => "Juli",
            8 => "Agustus",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Desember",
        );

        $syear = date("Y", $ux_sdate);
        $smonthint = date("n", $ux_sdate);
        $smonth = date("m", $ux_sdate);
        //echo $smonth.$syear;
        $eyear = date("Y", $ux_edate);
        $emonthint = date("n", $ux_edate);
        $emonth = date("m", $ux_edate);

        if ($syear < $eyear) {
            for ($i = $smonthint; $i <= 12; $i++) {
                $years[$syear][$i] = $montharray[$i];
            }
            for ($i = 1; $i <= $emonthint; $i++) {
                $years[$eyear][$i] = $montharray[$i];
            }
        }
        $num = 1;
        foreach ($years as $year => $months) {
            foreach ($months as $month => $monthname) {
                if (strlen($month) == 1) {
                    $month2 = "0" . $month;
                } else {
                    $month2 = $month;
                }
                $period = array(
                    'ap_intake_id' => "$intakeid",
                    'ap_month' => $month,
                    'ap_year' => "$year",
                    'ap_code' => "$month2/$year",
                    'ap_desc' => "Periode $monthname $year",
                    'ap_number' => "$num",
                );
                $lobjDbAdpt->insert("tbl_academic_period", $period);
                $num++;
            }
        }

    }

    public function getCurrentIntakeBydate($curdate)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array("a" => "tbl_intake"));
        //->where("`ApplicationStartDate` <= ?",$curdate)
        //->where("`ApplicationEndDate` >= ?",$curdate);
        $result = $db->fetchrow($sql);

        if (!is_array($result)) {
            return false;
        } else {
            return $result["IdIntake"];
        }
    }

    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }

    public function updateData($data, $id)
    {
        $this->update($data, "IdIntake = '" . $id . "'");
    }

    public function deleteData($id)
    {
        $this->delete("IdIntake = '" . $id . "'");
    }


    public function getIntakeBranch($ip_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array("i" => "tbl_intake_branch_mapping"))
            ->join(array('b' => 'tbl_branchofficevenue'), 'b.IdBranch=i.IdBranch', array('BranchName'))
            ->where('i.ip_id = ?', $ip_id);

        $result = $db->fetchAll($sql);
        return $result;
    }

}