<?php

class GeneralSetup_Model_DbTable_IntakeRegLocation extends Zend_Db_Table
{

    protected $_name = 'intake_registration_location';
    protected $_primary = 'rl_id';

    public function addData($data)
    {
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->lastInsertId();;
    }

    public function updateData($data, $id)
    {
        $this->update($data, $this->_primary . ' = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $this->delete($this->_primary . ' =' . (int)$id);
    }

    public function getDatabyId($id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("rl" => $this->_name))
            ->where($this->_primary . '=?', $id);
        $row = $db->fetchRow($select);

        return $row;
    }

    public function getLocationList($idIntake)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("rl" => $this->_name))
            ->join(array('b' => 'tbl_branchofficevenue'), 'b.IdBranch=rl.rl_branch', array('BranchName'))
            ->where('rl.IdIntake  =?', $idIntake)
            ->where('b.active  =?', 1);
        $row = $db->fetchAll($select);
        return $row;
    }

    public function checkDuplicate($idIntake, $idBranch)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("rl" => $this->_name))
            ->where('rl.IdIntake  =?', $idIntake);
        if ($idBranch) {
            $select->where('rl.rl_branch  =?', $idBranch);
        }

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getBranchName($rl_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array("a" => "intake_registration_location"))
            ->join(array('b' => 'tbl_branchofficevenue'), 'b.IdBranch=a.rl_branch', array('BranchName'))
            ->where('a.rl_id = ?', $rl_id);

        $result = $db->fetchAll($sql);

        return $result;
    }

    public function getBranchAddress($rl_branch)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = "select distinct add_address1,add_address2,add_zipcode,add_city,add_country from intake_registration_location a
left join tbl_branchofficevenue b on a.rl_branch = b.idbranch
left join tbl_address c on c.add_org_id = a.rl_branch
where c.`add_org_name` = 'tbl_branchofficevenue'
and add_address_type = (select id from registry_values where code ='permanent-address')
AND `add_org_id` = $rl_branch";

        $row = $db->fetchRow($select);

        return $row;
    }


}

?>