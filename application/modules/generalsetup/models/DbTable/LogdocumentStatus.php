<?php
class GeneralSetup_Model_DbTable_LogdocumentStatus extends Zend_Db_Table
{

    protected $_name = 'tbl_log_status';
    protected $_primary = "tls_id";

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public function addData($data)
    {
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $db->lastInsertId();
        return $id;
    }

    public function getStatusList($search = array(), $type = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('a' => $this->_name));

        if (isset($search['tls_status']) && !empty($search['tls_status'])) {
            $selectData = $selectData->where("a.tls_status  like '%' ? '%'", $search['tls_status']);
        }

        if (isset($search['tls_status_malay']) && !empty($search['tls_status_malay'])) {
            $selectData = $selectData->where("a.tls_status_malay  like '%' ? '%'", $search['tls_status_malay']);
        }

        if ($type == 'sql') {
            return $selectData;
        } elseif ($type == 'count') {

        } else {
            return $result = $db->fetchAll($selectData);
        }

    }

}

?>