<?php
class GeneralSetup_Model_DbTable_SidebarMenu extends Zend_Db_Table { 
		
	protected $_name = 'tbl_sidebar_menu';
	protected $_primary = "sm_id";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('tns'=>$this->_name))
					->where('tns.sm_id = '.$id);

			$row = $db->fetchRow($select);
		}else{
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('app'=>$this->_name));
								
			$row = $db->fetchAll($select);
		}
		
		return $row;
	}
	
	public function getSidebarList($menu_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('sm'=>$this->_name))
					->where("sm.sm_tm_id = '".$menu_id."'")
					->where("sm.sm_parentid=0")
					->order('sm.sm_seq_order');
								
		$row_parent = $db->fetchAll($select);
		
		foreach ($row_parent as $index=>$parent){
			
			//get child
			$select_c =  $db->select()
					->from(array('sm'=>$this->_name))
					->where("sm.sm_tm_id = '".$menu_id."'")
					->where("sm.sm_parentid=?",$parent['sm_id'])
					->order('sm.sm_seq_order');
								
			$row_child = $db->fetchAll($select_c);
			$row_parent[$index]['child']=$row_child;
		}
	
		return	$row_parent;	
	}
	
	public function insert(array $data) {
		
        $db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('sm'=>$this->_name), array(new Zend_Db_Expr('max(sm_seq_order) as max_seq')))
				->where('sm.sm_tm_id = '.$data['sm_tm_id']);
				
		if($data['sm_parentid']!=0){
			$select->where('sm.sm_parentid = '.$data['sm_parentid']);
		}else{
			$select->where('sm.sm_parentid = 0');
		}
		$row = $db->fetchRow($select);
        
		$data['sm_seq_order'] = $row['max_seq']+1;
		
        return parent::insert($data);
    }
	
	public function addData($postData){
		
		$data = array(
		        'role_id' => $postData['role_id'],
				'label' => $postData['label'],
				'title' => $postData['title'],
				'module' => $postData['module'],
				'controller' => $postData['controller'],
				'action' => $postData['action'],
				'order' => $postData['seq_order'],
		);
				
		$this->insert($data);
	}
	
	public function updateData($postData,$id){
		
		$data = array(
		        'role_id' => $postData['role_id'],
				'label' => $postData['label'],
				'title' => $postData['title'],
				'module' => $postData['module'],
				'controller' => $postData['controller'],
				'action' => $postData['action'],
				'order' => $postData['seq_order'],
		);
			
		$this->update($data, 'sm_id = '. (int)$id);
	}
	
	public function deleteData($id){
		if($id!=0){
			$this->delete('sm_id = '. (int)$id);
			$this->delete('tbl_top_menu', 'sm_tm_id ='.(int)$id);
		}
	}
	
	public function getParentSidebar($menu_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('sm'=>$this->_name))
					->where("sm.sm_tm_id = '".$menu_id."'")
					->where("sm.sm_parentid = 0")
					->order('sm.sm_seq_order');
								
		$row = $db->fetchAll($select);
		
		return $row;
		
	}
	
	public function getChildSidebar($pid){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('sm'=>$this->_name))					
					->where("sm.sm_parentid = ?",$pid)				
					->order('sm.sm_seq_order');
								
		$row = $db->fetchAll($select);
		
		return $row;
		
	}
	
	public function updateSideBar($data,$id,$old_parent_id){
		
		//check sequence
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('sm'=>$this->_name), array(new Zend_Db_Expr('max(sm_seq_order) as max_seq')))
				->where('sm.sm_tm_id = '.$data['sm_tm_id']);
				
		if($data['sm_parentid']!=0){
			
			$select->where('sm.sm_parentid = '.$data['sm_parentid']);
			$row = $db->fetchRow($select);
			
			//child if change parent
			if($old_parent_id!=$data['sm_tm_id']){
				$data['sm_seq_order'] = $row['max_seq']+1;
			}
			
		}else{
			$select->where('sm.sm_parentid = 0');			
		}
		
        			
		$this->update($data, 'sm_id = '. (int)$id);
	}
	
	public function getSidebar($menu_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('sm'=>$this->_name))
					->where("sm.sm_tm_id = '".$menu_id."'")				
					->order('sm.sm_seq_order');
								
		$rows = $db->fetchAll($select);
			
		return	$rows;	
	}

	public function updateResource($data, $id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update('tbl_resources_new', $data, 'r_id = '.$id);
	}
}
