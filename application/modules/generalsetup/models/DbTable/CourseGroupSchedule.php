<?php 

class GeneralSetup_Model_DbTable_CourseGroupSchedule extends Zend_Db_Table_Abstract {
	
	protected $_name = 'course_group_schedule';
	protected $_primary = "sc_id";
	
	
	public function addData($data){		
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $this->insert($data);
	   return $db->lastInsertId();
	}	
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getSchedule($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array("a"=>$this->_name))
					  ->joinLeft(array("b"=>"tbl_staffmaster"),'b.IdStaff=a.IdLecturer',array('FullName','FirstName','SecondName','ThirdName'))
					   ->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=a.sc_venue',array('DefinitionDesc','BahasaIndonesia'))
					  ->joinLeft(array("e"=>"tbl_definationms"),'e.idDefinition=a.idClassType',array('class_type'=>'DefinitionDesc','class_type_my'=>'BahasaIndonesia'))
					  ->where('idGroup = ?',$idGroup);					  
		 $row = $db->fetchAll($select);	
		 
		 return $row;
	}
	
	public function getData($idSchedule){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where('sc_id = ?',$idSchedule);					  
		 $row = $db->fetchRow($select);	
		 
		 return $row;
	}
	
	
	public function getDetailsInfo($idSchedule){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('s'=>$this->_name), array('value'=>'*', 'scdate'=>'s.sc_date'))
					  ->joinLeft(array('cg'=>'tbl_course_tagging_group'),'cg.IdCourseTaggingGroup=s.idGroup')
					  ->joinLeft(array('c'=>'tbl_subjectmaster'),'c.IdSubject=cg.IdSubject',array('SubjectName','SubCode','subjectMainDefaultLanguage'))
					  ->joinLeft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=cg.IdSemester',array('semester'=>'concat(sm.SemesterMainName, " Semester")'))
					  ->joinLeft(array('l'=>'tbl_staffmaster'),'l.IdStaff=cg.IdLecturer',array('FullName','FirstName','SecondName','ThirdName'))
					  ->joinLeft(array('sf'=>'tbl_staffmaster'),'sf.IdStaff=s.IdLecturer',array('sFullName'=>'FullName'))
                                          ->joinLeft(array('cls'=>'tbl_definationms'), 's.sc_venue=cls.idDefinition', array('venue'=>'cls.DefinitionDesc'))
					  ->where('sc_id = ?',$idSchedule);					  
		 $row = $db->fetchRow($select);	
		 
		 return $row;
	}
	
	
	public function getScheduleByItem($idItem,$idSemester,$idSubject,$idProgram,$idScheme,$idBranch){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$selectg = $db ->select()
					  ->from(array('cg'=>'tbl_course_tagging_group'),array('IdCourseTaggingGroup'))
					  ->join(array('cgp'=>'course_group_program'),'cg.`IdCourseTaggingGroup` = cgp.group_id',array())
					  ->where('cg.IdSubject = ?',$idSubject)
					  ->where('cg.IdSemester = ?',$idSemester)
					  ->where('(cg.IdBranch = 0 OR cg.IdBranch = ?)',$idBranch)
					  ->where('cgp.program_id = ?',$idProgram)
					  ->where('cgp.program_scheme_id = ?',$idScheme);
		
		 $row_g = $db->fetchAll($selectg);
		 
		 if(count($row_g)>0){
			 $select = $db ->select()
						  ->from(array('cgs'=>'course_group_schedule'))
						  ->where('cgs.idGroup IN (?)',$row_g)
						  ->where('cgs.idClassType = ?',$idItem);					  
			 $row = $db->fetchAll($select);			 
			 
		 }
		
		 if(isset($row) && count($row)>0){
		 	 return $row;
		 }else{
		 	return null;
		 }
		
	}
	
        public function getScheduleByGroup($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_definationms'), 'a.idClassType=b.idDefinition', array('classtype'=>'b.DefinitionDesc'))
                ->where('a.idGroup = ?', $id);
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getGroupProgram($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'course_group_program'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_program'), 'a.program_id=b.IdProgram')
                ->where('a.group_id = ?', $id)
                ->group('a.program_id');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getQpEvent($groupid, $scid){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'qp_event'), array('value'=>'*'))
                ->where('a.IdCourseTaggingGroup = ?', $groupid)
                ->where('a.sc_id = ?', $scid)
                ->where('class_type NOT IN ("Examination")');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function insertScheduleHistory($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('course_group_schedule_history', $bind);
            $id = $db->lastInsertId('course_group_schedule_history', 'id');
            return $id;
        }
        
        public function insertQpEventHistory($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('qp_event_history', $bind);
            $id = $db->lastInsertId('qp_event_history', 'id');
            return $id;
        }
        
        public function getDefination($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
                ->where('a.idDefinition = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getLecturer($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_staffmaster'), array('value'=>'*'))
                ->where('a.IdStaff = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function updateQpEvent($bind, $id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $update = $db->update('qp_event', $bind, 'id = '.$id);
            return $update;
        }
        
        public function getSemesterById($semid){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                ->where('a.IdSemesterMaster = ?', $semid);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function insertQpEvent($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('qp_event', $bind);
            $id = $db->lastInsertId('qp_event', 'id');
            return $id;
        }
        
        public function getGroupProgram2($groupid){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'course_group_program'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_program'), 'a.program_id=b.IdProgram')
                ->joinLeft(array('c'=>'tbl_program_scheme'), 'a.program_scheme_id = c.IdProgramScheme')
                ->joinLeft(array('d'=>'tbl_definationms'), 'c.mode_of_program=d.idDefinition', array('mop'=>'d.DefinitionDesc'))
                ->joinLeft(array('e'=>'tbl_definationms'), 'c.mode_of_study=e.idDefinition', array('mos'=>'e.DefinitionDesc'))
                ->joinLeft(array('f'=>'tbl_definationms'), 'c.program_type=f.idDefinition', array('pt'=>'f.DefinitionDesc'))
                ->joinLeft(array('g'=>'tbl_scheme'), 'b.IdScheme=g.IdScheme')
                ->where('a.group_id = ?', $groupid);
            
            $result = $db->fetchAll($select);
            return $result;
        }
}