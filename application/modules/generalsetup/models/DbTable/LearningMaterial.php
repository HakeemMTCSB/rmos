<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Model_DbTable_LearningMaterial extends Zend_Db_Table {
    
    public function getSemester(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->order('a.SemesterMainStartDate DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCourse($semesterid, $materialType = 731){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectsoffered'), array('value'=>'*'))
            ->join(array('b'=>'tbl_learning_material'), 'a.IdSubject = b.tlm_subjectId')
            ->join(array('c'=>'tbl_subjectmaster'), 'b.tlm_subjectId = c.IdSubject')
            ->where('a.IdSemester = ?', $semesterid)
            ->where('b.tlm_materialType = ?', $materialType)
            ->group('a.IdSubject');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudent($semesterid, $courseid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id = c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram = d.IdProgram')
            ->joinLeft(array('e'=>'tbl_program_scheme'), 'b.IdProgramScheme = e.IdProgramScheme')
            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
            ->where('a.IdSemesterMain = ?', $semesterid)
            ->where('a.IdSubject = ?', $courseid)
            ->group('a.IdStudentRegistration');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentInfo($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram')
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_study = e.idDefinition', array('mos'=>'e.DefinitionDesc'))
            ->where('a.IdStudentRegistration = ?', $studentid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getLearningMaterial($subjectid, $semesterid, $materialType = 731){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_learning_material'), array('value'=>'*'))
            ->where('a.tlm_subjectId = ?', $subjectid)
            ->where('a.tlm_semester = ?', $semesterid)
            ->where('a.tlm_materialType = ?', $materialType);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    static function getSponsorship($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sponsor_tag'), array('sponsorname'=>'concat(b.SponsorCode, " - ", b.fName)'))
            ->joinLeft(array('b'=>'tbl_sponsor'), 'b.idsponsor = a.Sponsor')
            ->where('a.StudentId = ?', $studentid);
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    static function getScholarship($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scholarship_studenttag'), array('scholarshipname'=>'b.sch_name'))
            ->joinLeft(array('b'=>'tbl_scholarship_sch'), 'b.sch_Id = a.sa_scholarship_type')
            ->where('a.sa_cust_id = ?', $studentid)
            ->where('a.sa_cust_type = ?', 1);
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    static function getDiscountPlan($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount_type_tag'), array('scholarshipname'=>'b.dt_discount'))
            ->joinLeft(array('b'=>'discount_type'), 'b.dt_id=a.dt_id')
            ->where('a.IdStudentRegistration = ?', $studentid);
        
        $result = $db->fetchOne($select);
        return $result;
    }
    
    public function checkLearningMaterial($studentid, $tlmid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_learning_material_tag'))
            ->where('a.lmt_studentid = ?', $studentid)
            ->where('a.lmt_tlmid = ?', $tlmid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function addLearningMaterial($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_learning_material_tag', $data);
        $id = $db->lastInsertId('tbl_learning_material_tag', 'lmt_id');
        return $id;
    }
    
    public function getLearningMaterialList($studentId, $subjectid, $semesterid, $materialType = 731){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_learning_material_tag'), array('value'=>'*'))
            ->join(array('b'=>'tbl_learning_material'), 'a.lmt_tlmid = b.tlm_id')
            ->where('a.lmt_studentid = ?', $studentId)
            ->where('b.tlm_subjectId = ?', $subjectid)
            ->where('b.tlm_semester = ?', $semesterid)
            ->where('b.tlm_materialType = ?', $materialType);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateLearningMaterial($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_learning_material_tag', $data, 'lmt_id = '.$id);
        return $update;
    }
    
    public function deleteLearningMaterial($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_learning_material_tag', 'lmt_id = '.$id);
        return $delete;
    }
}