<?php 

class GeneralSetup_Model_DbTable_CourseGroupProgram extends Zend_Db_Table_Abstract {
	
	protected $_name = 'course_group_program';
	protected $_primary = "id";
	
  public function getGroupData($group_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
                      ->from(array('cgp'=>$this->_name))
                      ->join(array('p'=>'tbl_program'), 'p.IdProgram = cgp.program_id')
                      ->join(array('s'=>'tbl_program_scheme'), 's.IdProgramScheme = cgp.program_scheme_id')
                      ->joinLeft(array('e'=>'tbl_definationms'), 's.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
                        ->joinLeft(array('f'=>'tbl_definationms'), 's.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
                        ->joinLeft(array('g'=>'tbl_definationms'), 's.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc'))
                      ->where('group_id = ?',$group_id);
							  
		 $row = $db->fetchAll($select);	
		 
		 return $row;
	}
	
	public function getListScheme($idGroup,$idProgram){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
                      ->from(array('cgp'=>$this->_name))                   
                      ->join(array('s'=>'tbl_program_scheme'), 's.IdProgramScheme = cgp.program_scheme_id',array('IdProgramScheme'))
                      ->joinLeft(array('e'=>'tbl_definationms'), 's.mode_of_program = e.idDefinition', array('ProgramMode'=>'e.DefinitionDesc'))
                      ->joinLeft(array('f'=>'tbl_definationms'), 's.mode_of_study = f.idDefinition', array('StudyMode'=>'f.DefinitionDesc'))
                      ->joinLeft(array('g'=>'tbl_definationms'), 's.program_type = g.idDefinition', array('ProgramType'=>'g.DefinitionDesc'))
                      ->where('group_id = ?',$idGroup)
                      ->where('program_id = ?',$idProgram);
							  
		 $row = $db->fetchAll($select);	
		 
		 return $row;
	}
	
	public function getGroupSchemByIdGroup($idProgram,$group_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
                      ->from(array('cgp'=>$this->_name))  
                      ->where('group_id = ?',$group_id)
                      ->where('program_id = ?',$idProgram);							  
		 $row = $db->fetchRow($select);	
		 
		 return $row;
	}
	
	public function addData($data){		
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $this->insert($data);
	   return $db->lastInsertId();
	}
	
}