<?php
class GeneralSetup_Model_DbTable_Race extends Zend_Db_Table {

	protected $_name = 'tbl_race_setup';
    protected $_primary = "id";

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getRaceData($post = array(), $returnType = null, $id = null)
    {

        $select = $this->db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'registry_values'),'a.race_type  = b.id',array('name'))
            ->order('a.race_id ASC');

        if (isset($post['race_id']) && $post['race_id'] != '') {
            $select->where('a.race_id = ?', $post['race_id']);
        }

        if (isset($post['race_desc']) && $post['race_desc'] != '') {
            $select->where('a.race_desc  like "%" ? "%"', $post['race_desc']);
        }

        if (isset($post['race_desc_english']) && $post['race_desc_english'] != '') {
            $select->where('a.race_desc_english  like "%" ? "%"', $post['race_desc_english']);
        }

        if (isset($post['race_type']) && $post['race_type'] != '') {
            $select->where('a.race_type  like "%" ? "%"', $post['race_type']);
        }

        if (isset($post['active']) && $post['active'] != '') {
            $select->where('a.active = ?', $post['active']);
        }

        if ($returnType == 'sql') {
            return $select;
        }elseif($id){
            $select->where('a.id = ?', $id);
            return $result = $this->db->fetchRow($select);
        } else {
            return $result = $this->db->fetchAll($select);
        }

    }

    public function insert(array $data)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['created_by'])) {
            $data['created_by'] = $auth->getIdentity()->iduser;
        }

        $data['created_at'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }

    public function update(array $data, $where)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['updated_by'])) {
            $data['updated_by'] = $auth->getIdentity()->iduser;
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        return parent::update($data, $where);
    }

}
