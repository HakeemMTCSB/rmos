<?php
class GeneralSetup_Model_DbTable_TblResources extends Zend_Db_Table { 
		
	protected $_name = 'tbl_resources_new';
	protected $_primary = "r_id";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('r'=>$this->_name))
					->where('r.r_id = '.$id);

			$row = $db->fetchRow($select);
		}else{
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('app'=>$this->_name));
								
			$row = $db->fetchAll($select);
		}
		
		return $row;
	}
	
	public function getMenuList(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('r'=>$this->_name))	
					->join(array('tm'=>'tbl_top_menu'),'tm.tm_id=r.r_menu_id',array('tm_id','tm_name'))				
					->order('tm.tm_seq_order');
								
		$row = $db->fetchAll($select);
		
		if($row){
			return $row;
		}else{
			return null;
		}
		
	}
	
	public function searchResource($data=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('r'=>$this->_name))	
					->join(array('tm'=>'tbl_top_menu'),'tm.tm_id=r.r_menu_id',array('tm_id','menu'=>'tm_name'))
					->joinLeft(array('sm'=>'tbl_sidebar_menu'),'sm.sm_id=r.r_title_id',array('sm_id','title'=>'sm_name'))
					->joinLeft(array('tsm'=>'tbl_sidebar_menu'),'tsm.sm_id=r.r_screen_id',array('screen'=>'sm_name'))
					->order('tm.tm_seq_order')
					->order('r.r_module')
					->order('r.r_controller')
					->order('r.r_action');
	
		if(isset($data)){
			if(isset($data['r_menu_id']) && $data['r_menu_id']!=''){
				$select->where('r.r_menu_id = ?',$data['r_menu_id']);
			}	
			
			if(isset($data['r_title_id']) && $data['r_title_id']!=''){
				$select->where('r.r_title_id = ?',$data['r_title_id']);
			}

			if(isset($data['r_screen_id']) && $data['r_screen_id']!=''){
				$select->where('r.r_screen_id = ?',$data['r_screen_id']);
			}
			
			if(isset($data['r_module']) && $data['r_module']!=''){
				$select->where("r.r_module LIKE '%".$data['r_module']."%'");
			}
			
			if(isset($data['r_controller']) && $data['r_controller']!=''){
				$select->where("r.r_controller LIKE '%".$data['r_controller']."%'");
			}
			
			if(isset($data['r_group']) && $data['r_group']!=''){
				$select->where('r.r_group = ?',$data['r_group']);
			}
			
			if(isset($data['r_action']) && $data['r_action']!=''){
				$select->where("r.r_action LIKE '%".$data['r_action']."%'");
			}
		}	

		//echo '<pre>';
		//echo $select;
		$row = $db->fetchAll($select);
		return $row;
	}
	
	
	public function deleteData($id){
		if($id!=0){
			$this->delete('r_id = '. (int)$id);
		}
	}
	
	public function getDistinctResource() 
	{
	    $select = $this->select()
	              ->group(array('r_module','r_controller','r_action'));
	
	    $rowset = $this->fetchAll($select)->toArray();
	
	    return $rowset;
	}
		
}
