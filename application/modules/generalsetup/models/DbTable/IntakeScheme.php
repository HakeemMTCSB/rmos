<?php

class GeneralSetup_Model_DbTable_IntakeScheme extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_intake_scheme';
    protected $_primary = "is_id";

    public function addData($data){
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $db->lastInsertId();
        return $id;
    }

    public function getIntakeScheme($idIntake,$rl_id){

        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
            ->from(array("is"=>$this->_name))
            ->join(array('s'=>'tbl_scheme'),'s.IdScheme = is.idScheme',array('EnglishDescription','IdScheme','SchemeCode'))
            //->where('ip.IdIntake = ?',$idIntake);
            ->where('is.rl_id = ?',$rl_id);
        $result = $db->fetchAll($sql);
       // dd($result);exit;

 /*       foreach($result as $index=>$res){

            //get defination scheme
            if ($res['ProgramMode'] != "") {
                $sql_prog_mode = $db->select()
                    ->from(array('dp' => 'tbl_definationms'), array('ProgramMode' => 'DefinitionCode', 'ProgramModeDesc' => 'DefinitionDesc'))
                    ->where('dp.idDefinition = ?', $res['ProgramMode']);
                $row_prog_mode = $db->fetchRow($sql_prog_mode);
                $result[$index]['ProgramMode'] = $row_prog_mode['ProgramMode'];
                $result[$index]['ProgramModeDesc'] = $row_prog_mode['ProgramModeDesc'];
            }else{
                $result[$index]['ProgramMode'] = "";
                $result[$index]['ProgramModeDesc'] = "";
            }

            //get defination scheme
            if($res['StudyMode'] != ""){
                $sql_study_mode = $db->select()
                    ->from(array('dp' => 'tbl_definationms'), array('StudyMode' => 'DefinitionCode', 'StudyModeDesc' => 'DefinitionDesc'))
                    ->where('dp.idDefinition = ?', $res['StudyMode']);
                $row_study_mode = $db->fetchRow($sql_study_mode);
                $result[$index]['StudyMode'] = $row_study_mode['StudyMode'];
                $result[$index]['StudyModeDesc'] = $row_study_mode['StudyModeDesc'];
            }else{
                $result[$index]['StudyMode'] = "";
                $result[$index]['StudyModeDesc'] = "";
            }

            //get defination type
            if ($res['ProgramType']!="") {
                $sql_type = $db->select()
                    ->from(array('dp' => 'tbl_definationms'), array('ProgramType' => 'DefinitionCode', 'ProgramTypeDesc' => 'DefinitionDesc'))
                    ->where('dp.idDefinition = ?', $res['ProgramType']);
                $row_type = $db->fetchRow($sql_type);
                $result[$index]['ProgramType'] = $row_type['ProgramType'];
                $result[$index]['ProgramTypeDesc'] = $row_type['ProgramTypeDesc'];
            }else{
                $result[$index]['ProgramType'] = "";
                $result[$index]['ProgramTypeDesc'] = "";
            }

        }*/

        return $result;
    }

    public function deleteData($id){
        $this->delete("ip_id = '".$id."'");
    }

    public function deleteIntakeData($id_intake){
        $this->delete("IdIntake = '".$id_intake."'");
    }

    public function checkDuplicate($idscheme,$idintake,$rl_id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("is"=>$this->_name))
            ->where('is.idScheme =?',$idscheme)
            ->where('is.IdIntake =?',$idintake)
            ->where('is.rl_id =?',$rl_id);
        $row = $db->fetchRow($select);
        return $row;
    }

    public function getIntakeSchemeInfo($is_id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$this->_name),
                array(
                    "key"=>"a.is_id",
                    "value"=>"a.*"
                ))
            ->join(array('b'=>'tbl_scheme'), 'a.idscheme = b.IdScheme')
            ->where('a.is_id = ?',$is_id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }


}
?>