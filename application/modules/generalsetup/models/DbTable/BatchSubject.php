<?php

class GeneralSetup_Model_DbTable_BatchSubject extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_batch_subject';
    protected $_primary   = 'IDBatchSubject';

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getData($idBatch){

        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $db->select()
            ->from(array('m'=>$this->_name))
            ->joinLeft(array("dt" => "tbl_subjectmaster"),'dt.IdSubject=m.idSubject', array('SubjectName'=>'SubjectName','SubCode'=>'SubCode'))
            ->joinLeft(array("b" => "registry_values"),'b.id=m.idType', array('name'=>'name'))
            ->where("m.IDBatch =?", $idBatch)
            ->order("m.idType");

        $result = $db->fetchAll($lstrSelect);

        if($result){
            return $result;
        }else{
            return null;
        }
    }

    public function getInfo(){//function to display all schemesetup details in list

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_batch_subject"),array("a.*"))
            ->order("a.IDBatch");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function addData($data){
        $this->insert($data);
    }

    public function updateData($data,$id){
        $this->update($data, $this->_primary .' = '. (int)$id);
    }

    public function deleteAllData($id){
        $this->delete("IdMajor = '".$id."'");
    }

    public function deleteData($id){
        $this->delete("IDBatchSubject = '".$id."'");
    }

    public function getDetails($IDBatchSubject){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('pm'=>'tbl_batch_subject'))
            ->where('pm.IDBatchSubject = ?', $IDBatchSubject);
        $row = $db->fetchRow($select);
        return $row;
    }

}
?>