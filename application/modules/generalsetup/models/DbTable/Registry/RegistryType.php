<?php

class GeneralSetup_Model_DbTable_Registry_RegistryType extends Zend_Db_Table
{

    protected $_name = 'registry_types';
    protected $_primary = "id";


    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public function getData($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getList($search = array(), $type = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->order('a.created_at asc');

        if (isset($search['code']) && !empty($search['code'])) {
            $select = $select->where("a.code  like '%' ? '%'", $search['code']);
        }

        if (isset($search['name']) && !empty($search['name'])) {
            $select = $select->where("a.name  like '%' ? '%'", $search['name']);
        }

        if ($type == 'sql') {
            return $select;
        } elseif ($type == 'count') {

        } else {
            return $result = $db->fetchAll($select);
        }


    }


    public function insert(array $data)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['created_by'])) {
            $data['created_by'] = $auth->getIdentity()->iduser;
        }

        $data['created_at'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }

    public function update(array $data, $where)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['updated_by'])) {
            $data['updated_by'] = $auth->getIdentity()->iduser;
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        return parent::update($data, $where);
    }


    /**
     * get data by code
     */

    public function getDataByCode($code)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->where('a.code = ?', $code);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getDataByCodeType($defms)
    {
        //$db = Zend_Db_Table::getDefaultAdapter();

        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('t' => 'registry_types'), array())
            ->join(array('v' => 'registry_values'), 't.id = v.type_id', array('key' => 'v.id', 'value' => 'v.name'))
            ->where('t.code = ?', $defms)
            //->where('t.active = 1')
            //->where('v.active = 1')
            ->order('v.name');

        $result = $this->fetchAll($select);
        return $result->toArray();
    }

}