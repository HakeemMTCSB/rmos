<?php

class GeneralSetup_Model_DbTable_Registry_RegistryValue extends Zend_Db_Table
{

    protected $_name = 'registry_values';
    protected $_primary = "id";


    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    /**
     * get data by id
     */

    public function getData($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }

    /**
     * get list of data by sql/count/array
     */

    public function getList($id, $search = array(), $type = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name),array("a.*","key"=>"a.id","value"=>"a.name","oumcode"=>"a.code"))
            ->join(array('b' => 'registry_types'), 'a.type_id = b.id', array('type' => 'b.name', 'type_code' => 'b.code'))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->where('a.type_id = ?', $id)
            ->order('a.created_at asc');

        if (isset($search['code']) && !empty($search['code'])) {
            $select = $select->where('a.code like "%" ? "%" ', $search['code']);
        }

        if (isset($search['name']) && !empty($search['name'])) {
            $select = $select->where("a.name like '%' ? '%'", $search['name']);
        }

        if ($type == 'sql') {
            return $select;
        } elseif ($type == 'count') {

        } else {
            return $result = $db->fetchAll($select);
        }

    }


    public function insert(array $data)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['created_by'])) {
            $data['created_by'] = $auth->getIdentity()->iduser;
        }

        $data['created_at'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }

    public function update(array $data, $where)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['updated_by'])) {
            $data['updated_by'] = $auth->getIdentity()->iduser;
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        return parent::update($data, $where);
    }

    /**
     * get data by code
     */

    public function getDataByCode($code)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->where('a.code = ?', $code);

        $result = $db->fetchRow($select);

        return $result;
    }


    /**
     * get list of data by registry type code
     */

    public function getListDataByCodeType($code)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name), array('*','idDefinition'=>'id','DefinitionDesc'=>'name'))
            ->join(array('b' => 'registry_types'), 'a.type_id = b.id', array('type' => 'b.name', 'type_code' => 'b.code'))
            ->join(array('u' => 'tbl_user'), 'u.idUser = a.created_by', array('created_user' => "concat_ws(' ',u.fName,u.mNAme,u.lName)"))
            ->joinLeft(array('ud' => 'tbl_user'), 'ud.idUser = a.updated_by', array('updated_user' => "concat_ws(' ',ud.fName,ud.mNAme,ud.lName)"))
            ->where('b.code = ?', $code)
            ->order('a.created_at asc');

        $result = $db->fetchAll($select);

        if($result){
            return $result;
        }else{
            return null;
        }

    }

}