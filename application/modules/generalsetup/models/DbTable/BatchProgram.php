<?php

class GeneralSetup_Model_DbTable_BatchProgram extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_batch_program';
    protected $_primary   = 'IDBatchProgram';

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getData($idBatch){

        $db = Zend_Db_Table::getDefaultAdapter();

        $lstrSelect = $db->select()
            ->from(array('m'=>$this->_name))
            ->joinLeft(array("dt" => "tbl_program"),'dt.IdProgram=m.idProgram', array('ProgramName'=>'ProgramName','ProgramCode'=>'ProgramCode'))
            ->joinLeft(array("b" => "registry_values"),'b.id=m.idStruc', array('name'=>'name'))
            ->joinLeft(array("c" => "tbl_programmajoring"),'c.IDProgramMajoring=m.idMajor', array('IdMajor'=>'IdMajor'))
            ->where("m.IDBatch =?", $idBatch);

        $result = $db->fetchAll($lstrSelect);

        if($result){
            return $result;
        }else{
            return null;
        }
    }

    public function getInfo(){//function to display all schemesetup details in list

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_batch_program"),array("a.*"))
            ->order("a.idProgram");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function addData($data){
        $this->insert($data);
    }

    public function updateData($data,$id){
        $this->update($data, $this->_primary .' = '. (int)$id);
    }

    public function deleteAllData($id){
        $this->delete("IdMajor = '".$id."'");
    }

    public function deleteData($id){
        $this->delete("IDBatchProgram = '".$id."'");
    }

    public function getDetails($IDBatchProgram){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('pm'=>'tbl_batch_program'))
            ->where('pm.IDBatchProgram = ?', $IDBatchProgram);
        $row = $db->fetchRow($select);
        return $row;
    }

}
?>