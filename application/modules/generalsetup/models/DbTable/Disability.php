<?php
class GeneralSetup_Model_DbTable_Disability extends Zend_Db_Table {

	protected $_name = 'tbl_disability_setup';
    protected $_primary = "id";

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getDisableData($post = array(), $returnType = null, $id = null)
    {

        $select = $this->db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'registry_values'),'a.disable_condition  = b.id',array('name'))
            ->order('a.id ASC')
        ;

        if (isset($post['disable_id']) && $post['disable_id'] != '') {
            $select->where('a.disable_id = ?', $post['disable_id']);
        }

        if (isset($post['disable_desc']) && $post['disable_desc'] != '') {
            $select->where('a.disable_desc  like "%" ? "%"', $post['disable_desc']);
        }

        if (isset($post['disable_desc_eng']) && $post['disable_desc_eng'] != '') {
            $select->where('a.disable_desc_eng  like "%" ? "%"', $post['disable_desc_eng']);
        }

        if (isset($post['disable_condition']) && $post['disable_condition'] != '') {
            $select->where('a.disable_condition  like "%" ? "%"', $post['disable_condition']);
        }

        if (isset($post['active']) && $post['active'] != '') {
            $select->where('a.active = ?', $post['active']);
        }

        if ($returnType == 'sql') {
            return $select;
        }elseif($id){
            $select->where('a.id = ?', $id);
            return $result = $this->db->fetchRow($select);
        } else {
            return $result = $this->db->fetchAll($select);
        }

    }

    public function insert(array $data)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['created_by'])) {
            $data['created_by'] = $auth->getIdentity()->iduser;
        }

        $data['created_at'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }

    public function update(array $data, $where)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['updated_by'])) {
            $data['updated_by'] = $auth->getIdentity()->iduser;
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        return parent::update($data, $where);
    }
}
