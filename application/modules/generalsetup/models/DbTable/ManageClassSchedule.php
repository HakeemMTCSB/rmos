<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 12/11/2015
 * Time: 9:22 AM
 */
class GeneralSetup_Model_DbTable_ManageClassSchedule extends Zend_Db_Table_Abstract {

    public function test(){
        $db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getSemester($scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $scheme);

        $select = $select.' ORDER BY a.AcademicYear DESC, CASE a.sem_seq '
            . 'WHEN "JAN" THEN 1 '
            . 'WHEN "FEB" THEN 2 '
            . 'WHEN "MAR" THEN 3 '
            . 'WHEN "APR" THEN 4 '
            . 'WHEN "MAY" THEN 5 '
            . 'WHEN "JUN" THEN 6 '
            . 'WHEN "JUL" THEN 7 '
            . 'WHEN "AUG" THEN 8 '
            . 'WHEN "SEP" THEN 9 '
            . 'WHEN "OCT" THEN 10 '
            . 'WHEN "NOV" THEN 11 '
            . 'WHEN "DEC" THEN 12 '
            . 'ELSE 13 '
            . 'END DESC';

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getClass($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemester = c.IdSemesterMaster');

        if ($search != false){
            if (isset($search['semester']) && $search['semester']!='') {
                $select->where($db->quoteInto('a.IdSemester = ?', $search['semester']));
            }
            if (isset($search['subcode']) && $search['subcode']!='') {
                $select->where($db->quoteInto('b.SubCode like ?', '%'.$search['subcode'].'%'));
            }
            if (isset($search['coursename']) && $search['coursename']!='') {
                $select->where($db->quoteInto('b.SubjectName like ?', '%'.$search['coursename'].'%'));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getLecturer($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_staffmaster'), array('value'=>'*'))
            ->where('a.IdStaff = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getDefinationById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefinition = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProfile($id){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db ->select()
            ->from(array('sp'=>'student_profile'))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.sp_id = sp.id')
            ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','nama_program'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
            ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=sr.IdProgramScheme',array())
            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc'))
            ->joinLeft(array("salute" => "tbl_definationms"), 'sp.appl_salutation=salute.idDefinition', array('Salutation'=>'DefinitionDesc'))
            ->joinLeft(array('city'=>'tbl_city'),'city.idCity=sp.appl_city',array('CityName'))
            ->joinLeft(array('state'=>'tbl_state'),'state.IdState=sp.appl_state',array('StateName'))
            ->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=sp.appl_country',array('CountryName'))
            ->where("sr.IdStudentRegistration  = ?",$id);

        $row = $db->fetchRow($select);

        if($row){
            return $row;
        }else{
            return null;
        }
    }

    public function getClassHistory($scid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule_history'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.sc_id = ?', $scid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getClassHistory2($tsdid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_schedule_details_history'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.tsd_lecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.tsd_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.tsd_id = ?', $tsdid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSchedule($scid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->where('a.sc_id = ?', $scid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkClassClash($groupid, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->where('a.ctd_groupid = ?', $groupid)
            ->where('a.ctd_scdate = ?', $date)
            ->where('a.ctd_class_status = ?', 1);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function addClassSenang($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('course_group_schedule', $data);
        $id = $db->lastInsertId('course_group_schedule', 'sc_id');
        return $id;
    }

    public function addClassSusah($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_schedule_details', $data);
        $id = $db->lastInsertId('tbl_schedule_details', 'tsd_id');
        return $id;
    }

    public function getClassSenang($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from (array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.sc_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getClassSusah($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_schedule_details'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.tsd_lecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.tsd_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.tsd_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertClassMerge($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_merge_group', $data);
        $id = $db->lastInsertId('tbl_merge_group', 'tmg_id');
        return $id;
    }

    public function updateClass($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_course_tagging_group', $data, 'IdCourseTaggingGroup IN ('.$id.')');
        return $update;
    }

    public function getMergeClass($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_merge_group'), array('value'=>'*'))
            ->where('a.tmg_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getClassById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemester = c.IdSemesterMaster')
            ->joinLeft(array('d'=>'tbl_merge_group'), 'a.merge_id = d.tmg_id')
            ->where('a.IdCourseTaggingGroup IN (?)', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateMergeClass($data, $id){
        //var_dump($data); exit;
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_merge_group', $data, 'tmg_id = '.$id);
        return $update;
    }

    public function deleteMergeClass($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_merge_group', 'tmg_id = '.$id);
        return $delete;
    }
}