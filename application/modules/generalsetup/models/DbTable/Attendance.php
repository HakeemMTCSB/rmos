<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class GeneralSetup_Model_DbTable_Attendance extends Zend_Db_Table_Abstract {
    
    public function getGoupInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer=b.IdStaff', array('lecturername'=>'b.FullName', 'FullName'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.Idsemester=c.IdSemesterMaster', array('semestername'=>'concat(c.SemesterMainName, " - ", c.SemesterMainCode)', 'semester'=>'concat(c.SemesterMainName, " - ", c.SemesterMainCode)', 'semester2'=>'c.SemesterMainName'))
            ->joinLeft(array('d'=>'tbl_subjectmaster'), 'a.IdSubject=d.IdSubject', array('subjectname'=>'concat(d.SubCode, " - ", d.SubjectName)', 'subjectcode'=>'d.SubCode', 'CourseType'=>'d.CourseType'))
            ->joinLeft(array('e'=>'course_group_schedule'), 'a.IdCourseTaggingGroup = e.idGroup')
            ->joinLeft(array('f'=>'tbl_definationms'), 'e.sc_venue=f.idDefinition', array('venue'=>'f.DefinitionDesc'))
            ->where('a.IdCourseTaggingGroup = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getGroupStudent($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration=b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id', array('studentName'=>'concat(c.appl_fname, " ", c.appl_lname)', 'appl_fname','appl_mname','appl_lname'))
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram', array('d.ProgramCode', 'd.*'))
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'b.IdProgramScheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->where('a.IdCourseTaggingGroup = ?', $id)
            ->where('IFNULL(a.exam_status,"Z") NOT IN ("EX","CT")')
            ->where('a.Active <> ?', 3)
            ->order('c.appl_fname ASC')
            ->group('a.IdStudentRegistration');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupSchedule($id, $type=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.idGroup = ?', $id)
            ->order('a.sc_date');

        if ($type == 0){
            $select->where('a.sc_status = ?', 1);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupProgram($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'course_group_program'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_program'), 'a.program_id=b.IdProgram')
                ->where('a.group_id = ?', $id)
                ->group('a.program_id');
            
            $result = $db->fetchAll($select);
            return $result;
        }
    
    public function getDefinition($id = 91){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id)
            ->where('a.Status = ?', 1)
            ->order('a.defOrder');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkAttendanceStatus($groupId, $studentId, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ctd_status=b.idDefinition')
            ->where('a.ctd_groupid = ?', $groupId)
            ->where('a.ctd_studentid = ?', $studentId)
            ->where('a.ctd_scdate = ?', $date);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertAttendance($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_classattendance', $bind);
        $id = $db->lastInsertId('tbl_classattendance', 'ctd_id');
        return $id;
    }
    
    public function updateAttendance($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_classattendance', $bind, 'ctd_id = '.$id);
        return $update;
    }
    
    public function getAttendanceDate($groupId, $type=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_classattendance'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.ctd_status=b.idDefinition')
            ->where('a.ctd_groupid = ?', $groupId)
            ->group('a.ctd_scdate');

        if ($type == 0){
            $select->where('a.ctd_class_status = ?', 1);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupScheduleByDate($id, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->where('a.idGroup = ?', $id)
            ->where('a.sc_date = ?', $date);
        //echo $select; exit;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateGroupSchedule($id, $data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('course_group_schedule', $data, 'sc_id = '.$id);
        return $update;
    }
    
    public function getStudentWithAttendance($formData=null){
    	
    	
    	 $db = Zend_Db_Table::getDefaultAdapter();
    	 
    	 $registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
    	 
    	 //get attendance setup
    	 $attendanceSetupDB = new Registration_Model_DbTable_AttendanceSetup();
    	 
    	 
    	 //get list student by $formdata search
    	 $select = $db ->select()
					  ->from(array('sr'=>'tbl_studentregistration'),array('IdStudentRegistration','registrationId','IdProgram','IdProgramScheme'))
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('IdCourseTaggingGroup'))					  
					  ->join(array('sp'=>'student_profile'),'sp.id = sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)','appl_fname','appl_lname'))
					  ->join(array('g'=>'tbl_course_tagging_group'),'g.IdCourseTaggingGroup=srs.IdCourseTaggingGroup',array('section_name'=>'CONCAT_WS("-",GroupName,GroupCode)'))
					  ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.student_id=srs.IdStudentRegistration AND srsd.semester_id=srs.IdSemesterMain AND srsd.subject_id=srs.IdSubject',array('item_id'))
					  ->join(array('i'=>'tbl_definationms'), 'srsd.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))
					  ->where('srs.IdSubject = ?',$formData['IdSubject'])
					  ->where('srs.IdSemesterMain = ?',$formData['IdSemester'])
					  ->where('srs.Active != ?',3)
					  ->where('srs.exam_bar != ?',0)
					  ->where('srs.IdCourseTaggingGroup IS NOT NULL OR srs.IdCourseTaggingGroup!=0')
					 // ->where('srs.IdStudentRegistration = ?',4210)
					  ->order('sp.appl_fname')
					  ->order('sp.appl_lname');
					  
			if(isset($formData)){
				if(isset($formData['IdGroup']) &&	$formData['IdGroup']!=''){
					$select->where('srs.IdCourseTaggingGroup = ?',$formData['IdGroup']);
				}
				
				if(isset($formData['IdItem']) &&	$formData['IdItem']!=''){					
					$select->where('srsd.item_id = ?',$formData['IdItem'])->where('srsd.status != ?',3);
				}
			}
	 

    	 $rows = $db->fetchAll($select);

    	 if(count($rows)>0){
             $idgrouparr = array();
	    	 foreach($rows as $index=>$row){
	    	 	
	    	 	$item = $registrationItemDB->getItemDetail($row['IdProgram'],$formData['IdSemester'],$row['IdProgramScheme'],$row['item_id']);
			
    	 		if(isset($item) && $item['mandatory']==1){

                    if (!in_array($row['IdCourseTaggingGroup'], $idgrouparr)) {
                        $this->updSchedule($row['IdCourseTaggingGroup']);
                        $idgrouparr[] = $row['IdCourseTaggingGroup'];
                    }


    	 			$calculate=$this->calculateAttendance($row['IdCourseTaggingGroup'],$row['IdStudentRegistration'],$row['item_id'], $formData);
    	 			$rows[$index]['attendance'] = $calculate['classesnAtt'];
    	 			$rows[$index]['percentage'] = $calculate['percentage'];
    	 			
    	 			//get item attendance setup
    	 			$attendance_setup = $attendanceSetupDB->getLatestPercentage($row['IdProgram'],$row['IdProgramScheme'],$row['item_id'],$formData['att_type']);
    	 		
	    	 		if($attendance_setup){
    	 				$rows[$index]['attendance_setup']  = true;
    	 				$rows[$index]['required_percentage']  = $attendance_setup['att_percentage'];

    	 				if($calculate['percentage']>=$attendance_setup['att_percentage']){
    	 					unset($rows[$index]);
    	 				}
    	 					
					}else{
						$rows[$index]['attendance_setup']  = false;
						if($calculate['percentage']==100){
							unset($rows[$index]);
						}
							
					}	
    	 		}else{
    	 			unset($rows[$index]);
    	 		}    	 	
	    	 }
    	 }
    	 
    	 //echo '<pre>';
    	 //print_r($rows);
		 return $rows;
    }
    
    public function checkClassAttendanceByItem($IdStudentRegistration,$IdCourseTaggingGroup,$item_id){

    	$db = Zend_Db_Tableg::getDefaultAdapter();
    	$select = $db ->select()
					  	->from(array('ca'=>'tbl_classattendance'))
						->where('ca.ctd_studentid = ?',$IdStudentRegistration)
						->where('ca.ctd_groupid = ?',$IdCourseTaggingGroup)
						->where('ca.ctd_item_type = ?',$item_id)
						->where('ca.ctd_status = ?',395);	//Present			  
		$rows = $db->fetchAll($select);
		return count($rows);
    }
    
	public function calculateAttendance($groupid, $studentid, $item, $formData = false){
		
		$attendanceReportDB = new GeneralSetup_Model_DbTable_AttendanceReport();
        $model = new GeneralSetup_Model_DbTable_Attendance();

       //kalo ubah kat sini peles ubah kat attendance model dalam registration module..
        $groupInfo = $attendanceReportDB->getGroupInfo($groupid);
        $groupSchedule = $attendanceReportDB->getGroupScheduleByItem($groupid, $item);
        //var_dump($groupSchedule);
        $sdate = $attendanceReportDB->getAttendanceDate($groupid);
        $presentAtt = $attendanceReportDB->getPresentAttendanceDateByItem($groupid, $studentid, $item);

        if ($groupSchedule){
            $classList = $groupSchedule;
        }else{
            $classList = false;
        }

        $weekArr = array();
        $monthArr = array();

        if ($classList){
            $i = 0;
            foreach ($classList as $key => $dateLoop){

                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }

                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop) {
                            //$classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            if ($sdateLoop['ctd_class_status'] == 1) {

                                $scdate = date('Y-m-d', strtotime($sdateLoop['ctd_scdate']));
                                $day = date('l', strtotime($sdateLoop['ctd_scdate']));

                                $schedule = $model->getSchedule($groupid, $scdate);

                                if (!$schedule) {
                                    $schedule = $model->getSchedule3($groupid, $scdate);

                                    if (!$schedule) {
                                        $schedule = $model->getSchedule2($groupid, $day);
                                        $starttime = $schedule['sc_start_time'];
                                        $endtime = $schedule['sc_end_time'];
                                        $venue = $schedule['sc_venue'];
                                        $lecturer = $schedule['IdLecturer'];
                                    } else {
                                        $starttime = $schedule['tsd_starttime'];
                                        $endtime = $schedule['tsd_enddate'];
                                        $venue = $schedule['tsd_venue'];
                                        $lecturer = $schedule['tsd_lecturer'];
                                    }
                                } else {
                                    $starttime = $schedule['sc_start_time'];
                                    $endtime = $schedule['sc_end_time'];
                                    $venue = $schedule['sc_venue'];
                                    $lecturer = $schedule['IdLecturer'];
                                }

                                $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                                $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                                $detectday1 = date('l', strtotime($sdateLoop['ctd_scdate']));
                                $detectday2 = $dateLoop['sc_day'];

                                if (!isset($classList[$i]['sc_id'])) {
                                    $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                                    $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                                    $classList[$i]['IdLecturer'] = $lecturer;
                                    $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                                    $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                                    $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                                    $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                                    $classList[$i]['sc_start_time'] = $starttime;
                                    $classList[$i]['sc_end_time'] = $endtime;
                                    $classList[$i]['sc_venue'] = $venue;
                                    $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                                    $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                                    $classList[$i]['sc_status'] = $sdateLoop['ctd_class_status'];
                                    $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                                    $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                                    $classList[$i]['classtype'] = $dateLoop['classtype'];
                                    $classList[$i]['lecturername'] = $schedule['lecturername'];
                                    $classList[$i]['veneuname'] = $schedule['veneuname'];
                                }

                                $i++;

                                $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                                $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                                if (!in_array($month, $monthArr)) {
                                    array_push($monthArr, $month);
                                }

                                if (!in_array($week, $weekArr)) {
                                    array_push($weekArr, $week);
                                }
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;

                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                            $classList[$i]['IdLecturer'] = $dateLoop['IdLecturer'];
                            $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                            $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                            $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                            $classList[$i]['sc_start_time'] = $dateLoop['sc_start_time'];
                            $classList[$i]['sc_end_time'] = $dateLoop['sc_end_time'];
                            $classList[$i]['sc_venue'] = $dateLoop['sc_venue'];
                            $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                            $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                            $classList[$i]['sc_status'] = $dateLoop['sc_status'];
                            $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                            $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                            $classList[$i]['classtype']=$dateLoop['classtype'];
                            $classList[$i]['lecturername']=$dateLoop['lecturername'];
                            $classList[$i]['veneuname']=$dateLoop['veneuname'];

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                }

                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));

                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }

                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
            }
        }

        if ($classList) {
            $this->aasort($classList, 'sc_date');
        }


        if (isset($formData['att_type']) && $formData['att_type']==1){
            $month = array();
            if (isset($formData['month']) && count($formData['month']) > 0) {
                foreach ($formData['month'] as $date) {
                    $month[]=date('Y-m', strtotime($date));
                }
            }

            //var_dump($monthArr);

            if ($presentAtt) {
                foreach ($presentAtt as $presentAttKey => $presentAttLoop) {
                    if (!in_array(date('Y-m', strtotime($presentAttLoop['ctd_scdate'])), $month)){
                        unset($presentAtt[$presentAttKey]);
                    }
                }
            }

            if ($classList) {
                foreach ($classList as $classListKey => $classListLoop) {
                    if (!in_array(date('Y-m', strtotime($classListLoop['sc_date'])), $month)){
                        unset($classList[$classListKey]);
                    }
                }
            }
        }

        $countAtt = 0;
        if ($classList){
            foreach ($classList as $classListKey2 => $classListLoop2){
                $status = $model->checkAttendanceStatus($groupid, $studentid, $classListLoop2['sc_date']);
                if ($status){
                    if ($status['idDefinition']==395){
                        $countAtt++;
                    }else if ($status['idDefinition']==396){
                        $countAtt++;
                    }
                }
            }
        }

        if ($groupSchedule){
            $classesAtt = $countAtt.'/'.count($classList);
            $percentageAtt = number_format((($countAtt/count($classList))*100), 0, '.', ',');
        }else{
            $classesAtt = '0/0';
            $percentageAtt = 0;
        }
        
        $returnArr = array(
            'classesnAtt' => $classesAtt,
            'percentage' => $percentageAtt
        );

        /*if ($studentid == 4345) {
            var_dump(count($presentAtt));
            var_dump($returnArr);
            var_dump($presentAtt);
            var_dump($classList);
            //var_dump($formData);
            exit;
        }*/

        return $returnArr;
    }
    
    public function getSchedule($groupid, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.idGroup = ?', $groupid)
            ->where('a.sc_date = ?', $date);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSchedule2($groupid, $day){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdLecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.idClassType = c.idDefinition', array('classtype'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.sc_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.idGroup = ?', $groupid)
            ->where('a.sc_day = ?', $day);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSchedule3($groupid, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_schedule_details'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.tsd_lecturer = b.IdStaff', array('lecturername'=>'b.FullName'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.tsd_venue = d.idDefinition', array('veneuname'=>'d.DefinitionDesc'))
            ->where('a.tsd_groupid = ?', $groupid)
            ->where('a.tsd_scdate = ?', $date);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getLecturer(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_staffmaster'), array('value'=>'*'))
            ->where('a.StaffAcademic = ?', 0)
            ->order('a.FullName');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertScheduleDetail($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_schedule_details', $bind);
        $id = $db->lastInsertId('tbl_schedule_details', 'tsd_id');
        return $id;
    }
    
    public function updateScheduleDetail($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_schedule_details', $bind, 'tsd_id = '.$id);
        return $update;
    }
    
    public function insertScheduleDetailHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_schedule_details_history', $bind);
        $id = $db->lastInsertId('tbl_schedule_details_history', 'id');
        return $id;
    }
    
    public function updateAttendanceDate($bind, $groupid, $date){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_classattendance', $bind, 'ctd_groupid = '.$groupid.' AND ctd_scdate = "'.$date.'"');
        return $update;
    }
    
    public function updateSchedule($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('course_group_schedule', $bind, 'sc_id = '.$id);
        return $update;
    }
    
    public function insertScheduleHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('course_group_schedule_history', $bind);
        $id = $db->lastInsertId('course_group_schedule_history', 'id');
        return $id;
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }

    public function updSchedule($groupId){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $model = new GeneralSetup_Model_DbTable_Attendance();

        $groupInfo = $model->getGoupInfo($groupId);

        $studentList = $model->getGroupStudent($groupId);

        $groupSchedule = $model->getGroupSchedule($groupId, 1);

        $sdate = $model->getAttendanceDate($groupId, 1);
        //var_dump($sdate);
        $status = $model->getDefinition();

        if ($groupSchedule){
            $classList = $groupSchedule;
        }else{
            //throw new Exception('Group Schedule Not Found');
            $classList =  false;
        }

        $weekArr = array();
        $monthArr = array();

        if ($classList){
            $i = 0;
            foreach ($classList as $key => $dateLoop){

                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }

                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            //$classList[$i]['sc_id'] = $dateLoop['sc_id'];

                            $scdate = date('Y-m-d', strtotime($sdateLoop['ctd_scdate']));
                            $day = date('l', strtotime($sdateLoop['ctd_scdate']));

                            $schedule = $model->getSchedule($groupId, $scdate);

                            if (!$schedule){
                                $schedule = $model->getSchedule3($groupId, $scdate);

                                if (!$schedule){
                                    $schedule = $model->getSchedule2($groupId, $day);
                                    $starttime = $schedule['sc_start_time'];
                                    $endtime = $schedule['sc_end_time'];
                                    $venue = $schedule['sc_venue'];
                                    $lecturer = $schedule['IdLecturer'];
                                }else{
                                    $starttime = $schedule['tsd_starttime'];
                                    $endtime = $schedule['tsd_enddate'];
                                    $venue = $schedule['tsd_venue'];
                                    $lecturer = $schedule['tsd_lecturer'];
                                }
                            }else{
                                $starttime = $schedule['sc_start_time'];
                                $endtime = $schedule['sc_end_time'];
                                $venue = $schedule['sc_venue'];
                                $lecturer = $schedule['IdLecturer'];
                            }

                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $detectday1 = date('l', strtotime($sdateLoop['ctd_scdate']));
                            $detectday2 = $dateLoop['sc_day'];

                            if (!isset($classList[$i]['sc_id'])) {
                                $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                                $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                                $classList[$i]['IdLecturer'] = $lecturer;
                                $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                                $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                                $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                                $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                                $classList[$i]['sc_start_time'] = $starttime;
                                $classList[$i]['sc_end_time'] = $endtime;
                                $classList[$i]['sc_venue'] = $venue;
                                $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                                $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                                $classList[$i]['sc_status'] = $sdateLoop['ctd_class_status'];
                                $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                                $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                                $classList[$i]['classtype']=$dateLoop['classtype'];
                                $classList[$i]['lecturername']=$schedule['lecturername'];
                                $classList[$i]['veneuname']=$schedule['veneuname'];
                            }

                            $i++;

                            $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                            $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;

                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                            $classList[$i]['IdLecturer'] = $dateLoop['IdLecturer'];
                            $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                            $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                            $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                            $classList[$i]['sc_start_time'] = $dateLoop['sc_start_time'];
                            $classList[$i]['sc_end_time'] = $dateLoop['sc_end_time'];
                            $classList[$i]['sc_venue'] = $dateLoop['sc_venue'];
                            $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                            $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                            $classList[$i]['sc_status'] = $dateLoop['sc_status'];
                            $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                            $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                            $classList[$i]['classtype']=$dateLoop['classtype'];
                            $classList[$i]['lecturername']=$dateLoop['lecturername'];
                            $classList[$i]['veneuname']=$dateLoop['veneuname'];

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                }

                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));

                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }

                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
            }
        }

        if ($classList) {
            $this->aasort($classList, 'sc_date');
        }

        if ($studentList){
            foreach ($studentList as $studentLoop2){
                if ($classList){
                    foreach ($classList as $classLoop2){
                        if ($classLoop2['sc_date'] != null){
                            $checkStatus = $model->checkAttendanceStatus($groupId, $studentLoop2['IdStudentRegistration'], $classLoop2['sc_date']);
                            $checkStatus2 = $model->checkAttendanceStatus($groupId, 0, $classLoop2['sc_date']);

                            if ($checkStatus){ //update
                                $data = array(
                                    'ctd_item_type'=>$classLoop2['idClassType'],
                                );
                                $model->updateAttendance($data, $checkStatus['ctd_id']);
                            }else{ //insert
                                $data = array(
                                    'ctd_groupid'=>$groupId,
                                    'ctd_studentid'=>$studentLoop2['IdStudentRegistration'],
                                    'ctd_item_type'=>$classLoop2['idClassType'],
                                    'ctd_scdate'=>$classLoop2['sc_date'],
                                    'ctd_status'=>395,
                                    'ctd_class_status'=>isset($checkStatus2['ctd_class_status']) ? $checkStatus2['ctd_class_status']:1,
                                    'ctd_reason'=>'',
                                    'ctd_upddate'=>date('Y-m-d h:i:s'),
                                    'ctd_updby'=>$userId
                                );
                                $model->insertAttendance($data);
                            }
                        }
                    }
                }
            }

            $model->deleteTempAtt($groupId);
        }else{
            if ($classList){
                foreach ($classList as $classLoop2){
                    if ($classLoop2['sc_date'] != null){
                        $checkStatus = $model->checkAttendanceStatus($groupId, 0, $classLoop2['sc_date']);

                        if ($checkStatus){ //update
                            $data = array(
                                'ctd_item_type'=>$classLoop2['idClassType'],
                            );
                            $model->updateAttendance($data, $checkStatus['ctd_id']);
                        }else{ //insert
                            $data = array(
                                'ctd_groupid'=>$groupId,
                                'ctd_studentid'=>0,
                                'ctd_item_type'=>$classLoop2['idClassType'],
                                'ctd_scdate'=>$classLoop2['sc_date'],
                                'ctd_status'=>395,
                                'ctd_reason'=>'',
                                'ctd_upddate'=>date('Y-m-d h:i:s'),
                                'ctd_updby'=>$userId
                            );
                            $model->insertAttendance($data);
                        }
                    }
                }
            }
        }

        return $classList;
    }

    public function deleteTempAtt($groupid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_classattendance', 'ctd_groupid = '.$groupid.' AND ctd_studentid = 0');
        return $delete;
    }
}
?>