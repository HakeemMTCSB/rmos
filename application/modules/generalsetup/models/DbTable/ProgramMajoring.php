<?php

class GeneralSetup_Model_DbTable_ProgramMajoring extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_programmajoring';
  protected $_primary   = 'IDProgramMajoring';
  
  public function getData($program_id){
  	
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	
	  	$lstrSelect = $db->select()
	                     ->from(array('m'=>$this->_name))	                   
	                     ->where("m.idProgram =?", $program_id);
	                    	  
	    $result = $db->fetchAll($lstrSelect);

	  	if($result){
	  		return $result;	
	  	}else{
	  		return null;
	  	}
  }

  public function getInfo($idProgramMajoring){
  	
  		$db = Zend_Db_Table::getDefaultAdapter();
  		
	  	$select = $db->select()
	                 ->from(array('pm'=>'tbl_programmajoring'))
	                 ->where('pm.IDProgramMajoring = ?', $idProgramMajoring);
        $row = $db->fetchRow($select);
        return $row;
  }

    public function getProgramMajor($idProgramMajoring){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('pm'=>'tbl_programmajoring'))
            ->joinLeft(array('c' => 'tbl_program'), 'pm.idProgram=c.IdProgram', array('ProgramName' => 'c.ProgramName' ))
            ->where('pm.IDProgramMajoring = ?', $idProgramMajoring);
        $row = $db->fetchRow($select);
        return $row;
    }

  public function addData($data){
		 $this->insert($data);
  }
  
  public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
  }
  
  public function deleteAllData($id){
		$this->delete("idProgram = '".$id."'");
  }
  
 public function deleteData($id){
		$this->delete("IDProgramMajoring = '".$id."'");
  }

    public function list_all() {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select  = $db->select()
            ->from(array("p" => "tbl_programmajoring"),array('IDProgramMajoring','IdMajor'))
            ->order('p.IdMajor');
        $rows = $db->fetchAll($select);
        return $rows;
    }

}
?>