<?php 

class GeneralSetup_Form_AddLevelSemForm extends Zend_Form
{
	protected $programID;
	protected $landscapeId;
	protected $SemsterCount;
	protected $LevelCount;
	protected $idBlock;
	
	public function setProgramID($programID){
		$this->programID = $programID; 
	}
	
	public function setLandscapeId($landscapeId){
		$this->landscapeId = $landscapeId; 
	}
	
	public function setSemsterCount($SemsterCount){
		$this->SemsterCount = $SemsterCount; 
	}
	
	public function setLevelCount($LevelCount){
		$this->LevelCount = $LevelCount; 
	}
	
	public function setIdBlock($idBlock){
		$this->idBlock = $idBlock; 
	}
	
	
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','detailForm');
		
		if($this->idBlock){
			$this->setAction('/generalsetup/landscape/edit-level-sem/');
		}else{
			$this->setAction('/generalsetup/landscape/add-level-sem/');
		}
						
		$this->addElement('hidden', 'id',array('value'=>$this->programID));
		$this->addElement('hidden', 'idlandscape',array('value'=>$this->landscapeId));
		$this->addElement('hidden', 'idblock',array('value'=>$this->idBlock));
		  

        //blockid
		$this->addElement('select','blockid', array(
			'label'=>$this->getView()->translate('Level')
		));	
		
		if($this->idBlock){
			$this->blockid->setAttrib('disabled','disabled');
		}else{
			$this->blockid->setAttrib('required',true);
		}

		$blockDb = new GeneralSetup_Model_DbTable_Landscapeblock();
		$blocks = $blockDb->getBlocksByLandscape($this->landscapeId);
		$this->blockid->addMultiOption(null,"-- Please Select --");	
		foreach( $blocks as $block )
		{
			$this->blockid->addMultiOption($block['idblock'],$block['blockname']);	
		}

		//Semester Count
		$this->addElement('select','semesterid', array(
			'label'=>$this->getView()->translate('Semester')
		));	
		
		if($this->idBlock){
			$this->semesterid->setAttrib('disabled','disabled');
		}else{
			$this->semesterid->setAttrib('required',true);
		}
       
		$this->semesterid->addMultiOption(null,"-- Please Select --");		
		for($i=1; $i<=$this->SemsterCount; $i++){
			$this->semesterid->addMultiOption($i,$i);
		}
        		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));       
       
        
        $this->addDisplayGroup(array('save'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        
       
        
	}
	
	
}
?>