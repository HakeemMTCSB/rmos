<?php
class GeneralSetup_Form_Batch extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
//        $this->setAttrib('id','search_registry');

        //batch id
        $this->addElement('text','BatchCode', array(
            'label'=>'Batch Code',
            'class'=>'input-txt'
        ));


        $this->addElement('select','BatchStatus', array(
            'label'=>'Status',
            'required'=>false
        ));

        $this->BatchStatus->addMultiOption("ACTIVE","ACTIVE");
        $this->BatchStatus->addMultiOption("INACTIVE","INACTIVE");

        $this->addElement('submit', 'save', array(
            'label'=>'Search',
            'decorators'=>array('ViewHelper')
        ));

        $this->addElement('reset', 'clear', array(
            'label'=>'Clear',
            'onclick'=>"window.location=window.location;",
            'decorators'=>array('ViewHelper'),
        ));

        $this->addDisplayGroup(array('save','clear'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));
    }
}