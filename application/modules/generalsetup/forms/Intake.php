<?php
class GeneralSetup_Form_Intake extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		$lobjsemester = new GeneralSetup_Model_DbTable_Semester();
		$lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster();
		$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();
		$lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$lobjprogram = new GeneralSetup_Model_DbTable_Program();

		$IdIntake = new Zend_Form_Element_Hidden('IdIntake');
		$IdIntake->removeDecorator("DtDdWrapper");
		$IdIntake->removeDecorator("Label");
		$IdIntake->removeDecorator('HtmlTag');

		//Intake Code
		$IntakeCode = new Zend_Form_Element_Text('IntakeId');
		$IntakeCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$IntakeCode->setAttrib('required',"true")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		//Intake Description
		$IntakeDescription = new Zend_Form_Element_Text('IntakeDesc');
		$IntakeDescription->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$IntakeDescription->setAttrib('required',"true")
		->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		//Intake Translation
		$IntakeTranslation = new Zend_Form_Element_Text('IntakeDescMalay');
		$IntakeTranslation->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$IntakeTranslation->setAttrib('required',"false")
		->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		//sem seq tagging
		$sem_seq = new Zend_Dojo_Form_Element_FilteringSelect('sem_seq');
		$sem_seq->removeDecorator("DtDdWrapper");
		$sem_seq->setAttrib('required',"false") ;
		$sem_seq->removeDecorator("Label");
		$sem_seq->removeDecorator('HtmlTag');
		$sem_seq->setRegisterInArrayValidator(false);
		$sem_seq->setAttrib('dojoType',"dijit.form.FilteringSelect");
			
		/*$sem_seq->addMultiOption('JAN','JANUARY');
                $sem_seq->addMultiOption('FEB','FEBRUARY');
                $sem_seq->addMultiOption('MAR','MARCH');
                $sem_seq->addMultiOption('APR','APRIL');
                $sem_seq->addMultiOption('MAY','MAY');
		$sem_seq->addMultiOption('JUN','JUNE');
                $sem_seq->addMultiOption('JUL','JULAI');
                $sem_seq->addMultiOption('AUG','AUGUST');
		$sem_seq->addMultiOption('SEP','SEPTEMBER');
                $sem_seq->addMultiOption('OCT','OCTOBER');
                $sem_seq->addMultiOption('NOV','NOVEMBER');
                $sem_seq->addMultiOption('DEC','DECEMBER');*/
        $sem_seq->addMultiOption(null, "Please Select");
        $sem_seq->addMultiOption('1','1');
        $sem_seq->addMultiOption('2','2');
        $sem_seq->addMultiOption('3','3');
        $sem_seq->addMultiOption('4','4');
        $sem_seq->addMultiOption('5','5');

		$ApplicationStartDate  = new Zend_Form_Element_Text('ApplicationStartDate');
		//$ApplicationStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$ApplicationStartDate->setAttrib('onChange',"dijit.byId('ApplicationEndDate').constraints.min = arguments[0];") ;
		$ApplicationStartDate->setAttrib('class', 'dojoinput datepicker');
		$ApplicationStartDate->removeDecorator("DtDdWrapper");
		//$ApplicationStartDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$ApplicationStartDate->removeDecorator("Label");
		$ApplicationStartDate->removeDecorator('HtmlTag');

		$ApplicationEndDate  = new Zend_Form_Element_Text('ApplicationEndDate');
		//$ApplicationEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$ApplicationEndDate->setAttrib('onChange',"dijit.byId('ApplicationStartDate').constraints.max = arguments[0];") ;
		$ApplicationEndDate->removeDecorator("DtDdWrapper");
		//$ApplicationEndDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$ApplicationEndDate->setAttrib('class', 'dojoinput datepicker');
		$ApplicationEndDate->removeDecorator("Label");
		$ApplicationEndDate->removeDecorator('HtmlTag');

		$AdmissionStartDate  = new Zend_Form_Element_Text('AdmissionStartDate');
		//$AdmissionStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$AdmissionStartDate->setAttrib('onChange',"dijit.byId('AdmissionEndDate').constraints.min = arguments[0];") ;
		$AdmissionStartDate->setAttrib('class', 'dojoinput datepicker');
		$AdmissionStartDate->removeDecorator("DtDdWrapper");
		//$AdmissionStartDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$AdmissionStartDate->removeDecorator("Label");
		$AdmissionStartDate->removeDecorator('HtmlTag');

	

		$AdmissionEndDate  = new Zend_Form_Element_Text('AdmissionEndDate');
		//$AdmissionEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$AdmissionEndDate->setAttrib('onChange',"dijit.byId('AdmissionStartDate').constraints.max = arguments[0];") ;
		$AdmissionEndDate->removeDecorator("DtDdWrapper");
		//$AdmissionEndDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$AdmissionEndDate->setAttrib('class', 'dojoinput datepicker');
		$AdmissionEndDate->removeDecorator("Label");
		$AdmissionEndDate->removeDecorator('HtmlTag');

		$Faculty  = new Zend_Dojo_Form_Element_FilteringSelect('Faculty');
		$Faculty->removeDecorator("DtDdWrapper");
		$Faculty->setAttrib('required',"false") ;
		$Faculty->removeDecorator("Label");
		$Faculty->removeDecorator('HtmlTag');
		$Faculty->setRegisterInArrayValidator(false);
		$Faculty->setAttrib('OnChange', 'fnloadPrograms');
		$Faculty->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$larrcollege = $lobjcollegemaster->fnGetListofCollege();
		$Faculty->addMultiOptions($larrcollege);

		$Program  = new Zend_Dojo_Form_Element_FilteringSelect('Program');
		$Program->removeDecorator("DtDdWrapper");
		$Program->setAttrib('required',"false") ;
		$Program->removeDecorator("Label");
		$Program->removeDecorator('HtmlTag');
		$Program->setRegisterInArrayValidator(false);
		$Program->setAttrib('OnChange', 'getProgramScheme()');
		$Program->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$programlists=$lobjprogram->fnGetProgramList();
		$Program->addMultiOptions( $programlists);

		$Branch  = new Zend_Dojo_Form_Element_FilteringSelect('Branch');
		$Branch->removeDecorator("DtDdWrapper");
		$Branch->setAttrib('required',"false") ;
		$Branch->removeDecorator("Label");
		$Branch->removeDecorator('HtmlTag');
		$Branch->setRegisterInArrayValidator(false);
		$Branch->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$larrbranchlist = $lobjintake->fngetBranchList();
		if(count($larrbranchlist) > 0) {
			$larrbranchlist[] = array("key" => "all","value" => "All");
		}
		$Branch->addMultiOptions($larrbranchlist);
		

		//student category
		$studentCategory  = new Zend_Dojo_Form_Element_FilteringSelect('studentCategory');
		$studentCategory->removeDecorator("DtDdWrapper");
		$studentCategory->setAttrib('required',"false") ;
		$studentCategory->removeDecorator("Label");
		$studentCategory->removeDecorator('HtmlTag');
		$studentCategory->setRegisterInArrayValidator(false);
		$studentCategory->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$definationDB = new App_Model_General_DbTable_Definationms();
		$category = $definationDB->getDataByType(95);
		foreach($category as $cat){		
			$studentCategory->addMultiOption($cat['idDefinition'],$cat['DefinitionDesc']);	
		}
		
       //Program Scheme
		$programScheme  = new Zend_Dojo_Form_Element_FilteringSelect('programScheme');
		$programScheme->removeDecorator("DtDdWrapper");
		$programScheme->setAttrib('required',"false") ;
		$programScheme->removeDecorator("Label");
		$programScheme->removeDecorator('HtmlTag');
		$programScheme->setRegisterInArrayValidator(false);
		$programScheme->setAttrib('dojoType',"dijit.form.FilteringSelect");
			
		
		//Quota
		$quota = new Zend_Form_Element_Text('quota');
		$quota->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$quota->setAttrib('required',"false")
		->addValidator('Digits')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
	
		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');

		$UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper");
		$UpdUser->removeDecorator("Label");
		$UpdUser->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		//$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag');
		//->class = "NormalBtn";

		$Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('class', 'NormalBtn');
		$Add->setAttrib('dojoType',"dijit.form.Button");
		$Add->setAttrib('OnClick', 'addentryx()')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType',"dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearpageAdd()');
		$clear->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');



		//Form Elements for Intake Copy
		//Semester Main Code

        $CopyIntakeId = new Zend_Dojo_Form_Element_FilteringSelect('CopyIntakeId');
        $CopyIntakeId->removeDecorator("DtDdWrapper");
        $CopyIntakeId->setAttrib('required',"true");
        $CopyIntakeId->removeDecorator("Label");
        $CopyIntakeId->removeDecorator('HtmlTag');
        $CopyIntakeId->setRegisterInArrayValidator(false);
        $CopyIntakeId->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $larrintakelist2 = $lobjintake->fngetallIntake();
        $CopyIntakeId->addMultiOptions($larrintakelist2);


		$CopyIntakeDescription = new Zend_Form_Element_Text('CopyIntakeDescription');
		$CopyIntakeDescription->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$CopyIntakeDescription->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$CopyApplicationStartDate  = new Zend_Form_Element_Text('CopyApplicationStartDate');
		$CopyApplicationStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
    $CopyApplicationStartDate->setAttrib('onChange',"dijit.byId('CopyApplicationEndDate').constraints.min = arguments[0];") ;
    $CopyApplicationStartDate->setAttrib('required',"true");
    $CopyApplicationStartDate->removeDecorator("DtDdWrapper");
    $CopyApplicationStartDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
    $CopyApplicationStartDate->removeDecorator("Label");
    $CopyApplicationStartDate->removeDecorator('HtmlTag');



		$CopyApplicationEndDate  = new Zend_Form_Element_Text('CopyApplicationEndDate');
		$CopyApplicationEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
    $CopyApplicationEndDate->setAttrib('onChange',"dijit.byId('CopyApplicationStartDate').constraints.max = arguments[0];") ;
    $CopyApplicationEndDate->setAttrib('required',"true");
    $CopyApplicationEndDate->removeDecorator("DtDdWrapper");
    $CopyApplicationEndDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
    $CopyApplicationEndDate->removeDecorator("Label");
    $CopyApplicationEndDate->removeDecorator('HtmlTag');

                $CopyAdmissionStartDate  = new Zend_Form_Element_Text('CopyAdmissionStartDate');
		$CopyAdmissionStartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
    $CopyAdmissionStartDate->setAttrib('onChange',"dijit.byId('CopyAdmissionEndDate').constraints.min = arguments[0];") ;
    $CopyAdmissionStartDate->setAttrib('required',"true");
    $CopyAdmissionStartDate->removeDecorator("DtDdWrapper");
    $CopyAdmissionStartDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
    $CopyAdmissionStartDate->removeDecorator("Label");
    $CopyAdmissionStartDate->removeDecorator('HtmlTag');



		$CopyAdmissionEndDate  = new Zend_Form_Element_Text('CopyAdmissionEndDate');
		$CopyAdmissionEndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
    $CopyAdmissionEndDate->setAttrib('onChange',"dijit.byId('CopyAdmissionStartDate').constraints.max = arguments[0];") ;
    $CopyAdmissionEndDate->setAttrib('required',"true");
    $CopyAdmissionEndDate->removeDecorator("DtDdWrapper");
    $CopyAdmissionEndDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
    $CopyAdmissionEndDate->removeDecorator("Label");
    $CopyAdmissionEndDate->removeDecorator('HtmlTag');


		$FromIntake = new Zend_Dojo_Form_Element_FilteringSelect('FromIntake');
		$FromIntake->removeDecorator("DtDdWrapper");
		$FromIntake->setAttrib('required',"true");
		$FromIntake->removeDecorator("Label");
		$FromIntake->removeDecorator('HtmlTag');
		//$FromIntake->setRegisterInArrayValidator(false);
		$FromIntake->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$larrintakelist = $lobjintake->fngetallIntake();
		$FromIntake->addMultiOptions($larrintakelist);


		//Copy Button
		$Copy = new Zend_Form_Element_Submit('Copy');
		$Copy->label = $gstrtranslate->_("Copy");
		//$Copy->dojotype="dijit.form.Button";
		$Copy->removeDecorator("DtDdWrapper");
		$Copy->removeDecorator('HtmlTag');
		//->class = "NormalBtn";
		
		
		
		/* registration location element */
		
		$rl_branch  = new Zend_Dojo_Form_Element_FilteringSelect('rl_branch');
		$rl_branch->removeDecorator("DtDdWrapper");
		$rl_branch->setAttrib('required',"true") ;
		$rl_branch->removeDecorator("Label");
		$rl_branch->removeDecorator('HtmlTag');
		$rl_branch->setRegisterInArrayValidator(false);
		$rl_branch->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$larrbranchlist = $lobjintake->fngetBranchList();
		if(count($larrbranchlist) > 0) {
			$larrbranchlist[] = array("key" => "all","value" => "All");
		}
		$rl_branch->addMultiOptions($larrbranchlist);
		

		$rl_date  = new Zend_Form_Element_Text('rl_date');
		//$rl_date->setAttrib('dojoType',"dijit.form.DateTextBox");       
        $rl_date->setAttrib('required',"false");
        $rl_date->removeDecorator("DtDdWrapper");
        //$rl_date->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
        $rl_date->setAttrib('class', 'dojoinput datepicker');
		$rl_date->removeDecorator("Label");
        $rl_date->removeDecorator('HtmlTag');
                
        
        $rl_time = new Zend_Form_Element_Text('rl_time');
		$rl_time->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$rl_time->setAttrib('required',"false")
				   ->setAttrib('maxlength','50')
				   ->removeDecorator("DtDdWrapper")
				   ->removeDecorator("Label")
				   ->removeDecorator('HtmlTag');
	
        
		$rl_address = new Zend_Form_Element_Textarea('rl_address');
		$rl_address->setAttrib('dojoType',"dijit.form.SimpleTextarea");
		$rl_address->setAttrib('required',"true")
				   ->setAttrib('maxlength','150')
				    ->setAttrib('cols', '40')
					->setAttrib('rows', '4')
				   ->removeDecorator("DtDdWrapper")
				   ->removeDecorator("Label")
				   ->removeDecorator('HtmlTag');
                
                $apply_online = new Zend_Form_Element_Checkbox('apply_online');
                $apply_online->setCheckedValue(1);  
                $apply_online->setUncheckedValue(0);
                $apply_online->removeDecorator("DtDdWrapper");
		$apply_online->removeDecorator("Label");
		$apply_online->removeDecorator('HtmlTag');

        $special_intake = new Zend_Form_Element_Checkbox('special_intake');
        $special_intake->setCheckedValue(1);
        $special_intake->setUncheckedValue(0);
        $special_intake->removeDecorator("DtDdWrapper");
        $special_intake->removeDecorator("Label");
        $special_intake->removeDecorator('HtmlTag');

        //intake year
        $IntakeYear = new Zend_Form_Element_Text('sem_year');
		$IntakeYear->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$IntakeYear->setAttrib('required',"false")
		->setAttrib('maxlength','150')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

        /* scheme information screen*/

        //Program Scheme2
        $programScheme2  = new Zend_Dojo_Form_Element_FilteringSelect('programScheme2');
        $programScheme2->removeDecorator("DtDdWrapper");
        $programScheme2->setAttrib('required',"false") ;
        $programScheme2->removeDecorator("Label");
        $programScheme2->removeDecorator('HtmlTag');
        $programScheme2->setRegisterInArrayValidator(false);
        $programScheme2->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();
        $scheme = $schemeDb->fngetSchemes();
        foreach($scheme as $data){
            $programScheme2->addMultiOption($data['key'],$data['value']);
        }

        //registration date
        $reg_date = new Zend_Form_Element_Textarea('reg_date');
        $reg_date->setAttrib('dojoType',"dijit.form.SimpleTextarea");
        $reg_date->setAttrib('required',"false")
            ->setAttrib('maxlength','100')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '1')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //registration date Malay
        $reg_date_malay = new Zend_Form_Element_Textarea('reg_date_malay');
        $reg_date_malay->setAttrib('dojoType',"dijit.form.SimpleTextarea");
        $reg_date_malay->setAttrib('required',"false")
            ->setAttrib('maxlength','100')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '1')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //registration time
        $reg_time = new Zend_Form_Element_Textarea('reg_time');
        $reg_time->setAttrib('dojoType',"dijit.form.SimpleTextarea");
        $reg_time->setAttrib('required',"false")
            ->setAttrib('maxlength','100')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '1')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //registration time Malay
        $reg_time_malay = new Zend_Form_Element_Textarea('reg_time_malay');
        $reg_time_malay->setAttrib('dojoType',"dijit.form.SimpleTextarea");
        $reg_time_malay->setAttrib('required',"false")
            ->setAttrib('maxlength','100')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '1')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //start semester date
        $start_reg_date = new Zend_Form_Element_Text('start_reg_date');
        $start_reg_date->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $start_reg_date->setAttrib('required',"false")
            ->setAttrib('maxlength','30')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //start semester date malay
        $start_reg_date_malay = new Zend_Form_Element_Text('start_reg_date_malay');
        $start_reg_date_malay->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $start_reg_date_malay->setAttrib('required',"false")
            ->setAttrib('maxlength','30')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //start briefing date
        $brief_date = new Zend_Form_Element_Text('brief_date');
        $brief_date->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $brief_date->setAttrib('required',"false")
            ->setAttrib('maxlength','30')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //start briefing date malay
        $brief_date_malay = new Zend_Form_Element_Text('brief_date_malay');
        $brief_date_malay->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $brief_date_malay->setAttrib('required',"false")
            ->setAttrib('maxlength','30')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //start briefing time
        $brief_time = new Zend_Form_Element_Text('brief_time');
        $brief_time->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $brief_time->setAttrib('required',"false")
            ->setAttrib('maxlength','30')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //start briefing time malay
        $brief_time_malay = new Zend_Form_Element_Text('brief_time_malay');
        $brief_time_malay->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $brief_time_malay->setAttrib('required',"false")
            ->setAttrib('maxlength','30')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');



        $this->addElements(array($IntakeCode,
		$IntakeDescription,
		$sem_seq,
		$IntakeTranslation,
		$ApplicationStartDate,
		$ApplicationEndDate,
        $AdmissionStartDate,
        $AdmissionEndDate,
		$Faculty,
		$Program,
		$Branch,
		$Add,
		$clear,
		$UpdDate,
		$UpdUser,
		$IdIntake,
		$Save,
		$CopyIntakeId,
		$CopyIntakeDescription,
		$CopyApplicationStartDate,
		$CopyApplicationEndDate,
        $CopyAdmissionStartDate,
        $CopyAdmissionEndDate,
		$FromIntake,
		$Copy,
		$studentCategory,	
		$quota,
		$programScheme,
		$rl_branch,
		$rl_date,
		$rl_time,
		$rl_address,
            $apply_online,
                $IntakeYear,
            $special_intake,
            $programScheme2,
            $reg_date,
            $reg_date_malay,
            $reg_time,
            $reg_time_malay,
            $start_reg_date,
            $start_reg_date_malay,
            $brief_date,
            $brief_date_malay,
            $brief_time,
            $brief_time_malay
		));

	}

}