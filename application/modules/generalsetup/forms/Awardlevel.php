<?php
class GeneralSetup_Form_Awardlevel extends Zend_Form
{


    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('Id', 'form_award_level');

        /*$this->addElement('text', 'GradeId',
            array(
                'label' => 'Grade Id',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );*/

        $this->addElement('text', 'GradeDesc',
            array(
                'label' => 'Description',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        $this->addElement('text', 'GradeDesc_Malay',
            array(
                'label' => 'Description (Malay)',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        $this->addElement('select', 'GradeType',
            array(
                'label' => 'Type',
                'class' => 'input-txt'
            )
        );


        $progType = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $listData = $progType->getDataByCodeType('program-type');

        $this->GradeType->addMultiOption(null, "Please Select");

        foreach ($listData as $list) {
            $this->GradeType->addMultiOption($list['key'], $list['value']);
        }

        /*$this->addElement('checkbox', 'ac_status',
            array(
                'label' => 'Active',
            )
        );*/


        //button
        $this->addElement('submit', 'save', array(
            'label' => 'Submit',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label' => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick' => "window.location ='" . $this->getView()->url(array('module' => 'generalsetup', 'controller' => 'awardlevel', 'action' => 'index'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}
?>