<?php

class GeneralSetup_Form_DisabilitySubmit extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
//		$this->setAttrib('id','form_registry_type');

        //disable id
        $this->addElement('text','disable_id', array(
            'label'=>'Disable ID',
            'class'=>'input-txt'
        ));

        //disable desc
        $this->addElement('text','disable_desc_eng', array(
            'label'=>'Name',
            'class'=>'input-txt'
        ));

        //disable desc english
        $this->addElement('text','disable_desc', array(
            'label'=>'Name(Malay)',
            'class'=>'input-txt'
        ));

        //disable condition
        $registryDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $this->addElement('select','disable_condition', array(
            'label'=>'Disable Condition',
            'required'=>false
        ));

        $this->disable_condition->addMultiOption(null,"-- Any --");
        foreach($registryDb->getDataByCodeType('disability-condition')  as $condition){
            $this->disable_condition->addMultiOption($condition["key"],$condition["value"]);
        }

        $this->addElement('checkbox', 'active', array(
            'label'      => 'Active',
            'required'   => false,
            'checked' => true,
        ));

		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'race','action'=>'index'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>