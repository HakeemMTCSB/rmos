<?php
class GeneralSetup_Form_AwardlevelSearch extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','form_search_award');

		//name
		$this->addElement('text','GradeDesc', array(
			'label'=>'Description',
			'class'=>'input-txt'
		));

        $this->addElement('text','GradeDesc_Malay', array(
            'label'=>'Description (Malay)',
            'class'=>'input-txt'
        ));

        $this->addElement('select', 'GradeType',
            array(
                'label' => 'Type',
                'class' => 'input-txt'
            )
        );

        $progType = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $listData = $progType->getDataByCodeType('program-type');

        $this->GradeType->addMultiOption(null, "Please Select");

        foreach ($listData as $list) {
            $this->GradeType->addMultiOption($list['key'], $list['value']);
        }


        //button

		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}