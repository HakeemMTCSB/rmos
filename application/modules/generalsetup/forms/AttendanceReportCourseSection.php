<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Form_AttendanceReportCourseSection extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $formData = $this->getAttrib('formData');
        
        //model
        $model = new GeneralSetup_Model_DbTable_AttendanceReport();
        
        //semester
        $semester = new Zend_Form_Element_Select('semester');
	$semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('required', true);
	$semester->removeDecorator("Label");
        $semester->setAttrib('onchange', 'getProgram(this.value);');
        
        $semester->addMultiOption('', '-- Select --');
        
        $semesterList = $model->getSemesterList();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
            }
        }
        
        //program
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('required', true);
	$program->removeDecorator("Label");
        $program->setAttrib('onchange', 'getCourse(this.value);');
        
        $program->addMultiOption('', '-- Select --');
        
        //course
        $course = new Zend_Form_Element_Select('course');
	$course->removeDecorator("DtDdWrapper");
        $course->setAttrib('class', 'select');
        $course->setAttrib('required', true);
	$course->removeDecorator("Label");
        $course->setAttrib('onchange', 'getCourseSection(this.value);');
        
        $course->addMultiOption('', '-- Select --');
        
        //course section
        $coursesection = new Zend_Form_Element_Select('coursesection');
	$coursesection->removeDecorator("DtDdWrapper");
        $coursesection->setAttrib('class', 'select');
        $coursesection->setAttrib('required', true);
	$coursesection->removeDecorator("Label");
        $coursesection->setAttrib('onchange', 'getItem(this.value); getLecturer(this.value);');
        
        $coursesection->addMultiOption('', '-- Select --');
        
        //lecturer
        $lecturer = new Zend_Form_Element_Select('lecturer');
	$lecturer->removeDecorator("DtDdWrapper");
        $lecturer->setAttrib('class', 'select');
        //$lecturer->setAttrib('required', true);
	$lecturer->removeDecorator("Label");
        //$lecturer->setAttrib('onchange', 'getItem(this.value);');
        
        $lecturer->addMultiOption('', '-- Select --');
        
        //registration item
        $item = new Zend_Form_Element_Select('item');
	$item->removeDecorator("DtDdWrapper");
        $item->setAttrib('class', 'select');
        //$item->setAttrib('required', true);
	$item->removeDecorator("Label");
        //$item->setAttrib('onchange', 'getItem(this.value);');
        
        $item->addMultiOption('', '-- Select --');
        
        if ($formData){
            if (isset($formData['semester'])){
                $semesterInfo = $model->getSemesterById($formData['semester']);
                $programList = $model->getProgramByScheme($semesterInfo['IdScheme']);
                
                if ($programList){
                    foreach ($programList as $programLoop){
                        $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
                    }
                }
            }
            
            if (isset($formData['program'])){
                $courseList = $model->getSubjectFromLandscape($formData['program']);
                
                if ($courseList){
                    foreach ($courseList as $courseLoop){
                        $course->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
                    }
                }
            }
            
            if (isset($formData['course']) && isset($formData['semester']) || isset($formData['program'])){
                $courseSectionList = $model->getCourseSection($formData['program'], $formData['semester'], $formData['course']);
                
                if ($courseSectionList){
                    foreach ($courseSectionList as $courseSectionLoop){
                        $coursesection->addMultiOption($courseSectionLoop['IdCourseTaggingGroup'], $courseSectionLoop['GroupCode'].' - '.$courseSectionLoop['GroupName']);
                    }
                }
            }
            
            if (isset($formData['coursesection'])){
                $lecturerList = $model->getLecturer($formData['coursesection']);
                
                if ($lecturerList){
                    foreach ($lecturerList as $lecturerLoop){
                        $lecturer->addMultiOption($lecturerLoop['IdStaff'], $lecturerLoop['FullName']);
                    }
                }
                
                $registrationItemList = $model->getRegistrationItem($formData['coursesection']);
                
                if ($registrationItemList){
                    foreach ($registrationItemList as $registrationItemLoop){
                        $item->addMultiOption($registrationItemLoop['idDefinition'], $registrationItemLoop['DefinitionDesc']);
                    }
                }
            }
        }
        
        $this->addElements(array(
            $semester,
            $program,
            $course,
            $coursesection,
            $lecturer,
            $item
        ));
    }
}