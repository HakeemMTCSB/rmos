<?php

class GeneralSetup_Form_BatchProgram extends Zend_Dojo_Form
{ //Formclass for the Programmaster	 module
    public function init()
    {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $BatchCode = new Zend_Form_Element_Text('BatchCode');
        $BatchCode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $BatchCode->setAttrib('required', "true")
            ->setAttrib('maxlength', '200')
            ->setAttrib('style', 'width:800px')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($BatchCode));

    }
}