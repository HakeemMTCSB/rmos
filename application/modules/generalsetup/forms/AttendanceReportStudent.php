<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Form_AttendanceReportStudent extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $formData = $this->getAttrib('formData');
        
        //model
        $model = new GeneralSetup_Model_DbTable_AttendanceReport();
        $model2 = new Records_Model_DbTable_ChangeMode();
        
        //semester
        $semester = new Zend_Form_Element_Select('semester');
	$semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        //$semester->setAttrib('required', true);
	$semester->removeDecorator("Label");
        $semester->setAttrib('onchange', 'getProgram(this.value);');
        
        $semester->addMultiOption('', '-- Select --');
        
        $semesterList = $model->getSemesterList();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
            }
        }
        
        //program
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        //$program->setAttrib('required', true);
	$program->removeDecorator("Label");
        $program->setAttrib('onchange', 'getCourse(this.value); getProgramScheme(this.value);');
        
        $program->addMultiOption('', '-- Select --');
        
        //programscheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        //$programscheme->setAttrib('required', true);
	$programscheme->removeDecorator("Label");
        //$programscheme->setAttrib('onchange', 'getCourse(this.value);');
        
        $programscheme->addMultiOption('', '-- Select --');
        
        //course
        $course = new Zend_Form_Element_Select('course');
	$course->removeDecorator("DtDdWrapper");
        $course->setAttrib('class', 'select');
        //$course->setAttrib('required', true);
	$course->removeDecorator("Label");
        //$course->setAttrib('onchange', 'getCourseSection(this.value);');
        
        $course->addMultiOption('', '-- Select --');
        
        //studentid
        $studentid = new Zend_Form_Element_Text('studentid');
	$studentid->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        if ($formData){
            if (isset($formData['semester'])){
                $semesterInfo = $model->getSemesterById($formData['semester']);
                
                if ($semesterInfo){
                    $programList = $model->getProgramByScheme($semesterInfo['IdScheme']);

                    if ($programList){
                        foreach ($programList as $programLoop){
                            $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
                        }
                    }
                }
            }else{
                $programList = $model->getProgramList();

                if ($programList){
                    foreach ($programList as $programLoop){
                        $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
                    }
                }
            }
            
            if (isset($formData['program'])){
                $courseList = $model->getSubjectFromLandscape($formData['program']);
                
                if ($courseList){
                    foreach ($courseList as $courseLoop){
                        $course->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
                    }
                }
                
                $programSchemeList = $model2->getProgramSchemeBasedOnProgram($formData['program']);
                
                if ($programSchemeList){
                    foreach ($programSchemeList as $programSchemeLoop){
                        $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop_name'].' '.$programSchemeLoop['mos_name'].' '.$programSchemeLoop['programType']);
                    }
                }
            }
        }else{
            $programList = $model->getProgramList();

            if ($programList){
                foreach ($programList as $programLoop){
                    $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
                }
            }
        }
        
        //student name
        $studentname = new Zend_Form_Element_Text('studentname');
	$studentname->setAttrib('class', 'input-txt')
            //->setAttrib("disable", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $this->addElements(array(
            $semester,
            $program,
            $programscheme,
            $course,
            $studentid,
            $studentname
        ));
    }
}