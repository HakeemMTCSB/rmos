<?php 

class GeneralSetup_Form_SearchCourse extends Zend_Form
{		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
                
	   //Semester
	   $this->addElement('select','IdSemester', array(
	                     'label'=>$this->getView()->translate('Semester'),
	                     'required'=>true,	                     
	                     'onchange'=>'checkOpenRegister(this.value)'
	    ));
                    
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
		}

		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),
		    'required'=>false,
		    'onchange'=>'getProgramScheme(this);'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}

		 //Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
		    'required'=>false
		));
		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));			
		
		$this->IdProgramScheme->setRegisterInArrayValidator(false);
		
		$this->addElement('text','SubjectName', array(
			'label'=>'Subject Name'
		));
		
		//name
		$this->addElement('text','SubjectCode', array(
			'label'=>'Course Code'
		));
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>