<?php 
class GeneralSetup_Form_TopMenu extends Zend_Form{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','top_menu_form');
		
		$this->addElement('text','tm_name', array(
			'label'=>'Menu Name',
			'required'=>'true'
		));
		
		$this->addElement('textarea','tm_desc', array(
			'label'=>'Description'
		));
		
		/*$this->addElement('text','tm_seq_order', array(
			'label'=>'Sequence Order'));
		$this->tm_seq_order->addValidator('Digits');
		*/
		
		$this->addElement('text','tm_module', array(
				'label'=>'Module',
				'required'=>'true'
		));
		
		$this->addElement('text','tm_controller', array(
				'label'=>'Controller',
				'required'=>'true'
		));
		
		$this->addElement('text','tm_action', array(
				'label'=>'Action',
				'required'=>'true'
		));
		
		$this->addElement('radio','tm_visibility', array(
			'label'=>'Visibility'));
		
		$this->tm_visibility->addMultiOptions(array(
			'1' => 'Show',
			'0' => 'Hide'
		))->setSeparator('&nbsp;&nbsp;')->setValue("1");

		$this->addElement('text','tm_seq_order', array(
		   'label' => 'Order',
           'required' => true
        ));

		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'menu','action'=>'menu'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>