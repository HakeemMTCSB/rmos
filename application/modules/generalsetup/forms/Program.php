<?php

class GeneralSetup_Form_Program extends Zend_Dojo_Form
{ //Formclass for the Programmaster	 module
    public function init()
    {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $IdProgram = new Zend_Form_Element_Hidden('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');

        $ActiveDB = new Zend_Form_Element_Hidden('ActiveDB');
        $ActiveDB->removeDecorator("DtDdWrapper");
        $ActiveDB->removeDecorator("Label");
        $ActiveDB->removeDecorator('HtmlTag');

        $IdHistory = new Zend_Form_Element_Hidden('IdHistory');
        $IdHistory->removeDecorator("DtDdWrapper");
        $IdHistory->removeDecorator("Label");
        $IdHistory->removeDecorator('HtmlTag');

        $IdProgramMajoring = new Zend_Form_Element_Hidden('IdProgramMajoring');
        $IdProgramMajoring->removeDecorator("DtDdWrapper");
        $IdProgramMajoring->removeDecorator("Label");
        $IdProgramMajoring->removeDecorator('HtmlTag');

        $IdProgramQuota = new Zend_Form_Element_Text('IdProgramQuota');
        $IdProgramQuota->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $IdProgramQuota->removeDecorator("DtDdWrapper");
        $IdProgramQuota->removeDecorator("Label");
        $IdProgramQuota->removeDecorator('HtmlTag');

        $ProgramName = new Zend_Form_Element_Text('ProgramName');
        $ProgramName->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramName->setAttrib('required', "true")
            ->setAttrib('maxlength', '200')
            ->setAttrib('style', 'width:800px')
            //->setAttrib('propercase','true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $IdMajor = new Zend_Form_Element_Text('IdMajor');
        $IdMajor->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $IdMajor->setAttrib('required', "false")
            ->setAttrib('maxlength', '100')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ProgramCode = new Zend_Form_Element_Text('ProgramCode');
        $ProgramCode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramCode->setAttrib('required', "true")
            ->setAttrib('maxlength', '20')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MinimumAge = new Zend_Form_Element_Text('MinimumAge', array('regExp' => '[1-9]+[0-9]*', 'invalidMessage' => "Numbers Only"));
        $MinimumAge->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $MinimumAge->setAttrib('required', "true")
            ->setAttrib('maxlength', '2')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        //-------------------
        $FrontSalutation = new Zend_Dojo_Form_Element_FilteringSelect('programSalutation');
        $FrontSalutation->setAttrib('required', "false");
        $FrontSalutation->removeDecorator("DtDdWrapper");
        $FrontSalutation->removeDecorator("Label");
        $FrontSalutation->removeDecorator('HtmlTag');
        $FrontSalutation->setAttrib('dojoType', "dijit.form.FilteringSelect");
        //--------------------		
        /*$ArabicName = new Zend_Form_Element_Text('ArabicName');
     $ArabicName->setAttrib('dojoType',"dijit.form.ValidationTextBox")
                     ->setAttrib('maxlength','100')
                     ->removeDecorator("DtDdWrapper")
                     ->removeDecorator("Label")
                     ->removeDecorator('HtmlTag');*/

        $MalayName = new Zend_Form_Element_Text('MalayName');
        $MalayName->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            ->setAttrib('maxlength', '500')
            ->setAttrib('style', 'width:800px')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        $ShortName = new Zend_Form_Element_Text('ShortName');
        $ShortName->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ShortName->setAttrib('required', "true")
            ->setAttrib('maxlength', '20')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ProgramMoheField = new Zend_Form_Element_Text('ProgramMoheField');
        $ProgramMoheField->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramMoheField->setAttrib('required', "false")
            ->setAttrib('maxlength', '100')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ProgramMoheSubfield = new Zend_Form_Element_Text('ProgramMoheSubfield');
        $ProgramMoheSubfield->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramMoheSubfield->setAttrib('required', "false")
            ->setAttrib('maxlength', '100')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ProgramMoheFieldId = new Zend_Form_Element_Text('ProgramMoheFieldId');
        $ProgramMoheFieldId->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramMoheFieldId->setAttrib('required', "false")
            ->setAttrib('maxlength', '5')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ProgramMoheSubfieldId = new Zend_Form_Element_Text('ProgramMoheSubfieldId');
        $ProgramMoheSubfieldId->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramMoheSubfieldId->setAttrib('required', "false")
            ->setAttrib('maxlength', '5')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ProgramApproved = new Zend_Form_Element_Text('ProgramApproved');
        $ProgramApproved->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ProgramApproved->setAttrib('required', "false")
            ->setAttrib('maxlength', '100')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //$Duration = new Zend_Form_Element_Text('Duration',array('regExp'=>'[1-9]+[0-9]*[.]?[0-9]?','invalidMessage'=>"Numbers Only"));

        $Duration = new Zend_Form_Element_Text('Duration', array('regExp' => '\d+(\.\d{1,2})?', 'invalidMessage' => "Decimal Only"));
        $Duration->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $Duration->setAttrib('required', "true")
            ->setAttrib('maxlength', '3')
            ->setAttrib('onblur', 'getYear(this,"#durationText","#MinYear","#MinMonth")')
            ->setAttribs(array('style' => 'width:30px;'))
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MinYear = new Zend_Form_Element_Hidden('MinYear');
        $MinMonth = new Zend_Form_Element_Hidden('MinMonth');

        $OptimalDuration = new Zend_Form_Element_Text('OptimalDuration', array('regExp' => '\d+(\.\d{1,2})?', 'invalidMessage' => "Decimal Only"));
        $OptimalDuration->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $OptimalDuration->setAttrib('required', "true")
            ->setAttrib('maxlength', '3')
            ->setAttrib('onblur', 'getYear(this,"#optdurationText","#MaxYear","#MaxMonth")')
            ->setAttribs(array('style' => 'width:30px;'))
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MaxYear = new Zend_Form_Element_Hidden('MaxYear');
        $MaxMonth = new Zend_Form_Element_Hidden('MaxMonth');

        $DurationType = new Zend_Dojo_Form_Element_FilteringSelect('DurationType');
        $DurationType->removeDecorator("DtDdWrapper");
        $DurationType->removeDecorator("Label");
        $DurationType->removeDecorator('HtmlTag');
        $DurationType->addMultiOptions(array('1' => 'Semester', '2' => 'Year'));
        $DurationType->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $OptDurationType = new Zend_Dojo_Form_Element_FilteringSelect('OptDurationType');
        $OptDurationType->removeDecorator("DtDdWrapper");
        $OptDurationType->removeDecorator("Label");
        $OptDurationType->removeDecorator('HtmlTag');
        $OptDurationType->addMultiOptions(array('1' => 'Semester', '2' => 'Year'));
        $OptDurationType->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $TotalCreditHours = new Zend_Form_Element_Text('TotalCreditHours', array('regExp' => '[1-9]+[0-9]*[.]?[0-9]*', 'invalidMessage' => "Numbers Only"));
        $TotalCreditHours->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $TotalCreditHours->setAttrib('required', "false")
            ->setAttrib('maxlength', '6')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $InternalExternal = new Zend_Dojo_Form_Element_FilteringSelect('InternalExternal');
        $InternalExternal->removeDecorator("DtDdWrapper");
        $InternalExternal->removeDecorator("Label");
        $InternalExternal->removeDecorator('HtmlTag');
        $InternalExternal->addMultiOptions(array('1' => 'Internal', '2' => 'External'));
        $InternalExternal->setAttrib('dojoType', "dijit.form.FilteringSelect");

        /*        $IdCourseMaster = new Zend_Dojo_Form_Element_FilteringSelect('IdCourseMaster');
                $IdCourseMaster->removeDecorator("DtDdWrapper");
                $IdCourseMaster->setAttrib('required',"true") ;
                $IdCourseMaster->removeDecorator("Label");
                $IdCourseMaster->removeDecorator('HtmlTag');
                $IdCourseMaster->setRegisterInArrayValidator(false);
                $IdCourseMaster->setAttrib('dojoType',"dijit.form.FilteringSelect");*/


        $LearningMode = new Zend_Form_Element_MultiCheckbox('LearningMode');
        $LearningMode->removeDecorator("DtDdWrapper");
        $LearningMode->setAttrib('required', "true");
        $LearningMode->removeDecorator("Label");
        $LearningMode->removeDecorator('HtmlTag');
        $LearningMode->setSeparator('<br/>');
        $LearningMode->setRegisterInArrayValidator(false);
        $LearningMode->setAttrib('dojoType', "dijit.form.CheckBox");

        $Award = new Zend_Dojo_Form_Element_FilteringSelect('Award');
        $Award->removeDecorator("DtDdWrapper");
        $Award->setAttrib('required', "true");
        $Award->removeDecorator("Label");
        $Award->removeDecorator('HtmlTag');
        $Award->setRegisterInArrayValidator(false);
        //$Award->setAttrib('onChange','fnGetAwardCode(this.value)');
        $Award->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $schemeDbObject = new GeneralSetup_Model_DbTable_Schemesetup();
        $schemeList = $schemeDbObject->fngetSchemes();

        $IdScheme = new Zend_Dojo_Form_Element_FilteringSelect('IdScheme');
        $IdScheme->removeDecorator("DtDdWrapper");
        $IdScheme->removeDecorator("Label");
        $IdScheme->removeDecorator('HtmlTag');
        $IdScheme->addMultiOptions($schemeList);
        $IdScheme->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $Active = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType', "dijit.form.CheckBox");
        //$Active->setAttrib('OnClick', 'checkhistory()');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');

        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype = "dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
            ->class = "NormalBtn";

        $Add = new Zend_Form_Element_Button('Add');
        $Add->dojotype = "dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('OnClick', 'QuotaDetails()')
            ->setValue('Add')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add1 = new Zend_Form_Element_Button('Add1');
        $Add1->dojotype = "dijit.form.Button";
        $Add1->label = $gstrtranslate->_("Update");
        $Add1->setAttrib('class', 'NormalBtn');
        $Add1->setAttrib('OnClick', 'QuotaDetails()')
            ->setValue('Add1')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype = "dijit.form.Button";
        $Back->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $AccreditionType = new Zend_Dojo_Form_Element_FilteringSelect('AccreditionType');
        $AccreditionType->removeDecorator("DtDdWrapper");
        $AccreditionType->setAttrib('required', "true");
        $AccreditionType->removeDecorator("Label");
        $AccreditionType->removeDecorator('HtmlTag');
        $AccreditionType->setRegisterInArrayValidator(false);
        $AccreditionType->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $AccreditionType->setAttrib('style', 'width:200px');
        $definationDB = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $AccreditionTypeList = $definationDB->getDataByCodeType('accredition-type');
        //$programMode->addMultiOption(Null, "Please Select");
        foreach ($AccreditionTypeList as $modes) {
            $AccreditionType->addMultiOption($modes['key'], $modes['value']);
        }


        $AccredictionDate = new Zend_Dojo_Form_Element_DateTextBox('AccredictionDate');
        $AccredictionDate->setAttrib('dojoType', "dijit.form.DateTextBox");
        $AccredictionDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $AccredictionDate->setAttrib('required', "false");
        $AccredictionDate->removeDecorator("DtDdWrapper");
        $AccredictionDate->setAttrib('title', "dd-mm-yyyy");
        $AccredictionDate->removeDecorator("Label");
        $AccredictionDate->removeDecorator('HtmlTag');

        $AccredictionReferences = new Zend_Form_Element_Text('AccredictionReferences');
        $AccredictionReferences->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $AccredictionReferences->setAttrib('required', "false")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $AccredictionNumber = new Zend_Form_Element_Text('AccredictionNumber', array('regExp' => '[1-9]+[0-9]*', 'invalidMessage' => "Numbers Only"));
        $AccredictionNumber->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $AccredictionNumber->setAttrib('required', "false")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ValidityFrom = new Zend_Dojo_Form_Element_DateTextBox('ValidityFrom');
        $ValidityFrom->setAttrib('dojoType', "dijit.form.DateTextBox");
        $ValidityFrom->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $ValidityFrom->setAttrib('required', "false");
        $ValidityFrom->removeDecorator("DtDdWrapper");
        $ValidityFrom->setAttrib('title', "dd-mm-yyyy");
        $ValidityFrom->removeDecorator("Label");
        $ValidityFrom->removeDecorator('HtmlTag');

        $ValidityTo = new Zend_Dojo_Form_Element_DateTextBox('ValidityTo');
        $ValidityTo->setAttrib('dojoType', "dijit.form.DateTextBox");
        $ValidityTo->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $ValidityTo->setAttrib('required', "false");
        $ValidityTo->removeDecorator("DtDdWrapper");
        $ValidityTo->setAttrib('title', "dd-mm-yyyy");
        $ValidityTo->removeDecorator("Label");
        $ValidityTo->removeDecorator('HtmlTag');

        $ApprovalDate = new Zend_Dojo_Form_Element_DateTextBox('ApprovalDate');
        $ApprovalDate->setAttrib('dojoType', "dijit.form.DateTextBox");
        $ApprovalDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $ApprovalDate->setAttrib('required', "false");
        $ApprovalDate->removeDecorator("DtDdWrapper");
        $ApprovalDate->setAttrib('title', "dd-mm-yyyy");
        $ApprovalDate->removeDecorator("Label");
        $ApprovalDate->removeDecorator('HtmlTag');


        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType', "dijit.form.Button");
        $Add->setAttrib('OnClick', 'accredictionInsert()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');


        $IdCollege = new Zend_Dojo_Form_Element_FilteringSelect('IdCollege');
        $IdCollege->removeDecorator("DtDdWrapper");
        //$IdCollege->setAttrib('required',"true") ;
        $IdCollege->removeDecorator("Label");
        $IdCollege->removeDecorator('HtmlTag');
        $IdCollege->setRegisterInArrayValidator(false);
        $IdCollege->setAttrib('onChange', 'fnGetColgDeptList');
        $IdCollege->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $IdCollege->setAttrib('style', 'width:800px');

        $IdDepartment = new Zend_Dojo_Form_Element_FilteringSelect('IdDepartment');
        $IdDepartment->removeDecorator("DtDdWrapper");
        $IdDepartment->setAttrib('required', "false");
        $IdDepartment->removeDecorator("Label");
        $IdDepartment->removeDecorator('HtmlTag');
        $IdDepartment->setRegisterInArrayValidator(false);
        //$IdDepartment->setAttrib('onChange','fnGetColgDeptid(this.value)');
        $IdDepartment->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $BahasaDescription = new Zend_Form_Element_Text('BahasaDescription');
        $BahasaDescription->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $BahasaDescription->setAttrib('required', "false")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $EnglishDescription = new Zend_Form_Element_Text('EnglishDescription');
        $EnglishDescription->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $EnglishDescription->setAttrib('required', "false")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MajorCreditComplete = new Zend_Form_Element_Text('MajorCreditComplete', array('regExp' => '[1-9]+[0-9]*[.]?[0-9]*', 'invalidMessage' => "Numbers Only"));
        $MajorCreditComplete->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $MajorCreditComplete->setAttrib('required', "false")
            ->setAttrib('maxlength', '6')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $Insert = new Zend_Form_Element_Button('Insert');
        $Insert->setAttrib('class', 'NormalBtn');
        $Insert->setAttrib('dojoType', "dijit.form.Button");
        $Insert->label = $gstrtranslate->_("Add");
        $Insert->setAttrib('OnClick', 'majoringAdd()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Erase = new Zend_Form_Element_Button('Erase');
        $Erase->setAttrib('class', 'NormalBtn');
        $Erase->setAttrib('dojoType', "dijit.form.Button");
        $Erase->label = $gstrtranslate->_("Clear");
        $Erase->setAttrib('OnClick', 'clearpagemajoring()');
        $Erase->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');


        //Mode of Program
        $programMode = new Zend_Dojo_Form_Element_FilteringSelect('programMode');
        $programMode->removeDecorator("DtDdWrapper");
        $programMode->setAttrib('required', "true");
        $programMode->removeDecorator("Label");
        $programMode->removeDecorator('HtmlTag');
        $programMode->setRegisterInArrayValidator(false);
        $programMode->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $programMode->setAttrib('style', 'width:400px');
        $definationDB = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $programModeList = $definationDB->getDataByCodeType('mode-of-program');
        //$programMode->addMultiOption(Null, "Please Select");
        foreach ($programModeList as $modes) {
            $programMode->addMultiOption($modes['key'], $modes['value']);
        }

        //Mode of Study
        $studyMode = new Zend_Dojo_Form_Element_FilteringSelect('studyMode');
        $studyMode->removeDecorator("DtDdWrapper");
        $studyMode->setAttrib('required', "false");
        $studyMode->removeDecorator("Label");
        $studyMode->removeDecorator('HtmlTag');
        $studyMode->setRegisterInArrayValidator(false);
        $studyMode->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $definationDB = new App_Model_General_DbTable_Definationms();
        $programStudyList = $definationDB->getDataByType(97);
        foreach ($programStudyList as $modes) {
            $studyMode->addMultiOption($modes['idDefinition'], $modes['DefinitionDesc']);
        }

        //Program type
        $programType = new Zend_Dojo_Form_Element_FilteringSelect('programType');
        $programType->removeDecorator("DtDdWrapper");
        //$programType->setAttrib('required', "false");
        $programType->removeDecorator("Label");
        $programType->removeDecorator('HtmlTag');
        $programType->setRegisterInArrayValidator(false);
        $programType->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $programType->setAttrib('style', 'width:400px');
        $definationDB = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $programTypeList = $definationDB->getDataByCodeType('pg-structure');
        $programType->addMultiOption('0', "Please Select");
        foreach ($programTypeList as $modes) {
            $programType->addMultiOption($modes['key'], $modes['value']);
        }

        $Insert_Scheme = new Zend_Form_Element_Button('Insert_Scheme');
        $Insert_Scheme->setAttrib('class', 'NormalBtn');
        $Insert_Scheme->setAttrib('dojoType', "dijit.form.Button");
        $Insert_Scheme->label = $gstrtranslate->_("Add");
        $Insert_Scheme->setAttrib('OnClick', 'addScheme()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Erase_Scheme = new Zend_Form_Element_Button('Erase_Scheme');
        $Erase_Scheme->setAttrib('class', 'NormalBtn');
        $Erase_Scheme->setAttrib('dojoType', "dijit.form.Button");
        $Erase_Scheme->label = $gstrtranslate->_("Clear");
        $Erase_Scheme->setAttrib('OnClick', 'clearscheme()');
        $Erase_Scheme->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $apply_online = new Zend_Form_Element_Checkbox('apply_online');
        $apply_online->setCheckedValue(1);
        $apply_online->setUncheckedValue(0);
        $apply_online->removeDecorator("DtDdWrapper");
        $apply_online->removeDecorator("Label");
        $apply_online->removeDecorator('HtmlTag');

        $IdMinor = new Zend_Form_Element_Text('IdMinor');
        $IdMinor->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $IdMinor->setAttrib('required', "false")
            ->setAttrib('maxlength', '20')
            ->setAttrib('propercase', 'true')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MinorMalayDescription = new Zend_Form_Element_Text('MinorMalayDescription');
        $MinorMalayDescription->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $MinorMalayDescription->setAttrib('required', "false")
            ->setAttrib('maxlength', '200')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MinorEngDescription = new Zend_Form_Element_Text('MinorEngDescription');
        $MinorEngDescription->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $MinorEngDescription->setAttrib('required', "false")
            ->setAttrib('maxlength', '200')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MinorCreditComplete = new Zend_Form_Element_Text('MinorCreditComplete', array('regExp' => '[1-9]+[0-9]*[.]?[0-9]*', 'invalidMessage' => "Numbers Only"));
        $MinorCreditComplete->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $MinorCreditComplete->setAttrib('required', "false")
            ->setAttrib('maxlength', '6')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $InsertMinor = new Zend_Form_Element_Button('InsertMinor');
        $InsertMinor->setAttrib('class', 'NormalBtn');
        $InsertMinor->setAttrib('dojoType', "dijit.form.Button");
        $InsertMinor->label = $gstrtranslate->_("Add");
        $InsertMinor->setAttrib('OnClick', 'minorAdd()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $EraseMinor = new Zend_Form_Element_Button('EraseMinor');
        $EraseMinor->setAttrib('class', 'NormalBtn');
        $EraseMinor->setAttrib('dojoType', "dijit.form.Button");
        $EraseMinor->label = $gstrtranslate->_("Clear");
        $EraseMinor->setAttrib('OnClick', 'clearpageminor()');
        $EraseMinor->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');


        $idStartIntake = new Zend_Dojo_Form_Element_FilteringSelect('idStartIntake');
        $idStartIntake->removeDecorator("DtDdWrapper");
        $idStartIntake->setAttrib('required', "true");
        $idStartIntake->removeDecorator("Label");
        $idStartIntake->removeDecorator('HtmlTag');
        $idStartIntake->setRegisterInArrayValidator(false);
        $idStartIntake->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $idStartIntake->setAttrib('style', 'width:400px');
        $definationDB = new GeneralSetup_Model_DbTable_Intake();
        $idStartIntakeList = $definationDB->fngetallIntake();
        //$programMode->addMultiOption(Null, "Please Select");
        foreach ($idStartIntakeList as $modes) {
            $idStartIntake->addMultiOption($modes['key'], $modes['value']);
        }

        $idEndIntake = new Zend_Dojo_Form_Element_FilteringSelect('idEndIntake');
        $idEndIntake->removeDecorator("DtDdWrapper");
        $idEndIntake->setAttrib('required', "true");
        $idEndIntake->removeDecorator("Label");
        $idEndIntake->removeDecorator('HtmlTag');
        $idEndIntake->setRegisterInArrayValidator(false);
        $idEndIntake->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $idEndIntake->setAttrib('style', 'width:400px');
        $definationDB = new GeneralSetup_Model_DbTable_Intake();
        $idEndIntakeList = $definationDB->fngetallIntake();
        $idEndIntake->addMultiOption(Null, "Please Select");
        foreach ($idEndIntakeList as $modes) {
            $idEndIntake->addMultiOption($modes['key'], $modes['value']);
        }

        $creditLimit = new Zend_Form_Element_Text('creditLimit', array('regExp' => '[1-9]+[0-9]*[.]?[0-9]*', 'invalidMessage' => "Numbers Only"));
        $creditLimit->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $creditLimit->setAttrib('required', "false")
            ->setAttrib('maxlength', '6')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $InsertMajIntake = new Zend_Form_Element_Button('InsertMajIntake');
        $InsertMajIntake->setAttrib('class', 'NormalBtn');
        $InsertMajIntake->setAttrib('dojoType', "dijit.form.Button");
        $InsertMajIntake->label = $gstrtranslate->_("Add");
        $InsertMajIntake->setAttrib('OnClick', 'AddMajorIntake()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $EraseMajIntake = new Zend_Form_Element_Button('EraseMajIntake');
        $EraseMajIntake->setAttrib('class', 'NormalBtn');
        $EraseMajIntake->setAttrib('dojoType', "dijit.form.Button");
        $EraseMajIntake->label = $gstrtranslate->_("Clear");
        $EraseMajIntake->setAttrib('OnClick', 'clearpageMajorIntake()');
        $EraseMajIntake->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $idStruc = new Zend_Dojo_Form_Element_FilteringSelect('idStruc');
        $idStruc->removeDecorator("DtDdWrapper");
        //$programType->setAttrib('required', "false");
        $idStruc->removeDecorator("Label");
        $idStruc->removeDecorator('HtmlTag');
        $idStruc->setRegisterInArrayValidator(false);
        $idStruc->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $idStruc->setAttrib('style', 'width:400px');
        $definationDB = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $idStrucList = $definationDB->getDataByCodeType('pg-structure');
        $idStruc->addMultiOption('0', "Please Select");
        foreach ($idStrucList as $modes) {
            $idStruc->addMultiOption($modes['key'], $modes['value']);
        }

        $InsertMajStruc = new Zend_Form_Element_Button('InsertMajStruc');
        $InsertMajStruc->setAttrib('class', 'NormalBtn');
        $InsertMajStruc->setAttrib('dojoType', "dijit.form.Button");
        $InsertMajStruc->label = $gstrtranslate->_("Add");
        $InsertMajStruc->setAttrib('OnClick', 'AddMajorStruc()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $EraseMajStruc = new Zend_Form_Element_Button('EraseMajStruc');
        $EraseMajStruc->setAttrib('class', 'NormalBtn');
        $EraseMajStruc->setAttrib('dojoType', "dijit.form.Button");
        $EraseMajStruc->label = $gstrtranslate->_("Clear");
        $EraseMajStruc->setAttrib('OnClick', 'clearpageMajorStruc()');
        $EraseMajStruc->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');


        //form elements
        $this->addElements(array($IdProgram, $BahasaDescription, $EnglishDescription,$MajorCreditComplete, $Insert, $Erase,
            $IdMinor,$MinorMalayDescription,$MinorEngDescription,$MinorCreditComplete,$InsertMinor,$EraseMinor,
            $IdProgramQuota,
            $ProgramName, $IdDepartment, $IdCollege, $Add1, $TotalCreditHours, $MinimumAge,
            $MalayName,$ProgramApproved,$ProgramMoheField,$ProgramMoheSubfield,$ProgramMoheSubfieldId,$ProgramMoheFieldId,
            $FrontSalutation,
            $ShortName,
            $ProgramCode,
            $LearningMode,
            $IdHistory,
            $Award, $IdScheme,
            $Active,
            $UpdDate,
            $UpdUser,
            $Save,
            $Back,
            $Add,
            $IdMajor, $IdProgramMajoring,
            $ActiveDB,
            $programMode, $studyMode, $programType, $Insert_Scheme, $Erase_Scheme,
            $AccreditionType, $AccredictionDate, $AccredictionReferences, $AccredictionNumber, $ValidityFrom, $ValidityTo, $ApprovalDate, $clear, $Add,
            $Duration, $OptimalDuration, $DurationType, $OptDurationType, $apply_online,
            $idStartIntake,$idEndIntake,$creditLimit,$InsertMajIntake,$EraseMajIntake,
            $idStruc,$EraseMajStruc,$InsertMajStruc,
            $MinYear, $MinMonth, $MaxYear, $MaxMonth));

    }
}