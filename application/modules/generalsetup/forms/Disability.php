<?php
class GeneralSetup_Form_Disability extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
//        $this->setAttrib('id','search_registry');

        //disable id
        $this->addElement('text','disable_id', array(
            'label'=>'Disable ID',
            'class'=>'input-txt'
        ));

        //disable desc
        $this->addElement('text','disable_desc_eng', array(
            'label'=>'Name',
            'class'=>'input-txt'
        ));

        //disable desc english
        $this->addElement('text','disable_desc', array(
            'label'=>'Name(Malay)',
            'class'=>'input-txt'
        ));

        //disable condition
        $registryDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $this->addElement('select','disable_condition', array(
            'label'=>'Disable Condition',
            'required'=>false
        ));

        $this->disable_condition->addMultiOption(null,"-- Any --");
        foreach($registryDb->getDataByCodeType('disability-condition')  as $condition){
            $this->disable_condition->addMultiOption($condition["key"],$condition["value"]);
        }

        $this->addElement('checkbox', 'active', array(
            'label'      => 'Active',
            'required'   => false,
            'checked' => true,
        ));

        $this->addElement('submit', 'save', array(
            'label'=>'Search',
            'decorators'=>array('ViewHelper')
        ));

        $this->addElement('reset', 'clear', array(
            'label'=>'Clear',
            'onclick'=>"window.location=window.location;",
            'decorators'=>array('ViewHelper'),
        ));

        $this->addDisplayGroup(array('save','clear'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));
    }
}