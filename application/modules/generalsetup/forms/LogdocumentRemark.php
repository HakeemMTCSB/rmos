<?php
class GeneralSetup_Form_LogdocumentRemark extends Zend_Form
{


    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('Id', 'form_log_remark');

        $this->addElement('text', 'tlr_description',
            array(
                'label' => 'Description',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        //button
        $this->addElement('submit', 'save', array(
            'label' => 'Submit',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label' => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick' => "window.location ='" . $this->getView()->url(array('module' => 'generalsetup', 'controller' => 'logdocument-remark', 'action' => 'index'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}
?>