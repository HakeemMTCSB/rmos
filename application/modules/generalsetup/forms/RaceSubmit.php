<?php

class GeneralSetup_Form_RaceSubmit extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
//		$this->setAttrib('id','form_registry_type');

        //race id
        $this->addElement('text','race_id', array(
            'label'=>'Race ID',
            'class'=>'input-txt'
        ));

        //race desc
        $this->addElement('text','race_desc_english', array(
            'label'=>'Name',
            'class'=>'input-txt'
        ));

        //race desc
        $this->addElement('text','race_desc', array(
            'label'=>'Name(Malay)',
            'class'=>'input-txt'
        ));

        //race type
        $registryDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $this->addElement('select','race_type', array(
            'label'=>'Race Type',
            'required'=>false
        ));

        $this->race_type->addMultiOption(null,"-- Any --");
        foreach($registryDb->getDataByCodeType('race-type')  as $type){
            $this->race_type->addMultiOption($type["key"],$type["value"]);
        }

        $this->addElement('checkbox', 'active', array(
            'label'      => 'Active',
            'required'   => false,
            'checked' => true,
        ));

		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'race','action'=>'index'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>