<?php 
class GeneralSetup_Form_SidebarMenu extends Zend_Form{
	
	protected  $menuid;	
	protected  $parentid;	

	public function setMenuid($menuid){
		$this->menuid = $menuid;
	}
	
	public function setParentid($parentid){
		$this->parentid = $parentid;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_sidebar');
		
		
		//title head
		$this->addElement('select','sm_parentid', array(
			'label'=>'Parent'
		));
				
		$this->sm_parentid->addMultiOption(null,"This is parent");		
		//get list parent
		$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
		$parent_list = $sidebarDb->getParentSidebar($this->menuid);
		
		if(count($parent_list)>0){
			foreach ($parent_list as $list){		
				$this->sm_parentid->addMultiOption($list['sm_id'],$list['sm_name']);
			}
		}
		
			
								
		$this->addElement('text','sm_name', array(
			'label'=>'Name',
			'required'=>'true'
		));		
		
		$this->addElement('textarea','sm_desc', array(
			'label'=>'Description'
		));	
		
		$this->addElement('text','sm_module', array(
				'label'=>'Module',
		));
		
		$this->addElement('text','sm_controller', array(
				'label'=>'Controller',
		));
		
		$this->addElement('text','sm_action', array(
				'label'=>'Action',
		));
		
		//visibility
		$this->addElement('radio','sm_visibility', array(
			'label'=>'Visibility'
		));
		
		$this->sm_visibility->addMultiOptions(array(
			'1' => 'Show',
			'0' => 'Hide'
		))->setSeparator('&nbsp;&nbsp;')->setValue("1");
		

		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'menu','action'=>'sidebar', 'menu_id'=>$this->menuid),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>