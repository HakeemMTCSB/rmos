<?php

class GeneralSetup_Form_SchoolSubject extends Zend_Form
{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_ss');


		$this->addElement('text','ss_subject', 
			array(
				'label'=>'Subject Name:',
				'required'=>'true',
                'class'=>'input-txt'
			)
		);
		
		$this->addElement('text','ss_subject_bahasa', 
			array(
				'label'=>'Subject (Malay):',
                'class'=>'input-txt'
			)
		);
		
		/*$this->addElement('checkbox','ss_core_subject',
			array(
				'label'=>'Basic Core Subject:'
			)
		);*/


		
	
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'cancel', array(
          'label'=>'Clear',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location=window.location;",
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>