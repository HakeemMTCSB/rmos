<?php

class GeneralSetup_Form_Registry_RegistryType extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_registry_type');

        $this->addElement('text','name',
            array(
                'label'=>'Name',
                'required'=>true,
                'class'=>'input-txt'
            )
        );

		$this->addElement('text','code',
				array(
						'label'=>'Code',
						'required'=>true,
						'class'=>'input-txt'
				)
		);
		
		$this->addElement('text','name_malay',
				array(
						'label'=>'Name (Malay)',
						'required'=>false,
						'class'=>'input-txt'
				)
		);

        $this->addElement('textarea','description',
            array(
                'label'=>'Description'
            )
        );

        $this->addElement('checkbox','active',
            array(
                'label'=>'Active',
            )
        );

        $this->addElement('checkbox','system',
				array(
						'label'=>'System',
				)
		);

		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'registry-type','action'=>'index'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>