<?php
class GeneralSetup_Form_Registry_Search extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_registry');

		//name
		$this->addElement('text','name', array(
			'label'=>'Name',
			'class'=>'input-txt'
		));
		
		//personal ID
		$this->addElement('text','code', array(
			'label'=>'Code',
			'class'=>'input-txt'
		));


		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}