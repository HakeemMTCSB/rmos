<?php 
class GeneralSetup_Form_ModuleAccess extends Zend_Form{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','module_access_form');
		
	
	    //Module Role
		$this->addElement('select','ma_role_id', array(
			'label'=>'Role',
			'required'=>'true'
		));
				
		$this->ma_role_id->addMultiOption(null,"-- Please select --");		
		//get list role
		$definationDb = new App_Model_General_DbTable_Definationms();
		$role_list = $definationDb->getDataByType(94); //Module Access Role
		
		if(count($role_list)>0){
			foreach ($role_list as $list){		
				$this->ma_role_id->addMultiOption($list['idDefinition'],$list['DefinitionCode']);
			}
		}

		$this->addElement('text','ma_start_date', array(
			'label'=>'Start Date',
			'required'=>'true'
		));		
		
		
		$this->addElement('text','ma_end_date', array(
			'label'=>'End Date'
		));		
		
		 //Faculty
		$this->addElement('select','ma_faculty_id', array(
			'label'=>'Faculty',
		    'onChange'=>'getData()'
		));
		
		$this->ma_faculty_id->addMultiOption(null,"-- All --");		
		//get list faculty
		$facultyDb = new GeneralSetup_Model_DbTable_Collegemaster();
		$faculty_list = $facultyDb->getCollege(); 
		
		if(count($faculty_list)>0){
			foreach ($faculty_list as $list){		
				$this->ma_faculty_id->addMultiOption($list['IdCollege'],$list['CollegeName']);
			}
		}		
		 //Program
		$this->addElement('select','ma_program_id', array(
			'label'=>'Program',
		    'onChange'=>'getBranch()'
		));		
		$this->ma_program_id->setRegisterInArrayValidator(false);	
		$this->ma_program_id->addMultiOption(null,"-- All --");		
		
		
		 //Branch
		$this->addElement('select','ma_branch_id', array(
			'label'=>'Branch'
		));		
		$this->ma_branch_id->setRegisterInArrayValidator(false);		
		$this->ma_branch_id->addMultiOption(null,"-- All --");		
		
		
		 //Course
		$this->addElement('select','ma_course_id', array(
			'label'=>'Course'
		));		
		$this->ma_course_id->setRegisterInArrayValidator(false);		
		$this->ma_course_id->addMultiOption(null,"-- All --");		
		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'module-accessibility','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>