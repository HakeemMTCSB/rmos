<?php
class GeneralSetup_Form_LogdocumentSearch extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','form_search_document');

		//name
		$this->addElement('text','doc_desc', array(
			'label'=>'Doc Description (English)',
			'class'=>'input-txt'
		));

        //name
        $this->addElement('text','doc_desc_malay', array(
            'label'=>'Doc Description (Malay)',
            'class'=>'input-txt'
        ));


        //button

		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}