<?php
class GeneralSetup_Form_Logdocument extends Zend_Form
{


    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('Id', 'form_log_document');

        $this->addElement('text', 'doc_desc',
            array(
                'label' => 'Doc Description (English)',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        $this->addElement('text', 'doc_desc_malay',
            array(
                'label' => 'Doc Description (Malay)',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        //button
        $this->addElement('submit', 'save', array(
            'label' => 'Submit',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label' => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick' => "window.location ='" . $this->getView()->url(array('module' => 'generalsetup', 'controller' => 'logdocument', 'action' => 'index'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}
?>