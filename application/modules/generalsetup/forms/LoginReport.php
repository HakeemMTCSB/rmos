<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 24/1/2017
 * Time: 2:04 PM
 */
class GeneralSetup_Form_LoginReport extends Zend_Dojo_Form {

    public function init(){
        $model = new GeneralSetup_Model_DbTable_Report();

        //type
        $type = new Zend_Form_Element_Select('type');
        $type->removeDecorator("DtDdWrapper");
        $type->setAttrib('class', 'select');
        $type->removeDecorator("Label");

        $type->addMultiOption('', '-- Select --');
        $type->addMultiOption(1, 'Staff');
        $type->addMultiOption(2, 'Student');

        //name
        $name = new Zend_Form_Element_Text('name');
        $name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //loginname
        $loginname = new Zend_Form_Element_Text('loginname');
        $loginname->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //status
        $status = new Zend_Form_Element_Select('status');
        $status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'select');
        $status->removeDecorator("Label");

        $status->addMultiOption('', '-- Select --');

        $statusList = $model->getDefination(20);

        if ($statusList){
            foreach ($statusList as $statusLoop){
                $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
            }
        }

        $status->addMultiOption(0, 'Inactive');

        $this->addElements(array(
            $type,
            $name,
            $loginname,
            $status
        ));
    }
}