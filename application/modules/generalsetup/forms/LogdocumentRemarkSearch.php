<?php
class GeneralSetup_Form_LogdocumentRemarkSearch extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','form_search_remark');

		//name
		$this->addElement('text','tlr_description', array(
			'label'=>'Description',
			'class'=>'input-txt'
		));


        //button

		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}