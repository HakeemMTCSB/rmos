<?php

class GeneralSetup_Form_AddressForm extends Zend_Form
{
	protected $IdBranch;
	protected $IdBrc;
	protected $AddId;
	
	public function setIdBranch($idBranch){
		$this->IdBranch = $idBranch;
	}
	
	public function setIdBrc($idBrc){
		$this->IdBrc = $idBrc;
	}
	
	public function setAddId($addId){
		$this->AddId = $addId;
	}
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form1');
	
		
		$this->addElement('hidden', 'idBranch' );
		$this->idBranch->setValue($this->IdBranch);
		
		$this->addElement('hidden', 'idBrc' );
		$this->idBrc->setValue($this->IdBrc);
		
		$this->addElement('hidden', 'add_id' );
		$this->add_id->setValue($this->AddId);
		
	    //Address Type
		$this->addElement('select','address_type', array(
			'label'=>'Address Type',
		    'required'=>true
		));
		
		/*$defDB = new App_Model_General_DbTable_Definationms();
		$address_type = $defDB->getDataByType(98);	//Address Type 	
    			
		$this->address_type->addMultiOption(null,"-- Please Select --");
		foreach ($address_type as $at){			
			$this->address_type->addMultiOption($at['idDefinition'],$at['DefinitionDesc']);
		}*/

        $defDB = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $address_type = $defDB->getDataByCodeType('address-type');    //Address Type
        $this->address_type->addMultiOption(null,"-- Please Select --");
        foreach ($address_type as $list){
            $this->address_type->addMultiOption($list['key'], $list['value']);
        }

	    //Address Country
		$this->addElement('select','country', array(
			'label'=>'Country',
		    'required'=>true,
		    'onchange'=>'getState();'
		));
		
		$countryDB = new App_Model_General_DbTable_Country();
		$country_data = $countryDB->getData();	
    			
		$this->country->addMultiOption(null,"-- Please Select --");
		foreach ($country_data as $list){			
			$this->country->addMultiOption($list['idCountry'],$list['CountryName']);
		}
		
		//Address State
		$this->addElement('select','state', array(
			'label'=>'State',
		    'required'=>true,
			'onchange'=>'getCity(this.value); checkOthers(this);'
		));		
		$this->state->setRegisterInArrayValidator(false);

		$this->addElement('text','state_others', array(
			'label'=>'',
			'class'=>'input-txt',
			'required'=>false		    
		));   			
		
		//Address City
		$this->addElement('select','city', array(
			'label'=>'City',
		    'required'=>true,
			'onchange'=>'checkOthers(this);'
		));
		$this->city->setRegisterInArrayValidator(false);
		
		$this->addElement('text','city_others', array(
			'label'=>'',
			'class'=>'input-txt',
			'required'=>false		    
		));
		
		
		//Address 1
		$this->addElement('textarea','address1', array(
			'label'=>$this->getView()->translate('Address 1'),
			'required'=>true,
			    
		));
		
		//Address 2
		$this->addElement('textarea','address2', array(
			'label'=>$this->getView()->translate('Address 2'),
			//'required'=>false		    
		));
		
		//Zip Code
		$this->addElement('text','zipcode', array(
			'label'=>$this->getView()->translate('Zip Code'),
			'class'=>'input-txt',
			'required'=>true		    
		));
		
		//Phone
		$this->addElement('text','phone', array(
			'label'=>$this->getView()->translate('Phone No'),
			'class'=>'input-txt',
			'required'=>true		    
		));
		
		//Emel
		$this->addElement('text','email', array(
			'label'=>$this->getView()->translate('Email'),
			'class'=>'input-txt',
			'required'=>true		    
		));
		
		
	}
}

?>