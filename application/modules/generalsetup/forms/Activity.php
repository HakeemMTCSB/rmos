<?php

class GeneralSetup_Form_Activity extends Zend_Dojo_Form
{ //Formclass for the user module
    public function init()
    {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $ActivityName = new Zend_Form_Element_Textarea('ActivityName');
        $ActivityName->setAttrib('cols', '30')
            ->setAttrib('rows', '3')
            ->setAttrib('style', 'width = 10%;')
            ->setAttrib('maxlength', '250')
            ->setAttrib('dojoType', "dijit.form.SimpleTextarea")
            ->setAttrib('style', 'margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        $ActivityColorCode = new Zend_Form_Element_Text('ActivityColorCode');
        // $ActivityColorCode ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ActivityColorCode->setAttrib('required', "true")
            ->setAttrib('maxlength', '50')
            ->setAttrib('class', 'dojoinput color')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ActivityType = new Zend_Dojo_Form_Element_FilteringSelect('ActivityType');
        $ActivityType->removeDecorator("DtDdWrapper");
        $ActivityType->setAttrib('required', "true");
        $ActivityType->removeDecorator("Label");
        $ActivityType->removeDecorator('HtmlTag');
        $ActivityType->setAttrib('OnChange', 'PermanentfnGetCountryStateList');
        $ActivityType->setRegisterInArrayValidator(false);
        $ActivityType->setAttrib('dojoType', "dijit.form.FilteringSelect");


        $defDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $activity = $defDb->getListDataByCodeType('activity-type');
        foreach ($activity as $act) {
            $ActivityType->addMultiOption($act['id'], $act['name']);
        }

        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');

        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $actvity = new Zend_Form_Element_Hidden('IdActivity');
        $actvity->removeDecorator("DtDdWrapper");
        $actvity->removeDecorator("Label");
        $actvity->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype = "dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        // adding elements to cuttent object
        $this->addElements(array($actvity,
            $ActivityName,
            $ActivityColorCode,
            $ActivityType,
            $Save,
            $UpdDate,
            $UpdUser
        ));
    }
}