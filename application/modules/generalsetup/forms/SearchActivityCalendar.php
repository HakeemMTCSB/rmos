<?php

class GeneralSetup_Form_SearchActivityCalendar extends Zend_Form
{
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
			
	    //Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester')
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- All --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["value2"]);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["value"]);
			}
		}
		
		
		 //Semester
		$this->addElement('select','IdActivity', array(
			'label'=>$this->getView()->translate('Activity')
		));

		$activityDB = new GeneralSetup_Model_DbTable_Activity();
		
		$this->IdActivity->addMultiOption(null,"-- All --");		
		foreach($activityDB->fngetActivityDetails() as $activity){
			$this->IdActivity->addMultiOption($activity["idActivity"],$activity["ActivityName"]);
		}
		
		//Subject Code
		$this->addElement('text','date_from', array(
			'label'=>$this->getView()->translate('Date From'),
		 	'class'=>'datepicker'
		));	
		
		$this->addElement('text','date_to', array(
			'label'=>$this->getView()->translate('Date To'),
			'class'=>'datepicker'
		));
		
			
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
}
?>