<?php

class GeneralSetup_Form_BatchSubmit extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
//		$this->setAttrib('id','form_registry_type');

        //batch id
        $this->addElement('text','BatchCode', array(
            'label'=>'Batch Code',
            'class'=>'input-txt'
        ));

        // batch status
        $this->addElement('select','BatchStatus', array(
            'label'=>'Status',
            'required'=>false
        ));

        $this->BatchStatus->addMultiOption("ACTIVE","ACTIVE");
        $this->BatchStatus->addMultiOption("INACTIVE","INACTIVE");

        //button
        $this->addElement('submit', 'save', array(
            'label'=>'Submit',
            'decorators'=>array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label'=>'Cancel',
            'decorators'=>array('ViewHelper'),
            'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'batch','action'=>'edit','id'=>'IDBatch'),'default',true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}
?>