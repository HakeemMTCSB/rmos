<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Form_EditAttendanceDetails extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $model = new GeneralSetup_Model_DbTable_Attendance();
        
        //date
        $date = new Zend_Form_Element_Text('date');
	$date->setAttrib('class', 'input-txt')
            ->setAttrib("required", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //starttime
        $starttime = new Zend_Form_Element_Text('starttime');
	$starttime->setAttrib('class', 'input-txt small')
            ->setAttrib("required", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //endtime
        $endtime = new Zend_Form_Element_Text('endtime');
	$endtime->setAttrib('class', 'input-txt small')
            ->setAttrib("required", "true")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //type
        $type = new Zend_Form_Element_Select('type');
        $type->removeDecorator("DtDdWrapper");
        $type->setAttrib('class', 'select');
        $type->removeDecorator("Label")
            ->setAttrib("required", "true");

        $type->addMultiOption('', '-- Select --');

        $typeList = $model->getDefinition(160);

        if ($typeList){
            foreach ($typeList as $typeLoop){
                $type->addMultiOption($typeLoop['idDefinition'], $typeLoop['DefinitionDesc']);
            }
        }
        
        //venue
        $venue = new Zend_Form_Element_Select('venue');
	$venue->removeDecorator("DtDdWrapper");
        $venue->setAttrib('class', 'select');
	$venue->removeDecorator("Label");
            //->setAttrib("required", "true");
        
        $venue->addMultiOption('', '-- Select --');
        
        $venueList = $model->getDefinition(148);
        
        if ($venueList){
            foreach ($venueList as $venueLoop){
                $venue->addMultiOption($venueLoop['idDefinition'], $venueLoop['DefinitionDesc']);
            }
        }
        
        //lecturer
        $lecturer = new Zend_Form_Element_Select('lecturer');
	$lecturer->removeDecorator("DtDdWrapper");
        $lecturer->setAttrib('class', 'select');
	$lecturer->removeDecorator("Label")
            ->setAttrib("required", "true");
        
        $lecturer->addMultiOption('', '-- Select --');
        
        $lecturerList = $model->getLecturer();
        
        if ($lecturerList){
            foreach ($lecturerList as $lecturerLoop){
                $lecturer->addMultiOption($lecturerLoop['IdStaff'], $lecturerLoop['FullName']);
            }
        }
        
        $this->addElements(array(
            $date,
            $starttime,
            $endtime,
            $type,
            $venue,
            $lecturer
        ));
    }
}