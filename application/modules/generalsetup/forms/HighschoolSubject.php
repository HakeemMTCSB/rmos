<?php
class GeneralSetup_Form_HighschoolSubject extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {

        $gstrtranslate = Zend_Registry::get('Zend_Translate');
        $lstrAmountConstraint = "{pattern:'0.##'}";

        /*$lobjsubject = new Application_Model_DbTable_Subjectsetup();
        $lobjsubjectlist = $lobjsubject->fngetsubjectList();*/

        $lobjqua = new Application_Model_DbTable_Qualificationsetup();
        $lobjqualist = $lobjqua->fngetQualificationList();

        $IdInstitutionsetup  = new Zend_Form_Element_Hidden('IdInstitution');
        $IdInstitutionsetup  	->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $UpdDate  = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $ss_core_subject  = new Zend_Form_Element_Hidden('ss_core_subject');
        $ss_core_subject  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');



        $ss_subject = new Zend_Form_Element_Text('ss_subject');
        $ss_subject	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            ->setAttrib('required',"true")
            ->setAttrib('maxlength','150')
            //->setAttrib('autocapitalize','on')
            //->setAttrib('onchange','toUpperCase(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase',"true");

        $ss_subject_bahasa = new Zend_Form_Element_Text('ss_subject_bahasa');
        $ss_subject_bahasa	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            ->setAttrib('required',"false")
            ->setAttrib('maxlength','150')
            //->setAttrib('autocapitalize','on')
            //->setAttrib('onchange','toUpperCase(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase',"true");

        $ss_subject_code = new Zend_Form_Element_Text('ss_subject_code');
        $ss_subject_code	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            ->setAttrib('required',"true")
            ->setAttrib('maxlength','50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase',"true");


        /*$QualificationType = new Zend_Dojo_Form_Element_FilteringSelect('QualificationType');
        $QualificationType->removeDecorator("DtDdWrapper");
        $QualificationType->setAttrib('required',"false") ;
        $QualificationType->removeDecorator("Label");
        $QualificationType->removeDecorator('HtmlTag');
        $QualificationType->setRegisterInArrayValidator(false);
        $QualificationType->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $QualificationType->addMultiOption('', 'Select');*/


        /*$InstitutionAddress1 = new Zend_Form_Element_Text('InstitutionAddress1');
        $InstitutionAddress1	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            //->setAttrib('required',"true")
            ->setAttrib('maxlength','50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase',"true");;

        $InstitutionAddress2 = new Zend_Form_Element_Text('InstitutionAddress2');
        $InstitutionAddress2	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            //->setAttrib('required',"false")
            ->setAttrib('maxlength','50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase',"true");;

        $City = new Zend_Dojo_Form_Element_FilteringSelect('City');
        $City->removeDecorator("DtDdWrapper");
        $City->removeDecorator("Label");
        $City->removeDecorator('HtmlTag');
        $City->setAttrib('required',"false");
        $City->setRegisterInArrayValidator(false);
        $City->setAttrib('dojoType',"dijit.form.FilteringSelect");

        $State = new Zend_Dojo_Form_Element_FilteringSelect('State');
        $State->removeDecorator("DtDdWrapper");
        $State->setAttrib('required',"false") ;
        $State->removeDecorator("Label");
        $State->removeDecorator('HtmlTag');
        $State->setAttrib('OnChange', 'fnGetStateCityListofUniversity');
        $State->setRegisterInArrayValidator(false);
        $State->setAttrib('dojoType',"dijit.form.FilteringSelect");


        $Country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
        $Country->removeDecorator("DtDdWrapper");
        $Country->setAttrib('required',"false") ;
        $Country->removeDecorator("Label");
        $Country->removeDecorator('HtmlTag');
        $Country->setAttrib('OnChange', "fnGetCountryStateList(this,'State')");
        $Country->setRegisterInArrayValidator(false);
        $Country->setAttrib('dojoType',"dijit.form.FilteringSelect");


        $Zip = new Zend_Form_Element_Text('Zip');
        $Zip->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Zip->setAttrib('maxlength','20');
        $Zip->removeDecorator("DtDdWrapper");
        $Zip->removeDecorator("Label");
        $Zip->removeDecorator('HtmlTag');

        $Phonecountrycode = new Zend_Form_Element_Text('Phonecountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $Phonecountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Phonecountrycode->setAttrib('maxlength','3');
        $Phonecountrycode->setAttrib('style','width:30px');
        $Phonecountrycode->removeDecorator("DtDdWrapper");
        $Phonecountrycode->removeDecorator("Label");
        $Phonecountrycode->removeDecorator('HtmlTag');

        $Phonestatecode = new Zend_Form_Element_Text('Phonestatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $Phonestatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Phonestatecode->setAttrib('maxlength','2');
        $Phonestatecode->setAttrib('style','width:30px');
        $Phonestatecode->removeDecorator("DtDdWrapper");
        $Phonestatecode->removeDecorator("Label");
        $Phonestatecode->removeDecorator('HtmlTag');

        $InstitutionPhoneNumber = new Zend_Form_Element_Text('InstitutionPhoneNumber',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Phone No."));
        $InstitutionPhoneNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $InstitutionPhoneNumber->setAttrib('maxlength','9');
        $InstitutionPhoneNumber->setAttrib('style','width:93px');
        $InstitutionPhoneNumber->removeDecorator("DtDdWrapper");
        $InstitutionPhoneNumber->removeDecorator("Label");
        $InstitutionPhoneNumber->removeDecorator('HtmlTag');

        $faxcountrycode = new Zend_Form_Element_Text('faxcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $faxcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $faxcountrycode->setAttrib('maxlength','3');
        $faxcountrycode->setAttrib('style','width:30px');
        $faxcountrycode->removeDecorator("DtDdWrapper");
        $faxcountrycode->removeDecorator("Label");
        $faxcountrycode->removeDecorator('HtmlTag');

        $faxstatecode = new Zend_Form_Element_Text('faxstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $faxstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $faxstatecode->setAttrib('maxlength','5');
        $faxstatecode->setAttrib('style','width:30px');
        $faxstatecode->removeDecorator("DtDdWrapper");
        $faxstatecode->removeDecorator("Label");
        $faxstatecode->removeDecorator('HtmlTag');

        $InstitutionFaxNumber = new Zend_Form_Element_Text('InstitutionFaxNumber',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Fax"));
        $InstitutionFaxNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $InstitutionFaxNumber->setAttrib('style','width:93px');
        $InstitutionFaxNumber->setAttrib('maxlength','20');
        $InstitutionFaxNumber->removeDecorator("DtDdWrapper");
        $InstitutionFaxNumber->removeDecorator("Label");
        $InstitutionFaxNumber->removeDecorator('HtmlTag');*/

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class','NormalBtn');
        $Save->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Search = new Zend_Form_Element_Submit('Search');
        $Search	->setAttrib('id', 'search')
            ->setAttrib('name', 'search')
            ->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType',"dijit.form.Button");
        $Add->setAttrib('OnClick', 'addSubjectentry()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType',"dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $IdSubject = new Zend_Dojo_Form_Element_FilteringSelect('IdSubject');
        $IdSubject->removeDecorator("DtDdWrapper");
        $IdSubject->setAttrib('required',"false") ;
        $IdSubject->removeDecorator("Label");
        $IdSubject->removeDecorator('HtmlTag');
        $IdSubject->setRegisterInArrayValidator(false);
        $IdSubject->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdSubject->addMultiOption('0', 'Select');
        $IdSubject->addMultiOptions($lobjqualist);


        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

        /*$ss_core_subject = new Zend_Form_Element_Checkbox('ss_core_subject');
        $ss_core_subject->setAttrib('dojoType',"dijit.form.CheckBox");
        $ss_core_subject->setvalue('1');
        $ss_core_subject->removeDecorator("DtDdWrapper");
        $ss_core_subject->removeDecorator("Label");
        $ss_core_subject->removeDecorator('HtmlTag');*/

        //form elements
        $this->addElements(array(
            $IdInstitutionsetup,
            $ss_subject,
            $ss_subject_bahasa,
            $ss_subject_code,
            //$InstitutionAddress1,
            //$InstitutionAddress2,
            //$Zip,
            $Active,
            //$Country,
            //$State,
            //$City,
            //$SubjectCode,
           /* $QualificationType,
            $Phonecountrycode,
            $Phonestatecode,
            $InstitutionPhoneNumber,
            $faxcountrycode,
            $faxstatecode,
            $InstitutionFaxNumber,*/
            $UpdDate,
            $UpdUser,
            $Save,
            $Search,
            $IdSubject,
            $Add,
            $ss_core_subject,
            $clear));
    }
}