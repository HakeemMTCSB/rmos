<?php
class GeneralSetup_AttendanceController extends Base_Base { 
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_Attendance();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Attendance');
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        $groupId = $this->_getParam('idGroup', 0);
        
        $groupInfo = $this->model->getGoupInfo($groupId);
        //var_dump($groupInfo); exit;
        $this->view->groupInfo = $groupInfo;
        
        $studentList = $this->model->getGroupStudent($groupId);
        //var_dump($studentList); exit;
        $this->view->studentList = $studentList;
        
        $groupSchedule = $this->model->getGroupSchedule($groupId);
        
        $sdate = $this->model->getAttendanceDate($groupId);
        
        $status = $this->model->getDefinition();
        $this->view->status = $status;
        
        if ($groupSchedule){
            $classList = array();
            /*if (count($groupSchedule)==1){
                if ($sdate){
                    $i = 0;
                    foreach ($sdate as $sdateLoop){
                        //$classList[$i]['sc_id'] = $groupSchedule[0]['sc_id'];
                        $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                        $classList[$i]['idClassType'] = $groupSchedule[0]['idClassType'];
                        $i++;
                    }
                }else{
                    $date = $groupInfo['class_start_date'];
                    $i = 0;
                    while ($date <= $groupInfo['class_end_date']){
                        //$classList[$i]['sc_id'] = $groupSchedule[0]['sc_id'];
                        $classList[$i]['sc_date'] = $date;
                        $classList[$i]['idClassType'] = $groupSchedule[0]['idClassType'];
                        $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                        $i++;
                    }
                }
            }else{
                $classList = $groupSchedule;
            }*/
            $classList = $groupSchedule;
        }else{
            throw new Exception('Group Schedule Not Found');
        }
        
        $weekArr = array();
        $monthArr = array();
        
        if ($classList){
            //$monthCount = 0;
            //$weekCount = 0;
            $i = 0;
            foreach ($classList as $key => $dateLoop){
                
                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }
                
                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            //$classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                            $i++;
                            
                            $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                            $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;
                            //var_dump($dkey);
                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                            //$date = $groupInfo['class_start_date'];
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            //$classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['sc_date'] = $date;
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                    //redirect here
                    //$this->_helper->flashMessenger->addMessage(array('error' => 'Please Set Schedule Date For Section "'.$groupInfo['GroupName'].'" Before View Attendance'));
                    //$this->_redirect($this->baseUrl . '/generalsetup/course-group/group-list/idSubject/'.$groupInfo['IdSubject'].'/idSemester/'.$groupInfo['IdSemester']);
                }
                
                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));
                
                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }
                
                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
                
                unset($classList[$key]['sc_id']);
                unset($classList[$key]['idGroup']);
                unset($classList[$key]['IdLecturer']);
                unset($classList[$key]['idCollege']);
                unset($classList[$key]['idBranch']);
                //unset($classList[$key]['idClassType']);
                unset($classList[$key]['sc_day']);
                unset($classList[$key]['sc_start_time']);
                unset($classList[$key]['sc_end_time']);
                unset($classList[$key]['sc_venue']);
                unset($classList[$key]['sc_class']);
                unset($classList[$key]['sc_remark']);
                unset($classList[$key]['sc_createdby']);
                unset($classList[$key]['sc_createddt']);
            }
        }
        
        if ($studentList){
            foreach ($studentList as $studentLoop2){
                if ($classList){
                    foreach ($classList as $classLoop2){
                        if ($classLoop2['sc_date'] != null){
                            $checkStatus = $this->model->checkAttendanceStatus($groupId, $studentLoop2['IdStudentRegistration'], $classLoop2['sc_date']);
                            $checkStatus2 = $this->model->checkAttendanceStatus($groupId, 0, $classLoop2['sc_date']);

                            if ($checkStatus){ //update
                                $data = array(
                                    'ctd_item_type'=>$classLoop2['idClassType'],
                                );
                                $this->model->updateAttendance($data, $checkStatus['ctd_id']);
                            }else{ //insert
                                $data = array(
                                    'ctd_groupid'=>$groupId, 
                                    'ctd_studentid'=>$studentLoop2['IdStudentRegistration'], 
                                    'ctd_item_type'=>$classLoop2['idClassType'],
                                    'ctd_scdate'=>$classLoop2['sc_date'], 
                                    'ctd_status'=>395,
                                    'ctd_class_status'=>isset($checkStatus2['ctd_class_status']) ? $checkStatus2['ctd_class_status']:1,
                                    'ctd_reason'=>'', 
                                    'ctd_upddate'=>date('Y-m-d h:i:s'), 
                                    'ctd_updby'=>$userId
                                );
                                $this->model->insertAttendance($data);
                            }
                        }
                    }
                }
            }

            $this->model->deleteTempAtt($groupId);
        }else{
            if ($classList){
                foreach ($classList as $classLoop2){
                    if ($classLoop2['sc_date'] != null){
                        $checkStatus = $this->model->checkAttendanceStatus($groupId, 0, $classLoop2['sc_date']);

                        if ($checkStatus){ //update
                            $data = array(
                                'ctd_item_type'=>$classLoop2['idClassType'],
                            );
                            $this->model->updateAttendance($data, $checkStatus['ctd_id']);
                        }else{ //insert
                            $data = array(
                                'ctd_groupid'=>$groupId,
                                'ctd_studentid'=>0,
                                'ctd_item_type'=>$classLoop2['idClassType'],
                                'ctd_scdate'=>$classLoop2['sc_date'],
                                'ctd_status'=>395,
                                'ctd_reason'=>'',
                                'ctd_upddate'=>date('Y-m-d h:i:s'),
                                'ctd_updby'=>$userId
                            );
                            $this->model->insertAttendance($data);
                        }
                    }
                }
            }
        }
        
        //var_dump($classList);
        $this->aasort($classList, 'sc_date');
        $this->view->classList = $classList;
        $this->view->monthArr = $monthArr;
        $this->view->weekArr = $weekArr;
        
        $this->view->filterby = 0;
        $this->view->week = 0;
        $this->view->month = 0;
        /*echo '---------------week-------------------<br/>';
        var_dump($weekArr);
        echo '---------------month------------------<br/>';
        var_dump($monthArr);
        exit;*/
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            
            if (isset($formData['submit'])){
                if (isset($formData['status'])){
                    foreach ($formData['status'] as $studentid=>$studentLoop){
                        foreach ($studentLoop as $date=>$classLoop){
                            $checkStatus = $this->model->checkAttendanceStatus($groupId, $studentid, $date);
                            
                            if ($checkStatus){ //update
                                $data = array(
                                    'ctd_status'=>$classLoop, 
                                    'ctd_item_type'=>$formData['item'][$studentid][$date],
                                    'ctd_reason'=>$formData['reason'][$studentid][$date],
                                    'ctd_scdate'=>date('Y-m-d', strtotime($formData['scheduledate'][$date])),
                                    'ctd_upddate'=>date('Y-m-d h:i:s'), 
                                    'ctd_updby'=>$userId
                                );
                                $this->model->updateAttendance($data, $checkStatus['ctd_id']);
                            }else{ //insert
                                $data = array(
                                    'ctd_groupid'=>$groupId, 
                                    'ctd_studentid'=>$studentid, 
                                    'ctd_scdate'=>date('Y-m-d', strtotime($formData['scheduledate'][$date])), 
                                    'ctd_status'=>$classLoop, 
                                    'ctd_item_type'=>$formData['item'][$studentid][$date],
                                    'ctd_reason'=>$formData['reason'][$studentid][$date],
                                    'ctd_upddate'=>date('Y-m-d h:i:s'), 
                                    'ctd_updby'=>$userId
                                );
                                $this->model->insertAttendance($data);
                            }
                            
                            $getScheduleDate = $this->model->getGroupScheduleByDate($groupId, $date);
                            
                            if ($getScheduleDate){
                                $scheduleData = array(
                                    'sc_day'=>date('l', strtotime($formData['scheduledate'][$date])),
                                    'sc_date'=>date('Y-m-d', strtotime($formData['scheduledate'][$date]))
                                );
                                $this->model->updateGroupSchedule($getScheduleDate['sc_id'], $scheduleData);
                            }
                        }
                    }
                }
                
                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
                $this->_redirect($this->baseUrl.'/generalsetup/attendance/index/idGroup/'.$groupId);
            }else{
                $classListFilter = array();
                if (isset($formData['filterby']) && $formData['filterby']!=0){
                    if ($formData['filterby']==1){
                        if ($classList){
                            $i = 0;
                            foreach ($classList as $dateLoop){
                                $week = date('W', strtotime($dateLoop['sc_date']));

                                if ($formData['weekfilter'] == $week){
                                    $classListFilter[$i]['sc_date'] = $dateLoop['sc_date'];
                                }
                                $i++;
                            }
                        }
                        $this->view->classList = $classListFilter;
                    }else{
                        if ($classList){
                            $i = 0;
                            foreach ($classList as $dateLoop){
                                $month = date('F', strtotime($dateLoop['sc_date']));

                                if ($formData['monthfilter'] == $month){
                                    $classListFilter[$i]['sc_date'] = $dateLoop['sc_date'];
                                }
                                $i++;
                            }
                        }
                        $this->view->classList = $classListFilter;
                    }
                }
                
                $this->view->filterby = $formData['filterby'];
                $this->view->week = $formData['weekfilter'];
                $this->view->month = $formData['monthfilter'];
            }
        }
    }
    
    public function printAction(){
        
        $this->_helper->layout->disableLayout();
        
        $groupId = $this->_getParam('id', 0);
        
        $groupInfo = $this->model->getGoupInfo($groupId);
        $this->view->groupInfo = $groupInfo;
        
        $studentList = $this->model->getGroupStudent($groupId);
        $this->view->studentList = $studentList;
        
        $groupSchedule = $this->model->getGroupSchedule($groupId);
        
        $sdate = $this->model->getAttendanceDate($groupId);
        
        $status = $this->model->getDefinition();
        $this->view->status = $status;
        
        if ($groupSchedule){
            $classList = array();
            /*if (count($groupSchedule)==1){
                if ($sdate){
                    $i = 0;
                    foreach ($sdate as $sdateLoop){
                        $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                        $i++;
                    }
                }else{
                    $date = $groupInfo['class_start_date'];
                    $i = 0;
                    while ($date <= $groupInfo['class_end_date']){
                        $classList[$i]['sc_date'] = $date;
                        $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                        $i++;
                    }
                }
            }else{
                $classList = $groupSchedule;
            }*/
            $classList = $groupSchedule;
        }else{
            throw new Exception('Group Schedule Not Found');
        }
        
        $weekArr = array();
        $monthArr = array();
        if ($classList){
            //$monthCount = 0;
            //$weekCount = 0;
            $i = 0;
            foreach ($classList as $key => $dateLoop){
                
                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }
                
                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $i++;
                            
                            $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                            $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;
                            //var_dump($dkey);
                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                            //$date = $groupInfo['class_start_date'];
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                    
                    //redirect here
                    //$this->_helper->flashMessenger->addMessage(array('error' => 'Please Set Schedule Date For Section "'.$groupInfo['GroupName'].'" Before View Attendance'));
                    //$this->_redirect($this->baseUrl . '/generalsetup/course-group/group-list/idSubject/'.$groupInfo['IdSubject'].'/idSemester/'.$groupInfo['IdSemester']);
                }
                
                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));
                
                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }
                
                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
                
                unset($classList[$key]['sc_id']);
                unset($classList[$key]['idGroup']);
                unset($classList[$key]['IdLecturer']);
                unset($classList[$key]['idCollege']);
                unset($classList[$key]['idBranch']);
                unset($classList[$key]['idClassType']);
                unset($classList[$key]['sc_day']);
                unset($classList[$key]['sc_start_time']);
                unset($classList[$key]['sc_end_time']);
                unset($classList[$key]['sc_venue']);
                unset($classList[$key]['sc_class']);
                unset($classList[$key]['sc_remark']);
                unset($classList[$key]['sc_createdby']);
                unset($classList[$key]['sc_createddt']);
            }
        }

        $this->aasort($classList, 'sc_date');
        $this->view->classList = $classList;
        $this->view->monthArr = $monthArr;
        $this->view->weekArr = $weekArr;
        
        $this->view->filterby = 0;
        $this->view->week = 0;
        $this->view->month = 0;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            
            $classListFilter = array();
            if (isset($formData['filterby']) && $formData['filterby']!=0){
                if ($formData['filterby']==1){
                    if ($classList){
                        $i = 0;
                        foreach ($classList as $dateLoop){
                            $week = date('W', strtotime($dateLoop['sc_date']));

                            if ($formData['weekfilter'] == $week){
                                $classListFilter[$i]['sc_date'] = $dateLoop['sc_date'];
                            }
                            $i++;
                        }
                    }
                    $this->view->classList = $classListFilter;
                }else{
                    if ($classList){
                        $i = 0;
                        foreach ($classList as $dateLoop){
                            $month = date('F', strtotime($dateLoop['sc_date']));

                            if ($formData['monthfilter'] == $month){
                                $classListFilter[$i]['sc_date'] = $dateLoop['sc_date'];
                            }
                            $i++;
                        }
                    }
                    $this->view->classList = $classListFilter;
                }
            }
            
            $this->view->filterby = $formData['filterby'];
            $this->view->week = $formData['weekfilter'];
            $this->view->month = $formData['monthfilter'];
        }
        
        $this->view->filename = date('Ymd').'_attendance_report.xls';
    }
    
    public function pdfAction(){
        $this->_helper->layout->disableLayout();
        
        $groupId = $this->_getParam('id', 0);
        
        $groupInfo = $this->model->getGoupInfo($groupId);
        $this->view->groupInfo = $groupInfo;
        
        $studentList = $this->model->getGroupStudent($groupId);
        $this->view->studentList = $studentList;
        
        $programList = $this->model->getGroupProgram($groupId);
        
        $groupSchedule = $this->model->getGroupSchedule($groupId);
        
        $sdate = $this->model->getAttendanceDate($groupId);
        
        $status = $this->model->getDefinition();
        $this->view->status = $status;
        
        if ($groupSchedule){
            $classList = array();
            /*if (count($groupSchedule)==1){
                if ($sdate){
                    $i = 0;
                    foreach ($sdate as $sdateLoop){
                        $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                        $i++;
                    }
                }else{
                    $date = $groupInfo['class_start_date'];
                    $i = 0;
                    while ($date <= $groupInfo['class_end_date']){
                        $classList[$i]['sc_date'] = $date;
                        $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                        $i++;
                    }
                }
            }else{
                $classList = $groupSchedule;
            }*/
            $classList = $groupSchedule;
        }else{
            throw new Exception('Group Schedule Not Found');
        }
        
        $weekArr = array();
        $monthArr = array();
        if ($classList){
            //$monthCount = 0;
            //$weekCount = 0;
            $i = 0;
            foreach ($classList as $key => $dateLoop){
                
                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }
                
                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $i++;
                            
                            $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                            $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;
                            //var_dump($dkey);
                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                            //$date = $groupInfo['class_start_date'];
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                    
                    //redirect here
                    //$this->_helper->flashMessenger->addMessage(array('error' => 'Please Set Schedule Date For Section "'.$groupInfo['GroupName'].'" Before View Attendance'));
                    //$this->_redirect($this->baseUrl . '/generalsetup/course-group/group-list/idSubject/'.$groupInfo['IdSubject'].'/idSemester/'.$groupInfo['IdSemester']);
                }
                
                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));
                
                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }
                
                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
                
                unset($classList[$key]['sc_id']);
                unset($classList[$key]['idGroup']);
                unset($classList[$key]['IdLecturer']);
                unset($classList[$key]['idCollege']);
                unset($classList[$key]['idBranch']);
                unset($classList[$key]['idClassType']);
                unset($classList[$key]['sc_day']);
                unset($classList[$key]['sc_start_time']);
                unset($classList[$key]['sc_end_time']);
                unset($classList[$key]['sc_venue']);
                unset($classList[$key]['sc_class']);
                unset($classList[$key]['sc_remark']);
                unset($classList[$key]['sc_createdby']);
                unset($classList[$key]['sc_createddt']);
            }
        }
        
        $this->view->classList = $classList;
        
        $this->view->monthArr = $monthArr;
        $this->view->weekArr = $weekArr;
        
        $this->view->filterby = 0;
        $this->view->week = 0;
        $this->view->month = 0;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            
            $classListFilter = array();
            if (isset($formData['filterby']) && $formData['filterby']!=0){
                if ($formData['filterby']==1){
                    if ($classList){
                        $i = 0;
                        foreach ($classList as $dateLoop){
                            $week = date('W', strtotime($dateLoop['sc_date']));

                            if ($formData['weekfilter'] == $week){
                                $classListFilter[$i]['sc_date'] = $dateLoop['sc_date'];
                            }
                            $i++;
                        }
                    }
                    $classList = $classListFilter;
                }else{
                    if ($classList){
                        $i = 0;
                        foreach ($classList as $dateLoop){
                            $month = date('F', strtotime($dateLoop['sc_date']));

                            if ($formData['monthfilter'] == $month){
                                $classListFilter[$i]['sc_date'] = $dateLoop['sc_date'];
                            }
                            $i++;
                        }
                    }
                    $classList = $classListFilter;
                }
            }
            $this->view->filterby = $formData['filterby'];
            $this->view->week = $formData['weekfilter'];
            $this->view->month = $formData['monthfilter'];
        }
        
        asort($classList);
        $this->aasort($classList, 'sc_date');
        
        global $schedule;
        $schedule = $groupInfo;
        //var_dump($schedule);
        global $data;
        $data = $studentList;
        //var_dump($data);
        global $class_list;
        $class_list = $classList;
        //var_dump($class_list);
        global $program_list;
        $program_list = $programList;
        //var_dump($program_list);
        
        //exit;
        $fieldValues = array(
            '$[LOGO]'=> APPLICATION_URL."/images/logo_text.png"
        );

        require_once 'dompdf_config.inc.php';

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        //$html_template_path = DOCUMENT_PATH."/template/CourseGroupingAttendance__.html";
        $html_template_path = DOCUMENT_PATH."/template/CourseGroupingAttendance.html";

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);	
        }

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        //$dompdf->set_paper('a4', 'potrait');
        $dompdf->set_paper('a4', 'landscape');
        $dompdf->render();

        $dompdf->stream("Attendance.pdf");
        exit;
    }
    
    public function editScheduleAction(){
        $this->_helper->layout->disableLayout();
        
        $groupId = $this->_getParam('groupid', 0);
        $date = $this->_getParam('date', null);
        
        $this->view->groupid = $groupId;
        $this->view->date = $date;
        
        $form = new GeneralSetup_Form_EditAttendanceDetails();
        
        $scdate = date('Y-m-d', $date);
        $day = date('l', $date);
        
        $schedule = $this->model->getSchedule($groupId, $scdate);
        
        if (!$schedule){
            $schedule = $this->model->getSchedule3($groupId, $scdate);
            
            if (!$schedule){
                $schedule = $this->model->getSchedule2($groupId, $day);
                
                $populateData = array(
                    'date'=>date('d-m-Y', $date),
                    'starttime'=>$schedule['sc_start_time'],
                    'endtime'=>$schedule['sc_end_time'],
                    'venue'=>$schedule['sc_venue'],
                    'lecturer'=>$schedule['IdLecturer']
                );
            }else{
                $populateData = array(
                    'date'=>date('d-m-Y', $date),
                    'starttime'=>$schedule['tsd_starttime'],
                    'endtime'=>$schedule['tsd_enddate'],
                    'venue'=>$schedule['tsd_venue'],
                    'lecturer'=>$schedule['tsd_lecturer']
                );
            }
        }else{
            $populateData = array(
                'date'=>date('d-m-Y', $date),
                'starttime'=>$schedule['sc_start_time'],
                'endtime'=>$schedule['sc_end_time'],
                'venue'=>$schedule['sc_venue'],
                'lecturer'=>$schedule['IdLecturer']
            );
        }
        
        $form->populate($populateData);
        $this->view->form = $form;
    }
    
    public function saveEditScheduleAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //var_dump($formData);
            $scdate = date('Y-m-d', $formData['scdate']);
            $day = date('l', $formData['scdate']);
            
            $schedule = $this->model->getSchedule($formData['groupid'], $scdate);
            //var_dump($schedule); exit;
            if (!$schedule){ //schedule susah
                $schedule = $this->model->getSchedule3($formData['groupid'], $scdate);

                if (!$schedule){
                    $data = array(
                        'tsd_groupid'=>$formData['groupid'], 
                        'tsd_scdate'=>date('Y-m-d', strtotime($formData['date'])), 
                        'tsd_starttime'=>$formData['starttime'], 
                        'tsd_enddate'=>$formData['endtime'], 
                        'tsd_venue'=>$formData['venue'], 
                        'tsd_lecturer'=>$formData['lecturer'], 
                        'tsd_upddate'=>date('Y-m-d H:i:s'), 
                        'tsd_updby'=>$userId
                    );
                    $this->model->insertScheduleDetail($data);
                }else{
                    $data = array(
                        'tsd_groupid'=>$formData['groupid'], 
                        'tsd_scdate'=>date('Y-m-d', strtotime($formData['date'])), 
                        'tsd_starttime'=>$formData['starttime'], 
                        'tsd_enddate'=>$formData['endtime'], 
                        'tsd_venue'=>$formData['venue'], 
                        'tsd_lecturer'=>$formData['lecturer'], 
                        'tsd_upddate'=>date('Y-m-d H:i:s'), 
                        'tsd_updby'=>$userId
                    );
                    $this->model->updateScheduleDetail($data, $schedule['tsd_id']);
                    
                    //history
                    $dataHis = array(
                        'tsd_id'=>$schedule['tsd_id'],
                        'tsd_groupid'=>$schedule['tsd_groupid'], 
                        'tsd_scdate'=>$schedule['tsd_scdate'], 
                        'tsd_starttime'=>$schedule['tsd_starttime'], 
                        'tsd_enddate'=>$schedule['tsd_enddate'], 
                        'tsd_venue'=>$schedule['tsd_venue'], 
                        'tsd_lecturer'=>$schedule['tsd_lecturer'], 
                        'tsd_upddate'=>$schedule['tsd_upddate'], 
                        'tsd_updby'=>$schedule['tsd_updby'],
                        'upddate'=>date('Y-m-d H:i:s'),
                        'updby'=>$userId
                    );
                    $this->model->insertScheduleDetailHistory($dataHis);
                }
            }else{ //schedule senang
                $data = array(
                    'IdLecturer'=>$formData['lecturer'], 
                    'sc_day'=>date('l', strtotime($formData['date'])), 
                    'sc_start_time'=>$formData['starttime'], 
                    'sc_end_time'=>$formData['endtime'], 
                    'sc_venue'=>$formData['venue'], 
                    'sc_date'=>date('Y-m-d', strtotime($formData['date'])), 
                    'sc_createdby'=>$userId, 
                    'sc_createddt'=>date('Y-m-d H:i:s')
                );
                $this->model->updateSchedule($data, $schedule['sc_id']);
                
                //history
                $dataHis = array(
                    'sc_id'=>$schedule['sc_id'], 
                    'idGroup'=>$schedule['idGroup'], 
                    'IdLecturer'=>$schedule['IdLecturer'], 
                    'idCollege'=>$schedule['idCollege'], 
                    'idBranch'=>$schedule['idBranch'], 
                    'idClassType'=>$schedule['idClassType'], 
                    'sc_day'=>$schedule['sc_day'], 
                    'sc_start_time'=>$schedule['sc_start_time'], 
                    'sc_end_time'=>$schedule['sc_end_time'], 
                    'sc_venue'=>$schedule['sc_venue'], 
                    'sc_class'=>$schedule['sc_class'], 
                    'sc_date'=>$schedule['sc_date'], 
                    'sc_remark'=>$schedule['sc_remark'],
                    'sc_status'=>$schedule['sc_status'],
                    'sc_createdby'=>$userId, 
                    'sc_createddt'=>date('Y-m-d H:i:s')
                );
                $this->model->insertScheduleHistory($dataHis);
                
                //update qp event
                $getQpEvents = $scheduleDB->getQpEvent($formData['groupid'], $schedule['sc_id']);

                if ($getQpEvents){
                    foreach($getQpEvents as $getQpEvent){
                    //insert qp_event history
                        $qpHistoryData = array(
                            'event_id'=>$getQpEvent['id'], 
                            'venue'=>$getQpEvent['venue'], 
                            'start_day'=>$getQpEvent['start_day'], 
                            'end_day'=>$getQpEvent['end_day'], 
                            'start_time'=>$getQpEvent['start_time'], 
                            'end_time'=>$getQpEvent['end_time'], 
                            'date_created'=>$getQpEvent['date_created'], 
                            'last_updated'=>$getQpEvent['last_updated'], 
                            'status'=>$getQpEvent['status'], 
                            'class_duration'=>$getQpEvent['class_duration'], 
                            'lecturer_code'=>$getQpEvent['lecturer_code'], 
                            'lecturer_name'=>$getQpEvent['lecturer_name'], 
                            'lecturer_email'=>$getQpEvent['lecturer_email'], 
                            'class_date'=>$getQpEvent['class_date'], 
                            'class_day'=>$getQpEvent['class_day'], 
                            'class_start_time'=>$getQpEvent['class_start_time'], 
                            'class_end_time'=>$getQpEvent['class_end_time'], 
                            'sequence_no'=>$getQpEvent['sequence_no'], 
                            'field_change1'=>$getQpEvent['field_change1'], 
                            'field_change2'=>$getQpEvent['field_change2'], 
                            'field_change3'=>$getQpEvent['field_change3'], 
                            'field_change4'=>$getQpEvent['field_change4'], 
                            'field_change5'=>$getQpEvent['field_change5'], 
                            'field_change6'=>$getQpEvent['field_change6'], 
                            'field_change7'=>$getQpEvent['field_change7']
                        );
                        $scheduleDB->insertQpEventHistory($qpHistoryData);

                        $classstarttime = $formData["starttime"];
                        $classendtime = $formData["endtime"];

                        if ($schedule["sc_date"]=='0000-00-00' || $schedule["sc_date"]==null){
                            $start_day = 0;
                            $end_day = 0;
                        }else{
                            $start_day = strtotime($formData["date"].' '.$classstarttime);
                            $end_day = strtotime($formData["date"].' '.$classendtime);
                        }

                        if ($getQpEvent['program_mode']=='Full Time'){
                            $program_mode = 2;
                        }else{
                            $program_mode = 1;
                        }

                        $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                        $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

                        $venue = $scheduleDB->getDefination($formData["venue"]);

                        $lecturer = $scheduleDB->getLecturer($formData["lecturer"]);

                        //status update
                        $field_change1 = 0;
                        $field_change2 = 0;
                        $field_change3 = 0;
                        $field_change4 = 0;
                        $field_change5 = 0;
                        $cstatus = 0;

                        if ($start_day != $getQpEvent['start_day']){
                            $field_change1 = 1;
                            $cstatus = 1;
                        }

                        if ($data["sc_day"] != $getQpEvent['class_day']){
                            $field_change2 = 1;
                            $cstatus = 1;
                        }

                        if ($start_time != $getQpEvent['start_time'] || $end_time != $getQpEvent['end_time']){
                            $field_change3 = 1;
                            $cstatus = 1;
                        }

                        if ($venue['DefinitionDesc'] != $getQpEvent['venue']){
                            $field_change4 = 1;
                            $cstatus = 1;
                        }

                        if ($formData["idLecturer"] != $getQpEvent['lecturer_code']){
                            $field_change5 = 1;
                            $cstatus = 1;
                        }

                        $qpData = array(
                            'start_day'=>$start_day,
                            'end_day'=>$end_day,
                            'class_date'=>$start_day,
                            'class_day'=>date('l', strtotime($formData['date'])),
                            'start_time'=>$start_time,
                            'end_time'=>$end_time,
                            'class_start_time'=>($classstarttime*60),
                            'class_end_time'=>($classendtime*60),
                            'venue'=>$venue['DefinitionDesc'],
                            'lecturer_code'=>$formData["lecturer"],
                            'lecturer_name'=>$lecturer['FullName'],
                            'lecturer_email'=>$lecturer['Email'],
                            'status'=>$cstatus,
                            'last_updated'=>date('Y-m-d H:i:s'),
                            'field_change1'=>$field_change1, 
                            'field_change2'=>$field_change2, 
                            'field_change3'=>$field_change3, 
                            'field_change4'=>$field_change4, 
                            'field_change5'=>$field_change5 
                        );
                        $scheduleDB->updateQpEvent($qpData, $getQpEvent['id']);
                    }
                }
            }
            
            $scheduleData = array(
                'ctd_scdate'=>date('Y-m-d', strtotime($formData['date']))
            );
            $this->model->updateAttendanceDate($scheduleData, $formData['groupid'], $scdate);
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl.'/generalsetup/attendance/index/idGroup/'.$formData['groupid']);
        }
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }

    public function attendanceTestAction(){
        $attendanceInfo = $this->model->calculateAttendance(1871, 3769, 830);
        var_dump($attendanceInfo);
        exit;
    }
}
?>