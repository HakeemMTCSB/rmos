<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_InstructorWorkloadController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_InstructorWorkload();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Instructor Workload Setup');
        
        $list = $this->model->getWorkloadList();
        $this->view->list = $list;
    }
    
    public function addAction(){
        $this->view->title = $this->view->translate('Add Instructor Workload');
        
        $form = new GeneralSetup_Form_InstructorWorkload();
        $this->view->form = $form;
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $check = $this->model->checkWorkload($formData['position'], $formData['jobtype'], $formData['workloadtype']);
            
            if ($check){
                $this->_helper->flashMessenger->addMessage(array('error' => "Duplicate data."));
                $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/add/');
            }else{
                $data = array(
                    'twi_position'=>$formData['position'],
                    'twi_jobtype'=>$formData['jobtype'], 
                    'twi_workloadtype'=>$formData['workloadtype'], 
                    'twi_min'=>$formData['workloadtype']==963 ? $formData['min']:0, 
                    'twi_max'=>$formData['workloadtype']==963 ? $formData['max']:0, 
                    'twi_nostudent'=>$formData['workloadtype']==964 ? $formData['nostudent']:0, 
                    'twi_upddate'=>date('Y-m-d H:i:s'), 
                    'twi_updby'=>$getUserIdentity->id
                );
                $id = $this->model->insertWorkload($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Data Updated."));
                $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/edit/id/'.$id);
            }
        }
    }
    
    public function editAction(){
        $this->view->title = $this->view->translate('Edit Instructor Workload');
        
        $id = $this->_getParam('id', 0);
        
        $info = $this->model->getWorkload($id);
        
        $form = new GeneralSetup_Form_InstructorWorkload();
        $this->view->form = $form;
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($info){
            
            $data = array(
                'position'=>$info['twi_position'],
                'jobtype'=>$info['twi_jobtype'], 
                'workloadtype'=>$info['twi_workloadtype'], 
                'min'=>$info['twi_min'], 
                'max'=>$info['twi_max'], 
                'nostudent'=>$info['twi_nostudent']
            );
            
            $form->populate($data);
            
            $this->view->info = $info;
        }else{
            $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/index/');
        }
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $check = $this->model->checkWorkload($formData['position'], $formData['jobtype'], $formData['workloadtype']);
            
            if ($check && $check['twi_id']!=$id){
                $this->_helper->flashMessenger->addMessage(array('error' => "Cannot Save Duplicate data."));
                $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/edit/id/'.$id);
            }else{
                $data = array(
                    'twi_position'=>$formData['position'],
                    'twi_jobtype'=>$formData['jobtype'], 
                    'twi_workloadtype'=>$formData['workloadtype'], 
                    'twi_min'=>$formData['workloadtype']==963 ? $formData['min']:0, 
                    'twi_max'=>$formData['workloadtype']==963 ? $formData['max']:0, 
                    'twi_nostudent'=>$formData['workloadtype']==964 ? $formData['nostudent']:0, 
                    'twi_upddate'=>date('Y-m-d H:i:s'), 
                    'twi_updby'=>$getUserIdentity->id
                );
                $this->model->editWorkload($data, $id);

                $this->_helper->flashMessenger->addMessage(array('success' => "Data Updated."));
                $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/edit/id/'.$id);
            }
        }
    }
    
    public function deleteAction(){
        
        $id = $this->_getParam('id', 0);
        
        $this->model->deleteWorkload($id);
        
        $this->_helper->flashMessenger->addMessage(array('success' => "Data Deleted."));
        $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/index/');
        
        exit;
    }
    
    public function individualWorkloadAction(){
        $id = $this->_getParam('id', 0);
        $this->view->id = $id;
        
        $form = new GeneralSetup_Form_InstructorWorkload();
        $this->view->form = $form;
        
        $info = $this->model->getWorkloadIndividual($id);
        $this->view->info = $info;
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $check = $this->model->checkWorkloadIndividual($formData['staffid'], $formData['workloadtype']);
            
            if ($check){
                $this->_helper->flashMessenger->addMessage(array('error' => "Duplicate data."));
                $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/individual-workload/id/'.$formData['staffid']);
            }else{
                $data = array(
                    'tiw_staffid'=>$formData['staffid'],
                    'tiw_workloadtype'=>$formData['workloadtype'], 
                    'tiw_min'=>$formData['workloadtype']==963 ? $formData['min']:0, 
                    'tiw_max'=>$formData['workloadtype']==963 ? $formData['max']:0, 
                    'tiw_nostudent'=>$formData['workloadtype']==964 ? $formData['nostudent']:0, 
                    'tiw_upddate'=>date('Y-m-d H:i:s'), 
                    'tiw_updby'=>$getUserIdentity->id
                );
                $id = $this->model->insertWorkloadIndividual($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Data Updated."));
                $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/individual-workload/id/'.$formData['staffid']);
            }
        }
    }
    
    public function deleteIndividualAction(){
        $id = $this->_getParam('id', 0);
        
        $this->model->deleteWorkload($id);
        
        $this->_helper->flashMessenger->addMessage(array('success' => "Data Deleted."));
        $this->_redirect( $this->baseUrl . '/generalsetup/instructor-workload/individual-workload/id/'.$id);
    }
}