<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_BarringsetupController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_Barringsetup();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate("Barring Setup");
        $barringSetupList = $this->model->getBarringSetup();
        //$roleList = $this->model->getRoleList();
        $this->view->barringSetupList = $barringSetupList;
        //$this->view->roleList = $roleList;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            $checkBarring = $this->model->checkBarringSetup($formData['key']);
            
            $role = implode('|', $formData['roleid']);
                
            if ($checkBarring){ //update
                $data = array(
                    'tbs_role'=>$role, 
                    'tbs_upddate'=>date('Y-m-d'), 
                    'tbs_updby'=>$userId
                );
                $this->model->updateBarringSetup($data, $checkBarring['tbs_id']);
            }else{ //insert
                $data = array(
                    'tbs_type'=>$formData['key'],
                    'tbs_role'=>$role, 
                    'tbs_upddate'=>date('Y-m-d'), 
                    'tbs_updby'=>$userId
                );
                $this->model->insertBarringSetup($data);
            }
            
            /*
             * role part
             */
            
            //delete role
            $this->model->deleteRole($formData['key']);
            
            //insert role
            if (count($formData['roleid']) > 0){
                foreach ($formData['roleid'] as $roleLoop){
                    $roleData = array(
                        'brole_type'=>$formData['key'], 
                        'brole_role'=>$roleLoop,
                        'brole_updby'=>$userId, 
                        'brole_upddate'=>date('Y-m-d')
                    );
                    $this->model->insertRole($roleData);
                }
            }
            
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/generalsetup/barringsetup/index');
        }
    }
    
    public function getroleAction(){
        $type = $this->_getParam('key', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $roleList = $this->model->getRoleList($type);
        
        $json = Zend_Json::encode($roleList);
		
	echo $json;
	exit();
    }
}

