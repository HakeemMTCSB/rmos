<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_LearningMaterialController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_LearningMaterial();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Learning Material');
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $form = new GeneralSetup_Form_LearningMaterialSearchForm(array('semesterid'=>$formData['semester']));
            $this->view->form = $form;
            $form->populate($formData);
            
            $studentList = $this->model->getStudent($formData['semester'], $formData['course']);
            
            $this->view->studentList = $studentList;
            $this->view->formData = $formData;
        }else{
            $form = new GeneralSetup_Form_LearningMaterialSearchForm();
            $this->view->form = $form;
        }
    }
    
    public function viewAction(){
        $this->view->title = $this->view->translate('Learning Material');
        $studentId = $this->_getParam('id', 0);
        $subjectId = $this->_getParam('subjectid', 0);
        $semesterId = $this->_getParam('semesterid', 0);
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            if (isset($formData['editaccesscode']) && count($formData['editaccesscode']) > 0){
                foreach ($formData['editaccesscode'] as $key=>$accesscode){
                    $data = array(
                        'lmt_accesscode'=>$accesscode
                    );
                    $this->model->updateLearningMaterial($data, $key);
                }
            }
            
            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved.'));
            $this->_redirect($this->baseUrl . '/generalsetup/learning-material/view/id/'.$studentId.'/subjectid/'.$subjectId.'/semesterid/'.$semesterId);
            
        }
        
        $this->view->subjectid = $subjectId;
        $this->view->semesterid = $semesterId;
        
        $studentInfo = $this->model->getStudentInfo($studentId);
        $learningMaterialList = $this->model->getLearningMaterial($subjectId, $semesterId);
        $isbnList = $this->model->getLearningMaterialList($studentId, $subjectId, $semesterId);
        
        $this->view->studentInfo = $studentInfo;
        $this->view->learningMaterialList = $learningMaterialList;
        $this->view->isbnList = $isbnList;
    }
    
    public function addAction(){
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;
            
            $check = $this->model->checkLearningMaterial($formData['studentid'], $formData['isbn']);
            
            if (!$check){
                $data = array(
                    'lmt_tlmid'=>$formData['isbn'], 
                    'lmt_studentid'=>$formData['studentid'], 
                    'lmt_accesscode'=>$formData['accesscode'], 
                    'lmt_upddate'=>date('Y-m-d H:i:s'), 
                    'lmt_updby'=>$userId
                );
                $this->model->addLearningMaterial($data);
                
                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved.'));
            }else{
                $this->_helper->flashMessenger->addMessage(array('error' => 'Duplicate ISBN.'));
            }
            
            //redirect here
            $this->_redirect($this->baseUrl . '/generalsetup/learning-material/view/id/'.$formData['studentid'].'/subjectid/'.$formData['subjectid'].'/semesterid/'.$formData['semesterid']);
        }
    }
    
    public function deleteAction(){
        $id = $this->_getParam('id', 0);
        $studentId = $this->_getParam('studentid', 0);
        $subjectId = $this->_getParam('subjectid', 0);
        $semesterId = $this->_getParam('semesterid', 0);
        
        $this->model->deleteLearningMaterial($id);
        
        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Information Deleted.'));
        $this->_redirect($this->baseUrl . '/generalsetup/learning-material/view/id/'.$studentId.'/subjectid/'.$subjectId.'/semesterid/'.$semesterId);
    }
    
    public function getCourseAction(){
        $semesterId = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseList = $this->model->getCourse($semesterId);
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
}