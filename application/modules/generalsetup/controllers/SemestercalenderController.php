<?php

class GeneralSetup_SemestercalenderController extends Base_Base
{
    private $lobjsemester;
    private $lobjactivity;
    private $lobjcalender;
    private $_gobjlog;
    private $lobjsemestercalenderForm;
    private $locale;
    private $registry;

    public function init()
    {
        $this->fnsetObj();
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->registry = Zend_Registry::getInstance();
        $this->locale = $this->registry->get('Zend_Locale');
        $this->_sis_session = new Zend_Session_Namespace('sis');

    }

    public function fnsetObj()
    {
        $this->lobjsemester = new GeneralSetup_Model_DbTable_Semester();
        $this->lobjactivity = new GeneralSetup_Model_DbTable_Activity();
        $this->lobjcalender = new GeneralSetup_Model_DbTable_Calender();
        $this->lobjsemestercalenderForm = new GeneralSetup_Form_Semestercalender(); //intialize user lobjuniversityForm.

    }

    //Index Action for calender display
    public function indexAction()
    {

        $this->view->title = "Calendar Setup";

        $this->view->id_activity_type = $this->_getParam('type', 0);

        //get activity list
        $this->view->activitylist = $this->lobjactivity->fngetActivityDetails();

        //get activity type list
        $defDb = new App_Model_General_DbTable_Definationms();
        $this->view->activity_type = $defDb->getDataByType(119);

    }

    //Index Action for calender display
    public function listAction()
    {
        $this->view->title = "Calendar Activity Listing";

        $form = new GeneralSetup_Form_SearchActivityCalendar();
        $this->view->form = $form;

        $this->view->id_activity_type = $this->_getParam('type', 0);

        $eventDb = new GeneralSetup_Model_DbTable_Activity();

        if ($this->_request->isPost()) {

            if ($form->isValid($_POST)) {

                $formData = $this->_request->getPost();

                $form->populate($formData);

                $data = $eventDb->searchEventList($formData);

            }

        } else {
            $data = $eventDb->searchEventList();
        }

        $this->view->data = $data;

    }


    // Function to create new calender
    public function addAction()
    {
        $this->view->title = "Add New Calendar Activity";

        $semesterId = $this->_getParam('semester_id', 0);

        $this->view->semesterId = $semesterId;
        $this->view->lobjsemestercalenderForm = $this->lobjsemestercalenderForm;
        $this->view->lobjsemestercalenderForm->IdSemester->setValue($semesterId);

        if ($this->_request->isPost()) {
            $formData = $this->getRequest()->getPost();
            //asd($formData);
            if (!$this->view->lobjsemestercalenderForm->isValid($this->getRequest()->getPost())) {
                $this->view->postdata = $formData;
                return $this->render("add");
            }
            //die;
            unset($formData['Save']);
            unset($formData['Back']);
            if ($this->lobjactivity->fnaddCalenderActivity($formData)) {

                $this->gobjsessionsis->flash = array('message' => 'New semester activity successfully added', 'type' => 'success');
                $this->_redirect($this->baseUrl . '/generalsetup/semester/editsemester/id/'.$semesterId);
            }
        }
    }

    public function editAction()
    {
        $this->view->title = "Edit Calendar";

        $calenderId = $this->_getParam('id', 0);

        $auth = Zend_Auth::getInstance();

        $ret = $this->lobjactivity->getcalenderDetails($calenderId);
        $this->view->lobjsemestercalenderForm = $this->lobjsemestercalenderForm;

        $this->view->semesterId = $ret['IdSemesterMain'];
        $this->view->lobjsemestercalenderForm->IdActivity->setValue($ret['IdActivity']);
        $this->view->lobjsemestercalenderForm->IdSemester->setValue($ret['IdSemesterMain']);
        $this->view->lobjsemestercalenderForm->StartDate->setValue($ret['StartDate']);
        $this->view->lobjsemestercalenderForm->EndDate->setValue($ret['EndDate']);
        $this->view->semesterId = $ret['IdSemesterMain'];

        if ($this->getRequest()->isPost()) {
            $formData = $larrvalidatedata = $this->getRequest()->getPost();

            if ($this->lobjsemestercalenderForm->isValid($larrvalidatedata)) {

                $this->lobjactivity->fnupdateCalenderActivity($formData, $calenderId);//update university

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                 'level'       => $priority,
                                 'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                 'time'        => date('Y-m-d H:i:s'),
                                 'message'     => 'Semester Calendar Edit Id =' . $calenderId,
                                 'Description' => Zend_Log::DEBUG,
                                 'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                $this->gobjsessionsis->flash = array('message' => 'Semester calendar has been successfully saved', 'type' => 'success');
                $this->_redirect($this->baseUrl . '/generalsetup/semester/editsemester/id/'.$ret['IdSemesterMain'].'#tab-3');
            }
        }
    }

    public function deleteAction()
    {
        $this->view->title = "Delete Calendar";
        $calenderId = $this->_getParam('id', 0);

        $ret = $this->lobjactivity->getcalenderDetails($calenderId);
        $this->lobjactivity->fnDeletecalender($calenderId);

        $this->_helper->flashMessenger->addMessage(array('success' => "Semester Calendar deleted"));
        $this->_redirect($this->baseUrl . '/generalsetup/semester/editsemester/id/'.$ret['IdSemesterMain'].'/#tab-3');
    }

    public function ajaxGetEventAction()
    {

        $start = $this->_getParam('start', 0);
        $end = $this->_getParam('end', 0);
        $activity_type = $this->_getParam('activity_type', 0);
        $semesterId = $this->_getParam('semester_id', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $dtStart = $start > 0 ? date('Y-m-d', $start) : '';
        $dtEnd = $end > 0 ? date('Y-m-d', $end) : '';

        //query here
        $eventDb = new GeneralSetup_Model_DbTable_Activity();
        $data = $eventDb->getEventList($dtStart, $dtEnd, $activity_type, null, $semesterId);

        //add course and venue detail
        $schedule = array();
        $i = 0;
        foreach ($data as $event) {

            $schedule[$i] = array(
                'id'              => $event['id'],
                'title'           => $event['ActivityName'],
                'allDay'          => true,
                'start'           => $event['StartDate'],
                'end'             => $event['EndDate'],
                'backgroundColor' => $event['ActivityColorCode'],
                'borderColor'     => '#000000',
                'className'       => 'tableCal'
            );
            $i++;
        }

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        echo $json = Zend_Json::encode($schedule);

        $this->view->json = $json;
        exit;

    }

    public function viewAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title = "View Calendar";

        $semesterId = $this->_getParam('semester_id', 0);

        $this->view->id_activity_type = $this->_getParam('type', 0);

        //get activity list
        $this->view->activitylist = $this->lobjactivity->fngetActivityDetails($semesterId, $group=1);

        //get activity type list
        $defDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $this->view->activity_type = $defDb->getListDataByCodeType('activity-type');
        $this->view->semesterId = $semesterId;

    }

}
