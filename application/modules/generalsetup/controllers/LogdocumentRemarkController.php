<?php
class GeneralSetup_LogdocumentRemarkController extends Base_Base {
	
	private $locale;
	private $registry;
	//private $lobjNewawardform;
	private $Logremark;
	private $_gobjlog;

	
	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');		
	}
	
	
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();		
		$this->lobjLogform = new GeneralSetup_Form_LogdocumentRemark();
		//$this->lobjNewawardform	= new GeneralSetup_Form_Newaward();
		$this->Logremark = new GeneralSetup_Model_DbTable_LogdocumentRemark();
	}

    public function indexAction(){

        $this->view->title = "Remark Setup";
        $form = new GeneralSetup_Form_LogdocumentRemarkSearch();
        $logRemarkDb = new GeneralSetup_Model_DbTable_LogdocumentRemark();
        $query = $logRemarkDb->getRemarkList(null,'sql');
        $pageCount = 50;

        //post
        if ($this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $logRemarkDb->getRemarkList($formData,'sql');

        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();


        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;

    }

    public function editAction(){

        $id = $this->_getParam('id',null);

        if($id==null){
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument-remark', 'action'=>'index'),'default',true));
        }

        $this->view->title = "Edit remark Setup";

        $form = new GeneralSetup_Form_LogdocumentRemark();

        $logRemarkDb = new GeneralSetup_Model_DbTable_LogdocumentRemark();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'tlr_id' => $id,
                    'tlr_description' => strtoupper($formData['tlr_description']),
                );



                $logRemarkDb->update($data,array('tlr_id=?'=>$id));

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update Remark Setup');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument-remark', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }else{
            $data = $logRemarkDb->fetchRow(array('tlr_id=?'=>$id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }

	public function addAction(){
        $this->view->title = "Add Remark Setup";
        $auth = Zend_Auth::getInstance();

        $form = new GeneralSetup_Form_LogdocumentRemark();


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $logRemarkDb = new GeneralSetup_Model_DbTable_LogdocumentRemark();

                $data = array(
                    'tlr_description' => strtoupper($formData['tlr_description']),
                    'tlr_createdby' => $auth->getIdentity()->iduser,
                    'tlr_createddt' => date ('Y-m-d H:i:s'),

                );

                $logRemarkDb->addData($data);

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add Remark Setup');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument-remark', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
	}

	
}


?>