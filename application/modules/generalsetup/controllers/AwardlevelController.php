<?php
class GeneralSetup_AwardlevelController extends Base_Base {
	
	private $locale;
	private $registry;
	private $Awardlevelform;
	private $lobjNewawardform;
	private $Awardlevel;
	private $_gobjlog;
	private $lobjvalidator;
	
	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');		
	}
	
	
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();		
		$this->lobjAwardlevelform = new GeneralSetup_Form_Awardlevel();
		$this->lobjNewawardform	= new GeneralSetup_Form_Newaward();
		$this->Awardlevel = new GeneralSetup_Model_DbTable_Awardlevel();
		$this->lobjdeftype = new App_Model_Definitiontype();
	}

    public function indexAction(){

        $this->view->title = "Award";
        $form = new GeneralSetup_Form_AwardlevelSearch();
        $awardLevelDb = new GeneralSetup_Model_DbTable_Awardlevel();
        $query = $awardLevelDb->getAwardList(null,'sql');
        $pageCount = 50;

        //post
        if ($this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $awardLevelDb->getAwardList($formData,'sql');

        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();


        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;

    }

    public function editawardAction(){

        $id = $this->_getParam('id',null);

        if($id==null){
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'awardlevel', 'action'=>'index'),'default',true));
        }

        $this->view->title = "Edit Award Level";

        $form = new GeneralSetup_Form_Awardlevel();

        $awardLevelDb = new GeneralSetup_Model_DbTable_Awardlevel();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'GradeId' => $id,
                    'GradeDesc' => $formData['GradeDesc'],
                    'GradeType' => $formData['GradeType'],
                    'GradeDesc_Malay' => $formData['GradeDesc_Malay'],
                   /* 'ac_status' => $formData['ac_status'],*/
                );



                $awardLevelDb->update($data,array('Id=?'=>$id));

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update Award Level');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'awardlevel', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }else{
            $data = $awardLevelDb->fetchRow(array('Id=?'=>$id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }

	/*public function awardlevelAction(){
		
		$this->view->lobjAwardlevelform= $this->lobjAwardlevelform;
		
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Award');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjAwardlevelform->IdLevel->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
			$this->lobjAwardlevelform->IdAllowanceLevel->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		$lintAward = ( int ) $this->_getParam ( 'id' );
		$this->view->Idaward = $lintAward;
		$lintAwardDesc = ( int ) $this->_getParam ( 'id' );
		$this->view->lobjAwardlevelform->IdLevel -> setValue($lintAwardDesc);
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjAwardlevelform->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAwardlevelform->UpdUser->setValue( $auth->getIdentity()->iduser);
	
		
				$larrResult = $this->Awardlevel->fnSearchAwardlevels($lintAward);	
				
				if($larrResult){
				$this->view->paginator = $this->lobjCommon->fnPagination($larrResult,$lintpage,$lintpagecount);
		    	$this->gobjsessionsis->Checklistpaginatorresult = $larrResult;	
				}
		
			if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();
			if ($this->lobjAwardlevelform->isValid($larrFormData)) {
			
				unset($larrFormData['Save']);		
				$this->Awardlevel->fnDeleteAwardlevel($larrFormData['IdLevel']);				
				$this->Awardlevel->fnaddAwardlevel($larrFormData);	
				
					// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Award Level Add id='.$lintAward,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
						
				$this->_redirect( $this->baseUrl . '/generalsetup/awardlevel/');
			}
			
		}	
		
	}*/
	
/*	public function awardlevellistAction() {
		
		$this->view->lobjAwardlevelform= $this->lobjAwardlevelform;
		
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Award');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjAwardlevelform->IdLevel->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjAwardlevelform->IdAllowanceLevel->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		
		
		$lintIdAllowance = ( int ) $this->_getParam ( 'id' );
		$this->view->IdAllowance = $lintIdAllowance;				
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjAwardlevelform->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAwardlevelform->UpdUser->setValue( $auth->getIdentity()->iduser);
		
		$larrEditResult = $this->Awardlevel->fnviewAwardListDtls($lintIdAllowance);		
		$this->lobjAwardlevelform->populate($larrEditResult);
		
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();
			if ($this->lobjAwardlevelform->isValid($larrFormData)) {
				unset($larrFormData['Save']);
				$result=$this->Awardlevel->fnupdateAwardlistDtls($lintIdAllowance,$larrFormData);	
						
					
				$this->_redirect( $this->baseUrl . '/generalsetup/awardlevel/awardlevel/id/'.$larrFormData['IdLevel']);
			}
			
		}

		
	}*/
	
	public function newawardAction(){
        $this->view->title = "Add Award Level";

        $form = new GeneralSetup_Form_Awardlevel();


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $awardLevelDb = new GeneralSetup_Model_DbTable_Awardlevel();
                //get max id and insert into grade id
                $output = $awardLevelDb->getLastId();

                $maxId = $output[0]['maxId'];
                $maxId = $maxId+1;

                $data = array(
                    'GradeId' => 0,
                    'GradeDesc' => $formData['GradeDesc'],
                    'GradeType' => $formData['GradeType'],
                    'GradeDesc_Malay' => $formData['GradeDesc_Malay'],
                );

                $lastId = $awardLevelDb->addData($data);
                $data = array(
                    'GradeId' => $lastId
                );
                $awardLevelDb->update($data, 'Id = ' . $lastId);



                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new account code');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'awardlevel', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
	}
	

	
}




?>