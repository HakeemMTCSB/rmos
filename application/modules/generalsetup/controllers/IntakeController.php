<?php

class GeneralSetup_IntakeController extends Base_Base {
	private $lobjagentpayment;

	/**
	 *
	 * @see Zend_Controller_Action::init()
	 */
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	/**
	 *
	 * @see Base_Base::fnsetObject()
	 */
	public function fnsetObj() {
		$this->lobjIntakeForm = new GeneralSetup_Form_Intake();
		$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Program();
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$this->lobjintakemapping = new GeneralSetup_Model_DbTable_IntakeBranchMapping();
		$this->lobjform = new App_Form_Search ();
		$this->lobjagentpayment = new GeneralSetup_Model_DbTable_Agentpaymentdetail();
		$this->regLocDB = new GeneralSetup_Model_DbTable_IntakeRegLocation();
        $this->intakeSchemeDB = new GeneralSetup_Model_DbTable_IntakeScheme();
        $this->countryDB = new GeneralSetup_Model_DbTable_Country();
	}

	/**
	 *
	 * Method to handle the index action
	 */
	public function indexAction() {
		
		
		 
		$this->view->title=$this->view->translate("Intake Setup");
		$this->view->lobjform = $this->lobjform;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$larrresult = $this->lobjintake->fngetIntakeList();
		$lobjsemester = new GeneralSetup_Model_DbTable_Semester();
		$larrscheme = $lobjsemester->fnGetShcemeList();
		$this->view->lobjform->field5->addMultiOptions($larrscheme);

		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->intakepaginatorresult);
		//$lintpagecount =$this->gintPageCount;
        $lintpagecount = 50;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
        $totalRecord = count($larrresult);
        $this->view->totalRecord =$totalRecord;

		if(isset($this->gobjsessionsis->intakepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->intakepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjintake->fnSearchIntake( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->intakepaginatorresult = $larrresult;
                $totalRecord = count($larrresult);
                $this->view->totalRecord =$totalRecord;
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->gobjsessionsis->flash = array('message' => 'Information has been successfully saved', 'type' => 'success');
			$this->_redirect( $this->baseUrl . '/generalsetup/intake/index');
		}

	}

	public function newintakeAction() {
        
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjIntakeForm = $this->lobjIntakeForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjIntakeForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjIntakeForm->UpdUser->setValue( $auth->getIdentity()->iduser);

        // THIS PART COMES if there is mismatch when performing Copy intake
		$errorMsg = $this->getRequest()->getParam('errorMsg');
		if($errorMsg=='1') { $this->view->errorMsg = '1'; }
		// END

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			//var_dump($formData); exit;
			if ($this->lobjIntakeForm->isValid($formData)) {

				// Write Logs
				$priority=Zend_Log::INFO;
				
				//add intake
				$IdIntake = $this->lobjintake->fnaddIntake($formData);
					
				
				if($IdIntake=='mismatch' || $IdIntake=='duplicate') {
					if($IdIntake=='mismatch') $this->view->errorMsg = '1';
					if($IdIntake=='duplicate') $this->view->errorMsg = '2';
				} else {
		
						//add intake program scheme
						for($i = 0; $i < count($formData['IdProgram']); $i++) {
							
								$intakeProgram['IdIntake']=$IdIntake;
								$intakeProgram['IdProgram']=$formData['IdProgram'][$i];
								$intakeProgram['IdStudentCategory']=$formData['IdStudentCategory'][$i];

								$intakeProgram['ApplicationStartDate']= $formData['appStartDate'][$i] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['appStartDate'][$i]));

								$intakeProgram['ApplicationEndDate']=$formData['appEndDate'][$i] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['appEndDate'][$i]));

	                            $intakeProgram['AdmissionStartDate']=$formData['adStartDate'][$i] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['adStartDate'][$i]));

								$intakeProgram['AdmissionEndDate']=$formData['adEndDate'][$i] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['adEndDate'][$i]));


								$intakeProgram['IdProgramScheme']=$formData['IdProgramScheme'][$i];		
								$intakeProgram['Quota']=$formData['programQuota'][$i];
								$intakeProgram['createdby'] = $auth->getIdentity()->iduser;
							    $intakeProgram['createddt'] = date ('Y-m-d H:i:s');			 
							       
							    $intakeProgramDB = new GeneralSetup_Model_DbTable_IntakeProgram(); 				
							    $result = $intakeProgramDB->checkDuplicate($formData['IdProgramScheme'][$i],$formData['IdProgram'][$i],$formData['IdStudentCategory'][$i],$IdIntake);
							    
							    //add
							    if(!$result){
							    	
							    	//add intake program
									$ip_id = $intakeProgramDB->addData($intakeProgram);
									
									
									//add intake branch mapping
									/*$intakeMap['IdIntake'] = $IdIntake;
									$intakeMap['ip_id'] = $ip_id;
									$intakeMap['IdProgram'] = $formData['IdProgram'][$i];									
									$intakeMap['UpdUser'] = $formData['UpdUser'];
									$intakeMap['UpdDate'] = $formData['UpdDate'];
											
									if($formData['IdBranch'][$i]=='all'){
										//loop all branch										
										$branch_list = $this->lobjintake->fngetBranchList();
										if(count($branch_list)>0){
											foreach($branch_list as $branch){
												//mapping branch
												$intakeMap['IdBranch'] = $branch['key'];											
												$this->lobjintakemapping->fnaddIntakeMapping($intakeMap);
											}
										}
									}else{
										$intakeMap['IdBranch'] = $formData['IdBranch'][$i];
										$this->lobjintakemapping->fnaddIntakeMapping($intakeMap);
									}*/
									
									
									//add registartion location		
									$info = array();
									
									if ( isset($formData['rladdress'][$i]) )
									{
										$info['IdIntake'] = $IdIntake;																
										$info['rl_date']=$formData['rldate'][$i];
										$info['rl_time']=$formData['rltime'][$i];
										$info['rl_address']=$formData['rladdress'][$i];
										$info['rl_createdby'] = $auth->getIdentity()->iduser;
										$info['rl_createddt'] = date('Y-m-d H:i:s');
										

										
										if($formData['rlbranch'][$i]=='all'){
											//loop all branch										
											$branch_list = $this->lobjintake->fngetBranchList();
											if(count($branch_list)>0){
												foreach($branch_list as $branch){
													//mapping branch
													$info['rl_branch'] = $branch['key'];											
													$this->regLocDB->addData($info);
												}
											}
										}else{
											$info['rl_branch']=$formData['rlbranch'][$i];
											$this->regLocDB->addData($info);
										}
									}
																	
									
								
							    }else{
							    	//echo 'duplicate entry do not add ->skip';
							    }
							    
							    
							    
													
						}//end for
						
						$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Intake added',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
						$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
						
			
					/*	$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
										  'level' => $priority,
										  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						                  'time' => date ( 'Y-m-d H:i:s' ),
						   				  'message' => 'Branch Intake Mapping Added',
										  'Description' =>  Zend_Log::DEBUG,
										  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
						$this->_gobjlog->write ( $larrlog ); //insert to tbl_log*/

						$this->_redirect( $this->baseUrl . '/generalsetup/intake/viewreglocation/id/'.$IdIntake);
				
				}//end if 
				
				
				
				
				

			}
		}
	}

	/**
	 *
	 * Action to get program list
	 */
	public function getprogramlistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidCollege = $this->_getParam('idCollege');
		$larrprogramList = $this->lobjcollegemaster->fngetProgramFromColl($lintidCollege);

		//$data = new Zend_Dojo_Data('value', $larrprogramList->toArray(), 'label');

		//$this->_helper->autoCompleteDojo($data);
		echo json_encode($larrprogramList);
		exit;
	}

	public function editintakeAction() {
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->title=$this->view->translate("Edit Intake");
		$this->view->lobjIntakeForm = $this->lobjIntakeForm;
		$IdIntake = $this->_getParam('id', 0);
		$result = $this->lobjintake->fetchAll('IdIntake  ='.$IdIntake);
		$result = $result->toArray();
		
		foreach($result as $intakeresult){
		}
		
		$this->view->lobjIntakeForm->populate($intakeresult);
		$this->view->lobjIntakeForm->IdIntake->setValue( $IdIntake );
		$this->view->lobjIntakeForm->sem_seq->setValue( $intakeresult['sem_seq'] );
		$this->view->IdIntake = $IdIntake;

		
		$intakeProgramDB = new GeneralSetup_Model_DbTable_IntakeProgram(); 
		$this->view->program_list = $intakeProgramDB->getIntakeProgram($IdIntake);		
		 
		//$this->view->larrintakedetails = $this->lobjintakemapping->fngetIntakeMappingDetails($IdIntake);

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjIntakeForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjIntakeForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		$priority=Zend_Log::INFO;
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if ($this->lobjIntakeForm->isValid($formData)) {
                     
				$idIntake = $this->lobjintake->fnupdateIntake($formData,$IdIntake);
				
				/*$this->lobjintakemapping->fndeleteIntakeMappings($IdIntake);
				for($i = 0; $i < count($formData['IdProgram']); $i++) {
					$intakeMap = array();
					$intakeMap['IdIntake'] = $IdIntake;
					$intakeMap['IdProgram'] = $formData['IdProgram'][$i];
					$intakeMap['IdBranch'] = $formData['IdBranch'][$i];
					$intakeMap['UpdUser'] = $formData['UpdUser'];
					$intakeMap['UpdDate'] = $formData['UpdDate'];
					$this->lobjintakemapping->fnaddIntakeMapping($intakeMap);
					$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
									  'level' => $priority,
									  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					                  'time' => date ( 'Y-m-d H:i:s' ),
					   				  'message' => 'Branch Intake Mapping Added',
									  'Description' =>  Zend_Log::DEBUG,
									  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
					$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				}
		*/
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Semester Edit Id=' . $IdIntake,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

				//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'semester', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/generalsetup/intake/editintake/id/'.$IdIntake);
			}
		}
	}

	function deleteintakedetailsAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Po details Id
		$Id = $this->_getParam('id');
		$this->lobjintakemapping->fnDeleteIntakeDetailById($Id);
		echo "1";
	}

	function copyintakeAction() {
		
		$auth = Zend_Auth::getInstance();
		
		$this->view->lobjIntakeForm = $this->lobjIntakeForm;
		
		if($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			unset($formData['Copy']);
			
			$IdIntake = $formData['FromIntake'];
			
			$larrintakedetails = $this->lobjintake->fngetIntakeDetails($IdIntake);
			
			foreach($larrintakedetails as $larrintakedetails){}
			
			$larrnewintake['IntakeId'] = $formData['CopyIntakeId'];
			$larrnewintake['IntakeDesc'] = $formData['CopyIntakeDescription'];
			$larrnewintake['IntakeDefaultLanguage'] = $larrintakedetails['IntakeDefaultLanguage'];
			$larrnewintake['ApplicationStartDate'] = $formData['CopyApplicationStartDate'];
			$larrnewintake['ApplicationEndDate'] = $formData['CopyApplicationEndDate'];
            $larrnewintake['AdmissionStartDate'] = $formData['CopyAdmissionStartDate'];
			$larrnewintake['AdmissionEndDate'] = $formData['CopyAdmissionEndDate'];
			$larrnewintake['UpdUser'] = $formData['UpdUser'];
			$larrnewintake['UpdDate'] = $formData['UpdDate'];
			$lintnewid = $this->lobjintake->fnaddIntake($larrnewintake);

				if($lintnewid=='mismatch') {
					$this->_redirect( $this->baseUrl . '/generalsetup/intake/newintake/errorMsg/1');
				} else {

				//Copy Intake Mappings
				$larroldmapping = $this->lobjintakemapping->fngetIntakeMappingDetails($IdIntake);
				foreach($larroldmapping as $larrmapping) {
					unset($larrmapping['Id']);
					unset($larrmapping['BranchName']);
					unset($larrmapping['Program']);
					$larrmapping['IdIntake'] = $lintnewid;
					$larrmapping['UpdUser'] = $formData['UpdUser'];
					$larrmapping['UpdDate'] = $formData['UpdDate'];
					$this->lobjintakemapping->fnaddIntakeMapping($larrmapping);
				}
			
				//Copy Agent Rate Details
				$larragentrate = $this->lobjagentpayment->fnGetIntakeListbyIntake($IdIntake);
				foreach($larragentrate as $temp) {
					unset($temp['IdPayment']);
					$temp['Intake'] = $lintnewid;
					$temp['UpdUser'] = $formData['UpdUser'];
					$temp['UpdDate'] = $formData['UpdDate'];
					$this->lobjagentpayment->fnaddpaymentdetails($temp);
				}

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
									  'level' => $priority,
									  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					                  'time' => date ( 'Y-m-d H:i:s' ),
					   				  'message' => 'New Intake Copied=',
									  'Description' =>  Zend_Log::DEBUG,
									  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				$this->_redirect( $this->baseUrl . '/generalsetup/intake/index');
		}

		}
	}
	
function deleteProgramAction(){
		
	  //get id
	  $ip_id = $this->_getParam('ip_id', 0);  
	  $intake_id = $this->_getParam('intake_id', 0);  
	
	  $this->_helper->layout->disableLayout();
	   
	  $ajaxContext = $this->_helper->getHelper('AjaxContext');
	  $ajaxContext->addActionContext('view', 'html');
	  $ajaxContext->initContext();
	   
	  //delete
	  $intakeprogramDb = new GeneralSetup_Model_DbTable_IntakeProgram();
	  $intakeprogramDb->deleteData($ip_id); 
	 
	  //query
	  $program_list = $intakeprogramDb->getIntakeProgram($intake_id);
	   
	  $ajaxContext->addActionContext('view', 'html')
	  ->addActionContext('form', 'html')
	  ->addActionContext('process', 'json')
	  ->initContext();
	   
	  $json = Zend_Json::encode($program_list);	   
	  echo $json;
	  exit();
	}
	
    function saveProgramAction(){
		
		$auth = Zend_Auth::getInstance();
	  
		$formData = $this->getRequest()->getPost();
		
		$this->_helper->layout->disableLayout();
	   
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();		   
					
		$intakeProgram['IdIntake']=$formData['IdIntake'];
		$intakeProgram['IdProgram']=$formData['IdProgram'];
		$intakeProgram['IdStudentCategory']=$formData['IdStudentCategory'];
		$intakeProgram['ApplicationStartDate']=date('Y-m-d',strtotime($formData['appStartDate']));
		$intakeProgram['ApplicationEndDate']=date('Y-m-d',strtotime($formData['appEndDate']));
		$intakeProgram['AdmissionStartDate']=date('Y-m-d',strtotime($formData['adStartDate']));
		$intakeProgram['AdmissionEndDate']=date('Y-m-d',strtotime($formData['adEndDate']));
		$intakeProgram['IdProgramScheme']=$formData['IdProgramScheme'];		
		$intakeProgram['Quota']=$formData['programQuota'];
		$intakeProgram['createdby'] = $auth->getIdentity()->iduser;
		$intakeProgram['createddt'] = date ('Y-m-d H:i:s');
		
		
		$intakeProgramDB = new GeneralSetup_Model_DbTable_IntakeProgram(); 	

		// TODO: check duplicate before insert
		$result = $intakeProgramDB->checkDuplicate($formData['IdProgramScheme'],$formData['IdProgram'],$formData['IdStudentCategory'],$formData['IdIntake']);
		
		//add
		if(!$result){
			$intakeProgramDB->addData($intakeProgram);
			
			//branch mapping	
			
			//delete old mapping branch
			$this->lobjintakemapping->fndeleteIntakeMappings($formData['IdIntake']);
			
			//add new map branch
			$intakeMap = array();
			$intakeMap['IdIntake'] = $formData['IdIntake'];
			$intakeMap['IdProgram'] = $formData['IdProgram'];
			$intakeMap['IdBranch'] = $formData['IdBranch'];
			$intakeMap['UpdUser'] = $auth->getIdentity()->iduser;
			$intakeMap['UpdDate'] = date ('Y-m-d H:i:s');
			$this->lobjintakemapping->fnaddIntakeMapping($intakeMap);
						
						
		}else{
			//echo 'duplicate entry';
		}
			
		//query
		$program_list = $intakeProgramDB->getIntakeProgram($formData['IdIntake']);
	   
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
	   
		$json = Zend_Json::encode($program_list);	   
		echo $json;
		exit();
	 	
	}
	
	public function deleteIntakeAction(){
		
	  $intake_id = $this->_getParam('id', 0);  
		 
	  $intakeDb = new GeneralSetup_Model_DbTable_Intake();
	  $intakeDb->deleteData($intake_id);
		 
	   //delete intake program
	  $intakeprogramDb = new GeneralSetup_Model_DbTable_IntakeProgram();
	  $intakeprogramDb->deleteIntakeData($intake_id); 
	  
	  $this->_redirect( $this->baseUrl . '/generalsetup/intake/index');
	}
	
	public function getProgramDataAction(){
    	
    	$idProgram = $this->_getParam('idProgram', 0);    	
    	$type = $this->_getParam('type', 0);
    	 
    	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
        if($type=='scheme'){
        
        	$programSchemeDB = new GeneralSetup_Model_DbTable_Programscheme();
        	$rows=$programSchemeDB->getProgSchemeByProgram($idProgram);

        	
        	$row = array();
        	foreach($rows as $index=>$r){
        		$row[$index]['key']=$r['IdProgramScheme'];
        		$row[$index]['name']=$r['ProgramMode'].' - '.$r['StudyMode'].' - '.$r['ProgramType'];
        	}
        		
        }
	 
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    public function editintakeprogramAction(){
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $IdIntake = $this->_getParam('id', 0);
        $intakeProgramDB = new GeneralSetup_Model_DbTable_IntakeProgram();
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $this->view->program_list = $intakeProgramDB->getIntakeProgram($IdIntake);
        //var_dump($this->view->program_list);
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->title=$this->view->translate("Edit Programme Intake");
        $this->view->intakeInfo = $intakeInfo;
    }
    
    public function addintakeprogramAction(){
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->title=$this->view->translate("Add Programme Intake");
        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
        $IdIntake = $this->_getParam('id', 0);
        $this->view->IdIntake = $IdIntake;
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->intakeInfo = $intakeInfo;
        $auth = Zend_Auth::getInstance();
        $this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'editintakeprogram', 'id'=>$IdIntake),'default',true);
        
        if ($this->getRequest()->isPost()) {
       		 $formData = $this->getRequest()->getPost();
       		 
       		   
            if ($this->lobjIntakeForm->isValid($formData)) {
             
                $intakeProgram = array();
                $intakeProgram['IdIntake']=$IdIntake;
                $intakeProgram['IdProgram']=$formData['Program'];
                $intakeProgram['IdStudentCategory']=$formData['studentCategory'];
                
				$intakeProgram['ApplicationStartDate']=$formData['ApplicationStartDate'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['ApplicationStartDate']));
                
				$intakeProgram['ApplicationEndDate']=$formData['ApplicationEndDate'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['ApplicationEndDate']));
                
				$intakeProgram['AdmissionStartDate']=$formData['AdmissionStartDate'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['AdmissionStartDate']));
                
				$intakeProgram['AdmissionEndDate']=$formData['AdmissionEndDate'] == '' ? '0000-00-00' : date('Y-m-d',strtotime($formData['AdmissionEndDate']));
                
				$intakeProgram['IdProgramScheme']=$formData['programScheme'];		
                $intakeProgram['Quota']=$formData['quota'];
                $intakeProgram['createdby'] = $auth->getIdentity()->iduser;
                $intakeProgram['createddt'] = date ('Y-m-d H:i:s');

                
                $intakeProgramDB = new GeneralSetup_Model_DbTable_IntakeProgram(); 	


                // TODO: check duplicate before insert
                $result = $intakeProgramDB->checkDuplicate($formData['programScheme'],$formData['Program'],$formData['studentCategory'],$IdIntake);
                
                //add
                if(!$result){
                    $ip_id = $intakeProgramDB->addData($intakeProgram);

                    $this->lobjintakemapping->fndeleteIntakeMappings($IdIntake);
                    //add new map branch
                    $intakeMap = array();
                    $intakeMap['IdIntake'] = $IdIntake;
                    $intakeMap['IdProgram'] = $formData['Program'];
                    $intakeMap['IdBranch'] = $formData['Branch'];
                    $intakeMap['UpdUser'] = $auth->getIdentity()->iduser;
                    $intakeMap['UpdDate'] = date ('Y-m-d H:i:s');
					$intakeMap['ip_id'] = $ip_id;
                    $this->lobjintakemapping->fnaddIntakeMapping($intakeMap);

					$this->_helper->flashMessenger->addMessage(array('success' => "Programme added"));
                    
                    $this->_redirect($this->view->backURL);
                }else{
                    $this->_helper->flashMessenger->addMessage(array('error' => "Cannot add duplicate data"));

                    $this->_redirect($this->view->backURL);
                }
            }
        }
    }
    
    public function editintakeprogrameditAction(){
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->title=$this->view->translate("Edit Programme Intake");
        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
        $IdIntake = $this->_getParam('id', 0);
        $IdProgIntake = $this->_getParam('intakeProg', 0);
        $this->view->IdIntake = $IdIntake;
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $intakeProgramDB = new GeneralSetup_Model_DbTable_IntakeProgram();
        $intakeProgInfo  = $intakeProgramDB->getIntakeProgramInfo($IdProgIntake);
        $intakeProgInfo['quota'] = $intakeProgInfo['Quota'];
        $this->view->intakeInfo = $intakeInfo;
        $this->view->intakeProgInfo = $intakeProgInfo;
        $auth = Zend_Auth::getInstance();
        $this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'editintakeprogram', 'id'=>$IdIntake),'default',true);
        
        if ($this->getRequest()->isPost()) {
        $formData = $this->getRequest()->getPost();
            if ($this->lobjIntakeForm->isValid($formData)) {
                //var_dump($formData); exit;

                $intakeProgram['ApplicationStartDate']=$formData['ApplicationStartDate'] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['ApplicationStartDate']));
                $intakeProgram['ApplicationEndDate']=$formData['ApplicationEndDate'] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['ApplicationEndDate']));
                $intakeProgram['AdmissionStartDate']=$formData['AdmissionStartDate'] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['AdmissionStartDate']));
                $intakeProgram['AdmissionEndDate']=$formData['AdmissionEndDate'] == '' ? '0000-00-00' :date('Y-m-d',strtotime($formData['AdmissionEndDate']));
                $intakeProgram['Quota']=$formData['quota'];
                $intakeProgram['createdby'] = $auth->getIdentity()->iduser;
                $intakeProgram['createddt'] = date ('Y-m-d H:i:s');

				$intakeProgramDB->update($intakeProgram, 'ip_id = '.$IdProgIntake);
		
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

				$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'editintakeprogramedit', 'id'=>$IdIntake,'intakeProg'=>$IdProgIntake),'default',true));

              
            }else{
                $this->lobjIntakeForm->populate($intakeProgInfo);
            }
        }else{
        	
        		$intakeProgInfo['ApplicationStartDate']=$intakeProgInfo['ApplicationStartDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['ApplicationStartDate'])):'';
                $intakeProgInfo['ApplicationEndDate']=$intakeProgInfo['ApplicationEndDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['ApplicationEndDate'])):'';
                $intakeProgInfo['AdmissionStartDate']=$intakeProgInfo['AdmissionStartDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['AdmissionStartDate'])):'';
                $intakeProgInfo['AdmissionEndDate']=$intakeProgInfo['AdmissionEndDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['AdmissionEndDate'])):'';
              
            	$this->lobjIntakeForm->populate($intakeProgInfo);
        }
    }
    
	public function viewreglocationAction(){
    	
    	$this->view->title=$this->view->translate("Registration Location");
    	  
    	$IdIntake = $this->_getParam('id', 0);
    	$this->view->idIntake =  $IdIntake;
    	
    	$intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->intakeInfo = $intakeInfo;
       
        $this->view->location_list = $this->regLocDB->getLocationList($IdIntake);
        
    }
    
	public function addreglocationAction(){
    	
    	$this->view->title=$this->view->translate("Add Registration Location");

    	$IdIntake = $this->_getParam('id', 0);
    	$this->view->idIntake =  $IdIntake;
    	
    	$intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->intakeInfo = $intakeInfo;
       
        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
         
		if ($this->getRequest()->isPost()) {
       		$formData = $this->getRequest()->getPost();
            if ($this->lobjIntakeForm->isValid($formData)) {

            	$auth = Zend_Auth::getInstance();
            	            	
            	$flag = false;
            	
            	//add registartion location		
				$info['IdIntake'] = $IdIntake;																
				$info['rl_date']=date('Y-m-d',strtotime($formData['rl_date']));
				$info['rl_time']=$formData['rl_time'];
				$info['rl_address']=$formData['rl_address'];
				$info['rl_createdby'] = $auth->getIdentity()->iduser;
		        $info['rl_createddt'] = date('Y-m-d H:i:s');

                if($formData['rl_branch']=='all'){
					//loop all branch										
					$branch_list = $this->lobjintake->fngetBranchList();
					if(count($branch_list)>0){
						foreach($branch_list as $branch){
							
							//check duplicate data
            				$duplicate_branch = $this->regLocDB->checkDuplicate($IdIntake,$branch['key']);
            	
            				if(!$duplicate_branch){
								//mapping branch
								$info['rl_branch'] = $branch['key'];											
								$this->regLocDB->addData($info);

                                $scheme['rl_branch'] = $branch['key'];
								$flag = true;
            				}
						}
					}
				}else{
					
					//check duplicate data
            		$duplicate_branch = $this->regLocDB->checkDuplicate($IdIntake,$formData['rl_branch']);
            	
            		if(!$duplicate_branch){

                    //add branch
						$info['rl_branch']=$formData['rl_branch'];
						$lastId = $this->regLocDB->addData($info);

                        //add scheme info

                        for($i = 0; $i < count($formData['progScheme2']); $i++) {
                            $scheme = array();

                            $result = $this->intakeSchemeDB->checkDuplicate($formData['progScheme2'][$i], $IdIntake,  $lastId);


                            if (!$result) {
                                $scheme['IdIntake'] = $IdIntake;
                                $scheme['rl_id'] = $lastId;
                                $scheme['idScheme'] = $formData['progScheme2'][$i];
                                $scheme['reg_date'] = $formData['regdate'][$i];
                                $scheme['reg_time'] = $formData['regtime'][$i];
                                $scheme['start_reg_date'] = $formData['startregdate'][$i];
                                $scheme['brief_date'] = $formData['briefdate'][$i];
                                $scheme['brief_time'] = $formData['brieftime'][$i];
                                $scheme['reg_date_malay'] = $formData['regdatemalay'][$i];
                                $scheme['reg_time_malay'] = $formData['regtimemalay'][$i];
                                $scheme['start_reg_date_malay'] = $formData['startregdatemalay'][$i];
                                $scheme['brief_date_malay'] = $formData['briefdatemalay'][$i];
                                $scheme['brief_time_malay'] = $formData['brieftimemalay'][$i];
                                $scheme['rl_createdby'] = $auth->getIdentity()->iduser;
                                $scheme['rl_createddt'] = date('Y-m-d H:i:s');


                                $this->intakeSchemeDB->addData($scheme);
                            }
                        }

						$flag = true;
            		}
				}
				
				if($flag==true){
					$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
				}else{
					$this->_helper->flashMessenger->addMessage(array('error' => "Duplicate entry of registration location"));
				}
            	
            	$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'viewreglocation','id'=>$IdIntake),'default',true));
            }
        }        
    }
    
    public function editreglocationAction(){
    	
    	$this->view->title=$this->view->translate("Edit Registration Location");
    	  
    	$IdIntake = $this->_getParam('id', 0);
    	$this->view->IdIntake = $IdIntake;
    	
    	$rl_id = $this->_getParam('rl_id', 0);
    	$this->view->rl_id = $rl_id;

    	
    	$intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->intakeInfo = $intakeInfo;
    	
    	$location = $this->regLocDB->getDatabyId($rl_id);

        if($location['rl_address'] == ""){
            $address = $this->regLocDB->getBranchAddress($location['rl_branch']);

            if($address){

                $country = $this->countryDB->getDatabyId($address['add_country']);
                $location['rl_address'] = $address['add_address1'].",".$address['add_address2'].",".$address['add_zipcode'].",".$address['add_city'].",".$country['CountryName'];

            }
        }

        $intakeScheme = new GeneralSetup_Model_DbTable_IntakeScheme();
        $this->view->scheme_list = $intakeScheme->getIntakeScheme($IdIntake,$rl_id);
   	    	 
    	$this->lobjIntakeForm->rl_branch->setAttrib('disabled','disabled');
        $this->lobjIntakeForm->populate($location);
        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
        
    	if ($this->getRequest()->isPost()) {
       		$formData = $this->getRequest()->getPost();
            if ($this->lobjIntakeForm->isValid($formData)) {
            	unset($formData['Save']);
            	$formData['rl_date'] = date('Y-m-d',strtotime($formData['rl_date']));
            	$this->regLocDB->updateData($formData,$formData['rl_id']);

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            	$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'viewreglocation','id'=>$formData['idIntake']),'default',true));
            }
        }
        
    }
    
    public function deleteReglocationAction(){
    
    	$IdIntake = $this->_getParam('id', 0);
    	$rl_id = $this->_getParam('rl_id', 0);
    	
    	$this->regLocDB->deleteData($rl_id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Registration location deleted"));
    	
    	$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'viewreglocation','id'=>$IdIntake),'default',true));
	}

    public function editintakeschemeAction(){
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $IdIntake = $this->_getParam('id', 0);
        $rl_id = $this->_getParam('rl_id', 0);
        $this->view->idIntake = $IdIntake;
        $intakeSchemeDB = new GeneralSetup_Model_DbTable_IntakeScheme();
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $this->view->scheme_list = $intakeSchemeDB->getIntakeScheme($IdIntake,$rl_id);

        //var_dump($this->view->program_list);
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->title=$this->view->translate("Edit Scheme Intake");
        $this->view->intakeInfo = $intakeInfo;

        $branch = $this->regLocDB->getBranchName($rl_id);
        $this->view->branch = $branch[0]['BranchName'];
    }

    public function addintakeschemeAction(){
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->title=$this->view->translate("Add Programme Intake");
        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
        $IdIntake = $this->_getParam('id', 0);
        $rl_id = $this->_getParam('rl_id', 0);
        $this->view->IdIntake = $IdIntake;
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->intakeInfo = $intakeInfo;


        $branch = $this->regLocDB->getBranchName($rl_id);
        $this->view->branch = $branch[0]['BranchName'];

        $auth = Zend_Auth::getInstance();
        $this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'editintakescheme', 'id'=>$IdIntake,'rl_id'=>$rl_id),'default',true);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($this->lobjIntakeForm->isValid($formData)) {

                $scheme = array();
                $intakeSchemeDB = new GeneralSetup_Model_DbTable_IntakeScheme();
                $result = $this->intakeSchemeDB->checkDuplicate($formData['programScheme2'], $IdIntake,  $rl_id);


                if (!$result) {
                    $scheme['IdIntake'] = $IdIntake;
                    $scheme['rl_id'] = $rl_id;
                    $scheme['idScheme'] = $formData['programScheme2'];
                    $scheme['reg_date'] = $formData['reg_date'];
                    $scheme['reg_time'] = $formData['reg_time'];
                    $scheme['start_reg_date'] = $formData['start_reg_date'];
                    $scheme['brief_date'] = $formData['brief_date'];
                    $scheme['brief_time'] = $formData['brief_time'];
                    $scheme['reg_date_malay'] = $formData['reg_date_malay'];
                    $scheme['reg_time_malay'] = $formData['reg_time_malay'];
                    $scheme['start_reg_date_malay'] = $formData['start_reg_date_malay'];
                    $scheme['brief_date_malay'] = $formData['brief_date_malay'];
                    $scheme['brief_time_malay'] = $formData['brief_time_malay'];
                    $scheme['rl_createdby'] = $auth->getIdentity()->iduser;
                    $scheme['rl_createddt'] = date('Y-m-d H:i:s');


                    $this->intakeSchemeDB->addData($scheme);

                    $this->_helper->flashMessenger->addMessage(array('success' => "Scheme added"));

                    $this->_redirect($this->view->backURL);
                }else{
                    $this->_helper->flashMessenger->addMessage(array('error' => "Cannot add duplicate data"));

                    $this->_redirect($this->view->backURL);
                }



            }
        }
    }

    public function editintakeschemeeditAction(){
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->title=$this->view->translate("Edit Scheme Intake");
        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
        $IdIntake = $this->_getParam('id', 0);
        $is_id = $this->_getParam('intakescheme', 0);
        $rl_id = $this->_getParam('rl_id', 0);
        $this->view->IdIntake = $IdIntake;
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeInfo = $intakeModel->fetchRow('IdIntake = '.$IdIntake);
        $this->view->intakeInfo = $intakeInfo;

        $intakeSchemeDB = new GeneralSetup_Model_DbTable_IntakeScheme();
        $intakeSchemeInfo  = $intakeSchemeDB->getIntakeSchemeInfo($is_id);

        $this->view->intakeSchemeInfo = $intakeSchemeInfo;

        $auth = Zend_Auth::getInstance();
        $this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'editintakescheme', 'id'=>$IdIntake,'rl_id'=>$rl_id),'default',true);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($this->lobjIntakeForm->isValid($formData)) {

                $scheme['reg_date'] = $formData['reg_date'];
                $scheme['reg_time'] = $formData['reg_time'];
                $scheme['start_reg_date'] = $formData['start_reg_date'];
                $scheme['brief_date'] = $formData['brief_date'];
                $scheme['brief_time'] = $formData['brief_time'];
                $scheme['reg_date_malay'] = $formData['reg_date_malay'];
                $scheme['reg_time_malay'] = $formData['reg_time_malay'];
                $scheme['start_reg_date_malay'] = $formData['start_reg_date_malay'];
                $scheme['brief_date_malay'] = $formData['brief_date_malay'];
                $scheme['brief_time_malay'] = $formData['brief_time_malay'];
                $scheme['rl_createdby'] = $auth->getIdentity()->iduser;
                $scheme['rl_createddt'] = date('Y-m-d H:i:s');

                $intakeSchemeDB->update($scheme, 'is_id = '.$is_id);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'intake', 'action'=>'editintakescheme', 'id'=>$IdIntake,'rl_id'=>$rl_id),'default',true));


            }else{
                $this->lobjIntakeForm->populate($intakeSchemeInfo);
            }
        }else{

            /*$intakeProgInfo['ApplicationStartDate']=$intakeProgInfo['ApplicationStartDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['ApplicationStartDate'])):'';
            $intakeProgInfo['ApplicationEndDate']=$intakeProgInfo['ApplicationEndDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['ApplicationEndDate'])):'';
            $intakeProgInfo['AdmissionStartDate']=$intakeProgInfo['AdmissionStartDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['AdmissionStartDate'])):'';
            $intakeProgInfo['AdmissionEndDate']=$intakeProgInfo['AdmissionEndDate'] != '' ? date('d-m-Y',strtotime($intakeProgInfo['AdmissionEndDate'])):'';*/

            $this->lobjIntakeForm->populate($intakeSchemeInfo);
        }
    }

    function copyreglocationAction() {

        $auth = Zend_Auth::getInstance();

        $this->view->lobjIntakeForm = $this->lobjIntakeForm;
        $IdIntakeNew = $this->_getParam('id', 0);

        $this->lobjIntakeForm->CopyIntakeId->setAttrib('disabled','disabled');
        $this->view->lobjIntakeForm->CopyIntakeId->setValue( $IdIntakeNew );
        $this->view->newintakeid = $IdIntakeNew;



        if($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();


            unset($formData['Copy']);

            $IdIntake = $formData['FromIntake'];
            $IdIntakeNew = $formData['newintakeid'];

            $checkNewIdIntake = $this->regLocDB->checkDuplicate($IdIntakeNew, '');

            if ($checkNewIdIntake) {
                $this->view->errorMsg = '2';
            } else {

                $reglocation = $this->regLocDB->getLocationList($IdIntake);

                //copy all location from intake to new intake
                foreach ($reglocation as $rl) {
                    $rl_id = $rl['rl_id'];

                    unset($rl['BranchName']);
                    unset($rl['rl_id']);

                    $rl['IdIntake'] = $IdIntakeNew;
                    $rl['rl_createddt'] = date('Y-m-d H:i:s');
                    $rl['rl_createdby'] = $auth->getIdentity()->iduser;
                    $new_rlid = $this->regLocDB->addData($rl);
                    //$new_rlid = 1927;

                    //copy from intake scheme every location to new intake scheme
                    $intakescheme = $this->intakeSchemeDB->getIntakeScheme($IdIntake, $rl_id);

                    foreach ($intakescheme as $scheme) {
                        unset($scheme['EnglishDescription']);
                        unset($scheme['IdScheme']);
                        unset($scheme['SchemeCode']);
                        unset($scheme['is_id']);

                        $scheme['IdIntake'] = $IdIntakeNew;
                        $scheme['rl_id'] = $new_rlid;
                        $scheme['rl_createddt'] = date('Y-m-d H:i:s');
                        $scheme['rl_createdby'] = $auth->getIdentity()->iduser;

                        $this->intakeSchemeDB->addData($scheme);

                    }

                }

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id' => $auth->getIdentity()->iduser,
                    'level' => $priority,
                    'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'time' => date('Y-m-d H:i:s'),
                    'message' => 'New Intake Copied=' . $IdIntake . 'to ' . $IdIntakeNew,
                    'Description' => Zend_Log::DEBUG,
                    'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log



            }
            $this->_redirect($this->baseUrl . '/generalsetup/intake/viewreglocation/id/' . $IdIntakeNew);
        }
    }
    
	 
    
}
