<?php
class GeneralSetup_LogdocumentController extends Base_Base {
	
	private $locale;
	private $registry;
	private $Logdoc;
	private $_gobjlog;

	
	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');		
	}
	
	
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();		
		$this->lobjLogform = new GeneralSetup_Form_Logdocument();
		$this->Logdoc = new GeneralSetup_Model_DbTable_Logdocument();
	}

    public function indexAction(){

        $this->view->title = "Document Setup";
        $form = new GeneralSetup_Form_LogdocumentSearch();
        $logdocDb = new GeneralSetup_Model_DbTable_Logdocument();
        $query = $logdocDb->getDocumentList(null,'sql');
        $pageCount = 50;

        //post
        if ($this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $logdocDb->getDocumentList($formData,'sql');

        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();


        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;

    }

    public function editAction(){

        $id = $this->_getParam('id',null);

        if($id==null){
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument', 'action'=>'index'),'default',true));
        }

        $this->view->title = "Edit Document Setup";

        $form = new GeneralSetup_Form_Logdocument();

        $logdocDb = new GeneralSetup_Model_DbTable_Logdocument();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'doc_desc' => strtoupper($formData['doc_desc']),
                    'doc_desc_malay' => strtoupper($formData['doc_desc_malay']),
                );



                $logdocDb->update($data,array('doc_id=?'=>$id));

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update Document Setup');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }else{
            $data = $logdocDb->fetchRow(array('doc_id=?'=>$id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }

	public function addAction(){
        $this->view->title = "Add Document Setup";
        $auth = Zend_Auth::getInstance();

        $form = new GeneralSetup_Form_Logdocument();


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $logdocDb = new GeneralSetup_Model_DbTable_Logdocument();

                $data = array(
                    'doc_desc' => strtoupper($formData['doc_desc']),
                    'doc_desc_malay' => strtoupper($formData['doc_desc_malay']),
                    'tld_createdby' => $auth->getIdentity()->iduser,
                    'tld_createddt' => date ('Y-m-d H:i:s'),

                );

                $logdocDb->addData($data);

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add Document Setup');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
	}

	
}


?>