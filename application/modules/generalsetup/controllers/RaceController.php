<?php
class GeneralSetup_RaceController extends Base_Base {

    public function indexAction()
    {
        $this->view->title = "Race Setup";

        $form = new GeneralSetup_Form_Race();

        $raceDB= new GeneralSetup_Model_DbTable_Race();
        $query = $raceDB->getRaceData();

        $pageCount = 25;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $raceDB->getRaceData($formData);

            $this->gobjsessionsis->raceQuery = $query;
            $form->populate($formData);
            $this->_redirect($this->baseUrl . '/generalsetup/race/index/search/1');
        }

        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->raceQuery);
        }else{
            $query = $this->gobjsessionsis->raceQuery;
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;
    }

    public function addAction()
    {
        $this->view->title = "Add Race";

        $form = new GeneralSetup_Form_RaceSubmit();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $raceDb = new GeneralSetup_Model_DbTable_Race();

                $data = array(
                    'race_id'           => $formData['race_id'],
                    'race_desc'         => $formData['race_desc'],
                    'race_desc_english' => $formData['race_desc_english'],
                    'race_type'         => $formData['race_type'],
                    'active'            => $formData['active'],
                );

                $raceDb->insert($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'race', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->_getParam('id', null);

        if ($id == null) {
            $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'race', 'action' => 'index'), 'default', true));
        }

        $this->view->title = "Edit Race";

        $form = new GeneralSetup_Form_RaceSubmit();

        $raceDb = new GeneralSetup_Model_DbTable_Race();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'race_id'           => $formData['race_id'],
                    'race_desc'         => $formData['race_desc'],
                    'race_desc_english' => $formData['race_desc_english'],
                    'race_type'         => $formData['race_type'],
                    'active'            => $formData['active'],
                );

                $raceDb->update($data, array('id=?' => $id));

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully updated"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'race', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        } else {
            $data = $raceDb->fetchRow(array('id=?' => $id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }
}