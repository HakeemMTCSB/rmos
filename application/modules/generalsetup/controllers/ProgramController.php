<?php
class GeneralSetup_ProgramController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $lobjSubjectprerequisites;
	private $lobjprogramquota;
    private $lobjaward;

	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate');
   	    Zend_Form::setDefaultTranslator($this->view->translate);

	}

	public function fnsetObj(){
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjprogramForm = new GeneralSetup_Form_Program ();
	  	$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
        $this->lobjaward = new GeneralSetup_Model_DbTable_Awardlevel();
	  	$this->lobjSubjectprerequisites = new GeneralSetup_Model_DbTable_Subjectprerequisites();
	  	$this->lobjprogramquota = new GeneralSetup_Model_DbTable_Programquota();
	  	$this->lobjStaffForm = new GeneralSetup_Form_Staffmaster();
	  	$this->lobjuniversityForm = new GeneralSetup_Form_University (); //intialize user lobjuniversityForm
	  	$this->lobjstaffmodel = new GeneralSetup_Model_DbTable_Staffmaster();
	  	$this->lobjchiefofprogram = new GeneralSetup_Model_DbTable_Chiefofprogram();
        $this->lobjProgMajorIntake = new GeneralSetup_Model_DbTable_ProgramMajorIntake();
        $this->lobjProgMajorStructure = new GeneralSetup_Model_DbTable_ProgramMajorStructure();
        $this->lobjIntake = new GeneralSetup_Model_DbTable_Intake();
        $this->lobjRegistry = new GeneralSetup_Model_DbTable_Registry_RegistryType();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = Zend_Registry::get('Zend_Locale');

	}
//	public function testAction(){
//		$this->view->title="Add New Profram";
//		$this->view->lobjform = $this->lobjform;
//		echo ("hello");
//	}

	public function indexAction() {
                $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$sessionID = Zend_Session::getId();
                $this->lobjprogram->fnDeleteTempAccredictionDetailsBysession($sessionID);
                $this->view->title=$this->view->translate("Programme Setup");
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjprogram->fngetProgramDetails (); //get user details
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->programpaginatorresult);

		//$lintpagecount = $this->gintPageCount;
        $lintpagecount = 50;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->programpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

        $larrdefmsresultset = $this->lobjaward->getAwardList('Award');
        foreach($larrdefmsresultset as $larrdefmsresult) {
            $this->lobjform->field5->addMultiOption($larrdefmsresult['Id'],$larrdefmsresult['GradeDesc']);
        }

		$sessionID = Zend_Session::getId();
        $this->lobjprogram->fnDeleteTempprogramquotaDetailsBysession($sessionID);
		
		if ($this->_request->isPost() && $this->_request->getPost('saveorder') ) 
		{
			$larrFormData = $this->_request->getPost();
			
			if ( isset($larrFormData['order']) )
			{
				$db = getDB();
				foreach( $larrFormData['order'] as $def_id => $order )
				{
					if ( (int) $order > 0 )
					{
						$db->update('tbl_program', array('seq_no' => $order), 'IdProgram='.(int) $def_id );
					}
				}

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'program', 'action'=>'index'),'default',true));
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjprogram->fnSearchProgram ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				@$this->gobjsessionsis->programpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/generalsetup/program');
		}

	}

	public function newprogramAction() {
		
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        
         //title
        $this->view->title=$this->view->translate("Add New Programme");
        
		$this->view->lobjprogramForm = $this->lobjprogramForm;
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		
		//$lobjcourse = $this->lobjcoursemaster->fnGetCourseList();
		//$this->lobjprogramForm->IdCourseMaster->addMultiOptions($lobjcourse);

		$this->view->lobjuniversityForm = $this->lobjuniversityForm; //send the lobjuniversityForm object to the view
		$this->view->lobjstaffmasterForm = $this->lobjStaffForm;

		if($this->locale == 'ar_YE')
		{
			$this->lobjuniversityForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			
		}

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		
		//Accredition
		$this->lobjuniversityForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
		//$this->lobjprogramForm->AccreditionType->addMultiOptions($this->lobjprogram->fnGetAccreditationList());
		
		//salutation
		/*$lobjSalutationList = $this->lobjprogram->fnGetSalutationList();
		$this->lobjprogramForm->programSalutation->addMultiOptions($lobjSalutationList);*/

		//college
		$larrCollegeList = $this->lobjSubjectprerequisites->fnGetCollegeList();
		$this->lobjprogramForm->IdCollege->addMultiOptions($larrCollegeList);

		//award
        $larrdefmsresultset = $this->lobjaward->getAwardList('Award');
        foreach($larrdefmsresultset as $larrdefmsresult) {
            $this->lobjprogramForm->Award->addMultiOption($larrdefmsresult['Id'],$larrdefmsresult['GradeDesc']);
        }
		
		//learning mode
		$learningmodeid=0;
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Learning Mode');
		foreach($larrdefmsresultset as $larrdefmsresult) {$learningmodeid++;
			$this->lobjprogramForm->LearningMode->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
			$iddef[]=$larrdefmsresult['idDefinition'];
		}

		//concurrent program
		$this->view->concurent_program_list = $this->lobjprogram->getProgramConcurrent();
		
		$this->view->iddef=$iddef;		
		$this->view->hiddencount=$learningmodeid;
		$this->view->lobjprogramForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
                $getUserIdentity = $auth->getIdentity();
		$this->view->lobjprogramForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		
		if ($this->getRequest()->isPost()) {
			
				$formData = $this->getRequest()->getPost();
				//var_dump($formData); exit;
				//program info
	   			$result = $this->lobjprogram->fnaddProgram($formData);	   			
	   			
	   			//program chief 
	   			//$this->lobjchiefofprogram->fninsertChiefofProgramList($formData,$result);
                                
                                //setup (this setup will set to default setup)
                                $confmodel = new Application_Model_DbTable_ApplicationInitialConfiguration();
                                $sectionList = $confmodel->fnGetItemBySection(1);
                                
                                /*if ($sectionList){
                                    foreach ($sectionList as $sectionLoop){
                                        if ($sectionLoop['id']!=1){
                                            $setupdata = array(
                                                'pds_program_id'=>$result,
                                                'pds_item_id'=>$sectionLoop['id'],
                                                //'pds_instruction_position'=>$formData['pds_instruction_position'],
                                                //'pds_instruction'=>$formData['pds_instruction'],
                                                //'pds_tooltip'=>$formData['pds_tooltip'],
                                                'pds_view'=>1,
                                                'pds_compulsory'=>1,
                                                'pds_enable'=>1,
                                                //'pds_hide'=>$formData['pds_hide'],
                                                'pds_updUser'=>$getUserIdentity->id,
                                                'pds_updDate'=>date('Y-m-d H:i:s')
                                            ); 
                                            $confmodel->insertProgramSectionDetail($setupdata);
                                        }
                                    }
                                }*/
				
	   			//program accredition	   			
				/*if(count($formData['AccreditionTypegrid'])>0){					
					for($c=0; $c<count($formData['AccreditionTypegrid']); $c++){						
						$accredition['IdProgram']= $this->lobjprogram->getMaxProgId();
						$accredition['AccreditionType']= $formData['AccreditionTypegrid'][$c];
						$accredition['AccredictionReferences']= $formData['AccreditionRferencegrid'][$c];
						$accredition['AccredictionDate']= $formData['AccreditionDategrid'][$c];
						$accredition['AccredictionNumber'] = $formData['AccredictionNumbergrid'][$c];
				        $accredition['ValidityFrom'] = $formData['ValidityFromgrid'][$c];
				        $accredition['ValidityTo'] = $formData['ValidityTogrid'][$c];
				        $accredition['ApprovalDate'] = $formData['ApprovalDategrid'][$c];
						$accredition['UpdDate']= $ldtsystemDate;
						$accredition['UpdUser']= $auth->getIdentity()->iduser;						
						$this->lobjprogram->addAccredition($accredition);						
					}					
				}*/
				
	   				   							
				//program majoring
				//$this->lobjprogram->fninsertMajoring($formData,$result);
				
				//program scheme
				/*if(count($formData['ModeOfProgram'])>0){					
					$programSchemeDB = new GeneralSetup_Model_DbTable_Programscheme();
					for($x=0; $x<count($formData['ModeOfProgram']); $x++){						
						$progscheme['IdProgram']= $result;
						$progscheme['mode_of_program']= $formData['ModeOfProgram'][$x];
						$progscheme['mode_of_study']= $formData['ModeOfStudy'][$x];//TypeOfProgram
						$progscheme['program_type']= $formData['TypeOfProgram'][$x];
                        $progscheme['Duration']= $formData['Duration'][$x];
                        $progscheme['DurationType']= $formData['DurationType'][$x];
                        $progscheme['OptimalDuration']= $formData['OptDuration'][$x];
                        $progscheme['OptDurationType']= $formData['OptDurationType'][$x];
						$progscheme['createddt']= $ldtsystemDate;
						$progscheme['createdby']= $auth->getIdentity()->iduser;
												
						//check duplicate
						$exist = $programSchemeDB->checkDuplicate($result,$formData['ModeOfProgram'][$x],$formData['ModeOfStudy'][$x],$formData['TypeOfProgram'][$x]);
						
						if($exist){
							//data already exist skip add.
							echo 'already';
						}else{
							$programSchemeDB->addData($progscheme);	
						}			
					}					
				}*/
				
				//concurrenr program
				/*if(count($formData['cprogram'])>0){	
					$CProgramDb = new GeneralSetup_Model_DbTable_ConcurrentProgram();
					for($z=0; $z<count($formData['cprogram']); $z++){	
						$concurrentprog['IdProgram']= $result;
						$concurrentprog['cp_program_id']= $formData['cprogram'][$z];
						$concurrentprog['createddt']= $ldtsystemDate;
						$concurrentprog['createdby']= $auth->getIdentity()->iduser;						
						$CProgramDb->addData($concurrentprog);
					}
				}*/
				
				$this->gobjsessionsis->flash = array('message' => 'Information has been successfully saved', 'type' => 'success');
				$this->_redirect( $this->baseUrl . '/generalsetup/program/editprogram/id/'.$result);
        }
    }

	public function editprogramAction(){

        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->title=$this->view->translate("Edit Programme");  //title
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view

		$this->view->lobjuniversityForm = $this->lobjuniversityForm; //send the lobjuniversityForm object to the view
		$this->view->lobjstaffmasterForm = $this->lobjStaffForm;

	 	/*if($this->_getParam('update')!= 'true') {
			$sessionID = Zend_Session::getId();
        	$this->lobjprogram->fnDeleteTempAccredictionDetailsBysession($sessionID);
 		}*/

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );

		//Staff List
		$this->lobjuniversityForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
		$AccreditationTypeList = $this->lobjRegistry->getDataByCodeType('accredition-type');
		$this->view->accTypeList = $AccreditationTypeList;
		//$this->lobjprogramForm->AccreditionType->addMultiOptions($AccreditationTypeList);

        $ModeProgramList = $this->lobjRegistry->getDataByCodeType('mode-of-program');
        $this->view->modeProgramList = $ModeProgramList;

        $PgStructureList = $this->lobjRegistry->getDataByCodeType('pg-structure');
        $this->view->PgStructureList = $PgStructureList;

		$lobjSalutationList = $this->lobjprogram->fnGetSalutationList();
		$this->lobjprogramForm->programSalutation->addMultiOptions($lobjSalutationList);

		$larrCollegeList = $this->lobjSubjectprerequisites->fnGetCollegeList();
		$this->lobjprogramForm->IdCollege->addMultiOptions($larrCollegeList);
		//-------------------
		$IdProgram = $this->_getParam('id', 0);
		$this->view->LintIdProgram=$IdProgram;
		$larrresult = $this->lobjchiefofprogram->fngetChiefofProgramList($IdProgram);
		$larrhistory = $this->lobjprogram->fngethistory($IdProgram);
		$this->view->history = $larrhistory;

		$this->view->lobjuniversityForm->FromDate->setValue ($larrresult['FromDate'] );
		$this->view->lobjuniversityForm->IdStaff->setValue ($larrresult['IdStaff'] );
		$this->view->IdChiefOfProgram = $larrresult['IdChiefOfProgramList'];

		//$lobjcourse = $this->lobjcoursemaster->fnGetCourseList();
		//$this->lobjprogramForm->IdCourseMaster->addMultiOptions($lobjcourse);
        $larrdefmsresultset = $this->lobjaward->getAwardList('Award');
        foreach($larrdefmsresultset as $larrdefmsresult) {
            $this->lobjprogramForm->Award->addMultiOption($larrdefmsresult['Id'],$larrdefmsresult['GradeDesc']);
        }

		$this->view->accredentialdtls=$accredintialdtls = $this->lobjprogram->fnviewAccredentialDetails($IdProgram);
		//var_dump($IdProgram);
		//concurrent program
		$this->view->concurent_program_list = $this->lobjprogram->getProgramConcurrent($IdProgram);

		$CProgramDb = new GeneralSetup_Model_DbTable_ConcurrentProgram();
		$this->view->sel_concurent_program_list = $CProgramDb->getDatabyProgramId($IdProgram);

		if($IdProgram) {
	    	$result = $this->lobjprogram->fetchAll('IdProgram = '.$IdProgram);
	    	$result = $result->toArray();
			foreach($result as $programresult) {
			}

			$deptfromcoll = $this->lobjprogram->fnGetDepartmentFromColl($programresult['IdCollege']);
			$this->lobjprogramForm->IdDepartment->addMultiOptions($deptfromcoll);

			$this->lobjprogramForm->ActiveDB->setValue($programresult['Active']);
			$this->lobjprogramForm->populate($programresult);
            //var_dump($programresult);
		}


		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjprogramForm->UpdUser->setValue( $auth->getIdentity()->iduser);

		$this->view->programmajoring=$this->lobjprogram->fnviewProgramMajoring($IdProgram);
        $this->view->programminor=$this->lobjprogram->fnviewProgramMinor($IdProgram);


		//get program scheme
		$progschemeDb =  new GeneralSetup_Model_DbTable_Programscheme();
		$this->view->program_scheme = $progschemeDb->getProgSchemeByProgram($IdProgram);

		//get dean history
		$this->view->dean_list = $this->lobjchiefofprogram->getHistoryDeanList($IdProgram);

    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
                    //var_dump($formData); exit;
	    	if ($this->lobjprogramForm->isValid($formData)) {

	    		//get current idStaff
	    		$current_dean = $this->lobjchiefofprogram->getCurrentDean($formData ['IdProgram']);
//print_r($current_dean);exit;
	    		if($current_dean['IdStaff']==$formData['IdStaff']){

	    			$chief['FromDate']=$formData['FromDate'];
	    			$chief['IdCollege'] = $formData['IdCollege'];
    				$chief['IdDepartment'] = $formData['IdDepartment'];
	    			$chief['UpdDate'] = date('Y-m-d H:i:s');
    				$chief['UpdUser'] = $auth->getIdentity()->iduser;

	    			$this->lobjchiefofprogram->updateData($chief,$formData['IdChiefOfProgram']);//update chief of program
	    		}else{
					//update previous pd
					$chief['Active']=0;
					$chief['UpdDate'] = date('Y-m-d H:i:s');
					$chief['UpdUser'] = $auth->getIdentity()->iduser;
					$this->lobjchiefofprogram->updateData($chief,$current_dean['IdChiefOfProgramList']);
	    			//add new dean
	    			$this->lobjchiefofprogram->fninsertChiefofProgramList($formData,$formData['IdProgram']);
	    		}

	    		$data['ProgramName']=$formData['ProgramName'];
			    $data['programSalutation']=$formData['programSalutation'];
			    $data['UpdDate']=$formData['UpdDate'];
			    $data['UpdUser']=$formData['UpdUser'];
			    $data['ArabicName']=$formData['ArabicName'];
			    $data['ProgramCode']=$formData['ProgramCode'];
			    //$data['ShortName']=$formData['ShortName'];
			    $data['Award']=$formData['Award'];
			    $data['TotalCreditHours']=$formData['TotalCreditHours'];
			    //$data['MinimumAge']=$formData['MinimumAge'];
                            $data['apply_online']=$formData['apply_online'];
			    $data['Active']=$formData['Active'];
			    $data['IdCollege']=$formData['IdCollege'];
	    		$data['IdScheme'] = $formData['IdScheme'];
	    		//$data['InternalExternal'] = $formData['InternalExternal'];
//                $data['ProgramMoheField'] = $formData['ProgramMoheField'];
//                $data['ProgramMoheSubfield'] = $formData['ProgramMoheSubfield'];
//                $data['ProgramMoheSubfieldId'] = $formData['ProgramMoheSubfieldId'];
//                $data['ProgramMoheFieldId'] = $formData['ProgramMoheFieldId'];
//                $data['ProgramApproved'] = $formData['ProgramApproved'];
//                $data['MalayName'] = $formData['MalayName'];

				$this->lobjprogram->fnupdateProgram($data,$formData ['IdProgram']);//update program

				//$this->gobjsessionsis->flash = array('message' => 'Information has been successfully saved', 'type' => 'success');

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

				$this->_redirect( $this->baseUrl . '/generalsetup/program/editprogram/id/'.$formData ['IdProgram']);

			}else{
				echo "invalid form";
				exit;
			}
    	}
    }

    public function editmajorstructureAction(){

        $this->view->title=$this->view->translate("Major By Structure");
        $this->view->lobjprogramForm = $this->lobjprogramForm;

        $idMajor = $this->_getParam('idMajor', 0);
        $this->view->idMajor =  $idMajor;
        $this->view->LintIdMajor=$idMajor;

        $MajorModel = new GeneralSetup_Model_DbTable_ProgramMajoring();
        $MajorInfo = $MajorModel->getProgramMajor($idMajor);

        $this->view->majorInfo = $MajorInfo;
        $this->view->majorStruclist = $this->lobjProgMajorStructure->getMajorStructureList($idMajor);

        $this->view->structurelist = $this->lobjRegistry->getDataByCodeType('pg-structure');


    }

    public function editmajorintakeAction(){

        $this->view->title=$this->view->translate("Major By Intake");
        $this->view->lobjprogramForm = $this->lobjprogramForm;

        $idMajor = $this->_getParam('idMajor', 0);
        $this->view->idMajor =  $idMajor;
        $this->view->LintIdMajor=$idMajor;

        $MajorModel = new GeneralSetup_Model_DbTable_ProgramMajoring();
        $MajorInfo = $MajorModel->getProgramMajor($idMajor);

        $this->view->majorInfo = $MajorInfo;
        $this->view->majorIntakelist = $this->lobjProgMajorIntake->getMajorIntakeList($idMajor);

        $this->view->intakelist = $this->lobjIntake->fngetallIntake();


    }

	public function deleteprogramquotadetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$IdTemp = $this->_getParam('IdTemp');

		$larrUOM = $this->lobjprogram->fnUpdateTempprogramquotadetails($IdTemp);
		echo "1";
	}

	public function getawardcodeAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$idaward = $this->_getParam('idaward');
	    $larrresultaward = $this->lobjprogram->fnGetAwardCode($idaward);
	    echo $larrresultaward['DefinitionCode'];
	}

	public function deleteaccridictionAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Po details Id
		$lintidtemp = $this->_getParam('idtemp');

		$larrDelete = $this->lobjprogram->fnUpdateTempAccridictionDetails($lintidtemp);
		echo "1";
	}

	public function getcolgdeptidAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$lobjCommonModel = new App_Model_Common();
		$lintColgDeptid = $this->_getParam('ColgDeptid');
		if($lintColgDeptid == 1){
			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjSubjectprerequisites->fnGetDepartmentList());
		} else {

			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($this->lobjSubjectprerequisites->fnGetCollegeList());
		}
		echo Zend_Json_Encoder::encode($larrDetails);

	}
	
	
	public function ajaxGetMajorintakeAction(){

    	$this->_helper->layout->disableLayout();
        $type = $this->_getParam('type', 0);

     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        if($type=='majorIntake'){

            $IDMajorIntake = $this->_getParam('IDMajorIntake', 0);
            $majorIntakeDB = new GeneralSetup_Model_DbTable_ProgramMajorIntake();
            $row=$majorIntakeDB->getInfo($IDMajorIntake);
        }

        if($type=='majorStruc'){

            $IDMajorStruc = $this->_getParam('IDMajorStruc', 0);
            $majorStrucDB = new GeneralSetup_Model_DbTable_ProgramMajorStructure();
            $row=$majorStrucDB->getInfo($IDMajorStruc);
        }

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();


        $json = Zend_Json::encode($row);

        echo $json;
        exit();


    }

    public function ajaxGetMajoringAction(){

        $idProgram = $this->_getParam('idProgram', 0);
        $type = $this->_getParam('type', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        if($type=='scheme'){
            $IdProgramScheme = $this->_getParam('IdProgramScheme', 0);
            $programDB = new GeneralSetup_Model_DbTable_Program();
            $row=$programDB->getProgramSchemeData($IdProgramScheme);

        }else
            if($type=='accreditation'){
                $idAcc = $this->_getParam('idAcc', 0);
                $programDB = new GeneralSetup_Model_DbTable_Program();
                $row=$programDB->getAccreditationData($idAcc);

            }else
                if($type=='majoring'){

                    $idProgramMajoring = $this->_getParam('idProgramMajoring', 0);
                    $programMajoringDB = new GeneralSetup_Model_DbTable_ProgramMajoring();
                    $row = $programMajoringDB->getInfo($idProgramMajoring);
                }

        if($type=='minor'){

            $idProgramMinor = $this->_getParam('idProgramMinor', 0);
            $programMinorDB = new GeneralSetup_Model_DbTable_ProgramMinor();
            $row = $programMinorDB->getInfo($idProgramMinor);
        }

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();


        $json = Zend_Json::encode($row);

        echo $json;
        exit();
    }
    
    public function saveEditMajoringAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	
    	if ($this->_request->isPost() ) {
    		
			$formData = $this->_request->getPost ();
			$type = $this->_getParam('type', 0);

    	    if($type=='scheme'){
				
				$data['Active']=$formData['EditSchemestatus'];
                $data['mode_of_program']=$formData['Editprogrammemode'];
                $data['program_type']=$formData['Editprogramtype'];

				$data['modifyby']=$auth->getIdentity()->iduser;	
				$data['modifydt']=date('Y-m-d H:i:s');
				
				$programDB = new GeneralSetup_Model_DbTable_Program();
				$programDB->updateProgramScheme($data,$formData["IdProgramScheme"]);

				$tab = 2;
				
			}else
			if($type=='accreditation'){
				
				$acc['AccreditionType']=$formData['AccType'];
				$acc['AccredictionReferences']=$formData['AccRef'];
				$acc['AccredictionDate']=date('Y-m-d',strtotime($formData['AccDate']));
				$acc['AccredictionNumber']=$formData['AccNumber'];
				$acc['ValidityFrom']=date('Y-m-d',strtotime($formData['AccValidFrom']));
				$acc['ValidityTo']=date('Y-m-d',strtotime($formData['AccValidTo']));
				$acc['ApprovalDate']=date('Y-m-d',strtotime($formData['AccApproveDate']));
				
				$programDB = new GeneralSetup_Model_DbTable_Program();
				$programDB->updateAccreditation($acc,$formData["IdProgramAccredition"]);

				$tab = 5;
				
			}elseif($type=='concurrent'){
			
				//concurrenr program
				if(count($formData['cprogram'])>0){	
					$CProgramDb = new GeneralSetup_Model_DbTable_ConcurrentProgram();
					for($z=0; $z<count($formData['cprogram']); $z++){	
						$concurrentprog['IdProgram']= $formData['idProgram'];
						$concurrentprog['cp_program_id']= $formData['cprogram'][$z];
						$concurrentprog['createddt']= date('Y-m-d H:i:s');
						$concurrentprog['createdby']= $auth->getIdentity()->iduser;						
						$CProgramDb->addData($concurrentprog);
					}
				}

				$tab = 4;
				
			}elseif($type=='majoring'){			
				
				$data["IdMajor"]=$formData["EditIdMajor"];
				$data["EnglishDescription"]=$formData["EditEnglishDescription"];
				$data["BahasaDescription"]=$formData["EditBahasaDescription"];
                $data["MajorCreditComplete"]=$formData["EditMajorCreditComplete"];
                $data["MajorAllIntake"]=$formData["EditMajorAllIntake"];
				$data["modifyby"]=$auth->getIdentity()->iduser;
				$data["modifydt"]=date ( 'Y-m-d H:i:s');
				
				$programMajoringDB = new GeneralSetup_Model_DbTable_ProgramMajoring();
				$programMajoringDB->updateData($data,$formData["idProgramMajoring"]);
				
				$tab = 3;

			}elseif($type=='minor'){

                $data["IdMinor"]=$formData["EditIdMinor"];
                $data["MinorEngDescription"]=$formData["EditMinorEngDescription"];
                $data["MinorMalayDescription"]=$formData["EditMinorMalayDescription"];
                $data["MinorCreditComplete"]=$formData["EditMinorCreditComplete"];
                $data["modifyby"]=$auth->getIdentity()->iduser;
                $data["modifydt"]=date ( 'Y-m-d H:i:s');

                $programMinorDB = new GeneralSetup_Model_DbTable_ProgramMinor();
                $programMinorDB->updateData($data,$formData["idProgramMinor"]);

                $tab = 6;
            }
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'program', 'action'=>'editprogram','id'=>$formData["idProgram"]),'default',true));

			$this->_redirect($this->baseUrl . '/generalsetup/program/editprogram/id/'.$formData['idProgram'].'#tab-'.$tab);
    	
    	}
    	
    }

    public function saveEditMajorintakeAction(){

        $auth = Zend_Auth::getInstance();

        if ($this->_request->isPost() ) {

            $formData = $this->_request->getPost ();
            $type = $this->_getParam('type', 0);

            if($type=='majorintake'){

                $data['idStartIntake']=$formData['EditidStartIntake'];
                $data['idEndIntake']=$formData['EditidEndIntake'];
                $data['creditLimit']=$formData['EditcreditLimit'];
                $data['Status']=$formData['EditStatus'];

                $data['modifyby']=$auth->getIdentity()->iduser;
                $data['modifydt']=date('Y-m-d H:i:s');

                $MajorIntakeDB = new GeneralSetup_Model_DbTable_ProgramMajorIntake();
                $MajorIntakeDB->updateData($data,$formData["IDMajorIntake"]);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/program/editmajorintake/idMajor/'.$formData['IdMajor']);
            }

            if($type=='majorstruc'){

                $data['idStruc']=$formData['EditidStruc'];

                $data['modifyby']=$auth->getIdentity()->iduser;
                $data['modifydt']=date('Y-m-d H:i:s');

                $MajorIntakeDB = new GeneralSetup_Model_DbTable_ProgramMajorStructure();
                $MajorIntakeDB->updateData($data,$formData["IDMajorStruc"]);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/program/editmajorstructure/idMajor/'.$formData['IdMajor']);

            }

        }

    }
    
    public function editProgramGpaAction()
    {
        
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $program_id = $this->_getParam('id',0);
        $this->view->program_id = $program_id;
        
        $intake_id = $this->_getParam('intake',0);
        $this->view->intake_id = $intake_id;
        
        if ($program_id != 0)
        {
            $program = $this->lobjprogram->fetchRow('IdProgram = '.$program_id);
            $this->view->program = $program;
        
            $Intake  = new GeneralSetup_Model_DbTable_Intake();
            $intakes = $Intake->fngetallIntakelist();
            
            $this->view->intakes = $intakes;
            
            if($intake_id != 0)
            {
                $GpaDetail  = new GeneralSetup_Model_DbTable_Programlimithead();
                $gpaHead    = $GpaDetail->fnGetHeadProgramIntake($program_id,$intake_id);
                
                if ($this->getRequest()->isPost()) 
                {
                    $formData = $this->getRequest()->getPost();
                    $count = count($formData['lower']);
                    
                    if($count > 0)
                    {
                        if(!isset($gpaHead['clid']))
                        {
                            $auth = Zend_Auth::getInstance();
                            ;
                            $gpa_head = array(
                                'progid'   => $program_id,
                                'intakeid' => $intake_id,
                                'createdby' => $auth->getIdentity()->iduser
                            );
                            
                            $GpaDetail->fnAddHead($gpa_head);
                            $gpaHead    = $GpaDetail->fnGetHeadProgramIntake($program_id,$intake_id);
                        }
                        
                        $GpaDetail->fnDeleteDetail($gpaHead['clid']);
                        
                        for($i=0;$i<$count;$i++)
                        {
                            $data = array(
                                'clid' => $gpaHead['clid'],
                                'rstart' => $formData['lower'][$i],
                                'rend' => $formData['upper'][$i],
                                'chlimit' => $formData['grade'][$i]
                            );
                        
                            if(($formData['lower'][$i] != '')&&($formData['upper'][$i] != '')&&($formData['grade'][$i] != ''))
                            {
                                $GpaDetail->fnAddDetail($data);
                            }
                        }
                    }
                
                }
                
                if(isset($gpaHead['clid']))
                {
                    $gpaDetails = $GpaDetail->fnGetDetailProgramIntake($gpaHead['clid']);
            
                    $this->view->gpaDetails = $gpaDetails;
                }
                else
                {
                    $this->view->gpaDetails = array();
                }
            }
        }
        
        //$ProgramLimit = new GeneralSetup_Model_DbTable_Programlimithead();
        
    }
    
    
	public function deleteProgramAction(){
		
	  $program_id = $this->_getParam('id', 0);

	  $programDb = new GeneralSetup_Model_DbTable_Program();
	  $copDB = new GeneralSetup_Model_DbTable_Chiefofprogram();	
	  $programSchemeDb = new GeneralSetup_Model_DbTable_Programscheme();
	  $progMajorDb = new GeneralSetup_Model_DbTable_ProgramMajoring();
      $progMinorDb = new GeneralSetup_Model_DbTable_ProgramMinor();
	   
	  //delete head	   
	  $copDB->deleteAllData($program_id);
	   
	  //delete majoring
	  $progMajorDb->deleteAllData($program_id);

      //delete minor
      $progMinorDb->deleteAllData($program_id);
	  
	  //delete accredition
	   $programDb->deleteAllData($program_id);
	  
	  //delete scheme	 
	  $programSchemeDb->deleteAllData($program_id);

	  //delete program
	  $programDb->deleteData($program_id);		 	 
	  
	  $this->_redirect( $this->baseUrl . '/generalsetup/intake/index');
	}

	public function ajaxSaveProgramInfoAction(){

    	$type= $this->_getParam('type', null);
    	$IdProgram= $this->_getParam('IdProgram', null);

    	$auth = Zend_Auth::getInstance();
    	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	if($type=='program_scheme'){

	  		$ProgramMode = $this->_getParam('ProgramMode', null);
	  		$StudyMode = $this->_getParam('StudyMode', null);
	  		$ProgramType = $this->_getParam('ProgramType', null);

	  		$Duration = $this->_getParam('Duration', null);
	  		$DurationType = $this->_getParam('DurationType', null);
	  		$MinYear = $this->_getParam('MinYear', null);
	  		$MinMonth = $this->_getParam('MinMonth', null);
	  		
	  		$OptimalDuration = $this->_getParam('OptimalDuration', null);
	  		$OptDurationType = $this->_getParam('OptDurationType', null);
	  		$MaxYear = $this->_getParam('MaxYear', null);
	  		$MaxMonth = $this->_getParam('MaxMonth', null);
            $ActiveScheme = $this->_getParam('ActiveScheme', null);

	  		
	  		$data['IdProgram'] = $IdProgram;
	  		$data['mode_of_program'] = $ProgramMode;
	  		$data['mode_of_study'] = $StudyMode;
            $data['program_type'] = $ProgramType;

	  		$data['Duration'] = $Duration;
	  		$data['DurationType'] = $DurationType;
	  		$data['MinYear'] = $MinYear;
	  		$data['MinMonth'] = $MinMonth;
	  		
	  		$data['OptimalDuration'] = $OptimalDuration;
	  		$data['OptDurationType'] = $OptDurationType;
	  		$data['MaxYear'] = $MaxYear;
	  		$data['MaxMonth'] = $MaxMonth;

	  		$data['createdby'] = $auth->getIdentity()->iduser;;
	  		$data['createddt'] = date ( 'Y-m-d H:i:s');

	  		$programSchemeDB = new GeneralSetup_Model_DbTable_Programscheme();
	  		//add
	  		$programSchemeDB->fnaddProgramscheme($data);

	  		//get list
	  		$rows = $programSchemeDB->getProgSchemeByProgram($IdProgram);
	  		
	  	}

		if($type=='accreditation'){
	  			  		
			$AccredictionDate = $this->_getParam('AccredictionDate', null);
			$ValidityFrom= $this->_getParam('ValidityFrom', null);
			$ValidityTo = $this->_getParam('ValidityTo', null);
			$ApprovalDate = $this->_getParam('ApprovalDate', null);
			
	  		$accredition['IdProgram'] = $IdProgram;
	  		$accredition['AccreditionType'] = $this->_getParam('AccreditionType', 0);
	  		$accredition['AccredictionReferences'] = $this->_getParam('AccredictionReferences', null);
			$accredition['AccredictionDate']= null;
			$accredition['AccredictionNumber'] = '';
            $accredition['ProgramCode'] = '';
			$accredition['ValidityFrom'] = date('Y-m-d',strtotime($ValidityFrom));
			$accredition['ValidityTo'] = date('Y-m-d',strtotime($ValidityTo));
			$accredition['ApprovalDate'] = null;
            $accredition['Validity'] = $this->_getParam('Validity', null);
			$accredition['UpdDate']= date ( 'Y-m-d H:i:s');
			$accredition['UpdUser']= $auth->getIdentity()->iduser;
						  		
	  		
	  		$programDB = new GeneralSetup_Model_DbTable_Program();
	  		//add
	  		$programDB->addAccredition($accredition);
	  		
	  		//get list
	  		$rows = $programDB->fnviewAccredentialDetails($IdProgram);
	  	}
	  	
	  	
	  	if($type=='majoring'){
	  		
	  		$majorDB = new GeneralSetup_Model_DbTable_ProgramMajoring();
	  		
	  		$major['IdProgram'] = $IdProgram;
	  		$major['IdMajor'] = $this->_getParam('IdMajor', null);
	  		$major['EnglishDescription']= $this->_getParam('EnglishDescription', null);
	  		$major['BahasaDescription'] = $this->_getParam('BahasaDescription', null);
            $major['MajorCreditComplete'] = $this->_getParam('MajorCreditComplete', null);
            $major['MajorAllIntake'] = $this->_getParam('MajorAllIntake', null);
	  		$major['modifydt']= date ( 'Y-m-d H:i:s');
			$major['modifyby']= $auth->getIdentity()->iduser;
	  		
	  		$majorDB->addData($major);
	  		
	  		//get list
	  		$rows = $majorDB->getData($IdProgram);
	  	}

        if($type=='minor'){

            $minorDB = new GeneralSetup_Model_DbTable_ProgramMinor();

            $minor['IdProgram'] = $IdProgram;
            $minor['IdMinor'] = $this->_getParam('IdMinor', null);
            $minor['MinorEngDescription']= $this->_getParam('MinorEngDescription', null);
            $minor['MinorMalayDescription'] = $this->_getParam('MinorMalayDescription', null);
            $minor['MinorCreditComplete'] = $this->_getParam('MinorCreditComplete', null);
            $minor['modifydt']= date ( 'Y-m-d H:i:s');
            $minor['modifyby']= $auth->getIdentity()->iduser;

            $minorDB->addData($minor);

            //get list
            $rows = $minorDB->getData($IdProgram);
        }
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($rows);
		
		echo $json;
		exit();
    }

    public function ajaxSaveMajorIntakeAction(){

        $type= $this->_getParam('type', null);
        $IdMajor = $this->_getParam('IdMajor', null);

        $auth = Zend_Auth::getInstance();
        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        if($type=='majorintake'){

            $data['IdMajor'] = $IdMajor;
            $data['idStartIntake'] = $this->_getParam('idStartIntake', null);
            $data['idEndIntake'] = $this->_getParam('idEndIntake', null);
            $data['creditLimit'] = $this->_getParam('creditLimit', 0);
            $data['Status'] = $this->_getParam('Status', null);

            $data['modifyby'] = $auth->getIdentity()->iduser;;
            $data['modifydt'] = date ( 'Y-m-d H:i:s');

            $majorIntakeDB = new GeneralSetup_Model_DbTable_ProgramMajorIntake();
            //add
            $majorIntakeDB->addData($data);

            //get list
            $rows = $majorIntakeDB->getMajorIntakeList($IdMajor);

        }

        if($type=='structure'){

            $data['IdMajor'] = $IdMajor;
            $data['idStruc'] = $this->_getParam('idStruc', null);
            $data['modifyby'] = $auth->getIdentity()->iduser;;
            $data['modifydt'] = date ( 'Y-m-d H:i:s');

            $majorStrucDB = new GeneralSetup_Model_DbTable_ProgramMajorStructure();
            //add
            $majorStrucDB->addData($data);

            //get list
            $rows = $majorStrucDB->getMajorStructureList($IdMajor);

        }


        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($rows);

        echo $json;
        exit();
    }
    
   public function ajaxDeleteProgramInfoAction(){
    	
    	$type= $this->_getParam('type', null);
    	$IdProgram= $this->_getParam('IdProgram', null);
    	
    	$auth = Zend_Auth::getInstance();
    	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	if($type=='program_scheme'){
	  		
	  		$IdProgramScheme= $this->_getParam('IdProgramScheme', 0);
	  		
	  		$programSchemeDB = new GeneralSetup_Model_DbTable_Programscheme();
	  		
	  		//delete
	  		$programSchemeDB->deleteData($IdProgramScheme);
	  		
	  		//get list
	  		$rows = $programSchemeDB->getProgSchemeByProgram($IdProgram);
	  	}
	  	
	  	
	  	if($type=='accreditation'){
	  		
	  		$IdProgramAccredition= $this->_getParam('IdProgramAccredition', 0);
	  		
	  		$programDB = new GeneralSetup_Model_DbTable_Program();
	  		
	  		//delete
	  		$programDB->deleteAccreditationData($IdProgramAccredition);
	  		
	  		//get list
	  		$rows = $programDB->fnviewAccredentialDetails($IdProgram);
	  	}
	  	
	  	
   		if($type=='majoring'){
	  		
	  		$IDProgramMajoring = $this->_getParam('IDProgramMajoring', 0);
	  		
	  		$majorDB = new GeneralSetup_Model_DbTable_ProgramMajoring();
	  		
	  		$majorDB->deleteData($IDProgramMajoring);
	  		
	  		$rows = $majorDB->getData($IdProgram);
	  	}

       if($type=='minor'){

           $IDProgramMinor = $this->_getParam('IDProgramMinor', 0);

           $minorDB = new GeneralSetup_Model_DbTable_ProgramMinor();

           $minorDB->deleteData($IDProgramMinor);

           $rows = $minorDB->getData($IdProgram);
       }
	  	
   		if($type=='concurrent'){
	  		
	  		$cp_id = $this->_getParam('cp_id', 0);
	  		
	  		$cpDB = new GeneralSetup_Model_DbTable_ConcurrentProgram();
	  		
	  		$cpDB->deleteData($cp_id);
	  		
	  		$rows = $cpDB->getDatabyProgramId($IdProgram);
	  	}

       if($type=='majorIntake'){

           $IDMajorIntake = $this->_getParam('IDMajorIntake', 0);
           $IdMajor = $this->_getParam('IdMajor', null);

           $majorIntakeDB = new GeneralSetup_Model_DbTable_ProgramMajorIntake();
           //add
           $majorIntakeDB->deleteData($IDMajorIntake);

           //get list
           $rows = $majorIntakeDB->getMajorIntakeList($IdMajor);
       }

       if($type=='majorStruc'){

           $IDMajorStruc = $this->_getParam('IDMajorStruc', 0);
           $IdMajor = $this->_getParam('IdMajor', null);

           $majorStrucDB = new GeneralSetup_Model_DbTable_ProgramMajorStructure();
           //add
           $majorStrucDB->deleteData($IDMajorStruc);

           //get list
           $rows = $majorStrucDB->getMajorStructureList($IdMajor);
       }
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($rows);
		
		echo $json;
		exit();
   }
   
   public function programSchemeAction(){
   	
   	
   		$this->_helper->layout->disableLayout();
   		
   		$IdProgram = $this->_getParam('id', 0);
   		$this->view->LintIdProgram=$IdProgram;
   		
   		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;       
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
   		
		//get program scheme
		$progschemeDb =  new GeneralSetup_Model_DbTable_Programscheme();
		$this->view->program_scheme = $progschemeDb->getProgSchemeByProgram($IdProgram);
   }
   
   public function deleteConcurentProgramAction(){
   	
		$IdProgram = $this->_getParam('id', 0);
		$cp_id = $this->_getParam('cp_id', 0);
		
		$cpDB = new GeneralSetup_Model_DbTable_ConcurrentProgram();
		
		$cpDB->deleteData($cp_id);
		
		$this->gobjsessionsis->flash = array('message' => 'Concurrent program has been deleted.', 'type' => 'success');
		
		//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'program', 'action'=>'editprogram','id'=>$IdProgram),'default',true));

		$this->_redirect($this->baseUrl . '/generalsetup/program/editprogram/id/'.$IdProgram.'#tab-3');
	  		
   }

}