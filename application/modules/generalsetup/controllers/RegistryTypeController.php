<?php

class Generalsetup_RegistryTypeController extends Base_Base
{

    public function indexAction()
    {

        $this->view->title = "Registry Types";

        $form = new GeneralSetup_Form_Registry_Search();

        $registryTypeDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $query = $registryTypeDb->getList(null, 'sql');

        $pageCount = 50;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $registryTypeDb->getList($formData, 'sql');

        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;

    }

    public function addAction()
    {

        $this->view->title = "Add Registry Type";

        $form = new GeneralSetup_Form_Registry_RegistryType();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $registryTypeDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();

                $data = array(
                    'code'        => $formData['code'],
                    'name'        => $formData['name'],
                    'name_malay'  => $formData['name_malay'],
                    'description' => $formData['description'],
                    'system'      => $formData['system'],
                );

                $registryTypeDb->insert($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'registry-type', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    public function editAction()
    {

        $id = $this->_getParam('id', null);

        if ($id == null) {
            $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'registry-type', 'action' => 'index'), 'default', true));
        }

        $this->view->title = "Edit Registry Type";

        $form = new GeneralSetup_Form_Registry_RegistryType();

        $registryTypeDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'code'        => $formData['code'],
                    'name'        => $formData['name'],
                    'name_malay'  => $formData['name_malay'],
                    'description' => $formData['description'],
                    'system'      => $formData['system'],
                );

                $registryTypeDb->update($data, array('id=?' => $id));

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully updated"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'registry-type', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        } else {
            $data = $registryTypeDb->fetchRow(array('id=?' => $id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }
}