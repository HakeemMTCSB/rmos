<?php
class GeneralSetup_LogdocumentStatusController extends Base_Base {
	
	private $locale;
	private $registry;
	//private $lobjNewawardform;
	private $Logstatus;
	private $_gobjlog;

	
	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');		
	}
	
	
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();		
		$this->lobjLogform = new GeneralSetup_Form_LogdocumentStatus();
		//$this->lobjNewawardform	= new GeneralSetup_Form_Newaward();
		$this->Logstatus = new GeneralSetup_Model_DbTable_LogdocumentStatus();
	}

    public function indexAction(){

        $this->view->title = "Status Setup";
        $form = new GeneralSetup_Form_LogdocumentStatusSearch();
        $logStatusDb = new GeneralSetup_Model_DbTable_LogdocumentStatus();
        $query = $logStatusDb->getStatusList(null,'sql');
        $pageCount = 50;

        //post
        if ($this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $logStatusDb->getStatusList($formData,'sql');

        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();


        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;

    }

    public function editAction(){

        $id = $this->_getParam('id',null);

        if($id==null){
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument-status', 'action'=>'index'),'default',true));
        }

        $this->view->title = "Edit Status Setup";

        $form = new GeneralSetup_Form_LogdocumentStatus();

        $logStatusDb = new GeneralSetup_Model_DbTable_LogdocumentStatus();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'tls_status' => strtoupper($formData['tls_status']),
                    'tls_status_malay' => strtoupper($formData['tls_status_malay']),
                );



                $logStatusDb->update($data,array('tls_id=?'=>$id));

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update Status Setup');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument-status', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }else{
            $data = $logStatusDb->fetchRow(array('tls_id=?'=>$id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }

	public function addAction(){
        $this->view->title = "Add Status Setup";
        $auth = Zend_Auth::getInstance();

        $form = new GeneralSetup_Form_LogdocumentStatus();


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $logStatusDb = new GeneralSetup_Model_DbTable_LogdocumentStatus();

                $data = array(
                    'tls_status' => strtoupper($formData['tls_status']),
                    'tls_status_malay' => strtoupper($formData['tls_status_malay']),
                    'tls_createdby' => $auth->getIdentity()->iduser,
                    'tls_createddt' => date ('Y-m-d H:i:s'),

                );

                $logStatusDb->addData($data);

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add Status Setup');

                $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'logdocument-status', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
	}

	
}


?>