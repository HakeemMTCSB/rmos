<?php

class GeneralSetup_ModuleAccessibilityController extends Base_Base {
	
	public function init(){
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	public function indexAction(){
		
		$this->view->title = $this->view->translate("Module Accessibility");
				
		$msg = $this->_getParam('msg',null);
		if($msg==1){
			$this->view->noticeSuccess = "Information has been saved";
		}
		if($msg==2){
			$this->view->noticeSuccess = "Information has been deleted";
		}
		
		$this->view->form = new GeneralSetup_Form_SearchModuleAccess();
			
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		$accessDb = new GeneralSetup_Model_DbTable_ModuleAccessibility();
		
		$formData = $this->getRequest()->getPost();
		$accessList = $accessDb->searchData($formData);
		
		
		$this->view->accessList = $this->lobjCommon->fnPagination($accessList,$lintpage,$lintpagecount);	
	}
	
	public function addAction(){
		
		$this->view->title = $this->view->translate("Module Accessibility - Add");
		
		$form = new GeneralSetup_Form_ModuleAccess();
		
		$this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'module-accessibility', 'action'=>'index','msg'=>1),'default',true);
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
				
				$auth = Zend_Auth::getInstance();
				$accessDb = new GeneralSetup_Model_DbTable_ModuleAccessibility();
				
				$data = array(							
							'ma_role_id' => $formData['ma_role_id'],
							'ma_start_date' => date('Y-m-d',strtotime($formData['ma_start_date'])),
							'ma_end_date' => date('Y-m-d',strtotime($formData['ma_end_date'])),
							'ma_faculty_id' => $formData['ma_faculty_id'],
							'ma_program_id' => $formData['ma_program_id'],
							'ma_branch_id' => $formData['ma_branch_id'],
							'ma_course_id' => $formData['ma_course_id'],
							'ma_createddt' => date('Y-m-d H:i:s'),
							'ma_createdby' =>  $auth->getIdentity()->iduser							
						);
				
				$accessDb->insert($data);
				
				//redirect
				$this->_redirect($this->view->backURL);	
			}else{
				$form->populate($formData);
			}
			
		}
		
		$this->view->form = $form;
	}
	
	
	public function editAction(){
		
		$this->view->title = $this->view->translate("Module Accessibility - Edit");
		
		$ma_id = $this->_getParam('id',null);
			
		$accessDb = new GeneralSetup_Model_DbTable_ModuleAccessibility();
		$role = $accessDb->getData($ma_id);
		$this->view->role = $role;	
			
		$form = new GeneralSetup_Form_ModuleAccess();
		
		$this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'module-accessibility', 'action'=>'index'),'default',true);
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
				
				$data = array(							
							'ma_role_id' => $formData['ma_role_id'],
							'ma_start_date' => $formData['ma_start_date'],
							'ma_end_date' => $formData['ma_end_date'],
							'ma_faculty_id' => $formData['ma_faculty_id'],
							'ma_program_id' => $formData['ma_program_id'],
							'ma_branch_id' => $formData['ma_branch_id'],
							'ma_course_id' => $formData['ma_course_id']						
						);
				
				$accessDb->update($data,'ma_id = '.$ma_id);
				
				//redirect
				$this->view->backURL2 = $this->view->url(array('module'=>'generalsetup','controller'=>'module-accessibility', 'action'=>'index','msg'=>1),'default',true);
		
				$this->_redirect($this->view->backURL2);	
			}else{
				$form->populate($formData);
			}
			
		}else{
			$form->populate($role);
		}
		
		$this->view->form = $form;
	}
	
	
public function deleteAction(){
		
		$ma_id = $this->_getParam('id',null);
		
		$accessDb = new GeneralSetup_Model_DbTable_ModuleAccessibility();
		$accessDb->deleteData($ma_id);
		
		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'module-accessibility', 'action'=>'index','msg'=>2),'default',true));
	}
}

?>