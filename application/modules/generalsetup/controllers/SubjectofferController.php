<?php
class GeneralSetup_SubjectofferController extends Base_Base 
{
	public function indexAction() 
	{
		$this->view->title = $this->view->translate("Course Offered");
		$form = new GeneralSetup_Form_CourseSearch();
		$form->removeElement('SubjectName');
		$form->removeElement('SubjectCode');
		$idsemester = $this->_getparam("idsemester");
		//var_dump($idsemester);
		$idcollege = $this->_getparam("IdCollege");
		//var_dump($idcollege);
		
		if($this->getRequest()->getPost())
		{
			$formData =$this->getRequest()->getPost();
			//$idsemester = $formData["IdSemester"];
			$idcollege = $formData["IdCollege"];			
		}		

		$sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
		
		if($idsemester!="" and $idcollege!="")
		{
			$form->populate(array("IdSemester"=>$idsemester,"IdCollege"=>$idcollege));

			$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();

			$this->view->idsem = $idsemester;
			$this->view->idcol = $idcollege;
			//Subject list base on Program Landscape from the selected faculty 

			//$programs = $progDB->fngetProgramDetails($idcollege);
			//$programs = $progDB->getProgramDetailsByCollege($idcollege);
                        $subjects = $sofferedDB->getSubjectOffered($idsemester, $idcollege);

			//var_dump($idcollege);
			/*$i=0;
			$j=0;
			$allblocklandscape=null;
			$allsemlandscape=null;
		
			if (count($programs))
			{
				foreach ($programs as $key => $program)
				{
					$activeLandscape=$landscapeDB->getAllLandscape($program["IdProgram"]);
					
					foreach($activeLandscape as $actl)
					{
						if($actl["LandscapeType"]!=44)
						{
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}
						elseif($actl["LandscapeType"]==44)
						{
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				}
			}
			else 
			{
				$subjects = FALSE;
			}

			if(is_array($allsemlandscape))
			{
				$subjectsem=$sofferedDB->getMultiLandscapeCourseOffer($allsemlandscape,"",$idsemester);
			}

			if(is_array($allblocklandscape))
			{
				$subjectblock=$sofferedDB->getMultiBlockLandscapeCourseOffer($allblocklandscape,null,$idsemester);
			}

			if(is_array($allsemlandscape) && is_array($allblocklandscape))
			{
				$subjects=array_merge( $subjectsem , $subjectblock );
			}
			else
			{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape))
				{
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape))
				{
					$subjects=$subjectblock;
				}
				else
				{
					$subjects=FALSE;
				}
			}*/			


			$this->view->courses= $subjects;
			//var_dump($subjects);
		}
		else
		{

			$form->populate(array("IdSemester"=>$idsemester));
                        $subjects = $sofferedDB->getSubjectOffered($idsemester);
                        $this->view->idsem = $idsemester;
			/*$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();

			$this->view->idsem = $idsemester;
			//$this->view->idcol = $idcollege;
			//Subject list base on Program Landscape from the selected faculty 
			$programs = $progDB->fngetProgramDetails();
			$i=0;
			$j=0;
			$allblocklandscape=null;
			$allsemlandscape=null;

			if (count($programs))
			{
				foreach ($programs as $key => $program)
				{
					$activeLandscape=$landscapeDB->getAllLandscape($program["IdProgram"]);
					foreach($activeLandscape as $actl)
					{
						if($actl["LandscapeType"]!=44)
						{
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}
						elseif($actl["LandscapeType"]==44)
						{
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				}
			}
			else 
			{
				$subjects = FALSE;
			}

			if(is_array($allsemlandscape))
			{
				$subjectsem=$sofferedDB->getMultiLandscapeCourseOffer($allsemlandscape,"",$idsemester);
			}
			
			if(is_array($allblocklandscape))
			{
				$subjectblock=$sofferedDB->getMultiBlockLandscapeCourseOffer($allblocklandscape,null,$idsemester);
			}

			if(is_array($allsemlandscape) && is_array($allblocklandscape))
			{
				$subjects=array_merge( $subjectsem , $subjectblock );
			}
			else
			{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape))
				{
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape))
				{
					$subjects=$subjectblock;
				}
				else
				{
					$subjects=FALSE;
				}	
			}*/	
			
			$this->view->courses= $subjects;
		}
                //var_dump($subjects);
		$this->view->form = $form;
	}
	
	public function addAction()
	{
		$this->view->title = $this->view->translate("Add Course Offer");
		$form = new GeneralSetup_Form_CourseSearch();
		$form->removeElement('SubjectName');
		$form->removeElement('SubjectCode');
		$idsemester = $this->_getparam("idsem");
		$idcollege = $this->_getparam("idcol");
		
		if($this->getRequest()->getPost())
		{
			$formData =$this->getRequest()->getPost();
			//$idsemester = $formData["IdSemester"];
			$idcollege = $formData["IdCollege"];		
			//var_dump($idcollege);
		}	
		
		$form->populate(array("IdSemester"=>$idsemester));
		$form->populate(array("IdCollege"=>$idcollege));

		$this->view->form = $form;
		
		if($idsemester!="" && $idcollege!="")
		{
			$progDB= new GeneralSetup_Model_DbTable_Program();
			$sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			
			$this->view->idsem = $idsemester;
			$this->view->idcol = $idcollege;
			//Subject list base on Program Landscape from the selected faculty 
			/*$programs = $progDB->getProgramDetailsByCollege($idcollege);
			//var_dump($programs);


			$i=0;
			$j=0;
			$allblocklandscape=null;
			$allsemlandscape=null;
			if (count($programs))
			{
				foreach ($programs as $key => $program)
				{
					$activeLandscape=$landscapeDB->getAllLandscape($program["IdProgram"]);
					//var_dump($activeLandscape);
					foreach($activeLandscape as $actl)
					{
						if($actl["LandscapeType"]!=44)
						{
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}
						elseif($actl["LandscapeType"]==44)
						{
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				}
			}
			else 
			{
				$subjects = FALSE;
			}	
			//var_dump($allsemlandscape); exit;
			if(is_array($allsemlandscape))
			{
				//$subjectsem=$sofferedDB->getMultiLandscapeNotCourseOffer($allsemlandscape,"",$idsemester);
                                $subjectsem = $sofferedDB->getSubjectToOffered($idcollege, $idsemester);
                                //var_dump($subjectsem); exit;
			}

			if(is_array($allblocklandscape))
			{
				//$subjectblock=$sofferedDB->getMultiBlockLandscapeNotCourseOffer($allblocklandscape,null,$idsemester);
				$subjectblock  = array();
			}

			if(is_array($allsemlandscape) && is_array($allblocklandscape))
			{
				$subjects=array_merge( $subjectsem , $subjectblock );
			}
			else
			{
				if(is_array($allsemlandscape) && !is_array($allblocklandscape))
				{
					$subjects=$subjectsem;
				}
				elseif(!is_array($allsemlandscape) && is_array($allblocklandscape))
				{
					$subjects=$subjectblock;
				}
				else
				{
					$subjects=FALSE;
                }		
			}*/
                        $subjects = $sofferedDB->getSubjectToOffered($idcollege, $idsemester);
			
			$this->view->courses= $subjects;
			//var_dump($subjects); exit;
		}
		else
		{
			$form->populate(array("IdSemester"=>$idsemester));

			/*$progDB= new GeneralSetup_Model_DbTable_Program();
			$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
			$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
			$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();*/

			$this->view->idsem = $idsemester;

			$subjects=FALSE;
			$this->view->courses= $subjects;
		}
	}
	
	public function runAction(){
		$sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
		$auth = Zend_Auth::getInstance();
		$userId = $auth->getIdentity()->iduser;
		$formData =$this->getRequest()->getPost();
		if($this->_getparam("type")=="add"){
			foreach($formData["idsubject"] as $key=>$val){
				$idsub = $val;
				$minq = $formData["minquota"][$key];
				$maxq = $formData["maxquota"][$key];
                                $unit = $formData["unit"][$key];
				$reserv = $formData["reservation"][$key];
				if($minq==""){
					$minq = $formData["dminquota"];
				}
				if($maxq==""){
					$maxq = $formData["dmaxquota"];
				}
                                /*if($unit==""){
					$unit = $formData["dunit"];
				}*/
				if($reserv==""){
					$reserv = $formData["dreservation"];
				}
				$data=array(
				'IdSemester'=>$formData["IdSemester"],
                                    'IdSubject'=>$idsub,
                                    'MinQuota'=>$minq,
                                    'MaxQuota'=>$maxq,
                                    //'unit'=>$unit,
                                    'reservation'=>$reserv,
                                    'UpdDate'=>date("Y-m-d H:i:s"),
                                    'UpdUser'=>$userId);
				//echo "$idsub - $minq - $maxq -".$formData["IdSemester"]."=".$formData["IdCollege"]."<br>";
				$sofferedDB->saveAllBranch($data);
		
			}
			//print_r($formData); 
		}elseif($this->_getparam("type")=="del"){
			foreach($formData["idsubject"] as $key=>$val){
				$sofferedDB->unoffered($key,$formData["IdSemester"]);
			}
		}
		$this->_redirect( $this->baseUrl . '/generalsetup/subjectoffer/index/idsemester/'.$formData["IdSemester"].'/IdCollege/'.$formData["IdCollege"]);
		exit;
	}
}