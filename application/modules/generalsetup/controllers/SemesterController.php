<?php

class GeneralSetup_SemesterController extends Base_Base
{
    private $lobjsemester;
    private $lobjsemesterForm;
    private $lobjsemestermaster;
    private $lobjinitialconfig;
    private $locale;
    private $registry;
    private $_gobjlog;
    private $lobjdeftype;

    public function init()
    {
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->fnsetObj();
        $this->registry = Zend_Registry::getInstance();
        $this->locale = $this->registry->get('Zend_Locale');

    }

    public function fnsetObj()
    {
        $this->lobjsemester = new GeneralSetup_Model_DbTable_Semester();
        $this->lobjsemesterForm = new GeneralSetup_Form_Semester ();
        $this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster ();
        $this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
        $this->lobcalendaractivity = new GeneralSetup_Model_DbTable_Activity();
        $this->lobjdeftype = new App_Model_Definitiontype();
    }

    public function indexAction()
    {
        $this->view->title = "Semester Setup";
        $this->view->lobjform = $this->lobjform;

        //Scheme List
        $larrscheme = $this->lobjsemester->fnGetShcemeList();
        foreach ($larrscheme as $larrdefmsresult) {
            $this->lobjform->field5->addMultiOption($larrdefmsresult['key'], $larrdefmsresult['value']);
        }

        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $query = $this->lobjsemester->fnSearchSemester();
        $this->view->locale = $this->locale;

        $pageCount = 50;

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();

            if ($this->lobjform->isValid($larrformData)) {
                $query = $this->lobjsemester->fnSearchSemester($larrformData); //searching the values for the user

                $this->gobjsessionsis->semesterQuery = $query;
                $this->lobjform->populate($larrformData);
                $this->_redirect($this->baseUrl . '/generalsetup/semester/index/search/1');

            }
        }

        if (!$this->_getParam('search')) {
            unset($this->gobjsessionsis->semesterQuery);
        } else {
            $query = $this->gobjsessionsis->semesterQuery;
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $this->view->paginator = $paginator;

    }

    public function newsemesterAction()
    { //title

        $idUniversity = $this->gobjsessionsis->idUniversity;
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);
        $this->view->title = "Add New Semester";
        $this->view->lobjsemesterForm = $this->lobjsemesterForm;

        if ($this->locale == 'ar_YE') {
            $this->view->lobjsemesterForm->SemesterMainStartDate->setAttrib('datePackage', "dojox.date.islamic");
            $this->view->lobjsemesterForm->SemesterMainEndDate->setAttrib('datePackage', "dojox.date.islamic");
        }

        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjsemesterForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
        $this->view->lobjsemesterForm->UpdUser->setValue($auth->getIdentity()->iduser);
        //$this->view->lobjsemesterForm->SemesterMainName->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_semestermaster', 'field' => 'SemesterMainName'));
        //$this->view->lobjsemesterForm->SemesterMainName->getValidator('Db_NoRecordExists')->setMessage("Record already exists");
        $this->view->lobjsemesterForm->SemesterMainCode->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_semestermaster', 'field' => 'SemesterMainCode'));
        $this->view->lobjsemesterForm->SemesterMainCode->getValidator('Db_NoRecordExists')->setMessage("Record already exists");


        if ($this->getRequest()->isPost()) {

            $formData = $formData2 = $this->getRequest()->getPost();

            if (!$this->view->lobjsemesterForm->isValid($formData)) {
                return $this->render("newsemester");
            }

            $checkSemDate = $this->cekSemesterDate($formData);

            if (intval($checkSemDate) == 0) {
                $this->gobjsessionsis->flash = array('message' => 'Cannot add semester. semester date must not have any gap', 'type' => 'error');
                $this->_redirect($this->baseUrl . '/generalsetup/semester/newsemester/');
            }

            //add semester info    
            unset($formData['Save']);
            $semesterMasterid = $this->lobjsemestermaster->fnaddSemester($formData);

            if ($semesterMasterid) {
                $moodle_semester = array(
                    'parent'      => '0',
                    'name'        => $formData['SemesterMainName'],
                    'idnumber'    => $formData['SemesterMainCode'],
                    'description' => $formData['SemesterMainName']
                );

                push_to_moodle('CreateCourseCategory', $moodle_semester);
            }
            // Write Logs
            $priority = Zend_Log::INFO;
            $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                             'level'       => $priority,
                             'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                             'time'        => date('Y-m-d H:i:s'),
                             'message'     => 'New Semester Add',
                             'Description' => Zend_Log::DEBUG,
                             'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
            $this->_gobjlog->write($larrlog); //insert to tbl_log

            $this->gobjsessionsis->flash = array('message' => 'Information has been successfully saved', 'type' => 'success');
            $this->_redirect($this->baseUrl . '/generalsetup/semester/index/');
        }
    }

    public function editsemesterAction()
    {

        $this->view->title = "Edit Semester";  //title
        $IdSemesterMaster = $this->_getParam('id', 0);

        $auth = Zend_Auth::getInstance();
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->lobjsemesterForm = $this->lobjsemesterForm;

        $this->view->lobjsemesterForm->SemesterMainCode->addValidator('Db_NoRecordExists', true, array(
                'table'   => 'tbl_semestermaster',
                'field'   => 'SemesterMainCode',
                'exclude' => array(
                    'field' => 'IdSemesterMaster',
                    'value' => $this->_getParam('id', 0)
                )
            )
        );

        $this->view->lobjsemesterForm->SemesterMainCode->getValidator('Db_NoRecordExists')->setMessage("Record already exists");
//        $this->lobjsemesterForm->SemesterMainCode->setAttrib('readonly', 'true');

        $result = $this->lobjsemestermaster->fetchRow('IdSemesterMaster  =' . $IdSemesterMaster);
        $result = $result->toArray();

        $this->lobjsemesterForm->populate($result);

        $ldtsystemDate = date('Y-m-d H:i:s');

        $this->view->lobjsemesterForm->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjsemesterForm->UpdUser->setValue($auth->getIdentity()->iduser);

        $statusSemDate = 1;
        $this->view->statusSemDate = $statusSemDate;
        $this->view->IdSemMas = $IdSemesterMaster;

        if ($this->getRequest()->isPost()) {
            $formData = $larrvalidatedata = $this->getRequest()->getPost();

            if ($this->lobjsemesterForm->isValid($larrvalidatedata)) {

                $lintIdSemester = $IdSemesterMaster;

                /*$checkSemDate = $this->cekSemesterDate($formData);

                if (intval($checkSemDate) == 0) {
                    $this->gobjsessionsis->flash = array('message' => 'Edit failed. semester date must not have any gap', 'type' => 'error');
                    $this->_redirect($this->baseUrl . '/generalsetup/semester/editsemester/id/' . $lintIdSemester);
                }*/

                $this->lobjsemestermaster->fnupdateSemester($formData, $lintIdSemester);//update university

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                 'level'       => $priority,
                                 'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                 'time'        => date('Y-m-d H:i:s'),
                                 'message'     => 'Semester Edit Id =' . $lintIdSemester,
                                 'Description' => Zend_Log::DEBUG,
                                 'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                $this->gobjsessionsis->flash = array('message' => 'Information has been successfully saved', 'type' => 'success');
                $this->_redirect($this->baseUrl . '/generalsetup/semester/index');
            }
        }
    }

    /**
     *
     * Action to handle semester copy
     */
    public function copysemesterAction()
    {
        $newSemesterData = array();
        $idUniversity = $this->gobjsessionsis->idUniversity;
        $auth = Zend_Auth::getInstance();
        $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            //echo "<pre>";print_r($formData);die;

            $IdSemesterMain = $formData['FromSemester'];
            $semesterData = $this->lobjsemestermaster->getData($IdSemesterMain);

            unset($formData['Copy']);
            unset($semesterData['IdSemesterMaster']);

            //Create the semester array to be inserted into the database
            $semesterData['SemesterMainName'] = $formData['CopySemesterMainName'];
            $semesterData['SemesterMainCode'] = $formData['CopySemesterMainCode'];
            $semesterData['UpdDate'] = date('Y-m-d H:i:s');
            $semesterData['UpdUser'] = $auth->getIdentity()->iduser;

            $newsemestermasterid = $this->lobjsemestermaster->fnaddSemester($semesterData);

            // Write Logs
            $priority = Zend_Log::INFO;
            $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                             'level'       => $priority,
                             'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                             'time'        => date('Y-m-d H:i:s'),
                             'message'     => 'Copy Semester',
                             'Description' => Zend_Log::DEBUG,
                             'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
            $this->_gobjlog->write($larrlog); //insert to tbl_log

            $this->gobjsessionsis->flash = array('message' => 'Successfully copy semester', 'type' => 'success');
            $this->_redirect($this->baseUrl . '/generalsetup/semester/editsemester/id/'.$newsemestermasterid);
        }
    }

    function deletesemesterdetailsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        //Get Po details Id
        $IdSemester = $this->_getParam('id');
        $this->lobjsemester->fnDeleteSemesterDetail($IdSemester);
        echo "1";
    }

    /**
     * Function to check duplicate records of Semester Code
     * @author Vipul
     */
    public function checksemcodeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $semCode = $this->getRequest()->getParam('value');
        $condition = " SemesterCode = '" . $semCode . "'  ";
        $conditionmainsem = " SemesterMainCode = '" . $semCode . "'  ";
        $resultData = $this->lobjsemester->fncheckdupSemCode($condition);
        $resultmainsem = $this->lobjsemester->fncheckdupMainSemCode($conditionmainsem);
        if ($resultData[0]['num'] != '0' || $resultmainsem[0]['num'] != '0') {
            echo 'false';
        } else {
            echo 'true';
        }
        die;

    }

    /**
     * Semester list program activity
     * @author Jasdy
     */
    public function semesterListAction()
    {
        $this->view->title = "Program Setup";
        $this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        //$larrresult = $this->lobjsemester->fngetSemesterDetails(); //get user details
        $larrresult = $this->lobjsemestermaster->fngetSemestermainDetails();

        if (!$this->_getParam('search'))
            unset($this->gobjsessionsis->semesterpaginatorresult);

        $lintpagecount = $this->gintPageCount;;// Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance
        if (isset($this->gobjsessionsis->semesterpaginatorresult)) {
            $this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->semesterpaginatorresult, $lintpage, $lintpagecount);
        } else {
            $this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
        }

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();
            if ($this->lobjform->isValid($larrformData)) {
                $larrresult = $this->lobjsemester->fnSearchSemester($this->lobjform->getValues()); //searching the values for the user
                $this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
                $this->gobjsessionsis->semesterpaginatorresult = $larrresult;
            }
        }
        if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
            //$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'semester', 'action'=>'index'),'default',true));
            $this->_redirect($this->baseUrl . '/generalsetup/semester/semester-list');
        }
    }

    public function semesterDetailProgramAction()
    {
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $this->view->title = "Edit Semester";  //title

        $Program = new GeneralSetup_Model_DbTable_Program();
        $program = $Program->fnGetProgramList();
        $this->view->program = $program;

        $idUniversity = $this->gobjsessionsis->idUniversity;
        $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);

        $IdSemesterMaster = $this->_getParam('id', 0);
        $result = $this->lobjsemestermaster->getData($IdSemesterMaster);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $larractivitylist = $this->lobcalendaractivity->fngetActivityActive();

            foreach ($larractivitylist as $index => $activity) {
                $startDate = 'StartDate' . $activity['key'];
                $endDate = 'EndDate' . $activity['key'];
                $id = 'id' . $activity['key'];
                if ((!empty($formData[$startDate])) && (!empty($formData[$endDate]))) {
                    /*
                     * if id has a value, update. if not insert 
                     */
                    if (empty($formData[$id])) {

                        $calendarSchedule = array(
                            'StartDate'        => $formData[$startDate],
                            'EndDate'          => $formData[$endDate],
                            'IdActivity'       => $activity['key'],
                            'IdSemesterMaster' => $formData['IdSemesterMaster'],
                            'IdProgram'        => $formData['idProgram']
                        );
                        //print_r($calendarSchedule);
                        $this->lobcalendaractivity->fnainsertCalenderActivity($calendarSchedule);

                    } else {
                        $calendarSchedule = array(
                            'StartDate'  => $formData[$startDate],
                            'EndDate'    => $formData[$endDate],
                            'IdActivity' => $activity['key'],
                            'IdProgram'  => $formData['idProgram'],
                            'id'         => $formData[$id]
                        );
                        //print_r($calendarSchedule);
                        $this->lobcalendaractivity->fnupdatecalender($calendarSchedule);
                    }

                }
            }

            //$this->_redirect( $this->baseUrl . '/generalsetup/semester/semester-list');
        }

        $this->view->result = $result;
        $this->view->IdSemesterMaster = $IdSemesterMaster;
    }


    public function semesterProgramActivityAction()
    {
        $this->_helper->layout->disableLayout();

        $programId = $this->_getParam('programid', 0);
        $IdSemesterMaster = $this->_getParam('semestermaster', 0);
        $this->view->lobjsemesterForm = $this->lobjsemesterForm;
        $larractivitylist = $this->lobcalendaractivity->fngetActivityActive();

        $this->view->larractivitylist = $larractivitylist;

        $larraddDrop = $this->lobcalendaractivity->getaddDrop($IdSemesterMaster, $programId);

        if (isset($larraddDrop[0])) {
            /*
             * Rearrange array for display purpose
             */

            foreach ($larraddDrop as $key => $value) {

                $addDropData['StartDate' . $value['IdActivity']] = $value['StartDate'];
                $addDropData['EndDate' . $value['IdActivity']] = $value['EndDate'];
                $addDropData['id' . $value['IdActivity']] = $value['id'];

            }

            $this->view->larraddDrop = $addDropData;
        } else {
            //echo 'sini';
            //Ambil value dari main activity
            $larraddDrop = $this->lobcalendaractivity->getaddDrop($IdSemesterMaster, 0);
            if (isset($larraddDrop[0])) {
                foreach ($larraddDrop as $key => $value) {

                    $addDropData['StartDate' . $value['IdActivity']] = $value['StartDate'];
                    $addDropData['EndDate' . $value['IdActivity']] = $value['EndDate'];
                    $addDropData['id' . $value['IdActivity']] = "";

                }
                //print_r($addDropData);
                $this->view->larraddDrop = $addDropData;
            }
        }


    }

    public function cekSemesterDate($formData)
    {
        //dd($formData);
        $status = 1;
        if ($formData['special_semester'] == 0) {
            $db = Zend_Db_Table::getDefaultAdapter();

            if ($formData['sem_seq'] == 'JAN') {
                $year = $formData['AcademicYear'] - 1;
                $month = 'SEP';
            } else if ($formData['sem_seq'] == 'JUN') {
                $year = $formData['AcademicYear'];
                $month = 'JAN';
            } else if ($formData['sem_seq'] == 'SEP') {
                $year = $formData['AcademicYear'];
                $month = 'JUN';
            }

            $selectPrevSem = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where('a.AcademicYear = ?', $year)
                ->where('a.sem_seq = ?', $month)
                ->where('a.IdScheme = ?', $formData['IdScheme'])
                ->where('a.special_semester = ?', $formData['special_semester']);

            $getPrevSem = $db->fetchRow($selectPrevSem);

            if ($getPrevSem) {
                //dd($getPrevSem);
                $now = strtotime($formData['SemesterMainStartDate']);
                $your_date = strtotime($getPrevSem['SemesterMainEndDate']);
                $datediff = $now - $your_date;

                $day = floor($datediff / (60 * 60 * 24));

                if (intval($day) > 1) {
                    $status = 0;
                    return $status;
                }
            }

            if ($formData['sem_seq'] == 'JAN') {
                $year = $formData['AcademicYear'];
                $month = 'JUN';
            } else if ($formData['sem_seq'] == 'JUN') {
                $year = $formData['AcademicYear'];
                $month = 'SEP';
            } else if ($formData['sem_seq'] == 'SEP') {
                $year = $formData['AcademicYear'] + 1;
                $month = 'JAN';
            }

            $selectNextSem = $db->select()
                ->from(array('a' => 'tbl_semestermaster'))
                ->where('a.AcademicYear = ?', $year)
                ->where('a.sem_seq = ?', $month)
                ->where('a.IdScheme = ?', $formData['IdScheme'])
                ->where('a.special_semester = ?', $formData['special_semester']);

            $getNextSem = $db->fetchRow($selectNextSem);

            if ($getNextSem) {
                //dd($getNextSem);
                $now = strtotime($getNextSem['SemesterMainStartDate']);
                $your_date = strtotime($formData['SemesterMainEndDate']);
                $datediff = $now - $your_date;

                $day = floor($datediff / (60 * 60 * 24));

                if (intval($day) > 1) {
                    $status = 0;
                    return $status;
                }
            }
        }

        return $status;
    }
}