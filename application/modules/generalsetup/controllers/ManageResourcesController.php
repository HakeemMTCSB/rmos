<?php

class GeneralSetup_ManageResourcesController extends Base_Base {
	
		
	public function viewResourcesAction(){
		
		$this->view->title = $this->view->translate("Manage Resources - View Resources");
		
		$form = new GeneralSetup_Form_SearchResource();
		$this->view->form = $form;
		
		$resourceDb = new GeneralSetup_Model_DbTable_TblResources();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {		
				$data = $resourceDb->searchResource($formData);
			}else{
				$form->populate($formData);
			}
			
		}else{
			$data = $resourceDb->searchResource();
		}
		//var_dump($data);
		//exit;
		$this->view->resources = $data;
		
	}
	
	public function scanAction()
	{
		$auth = Zend_Auth::getInstance();
		$this->view->iduser =  $iduser = $auth->getIdentity()->iduser;

		// im not sure why but probably you dont want other users to use this tool
		if ( $iduser != 1 )
		{
			die('You cannot use this function, only userid: 1 can.');
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost ();
			
			if ( count($larrformData['missing']) > 0 )
			{
				foreach ( $larrformData['missing'] as $id )
				{
					$data = $larrformData['values'][$id]['Module'];
					
					$auth = Zend_Auth::getInstance();
					$resourceDb = new GeneralSetup_Model_DbTable_TblResources();
					
					$data = array(							
								'r_menu_id' => 1,
								'r_title_id' => 0,
								'r_screen_id' => 0,
								'r_module' => $larrformData['values'][$id]['Module'],
								'r_controller' => $larrformData['values'][$id]['Controller'],
								'r_group' => 1,
								'r_action' => $larrformData['values'][$id]['Action'],
								'r_createddt' => date('Y-m-d H:i:s'),
								'r_createdby' => $auth->getIdentity()->iduser							
							);
					
					$resourceDb->insert($data);
				}
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Resources added. Don't forget to add permission in the menu if needed :)"));
				
				$this->_redirect( $this->baseUrl . '/generalsetup/manage-resources/scan/resetACL/1' );
			}
		}

		$objResources = new Sis_ACL_Resources();
		$objResources->buildAllArrays();

		$missing = $objResources->getMissing();
		
		$this->view->missing = $missing;
	}

	public function addResourceAction(){
		
		$this->view->title = $this->view->translate("Manage Resources - Add Resource");
		
		$form = new GeneralSetup_Form_Resources();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
				
				$auth = Zend_Auth::getInstance();
				$resourceDb = new GeneralSetup_Model_DbTable_TblResources();
				
				$data = array(							
							'r_menu_id' => $formData['r_menu_id'],
							'r_title_id' => $formData['r_title_id'],
							'r_screen_id' => $formData['r_screen_id'],
							'r_module' => $formData['r_module'],
							'r_controller' => $formData['r_controller'],
							'r_group' => $formData['r_group'],
							'r_action' => $formData['r_action'],
							'r_createddt' => date('Y-m-d H:i:s'),
							'r_createdby' => $auth->getIdentity()->iduser							
						);
				
				$resourceDb->insert($data);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'generalsetup', 'controller'=>'manage-resources','action'=>'view-resources','resetACL'=>1),'default',true));	
			}else{
				$form->populate($formData);
			}
			
		}
	}
	
	public function editResourceAction(){
		
		$this->view->title = $this->view->translate("Manage Resources - Edit Resource");
		
		$form = new GeneralSetup_Form_Resources();
		
		$r_id = $this->_getParam('id',null);
		
		$resourceDb = new GeneralSetup_Model_DbTable_TblResources();
		$data = $resourceDb->getData($r_id);
		$this->view->data = $data;
				
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
		
				$data = array(							
							'r_menu_id' => $formData['r_menu_id'],
							'r_title_id' => $formData['r_title_id'],
							'r_screen_id' => $formData['r_screen_id'],
							'r_controller' => $formData['r_controller'],
							'r_group' => $formData['r_group'],
							'r_action' => $formData['r_action']						
						);
				
				$resourceDb->update($data,'r_id = '.$r_id);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'generalsetup', 'controller'=>'manage-resources','action'=>'view-resources'),'default',true));	
			}else{
				$form->populate($formData);
			}
			
		}else{
			$form->populate($data);
		}
		
		$this->view->form = $form;
		
	
	}
	
	public function deleteResourceAction(){
		
		$r_id = $this->_getParam('id',null);
		
		$resourceDb = new GeneralSetup_Model_DbTable_TblResources();
		$resourceDb->deleteData($r_id);
		
		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'manage-resources', 'action'=>'view-resources'),'default',true));
	}
	
}
?>