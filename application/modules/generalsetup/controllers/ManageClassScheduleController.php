<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 12/11/2015
 * Time: 9:19 AM
 */
class GeneralSetup_ManageClassScheduleController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_ManageClassSchedule();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Manage Class Schedule');

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Schedule_Form_ClassScheduleSearch(array('schemeid'=>$formData['scheme']));
            $form->populate($formData);
            $this->view->form = $form;

            $class = $this->model->getClass($formData);
            $this->view->class = $class;
        }else{
            $form = new Schedule_Form_ClassScheduleSearch();
            $this->view->form = $form;
        }
    }

    public function clearAction(){
        $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/index');
        exit;
    }

    public function manageClassAction(){
        $this->view->title = $this->view->translate('Manage Class Schedule');

        $id = $this->_getParam('id',null);
        $this->view->groupid = $id;

        $atmodel = new GeneralSetup_Model_DbTable_Attendance();

        $groupInfo = $atmodel->getGoupInfo($id);
        $classList = $this->getSchedule($id);

        //var_dump($classList);

        $this->view->groupInfo = $groupInfo;
        $this->view->classList = $classList;
    }

    public function getSchedule($groupId){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $model = new GeneralSetup_Model_DbTable_Attendance();

        $groupInfo = $model->getGoupInfo($groupId);

        $this->view->groupInfo = $groupInfo;

        $studentList = $model->getGroupStudent($groupId);

        $this->view->studentList = $studentList;

        $groupSchedule = $model->getGroupSchedule($groupId, 1);

        $sdate = $model->getAttendanceDate($groupId, 1);
        //var_dump($sdate);
        $status = $model->getDefinition();
        $this->view->status = $status;

        if ($groupSchedule){
            $classList = $groupSchedule;
        }else{
            throw new Exception('Group Schedule Not Found');
        }

        $weekArr = array();
        $monthArr = array();

        if ($classList){
            $i = 0;
            foreach ($classList as $key => $dateLoop){

                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }

                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            //$classList[$i]['sc_id'] = $dateLoop['sc_id'];

                            $scdate = date('Y-m-d', strtotime($sdateLoop['ctd_scdate']));
                            $day = date('l', strtotime($sdateLoop['ctd_scdate']));

                            $schedule = $model->getSchedule($groupId, $scdate);

                            if (!$schedule){
                                $schedule = $model->getSchedule3($groupId, $scdate);

                                if (!$schedule){
                                    $schedule = $model->getSchedule2($groupId, $day);
                                        $starttime = $schedule['sc_start_time'];
                                        $endtime = $schedule['sc_end_time'];
                                        $venue = $schedule['sc_venue'];
                                        $lecturer = $schedule['IdLecturer'];
                                }else{
                                        $starttime = $schedule['tsd_starttime'];
                                        $endtime = $schedule['tsd_enddate'];
                                        $venue = $schedule['tsd_venue'];
                                        $lecturer = $schedule['tsd_lecturer'];
                                }
                            }else{
                                    $starttime = $schedule['sc_start_time'];
                                    $endtime = $schedule['sc_end_time'];
                                    $venue = $schedule['sc_venue'];
                                    $lecturer = $schedule['IdLecturer'];
                            }

                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $detectday1 = date('l', strtotime($sdateLoop['ctd_scdate']));
                            $detectday2 = $dateLoop['sc_day'];

                            //if (!isset($classList[$i]['sc_id'])) {
                                $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                                $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                                $classList[$i]['IdLecturer'] = $lecturer;
                                $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                                $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                                $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                                $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                                $classList[$i]['sc_start_time'] = $starttime;
                                $classList[$i]['sc_end_time'] = $endtime;
                                $classList[$i]['sc_venue'] = $venue;
                                $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                                $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                                $classList[$i]['sc_status'] = $sdateLoop['ctd_class_status'];
                                $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                                $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                                $classList[$i]['classtype']=$dateLoop['classtype'];
                                $classList[$i]['lecturername']=$schedule['lecturername'];
                                $classList[$i]['veneuname']=$schedule['veneuname'];
                            //}

                            $i++;

                            $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                            $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;

                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                            $classList[$i]['IdLecturer'] = $dateLoop['IdLecturer'];
                            $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                            $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                            $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                            $classList[$i]['sc_start_time'] = $dateLoop['sc_start_time'];
                            $classList[$i]['sc_end_time'] = $dateLoop['sc_end_time'];
                            $classList[$i]['sc_venue'] = $dateLoop['sc_venue'];
                            $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                            $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                            $classList[$i]['sc_status'] = $dateLoop['sc_status'];
                            $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                            $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                            $classList[$i]['classtype']=$dateLoop['classtype'];
                            $classList[$i]['lecturername']=$dateLoop['lecturername'];
                            $classList[$i]['veneuname']=$dateLoop['veneuname'];

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                }

                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));

                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }

                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
            }
        }

        //$this->aasort($classList, 'sc_date');

        if ($studentList){
            foreach ($studentList as $studentLoop2){
                if ($classList){
                    foreach ($classList as $classLoop2){
                        if ($classLoop2['sc_date'] != null){
                            $checkStatus = $model->checkAttendanceStatus($groupId, $studentLoop2['IdStudentRegistration'], $classLoop2['sc_date']);
                            $checkStatus2 = $model->checkAttendanceStatus($groupId, 0, $classLoop2['sc_date']);

                            if ($checkStatus){ //update
                                $data = array(
                                    'ctd_item_type'=>$classLoop2['idClassType'],
                                );
                                $model->updateAttendance($data, $checkStatus['ctd_id']);
                            }else{ //insert
                                $data = array(
                                    'ctd_groupid'=>$groupId,
                                    'ctd_studentid'=>$studentLoop2['IdStudentRegistration'],
                                    'ctd_item_type'=>$classLoop2['idClassType'],
                                    'ctd_scdate'=>$classLoop2['sc_date'],
                                    'ctd_status'=>395,
                                    'ctd_class_status'=>isset($checkStatus2['ctd_class_status']) ? $checkStatus2['ctd_class_status']:1,
                                    'ctd_reason'=>'',
                                    'ctd_upddate'=>date('Y-m-d h:i:s'),
                                    'ctd_updby'=>$userId
                                );
                                $model->insertAttendance($data);
                            }
                        }
                    }
                }
            }

            $model->deleteTempAtt($groupId);
        }else{
            if ($classList){
                foreach ($classList as $classLoop2){
                    if ($classLoop2['sc_date'] != null){
                        $checkStatus = $model->checkAttendanceStatus($groupId, 0, $classLoop2['sc_date']);

                        if ($checkStatus){ //update
                            $data = array(
                                'ctd_item_type'=>$classLoop2['idClassType'],
                            );
                            $model->updateAttendance($data, $checkStatus['ctd_id']);
                        }else{ //insert
                            $data = array(
                                'ctd_groupid'=>$groupId,
                                'ctd_studentid'=>0,
                                'ctd_item_type'=>$classLoop2['idClassType'],
                                'ctd_scdate'=>$classLoop2['sc_date'],
                                'ctd_status'=>395,
                                'ctd_reason'=>'',
                                'ctd_upddate'=>date('Y-m-d h:i:s'),
                                'ctd_updby'=>$userId
                            );
                            $model->insertAttendance($data);
                        }
                    }
                }
            }
        }

        return $classList;
    }

    public function editClassAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $this->view->title = $this->view->translate('Manage Class Schedule');

        $model = new GeneralSetup_Model_DbTable_Attendance();
        $scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();

        $groupId = $this->_getParam('groupid', 0);
        $date = $this->_getParam('date', null);

        $this->view->groupid = $groupId;
        $this->view->date = $date;

        $form = new GeneralSetup_Form_EditAttendanceDetails();

        $scdate = date('Y-m-d', $date);
        $day = date('l', $date);

        $schedule = $model->getSchedule($groupId, $scdate);

        if (!$schedule){
            $schedule = $model->getSchedule3($groupId, $scdate);

            if (!$schedule){
                $schedule = $model->getSchedule2($groupId, $day);

                $populateData = array(
                    'date'=>date('d-m-Y', $date),
                    'starttime'=>$schedule['sc_start_time'],
                    'endtime'=>$schedule['sc_end_time'],
                    'venue'=>$schedule['sc_venue'],
                    'lecturer'=>$schedule['IdLecturer']
                );
            }else{
                $populateData = array(
                    'date'=>date('d-m-Y', $date),
                    'starttime'=>$schedule['tsd_starttime'],
                    'endtime'=>$schedule['tsd_enddate'],
                    'venue'=>$schedule['tsd_venue'],
                    'lecturer'=>$schedule['tsd_lecturer']
                );
            }
        }else{
            $populateData = array(
                'date'=>date('d-m-Y', $date),
                'starttime'=>$schedule['sc_start_time'],
                'endtime'=>$schedule['sc_end_time'],
                'venue'=>$schedule['sc_venue'],
                'lecturer'=>$schedule['IdLecturer']
            );
        }

        $form->populate($populateData);
        $this->view->form = $form;

        $manageType = $this->model->getDefination(185);
        $this->view->manageType = $manageType;
        $this->view->populateData = $populateData;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $scdate = date('Y-m-d', $formData['scdate']);
            $day = date('l', $formData['scdate']);

            $schedule = $model->getSchedule($formData['groupid'], $scdate);

            if ($formData['mtype']==1032){ //cancel
                if (!$schedule) { //schedule susah
                    $scheduleCancelData = array(
                        'ctd_class_status'=>0
                    );

                    $model->updateAttendanceDate($scheduleCancelData, $formData['groupid'], $scdate);
                }else{ //schedule senang
                    $scheduleCancelData = array(
                        'sc_status'=>0
                    );
                    $model->updateSchedule($scheduleCancelData, $schedule['sc_id']);

                    $scheduleCancelData2 = array(
                        'ctd_class_status'=>0
                    );
                    $model->updateAttendanceDate($scheduleCancelData2, $formData['groupid'], $scdate);
                }
            }else{ //postpone
                if (!$schedule){ //schedule susah
                    $schedule = $model->getSchedule3($formData['groupid'], $scdate);

                    if (!$schedule){
                        $data = array(
                            'tsd_groupid'=>$formData['groupid'],
                            'tsd_scdate'=>date('Y-m-d', strtotime($formData['date'])),
                            'tsd_starttime'=>$formData['starttime'],
                            'tsd_enddate'=>$formData['endtime'],
                            'tsd_venue'=>$formData['venue']=='' ? 0:$formData['venue'],
                            'tsd_lecturer'=>$formData['lecturer'],
                            'tsd_upddate'=>date('Y-m-d H:i:s'),
                            'tsd_updby'=>$userId
                        );
                        $tsdid = $model->insertScheduleDetail($data);

                        //history
                        $dataHis = array(
                            'tsd_id'=>$tsdid,
                            'tsd_groupid'=>$formData['groupid'],
                            'tsd_scdate'=>date('Y-m-d', strtotime($populateData['date'])),
                            'tsd_starttime'=>$populateData['starttime'],
                            'tsd_enddate'=>$populateData['endtime'],
                            'tsd_venue'=>$populateData['venue']!=null ? $populateData['venue']:0,
                            'tsd_lecturer'=>$populateData['lecturer']!=null ? $populateData['lecturer']:0,
                            'tsd_upddate'=>date('Y-m-d H:i:s'),
                            'tsd_updby'=>$userId,
                            'upddate'=>date('Y-m-d H:i:s'),
                            'updby'=>$userId
                        );
                        $model->insertScheduleDetailHistory($dataHis);
                    }else{
                        $data = array(
                            'tsd_groupid'=>$formData['groupid'],
                            'tsd_scdate'=>date('Y-m-d', strtotime($formData['date'])),
                            'tsd_starttime'=>$formData['starttime'],
                            'tsd_enddate'=>$formData['endtime'],
                            'tsd_venue'=>$formData['venue']=='' ? 0:$formData['venue'],
                            'tsd_lecturer'=>$formData['lecturer'],
                            'tsd_upddate'=>date('Y-m-d H:i:s'),
                            'tsd_updby'=>$userId
                        );
                        $model->updateScheduleDetail($data, $schedule['tsd_id']);

                        //history
                        $dataHis = array(
                            'tsd_id'=>$schedule['tsd_id'],
                            'tsd_groupid'=>$schedule['tsd_groupid'],
                            'tsd_scdate'=>$schedule['tsd_scdate'],
                            'tsd_starttime'=>$schedule['tsd_starttime'],
                            'tsd_enddate'=>$schedule['tsd_enddate'],
                            'tsd_venue'=>$schedule['tsd_venue']!=null ? $schedule['tsd_venue']:0,
                            'tsd_lecturer'=>$schedule['tsd_lecturer']!=null ? $schedule['tsd_lecturer']:0,
                            'tsd_upddate'=>$schedule['tsd_upddate'],
                            'tsd_updby'=>$schedule['tsd_updby'],
                            'upddate'=>date('Y-m-d H:i:s'),
                            'updby'=>$userId
                        );
                        $model->insertScheduleDetailHistory($dataHis);
                    }
                }else{ //schedule senang
                    $data = array(
                        'IdLecturer'=>$formData['lecturer'],
                        'sc_day'=>date('l', strtotime($formData['date'])),
                        'sc_start_time'=>$formData['starttime'],
                        'sc_end_time'=>$formData['endtime'],
                        'sc_venue'=>$formData['venue']=='' ? 0:$formData['venue'],
                        'sc_date'=>date('Y-m-d', strtotime($formData['date'])),
                        'sc_createdby'=>$userId,
                        'sc_createddt'=>date('Y-m-d H:i:s')
                    );
                    $model->updateSchedule($data, $schedule['sc_id']);

                    //history
                    $dataHis = array(
                        'sc_id'=>$schedule['sc_id'],
                        'idGroup'=>$schedule['idGroup'],
                        'IdLecturer'=>$schedule['IdLecturer']!=null ? $schedule['IdLecturer']:0,
                        'idCollege'=>$schedule['idCollege'],
                        'idBranch'=>$schedule['idBranch'],
                        'idClassType'=>$schedule['idClassType'],
                        'sc_day'=>$schedule['sc_day'],
                        'sc_start_time'=>$schedule['sc_start_time'],
                        'sc_end_time'=>$schedule['sc_end_time'],
                        'sc_venue'=>$schedule['sc_venue']!=null ? $schedule['sc_venue']:0,
                        'sc_class'=>$schedule['sc_class'],
                        'sc_date'=>$schedule['sc_date'],
                        'sc_remark'=>$schedule['sc_remark'],
                        'sc_status'=>$schedule['sc_status'],
                        'sc_createdby'=>$userId,
                        'sc_createddt'=>date('Y-m-d H:i:s')
                    );
                    $model->insertScheduleHistory($dataHis);

                    //update qp event
                    $getQpEvents = $scheduleDB->getQpEvent($formData['groupid'], $schedule['sc_id']);

                    if ($getQpEvents){
                        foreach($getQpEvents as $getQpEvent){
                            //insert qp_event history
                            $qpHistoryData = array(
                                'event_id'=>$getQpEvent['id'],
                                'venue'=>$getQpEvent['venue'],
                                'start_day'=>$getQpEvent['start_day'],
                                'end_day'=>$getQpEvent['end_day'],
                                'start_time'=>$getQpEvent['start_time'],
                                'end_time'=>$getQpEvent['end_time'],
                                'date_created'=>$getQpEvent['date_created'],
                                'last_updated'=>$getQpEvent['last_updated'],
                                'status'=>$getQpEvent['status'],
                                'class_duration'=>$getQpEvent['class_duration'],
                                'lecturer_code'=>$getQpEvent['lecturer_code'],
                                'lecturer_name'=>$getQpEvent['lecturer_name'],
                                'lecturer_email'=>$getQpEvent['lecturer_email'],
                                'class_date'=>$getQpEvent['class_date'],
                                'class_day'=>$getQpEvent['class_day'],
                                'class_start_time'=>$getQpEvent['class_start_time'],
                                'class_end_time'=>$getQpEvent['class_end_time'],
                                'sequence_no'=>$getQpEvent['sequence_no'],
                                'field_change1'=>$getQpEvent['field_change1'],
                                'field_change2'=>$getQpEvent['field_change2'],
                                'field_change3'=>$getQpEvent['field_change3'],
                                'field_change4'=>$getQpEvent['field_change4'],
                                'field_change5'=>$getQpEvent['field_change5'],
                                'field_change6'=>$getQpEvent['field_change6'],
                                'field_change7'=>$getQpEvent['field_change7']
                            );
                            $scheduleDB->insertQpEventHistory($qpHistoryData);

                            $classstarttime = $formData["starttime"];
                            $classendtime = $formData["endtime"];

                            if ($schedule["sc_date"]=='0000-00-00' || $schedule["sc_date"]==null){
                                $start_day = 0;
                                $end_day = 0;
                            }else{
                                $start_day = strtotime($formData["date"].' '.$classstarttime);
                                $end_day = strtotime($formData["date"].' '.$classendtime);
                            }

                            if ($getQpEvent['program_mode']=='Full Time'){
                                $program_mode = 2;
                            }else{
                                $program_mode = 1;
                            }

                            $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                            $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

                            $venue = $scheduleDB->getDefination($formData["venue"]);

                            $lecturer = $scheduleDB->getLecturer($formData["lecturer"]);

                            //status update
                            $field_change1 = 0;
                            $field_change2 = 0;
                            $field_change3 = 0;
                            $field_change4 = 0;
                            $field_change5 = 0;
                            $cstatus = 0;

                            if ($start_day != $getQpEvent['start_day']){
                                $field_change1 = 1;
                                $cstatus = 1;
                            }

                            if ($data["sc_day"] != $getQpEvent['class_day']){
                                $field_change2 = 1;
                                $cstatus = 1;
                            }

                            if ($start_time != $getQpEvent['start_time'] || $end_time != $getQpEvent['end_time']){
                                $field_change3 = 1;
                                $cstatus = 1;
                            }

                            if ($venue['DefinitionDesc'] != $getQpEvent['venue']){
                                $field_change4 = 1;
                                $cstatus = 1;
                            }

                            if ($formData["idLecturer"] != $getQpEvent['lecturer_code']){
                                $field_change5 = 1;
                                $cstatus = 1;
                            }

                            $qpData = array(
                                'start_day'=>$start_day,
                                'end_day'=>$end_day,
                                'class_date'=>$start_day,
                                'class_day'=>date('l', strtotime($formData['date'])),
                                'start_time'=>$start_time,
                                'end_time'=>$end_time,
                                'class_start_time'=>($classstarttime*60),
                                'class_end_time'=>($classendtime*60),
                                'venue'=>$venue['DefinitionDesc'],
                                'lecturer_code'=>$formData["lecturer"],
                                'lecturer_name'=>$lecturer['FullName'],
                                'lecturer_email'=>$lecturer['Email'],
                                'status'=>$cstatus,
                                'last_updated'=>date('Y-m-d H:i:s'),
                                'field_change1'=>$field_change1,
                                'field_change2'=>$field_change2,
                                'field_change3'=>$field_change3,
                                'field_change4'=>$field_change4,
                                'field_change5'=>$field_change5
                            );
                            $scheduleDB->updateQpEvent($qpData, $getQpEvent['id']);
                        }
                    }
                }

                $scheduleData = array(
                    'ctd_scdate'=>date('Y-m-d', strtotime($formData['date']))
                );
                $model->updateAttendanceDate($scheduleData, $formData['groupid'], $scdate);
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Schedule has been updated.'));
            $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/notification/groupid/'.$formData['groupid'].'/date/'.$formData['scdate'].'/datenew/'.(isset($formData['date']) ? strtotime($formData['date']):$formData['scdate']).'/type/'.$formData['mtype']);
        }
    }

    public function notificationAction(){
        $groupid = $this->_getParam('groupid',null);
        $date = $this->_getParam('date',null);
        $datenew = $this->_getParam('datenew',null);
        $type = $this->_getParam('type',null);

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $this->view->title = $this->view->translate('Send Notification');

        $model = new GeneralSetup_Model_DbTable_Attendance();
        $scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();

        $groupInfo = $model->getGoupInfo($groupid);
        $studentList = $model->getGroupStudent($groupid);

        $this->view->groupInfo = $groupInfo;
        $this->view->studentList = $studentList;

        $this->view->groupid = $groupid;
        $this->view->date = $date;

        $scdate = date('Y-m-d', $date);
        $day = date('l', $date);

        $scdatenew = date('Y-m-d', $datenew);
        $daynew = date('l', $datenew);

        $schedule = $model->getSchedule($groupid, $scdatenew);

        $content = '';

        if (!$schedule) { //schedule susah
            $schedule = $model->getSchedule3($groupid, $scdatenew);

            if (!$schedule) {
                $schedule = $model->getSchedule2($groupid, $day);

                $populateData = array(
                    'date'=>date('d-m-Y', $date),
                    'starttime'=>$schedule['sc_start_time'],
                    'endtime'=>$schedule['sc_end_time'],
                    'venue'=>$schedule['sc_venue'],
                    'lecturer'=>$schedule['IdLecturer']
                );

                $lecturerInfo = $this->model->getLecturer($populateData['lecturer']);
                $venueInfo = $this->model->getDefinationById($populateData['venue']);

                if ($type == 1032){ //cancel
                    $content = 'The following class has been Cancelled: <br />';
                    $content .= '<br />';
                    $content .= '<table width="100%">';
                    $content .= '<tr>';
                    $content .= '<td>Class</td><td>:</td><td>'.$groupInfo['GroupName'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Date</td><td>:</td><td>'.$populateData['date'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Day</td><td>:</td><td>'.date('l', strtotime($populateData['date'])).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($populateData['starttime']))).' to '.strtoupper(date('h:i a', strtotime($populateData['endtime']))).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($lecturerInfo['FullName']) ? $lecturerInfo['FullName']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Venue</td><td>:</td><td>'.(isset($venueInfo['DefinitionDesc']) ? $venueInfo['DefinitionDesc']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '</table>';
                }else{ //postpone
                    $content = 'Class '.$groupInfo['GroupName'].' on '.$populateData['date'].' has been postponed to a new date, please refer information below : <br />';
                    $content .= '<br />';
                    $content .= '<table width="100%">';
                    $content .= '<tr>';
                    $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Date</td><td>:</td><td>'.date('d-m-Y', $datenew).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Day</td><td>:</td><td>'.date('l', $datenew).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($populateData['starttime']))).' to '.strtoupper(date('h:i a', strtotime($populateData['endtime']))).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($lecturerInfo['FullName']) ? $lecturerInfo['FullName']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Venue</td><td>:</td><td>'.(isset($venueInfo['DefinitionDesc']) ? $venueInfo['DefinitionDesc']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '</table>';
                }
            }else{
                $populateData = array(
                    'date'=>date('d-m-Y', $date),
                    'starttime'=>$schedule['tsd_starttime'],
                    'endtime'=>$schedule['tsd_enddate'],
                    'venue'=>$schedule['tsd_venue'],
                    'lecturer'=>$schedule['tsd_lecturer']
                );

                $lecturerInfo = $this->model->getLecturer($populateData['lecturer']);
                $venueInfo = $this->model->getDefinationById($populateData['venue']);

                if ($type == 1032){ //cancel
                    $content = 'The following class has been Cancelled: <br />';
                    $content .= '<br />';
                    $content .= '<table width="100%">';
                    $content .= '<tr>';
                    $content .= '<td>Class</td><td>:</td><td>'.$groupInfo['GroupName'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Date</td><td>:</td><td>'.$populateData['date'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Day</td><td>:</td><td>'.date('l', strtotime($populateData['date'])).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($populateData['starttime']))).' to '.strtoupper(date('h:i a', strtotime($populateData['endtime']))).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($lecturerInfo['FullName']) ? $lecturerInfo['FullName']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Venue</td><td>:</td><td>'.(isset($venueInfo['DefinitionDesc']) ? $venueInfo['DefinitionDesc']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '</table>';
                }else{ //postpone
                    $content = 'Class '.$groupInfo['GroupName'].' on '.$populateData['date'].' has been postponed to a new date, please refer information below : <br />';
                    $content .= '<br />';
                    $content .= '<table width="100%">';
                    $content .= '<tr>';
                    $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Date</td><td>:</td><td>'.date('d-m-Y', $datenew).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Day</td><td>:</td><td>'.date('l', $datenew).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($populateData['starttime']))).' to '.strtoupper(date('h:i a', strtotime($populateData['endtime']))).'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($lecturerInfo['FullName']) ? $lecturerInfo['FullName']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                    $content .= '<td>Venue</td><td>:</td><td>'.(isset($venueInfo['DefinitionDesc']) ? $venueInfo['DefinitionDesc']:'').'</td>';
                    $content .= '</tr>';
                    $content .= '</table>';
                }
            }
        }else{ //schedule senang
            $populateData = array(
                'date'=>date('d-m-Y', $date),
                'starttime'=>$schedule['sc_start_time'],
                'endtime'=>$schedule['sc_end_time'],
                'venue'=>$schedule['sc_venue'],
                'lecturer'=>$schedule['IdLecturer']
            );

            $lecturerInfo = $this->model->getLecturer($populateData['lecturer']);
            $venueInfo = $this->model->getDefinationById($populateData['venue']);

            if ($type == 1032){ //cancel
                $content = 'The following class has been Cancelled: <br />';
                $content .= '<br />';
                $content .= '<table width="100%">';
                $content .= '<tr>';
                $content .= '<td>Class</td><td>:</td><td>'.$groupInfo['GroupName'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Date</td><td>:</td><td>'.$populateData['date'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Day</td><td>:</td><td>'.date('l', strtotime($populateData['date'])).'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($populateData['starttime']))).' to '.strtoupper(date('h:i a', strtotime($populateData['endtime']))).'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($lecturerInfo['FullName']) ? $lecturerInfo['FullName']:'').'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Venue</td><td>:</td><td>'.(isset($venueInfo['DefinitionDesc']) ? $venueInfo['DefinitionDesc']:'').'</td>';
                $content .= '</tr>';
                $content .= '</table>';
            }else{ //postpone
                $content = 'Class '.$groupInfo['GroupName'].' on '.$populateData['date'].' has been postponed to a new date, please refer information below : <br />';
                $content .= '<br />';
                $content .= '<table width="100%">';
                $content .= '<tr>';
                $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Date</td><td>:</td><td>'.date('d-m-Y', $datenew).'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Day</td><td>:</td><td>'.date('l', $datenew).'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($populateData['starttime']))).' to '.strtoupper(date('h:i a', strtotime($populateData['endtime']))).'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($lecturerInfo['FullName']) ? $lecturerInfo['FullName']:'').'</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td>Venue</td><td>:</td><td>'.(isset($venueInfo['DefinitionDesc']) ? $venueInfo['DefinitionDesc']:'').'</td>';
                $content .= '</tr>';
                $content .= '</table>';
            }
        }

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //send notification
            if (isset($formData['check']) && count($formData['check'])>0) {
                foreach ($formData['check'] as $id => $on) {
                    $this->sendEmail($id, $formData['content_ide'], $type);
                }
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Notification has been sent'));
            $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/manage-class/id/'.$groupid);
        }

        $this->view->content = $content;
    }

    private function sendEmail($id, $content, $type){

        $vmodel = new Records_Model_DbTable_Visitingstudent();

        $cmsTags = new Cms_TemplateTags();

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');

        $student = $this->model->getProfile($id);

        $email_receipient = $student["appl_email"];
        $email_personal_receipient = $student["appl_email_personal"];
        $name_receipient  = $student["appl_fname"].' '.$student["appl_lname"];

        $emailDb = new App_Model_System_DbTable_Email();

        if ($type == 1032){ //cancel
            $subject = 'Class Cancelled';
        }else if ($type == 1033){ //postpone
            $subject = 'Class Postponed';
        }else{ //add
            $subject = 'New Class';
        }

        //email personal
        $data_inceif = array(
            'recepient_email' => $email_personal_receipient,
            'subject' => $subject,
            'content' => $content,
            'attachment_path'=>null,
            'attachment_filename'=>null
        );

        //add email to email que
        $email_id2 = $emailDb->addData($data_inceif);

        //save to history
        $data_compose = array(
            'comp_subject'=> $subject,
            'comp_module' => 'records',
            'comp_type' => 585,
            'comp_tpl_id' => 0,
            'comp_rectype' => 'students',
            'comp_lang' => 'en_US',
            'comp_totalrecipients' => 1,
            'comp_rec' => $student['IdStudentRegistration'],
            'comp_content' => $content,
            'created_by' => $this->auth->getIdentity()->iduser,
            'created_date' => date('Y-m-d H:i:s')
        );
        $comId = $vmodel->insertCommpose($data_compose);

        $data_compose_rec = array(
            'cr_comp_id' => $comId,
            'cr_subject' => $subject,
            'cr_content' => $content,
            'cr_rec_id' => $student['IdStudentRegistration'],
            'cr_email' => $email_personal_receipient,
            'cr_status' => 1,
            'cr_datesent' => date('Y-m-d H:i:s')
        );
        $vmodel->insertComposeRec($data_compose_rec);
    }

    public function historyAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $this->view->title = $this->view->translate('Class History');

        $model = new GeneralSetup_Model_DbTable_Attendance();
        $scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();

        $groupId = $this->_getParam('groupid', 0);
        $date = $this->_getParam('date', null);
        $scid = $this->_getParam('scid', 0);

        $this->view->groupid = $groupId;
        $this->view->date = $date;

        $scdate = date('Y-m-d', $date);
        $day = date('l', $date);

        $schedule = $model->getSchedule($groupId, $scdate);

        $hisList = array();
        if (!$schedule) { //schedule susah
            $schedule = $model->getSchedule3($groupId, $scdate);

            if ($schedule) {
                $classType = $this->model->getSchedule($scid);

                $hisList2 = $this->model->getClassHistory2($schedule['tsd_id']);

                if ($hisList2){
                    foreach ($hisList2 as $hisList2Loop){
                        $hisArr = array(
                            'idGroup' =>  $groupId,
                            'IdLecturer' =>  $hisList2Loop['tsd_lecturer'],
                            'idClassType' =>  $hisList2Loop['tsd_lecturer'],
                            'sc_day' =>  date('l', strtotime($hisList2Loop['tsd_scdate'])),
                            'sc_start_time' =>  $hisList2Loop['tsd_starttime'],
                            'sc_end_time' =>  $hisList2Loop['tsd_enddate'],
                            'sc_venue' =>  $hisList2Loop['tsd_venue'],
                            'sc_date' =>  $hisList2Loop['tsd_scdate'],
                            'sc_createddt' => $hisList2Loop['upddate'],
                            'lecturername' => $hisList2Loop['lecturername'],
                            'classtype' =>  $classType['classtype'],
                            'veneuname' => $hisList2Loop['veneuname']
                        );

                        $hisList[]=$hisArr;
                    }
                }
            }
        }else{ //schedule senang
            $hisList = $this->model->getClassHistory($schedule['sc_id']);
        }

        $this->view->hisList = $hisList;
    }

    public function addClassAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $this->view->title = $this->view->translate('Manage Class Schedule');

        $id = $this->_getParam('id',null);
        $this->view->groupid = $id;

        $atmodel = new GeneralSetup_Model_DbTable_Attendance();
        $form = new GeneralSetup_Form_EditAttendanceDetails();

        $groupSchedule = $atmodel->getGroupSchedule($id, 1);
        $studentList = $atmodel->getGroupStudent($id);

        $scheduleType = 1;

        if ($groupSchedule){
            foreach ($groupSchedule as $key => $dateLoop){
                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    $populateArr = array(
                        'type'=>$dateLoop['idClassType']
                    );
                    $form->populate($populateArr);
                    $scheduleType = 0;
                }else{
                    $scheduleType = 1;
                }
            }
        }

        $groupInfo = $atmodel->getGoupInfo($id);

        $this->view->groupInfo = $groupInfo;
        $this->view->form = $form;
        $this->view->scheduleType = $scheduleType;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $check = $this->model->checkClassClash($id, date('Y-m-d', strtotime($formData['date'])));

            if ($check){
                //redirect here
                //$this->_helper->flashMessenger->addMessage(array('error' => 'Cannot add, class clash.'));
                //$this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/add-class/id/'.$id);
            }

            if ($scheduleType == 1){
                $newscheduledata = array(
                    'idGroup'=>$id,
                    'IdLecturer'=>$formData['lecturer'],
                    'idBranch'=>1,
                    'idClassType'=>$formData['type'],
                    'sc_day'=>date('l', strtotime($formData['date'])),
                    'sc_start_time'=>$formData['starttime'],
                    'sc_end_time'=>$formData['endtime'],
                    'sc_venue'=>$formData['venue'],
                    'sc_date'=>date('Y-m-d', strtotime($formData['date'])),
                    'sc_remark'=>'',
                    'sc_status'=>1,
                    'sc_createdby'=>$userId,
                    'sc_createddt'=>date('Y-m-d H:i:s')
                );
                $classid = $this->model->addClassSenang($newscheduledata);
            }else{
                $newscheduledata  = array(
                    'tsd_groupid'=>$id,
                    'tsd_scdate'=>date('Y-m-d', strtotime($formData['date'])),
                    'tsd_starttime'=>$formData['starttime'],
                    'tsd_enddate'=>$formData['endtime'],
                    'tsd_venue'=>$formData['venue'],
                    'tsd_lecturer'=>$formData['lecturer'],
                    'tsd_upddate'=>date('Y-m-d H:i:s'),
                    'tsd_updby'=>$userId
                );
                $classid = $this->model->addClassSusah($newscheduledata);

                $formData['type'] = $populateArr['type'];
            }

            if ($studentList){
                foreach ($studentList as $studentLoop2){
                    $data = array(
                        'ctd_groupid'=>$id,
                        'ctd_studentid'=>$studentLoop2['IdStudentRegistration'],
                        'ctd_item_type'=>$formData['type'],
                        'ctd_scdate'=>date('Y-m-d', strtotime($formData['date'])),
                        'ctd_status'=>395,
                        'ctd_reason'=>'',
                        'ctd_upddate'=>date('Y-m-d h:i:s'),
                        'ctd_updby'=>$userId
                    );
                    $atmodel->insertAttendance($data);
                }
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Class has been added.'));
            $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/add-class-notification/id/'.$id.'/classid/'.$classid);
        }
    }

    public function addClassNotificationAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        $this->view->title = $this->view->translate('Notification');

        $id = $this->_getParam('id',null);
        $classid = $this->_getParam('classid',null);

        $atmodel = new GeneralSetup_Model_DbTable_Attendance();

        $groupSchedule = $atmodel->getGroupSchedule($id, 1);
        $studentList = $atmodel->getGroupStudent($id);
        $groupInfo = $atmodel->getGoupInfo($id);

        $this->view->studentList = $studentList;
        $this->view->groupid = $id;
        $groupid = $id;

        $scheduleType = 1;

        if ($groupSchedule){
            foreach ($groupSchedule as $key => $dateLoop){
                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    $scheduleType = 0;
                }else{
                    $scheduleType = 1;
                }
            }
        }
        //var_dump($scheduleType);
        $content = '';
        if ($scheduleType == 1){ //schedule senang
            $schedule = $this->model->getClassSenang($classid);

            $content = 'New class has been added, please refer information below : <br />';
            $content .= '<br />';
            $content .= '<table width="100%">';
            $content .= '<tr>';
            $content .= '<td>Class</td><td>:</td><td>'.$groupInfo['GroupName'].'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Date</td><td>:</td><td>'.date('d-m-Y', strtotime($schedule['sc_date'])).'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Day</td><td>:</td><td>'.date('l', strtotime($schedule['sc_date'])).'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($schedule['sc_start_time']))).' to '.strtoupper(date('h:i a', strtotime($schedule['sc_end_time']))).'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($schedule['lecturername']) ? $schedule['lecturername']:'').'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Venue</td><td>:</td><td>'.(isset($schedule['veneuname']) ? $schedule['veneuname']:'').'</td>';
            $content .= '</tr>';
            $content .= '</table>';
        }else{ //schedule susah
            $schedule = $this->model->getClassSusah($classid);
            //var_dump($schedule);
            $content = 'New class has been added, please refer information below : <br />';
            $content .= '<br />';
            $content .= '<table width="100%">';
            $content .= '<tr>';
            $content .= '<td>Class</td><td>:</td><td>'.$groupInfo['GroupName'].'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Class Code</td><td>:</td><td>'.$groupInfo['GroupCode'].'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Course</td><td>:</td><td>'.$groupInfo['subjectname'].'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Date</td><td>:</td><td>'.date('d-m-Y', strtotime($schedule['tsd_scdate'])).'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Day</td><td>:</td><td>'.date('l', strtotime($schedule['tsd_scdate'])).'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Time</td><td>:</td><td>'.strtoupper(date('h:i a', strtotime($schedule['tsd_starttime']))).' to '.strtoupper(date('h:i a', strtotime($schedule['tsd_enddate']))).'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Lecturer</td><td>:</td><td>'.(isset($schedule['lecturername']) ? $schedule['lecturername']:'').'</td>';
            $content .= '</tr>';
            $content .= '<tr>';
            $content .= '<td>Venue</td><td>:</td><td>'.(isset($schedule['veneuname']) ? $schedule['veneuname']:'').'</td>';
            $content .= '</tr>';
            $content .= '</table>';
        }

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //send notification
            if (isset($formData['check']) && count($formData['check'])>0) {
                foreach ($formData['check'] as $studid => $on) {
                    $this->sendEmail($studid, $formData['content_ide'], 0);
                }
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Notification has been sent'));
            $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/manage-class/id/'.$groupid);
        }

        $this->view->content = $content;
    }

    public function mergeClassAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $mergearr = array();
            if (isset($formData['merge']) && count($formData['merge']) > 0){
                foreach ($formData['merge'] as $key => $value){
                    $mergearr[] = $key;
                }
            }

            $data = array(
                'tmg_groupid'=>implode("|",$mergearr),
                'tmg_quota'=>$formData['qoutah'],
                'tmg_updby'=>$userId,
                'tmg_upddate'=>date('Y-m-d H:i:s')
            );
            $mergeid = $this->model->insertClassMerge($data);

            if (count($mergearr) > 0){
                $data2 = array(
                    'merge_id'=>$mergeid,
                );
                $this->model->updateClass($data2, implode(",",$mergearr));
            }
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Class Merged'));
        $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/index');
        exit;
    }

    public function editMergeClassAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $data = array(
                'tmg_quota'=>$formData['qoutah2'],
                'tmg_updby'=>$userId,
                'tmg_upddate'=>date('Y-m-d H:i:s')
            );
            //var_dump($data); exit;
            $this->model->updateMergeClass($data, $formData['merge_id']);
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Qouta Updated.'));
        $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/index');
        exit;
    }

    public function removeMergeClassAction(){
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $mergeInfo = $this->model->getMergeClass($formData['merge_id']);
            $mergearr = explode('|', $mergeInfo['tmg_groupid']);

            $this->model->deleteMergeClass($formData['merge_id']);

            $data2 = array(
                'merge_id'=>0,
            );
            $this->model->updateClass($data2, implode(",",$mergearr));
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Merge Removed.'));
        $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/index');
        exit;
    }

    public function getMergeClassAction(){
        $id = $this->_getParam('id',0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $mergeInfo = $this->model->getMergeClass($id);

        if ($mergeInfo){
            $mergeArr = explode('|', $mergeInfo['tmg_groupid']);

            $grouList = $this->model->getClassById($mergeArr);
            //var_dump($grouList);
        }

        //exit;

        $json = Zend_Json::encode($grouList);

        echo $json;
        exit();
    }

    public function getSemesterAction(){
        $id = $this->_getParam('id',null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $semesterList = $this->model->getSemester($id);

        $json = Zend_Json::encode($semesterList);

        echo $json;
        exit();
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }

    public function activateClassAction(){
        $model = new GeneralSetup_Model_DbTable_Attendance();
        $scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();

        $groupId = $this->_getParam('groupid', 0);
        $date = $this->_getParam('date', null);

        $scdate = date('Y-m-d', $date);
        $day = date('l', $date);

        $schedule = $model->getSchedule($groupId, $scdate);

        if (!$schedule) { //schedule susah
            $scheduleCancelData = array(
                'ctd_class_status'=>1
            );
            $model->updateAttendanceDate($scheduleCancelData, $groupId, $scdate);
        }else{ //schedule senang
            $scheduleCancelData = array(
                'sc_status'=>1
            );
            $model->updateSchedule($scheduleCancelData, $schedule['sc_id']);

            $scheduleCancelData2 = array(
                'ctd_class_status'=>1
            );
            $model->updateAttendanceDate($scheduleCancelData2, $groupId, $scdate);
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Class Activated'));
        $this->_redirect($this->baseUrl . '/generalsetup/manage-class-schedule/manage-class/id/'.$groupId);
        exit;
    }
}